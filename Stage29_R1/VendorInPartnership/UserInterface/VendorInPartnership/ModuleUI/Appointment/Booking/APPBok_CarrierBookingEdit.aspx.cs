﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Languages.Languages;
using BusinessEntities.ModuleBE.OnTimeInFull.CountrySetting;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Upload;
using Utilities;
using WebUtilities;
using System.Text;

public partial class APPBok_CarrierBookingEdit : CommonPage
{
    bool isDoorTypeNotChanged = false;
    bool isDoorCheckPopup = false;
    protected string BK_ALT_CorrectVendor = WebCommon.getGlobalResourceValue("BK_ALT_CorrectVendor");
    protected string BK_ALT_Wait = WebCommon.getGlobalResourceValue("BK_ALT_Wait");
    protected string BK_ALT_04_INT = WebCommon.getGlobalResourceValue("BK_MS_04_INT");
    protected string BK_MS_04_INT_Carrier = WebCommon.getGlobalResourceValue("BK_MS_04_INT_Carrier");
    protected string BK_ALT_TimeNotAvailable = WebCommon.getGlobalResourceValue("BK_ALT_TimeNotAvailable");
    protected string BK_ALT_MoveBooking = WebCommon.getGlobalResourceValue("BK_ALT_MoveBooking");
    protected string BK_ALT_Selected_Time = WebCommon.getGlobalResourceValue("BK_ALT_Selected_Time");
    protected string BK_ALT_Selected_Door = WebCommon.getGlobalResourceValue("BK_ALT_Selected_Door");
    protected string BK_ALT_Insufficeint_Space_Ext = WebCommon.getGlobalResourceValue("BK_ALT_Insufficeint_Space_Ext");
    protected string BK_ALT_Insufficeint_Space_Int = WebCommon.getGlobalResourceValue("BK_ALT_Insufficeint_Space_Int");
    protected string AtDoorName = WebCommon.getGlobalResourceValue("AtDoorName");
    string strIsVendorRequiredForSeparateBooking = WebCommon.getGlobalResourceValue("IsVendorRequiredForSeparateBooking");
    //-----------Start Stage 9 point 2a/2b--------------//
    protected string confirmprovisionalbooking = WebCommon.getGlobalResourceValue("confirmprovisionalbooking");
    protected string MesgConfirmAlternateTime = WebCommon.getGlobalResourceValue("MesgConfirmAlternateTime");
    //-----------End Stage 9 point 2a/2b--------------//

    /// <summary>
    /// view
    /// </summary>
    protected string BK_Old_View = WebCommon.getGlobalResourceValue("BookingOldView");
    protected string BK_Modern_View = WebCommon.getGlobalResourceValue("BookingModernView");

    string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/Security/Login.aspx";
    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    string templatePathName = @"/EmailTemplates/Appointment/";

    static int intTotalPallets = 0;
    static int intTotalCartons = 0;
    static int intTotalLines = 0;
    string sVendorCarrierNameNew = string.Empty;
    string AllocationType = string.Empty;
    string SiteDoorNumberID = string.Empty;

    string RTotalPallets = string.Empty;
    string RTotalCartons = string.Empty;
    string RTotalLifts = string.Empty;
    string RTotalLines = string.Empty;
    DataTable dtAlternateSlot;
  
    public class AlternateSlot
    {
        public string ArrSlotTime { get; set; }
        public string EstSlotTime { get; set; }
        public string Vendor { get; set; }
        public string Pallets { get; set; }
        public string Cartoons { get; set; }
        public string Lines { get; set; }
        public string Lifts { get; set; }
        public string Color { get; set; }
        public string DoorNo { get; set; }
        public string FixedSlotID { get; set; }
        public string SlotTimeID { get; set; }
        public string SiteDoorNumberID { get; set; }
        public string SupplierType { get; set; }
        public string AllocationType { get; set; }
        public string ArrSlotTime15min { get; set; }
        public string EstSlotTime15min { get; set; }
        public string DoorNameDisplay { get; set; }
        public string DoorNoDisplay { get; set; }
        public string VendorDisplay { get; set; }
        public string SlotStartDay { get; set; }
        public string siTimeSlotOrderBYID { get; set; }
        public string EstSlotTimeDisplay { get; set; }
        public string BookingID { get; set; }
        public string BookingStatus { get; set; }
        public string BookingStatusID { get; set; }
        public string Weekday { get; set; }
        public string Issue { get; set; }
        public string Carrier { get; set; }

    }
    //APPBOK_BookingBAL APPBOK_BookingBAL

    //--- Sprint 3b ---Slot time length---//
    public int iSlotTimeLength = 15;
    //-----------------------------------//

    public bool? IsBookingValidationExcluded
    {
        get
        {
            return (Convert.ToBoolean(IsBookingValidationExcludedCarrier) || Convert.ToBoolean(IsBookingValidationExcludedDelivery));
        }
        set
        {
            ViewState["IsBookingValidationExcluded"] = value;
        }
    }

    public bool? IsBookingValidationExcludedDelivery
    {
        get
        {
            return ViewState["IsBookingValidationExcludedDelivery"] != null ? Convert.ToBoolean(ViewState["IsBookingValidationExcludedDelivery"]) : false;
        }
        set
        {
            ViewState["IsBookingValidationExcludedDelivery"] = value;
        }
    }

    public bool? IsBookingValidationExcludedCarrier
    {
        get
        {
            return ViewState["IsBookingValidationExcludedCarrier"] != null ? Convert.ToBoolean(ViewState["IsBookingValidationExcludedCarrier"]) : false;
        }
        set
        {
            ViewState["IsBookingValidationExcludedCarrier"] = value;
        }
    }

    public bool? IsNonTime
    {
        get
        {
            return ViewState["IsNonTime"] != null ? Convert.ToBoolean(ViewState["IsNonTime"]) : false;
        }
        set
        {
            ViewState["IsNonTime"] = value;
        }
    }

    public bool? IsNonTimeBooking
    {
        get
        {
            return ViewState["IsNonTimeBooking"] != null ? Convert.ToBoolean(ViewState["IsNonTimeBooking"]) : false;
        }
        set
        {
            ViewState["IsNonTimeBooking"] = value;
        }
    }

    public bool? IsNonTimeCarrier
    {
        get
        {
            return ViewState["IsNonTimeCarrier"] != null ? Convert.ToBoolean(ViewState["IsNonTimeCarrier"]) : false;
        }
        set
        {
            ViewState["IsNonTimeCarrier"] = value;
        }
    }

    public bool? isPostBack
    {
        get
        {
            return ViewState["isPostBack"] != null ? Convert.ToBoolean(ViewState["isPostBack"]) : false;
        }
        set
        {
            ViewState["isPostBack"] = value;
        }
    }

    public bool? IsPreAdviseNoteRequired
    {
        get
        {
            return ViewState["IsPreAdviseNoteRequired"] != null ? Convert.ToBoolean(ViewState["IsPreAdviseNoteRequired"]) : false;
        }
        set
        {
            ViewState["IsPreAdviseNoteRequired"] = value;
        }
    }

    public string strDeliveryValidationExc
    {
        get
        {
            return ViewState["strDeliveryValidationExc"] != null ? ViewState["strDeliveryValidationExc"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strDeliveryValidationExc"] = value;
        }
    }

    public string strDeliveryNonTime
    {
        get
        {
            return ViewState["strDeliveryNonTime"] != null ? ViewState["strDeliveryNonTime"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strDeliveryNonTime"] = value;
        }
    }

    public string strCarrierValidationExc
    {
        get
        {
            return ViewState["strCarrierValidationExc"] != null ? ViewState["strCarrierValidationExc"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strCarrierValidationExc"] = value;
        }
    }

    public string CarrierNarrativeRequired
    {
        get
        {
            return ViewState["CarrierNarrativeRequired"] != null ? ViewState["CarrierNarrativeRequired"].ToString() : string.Empty;
        }
        set
        {
            ViewState["CarrierNarrativeRequired"] = value;
        }
    }

    public string strVehicleDoorTypeMatrix
    {
        get
        {
            return ViewState["strVehicleDoorTypeMatrix"] != null ? ViewState["strVehicleDoorTypeMatrix"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strVehicleDoorTypeMatrix"] = value;
        }
    }

    public string strVehicleDoorTypeMatrixSKU
    {
        get
        {
            return ViewState["strVehicleDoorTypeMatrixSKU"] != null ? ViewState["strVehicleDoorTypeMatrixSKU"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strVehicleDoorTypeMatrixSKU"] = value;
        }
    }

    public string VehicleNarrativeRequired
    {
        get
        {
            return ViewState["VehicleNarrativeRequired"] != null ? ViewState["VehicleNarrativeRequired"].ToString() : string.Empty;
        }
        set
        {
            ViewState["VehicleNarrativeRequired"] = value;
        }
    }

    public int? PreSiteCountryID
    {
        get
        {
            return ViewState["PreSiteCountryID"] != null ? Convert.ToInt32(ViewState["PreSiteCountryID"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["PreSiteCountryID"] = value;
        }
    }

    public NameValueCollection ArrDoorTypePriority
    {
        get
        {
            return ViewState["ArrDoorTypePriority"] != null ? (NameValueCollection)ViewState["ArrDoorTypePriority"] : null;
        }
        set
        {
            ViewState["ArrDoorTypePriority"] = value;
        }
    }

    public NameValueCollection ArrDoorTypePrioritySKU
    {
        get
        {
            return ViewState["ArrDoorTypePrioritySKU"] != null ? (NameValueCollection)ViewState["ArrDoorTypePrioritySKU"] : null;
        }
        set
        {
            ViewState["ArrDoorTypePrioritySKU"] = value;
        }
    }
    public NameValueCollection MsgSupressed
    {
        get
        {
            return ViewState["MsgSupressed"] != null ? (NameValueCollection)ViewState["MsgSupressed"] : null;
        }
        set
        {
            ViewState["MsgSupressed"] = value;
        }
    }

    public string Lines
    {
        get
        {
            return ViewState["Lines"] != null ? ViewState["Lines"].ToString() : "XX";
        }
        set
        {
            ViewState["Lines"] = value;
        }
    }

    public string TotalLines
    {
        get
        {
            return ViewState["TotalLines"] != null ? ViewState["TotalLines"].ToString() : "XX";
        }
        set
        {
            ViewState["TotalLines"] = value;
        }
    }

    public int? NonTimeDeliveryCartonVolume
    {
        get
        {
            return ViewState["NonTimeDeliveryCartonVolume"] != null ? Convert.ToInt32(ViewState["NonTimeDeliveryCartonVolume"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["NonTimeDeliveryCartonVolume"] = value;
        }
    }

    public int? NonTimeDeliveryPalletVolume
    {
        get
        {
            return ViewState["NonTimeDeliveryPalletVolume"] != null ? Convert.ToInt32(ViewState["NonTimeDeliveryPalletVolume"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["NonTimeDeliveryPalletVolume"] = value;
        }
    }

    IDictionary<string, CarrierDetails> SlotDoorCounter = new Dictionary<string, CarrierDetails>();

    public int BookedPallets
    {
        get
        {
            return ViewState["BookedPallets"] != null ? Convert.ToInt32(ViewState["BookedPallets"]) : 0;
        }
        set
        {
            ViewState["BookedPallets"] = value;
        }
    }

    public int BookedLines
    {
        get
        {
            return ViewState["BookedLines"] != null ? Convert.ToInt32(ViewState["BookedLines"]) : 0;
        }
        set
        {
            ViewState["BookedLines"] = value;
        }
    }

    public DateTime LogicalSchedulDateTime
    {
        get
        {
            return ViewState["LogicalSchedulDateTime"] != null ? Convert.ToDateTime(ViewState["LogicalSchedulDateTime"].ToString()) : new DateTime(1900, 01, 01, 0, 0, 00);
        }
        set
        {
            ViewState["LogicalSchedulDateTime"] = value;
        }
    }

    public DateTime ActualSchedulDateTime
    {
        get
        {
            return ViewState["ActualSchedulDateTime"] != null ? Convert.ToDateTime(ViewState["ActualSchedulDateTime"].ToString()) : new DateTime(1900, 01, 01, 0, 0, 00);
        }
        set
        {
            ViewState["ActualSchedulDateTime"] = value;
        }
    }

    public bool isProvisional
    {
        get
        {
            return ViewState["isProvisional"] != null ? Convert.ToBoolean(ViewState["isProvisional"]) : false;
        }
        set
        {
            ViewState["isProvisional"] = value;
        }
    }

    public string ProvisionalReason
    {
        get
        {
            return ViewState["ProvisionalReason"] != null ? Convert.ToString(ViewState["ProvisionalReason"]) : "";
        }
        set
        {
            ViewState["ProvisionalReason"] = value;
        }
    }

    public int? ProvisionalReasonType
    {
        get
        {
            return ViewState["ProvisionalReasonType"] != null ? Convert.ToInt32(ViewState["ProvisionalReasonType"].ToString()) : (int?)null;
        }
        set
        {
            ViewState["ProvisionalReasonType"] = value;
        }
    }

    public enum BookingType
    {
        FixedSlot = 1, Unscheduled = 2, NonTimedDelivery = 3, BookedSlot = 4, Provisional = 5, ProvisionalNonTimedDelivery = 6
    };

    public string TotalPallets
    {
        get
        {
            return ViewState["TotalPallets"] != null ? ViewState["TotalPallets"].ToString() : "0";
        }
        set
        {
            ViewState["TotalPallets"] = value;
        }
    }

    public string TotalCartons
    {
        get
        {
            return ViewState["TotalCartons"] != null ? ViewState["TotalCartons"].ToString() : "0";
        }
        set
        {
            ViewState["TotalCartons"] = value;
        }
    }

    public string BeforeSlotTimeID
    {
        get
        {
            return ViewState["BeforeSlotTimeID"] != null ? ViewState["BeforeSlotTimeID"].ToString() : "0";
        }
        set
        {
            ViewState["BeforeSlotTimeID"] = value;
        }
    }

    public string AfterSlotTimeID
    {
        get
        {
            return ViewState["AfterSlotTimeID"] != null ? ViewState["AfterSlotTimeID"].ToString() : "300";
        }
        set
        {
            ViewState["AfterSlotTimeID"] = value;
        }
    }

    //----Stage 10 Point 10 L 5.2-----//
    public string VendorBeforeSlotTimeID
    {
        get
        {
            return ViewState["VendorBeforeSlotTimeID"] != null ? ViewState["VendorBeforeSlotTimeID"].ToString() : "0";
        }
        set
        {
            ViewState["VendorBeforeSlotTimeID"] = value;
        }
    }

    public string VendorAfterSlotTimeID
    {
        get
        {
            return ViewState["VendorAfterSlotTimeID"] != null ? ViewState["VendorAfterSlotTimeID"].ToString() : "300";
        }
        set
        {
            ViewState["VendorAfterSlotTimeID"] = value;
        }
    }

    public string strOpenDoorIds
    {
        get
        {
            return ViewState["strOpenDoorIds"] != null ? ViewState["strOpenDoorIds"].ToString() : ",";
        }
        set
        {
            ViewState["strOpenDoorIds"] = value;
        }
    }

    static int iTotalPalletsglobal = 0;
    static int iTotalCartonsglobal = 0;
    static int iTotalLinesglobal = 0;

    //----Stage 10 Point 10 L 5.1-----//
    public string IsVehicleContainer
    {
        get
        {
            return ViewState["IsVehicleContainer"] != null ? ViewState["IsVehicleContainer"].ToString() : string.Empty;
        }
        set
        {
            ViewState["IsVehicleContainer"] = value;
        }
    }

    public bool isContainerError
    {
        get
        {
            return ViewState["isContainerError"] != null ? Convert.ToBoolean(ViewState["isContainerError"]) : false;
        }
        set
        {
            ViewState["isContainerError"] = value;
        }
    }
    //--------------------------//


    public int? RemainingPallets
    {
        get
        {
            return ViewState["RemainingPallets"] != null ? Convert.ToInt32(ViewState["RemainingPallets"]) : (int?)null;
        }
        set
        {
            ViewState["RemainingPallets"] = value;
        }
    }

    public int? RemainingLines
    {
        get
        {
            return ViewState["RemainingLines"] != null ? Convert.ToInt32(ViewState["RemainingLines"]) : (int?)null;
        }
        set
        {
            ViewState["RemainingLines"] = value;
        }
    }

    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSourceCountry.CurrentPage = this;
        ddlSourceCountry.IsAllRequired = false;
        ddlSite.CurrentPage = this;
        ddlSite.TimeSlotWindow = "S";
        txtSchedulingdate.CurrentPage = this;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            pnlPalletCheckforCountryDisplay.Visible = chkSiteSetting.Checked;

            hdnCurrentMode.Value = GetQueryStringValue("Mode").ToString();

            if (GetQueryStringValue("FixedSlot") == null)
                hdnFixedSlotMode.Value = "false";
            else if (GetQueryStringValue("FixedSlot") == "True")
                hdnFixedSlotMode.Value = "true";

                hdnLogicalScheduleDate.Value = GetQueryStringValue("Logicaldate").ToString();
                hdnActualScheduleDate.Value = GetQueryStringValue("Scheduledate").ToString();
                LogicalSchedulDateTime = Utilities.Common.TextToDateFormat(hdnLogicalScheduleDate.Value);
                ActualSchedulDateTime = Utilities.Common.TextToDateFormat(hdnActualScheduleDate.Value);

        }

        if (!Page.IsPostBack)
        {

            if (GetQueryStringValue("SiteCountryID") == null)
            {
                hdnActualScheduleDate.Value = string.Empty; // DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                ddlSite.innerControlddlSite.AutoPostBack = true;
                PreSiteCountryID = Convert.ToInt32(Session["SiteCountryID"]);
            }
            else
            {
                ddlSite.innerControlddlSite.AutoPostBack = true;
                PreSiteCountryID = Convert.ToInt32(GetQueryStringValue("SiteCountryID").ToString());
            }

            if (hdnCurrentMode.Value == "Edit")
            {
                APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
                APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

                oAPPBOK_BookingBE.Action = "GetCarrierBookingDeatilsByID";
                oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());

                List<APPBOK_BookingBE> lstBookingDeatils = new List<APPBOK_BookingBE>();
                lstBookingDeatils = oAPPBOK_BookingBAL.GetCarrierBookingDetailsBAL(oAPPBOK_BookingBE);

                if (lstBookingDeatils != null && lstBookingDeatils.Count > 0)
                {
                    #region Country Pallet Check
                    CountryData.Visible = lstBookingDeatils[0].ISPM15CountryPalletChecking;
                    pnlPalletCheckforCountryDisplay.Visible = lstBookingDeatils[0].ISPM15CountryPalletChecking;
                    lblYesNo.Text = lstBookingDeatils[0].ISPM15CountryPalletChecking ? "Yes" : "No";
                    chkSiteSetting.Checked = lstBookingDeatils[0].ISPM15CountryPalletChecking;
                    ddlSourceCountry.innerControlddlCountry.SelectedValue = lstBookingDeatils[0].ISPM15FromCountryID.ToString();
                    lblFromCountryValue.Text = ddlSourceCountry.innerControlddlCountry.SelectedItem.Text;
                    #endregion

                    hdnBookingRef.Value = lstBookingDeatils[0].BookingRef;

                    Session["bref"] = lstBookingDeatils[0].BookingRef;

                    // stage 15 R2 point no 17
                    hdnIsBookingAccepted.Value = Convert.ToString(lstBookingDeatils[0].IsBookingAccepted);
                    //

                    if (lstBookingDeatils[0].BookingTypeID == 3) //Non time booking
                        IsNonTimeBooking = true;
                    else if (lstBookingDeatils[0].BookingTypeID == 5)  //Stage 9 point 2a/2b
                        hdnisProvisionalBooking.Value = "true";

                    spCarrier.Style.Add("display", "none");
                    hdnProvisionalReason.Value = lstBookingDeatils[0].ProvisionalReason;
                    hdnBookingTypeID.Value = Convert.ToString(lstBookingDeatils[0].BookingTypeID);
                    hdnLine.Value = Convert.ToString(lstBookingDeatils[0].OldNumberOfLines);
                    ltCarrierName.Visible = true;
                    ltCarrierName.Text = lstBookingDeatils[0].Carrier.CarrierName;


                    hdnSuggestedSlotTimeID.Value = lstBookingDeatils[0].SlotTime.SlotTimeID.ToString();

                    hdnSuggestedFixedSlotID.Value = lstBookingDeatils[0].FixedSlot.FixedSlotID.ToString();

                    ActivateFistView();

                    ddlDeliveryType.SelectedIndex = ddlDeliveryType.Items.IndexOf(ddlDeliveryType.Items.FindByValue(lstBookingDeatils[0].Delivery.DeliveryTypeID.ToString()));
                    ddlDeliveryType_SelectedIndexChanged(sender, e);
                    //ddlCarrier_SelectedIndexChanged(sender, e);
                    //ddlVehicleType_SelectedIndexChanged(sender, e);


                    ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(lstBookingDeatils[0].Carrier.CarrierID.ToString()));

                    ddlVehicleType.SelectedIndex = ddlVehicleType.Items.IndexOf(ddlVehicleType.Items.FindByValue(lstBookingDeatils[0].VehicleType.VehicleTypeID.ToString()));
                    hdnPreviousVehicleType.Value = lstBookingDeatils[0].VehicleType.VehicleTypeID.ToString();

                    ddlCarrier_SelectedIndexChanged(sender, e);
                    if (lstBookingDeatils[0].OtherCarrier != string.Empty)
                    {

                        txtCarrierOther.Style.Add("display", "");
                        //lblCarrierAdvice.Style.Add("display", "");
                        rfvCarrierOtherRequired.Enabled = true;
                        txtCarrierOther.Text = lstBookingDeatils[0].OtherCarrier;
                    }

                    ddlVehicleType_SelectedIndexChanged(sender, e);
                    if (lstBookingDeatils[0].OtherVehicle != string.Empty)
                    {

                        txtVehicleOther.Style.Add("display", "");
                        //lblVehicleAdvice.Style.Add("display", "");
                        rfvVehicleTypeOtherRequired.Enabled = true;
                        txtVehicleOther.Text = lstBookingDeatils[0].OtherVehicle;
                    }

                    ltCarrier.Text = ddlCarrier.SelectedItem.Text;
                    ltOtherCarrier.Text = ddlCarrier.SelectedItem.Text;
                    ltDeliveryType.Text = ddlDeliveryType.SelectedItem.Text;

                    //BIND ALREADY BOOKED SLOT DETAILS//
                    ltBookingDate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    ltSuggestedSlotTime.Text = lstBookingDeatils[0].SlotTime.SlotTime + " " + AtDoorName + " " + lstBookingDeatils[0].DoorNoSetup.DoorNumber.ToString();
                    
                    hdnSuggestedFixedSlotID.Value = lstBookingDeatils[0].FixedSlot.FixedSlotID.ToString();
                    hdnSuggestedSiteDoorNumberID.Value = lstBookingDeatils[0].DoorNoSetup.SiteDoorNumberID.ToString();
                    hdnSuggestedSiteDoorNumber.Value = lstBookingDeatils[0].DoorNoSetup.DoorNumber.ToString();

                    hdnSuggestedSlotTime.Value = lstBookingDeatils[0].SlotTime.SlotTime;
                    ltTimeSlot.Text = hdnSuggestedSlotTime.Value;
                    txtEditBookingCommentF.Text = Convert.ToString(lstBookingDeatils[0].BookingComments);
                    txtEditBookingComment.Text = Convert.ToString(lstBookingDeatils[0].BookingComments);


                    ltDoorNo.Text = hdnSuggestedSiteDoorNumber.Value;
                    ltTimeSlot_1.Text = lstBookingDeatils[0].SlotTime.SlotTime;
                    ltDoorNo_1.Text = hdnSuggestedSiteDoorNumber.Value;
                    hdnSuggestedWeekDay.Value = getWeekday(lstBookingDeatils[0].WeekDay);

                    ShowHazordousItem();
                    if (lstBookingDeatils[0].IsEnableHazardouesItemPrompt)
                    {
                        checkBoxEnableHazardouesItemPrompt.Checked = true;
                    }
                    else
                    {
                        checkBoxEnableHazardouesItemPrompt.Checked = false;
                    }

                    if (hdnCurrentMode.Value == "Edit")
                    {
                        // ProceedAddPO(); // btnProceedAddPO_Click(sender, e);
                        BindAllPOGrid();
                        BindVolumeDetailsGrid();
                        pnlBookingDetailsArrival.Visible = true;
                        mvBookingEdit.ActiveViewIndex = 0;
                    }

                    //-----------Start Stage 9 point 2a/2b--------------//
                    if (GetQueryStringValue("PNTB") != null && GetQueryStringValue("PNTB").ToString() == "True")
                    {
                        Session["BookingCreated"] = false;

                        BindAlternateViewVolume();
                        PaintAlternateSlotScreen();
                    }
                    if (lstBookingDeatils[0].BookingTypeID == 5 && hdnCurrentRole.Value != "Carrier")
                    {
                        if (hdnLine.Value != "0" && hdnProvisionalReason.Value == "EDITED" && Session["Role"].ToString().ToLower() != "carrier")
                        {
                            btnReject.Visible = false;
                            btnReject_1.Visible = false;
                        }
                        else
                        {
                            btnReject.Visible = true;
                            btnReject_1.Visible = true;
                        }

                    }
                    //-----------End Stage 9 point 2a/2b--------------//
                }
            }
            else if (hdnFixedSlotMode.Value == "true")
            {

                MASSIT_FixedSlotBE oMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();
                APPSIT_FixedSlotBAL oMASSIT_FixedSlotBAL = new APPSIT_FixedSlotBAL();

                oMASSIT_FixedSlotBE.Action = "ShowAll";
                oMASSIT_FixedSlotBE.FixedSlotID = Convert.ToInt32(GetQueryStringValue("ID"));
                oMASSIT_FixedSlotBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oMASSIT_FixedSlotBE.User.UserID = Convert.ToInt32(Session["UserID"]);

                List<MASSIT_FixedSlotBE> lstFixedSlot = oMASSIT_FixedSlotBAL.GetAllFixedSlotBAL(oMASSIT_FixedSlotBE);

                if (lstFixedSlot != null && lstFixedSlot.Count > 0)
                {

                    spCarrier.Style.Add("display", "none");

                    ltCarrierName.Visible = true;
                    ltCarrierName.Text = lstFixedSlot[0].Vendor.VendorName;

                    hdnSuggestedFixedSlotID.Value = lstFixedSlot[0].FixedSlotID.ToString();
                    ActivateFistView();

                    ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(lstFixedSlot[0].CarrierID.ToString()));

                    //BIND ALREADY BOOKED SLOT DETAILS//

                    ltSuggestedSlotTime.Text = lstFixedSlot[0].SlotTime.SlotTime + " " + AtDoorName + " " + lstFixedSlot[0].DoorNo.DoorNumber.ToString();
                    hdnSuggestedSlotTimeID.Value = lstFixedSlot[0].SlotTimeID.ToString();

                    hdnSuggestedSiteDoorNumberID.Value = lstFixedSlot[0].SiteDoorNumberID.ToString();
                    hdnSuggestedSiteDoorNumber.Value = lstFixedSlot[0].DoorNo.DoorNumber.ToString();

                    hdnSuggestedSlotTime.Value = lstFixedSlot[0].SlotTime.SlotTime;
                    ltTimeSlot.Text = hdnSuggestedSlotTime.Value;


                    ltDoorNo.Text = hdnSuggestedSiteDoorNumber.Value;
                    ltTimeSlot_1.Text = lstFixedSlot[0].SlotTime.SlotTime;
                    ltDoorNo_1.Text = hdnSuggestedSiteDoorNumber.Value;

                    spSCDate.Style.Add("display", "none");
                    ltSCdate.Visible = true;
                    ltSCdate.Text = string.Empty;
                    //if (GetQueryStringValue("BookingWeekDay") != null && GetQueryStringValue("BookingWeekDay") != string.Empty && GetQueryStringValue("BookingWeekDay") != "8") {
                    //    ltBookingDate.Text = txtSchedulingdate.GetDate.AddDays(-1).ToString("dd/MM/yyyy");
                    //    hdnSuggestedWeekDay.Value = txtSchedulingdate.GetDate.AddDays(-1).ToString("ddd");
                    //    ltSCdate.Text = txtSchedulingdate.GetDate.AddDays(-1).ToString("dd/MM/yyyy") + " " + ltSuggestedSlotTime.Text;
                    //}
                    //else {
                    //    ltBookingDate.Text = txtSchedulingdate.GetDate.Day.ToString() + "/" + txtSchedulingdate.GetDate.Month.ToString() + "/" + txtSchedulingdate.GetDate.Year.ToString();
                    //    hdnSuggestedWeekDay.Value = txtSchedulingdate.GetDate.ToString("ddd");
                    //    ltSCdate.Text = hdnSchedulerdate.Value + " " + ltSuggestedSlotTime.Text;
                    //}


                    ltBookingDate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    hdnSuggestedWeekDay.Value = ActualSchedulDateTime.ToString("ddd");
                    ltSCdate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy") + " " + ltSuggestedSlotTime.Text;

                }
            }
            else if (hdnCurrentMode.Value == "Add")
            {
                ActivateFistView();

                //if (Session["UserCarrierID"] != null) {
                //    //ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(Session["UserCarrierID"].ToString()));
                //    ddlCarrier_SelectedIndexChanged(sender, e);
                //}
            }
            CheckSiteClosure();
        }

        if (!IsPostBack)
        {
            string strBookingId = Convert.ToString(GetQueryStringValue("ID"));
        }

        hdnVendorAfterSlotTimeID.Value = VendorAfterSlotTimeID;
        hdnVendorBeforeSlotTimeID.Value = VendorBeforeSlotTimeID;
        hdnIsBookingExclusions.Value = ddlSite.IsBookingExclusions != null ? Convert.ToString(ddlSite.IsBookingExclusions).ToLower() : "false";

        //Emergency fix - temp as per Anthony's mail on 11 jan 2016
        if (!IsPostBack)
        {
            if ((Session["Role"].ToString().ToLower() == "vendor" || Session["Role"].ToString().ToLower() == "carrier"))
            {
                BindSchedulingClosedown();
            }
        }
        //----------------------------------------------------------
    }

    private void BindSchedulingClosedown()
    {
        MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE = new MASSIT_SchedulingClosedownBE();
        MASSIT_SchedulingClosedownBAL oMASSIT_SchedulingClosedownBAL = new MASSIT_SchedulingClosedownBAL();

        oMASSIT_SchedulingClosedownBE.Action = "ShowAll";

        List<MASSIT_SchedulingClosedownBE> lstSchedulingClosedownBE = oMASSIT_SchedulingClosedownBAL.GetSchedulingClosedownBAL(oMASSIT_SchedulingClosedownBE);

        if (lstSchedulingClosedownBE != null && lstSchedulingClosedownBE.Count > 0)
        {
            for (int icount = 0; icount < lstSchedulingClosedownBE.Count; icount++)
            {
                if (ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(lstSchedulingClosedownBE[icount].SiteID.ToString())) >= 0)
                    ddlSite.innerControlddlSite.Items.RemoveAt(ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(lstSchedulingClosedownBE[icount].SiteID.ToString())));
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = "Appointment Scheduling - Booking";
        AddedControlIDs = string.Empty;

        if (!Page.IsPostBack)
        {
            Session["BookingCreated"] = false;
            ViewState["IsShowPOPopup"] = false;
            hdnCurrentRole.Value = Session["Role"].ToString();
        }

        if (!Page.IsPostBack)
        {
            hdnCurrentRole.Value = Session["Role"].ToString();
        }

        //ScriptManager.RegisterStartupScript(this, this.GetType(), "scroll", "<script>setScroll();</script>", false);
        hdnBeforeSlotTimeID.Value = BeforeSlotTimeID;
        hdnAfterSlotTimeID.Value = AfterSlotTimeID;

    }

    private void ActivateFistView()
    {

        mvBookingEdit.ActiveViewIndex = 0;

        if (hdnActualScheduleDate.Value != string.Empty)
        {
            //changes for edit case for across the day
            //txtSchedulingdate.innerControltxtDate.Value = ActualSchedulDateTime.ToString("dd/MM/yyyy");
            txtSchedulingdate.innerControltxtDate.Value = ActualSchedulDateTime.ToString("dd/MM/yyyy");

            // txtSchedulingdate.innerControltxtDate.Value = ActualSchedulDateTime.ToString("dd/MM/yyyy");  // Convert.ToDateTime(Convert.ToDateTime(hdnSchedulerdate.Value).ToString("dd/MM/yyyy"));            
            //}

            //if (hdnCurrentMode.Value == "Edit" && hdnCurrentRole.Value == "Carrier")
            //{
            //    txtSchedulingdate.innerControltxtDate.Value = ActualSchedulDateTime.ToString("dd/MM/yyyy");
            //}
            //else
            //{
            //    //if (!string.IsNullOrEmpty(hdnSuggestedFixedSlotID.Value))
            //    //{
            //    //    txtSchedulingdate.innerControltxtDate.Value = ActualSchedulDateTime.ToString("dd/MM/yyyy");                    
            //    //}
            //    //else
            //    //{
            //        txtSchedulingdate.innerControltxtDate.Value = LogicalSchedulDateTime.ToString("dd/MM/yyyy");
            //    //}
            //}
        }

        if (hdnCurrentMode.Value == "Add" && hdnFixedSlotMode.Value == "false" && hdnCurrentRole.Value == "Carrier")
        {
            txtSchedulingdate.innerControltxtDate.Value = "";
        }

        if (hdnCurrentMode.Value == "Edit" && hdnCurrentRole.Value == "Carrier")
        {
            //spSCDate.Style.Add("display", "none");
            ltSCdate.Visible = true;
            ltSCdate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
            spltdate.Style.Add("display", "none");
        }

        if (GetQueryStringValue("SiteCountryID") != null)
        {
            if (GetQueryStringValue("SiteID").ToString() != "0")
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(GetQueryStringValue("SiteID").ToString()));
            else
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Session["SiteID"].ToString()));

            ddlSite.CountryID = Convert.ToInt32(GetQueryStringValue("SiteCountryID").ToString());
        }
        else
        {
            ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Session["SiteID"].ToString()));
            ddlSite.CountryID = Convert.ToInt32(Session["SiteCountryID"].ToString());
        }

        SiteSelectedIndexChanged();

        if (hdnCurrentMode.Value == "Edit")
        {
            spSite.Style.Add("display", "none");
            ltSelectedSite.Visible = true;
            ltSelectedSite.Text = ddlSite.innerControlddlSite.SelectedItem.Text;
        }
        if (hdnBookingTypeID.Value == "5" && hdnProvisionalReason.Value == "EDITED" && hdnLine.Value != "0" && Session["Role"].ToString().ToLower() != "carrier")
        {
            btnProceedBooking.Visible = false;
            btnAccept.Visible = true;
            btnRejectChanges.Visible = true;
            btnRejectBooking.Visible = true;
            lblVolumeDetailDesc.Visible = false;
            lblVolumeDetailDescProvisional.Visible = true;
        }
        else
        {
            btnProceedBooking.Visible = true;
            btnAccept.Visible = false;
            btnRejectChanges.Visible = false;
            btnRejectBooking.Visible = false;
            lblVolumeDetailDesc.Visible = true;
            lblVolumeDetailDescProvisional.Visible = false;
        }
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        MAS_SiteBE oMAS_SiteBE = ddlSite.oSiteBE;
        if (oMAS_SiteBE != null)
        {
            ViewState["oMAS_SiteBE"] = oMAS_SiteBE;
        }
    }

    public override void SiteSelectedIndexChanged()
    {
        base.SiteSelectedIndexChanged();
        if (ddlSite.innerControlddlSite.SelectedValue != "0" && !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue))
        {
            ISPM15PalletCheckingBAL iSPM15PalletCheckingBAL = new ISPM15PalletCheckingBAL();
            ISPM15PalletCheckingBE ispm15 = new ISPM15PalletCheckingBE();
            ispm15.Action = "GetISPM15BySiteID";
            ispm15.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue);
            pnlPalletCheckforCountry.Visible = iSPM15PalletCheckingBAL.IsSiteCountryExistsBAL(ispm15);
        }
        IsBookingValidationExcludedDelivery = false;

        BindVehicleType();

        MAS_SiteBE oMAS_SiteBE = ddlSite.oSiteBE;
        if (oMAS_SiteBE == null)
        {
            BindDelivaryType();
            BindCarrier();
            oMAS_SiteBE = (MAS_SiteBE)ViewState["oMAS_SiteBE"];
        }

        if (oMAS_SiteBE != null)
        {

            int CurrentSiteCountryID = Convert.ToInt32(oMAS_SiteBE.SiteCountryID);

            //---Pre Advise Note Required show/hide---//
            if (oMAS_SiteBE.IsPreAdviseNoteRequired == true)
            {
                trpreadvise.Visible = true;
                IsPreAdviseNoteRequired = true;
            }
            else
            {
                trpreadvise.Visible = false;
                IsPreAdviseNoteRequired = false;
            }
            //-------------------------------------//

            NonTimeDeliveryCartonVolume = oMAS_SiteBE.NonTimeDeliveryCartonVolume;
            NonTimeDeliveryPalletVolume = oMAS_SiteBE.NonTimeDeliveryPalletVolume;

            if (PreSiteCountryID != CurrentSiteCountryID)
            {

                BindDelivaryType();
                BindCarrier();
            }
            else
            {
                if (!Page.IsPostBack)
                {

                    BindDelivaryType();
                    BindCarrier();
                }
            }

            if (Convert.ToBoolean(isPostBack))
                PreSiteCountryID = CurrentSiteCountryID;
            else
                isPostBack = true;
        }
        else
        {
            BindDelivaryType();
            BindCarrier();
        }

        //if (Session["UserCarrierID"] != null) {
        //    if (ddlCarrier.SelectedIndex <= 0)
        //        ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(Session["UserCarrierID"].ToString()));
        //}
        //CarrierValidationExc();
        DeliveryValidationExc();
        CarrierValidationExc();
        if (!GetToleratedDays())
        {

            GetMaximumCapacity();

            if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
            {
                string OpenDoorIds = GetDoorConstraints();

                if (OpenDoorIds == string.Empty)
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_EXT"); // "No space available on any compatible doors.Please contact Goods In office or select a different date.";

                    if (hdnCurrentRole.Value == "Carrier")
                    {
                        //Show err msg
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        return;
                    }
                    else
                    {
                        //Show err msg
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_INT"); //"No space available on any compatible doors.";
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        return;
                    }
                }
            }

        }
        ShowWarningMessages();

        if ((","+strCarrierValidationExc+",").Contains("," + ddlCarrier.SelectedItem.Value + ","))
        {
            IsBookingValidationExcludedCarrier = true;
        }
        else
        {
            IsBookingValidationExcludedCarrier = false;
        }

        //------Phase 15 R2 Point 21-----------//
        Session["CurrentSiteId"] = ddlSite.innerControlddlSite.SelectedValue;
        txtSchedulingdate.LoadDates();
        //------------------------------------//

        ShowHazordousItem();
    }

    private void BindVehicleType()
    {
        MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

        oMASSIT_VehicleTypeBE.Action = "ShowAll";
        oMASSIT_VehicleTypeBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<MASSIT_VehicleTypeBE> lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsBAL(oMASSIT_VehicleTypeBE);

        if (lstVehicleType.Count > 0)
        {
            FillControls.FillDropDown(ref ddlVehicleType, lstVehicleType, "VehicleType", "VehicleTypeID", "--Select--");
        }

        VehicleNarrativeRequired = string.Empty;

        for (int iCount = 0; iCount < lstVehicleType.Count; iCount++)
        {
            if (lstVehicleType[iCount].IsNarrativeRequired)
            {
                if (VehicleNarrativeRequired == string.Empty)
                    VehicleNarrativeRequired = "," + lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
                else
                    VehicleNarrativeRequired += lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
            }

            //----Stage 10 Point 10 L 5.1-----//
            if (lstVehicleType[iCount].IsContainer)
            {
                if (IsVehicleContainer == string.Empty)
                    IsVehicleContainer = "," + lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
                else
                    IsVehicleContainer += lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
            }
            //---------------------------------//
        }

        //if (Convert.ToString(Session["Role"]).Trim().ToLower() == "carrier") 
        //{
        MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        if (singleSiteSetting != null)
        {
            ddlVehicleType.SelectedIndex = ddlVehicleType.Items.IndexOf(ddlVehicleType.Items.FindByValue(Convert.ToString(singleSiteSetting.VehicleTypeId)));
            ddlVehicleType_SelectedIndexChanged(null, null);
        }
        //}
    }

    private void BindDelivaryType()
    {
        MAS_SiteBE singleSiteSetting = null;
        int countrySiteId = 0;
        if (!IsPostBack)
        {
            if (GetQueryStringValue("SiteID") != null)
                singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(GetQueryStringValue("SiteID")));
            else
                singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        }
        else
        {
            singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        }

        if (singleSiteSetting != null)
            countrySiteId = Convert.ToInt32(singleSiteSetting.SiteCountryID);

        MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE = new MASCNT_DeliveryTypeBE();
        APPCNT_DeliveryTypeBAL oMASCNT_DeliveryTypeBAL = new APPCNT_DeliveryTypeBAL();

        oMASCNT_DeliveryTypeBE.Action = "ShowAll";

        if (countrySiteId != 0)
            oMASCNT_DeliveryTypeBE.CountryID = countrySiteId;
        else
            oMASCNT_DeliveryTypeBE.CountryID = ddlSite.CountryID;

        oMASCNT_DeliveryTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_DeliveryTypeBE.User.UserID = 0;
        //oMASCNT_DeliveryTypeBE.EnabledAsVendorOption = "1";
        List<MASCNT_DeliveryTypeBE> lstDeliveryType = oMASCNT_DeliveryTypeBAL.GetDeliveryTypeDetailsBAL(oMASCNT_DeliveryTypeBE);

        if (Session["Role"].ToString() == "Carrier")
        {
            lstDeliveryType = lstDeliveryType.FindAll(delegate(MASCNT_DeliveryTypeBE p) { return p.IsEnabledAsVendorOption == true; });
        }

        ddlDeliveryType.SelectedIndex = -1;
        if (lstDeliveryType.Count > 0)
        {
            FillControls.FillDropDown(ref ddlDeliveryType, lstDeliveryType, "DeliveryType", "DeliveryTypeID", "--Select--");
        }
        else
        {
            ddlDeliveryType.Items.Clear();
            ddlDeliveryType.Items.Add(new ListItem("---Select---", "0"));
        }

        //if (Convert.ToString(Session["Role"]).Trim().ToLower() == "carrier")
        //{
        if (singleSiteSetting != null)
        {
            ddlDeliveryType.SelectedIndex = ddlDeliveryType.Items.IndexOf(ddlDeliveryType.Items.FindByValue(Convert.ToString(singleSiteSetting.DeliveryTypeId)));
            ddlDeliveryType_SelectedIndexChanged(null, null);
        }
        //}
    }

    private void BindCarrier()
    {
        MAS_SiteBE singleSiteSetting = null;
        int countrySiteId = 0;

        if (!IsPostBack)
        {
            if (GetQueryStringValue("SiteID") != null)
                singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(GetQueryStringValue("SiteID")));
            else
                singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        }
        else
        {
            singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        }

        if (singleSiteSetting != null)
            countrySiteId = Convert.ToInt32(singleSiteSetting.SiteCountryID);

        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        if (countrySiteId != 0)
            oMASCNT_CarrierBE.CountryID = countrySiteId;
        else
            oMASCNT_CarrierBE.CountryID = ddlSite.CountryID;

        oMASCNT_CarrierBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_CarrierBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        if (Convert.ToString(Session["Role"]) == "Carrier")
        {
            oMASCNT_CarrierBE.Action = "GetUserCarrier";
        }
        else
        {
            oMASCNT_CarrierBE.Action = "ShowAll";
        }
        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);

        //if (Convert.ToString(Session["Role"]) == "Carrier") {
        //lstCarrier = lstCarrier.FindAll(delegate(MASCNT_CarrierBE c) { return c.CarrierID == Convert.ToInt32(Session["UserCarrierID"]); });
        // }

        if (lstCarrier.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCarrier, lstCarrier, "CarrierName", "CarrierID", "--Select--");
        }
        else
        {
            ddlCarrier.Items.Clear();
            ddlCarrier.Items.Insert(0, new ListItem("----Select----", "0"));
        }

        //if (Session["UserCarrierID"] != null) {
        //    if (ddlCarrier.SelectedIndex <= 0)
        //        ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(Session["UserCarrierID"].ToString()));
        //}

        CarrierNarrativeRequired = ",";
        for (int iCount = 0; iCount < lstCarrier.Count; iCount++)
        {
            if (lstCarrier[iCount].IsNarrativeRequired)
            {
                CarrierNarrativeRequired += lstCarrier[iCount].CarrierID.ToString() + ",";
            }
        }

        if (oMASCNT_CarrierBE.Action.Trim().ToUpper() == "GETUSERCARRIER" && ddlCarrier.Items.Count > 1)
            ddlCarrier.SelectedIndex = 1;
    }

    private void DeliveryValidationExc()
    {
        DataTable dt1;

        MASSIT_BookingTypeExclusionBE oMASCNT_BookingTypeExclusionBE = new MASSIT_BookingTypeExclusionBE();
        APPSIT_BookingTypeExclusionBAL oMASCNT_BookingTypeExclusionBAL = new APPSIT_BookingTypeExclusionBAL();

        oMASCNT_BookingTypeExclusionBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASCNT_BookingTypeExclusionBE.Action = "GetDeliveries";
        dt1 = oMASCNT_BookingTypeExclusionBAL.GetBookingTypeExclusionDetailsBAL(oMASCNT_BookingTypeExclusionBE, "");

        strDeliveryValidationExc = string.Empty;

        for (int iCount = 0; iCount < dt1.Rows.Count; iCount++)
        {
            strDeliveryValidationExc += dt1.Rows[iCount]["SiteDeliveryID"].ToString() + ",";
        }

        //Validate Delivery Type--//
        if (ddlDeliveryType.SelectedIndex > 0 && strDeliveryValidationExc.Contains(ddlDeliveryType.SelectedItem.Value + ","))
        {
            IsBookingValidationExcludedDelivery = true;
        }
        //--------------------//
    }

    private bool GetToleratedDays()
    {

        bool isError = false;

        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty)
        {
            MAS_SiteBE oMAS_SITEBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SITEBAL = new MAS_SiteBAL();

            List<MAS_SiteBE> lstSites = new List<MAS_SiteBE>();

            oMAS_SITEBE.Action = "ShowAll";
            oMAS_SITEBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SITEBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SITEBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            lstSites = oMAS_SITEBAL.GetSiteMisSettingBAL(oMAS_SITEBE);

            if (lstSites != null && lstSites.Count > 0)
            {
                int? ActNoticePeriodDays = lstSites[0].BookingNoticePeriodInDays;

                int? NoticePeriodDays = lstSites[0].BookingNoticePeriodInDays;

                //--Stage 14 Point 16----//
                int SiteCloserDays = CheckSiteClosureByRange();
                if (SiteCloserDays > 0)
                {
                    if (NoticePeriodDays != null)
                        NoticePeriodDays = NoticePeriodDays + SiteCloserDays;
                    else
                        NoticePeriodDays = SiteCloserDays;
                }
                //---------------------------//

                DateTime SchDate = LogicalSchedulDateTime;
                DateTime CurrentDate = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString());

                //TimeSpan DateDiff = SchDate - CurrentDate;
                //int DaysDiff = DateDiff.Days;
                //int DaysDiff = Enumerable.Range(0, Convert.ToInt32(SchDate.Subtract(CurrentDate).TotalDays)).Select(i => new[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(CurrentDate.AddDays(i).DayOfWeek) ? 0 : 1).Sum();
                int DaysDiff = 0;
                if (SchDate >= CurrentDate)
                {
                    DaysDiff = Enumerable.Range(0, Convert.ToInt32(SchDate.Subtract(CurrentDate).TotalDays)).Select(i => new[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(CurrentDate.AddDays(i).DayOfWeek) ? 0 : 1).Sum();
                }


                //string errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_1") + " " + NoticePeriodDays + " " + WebCommon.getGlobalResourceValue("BK_MS_05_EXT_2");
                // "We required " + NoticePeriodDays + " days notification for all deliveries scheduled  into our sites, therefore we cannot accept this booking on the date you have requested. please select another date.";

                //---Stage 4 Point 4---
                //All deliveries scheduled into this site must be booked before 12:00 and 1 working day(s) before delivery, therefore we cannot accept this booking on the date you have requested. Please select another date or contact the site's booking in department.
                string errMsg = string.Empty;
                if (NoticePeriodDays == null || NoticePeriodDays == 0)
                {
                    if (lstSites[0].BookingNoticeTime != null)
                    {
                        string NoticeTime = Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Hour.ToString()) + ":" + Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Minute.ToString());
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_2").Replace("##Time##", NoticeTime) + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", "1");
                    }

                }
                //Commeted and chnages as per anthony mail 13 Sep 2013 
                //else if (NoticePeriodDays == 1) {
                //    if (lstSites[0].BookingNoticeTime != null) {
                //        string NoticeTime = Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Hour.ToString()) + ":" + Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Minute.ToString());
                //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                //            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_2").Replace("##Time##", NoticeTime) + " " +
                //            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", "1");
                //    }
                //    else if (lstSites[0].BookingNoticeTime == null) {
                //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                //            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", "2");
                //    }
                //}
                else
                {
                    if (lstSites[0].BookingNoticeTime != null)
                    {
                        string NoticeTime = Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Hour.ToString()) + ":" + Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Minute.ToString());
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_2").Replace("##Time##", NoticeTime) + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", (ActNoticePeriodDays).ToString());
                    }
                    else
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_3").Replace("##NoticePeriodInDays##", (ActNoticePeriodDays).ToString());
                    }
                }
                //-----------------------//

                bool isAllowed = false;

                if (NoticePeriodDays == null)
                {
                    if (DaysDiff == 0)
                    {
                        DateTime dt1, dt2;
                        dt1 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);
                        if (lstSites[0].BookingNoticeTime != null)
                            dt2 = new DateTime(1900, 01, 01, lstSites[0].BookingNoticeTime.Value.Hour, lstSites[0].BookingNoticeTime.Value.Minute, 00);
                        else
                            dt2 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);

                        if (dt1 <= dt2)
                        {
                            isAllowed = true;
                        }
                    }
                    else
                    {
                        isAllowed = true;
                    }
                }

                if (isAllowed == false && DaysDiff >= NoticePeriodDays + 1)
                    isAllowed = true;

                if (isAllowed == false && DaysDiff == NoticePeriodDays)
                {
                    DateTime dt1, dt2;
                    dt1 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);
                    if (lstSites[0].BookingNoticeTime != null)
                        dt2 = new DateTime(1900, 01, 01, lstSites[0].BookingNoticeTime.Value.Hour, lstSites[0].BookingNoticeTime.Value.Minute, 00);
                    else
                        dt2 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);

                    if (dt1 <= dt2)
                    {
                        isAllowed = true;
                    }
                }

                if (isAllowed == false && NoticePeriodDays == 0 && SchDate > CurrentDate)
                {
                    isAllowed = true;
                }

                if (!isAllowed)
                {
                    //error
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {
                        if (hdnCurrentMode.Value == "Add")
                        {
                            ShowErrorMessage(errMsg, "BK_MS_05_EXT");
                            return true;
                        }
                    }
                    else
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_INT"); // "There is insufficient notice given for this booking. Do you want to continue?";
                        AddWarningMessages(errMsg, "BK_MS_05_INT");
                    }
                }

                //if (NoticePeriodDays > 0 && DaysDiff < NoticePeriodDays) {
                //    //error         

                //    if (Session["Role"].ToString() == "Vendor" || Session["Role"].ToString() == "Carrier") {
                //        ShowErrorMessage(errMsg, "BK_MS_05_EXT");
                //        return true;
                //    }
                //    else {
                //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_INT"); // "There is insufficient notice given for this booking. Do you want to continue?";
                //        AddWarningMessages(errMsg, "BK_MS_05_INT");
                //    }
                //}

                //if (NoticePeriodDays >= 0 && DaysDiff == NoticePeriodDays) {
                //    DateTime dt1, dt2;
                //    dt1 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);
                //    if (lstSites[0].BookingNoticeTime != null)
                //        dt2 = new DateTime(1900, 01, 01, lstSites[0].BookingNoticeTime.Value.Hour, lstSites[0].BookingNoticeTime.Value.Minute, 00);
                //    else
                //        dt2 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);

                //    if (dt1 > dt2) {
                //        //error
                //        if (Session["Role"].ToString() == "Vendor" || Session["Role"].ToString() == "Carrier") {
                //            ShowErrorMessage(errMsg, "BK_MS_05_EXT");
                //            return true;
                //        }
                //        else {
                //            errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_INT"); //"There is insufficient notice given for this booking. Do you want to continue?";
                //            AddWarningMessages(errMsg, "BK_MS_05_INT");
                //        }

                //    }
                //}
            }

            ScriptManager.RegisterStartupScript(Page, this.GetType(), "id", "$get('" + btnProceedAddPO.ClientID + "').style.display='';", true);
        }
        return isError;
    }

    //--Stage 14 Point 16----//
    private int CheckSiteClosureByRange()
    {
        int iSiteClosureDays = 0;

        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
        {

            MASSIT_HolidayBE oMASSIT_HolidayBE = new MASSIT_HolidayBE();
            MASSIT_HolidayBAL oMASSIT_HolidayBAL = new MASSIT_HolidayBAL();

            oMASSIT_HolidayBE.Action = "ShowAllByRange";
            oMASSIT_HolidayBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            if (ddlSite.innerControlddlSite.SelectedIndex >= 0)
                oMASSIT_HolidayBE.Site.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : 0;

            oMASSIT_HolidayBE.HolidayDate = LogicalSchedulDateTime;

            List<MASSIT_HolidayBE> lstHoliday = oMASSIT_HolidayBAL.GetHolidaysBAL(oMASSIT_HolidayBE);

            if (lstHoliday != null && lstHoliday.Count > 0)
            {
                iSiteClosureDays = lstHoliday.Count;
            }
        }
        return iSiteClosureDays;
    }
    //---------------------//

    private void GetMaximumCapacity()
    {
        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty)
        {
            MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
            MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

            oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_WeekSetupBE.EndWeekday = LogicalSchedulDateTime.DayOfWeek.ToString();
            oMASSIT_WeekSetupBE.ScheduleDate = LogicalSchedulDateTime;

            List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup == null || lstWeekSetup.Count <= 0)
            {
                oMASSIT_WeekSetupBE.Action = "ShowAll";
                oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                oMASSIT_WeekSetupBE.EndWeekday = LogicalSchedulDateTime.DayOfWeek.ToString();
                //oMASSIT_WeekSetupBE.ScheduleDate = DateTime.Now.Date;
                lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
            }

            string errMsg;

            if (lstWeekSetup != null && lstWeekSetup.Count > 0)
            {

                //Set Current Running Total of alternate screen
                ltMaximumLifts.Text = lstWeekSetup[0].MaximumLift != (int?)null ? lstWeekSetup[0].MaximumLift.ToString() : "-";
                ltMaximumPallets.Text = lstWeekSetup[0].MaximumPallet != (int?)null ? lstWeekSetup[0].MaximumPallet.ToString() : "-";
                ltMaximumLines.Text = lstWeekSetup[0].MaximumLine != (int?)null ? lstWeekSetup[0].MaximumLine.ToString() : "-";
                //-----------------------------------//

                APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
                APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

                oAPPBOK_BookingBE.Action = "GetBookedVolume";
                oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;

                //----Stage 10 Point 10 L 5.1-----//
                if (hdnCurrentMode.Value == "Edit")
                    oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
                //---------------------------------//

                List<APPBOK_BookingBE> lstBookedVolume = oAPPBOK_BookingBAL.GetBookedVolumeBAL(oAPPBOK_BookingBE);

                if (lstBookedVolume != null && lstBookedVolume.Count > 0)
                {

                    //Set Current Running Total of alternate screen//
                    ltRemainingLifts.Text = lstWeekSetup[0].MaximumLift != (int?)null && (lstWeekSetup[0].MaximumLift - lstBookedVolume[0].NumberOfLift) > 0 ? (lstWeekSetup[0].MaximumLift - lstBookedVolume[0].NumberOfLift).ToString() : "-";
                    ltRemainingPallets.Text = lstWeekSetup[0].MaximumPallet != (int?)null && (lstWeekSetup[0].MaximumPallet - lstBookedVolume[0].NumberOfPallet) > 0 ? (lstWeekSetup[0].MaximumPallet - lstBookedVolume[0].NumberOfPallet).ToString() : "-";
                    ltRemainingLines.Text = lstWeekSetup[0].MaximumLine != (int?)null && (lstWeekSetup[0].MaximumLine - lstBookedVolume[0].NumberOfLines) > 0 ? (lstWeekSetup[0].MaximumLine - lstBookedVolume[0].NumberOfLines).ToString() : "-";
                    //----Stage 10 Point 10 L 5.1-----//
                    //RemainingCartons = lstWeekSetup[0].MaximumContainer != (int?)null ? (lstWeekSetup[0].MaximumContainer - lstBookedVolume[0].NumberOfContainer).ToString() : "-";
                    //---------------------------------//

                    RemainingPallets = lstWeekSetup[0].MaximumPallet != (int?)null ? Convert.ToInt32(lstWeekSetup[0].MaximumPallet) - Convert.ToInt32(lstBookedVolume[0].NumberOfPallet) : (int?)null;
                    RemainingLines = lstWeekSetup[0].MaximumLine != (int?)null ? Convert.ToInt32(lstWeekSetup[0].MaximumLine) - Convert.ToInt32(lstBookedVolume[0].NumberOfLines) : (int?)null;


                    //ltRemainingLifts.Text = lstBookedVolume[0].NumberOfLift.ToString();
                    //ltRemainingPallets.Text = lstBookedVolume[0].NumberOfPallet.ToString();
                    //ltRemainingLines.Text = lstBookedVolume[0].NumberOfLines.ToString() ;

                    //-----------------------------------//
                    //if not the confirm fixed slot case

                    /* COMMENTED AS PER REQUIREMENT OF PHASE-11-C POINT-1(17)*/

                    if (hdnSuggestedFixedSlotID.Value == "-1" || hdnSuggestedFixedSlotID.Value == string.Empty)
                    {
                        if (lstBookedVolume[0].NumberOfLines >= (lstWeekSetup[0].MaximumLine > 0 ? lstWeekSetup[0].MaximumLine : 99999)
                             || lstBookedVolume[0].NumberOfPallet >= (lstWeekSetup[0].MaximumPallet > 0 ? lstWeekSetup[0].MaximumPallet : 99999)
                             || lstBookedVolume[0].NumberOfDeliveries >= (lstWeekSetup[0].MaximumDeliveries > 0 ? lstWeekSetup[0].MaximumDeliveries : 99999))
                        {
                            //error
                            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                            {
                                if (hdnCurrentMode.Value == "Add")
                                {
                                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_EXT"); // "There is no capacity for any further deliveries on the date selected. Please select a different date or, if the delivery is urgent, please contact the booking office.";
                                    ShowErrorMessage(errMsg, "BK_MS_06_EXT");
                                    return;
                                }
                            }
                            else
                            {
                                if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
                                {  // If coming from Provisional page ,ignore capacity warning -- Phase 12 R2 Point 2
                                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_INT"); // "There is no capacity for any further deliveries on the date selected. Click CONTINUE to make booking on this date.";
                                    AddWarningMessages(errMsg, "BK_MS_06_INT");
                                }
                            }
                        }

                        //-----------Point 10L-5.1---------------//                        

                        if (lstBookedVolume[0].NumberOfContainer >= (lstWeekSetup[0].MaximumContainer > 0 ? lstWeekSetup[0].MaximumContainer : 99999))
                        {
                            if (ddlVehicleType.SelectedIndex > -1)
                            {
                                if (IsVehicleContainer.Contains("," + ddlVehicleType.SelectedItem.Value + ","))
                                {
                                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                                    {
                                        //if (hdnCurrentMode.Value == "Add") {
                                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_Ext_MaxContainers"); // The maximum amount of containers for this date has been reached.....
                                        ShowErrorMessage(errMsg, "BK_MS_06_Ext_MaxContainers");
                                        return;
                                        //}
                                    }
                                    else
                                    {
                                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_INT_MaxContainers"); //The maximum amount of containers for this date has been reached. Do you want to continue?
                                        AddWarningMessages(errMsg, "BK_MS_06_INT_MaxContainers");
                                    }
                                }
                            }
                            isContainerError = true;
                        }
                        //----------------------------------------//
                    }
                }
            }
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "id", "$get('" + btnProceedAddPO.ClientID + "').style.display='';", true);
        }
    }

    protected void ddlCarrier_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtCarrierOther.Text = string.Empty;

        //if (Session["UserCarrierID"] != null) {
        //    if (ddlCarrier.SelectedIndex <= 0)
        //        ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(Session["UserCarrierID"].ToString()));
        //}

        if (ddlCarrier.Items.Count > 0)
        {
            CarrierValidationExc();
            rfvCarrierOtherRequired.Enabled = false;
            if (CarrierNarrativeRequired.Contains("," + ddlCarrier.SelectedItem.Value + ","))
            {
                txtCarrierOther.Style.Add("display", "");
                txtCarrierOther.Visible = true;
                rfvCarrierOtherRequired.Enabled = true;
            }
            else
            {
                txtCarrierOther.Style.Add("display", "none");
                txtCarrierOther.Visible = false;
                rfvCarrierOtherRequired.Enabled = false;
            }

            if (("," + strCarrierValidationExc + ",").Contains("," + ddlCarrier.SelectedItem.Value + ","))
            {
                IsBookingValidationExcludedCarrier = true;
            }
            else
            {
                IsBookingValidationExcludedCarrier = false;
            }
        }
        else
        {
            txtCarrierOther.Style.Add("display", "none");
            rfvCarrierOtherRequired.Enabled = false;
            IsBookingValidationExcludedCarrier = false;
        }
    }

    protected void ddlVehicleType_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtVehicleOther.Text = string.Empty;
        //-----------Point 10L-5.1------------//
        string errMsg = string.Empty;
        if (ddlVehicleType.SelectedIndex > -1 && !string.IsNullOrEmpty(txtSchedulingdate.innerControltxtDate.Value))
        {
            if (IsVehicleContainer.Contains("," + ddlVehicleType.SelectedItem.Value + ","))
            {

                if (isContainerError == true)
                {
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {
                        //if (hdnCurrentMode.Value == "Add") {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_Ext_MaxContainers"); // The maximum amount of containers for this date has been reached.....
                        ShowErrorMessage(errMsg, "BK_MS_06_Ext_MaxContainers");
                        return;
                        //}
                    }
                    else
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_06_INT_MaxContainers"); //The maximum amount of containers for this date has been reached. Do you want to continue?
                        AddWarningMessages(errMsg, "BK_MS_06_INT_MaxContainers");
                    }
                }
            }
        }

        if (!string.IsNullOrEmpty(errMsg))
            ShowWarningMessages();
        else
            VehicleTypeSelect();

        //------------------------------------------//   


    }

    private void VehicleTypeSelect()
    {
        if (ddlVehicleType.SelectedIndex > -1)
        {
            rfvVehicleTypeOtherRequired.Enabled = false;
            if (VehicleNarrativeRequired.Contains(ddlVehicleType.SelectedItem.Value + ","))
            {
                txtVehicleOther.Style.Add("display", "");
                txtVehicleOther.Visible = true;
                rfvVehicleTypeOtherRequired.Enabled = true;
            }
            else
            {
                txtVehicleOther.Style.Add("display", "none");
                txtVehicleOther.Visible = false;
                rfvVehicleTypeOtherRequired.Enabled = false;
            }

            //Get Door Type as per selected Vehicle Type
            strVehicleDoorTypeMatrix = string.Empty;
            MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();

            APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();

            oMASSIT_VehicleDoorMatrixBE.Action = "ShowAll";
            oMASSIT_VehicleDoorMatrixBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleDoorMatrixBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_VehicleDoorMatrixBE.SiteID = Convert.ToInt16(ddlSite.innerControlddlSite.SelectedItem.Value);

            List<MASSIT_VehicleDoorMatrixBE> lstVehicleDoorMatrix = oMASSIT_VehicleDoorMatrixBAL.GetVehicleDoorMatrixDetailsBAL(oMASSIT_VehicleDoorMatrixBE);

            //---FOR SKU DOOR MATRIX, NEED TO SKIP VEHICLE DOOR MATIX CONSTRAINTS--- SPRINT 4 POINT 7
            NameValueCollection collection = new NameValueCollection();

            for (int iCount = 0; iCount < lstVehicleDoorMatrix.Count; iCount++)
            {
                strVehicleDoorTypeMatrixSKU += lstVehicleDoorMatrix[iCount].DoorTypeID.ToString() + ",";
                collection.Add(lstVehicleDoorMatrix[iCount].DoorTypeID.ToString(), lstVehicleDoorMatrix[iCount].Priority.ToString());
            }

            ArrDoorTypePrioritySKU = new NameValueCollection();
            foreach (string key in collection.AllKeys)
            {
                string Val = collection.Get(key);
                string[] ArrVal = Val.Split(',');
                ArrDoorTypePrioritySKU.Add(key, ArrVal[0].ToString());
            }
            //---------------------------------------------------------------------------------------

            //Get Door Type as per selected Vehicle Type 
            lstVehicleDoorMatrix = lstVehicleDoorMatrix.Where(Matrix => Matrix.VehicleTypeID == Convert.ToInt32(ddlVehicleType.SelectedItem.Value)).ToList();

            collection = new NameValueCollection();

            for (int iCount = 0; iCount < lstVehicleDoorMatrix.Count; iCount++)
            {
                strVehicleDoorTypeMatrix += lstVehicleDoorMatrix[iCount].DoorTypeID.ToString() + ",";
                collection.Add(lstVehicleDoorMatrix[iCount].DoorTypeID.ToString(), lstVehicleDoorMatrix[iCount].Priority.ToString());
            }

            ArrDoorTypePriority = new NameValueCollection();
            foreach (string key in collection.AllKeys)
            {
                string Val = collection.Get(key);
                string[] ArrVal = Val.Split(',');
                ArrDoorTypePriority.Add(key, ArrVal[0].ToString());
            }
        }
        //-----------------//   
    }

    protected void ddlDeliveryType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (strDeliveryValidationExc.Contains(ddlDeliveryType.SelectedItem.Value + ","))
        {
            IsBookingValidationExcludedDelivery = true;
        }
        else
        {
            IsBookingValidationExcludedDelivery = false;
        }
    }

    private bool CarrierValidationExc()
    {

        bool bCarrierValidation = true;

        if (ddlSite.innerControlddlSite.SelectedIndex > -1)
        {
            DataTable dt1;

            MASSIT_BookingTypeExclusionBE oMASCNT_BookingTypeExclusionBE = new MASSIT_BookingTypeExclusionBE();
            APPSIT_BookingTypeExclusionBAL oMASCNT_BookingTypeExclusionBAL = new APPSIT_BookingTypeExclusionBAL();

            oMASCNT_BookingTypeExclusionBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASCNT_BookingTypeExclusionBE.Action = "GetCarriers";
            dt1 = oMASCNT_BookingTypeExclusionBAL.GetBookingTypeExclusionDetailsBAL(oMASCNT_BookingTypeExclusionBE, "");

            strCarrierValidationExc = string.Empty;

            for (int iCount = 0; iCount < dt1.Rows.Count; iCount++)
            {
                strCarrierValidationExc += dt1.Rows[iCount]["SiteCarrierID"].ToString() + ",";
            }

            //Carrier Micellaneous Constraints -- Sprint 3a -----//
            BeforeSlotTimeID = "0";
            AfterSlotTimeID = "300";
            string errMsg = string.Empty;

            MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
            APPCNT_CarrierBAL oAPPCNT_CarrierBAL = new APPCNT_CarrierBAL();

            if (ddlCarrier.SelectedIndex > 0)
            {
                oMASCNT_CarrierBE.Action = "GetAllCarrierMiscCons";
                oMASCNT_CarrierBE.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
                oMASCNT_CarrierBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                List<MASCNT_CarrierBE> lstCarrierBE = oAPPCNT_CarrierBAL.GetAllCarrierMiscConsBAL(oMASCNT_CarrierBE);

                ViewState.Add("lstCarrier", lstCarrierBE.ToArray());


                //var dataBeforAfter = (from p in lstCarrierBE
                //                      select new
                //                      {
                //                          BeforeOrderBYId = p.BeforeOrderBYId,
                //                          AfterOrderBYId = p.AfterOrderBYId
                //                      }).ToList();


                if (lstCarrierBE != null && lstCarrierBE.Count > 0)
                {

                    bool AllowedDay = true;

                    if (txtSchedulingdate.innerControltxtDate.Value != string.Empty)
                    {
                        string Weekday = LogicalSchedulDateTime.DayOfWeek.ToString().ToUpper();

                        switch (Weekday)
                        {
                            case "SUNDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnSunday == true)
                                    AllowedDay = false;
                                break;
                            case "MONDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnMonday == true)
                                    AllowedDay = false;
                                break;
                            case "TUESDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnTuesday == true)
                                    AllowedDay = false;
                                break;
                            case "WEDNESDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnWednessday == true)
                                    AllowedDay = false;
                                break;
                            case "THURSDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnThrusday == true)
                                    AllowedDay = false;
                                break;
                            case "FRIDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnFriday == true)
                                    AllowedDay = false;
                                break;
                            case "SATURDAY":
                                if (lstCarrierBE[0].APP_CannotDeliveryOnSaturday == true)
                                    AllowedDay = false;
                                break;
                        }

                        if (AllowedDay == false)
                        {
                            bCarrierValidation = false;
                            //error               
                            if (hdnCurrentRole.Value == "Carrier")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_04_EXT");  // "This date is not available for your booking. Please select another date or contact Office Depot to make booking on this date.";
                                ShowErrorMessage(errMsg, "BK_MS_04_EXT");
                            }
                            else
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_04_INT_Carrier"); // "There is a constraint against the vendor to deliver on this date, do you want to continue?";
                                AddWarningMessages(errMsg, "BK_MS_04_INT_Carrier");
                            }
                        }


                        //Check Maximum Deliveries In A Day
                        if (lstCarrierBE[0].APP_MaximumDeliveriesInADay != null && lstCarrierBE[0].APP_MaximumDeliveriesInADay > 0)
                        {

                            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
                            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

                            oAPPBOK_BookingBE.Action = "CheckCarrierBooking";
                            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
                            oAPPBOK_BookingBE.Carrier = new MASCNT_CarrierBE();
                            oAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);

                            int? BookingDeatils = oAPPBOK_BookingBAL.CheckVendorBookingBAL(oAPPBOK_BookingBE);

                            if (BookingDeatils >= lstCarrierBE[0].APP_MaximumDeliveriesInADay)
                            {
                                bCarrierValidation = false;
                                if (hdnCurrentRole.Value == "Carrier")
                                {
                                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_27_EXT"); // +" " + BookingDeatils.ToString() + ")."; // "Your maximum number of deliveries per day has already been reached (Max - " + BookingDeatils.ToString() + ").";
                                    ShowErrorMessage(errMsg, "BK_MS_27_EXT");
                                }
                                else
                                {
                                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_27_INT_Carrier") + " " + BookingDeatils.ToString() + ")." + WebCommon.getGlobalResourceValue("BK_MS_27_INT_2");  //"Maximum number of deliveries per day has been reached for this carrier (Max - " + BookingDeatils.ToString() + "). Do you want to continue?";
                                    AddWarningMessages(errMsg, "BK_MS_27_INT");
                                }
                            }
                        }
                    }

                    //Befor Time and After Time               
                    BeforeSlotTimeID = lstCarrierBE[0].BeforeOrderBYId.ToString();
                    AfterSlotTimeID = lstCarrierBE[0].AfterOrderBYId.ToString() != "0" ? lstCarrierBE[0].AfterOrderBYId.ToString() : "300";

                    //if (dataBeforAfter.Count > 0)
                    //{
                    //    //BeforeSlotTimeID= dataBeforAfter.Select(x => x.BeforeOrderBYId).ToString();
                    //    //AfterSlotTimeID = dataBeforAfter.Select(x => x.AfterOrderBYId).ToString();

                    //    if (dataBeforAfter.Min(x => x.BeforeOrderBYId) > 0)
                    //    {
                    //        BeforeSlotTimeID = dataBeforAfter.Min(x => x.BeforeOrderBYId).ToString();
                    //    }
                    //    else
                    //    {
                    //        BeforeSlotTimeID = "0";
                    //    }
                    //    if (dataBeforAfter.Max(x => x.AfterOrderBYId) > 0)
                    //    {
                    //        AfterSlotTimeID = dataBeforAfter.Max(x => x.AfterOrderBYId).ToString();
                    //    }
                    //    else
                    //    {
                    //        AfterSlotTimeID = "300";
                    //    }
                    //}

                    ShowWarningMessages();
                }
                //---------------------------------------------------//
            }
        }
        return bCarrierValidation;
    }

    //--Stage 10 Point 10L 5.2----//   
    private bool VendorValidationExc(string ucSeacrhVendorID)
    {

        bool bVendorValidation = true;

        string VendorNo = ucSeacrhVendorID; // ucSeacrhVendorNo; // ((ucTextbox)ucSeacrhVendor1.FindControl("txtVendorNo")).Text;

        if (ddlSite.IsBookingExclusions)
        {
            if (VendorNo != string.Empty)
            {

                VendorBeforeSlotTimeID = "0";
                VendorAfterSlotTimeID = "300";

                UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
                UP_VendorBE oUP_VendorBE = new UP_VendorBE();

                oUP_VendorBE.Action = "ValidateVendorExc";
                oUP_VendorBE.Vendor_No = VendorNo.Trim();
                oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                oUP_VendorBE.Site.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                List<UP_VendorBE> lstUPVendor = oUP_VendorBAL.ValidateVendorBAL(oUP_VendorBE);

                //BeforeSlotTimeID = string.Empty;
                //AfterSlotTimeID = string.Empty;
                string errMsg = string.Empty;


                //var dataBeforAfter = (from p in lstUPVendor
                //                      select new
                //                      {
                //                          BeforeOrderBYId = p.VendorDetails.BeforeOrderBYId,
                //                          AfterOrderBYId = p.VendorDetails.AfterOrderBYId
                //                      }).ToList();

                if (lstUPVendor.Count > 0)
                {

                    lstUPVendor = lstUPVendor.FindAll(delegate(UP_VendorBE ven)
                    {
                        return ven.VendorDetails.IsConstraintsDefined == true;
                    });
                }

                if (lstUPVendor.Count > 0)
                {

                    List<UP_VendorBE> lstUPVendors = null;
                    UP_VendorBE[] newArray = null;

                    if (ViewState["UPVendors"] != null)
                    {
                        newArray = (UP_VendorBE[])ViewState["UPVendors"];
                        lstUPVendors = new List<UP_VendorBE>(newArray);
                        if (lstUPVendors.FindAll(delegate(UP_VendorBE v) { return v.VendorID == lstUPVendor[0].VendorID; }).Count == 0)
                        {
                            lstUPVendors.AddRange(lstUPVendor);
                            ViewState.Add("UPVendors", lstUPVendors.ToArray());
                        }
                    }
                    else
                    {
                        ViewState.Add("UPVendors", lstUPVendor.ToArray());
                    }

                    bool AllowedDay = true;

                    if (txtSchedulingdate.innerControltxtDate.Value != string.Empty)
                    {
                        string Weekday = LogicalSchedulDateTime.DayOfWeek.ToString().ToUpper();

                        switch (Weekday)
                        {
                            case "SUNDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnSunday == true)
                                    AllowedDay = false;
                                break;
                            case "MONDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnMonday == true)
                                    AllowedDay = false;
                                break;
                            case "TUESDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnTuesday == true)
                                    AllowedDay = false;
                                break;
                            case "WEDNESDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnWednessday == true)
                                    AllowedDay = false;
                                break;
                            case "THURSDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnThrusday == true)
                                    AllowedDay = false;
                                break;
                            case "FRIDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnFriday == true)
                                    AllowedDay = false;
                                break;
                            case "SATURDAY":
                                if (lstUPVendor[0].VendorDetails.APP_CannotDeliveryOnSaturday == true)
                                    AllowedDay = false;
                                break;
                        }

                        if (AllowedDay == false)
                        {
                            bVendorValidation = false;
                            //error               
                            if (hdnCurrentRole.Value == "Carrier")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_04_EXT_Carrier");  // "This date is not available for your booking. Please select another date or contact Office Depot to make booking on this date.";
                                ShowErrorMessage(errMsg, "BK_MS_04_EXT_Carrier");
                            }
                            else
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_04_INT"); // "There is a constraint against the vendor to deliver on this date, do you want to continue?";
                                AddWarningMessages(errMsg, "BK_MS_04_INT");
                            }
                        }
                    }

                    //Befor Time and After Time
                    //if (lstUPVendor[0].VendorDetails.IsConstraintsDefined) {
                    //    BeforeSlotTimeID = lstUPVendor[0].VendorDetails.BeforeOrderBYId.ToString();
                    //    AfterSlotTimeID = lstUPVendor[0].VendorDetails.AfterOrderBYId.ToString() != "0" ? lstUPVendor[0].VendorDetails.AfterOrderBYId.ToString() : null;
                    //}

                    //Befor Time and After Time   
                    if (lstUPVendor[0].VendorDetails.IsConstraintsDefined)
                    {
                        VendorBeforeSlotTimeID = lstUPVendor[0].VendorDetails.BeforeOrderBYId.ToString();
                        VendorAfterSlotTimeID = lstUPVendor[0].VendorDetails.AfterOrderBYId.ToString() != "0" ? lstUPVendor[0].VendorDetails.AfterOrderBYId.ToString() : "300";

                        //if (dataBeforAfter.Count > 0)
                        //{
                        //    //VendorBeforeSlotTimeID = dataBeforAfter.Where(x => x.BeforeOrderBYId != 0).Min(x => x.BeforeOrderBYId) != 0 ? dataBeforAfter.Where(x => x.BeforeOrderBYId != 0).Min(x => x.BeforeOrderBYId).ToString() : "0";
                        //    //VendorAfterSlotTimeID = dataBeforAfter.Where(x => x.AfterOrderBYId != 0).Max(x => x.AfterOrderBYId) != 0 ? dataBeforAfter.Where(x => x.AfterOrderBYId != 0).Max(x => x.AfterOrderBYId).ToString() : "300";


                        //    if (dataBeforAfter.Min(x => x.BeforeOrderBYId) > 0)
                        //    {
                        //        VendorBeforeSlotTimeID = dataBeforAfter.Min(x => x.BeforeOrderBYId).ToString();
                        //    }
                        //    else
                        //    {
                        //        VendorBeforeSlotTimeID = "0";
                        //    }
                        //    if (dataBeforAfter.Max(x => x.AfterOrderBYId) > 0)
                        //    {
                        //        VendorAfterSlotTimeID = dataBeforAfter.Max(x => x.AfterOrderBYId).ToString();
                        //    }
                        //    else
                        //    {
                        //        VendorAfterSlotTimeID = "300";
                        //    }
                        //}

                        hdnVendorAfterSlotTimeID.Value = VendorAfterSlotTimeID;
                        hdnVendorBeforeSlotTimeID.Value = VendorBeforeSlotTimeID;
                        hdnIsBookingExclusions.Value = "true";
                    }
                }
            }
        }
        //if (bVendorValidation) {
        //    if (ddlSite.IsBookingExclusions) {
        //        bVendorValidation = CarrierValidationExc();
        //    }
        //}
        if (!bVendorValidation)
            ShowWarningMessages();
        return bVendorValidation;

    }
    //-----------------//

    //--Stage 3 Point 4----//
    private bool CheckSiteClosure()
    {
        bool bSiteClosure = false;
        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
        {

            MASSIT_HolidayBE oMASSIT_HolidayBE = new MASSIT_HolidayBE();
            MASSIT_HolidayBAL oMASSIT_HolidayBAL = new MASSIT_HolidayBAL();

            oMASSIT_HolidayBE.Action = "ShowAll";
            oMASSIT_HolidayBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            if (ddlSite.innerControlddlSite.SelectedIndex >= 0)
                oMASSIT_HolidayBE.Site.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : 0;

            oMASSIT_HolidayBE.HolidayDate = LogicalSchedulDateTime;

            List<MASSIT_HolidayBE> lstHoliday = oMASSIT_HolidayBAL.GetHolidaysBAL(oMASSIT_HolidayBE);

            if (lstHoliday != null && lstHoliday.Count > 0)
            {
                string errMsg = WebCommon.getGlobalResourceValue("BK_MS_36"); // Site is closed on this date. Please select a different date.
                ShowErrorMessage(errMsg, "BK_MS_36");
                bSiteClosure = true;
            }
        }
        return bSiteClosure;
    }
    //---------------------//

    public override void PostDate_Change()
    {
        base.PostDate_Change();
        Date_Change(true);
    }

    private bool Date_Change(bool isDateChange)
    {
        if (ltSCdate.Visible == true)
        {
            if (LogicalSchedulDateTime < DateTime.Now.Date)
            {
                string errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT"); // "Booking not allowed in past date.";
                ShowErrorMessage(errMsg, "BK_MS_05_EXT");
                return false;
            }
        }
        else
        {
            if (txtSchedulingdate.GetDate.Date < DateTime.Now.Date)
            {
                string errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT"); // "Booking not allowed in past date.";
                ShowErrorMessage(errMsg, "BK_MS_05_EXT");
                return false;
            }
        }

        if (isDateChange)
        {
            hdnActualScheduleDate.Value = txtSchedulingdate.innerControltxtDate.Value;
            hdnLogicalScheduleDate.Value = txtSchedulingdate.innerControltxtDate.Value;

            LogicalSchedulDateTime = txtSchedulingdate.GetDate; // Utilities.Common.TextToDateFormat(txtSchedulingdate.innerControltxtDate.Value);
            ActualSchedulDateTime = txtSchedulingdate.GetDate; // Utilities.Common.TextToDateFormat(txtSchedulingdate.innerControltxtDate.Value);
        }

        if (CheckSiteClosure())
            return false;

        if (!GetToleratedDays())
        {
            GetMaximumCapacity();

            CarrierValidationExc();


            if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
            {

                string OpenDoorIds = GetDoorConstraints();

                if (OpenDoorIds == string.Empty)
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_EXT"); // "No space available on any compatible doors.Please contact Goods In office or select a different date.";

                    if (hdnCurrentRole.Value == "Vendor")
                    {
                        //Show err msg
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        return false;
                    }
                    else
                    {
                        //Show err msg
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_INT"); //"No space available on any compatible doors.";
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        return false;
                    }
                }
            }
        }
        ShowWarningMessages();
        return true;
    }

    protected void btnProceedAddPO_Click(object sender, EventArgs e)
    {
        ViewState["dtVolumeDetails"] = null;
        if (!txtSchedulingdate.isPostBackDone)
        {
            if (!Date_Change(false))
            {
                return;
            }
        }
        else
        {
            if (CheckSiteClosure())
                return;
        }

        if (ddlCarrier.SelectedIndex <= 0 || ddlDeliveryType.SelectedIndex <= 0 || ddlVehicleType.SelectedIndex <= 0)
        {
            return;
        }

        // if (ltSCdate.Visible == false && hdnCurrentRole.Value == "Carrier")
        if (ltSCdate.Visible == false)
         {
            //changes done by shrish 
            if (ActualSchedulDateTime.ToString("dd/MM/yyyy") != txtSchedulingdate.innerControltxtDate.Value)
            {
                hdnActualScheduleDate.Value = txtSchedulingdate.innerControltxtDate.Value;
                hdnLogicalScheduleDate.Value = txtSchedulingdate.innerControltxtDate.Value;

                LogicalSchedulDateTime = txtSchedulingdate.GetDate; // Utilities.Common.TextToDateFormat(txtSchedulingdate.innerControltxtDate.Value);
                ActualSchedulDateTime = txtSchedulingdate.GetDate; // Utilities.Common.TextToDateFormat(txtSchedulingdate.innerControltxtDate.Value);
            }
        }

        if (chkSiteSetting.Checked)
        {
            lblFromCountryValue.Text = ddlSourceCountry.innerControlddlCountry.SelectedItem.Text;
        }

        ProceedAddPO();
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        mdlNoSlotFound.Hide();
    }

    private void ProceedAddPO()
    {
        ltSiteName.Text = ddlSite.innerControlddlSite.SelectedItem.Text;
        ltDeliveryType.Text = ddlDeliveryType.SelectedItem.Text;
        ltCarrier.Text = ddlCarrier.SelectedItem.Text;
        ltOtherCarrier.Text = "[" + txtCarrierOther.Text.Trim() + "]";
        if (txtCarrierOther.Text.Trim() != string.Empty)
        {
            ltOtherCarrier.Visible = true;
        }
        else
        {
            ltOtherCarrier.Visible = false;
        }

        ltVehicleType.Text = ddlVehicleType.SelectedItem.Text;
        ltOtherVehicleType.Text = "[" + txtVehicleOther.Text.Trim() + "]";
        if (txtVehicleOther.Text.Trim() != string.Empty)
        {
            ltOtherVehicleType.Visible = true;
        }
        else
        {
            ltOtherVehicleType.Visible = false;
        }

        ltSchedulingDate.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");

        mvBookingEdit.ActiveViewIndex = 1;
        if (hdnCurrentMode.Value == "Add")
            BindPOGrid();
        else if (hdnCurrentMode.Value == "Edit")
        {
            BindAllPOGrid();
            BindVolumeDetailsGrid();
        }

        // Sprint 4 - Point 1 - Begin
        txtEditBookingCommentF.Text = txtEditBookingComment.Text;
        // Sprint 4 - Point 1 - End

        ShowHazordousItem();
        if (checkBoxEnableHazardouesItemPrompt.Checked == true)
        {
            chkHazardoustick.Checked = true;
            chkHazardoustick.Enabled = false;
        }
        else
        {
            chkHazardoustick.Enabled = false;
        }
    }

    protected void btnAddPO_Click(object sender, EventArgs e)
    {
        // Sprint 1 - Point 1 - Begin
        string errMsg = string.Empty;

        if (txtPurchaseNumber.Text != string.Empty)
        {
            // Here getting Vendor Id from PO list.
            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
            oUp_PurchaseOrderDetailBE.Action = "GetNewBookingPODetails";
            oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseNumber.Text;
            oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);
            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);
            int intVendorID = 0;

            foreach (Up_PurchaseOrderDetailBE po in lstPO)
            {
                intVendorID = po.Vendor.VendorID;
                if (intVendorID > 0)
                    break;
            }

            #region Commented code for point 31
            //// Here checking multiple vendor's po allow for site or not to Carrier.
            //if (gvPO.Rows.Count > 0 && Convert.ToString(Session["Role"]).Trim().ToLower() == "carrier") {
            //    if (ddlSite.innerControlddlSite.SelectedValue != null) {
            //        MAS_SiteBE singleSiteSetting =
            //            GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));

            //        if (singleSiteSetting.PerBookingVendor.Trim().ToUpper() == "V") {
            //            bool isBreakCheck = true;
            //            for (int index = 0; index < gvPO.Rows.Count; index++) {
            //                HiddenField hdnPOVendorId = (HiddenField)gvPO.Rows[index].FindControl("hdnPOVendorId");
            //                if (hdnPOVendorId != null) {
            //                    isBreakCheck = true;
            //                    foreach (Up_PurchaseOrderDetailBE po in lstPO) {
            //                        if (Convert.ToInt32(hdnPOVendorId.Value) != po.Vendor.VendorID) {
            //                            isBreakCheck = false;
            //                            break;
            //                        }
            //                    }

            //                    if (!isBreakCheck) {
            //                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + strIsVendorRequiredForSeparateBooking + "');</script>", false);
            //                        return;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            #endregion

            IsPOValid(intVendorID);

        }
        else
        {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_CorrectPO"); //"Please enter correct Purchase Order Number.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
        }
        // Sprint 1 - Point 1 - End
    }

    private void IsPOValid(int intVendorID)
    {
        string errMsg = string.Empty;
        bool isWarning = false;

        if (IsBookingValidationExcluded == false)
        {  //If there is no Validation Exclusion for the vendor/carrier or booking type

            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

            oUp_PurchaseOrderDetailBE.Action = "GetAllPODetails";
            oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseNumber.Text.Trim();
            oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);

            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllPODetailsBAL(oUp_PurchaseOrderDetailBE);

            if (lstPO.Count <= 0)
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); // "The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                return;
                //if (hdnCurrentRole.Value == "Carrier") {
                //    errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); //"The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                //    ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                //    return;
                //}
                //else {
                //    errMsg = "The Purchase Order Number you have supplied is not valid. Do you want to continue?";
                //    AddWarningMessages(errMsg, "BK_MS_11_INT");
                //    isWarning = true;
                //}
            }

            if (lstPO.Count > 0)
            {
                if (lstPO[0].LastUploadedFlag == "N")
                {
                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); //"The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                    ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                    return;
                }
            }
        }

        if (isWarning == true)
        {
            ShowWarningMessages();
        }
        else
        {
            // Here checking PO is existing or not.
            APPBOK_BookingBE appbok_BookingBE = new APPBOK_BookingBE();
            appbok_BookingBE.Action = "IsExistingPO";
            appbok_BookingBE.PurchaseOrders = txtPurchaseNumber.Text;
            appbok_BookingBE.VendorID = Convert.ToInt32(intVendorID);
            appbok_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            APPBOK_BookingBAL appbok_BookingBAL = new APPBOK_BookingBAL();
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = appbok_BookingBAL.IsExistingPO(appbok_BookingBE);

            if (lstAPPBOK_BookingBE.Count == 0)
            {
                if (VendorValidationExc(Convert.ToString(intVendorID)))   //Stage 10 Point 10L 5.2
                    this.AddPO(true);
            }
            else
            {
                foreach (APPBOK_BookingBE oAPPBOK_BookingBE in lstAPPBOK_BookingBE)
                {
                    #region Double booking warning message formatting.
                    errMsg = string.Format("<table><tr><td style=\"text-align:center\">{0}</td></tr>",
                        WebCommon.getGlobalResourceValue("BK_MS_29_Warning_1"));

                    errMsg = string.Format("{0}<tr><td style=\"text-align: center\">{1}</td></tr>",
                        errMsg, WebCommon.getGlobalResourceValue("BK_MS_29_Warning_2").Replace("##dd/mm/yyyy##", "<b>" + Convert.ToDateTime(oAPPBOK_BookingBE.ScheduleDate).ToString("dd/MM/yyyy") + "</b>"));

                    errMsg = string.Format("{0}<tr><td style=\"text-align: center\">{1}</td></tr>",
                        errMsg, WebCommon.getGlobalResourceValue("BK_MS_29_Warning_3").Replace("##tt:mm##", String.Format("{0}", "<b>" + oAPPBOK_BookingBE.SlotTime.SlotTime) + "</b>"));

                    errMsg = string.Format("{0}<tr><td style=\"text-align: center\">{1}</td></tr></table>",
                        errMsg, WebCommon.getGlobalResourceValue("BK_MS_29_Warning_4"));
                    #endregion
                    break; // Here being break because need only zero index value.
                }

                //  lblWarningText.Visible = true;
                ViewState["WarningStatus"] = 1;
                ltWarningConfirm.Text = errMsg;
                mdlWarningConfirm.Show();
                return;
            }
        }
    }

    private void AddPO(bool isValidPOChecked = false)
    {
        string errMsg = string.Empty;

        if (txtPurchaseNumber.Text != string.Empty)
        {

            DataTable myDataTable = null;

            if (ViewState["myDataTable"] != null)
            {
                myDataTable = (DataTable)ViewState["myDataTable"];

                //Check Item alrady added
                DataRow[] drr = myDataTable.Select("PurchaseNumber='" + txtPurchaseNumber.Text.Trim() + "'");

                if (drr.Length > 0)
                {
                    errMsg = WebCommon.getGlobalResourceValue("PurchaseOrderExist"); //"The Purchase Order Number you have supplied already exists.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
                    txtPurchaseNumber.Text = string.Empty;
                    return;
                }
            }

            if (IsBookingValidationExcluded == false)
            {  //If there is no Validation Exclusion for the vendor/carrier or booking type

                bool isWarning = false;

                Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

                oUp_PurchaseOrderDetailBE.Action = "GetAllPODetails";
                oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseNumber.Text.Trim();
                oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);
                //oUp_PurchaseOrderDetailBE.Vendor = new UP_VendorBE();
                // oUp_PurchaseOrderDetailBE.Vendor.VendorID = Convert.ToInt32(ucSeacrhVendor1.VendorNo);

                List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllPODetailsBAL(oUp_PurchaseOrderDetailBE);

                if (!isValidPOChecked)
                {
                    if (lstPO.Count <= 0)
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); // "The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                        ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                        return;
                        //if (Session["Role"].ToString() == "Vendor" || Session["Role"].ToString() == "Carrier") {
                        //    errMsg = "The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                        //    ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                        //    return;
                        //}
                        //else {
                        //    errMsg = "The Purchase Order Number you have supplied is not valid. Do you want to continue?";
                        //    AddWarningMessages(errMsg, "BK_MS_11_INT");
                        //    isWarning = true;
                        //}
                    }
                }

                if (lstPO.Count > 0)
                {

                    if (!isValidPOChecked)
                    {
                        if (lstPO[0].LastUploadedFlag == "N")
                        {
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_11_EXT"); //"The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                            ShowErrorMessage(errMsg, "BK_MS_11_EXT");
                            return;
                        }
                    }

                    if (Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value) != lstPO[0].Site.SiteID)
                    {

                        //Cross docking check

                        MASCNT_CrossDockBE oMASCNT_CrossDockBE = new MASCNT_CrossDockBE();
                        APPCNT_CrossDockBAL oMASCNT_CrossDockBAL = new APPCNT_CrossDockBAL();

                        oMASCNT_CrossDockBE.Action = "GetDestSiteById";
                        oMASCNT_CrossDockBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                        oMASCNT_CrossDockBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                        List<MASCNT_CrossDockBE> lstCrossDock = oMASCNT_CrossDockBAL.GetCrossDockDetailsBAL(oMASCNT_CrossDockBE);

                        if (lstCrossDock != null && lstCrossDock.Count > 0)
                        {

                            lstCrossDock = lstCrossDock.FindAll(delegate(MASCNT_CrossDockBE cd)
                            {
                                return cd.DestinationSiteID == lstPO[0].Site.SiteID;
                            });
                        }

                        if (lstCrossDock == null || lstCrossDock.Count == 0)
                        {
                            if (Session["Role"].ToString() == "Vendor" || Session["Role"].ToString() == "Carrier")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_14_EXT").Replace("##Ponumber##", txtPurchaseNumber.Text.Trim()).Replace("##Sitename##", lstPO[0].Site.SiteName);//"The Purchase Order you have quoted is not for the Office Depot site you have selected." +
                                //"Please recheck the entries made and resubmit the correct information";
                                ShowErrorMessage(errMsg, "BK_MS_14_EXT");
                                return;
                            }
                            else
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_14_INT").Replace("##Ponumber##", txtPurchaseNumber.Text.Trim()).Replace("##Sitename##", lstPO[0].Site.SiteName);//"The Purchase Order you have quoted is not for the Office Depot site you have selected. Do you want to continue?";
                                AddWarningMessages(errMsg, "BK_MS_14_INT");
                                isWarning = true;
                            }
                        }
                    }



                    int? ToleranceDueDay = lstPO[0].Site.ToleranceDueDay;

                    //DateTime PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.Value.Day.ToString() + "/" + lstPO[0].Original_due_date.Value.Month.ToString() + "/" + lstPO[0].Original_due_date.Value.Year.ToString()));

                    DateTime PODueDate = DateTime.Now;
                    if (lstPO[0].Expected_date != null) // This condition is given, if [Expected Date] will come null then [Original due date] will be set like earlier.
                    {
                        PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Expected_date.Value.Day.ToString() + "/" + lstPO[0].Expected_date.Value.Month.ToString() + "/" + lstPO[0].Expected_date.Value.Year.ToString()));
                    }
                    else
                    {
                        PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.Value.Day.ToString() + "/" + lstPO[0].Original_due_date.Value.Month.ToString() + "/" + lstPO[0].Original_due_date.Value.Year.ToString()));
                    }

                    //DateTime CurrentDate = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString());
                    DateTime ScheduledDate = LogicalSchedulDateTime;  //SchDate; // Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.ToString()));

                    //OTIF Future Date PO check//
                    bool isOTIFPO = false;
                    CntOTIF_FutureDateSetupBE oCntOTIF_FutureDateSetupBE = new CntOTIF_FutureDateSetupBE();
                    CntOTIF_FutureDateSetupBAL oCntOTIF_FutureDateSetupBAL = new CntOTIF_FutureDateSetupBAL();
                    oCntOTIF_FutureDateSetupBE.Action = "ShowAll";
                    oCntOTIF_FutureDateSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oCntOTIF_FutureDateSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                    oCntOTIF_FutureDateSetupBE.CountryID = Convert.ToInt32(PreSiteCountryID);
                    List<CntOTIF_FutureDateSetupBE> lstCountryFutureDatePO = oCntOTIF_FutureDateSetupBAL.GetCntOTIF_FutureDateSetupDetailsBAL(oCntOTIF_FutureDateSetupBE);
                    if (lstCountryFutureDatePO != null && lstCountryFutureDatePO.Count > 0)
                    {
                        // This condition is given, if [Expected Date] will come null then [Original due date] will be set like earlier.
                        if (lstPO[0].Expected_date != null)
                        {
                            // Sprint 1 - Point 14 - Begin - Changing from [Original_due_date] to [Expected Date]
                            /*if (lstPO[0].Original_due_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Original_due_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth))*/
                            if (lstPO[0].Expected_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Expected_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth))
                            // Sprint 1 - Point 14 - End - Changing from [Original_due_date] to [Expected Date]
                            {
                                //This is OTIF PO Date
                                isOTIFPO = true;
                            }
                        }
                        else
                        {
                            if (lstPO[0].Original_due_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Original_due_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth))
                            {
                                //This is OTIF PO Date
                                isOTIFPO = true;
                            }
                        }
                    }
                    //------------------------//

                    bool isError = false;

                    if (isOTIFPO == false)
                    {
                        if (lstPO[0].Site.ToleranceDueDay == 0)
                        {
                            if (ScheduledDate < PODueDate)
                            { //if (ScheduledDate < PODueDate) {
                                isError = true;
                            }
                        }
                        else if (lstPO[0].Site.ToleranceDueDay > 0)
                        {
                            //if (PODueDate > ScheduledDate.AddDays(Convert.ToDouble(lstPO[0].Site.ToleranceDueDay))) {
                            //    isError = true; 
                            //}

                            //----- for weekend --------------------//
                            int DaysDiff = 0;
                            if (PODueDate >= ScheduledDate)
                            {
                                DaysDiff = Enumerable.Range(0, Convert.ToInt32(PODueDate.Subtract(ScheduledDate).TotalDays)).Select(i => new[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(ScheduledDate.AddDays(i).DayOfWeek) ? 1 : 0).Sum();
                            }

                            if (PODueDate > ScheduledDate.AddDays(Convert.ToDouble(lstPO[0].Site.ToleranceDueDay + DaysDiff)))
                            {
                                isError = true;
                            }
                        }

                        if (isError)
                        {           //error     
                            if (Session["Role"].ToString() == "Vendor" || Session["Role"].ToString() == "Carrier")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_13_EXT").Replace("##dd/mm/yyyy##", PODueDate.ToString("dd/MM/yyyy")); // "The  Purchase Order quoted is not due in on the date selected. Please rebook this Purchase Order for the correct date. <br> Due Date:" + PODueDate.ToString("dd/MM/yyyy"); 
                                ShowErrorMessage(errMsg, "BK_MS_13_EXT");
                                return;
                            }
                            else
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_13_INT").Replace("##dd/mm/yyyy##", PODueDate.ToString("dd/MM/yyyy")); // The Purchase Order quoted is not due in for this date.The correct due date is dd/mm/yyyy. Do you want to continue?
                                AddWarningMessages(errMsg, "BK_MS_13_INT");
                                isWarning = true;
                            }
                        }
                    }
                }

                if (isWarning == false)
                    BindPOGrid();
                else
                    ShowWarningMessages();
            }
            else if (IsBookingValidationExcluded == true)
            {  //If there is Validation Exclusion for the vendor/carrier or booking type
                BindPOGrid();
            }
        }
    }

    private string GetFormatedDate(string DateToFormat)
    {
        if (DateToFormat != string.Empty)
        {
            string[] arrDate = DateToFormat.Split('/');
            string DD, MM, YYYY;
            if (arrDate[0].Length < 2)
                DD = "0" + arrDate[0].ToString();
            else
                DD = arrDate[0];

            if (arrDate[1].Length < 2)
                MM = "0" + arrDate[1].ToString();
            else
                MM = arrDate[1];

            YYYY = arrDate[2].ToString().Substring(0, 4);

            return DD + "/" + MM + "/" + YYYY.Trim();
        }
        else
        {
            return DateToFormat;
        }
    }

    private string GetFormatedTime(string TimeToFormat)
    {
        if (TimeToFormat != string.Empty)
        {
            string[] arrDate = TimeToFormat.Split(':');
            string HH, MM;
            if (arrDate[0].Length < 2)
                HH = "0" + arrDate[0].ToString();
            else
                HH = arrDate[0];

            if (arrDate[1].Length < 2)
                MM = "0" + arrDate[1].ToString();
            else
                MM = arrDate[1];


            return HH + ":" + MM;
        }
        else
        {
            return TimeToFormat;
        }
    }

    protected void BindAllPOGrid()
    {
        DataTable myDataTable = null;
        myDataTable = new DataTable();

        DataColumn auto = new DataColumn("AutoID", typeof(System.Int32));
        // specify it as auto increment field
        auto.AutoIncrement = true;
        auto.AutoIncrementSeed = 1;
        auto.ReadOnly = true;
        auto.Unique = true;
        myDataTable.Columns.Add(auto);

        DataColumn myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "VendorID";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "PurchaseNumber";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Original_due_date";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "VendorName";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "OutstandingLines";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Qty_On_Hand";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "qty_on_backorder";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "SiteName";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "PurchaseOrderID";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "ExpectedDate";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "LinesToBeDelivered";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "IsProvisionResonChangesUpdate";
        myDataTable.Columns.Add(myDataColumn);

        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        oUp_PurchaseOrderDetailBE.Action = "GetAllBookingPODetails";
        oUp_PurchaseOrderDetailBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());

        List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllBookedProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);

        if (lstPO.Count > 0)
        {
            for (int iCount = 0; iCount < lstPO.Count; iCount++)
            {
                DataRow dataRow = myDataTable.NewRow();

                //oUp_PurchaseOrderDetailBE.Action = "GetStockDetails";
                //oUp_PurchaseOrderDetailBE.Purchase_order = lstPO[iCount].Purchase_order;
                //oUp_PurchaseOrderDetailBE.Site = new MAS_SiteBE();
                //oUp_PurchaseOrderDetailBE.Site.SiteID = lstPO[iCount].Site.SiteID;  //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                //List<Up_PurchaseOrderDetailBE> lstPOStock = oUP_PurchaseOrderDetailBAL.GetPriorityBAL(oUp_PurchaseOrderDetailBE);

                //if (lstPOStock != null && lstPOStock.Count > 0) {

                //    List<Up_PurchaseOrderDetailBE> lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.Qty_On_Hand) == 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });

                //    if (lstPOTemp != null && lstPOTemp.Count > 0) {
                //        dataRow["Qty_On_Hand"] = lstPOTemp.Count.ToString();
                //    }
                //    else {
                //        dataRow["Qty_On_Hand"] = "0";
                //    }

                //    //lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.qty_on_backorder) > 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });
                //    lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) {
                //        return (Convert.ToDecimal(p.SKU.Qty_On_Hand) - Convert.ToDecimal(p.SKU.qty_on_backorder) < 0);
                //    }
                // );
                //    if (lstPOTemp != null && lstPOTemp.Count > 0) {
                //        dataRow["qty_on_backorder"] = lstPOTemp.Count.ToString();
                //    }
                //    else {
                //        dataRow["qty_on_backorder"] = "0";
                //    }
                //}
                //else {
                //    dataRow["Qty_On_Hand"] = "0";
                //    dataRow["qty_on_backorder"] = "0";
                //}


                dataRow["VendorID"] = lstPO[iCount].Vendor.VendorID.ToString();
                dataRow["PurchaseNumber"] = lstPO[iCount].Purchase_order;
                dataRow["Original_due_date"] = lstPO[iCount].Original_due_date.Value.ToString("dd/MM/yyyy");
                dataRow["VendorName"] = lstPO[iCount].Vendor.VendorName;

                dataRow["Qty_On_Hand"] = lstPO[iCount].Stockouts;
                dataRow["IsProvisionResonChangesUpdate"] = lstPO[iCount].IsProvisionResonChangesUpdate;
                dataRow["qty_on_backorder"] = lstPO[iCount].Backorders;
                if (lstPO[iCount].OutstandingLines > 0)
                {
                    dataRow["OutstandingLines"] = lstPO[iCount].OutstandingLines.ToString(); //lstPOStock.Count.ToString(); //
                    dataRow["LinesToBeDelivered"] = lstPO[iCount].OutstandingLines.ToString(); //lstPOStock.Count.ToString(); //
                }
                else
                {
                    dataRow["OutstandingLines"] = 1;
                    dataRow["LinesToBeDelivered"] = 1;
                }


                //dataRow["Qty_On_Hand"] = lstPO[iCount].SKU.Qty_On_Hand;
                //dataRow["qty_on_backorder"] = lstPO[iCount].SKU.qty_on_backorder;
                dataRow["SiteName"] = lstPO[iCount].Site.SiteName;
                dataRow["PurchaseOrderID"] = lstPO[iCount].PurchaseOrderID.ToString();
                //dataRow["LinesToBeDelivered"] = lstPOStock.Count.ToString();  //lstPO[0].OutstandingLines.ToString();


                if (lstPO[iCount].Expected_date != null)
                {
                    dataRow["ExpectedDate"] = lstPO[iCount].Expected_date.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    dataRow["ExpectedDate"] = lstPO[iCount].Original_due_date.Value.ToString("dd/MM/yyyy");
                }

                myDataTable.Rows.Add(dataRow);
            }
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            gvPO.DataSource = myDataTable;
            gvPO.DataBind();

            gvPOHistory.DataSource = myDataTable;
            gvPOHistory.DataBind();
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            ViewState["myDataTable"] = myDataTable;
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
            ViewState["myDataTable"] = null;
        }

        int iTotalLines = 0;
        foreach (DataRow dr in myDataTable.Rows)
        {
            if (dr["OutstandingLines"].ToString() != string.Empty)
            {
                //TotalLines += Convert.ToInt32(dr["OutstandingLines"]);
                iTotalLines += Convert.ToInt32(dr["OutstandingLines"]);
            }
        }

        //lblExpectedLines.Text = lblExpectedLines.Text.Replace(Lines, TotalLines.ToString());
        Lines = iTotalLines.ToString();



        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            ViewState["myDataTable"] = myDataTable;


            //Build Volume Details Settings--------------//
            DataTable dtVolumeDetails = null;
            for (int iCount = 0; iCount < myDataTable.Rows.Count; iCount++)
            {


                if (ViewState["dtVolumeDetails"] != null)
                {
                    dtVolumeDetails = (DataTable)ViewState["dtVolumeDetails"];

                    DataRow[] UpdateRow = dtVolumeDetails.Select("VendorID=" + myDataTable.Rows[iCount]["VendorID"].ToString());

                    if (UpdateRow != null && UpdateRow.Length > 0)
                    {
                        UpdateRow[0]["ExpectedLines"] = Convert.ToInt32(UpdateRow[0]["ExpectedLines"]) + Convert.ToInt32(myDataTable.Rows[iCount]["OutstandingLines"].ToString());
                        UpdateRow[0]["NoOfPO"] = (Convert.ToInt32(UpdateRow[0]["NoOfPO"]) + 1).ToString();
                        // UpdateRow[0]["LinesToBeDelivered"] = Convert.ToInt32(UpdateRow[0]["LinesToBeDelivered"]) + Convert.ToInt32(myDataTable.Rows[iCount]["LinesToBeDelivered"].ToString());
                    }
                    else
                    {
                        DataRow drdataRow = dtVolumeDetails.NewRow();
                        if (lstPO.Count > 0)
                        {
                            drdataRow["VendorID"] = myDataTable.Rows[iCount]["VendorID"].ToString();
                            drdataRow["VendorName"] = myDataTable.Rows[iCount]["VendorName"].ToString();
                            drdataRow["ExpectedLines"] = myDataTable.Rows[iCount]["OutstandingLines"].ToString();

                            drdataRow["NoOfPO"] = "1";
                            //drdataRow["LinesToBeDelivered"] = myDataTable.Rows[iCount]["LinesToBeDelivered"].ToString();
                        }
                        dtVolumeDetails.Rows.Add(drdataRow);
                    }
                }
                else
                {

                    dtVolumeDetails = new DataTable();

                    auto = new DataColumn("AutoID", typeof(System.Int32));
                    // specify it as auto increment field
                    auto.AutoIncrement = true;
                    auto.AutoIncrementSeed = 1;
                    auto.ReadOnly = true;
                    auto.Unique = true;
                    dtVolumeDetails.Columns.Add(auto);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "VendorID";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "VendorName";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "ExpectedLines";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "Pallets";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "Cartons";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "NoOfPO";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "LinesToBeDelivered";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "OldNumberOfPallet";
                    dtVolumeDetails.Columns.Add(myDataColumn);


                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "OldNumberOfCartons";
                    dtVolumeDetails.Columns.Add(myDataColumn);


                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "OldNumberOfLine";
                    dtVolumeDetails.Columns.Add(myDataColumn);


                    DataRow drdataRow = dtVolumeDetails.NewRow();
                    if (myDataTable.Rows.Count > 0)
                    {
                        drdataRow["VendorID"] = myDataTable.Rows[iCount]["VendorID"].ToString();
                        drdataRow["VendorName"] = myDataTable.Rows[iCount]["VendorName"].ToString();
                        drdataRow["ExpectedLines"] = myDataTable.Rows[iCount]["OutstandingLines"].ToString();
                        drdataRow["NoOfPO"] = "1";
                        //drdataRow["LinesToBeDelivered"] = Convert.ToString(myDataTable.Rows[iCount]["LinesToBeDelivered"]);
                    }
                    dtVolumeDetails.Rows.Add(drdataRow);
                }

                if (dtVolumeDetails != null && dtVolumeDetails.Rows.Count > 0)
                {
                    ViewState["dtVolumeDetails"] = dtVolumeDetails;
                }
                else
                {
                    ViewState["dtVolumeDetails"] = null;
                }
                //------------end volume details-------------//
            }

            if (dtVolumeDetails != null && dtVolumeDetails.Rows.Count > 0)
            {
                gvVolume.DataSource = dtVolumeDetails;
                gvVolume.DataBind();
            }

        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
            ViewState["myDataTable"] = null;
        }




        //for (int iCount = 0; iCount < myDataTable.Rows.Count; iCount++) {
        //    TotalLines += Convert.ToInt32(lstPO[0].OutstandingLines.ToString()); //Convert.ToInt32(dataRow["OutstandingLines"]);
        //}

        TotalLines = iTotalLines.ToString();

        txtPurchaseNumber.Text = string.Empty;

    }

    private void BindAlternateViewVolume()
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "GetCarrierVolumeDeatilsByID";
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());

        List<APPBOK_BookingBE> lstBookingDeatils = new List<APPBOK_BookingBE>();
        lstBookingDeatils = oAPPBOK_BookingBAL.GetCarrierVolumeDeatilsBAL(oAPPBOK_BookingBE);

        if (lstBookingDeatils != null && lstBookingDeatils.Count > 0)
        {

            int iTotalPallets = 0;
            int iTotalCartons = 0;
            int iTotalLines = 0;

            for (int iVol = 0; iVol < lstBookingDeatils.Count; iVol++)
            {
                iTotalCartons = iTotalCartons + Convert.ToInt32(lstBookingDeatils[iVol].NumberOfCartons);
                iTotalPallets = iTotalPallets + Convert.ToInt32(lstBookingDeatils[iVol].NumberOfPallet);
                iTotalLines = iTotalLines + Convert.ToInt32(lstBookingDeatils[iVol].NumberOfLines);
            }
            TotalCartons = iTotalCartons.ToString();
            TotalPallets = iTotalPallets.ToString();
            TotalLines = iTotalLines.ToString();
            ltNumberofPallets.Text = TotalPallets.Trim() != string.Empty ? TotalPallets.Trim() : "-";
            ltNumberofLines.Text = TotalLines.Trim() != string.Empty ? TotalLines.Trim() : "-";
            ltNumberofCartons.Text = TotalCartons.Trim() != string.Empty ? TotalCartons.Trim() : "-";
        }
    }

    private void BindVolumeDetailsGrid()
    {

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "GetCarrierVolumeDeatilsByID";
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());

        List<APPBOK_BookingBE> lstBookingDeatils = new List<APPBOK_BookingBE>();
        lstBookingDeatils = oAPPBOK_BookingBAL.GetCarrierVolumeDeatilsBAL(oAPPBOK_BookingBE);

        List<APPBOK_BookingBE> lstVolumeDeatils = new List<APPBOK_BookingBE>();

        DataTable dtVolumeDetails = null;
        BookedLines = 0;
        BookedPallets = 0;
        if (lstBookingDeatils != null && lstBookingDeatils.Count > 0)
        {

            foreach (GridViewRow gvRow in gvVolume.Rows)
            {
                if (gvRow.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hdngvVendorID = (HiddenField)gvRow.FindControl("hdngvVendorID");
                    ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvRow.FindControl("txtVendorPallets");
                    ucNumericTextbox txtVendorCartons = (ucNumericTextbox)gvRow.FindControl("txtVendorCartons");
                    ucLiteral ltVendorExpectedLines = (ucLiteral)gvRow.FindControl("ltVendorExpectedLines");
                    ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvRow.FindControl("txtLinesToBeDelivered");

                    lstVolumeDeatils = lstBookingDeatils.FindAll(delegate(APPBOK_BookingBE Bok) { return Bok.VendorID == Convert.ToInt32(hdngvVendorID.Value); });

                    if (lstVolumeDeatils != null && lstVolumeDeatils.Count > 0)
                    {
                        txtVendorPallets.Text = lstVolumeDeatils[0].NumberOfPallet.ToString();
                        txtVendorCartons.Text = lstVolumeDeatils[0].NumberOfCartons.ToString();

                        txtLinesToBeDelivered.Text = lstVolumeDeatils[0].NumberOfLines.ToString();  //ltVendorExpectedLines.Text; // 

                        BookedLines = BookedLines + Convert.ToInt32(ltVendorExpectedLines.Text);
                        BookedPallets = BookedPallets + Convert.ToInt32(txtVendorPallets.Text);
                        //-----Stage 10 Point 10L 5.1-------
                        //BookedCartons = BookedCartons + Convert.ToInt32(txtVendorCartons.Text);
                        //----------------------------------


                        if (ViewState["dtVolumeDetails"] != null)
                        {
                            dtVolumeDetails = (DataTable)ViewState["dtVolumeDetails"];

                            DataRow[] UpdateRow = dtVolumeDetails.Select("VendorID='" + hdngvVendorID.Value.ToString() + "'");

                            if (UpdateRow != null && UpdateRow.Length > 0)
                            {
                                UpdateRow[0]["Pallets"] = Convert.ToInt32(UpdateRow[0]["Pallets"] == DBNull.Value ? 0 : UpdateRow[0]["Pallets"]) + Convert.ToInt32(lstVolumeDeatils[0].NumberOfPallet.ToString());
                                UpdateRow[0]["Cartons"] = (Convert.ToInt32(UpdateRow[0]["Cartons"] == DBNull.Value ? 0 : UpdateRow[0]["Cartons"]) + Convert.ToInt32(lstVolumeDeatils[0].NumberOfCartons.ToString()));
                                UpdateRow[0]["LinesToBeDelivered"] = (Convert.ToInt32(UpdateRow[0]["LinesToBeDelivered"] == DBNull.Value ? 0 : UpdateRow[0]["LinesToBeDelivered"]) + Convert.ToInt32(lstVolumeDeatils[0].NumberOfLines.ToString()));

                                UpdateRow[0]["OldNumberOfPallet"] = (Convert.ToInt32(lstVolumeDeatils[0].OldNumberOfPallet == null ? 0 : Convert.ToInt32(lstVolumeDeatils[0].OldNumberOfPallet.ToString())));
                                UpdateRow[0]["OldNumberOfCartons"] = (Convert.ToInt32(lstVolumeDeatils[0].OldNumberOfCartons == null ? 0 : Convert.ToInt32(lstVolumeDeatils[0].OldNumberOfCartons.ToString())));
                                UpdateRow[0]["OldNumberOfLine"] = (Convert.ToInt32(lstVolumeDeatils[0].OldNumberOfLines == null ? 0 : Convert.ToInt32(lstVolumeDeatils[0].OldNumberOfLines.ToString())));
                            }
                        }


                    }
                }
            }
            if (dtVolumeDetails != null && dtVolumeDetails.Rows.Count > 0)
            {
                gvVolume.DataSource = dtVolumeDetails;
                gvVolume.DataBind();

                gvVolumeHistory.DataSource = dtVolumeDetails;
                gvVolumeHistory.DataBind();
            }
        }
    }

    protected void btnNotHappy_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPBok_BookingOverview.aspx");
    }

    protected void BindPOGrid()
    {

        if (txtPurchaseNumber.Text != string.Empty)
        {

            DataTable myDataTable = null;

            if (ViewState["myDataTable"] != null)
            {
                myDataTable = (DataTable)ViewState["myDataTable"];

                //Check Item alrady added
                DataRow[] drr = myDataTable.Select("PurchaseNumber='" + txtPurchaseNumber.Text.Trim() + "'");

                if (drr.Length > 0)
                {
                    return;
                }
            }
            else
            {

                myDataTable = new DataTable();
                DataColumn[] keys = new DataColumn[1];

                DataColumn auto = new DataColumn("AutoID", typeof(System.Int32));
                // specify it as auto increment field
                auto.AutoIncrement = true;
                auto.AutoIncrementSeed = 1;
                auto.ReadOnly = true;
                auto.Unique = true;
                myDataTable.Columns.Add(auto);
                keys[0] = auto;

                myDataTable.PrimaryKey = keys;

                DataColumn myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "VendorID";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseNumber";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Original_due_date";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "VendorName";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Pallets";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Cartons";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "OutstandingLines";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Qty_On_Hand";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "qty_on_backorder";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "SiteName";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseOrderID";
                myDataTable.Columns.Add(myDataColumn);

                // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "ExpectedDate";
                myDataTable.Columns.Add(myDataColumn);
                // Sprint 1 - Point 14 - End - Adding [Expected Date]
                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "IsProvisionResonChangesUpdate";
                myDataTable.Columns.Add(myDataColumn);
            }


            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

            oUp_PurchaseOrderDetailBE.Action = "GetNewBookingPODetails";
            oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseNumber.Text;
            oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(PreSiteCountryID);

            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);
            DataRow dataRow = myDataTable.NewRow();

            int OutstandingLines = 0;

            if (lstPO.Count > 0)
            {

                //oUp_PurchaseOrderDetailBE.Action = "GetStockDetails";
                //oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseNumber.Text;
                //oUp_PurchaseOrderDetailBE.Site = new MAS_SiteBE();
                //oUp_PurchaseOrderDetailBE.Site.SiteID = lstPO[0].Site.SiteID; // Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                //List<Up_PurchaseOrderDetailBE> lstPOStock = oUP_PurchaseOrderDetailBAL.GetPriorityBAL(oUp_PurchaseOrderDetailBE);

                //if (lstPOStock != null && lstPOStock.Count > 0) {

                //    List<Up_PurchaseOrderDetailBE> lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.Qty_On_Hand) == 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });

                //    if (lstPOTemp != null && lstPOTemp.Count > 0) {
                //        dataRow["Qty_On_Hand"] = lstPOTemp.Count.ToString();
                //    }
                //    else {
                //        dataRow["Qty_On_Hand"] = "0";
                //    }

                //    //lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.qty_on_backorder) > 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });
                //    lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) {
                //        return (Convert.ToDecimal(p.SKU.Qty_On_Hand) - Convert.ToDecimal(p.SKU.qty_on_backorder) < 0);
                //    }
                //  );
                //    if (lstPOTemp != null && lstPOTemp.Count > 0) {
                //        dataRow["qty_on_backorder"] = lstPOTemp.Count.ToString();
                //    }
                //    else {
                //        dataRow["qty_on_backorder"] = "0";
                //    }
                //}
                //else {
                //    dataRow["Qty_On_Hand"] = "0";
                //    dataRow["qty_on_backorder"] = "0";
                //}

                dataRow["Qty_On_Hand"] = lstPO[0].Stockouts.ToString();
                dataRow["IsProvisionResonChangesUpdate"] = false;
                dataRow["qty_on_backorder"] = lstPO[0].Backorders.ToString();
                dataRow["OutstandingLines"] = lstPO[0].OutstandingLines.ToString();

                dataRow["VendorID"] = lstPO[0].Vendor.VendorID.ToString();
                dataRow["PurchaseNumber"] = txtPurchaseNumber.Text;
                dataRow["Original_due_date"] = lstPO[0].Original_due_date.Value.ToString("dd/MM/yyyy");

                // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                if (lstPO[0].Expected_date != null)
                    dataRow["ExpectedDate"] = lstPO[0].Expected_date.Value.ToString("dd/MM/yyyy");
                else
                    dataRow["ExpectedDate"] = lstPO[0].Original_due_date.Value.ToString("dd/MM/yyyy");
                // Sprint 1 - Point 14 - End - Adding [Expected Date]

                dataRow["VendorName"] = lstPO[0].Vendor.VendorName;
                //dataRow["OutstandingLines"] = lstPO.Count.ToString(); // lstPOStock.Count.ToString(); // lstPO[0].OutstandingLines.ToString(); // 
                //dataRow["Qty_On_Hand"] = lstPO[0].SKU.Qty_On_Hand;
                //dataRow["qty_on_backorder"] = lstPO[0].SKU.qty_on_backorder;
                dataRow["SiteName"] = lstPO[0].Site.SiteName;
                dataRow["PurchaseOrderID"] = lstPO[0].PurchaseOrderID.ToString();

                OutstandingLines = lstPO.Count;

                myDataTable.Rows.Add(dataRow);
            }
            else
            {
                if (IsBookingValidationExcluded == true)
                {
                    dataRow["IsProvisionResonChangesUpdate"] = false;
                    dataRow["VendorID"] = "-1";
                    dataRow["PurchaseNumber"] = txtPurchaseNumber.Text;
                    dataRow["Original_due_date"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");

                    // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                    dataRow["ExpectedDate"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    // Sprint 1 - Point 14 - End - Adding [Expected Date]

                    dataRow["VendorName"] = "UNKNOWN";
                    dataRow["OutstandingLines"] = "1";
                    dataRow["Qty_On_Hand"] = "0";
                    dataRow["qty_on_backorder"] = "0";
                    dataRow["SiteName"] = ddlSite.innerControlddlSite.SelectedItem.Text;
                    dataRow["PurchaseOrderID"] = "0";
                    OutstandingLines = 1;
                    myDataTable.Rows.Add(dataRow);
                }

                #region Adding PO for OD user.

                /*
                 * [ISSUE No - 1] by client in [VIP Outstanding Issue Overview 22-04-13 Testing.xls] file.
                 * When Office Depot are making a booking, we should be allowed to add POs for incorrect vendors. The system should warn of the potential problem but
                 * Previously, we had an issue where the PO was being added whether the user hit CONTINUE or BACK. This was fixed but now we are unable to add to
                 *
                 * As per upper side problem, I have added this code as per Vikram suggestion on Date 24/04/2013 6:20 PM .
                 * This code will allow to add the non matched PO for vendor if logged-in user is OD user. 
                 * 
                 * Also this code will add PO in case on the [Continue] button clicked by only OD user.
                 */

                string Role = Session["Role"].ToString().Trim().ToLower();
                if (Role != "vendor" && Role != "carrier" && Convert.ToString(ViewState["ByContinue"]).Trim().ToLower() == "bycontinue")
                {
                    dataRow["IsProvisionResonChangesUpdate"] = false;
                    dataRow["VendorID"] = "-1";
                    dataRow["PurchaseNumber"] = txtPurchaseNumber.Text;
                    dataRow["Original_due_date"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                    dataRow["ExpectedDate"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                    // Sprint 1 - Point 14 - End - Adding [Expected Date]

                    dataRow["VendorName"] = "UNKNOWN";
                    dataRow["OutstandingLines"] = "1";
                    dataRow["Qty_On_Hand"] = "0";
                    dataRow["qty_on_backorder"] = "0";
                    dataRow["SiteName"] = ddlSite.innerControlddlSite.SelectedItem.Text;
                    dataRow["PurchaseOrderID"] = 0;
                    myDataTable.Rows.Add(dataRow);
                    ViewState["ByContinue"] = null;
                }

                #endregion
            }



            if (myDataTable != null && myDataTable.Rows.Count > 0)
            {
                gvPO.DataSource = myDataTable;
                gvPO.DataBind();
            }

            //Fill entered volume----
            DataTable dtVolumeDetails = null;
            if (ViewState["dtVolumeDetails"] != null)
            {
                dtVolumeDetails = (DataTable)ViewState["dtVolumeDetails"];

                foreach (GridViewRow Row in gvVolume.Rows)
                {
                    HiddenField hdngvVendorID = (HiddenField)Row.FindControl("hdngvVendorID");

                    DataRow[] UpdateRow = dtVolumeDetails.Select("VendorID='" + hdngvVendorID.Value.ToString() + "'");

                    if (UpdateRow != null && UpdateRow.Length > 0)
                    {

                        ucNumericTextbox txtVendorPallets = (ucNumericTextbox)Row.FindControl("txtVendorPallets");
                        ucNumericTextbox txtVendorCartons = (ucNumericTextbox)Row.FindControl("txtVendorCartons");

                        // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                        ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)Row.FindControl("txtLinesToBeDelivered");
                        // Sprint 1 - Point 18 - End - Carrier lines to be editable

                        UpdateRow[0]["Pallets"] = txtVendorPallets.Text;
                        UpdateRow[0]["Cartons"] = txtVendorCartons.Text;

                        // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                        UpdateRow[0]["LinesToBeDelivered"] = txtLinesToBeDelivered.Text;
                        // Sprint 1 - Point 18 - End - Carrier lines to be editable                   
                    }
                }
            }
            //------------------------

            int iTotalLines = 0;
            foreach (DataRow dr in myDataTable.Rows)
            {
                iTotalLines += Convert.ToInt32(dr["OutstandingLines"]);
            }

            if (myDataTable != null && myDataTable.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
                ViewState["myDataTable"] = myDataTable;


                //Build Volume Details Settings--------------//
                dtVolumeDetails = null;
                if (ViewState["dtVolumeDetails"] != null)
                {
                    dtVolumeDetails = (DataTable)ViewState["dtVolumeDetails"];

                    if (lstPO != null && lstPO.Count > 0)
                    {
                        DataRow[] UpdateRow = dtVolumeDetails.Select("VendorID='" + lstPO[0].Vendor.VendorID.ToString() + "'");

                        if (UpdateRow != null && UpdateRow.Length > 0)
                        {
                            UpdateRow[0]["ExpectedLines"] = (Convert.ToInt32(UpdateRow[0]["ExpectedLines"]) + OutstandingLines).ToString();
                            UpdateRow[0]["NoOfPO"] = (Convert.ToInt32(UpdateRow[0]["NoOfPO"]) + 1).ToString();
                        }
                        else
                        {
                            DataRow drdataRow = dtVolumeDetails.NewRow();
                            if (lstPO.Count > 0)
                            {
                                drdataRow["VendorID"] = lstPO[0].Vendor.VendorID.ToString();
                                drdataRow["VendorName"] = lstPO[0].Vendor.VendorName;
                                drdataRow["Pallets"] = "";
                                drdataRow["Cartons"] = "";
                                drdataRow["ExpectedLines"] = OutstandingLines.ToString(); // lstPO[0].OutstandingLines.ToString();
                                drdataRow["NoOfPO"] = "1";

                                // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                                drdataRow["LinesToBeDelivered"] = "";
                                // Sprint 1 - Point 18 - End - Carrier lines to be editable

                            }
                            dtVolumeDetails.Rows.Add(drdataRow);
                        }
                    }
                    else
                    {
                        DataRow[] UpdateRow = dtVolumeDetails.Select("VendorName='UNKNOWN'");
                        if (UpdateRow != null && UpdateRow.Length > 0)
                        {
                            UpdateRow[0]["ExpectedLines"] = (iTotalLines).ToString();
                            UpdateRow[0]["NoOfPO"] = (iTotalLines).ToString();
                        }
                    }
                }
                else
                {

                    dtVolumeDetails = new DataTable();

                    DataColumn auto = new DataColumn("AutoID", typeof(System.Int32));
                    // specify it as auto increment field
                    auto.AutoIncrement = true;
                    auto.AutoIncrementSeed = 1;
                    auto.ReadOnly = true;
                    auto.Unique = true;
                    dtVolumeDetails.Columns.Add(auto);

                    DataColumn myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "VendorID";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "VendorName";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "Pallets";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "Cartons";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "ExpectedLines";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "NoOfPO";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "LinesToBeDelivered";
                    dtVolumeDetails.Columns.Add(myDataColumn);
                    // Sprint 1 - Point 18 - End - Carrier lines to be editable

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "OldNumberOfPallet";
                    dtVolumeDetails.Columns.Add(myDataColumn);


                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "OldNumberOfCartons";
                    dtVolumeDetails.Columns.Add(myDataColumn);


                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "OldNumberOfLine";
                    dtVolumeDetails.Columns.Add(myDataColumn);

                    DataRow drdataRow = dtVolumeDetails.NewRow();
                    if (lstPO.Count > 0)
                    {
                        drdataRow["VendorID"] = lstPO[0].Vendor.VendorID.ToString();
                        drdataRow["VendorName"] = lstPO[0].Vendor.VendorName;
                        drdataRow["Pallets"] = "";
                        drdataRow["Cartons"] = "";
                        drdataRow["ExpectedLines"] = iTotalLines.ToString();
                        drdataRow["NoOfPO"] = "1";
                        drdataRow["LinesToBeDelivered"] = "";
                    }
                    else
                    {
                        if (IsBookingValidationExcluded == true)
                        {
                            drdataRow["VendorID"] = "-1";
                            drdataRow["VendorName"] = "UNKNOWN";
                            drdataRow["Pallets"] = "";
                            drdataRow["Cartons"] = "";
                            drdataRow["ExpectedLines"] = "1";
                            drdataRow["NoOfPO"] = "1";
                            drdataRow["LinesToBeDelivered"] = "";
                        }
                    }
                    dtVolumeDetails.Rows.Add(drdataRow);
                }
                if (dtVolumeDetails != null && dtVolumeDetails.Rows.Count > 0)
                {
                    ViewState["dtVolumeDetails"] = dtVolumeDetails;

                    gvVolume.DataSource = dtVolumeDetails;
                    gvVolume.DataBind();
                }

                //------------end volume details-------------//

            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
                ViewState["myDataTable"] = null;
            }




            //for (int iCount = 0; iCount < myDataTable.Rows.Count; iCount++) {
            //    TotalLines += Convert.ToInt32(lstPO[0].OutstandingLines.ToString()); //Convert.ToInt32(dataRow["OutstandingLines"]);
            //}

            TotalLines = iTotalLines.ToString();

            txtPurchaseNumber.Text = string.Empty;
        }

    }

    protected void gvPO_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        string DelMsg = WebCommon.getGlobalResourceValue("RemovePO");

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // loop all data rows
            if (hdnBookingTypeID.Value == "5" && hdnProvisionalReason.Value == "EDITED" && hdnLine.Value != "0" && Session["Role"].ToString().ToLower() != "carrier")
            {
                if (gvPO.DataKeys[e.Row.RowIndex].Values[1].ToString() == "True")
                {
                    e.Row.Cells[0].ForeColor = Color.Red;
                    e.Row.Cells[1].ForeColor = Color.Red;
                    e.Row.Cells[2].ForeColor = Color.Red;
                    e.Row.Cells[3].ForeColor = Color.Red;
                    e.Row.Cells[4].ForeColor = Color.Red;
                    e.Row.Cells[5].ForeColor = Color.Red;
                    e.Row.Cells[6].ForeColor = Color.Red;
                    e.Row.Cells[7].ForeColor = Color.Red;
                }
            }

            foreach (DataControlFieldCell cell in e.Row.Cells)
            {
                // check all cells in one row
                foreach (Control control in cell.Controls)
                {
                    LinkButton button = control as LinkButton;
                    if (button != null && button.CommandName == "Delete")
                        // Add delete confirmation
                        button.OnClientClick = "return (confirm('" + DelMsg + "')) ;";
                }
            }
        }
    }

    protected void gvPO_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        DataTable myDataTable = null;

        if (ViewState["myDataTable"] != null)
        {
            myDataTable = (DataTable)ViewState["myDataTable"];
        }

        if (myDataTable != null)
        {

            //Update Volume Details-----------//

            DataRow dr = null;//= myDataTable.Rows.Find(e.Keys[0]);

            foreach (DataRow dataRow in myDataTable.Rows)
            {
                if (dataRow["AutoID"].ToString() == e.Keys[0].ToString())
                {
                    dr = dataRow;
                    break;
                }
            }

            DataTable dtVolumeDetails = null;
            if (ViewState["dtVolumeDetails"] != null)
            {
                dtVolumeDetails = (DataTable)ViewState["dtVolumeDetails"];

                DataRow[] UpdateRow = dtVolumeDetails.Select("VendorID='" + dr["VendorID"].ToString() + "'");

                if (UpdateRow != null && UpdateRow.Length > 0)
                {

                    if (Convert.ToInt32(UpdateRow[0]["NoOfPO"]) > 1)
                    {
                        UpdateRow[0]["ExpectedLines"] = (Convert.ToInt32(UpdateRow[0]["ExpectedLines"]) - Convert.ToInt32(dr["OutstandingLines"])).ToString();
                        UpdateRow[0]["NoOfPO"] = (Convert.ToInt32(UpdateRow[0]["NoOfPO"]) - 1).ToString();
                    }
                    else
                    {
                        dtVolumeDetails.Rows.Remove(UpdateRow[0]);
                        dtVolumeDetails.AcceptChanges();
                    }
                }
                if (dtVolumeDetails != null && dtVolumeDetails.Rows.Count > 0)
                {
                    ViewState["dtVolumeDetails"] = dtVolumeDetails;
                    gvVolume.DataSource = dtVolumeDetails;
                    gvVolume.DataBind();
                }
                else
                {
                    gvVolume.DataSource = null;
                    gvVolume.DataBind();
                    ViewState["dtVolumeDetails"] = null;
                }
            }
            //----------end volume details update-----------------//

            myDataTable.Rows.RemoveAt(e.RowIndex); // (Convert.ToInt32(e.Keys[0]) - 1);
            myDataTable.AcceptChanges();
            gvPO.DataSource = myDataTable;
            gvPO.DataBind();
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            ViewState["myDataTable"] = myDataTable;
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
            ViewState["myDataTable"] = null;
        }


        int iTotalLines = 0;
        foreach (DataRow dataRow in myDataTable.Rows)
        {
            iTotalLines += Convert.ToInt32(dataRow["OutstandingLines"]);
        }

        TotalLines = iTotalLines.ToString();
    }

    protected void gvPO_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["myDataTable"] != null)
        {
            DataTable myDataTable = (DataTable)ViewState["myDataTable"];
            ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";
            DataView view = myDataTable.DefaultView;
            view.Sort = e.SortExpression + " " + Convert.ToString(ViewState["sortDirection"]);
            gvPO.DataSource = view;
            gvPO.DataBind();
        }
    }

    private void DeliverTypenontime()
    {
        DataTable dt1;

        MASSIT_NonTimeDeliveryBE oMASCNT_NonTimeDeliveryBE = new MASSIT_NonTimeDeliveryBE();
        APPSIT_NonTimeDeliveryBAL oMASCNT_NonTimeDeliveryBAL = new APPSIT_NonTimeDeliveryBAL();

        oMASCNT_NonTimeDeliveryBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASCNT_NonTimeDeliveryBE.Action = "GetDeliveries";
        dt1 = oMASCNT_NonTimeDeliveryBAL.GetNonTimeDeliveryDetailsBAL(oMASCNT_NonTimeDeliveryBE, "");

        strDeliveryNonTime = string.Empty;

        for (int iCount = 0; iCount < dt1.Rows.Count; iCount++)
        {
            strDeliveryNonTime += dt1.Rows[iCount]["SiteDeliveryID"].ToString() + ",";
        }
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        txtPurchaseNumber.Text = string.Empty;
        ViewState["myDataTable"] = null;
        ViewState["dtVolumeDetails"] = null;
        gvPO.DataSource = null;
        gvPO.DataBind();
        gvVolume.DataSource = null;
        gvVolume.DataBind();
        mvBookingEdit.ActiveViewIndex = 0;
    }

    protected void btnProceedBooking_Click(object sender, EventArgs e)
    {

        if (hdnCurrentMode.Value == "Edit" && hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue)
        {
            MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();

            APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();

            oMASSIT_VehicleDoorMatrixBE.Action = "VechileTypeMatched";
            oMASSIT_VehicleDoorMatrixBE.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedValue);
            oMASSIT_VehicleDoorMatrixBE.PreviousVehicleTypeID = Convert.ToInt32(hdnPreviousVehicleType.Value);
            isDoorTypeNotChanged = oMASSIT_VehicleDoorMatrixBAL.isVehcileTypeMatchedBAL(oMASSIT_VehicleDoorMatrixBE);
        }

        if (txtSchedulingdate.innerControltxtDate.Value != GetQueryStringValue("Scheduledate").ToString() && hdnCurrentMode.Value == "Edit")
        {
            btnNotHappy.Visible = true;
        }
        else
        {
            btnNotHappy.Visible = false;
        }

        bool isWarning = false;
        //Check Added PO//
        DataTable myDataTable = null;

        if (ViewState["myDataTable"] != null)
        {
            myDataTable = (DataTable)ViewState["myDataTable"];
        }
        else
        {
            myDataTable = new DataTable();
        }

        string errMsg = WebCommon.getGlobalResourceValue("BK_MS_17"); // "The Purchase Order/Delivery Reference field must be populated. Please enter either a Purchase Order number or a deliver reference";
        if (myDataTable.Rows.Count <= 0)
        {
            ShowErrorMessage(errMsg, "BK_MS_17");
            return;
        }
        //-------------//       

        if (isWarning == false)
            ValidateEnteredVolume();
        else
            ShowWarningMessages();

        //-------------------------------//      
    }

    //-----------Start Stage 9 point 2a/2b--------------//
    protected void btnReject_Click(object sender, EventArgs e)
    {
        //string errMsg = WebCommon.getGlobalResourceValue("DeleteBookingConfirmOD");
        //AddWarningMessages(errMsg, "Booking");
        //ShowWarningMessages();
        mdlRejectProvisional.Show();
    }

    protected void btnContinue_2_Click(object sender, EventArgs e)
    {
        DeleteBooking();
    }

    protected void btnBack_2_Click(object sender, EventArgs e)
    {

        mdlRejectProvisional.Hide();
    }

    private void DeleteBooking()
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Action = "DeleteBooking";
        oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
        oAPPBOK_BookingBAL.DeleteBookingDetailBAL(oAPPBOK_BookingBE);

        #region Logic to send emails ...
        var BookingId = Convert.ToInt32(GetQueryStringValue("ID").ToString());
        oAPPBOK_BookingBE = new APPBOK_BookingBE();
        oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.Action = "GetDeletedBookingDeatilsByID";
        oAPPBOK_BookingBE.BookingID = BookingId;
        var lstBooking = oAPPBOK_BookingBAL.GetDeletedBookingDetailsByIDBAL(oAPPBOK_BookingBE);
        if (lstBooking.Count > 0)
            this.SendProvisionalRefusalLetterMail(lstBooking, BookingId);

        #endregion

        if (GetQueryStringValue("PNTB") != null && GetQueryStringValue("PNTB") == "True")
        {
            EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
        }
        else if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro")
        {
            EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
        }
        else
        {
            EncryptQueryString("APPBok_BookingOverview.aspx");
        }

        //if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro") {
        //    EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
        //}
        //else {
        //    EncryptQueryString("APPBok_BookingOverview.aspx");
        //}
    }
    //-----------End Stage 9 point 2a/2b--------------//
    private void ValidateEnteredVolume()
    {
        string errMsg = WebCommon.getGlobalResourceValue("BK_MS_19"); // "There is insufficient volume information entered for this booking. The number of lines must be >0 and either pallets or cartons must be >0.";

        int iTotalPallets = 0;
        int iTotalCartons = 0;
        int iTotalLines = 0;

        foreach (GridViewRow gvRow in gvVolume.Rows)
        {
            if (gvRow.RowType == DataControlRowType.DataRow)
            {
                ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvRow.FindControl("txtVendorPallets");
                ucNumericTextbox txtVendorCartons = (ucNumericTextbox)gvRow.FindControl("txtVendorCartons");

                // Added new code to get the exact Expected line for particular row.
                ucLiteral ltVendorExpectedLines = (ucLiteral)gvRow.FindControl("ltVendorExpectedLines");
                if (ltVendorExpectedLines != null)
                {
                    TotalLines = Convert.ToString(ltVendorExpectedLines.Text);
                }

                // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvRow.FindControl("txtLinesToBeDelivered");
                // Sprint 1 - Point 18 - End - Carrier lines to be editable

                string sTotalPallets = txtVendorPallets.Text.Trim() != string.Empty ? txtVendorPallets.Text.Trim() : "0";
                string sTotalCartons = txtVendorCartons.Text.Trim() != string.Empty ? txtVendorCartons.Text.Trim() : "0";

                // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                string sLinesToBeDelivered = txtLinesToBeDelivered.Text.Trim() != string.Empty ? txtLinesToBeDelivered.Text.Trim() : "0";
                // Sprint 1 - Point 18 - End - Carrier lines to be editable

                if (sTotalPallets == "0" && sTotalCartons == "0" && TotalLines == "0")
                {
                    ShowErrorMessage(errMsg, "BK_MS_19");
                    return;
                }
                else if ((Convert.ToInt32(TotalLines) == 0) || (Convert.ToInt32(sTotalPallets) == 0 && Convert.ToInt32(sTotalCartons) == 0))
                {
                    ShowErrorMessage(errMsg, "BK_MS_19");
                    return;
                }
                // Sprint 1 - Point 18 - Start - Carrier lines to be editable
                else if (sLinesToBeDelivered == "0" || sLinesToBeDelivered == string.Empty)
                {
                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_32_Message");
                    ShowErrorMessage(errMsg, "BK_MS_32_Message");
                    return;
                }

                //if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
                //    if (Convert.ToInt32(sLinesToBeDelivered) > Convert.ToInt32(TotalLines)) {
                //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_31_Warning");
                //        ShowErrorMessage(errMsg, "BK_MS_31_Warning");
                //        return;
                //    }
                //    else if (Convert.ToInt32(sLinesToBeDelivered) < Convert.ToInt32(TotalLines)) {
                //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_34_Warning");
                //        ShowErrorMessage(errMsg, "BK_MS_34_Warning");
                //        return;
                //    }
                //}
                //else {
                //    if (Convert.ToInt32(sLinesToBeDelivered) > Convert.ToInt32(TotalLines)) {
                //        ViewState["WarningStatus"] = 0;
                //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_31_Warning");
                //        ltWarningConfirm.Text = errMsg;
                //        mdlWarningConfirm.Show();
                //        return;
                //    }
                //    else if (Convert.ToInt32(sLinesToBeDelivered) < Convert.ToInt32(TotalLines)) {
                //        ViewState["WarningStatus"] = 0;
                //        errMsg = WebCommon.getGlobalResourceValue("BK_MS_34_Warning");
                //        ltWarningConfirm.Text = errMsg;
                //        mdlWarningConfirm.Show();
                //        return;
                //    }
                //}


                iTotalPallets += Convert.ToInt32(sTotalPallets);
                iTotalCartons += Convert.ToInt32(sTotalCartons);
                iTotalLines += Convert.ToInt32(sLinesToBeDelivered);

                ViewState["RTotalCartons"] = iTotalCartons.ToString();
                ViewState["RTotalPallets"] = iTotalPallets.ToString();
                ViewState["RTotalLines"] = iTotalLines.ToString();

                if (Convert.ToInt32(sLinesToBeDelivered) > Convert.ToInt32(TotalLines))
                {
                    if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
                    {  // If coming from Provisional page , ignore less lines check -- Phase 12 R2 Point 2
                        ViewState["WarningStatus"] = 0;
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_31_Warning");
                        ltWarningConfirm.Text = errMsg;
                        mdlWarningConfirm.Show();
                        return;
                    }
                }
                else if (Convert.ToInt32(sLinesToBeDelivered) < Convert.ToInt32(TotalLines))
                {
                    if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
                    {  // If coming from Provisional page , ignore less lines check -- Phase 12 R2 Point 2
                        ViewState["WarningStatus"] = 0;
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_34_Warning");
                        ltWarningConfirm.Text = errMsg;
                        mdlWarningConfirm.Show();
                        return;
                    }
                }
                // Sprint 1 - Point 18 - End - Carrier lines to be editable   
            }
        }



        iTotalCartonsglobal = iTotalCartons;
        iTotalLinesglobal = iTotalLines;
        iTotalPalletsglobal = iTotalPallets;

        //this.ProceedBooking(iTotalCartons, iTotalPallets, iTotalLines);
        hdnCurrentWeekDay.Value = ActualSchedulDateTime.ToString("ddd").ToLower();

        //---Stage 10 Point 10 L 5.2----//
        if (ddlSite.IsBookingExclusions)
        {
            bool isVendorVolumeNotValid = false;
            foreach (GridViewRow gvRow in gvVolume.Rows)
            {
                if (gvRow.RowType == DataControlRowType.DataRow)
                {
                    ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvRow.FindControl("txtVendorPallets");
                    ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvRow.FindControl("txtLinesToBeDelivered");

                    string sTotalPallets = txtVendorPallets.Text.Trim() != string.Empty ? txtVendorPallets.Text.Trim() : "0";
                    string sLinesToBeDelivered = txtLinesToBeDelivered.Text.Trim() != string.Empty ? txtLinesToBeDelivered.Text.Trim() : "0";

                    HiddenField hdnPOVendorId = (HiddenField)gvRow.FindControl("hdnPOVendorId");

                    if (isEnteredVendorVolumeNotValid(Convert.ToInt32(hdnPOVendorId.Value), Convert.ToInt32(sTotalPallets), Convert.ToInt32(sLinesToBeDelivered)))
                        isVendorVolumeNotValid = true;
                }
            }
            if (isVendorVolumeNotValid)
                ShowWarningMessages();
            else
                this.ProceedBooking(iTotalCartons, iTotalPallets, iTotalLines);
        }
        else
            this.ProceedBooking(iTotalCartons, iTotalPallets, iTotalLines);
        //----------------------------------------//

        ViewState["RTotalCartons"] = iTotalCartons.ToString();
        ViewState["RTotalPallets"] = iTotalPallets.ToString();
        ViewState["RTotalLines"] = iTotalLines.ToString();

    }

    //----Stage 10 Point 10L 5.2----//
    private bool isEnteredVendorVolumeNotValid(int iVendorID, int VenPallets, int VenLines)
    {
        //----Check Vendor Constraints----//
        UP_VendorBE[] newArray = null;
        List<UP_VendorBE> newList = null;
        string errMsg = string.Empty;
        bool isWarning = false;

        if (ViewState["UPVendors"] != null)
        {
            newArray = (UP_VendorBE[])ViewState["UPVendors"];
            newList = new List<UP_VendorBE>(newArray);
        }

        if (newList != null && newList.Count > 0)
        {
            newList = newList.FindAll(delegate(UP_VendorBE ven)
            {
                return ven.VendorDetails.IsConstraintsDefined == true && ven.VendorID == iVendorID;
            });
        }

        if (newList != null && newList.Count > 0)
        {

            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
            {
                //errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_EXT"); // "The volume being booked in exceeds the scheduled capacity for you.";
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT") + "[ " + newList[0].VendorName + " ]."; // "The volume being booked in exceeds the scheduled capacity for vendor [ " + ltvendorName.Text + " ]."; 
            }
            else
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT") + "[ " + newList[0].VendorName + " ]."; // "The volume being booked in exceeds the scheduled capacity for vendor [ " + ltvendorName.Text + " ]."; 
            }

            errMsg = errMsg + "<br>";
            if (newList[0].VendorDetails.APP_MaximumPallets > 0)
            {
                errMsg = errMsg + WebCommon.getGlobalResourceValue("BK_MS_18_INT_MAXPallets").Replace("##MaxPallets##", newList[0].VendorDetails.APP_MaximumPallets.ToString());
                errMsg = errMsg + "&nbsp;&nbsp;&nbsp;&nbsp;";
            }
            if (newList[0].VendorDetails.APP_MaximumLines > 0)
                errMsg = errMsg + WebCommon.getGlobalResourceValue("BK_MS_18_INT_MAXLines").Replace("##MaxLines##", newList[0].VendorDetails.APP_MaximumLines.ToString());

            if (VenPallets != 0)
            {
                if (Convert.ToInt32(VenPallets) > newList[0].VendorDetails.APP_MaximumPallets)
                {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {

                        ShowErrorMessage(errMsg, "BK_MS_18_EXT_VEN");
                        return true;

                    }
                    else
                    {
                        AddWarningMessages(errMsg, "BK_MS_18_INT_VEN_" + iVendorID.ToString());
                        isWarning = true;
                    }
                }
            }

            if (VenLines != 0)
            {
                if (Convert.ToInt32(VenLines) > newList[0].VendorDetails.APP_MaximumLines)
                {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {
                        ShowErrorMessage(errMsg, "BK_MS_18_EXT_VEN");
                        return true;
                    }
                    else
                    {
                        AddWarningMessages(errMsg, "BK_MS_18_INT_VEN_" + iVendorID.ToString());
                        isWarning = true;
                    }
                }
            }
        }

        return isWarning;
    }
    //----------------------//

    //----Stage 4 Point 13----//
    private void ProceedBooking(int iTotalCartons, int iTotalPallets, int iTotalLines)
    {
        bool isWarning = false;
        string errMsg = String.Empty;

        intTotalCartons = iTotalCartons;
        intTotalPallets = iTotalPallets;
        intTotalLines = iTotalLines;

        //Check Site Setting Max Volume per delivery per site
        MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        if (
           (singleSiteSetting.MaxPalletsPerDelivery > 0
           && Convert.ToInt32(iTotalPallets) > singleSiteSetting.MaxPalletsPerDelivery) ||
           (singleSiteSetting.MaxCartonsPerDelivery > 0 &&
           Convert.ToInt32(iTotalCartons) > singleSiteSetting.MaxCartonsPerDelivery) ||
           (singleSiteSetting.MaxLinesPerDelivery > 0 &&
           Convert.ToInt32(iTotalLines) > singleSiteSetting.MaxLinesPerDelivery))
        {

            errMsg = WebCommon.getGlobalResourceValue("BK_MS_35"); //You have broken the maximum allowed volume for this site. Please check your entry for pallets, cartons & lines.
            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
            {
                ShowErrorMessage(errMsg, "BK_MS_35");
                return;
            }
            else
            {
                AddWarningMessages(errMsg, "BK_MS_35");
                isWarning = true;
            }

            if (isWarning)
                ShowWarningMessages();
        }
        else
        {
            SuggestBooking(iTotalCartons, iTotalPallets, iTotalLines);
        }
        //----------------------------------//
    }

    // Sprint 1 - Point 18 - Start - Carrier lines to be editable
    private void SuggestBooking(int iTotalCartons, int iTotalPallets, int iTotalLines)
    {

        TotalCartons = iTotalCartons.ToString();
        TotalPallets = iTotalPallets.ToString();
        TotalLines = iTotalLines.ToString();

        bool isWarning = false;
        string errMsg = String.Empty;

        //----Sprint 3a start Check Vendor Constraints----//
        MASCNT_CarrierBE[] newArray = null;
        List<MASCNT_CarrierBE> newList = null;

        //ViewState.Add("lstCarrier", lstCarrierBE.ToArray());
        if (ViewState["lstCarrier"] != null)
        {
            newArray = (MASCNT_CarrierBE[])ViewState["lstCarrier"];
            newList = new List<MASCNT_CarrierBE>(newArray);
        }

        if (hdnCurrentRole.Value == "Carrier")
        {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_EXT"); // "The volume being booked in exceeds the scheduled capacity for you.";
        }
        else
        {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT_Carrier") + "[ " + ddlCarrier.SelectedItem.Text + " ]."; // "The volume being booked in exceeds the scheduled capacity for vendor [ " + ltvendorName.Text + " ]."; 
        }

        if (newList != null && newList.Count > 0)
        {

            if (TotalPallets != "0" && newList[0].APP_MaximumPallets != 0)
            {
                if (Convert.ToInt32(TotalPallets) > newList[0].APP_MaximumPallets)
                {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Carrier")
                    {
                        ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                        return;
                    }
                    else
                    {
                        if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
                        {  // If coming from Provisional page , ignore fixed slot capacity -- Phase 12 R2 Point 2
                            AddWarningMessages(errMsg, "BK_MS_18_INT");
                            isWarning = true;
                        }
                    }
                }
            }

            if (TotalLines != "0" && newList[0].APP_MaximumLines != 0)
            {
                if (Convert.ToInt32(TotalLines) > newList[0].APP_MaximumLines)
                {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Carrier")
                    {
                        ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                        return;
                    }
                    else
                    {
                        if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
                        {  // If coming from Provisional page , ignore fixed slot capacity -- Phase 12 R2 Point 2
                            AddWarningMessages(errMsg, "BK_MS_18_INT");
                            isWarning = true;
                        }
                    }
                }
            }
        }
        //----Sprint 3a End--------------------//



        //Check Maximum Capacity for this day  - if not the confirm fixed slot case
        //Revert back as per anthony mail on 04 Sep 2013 to skip - Daily capacities do not matter for Fixed Slots

        /* COMMENTED BACK AS PER REQUIREMENT OF PHASE-11-C POINT-1(17)*/

        if (hdnSuggestedFixedSlotID.Value == "-1" || hdnSuggestedFixedSlotID.Value == string.Empty)
        {
            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT_1"); //("BK_MS_18_EXT_1"); // "There is no capacity for any further deliveries on the date selected. Please select a different date or, if the delivery is urgent, please contact the booking office.";
            }
            else
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_18_INT_1"); //"There is no capacity for any further deliveries on the date selected. Click CONTINUE to make booking on this date.";
            }

            if (TotalPallets != "0" && ltRemainingPallets.Text != string.Empty && ltRemainingPallets.Text != "-")
            {
                if (Convert.ToInt32(TotalPallets) > (Convert.ToInt32(ltRemainingPallets.Text) + BookedPallets))
                {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {
                        //if (hdnCurrentMode.Value == "Add") {
                        //    ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                        //    return;
                        //}
                        AddWarningMessages(errMsg, "BK_MS_18_INT");
                        isWarning = true;
                    }
                    else
                    {
                        AddWarningMessages(errMsg, "BK_MS_18_INT");
                        isWarning = true;
                    }
                }
            }

            if (TotalLines != "0" && ltRemainingLines.Text != string.Empty && ltRemainingLines.Text != "-")
            {
                if (Convert.ToInt32(TotalLines) > (Convert.ToInt32(ltRemainingLines.Text) + BookedLines))
                {
                    //Show err msg
                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {
                        //if (hdnCurrentMode.Value == "Add") {
                        //    ShowErrorMessage(errMsg, "BK_MS_18_EXT");
                        //    return;
                        //}
                        AddWarningMessages(errMsg, "BK_MS_18_INT");
                        isWarning = true;
                    }
                    else
                    {
                        AddWarningMessages(errMsg, "BK_MS_18_INT");
                        isWarning = true;
                    }
                }
            }
        }
        //-------------------------------//

        //if (Convert.ToInt32(TotalLines) < Convert.ToInt32(Lines)) {
        //    errMsg = "The number of lines is less than expected. Please contact your Stock Planner.";
        //    AddWarningMessages(errMsg, "BK_MS_26_EXT");
        //    isWarning = true;
        //}



        //----Check Vendor Constraints----//
        //UP_VendorBE[] newArray = null;
        //List<UP_VendorBE> newList = null;

        //if (ViewState["UPVendor"] != null) {
        //    newArray = (UP_VendorBE[])ViewState["UPVendor"];
        //    newList = new List<UP_VendorBE>(newArray);
        //}

        //if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier") {
        //    errMsg = "The volume been booked in exceeds the scheduled capacity for you.";
        //}
        //else { errMsg = "The volume been booked in exceeds the scheduled capacity for vendor [ " + ddlCarrier.SelectedItem.Text + " ]."; }

        //if (newList != null && newList.Count > 0) {
        //    if (iTotalPallets != 0) {
        //        if (Convert.ToInt32(iTotalPallets) > newList[0].VendorDetails.APP_MaximumPallets) {
        //            //Show err msg
        //            ShowErrorMessage(errMsg, "BK_MS_18");
        //            return;
        //        }
        //    }

        //    if (TotalLines != "0") {
        //        if (Convert.ToInt32(TotalLines) > newList[0].VendorDetails.APP_MaximumLines) {
        //            //Show err msg
        //            ShowErrorMessage(errMsg, "BK_MS_18");
        //            return;
        //        }
        //    }
        //}
        //-------------------------------//



        if (hdnCurrentRole.Value == "Carrier")
        {
            btnAlternateSlot.Visible = false;

            // Here checking Alternate Slot button allow for vendor or not.
            MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
            if (singleSiteSetting != null)
            {
                if (singleSiteSetting.AlternateSlotOnOff.Trim().ToUpper() == "Y")
                {
                    btnAlternateSlotCarrier.Visible = true;
                    lblUnsatisfactorySuggestedTime.Visible = false;
                }
                else
                {
                    btnAlternateSlotCarrier.Visible = false;
                    lblUnsatisfactorySuggestedTime.Visible = true;
                }
            }
            else
            {
                btnAlternateSlotCarrier.Visible = true;
                lblUnsatisfactorySuggestedTime.Visible = false;
            }
        }
        else
        {
            btnAlternateSlot.Visible = true;
            btnAlternateSlotCarrier.Visible = false;
        }

        if (isWarning == false)
        {
            CalculateProposedBookingTimeSlot();
            if (isDoorCheckPopup == false)
            {
                mvBookingEdit.ActiveViewIndex = 2;
            }
        }
        else
            ShowWarningMessages();

        //Hide 'Non time Booking' and 'Alternate Slot' button in case of Confirm a fixed slot
        if (hdnFixedSlotMode.Value == "true" && hdnCurrentRole.Value == "Carrier")
        {
            btnMakeNonTimeBooking.Visible = false;
        }
    }
    // Sprint 1 - Point 18 - End - Carrier lines to be editable


    private void CalculateProposedBookingTimeSlot()
    {  //Find out unloading time for slot suggestion


        //----Start Non Time Delivary ------------//

        if (hdnCurrentRole.Value == "Carrier")
        { // When vendor is loggedin

            DeliverTypenontime();
            IsNonTime = false;

            if (!Convert.ToBoolean(IsNonTimeCarrier))
            {  //Check Non Time delivary for vendor

                if (!strDeliveryNonTime.Contains(ddlDeliveryType.SelectedItem.Value + ","))
                {  //Check Non Time delivary for Delivery Type

                    //if (TotalPallets.Trim() != string.Empty) {  //Check non time pallets 
                    //    if (NonTimeDeliveryPalletVolume > Convert.ToInt32(TotalPallets.Trim())) {
                    //        IsNonTime = true;
                    //    }
                    //}

                    //if (IsNonTime == false && TotalCartons.Trim() != string.Empty) {  //Check non time cartons 
                    //    if (NonTimeDeliveryCartonVolume > Convert.ToInt32(TotalCartons.Trim())) {
                    //        IsNonTime = true;
                    //    }
                    //}
                    bool IsPalletVolumeMatch = false;
                    bool IsCartonVolumeMatch = false;

                    if (TotalPallets.Trim() != string.Empty)
                    {  //Check non time pallets 
                        if (NonTimeDeliveryPalletVolume >= Convert.ToInt32(TotalPallets.Trim()))
                        {
                            //IsNonTime = true;
                            IsPalletVolumeMatch = true;
                        }
                    }
                    else
                    {
                        IsPalletVolumeMatch = true;
                    }

                    if (TotalCartons.Trim() != string.Empty)
                    {  //Check non time cartons 
                        if (NonTimeDeliveryCartonVolume >= Convert.ToInt32(TotalCartons.Trim()))
                        {
                            //IsNonTime = true;
                            IsCartonVolumeMatch = true;
                        }
                    }
                    else
                    {
                        IsCartonVolumeMatch = true;
                    }

                    if (IsPalletVolumeMatch && IsCartonVolumeMatch)
                    {
                        IsNonTime = true;
                    }
                }
                else
                {
                    IsNonTime = true;
                }
            }
            else
            {
                IsNonTime = true;
            }
           // GenerateAlternateSlot();
        }
        else
        {  // When OD staff is loggedin
            IsNonTime = true;
        }
        //--------End of Non Time Delivary-------------------------//     

        BindFixedSlot();

        btnMakeNonTimeBooking.Enabled = true;
        btnMakeNonTimeBooking.Visible = Convert.ToBoolean(IsNonTime);
    }

    private int GetTimeSlotLength()
    {
        return GetTimeSlotLength(-1);
    }

    private int GetTimeSlotLength(int VehicleTypeID)
    {

        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;

        //Step1 >Check Carrier Processing Window 
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {

            MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
            APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

            oMASSIT_CarrerProcessingWindowBE.Action = "GetConstraints";
            oMASSIT_CarrerProcessingWindowBE.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
            oMASSIT_CarrerProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            MASSIT_CarrierProcessingWindowBE lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetConstraintsBAL(oMASSIT_CarrerProcessingWindowBE);


            if (lstCarrier != null)
            {  //Carrier Processing Window Not found

                if (iPallets != 0 && isTimeCalculatedForPallets == false && lstCarrier.APP_PalletsUnloadedPerHour > 0)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstCarrier.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (iCartons != 0 && isTimeCalculatedForCartons == false && lstCarrier.APP_CartonsUnloadedPerHour > 0)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstCarrier.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }

            }
        }
        //---------------------------------// 

        //Step2 > Check Vehicle Type Setup
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
            APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

            oMASSIT_VehicleTypeBE.Action = "ShowById";
            oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            if (VehicleTypeID <= 0)
                oMASSIT_VehicleTypeBE.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
            else
                oMASSIT_VehicleTypeBE.VehicleTypeID = VehicleTypeID;

            MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType != null)
            {
                //Volume Capacity 
                if (iPallets != 0 && isTimeCalculatedForPallets == false && lstVehicleType.APP_PalletsUnloadedPerHour > 0)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (iCartons != 0 && isTimeCalculatedForCartons == false && lstVehicleType.APP_CartonsUnloadedPerHour > 0)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }
                if (isTimeCalculatedForPallets == false && isTimeCalculatedForCartons == false)
                { //Time Based 
                    if (lstVehicleType.TotalUnloadTimeInMinute > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
                        isTimeCalculatedForPallets = true;
                        isTimeCalculatedForCartons = true;
                    }
                }

                //---------------Stage 14 Point 15-------------//
                if (lstVehicleType.TotalUnloadTimeInMinute <= 0 && TotalCalculatedTime < lstVehicleType.MinimumTimePerDeliveryInMinute)
                {
                    TotalCalculatedTime = lstVehicleType.MinimumTimePerDeliveryInMinute;
                }
                //-------------------------------------------//
            }
        }
        //---------------------------------// 


        //Step3 >Check Site Miscellaneous Settings  - skip this step
        //if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false) {

        //    MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        //    MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        //    oMAS_SiteBE.Action = "ShowAll";
        //    oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        //    oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        //    oMAS_SiteBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        //    List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
        //    lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

        //    if (lstSiteMisSettings != null) {
        //        if (iPallets != 0 && isTimeCalculatedForPallets == false) {
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour));      // Convert.ToDecimal((Convert.ToInt32(iPallets) / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour) * 60);
        //            isTimeCalculatedForPallets = true;
        //        }
        //        if (iCartons != 0 && isTimeCalculatedForCartons == false) {
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour));      //Convert.ToDecimal((Convert.ToInt32(iCartons) / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour) * 60);
        //            isTimeCalculatedForCartons = true;
        //        }
        //    }
        //}
        //---------------------------------//   

        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime % iSlotTimeLength;
        int intr = (int)TotalCalculatedTime / iSlotTimeLength;
        if (intResult > 0)
            intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;

        return intr;
    }

    private string GetDoorConstraints()
    {
        string OpenDoorIds = string.Empty;

        if (txtSchedulingdate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
        {

            bool IsSpecificWeekDefined = false;

            DateTime dtSelectedDate = LogicalSchedulDateTime;
            string WeekDay = dtSelectedDate.ToString("dddd");
            DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

            MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
            MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

            oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
            oMASSIT_WeekSetupBE.ScheduleDate = dtSelectedDate;

            List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup != null && lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
            {
                IsSpecificWeekDefined = true;
            }


            //------Check for Specific Entry First--------//
            APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
            MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

            APPSIT_DoorOpenTimeSpecificBAL oAPPSIT_DoorOpenTimeSpecificBAL = new APPSIT_DoorOpenTimeSpecificBAL();
            MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE = new MASSIT_DoorOpenTimeSpecificBE();
            oMASSIT_DoorOpenTimeSpecificBE.Action = "GetDoorConstraintsSpecific";
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime = new MASSIT_DoorOpenTimeBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday = WeekDay;
            oMASSIT_DoorOpenTimeSpecificBE.WeekStartDate = dtMon;
            List<MASSIT_DoorOpenTimeSpecificBE> lstDoorConstraintsSpecific = oAPPSIT_DoorOpenTimeSpecificBAL.GetDoorConstraintsSpecificBAL(oMASSIT_DoorOpenTimeSpecificBE);
            if (IsSpecificWeekDefined == true)
            {
                if (lstDoorConstraintsSpecific.Count > 0)
                { //Get Specific settings 
                    for (int iCount = 0; iCount < lstDoorConstraintsSpecific.Count; iCount++)
                    {
                        OpenDoorIds += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + ",";
                    }
                }
                else
                { //Get Generic settings 
                    oMASSIT_DoorOpenTimeBE.Action = "GetDoorConstraints";
                    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
                    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                    oMASSIT_DoorOpenTimeBE.Weekday = WeekDay;
                    List<MASSIT_DoorOpenTimeBE> lstDoorConstraints = oMASSIT_DoorOpenTimeBAL.GetDoorConstraintsBAL(oMASSIT_DoorOpenTimeBE);
                    for (int iCount = 0; iCount < lstDoorConstraints.Count; iCount++)
                    {
                        OpenDoorIds += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";

                    }
                }
            }
            //if (lstDoorConstraintsSpecific.Count > 0 && IsSpecificWeekDefined == true) { //Get Specific settings 
            //    for (int iCount = 0; iCount < lstDoorConstraintsSpecific.Count; iCount++) {
            //        OpenDoorIds += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + ",";
            //    }
            //}
            else
            {
                if (IsSpecificWeekDefined == false)
                {
                    //Get Generic settings 
                    oMASSIT_DoorOpenTimeBE.Action = "GetDoorConstraints";
                    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
                    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                    oMASSIT_DoorOpenTimeBE.Weekday = WeekDay;
                    List<MASSIT_DoorOpenTimeBE> lstDoorConstraints = oMASSIT_DoorOpenTimeBAL.GetDoorConstraintsBAL(oMASSIT_DoorOpenTimeBE);
                    for (int iCount = 0; iCount < lstDoorConstraints.Count; iCount++)
                    {
                        OpenDoorIds += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";

                    }
                }
            }
        }

        strOpenDoorIds = OpenDoorIds;
        return OpenDoorIds;
    }

    protected void BindFixedSlot()
    {

        //-----------------Start Collect Basic Information --------------------------//

        DateTime dtSelectedDate = LogicalSchedulDateTime;
        string WeekDay = dtSelectedDate.ToString("dddd");

        //DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
        oMASSIT_WeekSetupBE.ScheduleDate = dtSelectedDate;

        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
        {

            oMASSIT_WeekSetupBE.Action = "ShowAll";
            lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
        }
        //----------------------------------------------------------------------------//

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.Carrier = new MASCNT_CarrierBE();

        oAPPBOK_BookingBE.Action = "CarrierBookingFixedSlotDetails";
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime;

        List<APPBOK_BookingBE> lstBookings = oAPPBOK_BookingBAL.GetVendorBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

        //Filter out fixed slot not in timeslot range
        if (lstWeekSetup.Count > 0)
        {
            lstBookings = lstBookings.FindAll(delegate(APPBOK_BookingBE St)
            {
                DateTime? dt = (DateTime?)null;
                if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
                {
                    dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                }
                else
                {
                    dt = new DateTime(1900, 1, 1, 0, 0, 0);
                }
                return dt <= lstWeekSetup[0].EndTime;
            });
        }


        if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
        {

            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
            {
                oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime.AddDays(-1);
                List<APPBOK_BookingBE> lstBookingsStartDay = oAPPBOK_BookingBAL.GetVendorBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

                lstBookingsStartDay = lstBookingsStartDay.FindAll(delegate(APPBOK_BookingBE St)
                {
                    DateTime? dt = (DateTime?)null;
                    if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
                    {
                        dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    }
                    else
                    {
                        dt = new DateTime(1900, 1, 1, 0, 0, 0);
                    }

                    return dt >= lstWeekSetup[0].StartTime;
                });

                if (lstBookingsStartDay != null && lstBookingsStartDay.Count > 0)
                {
                    lstBookings = MergeListCollections(lstBookingsStartDay, lstBookings);
                }
            }
        }

        //if (txtSchedulingdate.innerControltxtDate.Value == GetQueryStringValue("Scheduledate").ToString() && hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue && isDoorTypeNotChanged == false && hdnCurrentMode.Value == "Edit")
        //{
        //  hdnSuggestedFixedSlotID.Value = "";
        //  hdnBookingRef.Value = "";
        //}
        //else if (txtSchedulingdate.innerControltxtDate.Value != GetQueryStringValue("Scheduledate").ToString() && hdnCurrentMode.Value == "Edit") 
        //{
        //    hdnSuggestedFixedSlotID.Value = "";
        //    hdnBookingRef.Value = "";
        //}

        if (txtSchedulingdate.innerControltxtDate.Value == GetQueryStringValue("Scheduledate").ToString()
           && hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue && isDoorTypeNotChanged == false && hdnCurrentMode.Value == "Edit")
        {
            if (isDoorTypeNotChanged == false)
            {
                hdnSuggestedFixedSlotID.Value = "";
                hdnBookingRef.Value = "";
            }
        }
        else if (txtSchedulingdate.innerControltxtDate.Value != GetQueryStringValue("Scheduledate").ToString()
            && hdnCurrentMode.Value == "Edit")
        {
            //check next time
            if (!(lstBookings.FindAll(x => x.BookingDate == GetQueryStringValue("Scheduledate").ToString() && x.BookingRef == hdnBookingRef.Value).Count > 0))
            {
                //if (!(lstBookings.FindAll(x => x.BookingDate == GetQueryStringValue("Scheduledate").ToString()).Count > 0))
                //{
                hdnSuggestedFixedSlotID.Value = "";
                hdnBookingRef.Value = "";
            }
        }


        //-----------------Get top fisrt unused fixed slot to be remonded-------------//
        bool bUnconfirmedFixedSlotFound = false;
        List<APPBOK_BookingBE> lstUnuseedFixedSlot = null;
        if (hdnCurrentMode.Value == "Add")
            lstUnuseedFixedSlot = lstBookings.FindAll(delegate(APPBOK_BookingBE St) { return St.BookingRef == "Fixed booking"; });
        else if (hdnCurrentMode.Value == "Edit" && hdnSuggestedFixedSlotID.Value != "-1" && hdnSuggestedFixedSlotID.Value != "")
            lstUnuseedFixedSlot = lstBookings.FindAll(delegate(APPBOK_BookingBE St) { return St.FixedSlot.FixedSlotID == Convert.ToInt32(hdnSuggestedFixedSlotID.Value); });

        if (lstUnuseedFixedSlot != null && lstUnuseedFixedSlot.Count > 0)
        {
            bUnconfirmedFixedSlotFound = true;
        }
        //----------------------------------------------------------------------------//

        //-------IF NO UNCONFIRMED FIXED SLOT NOT FOUND , CHECK THE SKU/DOOR MATRIX - SPRINT 4 POINT 7

        //-----------Start commented as per anthony mail on 28 Jan 2015 --------------------//

        //bool bSKUMatrixDoorFound = false;
        int iSiteDoorNumberID = 0;
        int iSKUDoorTypeID = 0;
        //if (bUnconfirmedFixedSlotFound == false) {
        //    DataTable myDataTable = null;
        //    if (ViewState["myDataTable"] != null) {
        //        myDataTable = (DataTable)ViewState["myDataTable"];

        //        var column2Values = myDataTable.AsEnumerable().Select(x => x.Field<string>("PurchaseNumber"));
        //        string PurchaseNumbers = String.Join(",", column2Values.ToArray());
        //        if (!string.IsNullOrEmpty(PurchaseNumbers)) {

        //            APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
        //            MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

        //            oMASSIT_DoorOpenTimeBE.Action = "GetSKUMatrixDoorDetails";
        //            oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        //            oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        //            oMASSIT_DoorOpenTimeBE.PurchaseNumbers = PurchaseNumbers;


        //            List<MASSIT_DoorOpenTimeBE> lstSKUMatrixDoorDetails = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

        //            if (lstSKUMatrixDoorDetails != null && lstSKUMatrixDoorDetails.Count > 0) {
        //                bSKUMatrixDoorFound = true;
        //                iSiteDoorNumberID = lstSKUMatrixDoorDetails[0].SiteDoorNumberID;
        //                iSKUDoorTypeID = lstSKUMatrixDoorDetails[0].DoorType.DoorTypeID;
        //                if (lstSKUMatrixDoorDetails.Count > 1) {  //there is a conflict i.e the PO's contain SKU with different doors 
        //                    isProvisional = true;
        //                    ProvisionalReasonType = 1;
        //                    ProvisionalReason = "FIXED SLOT";
        //                }
        //            }
        //        }
        //    }
        //}

        //-----------End commented as per anthony mail on 28 Jan 2015 --------------------//
        //------------------------------------------------------------------------//


        oAPPBOK_BookingBE.Action = "CarrierBookingFindFixedSlotDoor";
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        oAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;

        oAPPBOK_BookingBE.VehicleType = new MASSIT_VehicleTypeBE();
        if (bUnconfirmedFixedSlotFound)
            oAPPBOK_BookingBE.VehicleType.VehicleTypeID = 0;
        else
            oAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);

        List<APPBOK_BookingBE> lstFixedDoor = oAPPBOK_BookingBAL.GetVendorBookingFixedSlotDoorBAL(oAPPBOK_BookingBE);

        if (((hdnSuggestedFixedSlotID.Value == "" || hdnSuggestedFixedSlotID.Value == "-1") && hdnBookingRef.Value == "")
                || (hdnFixedSlotMode.Value == "true")
            )
        {
            if (lstFixedDoor.Count > 0)
            {
                if (lstFixedDoor.FindAll(delegate(APPBOK_BookingBE St) { return St.VehicleType.VehicleTypeID == Convert.ToInt32(ddlVehicleType.SelectedItem.Value); }).Count <= 0)
                {
                    isProvisional = true;
                    ProvisionalReasonType = 2;
                    ProvisionalReason = "FIXED SLOT";
                }
            }
        }

        if (hdnCurrentMode.Value == "Edit" && hdnSuggestedFixedSlotID.Value != "-1" && hdnSuggestedFixedSlotID.Value != "")
        {
            if (lstFixedDoor.Count > 0)
            {
                if (lstFixedDoor.FindAll(delegate(APPBOK_BookingBE St) { return St.VehicleType.VehicleTypeID == Convert.ToInt32(ddlVehicleType.SelectedItem.Value); }).Count <= 0)
                {
                    isProvisional = true;
                    ProvisionalReasonType = 3;
                    ProvisionalReason = "FIXED SLOT";
                }
            }
        }


        lstFixedDoor = lstFixedDoor.FindAll(delegate(APPBOK_BookingBE St)
        {
            DateTime? dt = (DateTime?)null;
            if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
            {
                dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
            }
            else
            {
                dt = new DateTime(1900, 1, 1, 0, 0, 0);
            }
            //DateTime dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
            return dt <= lstWeekSetup[0].EndTime;
        });

        if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
        {

            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
            {
                oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1);
                List<APPBOK_BookingBE> lstFixedDoorStartDay = oAPPBOK_BookingBAL.GetVendorBookingFixedSlotDoorBAL(oAPPBOK_BookingBE);

                lstFixedDoorStartDay = lstFixedDoorStartDay.FindAll(delegate(APPBOK_BookingBE St)
                {
                    DateTime? dt = (DateTime?)null;
                    if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
                    {
                        dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    }
                    else
                    {
                        dt = new DateTime(1900, 1, 1, 0, 0, 0);
                    }
                    //DateTime dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    return dt >= lstWeekSetup[0].StartTime;
                });

                if (lstFixedDoorStartDay != null && lstFixedDoorStartDay.Count > 0)
                {
                    lstFixedDoor = MergeListCollections(lstFixedDoor, lstFixedDoorStartDay);
                }
            }
        }

        List<APPBOK_BookingBE> lstAvailableFixedDoorSlots = lstFixedDoor;

        //check if fixed slot broke the daily capacity
        bool isDailyCapacity = false;
        List<APPBOK_BookingBE> lstCurrentFixedSlots = new List<APPBOK_BookingBE>();
        if (hdnSuggestedFixedSlotID.Value != "" && hdnSuggestedFixedSlotID.Value != "-1")
        {
            lstCurrentFixedSlots = lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE St) { return St.FixedSlot.FixedSlotID == Convert.ToInt32(hdnSuggestedFixedSlotID.Value); });

            if (lstCurrentFixedSlots.Count > 0)
            {
                if (TotalPallets != "0" && ltRemainingPallets.Text != string.Empty && ltRemainingPallets.Text != "-")
                {

                    //-phase 19 point 1 - if soft filed slot , volume not be considered --//
                    if (lstCurrentFixedSlots[0].FixedSlot.AllocationType == "S" && hdnCurrentMode.Value != "Edit")
                        lstCurrentFixedSlots[0].FixedSlot.MaximumPallets = 0;
                    else
                        lstCurrentFixedSlots[0].FixedSlot.MaximumPallets = BookedPallets;
                    //-------------------------------------------------------------------//

                    if (Convert.ToInt32(TotalPallets) > (Convert.ToInt32(ltRemainingPallets.Text) + lstCurrentFixedSlots[0].FixedSlot.MaximumPallets))
                    {
                        isDailyCapacity = true;
                    }
                }
                else
                {
                    isDailyCapacity = true;
                }

                if (TotalLines != "0" && ltRemainingLines.Text != string.Empty && ltRemainingLines.Text != "-")
                {

                    //-phase 19 point 1 - if soft filed slot , volume not be considered --//
                    if (lstCurrentFixedSlots[0].FixedSlot.AllocationType == "S" && hdnCurrentMode.Value != "Edit")
                        lstCurrentFixedSlots[0].FixedSlot.MaximumLines = 0;
                    else
                        lstCurrentFixedSlots[0].FixedSlot.MaximumLines = BookedLines;
                    //-------------------------------------------------------------------//

                    if (Convert.ToInt32(TotalLines) > (Convert.ToInt32(ltRemainingLines.Text) + lstCurrentFixedSlots[0].FixedSlot.MaximumLines))
                    {
                        isDailyCapacity = true;
                    }
                }
                else
                {
                    isDailyCapacity = true;
                }
            }
        }
        //----------------------------------------------//
        //filter Fixed Slots on the basis of Fixed Slot Setup's Volume MaximumCatrons capacity 

        //-----------Start commented as per anthony mail on 28 Jan 2015 --------------------//

        //List<APPBOK_BookingBE> lstAvailableFixedDoorSlots = lstFixedDoor.FindAll(delegate(APPBOK_BookingBE vol) {
        //    if (vol.FixedSlot.MaximumCatrons > 0) {
        //        if (vol.FixedSlot.MaximumCatrons >= Convert.ToInt32(TotalCartons)) {
        //            // We just found fixed slot. Capture it.
        //            return true;
        //        }
        //        else {
        //            // Not a fixed slot
        //            return false;
        //        }
        //    }
        //    else {
        //        return true;
        //    }
        //});

        //filter Fixed Slots on the basis of Fixed Slot Setup's Volume MaximumLines capacity
        lstAvailableFixedDoorSlots = lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE vol)
        {
            if (vol.FixedSlot.MaximumLines > 0)
            {
                if (vol.FixedSlot.MaximumLines >= Convert.ToInt32(TotalLines))
                {
                    // We just found fixed slot. Capture it.
                    return true;
                }
                else
                {
                    // Not a fixed slot
                    return false;
                }
            }
            else
            {
                return true;
            }
        });

        //filter Fixed Slots on the basis of Fixed Slot Setup's Volume MaximumPallets capacity
        lstAvailableFixedDoorSlots = lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE vol)
        {
            if (vol.FixedSlot.MaximumPallets > 0)
            {
                if (vol.FixedSlot.MaximumPallets >= Convert.ToInt32(TotalPallets))
                {
                    // We just found fixed slot. Capture it.
                    return true;
                }
                else
                {
                    // Not a fixed slot
                    return false;
                }
            }
            else
            {
                return true;
            }
        });
        //------------------------------//

        // Not a fixed slot        
        if (lstFixedDoor.Count > 0 && lstAvailableFixedDoorSlots.Count <= 0)
        {
            lstAvailableFixedDoorSlots = lstFixedDoor;
            if (isDailyCapacity)
            {
                isProvisional = true;
                ProvisionalReasonType = 4;
                ProvisionalReason = "FIXED SLOT";
                if (bUnconfirmedFixedSlotFound == false)
                    hdnCapacityWarning.Value = "true";
            }
        }
        else
        {
            if (hdnSuggestedFixedSlotID.Value != "" && hdnSuggestedFixedSlotID.Value != "-1" && hdnCurrentMode.Value != "Edit")
            {
                if (lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE St) { return St.FixedSlot.FixedSlotID == Convert.ToInt32(hdnSuggestedFixedSlotID.Value); }).Count <= 0)
                {
                    if (isDailyCapacity)
                    {
                        isProvisional = true;
                        ProvisionalReasonType = 5;
                        ProvisionalReason = "FIXED SLOT";
                    }
                }
            }
        }

        //-----------End commented as per anthony mail on 28 Jan 2015 --------------------//


        //---------------IF ALLOCATION TYPE IS SOFT FIXED SLOT , AND CAPCITY REACHED----//
        if (hdnSuggestedFixedSlotID.Value != "" && hdnSuggestedFixedSlotID.Value != "-1")
        {
            if (isDailyCapacity)
            {
                if (lstCurrentFixedSlots.Count > 0)
                {
                    if (lstCurrentFixedSlots[0].FixedSlot.AllocationType == "S")
                    {
                        isProvisional = true;
                        ProvisionalReasonType = 16;
                        ProvisionalReason = "FIXED SLOT";
                        hdnCapacityWarning.Value = "true";
                        hdnIsProvisionalBySoftSlot.Value = "true";
                    }
                }
            }
        }
        //------------------------------------------------------------------------------//


        //------Check for Specific Entry First--------//

        DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

        string SlotIDs = string.Empty;

        if (strOpenDoorIds != ",")
            SlotIDs = strOpenDoorIds;
        else
            SlotIDs = GetDoorConstraints();

        if (bUnconfirmedFixedSlotFound == false)
            lstBookings = lstBookings.FindAll(delegate(APPBOK_BookingBE St) { return SlotIDs.Contains(St.SlotTime.SlotTimeID + "@"); });


        //------------------------------------------------------//


        //FILETR OUT NON TIME DELIVERY
        lstBookings = lstBookings.FindAll(delegate(APPBOK_BookingBE St) { return St.SlotTime.SlotTime != ""; });

        if (lstBookings.Count > 0)
        {

            //if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null) {
            //    if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday) {
            //        var lstBookingsSort = from element in lstBookings
            //                              orderby element.SlotTime.SlotTimeID ascending
            //                              select element;
            //        lstBookings = lstBookingsSort.ToList();
            //    }
            //}       

            gvBookingSlots.DataSource = lstBookings;
            gvBookingSlots.DataBind();
            //ViewState["lstBookings"] = lstBookings;
        }


        //filter Booked Slots which are alrady booked
        List<APPBOK_BookingBE> lstAvailableSlots = new List<APPBOK_BookingBE>();
        if (hdnCurrentMode.Value == "Add")
        {
            lstAvailableSlots = lstBookings.FindAll(delegate(APPBOK_BookingBE St) { return St.Status != "Booked"; });
        }
         // changes
        else if (hdnCurrentMode.Value == "Edit" && (txtSchedulingdate.innerControltxtDate.Value != GetQueryStringValue("Scheduledate").ToString()
             || (hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue && isDoorTypeNotChanged == false))
            )
        {
            lstAvailableSlots = lstBookings.FindAll(delegate (APPBOK_BookingBE St) { return St.Status != "Booked"; });
        }
        else if (hdnCurrentMode.Value == "Edit") {
            lstAvailableSlots = lstBookings;
        }


        //find a specific fixed slot for suggestion
        List<APPBOK_BookingBE> ObjAPPBOK_BookingBE = null;
        bool isFixedSlotFind = false;
        bool isWarning = false;

        if (
            (hdnSuggestedFixedSlotID.Value == "" || hdnSuggestedFixedSlotID.Value == "-1") && hdnBookingRef.Value == "" ) 
            //|| 
            //(txtSchedulingdate.innerControltxtDate.Value != GetQueryStringValue("Scheduledate").ToString() && hdnCurrentMode.Value == "Edit") ||
            //(hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue && hdnPreviousVehicleType.Value!="")
            
        {
            for (int iCount = 0; iCount < lstAvailableSlots.Count; iCount++)
            {
                ObjAPPBOK_BookingBE = lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE p) { return p.FixedSlot.FixedSlotID == lstAvailableSlots[iCount].FixedSlot.FixedSlotID; });
                if (ObjAPPBOK_BookingBE.Count > 0)
                {

                    //Check delivery constraints
                    bool isValidTimeSlot = CheckValidSlot(ObjAPPBOK_BookingBE[0].SlotTime.SlotTime, ObjAPPBOK_BookingBE[0].ScheduleDate.Value.ToString("ddd"), TotalPallets, TotalLines);

                    if (bUnconfirmedFixedSlotFound)
                        isValidTimeSlot = true;

                    if (isValidTimeSlot == false)
                    {
                        continue;
                    }
                    else
                    {
                        isFixedSlotFind = true;
                        ltBookingDate.Text = ObjAPPBOK_BookingBE[0].ScheduleDate.Value.Day.ToString() + "/" + ObjAPPBOK_BookingBE[0].ScheduleDate.Value.Month.ToString() + "/" + ObjAPPBOK_BookingBE[0].ScheduleDate.Value.Year.ToString(); // ActualSchedulDateTime.ToString("dd/MM/yyyy");
                        ltSuggestedSlotTime.Text = ObjAPPBOK_BookingBE[0].SlotTime.SlotTime + " " + AtDoorName + " " + ObjAPPBOK_BookingBE[0].DoorNoSetup.DoorNumber.ToString();
                        hdnSuggestedSlotTimeID.Value = ObjAPPBOK_BookingBE[0].SlotTime.SlotTimeID.ToString();
                        hdnSuggestedFixedSlotID.Value = ObjAPPBOK_BookingBE[0].FixedSlot.FixedSlotID.ToString();
                        hdnSuggestedSiteDoorNumberID.Value = ObjAPPBOK_BookingBE[0].DoorNoSetup.SiteDoorNumberID.ToString();
                        hdnSuggestedSiteDoorNumber.Value = ObjAPPBOK_BookingBE[0].DoorNoSetup.DoorNumber.ToString();
                        hdnSuggestedSlotTime.Value = ObjAPPBOK_BookingBE[0].SlotTime.SlotTime;
                        ltTimeSlot.Text = hdnSuggestedSlotTime.Value;
                        ltDoorNo.Text = hdnSuggestedSiteDoorNumber.Value;
                        ltTimeSlot_1.Text = ObjAPPBOK_BookingBE[0].SlotTime.SlotTime;
                        ltDoorNo_1.Text = hdnSuggestedSiteDoorNumber.Value;
                        hdnSuggestedWeekDay.Value = ObjAPPBOK_BookingBE[0].ScheduleDate.Value.ToString("ddd"); // ActualSchedulDateTime.ToString("ddd");

                        //Check Volume Details//
                        if (ObjAPPBOK_BookingBE[0].FixedSlot.MaximumPallets < Convert.ToInt32(TotalPallets))
                        {
                            int fixedslotlength = GetRequiredTimeSlotLength(ObjAPPBOK_BookingBE[0].FixedSlot.MaximumPallets, ObjAPPBOK_BookingBE[0].FixedSlot.MaximumCatrons);
                            int BookingLength = GetRequiredTimeSlotLength(Convert.ToInt32(TotalPallets), Convert.ToInt32(TotalCartons));
                            if (fixedslotlength < BookingLength)
                            {
                                //-------------CHECK IF BOOKING EFFECTING ANOTHER BOOKING PHASE 12 POINT 3---------------//
                                if (CheckCurrentBookingImpact(ObjAPPBOK_BookingBE[0].DoorNoSetup.SiteDoorNumberID, ObjAPPBOK_BookingBE[0].SlotTime.OrderBYID, fixedslotlength, BookingLength))
                                {
                                    isProvisional = true;
                                    ProvisionalReasonType = 6;
                                    ProvisionalReason = "FIXED SLOT";
                                    hdnCapacityWarning.Value = "true";
                                }
                                //---------------------------------------------------------------------//
                            }
                        }
                        else
                        {
                            if (lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE St) { return St.FixedSlot.FixedSlotID == Convert.ToInt32(ObjAPPBOK_BookingBE[0].FixedSlot.FixedSlotID) && St.VehicleType.VehicleTypeID == Convert.ToInt32(ddlVehicleType.SelectedItem.Value); }).Count <= 0)
                            {
                                isProvisional = true;
                                ProvisionalReasonType = 7;
                                ProvisionalReason = "FIXED SLOT";
                            }
                        }
                        //-------------------//

                        //changes for showing lpropose label and confirm button...
                        btnConfirmBooking.Visible = true;
                        lblProposeBookingInfo.Visible = true;
                        ltSuggestedSlotTime.Visible = true;
                        ltBookingDate.Visible = true;

                        break;
                    }
                }
            }


            //if (ObjAPPBOK_BookingBE == null || ObjAPPBOK_BookingBE.Count == 0) {  //No Fixed Slot find for suggestion,find alternate slot
            if (isFixedSlotFind == false)
            {    //No Fixed Slot find for suggestion,find alternate slot

                btnConfirmBooking.Visible = false;
                lblProposeBookingInfo.Visible = false;
                btnNotHappy.Visible = false;

                //ltBookingDate.Visible = false;
                
 		        ltSuggestedSlotTime.Visible = false;
                ltBookingDate.Visible = false;


                //Find Slot for this booking 
                GenerateAlternateSlot(true, iSiteDoorNumberID, iSKUDoorTypeID);
                //GenerateAlternateSlot(true);

                // tableDiv_General.Controls.RemoveAt(0);
            }



            //---------start Phase 9 Point 2----------------------//

            int intCapacityAvailable = 0;
            if (hdnSuggestedSlotTimeID.Value == string.Empty || hdnSuggestedSlotTimeID.Value == "-1")
            {
                //if (isFixedSlotFind == false) { 
                if (TotalPallets != "0" && ltRemainingPallets.Text != string.Empty && ltRemainingPallets.Text != "-")
                {
                    if (Convert.ToInt32(ltRemainingPallets.Text) > 0)
                    {
                        intCapacityAvailable++;
                    }
                }

                if (TotalLines != "0" && ltRemainingLines.Text != string.Empty && ltRemainingLines.Text != "-")
                {
                    if (Convert.ToInt32(ltRemainingLines.Text) > 0)
                    {
                        intCapacityAvailable++;
                    }
                }
            }
            //---------End Phase 9 Point 2----------------------//

            if (intCapacityAvailable == 2)
            {

                if (hdnCurrentRole.Value == "Carrier")
                {
                    btnConfirmBooking.Visible = false;
                    isWarning = true;
                }
            }
            else
            {
                //No space remaining on compatable door  -- show advice
                if (hdnSuggestedSlotTimeID.Value == string.Empty || hdnSuggestedSlotTimeID.Value == "-1")
                {
                    string errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_EXT"); // "No space available on any compatible doors.Please contact Goods In office or select a different date.";

                    if (hdnCurrentRole.Value == "Carrier")
                    {
                        //Show err msg
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        //return;
                    }
                    else
                    {
                        //Show err msg
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_28_INT"); //"No space available on any compatible doors.";
                        ShowErrorMessage(errMsg, "BK_MS_28");
                        //return;
                    }
                }
            }
        }
        else
        {
            //added by shrish
            ////Find Slot for this booking 
            if (hdnCurrentRole.Value != "Carrier")
            {
                GenerateAlternateSlot(false, iSiteDoorNumberID, iSKUDoorTypeID);
            }

            //Check Volume Details//
            ObjAPPBOK_BookingBE = lstAvailableFixedDoorSlots.FindAll(delegate(APPBOK_BookingBE p) { return p.FixedSlot.FixedSlotID == Convert.ToInt32(hdnSuggestedFixedSlotID.Value); });
            if (ObjAPPBOK_BookingBE.Count > 0)
            {
                if (ObjAPPBOK_BookingBE[0].FixedSlot.MaximumPallets < Convert.ToInt32(TotalPallets))
                {
                    int fixedslotlength = GetRequiredTimeSlotLength(ObjAPPBOK_BookingBE[0].FixedSlot.MaximumPallets, ObjAPPBOK_BookingBE[0].FixedSlot.MaximumCatrons);
                    int BookingLength = GetRequiredTimeSlotLength(Convert.ToInt32(TotalPallets), Convert.ToInt32(TotalCartons));
                    if (fixedslotlength < BookingLength)
                    {
                        //-------------CHECK IF BOOKING EFFECTING ANOTHER BOOKING PHASE 12 POINT 3---------------//
                        if (CheckCurrentBookingImpact(ObjAPPBOK_BookingBE[0].DoorNoSetup.SiteDoorNumberID, ObjAPPBOK_BookingBE[0].SlotTime.OrderBYID, fixedslotlength, BookingLength))
                        {
                            isProvisional = true;
                            ProvisionalReasonType = 6;
                            ProvisionalReason = "FIXED SLOT";
                            hdnCapacityWarning.Value = "true";
                        }
                        //---------------------------------------------------------------------//
                    }
                }
            }
        }



        //---------------------To Fix the follwoing issue--------------//
        //1. OD makes booking on a date which is already over capacity for lines
        //2. Vendor then edits this booking and raises the lines significantly
        //3. System allows this, booking does not go Provisional

        if (hdnCurrentMode.Value == "Edit" && isWarning == false)
        {
            int intCapacityAvailableDaily = 0;

            if ((hdnSuggestedFixedSlotID.Value == "" || hdnSuggestedFixedSlotID.Value == "-1"))
            {
                if (TotalPallets != "0" && RemainingPallets != (int?)null)
                {
                    if (Convert.ToInt32(TotalPallets) > RemainingPallets + BookedPallets)
                    {
                        intCapacityAvailableDaily++;
                    }
                }

                if (TotalLines != "0" && RemainingLines != (int?)null)
                {
                    if (Convert.ToInt32(TotalLines) > RemainingLines + BookedLines)
                    {
                        intCapacityAvailableDaily++;
                    }
                }
                if (intCapacityAvailableDaily > 0)
                {
                    if (hdnCurrentRole.Value == "Carrier")
                    {
                        //btnConfirmBooking.Visible = false;
                        //isWarning = true;
                        isProvisional = true;
                        ProvisionalReasonType = 15;
                        ProvisionalReason = "EDITED";
                    }
                }
            }
        }
        //---------------------------------------------------------------------------//




        if (hdnCurrentMode.Value == "Add")
        {
            foreach (GridViewRow gvRow in gvBookingSlots.Rows)
            {
                if (gvRow.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hdnFixedSlotID = (HiddenField)gvRow.FindControl("hdnAvailableFixedSlotID");
                    if (hdnFixedSlotID != null && hdnFixedSlotID.Value == hdnSuggestedFixedSlotID.Value && hdnSuggestedFixedSlotID.Value != "-1")
                    {
                        gvRow.Cells[6].Text = "This Booking";
                        gvRow.BackColor = Color.GreenYellow;
                        gvRow.ForeColor = Color.Black;
                        break;
                    }
                }
            }
        }
        else if (hdnCurrentMode.Value == "Edit")
        {

            bool isSlotFound = false;
            foreach (GridViewRow gvRow in gvBookingSlots.Rows)
            {
                if (gvRow.RowType == DataControlRowType.DataRow)
                {
                    if (gvRow.Cells[3].Text.Trim() == hdnBookingRef.Value)
                    {
                        gvRow.Cells[6].Text = "This Booking";
                        gvRow.BackColor = Color.GreenYellow;
                        gvRow.ForeColor = Color.Black;
                        isSlotFound = true;
                        break;
                    }
                }
            }

            if (isSlotFound == false)
            {
                foreach (GridViewRow gvRow in gvBookingSlots.Rows)
                {
                    if (gvRow.RowType == DataControlRowType.DataRow)
                    {
                        HiddenField hdnFixedSlotID = (HiddenField)gvRow.FindControl("hdnAvailableFixedSlotID");
                        if (hdnFixedSlotID != null && hdnFixedSlotID.Value == hdnSuggestedFixedSlotID.Value && hdnSuggestedFixedSlotID.Value != "-1")
                        {
                            gvRow.Cells[6].Text = "This Booking";
                            gvRow.BackColor = Color.GreenYellow;
                            gvRow.ForeColor = Color.Black;
                            break;
                        }
                    }
                }
            }

            //---------start Phase 9 Point 2----------------------//
            if (isSlotFound == false)
            {
                int intCapacityAvailable = 0;
                if (hdnSuggestedSlotTimeID.Value == string.Empty || hdnSuggestedSlotTimeID.Value == "-1" || hdnSuggestedSlotTimeID.Value == "0")
                {
                    if (TotalPallets != "0" && ltRemainingPallets.Text != string.Empty && ltRemainingPallets.Text != "-")
                    {

                        if (Convert.ToInt32(ltRemainingPallets.Text) >= 0)
                        {
                            if (Convert.ToInt32(ltRemainingPallets.Text) + BookedPallets - Convert.ToInt32(TotalPallets) < 0)
                            {
                                intCapacityAvailable++;
                            }
                        }
                        else
                        {
                            if (BookedPallets - Convert.ToInt32(TotalPallets) < 0)
                            {
                                intCapacityAvailable++;
                            }
                        }
                    }

                    if (TotalLines != "0" && ltRemainingLines.Text != string.Empty && ltRemainingLines.Text != "-")
                    {
                        if (Convert.ToInt32(ltRemainingLines.Text) >= 0)
                        {
                            if (Convert.ToInt32(ltRemainingLines.Text) + BookedLines - Convert.ToInt32(TotalLines) < 0)
                            {
                                intCapacityAvailable++;
                            }
                        }
                        else
                        {
                            if (BookedLines - Convert.ToInt32(TotalLines) < 0)
                            {
                                intCapacityAvailable++;
                            }
                        }
                    }
                }

                if (intCapacityAvailable > 0)
                {

                    if (hdnCurrentRole.Value == "Carrier")
                    {
                        btnConfirmBooking.Visible = false;
                        isWarning = true;
                    }
                }
            }
            //---------End Phase 9 Point 2----------------------//
        }

        //---------start Phase 9 Point 2----------------------//
        if (isWarning)
            ShowProvisionalWarning();
        //---------End Phase 9 Point 2----------------------//
    }



    private bool CheckCurrentBookingImpact(int DoorID, int StartSlotID, int FixedslotLenght, int BookingLength)
    {

        bool isBookingImpact = false;
        DateTime dtSelectedDate = LogicalSchedulDateTime;
        string WeekDay = dtSelectedDate.ToString("dddd");

        DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
        oMASSIT_WeekSetupBE.ScheduleDate = LogicalSchedulDateTime;

        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
        {
            oMASSIT_WeekSetupBE.Action = "ShowAll";
            lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
        }


        //--------Find all Fixed Slots, Bookings For all Vendor/Carrier-------//
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime;
        lstAllFixedDoor = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

        lstAllFixedDoor = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE St)
        {
            DateTime? dt = (DateTime?)null;
            if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
            {
                dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
            }
            else
            {
                dt = new DateTime(1900, 1, 1, 0, 0, 0);
            }
            return dt <= lstWeekSetup[0].EndTime;
        });

        //--------------------------------------//


        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        {
            oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1);
            List<APPBOK_BookingBE> lstAllFixedDoorStartWeek = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

            //Filetr Non Time Delivery
            lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
            {
                return St.SlotTime.SlotTime != "";
            });

            if (lstAllFixedDoorStartWeek != null && lstAllFixedDoorStartWeek.Count > 0)
            {
                lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
                {
                    DateTime? dt = (DateTime?)null;
                    if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
                    {
                        dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    }
                    else
                    {
                        dt = new DateTime(1900, 1, 1, 0, 0, 0);
                    }
                    return dt >= lstWeekSetup[0].StartTime;
                });

                if (lstAllFixedDoorStartWeek.Count > 0)
                {
                    lstAllFixedDoor = MergeListCollections(lstAllFixedDoor, lstAllFixedDoorStartWeek);
                }
            }
        }

        int iFixedSlotLen = StartSlotID; // +FixedslotLenght;
        int iBookingLen = StartSlotID + BookingLength;
        int iNextDaySlost = -1;
        if (iBookingLen > 288)
        {
            iNextDaySlost = iBookingLen - 288;
            iBookingLen = 288;
        }

        //filter Door no
        lstAllFixedDoor = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE St) { return St.DoorNoSetup.SiteDoorNumberID == DoorID; });

        for (int iCount = iFixedSlotLen + 1; iCount < iBookingLen; iCount++)
        {
            if (lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE St) { return St.SlotTime.OrderBYID == iCount; }).Count > 0)
            {
                isBookingImpact = true;
                break;
            }
        }

        if (isBookingImpact == false && iNextDaySlost != -1)
        {
            for (int iCount = 1; iCount < iNextDaySlost; iCount++)
            {
                if (lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE St) { return St.SlotTime.OrderBYID == iCount; }).Count > 0)
                {
                    isBookingImpact = true;
                    break;
                }
            }
        }

        return isBookingImpact;
    }

    private bool CheckCurrentBookingImpactEdit(int DoorID, int StartSlotID, int BookingLength)
    {

        bool isBookingImpact = false;
        DateTime dtSelectedDate = LogicalSchedulDateTime;
        string WeekDay = dtSelectedDate.ToString("dddd");

        DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
        oMASSIT_WeekSetupBE.ScheduleDate = LogicalSchedulDateTime;

        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
        {
            oMASSIT_WeekSetupBE.Action = "ShowAll";
            lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
        }


        //--------Find all Fixed Slots, Bookings For all Vendor/Carrier-------//
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime;
        lstAllFixedDoor = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

        lstAllFixedDoor = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE St)
        {
            DateTime? dt = (DateTime?)null;
            if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
            {
                dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
            }
            else
            {
                dt = new DateTime(1900, 1, 1, 0, 0, 0);
            }
            return dt <= lstWeekSetup[0].EndTime;
        });

        //--------------------------------------//


        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        {
            oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1);
            List<APPBOK_BookingBE> lstAllFixedDoorStartWeek = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

            //Filetr Non Time Delivery
            lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
            {
                return St.SlotTime.SlotTime != "";
            });

            if (lstAllFixedDoorStartWeek != null && lstAllFixedDoorStartWeek.Count > 0)
            {
                lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
                {
                    DateTime? dt = (DateTime?)null;
                    if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
                    {
                        dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    }
                    else
                    {
                        dt = new DateTime(1900, 1, 1, 0, 0, 0);
                    }
                    return dt >= lstWeekSetup[0].StartTime;
                });

                if (lstAllFixedDoorStartWeek.Count > 0)
                {
                    lstAllFixedDoor = MergeListCollections(lstAllFixedDoor, lstAllFixedDoorStartWeek);
                }
            }
        }


        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();

        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);
        //lstSlotTime = lstSlotTime.Where(s => s.SlotTimeID == StartSlotID).ToList();
        StartSlotID = lstSlotTime.Where(s => s.SlotTimeID == StartSlotID).ToList()[0].SlotTimeID;


        //  int iFixedSlotLen = StartSlotID + FixedslotLenght; //this is basically orderby id
        int iBookingLen = StartSlotID + BookingLength; //this is basically orderby id
        int iNextDaySlost = -1;
        //if (iBookingLen > 288)
        //{
        //    iNextDaySlost = iBookingLen - 288;
        //    iBookingLen = 288;
        //}

        if (iBookingLen > 96)
        {
            iNextDaySlost = iBookingLen - 96;
            iBookingLen = 96;
        }

        //filter Door no
        lstAllFixedDoor = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE St) { return St.DoorNoSetup.SiteDoorNumberID == DoorID; });


        for (int iCount = StartSlotID + 1; iCount < iBookingLen; iCount++)
        {//(lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE St) { return St.SlotTime.SlotTimeID == iCount; }).Count > 0)
            if (lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE St) { return St.SlotTime.SlotTimeID == iCount; }).Count > 0)
            {
                isBookingImpact = true;
                break;
            }
            else
            {//!strOpenDoorIds.Contains("," + lstSlotTime.Where(s => s.OrderBYID == iCount).ToList()[0].SlotTimeID + "@" + DoorID)
                if (!strOpenDoorIds.Contains("," + lstSlotTime.Where(s => s.SlotTimeID == iCount).ToList()[0].SlotTimeID + "@" + DoorID))
                {  //Door Not Available 
                    isBookingImpact = true;
                    break;
                }
            }
        }

        if (isBookingImpact == false && iNextDaySlost != -1)
        {
            for (int iCount = 1; iCount < iNextDaySlost; iCount++)
            {//delegate(APPBOK_BookingBE St) { return St.SlotTime.OrderBYID == iCount; }
                if (lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE St) { return St.SlotTime.SlotTimeID == iCount; }).Count > 0)
                {
                    isBookingImpact = true;
                    break;
                }
            }
        }

        return isBookingImpact;
    }

    //---------start Phase 9 Point 2----------------------//
    private void ShowProvisionalWarning()
    {
        mdlProConfirmMsg.Show();
        btnConfirmBooking.Visible = false;
    }

    protected void btnProContinue_Click(object sender, EventArgs e)
    {
        isProvisional = true;
        ProvisionalReasonType = 9;
        ProvisionalReason = "NO SPACE";
        MakeBooking(BookingType.ProvisionalNonTimedDelivery.ToString());
    }

    protected void btnProBack_Click(object sender, EventArgs e)
    {
        mdlProConfirmMsg.Hide();
        EncryptQueryString("APPBok_BookingOverview.aspx");
    }
    //---------End Phase 9 Point 2----------------------//

    //Check delivery constraints
    private bool CheckValidSlot(string strStartTime, string strStartWeekDay, string strTotalPallets, string strTotalLines)
    {

        if (string.IsNullOrEmpty(strTotalPallets))
            strTotalPallets = "0";
        if (string.IsNullOrEmpty(strTotalLines))
            strTotalLines = "0";

        APPSIT_DeliveryConstraintSpecificBAL oAPPSIT_DeliveryConstraintSpecificBAL = new APPSIT_DeliveryConstraintSpecificBAL();
        MASSIT_DeliveryConstraintSpecificBE oMASSIT_DeliveryConstraintSpecificBE = new MASSIT_DeliveryConstraintSpecificBE();

        DateTime dtSelectedDate = LogicalSchedulDateTime;
        string WeekDay = dtSelectedDate.ToString("ddd");

        oMASSIT_DeliveryConstraintSpecificBE.Action = "GetDeliveryConstraintForBooking";
        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint = new MASSIT_DeliveryConstraintBE();
        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.Weekday = WeekDay;
        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartTime = Convert.ToInt32(strStartTime.Split(':')[0]);
        oMASSIT_DeliveryConstraintSpecificBE.WeekStartDate = dtSelectedDate;
        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartWeekday = strStartWeekDay;

        List<MASSIT_DeliveryConstraintSpecificBE> lstDeliveryConstraintSpecific = oAPPSIT_DeliveryConstraintSpecificBAL.GetDeliveryConstraintSpecificBAL(oMASSIT_DeliveryConstraintSpecificBE);

        if (lstDeliveryConstraintSpecific != null && lstDeliveryConstraintSpecific.Count > 0)
        {
            //Get Booked Volume
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

            oAPPBOK_BookingBE.Action = "GetBookingVolumeDeatils";
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;

            string strWeekDay = LogicalSchedulDateTime.DayOfWeek.ToString().ToUpper();
            if (strWeekDay == "SUNDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 1;
            }
            else if (strWeekDay == "MONDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 2;
            }
            else if (strWeekDay == "TUESDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 3;
            }
            else if (strWeekDay == "WEDNESDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 4;
            }
            else if (strWeekDay == "THURSDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 5;
            }
            else if (strWeekDay == "FRIDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 6;
            }
            else if (strWeekDay == "SATURDAY")
            {
                oAPPBOK_BookingBE.WeekDay = 7;
            }
            oAPPBOK_BookingBE.StartTime = strStartTime.Split(':')[0] + ":00";

            List<APPBOK_BookingBE> lstBookedVolume = oAPPBOK_BookingBAL.GetBookingVolumeDeatilsBAL(oAPPBOK_BookingBE);

            //if (lstBookedVolume != null && lstBookedVolume.Count > 0) {

            //    if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.RestrictDelivery == 0) ||
            //       (lstBookedVolume[0].BookingCount + 1 <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.RestrictDelivery)))
            //        return false;

            //    if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLine == 0) ||
            //        (lstBookedVolume[0].NumberOfLines + Convert.ToInt32(strTotalLines) <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLine)))
            //        return false;

            //    if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLift == 0) ||
            //        (lstBookedVolume[0].NumberOfPallet + Convert.ToInt32(strTotalPallets) <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLift))) //Here Lift means pallets
            //        return false;

            //    if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryPalletRestriction == 0) ||
            //        (Convert.ToInt32(strTotalPallets) <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryPalletRestriction)))
            //        return false;

            //    if (!((lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryLineRestriction == 0) ||
            //        (Convert.ToInt32(strTotalLines) <= lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryLineRestriction)))
            //        return false;
            //}

            if (lstBookedVolume != null && lstBookedVolume.Count > 0)
            {

                if (hdnCurrentMode.Value == "Add")
                {
                    if (((lstDeliveryConstraintSpecific[0].DeliveryConstraint.RestrictDelivery > 0) &&
                        (lstBookedVolume[0].BookingCount + 1 > lstDeliveryConstraintSpecific[0].DeliveryConstraint.RestrictDelivery)))
                        return false;
                }

                if (((lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLine > 0) &&
                    ((lstBookedVolume[0].NumberOfLines + Convert.ToInt32(strTotalLines)) - BookedLines > lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLine)))
                    return false;

                if (((lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLift > 0) &&
                    ((lstBookedVolume[0].NumberOfPallet + Convert.ToInt32(strTotalPallets)) - BookedPallets > lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLift))) //Here Lift means pallets
                    return false;
            }

            if (((lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryPalletRestriction > 0) &&
                (Convert.ToInt32(strTotalPallets) > lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryPalletRestriction)))
                return false;

            if (((lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryLineRestriction > 0) &&
                (Convert.ToInt32(strTotalLines) > lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryLineRestriction)))
                return false;

            return true;
        }
        return true;
    }

    //public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {
    //return Utilities.GenericListHelper<APPBOK_BookingBE>.SortList((List<APPBOK_BookingBE>)ViewState["lstBookings"], e.SortExpression, e.SortDirection).ToArray();
    //}

    protected void gvBookingSlots_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[6].Text == "Booked")
            {
                e.Row.BackColor = Color.Purple;
                e.Row.ForeColor = Color.White;
            }
        }
    }

    protected void btnConfirmBooking_Click(object sender, EventArgs e)
    {
        //-----------Start Stage 9 point 2a/2b--------------//
        if (hdnisProvisionalBooking.Value == "true" && hdnCurrentMode.Value == "Edit")
        {
            if (hdnSuggestedSlotTimeID.Value == "-1" || hdnSuggestedSlotTimeID.Value == string.Empty || hdnSuggestedSlotTimeID.Value == "0")
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("confirmprovisionalbooking");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                return;
            }
        }
        //-----------End Stage 9 point 2a/2b--------------//

        ConfirmFixedSlotBooking();
    }


    private void ConfirmFixedSlotBooking()
    {
        string errMsg = string.Empty;
        bool isWarning = false;

        if ((isProvisional) || (hdnCapacityWarning.Value == "true" && hdnSuggestedFixedSlotID.Value != "-1" && hdnSuggestedFixedSlotID.Value != string.Empty))
        {
            //if (hdnCapacityWarning.Value == "true" && hdnSuggestedFixedSlotID.Value != "-1" && hdnSuggestedFixedSlotID.Value != string.Empty) {
            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
            {
                errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_EXT");// "The volume being booked in exceeds the scheduled capacity for your slot - This delivery has been provisionally accepted. Office Depot's Goods In team will review and will contact you if we need to reschedule this booking.";
                AddWarningMessages(errMsg, "BK_MS_23_EXT");
                isWarning = true;
            }
            else
            {
                if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
                {  // If coming from Provisional page , ignore fixed slot capacity -- Phase 12 R2 Point 2
                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_INT");// "The volume being booked in exceeds the scheduled capacity for this fixed slot. Click Continue to confirm booking or click Back to select a different slot.";
                    AddWarningMessages(errMsg, "BK_MS_23_INT");
                    isWarning = true;
                }
            }
        }

        //----------soft fixed slot---------------//
        if (isWarning == false)
        {
            if (hdnIsProvisionalBySoftSlot.Value == "true")
            {

                if (hdnSuggestedFixedSlotID.Value != "-1" && hdnSuggestedFixedSlotID.Value != string.Empty)
                {

                    if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_EXT");// "The volume being booked in exceeds the scheduled capacity for your slot - This delivery has been provisionally accepted. Office Depot's Goods In team will review and will contact you if we need to reschedule this booking.";
                        AddWarningMessages(errMsg, "BK_MS_23_EXT");
                        isWarning = true;
                    }
                    else
                    {
                        if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
                        {  // If coming from Provisional page , ignore fixed slot capacity -- Phase 12 R2 Point 2
                            errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_INT");// "The volume being booked in exceeds the scheduled capacity for this fixed slot. Click Continue to confirm booking or click Back to select a different slot.";
                            AddWarningMessages(errMsg, "BK_MS_23_INT");
                            isWarning = true;
                        }
                    }
                }
            }
        }
        //---------------------------------------//

        if (isWarning == false)
            if (hdnSuggestedFixedSlotID.Value != "-1" && hdnSuggestedFixedSlotID.Value != string.Empty)
            {
                MakeBooking(BookingType.FixedSlot.ToString());
            }
            else
            {
                if (IsNonTimeBooking == true)
                    MakeBooking(BookingType.NonTimedDelivery.ToString());
                else
                    if (hdnCurrentMode.Value == "Edit")
                    {
                        //chack if booking effecting another booking or door is closed         
                        int BookingLength = GetRequiredTimeSlotLength(Convert.ToInt32(TotalPallets), Convert.ToInt32(TotalCartons));

                        //-------------CHECK IF BOOKING EFFECTING ANOTHER BOOKING PHASE 12 POINT 3---------------//
                        if (CheckCurrentBookingImpactEdit(Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value), Convert.ToInt32(hdnSuggestedSlotTimeID.Value), BookingLength))
                        {

                            if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                            {
                                errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_EXT");// "The volume being booked in exceeds the scheduled capacity for your slot - This delivery has been provisionally accepted. Office Depot's Goods In team will review and will contact you if we need to reschedule this booking.";
                                AddWarningMessages(errMsg, "BK_MS_23_EXT_1");
                                isWarning = true;
                            }
                            else
                            {
                                if (!(hdnCurrentMode.Value == "Edit" && GetQueryStringValue("BP") != null && GetQueryStringValue("BP").ToString() == "Pro"))
                                {  // If coming from Provisional page ,ignore fixed slot capacity -- Phase 12 R2 Point 2
                                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_23_INT");// "The volume being booked in exceeds the scheduled capacity for this fixed slot. Click Continue to confirm booking or click Back to select a different slot.";
                                    AddWarningMessages(errMsg, "BK_MS_23_INT_1");
                                    isWarning = true;
                                }
                            }
                        }
                        //---------------------------------------------------------------------//
                    }

                if (isWarning == false)
                    MakeBooking(BookingType.BookedSlot.ToString());
                else
                    ShowWarningMessages();
            }
        else
            ShowWarningMessages();
    }

    private void MakeBooking(string ThisBookingType)
    {
        if (Session["BookingCreated"] != null && Convert.ToBoolean(Session["BookingCreated"]) == true)
        {

            EncryptQueryString("APPBok_Confirm.aspx?MailSend=False&BookingID=" + Session["CreatedBookingID"].ToString() + "&IsBookingAmended=" + Session["IsBookingAmended"].ToString());
            return;
        }

        //-----------Start Stage 9 point 2a/2b--------------//
        if (ThisBookingType != BookingType.ProvisionalNonTimedDelivery.ToString())
        {
            //-----------End Stage 9 point 2a/2b--------------//
            if (ThisBookingType != BookingType.NonTimedDelivery.ToString())
            {
                if (hdnSuggestedSlotTimeID.Value == "-1" || hdnSuggestedSlotTimeID.Value == string.Empty)
                    return;
            }

            if (ThisBookingType == BookingType.NonTimedDelivery.ToString())
            {
                isProvisional = false;
                hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.DayOfWeek.ToString().ToLower().Substring(0, 3);
            }

            if (isProvisional)
            {
                if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                {
                    ThisBookingType = BookingType.Provisional.ToString();
                }
            }
        }

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        //-------Check booking is made for same time slot -----//
        if (ThisBookingType != BookingType.NonTimedDelivery.ToString() && hdnSuggestedSlotTimeID.Value != "-1" && hdnSuggestedSlotTimeID.Value != string.Empty)
        {
            oAPPBOK_BookingBE.SlotTime = new SYS_SlotTimeBE();
            oAPPBOK_BookingBE.DoorNoSetup = new MASSIT_DoorNoSetupBE();

            oAPPBOK_BookingBE.Action = "CheckBooking";
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
            oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
            if (hdnCurrentMode.Value == "Edit")
            {
                oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
            }
            int? iBookingCount = oAPPBOK_BookingBAL.CheckBookingBAL(oAPPBOK_BookingBE);

            if (Convert.ToInt32(iBookingCount) > 0)
            {
                string errMsg = WebCommon.getGlobalResourceValue("BK_MS_20"); // "This slot is no longer available,please select another slot.";
                ShowErrorMessage(errMsg, "BK_MS_20");
                return;
            }
        }
        //----------------------------------------------------//

        ///for weekday setting value...
        if (ThisBookingType == BookingType.ProvisionalNonTimedDelivery.ToString())
        {
            hdnSuggestedWeekDay.Value = LogicalSchedulDateTime.DayOfWeek.ToString().ToLower().Substring(0, 3);
        }

        string strBookingID = string.Empty;

        #region Commented code for point 31
        //MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        //if (singleSiteSetting.PerBookingVendor.Trim().ToUpper().Equals("B"))
        //{
        //    strBookingID = this.MakeMultiVendorBooking(ThisBookingType);
        //}
        //else
        //{
        // Note --> Here need to cut and paste the complete below [Make Booking Logic ...] region section for point 31.
        //}
        #endregion

        #region Make Booking Logic ...
        oAPPBOK_BookingBE = new APPBOK_BookingBE();
        oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.VehicleType = new MASSIT_VehicleTypeBE();
        oAPPBOK_BookingBE.Delivery = new MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.Carrier = new MASCNT_CarrierBE();
        oAPPBOK_BookingBE.SlotTime = new SYS_SlotTimeBE();
        oAPPBOK_BookingBE.DoorNoSetup = new MASSIT_DoorNoSetupBE();
        oAPPBOK_BookingBE.FixedSlot = new MASSIT_FixedSlotBE();

        if (hdnCurrentMode.Value == "Add")
        {
            oAPPBOK_BookingBE.Action = "AddCarrierBooking";
            oAPPBOK_BookingBE.IsBookingAmended = false;
            if (hdnCurrentRole.Value == "Carrier")
                oAPPBOK_BookingBE.IsOnlineBooking = true;
            else
                oAPPBOK_BookingBE.IsOnlineBooking = false;
        }
        else if (hdnCurrentMode.Value == "Edit")
        {
            oAPPBOK_BookingBE.Action = "EditCarrierBooking";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
            oAPPBOK_BookingBE.BookingRef = Session["bref"].ToString();  // hdnBookingRef.Value;
            oAPPBOK_BookingBE.IsBookingAmended = true;

            if (Session["Role"].ToString().ToLower() != "carrier")
            {
                oAPPBOK_BookingBE.IsBookingAccepted = true;
            }

        }

        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.SupplierType = "C";
        oAPPBOK_BookingBE.Delivery.DeliveryTypeID = Convert.ToInt32(ddlDeliveryType.SelectedItem.Value);
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
        oAPPBOK_BookingBE.OtherCarrier = txtCarrierOther.Text.Trim();
        oAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
        oAPPBOK_BookingBE.OtherVehicle = txtVehicleOther.Text.Trim();
        if (IsPreAdviseNoteRequired == true)
            oAPPBOK_BookingBE.PreAdviseNotification = rdoAdvice1.Checked ? "Y" : "N";

        #region Booking type sttings ...
        if (ThisBookingType == BookingType.FixedSlot.ToString())
        {
            oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = hdnSuggestedSiteDoorNumber.Value;
            oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
            oAPPBOK_BookingBE.BookingTypeID = 1;    //Fixed Slot
            oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(hdnSuggestedFixedSlotID.Value);
        }
        else if (ThisBookingType == BookingType.Provisional.ToString())
        {
            oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = hdnSuggestedSiteDoorNumber.Value;
            oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
            oAPPBOK_BookingBE.BookingTypeID = 5;    //Provisional Booking
            oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(hdnSuggestedFixedSlotID.Value);
            //Stage 4  Point 5
            oAPPBOK_BookingBE.ProvisionalReason = ProvisionalReason;
            //-----------
            oAPPBOK_BookingBE.ProvisionalReasonType = ProvisionalReasonType;
        }
        else if (ThisBookingType == BookingType.NonTimedDelivery.ToString())
        {
            oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = String.Empty; //0;
            oAPPBOK_BookingBE.BookingStatusID = 1;   //Confirmed Slot
            oAPPBOK_BookingBE.BookingTypeID = 3;     //Non Timed Delivery            
        }
        else if (ThisBookingType == BookingType.BookedSlot.ToString())
        {
            oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
            oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = hdnSuggestedSiteDoorNumber.Value;
            oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
            oAPPBOK_BookingBE.BookingTypeID = 4;    //Booked Slot            
        }
        //----------Stage 9 Point 2--------------------//
        else if (ThisBookingType == BookingType.ProvisionalNonTimedDelivery.ToString())
        {
            oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = string.Empty; //0;
            oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
            oAPPBOK_BookingBE.BookingTypeID = 5;    //Provisional Booking
            oAPPBOK_BookingBE.ProvisionalReason = ProvisionalReason;
        }
        //--------------------------------------------//

        // Stage 15 R2 Point No 17

        if (hdnCurrentMode.Value == "Edit" && Session["Role"].ToString().ToLower() == "carrier")
        {
            if (hdnProvisionalReason.Value == "FIXED SLOT" && hdnBookingTypeID.Value == "5")
            {
                if (hdnIsBookingAccepted.Value == "true")
                {
                    oAPPBOK_BookingBE.ProvisionalReason = "EDITED";
                }
                else
                {
                    oAPPBOK_BookingBE.ProvisionalReason = "FIXED SLOT";
                }

            }
            else if (!string.IsNullOrEmpty(hdnSuggestedSlotTimeID.Value) && (ThisBookingType == BookingType.Provisional.ToString() ||
                     ThisBookingType == BookingType.ProvisionalNonTimedDelivery.ToString()))
            {
                oAPPBOK_BookingBE.ProvisionalReason = "EDITED";
            }
        }

        //---------------------------------------------//



        #endregion

        oAPPBOK_BookingBE.BookingComments = txtEditBookingCommentF.Text.Trim();
        oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
        oAPPBOK_BookingBE.WeekDay = getWeekdayNo();



        //----------Stage 6 Point 8----------------//
        bool BookingEditFlag = false;
        if (hdnCurrentMode.Value == "Edit")
            BookingEditFlag = IsBookingEdit(oAPPBOK_BookingBE);

        string VendorIDs = string.Empty;
        DataTable myDataTable = null;
        if (ViewState["myDataTable"] != null)
            myDataTable = (DataTable)ViewState["myDataTable"];

        string strPoIds = string.Empty;
        string strPoNos = string.Empty;

        if (hdnCurrentMode.Value == "Edit" && BookingEditFlag == false)
        {
            foreach (GridViewRow gvRow in gvVolume.Rows)
            {
                if (gvRow.RowType == DataControlRowType.DataRow)
                {
                    ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvRow.FindControl("txtVendorPallets");
                    ucNumericTextbox txtVendorCartons = (ucNumericTextbox)gvRow.FindControl("txtVendorCartons");
                    HiddenField hdngvVendorID = (HiddenField)gvRow.FindControl("hdngvVendorID");
                    ucLiteral ltVendorExpectedLines = (ucLiteral)gvRow.FindControl("ltVendorExpectedLines");

                    ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvRow.FindControl("txtLinesToBeDelivered");

                    strPoIds = string.Empty;
                    strPoNos = string.Empty;

                    if (myDataTable != null)
                    {
                        DataRow[] drr = myDataTable.Select("VendorID='" + hdngvVendorID.Value.ToString() + "'");
                        for (int jCount = 0; jCount < drr.Length; jCount++)
                        {
                            if (strPoIds == string.Empty)
                            {
                                strPoIds = drr[jCount]["PurchaseOrderID"].ToString();
                                strPoNos = drr[jCount]["PurchaseNumber"].ToString();
                            }
                            else
                            {
                                strPoIds = strPoIds + "," + drr[jCount]["PurchaseOrderID"].ToString();
                                strPoNos = strPoNos + "," + drr[jCount]["PurchaseNumber"].ToString();
                            }
                        }

                        if (hdngvVendorID.Value.ToString() != "-1")
                        {
                            drr = myDataTable.Select("VendorID='-1'");
                            for (int jCount = 0; jCount < drr.Length; jCount++)
                            {
                                if (strPoIds == string.Empty)
                                {
                                    strPoIds = drr[jCount]["PurchaseOrderID"].ToString();
                                    strPoNos = drr[jCount]["PurchaseNumber"].ToString();
                                }
                                else
                                {
                                    strPoIds = strPoIds + "," + drr[jCount]["PurchaseOrderID"].ToString();
                                    strPoNos = strPoNos + "," + drr[jCount]["PurchaseNumber"].ToString();
                                }
                            }
                        }
                    }

                    oAPPBOK_BookingBE.VendorID = Convert.ToInt32(hdngvVendorID.Value);
                    oAPPBOK_BookingBE.PurchaseOrdersIDs = strPoIds;
                    oAPPBOK_BookingBE.PurchaseOrders = strPoNos;

                    oAPPBOK_BookingBE.NumberOfCartons = txtVendorCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtVendorCartons.Text.Trim()) : 0;
                    oAPPBOK_BookingBE.NumberOfLift = 0;
                    oAPPBOK_BookingBE.NumberOfPallet = txtVendorPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtVendorPallets.Text.Trim()) : 0;
                    oAPPBOK_BookingBE.NumberOfLines = txtLinesToBeDelivered.Text.Trim() != string.Empty ? Convert.ToInt32(txtLinesToBeDelivered.Text.Trim()) : 0;

                    if (BookingEditFlag == false)
                        BookingEditFlag = IsBookingEdit(oAPPBOK_BookingBE, true);
                }
            }
        }
        //------------------------------------------//

        oAPPBOK_BookingBE.IsEnableHazardouesItemPrompt = checkBoxEnableHazardouesItemPrompt.Checked;
        oAPPBOK_BookingBE.ISPM15CountryPalletChecking = Convert.ToBoolean(chkSiteSetting.Checked);
        oAPPBOK_BookingBE.ISPM15FromCountryID = !string.IsNullOrEmpty(ddlSourceCountry.innerControlddlCountry.SelectedValue) ? Convert.ToInt32(ddlSourceCountry.innerControlddlCountry.SelectedValue) : 0;

        int? BookingID = oAPPBOK_BookingBAL.AddCarrierBookingBAL(oAPPBOK_BookingBE);
       
        strBookingID = Convert.ToString(BookingID);

        oAPPBOK_BookingBE.Action = "AddCarrierBookingDetails";
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(BookingID);

        VendorIDs = string.Empty;
        myDataTable = null;
        if (ViewState["myDataTable"] != null)
            myDataTable = (DataTable)ViewState["myDataTable"];

        foreach (GridViewRow gvRow in gvVolume.Rows)
        {
            if (gvRow.RowType == DataControlRowType.DataRow)
            {
                ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvRow.FindControl("txtVendorPallets");
                ucNumericTextbox txtVendorCartons = (ucNumericTextbox)gvRow.FindControl("txtVendorCartons");
                HiddenField hdngvVendorID = (HiddenField)gvRow.FindControl("hdngvVendorID");
                ucLiteral ltVendorExpectedLines = (ucLiteral)gvRow.FindControl("ltVendorExpectedLines");

                ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvRow.FindControl("txtLinesToBeDelivered");

                strPoIds = string.Empty;
                strPoNos = string.Empty;

                if (myDataTable != null)
                {
                    DataRow[] drr = myDataTable.Select("VendorID='" + hdngvVendorID.Value.ToString() + "'");
                    for (int jCount = 0; jCount < drr.Length; jCount++)
                    {
                        if (strPoIds == string.Empty)
                        {
                            strPoIds = drr[jCount]["PurchaseOrderID"].ToString();
                            strPoNos = drr[jCount]["PurchaseNumber"].ToString();
                        }
                        else
                        {
                            strPoIds = strPoIds + "," + drr[jCount]["PurchaseOrderID"].ToString();
                            strPoNos = strPoNos + "," + drr[jCount]["PurchaseNumber"].ToString();
                        }
                    }

                    if (hdngvVendorID.Value.ToString() != "-1")
                    {
                        drr = myDataTable.Select("VendorID='-1'");
                        for (int jCount = 0; jCount < drr.Length; jCount++)
                        {
                            if (strPoIds == string.Empty)
                            {
                                strPoIds = drr[jCount]["PurchaseOrderID"].ToString();
                                strPoNos = drr[jCount]["PurchaseNumber"].ToString();
                            }
                            else
                            {
                                strPoIds = strPoIds + "," + drr[jCount]["PurchaseOrderID"].ToString();
                                strPoNos = strPoNos + "," + drr[jCount]["PurchaseNumber"].ToString();
                            }
                        }
                    }
                }

                oAPPBOK_BookingBE.VendorID = Convert.ToInt32(hdngvVendorID.Value);
                oAPPBOK_BookingBE.PurchaseOrdersIDs = strPoIds;
                oAPPBOK_BookingBE.PurchaseOrders = strPoNos;

                oAPPBOK_BookingBE.NumberOfCartons = txtVendorCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtVendorCartons.Text.Trim()) : (int?)null;
                oAPPBOK_BookingBE.NumberOfLift = (int?)null;
                oAPPBOK_BookingBE.NumberOfPallet = txtVendorPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtVendorPallets.Text.Trim()) : (int?)null;
                oAPPBOK_BookingBE.NumberOfLines = txtLinesToBeDelivered.Text.Trim() != string.Empty ? Convert.ToInt32(txtLinesToBeDelivered.Text.Trim()) : (int?)null;

                oAPPBOK_BookingBAL.AddCarrierBookingDetailsBAL(oAPPBOK_BookingBE);
            }
        }
        #endregion

        #region Commented Code ...
        //if (ViewState["dtVolumeDetails"] != null) {
        //    dtVolumeDetails = (DataTable)ViewState["dtVolumeDetails"];
        //    if (dtVolumeDetails != null) {
        //        for (int iCount = 0; iCount < dtVolumeDetails.Rows.Count; iCount++) {
        //            //if (VendorIDs == string.Empty)
        //            //    VendorIDs = dtVolumeDetails.Rows[iCount]["VendorID"].ToString();
        //            //else
        //            //    VendorIDs = VendorIDs + "," + dtVolumeDetails.Rows[iCount]["VendorID"].ToString();
        //            strPo = string.Empty;
        //            if (myDataTable != null) {
        //                DataRow[] drr = myDataTable.Select("VendorID='" + dtVolumeDetails.Rows[iCount]["VendorID"].ToString() + "'");
        //                for (int jCount = 0; jCount < drr.Length; jCount++) {
        //                    if (strPo == string.Empty)
        //                        strPo = drr[jCount]["PurchaseNumber"].ToString();
        //                    else
        //                        strPo = strPo + "," + drr[jCount]["PurchaseNumber"].ToString();
        //                }
        //            }
        //        }
        //    }
        //}
        #endregion

        // Phase 15 R2-Overdue Booking Report
        oAPPBOK_BookingBE.Action = "OverdueBookingReportDetail";
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(BookingID);
        oAPPBOK_BookingBAL.OverdueBookingReportDetailBAL(oAPPBOK_BookingBE);


        Session["BookingCreated"] = true;
        Session["CreatedBookingID"] = strBookingID;
        Session["IsBookingAmended"] = BookingEditFlag.ToString();

        //----------Start Stage 9 point 2a/2b------------//
        if (ThisBookingType == BookingType.ProvisionalNonTimedDelivery.ToString())
            EncryptQueryString("APPBok_Confirm.aspx?BookingType=PNTD&MailSend=True&BookingID=" + BookingID + "&IsBookingAmended=" + BookingEditFlag.ToString());
        else
            EncryptQueryString("APPBok_Confirm.aspx?MailSend=True&BookingID=" + BookingID + "&IsBookingAmended=" + BookingEditFlag.ToString());
        //----------End Stage 9 point 2a/2b------------//


    }

    private bool IsBookingEdit(APPBOK_BookingBE APPBOK_BookingBE, bool bVolumeDetails = false)
    {

        bool bBookingEdit = false;

        try
        {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
            List<APPBOK_BookingBE> lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);

            if (lstBooking.Count > 0)
            {

                if (bVolumeDetails)
                {

                    List<APPBOK_BookingBE> lstVendorBooking = lstBooking.FindAll(delegate(APPBOK_BookingBE St)
                    {
                        return St.VendorID == APPBOK_BookingBE.VendorID;
                    });

                    if (lstVendorBooking != null && lstVendorBooking.Count > 0)
                    {
                        int iTotalPalllets = 0;
                        int iTotalLines = 0;
                        int iTotalCartons = 0;
                        int iTotalLifts = 0;

                        for (int iCount = 0; iCount < lstVendorBooking.Count; iCount++)
                        {
                            iTotalPalllets += Convert.ToInt32(lstVendorBooking[iCount].NumberOfPallet);
                            iTotalLines += Convert.ToInt32(lstVendorBooking[iCount].NumberOfLines);
                            iTotalCartons += Convert.ToInt32(lstVendorBooking[iCount].NumberOfCartons);
                            iTotalLifts += Convert.ToInt32(lstVendorBooking[iCount].NumberOfLift);
                        }

                        if (iTotalLines != APPBOK_BookingBE.NumberOfLines)
                            bBookingEdit = true;
                        else if (iTotalPalllets != APPBOK_BookingBE.NumberOfPallet)
                            bBookingEdit = true;
                        else if (iTotalCartons != APPBOK_BookingBE.NumberOfCartons)
                            bBookingEdit = true;
                        else if (iTotalLifts != APPBOK_BookingBE.NumberOfLift)
                            bBookingEdit = true;
                        else if (!IsPoAreSame(lstVendorBooking[0].PurchaseOrders, APPBOK_BookingBE.PurchaseOrders)) //to do
                            bBookingEdit = true;
                        else if (lstVendorBooking[0].VendorID != APPBOK_BookingBE.VendorID)
                            bBookingEdit = true;
                        else if (lstBooking[0].BookingComments != APPBOK_BookingBE.BookingComments)
                            bBookingEdit = true;
                    }
                    else
                    {
                        bBookingEdit = true;
                    }
                }

                if (bBookingEdit == false && bVolumeDetails == false)
                {
                    if (lstBooking[0].SlotTime.SlotTimeID != APPBOK_BookingBE.SlotTime.SlotTimeID)
                        bBookingEdit = true;
                    else if (lstBooking[0].FixedSlot.FixedSlotID != APPBOK_BookingBE.FixedSlot.FixedSlotID)
                        bBookingEdit = true;
                    else if (lstBooking[0].DoorNoSetup.SiteDoorNumberID != APPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID)
                        bBookingEdit = true;
                    else if (lstBooking[0].Carrier.CarrierID != APPBOK_BookingBE.Carrier.CarrierID)
                        bBookingEdit = true;
                    else if (lstBooking[0].Delivery.DeliveryTypeID != APPBOK_BookingBE.Delivery.DeliveryTypeID)
                        bBookingEdit = true;
                    else if (lstBooking[0].VehicleType.VehicleTypeID != APPBOK_BookingBE.VehicleType.VehicleTypeID)
                        bBookingEdit = true;
                    else if (lstBooking[0].ScheduleDate != APPBOK_BookingBE.ScheduleDate)
                        bBookingEdit = true;
                    else if (lstBooking[0].BookingComments != APPBOK_BookingBE.BookingComments)
                        bBookingEdit = true;
                }
            }
        }
        catch
        {
            bBookingEdit = true;
        }
        return bBookingEdit;
    }

    private bool IsPoAreSame(string OldPos, string NewPos)
    {

        var firstOrdered = OldPos.Split(',')
                              .Select(t => t.Trim())
                              .Where(t => t != string.Empty)
                              .OrderBy(t => t);
        var secondOrdered = NewPos.Split(',')
                                        .Select(t => t.Trim())
                                        .Where(t => t != string.Empty)
                                        .OrderBy(t => t);
        //bool stringsAreEqual = firstOrdered.SequenceEqual(secondOrdered);
        bool stringsAreEqual = secondOrdered.All(x => firstOrdered.Contains(x));

        return stringsAreEqual;
    }

    private string MakeMultiVendorBooking(string ThisBookingType)
    {
        string strBookingID = string.Empty;
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        // Here looping on [gvVolume] grid because it's a distinct for Vendors.
        string strPoIds = string.Empty;
        string strPoNos = string.Empty;
        for (int index = 0; index < gvVolume.Rows.Count; index++)
        {
            #region Initializations ...
            oAPPBOK_BookingBE = new APPBOK_BookingBE();
            oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.VehicleType = new MASSIT_VehicleTypeBE();
            oAPPBOK_BookingBE.Delivery = new MASCNT_DeliveryTypeBE();
            oAPPBOK_BookingBE.Carrier = new MASCNT_CarrierBE();
            oAPPBOK_BookingBE.SlotTime = new SYS_SlotTimeBE();
            oAPPBOK_BookingBE.DoorNoSetup = new MASSIT_DoorNoSetupBE();
            oAPPBOK_BookingBE.FixedSlot = new MASSIT_FixedSlotBE();
            #endregion

            oAPPBOK_BookingBE.Action = "AddCarrierBooking";
            oAPPBOK_BookingBE.IsBookingAmended = false;
            if (hdnCurrentRole.Value == "Carrier")
                oAPPBOK_BookingBE.IsOnlineBooking = true;
            else
                oAPPBOK_BookingBE.IsOnlineBooking = false;

            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.SupplierType = "C";
            oAPPBOK_BookingBE.Delivery.DeliveryTypeID = Convert.ToInt32(ddlDeliveryType.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
            oAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
            oAPPBOK_BookingBE.OtherCarrier = txtCarrierOther.Text.Trim();
            oAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
            oAPPBOK_BookingBE.OtherVehicle = txtVehicleOther.Text.Trim();
            if (IsPreAdviseNoteRequired == true)
                oAPPBOK_BookingBE.PreAdviseNotification = rdoAdvice1.Checked ? "Y" : "N";

            if (index == 0)
                oAPPBOK_BookingBE.IsMultiVendCarrier = "N";
            else
                oAPPBOK_BookingBE.IsMultiVendCarrier = "Y";

            #region Booking type sttings ...
            if (ThisBookingType == BookingType.FixedSlot.ToString())
            {
                oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
                oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
                oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = hdnSuggestedSiteDoorNumber.Value;
                oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
                oAPPBOK_BookingBE.BookingTypeID = 1;    //Fixed Slot
                oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(hdnSuggestedFixedSlotID.Value);
            }
            else if (ThisBookingType == BookingType.Provisional.ToString())
            {
                oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
                oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
                oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = hdnSuggestedSiteDoorNumber.Value;
                oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
                oAPPBOK_BookingBE.BookingTypeID = 5;    //Provisional Booking
                oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(hdnSuggestedFixedSlotID.Value);
            }
            else if (ThisBookingType == BookingType.NonTimedDelivery.ToString())
            {
                oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = String.Empty; //0;
                oAPPBOK_BookingBE.BookingStatusID = 1;   //Confirmed Slot
                oAPPBOK_BookingBE.BookingTypeID = 3;     //Non Timed Delivery            
            }
            else if (ThisBookingType == BookingType.BookedSlot.ToString())
            {
                oAPPBOK_BookingBE.SlotTime.SlotTimeID = Convert.ToInt32(hdnSuggestedSlotTimeID.Value);
                oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value);
                oAPPBOK_BookingBE.DoorNoSetup.DoorNumber = hdnSuggestedSiteDoorNumber.Value;
                oAPPBOK_BookingBE.BookingStatusID = 1;  //Confirmed Slot
                oAPPBOK_BookingBE.BookingTypeID = 4;    //Booked Slot            
            }
            #endregion

            oAPPBOK_BookingBE.BookingComments = txtEditBookingCommentF.Text.Trim();
            oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
            oAPPBOK_BookingBE.WeekDay = getWeekdayNo();
            int? BookingID = oAPPBOK_BookingBAL.AddCarrierBookingBAL(oAPPBOK_BookingBE);
            if (BookingID != null)
            {
                if (index == 0)
                    strBookingID = Convert.ToString(BookingID);
                else
                    strBookingID = string.Format("{0},{1}", strBookingID, Convert.ToString(BookingID));
            }

            oAPPBOK_BookingBE.Action = "AddCarrierBookingDetails";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(BookingID);

            string VendorIDs = string.Empty;
            DataTable myDataTable = null;
            if (ViewState["myDataTable"] != null)
                myDataTable = (DataTable)ViewState["myDataTable"];

            ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvVolume.Rows[index].FindControl("txtVendorPallets");
            ucNumericTextbox txtVendorCartons = (ucNumericTextbox)gvVolume.Rows[index].FindControl("txtVendorCartons");
            HiddenField hdngvVendorID = (HiddenField)gvVolume.Rows[index].FindControl("hdngvVendorID");
            ucLiteral ltVendorExpectedLines = (ucLiteral)gvVolume.Rows[index].FindControl("ltVendorExpectedLines");
            ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvVolume.Rows[index].FindControl("txtLinesToBeDelivered");
            HiddenField hdnPOVendorId = (HiddenField)gvVolume.Rows[index].FindControl("hdnPOVendorId");

            strPoIds = string.Empty;
            strPoNos = string.Empty;

            if (myDataTable != null)
            {
                DataRow[] drr = myDataTable.Select("VendorID='" + hdnPOVendorId.Value.ToString() + "'");
                for (int jCount = 0; jCount < drr.Length; jCount++)
                {
                    if (strPoIds == string.Empty)
                    {
                        strPoIds = drr[jCount]["PurchaseOrderID"].ToString();
                        strPoNos = drr[jCount]["PurchaseNumber"].ToString();
                    }
                    else
                    {
                        strPoIds = strPoIds + "," + drr[jCount]["PurchaseOrderID"].ToString();
                        strPoNos = strPoNos + "," + drr[jCount]["PurchaseNumber"].ToString();
                    }
                }
            }

            oAPPBOK_BookingBE.VendorID = Convert.ToInt32(hdnPOVendorId.Value);
            oAPPBOK_BookingBE.PurchaseOrdersIDs = strPoIds;
            oAPPBOK_BookingBE.PurchaseOrders = strPoNos;

            oAPPBOK_BookingBE.NumberOfCartons = txtVendorCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtVendorCartons.Text.Trim()) : (int?)null;
            oAPPBOK_BookingBE.NumberOfLift = (int?)null;
            oAPPBOK_BookingBE.NumberOfPallet = txtVendorPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtVendorPallets.Text.Trim()) : (int?)null;
            oAPPBOK_BookingBE.NumberOfLines = txtLinesToBeDelivered.Text.Trim() != string.Empty ? Convert.ToInt32(txtLinesToBeDelivered.Text.Trim()) : (int?)null;
            oAPPBOK_BookingBAL.AddCarrierBookingDetailsBAL(oAPPBOK_BookingBE);
        }

        return strBookingID;
    }

    private int getWeekdayNo()
    {
        string Weekday = string.Empty;

        if (hdnSuggestedWeekDay.Value.Trim() != string.Empty)
            Weekday = hdnSuggestedWeekDay.Value.ToLower();
        else
            Weekday = LogicalSchedulDateTime.DayOfWeek.ToString().ToLower().Substring(0, 3);

        int iWeekNo = 0;
        switch (Weekday)
        {
            case ("sun"):
                iWeekNo = 1;
                break;
            case ("mon"):
                iWeekNo = 2;
                break;
            case ("tue"):
                iWeekNo = 3;
                break;
            case ("wed"):
                iWeekNo = 4;
                break;
            case ("thu"):
                iWeekNo = 5;
                break;
            case ("fri"):
                iWeekNo = 6;
                break;
            case ("sat"):
                iWeekNo = 7;
                break;
            default:
                iWeekNo = 0;
                break;
        }
        return iWeekNo;
    }

    private string getWeekday(int iWeekNo)
    {
        string Weekday = string.Empty;

        switch (iWeekNo)
        {
            case (1):
                Weekday = "sun";
                break;
            case (2):
                Weekday = "mon";
                break;
            case (3):
                Weekday = "tue";
                break;
            case (4):
                Weekday = "wed";
                break;
            case (5):
                Weekday = "thu";
                break;
            case (6):
                Weekday = "fri";
                break;
            case (7):
                Weekday = "sat";
                break;
            default:
                Weekday = string.Empty;
                break;
        }
        return Weekday;
    }

    protected void btnMakeNonTimeBooking_Click(object sender, EventArgs e)
    {
        string errMsg = string.Empty;

        if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
        {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_24_EXT_1") + " " + ActualSchedulDateTime.ToString("dd/MM/yyyy") + " " + WebCommon.getGlobalResourceValue("BK_MS_24_EXT_2");  // "This booking is to be made on " + txtSchedulingdate.GetDate.ToString() + " with no fixed time. Please ensure you make the delivery within the site's receiving hours. ";
            ShowErrorMessage(errMsg, "BK_MS_24_EXT");
            return;
        }
        else
        {
            MakeBooking(BookingType.NonTimedDelivery.ToString());
            //Commented As per Stage 6 V2 Point 3 (15)
            //errMsg = WebCommon.getGlobalResourceValue("BK_MS_24_INT");  // "This booking is to be made without a delivery time. Click 'CONTINUE' to confirm or 'BACK' to add a time to the booking";
            //AddWarningMessages(errMsg, "BK_MS_24_INT");
            //ShowWarningMessages();
        }
    }

    protected void btnAlternateSlot_Click(object sender, EventArgs e)
    {
        //string errMsg = string.Empty;

        //if (btnConfirmBooking.Visible == true) {  //(hdnSuggestedFixedSlotID.Value != string.Empty && hdnSuggestedFixedSlotID.Value != "-1") {  // 
        //    errMsg = WebCommon.getGlobalResourceValue("BK_MS_22_INT");// "There is a slot available for this delivery? Are you sure want to search for an alternative slot?";
        //    ltWarningConfirm.Text = errMsg;
        //    mdlWarningConfirm.Show();
        //}
        //else {
        //    PaintAlternateSlotScreen();
        //}

        PaintAlternateSlotScreen();
        GenerateVolumeDetails();
    }

    protected void btnErrWarningConfirm_Click(object sender, EventArgs e)
    {
        // Sprint 1 - Point 18 - Start - Carrier lines to be editable
        if (ViewState["WarningStatus"] == null)
            ViewState["WarningStatus"] = -1;

        this.ErrWarningConfirmation(Convert.ToInt32(ViewState["WarningStatus"]));
        ViewState["WarningStatus"] = -1;
        // lblWarningText.Visible = false;
        // Sprint 1 - Point 18 - End - Carrier lines to be editable
    }

    // Sprint 1 - Point 18 - Start - Carrier lines to be editable
    private void ErrWarningConfirmation(int intStatus)
    {
        switch (intStatus)
        {
            case 0:
                {
                    int iTotalPallets = 0;
                    int iTotalCartons = 0;
                    int iTotalLines = 0;
                    bool isVendorVolumeNotValid = false;
                    foreach (GridViewRow gvRow in gvVolume.Rows)
                    {
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {
                            ucNumericTextbox txtVendorPallets = (ucNumericTextbox)gvRow.FindControl("txtVendorPallets");
                            ucNumericTextbox txtVendorCartons = (ucNumericTextbox)gvRow.FindControl("txtVendorCartons");
                            ucNumericTextbox txtLinesToBeDelivered = (ucNumericTextbox)gvRow.FindControl("txtLinesToBeDelivered");

                            string sTotalPallets = txtVendorPallets.Text.Trim() != string.Empty ? txtVendorPallets.Text.Trim() : "0";
                            string sTotalCartons = txtVendorCartons.Text.Trim() != string.Empty ? txtVendorCartons.Text.Trim() : "0";
                            string sTotalLines = txtLinesToBeDelivered.Text.Trim() != string.Empty ? txtLinesToBeDelivered.Text.Trim() : "0";

                            iTotalPallets += Convert.ToInt32(sTotalPallets);
                            iTotalCartons += Convert.ToInt32(sTotalCartons);
                            iTotalLines += Convert.ToInt32(sTotalLines);

                            if ((Convert.ToInt32(TotalLines) == 0) || (Convert.ToInt32(sTotalPallets) == 0 && Convert.ToInt32(sTotalCartons) == 0))
                            {
                                return;
                            }

                            //---Stage 10 Point 10L 5.2----//     
                            if (ddlSite.IsBookingExclusions)
                            {
                                HiddenField hdnPOVendorId = (HiddenField)gvRow.FindControl("hdnPOVendorId");
                                if (isEnteredVendorVolumeNotValid(Convert.ToInt32(hdnPOVendorId.Value), Convert.ToInt32(sTotalPallets), Convert.ToInt32(sTotalLines)))
                                    isVendorVolumeNotValid = true;
                            }
                            //----------------------------------------//
                        }
                    }
                    //---Stage 10 Point 10L 5.2----//                   
                    if (isVendorVolumeNotValid)
                        ShowWarningMessages();
                    else
                        this.ProceedBooking(iTotalCartons, iTotalPallets, iTotalLines);
                    //----------------------------------------//

                    break;
                }
            case 1:
                {
                    this.AddPO();
                    break;
                }
            default:
                {
                    if (hdnCurrentRole.Value == "Carrier")
                    {
                        GenerateAlternateSlotForVendor();
                        PaintAlternateScreenForVendor();
                        mvBookingEdit.ActiveViewIndex = 4;
                    }
                    else
                    {
                        PaintAlternateSlotScreen();
                    }
                    break;
                }
        }
    }
    // Sprint 1 - Point 18 - End - Carrier lines to be editable

    protected void btnErrWarningBack_Click(object sender, EventArgs e)
    {
        //lblWarningText.Visible = false;
        mdlWarningConfirm.Hide();
    }

    private void PaintAlternateSlotScreen()
    {

        ltCarrier_2.Text = ddlCarrier.SelectedItem.Text;
        ltOtherCarrier_2.Text = "[" + txtCarrierOther.Text.Trim() + "]";
        if (txtCarrierOther.Text.Trim() != string.Empty)
        {
            ltOtherCarrier_2.Visible = true;
        }
        else
        {
            ltOtherCarrier_2.Visible = false;
        }

        ltVehicleType_2.Text = ddlVehicleType.SelectedItem.Text;
        ltOtherVehicleType_2.Text = "[" + txtVehicleOther.Text.Trim() + "]";
        if (txtVehicleOther.Text.Trim() != string.Empty)
        {
            ltOtherVehicleType_2.Visible = true;
        }
        else
        {
            ltOtherVehicleType_2.Visible = false;
        }
       // ltSchedulingDate_2.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
        ltTimeSuggested_2.Text = ltSuggestedSlotTime.Text;

        ltlStartTime.Text = ltTimeSlot.Text;
        ltlSuggestedDoor.Text = ltDoorNo.Text;

        if (btnConfirmBooking.Visible)
        {
            iPallets = Convert.ToInt32(TotalPallets);
            iCartons = Convert.ToInt32(TotalCartons);

            int SlotLen = GetTimeSlotLength();

            string[] StartTime = ltTimeSuggested_2.Text.Split(':');
            if (StartTime.Length == 2)
            {
                TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1].Substring(0, 2)), 00);
                TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
                //TimeSpan totalTime = new TimeSpan(00, 00, 00); ;
                for (int iLen = 1; iLen <= SlotLen; iLen++)
                {
                    if (time1.Hours == 23 && time1.Minutes == 45)
                    {
                        time1 = new TimeSpan(00, 00, 00);
                    }
                    else
                    {
                        time1 = time1.Add(time2);
                    }
                }

                ltlEstEnd_2.Text = time1.Hours.ToString("00") + ":" + time1.Minutes.ToString("00");
            }
        }

        ltNumberofPallets.Text = TotalPallets.Trim() != string.Empty ? TotalPallets.Trim() : "-";
        ltNumberofLines.Text = TotalLines.Trim() != string.Empty ? TotalLines.Trim() : "-";
        ltNumberofCartons.Text = TotalCartons.Trim() != string.Empty ? TotalCartons.Trim() : "-";

        GenerateNonTimeBooking();
        GenerateAlternateSlot();
        mvBookingEdit.ActiveViewIndex = 3;


    }

    protected void btnAlternateSlotCarrier_Click(object sender, EventArgs e)
    {
        //string errMsg = string.Empty;

        //if (btnConfirmBooking.Visible == true) {
        //    if (hdnSuggestedFixedSlotID.Value != string.Empty && hdnSuggestedFixedSlotID.Value != "-1") {
        //        errMsg = "You have a fixed slot on this day  which you have chosen not to use. If this Fixed Slot is not required then please cancel this slot or it will adversely affect your vendor evaluation.";
        //    }
        //    else {
        //        errMsg = "There is a slot available for this delivery? Are you sure want to search for an alternative slot?";
        //    }                    
        //    ltWarningConfirm.Text = errMsg;
        //    mdlWarningConfirm.Show();
        //}
        //else {
        //    GenerateAlternateSlotForVendor();
        //    PaintAlternateScreenForVendor();
        //    mvBookingEdit.ActiveViewIndex = 4;
        //}

        GenerateAlternateSlotForVendor();
        PaintAlternateScreenForVendor();
        mvBookingEdit.ActiveViewIndex = 4;
    }

    private void AddWarningMessages(string ErrMessage, string CommandName)
    {
        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }
        if (ErrorMessages.Get(CommandName) == null)
        {
            ErrorMessages.Add(CommandName, ErrMessage);
            ViewState["ErrorMessages"] = ErrorMessages;
        }
    }

    private void ShowWarningMessages()
    {
        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }
        if (ErrorMessages.Keys.Count > 0)
        {
            string errMsg = ErrorMessages.Get(0);
            string CommandName = ErrorMessages.GetKey(0);
            ltConfirmMsg.Text = errMsg;
            btnErrContinue.CommandName = CommandName;
            btnErrBack.CommandName = CommandName;
            mdlConfirmMsg.Show();
            ErrorMessages.Remove(CommandName);
            ViewState["ErrorMessages"] = ErrorMessages;
        }
    }

    //---- Stage 10 point 10L 5.2---//
    private void ShowOtherWarningMsgForVandorVolume()
    {
        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }
        if (ErrorMessages.Keys.Count > 0)
        {

            for (int iKeyCount = 0; iKeyCount < ErrorMessages.Keys.Count; iKeyCount++)
            {
                if (ErrorMessages.GetKey(iKeyCount).Contains("BK_MS_18_INT_VEN_"))
                {
                    //ShowWarningMessages();
                    return;
                }
            }
        }
        this.ProceedBooking(iTotalCartonsglobal, iTotalPalletsglobal, iTotalLinesglobal);
    }
    //----------------------//

    private void ShowErrorMessage(string ErrMessage, string CommandName)
    {
        ltErrorMsg.Text = ErrMessage;
        btnErrorMsgOK.CommandName = CommandName;
        mdlErrorMsg.Show();
    }

    protected void btnContinue_Click(object sender, CommandEventArgs e)
    {

        ShowWarningMessages();

        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null)
        {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }
        if (ErrorMessages.Keys.Count == 0)
        {
            if (e.CommandName == "BK_MS_05_INT" || e.CommandName == "BK_MS_04_INT_Carrier" || e.CommandName == "BK_MS_06_INT" || e.CommandName == "BK_MS_27_INT")
            {
                mvBookingEdit.ActiveViewIndex = 0;
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedAddPO.ClientID + "').style.display='';", true);
            }
            else if (e.CommandName == "BK_MS_11_INT" || e.CommandName == "BK_MS_12_INT" || e.CommandName == "BK_MS_13_INT" || e.CommandName == "BK_MS_14_INT")
            {
                ViewState["ByContinue"] = "ByContinue";
                BindPOGrid();
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            }
            else if (e.CommandName == "BK_MS_22_INT")
            {
                PaintAlternateSlotScreen();
            }
            else if (e.CommandName == "BK_MS_22_EXT")
            {
                mvBookingEdit.ActiveViewIndex = 4;
                GenerateAlternateSlotForVendor();
                PaintAlternateScreenForVendor();

            }
            else if (e.CommandName == "BK_MS_24_INT")
            {
                MakeBooking(BookingType.NonTimedDelivery.ToString());
            }
            //else if (e.CommandName == "BK_MS_26_EXT") {
            //    CalculateProposedBookingTimeSlot();
            //    mvBookingEdit.ActiveViewIndex = 3;
            //}
            else if (e.CommandName == "BK_MS_23_EXT")
            {
                hdnCapacityWarning.Value = "false";
                isProvisional = true;
                ProvisionalReasonType = 8;
                ProvisionalReason = "FIXED SLOT";
                MakeBooking(BookingType.Provisional.ToString());   // For Vendor
            }
            else if (e.CommandName == "BK_MS_23_EXT_1")
            {
                hdnCapacityWarning.Value = "false";
                isProvisional = true;
                ProvisionalReasonType = 13;
                ProvisionalReason = "EDITED";
                MakeBooking(BookingType.Provisional.ToString());   // For Vendor
            }
            else if (e.CommandName == "BK_MS_23_INT_1")
            {
                hdnCapacityWarning.Value = "false";
                MakeBooking(BookingType.BookedSlot.ToString());// For OD Staff               
            }
            else if (e.CommandName == "BK_MS_23_INT")
            {
                hdnCapacityWarning.Value = "false";
                MakeBooking(BookingType.FixedSlot.ToString());// For OD Staff               
            }
            else if (e.CommandName == "BK_MS_18_INT")
            {
                mvBookingEdit.ActiveViewIndex = 2;
                if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                {
                    isProvisional = true;
                    ProvisionalReasonType = 10;
                    if (hdnCurrentMode.Value == "Add")
                    {
                        ProvisionalReason = "DAILY CAPACITY";
                    }
                    else
                    {
                        ProvisionalReason = "EDITED";
                    }
                }
                CalculateProposedBookingTimeSlot();
            }
            else if (e.CommandName == "BK_MS_35")
            {
                //ValidateEnteredVolume();  
                SuggestBooking(intTotalCartons, intTotalPallets, intTotalLines);
            }
            else if (e.CommandName == "Booking")
            { //Stage 9 point 3
                DeleteBooking();
            }
            else if (e.CommandName.Contains("BK_MS_18_INT_VEN_"))
            {
                ShowOtherWarningMsgForVandorVolume();
            }
            else if (e.CommandName == "BK_MS_04_INT")
            {
                this.AddPO(true);
            }
            //----Stage 10 Point 10 L 5.1-----//
            else if (e.CommandName == "BK_MS_06_INT_MaxContainers_Proceed")
            {
                mvBookingEdit.ActiveViewIndex = 2;
                CalculateProposedBookingTimeSlot();
            }
            else if (e.CommandName == "BK_MS_06_INT_MaxContainers")
            {
                VehicleTypeSelect();
            }
            //---------------------------------//
        }

        //MsgSupressed.Add(e.CommandName, "true");

    }

    protected void btnBack_Click(object sender, CommandEventArgs e)
    {

        if (e.CommandName == "BK_MS_05_INT" || e.CommandName == "BK_MS_04_INT_Carrier" || e.CommandName == "BK_MS_06_INT" || e.CommandName == "BK_MS_27_INT"
            || e.CommandName == "BK_MS_06_INT_MaxContainers")
        {
            txtSchedulingdate.innerControltxtDate.Value = string.Empty;
            mvBookingEdit.ActiveViewIndex = 0;
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedAddPO.ClientID + "').style.display='none';", true);
        }
        else if (e.CommandName == "BK_MS_11_INT" || e.CommandName == "BK_MS_12_INT" || e.CommandName == "BK_MS_13_INT" || e.CommandName == "BK_MS_14_INT")
        {
            txtPurchaseNumber.Text = string.Empty;
            DataTable myDataTable = null;

            if (ViewState["myDataTable"] != null)
                myDataTable = (DataTable)ViewState["myDataTable"];
            else
                myDataTable = new DataTable();

            if (myDataTable.Rows.Count <= 0)
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);

        }
        else if (e.CommandName == "BK_MS_23_EXT" || (e.CommandName == "BK_MS_23_INT"))
        {
            BindFixedSlot();
            if (mvBookingEdit.ActiveViewIndex == 3) // For OD Staff
                GenerateAlternateSlot();
            else if (mvBookingEdit.ActiveViewIndex == 4) // For Vendor
                GenerateAlternateSlotForVendor();
        }
        else if (e.CommandName == "Booking")
        {  //Stage 9 Point2a/2b/3
            // tableDiv_General.Controls.RemoveAt(0);
            GenerateAlternateSlot();
        }

        ViewState["ErrorMessages"] = null;

        //if (e.CommandName == "BK_MS_2") {
        //    txtSchedulingdate.innerControltxtDate.Value = string.Empty;            
        //    ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + ucProceedAddPO.ClientID + "').style.display='none';", true);
        //}
        //else if (e.CommandName == "BK_MS_04_INT") {
        //    txtSchedulingdate.innerControltxtDate.Value = string.Empty;
        //    ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + ucProceedAddPO.ClientID + "').style.display='none';", true);
        //}
    }

    protected void btnErrorMsgOK_Click(object sender, CommandEventArgs e)
    {

        //ScriptManager.RegisterStartupScript(this, this.GetType(), "BK_MS_0", "<script>alert('" + errMsg + "');</script>", false);

        if (e.CommandName == "BK_MS_04_EXT" || e.CommandName == "BK_MS_06_EXT" || e.CommandName == "BK_MS_36" || e.CommandName == "BK_MS_27_EXT")
        {
            txtSchedulingdate.innerControltxtDate.Value = string.Empty;
        }
        else if (e.CommandName == "BK_MS_05_EXT")
        {
            txtSchedulingdate.innerControltxtDate.Value = string.Empty;
            mvBookingEdit.ActiveViewIndex = 0;
            //EncryptQueryString("APPBok_BookingOverview.aspx?ModuleName=App. Scheduling");
            return;
        }
        else if (e.CommandName == "BK_MS_11_EXT" || e.CommandName == "BK_MS_12_EXT" || e.CommandName == "BK_MS_13_EXT" || e.CommandName == "BK_MS_14_EXT" || e.CommandName == "BK_MS_04_EXT_Carrier")
        {
            txtPurchaseNumber.Text = string.Empty;
        }
        else if (e.CommandName == "BK_MS_17")
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
        }
        else if (e.CommandName == "BK_MS_28")
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedAddPO.ClientID + "').style.display='none';", true);
        }
        else if (e.CommandName == "BK_MS_19")
        {
            //TotalPallets = string.Empty;
            //TotalCartons = string.Empty;
            //TotalLines = string.Empty;
        }
        else if (e.CommandName == "BK_MS_24_EXT")
        {
            MakeBooking(BookingType.NonTimedDelivery.ToString());
        }
        else if (e.CommandName == "BK_MS_20")
        {
            hdnbutton_Click(null, null);
        }
        else if (e.CommandName == "BK_MS_06_Ext_MaxContainers")
        {
            txtSchedulingdate.innerControltxtDate.Value = string.Empty;
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedAddPO.ClientID + "').style.display='none';", true);
        }
    }

    List<APPBOK_BookingBE> lstAllFixedDoor = new List<APPBOK_BookingBE>();
    int iTimeSlotID, iSiteDoorID, iPallets, iCartons, iLifts, iLines, actSlotLength, iNextSlotLength, iNextSlotLength2, iTimeSlotOrderBYID;
    bool isClickable, isCurrentSlotFixed, isNextSlotCovered;
    string DoorIDs = ",";
    string sTimeSlot = "";
    string sDoorNo;
    string sVendorCarrierName = "";
    string NextRequiredSlot = string.Empty;
    string sVendorCarrierNameNextSlot = string.Empty;
    string sCurrentFixedSlotID = "-1";
    string sSlotStartDay = string.Empty;

    protected void GenerateNonTimeBooking()
    {

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oMASBOK_BookingBAL = new APPBOK_BookingBAL();
        LogicalSchedulDateTime = Utilities.Common.TextToDateFormat(hdnLogicalScheduleDate.Value);
        oAPPBOK_BookingBE.Action = "GetNonTimeBooking";
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<APPBOK_BookingBE> lstNonTimeBooking = oMASBOK_BookingBAL.GetNonTimeBookingBAL(oAPPBOK_BookingBE);

        if (lstNonTimeBooking.Count > 0)
        {
            gvNonTimeBooking.DataSource = lstNonTimeBooking;
            gvNonTimeBooking.DataBind();
        }
        else
        {
            gvNonTimeBooking.DataSource = null;
            gvNonTimeBooking.DataBind();
        }

    }

    private void GenerateAlternateSlot()
    {
        GenerateAlternateSlot(false);
    }

    List<SYS_SlotTimeBE> lstNewSlotTime = new List<SYS_SlotTimeBE>();
    MASSIT_WeekSetupBE objWeekSetup = new MASSIT_WeekSetupBE();

    string strWeekDay = string.Empty;


    private void GenerateAlternateSlot(bool isStandardBookingRequired, int iSKUSiteDoorID = 0, int iSKUSiteDoorTypeID = 0)
    {

        int? iTotalPallets = TotalPallets.Trim() != string.Empty ? Convert.ToInt32(TotalPallets.Trim()) : (int?)null;
        int? iTotalCartons = TotalCartons.Trim() != string.Empty ? Convert.ToInt32(TotalCartons.Trim()) : (int?)null;

        actSlotLength = GetRequiredTimeSlotLength(iTotalPallets, iTotalCartons);

        #region -------Collect Basic Information-----------
        //-----------------Start Collect Basic Information --------------------------//
        DateTime dtSelectedDate = LogicalSchedulDateTime;
        string WeekDay = dtSelectedDate.ToString("dddd");

        DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
        oMASSIT_WeekSetupBE.ScheduleDate = LogicalSchedulDateTime;

        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
        {

            oMASSIT_WeekSetupBE.Action = "ShowAll";
            lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup.Count <= 0)
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
                return;
            }
            else if (lstWeekSetup[0].StartTime == null)
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
                return;
            }
        }

        APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
        MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

        //------Check for Specific Entry First--------//
        //APPSIT_DoorOpenTimeSpecificBAL oAPPSIT_DoorOpenTimeSpecificBAL = new APPSIT_DoorOpenTimeSpecificBAL();
        //MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE = new MASSIT_DoorOpenTimeSpecificBE();
        //oMASSIT_DoorOpenTimeSpecificBE.Action = "GetDoorConstraintsSpecific";
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime = new MASSIT_DoorOpenTimeBE();
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday = WeekDay;
        //oMASSIT_DoorOpenTimeSpecificBE.WeekStartDate = dtMon;
        //List<MASSIT_DoorOpenTimeSpecificBE> lstDoorConstraintsSpecific = oAPPSIT_DoorOpenTimeSpecificBAL.GetDoorConstraintsSpecificBAL(oMASSIT_DoorOpenTimeSpecificBE);
        //if (lstDoorConstraintsSpecific.Count > 0) { //Get Specific settings 
        //    for (int iCount = 0; iCount < lstDoorConstraintsSpecific.Count; iCount++) {
        //        DoorIDs += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + ",";
        //    }
        //}
        //else {  //Get Generic settings 
        //    oMASSIT_DoorOpenTimeBE.Action = "GetDoorConstraints";
        //    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        //    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        //    oMASSIT_DoorOpenTimeBE.Weekday = WeekDay;
        //    List<MASSIT_DoorOpenTimeBE> lstDoorConstraints = oMASSIT_DoorOpenTimeBAL.GetDoorConstraintsBAL(oMASSIT_DoorOpenTimeBE);
        //    for (int iCount = 0; iCount < lstDoorConstraints.Count; iCount++) {
        //        DoorIDs += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";

        //    }
        //}

        if (strOpenDoorIds != ",")
            DoorIDs = strOpenDoorIds;
        else
            DoorIDs = GetDoorConstraints();

        oMASSIT_DoorOpenTimeBE.Action = "GetDoorNoAndType";
        oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<MASSIT_DoorOpenTimeBE> lstDoorNoType = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

        int iDoorCount = lstDoorNoType.Count;

        //decimal decCellWidth = (87 / (iDoorCount));
        //double dblCellWidth = Convert.ToDouble(Math.Ceiling(decCellWidth));

        int dblCellWidth = 165;
        int i1stColumnWidth = 145;
        //-------------------------End Collect Basic Information ------------------------------//
        #endregion



        #region --------------Grid Header---------------
        dtAlternateSlot = new DataTable();
        dtAlternateSlot.Columns.Add("SlotStartDay", typeof(string));
        dtAlternateSlot.Columns.Add("TimeSlotOrderBYID", typeof(string));
        dtAlternateSlot.Columns.Add("ArrSlotTime", typeof(string));
        dtAlternateSlot.Columns.Add("EstSlotTime", typeof(string));
        dtAlternateSlot.Columns.Add("Vendor", typeof(string));
        dtAlternateSlot.Columns.Add("Pallets", typeof(string));
        dtAlternateSlot.Columns.Add("Cartoons", typeof(string));
        dtAlternateSlot.Columns.Add("Lines", typeof(string));
        dtAlternateSlot.Columns.Add("Lifts", typeof(string));
        dtAlternateSlot.Columns.Add("Color", typeof(string));
        dtAlternateSlot.Columns.Add("DoorNo", typeof(string));
        dtAlternateSlot.Columns.Add("FixedSlotID", typeof(string));
        dtAlternateSlot.Columns.Add("SlotTimeID", typeof(string));
        dtAlternateSlot.Columns.Add("SiteDoorNumberID", typeof(string));
        dtAlternateSlot.Columns.Add("SupplierType", typeof(string));
        dtAlternateSlot.Columns.Add("AllocationType", typeof(string));
        dtAlternateSlot.Columns.Add("BookingID", typeof(string));
        dtAlternateSlot.Columns.Add("BookingStatus", typeof(string));
        dtAlternateSlot.Columns.Add("BookingStatusID", typeof(string));
        dtAlternateSlot.Columns.Add("ArrSlotTime15min", typeof(string));
        dtAlternateSlot.Columns.Add("EstSlotTime15min", typeof(string));
        dtAlternateSlot.Columns.Add("DoorNameDisplay", typeof(string));
        dtAlternateSlot.Columns.Add("DoorNoDisplay", typeof(string));
        dtAlternateSlot.Columns.Add("VendorDisplay", typeof(string));
        dtAlternateSlot.Columns.Add("EstSlotTimeDisplay", typeof(string));
        dtAlternateSlot.Columns.Add("Weekday", typeof(string));

        //-------------Start Grid Header--------------//
        Table tblSlot = new Table();
        tblSlot.CellSpacing = 0;
        tblSlot.CellPadding = 0;
        tblSlot.CssClass = "FixedTables";
        tblSlot.ID = "Open_Text_General";
        tblSlot.Width = Unit.Pixel((dblCellWidth * iDoorCount) + i1stColumnWidth);

        // tableDiv_General.Controls.Add(tblSlot);

        TableRow trDoorNo = new TableRow();
        trDoorNo.TableSection = TableRowSection.TableHeader;
        TableHeaderCell tcDoorNo = new TableHeaderCell();
        tcDoorNo.Text = "Door Name / Door Type";  // +"<input type=image src='../../../Images/arrowleft.gif' style=height:16px; width:26px; />";
        tcDoorNo.BorderWidth = Unit.Pixel(1);
        tcDoorNo.Width = Unit.Pixel(i1stColumnWidth);
        trDoorNo.Cells.Add(tcDoorNo);

        TableRow trTime = new TableRow();
        trTime.TableSection = TableRowSection.TableHeader;
        TableHeaderCell tcTime = new TableHeaderCell();
        tcTime.Text = "Time";
        tcTime.BorderWidth = Unit.Pixel(1);
        tcTime.Width = Unit.Pixel(i1stColumnWidth);
        trTime.Cells.Add(tcTime);

        for (int iCount = 1; iCount <= iDoorCount; iCount++)
        {
            tcDoorNo = new TableHeaderCell();
            tcDoorNo.BorderWidth = Unit.Pixel(1);
            tcDoorNo.Text = lstDoorNoType[iCount - 1].DoorNo.DoorNumber.ToString() + " / " + lstDoorNoType[iCount - 1].DoorType.DoorType; ;
            tcDoorNo.Width = Unit.Pixel(dblCellWidth);
            trDoorNo.Cells.Add(tcDoorNo);

            tcTime = new TableHeaderCell();
            tcTime.BorderWidth = Unit.Pixel(1);
            tcTime.Text = "Vendor/Carrier";
            tcTime.Width = Unit.Pixel(dblCellWidth);
            trTime.Cells.Add(tcTime);
        }
        trDoorNo.BackColor = System.Drawing.Color.LightGray;
        tblSlot.Rows.Add(trDoorNo);

        trTime.BackColor = System.Drawing.Color.LightGray;
        tblSlot.Rows.Add(trTime);
        //-------------End Grid Header -------------//
        #endregion


        #region --------------Slot Ploting---------------
        //-------------Start Slot Ploting--------------//        

        //--------Find all Fixed Slots, Bookings For all Vendor/Carrier-------//
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime;
        //oAPPBOK_BookingBE.VehicleType = new MASSIT_VehicleTypeBE();
        //oAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
        lstAllFixedDoor = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

        lstAllFixedDoor = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE St)
        {
            DateTime? dt = (DateTime?)null;
            if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
            {
                dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
            }
            else
            {
                dt = new DateTime(1900, 1, 1, 0, 0, 0);
            }
            //DateTime dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
            //return dt <= lstWeekSetup[0].EndTime;
            if (St.BookingRef == "Fixed booking")
                return dt <= lstWeekSetup[0].EndTime;
            else
                return true;
        });

        //--------------------------------------//


        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        {
            oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1); 
            List<APPBOK_BookingBE> lstAllFixedDoorStartWeek = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

            //string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + ":" + lstWeekSetup[0].StartTime.Value.Minute.ToString();

            //Filetr Non Time Delivery
            lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
            {
                return St.SlotTime.SlotTime != "";
            });

            if (lstAllFixedDoorStartWeek != null && lstAllFixedDoorStartWeek.Count > 0)
            {
                lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
                {
                    DateTime? dt = (DateTime?)null;
                    if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
                    {
                        dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    }
                    else
                    {
                        dt = new DateTime(1900, 1, 1, 0, 0, 0);
                    }
                    //DateTime dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    return dt >= lstWeekSetup[0].StartTime;
                });

                if (lstAllFixedDoorStartWeek.Count > 0)
                {
                    lstAllFixedDoor = MergeListCollections(lstAllFixedDoor, lstAllFixedDoorStartWeek);
                    //List<APPBOK_BookingBE>  lstAllFixedDoorFinal = new List<APPBOK_BookingBE>(lstAllFixedDoor.Count + lstAllFixedDoorStartWeek.Count);
                }
            }
        }


        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();

        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);

        lstNewSlotTime = lstSlotTime;

        for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
        {
            string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + "." + lstWeekSetup[0].StartTime.Value.Minute.ToString("00");
            if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
            {
                StartTime = "24." + lstWeekSetup[0].StartTime.Value.Minute.ToString("00");
            }

            string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString("00");
            if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
            {
                EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString("00");
            }

            decimal dSlotTime = Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.'));
            decimal dStartTime = Convert.ToDecimal(StartTime);
            decimal dEndTime = Convert.ToDecimal(EndTime);

            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
            {
                dEndTime = Convert.ToDecimal("24.00");
            }

            if (dSlotTime >= dStartTime && dSlotTime < dEndTime)
            {

                TableRow tr = new TableRow();
                tr.TableSection = TableRowSection.TableBody;
                TableCell tc = new TableCell();
                tc.BorderWidth = Unit.Pixel(1);


                if (lstSlotTime.Count - 1 > jCount)
                    tc.Text = lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime);
                else
                    tc.Text = lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);


                tc.Width = Unit.Pixel(i1stColumnWidth);
                tr.Cells.Add(tc);


                for (int iCount = 1; iCount <= iDoorCount; iCount++)
                {
                    tc = new TableCell();
                    tc.BorderWidth = Unit.Pixel(1);
                    tc.Width = Unit.Pixel(dblCellWidth);
                    iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
                    iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
                    sTimeSlot = lstSlotTime[jCount].SlotTime;
                    sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
                    sSlotStartDay = lstWeekSetup[0].StartWeekday;
                    SiteDoorNumberID = Convert.ToString(lstDoorNoType[iCount - 1].DoorType.DoorTypeID);

                    //----Sprint 3b------//
                    iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
                    //------------------//
                    //test  for an other day                   
                    objWeekSetup = lstWeekSetup[0];
                    strWeekDay = lstWeekSetup[0].StartWeekday;

                    Literal ddl = SetCellColor(ref tc);
                    tc.Controls.Add(ddl);
                    tr.Cells.Add(tc);
                }
                tblSlot.Rows.Add(tr);
            }
        }



        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        {
            for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
            {
                string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString("00");
                if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
                {
                    EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString();
                }
                if (Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.')) < Convert.ToDecimal(EndTime))
                {
                    TableRow tr = new TableRow();
                    tr.TableSection = TableRowSection.TableBody;
                    TableCell tc = new TableCell();
                    tc.BorderWidth = Unit.Pixel(1);

                    if (lstSlotTime.Count - 1 > jCount)
                        tc.Text = lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime);
                    else
                        tc.Text = lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);

                    tc.Width = Unit.Pixel(i1stColumnWidth);
                    tr.Cells.Add(tc);

                    for (int iCount = 1; iCount <= iDoorCount; iCount++)
                    {

                        tc = new TableCell();
                        tc.Width = Unit.Pixel(dblCellWidth);
                        tc.BorderWidth = Unit.Pixel(1);
                        iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
                        iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
                        sTimeSlot = lstSlotTime[jCount].SlotTime;
                        sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
                        sSlotStartDay = lstWeekSetup[0].EndWeekday;
                        SiteDoorNumberID = Convert.ToString(lstDoorNoType[iCount - 1].DoorType.DoorTypeID);
                        //----Sprint 3b------//
                        iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
                        //------------------//
                        //test  for an other day                   
                        objWeekSetup = lstWeekSetup[0];
                        strWeekDay = lstWeekSetup[0].EndWeekday;

                        Literal ddl = SetCellColor(ref tc);
                        tc.Controls.Add(ddl);
                        tr.Cells.Add(tc);

                    }
                    tblSlot.Rows.Add(tr);

                }
            }
        }

        //   DataTable dtAlternateSlotFinal = dtAlternateSlot;

        //-------------End Slot Ploting--------------//
        #endregion

        #region Start Finding Insufficent Slot Time
        Color cBackColorFirstCell, cBackColor, cBorderColor, cBackColorPreviousCell, cBorderColorPreviousCell, cBackColorFirstSlot;
        string att, abbr;
        bool isNextSlot;
        bool isSlotInsufficent;
        bool isNewCell = false;
        cBorderColorPreviousCell = Color.Empty;


        for (int iCell = 1; iCell <= iDoorCount; iCell++)
        {
            cBackColorPreviousCell = Color.Empty;
            for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
            {
                //FindNextSufficentSlotTime(ref tblSlot, iCell, iRow);
                isSlotInsufficent = false;
                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBackColorFirstCell = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                cBackColorFirstSlot = Color.Empty;

                if ((cBackColor.Name == "0" || cBackColor == Color.White || cBackColor == Color.GreenYellow || cBackColor == Color.Orange) && cBorderColor != Color.Red)
                {
                    isNextSlot = true;
                    for (int iLen = 0; iLen < actSlotLength; iLen++)
                    {
                        if (iRow + iLen < tblSlot.Rows.Count)
                        {
                            cBackColor = tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor;
                            cBorderColor = tblSlot.Rows[iRow + iLen].Cells[iCell].BorderColor;

                            att = tblSlot.Rows[iRow + iLen].Cells[iCell].Attributes["onclick"];
                            if (att != null && att.Contains("setAletrnateSlot"))
                            {
                                tblSlot.Rows[iRow + iLen].Cells[iCell].Attributes["onclick"].Remove(0);
                            }

                            //if (cBackColor.Name != "0" && cBackColor != Color.White && cBackColor != Color.GreenYellow && cBackColor != Color.Orange) {
                            //    isNextSlot = false;
                            //    break;
                            //}

                            abbr = tblSlot.Rows[iRow + iLen].Cells[iCell].Attributes["abbr"];
                            if (abbr != null && abbr != string.Empty && abbr.Contains("V"))
                            {
                                isNextSlot = false;
                                break;
                            }
                            else if (abbr != null && abbr != string.Empty && abbr.Contains("C"))
                            {
                                string[] arrabbr = abbr.Split('-');
                                if (arrabbr[1] != ddlCarrier.SelectedItem.Value)
                                {
                                    isNextSlot = false;
                                    break;
                                }
                                else if (arrabbr[1] == ddlCarrier.SelectedItem.Value && iLen > 0 && cBackColor != Color.White && cBackColor != Color.GreenYellow && cBorderColor != Color.Red)
                                {
                                    isNextSlot = false;
                                    break;
                                }
                            }
                            else if (cBackColor == Color.Black)
                            {
                                isNextSlot = false;
                                break;
                            }
                        }
                        else
                        {
                            cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;

                            att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];
                            if (att != null && att.Contains("setAletrnateSlot"))
                            {
                                tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"].Remove(0);
                            }
                            //if (cBackColor.Name != "0" && cBackColor != Color.White && cBackColor != Color.GreenYellow && cBackColor != Color.Orange) {
                            isNextSlot = false;
                            //}
                            isSlotInsufficent = true;
                            break;
                        }

                        //if (hdnCurrentRole.Value != "Carrier") {
                        //    if (cBackColorFirstSlot == Color.GreenYellow) {
                        //        if (tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor == Color.Orange) {
                        //            tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor = Color.GreenYellow;
                        //        }
                        //    }
                        //}
                    }
                    if (isNextSlot == false)
                    {
                        att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];

                        if (hdnCurrentRole.Value == "Carrier")
                        {
                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
                            tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
                            tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
                            tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                        }
                        else
                        {
                            if (cBackColor.Name != "0" && cBackColor != Color.White && cBackColor != Color.GreenYellow && cBackColor != Color.Orange)
                            {
                                if ((att != null && att.Contains("CheckAletrnateSlot")) || isSlotInsufficent)
                                {
                                    att = att.Replace("CheckAletrnateSlot(", "");
                                    att = att.Replace(");", "");
                                    string[] Arratt = att.Split(',');
                                    string att0 = Arratt[0];
                                    string att1 = Arratt[1];
                                    string att2 = Arratt[2];
                                    string att3 = Arratt[3];
                                    string att4 = Arratt[5];
                                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "setAletrnateSlot('true'," + att0 + "," + att1 + "," + att2 + ",'-1'," + att3 + "," + att4 + ");");
                                }
                            }
                            else
                            {
                                if (att != null && att.Contains("CheckAletrnateSlot"))
                                {
                                    att = att.Replace("CheckAletrnateSlot(", "");
                                    att = att.Replace(");", "");
                                    string[] Arratt = att.Split(',');
                                    string att0 = Arratt[0];
                                    string att1 = Arratt[1];
                                    string att2 = Arratt[2];
                                    string att3 = Arratt[3];
                                    string att4 = Arratt[5];
                                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "setAletrnateSlot('true'," + att0 + "," + att1 + "," + att2 + ",'-1'," + att3 + "," + att4 + ");");
                                }
                            }

                            if (cBackColorFirstCell != Color.GreenYellow && cBackColorFirstCell != Color.Orange)
                            {
                                tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightGray;
                            }
                        }
                    }
                }

                //--Blackout the slots which are not matched with the seleted vehicle type-------// 
                //if (!strVehicleDoorTypeMatrix.Contains(lstDoorNoType[iCell - 1].DoorNo.SiteDoorTypeID.ToString() + ",")) {
                //    if ((tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.White || tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.LightGray) && tblSlot.Rows[iRow].Cells[iCell].BorderColor == Color.Red) {
                //        tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.White;
                //    }

                //    if (tblSlot.Rows[iRow].Cells[iCell].BackColor != Color.GreenYellow) {
                //        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
                //        tblSlot.Rows[iRow].Cells[iCell].BorderColor = Color.Black;
                //        tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
                //    }

                //    if (hdnCurrentRole.Value == "Carrier") {
                //        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
                //        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
                //        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                //    }
                //}

                //--------Sprint 3a ------------//

                string ReqStrVehicleDoorTypeMatrix = string.Empty;

                if (isStandardBookingRequired == true && iSKUSiteDoorID != 0)
                {
                    ReqStrVehicleDoorTypeMatrix = strVehicleDoorTypeMatrixSKU;
                }
                else
                {
                    ReqStrVehicleDoorTypeMatrix = strVehicleDoorTypeMatrix;
                }

                if (!ReqStrVehicleDoorTypeMatrix.Contains(lstDoorNoType[iCell - 1].DoorNo.SiteDoorTypeID.ToString() + ","))
                {
                    if ((tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.White || tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.LightGray) && tblSlot.Rows[iRow].Cells[iCell].BorderColor == Color.Red)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.Black;
                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightYellow;
                    }

                    //If SKU door matrix find the skip to blackout that type of door  SPRINT 4 POINT 7
                    if (!(isStandardBookingRequired == true && iSKUSiteDoorID != 0 && iSKUSiteDoorTypeID == lstDoorNoType[iCell - 1].DoorNo.SiteDoorTypeID))
                    {

                        if (tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.White || tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.LightGray)
                        {
                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
                            tblSlot.Rows[iRow].Cells[iCell].BorderColor = Color.White;
                            //tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "1px");  // .BorderWidth = 0;
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "1px");
                        }

                        if (hdnCurrentRole.Value == "Carrier")
                        {
                            tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
                            tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
                            tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                        }
                    }
                }
                //------------------------------//

                //----------------------------------------------------------------------------//

                #region Row Spaning
                //-----------------------Row Spaning-------------//

                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                if (cBackColor != cBackColorPreviousCell || cBorderColor != cBorderColorPreviousCell)
                {
                    isNewCell = true;
                }

                bool isCellCreated = false;
                if (cBackColor == cBackColorPreviousCell && cBackColor != Color.White)
                {

                    isCellCreated = true;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");

                    if (cBorderColor == Color.Red || cBackColor == Color.Purple)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "2px");
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "2px");
                    }
                    else
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "1px");
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "1px");
                    }

                    if (cBackColorPreviousCell == cBackColor && iRow > 0 && isNewCell == true)
                    {
                        isNewCell = false;
                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "0px");

                        if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "2px");
                        }
                        else
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "1px");
                        }
                    }
                }
                else if (cBackColor == cBackColorPreviousCell && cBorderColor == Color.Red)
                {

                    isCellCreated = true;

                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");

                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "2px");
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "2px");


                    if (cBackColorPreviousCell == cBackColor && iRow > 0 && isNewCell == true)
                    {
                        isNewCell = false;

                        if (cBorderColorPreviousCell != Color.Red)
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");

                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "0px");
                        if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "2px");
                        }
                        else
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "1px");
                        }
                    }
                }

                if (isCellCreated == false && iRow > 0)
                {
                    if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "2px");
                    else
                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "1px");
                }

                abbr = tblSlot.Rows[iRow].Cells[iCell].Attributes["abbr"];
                if (abbr != null && abbr != string.Empty)
                {
                    if (cBorderColor == Color.Red || cBackColor == Color.Purple)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");
                    }
                    else
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "1px");
                    }
                }

                cBackColorPreviousCell = cBackColor;
                cBorderColorPreviousCell = cBorderColor;

                //--------------------------------------------------//
                #endregion
            }
        }

        //foreach (TableRow row in tblSlot.Rows) {            
        //        cl = row.Cells[iCell].BackColor;
        //        if (cl == Color.White) {
        //            //Find Next While Cell

        //        }
        //    }

        #endregion


        #region Standard Booking Required

        if (isStandardBookingRequired)
        {
            bool isSlotFind = false;
            string strCheckedHour = "-10";
            bool isProrityChecked = false;
            bool isMoveBack = false;
            int iCellNo = 1;
            string iLowestPriority = string.Empty;  // Function
            bool isValidHour = false;

            //changes for suggestion slot
            hdnSuggestedSlotTimeID.Value = "-1";
            ltBookingDate.Text = string.Empty;
            hdnSuggestedSiteDoorNumberID.Value = "-1";
            hdnSuggestedSiteDoorNumber.Value = string.Empty;
            ltSuggestedSlotTime.Text = string.Empty;

            DateTime dtBookingDate = LogicalSchedulDateTime;
            DateTime LogicalSchedulDateTimePrev;
            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
                LogicalSchedulDateTimePrev = LogicalSchedulDateTime.AddDays(-1);
            else
                LogicalSchedulDateTimePrev = LogicalSchedulDateTime;

            if (iSKUSiteDoorID == 0) //SPRINT 4 POINT 7
                iLowestPriority = GetLowestPriority(ArrDoorTypePriority);   // Function
            else
                iLowestPriority = GetLowestPriority(ArrDoorTypePrioritySKU);

            string iSuggestedPriority = "1";

            for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
            {
                isProrityChecked = false;
                isMoveBack = false;

                for (int iCell = 1; iCell <= iDoorCount; iCell++)
                {

                    if (iSKUSiteDoorID == 0 || lstDoorNoType[iCell - 1].SiteDoorNumberID == iSKUSiteDoorID)
                    {  //SPRINT 4 POINT 7

                        int iDoorTypeID = lstDoorNoType[iCell - 1].DoorType.DoorTypeID;

                        cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                        cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                        bool isCurrentVendorSlot = false;
                        abbr = tblSlot.Rows[iRow].Cells[iCell].Attributes["abbr"];
                        if ((abbr == null || abbr == string.Empty) && cBorderColor != Color.Red)
                        {
                            isCurrentVendorSlot = true;
                        }
                        else if (abbr != null && abbr != string.Empty && abbr.Contains("V"))
                        {
                            isCurrentVendorSlot = false;
                        }
                        else if (abbr != null && abbr != string.Empty && abbr.Contains("C"))
                        {
                            string[] arrabbr = abbr.Split('-');
                            if (arrabbr[1] != ddlCarrier.SelectedItem.Value)
                            {
                                isCurrentVendorSlot = false;
                            }
                        }


                        if (cBackColor == Color.White && isCurrentVendorSlot)
                        {
                            att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];
                            if (att.Contains("CheckAletrnateSlot"))
                            {
                                att = att.Replace("CheckAletrnateSlot(", "");
                                att = att.Replace(");", "");
                                string[] Arratt = att.Split(',');
                                string att0 = Arratt[0].Replace("'", "");
                                string att1 = Arratt[1].Replace("'", "");
                                string att2 = Arratt[2].Replace("'", "");
                                string att3 = Arratt[3].Replace("'", "");
                                string att4 = Arratt[5].Replace("'", "");

                                //--------- Sprint 3b----------//
                                string att6 = Arratt[6].Replace("'", "");
                                //-----------------------------//

                                if (isMoveBack == false)
                                {

                                    //-- Sprint 3a Start -- Check Before and after time//
                                    if (LogicalSchedulDateTime.ToString("ddd").ToLower() == att4.ToLower())
                                    {
                                        //if (LogicalSchedulDateTime.ToString("ddd").ToLower() == att4.ToLower() || LogicalSchedulDateTimePrev.ToString("ddd").ToLower() == att4.ToLower()) {
                                        if (Convert.ToInt32(att6) < Convert.ToInt32(BeforeSlotTimeID) || Convert.ToInt32(att6) > Convert.ToInt32(AfterSlotTimeID))
                                        {
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        //if (Convert.ToInt32(BeforeSlotTimeID) == 0 || Convert.ToInt32(AfterSlotTimeID) == 100)
                                        continue;
                                    }
                                    //------------Sprint 3a End--------------//


                                    //----Stage 10 Point 10L 5.1-----//
                                    //Carrie Trime Constraint//
                                    if (ddlSite.IsBookingExclusions)
                                    {   //Only if Booking constraints set to yes 

                                        if (LogicalSchedulDateTime.ToString("ddd").ToLower() == att4.ToLower())
                                        {
                                            //if (LogicalSchedulDateTime.ToString("ddd").ToLower() == att4.ToLower() || LogicalSchedulDateTimePrev.ToString("ddd").ToLower() == att4.ToLower()) {

                                            if (Convert.ToInt32(att6) < Convert.ToInt32(VendorBeforeSlotTimeID) || Convert.ToInt32(att6) > Convert.ToInt32(VendorAfterSlotTimeID))
                                            {
                                                continue;
                                            }

                                        }
                                        else
                                        {
                                            continue;
                                        }
                                    }
                                    //---------------------//

                                    //Check delivery constraints//
                                    bool isValidTimeSlot = false;

                                    if (att3.Split(':')[0] != strCheckedHour)
                                    {
                                        isValidTimeSlot = CheckValidSlot(att3, att4, Convert.ToString(TotalPallets), Convert.ToString(TotalLines));
                                        isValidHour = isValidTimeSlot;
                                    }
                                    else
                                    {
                                        isValidTimeSlot = isValidHour;
                                    }

                                    if (isValidTimeSlot == false)
                                    {
                                        strCheckedHour = att3.Split(':')[0];
                                        continue;
                                    }
                                    //------------------------------//

                                    //---------Check Door Priority---------//                             
                                    string iPriority = string.Empty;

                                    if (isStandardBookingRequired == true && iSKUSiteDoorID != 0)
                                    {
                                        iPriority = ArrDoorTypePrioritySKU.Get(iDoorTypeID.ToString());
                                    }
                                    else
                                    {
                                        iPriority = ArrDoorTypePriority.Get(iDoorTypeID.ToString());
                                    }

                                    //if (iCell == 1)
                                    //    iPriority = "3";
                                    //if (iCell == 2)
                                    //    iPriority = "2";
                                    //if (iCell == 3)
                                    //    iPriority = "4";

                                    if (Convert.ToInt32(iPriority) != Convert.ToInt32(iLowestPriority))
                                    {
                                        if (isProrityChecked == false)
                                        {
                                            iCellNo = iCell;
                                            iSuggestedPriority = iPriority;
                                            isProrityChecked = true;
                                            continue;
                                        }
                                        else if (isProrityChecked == true)
                                        {
                                            if (Convert.ToInt32(iPriority) < Convert.ToInt32(iSuggestedPriority))
                                            {
                                                iCellNo = iCell;
                                                iSuggestedPriority = iPriority;
                                                isProrityChecked = true;
                                                continue;
                                            }
                                            else
                                            {
                                                if (iCell < iDoorCount)
                                                {
                                                    continue;
                                                }
                                                if (iCell == iDoorCount)
                                                {  //last cell
                                                    iCell = iCellNo - 1;
                                                    isMoveBack = true;
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                    //-------------------------------------//
                                }


                                if (LogicalSchedulDateTime.ToString("ddd").ToLower() != att4.ToLower())
                                {
                                    dtBookingDate = LogicalSchedulDateTime.AddDays(-1);
                                }                               

                                ltBookingDate.Text = dtBookingDate.Day.ToString() + "/" + dtBookingDate.Month.ToString() + "/" + dtBookingDate.Year.ToString();
                                ltSuggestedSlotTime.Text = att3 + " " + AtDoorName + " " + att2;
                                hdnSuggestedSlotTimeID.Value = att0;
                                hdnSuggestedFixedSlotID.Value = "-1";
                                hdnSuggestedSiteDoorNumberID.Value = att1;
                                hdnSuggestedSiteDoorNumber.Value = att2;
                                hdnSuggestedSlotTime.Value = att3;
                                ltTimeSlot.Text = hdnSuggestedSlotTime.Value;
                                ltDoorNo.Text = hdnSuggestedSiteDoorNumber.Value;
                                ltTimeSlot_1.Text = att3;
                                ltDoorNo_1.Text = hdnSuggestedSiteDoorNumber.Value;
                                hdnSuggestedWeekDay.Value = att4;

                                btnConfirmBooking.Visible = true;
                                lblProposeBookingInfo.Visible = true;
                                isSlotFind = true;

                                if (txtSchedulingdate.innerControltxtDate.Value != GetQueryStringValue("Scheduledate").ToString() && hdnCurrentMode.Value == "Edit")
                                {
                                    btnNotHappy.Visible = true;
                                }
                                ltSuggestedSlotTime.Visible = true;
                                ltBookingDate.Visible = true;                           
                            }
                        }
                        //Last Cell ,still No slot found but a slot alrady found with lowest priority , go for that
                        if (iCell == iDoorCount && isSlotFind == false && isProrityChecked == true)
                        {
                            iCell = iCellNo - 1;
                            isMoveBack = true;
                            continue;
                        }
                    } //SPRINT 4 POINT 7
                    if (isSlotFind)
                        break;
                }
                if (isSlotFind)
                    break;
            }
            //if (isSlotFind == false && hdnCurrentMode.Value == "Edit")
            //{
            //    //changes for all days alternative slots...
            //    //if (txtSchedulingdate.innerControltxtDate.Value == GetQueryStringValue("Scheduledate").ToString() && hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue && isDoorTypeNotChanged == false && hdnPreviousVehicleType.Value != "")
            //    //{
            //    //if (hdnPreviousVehicleType.Value != ddlVehicleType.SelectedValue && isDoorTypeNotChanged == false && hdnPreviousVehicleType.Value != "")
            //    //{
            //        mdlNoSlotFound.Show();
            //        isDoorCheckPopup = true;
            //        return;
            //   // }
            //}
        }

        #endregion


        #region Set Background color for Fixed slot
        for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
        {
            for (int iCell = 1; iCell <= iDoorCount; iCell++)
            {
                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                if (cBorderColor == Color.Red && (cBackColor == Color.White || cBackColor == Color.LightGray))
                {

                    //------------Phase 19 Point 1---------------------------//
                    string prefix = tblSlot.Rows[iRow].Cells[iCell].Attributes["prefix"];
                    string[] arrprefix = new string[2];
                    if (!string.IsNullOrEmpty(prefix))
                        arrprefix = prefix.Split('-');
                    //------------------------------------------------------//

                    if (string.IsNullOrEmpty(arrprefix[0]) || arrprefix[0] == "H")
                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightYellow;
                    else
                    {
                        for (int iLen = 0; iLen < Convert.ToInt32(arrprefix[1]); iLen++)
                        {
                            if (iRow + iLen < tblSlot.Rows.Count)
                            {
                                Color cBackColorInner = tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor;
                                Color cBorderColorInner = tblSlot.Rows[iRow + iLen].Cells[iCell].BorderColor;

                                if (cBorderColorInner == Color.Red && (cBackColorInner == Color.White || cBackColorInner == Color.LightGray))
                                {
                                    tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor = Color.LightPink;
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        hdnGraySlotClicked.Value = "false";


        try
        {

            for (int i = 0; i < lstSlot.Count; i++)
            {
                bool isDataPresent = dtAlternateSlot.AsEnumerable().Any(x => x.Field<string>("ArrSlotTime") == lstSlot[i].ArrSlotTime.ToString()
                              && x.Field<string>("EstSlotTime") == lstSlot[i].EstSlotTime.ToString()
                              && x.Field<string>("DoorNo") == lstSlot[i].DoorNo.ToString()
                              );
                if (isDataPresent == false)
                {
                    if (lstSlot[i].ArrSlotTime != lstWeekSetup[0].EndTime.Value.TimeOfDay.ToString())
                    {
                        DataRow dr = dtAlternateSlot.NewRow();
                        dr["SlotStartDay"] = lstSlot[i].SlotStartDay;
                        dr["TimeSlotOrderBYID"] = lstSlot[i].siTimeSlotOrderBYID;
                        dr["ArrSlotTime"] = lstSlot[i].ArrSlotTime;
                        dr["EstSlotTime"] = lstSlot[i].EstSlotTime;
                        dr["SiteDoorNumberID"] = lstSlot[i].SiteDoorNumberID;
                        dr["SlotTimeID"] = lstSlot[i].SlotTimeID;
                        dr["DoorNo"] = lstSlot[i].DoorNo;
                        dr["Color"] = "Black";
                        dr["Pallets"] = "0";
                        dr["Cartoons"] = "0";
                        dr["Lines"] = "0";
                        dr["Lifts"] = "0";
                        dr["FixedSlotID"] = "";
                        dr["Vendor"] = "";
                        dr["SupplierType"] = "";
                        dr["AllocationType"] = "";
                        dr["ArrSlotTime15min"] = "";
                        dr["EstSlotTime15min"] = "";
                        dr["DoorNameDisplay"] = "";
                        dr["DoorNoDisplay"] = "";
                        dr["VendorDisplay"] = "";
                        dr["BookingID"] = "";
                        dr["BookingStatus"] = "Unconfirmed Fixed";
                        dr["BookingStatusID"] = "-1";
                        dtAlternateSlot.Rows.Add(dr);
                        dtAlternateSlot.AcceptChanges();
                    }
                }
            }
           

            if (dtAlternateSlot.Rows.Count > 0)
            {


                for (int i = 0; i < dtAlternateSlot.Rows.Count; i++)
                {
                    string weekday = string.Empty;
                    if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "SUN")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "1";
                    }
                    else if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "MON")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "2";
                    }
                    else if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "TUE")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "3";
                    }
                    else if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "WED")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "4";
                    }
                    else if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "THU")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "5";
                    }
                    else if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "FRI")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "6";
                    }
                    else if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "SAT")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "7";
                    }

                    DataRow dr = dtAlternateSlot.Rows[i];
                    if (dr["Color"].ToString().Trim() == "Orange")
                    {
                        dr.Delete();
                        dtAlternateSlot.AcceptChanges();
                    }


                    if (dtAlternateSlot.Rows[i]["Color"].ToString().Trim() == "White")
                    {
                        dtAlternateSlot.Rows[i]["Vendor"] = "";
                        dtAlternateSlot.Rows[i]["Pallets"] = "-";
                        dtAlternateSlot.Rows[i]["Cartoons"] = "-";
                        dtAlternateSlot.Rows[i]["Lines"] = "-";
                        dtAlternateSlot.Rows[i]["Lifts"] = "-";
                    }
                    if (dtAlternateSlot.Rows[i]["Pallets"].ToString().Trim() == "-1" || dtAlternateSlot.Rows[i]["Pallets"].ToString().Trim() == "0")
                    {
                        dtAlternateSlot.Rows[i]["Pallets"] = "-";
                    }
                    if (dtAlternateSlot.Rows[i]["Cartoons"].ToString().Trim() == "-1" || dtAlternateSlot.Rows[i]["Cartoons"].ToString().Trim() == "0")
                    {
                        dtAlternateSlot.Rows[i]["Cartoons"] = "-";
                    }
                    if (dtAlternateSlot.Rows[i]["Lines"].ToString().Trim() == "-1" || dtAlternateSlot.Rows[i]["Lines"].ToString().Trim() == "0")
                    {
                        dtAlternateSlot.Rows[i]["Lines"] = "-";
                    }
                    if (dtAlternateSlot.Rows[i]["Lifts"].ToString().Trim() == "-1" || dtAlternateSlot.Rows[i]["Lifts"].ToString().Trim() == "0")
                    {
                        dtAlternateSlot.Rows[i]["Lifts"] = "-";
                    }

                    if (dtAlternateSlot.Rows[i]["AllocationType"].ToString().Trim() == "S" && dtAlternateSlot.Rows[i]["Color"].ToString().Trim() != "GreenYellow")
                    {
                        dtAlternateSlot.Rows[i]["Color"] = "Pink";
                    }

                   

                    string[] ArrSlotTime = dtAlternateSlot.Rows[i]["ArrSlotTime"].ToString().Split(':');
                    string[] EstSlotTime = dtAlternateSlot.Rows[i]["EstSlotTime"].ToString().Split(':');


                    // || ArrSlotTime[1] == "05" || ArrSlotTime[1] == "10"
                    if (ArrSlotTime[1] == "00")
                    {
                        dtAlternateSlot.Rows[i]["ArrSlotTime15min"] = ArrSlotTime[0] + ":00";
                    }

                    //EstSlotTime[1] == "05" || EstSlotTime[1] == "10" ||
                    if (EstSlotTime[1] == "15")
                    {
                        dtAlternateSlot.Rows[i]["EstSlotTime15min"] = EstSlotTime[0] + ":15";
                    }

                    //|| ArrSlotTime[1] == "20" || ArrSlotTime[1] == "25"
                    if (ArrSlotTime[1] == "15")
                    {
                        dtAlternateSlot.Rows[i]["ArrSlotTime15min"] = ArrSlotTime[0] + ":15";
                    }
                    //EstSlotTime[1] == "20" || EstSlotTime[1] == "25" ||
                    if (EstSlotTime[1] == "30")
                    {
                        dtAlternateSlot.Rows[i]["EstSlotTime15min"] = EstSlotTime[0] + ":30";
                    }

                    //|| ArrSlotTime[1] == "35" || ArrSlotTime[1] == "40"
                    if (ArrSlotTime[1] == "30")
                    {
                        dtAlternateSlot.Rows[i]["ArrSlotTime15min"] = ArrSlotTime[0] + ":30";
                    }
                    //EstSlotTime[1] == "35" || EstSlotTime[1] == "40" ||
                    if (EstSlotTime[1] == "45")
                    {
                        dtAlternateSlot.Rows[i]["EstSlotTime15min"] = EstSlotTime[0] + ":45";
                    }
                    //|| ArrSlotTime[1] == "50" || ArrSlotTime[1] == "55"
                    if (ArrSlotTime[1] == "45")
                    {
                        dtAlternateSlot.Rows[i]["ArrSlotTime15min"] = ArrSlotTime[0] + ":45";
                    }
                    //EstSlotTime[1] == "50" || EstSlotTime[1] == "55" ||
                    if (EstSlotTime[1] == "00")
                    {
                        if (EstSlotTime[0] != "23" && EstSlotTime[1] != "00")
                        {
                            int EstSlotTimeNew = Convert.ToInt32(EstSlotTime[0]) + 1;
                            dtAlternateSlot.Rows[i]["EstSlotTime15min"] = Convert.ToInt32(EstSlotTime[0]) + 1 + ":00";
                        }
                        else
                        {
                            // new logic for time
                            if (EstSlotTime[0] == "23" && EstSlotTime[1] != "00")
                            {
                                dtAlternateSlot.Rows[i]["EstSlotTime15min"] = "00:00";
                            }
                            else
                            {
                                dtAlternateSlot.Rows[i]["EstSlotTime15min"] = EstSlotTime[0] + ":00";
                            }
                        }
                    }

                    if (dtAlternateSlot.Rows[i]["EstSlotTime15min"].ToString().Split(':')[0].Length == 1)
                    {
                        dtAlternateSlot.Rows[i]["EstSlotTime15min"] = "0" + dtAlternateSlot.Rows[i]["EstSlotTime15min"].ToString().Split(':')[0] + ":00";
                    }
                    if (dtAlternateSlot.Rows[i]["ArrSlotTime"].ToString().Substring(0, 5).Split(':')[0].ToArray().Length == 1)
                    {
                        dtAlternateSlot.Rows[i]["ArrSlotTime"] = "0" + dtAlternateSlot.Rows[i]["ArrSlotTime"].ToString().Substring(0, 5);
                    }
                    else
                    {
                        dtAlternateSlot.Rows[i]["ArrSlotTime"] = dtAlternateSlot.Rows[i]["ArrSlotTime"].ToString().Substring(0, 5);
                    }
                    if (dtAlternateSlot.Rows[i]["EstSlotTime"].ToString().Substring(0, 5).Split(':')[0].ToArray().Length == 1)
                    {
                        dtAlternateSlot.Rows[i]["EstSlotTime"] = "0" + dtAlternateSlot.Rows[i]["EstSlotTime"].ToString().Substring(0, 5);
                    }
                    else
                    {
                        dtAlternateSlot.Rows[i]["EstSlotTime"] = dtAlternateSlot.Rows[i]["EstSlotTime"].ToString().Substring(0, 5);
                    }


                    var SlotID = lstSlotTime.Where(x => x.SlotTime == dtAlternateSlot.Rows[i]["ArrSlotTime"].ToString().Trim());
                    if (SlotID != null)
                    {
                        dtAlternateSlot.Rows[i]["SlotTimeID"] = SlotID.Select(x => x.SlotTimeID).FirstOrDefault();

                    }
                    if (dtAlternateSlot.Rows[i]["EstSlotTime"].ToString() == "00:00")
                    {
                        dtAlternateSlot.Rows[i]["EstSlotTime"] = "00:00"; // to categories 00:00 in min and max
                    }


                }
                for (int i = 0; i < dtAlternateSlot.Rows.Count; i++)
                {
                    // Remove multipe data due to 15 min slots
                    bool isDataPresent = dtAlternateSlot.AsEnumerable().Any(x => x.Field<string>("ArrSlotTime15min") == dtAlternateSlot.Rows[i]["ArrSlotTime15min"].ToString()
                                   && x.Field<string>("EstSlotTime15min") == dtAlternateSlot.Rows[i]["EstSlotTime15min"].ToString()
                                   && x.Field<string>("DoorNo") == dtAlternateSlot.Rows[i]["DoorNo"].ToString()
                                   && x.Field<string>("Color").Trim() == "Black");
                    if (isDataPresent)
                    {
                        var lst = dtAlternateSlot.AsEnumerable().Where(x => x.Field<string>("ArrSlotTime15min") == dtAlternateSlot.Rows[i]["ArrSlotTime15min"].ToString()
                                       && x.Field<string>("EstSlotTime15min") == dtAlternateSlot.Rows[i]["EstSlotTime15min"].ToString()
                                       && x.Field<string>("DoorNo") == dtAlternateSlot.Rows[i]["DoorNo"].ToString()).ToList();
                        if (lst.Count() >= 2)
                        {
                            int count = lst.Select(x => x.Field<string>("Color").Trim()).Distinct().Count();
                            if (count >= 2)
                            {
                                for (int j = 0; j < lst.Count(); j++)
                                {
                                    if (lst[j]["Color"].ToString() == "Black")
                                    {
                                        DataRow drDelete = dtAlternateSlot.AsEnumerable().Where(x => x.Field<string>("ArrSlotTime15min") == dtAlternateSlot.Rows[i]["ArrSlotTime15min"].ToString()
                                                   && x.Field<string>("EstSlotTime15min") == dtAlternateSlot.Rows[i]["EstSlotTime15min"].ToString()
                                                   && x.Field<string>("DoorNo") == dtAlternateSlot.Rows[i]["DoorNo"].ToString()
                                                   && x.Field<string>("Color").Trim() == "Black").FirstOrDefault();
                                        drDelete.Delete();
                                        dtAlternateSlot.AcceptChanges();
                                    }

                                }

                            }

                        }
                    }
                }

                dtAlternateSlot = (from c in dtAlternateSlot.AsEnumerable()
                                   orderby c.Field<string>("Weekday"), c.Field<string>("ArrSlotTime")
                                   select c).CopyToDataTable();

                var SlotFinal = (from c in dtAlternateSlot.AsEnumerable()
                                 select new
                                 {
                                     DoorNo = c.Field<string>("DoorNo"),
                                     FixedSlotID = c.Field<string>("FixedSlotID"),
                                     ArrSlotTime = c.Field<string>("ArrSlotTime15min"),
                                     EstSlotTime = c.Field<string>("EstSlotTime15min"),
                                     Weekday = c.Field<string>("Weekday")
                                 }).Distinct();

                //foreach (var itemSlotFinal in SlotFinal)
                //{
                //    var dtAlternateSlotDuplicate = dtAlternateSlot.AsEnumerable().Where(x => x.Field<string>("ArrSlotTime15min") == itemSlotFinal.ArrSlotTime
                //        && x.Field<string>("EstSlotTime15min") == itemSlotFinal.EstSlotTime
                //          && x.Field<string>("DoorNo") == itemSlotFinal.DoorNo && x.Field<string>("FixedSlotID") == itemSlotFinal.FixedSlotID).ToList();
                //    if (dtAlternateSlotDuplicate.Count > 1)
                //    { 
                //        DataRow slot=dtAlternateSlotDuplicate.ToList().Where(x=>x.Field<string>("Color")=="Black").FirstOrDefault();                   
                //    }

                //}




                //// setting dummy fixed slot id to get isolate record for pop up data
                var UpdateDummyFixedSLot = (from x in dtAlternateSlot.AsEnumerable()
                                            where (x.Field<string>("FixedSlotID").Trim() == "" )
                                            //|| x.Field<string>("FixedSlotID").Trim() == "-1")
                                                  && x.Field<string>("Color").Trim() == "Purple"
                                            select x).ToList();

                var UpdateDummyFixedSlotTime = (from x in dtAlternateSlot.AsEnumerable()
                                                where (x.Field<string>("FixedSlotID").Trim() == ""
                                                //|| x.Field<string>("FixedSlotID").Trim() == "-1"
                                                )
                                                      && x.Field<string>("Color").Trim() == "Purple"
                                                group x by new
                                                {
                                                    EstSlotTime15min = x.Field<string>("EstSlotTime15min"),
                                                    ArrSlotTime15min = x.Field<string>("ArrSlotTime15min"),
                                                    DoorNo = x.Field<string>("DoorNo")
                                                } into t
                                                select t).ToList();


                int dummy = 1000000;
                foreach (var item in UpdateDummyFixedSlotTime)
                {
                    var data = UpdateDummyFixedSLot.Where(x => x.Field<string>("ArrSlotTime15min") == item.Key.ArrSlotTime15min
                                                          && x.Field<string>("EstSlotTime15min") == item.Key.EstSlotTime15min
                                                          && x.Field<string>("DoorNo") == item.Key.DoorNo
                                                          ).ToList();
                    for (int i = 0; i < data.Count; i++)
                    {
                        data[i]["FixedSlotID"] = dummy.ToString();
                        dtAlternateSlot.AcceptChanges();
                    }
                    dummy = dummy + 1;
                }


                //  Update Door Name

                foreach (var item in lstDoorNoType)
                {
                    var doorNameUpdate = from c in dtAlternateSlot.AsEnumerable()
                                         where c.Field<string>("DoorNo") == Convert.ToString(item.SiteDoorNumberID)
                                         select c;
                    foreach (var items in doorNameUpdate)
                    {
                        items["DoorNameDisplay"] = item.DoorType.DoorType;
                        items["DoorNoDisplay"] = item.DoorNo.DoorNumber;
                    }
                }

                var dtAlternateSlotFinal = (from c in dtAlternateSlot.AsEnumerable()
                                            group c by new
                                            {
                                                Vendor = c.Field<string>("Vendor"),
                                                Pallets = c.Field<string>("Pallets"),
                                                Cartoons = c.Field<string>("Cartoons"),
                                                Lines = c.Field<string>("Lines"),
                                                Lifts = c.Field<string>("Lifts"),
                                                Color = c.Field<string>("Color"),
                                                DoorNo = c.Field<string>("DoorNo"),
                                                FixedSlotID = c.Field<string>("FixedSlotID"),
                                                // SlotTimeID = c.Field<string>("SlotTimeID"),
                                                SiteDoorNumberID = c.Field<string>("SiteDoorNumberID"),
                                                SupplierType = c.Field<string>("SupplierType"),
                                                AllocationType = c.Field<string>("AllocationType"),
                                                ArrSlotTime15min = c.Field<string>("ArrSlotTime15min"),
                                                EstSlotTime15min = c.Field<string>("EstSlotTime15min"),
                                                DoorNameDisplay = c.Field<string>("DoorNameDisplay"),
                                                DoorNoDisplay = c.Field<string>("DoorNoDisplay"),
                                                VendorDisplay = c.Field<string>("Vendor"),
                                                BookingID = c.Field<string>("BookingID"),
                                                BookingStatus = c.Field<string>("BookingStatus"),
                                                BookingStatusID = c.Field<string>("BookingStatusID"),
                                                Weekday = c.Field<string>("Weekday")
                                            } into g
                                            select new AlternateSlot
                                            {
                                                Vendor = g.Key.Vendor,
                                                Pallets = g.Key.Pallets,
                                                Cartoons = g.Key.Cartoons,
                                                Lines = g.Key.Lines,
                                                Lifts = g.Key.Lifts,
                                                Color = g.Key.Color,
                                                DoorNo = g.Key.DoorNo,
                                                FixedSlotID = g.Key.FixedSlotID,
                                                SlotTimeID = "",
                                                SiteDoorNumberID = g.Key.SiteDoorNumberID,
                                                SupplierType = g.Key.SupplierType,
                                                AllocationType = g.Key.AllocationType,
                                                ArrSlotTime15min = g.Key.ArrSlotTime15min,
                                                EstSlotTime15min = g.Key.EstSlotTime15min,
                                                DoorNameDisplay = g.Key.DoorNameDisplay,
                                                DoorNoDisplay = g.Key.DoorNoDisplay,
                                                VendorDisplay = g.Key.Vendor,
                                                ArrSlotTime = "",
                                                EstSlotTime = "",
                                                BookingID = g.Key.BookingID,
                                                BookingStatus = g.Key.BookingStatus,
                                                BookingStatusID = g.Key.BookingStatusID,
                                                Weekday = g.Key.Weekday
                                            }).ToList();



                // set Orange color           
                foreach (var item in SlotFinal)
                {

                    var dataCheckExtraRow = dtAlternateSlotFinal.Where(x => x.DoorNo == item.DoorNo && x.Color.Trim() != "White" && x.ArrSlotTime15min == item.ArrSlotTime && x.EstSlotTime15min == item.EstSlotTime && x.Weekday == item.Weekday);
                    if (dataCheckExtraRow.Count() > 0)
                    {
                        // remove extra white if there is already other color data is present
                        AlternateSlot removedata = dtAlternateSlotFinal.ToList().Where(x => x.DoorNo == item.DoorNo && x.Color.Trim() == "White" && x.ArrSlotTime15min == item.ArrSlotTime && x.EstSlotTime15min == item.EstSlotTime && x.Weekday == item.Weekday).FirstOrDefault();
                        dtAlternateSlotFinal.Remove(removedata);
                        if (dataCheckExtraRow.Count() >= 2)
                        {
                            int count = 1;
                            foreach (var itemdataCheckExtraRow in dataCheckExtraRow)
                            {
                                if (count != 1 && itemdataCheckExtraRow.Color.Trim() != "GreenYellow")
                                {
                                    itemdataCheckExtraRow.Color = "Orange";
                                }
                                count++;
                            }
                        }
                    }
                }

                // data for pop up
                var SetMinMaxTimeForPallets = (from x in dtAlternateSlot.AsEnumerable()
                                               where x.Field<string>("Color").Trim() != "White"
                                               //where x.Field<string>("DoorNo") == item.DoorNo && x.Field<string>("Color").Trim() != "White" 
                                               //&& x.Field<string>("ArrSlotTime15min") == item.ArrSlotTime && x.Field<string>("EstSlotTime15min") == item.EstSlotTime
                                               group x by new
                                               {
                                                   Vendor = x.Field<string>("Vendor"),
                                                   Color = x.Field<string>("Color"),
                                                   DoorNo = x.Field<string>("DoorNo"),
                                                   FixedSlotID = x.Field<string>("FixedSlotID"),
                                                   SupplierType = x.Field<string>("SupplierType"),
                                                  
                                                   AllocationType = x.Field<string>("AllocationType"),
                                                   WeekDay = x.Field<string>("WeekDay"),
                                                   BookingID = x.Field<string>("BookingID")
                                               } into g
                                               select new AlternateSlot
                                               {
                                                   Color = g.Key.Color,
                                                   Vendor = g.Key.Vendor,
                                                   DoorNo = g.Key.DoorNo,
                                                   FixedSlotID = g.Key.FixedSlotID,
                                                   ArrSlotTime = g.Min(x => x.Field<string>("ArrSlotTime")),
                                                   EstSlotTime = g.Max(x => x.Field<string>("EstSlotTime")),
                                                   SlotStartDay = g.Min(x => x.Field<string>("SlotStartDay")),
                                                   siTimeSlotOrderBYID = Convert.ToString(g.Min(x => Int32.Parse(x.Field<string>("TimeSlotOrderBYID")))),
                                                   SlotTimeID = Convert.ToString(g.Min(x => Int32.Parse(x.Field<string>("SlotTimeID")))),// Minimun slot id for click
                                                   Weekday = g.Key.WeekDay,
                                                   BookingID = g.Key.BookingID
                                               }).ToList();

                string StartDay = string.Empty;

                string EndDay = string.Empty;

                if (lstWeekSetup[0].StartWeekday.ToUpper() == "SUN")
                {
                    StartDay = "1";
                }
                else if (lstWeekSetup[0].StartWeekday.ToUpper() == "MON")
                {
                    StartDay = "2";
                }
                else if (lstWeekSetup[0].StartWeekday.ToUpper() == "TUE")
                {
                    StartDay = "3";
                }
                else if (lstWeekSetup[0].StartWeekday.ToUpper() == "WED")
                {
                    StartDay = "4";
                }
                else if (lstWeekSetup[0].StartWeekday.ToUpper() == "THU")
                {
                    StartDay = "5";
                }
                else if (lstWeekSetup[0].StartWeekday.ToUpper() == "FRI")
                {
                    StartDay = "6";
                }
                else if (lstWeekSetup[0].StartWeekday.ToUpper() == "SAT")
                {
                    StartDay = "7";
                }

                if (lstWeekSetup[0].EndWeekday.ToUpper() == "SUN")
                {
                    EndDay = "1";
                }
                else if (lstWeekSetup[0].EndWeekday.ToUpper() == "MON")
                {
                    EndDay = "2";
                }
                else if (lstWeekSetup[0].EndWeekday.ToUpper() == "TUE")
                {
                    EndDay = "3";
                }
                else if (lstWeekSetup[0].EndWeekday.ToUpper() == "WED")
                {
                    EndDay = "4";
                }
                else if (lstWeekSetup[0].EndWeekday.ToUpper() == "THU")
                {
                    EndDay = "5";
                }
                else if (lstWeekSetup[0].EndWeekday.ToUpper() == "FRI")
                {
                    EndDay = "6";
                }
                else if (lstWeekSetup[0].EndWeekday.ToUpper() == "SAT")
                {
                    EndDay = "7";
                }

                var SetMinMaxTimeForPalletsOld = SetMinMaxTimeForPallets.Where(x => x.Weekday == StartDay).ToList();
                var SetMinMaxTimeForPalletsNew = SetMinMaxTimeForPallets.Where(x => x.Weekday == EndDay).ToList();

                //for across the day
                for (int i = 0; i < SetMinMaxTimeForPalletsOld.Count; i++)
                {
                    if (SetMinMaxTimeForPalletsNew.FindAll(x => x.Color == SetMinMaxTimeForPalletsOld[i].Color && x.Vendor == SetMinMaxTimeForPalletsOld[i].Vendor && x.DoorNo == SetMinMaxTimeForPalletsOld[i].DoorNo
                    && x.FixedSlotID == SetMinMaxTimeForPalletsOld[i].FixedSlotID
                    && x.BookingID == SetMinMaxTimeForPalletsOld[i].BookingID).Count > 0)
                    {
                        foreach (var z in SetMinMaxTimeForPalletsNew.FindAll(x => x.Color == SetMinMaxTimeForPalletsOld[i].Color && x.Vendor == SetMinMaxTimeForPalletsOld[i].Vendor && x.DoorNo == SetMinMaxTimeForPalletsOld[i].DoorNo
                        && x.FixedSlotID == SetMinMaxTimeForPalletsOld[i].FixedSlotID
                        && x.BookingID == SetMinMaxTimeForPalletsOld[i].BookingID))
                        {
                            z.ArrSlotTime = SetMinMaxTimeForPalletsOld[i].ArrSlotTime;
                            z.SlotStartDay = SetMinMaxTimeForPalletsOld[i].SlotStartDay;
                            z.siTimeSlotOrderBYID = SetMinMaxTimeForPalletsOld[i].siTimeSlotOrderBYID;
                            z.SlotTimeID = SetMinMaxTimeForPalletsOld[i].SlotTimeID;
                        }
                    }
                    else
                    {
                        SetMinMaxTimeForPalletsNew.Add(SetMinMaxTimeForPalletsOld[i]);
                    }
                }

                SetMinMaxTimeForPallets = SetMinMaxTimeForPalletsNew;
                //change for time having in both weekday

                foreach (var itemSetMinMaxTimeForPallets in SetMinMaxTimeForPallets)
                {
                    var updateMinMaxArrival = dtAlternateSlotFinal.ToList().Where(x => x.DoorNo == itemSetMinMaxTimeForPallets.DoorNo && x.Color.Trim() != "White"
                        // && x.ArrSlotTime15min == itemSetMinMaxTimeForPallets.ArrSlotTime && x.EstSlotTime15min == itemSetMinMaxTimeForPallets.EstSlotTime
                                              && x.Vendor == itemSetMinMaxTimeForPallets.Vendor && x.FixedSlotID == itemSetMinMaxTimeForPallets.FixedSlotID
                                              && x.BookingID == itemSetMinMaxTimeForPallets.BookingID);
                    //.OrderBy(x => x.Vendor).ThenBy(x => x.DoorNo).ThenBy(x => x.FixedSlotID);

                    foreach (var items in updateMinMaxArrival)
                    {
                        items.ArrSlotTime = itemSetMinMaxTimeForPallets.ArrSlotTime;
                        items.EstSlotTime = itemSetMinMaxTimeForPallets.EstSlotTime;
                        items.SlotStartDay = itemSetMinMaxTimeForPallets.SlotStartDay;
                        items.siTimeSlotOrderBYID = itemSetMinMaxTimeForPallets.siTimeSlotOrderBYID;
                        items.SlotTimeID = itemSetMinMaxTimeForPallets.SlotTimeID;
                        items.Vendor = itemSetMinMaxTimeForPallets.Vendor;
                    }
                }

                ///for black bug (Door closed)
                var SetMinMaxTimeForPallets_Black = (from x in dtAlternateSlot.AsEnumerable()
                                                     where x.Field<string>("Color").Trim() == "Black"
                                                     group x by new
                                                     {
                                                         Color = x.Field<string>("Color"),
                                                         DoorNo = x.Field<string>("DoorNo"),
                                                         ArrSlotTime15min = x.Field<string>("ArrSlotTime15min"),
                                                         EstSlotTime15min = x.Field<string>("EstSlotTime15min"),
                                                         Weekday = x.Field<string>("Weekday"),
                                                         BookingID = x.Field<string>("BookingID")
                                                     } into g
                                                     select new AlternateSlot
                                                     {
                                                         DoorNo = g.Key.DoorNo,
                                                         SlotStartDay = g.Min(x => x.Field<string>("SlotStartDay")),
                                                         siTimeSlotOrderBYID = Convert.ToString(g.Min(x => Int32.Parse(x.Field<string>("TimeSlotOrderBYID")))),
                                                         SlotTimeID = Convert.ToString(g.Min(x => Int32.Parse(x.Field<string>("SlotTimeID")))),// Minimun slot id for click
                                                         ArrSlotTime15min = g.Key.ArrSlotTime15min,
                                                         EstSlotTime15min = g.Key.EstSlotTime15min,
                                                         ArrSlotTime = g.Min(x => x.Field<string>("ArrSlotTime")),
                                                         EstSlotTime = g.Max(x => x.Field<string>("EstSlotTime")),
                                                         Weekday = g.Max(x => x.Field<string>("Weekday"))
                                                     })
                                                     .ToList();

                foreach (var itemSetMinMaxTimeForPallets in SetMinMaxTimeForPallets_Black)
                {

                    var updateMinMax = dtAlternateSlotFinal.ToList().Where(x => x.DoorNo == itemSetMinMaxTimeForPallets.DoorNo && x.Color.Trim() == "Black"
                                              && x.ArrSlotTime15min == itemSetMinMaxTimeForPallets.ArrSlotTime15min && x.EstSlotTime15min == itemSetMinMaxTimeForPallets.EstSlotTime15min
                                              && x.Weekday == itemSetMinMaxTimeForPallets.Weekday);
                    foreach (var items in updateMinMax)
                    {
                        items.SlotStartDay = itemSetMinMaxTimeForPallets.SlotStartDay;
                        items.siTimeSlotOrderBYID = itemSetMinMaxTimeForPallets.siTimeSlotOrderBYID;
                        items.SlotTimeID = itemSetMinMaxTimeForPallets.SlotTimeID;
                        items.ArrSlotTime = itemSetMinMaxTimeForPallets.ArrSlotTime;
                        items.EstSlotTime = itemSetMinMaxTimeForPallets.EstSlotTime;
                    }
                }




                var SetMinMaxTimeForPallets_White = (from x in dtAlternateSlot.AsEnumerable()
                                                     where x.Field<string>("Color").Trim() == "White"
                                                     group x by new
                                                     {
                                                         Color = x.Field<string>("Color"),
                                                         DoorNo = x.Field<string>("DoorNo"),
                                                         ArrSlotTime15min = x.Field<string>("ArrSlotTime15min"),
                                                         EstSlotTime15min = x.Field<string>("EstSlotTime15min"),
                                                         Weekday = x.Field<string>("Weekday"),
                                                         BookingID = x.Field<string>("BookingID")
                                                     } into g
                                                     select new AlternateSlot
                                                     {
                                                         DoorNo = g.Key.DoorNo,
                                                         SlotStartDay = g.Min(x => x.Field<string>("SlotStartDay")),
                                                         siTimeSlotOrderBYID = Convert.ToString(g.Min(x => Int32.Parse(x.Field<string>("TimeSlotOrderBYID")))),
                                                         SlotTimeID = Convert.ToString(g.Min(x => Int32.Parse(x.Field<string>("SlotTimeID")))),// Minimun slot id for click
                                                         ArrSlotTime15min = g.Key.ArrSlotTime15min,
                                                         EstSlotTime15min = g.Key.EstSlotTime15min,
                                                         ArrSlotTime = g.Min(x => x.Field<string>("ArrSlotTime")),
                                                         EstSlotTime = g.Max(x => x.Field<string>("EstSlotTime")),
                                                         Weekday = g.Max(x => x.Field<string>("Weekday")),
                                                         BookingID = g.Key.BookingID
                                                     })
                                                     .ToList();
                foreach (var itemSetMinMaxTimeForPallets in SetMinMaxTimeForPallets_White)
                {

                    var updateMinMax = dtAlternateSlotFinal.ToList().Where(x => x.DoorNo == itemSetMinMaxTimeForPallets.DoorNo && x.Color.Trim() == "White"
                                              && x.ArrSlotTime15min == itemSetMinMaxTimeForPallets.ArrSlotTime15min && x.EstSlotTime15min == itemSetMinMaxTimeForPallets.EstSlotTime15min
                                              && x.DoorNo == itemSetMinMaxTimeForPallets.DoorNo && x.Weekday == itemSetMinMaxTimeForPallets.Weekday
                                              && x.BookingID == itemSetMinMaxTimeForPallets.BookingID);
                    foreach (var items in updateMinMax)
                    {
                        items.SlotStartDay = itemSetMinMaxTimeForPallets.SlotStartDay;
                        items.siTimeSlotOrderBYID = itemSetMinMaxTimeForPallets.siTimeSlotOrderBYID;
                        items.SlotTimeID = itemSetMinMaxTimeForPallets.SlotTimeID;
                        items.ArrSlotTime = itemSetMinMaxTimeForPallets.ArrSlotTime;
                        items.EstSlotTime = itemSetMinMaxTimeForPallets.EstSlotTime;
                        items.BookingID = itemSetMinMaxTimeForPallets.BookingID;
                    }
                }




                // data for pop up and blank consecutive name of vendor
                string PreviousVendorname = string.Empty;
                string PreviousFixedSlotID = string.Empty;
                string PreviousDoor = string.Empty;
                string PreviousSlotID = string.Empty;
                string PreviousBookingID = string.Empty;

                var filter = dtAlternateSlotFinal.ToList().OrderBy(x => x.Vendor).ThenBy(x => x.DoorNo).ThenBy(x => x.FixedSlotID);
                foreach (var items in filter)
                {
                    string Door = items.DoorNo.ToString();
                    string FixedSlotID = items.FixedSlotID.ToString();
                    string Vendor = items.Vendor.ToString();
                    string BookingID = items.BookingID;

                    if (items.Vendor.ToString().Trim() == PreviousVendorname && PreviousFixedSlotID == FixedSlotID && PreviousDoor == Door
                        && PreviousBookingID == items.BookingID)
                    {
                        items.VendorDisplay = items.Vendor;
                        items.Vendor = "";
                        items.SlotTimeID = items.SlotTimeID;

                    }
                    else
                    {
                        PreviousVendorname = "";
                        PreviousFixedSlotID = "";
                        PreviousDoor = "";
                        PreviousSlotID = "";
                    }
                    PreviousVendorname = Vendor;
                    PreviousFixedSlotID = FixedSlotID;
                    PreviousDoor = Door;
                    PreviousBookingID = BookingID;
                }


                SetMinMaxTimeForPallets = (from x in dtAlternateSlot.AsEnumerable()
                                           //where x.Field<string>("Color").Trim() != "White"
                                           //&& x.Field<string>("Vendor").Trim()=="066 - METSA BOARD CORPORATION"
                                           // && x.Field<string>("TimeSlotOrderBYID").Trim() == "280"
                                           //where x.Field<string>("DoorNo") == "106" 
                                           //&& (x.Field<string>("ArrSlotTime15min") == "04:45" ||  x.Field<string>("ArrSlotTime15min") == "05:00" || x.Field<string>("EstSlotTime15min") == "05:30")
                                           group x by new
                                           {
                                               ArrSlotTime15min = x.Field<string>("ArrSlotTime15min"),
                                               EstSlotTime15min = x.Field<string>("EstSlotTime15min"),
                                               BookingID = x.Field<string>("BookingID")
                                           } into g
                                           select new AlternateSlot
                                           {
                                               ArrSlotTime15min = g.Key.ArrSlotTime15min,
                                               EstSlotTime15min = g.Key.EstSlotTime15min,
                                               SlotTimeID = Convert.ToString(g.Min(x => Int32.Parse(x.Field<string>("SlotTimeID")))),// Minimun slot id for click
                                               BookingID = g.Key.BookingID
                                           }).ToList();



                foreach (var itemSetMinMaxTimeForPallets in SetMinMaxTimeForPallets)
                {

                    var updateMinMaxArrival = dtAlternateSlotFinal.ToList().Where(x => x.Color.Trim() != "White"
                                             && x.ArrSlotTime15min == itemSetMinMaxTimeForPallets.ArrSlotTime15min && x.EstSlotTime15min == itemSetMinMaxTimeForPallets.EstSlotTime15min
                                             && x.BookingID == itemSetMinMaxTimeForPallets.BookingID).ToList();
                    //.OrderBy(x => x.Vendor).ThenBy(x => x.DoorNo).ThenBy(x => x.FixedSlotID);

                    foreach (var items in updateMinMaxArrival)
                    {
                        items.SlotTimeID = itemSetMinMaxTimeForPallets.SlotTimeID;
                        items.BookingID = itemSetMinMaxTimeForPallets.BookingID;
                    }

                }

                oAPPBOK_BookingBE.Action = "AllTodayBookingWithVendorDetails";
                oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime; 
                DataTable dtAlternateSlotFinalTable = new DataTable();
                DataTable dtAlternateSlotNewTable = new DataTable();

                dtAlternateSlotFinalTable.Columns.Add("SupplierType", typeof(string));
                dtAlternateSlotFinalTable.Columns.Add("BookingID", typeof(string));
                dtAlternateSlotFinalTable.Columns.Add("BookingTypeID", typeof(string));
                dtAlternateSlotFinalTable.Columns.Add("Issue", typeof(string));
                dtAlternateSlotFinalTable.Columns.Add("BookingStatus", typeof(string));
                dtAlternateSlotFinalTable.Columns.Add("Pallets", typeof(string));
                dtAlternateSlotFinalTable.Columns.Add("Lifts", typeof(string));
                dtAlternateSlotFinalTable.Columns.Add("Cartoons", typeof(string));
                dtAlternateSlotFinalTable.Columns.Add("Lines", typeof(string));
                dtAlternateSlotFinalTable.Columns.Add("Vendor", typeof(string));
                dtAlternateSlotFinalTable.Columns.Add("Carrier", typeof(string));

                DataTable dt = oAPPBOK_BookingBAL.GetAllTodayBookingWithVendorDetailsBAL(oAPPBOK_BookingBE);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dtAlternateSlotFinalTable.Rows.Add(new object[]
                    {
                        Convert.ToString(dt.Rows[i]["SupplierType"]),
                        Convert.ToString(dt.Rows[i]["BookingID"]),
                        Convert.ToString(dt.Rows[i]["BookingTypeID"]),
                        Convert.ToString(dt.Rows[i]["Issue"]),
                        Convert.ToString(dt.Rows[i]["BookingStatus"]),
                        Convert.ToString(dt.Rows[i]["Pallets"]),
                        Convert.ToString(dt.Rows[i]["Lifts"]),
                        Convert.ToString(dt.Rows[i]["Cartoons"]),
                        Convert.ToString(dt.Rows[i]["Lines"]),
                        Convert.ToString(dt.Rows[i]["Vendor"]),
                        Convert.ToString(dt.Rows[i]["CarrierName"])

                   });
                }

                if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
                {
                    oAPPBOK_BookingBE.Action = "AllTodayBookingWithVendorDetails";
                    oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                    oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1);


                    dtAlternateSlotNewTable.Columns.Add("SupplierType", typeof(string));
                    dtAlternateSlotNewTable.Columns.Add("BookingID", typeof(string));
                    dtAlternateSlotNewTable.Columns.Add("BookingTypeID", typeof(string));
                    dtAlternateSlotNewTable.Columns.Add("Issue", typeof(string));
                    dtAlternateSlotNewTable.Columns.Add("BookingStatus", typeof(string));
                    dtAlternateSlotNewTable.Columns.Add("Pallets", typeof(string));
                    dtAlternateSlotNewTable.Columns.Add("Lifts", typeof(string));
                    dtAlternateSlotNewTable.Columns.Add("Cartoons", typeof(string));
                    dtAlternateSlotNewTable.Columns.Add("Lines", typeof(string));
                    dtAlternateSlotNewTable.Columns.Add("Vendor", typeof(string));
                    dtAlternateSlotNewTable.Columns.Add("Carrier", typeof(string));

                    DataTable dt1 = oAPPBOK_BookingBAL.GetAllTodayBookingWithVendorDetailsBAL(oAPPBOK_BookingBE);
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        dtAlternateSlotNewTable.Rows.Add(new object[]
                        {
                        Convert.ToString(dt1.Rows[i]["SupplierType"]),
                        Convert.ToString(dt1.Rows[i]["BookingID"]),
                        Convert.ToString(dt1.Rows[i]["BookingTypeID"]),
                        Convert.ToString(dt1.Rows[i]["Issue"]),
                        Convert.ToString(dt1.Rows[i]["BookingStatus"]),
                        Convert.ToString(dt1.Rows[i]["Pallets"]),
                        Convert.ToString(dt1.Rows[i]["Lifts"]),
                        Convert.ToString(dt1.Rows[i]["Cartoons"]),
                        Convert.ToString(dt1.Rows[i]["Lines"]),
                        Convert.ToString(dt1.Rows[i]["Vendor"]),
                        Convert.ToString(dt1.Rows[i]["CarrierName"])

                       });
                    }
                }

                dtAlternateSlotFinalTable.Merge(dtAlternateSlotNewTable);


                DataTable dtAlternateSlotFinalTableBind = new DataTable();
                dtAlternateSlotFinalTableBind.Columns.Add("ArrSlotTime", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("EstSlotTime", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("Vendor", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("Pallets", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("Cartoons", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("Lines", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("Lifts", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("Color", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("DoorNo", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("FixedSlotID", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("SlotTimeID", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("SiteDoorNumberID", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("SupplierType", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("AllocationType", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("ArrSlotTime15min", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("EstSlotTime15min", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("DoorNameDisplay", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("DoorNoDisplay", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("VendorDisplay", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("SlotStartDay", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("iTimeSlotOrderBYID", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("BookingID", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("BookingStatus", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("BookingStatusID", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("WeekDay", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("DropDownText", typeof(string));
                dtAlternateSlotFinalTableBind.Columns.Add("DropDownValue", typeof(string));

                for (int i = 0; i < dtAlternateSlotFinal.Count(); i++)
                {
                    string fixedSlotid = "-1";
                    CarrierDetails vd = new CarrierDetails();

                    string[] StartTime = dtAlternateSlotFinal[i].ArrSlotTime.Split(':');
                    TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);

                    if (SlotDoorCounter.ContainsKey(time1 + dtAlternateSlotFinal[i].DoorNo.ToString()))
                    {
                        vd = SlotDoorCounter[time1 + dtAlternateSlotFinal[i].DoorNo.ToString()];
                        if (vd.CurrentFixedSlotID != null)
                        {
                            fixedSlotid = vd.CurrentFixedSlotID.ToString();
                        }
                    }

                    string weekday = string.Empty;
                    if (dtAlternateSlotFinal[i].SlotStartDay.ToUpper() == "SUN")
                    {
                        weekday = "1";
                    }
                    else if (dtAlternateSlotFinal[i].SlotStartDay.ToUpper() == "MON")
                    {
                        weekday = "2";
                    }
                    else if (dtAlternateSlotFinal[i].SlotStartDay.ToUpper() == "TUE")
                    {
                        weekday = "3";
                    }
                    else if (dtAlternateSlotFinal[i].SlotStartDay.ToUpper() == "WED")
                    {
                        weekday = "4";
                    }
                    else if (dtAlternateSlotFinal[i].SlotStartDay.ToUpper() == "THU")
                    {
                        weekday = "5";
                    }
                    else if (dtAlternateSlotFinal[i].SlotStartDay.ToUpper() == "FRI")
                    {
                        weekday = "6";
                    }
                    else if (dtAlternateSlotFinal[i].SlotStartDay.ToUpper() == "SAT")
                    {
                        weekday = "7";
                    }

                    dtAlternateSlotFinalTableBind.Rows.Add(new object[] { 
                    dtAlternateSlotFinal[i].ArrSlotTime,
                    dtAlternateSlotFinal[i].EstSlotTime,
                    dtAlternateSlotFinal[i].Vendor,
                    dtAlternateSlotFinal[i].Pallets,
                    dtAlternateSlotFinal[i].Cartoons,
                    dtAlternateSlotFinal[i].Lines,
                    dtAlternateSlotFinal[i].Lifts,
                    dtAlternateSlotFinal[i].Color,
                    dtAlternateSlotFinal[i].DoorNo,
                    dtAlternateSlotFinal[i].FixedSlotID,            
                    dtAlternateSlotFinal[i].SlotTimeID,
                    dtAlternateSlotFinal[i].SiteDoorNumberID,
                    dtAlternateSlotFinal[i].SupplierType,
                    dtAlternateSlotFinal[i].AllocationType,
                    dtAlternateSlotFinal[i].ArrSlotTime15min,
                    dtAlternateSlotFinal[i].EstSlotTime15min,
                    dtAlternateSlotFinal[i].DoorNameDisplay,
                    dtAlternateSlotFinal[i].DoorNoDisplay,
                    dtAlternateSlotFinal[i].VendorDisplay,
                    dtAlternateSlotFinal[i].SlotStartDay,
                    dtAlternateSlotFinal[i].siTimeSlotOrderBYID,
                     dtAlternateSlotFinal[i].BookingID,
                    dtAlternateSlotFinal[i].BookingStatus,
                    dtAlternateSlotFinal[i].BookingStatusID,
                     dtAlternateSlotFinal[i].Weekday,
                     dtAlternateSlotFinal[i].ArrSlotTime15min +"-"+dtAlternateSlotFinal[i].EstSlotTime15min +" ("+ dtAlternateSlotFinal[i].DoorNoDisplay+" / "+dtAlternateSlotFinal[i].DoorNameDisplay +" )" ,                    
                    dtAlternateSlotFinal[i].SlotTimeID + "," + dtAlternateSlotFinal[i].DoorNo 
                   + "," + dtAlternateSlotFinal[i].DoorNoDisplay + "," + dtAlternateSlotFinal[i].ArrSlotTime15min + "," 
                   + fixedSlotid + "," + dtAlternateSlotFinal[i].SlotStartDay + "," + dtAlternateSlotFinal[i].siTimeSlotOrderBYID
                    });
                }

                dtAlternateSlotFinalTableBind.DefaultView.Sort = "WeekDay asc, SlotTimeID asc";
                dtAlternateSlotFinalTableBind = dtAlternateSlotFinalTableBind.DefaultView.ToTable();

                // Logic for Setting grey color
                decimal slotlength = Math.Ceiling(Convert.ToDecimal(actSlotLength * 15) / Convert.ToDecimal(15));

                int Count = 0;
                string[] OpenDoor = strOpenDoorIds.Split(',');
                string[] OpenDoorVechileSetup = strVehicleDoorTypeMatrix.Split(',');
                string[] OpenVehicleDoorTypeMatrixSKU = strVehicleDoorTypeMatrixSKU.Split(',');
                for (int i = 0; i < dtAlternateSlotFinalTableBind.Rows.Count; i++)
                {
                    if (isStandardBookingRequired == true && iSKUSiteDoorID != 0)
                    {
                        if (!OpenVehicleDoorTypeMatrixSKU.Contains(dtAlternateSlotFinalTableBind.Rows[i]["SiteDoorNumberID"].ToString().Trim()))
                        {
                            if (dtAlternateSlotFinalTableBind.Rows[i]["Color"].ToString().Trim() == "White" || dtAlternateSlotFinalTableBind.Rows[i]["Color"].ToString().Trim() == "LightGray")
                            {
                                dtAlternateSlotFinalTableBind.Rows[i]["Color"] = "Black";
                            }
                        }
                    }
                    else
                    {
                        if (!OpenDoorVechileSetup.Contains(dtAlternateSlotFinalTableBind.Rows[i]["SiteDoorNumberID"].ToString().Trim()))
                        {
                            if (dtAlternateSlotFinalTableBind.Rows[i]["Color"].ToString().Trim() == "White" || dtAlternateSlotFinalTableBind.Rows[i]["Color"].ToString().Trim() == "LightGray")
                            {
                                dtAlternateSlotFinalTableBind.Rows[i]["Color"] = "Black";
                            }
                        }
                    }
                    if (!OpenDoorVechileSetup.Contains(dtAlternateSlotFinalTableBind.Rows[i]["SiteDoorNumberID"].ToString().Trim()) && !OpenDoor.Contains(dtAlternateSlotFinalTableBind.Rows[i]["SlotTimeID"].ToString().Trim() + "@" + dtAlternateSlotFinalTableBind.Rows[i]["DoorNo"].ToString().Trim()))
                    {
                        if (dtAlternateSlotFinalTableBind.Rows[i]["Color"].ToString().Trim() == "White" || dtAlternateSlotFinalTableBind.Rows[i]["Color"].ToString().Trim() == "LightGray")
                        {
                            dtAlternateSlotFinalTableBind.Rows[i]["Color"] = "Black";
                        }
                    }
                }

                
                // insufficient slot
                var Distinctdoor = dtAlternateSlotFinalTableBind.AsEnumerable().Select(x => x.Field<string>("DoorNo")).Distinct();
                foreach (var item in Distinctdoor)
                {
                    var filterDoorWiseData = (from c in dtAlternateSlotFinalTableBind.AsEnumerable()
                                              where c.Field<string>("DoorNo") == item
                                              group c by new
                                              {
                                                  Vendor = c.Field<string>("Vendor"),
                                                  Color = c.Field<string>("Color"),
                                                  DoorNo = c.Field<string>("DoorNo"),
                                                  ArrSlotTime15min = c.Field<string>("ArrSlotTime15min"),
                                                  EstSlotTime15min = c.Field<string>("EstSlotTime15min"),
                                                  WeekDay = c.Field<string>("WeekDay")
                                              } into g
                                              select new AlternateSlot
                                              {
                                                  Vendor = g.Key.Vendor,
                                                  Color = g.Key.Color,
                                                  DoorNo = g.Key.DoorNo,
                                                  ArrSlotTime15min = g.Key.ArrSlotTime15min,
                                                  EstSlotTime15min = g.Key.EstSlotTime15min,
                                                  Weekday = g.Key.WeekDay
                                              }).OrderBy(x => x.Weekday).ThenBy(x => x.ArrSlotTime15min).ThenBy(x => x.EstSlotTime15min).ThenBy(x => x.DoorNo).ToList();
                    for (int i = 0; i < filterDoorWiseData.Count(); i++)
                    {
                        if (filterDoorWiseData[i].Color.Trim() == "White")
                        {
                            Count++;
                        }
                        else
                        {
                            int slotlengthloop = Convert.ToInt32(slotlength);
                            for (int j = 1; j <= Count; j++)
                            {
                                if (slotlengthloop == 1)
                                {
                                    break;
                                }
                                else
                                {
                                    if (i != 0)
                                    {
                                        filterDoorWiseData[i - j].Color = "LightGray";
                                    }
                                    slotlengthloop = Convert.ToInt32(slotlengthloop) - 1;
                                }
                            }

                            Count = 0;
                        }
                    }

                    if (Count != 0)
                    {
                        for (int i = filterDoorWiseData.Count(); i > filterDoorWiseData.Count() - 1; i--)
                        {
                            int slotlengthloop = Convert.ToInt32(slotlength);
                            for (int j = 1; j <= Count; j++)
                            {
                                if (slotlengthloop == 1)
                                {
                                    break;
                                }
                                else
                                {
                                    if (i != 0)
                                    {
                                        filterDoorWiseData[i - j].Color = "LightGray";

                                    }
                                    slotlengthloop = Convert.ToInt32(slotlengthloop) - 1;
                                }
                            }
                        }
                        Count = 0;
                    }

                    foreach (var items in filterDoorWiseData)
                    {
                        if (items.Color.Trim() == "LightGray")
                        {
                            var updateInsufficientSlot = (from c in dtAlternateSlotFinalTableBind.AsEnumerable()
                                                          where c.Field<string>("DoorNo") == item &&
                                                          c.Field<string>("ArrSlotTime15min") == items.ArrSlotTime15min
                                                          && c.Field<string>("EstSlotTime15min") == items.EstSlotTime15min
                                                          select c).ToList();
                            foreach (var itemtoUpdate in updateInsufficientSlot)
                            {
                                itemtoUpdate["Color"] = "LightGray";
                            }
                        }
                    }
                }

                

                if ((dtAlternateSlotFinalTableBind.AsEnumerable().Where(x => x.Field<string>("Color").Trim() == "White").Count() > 0))
                {
                    // ddlAlternateslotTime.DataSource = dtAlternateSlotFinalTableBind.AsEnumerable().Where(x => x.Field<string>("Color").Trim() == "White").CopyToDataTable();

                    if (hdnCurrentRole.Value == "Carrier")
                    {
                        var beforeSlotTime = Convert.ToInt32(hdnBeforeSlotTimeID.Value);
                        var afterSlotTime = Convert.ToInt32(hdnAfterSlotTimeID.Value);
                        var data = dtAlternateSlotFinalTableBind.AsEnumerable().Where(x => x.Field<string>("Color").Trim() == "White");
                        var dataforDropDown = data.Where(x =>
                        (Convert.ToInt32(x.Field<string>("iTimeSlotOrderBYID")) >= beforeSlotTime
                            && Convert.ToInt32(x.Field<string>("iTimeSlotOrderBYID")) < afterSlotTime));

                        ddlAlternateslotTime.DataSource = dataforDropDown.CopyToDataTable();
                    }
                    else
                    {
                        ddlAlternateslotTime.DataSource = dtAlternateSlotFinalTableBind.AsEnumerable().Where(x => x.Field<string>("Color").Trim() == "White").CopyToDataTable();
                    }

                    ddlAlternateslotTime.DataTextField = "DropDownText";
                    ddlAlternateslotTime.DataValueField = "DropDownValue";
                    ddlAlternateslotTime.DataBind();
                }
                btnAlternateSlotCarrier.Visible = false;
                ddlAlternateslotTime.Items.Insert(0, new ListItem("--Select--", "0"));

                string FixHeader = "tableCol" + Convert.ToString(Distinctdoor.Count());
                int countID = 0;
                Table tblSlotNew = new Table();
                tblSlotNew.CellSpacing = 0;
                tblSlotNew.CellPadding = 0;
                tblSlotNew.CssClass = "tableData outerTable";
                tblSlotNew.ID = "Open_Text_GeneralNew";
                //tblSlotNew.Width = Unit.Pixel(1030);
                tblSlotNew.Attributes.Add("border-collapse", "collapse");

                TableRow trDoorNoNew = new TableRow();
                trDoorNoNew.TableSection = TableRowSection.TableHeader;
                TableHeaderCell tcDoorNoNew = new TableHeaderCell();
                tcDoorNoNew.CssClass = "timeCol";
                tcDoorNoNew.Text = "Time";
                trDoorNoNew.Cells.Add(tcDoorNoNew);

                var DistinctdoorTable = (from c in dtAlternateSlotFinalTableBind.AsEnumerable()
                                         select new
                                         {
                                             DoorNameDisplay = c.Field<string>("DoorNameDisplay"),
                                             DoorNoDisplay = c.Field<string>("DoorNoDisplay"),
                                             DoorNo = c.Field<string>("DoorNo")
                                         }
                                       ).Distinct().ToList();
                foreach (var item in DistinctdoorTable)
                {
                    tcDoorNoNew = new TableHeaderCell();
                    tcDoorNoNew.Text = item.DoorNoDisplay + " / " + item.DoorNameDisplay;
                    tcDoorNoNew.CssClass = "borderRightMain";
                    //tcDoorNoNew.Attributes.Add("width", "150px");
                    trDoorNoNew.Cells.Add(tcDoorNoNew);
                }
                tblSlotNew.Rows.Add(trDoorNoNew);

                var DistinctSlotFinal = from c in SlotFinal.AsEnumerable()
                                        group c by new { ArrSlotTime = c.ArrSlotTime, Weekday = c.Weekday } into t
                                        select new { ArrSlotTime = t.Key.ArrSlotTime, Weekday = t.Key.Weekday }
                                             ;

                foreach (var item in DistinctSlotFinal)
                {
                    TableRow tr = new TableRow();
                    tr.TableSection = TableRowSection.TableBody;
                    TableCell tc = new TableCell();
                    tc.Text = item.ArrSlotTime.ToString();
                    tc.CssClass = "timeCol";
               tc.CssClass = "timeCol innerTable";
                    tr.Cells.Add(tc);

                    foreach (var door in Distinctdoor)
                    {
                        var filterTableTdRow = (from c in dtAlternateSlotFinalTableBind.AsEnumerable()
                                                where c.Field<string>("DoorNo") == door &&
                                                 c.Field<string>("ArrSlotTime15min") == item.ArrSlotTime
                                                     && c.Field<string>("Weekday") == item.Weekday
                                                select new AlternateSlot
                                                {
                                                    Vendor = c.Field<string>("Vendor"),
                                                    Pallets = c.Field<string>("Pallets"),
                                                    Cartoons = c.Field<string>("Cartoons"),
                                                    Lines = c.Field<string>("Lines"),
                                                    Lifts = c.Field<string>("Lifts"),
                                                    Color = c.Field<string>("Color"),
                                                    DoorNo = c.Field<string>("DoorNo"),
                                                    FixedSlotID = c.Field<string>("FixedSlotID"),
                                                    SlotTimeID = c.Field<string>("SlotTimeID"),
                                                    SiteDoorNumberID = c.Field<string>("SiteDoorNumberID"),
                                                    SupplierType = c.Field<string>("SupplierType"),
                                                    AllocationType = c.Field<string>("AllocationType"),
                                                    ArrSlotTime15min = c.Field<string>("ArrSlotTime15min"),
                                                    EstSlotTime15min = c.Field<string>("EstSlotTime15min"),
                                                    DoorNameDisplay = c.Field<string>("DoorNameDisplay"),
                                                    DoorNoDisplay = c.Field<string>("DoorNoDisplay"),
                                                    ArrSlotTime = c.Field<string>("ArrSlotTime"),
                                                    EstSlotTime = c.Field<string>("EstSlotTime"),
                                                    VendorDisplay = c.Field<string>("VendorDisplay"),
                                                    SlotStartDay = c.Field<string>("SlotStartDay"),
                                                    siTimeSlotOrderBYID = c.Field<string>("iTimeSlotOrderBYID"),
                                                    BookingID = c.Field<string>("BookingID"),
                                                    BookingStatus = c.Field<string>("BookingStatus"),
                                                    BookingStatusID = c.Field<string>("BookingStatusID")
                                                }).ToList();

                        if (filterTableTdRow.Where(x => x.Color.Trim() == "Orange").Count() > 2)
                        {
                            // Remove consecutive data of orange with particular filter to avoid one more td
                            AlternateSlot RemoveData = filterTableTdRow.Where(x => x.Color == "Orange" && x.Vendor == "").FirstOrDefault();
                            filterTableTdRow.Remove(RemoveData);
                        }
                        if (filterTableTdRow.Count() >= 2)
                        {
                            // Remove extra white space
                            AlternateSlot RemoveData = filterTableTdRow.Where(x => x.Color.Trim() == "White" && x.Vendor == "").FirstOrDefault();
                            filterTableTdRow.Remove(RemoveData);
                        }

                        if (filterTableTdRow.Where(x => x.Color.Trim() == "Purple").Count() > 2)
                        {
                            // Remove consecutive data of Purple with particular filter to avoid one more td
                            AlternateSlot RemoveData = filterTableTdRow.Where(x => x.Color.Trim() == "Purple" && x.Vendor == "").FirstOrDefault();
                            filterTableTdRow.Remove(RemoveData);
                        }
                        tc = new TableCell();
                        tc.CssClass = "borderRightMain";
                        Table tblChild = new Table();
                  
                        tblChild.CssClass = "tableData innerTable";
                        TableRow trChild = new TableRow();

                        foreach (var filterTableTd in filterTableTdRow)
                        {
                            // count is used to create unique id of pop up
                            countID += 1;
                            TableCell tcChild = new TableCell();
                            tcChild.CssClass = "tableData";
                            Literal ddl = new Literal();
                            ddl.Text = filterTableTd.Vendor.ToString();

                            if (filterTableTdRow.Count > 1)
                            {
                                if (filterTableTd.Color.Trim() == "Green")
                                {
                                    tcChild.CssClass = "greenTd width85";
                                }
                                else if (filterTableTd.Color.Trim() == "Red")
                                {
                                    tcChild.CssClass = "redTd width85";
                                }
                                else if (filterTableTd.Color.Trim() == "Orange")
                                {
                                    tcChild.CssClass = "orangeTd width85";
                                }
                                else if (filterTableTd.Color.Trim() == "Black")
                                {
                                    tcChild.CssClass = "blueTd width85";
                                }
                                else if (filterTableTd.Color.Trim() == "LightGray")
                                {
                                    tcChild.CssClass = "greyTd width85";
                                }
                                else if (filterTableTd.Color.Trim() == "Pink")
                                {
                                    tcChild.CssClass = "pinkTd width85";
                                }
                                else if (filterTableTd.Color.Trim() == "GreenYellow")
                                {
                                    tcChild.CssClass = "greenYellowTd width85";
                                }
                                else if (filterTableTd.Color.Trim() == "Purple")
                                {
                                    tcChild.CssClass = "greenTd width85";
                                }
                                else
                                {
                                    tcChild.CssClass = "whiteTd ";
                                }
                            }
                            else
                            {
                                if (filterTableTd.Color.Trim() == "Green")
                                {
                                    tcChild.CssClass = "greenTd width170";
                                }
                                else if (filterTableTd.Color.Trim() == "Red")
                                {
                                    tcChild.CssClass = "redTd width170";
                                }
                                else if (filterTableTd.Color.Trim() == "Orange")
                                {
                                    tcChild.CssClass = "orangeTd width170";
                                }
                                else if (filterTableTd.Color.Trim() == "Black")
                                {
                                    tcChild.CssClass = "blueTd width170";
                                }
                                else if (filterTableTd.Color.Trim() == "LightGray")
                                {
                                    tcChild.CssClass = "greyTd width170";
                                }
                                else if (filterTableTd.Color.Trim() == "Pink")
                                {
                                    tcChild.CssClass = "pinkTd width170";
                                }
                                else if (filterTableTd.Color.Trim() == "GreenYellow")
                                {
                                    tcChild.CssClass = "greenYellowTd width170";
                                }
                                else if (filterTableTd.Color.Trim() == "Purple")
                                {
                                    tcChild.CssClass = "greenTd width170";
                                }
                                else
                                {
                                    tcChild.CssClass = "whiteTd";
                                }
                            }

                            if (filterTableTd.Color.Trim() != "Black" && filterTableTd.Color.Trim() != "White" && filterTableTd.Color.Trim() != "LightGray")
                            {
                                CreateDynamicDivNew(tcChild, filterTableTd.ArrSlotTime, filterTableTd.EstSlotTime == "25:00" ? "00:00" : filterTableTd.EstSlotTime, filterTableTd.Pallets, filterTableTd.Cartoons, filterTableTd.Lines, filterTableTd.Lifts, filterTableTd.VendorDisplay, countID, filterTableTd.BookingID, filterTableTd.BookingStatus, dtAlternateSlotFinalTable, filterTableTd.Color);
                            }

                            MakeCellClickableNew(tcChild, filterTableTd.SlotTimeID, filterTableTd.DoorNo, filterTableTd.ArrSlotTime15min, filterTableTd.DoorNoDisplay, filterTableTd.SlotStartDay, filterTableTd.siTimeSlotOrderBYID);

                            tcChild.Controls.Add(ddl);
                            trChild.Cells.Add(tcChild);
                            tblChild.Controls.Add(trChild);
                        }
                        if (filterTableTdRow.Count() == 0)
                        {
                            // if slot is not there in datatable then set it black
                            TableCell tcChild = new TableCell();
                            tcChild.CssClass = "tableData";
                            tcChild.CssClass = "blueTd width170";
                            AlternateSlot filterDataForBlankRow = (from c in dtAlternateSlotFinalTableBind.AsEnumerable()
                                                                   where c.Field<string>("ArrSlotTime15min") == item.ArrSlotTime
                                                                   //&& c.Field<string>("Weekday") == item.Weekday
                                                                   //&& (c.Field<string>("Color") != "Green" &&
                                                                   //    c.Field<string>("Color") != "Red" &&
                                                                   //    c.Field<string>("Color") != "Orange" &&
                                                                   //    c.Field<string>("Color") != "LightGray" &&
                                                                   //    c.Field<string>("Color") != "Pink" &&
                                                                   //    c.Field<string>("Color") != "GreenYellow" &&
                                                                   //    c.Field<string>("Color") != "Purple" &&
                                                                   //    c.Field<string>("Color") != "White")
                                                                   select new AlternateSlot
                                                                   {
                                                                       DoorNo = c.Field<string>("DoorNo"),
                                                                       FixedSlotID = c.Field<string>("FixedSlotID"),
                                                                       SlotTimeID = c.Field<string>("SlotTimeID"),
                                                                       SiteDoorNumberID = c.Field<string>("SiteDoorNumberID"),
                                                                       ArrSlotTime15min = c.Field<string>("ArrSlotTime15min"),
                                                                       EstSlotTime15min = c.Field<string>("EstSlotTime15min"),
                                                                       SlotStartDay = c.Field<string>("SlotStartDay"),
                                                                       siTimeSlotOrderBYID = c.Field<string>("iTimeSlotOrderBYID")
                                                                   }).FirstOrDefault();
                            var DistinctdoorTableInfo = DistinctdoorTable.Where(x => x.DoorNo == door).ToList();
                            foreach (var items in DistinctdoorTableInfo)
                            {
                                //MakeCellClickableNew(tcChild, filterDataForBlankRow.SlotTimeID, door, item, items.DoorNoDisplay, filterDataForBlankRow.SlotStartDay, filterDataForBlankRow.siTimeSlotOrderBYID);
                            }

                            trChild.Cells.Add(tcChild);
                            tblChild.Controls.Add(trChild);
                        }
                        tc.Controls.Add(tblChild);
                        tr.Cells.Add(tc);
                        tblSlotNew.Rows.Add(tr);
                    }
                }
                if (hdnCurrentRole.Value != "Carrier")
                {
                    tableDiv_GeneralNew.Controls.Add(tblSlotNew);
                }
            }
        }
        catch (Exception ex)
        { }

    }

    private string GetLowestPriority(NameValueCollection DoorTypePriority)
    {  //SPRINT 4 POINT 7
        NameValueCollection collection = (NameValueCollection)DoorTypePriority;
        string LowestPri = "1";
        if (collection != null)
        {
            foreach (string key in collection.AllKeys)
            {
                if (Convert.ToInt32(collection.Get(key)) < Convert.ToInt32(LowestPri))
                {
                    LowestPri = collection.Get(key);
                }
            }
        }
        return LowestPri;
    }

    public string AddedControlIDs
    {
        get
        {
            return ViewState["AddedControlIDs"] != null ? ViewState["AddedControlIDs"].ToString() : string.Empty;
        }
        set
        {
            ViewState["AddedControlIDs"] = value;
        }
    }

    private void CreateDynamicDiv(ref TableCell tcLocal, int SlotLength, string SlotStartTime)
    {

        if (hdnCurrentRole.Value == "Carrier")
        {
            if (sVendorCarrierName != ddlCarrier.SelectedItem.Text)
            {
                return;
            }
        }

        string[] StartTime = SlotStartTime.Split(':');
        TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1].Substring(0, 2)), 00);
        TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
        for (int iLen = 1; iLen <= SlotLength; iLen++)
        {
            if (time1.Hours == 23 && time1.Minutes == 45)
            {
                time1 = new TimeSpan(00, 00, 00);
            }
            else
            {
                time1 = time1.Add(time2);
            }
        }

        string sSlotLength1;
        //if ((iSiteDoorID == 58 || iSiteDoorID==60) && (iTimeSlotID == 42 || iTimeSlotID == 49))
        //if (iSiteDoorID == 138 && iTimeSlotID == 95)
        //    sSlotLength1 = "0";


        // Create a new HtmlGenericControl.
        HtmlGenericControl NewControl = new HtmlGenericControl("div");

        // Set the properties of the new HtmlGenericControl control.
        string ControlID = "Volume" + iTimeSlotID.ToString() + iSiteDoorID.ToString();

        if (AddedControlIDs.IndexOf("," + ControlID + ",") > 0)
        {
            //return;
            ControlID = ControlID + "_1";
        }

        if (AddedControlIDs == string.Empty)
            AddedControlIDs = "," + ControlID + ",";
        else
            AddedControlIDs += ControlID + ",";

        NewControl.ID = "," + ControlID + ",";

        if (divVolume.FindControl(NewControl.ID) != null)
            divVolume.Controls.Remove(divVolume.FindControl(NewControl.ID));

        string InnerHTML = "<table width='100%'> " +
                              "<tr><td colspan='5' align='center'><strong>{CarrierName}</strong></td></tr> " +
                              "<tr> " +
                                "<td><strong>Arrival {Atime} </strong></td> " +
                                "<td>Lifts</td> " +
                                "<td>Pallets</td> " +
                                "<td>Cartons</td> " +
                                "<td>Lines</td> " +
                              "</tr> " +
                              "<tr> ";
        if (hdnCurrentRole.Value != "Carrier")
        {
            InnerHTML = InnerHTML + "<td><strong>Est Departure {DTime} </strong></td> ";
            InnerHTML = InnerHTML.Replace("{DTime}", GetFormatedTime(time1.ToString()));
        }
        else
        {
            InnerHTML = InnerHTML + "<td>&nbsp;</td>";
        }
        InnerHTML = InnerHTML + "<td>{Lifts}</td> " +
                                "<td>{Pallets}</td> " +
                                "<td>{Cartons}" +
                                "<td>{Lines}</td> " +
                              "</tr> " +
                            "</table>";

        InnerHTML = InnerHTML.Replace("{CarrierName}", sVendorCarrierName);
        InnerHTML = InnerHTML.Replace("{Atime}", GetFormatedTime(SlotStartTime));


        if (iLifts != 0 && iLifts != -1)
            InnerHTML = InnerHTML.Replace("{Lifts}", iLifts.ToString());
        else
            InnerHTML = InnerHTML.Replace("{Lifts}", "-");

        if (iPallets != 0)
            InnerHTML = InnerHTML.Replace("{Pallets}", iPallets.ToString());
        else
            InnerHTML = InnerHTML.Replace("{Pallets}", "-");

        if (iCartons != 0)
            InnerHTML = InnerHTML.Replace("{Cartons}", iCartons.ToString());
        else
            InnerHTML = InnerHTML.Replace("{Cartons}", "-");

        if (iLines != 0)
            InnerHTML = InnerHTML.Replace("{Lines}", iLines.ToString());
        else
            InnerHTML = InnerHTML.Replace("{Lines}", "-");

        NewControl.InnerHtml = InnerHTML;
        NewControl.Attributes.Add("display", "none");
        // Add the new HtmlGenericControl to the Controls collection of the
        // TableCell control. 


        divVolume.Controls.Add(NewControl);
        if (!isClickable)
        {
            tcLocal.Attributes.Add("onmouseover", "ddrivetip('ctl00_ContentPlaceHolder1_," + ControlID + ",','#ededed','400');");
            tcLocal.Attributes.Add("onmouseout", "hideddrivetip();");
        }
        else
        {
            tcLocal.Attributes.Add("onmouseover", "defColor=this.style.color;this.style.cursor = 'hand';  this.style.color='Red';ddrivetip('ctl00_ContentPlaceHolder1_," + ControlID + ",','#ededed','400');");
            tcLocal.Attributes.Add("onmouseout", "this.style.color=defColor;this.style.cursor = 'default';hideddrivetip();");
        }
    }

    private void MakeCellClickableForOtherVendor(ref TableCell tclocal)
    {
        string fixedSlotid = "-1";
        tclocal.Attributes.Add("onclick", "CheckAletrnateSlot('" + iTimeSlotID.ToString() + "','" + iSiteDoorID.ToString() + "','" + sDoorNo + "','" + sTimeSlot + "','" + fixedSlotid + "','" + sSlotStartDay + "','" + iTimeSlotOrderBYID + "');");
    }

    private void MakeCellClickable(ref TableCell tclocal)
    {
        tclocal.Attributes.Add("onmouseover", "this.style.cursor = 'hand'");
        tclocal.Attributes.Add("onmouseout", "this.style.cursor = 'default'");

        string fixedSlotid = "-1";
        CarrierDetails vd = new CarrierDetails();

        string[] StartTime = sTimeSlot.Split(':');
        TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);

        if (SlotDoorCounter.ContainsKey(time1 + iSiteDoorID.ToString()))
        {
            vd = SlotDoorCounter[time1 + iSiteDoorID.ToString()];
            if (vd.CurrentFixedSlotID != null)
            {
                fixedSlotid = vd.CurrentFixedSlotID.ToString();
            }
        }

        tclocal.Attributes.Add("onclick", "CheckAletrnateSlot('" + iTimeSlotID.ToString() + "','" + iSiteDoorID.ToString() + "','" + sDoorNo + "','" + sTimeSlot + "','" + fixedSlotid + "','" + sSlotStartDay + "','" + iTimeSlotOrderBYID + "');");
    }

    private void RemoveCellClickable(ref TableCellCollection tcClocal)
    {
        TableCell tclocal = tcClocal[0];
        tclocal.Attributes.Add("onmouseover", "this.style.cursor = 'default'");
        tclocal.Attributes.Add("onmouseout", "this.style.cursor = 'default'");
        tclocal.Attributes.Add("onclick", "");
    }

    string BookingID = string.Empty;
    string BookingStatusID = string.Empty;
    string BookingStatus = string.Empty;
    string SlotTimeID = string.Empty;
    List<AlternateSlot> lstSlot = new List<AlternateSlot>();
    private Literal SetCellColor(ref TableCell tclocal)
    {
        string VendorCarrierName = string.Empty;
        int SlotLength = 0;
        NextRequiredSlot = string.Empty;
        iNextSlotLength = 0;
        iNextSlotLength2 = 0;
        RTotalPallets = Convert.ToString(ViewState["RTotalPallets"]);
        RTotalCartons = Convert.ToString(ViewState["RTotalCartons"]);
        RTotalLines = Convert.ToString(ViewState["RTotalLines"]);
        RTotalLifts = "-";
        //if ((iSiteDoorID == 58 || iSiteDoorID==60) && (iTimeSlotID == 42 || iTimeSlotID == 49))
        //if (iTimeSlotID == 45)
        //    SlotLength = 0;


        //Find Next Fixed Slot on the Same door No if seledted slot is a fixed slot
        List<APPBOK_BookingBE> lstNextFixedDoorLocal = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE Bok) { return Bok.DoorNoSetup.SiteDoorNumberID == iSiteDoorID; });

        for (int LoopCount = 0; LoopCount < lstNextFixedDoorLocal.Count; LoopCount++)
        {
            if (lstNextFixedDoorLocal[LoopCount].SlotTime.SlotTimeID == iTimeSlotID && lstNextFixedDoorLocal[LoopCount].DoorNoSetup.SiteDoorNumberID == iSiteDoorID)
            {
                if (LoopCount < lstNextFixedDoorLocal.Count - 1)
                {
                    NextRequiredSlot = lstNextFixedDoorLocal[LoopCount + 1].SlotTime.SlotTime;
                    sVendorCarrierNameNextSlot = lstNextFixedDoorLocal[LoopCount + 1].Vendor.VendorName;
                    iNextSlotLength = 0; // GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons);
                    BookingID = Convert.ToString(lstNextFixedDoorLocal[LoopCount + 1].BookingID);
                    BookingStatusID = Convert.ToString(lstNextFixedDoorLocal[LoopCount + 1].BookingStatusID);
                    BookingStatus = Convert.ToString(lstNextFixedDoorLocal[LoopCount + 1].BookingStatus);

                    if (lstNextFixedDoorLocal[LoopCount + 1].Status != "Booked")
                    {
                        if (lstNextFixedDoorLocal[LoopCount + 1].SupplierType == "C")
                            iNextSlotLength2 = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons, lstNextFixedDoorLocal[LoopCount + 1].SupplierType, lstNextFixedDoorLocal[LoopCount + 1].Carrier.CarrierID); // GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons);
                        else if (lstNextFixedDoorLocal[LoopCount + 1].SupplierType == "V")
                            iNextSlotLength2 = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons, lstNextFixedDoorLocal[LoopCount + 1].SupplierType, lstNextFixedDoorLocal[LoopCount + 1].VendorID);
                    }
                    else
                    {
                        iNextSlotLength2 = GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons);
                    }

                }
                break;
            }
        }
        //---------------------------------//

        //Find Next Slot on the Same door No if seledted slot is not a fixed slot
        if (NextRequiredSlot == string.Empty)
        {
            string[] StartTime = sTimeSlot.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            int iLen = 0;
            for (iLen = 1; iLen <= actSlotLength; iLen++)
            {
                if (time1.Hours == 23 && time1.Minutes == 45)
                {
                    time1 = new TimeSpan(00, 00, 00);
                    //strWeekDay = objWeekSetup.EndWeekday;
                    //sSlotStartDay = strWeekDay;
                    //string time = time1.Hours + ":" + time1.Minutes;
                    //iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                }
                else
                {
                    time1 = time1.Add(time2);
                    //strWeekDay = objWeekSetup.EndWeekday;
                    //sSlotStartDay = strWeekDay;
                    //string time = time1.Hours + ":" + time1.Minutes;
                    //iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                }
                for (int LoopCount = 0; LoopCount < lstNextFixedDoorLocal.Count; LoopCount++)
                {
                    if (GetFormatedTime(lstNextFixedDoorLocal[LoopCount].SlotTime.SlotTime) == GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) && lstNextFixedDoorLocal[LoopCount].DoorNoSetup.SiteDoorNumberID == iSiteDoorID)
                    {

                        if (lstNextFixedDoorLocal[LoopCount].Vendor.VendorName != ddlCarrier.SelectedItem.Text.Trim())
                        {

                            if (lstNextFixedDoorLocal[LoopCount].Status != "Booked")
                            {
                                if (lstNextFixedDoorLocal[LoopCount].SupplierType == "C")
                                    iNextSlotLength = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons, lstNextFixedDoorLocal[LoopCount].SupplierType, lstNextFixedDoorLocal[LoopCount].Carrier.CarrierID); // GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons);
                                else if (lstNextFixedDoorLocal[LoopCount].SupplierType == "V")
                                    iNextSlotLength = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons, lstNextFixedDoorLocal[LoopCount].SupplierType, lstNextFixedDoorLocal[LoopCount].VendorID);
                            }
                            else
                            {

                                if (lstNextFixedDoorLocal[LoopCount].SupplierType == "C")
                                    iNextSlotLength = GetRequiredTimeSlotLengthForBookedSlotOfOthers(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons, lstNextFixedDoorLocal[LoopCount].SupplierType, lstNextFixedDoorLocal[LoopCount].Carrier.CarrierID, lstNextFixedDoorLocal[LoopCount].VehicleType.VehicleTypeID); // GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons);
                                else if (lstNextFixedDoorLocal[LoopCount].SupplierType == "V")
                                    iNextSlotLength = GetRequiredTimeSlotLengthForBookedSlotOfOthers(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons, lstNextFixedDoorLocal[LoopCount].SupplierType, lstNextFixedDoorLocal[LoopCount].VendorID, lstNextFixedDoorLocal[LoopCount].VehicleType.VehicleTypeID);
                            }



                            //iNextSlotLength = GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons);
                            NextRequiredSlot = lstNextFixedDoorLocal[LoopCount].SlotTime.SlotTime;
                            sVendorCarrierNameNextSlot = lstNextFixedDoorLocal[LoopCount].Vendor.VendorName;
                            break;
                        }
                    }
                }
                if (NextRequiredSlot != string.Empty)
                {
                    //iNextSlotLength = actSlotLength - iLen;  //(iLen + iNextSlotLength) - actSlotLength;  //
                    isNextSlotCovered = true;
                    if (actSlotLength - iLen < iNextSlotLength)
                    {
                        isNextSlotCovered = false;
                    }
                    break;
                }
            }
        }
        //---------------------------------//




        //Filter slot as per SlotTimeID and SiteDoorNumberID
        //List<APPBOK_BookingBE> lstAllFixedDoorLocal = lstAllFixedDoor.Where(Bok => Bok.SlotTime.SlotTimeID == iTimeSlotID && Bok.DoorNoSetup.SiteDoorNumberID == iSiteDoorID).ToList();
        List<APPBOK_BookingBE> lstAllFixedDoorLocal = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE Bok) { return Bok.SlotTime.SlotTimeID == iTimeSlotID && Bok.DoorNoSetup.SiteDoorNumberID == iSiteDoorID; });
        if (lstAllFixedDoorLocal.Count > 0)
        {
            AllocationType = lstAllFixedDoorLocal[0].FixedSlot.AllocationType;
            if (lstAllFixedDoorLocal[0].Carrier.CarrierID == Convert.ToInt32(ddlCarrier.SelectedItem.Value) && lstAllFixedDoorLocal[0].SupplierType == "C")
            {
                sVendorCarrierName = ddlCarrier.SelectedItem.Text;
                iCartons = lstAllFixedDoorLocal[0].NumberOfCartons != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfCartons) : 0;
                iLifts = lstAllFixedDoorLocal[0].NumberOfLift != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLift) : 0;
                iLines = lstAllFixedDoorLocal[0].NumberOfLines != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfLines) : 0;
                iPallets = lstAllFixedDoorLocal[0].NumberOfPallet != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[0].NumberOfPallet) : 0;
                int VahicalTypeID = lstAllFixedDoorLocal[0].VehicleType.VehicleTypeID;
                sVendorCarrierNameNew = lstAllFixedDoorLocal[0].Vendor.VendorName;
                string supplierType = lstAllFixedDoorLocal[0].SupplierType;
                string FixedSlotID = Convert.ToString(lstAllFixedDoorLocal[0].FixedSlot.FixedSlotID);
                BookingID = Convert.ToString(lstAllFixedDoorLocal[0].BookingID);
                BookingStatusID = Convert.ToString(lstAllFixedDoorLocal[0].BookingStatusID);
                BookingStatus = Convert.ToString(lstAllFixedDoorLocal[0].BookingStatus);
                // string SlotTimeID = string.Empty;
                SlotTimeID = Convert.ToString(lstAllFixedDoorLocal[0].SlotTime.SlotTimeID);
                SetSlotLegendForVendor(ref tclocal, VahicalTypeID, supplierType, FixedSlotID, SlotTimeID);
                tclocal.Attributes.Add("abbr", "C-" + ddlCarrier.SelectedItem.Value.ToString());

                if (hdnSuggestedSiteDoorNumberID.Value == iSiteDoorID.ToString() && hdnSuggestedSlotTimeID.Value == iTimeSlotID.ToString())
                {
                    VendorCarrierName = ddlCarrier.SelectedItem.Text;
                }
                else
                {
                    VendorCarrierName = sVendorCarrierName; // "";
                }
            }
            else
            {
                //if (hdnCurrentRole.Value == "Vendor") {
                for (int i = 0; i < lstAllFixedDoorLocal.Count(); i++)
                {
                    isClickable = true;
                    sVendorCarrierName = lstAllFixedDoorLocal[i].Vendor.VendorName;
                    sVendorCarrierNameNew = lstAllFixedDoorLocal[i].Vendor.VendorName;
                    string supplierType = lstAllFixedDoorLocal[i].SupplierType;
                    string FixedSlotID = Convert.ToString(lstAllFixedDoorLocal[i].FixedSlot.FixedSlotID);
                    SlotTimeID = Convert.ToString(lstAllFixedDoorLocal[i].SlotTime.SlotTimeID);
                    // string SlotTimeID = string.Empty;

                    AllocationType = Convert.ToString(lstAllFixedDoorLocal[i].FixedSlot.AllocationType);
                    isClickable = true;
                    sVendorCarrierName = lstAllFixedDoorLocal[i].Vendor.VendorName;
                    //commented Just for run Fixed slot
                    iCartons = lstAllFixedDoorLocal[i].NumberOfCartons != null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfCartons) : 0;

                    iPallets = lstAllFixedDoorLocal[i].NumberOfPallet != null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfPallet) : 0;

                    iLines = lstAllFixedDoorLocal[i].NumberOfLines != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfLines) : 0;

                    iLifts = lstAllFixedDoorLocal[i].NumberOfLift != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfLift) : 0;

                    BookingID = Convert.ToString(lstAllFixedDoorLocal[i].BookingID);
                    BookingStatusID = Convert.ToString(lstAllFixedDoorLocal[i].BookingStatusID);
                    BookingStatus = Convert.ToString(lstAllFixedDoorLocal[i].BookingStatus);

                    if (lstAllFixedDoorLocal[i].Status != "Booked")
                    {
                        if (lstAllFixedDoorLocal[i].SupplierType == "C")
                        {
                            SlotLength = GetRequiredTimeSlotLengthForOthers(lstAllFixedDoorLocal[i].NumberOfPallet, lstAllFixedDoorLocal[i].NumberOfCartons, lstAllFixedDoorLocal[i].SupplierType, lstAllFixedDoorLocal[i].Carrier.CarrierID);
                            tclocal.Attributes.Add("abbr", "C-" + lstAllFixedDoorLocal[i].Carrier.CarrierID.ToString());
                        }
                        else if (lstAllFixedDoorLocal[i].SupplierType == "V")
                        {
                            SlotLength = GetRequiredTimeSlotLengthForOthers(lstAllFixedDoorLocal[i].NumberOfPallet, lstAllFixedDoorLocal[i].NumberOfCartons, lstAllFixedDoorLocal[i].SupplierType, lstAllFixedDoorLocal[i].VendorID);
                            tclocal.Attributes.Add("abbr", "V-" + lstAllFixedDoorLocal[i].VendorID.ToString());
                        }
                    }
                    else
                    {
                        if (lstAllFixedDoorLocal[i].SupplierType == "C")
                        {
                            SlotLength = GetRequiredTimeSlotLengthForBookedSlotOfOthers(lstAllFixedDoorLocal[i].NumberOfPallet, lstAllFixedDoorLocal[i].NumberOfCartons, lstAllFixedDoorLocal[i].SupplierType, lstAllFixedDoorLocal[i].Carrier.CarrierID, lstAllFixedDoorLocal[i].VehicleType.VehicleTypeID);
                            tclocal.Attributes.Add("abbr", "C-" + lstAllFixedDoorLocal[i].Carrier.CarrierID.ToString());
                        }
                        else if (lstAllFixedDoorLocal[i].SupplierType == "V")
                        {
                            SlotLength = GetRequiredTimeSlotLengthForBookedSlotOfOthers(lstAllFixedDoorLocal[i].NumberOfPallet, lstAllFixedDoorLocal[i].NumberOfCartons, lstAllFixedDoorLocal[i].SupplierType, lstAllFixedDoorLocal[i].VendorID, lstAllFixedDoorLocal[i].VehicleType.VehicleTypeID);
                            tclocal.Attributes.Add("abbr", "V-" + lstAllFixedDoorLocal[i].VendorID.ToString());
                        }
                    }

                    //---Phase 19 point 1--//
                    tclocal.Attributes.Add("prefix", lstAllFixedDoorLocal[i].FixedSlot.AllocationType + "-" + SlotLength.ToString());
                    //---------------------//

                    //SlotLength = GetTimeSlotLength();

                    isCurrentSlotFixed = false;
                    string tempVendorName = "";

                    sCurrentFixedSlotID = lstAllFixedDoorLocal[i].FixedSlot.FixedSlotID.ToString();
                    //tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Black, lstAllFixedDoorLocal[0].SlotTime.SlotTime);
                    //sCurrentFixedSlotID == hdnCurrentFixedSlotID.Value &&
                    if (hdnSuggestedSlotTime.Value == sTimeSlot && hdnSuggestedSiteDoorNumber.Value == sDoorNo)
                    {
                        sVendorCarrierName = ddlCarrier.SelectedItem.Text;
                        if (iSiteDoorID == Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value) && iTimeSlotID == Convert.ToInt32(hdnSuggestedSlotTimeID.Value))
                        {
                            sVendorCarrierNameNew = sVendorCarrierName;
                        }
                        if (lstAllFixedDoorLocal[i].Status == "") // to determine whether slot is recommended
                        {
                            tempVendorName = SetCallHeight(actSlotLength, ref tclocal, Color.GreenYellow, hdnSuggestedSlotTime.Value, RTotalPallets, RTotalCartons, RTotalLines, RTotalLifts, FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);
                        }
                        else if (lstAllFixedDoorLocal[i].Status == "Booked") // to determine whether Booked is recommended
                        {
                            tempVendorName = SetCallHeight(actSlotLength, ref tclocal, Color.Purple, hdnSuggestedSlotTime.Value, iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);
                        }
                        else
                        {
                            tempVendorName = SetCallHeight(actSlotLength, ref tclocal, Color.Red, hdnSuggestedSlotTime.Value, iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);
                        }
                        sVendorCarrierName = "";
                    }
                    else
                    {
                        if (hdnCurrentRole.Value == "Carrier")
                        {
                            tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Black, lstAllFixedDoorLocal[i].SlotTime.SlotTime, iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);
                        }
                        else
                        {
                            if (lstAllFixedDoorLocal[i].SlotType == "FIXED SLOT" && lstAllFixedDoorLocal[i].Status != "Booked")
                            {
                                isCurrentSlotFixed = true;
                                tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Red, lstAllFixedDoorLocal[i].SlotTime.SlotTime, iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);

                                if (!tempVendorName.Contains(sVendorCarrierName))
                                {
                                    if (tempVendorName != string.Empty)
                                    {
                                        if (sVendorCarrierName != tempVendorName)
                                        {
                                            tempVendorName = sVendorCarrierName + " / " + tempVendorName;
                                        }
                                    }
                                    else
                                    {
                                        if (sVendorCarrierName != tempVendorName)
                                        {
                                            tempVendorName = sVendorCarrierName;
                                        }
                                    }
                                }

                            }
                            else if (lstAllFixedDoorLocal[i].Status == "Booked")
                            {
                                tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Purple, lstAllFixedDoorLocal[i].SlotTime.SlotTime, iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);

                                if (!tempVendorName.Contains(sVendorCarrierName))
                                {
                                    if (tempVendorName != string.Empty)
                                    {
                                        if (sVendorCarrierName != tempVendorName)
                                        {
                                            tempVendorName = sVendorCarrierName + " / " + tempVendorName;
                                        }
                                    }
                                    else
                                    {
                                        if (sVendorCarrierName != tempVendorName)
                                        {
                                            tempVendorName = sVendorCarrierName;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (tempVendorName != string.Empty)
                    {
                        VendorCarrierName = tempVendorName;
                    }
                    else
                    {
                        //SetCellBacknForeColor(ref tclocal, Color.Black, false);
                        VendorCarrierName = lstAllFixedDoorLocal[i].Vendor.VendorName;
                        iTimeSlotID = lstAllFixedDoorLocal[i].SlotTime.SlotTimeID;
                        iSiteDoorID = lstAllFixedDoorLocal[i].DoorNoSetup.SiteDoorNumberID;

                        iCartons = lstAllFixedDoorLocal[i].NumberOfCartons != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfCartons) : 0;
                        iLifts = lstAllFixedDoorLocal[i].NumberOfLift != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfLift) : 0;
                        iLines = lstAllFixedDoorLocal[i].NumberOfLines != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfLines) : 0;
                        iPallets = lstAllFixedDoorLocal[i].NumberOfPallet != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfPallet) : 0;

                        //CreateDynamicDiv(ref tclocal, true);
                    }
                    //}
                    //else {
                    //    isCurrentSlotFixed = false;
                    //    if (DoorIDs.Contains("," + iTimeSlotID + "@" + iSiteDoorID)) {  //Door Available         
                    //        //SetCellBacknForeColor(ref tclocal, Color.White, false);                
                    //        MakeCellClickable(ref tclocal);
                    //        VendorCarrierName = SetCallHeight(1, ref tclocal, Color.White, sTimeSlot);
                    //    }
                    //    else {                                                  // Door Not Available
                    //        isCurrentSlotFixed = false;
                    //        SetCellBacknForeColor(ref tclocal, Color.Black);
                    //        VendorCarrierName = "";
                    //    }
                    //}

                    if (hdnCurrentRole.Value == "Carrier")
                    {
                        VendorCarrierName = "";
                    }

                    if (hdnCurrentRole.Value != "Carrier")
                    {
                        MakeCellClickableForOtherVendor(ref tclocal);
                    }

                }
            }
        }
        else
        {

            isCurrentSlotFixed = false;
            bool isLoopProcessed = false;

            if (DoorIDs.Contains("," + iTimeSlotID + "@" + iSiteDoorID))
            {  //Door Available         
                //SetCellBacknForeColor(ref tclocal, Color.White, false);  
                isLoopProcessed = true;

                if (hdnSuggestedSiteDoorNumberID.Value == string.Empty)
                {
                    hdnSuggestedSiteDoorNumberID.Value = "-1";
                    hdnSuggestedSlotTimeID.Value = "-1";
                }

                if (iSiteDoorID == Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value) && iTimeSlotID == Convert.ToInt32(hdnSuggestedSlotTimeID.Value))
                {

                    int? iTotalPallets = TotalPallets.Trim() != string.Empty ? Convert.ToInt32(TotalPallets.Trim()) : (int?)null;
                    int? iTotalCartons = TotalCartons.Trim() != string.Empty ? Convert.ToInt32(TotalCartons.Trim()) : (int?)null;
                    int? iTotalLifts = (int?)null;
                    int? iTotalLines = TotalLines.Trim() != string.Empty ? Convert.ToInt32(TotalLines.Trim()) : (int?)null;

                    iPallets = iTotalPallets != (int?)null ? Convert.ToInt32(iTotalPallets) : 0;
                    iCartons = iTotalCartons != (int?)null ? Convert.ToInt32(iTotalCartons) : 0;
                    iLines = iTotalLines != (int?)null ? Convert.ToInt32(iTotalLines) : 0;
                    iLifts = iTotalLifts != (int?)null ? Convert.ToInt32(iTotalLifts) : 0;

                    sVendorCarrierName = ddlCarrier.SelectedItem.Text;
                    sVendorCarrierNameNew = ddlCarrier.SelectedItem.Text;
                    SetCallHeight(actSlotLength, ref tclocal, Color.GreenYellow, sTimeSlot, RTotalPallets, RTotalCartons, RTotalLines, RTotalLifts, SiteDoorNumberID: SiteDoorNumberID);
                    //if (hdnGraySlotClicked.Value == "true")
                    //    SetCallHeight(actSlotLength, ref tclocal, Color.Orange, sTimeSlot);
                    //else
                    //    SetCallHeight(actSlotLength, ref tclocal, Color.GreenYellow, sTimeSlot);

                    VendorCarrierName = ddlCarrier.SelectedItem.Text;

                    tclocal.Attributes.Add("abbr", "C-" + ddlCarrier.SelectedItem.Value);
                }
                else
                {
                    MakeCellClickable(ref tclocal);
                    VendorCarrierName = SetCallHeight(1, ref tclocal, Color.White, sTimeSlot, iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), SiteDoorNumberID: SiteDoorNumberID);
                    if (hdnCurrentRole.Value == "Carrier")
                    {
                        VendorCarrierName = "";
                    }
                }

            }
            else if (!DoorIDs.Contains("," + iTimeSlotID + "@" + iSiteDoorID) )
            {  //Door Not Available but User is internal user



                MakeCellClickable(ref tclocal);

                if (hdnSuggestedSiteDoorNumberID.Value == string.Empty)
                {
                    hdnSuggestedSiteDoorNumberID.Value = "-1";
                    hdnSuggestedSlotTimeID.Value = "-1";
                }

                if (iSiteDoorID == Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value) && iTimeSlotID == Convert.ToInt32(hdnSuggestedSlotTimeID.Value))
                {

                    isLoopProcessed = true;
                    int? iTotalPallets = TotalPallets.Trim() != string.Empty ? Convert.ToInt32(TotalPallets.Trim()) : (int?)null;
                    int? iTotalCartons = TotalCartons.Trim() != string.Empty ? Convert.ToInt32(TotalCartons.Trim()) : (int?)null;
                    int? iTotalLifts = (int?)null;
                    int? iTotalLines = TotalLines.Trim() != string.Empty ? Convert.ToInt32(TotalLines.Trim()) : (int?)null;

                    iPallets = iTotalPallets != (int?)null ? Convert.ToInt32(iTotalPallets) : 0;
                    iCartons = iTotalCartons != (int?)null ? Convert.ToInt32(iTotalCartons) : 0;
                    iLines = iTotalLines != (int?)null ? Convert.ToInt32(iTotalLines) : 0;
                    iLifts = iTotalLifts != (int?)null ? Convert.ToInt32(iTotalLifts) : 0;

                    sVendorCarrierName = ddlCarrier.SelectedItem.Text;

                    sVendorCarrierNameNew = ddlCarrier.SelectedItem.Text;
                    SetCallHeight(actSlotLength, ref tclocal, Color.GreenYellow, sTimeSlot, iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), SiteDoorNumberID: SiteDoorNumberID);
                    //if (hdnGraySlotClicked.Value == "true")
                    //    SetCallHeight(actSlotLength, ref tclocal, Color.Orange, sTimeSlot);
                    //else
                    //    SetCallHeight(actSlotLength, ref tclocal, Color.GreenYellow, sTimeSlot);

                    VendorCarrierName = ddlCarrier.SelectedItem.Text;

                    tclocal.Attributes.Add("abbr", "C-" + ddlCarrier.SelectedItem.Value);
                }

            }

            if (isLoopProcessed == false)
            { // Door Not Available and external user                                                 // Door Not Available
                isCurrentSlotFixed = false;

                CarrierDetails vd = new CarrierDetails();

                string[] StartTime = sTimeSlot.Split(':');
                TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);

                if (SlotDoorCounter.ContainsKey(time1 + iSiteDoorID.ToString()))
                {
                    vd = SlotDoorCounter[time1 + iSiteDoorID.ToString()];
                    if (vd.CellColor == Color.GreenYellow)
                    {
                        SetCellBacknForeColor(ref tclocal, Color.Orange);
                        VendorCarrierName = vd.CarrierName;
                    }
                    else
                    {
                        SetCellBacknForeColor(ref tclocal, Color.Black);
                        if (hdnCurrentRole.Value != "Carrier")
                            MakeCellClickable(ref tclocal);
                        VendorCarrierName = "";
                    }
                }
                else
                {
                    SetCellBacknForeColor(ref tclocal, Color.Black);
                    // to get all slot
                    // SetCallHeight(actSlotLength, ref tclocal, Color.Black, sTimeSlot, iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), SiteDoorNumberID: SiteDoorNumberID);
                    if (hdnCurrentRole.Value != "Carrier")
                        MakeCellClickable(ref tclocal);
                    VendorCarrierName = "";
                }
            }
        }


        //for (int LoopCount = 0; LoopCount < lstAllFixedDoor.Count; LoopCount++) {
        //    if (lstAllFixedDoor[LoopCount].SlotTime.SlotTimeID == jCount && lstAllFixedDoor[LoopCount].DoorNoSetup.SiteDoorNumberID == iCount) {

        //        if (hdnCurrentRole.Value == "Vendor" || lstAllFixedDoor[LoopCount].VendorID == Convert.ToInt32(ucSeacrhVendor1.VendorNo)) {
        //            SetSlotLegendForVendor(dCount, ref tclocal, jCount, iCount, lstAllFixedDoor[LoopCount].NumberOfCartons, lstAllFixedDoor[LoopCount].NumberOfLift, lstAllFixedDoor[LoopCount].NumberOfLines, lstAllFixedDoor[LoopCount].NumberOfPallet);
        //            VendorCarrierName = ddlCarrier.SelectedItem.Text;
        //        }
        //        else {
        //            SetCellBacknForeColor(ref tclocal, Color.Black, false);
        //            //CreateDynamicDiv(ref tclocal, lstAllFixedDoor[LoopCount].SlotTime.SlotTimeID, lstAllFixedDoor[LoopCount].DoorNoSetup.SiteDoorNumberID, lstAllFixedDoor[LoopCount].NumberOfCartons, lstAllFixedDoor[LoopCount].NumberOfLift, lstAllFixedDoor[LoopCount].NumberOfLines, lstAllFixedDoor[LoopCount].NumberOfPallet);
        //            SlotLength = GetTimeSlotLength(lstAllFixedDoor[LoopCount].NumberOfPallet, lstAllFixedDoor[LoopCount].NumberOfCartons);

        //            VendorCarrierName = lstAllFixedDoor[LoopCount].Vendor.VendorName;
        //        }
        //        break;
        //    }
        //}
        string[] StartTimeslot = sTimeSlot.Split(':');
        TimeSpan slottime1 = new TimeSpan(Convert.ToInt32(StartTimeslot[0]), Convert.ToInt32(StartTimeslot[1]), 00);
        TimeSpan slottime2 = new TimeSpan(00, iSlotTimeLength, 00);
        TimeSpan slotNewAlternateTime1 = slottime1;
        lstSlot.Add(new AlternateSlot()
        {
            SlotStartDay = sSlotStartDay,
            siTimeSlotOrderBYID = Convert.ToString(iTimeSlotOrderBYID),
            ArrSlotTime = slotNewAlternateTime1.ToString(),
            EstSlotTime = slotNewAlternateTime1.Hours.ToString() == "23"
            && slotNewAlternateTime1.Minutes.ToString() == "45" ? "00:00:00" : slotNewAlternateTime1.Add(slottime2).ToString(),
            SiteDoorNumberID = SiteDoorNumberID,
            SlotTimeID = Convert.ToString(SlotTimeID),
            DoorNo = Convert.ToString(iSiteDoorID)
        });

        Literal lt = new Literal();
        lt.Text = VendorCarrierName;
        BookingID = "";
        BookingStatusID = "";
        BookingStatus = "";
        return lt;
    }

    private int GetRequiredTimeSlotLength(int? TotalPallets, int? TotalCartons)
    {
        int itempCartons, itempPallets;
        itempCartons = iCartons;
        itempPallets = iPallets;

        //int? TotalPallets = TotalPallets.Trim() != string.Empty ? Convert.ToInt32(TotalPallets.Trim()) : (int?)null;
        //int? TotalCartons = TotalCartons.Trim() != string.Empty ? Convert.ToInt32(TotalCartons.Trim()) : (int?)null;

        iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
        iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;

        int reqSlotLength = GetTimeSlotLength();

        iCartons = itempCartons;
        iPallets = itempPallets;

        return reqSlotLength;
    }

    private int GetRequiredTimeSlotLengthForOthers(int? TotalPallets, int? TotalCartons, string SupplierType, int ID)
    {
        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;
        TotalPallets = TotalPallets == null ? 0 : TotalPallets;
        TotalCartons = TotalCartons == null ? 0 : TotalCartons;

        if (SupplierType == "V")
        {
            //Step1 >Check Vendor Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
                APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

                oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
                oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                oMASSIT_VendorProcessingWindowBE.VendorID = ID;
                oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);


                MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
                lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);

                if (lstVendor != null)
                {  //Vendor Processing Window Not found

                    if (TotalPallets != 0 && isTimeCalculatedForPallets == false && lstVendor.APP_PalletsUnloadedPerHour > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (TotalCartons != 0 && isTimeCalculatedForCartons == false && lstVendor.APP_CartonsUnloadedPerHour > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }

                }
            }
            //---------------------------------// 
        }

        if (SupplierType == "C")
        {
            //Step1 >Check Carrier Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
                APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

                oMASSIT_CarrerProcessingWindowBE.Action = "GetConstraints";
                oMASSIT_CarrerProcessingWindowBE.CarrierID = ID;
                oMASSIT_CarrerProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                MASSIT_CarrierProcessingWindowBE lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetConstraintsBAL(oMASSIT_CarrerProcessingWindowBE);


                if (lstCarrier != null)
                {  //Carrier Processing Window Not found

                    if (TotalPallets != 0 && isTimeCalculatedForPallets == false && lstCarrier.APP_PalletsUnloadedPerHour > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstCarrier.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (TotalCartons != 0 && isTimeCalculatedForCartons == false && lstCarrier.APP_CartonsUnloadedPerHour > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstCarrier.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }

                }
            }
            //---------------------------------// 
        }


        //Step2 > Check Vehicle Type Setup  -- skip this step
        //if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false) {
        //    MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        //    APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

        //    oMASSIT_VehicleTypeBE.Action = "ShowById";
        //    oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        //    oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        //    oMASSIT_VehicleTypeBE.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
        //    MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
        //    if (lstVehicleType != null) {
        //        if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 &&
        //                lstVehicleType.APP_CartonsUnloadedPerHour > 0) { //Volume Capacity 
        //            if (TotalPallets != 0 && isTimeCalculatedForPallets == false) {
        //                TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
        //                isTimeCalculatedForPallets = true;
        //            }
        //            if (TotalCartons != 0 && isTimeCalculatedForCartons == false) {
        //                TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
        //                isTimeCalculatedForCartons = true;
        //            }
        //        }
        //        else { //Time Based 
        //            TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
        //            isTimeCalculatedForPallets = true;
        //            isTimeCalculatedForCartons = true;
        //        }
        //    }
        //}
        //---------------------------------// 


        //Step3 >Check Site Miscellaneous Settings
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {

            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

            oMAS_SiteBE.Action = "ShowAll";
            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
            lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

            if (lstSiteMisSettings != null)
            {
                if (TotalPallets != 0 && isTimeCalculatedForPallets == false && lstSiteMisSettings[0].AveragePalletUnloadedPerManHour > 0)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour));      // Convert.ToDecimal((Convert.ToInt32(TotalPallets) / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour) * 60);
                    isTimeCalculatedForPallets = true;
                }
                if (TotalCartons != 0 && isTimeCalculatedForCartons == false && lstSiteMisSettings[0].AverageCartonUnloadedPerManHour > 0)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour));      //Convert.ToDecimal((Convert.ToInt32(TotalCartons) / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour) * 60);
                    isTimeCalculatedForCartons = true;
                }
            }
        }
        //---------------------------------//   

        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime % iSlotTimeLength;
        int intr = (int)TotalCalculatedTime / iSlotTimeLength;
        if (intResult > 0)
            intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;
        //if (intr == 1)
        //    intr--;

        return intr;
    }

    private int GetRequiredTimeSlotLengthForBookedSlotOfOthers(int? TotalPallets, int? TotalCartons, string SupplierType, int ID, int VehicleTypeID)
    {
        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;

        TotalPallets = TotalPallets == null ? 0 : TotalPallets;
        TotalCartons = TotalCartons == null ? 0 : TotalCartons;

        if (SupplierType == "V")
        {
            //Step1 >Check Vendor Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
                APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

                oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
                oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                oMASSIT_VendorProcessingWindowBE.VendorID = ID;
                oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);


                MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
                lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);

                if (lstVendor != null)
                {  //Vendor Processing Window Not found

                    if (lstVendor.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstVendor.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------// 
        }

        if (SupplierType == "C")
        {
            //Step1 >Check Carrier Processing Window 
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {

                MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
                APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

                oMASSIT_CarrerProcessingWindowBE.Action = "GetConstraints";
                oMASSIT_CarrerProcessingWindowBE.CarrierID = ID;
                oMASSIT_CarrerProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                MASSIT_CarrierProcessingWindowBE lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetConstraintsBAL(oMASSIT_CarrerProcessingWindowBE);


                if (lstCarrier != null)
                {  //Carrier Processing Window Not found

                    if (lstCarrier.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstCarrier.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstCarrier.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstCarrier.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------// 
        }


        //Step2 > Check Vehicle Type Setup
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
            APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

            oMASSIT_VehicleTypeBE.Action = "ShowById";
            oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);

            if (VehicleTypeID <= 0)
                oMASSIT_VehicleTypeBE.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
            else
                oMASSIT_VehicleTypeBE.VehicleTypeID = VehicleTypeID;

            MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType != null)
            {
                //Volume Capacity 
                if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVehicleType.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }

                if (isTimeCalculatedForPallets == false && isTimeCalculatedForCartons == false)
                { //Time Based 
                    if (lstVehicleType.TotalUnloadTimeInMinute > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
                        isTimeCalculatedForPallets = true;
                        isTimeCalculatedForCartons = true;
                    }
                }

                //---------------Stage 14 Point 15-------------//
                if (lstVehicleType.TotalUnloadTimeInMinute <= 0 && TotalCalculatedTime < lstVehicleType.MinimumTimePerDeliveryInMinute)
                {
                    TotalCalculatedTime = lstVehicleType.MinimumTimePerDeliveryInMinute;
                }
                //-------------------------------------------//
            }
        }
        //---------------------------------// 

        //Step3 >Check Site Miscellaneous Settings
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {

            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();


            oMAS_SiteBE.Action = "ShowAll";
            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
            lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

            if (lstSiteMisSettings != null && lstSiteMisSettings.Count > 0)
            {
                if (lstSiteMisSettings[0].AveragePalletUnloadedPerManHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour));      // Convert.ToDecimal((Convert.ToInt32(TotalPallets) / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour) * 60);
                    isTimeCalculatedForPallets = true;
                }
                if (lstSiteMisSettings[0].AverageCartonUnloadedPerManHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour));      //Convert.ToDecimal((Convert.ToInt32(TotalCartons) / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour) * 60);
                    isTimeCalculatedForCartons = true;
                }
            }
        }
        //---------------------------------//  

        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime % iSlotTimeLength;
        int intr = (int)TotalCalculatedTime / iSlotTimeLength;
        if (intResult > 0)
            intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;

        //if (intr == 1)
        //    intr--;

        return intr;
    }

    private void SetSlotLegendForVendor(ref TableCell tclocal, int VahicalTypeID, string supplierType = "", string FixedSlotID = "", string SlotTimeID = "")
    {

        int SlotLength = 0;
        string CapacityWarning = "false";
        int? iTotalPallets = TotalPallets.Trim() != string.Empty ? Convert.ToInt32(TotalPallets.Trim()) : (int?)null;
        int? iTotalCartons = TotalCartons.Trim() != string.Empty ? Convert.ToInt32(TotalCartons.Trim()) : (int?)null;
        int? iTotalLifts = (int?)null;
        int? iTotalLines = TotalLines.Trim() != string.Empty ? Convert.ToInt32(TotalLines.Trim()) : (int?)null;
        string tempVendorName = string.Empty;
        //bool isAvailable = false;


        //if (iCartons < TotalCartons || iPallets < TotalPallets) {
        //    CapacityWarning = "true";
        //}



        foreach (GridViewRow gvRow in gvBookingSlots.Rows)
        {
            if (gvRow.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnAvailableFixedSlotID = (HiddenField)gvRow.FindControl("hdnAvailableFixedSlotID");
                HiddenField hdnAvailableSlotTimeID = (HiddenField)gvRow.FindControl("hdnAvailableSlotTimeID");
                HiddenField hdnAvailableSiteDoorNumberID = (HiddenField)gvRow.FindControl("hdnAvailableSiteDoorNumberID");
                HiddenField hdnAvailableSiteDoorNumber = (HiddenField)gvRow.FindControl("hdnAvailableSiteDoorNumber");
                HiddenField hdnAllocationType = (HiddenField)gvRow.FindControl("hdnAllocationType");

                hdnAllocationType.Value = string.IsNullOrEmpty(hdnAllocationType.Value) ? "H" : hdnAllocationType.Value;


                ucLiteral ltTime = (ucLiteral)gvRow.FindControl("ltTime");

                if (gvRow.Cells[5].Text == "FIXED SLOT")
                    isCurrentSlotFixed = true;
                if (hdnAvailableSlotTimeID.Value == iTimeSlotID.ToString() && hdnAvailableSiteDoorNumberID.Value == iSiteDoorID.ToString())
                {
                    if (hdnAvailableFixedSlotID != null && hdnAvailableFixedSlotID.Value == hdnSuggestedFixedSlotID.Value && hdnSuggestedFixedSlotID.Value != "-1")
                    { //This Booking //if (gvRow.Cells[4].Text == "This Booking") {                       

                        iPallets = iTotalPallets != (int?)null ? Convert.ToInt32(iTotalPallets) : 0;
                        iCartons = iTotalCartons != (int?)null ? Convert.ToInt32(iTotalCartons) : 0;
                        iLines = iTotalLines != (int?)null ? Convert.ToInt32(iTotalLines) : 0;
                        iLifts = 0;
                        isClickable = false;

                        if (hdnCurrentMode.Value == "Edit")
                            SlotLength = GetTimeSlotLength();
                        else
                            SlotLength = GetRequiredTimeSlotLengthForOthers(iPallets, iCartons, "C", Convert.ToInt32(ddlCarrier.SelectedItem.Value));

                        SetCallHeight(SlotLength, ref tclocal, Color.GreenYellow, ltTime.Text.Trim(), iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);

                        //---Phase 19 point 1--//
                        tclocal.Attributes.Add("prefix", hdnAllocationType.Value + "-" + SlotLength.ToString());
                        //---------------------//

                        if (actSlotLength > SlotLength)
                        {
                            string[] StartTime = ltTime.Text.Split(':');
                            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1].Substring(0, 2)), 00);
                            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
                            for (int iLen = 1; iLen <= SlotLength - 1; iLen++)
                            {
                                if (time1.Hours == 23 && time1.Minutes == 45)
                                {
                                    time1 = new TimeSpan(00, 00, 00);
                                }
                                else
                                {
                                    time1 = time1.Add(time2);
                                }
                            }
                            SetCallHeight(actSlotLength - SlotLength, Color.Red, time1.Hours.ToString() + ":" + time1.Minutes.ToString(), iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);
                        }

                        if (isCurrentSlotFixed && hdnCurrentRole.Value == "Carrier")
                        {
                            tclocal.Attributes.Add("onclick", "setAletrnateSlot('" + CapacityWarning + "','" + hdnAvailableSlotTimeID.Value + "','" + hdnAvailableSiteDoorNumberID.Value + "','" + hdnAvailableSiteDoorNumber.Value + "','" + hdnAvailableFixedSlotID.Value + "','" + ltTime.Text + "','" + sSlotStartDay + "');");

                        }
                        //tclocal.Attributes.Add("onclick", "setAletrnateSlot('" + CapacityWarning + "','" + hdnAvailableSlotTimeID.Value + "','" + hdnAvailableSiteDoorNumberID.Value + "','" + hdnAvailableSiteDoorNumber.Value + "','" + hdnAvailableFixedSlotID.Value + "','" + ltTime.Text + "');");

                        //isAvailable = true;
                        break;
                    }
                    else if (hdnCurrentMode.Value == "Edit" && hdnSuggestedSlotTimeID.Value == iTimeSlotID.ToString() && hdnSuggestedSiteDoorNumberID.Value == iSiteDoorID.ToString())
                    { //This Booking //if (gvRow.Cells[4].Text == "This Booking") {                       

                        iPallets = iTotalPallets != (int?)null ? Convert.ToInt32(iTotalPallets) : 0;
                        iCartons = iTotalCartons != (int?)null ? Convert.ToInt32(iTotalCartons) : 0;
                        iLines = iTotalLines != (int?)null ? Convert.ToInt32(iTotalLines) : 0;
                        iLifts = 0;
                        isClickable = false;
                        //CreateDynamicDiv(ref tclocal,false);
                        SlotLength = GetTimeSlotLength();

                        SetCallHeight(SlotLength, ref tclocal, Color.GreenYellow, ltTime.Text.Trim(), iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);

                        //---Phase 19 point 1--//
                        tclocal.Attributes.Add("prefix", hdnAllocationType.Value + "-" + SlotLength.ToString());
                        //---------------------//

                        if (isCurrentSlotFixed && hdnCurrentRole.Value == "Carrier")
                        {
                            tclocal.Attributes.Add("onclick", "setAletrnateSlot('" + CapacityWarning + "','" + hdnAvailableSlotTimeID.Value + "','" + hdnAvailableSiteDoorNumberID.Value + "','" + hdnAvailableSiteDoorNumber.Value + "','" + hdnAvailableFixedSlotID.Value + "','" + ltTime.Text + "','" + sSlotStartDay + "');");

                        }
                        break;
                    }
                    else if (gvRow.Cells[6].Text == "Booked")
                    {   //Booked Slots                       
                        //SetCellBacknForeColor(ref tclocal, Color.Purple, isCurrentSlotFixed);
                        isClickable = false;
                        //CreateDynamicDiv(ref tclocal,false);
                        SlotLength = GetTimeSlotLength(VahicalTypeID);
                        //SetCallHeight(SlotLength, ref tclocal, Color.Purple, ltTime.Text.Trim());

                        //---Phase 19 point 1--//
                        tclocal.Attributes.Add("prefix", hdnAllocationType.Value + "-" + SlotLength.ToString());
                        //---------------------//
                        sVendorCarrierNameNew = sVendorCarrierName;
                        //For Display current vendor name-------
                        tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Purple, ltTime.Text.Trim(), iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);
                        if (!tempVendorName.Contains(sVendorCarrierName))
                        {
                            if (tempVendorName != string.Empty)
                            {
                                if (sVendorCarrierName != tempVendorName)
                                {
                                    tempVendorName = sVendorCarrierName + " / " + tempVendorName;
                                }
                            }
                            else
                            {
                                if (sVendorCarrierName != tempVendorName)
                                {
                                    tempVendorName = sVendorCarrierName;
                                }
                            }
                        }
                        sVendorCarrierName = tempVendorName;
                        //---------------------------------------

                        if (isCurrentSlotFixed && hdnCurrentRole.Value == "Carrier")
                        {
                            tclocal.Attributes.Add("onclick", "setAletrnateSlot('" + CapacityWarning + "','" + hdnAvailableSlotTimeID.Value + "','" + hdnAvailableSiteDoorNumberID.Value + "','" + hdnAvailableSiteDoorNumber.Value + "','" + hdnAvailableFixedSlotID.Value + "','" + ltTime.Text + "','" + sSlotStartDay + "');");

                        }

                        //isAvailable = true;
                        break;
                    }
                    else if (gvRow.Cells[5].Text == "FIXED SLOT")
                    {  // Fixed Slots        
                        sCurrentFixedSlotID = "-1";
                        isClickable = true;

                        //SlotLength = GetTimeSlotLength();
                        SlotLength = GetRequiredTimeSlotLengthForOthers(iPallets, iCartons, "C", Convert.ToInt32(ddlCarrier.SelectedItem.Value));

                        if (actSlotLength > SlotLength)
                        {
                            CapacityWarning = "true";
                        }
                        if (hdnAvailableFixedSlotID != null)
                        {
                            sCurrentFixedSlotID = hdnAvailableFixedSlotID.Value;
                        }

                        if (hdnAvailableFixedSlotID != null && hdnAvailableFixedSlotID.Value != hdnCurrentFixedSlotID.Value)
                        {

                            if (hdnCurrentRole.Value == "Carrier")
                            {
                                tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Red, ltTime.Text.Trim(), iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);
                            }
                            else
                            {
                                if (actSlotLength > SlotLength)
                                {

                                    string[] StartTime = ltTime.Text.Trim().Split(':');
                                    TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
                                    TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
                                    CarrierDetails vd = new CarrierDetails();

                                    if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                                    {
                                        isCurrentSlotFixed = vd.isCurrentSlotFixed;
                                        tempVendorName = SetCallHeight(SlotLength, ref tclocal, vd.CellColor, ltTime.Text.Trim(), iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);
                                    }
                                    else
                                    {
                                        tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.LightGray, ltTime.Text.Trim(), iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);
                                    }

                                    //if (hdnGraySlotClicked.Value == "true")
                                    //    SetCallHeight(SlotLength, ref tclocal, Color.GreenYellow, ltTime.Text.Trim());
                                    //else
                                    //   SetCallHeight(SlotLength, ref tclocal, Color.LightGray, ltTime.Text.Trim());
                                }
                                else
                                {
                                    tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Red, ltTime.Text.Trim(), iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);
                                }
                            }
                        }

                        //---Phase 19 point 1--//
                        tclocal.Attributes.Add("prefix", hdnAllocationType.Value + "-" + SlotLength.ToString());
                        //---------------------//

                        DateTime dtSelectedDate = LogicalSchedulDateTime;
                        string WeekDay = dtSelectedDate.ToString("ddd");

                        tclocal.Attributes.Add("onclick", "setAletrnateSlot('" + CapacityWarning + "','" + hdnAvailableSlotTimeID.Value + "','" + hdnAvailableSiteDoorNumberID.Value + "','" + hdnAvailableSiteDoorNumber.Value + "','" + hdnAvailableFixedSlotID.Value + "','" + ltTime.Text + "','" + WeekDay + "');");
                        //isAvailable = true;

                        //For Display current vendor name-------                        
                        if (!tempVendorName.Contains(sVendorCarrierName))
                        {
                            if (tempVendorName != string.Empty)
                            {
                                if (sVendorCarrierName != tempVendorName)
                                {
                                    tempVendorName = sVendorCarrierName + " / " + tempVendorName;
                                }
                            }
                            else
                            {
                                if (sVendorCarrierName != tempVendorName)
                                {
                                    tempVendorName = sVendorCarrierName;
                                }
                            }
                        }
                        sVendorCarrierName = tempVendorName;
                        //---------------------------------------

                        break;
                    }
                }


                //if (hdnSuggestedSlotTimeID.Value == iTimeSlotID.ToString()) {
                //    if (hdnSuggestedSlotTime.Value != ltTime.Text.Trim()) {
                //        isAvailable = false;
                //    }
                //}

            }
        }




        //if (!isAvailable) {     //Standard Booking
        //    isAvailable = false;
        //}



    }

    private void SetCallHeight(int SlotLength, Color CellBackColor, string SlotStartTime, string Pallets, string Cartoons, string Lines, string Lifts, string FixedSlotID = ""
        , string SlotTimeID = "", string SiteDoorNumberID = "", string SupplierType = "", string AllocationType = "")
    {
        if (SlotLength >= 1)
        {
            string[] StartTime = SlotStartTime.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            int iLen = 0;
            CarrierDetails vd = new CarrierDetails();
            for (iLen = 1; iLen <= SlotLength; iLen++)
            {
                if (time1.Hours == 23 && time1.Minutes == 45)
                {
                    time1 = new TimeSpan(00, 00, 00);
                }
                else
                {
                    time1 = time1.Add(time2);
                }
                vd.CarrierName = sVendorCarrierName;
                if (hdnCurrentRole.Value == "Carrier")
                {
                    vd.CellColor = Color.Black;
                    // new alternate slot logic   
                  
                        dtAlternateSlot.Rows.Add(new object[] {sSlotStartDay,iTimeSlotOrderBYID,
                    SlotStartTime,time1.Hours == 23 && time1.Minutes == 45 ? new TimeSpan(00, 00, 00) :time1.Add(time2),sVendorCarrierNameNew,
                    Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                    ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                    });
                   
                }
                else
                {
                    vd.CellColor = CellBackColor;
                    // new alternate slot logic
                  
                        dtAlternateSlot.Rows.Add(new object[] {sSlotStartDay,iTimeSlotOrderBYID,
                    SlotStartTime,time1.Hours == 23 && time1.Minutes == 45 ? new TimeSpan(00, 00, 00) :time1.Add(time2),sVendorCarrierNameNew,
                    Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                    ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                    });
                  
                }
                vd.isCurrentSlotFixed = isCurrentSlotFixed;

                if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                {
                    SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
                }

                SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
            }
        }
    }

    private string SetCallHeight(int SlotLength, ref TableCell tclocal, Color CellBackColor, string SlotStartTime, string Pallets,
       string Cartoons, string Lines, string Lifts, string FixedSlotID = ""
       , string SlotTimeID = "", string SiteDoorNumberID = "", string SupplierType = "", string AllocationType = "")
    {

        string VendorName = string.Empty;
        string temp = string.Empty;

        if (iTimeSlotID == 95)
            temp = string.Empty;
        TimeSpan NewAlternateTime1;
        if (SlotLength >= 1)
        {
            string[] StartTime = SlotStartTime.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            int iLen = 0;


            for (iLen = 1; iLen <= SlotLength; iLen++)
            {
                //if (GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) != NextRequiredSlot)
                //    time1 = time1.Add(time2);
                //else if (GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) == NextRequiredSlot) {
                //    SetCellBacknForeColor(ref tclocal, Color.Orange, isCurrentSlotFixed);
                //    break;
                //}
                CarrierDetails vd = new CarrierDetails();
                if (iLen == 1)
                {
                    NewAlternateTime1 = time1;
                    if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                    {
                        vd = SlotDoorCounter[time1.ToString() + iSiteDoorID.ToString()];
                        if (vd.CarrierName.Trim() != ddlCarrier.SelectedItem.Text)
                        {

                            if (hdnCurrentRole.Value == "Carrier")
                            {
                                if (isCurrentSlotFixed == false)
                                {
                                    VendorName = vd.CarrierName;
                                    isCurrentSlotFixed = vd.isCurrentSlotFixed;
                                    SetCellBacknForeColor(ref tclocal, vd.CellColor);
                                    // new alternate slot logic      
                                   
                                        dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                                    NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2), sVendorCarrierNameNew,
                                    Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                                    ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                                    });
                                  
                                }
                                else
                                {
                                    VendorName = ddlCarrier.SelectedItem.Text;
                                    SetCellBacknForeColor(ref tclocal, CellBackColor);
                                    // new alternate slot logic     
                                    
                                        dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                                    NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                                    Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                                    ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                                    });
                                  
                                }
                            }
                            else
                            {
                                VendorName = vd.CarrierName;
                                isCurrentSlotFixed = vd.isCurrentSlotFixed;
                                SetCellBacknForeColor(ref tclocal, vd.CellColor);
                                // new alternate slot logic
                                
                                    dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                                NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                                Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                                ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                                });
                               
                            }
                        }
                        else
                        {
                            if (isCurrentSlotFixed == false)
                            {
                                VendorName = vd.CarrierName;
                                isCurrentSlotFixed = vd.isCurrentSlotFixed;
                                SetCellBacknForeColor(ref tclocal, vd.CellColor);

                                // new alternate slot logic  
                               
                                    dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                                NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                                Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                                ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                                });
                                
                            }
                            else
                            {
                                if (vd.CellColor == Color.GreenYellow)
                                {
                                    VendorName = ddlCarrier.SelectedItem.Text;
                                    SetCellBacknForeColor(ref tclocal, CellBackColor);
                                    // new alternate slot logic  
                                   
                                        dtAlternateSlot.Rows.Add(new object[] {sSlotStartDay,iTimeSlotOrderBYID, 
                                    NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                                    Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                                    ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                                    });
                                   
                                }
                                else
                                {
                                    VendorName = vd.CarrierName;
                                    isCurrentSlotFixed = vd.isCurrentSlotFixed;
                                    if (isCurrentSlotFixed)
                                        SetCellBacknForeColor(ref tclocal, Color.Red);
                                    else
                                        SetCellBacknForeColor(ref tclocal, vd.CellColor);
                                    // new alternate slot logic   
                                   
                                        dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                                    NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                                    Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                                    ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                                    });
                                  
                                }
                            }
                        }
                    }
                    else
                    {

                        vd.CarrierName = sVendorCarrierName;
                        vd.CellColor = CellBackColor;
                        vd.isCurrentSlotFixed = isCurrentSlotFixed;
                        SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
                        SetCellBacknForeColor(ref tclocal, CellBackColor);
                        // new alternate slot logic
                       
                            dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                        NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                        Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                        ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                        });
                        
                    }

                    if (CellBackColor != Color.White)
                    {
                        CreateDynamicDiv(ref tclocal, SlotLength, SlotStartTime);
                    }
                }
                else
                {
                    if (time1.Hours == 23 && time1.Minutes == 45)
                    {
                        time1 = new TimeSpan(00, 00, 00);
                        strWeekDay = objWeekSetup.EndWeekday;
                        sSlotStartDay = strWeekDay;
                        string time = time1.Hours + ":" + time1.Minutes;
                        iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                    }
                    else
                    {
                        time1 = time1.Add(time2);
                        //strWeekDay = objWeekSetup.EndWeekday;
                        sSlotStartDay = strWeekDay;
                        string time = time1.Hours + ":" + time1.Minutes;
                        iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                    }
                    //if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString())) {
                    //    vd = SlotDoorCounter[time1.ToString() + iSiteDoorID.ToString()];
                    //    if (vd.CarrierName.Trim() != ddlCarrier.SelectedItem.Text) {
                    //        if (isCurrentSlotFixed == false) {
                    //            VendorName = vd.CarrierName;
                    //            isCurrentSlotFixed = vd.isCurrentSlotFixed;
                    //            //SetCellBacknForeColor(ref tclocal, vd.CellColor);
                    //        }
                    //        else {
                    //            VendorName = ddlCarrier.SelectedItem.Text;
                    //            //SetCellBacknForeColor(ref tclocal, CellBackColor);
                    //        }
                    //    }
                    //    else {
                    //        if (isCurrentSlotFixed == false) {
                    //            VendorName = vd.CarrierName;
                    //            isCurrentSlotFixed = vd.isCurrentSlotFixed;
                    //            //SetCellBacknForeColor(ref tclocal, vd.CellColor);
                    //        }
                    //        else {
                    //            VendorName = ddlCarrier.SelectedItem.Text;
                    //            //SetCellBacknForeColor(ref tclocal, CellBackColor);
                    //        }
                    //    }
                    //}
                    //else {                           
                    //    vd.CarrierName = sVendorCarrierName;
                    //    vd.CellColor = CellBackColor;
                    //    vd.isCurrentSlotFixed = isCurrentSlotFixed;
                    //    SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
                    //}

                    if (!SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                    {
                        vd.CarrierName = string.Empty; // sVendorCarrierName;
                        NewAlternateTime1 = time1;
                        if (hdnCurrentRole.Value == "Carrier" && CellBackColor != Color.GreenYellow)
                        {
                            vd.CellColor = Color.Black;

                            // new alternate slot logic 
                           
                                dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                        NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                        Pallets,Cartoons,Lines,Lifts,Color.Black.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                        ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                        });
                           
                        }
                        else
                        {
                            vd.CellColor = CellBackColor;
                            // new alternate slot logic
                          
                                dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                        NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                        Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                        ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                        });
                          
                        }

                        vd.isCurrentSlotFixed = isCurrentSlotFixed;
                        vd.CurrentFixedSlotID = sCurrentFixedSlotID;
                        SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);

                    }
                    else
                    {
                        NewAlternateTime1 = time1;
                       
                            dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                        NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                        Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                        ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                        });
                       
                    }
                }
            }
        }

        if (SlotLength >= 1 && NextRequiredSlot != string.Empty)
        {
            string[] StartTime = SlotStartTime.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);


            int iLen = 0;
            //CarrierDetails vd = new CarrierDetails();
            for (iLen = 1; iLen <= SlotLength; iLen++)
            {
                if (GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) != GetFormatedTime(NextRequiredSlot))
                {
                    if (time1.Hours == 23 && time1.Minutes == 45)
                    {
                        time1 = new TimeSpan(00, 00, 00);
                        strWeekDay = objWeekSetup.EndWeekday;
                        sSlotStartDay = strWeekDay;
                        string time = time1.Hours + ":" + time1.Minutes;
                        iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                    }
                    else
                    {
                        time1 = time1.Add(time2);
                        //strWeekDay = objWeekSetup.EndWeekday;
                        sSlotStartDay = strWeekDay;
                        string time = time1.Hours + ":" + time1.Minutes;
                        iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                    }
                }
                else if (GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) == GetFormatedTime(NextRequiredSlot))
                {
                    ////SetCellBacknForeColor(ref tclocal, Color.Orange);
                    //if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString())) {
                    //    SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
                    //}
                    //vd.CarrierName = sVendorCarrierNameNextSlot + " / " + sVendorCarrierName; // +" / " + ddlCarrier.SelectedItem.Text;
                    //vd.CellColor = Color.Orange;
                    //vd.isCurrentSlotFixed = isCurrentSlotFixed;
                    //SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);


                    SetCallHeightOverSpill(SlotLength - iLen + 1, Color.Orange, time1.Subtract(time2).ToString(), Pallets, Cartoons, Lines, Lifts);


                    break;
                }
            }
            NextRequiredSlot = string.Empty;
        }

        sCurrentFixedSlotID = "-1";
        return VendorName;
    }

    private void SetCallHeightOverSpill()
    {
        if (iNextSlotLength >= 1)
        {
            string[] StartTime = NextRequiredSlot.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            time1 = time1.Subtract(time2);
            int iLen = 0;
            CarrierDetails vd = new CarrierDetails();
            for (iLen = 1; iLen <= iNextSlotLength; iLen++)
            {
                if (time1.Hours == 23 && time1.Minutes == 45)
                {
                    time1 = new TimeSpan(00, 00, 00);
                }
                else
                {
                    time1 = time1.Add(time2);
                }

                if (iLen <= iNextSlotLength2)
                    vd.CarrierName = sVendorCarrierNameNextSlot + " / " + sVendorCarrierName;
                else
                    vd.CarrierName = sVendorCarrierName;

                //vd.CarrierName = sVendorCarrierNameNextSlot + " / " + sVendorCarrierName;

                if (hdnCurrentRole.Value == "Carrier")
                {
                    vd.CellColor = Color.Black;
                }
                else
                {
                    vd.CellColor = Color.Orange;
                }
                vd.isCurrentSlotFixed = isCurrentSlotFixed;

                if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                {
                    SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
                }

                SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
            }
        }
    }

    private void SetCallHeightOverSpill(int SlotLength, Color CellBackColor, string SlotStartTime, string Pallets, string Cartoons, string Lines, string Lifts)
    {
        if (SlotLength >= 1)
        {

            int LoopCounter;

            if (isNextSlotCovered)
                LoopCounter = iNextSlotLength;
            else
                LoopCounter = SlotLength;

            string[] StartTime = SlotStartTime.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            int iLen = 0;
            TimeSpan NewAlternateTime1;
            CarrierDetails vd = new CarrierDetails();
            for (iLen = 1; iLen <= LoopCounter; iLen++)
            {
                if (time1.Hours == 23 && time1.Minutes == 45)
                {
                    time1 = new TimeSpan(00, 00, 00);
                    strWeekDay = objWeekSetup.EndWeekday;
                    sSlotStartDay = strWeekDay;
                    string time = time1.Hours + ":" + time1.Minutes;
                    iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                }
                else
                {
                    time1 = time1.Add(time2);
                    //strWeekDay = objWeekSetup.EndWeekday;
                    sSlotStartDay = strWeekDay;
                    string time = time1.Hours + ":" + time1.Minutes;
                    iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                }

                if (iLen <= iNextSlotLength2)
                    vd.CarrierName = sVendorCarrierNameNextSlot + " / " + sVendorCarrierName;
                else
                    vd.CarrierName = sVendorCarrierName;
                NewAlternateTime1 = time1;
                //vd.CarrierName = sVendorCarrierNameNextSlot + " / " + sVendorCarrierName;
                if (hdnCurrentRole.Value == "Carrier")
                {
                    vd.CellColor = Color.Black;
                }
                else
                {
                    vd.CellColor = CellBackColor;
                }
                vd.isCurrentSlotFixed = isCurrentSlotFixed;

                if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                {
                    SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
                }

                SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
            }
        }
    }

    private void SetCellBacknForeColor(ref TableCell tclocal, Color CellBackColor)
    {
        string temp = string.Empty;

        if (iSiteDoorID == 58 && iTimeSlotID == 50)
            temp = string.Empty;

        if (CellBackColor == Color.Red)
        {
            tclocal.BackColor = System.Drawing.Color.White;
            tclocal.ForeColor = System.Drawing.Color.Black;
            tclocal.BorderColor = System.Drawing.Color.Red;
            tclocal.BorderWidth = 2;
            return;
        }
        else if (CellBackColor == Color.Purple)
        {
            tclocal.BackColor = System.Drawing.Color.Purple;
            tclocal.ForeColor = System.Drawing.Color.White;
            if (isCurrentSlotFixed)
            {
                tclocal.BorderColor = System.Drawing.Color.Red;
                tclocal.BorderWidth = 2;
            }
            else
            {
                tclocal.BorderColor = System.Drawing.Color.Black;
                tclocal.BorderStyle = BorderStyle.Dashed;
                tclocal.BorderWidth = 2;
            }

            return;
        }
        else if (CellBackColor == Color.Orange)
        {
            tclocal.BackColor = System.Drawing.Color.Orange;
            tclocal.ForeColor = System.Drawing.Color.Black;
            if (isCurrentSlotFixed)
            {
                tclocal.BorderColor = System.Drawing.Color.Red;
                tclocal.BorderWidth = 2;
            }
            return;
        }
        else if (CellBackColor == Color.GreenYellow)
        {
            tclocal.BackColor = System.Drawing.Color.GreenYellow;
            tclocal.ForeColor = System.Drawing.Color.Black;
            if (isCurrentSlotFixed)
            {
                tclocal.BorderColor = System.Drawing.Color.Red;
                tclocal.BorderWidth = 2;
            }
            return;
        }
        else if (CellBackColor == Color.Black)
        {
            tclocal.BackColor = System.Drawing.Color.Black;
            tclocal.ForeColor = System.Drawing.Color.White;
            tclocal.BorderColor = System.Drawing.Color.White;
            tclocal.BorderWidth = 1;
            return;
        }
        else if (CellBackColor == Color.White)
        {
            tclocal.BackColor = System.Drawing.Color.White;
            tclocal.ForeColor = System.Drawing.Color.Black;
            tclocal.BorderColor = System.Drawing.Color.Black;
            return;
        }
        else if (CellBackColor == Color.LightGray)
        {
            tclocal.BackColor = System.Drawing.Color.LightGray;
            tclocal.ForeColor = System.Drawing.Color.Black;
            tclocal.BorderColor = System.Drawing.Color.Black;
            if (isCurrentSlotFixed)
            {
                tclocal.BorderColor = System.Drawing.Color.Red;
                tclocal.BorderWidth = 2;
            }
            return;
        }
    }

    protected void btnConfirmAlternateSlot_Click(object sender, EventArgs e)
    {
        if (hdnSuggestedFixedSlotID.Value != "-1")
        {
            ConfirmFixedSlotBooking();
        }
        else if (hdnSuggestedSiteDoorNumber.Value == string.Empty)
        {
            MakeBooking(BookingType.NonTimedDelivery.ToString());
        }
        else
        {
            MakeBooking(BookingType.BookedSlot.ToString());
        }
    }

    private void PaintAlternateScreenForVendor()
    {

        ltSite.Text = ddlSite.innerControlddlSite.SelectedItem.Text;
        //ltVendor_3.Text = ddlCarrier; // ((HiddenField)ucSeacrhVendor1.FindControl("hdnVandorName")).Value;

        ltCarrier_3.Text = ddlCarrier.SelectedItem.Text;
        ltOtherCarrier_3.Text = "[" + txtCarrierOther.Text.Trim() + "]";

        if (txtCarrierOther.Text.Trim() != string.Empty)
        {
            ltOtherCarrier_3.Visible = true;
        }
        else
        {
            ltOtherCarrier_3.Visible = false;
        }

        ltVehicleType_3.Text = ddlVehicleType.SelectedItem.Text;
        ltOtherVehicleType_3.Text = "[" + txtVehicleOther.Text.Trim() + "]";
        if (txtVehicleOther.Text.Trim() != string.Empty)
        {
            ltOtherVehicleType_3.Visible = true;
        }
        else
        {
            ltOtherVehicleType_3.Visible = false;
        }
        ltSchedulingDate_3.Text = ActualSchedulDateTime.ToString("dd/MM/yyyy");
        ltTimeSuggested_3.Text = ltSuggestedSlotTime.Text;


        ltExpectedLifts.Text = "-";
        ltExpectedPallets.Text = TotalPallets.Trim() != string.Empty ? TotalPallets.Trim() : "-";
        ltExpectedLines.Text = TotalLines.Trim() != string.Empty ? TotalLines.Trim() : "-";
        ltExpectedCartons.Text = TotalCartons.Trim() != string.Empty ? TotalCartons.Trim() : "-";
    }

    private void GenerateAlternateSlotForVendor()
    {
        dtAlternateSlot = new DataTable();
        dtAlternateSlot.Columns.Add("SlotStartDay", typeof(string));
        dtAlternateSlot.Columns.Add("TimeSlotOrderBYID", typeof(string));
        dtAlternateSlot.Columns.Add("ArrSlotTime", typeof(string));
        dtAlternateSlot.Columns.Add("EstSlotTime", typeof(string));
        dtAlternateSlot.Columns.Add("Vendor", typeof(string));
        dtAlternateSlot.Columns.Add("Pallets", typeof(string));
        dtAlternateSlot.Columns.Add("Cartoons", typeof(string));
        dtAlternateSlot.Columns.Add("Lines", typeof(string));
        dtAlternateSlot.Columns.Add("Lifts", typeof(string));
        dtAlternateSlot.Columns.Add("Color", typeof(string));
        dtAlternateSlot.Columns.Add("DoorNo", typeof(string));
        dtAlternateSlot.Columns.Add("FixedSlotID", typeof(string));
        dtAlternateSlot.Columns.Add("SlotTimeID", typeof(string));
        dtAlternateSlot.Columns.Add("SiteDoorNumberID", typeof(string));
        dtAlternateSlot.Columns.Add("SupplierType", typeof(string));
        dtAlternateSlot.Columns.Add("AllocationType", typeof(string));
        dtAlternateSlot.Columns.Add("BookingID", typeof(string));
        dtAlternateSlot.Columns.Add("BookingStatus", typeof(string));
        dtAlternateSlot.Columns.Add("BookingStatusID", typeof(string));
        dtAlternateSlot.Columns.Add("ArrSlotTime15min", typeof(string));
        dtAlternateSlot.Columns.Add("EstSlotTime15min", typeof(string));
        dtAlternateSlot.Columns.Add("DoorNameDisplay", typeof(string));
        dtAlternateSlot.Columns.Add("DoorNoDisplay", typeof(string));
        dtAlternateSlot.Columns.Add("VendorDisplay", typeof(string));
        dtAlternateSlot.Columns.Add("EstSlotTimeDisplay", typeof(string));
        dtAlternateSlot.Columns.Add("Weekday", typeof(string));
        int? iTotalPallets = TotalPallets.Trim() != string.Empty ? Convert.ToInt32(TotalPallets.Trim()) : (int?)null;
        int? iTotalCartons = TotalCartons.Trim() != string.Empty ? Convert.ToInt32(TotalCartons.Trim()) : (int?)null;

        actSlotLength = GetRequiredTimeSlotLength(iTotalPallets, iTotalCartons);

        #region ------------Basic Information--------------------------
        //-----------------Start Collect Basic Information --------------------------//
        DateTime dtSelectedDate = LogicalSchedulDateTime;
        string WeekDay = dtSelectedDate.ToString("dddd");

        DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
        oMASSIT_WeekSetupBE.ScheduleDate = LogicalSchedulDateTime;

        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
        {

            oMASSIT_WeekSetupBE.Action = "ShowAll";
            lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup.Count <= 0)
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
                return;
            }
            else if (lstWeekSetup[0].StartTime == null)
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
                return;
            }
        }


        APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
        MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

        //------Check for Specific Entry First--------//
        //APPSIT_DoorOpenTimeSpecificBAL oAPPSIT_DoorOpenTimeSpecificBAL = new APPSIT_DoorOpenTimeSpecificBAL();
        //MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE = new MASSIT_DoorOpenTimeSpecificBE();
        //oMASSIT_DoorOpenTimeSpecificBE.Action = "GetDoorConstraintsSpecific";
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime = new MASSIT_DoorOpenTimeBE();
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        //oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday = WeekDay;
        //oMASSIT_DoorOpenTimeSpecificBE.WeekStartDate = dtMon;
        //List<MASSIT_DoorOpenTimeSpecificBE> lstDoorConstraintsSpecific = oAPPSIT_DoorOpenTimeSpecificBAL.GetDoorConstraintsSpecificBAL(oMASSIT_DoorOpenTimeSpecificBE);
        //if (lstDoorConstraintsSpecific.Count > 0) { //Get Specific settings 
        //    for (int iCount = 0; iCount < lstDoorConstraintsSpecific.Count; iCount++) {
        //        DoorIDs += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + ",";
        //    }
        //}
        //else {  //Get Generic settings 
        //    oMASSIT_DoorOpenTimeBE.Action = "GetDoorConstraints";
        //    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        //    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        //    oMASSIT_DoorOpenTimeBE.Weekday = WeekDay;
        //    List<MASSIT_DoorOpenTimeBE> lstDoorConstraints = oMASSIT_DoorOpenTimeBAL.GetDoorConstraintsBAL(oMASSIT_DoorOpenTimeBE);
        //    for (int iCount = 0; iCount < lstDoorConstraints.Count; iCount++) {
        //        DoorIDs += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";

        //    }
        //}

        if (strOpenDoorIds != ",")
            DoorIDs = strOpenDoorIds;
        else
            DoorIDs = GetDoorConstraints();

        oMASSIT_DoorOpenTimeBE.Action = "GetDoorNoAndType";
        oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<MASSIT_DoorOpenTimeBE> lstDoorNoType = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

        int iDoorCount = lstDoorNoType.Count;

        //decimal decCellWidth = (90 / (iDoorCount + 1));
        //double dblCellWidth = Convert.ToDouble(Math.Ceiling(decCellWidth));

        int dblCellWidth = 165;
        int i1stColumnWidth = 145;

        //-------------------------End Collect Basic Information ------------------------------//
        #endregion

        #region --------------Grid Header---------------
        //-------------Start Grid Header--------------//
        Table tblSlot = new Table();
        tblSlot.CellSpacing = 0;
        tblSlot.CellPadding = 0;
        tblSlot.CssClass = "FixedTables";
        tblSlot.ID = "Open_Text_General";
        tblSlot.Width = Unit.Pixel((dblCellWidth * iDoorCount) + i1stColumnWidth);

        tableDiv_GeneralVendor.Controls.Add(tblSlot);

        TableRow trDoorNo = new TableRow();
        trDoorNo.TableSection = TableRowSection.TableHeader;
        TableHeaderCell tcDoorNo = new TableHeaderCell();
        tcDoorNo.Text = "Door Name / Door Type";  // +"<input type=image src='../../../Images/arrowleft.gif' style=height:16px; width:26px; />";
        tcDoorNo.BorderWidth = Unit.Pixel(1);
        tcDoorNo.Width = Unit.Pixel(i1stColumnWidth);
        trDoorNo.Cells.Add(tcDoorNo);

        TableRow trTime = new TableRow();
        trTime.TableSection = TableRowSection.TableHeader;
        TableHeaderCell tcTime = new TableHeaderCell();
        tcTime.Text = "Time";
        tcTime.BorderWidth = Unit.Pixel(1);
        tcTime.Width = Unit.Pixel(i1stColumnWidth);
        trTime.Cells.Add(tcTime);

        for (int iCount = 1; iCount <= iDoorCount; iCount++)
        {
            tcDoorNo = new TableHeaderCell();
            tcDoorNo.BorderWidth = Unit.Pixel(1);
            tcDoorNo.Text = lstDoorNoType[iCount - 1].DoorNo.DoorNumber.ToString() + " / " + lstDoorNoType[iCount - 1].DoorType.DoorType; ;
            tcDoorNo.Width = Unit.Pixel(dblCellWidth);
            trDoorNo.Cells.Add(tcDoorNo);

            tcTime = new TableHeaderCell();
            tcTime.BorderWidth = Unit.Pixel(1);
            tcTime.Text = "Vendor/Carrier";
            tcTime.Width = Unit.Pixel(dblCellWidth);
            trTime.Cells.Add(tcTime);
        }
        trDoorNo.BackColor = System.Drawing.Color.LightGray;
        tblSlot.Rows.Add(trDoorNo);

        trTime.BackColor = System.Drawing.Color.LightGray;
        tblSlot.Rows.Add(trTime);
        //-------------End Grid Header -------------//
        #endregion

        #region --------------Slot Ploting---------------
        //-------------Start Slot Ploting--------------//

        //--------Find all Fixed Slots, Bookings For all Vendor/Carrier-------//
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.VehicleType = new MASSIT_VehicleTypeBE();
        //oAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
        lstAllFixedDoor = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);
        //--------------------------------------//

        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        {
            oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1);
            List<APPBOK_BookingBE> lstAllFixedDoorStartWeek = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

            //string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + ":" + lstWeekSetup[0].StartTime.Value.Minute.ToString();

            //Filetr Non Time Delivery
            lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
            {
                return St.SlotTime.SlotTime != "";
            });

            if (lstAllFixedDoorStartWeek != null && lstAllFixedDoorStartWeek.Count > 0)
            {
                lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate(APPBOK_BookingBE St)
                {
                    DateTime? dt = (DateTime?)null;
                    if (St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty)
                    {
                        dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    }
                    else
                    {
                        dt = new DateTime(1900, 1, 1, 0, 0, 0);
                    }
                    //DateTime dt = new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0);
                    return dt >= lstWeekSetup[0].StartTime;
                });

                if (lstAllFixedDoorStartWeek.Count > 0)
                {
                    lstAllFixedDoor = MergeListCollections(lstAllFixedDoor, lstAllFixedDoorStartWeek);
                    //List<APPBOK_BookingBE>  lstAllFixedDoorFinal = new List<APPBOK_BookingBE>(lstAllFixedDoor.Count + lstAllFixedDoorStartWeek.Count);
                }
            }
        }

        //if (hdnCurrentRole.Value == "Carrier") {
        //    lstAllFixedDoor = lstAllFixedDoor.FindAll(delegate(APPBOK_BookingBE fx) {
        //        return fx.SupplierType == "C";
        //    });
        //}


        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();

        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);

        for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
        {
            string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + "." + lstWeekSetup[0].StartTime.Value.Minute.ToString("00");
            if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
            {
                StartTime = "24." + lstWeekSetup[0].StartTime.Value.Minute.ToString("00");
            }

            string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString("00");
            if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
            {
                EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString("00");
            }

            decimal dSlotTime = Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.'));
            decimal dStartTime = Convert.ToDecimal(StartTime);
            decimal dEndTime = Convert.ToDecimal(EndTime);

            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
            {
                dEndTime = Convert.ToDecimal("24.00");
            }

            if (dSlotTime >= dStartTime && dSlotTime <= dEndTime)
            {
                TableRow tr = new TableRow();
                tr.TableSection = TableRowSection.TableBody;
                TableCell tc = new TableCell();
                tc.BorderWidth = Unit.Pixel(1);

                if (lstSlotTime.Count - 1 > jCount)
                    tc.Text = lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime);
                else
                    tc.Text = lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);

                tc.Width = Unit.Pixel(i1stColumnWidth);
                tr.Cells.Add(tc);

                for (int iCount = 1; iCount <= iDoorCount; iCount++)
                {
                    tc = new TableCell();
                    tc.BorderWidth = Unit.Pixel(0);
                    tc.Width = Unit.Pixel(dblCellWidth);

                    iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
                    iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
                    sTimeSlot = lstSlotTime[jCount].SlotTime;
                    sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
                    sSlotStartDay = lstWeekSetup[0].StartWeekday;
                    if (iTimeSlotID == 177)
                        iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
                    //----Sprint 3b------//
                    iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
                    //------------------//

                    //bug fixes
                    objWeekSetup = lstWeekSetup[0];
                    strWeekDay = lstWeekSetup[0].StartWeekday;

                    Literal ddl = SetCellColor(ref tc);
                    tc.Controls.Add(ddl);
                    tr.Cells.Add(tc);
                }
                tblSlot.Rows.Add(tr);
            }
        }

        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        {
            for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
            {
                string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString("00");
                if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
                {
                    EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString("00");
                }
                if (Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.')) <= Convert.ToDecimal(EndTime))
                {
                    TableRow tr = new TableRow();
                    tr.TableSection = TableRowSection.TableBody;
                    TableCell tc = new TableCell();
                    tc.BorderWidth = Unit.Pixel(1);

                    if (lstSlotTime.Count - 1 > jCount)
                        tc.Text = lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime);
                    else
                        tc.Text = lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);

                    tc.Width = Unit.Pixel(i1stColumnWidth);
                    tr.Cells.Add(tc);

                    for (int iCount = 1; iCount <= iDoorCount; iCount++)
                    {


                        tc = new TableCell();
                        tc.Width = Unit.Pixel(dblCellWidth);
                        tc.BorderWidth = Unit.Pixel(0);

                        iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
                        iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
                        sTimeSlot = lstSlotTime[jCount].SlotTime;
                        sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
                        sSlotStartDay = lstWeekSetup[0].EndWeekday;

                        //----Sprint 3b------//
                        iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
                        //------------------//
                        //bug fixes
                        objWeekSetup = lstWeekSetup[0];
                        strWeekDay = lstWeekSetup[0].StartWeekday;

                        Literal ddl = SetCellColor(ref tc);
                        tc.Controls.Add(ddl);
                        tr.Cells.Add(tc);

                    }
                    tblSlot.Rows.Add(tr);

                }
            }
        }

        //-------------End Slot Ploting--------------//
        #endregion

        #region -------------Start Finding Insufficent Slot Time---------------------
        Color cBackColor, cBorderColor, cBackColorPreviousCell, cBorderColorPreviousCell;
        bool isNextSlot;
        bool isNewCell = false;
        cBorderColorPreviousCell = Color.Empty;
        string strCheckedHour = "-10";
        bool isValidTimeSlot = false;
        bool isValidHour = false;

        for (int iCell = 1; iCell <= iDoorCount; iCell++)
        {
            cBackColorPreviousCell = Color.Empty;
            for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
            {

                //if (iRow==15 || iRow == 16 || iRow==18)
                //    cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;

                //FindNextSufficentSlotTime(ref tblSlot, iCell, iRow);
                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                if (cBackColor == Color.White && cBorderColor != Color.Red)
                {
                    isNextSlot = true;
                    for (int iLen = 0; iLen < actSlotLength; iLen++)
                    {
                        if (iRow + iLen < tblSlot.Rows.Count)
                        {
                            cBackColor = tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor;
                            if (cBackColor == Color.GreenYellow)
                            {
                                isNextSlot = true;
                                break;
                            }
                            if (cBackColor != Color.White)
                            {
                                isNextSlot = false;
                                break;
                            }
                        }
                        else
                        {
                            isNextSlot = false;
                            break;
                        }
                    }
                    if (isNextSlot == false)
                    {

                        //tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightGray;

                        if (hdnCurrentRole.Value == "Carrier")
                        {
                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
                            tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
                        }
                        else
                        {
                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightGray;
                            tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
                        }

                        //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
                        //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
                        //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                    }
                }
                else if (cBackColor == Color.GreenYellow)
                {
                    string abbr = tblSlot.Rows[iRow].Cells[iCell].Attributes["abbr"];
                    if (abbr == null || abbr == string.Empty)
                    {
                        if (cBorderColor != Color.Red)
                        {
                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.White;
                            tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 1;
                        }
                        else if (cBorderColor == Color.Red)
                        {
                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
                            tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
                        }
                    }
                }

                #region -- Blackout the slots which are not fullfill the delivery constraints-------//
                //Check delivery constraints//
                string att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];

                if (att != null && att != string.Empty && att.Contains("CheckAletrnateSlot"))
                {
                    att = att.Replace("CheckAletrnateSlot(", "");
                    att = att.Replace(");", "");
                    string[] Arratt = att.Split(',');

                    string att0 = Arratt[0].Replace("'", "");
                    string att3 = Arratt[3].Replace("'", "");
                    string att4 = Arratt[5].Replace("'", "");
                    string att5 = Arratt[5].Replace("'", "");
                    //--------- Sprint 3b----------//
                    string att6 = Arratt[6].Replace("'", "");
                    //-----------------------------//


                    if (att3.Split(':')[0] != strCheckedHour)
                    {
                        isValidTimeSlot = CheckValidSlot(att3, att5, Convert.ToString(TotalPallets), Convert.ToString(TotalLines));
                        strCheckedHour = att3.Split(':')[0];
                        isValidHour = isValidTimeSlot;
                    }
                    else
                    {
                        isValidTimeSlot = isValidHour;
                    }


                    //--Sprint 3a start - Check Before and after time//
                    if (LogicalSchedulDateTime.ToString("ddd").ToLower() == att4.ToLower())
                    {
                        if (Convert.ToInt32(att6) < Convert.ToInt32(BeforeSlotTimeID) || Convert.ToInt32(att6) > Convert.ToInt32(AfterSlotTimeID))
                        {
                            isValidTimeSlot = false;
                        }
                    }
                    else
                    {
                        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
                        {
                            if (LogicalSchedulDateTime.AddDays(-1).ToString("ddd").ToLower() == att4.ToLower())
                            {
                                if (Convert.ToInt32(att6) < Convert.ToInt32(BeforeSlotTimeID) || Convert.ToInt32(att6) > Convert.ToInt32(AfterSlotTimeID))
                                {
                                    isValidTimeSlot = false;
                                }
                            }
                        }
                    }
                    //-----------Sprint 3a End ---------------//


                    //----Stage 10 Point 10L 5.2-----//
                    //Carrie Trime Constraint//
                    if (ddlSite.IsBookingExclusions)
                    {   //Only if Booking constraints set to yes 

                        if (LogicalSchedulDateTime.ToString("ddd").ToLower() == att4.ToLower())
                        {
                            if (Convert.ToInt32(att6) < Convert.ToInt32(VendorBeforeSlotTimeID) || Convert.ToInt32(att6) > Convert.ToInt32(VendorAfterSlotTimeID))
                            {
                                isValidTimeSlot = false;
                            }
                        }
                        else
                        {
                            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
                            {
                                if (LogicalSchedulDateTime.AddDays(-1).ToString("ddd").ToLower() == att4.ToLower())
                                {
                                    if (Convert.ToInt32(att6) < Convert.ToInt32(VendorBeforeSlotTimeID) || Convert.ToInt32(att6) > Convert.ToInt32(VendorAfterSlotTimeID))
                                    {
                                        isValidTimeSlot = false;
                                    }
                                }
                            }
                        }
                    }
                    //---------------------//

                }
                else if (att != null && att != string.Empty && att.Contains("setAletrnateSlot"))
                {
                    att = att.Replace("setAletrnateSlot(", "");
                    att = att.Replace(");", "");
                    string[] Arratt = att.Split(',');
                    string att5 = Arratt[5].Replace("'", "");
                    string att6 = Arratt[6].Replace("'", "");

                    if (att5.Split(':')[0] != strCheckedHour)
                    {
                        isValidTimeSlot = CheckValidSlot(att5, att6, Convert.ToString(TotalPallets), Convert.ToString(TotalLines));
                        strCheckedHour = att5.Split(':')[0];
                        isValidHour = isValidTimeSlot;
                    }
                    else
                    {
                        isValidTimeSlot = isValidHour;
                    }


                    //if (isValidTimeSlot == false) {
                    //    strCheckedHour = att5.Split(':')[0];
                    //}
                }

                att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];

                if (att != null && att != string.Empty && isValidTimeSlot == false)
                {

                    if (tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.White && tblSlot.Rows[iRow].Cells[iCell].BorderColor == Color.Red)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.White;
                    }
                    string att4 = string.Empty;
                    if (att.Contains("setAletrnateSlot"))
                    {
                        att = att.Replace("setAletrnateSlot(", "");
                        att = att.Replace(");", "");
                        string[] Arratt = att.Split(',');
                        string att0 = Arratt[0].Replace("'", "");
                        string att1 = Arratt[1].Replace("'", "");
                        string att2 = Arratt[2].Replace("'", "");
                        string att3 = Arratt[3].Replace("'", "");
                        att4 = Arratt[4].Replace("'", "");  //fixed slot id
                        string att5 = Arratt[5].Replace("'", "");
                        string att6 = Arratt[6].Replace("'", "");

                        //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "setAletrnateSlotForVendor('true','" + att1 + "','" + att2 + "','" + att3 + "','" + att4 + "','" + att5 + "','" + att6 + "');");

                    }

                    if (att4 == string.Empty || att4 == "-1")
                    {

                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
                        tblSlot.Rows[iRow].Cells[iCell].BorderColor = Color.Black;
                        tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 1;

                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");

                    }
                }
                #endregion--------------------------------------------------------------------------//

                #region----Blackout the slots which are not matched with the seleted vehicle type-------//
                att = tblSlot.Rows[iRow].Cells[iCell].Attributes["onclick"];

                if (!strVehicleDoorTypeMatrix.Contains(lstDoorNoType[iCell - 1].DoorNo.SiteDoorTypeID.ToString() + ","))
                {
                    if (tblSlot.Rows[iRow].Cells[iCell].BackColor == Color.White && tblSlot.Rows[iRow].Cells[iCell].BorderColor == Color.Red)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.White;
                    }

                    string att4 = string.Empty;
                    if (att != null && att != string.Empty && att.Contains("setAletrnateSlot"))
                    {
                        att = att.Replace("setAletrnateSlot(", "");
                        att = att.Replace(");", "");
                        string[] Arratt = att.Split(',');
                        string att0 = Arratt[0].Replace("'", "");
                        string att1 = Arratt[1].Replace("'", "");
                        string att2 = Arratt[2].Replace("'", "");
                        string att3 = Arratt[3].Replace("'", "");
                        att4 = Arratt[4].Replace("'", "");  //fixed slot id
                        string att5 = Arratt[5].Replace("'", "");
                        string att6 = Arratt[6].Replace("'", "");

                        //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "setAletrnateSlotForVendor('true','" + att1 + "','" + att2 + "','" + att3 + "','" + att4 + "','" + att5 + "','" + att6 + "');");

                    }

                    if (att4 == "520")
                    {
                        att4 = "520";
                    }

                    if (att4 == string.Empty || att4 == "-1")
                    {
                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Black;
                        tblSlot.Rows[iRow].Cells[iCell].BorderColor = Color.Black;
                        tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 1;

                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
                        tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                    }
                }
                #endregion---------------------------------------------------------------------------//

                #region Row Spaning
                //-----------------------Row Spaning-------------//

                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                if (cBackColor != cBackColorPreviousCell || cBorderColor != cBorderColorPreviousCell)
                {
                    isNewCell = true;
                }

                bool isCellCreated = false;
                if (cBackColor == cBackColorPreviousCell && cBackColor != Color.White)
                {

                    isCellCreated = true;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");

                    if (cBorderColor == Color.Red || cBackColor == Color.Purple)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "2px");
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "2px");
                    }
                    else
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "1px");
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "1px");
                    }

                    if (cBackColorPreviousCell == cBackColor && iRow > 0 && isNewCell == true)
                    {
                        isNewCell = false;
                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "0px");

                        if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "2px");
                        }
                        else
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "1px");
                        }
                    }
                }
                else if (cBackColor == cBackColorPreviousCell && cBorderColor == Color.Red)
                {

                    isCellCreated = true;

                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");

                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "2px");
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "2px");


                    if (cBackColorPreviousCell == cBackColor && iRow > 0 && isNewCell == true)
                    {
                        isNewCell = false;

                        if (cBorderColorPreviousCell != Color.Red)
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");

                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "0px");
                        if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "2px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "2px");
                        }
                        else
                        {
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-top-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-left-width", "1px");
                            tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-right-width", "1px");
                        }
                    }
                }

                if (isCellCreated == false && iRow > 0)
                {
                    if (cBorderColorPreviousCell == Color.Red || cBackColorPreviousCell == Color.Purple)
                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "2px");
                    else
                        tblSlot.Rows[iRow - 1].Cells[iCell].Style.Add("border-bottom-width", "1px");
                }

                string abbr1 = tblSlot.Rows[iRow].Cells[iCell].Attributes["abbr"];
                if (abbr1 != null && abbr1 != string.Empty)
                {
                    if (cBorderColor == Color.Red || cBackColor == Color.Purple)
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");
                    }
                    else
                    {
                        tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "1px");
                    }
                }

                cBackColorPreviousCell = cBackColor;
                cBorderColorPreviousCell = cBorderColor;

                //--------------------------------------------------//
                #endregion

                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                if (cBackColor == Color.Black)
                {
                    tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 0;
                    tblSlot.Rows[iRow].Cells[iCell].BorderColor = Color.Black;

                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseover", "this.style.cursor = 'default'");
                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onmouseout", "this.style.cursor = 'default'");
                    tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                }
            }
        }

        #endregion

        #region Set Background color for Fixed slot
        bool GreenCellFound = false;
        for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++)
        {
            for (int iCell = 1; iCell <= iDoorCount; iCell++)
            {
                cBackColor = tblSlot.Rows[iRow].Cells[iCell].BackColor;
                cBorderColor = tblSlot.Rows[iRow].Cells[iCell].BorderColor;

                //------------Phase 19 Point 1---------------------------//
                string prefix = tblSlot.Rows[iRow].Cells[iCell].Attributes["prefix"];
                string[] arrprefix = new string[2];
                if (!string.IsNullOrEmpty(prefix))
                    arrprefix = prefix.Split('-');
                //------------------------------------------------------//


                if (cBorderColor == Color.Red && (cBackColor == Color.White))
                {
                    if (string.IsNullOrEmpty(arrprefix[0]) || arrprefix[0] == "H")
                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightYellow;
                    else
                    {
                        for (int iLen = 0; iLen < Convert.ToInt32(arrprefix[1]); iLen++)
                        {
                            if (iRow + iLen < tblSlot.Rows.Count)
                            {
                                Color cBackColorInner = tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor;
                                Color cBorderColorInner = tblSlot.Rows[iRow + iLen].Cells[iCell].BorderColor;

                                if (cBorderColorInner == Color.Red && (cBackColorInner == Color.White))
                                {
                                    tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor = Color.LightPink;
                                }
                            }
                        }
                    }
                    tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.Black;
                }
                if (cBorderColor == Color.Red && cBackColor == Color.GreenYellow && GreenCellFound == true)
                {
                    if (string.IsNullOrEmpty(arrprefix[0]) || arrprefix[0] == "H")
                        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.LightYellow;
                    else
                    {
                        for (int iLen = 0; iLen < Convert.ToInt32(arrprefix[1]); iLen++)
                        {
                            if (iRow + iLen < tblSlot.Rows.Count)
                            {
                                Color cBackColorInner = tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor;
                                Color cBorderColorInner = tblSlot.Rows[iRow + iLen].Cells[iCell].BorderColor;

                                if (cBorderColorInner == Color.Red && cBackColorInner == Color.GreenYellow && GreenCellFound == true)
                                {
                                    tblSlot.Rows[iRow + iLen].Cells[iCell].BackColor = Color.LightPink;
                                }
                            }
                        }
                    }
                    tblSlot.Rows[iRow].Cells[iCell].ForeColor = Color.Black;
                }

                if (cBackColor == Color.LightYellow || cBackColor == Color.Purple)
                {
                    //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "");
                    tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 2;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "2px");
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "2px");
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "2px");
                }
                else
                {
                    tblSlot.Rows[iRow].Cells[iCell].BorderWidth = 1;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "1px");
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "1px");
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "1px");
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "1px");
                }

                if (cBackColor == Color.GreenYellow)
                    GreenCellFound = true;
            }
        }
        #endregion
    }

    public List<T> MergeListCollections<T>(List<T> firstList, List<T> secondList)
    {
        List<T> mergedList = new List<T>();
        mergedList.InsertRange(0, firstList);
        mergedList.InsertRange(mergedList.Count, secondList);
        return mergedList;
    }

    protected void btnConfirmAlternateSlotVendor_Click(object sender, EventArgs e)
    {
        if (hdnSuggestedFixedSlotID.Value != "-1")
        {
            ConfirmFixedSlotBooking();
        }
        else if (hdnSuggestedSiteDoorNumber.Value == string.Empty)
        {
            MakeBooking(BookingType.NonTimedDelivery.ToString());
        }
        else
        {
            if (hdnCurrentMode.Value == "Edit")
            {
                //chack if booking effecting another booking or door is closed         
                int BookingLength = GetRequiredTimeSlotLength(Convert.ToInt32(TotalPallets), Convert.ToInt32(TotalCartons));

                //-------------CHECK IF BOOKING EFFECTING ANOTHER BOOKING PHASE 12 POINT 3---------------//
                if (CheckCurrentBookingImpactEdit(Convert.ToInt32(hdnSuggestedSiteDoorNumberID.Value), Convert.ToInt32(hdnSuggestedSlotTimeID.Value), BookingLength))
                {
                    isProvisional = true;
                    ProvisionalReasonType = 13;
                    ProvisionalReason = "EDITED";
                }
                //---------------------------------------------------------------------//
            }
            MakeBooking(BookingType.BookedSlot.ToString());
        }
    }

    protected void hdnbutton_Click(object sender, EventArgs e)
    {

        SlotDoorCounter.Clear();
        SlotDoorCounter = new Dictionary<string, CarrierDetails>();
        isProvisional = false;

        if (hdnCurrentRole.Value != "Carrier")
        {
            ltTimeSlot.Text = hdnSuggestedSlotTime.Value;
            ltDoorNo.Text = hdnSuggestedSiteDoorNumber.Value;
        }
        if (hdnCurrentRole.Value == "Carrier")
        {
            ltTimeSlot_1.Text = hdnSuggestedSlotTime.Value;
            ltDoorNo_1.Text = hdnSuggestedSiteDoorNumber.Value;
        }
        if (mvBookingEdit.ActiveViewIndex == 3) // For OD Staff
        {
            tableDiv_GeneralNew.Controls.RemoveAt(0);
            // tableDiv_General.Controls.RemoveAt(0);
            GenerateAlternateSlot();
        }
        else if (mvBookingEdit.ActiveViewIndex == 4) // For Carrier
        {
            tableDiv_GeneralNew.Controls.RemoveAt(0);
            tableDiv_GeneralVendor.Controls.RemoveAt(0);
            GenerateAlternateSlotForVendor();
        }
    }

    private MAS_SiteBE GetSingleSiteSetting(int siteId)
    {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSingleSiteSetting";
        oMAS_SiteBE.SiteID = siteId;
        MAS_SiteBE localMAS_SiteBE = oMAS_SiteBAL.GetSingleSiteSettingBAL(oMAS_SiteBE);
        return localMAS_SiteBE;
    }

    private void SendProvisionalRefusalLetterMail(List<APPBOK_BookingBE> lstBooking, int BookingID)
    {
        try
        {
            var oMASSIT_VendorBE = new MASSIT_VendorBE();
            var oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
            string VendorEmails = string.Empty;
            List<MASSIT_VendorBE> lstVendorDetails = null;
            string[] sentTo;
            string[] language;
            string[] VendorData = new string[2];

            if (lstBooking[0].SupplierType == "C")
            {
                oMASSIT_VendorBE.Action = "GetDeletedCarriersVendor";
                oMASSIT_VendorBE.BookingID = BookingID;
                lstVendorDetails = oAPPSIT_VendorBAL.GetDeletedCarriersVendorIDBAL(oMASSIT_VendorBE);
                if (lstVendorDetails != null && lstVendorDetails.Count > 0)
                {
                    for (int i = 0; i < lstVendorDetails.Count; i++)
                    {
                        VendorData = CommonPage.GetVendorEmailsWithLanguage(lstVendorDetails[i].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                        {
                            sentTo = VendorData[0].Split(',');
                            language = VendorData[1].Split(',');
                            for (int j = 0; j < sentTo.Length; j++)
                            {
                                if (sentTo[j].Trim() != "")
                                {
                                    SendMailToVendor(sentTo[j], language[j], lstBooking, BookingID);
                                }
                            }
                        }
                        else
                            SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, BookingID);

                    }
                }


                //Send mail to Carrier
                VendorData = CommonPage.GetCarrierDefaultEmailWithLanguage(lstBooking[0].Carrier.CarrierID);
                if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                {
                    sentTo = VendorData[0].Split(',');
                    language = VendorData[1].Split(',');
                    for (int j = 0; j < sentTo.Length; j++)
                    {
                        if (sentTo[j].Trim() != "")
                        {
                            SendMailToVendor(sentTo[j], language[j], lstBooking, BookingID);
                        }
                    }
                }
                else
                    SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, BookingID);

                //----------------------//

            }
            else if (lstBooking[0].SupplierType == "V")
            {
                VendorData = CommonPage.GetVendorEmailsWithLanguage(lstBooking[0].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                {
                    sentTo = VendorData[0].Split(',');
                    language = VendorData[1].Split(',');
                    for (int j = 0; j < sentTo.Length; j++)
                    {
                        if (sentTo[j].Trim() != "")
                        {
                            SendMailToVendor(sentTo[j], language[j], lstBooking, BookingID);
                        }
                    }
                }
                else
                    SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, BookingID);
            }

        }
        catch { }
    }

    private void SendMailToVendor(string toAddress, string language, List<APPBOK_BookingBE> lstBooking, int BookingID)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            #region Pallet, Lines, Cartons, DeliveryDateValue and DeliveryTimeValue ...
            string DeliveryDateValue = string.Empty;

            int iTotalPalllets, iTotalLines, iTotalCartons, iTotalLifts;
            iTotalPalllets = iTotalLines = iTotalCartons = iTotalLifts = 0;

            for (int iCount = 0; iCount < lstBooking.Count; iCount++)
            {
                iTotalPalllets += Convert.ToInt32(lstBooking[iCount].NumberOfPallet);
                iTotalLines += Convert.ToInt32(lstBooking[iCount].NumberOfLines);
                iTotalCartons += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
                iTotalLifts += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
            }

            DeliveryDateValue = lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy");
            string strWeekDay = lstBooking[0].ScheduleDate.Value.DayOfWeek.ToString().ToUpper();
            if (strWeekDay == "SUNDAY")
            {
                if (lstBooking[0].WeekDay != 1)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "MONDAY")
            {
                if (lstBooking[0].WeekDay != 2)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "TUESDAY")
            {
                if (lstBooking[0].WeekDay != 3)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "WEDNESDAY")
            {
                if (lstBooking[0].WeekDay != 4)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "THURSDAY")
            {
                if (lstBooking[0].WeekDay != 5)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "FRIDAY")
            {
                if (lstBooking[0].WeekDay != 6)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }
            else if (strWeekDay == "SATURDAY")
            {
                if (lstBooking[0].WeekDay != 7)
                    DeliveryDateValue = lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
            }

            #endregion

            string htmlBody = string.Empty;
            var oSendCommunicationCommon = new sendCommunicationCommon();

            //string vendorName = string.Join(",", lstBooking.Select(x => x.Vendor.VendorName).ToArray());

            string templatePath = null;
            templatePath = path + templatePathName;
            string LanguageFile = string.Empty;

            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

            foreach (MAS_LanguageBE objLanguage in oLanguages)
            {
                bool MailSentInLanguage = false;

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    MailSentInLanguage = true;
                }

                //LanguageFile = "ProvisionalRefusalLetter." + objLanguage.Language + ".htm";
                LanguageFile = "ProvisionalRefusalLetter.english.htm";
                var singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(lstBooking[0].SiteId));


                using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
                {
                    #region mailBody
                    htmlBody = sReader.ReadToEnd();
                    //htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                    htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{ProvisionalRefusalText1}", WebCommon.getGlobalResourceValue("ProvisionalRefusalText1", objLanguage.Language));

                    htmlBody = htmlBody.Replace("{GoodsInComment}", WebCommon.getGlobalResourceValue("GoodsInComment", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{GINCOMMENTValue}", txtRejectProCmments.Text.Trim());

                    htmlBody = htmlBody.Replace("{BookingRef}", lstBooking[0].BookingRef);

                    htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{DateValue}", DeliveryDateValue);

                    htmlBody = htmlBody.Replace("{Time}", WebCommon.getGlobalResourceValue("Time", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{TimeValue}", WebCommon.getGlobalResourceValue("ProvisionalBooking", objLanguage.Language).ToUpper());

                    htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{CarrierValue}", lstBooking[0].Carrier.CarrierName);

                    htmlBody = htmlBody.Replace("{VehicleTypeActual}", WebCommon.getGlobalResourceValue("VehicleTypeActual", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{VehicleTypeValue}", lstBooking[0].VehicleType.VehicleType);

                    htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValue("NumberofPallets", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{PalletsValue}", Convert.ToString(iTotalPalllets));

                    htmlBody = htmlBody.Replace("{NumberofLifts}", WebCommon.getGlobalResourceValue("NumberofLifts", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{LiftsValue}", Convert.ToString(iTotalLifts));

                    htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValue("NumberofCartons", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{CartonsValue}", Convert.ToString(iTotalCartons));

                    htmlBody = htmlBody.Replace("{NumberofPOLines}", WebCommon.getGlobalResourceValue("NumberofPOLines", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{LinesValue}", Convert.ToString(iTotalLines));

                    htmlBody = htmlBody.Replace("{BookedPONo}", WebCommon.getGlobalResourceValue("BookedPONo", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{BookedPOsValue}", lstBooking[0].PurchaseOrders.TrimStart(',').TrimEnd(','));

                    htmlBody = htmlBody.Replace("{dd/mm/yyyy}", lstBooking[0].BookingDate);
                    htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");

                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully", objLanguage.Language));
                    //---Stage 7 Point 29-----//
                    if (singleSiteSetting != null && singleSiteSetting.NonTimeStart != string.Empty)
                    {
                        htmlBody = htmlBody.Replace("{tblVisible}", "");
                        htmlBody = htmlBody.Replace("{SiteNameValue}", singleSiteSetting.SiteName);
                        htmlBody = htmlBody.Replace("{NonTimedStartTimeValue}", singleSiteSetting.NonTimeStart);
                        htmlBody = htmlBody.Replace("{NonTimedEndTimeValue}", singleSiteSetting.NonTimeEnd);
                    }
                    else
                    {
                        htmlBody = htmlBody.Replace("{tblVisible}", "none");
                    }
                    #endregion
                }

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                // Resending of Delivery mails
                oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                clsEmail oclsEmail = new clsEmail();

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    //ProvisionalRefusal
                    oclsEmail.sendMail(toAddress, htmlBody, WebCommon.getGlobalResourceValue("ProvisionalRefusal", objLanguage.Language), sFromAddress, true);
                }
                oAPPBOK_CommunicationBAL.AddDeletedItemBAL(BookingID, sFromAddress, toAddress, WebCommon.getGlobalResourceValue("ProvisionalRefusal", objLanguage.Language), htmlBody,
                    Convert.ToInt32(Session["UserID"]), CommunicationType.Enum.ProvisionalRefusal, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);
            }
        }
    }

    protected void btnRejectChanges_Click(object sender, EventArgs e)
    {
        mdlRejectProvisionalChanges.Show();
    }
    protected void btnContinueChanges_2_Click(object sender, EventArgs e)
    {
        RejectChangesBooking();
    }

    protected void btnBackChanges_2_Click(object sender, EventArgs e)
    {

        mdlRejectProvisionalChanges.Hide();
    }

    private void RejectChangesBooking()
    {
        try
        {

            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
            oAPPBOK_BookingBE.Action = "RejectChangesBooking";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
            oAPPBOK_BookingBAL.RejectChangesBookingDetailBAL(oAPPBOK_BookingBE);

            //--------------------start send reject changes letter phase 14 r2 point 20----------------------//
            sendRejectChangesLetter();
            //--------------------end send reject changes letter phase 14 r2 point 20----------------------//

            if (GetQueryStringValue("PNTB") != null && GetQueryStringValue("PNTB") == "True")
            {
                EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
            }
            else if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro")
            {
                EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
            }
            else
            {
                EncryptQueryString("APPBok_BookingOverview.aspx");
            }

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    //---Start Stage 14 Point 20-----//
    private void sendRejectChangesLetter()
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        List<APPBOK_BookingBE> lstBooking = new List<APPBOK_BookingBE>();



        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

        int BookingID = Convert.ToInt32(GetQueryStringValue("ID").ToString());

        oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
        oAPPBOK_BookingBE.BookingID = BookingID;
        lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);

        if (lstBooking.Count > 0)
        {

            MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(lstBooking[0].SiteId));
            try
            {
                MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
                APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();

                string VendorEmails = string.Empty;
                List<MASSIT_VendorBE> lstVendorDetails = null;
                string[] sentTo;
                string[] language;
                string[] VendorData = new string[2];
                bool isMailSend = false;



                if (lstBooking[0].SupplierType == "C")
                {
                    oMASSIT_VendorBE.Action = "GetCarriersVendor";
                    oMASSIT_VendorBE.BookingID = BookingID;

                    lstVendorDetails = oAPPSIT_VendorBAL.GetCarriersVendorIDBAL(oMASSIT_VendorBE);
                    if (lstVendorDetails != null && lstVendorDetails.Count > 0)
                    {
                        for (int i = 0; i < lstVendorDetails.Count; i++)
                        {
                            isMailSend = false;
                            VendorData = CommonPage.GetVendorEmailsWithLanguage(lstVendorDetails[i].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                            if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                            {
                                sentTo = VendorData[0].Split(',');
                                language = VendorData[1].Split(',');
                                for (int j = 0; j < sentTo.Length; j++)
                                {
                                    if (sentTo[j].Trim() != "")
                                    {
                                        SendMailToVendor(sentTo[j], language[j], lstBooking, singleSiteSetting, oLanguages);
                                        //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                                        isMailSend = true;
                                    }
                                }
                            }

                            if (!isMailSend)
                            {
                                SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
                                //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                            }

                        }
                    }


                    //Send mail to Carrier
                    isMailSend = false;
                    //VendorData = CommonPage.GetCarrierDefaultEmailWithLanguage(lstBooking[0].Carrier.CarrierID);
                    VendorData = CommonPage.GetCarrierEmailsWithLanguage(lstBooking[0].Carrier.CarrierID, Convert.ToInt32(lstBooking[0].SiteId));
                    if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                    {
                        sentTo = VendorData[0].Split(',');
                        language = VendorData[1].Split(',');
                        for (int j = 0; j < sentTo.Length; j++)
                        {
                            if (sentTo[j].Trim() != "")
                            {
                                SendMailToVendor(sentTo[j], language[j], lstBooking, singleSiteSetting, oLanguages);
                                //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                                isMailSend = true;
                            }
                        }
                    }
                    if (!isMailSend)
                    {
                        SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
                        //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                    }

                    //----------------------//

                }
                else if (lstBooking[0].SupplierType == "V")
                {
                    isMailSend = false;
                    VendorData = CommonPage.GetVendorEmailsWithLanguage(lstBooking[0].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                    if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                    {
                        sentTo = VendorData[0].Split(',');
                        language = VendorData[1].Split(',');
                        for (int j = 0; j < sentTo.Length; j++)
                        {
                            if (sentTo[j].Trim() != "")
                            {
                                SendMailToVendor(sentTo[j], language[j], lstBooking, singleSiteSetting, oLanguages);
                                //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                                isMailSend = true;
                            }
                        }
                    }
                    if (!isMailSend)
                    {
                        SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
                        //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                    }
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", lstBooking, singleSiteSetting, oLanguages);
            }
        }
    }

    sendCommunication communication = new sendCommunication();
    private void SendMailToVendor(string toAddress, string language, List<APPBOK_BookingBE> lstBooking, MAS_SiteBE singleSiteSetting, List<MAS_LanguageBE> oLanguages)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            string htmlBody = string.Empty;
            string templatePath = null;
            templatePath = path + templatePathName;
            string LanguageFile = string.Empty;
            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            clsEmail oclsEmail = new clsEmail();
            int iTotalPalllets = 0;
            int iTotalLines = 0;
            int iTotalCartons = 0;
            int iTotalLifts = 0;
            if (lstBooking.Count > 0)
            {

                for (int iCount = 0; iCount < lstBooking.Count; iCount++)
                {
                    iTotalPalllets += Convert.ToInt32(lstBooking[iCount].NumberOfPallet);
                    iTotalLines += Convert.ToInt32(lstBooking[iCount].NumberOfLines);
                    iTotalCartons += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
                    iTotalLifts += Convert.ToInt32(lstBooking[iCount].NumberOfLift);
                }
            }

            foreach (MAS_LanguageBE objLanguage in oLanguages)
            {
                bool MailSentInLanguage = false;

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    MailSentInLanguage = true;
                }

                LanguageFile = "RejectChanges.english.htm";

                using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
                {
                    #region mailBody
                    htmlBody = sReader.ReadToEnd();

                    htmlBody = htmlBody.Replace("{BookingReference}", WebCommon.getGlobalResourceValueByLangID("BookingReference", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValueByLangID("DearSirMadam", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{RejectChangesText1}", WebCommon.getGlobalResourceValueByLangID("RejectChangesText1", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{RejectChangesText2}", WebCommon.getGlobalResourceValueByLangID("RejectChangesText2", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{ProvisionalText7}", WebCommon.getGlobalResourceValueByLangID("ProvisionalText7", objLanguage.LanguageID));

                    /* Changed For All letters files */
                    htmlBody = htmlBody.Replace("{BookingRefValue}", lstBooking[0].BookingRef);

                    htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValueByLangID("Date", objLanguage.LanguageID));


                    string strWeekDay = lstBooking[0].ScheduleDate.Value.DayOfWeek.ToString().ToUpper();
                    if (strWeekDay == "SUNDAY")
                    {
                        if (lstBooking[0].WeekDay != 1)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "MONDAY")
                    {
                        if (lstBooking[0].WeekDay != 2)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "TUESDAY")
                    {
                        if (lstBooking[0].WeekDay != 3)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "WEDNESDAY")
                    {
                        if (lstBooking[0].WeekDay != 4)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "THURSDAY")
                    {
                        if (lstBooking[0].WeekDay != 5)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "FRIDAY")
                    {
                        if (lstBooking[0].WeekDay != 6)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    else if (strWeekDay == "SATURDAY")
                    {
                        if (lstBooking[0].WeekDay != 7)
                            htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy"));
                    }
                    htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy"));

                    htmlBody = htmlBody.Replace("{Time}", WebCommon.getGlobalResourceValueByLangID("Time", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{TimeActual}", WebCommon.getGlobalResourceValueByLangID("TimeActual", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{TimeValue}", lstBooking[0].SlotTime.SlotTime);

                    htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValueByLangID("Carrier", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{CarrierValue}", lstBooking[0].Carrier.CarrierName);

                    htmlBody = htmlBody.Replace("{VehicleTypeActual}", WebCommon.getGlobalResourceValueByLangID("VehicleTypeActual", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{VehicleType}", WebCommon.getGlobalResourceValueByLangID("VehicleType", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{VehicleTypeValue}", lstBooking[0].VehicleType.VehicleType);

                    htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValueByLangID("NumberofPallets", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{PalletsValue}", Convert.ToString(iTotalPalllets));

                    htmlBody = htmlBody.Replace("{NumberofLifts}", WebCommon.getGlobalResourceValueByLangID("NumberofLifts", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{LiftsValue}", Convert.ToString(iTotalLifts));

                    htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValueByLangID("NumberofCartons", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{CartonsValue}", Convert.ToString(iTotalCartons));

                    htmlBody = htmlBody.Replace("{NumberofPOLines}", WebCommon.getGlobalResourceValueByLangID("NumberofPOLines", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{LinesValue}", Convert.ToString(iTotalLines));

                    htmlBody = htmlBody.Replace("{BookedPONo}", WebCommon.getGlobalResourceValueByLangID("BookedPONo", objLanguage.LanguageID));
                    htmlBody = htmlBody.Replace("{BookedPOsValue}", lstBooking[0].PurchaseOrders.TrimStart(',').TrimEnd(','));

                    htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");

                    if (singleSiteSetting != null && singleSiteSetting.NonTimeStart != string.Empty)
                    {
                        htmlBody = htmlBody.Replace("{tblVisible}", "");
                        htmlBody = htmlBody.Replace("{SiteNameValue}", singleSiteSetting.SiteName);
                        htmlBody = htmlBody.Replace("{NonTimedStartTimeValue}", singleSiteSetting.NonTimeStart);
                        htmlBody = htmlBody.Replace("{NonTimedEndTimeValue}", singleSiteSetting.NonTimeEnd);
                    }
                    else
                    {
                        htmlBody = htmlBody.Replace("{tblVisible}", "none");
                    }

                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValueByLangID("Yoursfaithfully", objLanguage.LanguageID));

                    //------------------------//
                    #endregion
                }

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                oAPPBOK_CommunicationBAL.AddItemBAL(lstBooking[0].BookingID, sFromAddress, toAddress, "Reject Changes of your Booking", htmlBody, Convert.ToInt32(Session["UserID"]), CommunicationType.Enum.ProvisionalReject, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);
                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    oclsEmail.sendMail(toAddress, htmlBody, "Reject Changes of your Booking", sFromAddress, true);
                }
            }
        }
    }
    //---End Stage 14 Point 20-----//

    protected void gvVolume_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (hdnLine.Value != "0" && hdnProvisionalReason.Value == "EDITED" && hdnBookingTypeID.Value == "5"
            && Session["Role"].ToString().ToLower() != "carrier")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Literal lblPalletsProvisional = (Literal)e.Row.FindControl("lblPalletsProvisional");
                Literal lblCartonsProvisional = (Literal)e.Row.FindControl("lblCartonsProvisional");
                Literal lblExpectedLineProvisional = (Literal)e.Row.FindControl("lblExpectedLineProvisional");
                TextBox txtVendorPallets = (TextBox)e.Row.FindControl("txtVendorPallets");
                TextBox txtLinesToBeDelivered = (TextBox)e.Row.FindControl("txtLinesToBeDelivered");
                TextBox txtVendorCartons = (TextBox)e.Row.FindControl("txtVendorCartons");
                if (lblPalletsProvisional.Text != "0" && lblCartonsProvisional.Text != "0" && lblExpectedLineProvisional.Text != "0")
                {
                    lblPalletsProvisional.Visible = true;
                    lblCartonsProvisional.Visible = true;
                    lblExpectedLineProvisional.Visible = true;
                    txtVendorPallets.ForeColor = Color.Red;
                    txtLinesToBeDelivered.ForeColor = Color.Red;
                    txtVendorCartons.ForeColor = Color.Red;
                }
                else
                {
                    lblPalletsProvisional.Visible = true;
                    lblCartonsProvisional.Visible = true;
                    lblExpectedLineProvisional.Visible = true;
                    lblPalletsProvisional.Text = "&nbsp;";
                    lblCartonsProvisional.Text = "&nbsp;";
                    lblExpectedLineProvisional.Text = "&nbsp;";
                    txtVendorPallets.ForeColor = Color.Gray;
                    txtLinesToBeDelivered.ForeColor = Color.Gray;
                    txtVendorCartons.ForeColor = Color.Gray;
                }
            }

        }
        else
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Literal lblPalletsProvisional = (Literal)e.Row.FindControl("lblPalletsProvisional");
                Literal lblCartonsProvisional = (Literal)e.Row.FindControl("lblCartonsProvisional");
                Literal lblExpectedLineProvisional = (Literal)e.Row.FindControl("lblExpectedLineProvisional");
                TextBox txtVendorPallets = (TextBox)e.Row.FindControl("txtVendorPallets");
                TextBox txtLinesToBeDelivered = (TextBox)e.Row.FindControl("txtLinesToBeDelivered");
                TextBox txtVendorCartons = (TextBox)e.Row.FindControl("txtVendorCartons");
                lblPalletsProvisional.Visible = false;
                lblCartonsProvisional.Visible = false;
                lblExpectedLineProvisional.Visible = false;
                txtVendorPallets.ForeColor = Color.Gray;
                txtLinesToBeDelivered.ForeColor = Color.Gray;
                txtVendorCartons.ForeColor = Color.Gray;
            }
        }
    }



    public void GenerateVolumeDetails()
    {
        string strWeekDay = string.Empty;
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        strWeekDay = Convert.ToDateTime(Common.GetYYYY_MM_DD(GetQueryStringValue("Scheduledate"))).DayOfWeek.ToString();
        oAPPBOK_BookingBE.ScheduleDate = Convert.ToDateTime(Common.GetYYYY_MM_DD(GetQueryStringValue("Scheduledate")));
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(GetQueryStringValue("SiteID"));
        if (strWeekDay.ToUpper() == "SUNDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 1;
        }
        else if (strWeekDay.ToUpper() == "MONDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 2;
        }
        else if (strWeekDay.ToUpper() == "TUESDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 3;
        }
        else if (strWeekDay.ToUpper() == "WEDNESDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 4;
        }
        else if (strWeekDay.ToUpper() == "THURSDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 5;
        }
        else if (strWeekDay.ToUpper() == "FRIDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 6;
        }
        else if (strWeekDay.ToUpper() == "SATURDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 7;
        }

        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        DataSet ds = oAPPBOK_BookingBAL.etVolumeDetailsAlternateSlotBAL(oAPPBOK_BookingBE);
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblMaxDeliveriesValue.Text = ds.Tables[0].Rows[0]["MAX_Deliveries"].ToString();
                lblConfirmedDeliveriesValue.Text = ds.Tables[0].Rows[0]["Confirmed_Deliveries"].ToString();
                lblUnConfirmedDeliveriesValue.Text = ds.Tables[0].Rows[0]["UNConfirmed_Deliveries"].ToString();
                lblNonTimedDeliveriesValue.Text = ds.Tables[0].Rows[0]["NonTimed_Deliveries"].ToString();
                lblTotalDeliveriesValue.Text = ds.Tables[0].Rows[0]["Total_Deliveries"].ToString();
                lblSpaceDeliveriesValue.Text = ds.Tables[0].Rows[0]["Space_Deliveries"].ToString();
                lblArrivedDeliveriesValue.Text = ds.Tables[0].Rows[0]["Arrived_Deliveries"].ToString();
                lblUnloadedDeliveriesValue.Text = ds.Tables[0].Rows[0]["Unload_Deliveries"].ToString();
                lblRefusedNoShowDeliveriesValue.Text = ds.Tables[0].Rows[0]["NoShowAndRefused_Deliveries"].ToString();
                lblMaxPalletsValue.Text = ds.Tables[0].Rows[0]["MAX_Pallets"].ToString();
                lblConfirmedPalletsValue.Text = ds.Tables[0].Rows[0]["Confirmed_Pallets"].ToString();
                lblUnConfirmedPalletsValue.Text = ds.Tables[0].Rows[0]["UNConfirmed_Pallets"].ToString();
                lblNonTimedPalletsValue.Text = ds.Tables[0].Rows[0]["NonTimed_Pallets"].ToString();
                lblTotalPalletsValue.Text = ds.Tables[0].Rows[0]["Total_Pallets"].ToString();
                lblSpacePalletsValue.Text = ds.Tables[0].Rows[0]["Space_Pallets"].ToString();
                lblArrivedPalletsValue.Text = ds.Tables[0].Rows[0]["Arrived_Pallets"].ToString();
                lblUnloadedPalletsValue.Text = ds.Tables[0].Rows[0]["Unload_Pallets"].ToString();
                lblRefusedNoShowPalletsValue.Text = ds.Tables[0].Rows[0]["NoShowAndRefused_Pallets"].ToString();
                lblMaxLinesValue.Text = ds.Tables[0].Rows[0]["MAX_Lines"].ToString();
                lblConfirmedLinesValue.Text = ds.Tables[0].Rows[0]["Confirmed_Lines"].ToString();
                lblUnConfirmedLinesValue.Text = ds.Tables[0].Rows[0]["UNConfirmed_Lines"].ToString();
                lblNonTimedLinesValue.Text = ds.Tables[0].Rows[0]["NonTimed_Lines"].ToString();
                lblTotalLinesValue.Text = ds.Tables[0].Rows[0]["Total_Lines"].ToString();
                lblSpaceLinesValue.Text = ds.Tables[0].Rows[0]["Space_Lines"].ToString();
                lblArrivedLinesValue.Text = ds.Tables[0].Rows[0]["Arrived_Lines"].ToString();
                lblUnloadedLinesValue.Text = ds.Tables[0].Rows[0]["Unload_Lines"].ToString();
                lblRefusedNoShowLinesValue.Text = ds.Tables[0].Rows[0]["NoShowAndRefused_Lines"].ToString();
                lblMaxCartonsValue.Text = ds.Tables[0].Rows[0]["MAX_Cartoons"].ToString();
                lblConfirmedCartonsValue.Text = ds.Tables[0].Rows[0]["Confirmed_Cartoons"].ToString();
                lblUnConfirmedCartonsValue.Text = ds.Tables[0].Rows[0]["UNConfirmed_Cartoons"].ToString();
                lblNonTimedCartonsValue.Text = ds.Tables[0].Rows[0]["NonTimed_Cartoons"].ToString();
                lblTotalCartonsValue.Text = ds.Tables[0].Rows[0]["Total_Cartoons"].ToString();
                lblSpaceCartonsValue.Text = ds.Tables[0].Rows[0]["Space_Cartoons"].ToString();
                lblArrivedCartonsValue.Text = ds.Tables[0].Rows[0]["Arrived_Cartoons"].ToString();
                lblUnloadedCartonsValue.Text = ds.Tables[0].Rows[0]["Unload_Cartoons"].ToString();
                lblRefusedNoShowCartonsValue.Text = ds.Tables[0].Rows[0]["NoShowAndRefused_Cartoons"].ToString();
                if (ds.Tables[0].Rows[0]["Color_Deliveries"].ToString() == "Red")
                {
                    lblTotalDeliveriesValue.ForeColor = System.Drawing.Color.Red;
                }
                if (ds.Tables[0].Rows[0]["Color_Pallets"].ToString() == "Red")
                {
                    lblTotalPalletsValue.ForeColor = System.Drawing.Color.Red;
                }
                if (ds.Tables[0].Rows[0]["Color_Lines"].ToString() == "Red")
                {
                    lblTotalLinesValue.ForeColor = System.Drawing.Color.Red;
                }
                if (ds.Tables[0].Rows[0]["Color_Cartoons"].ToString() == "Red")
                {
                    lblTotalCartonsValue.ForeColor = System.Drawing.Color.Red;
                }

            }
        }
    }
    private void CreateDynamicDivNew(TableCell tcLocal, string SlotStartTime, string SlotEndTime, string Pallets, string Cartoons, string Lines, string Lifts, string Vendor, int UniqueID, string BookingID, string BookingStatus, DataTable dtAlternateSlotFinalTable, string color)
    {

        try
        {
            // Create a new HtmlGenericControl.
            HtmlGenericControl NewControl = new HtmlGenericControl("div");
            string ControlID = "VolumeNew" + Convert.ToString(UniqueID);
            NewControl.ID = "," + ControlID + ",";

            divVolumeNew.Controls.Remove(divVolumeNew.FindControl(NewControl.ID));
   

            // GreenYellow case is for recomended slot
            if (BookingStatus != "Unconfirmed Fixed" && BookingStatus != "" && color.Trim() != "GreenYellow")
            {
               var lstAlternateSlotFinalTable = dtAlternateSlotFinalTable.AsEnumerable().Where(x => x.Field<string>("BookingID") == BookingID).ToList();


                foreach (var door in lstAlternateSlotFinalTable)
                {
                    var filterTableRow = (from c in lstAlternateSlotFinalTable.AsEnumerable()
                                          select new AlternateSlot
                                          {
                                              Vendor = c.Field<string>("Vendor"),
                                              Pallets = c.Field<string>("Pallets"),
                                              Cartoons = c.Field<string>("Cartoons"),
                                              Lines = c.Field<string>("Lines"),
                                              Lifts = c.Field<string>("Lifts"),
                                              SupplierType = c.Field<string>("SupplierType"),
                                              BookingID = c.Field<string>("BookingID"),
                                              BookingStatus = c.Field<string>("BookingStatus"),
                                              Issue = c.Field<string>("Issue"),
                                              Carrier = c.Field<string>("Carrier")
                                          }).ToList();


                    string InnerHTML = "<table width='100%'>" +
                                  "<tr><td colspan = '5' align = 'center' ><strong>{VendorName}</strong></td></tr> " +
                                  "<tr> " +
                                  "<td colspan = '5'> " +
                                  " <table style = 'width:100%' >" +
                                  " <tr> " +
                                  "<td><strong> Status : </strong> {Status}<br/> <strong> Issue : </strong> {Issue} </td>" +
                                  "<td valign='top'> <strong> Arrival : </strong> {Atime}</td> " +
                                  "<td  valign='top'><strong> Est Depart: </strong>{DTime}</td> " +
                                  "</tr> " +
                                  "</table >" +
                                  "</td>" +
                                  " </tr> " +
                                  " <tr> " +
                                  "<td></td>" +
                                  "<td align='center'><strong> Lifts </strong></td> " +
                                  " <td align='center'><strong> Pallets </strong></td> " +
                                  "<td align='center'><strong> Cartons </strong></td> " +
                                  "<td align='center'><strong> Lines </strong></td> " +
                                  "</tr>" +
                                  "<tr> " +
                                  " <td> <strong> Total </strong></td> " +
                                  " <td align='center'>{LiftsTotal}</td> " +
                                  " <td align='center'>{PalletsTotal}</td> " +
                                  " <td align='center'>{CartonsTotal}</td> " +
                                  "<td align='center'>{LinesTotal}</td> " +
                                  "</tr> ";

                    InnerHTML = InnerHTML.Replace("{Atime}", SlotStartTime);
                    InnerHTML = InnerHTML.Replace("{DTime}", SlotEndTime);
                    InnerHTML = InnerHTML.Replace("{LiftsTotal}", Lifts.ToString());
                    InnerHTML = InnerHTML.Replace("{PalletsTotal}", Pallets.ToString());
                    InnerHTML = InnerHTML.Replace("{CartonsTotal}", Cartoons.ToString());
                    InnerHTML = InnerHTML.Replace("{LinesTotal}", Lines.ToString());


                    if (filterTableRow[0].SupplierType == "V")
                    {
                        InnerHTML = InnerHTML +
                                     "</table> ";

                        InnerHTML = InnerHTML.Replace("{Status}", filterTableRow[0].BookingStatus);
                        InnerHTML = InnerHTML.Replace("{Issue}", filterTableRow[0].Issue);
                        InnerHTML = InnerHTML.Replace("{VendorName}", filterTableRow[0].Vendor);

                    }
                    else
                    {
                        for (int i = 0; i < lstAlternateSlotFinalTable.Count; i++)
                        {
                            InnerHTML = InnerHTML + "<tr> " +
                                          "<td> <strong> {Vendor} </strong></td> " +
                                          "<td align='center'>{Lifts}</td>" +
                                          "<td align='center'>{Pallets}</td>" +
                                          "<td align='center'>{Cartons}</td> " +
                                          "<td align='center'>{Lines}</td> " +
                                          "</tr> ";

                            InnerHTML = InnerHTML.Replace("{Atime}", SlotStartTime);
                            InnerHTML = InnerHTML.Replace("{DTime}", SlotEndTime);
                            InnerHTML = InnerHTML.Replace("{Lifts}", filterTableRow[i].Lifts != "" ? filterTableRow[i].Lifts : "-");
                            InnerHTML = InnerHTML.Replace("{Pallets}", filterTableRow[i].Pallets != "" ? filterTableRow[i].Pallets : "-");
                            InnerHTML = InnerHTML.Replace("{Cartons}", filterTableRow[i].Cartoons != "" ? filterTableRow[i].Cartoons : "-");
                            InnerHTML = InnerHTML.Replace("{Lines}", filterTableRow[i].Lines != "" ? filterTableRow[i].Lines : "-");
                            InnerHTML = InnerHTML.Replace("{Status}", filterTableRow[i].BookingStatus);
                            InnerHTML = InnerHTML.Replace("{Issue}", filterTableRow[i].Issue);
                            InnerHTML = InnerHTML.Replace("{Vendor}", filterTableRow[i].Vendor);

                            if (filterTableRow[i].SupplierType == "C")
                            {
                                InnerHTML = InnerHTML.Replace("{VendorName}", filterTableRow[i].Carrier);
                            }
                            else
                            {
                                InnerHTML = InnerHTML.Replace("{VendorName}", filterTableRow[i].Vendor);
                            }

                            if (i == lstAlternateSlotFinalTable.Count - 1)
                            {
                                InnerHTML = InnerHTML +
                                          "</table> ";
                            }
                        }
                    }


                    NewControl.InnerHtml = InnerHTML;
                }               
            }
            else
            {

                string InnerHTML = "<table width='100%'> " +
                                  "<tr><td colspan='5' align='center'><strong>{VendorName}</strong></td></tr> " +
                                  "<tr> " +
                                    "<td><strong>Arrival {Atime} </strong></td> " +
                                    "<td>Lifts</td> " +
                                    "<td>Pallets</td> " +
                                    "<td>Cartons</td> " +
                                    "<td>Lines</td> " +
                                  "</tr> " +
                                  "<tr> ";

                InnerHTML = InnerHTML + "<td><strong>Est Departure {DTime} </strong></td> ";
                InnerHTML = InnerHTML.Replace("{DTime}", SlotEndTime);

                InnerHTML = InnerHTML + "<td>{Lifts}</td> " +
                                        "<td>{Pallets}</td> " +
                                        "<td>{Cartons}" +
                                        "<td>{Lines}</td> " +
                                      "</tr> " +
                                    "</table>";

                InnerHTML = InnerHTML.Replace("{VendorName}", Vendor);
                InnerHTML = InnerHTML.Replace("{Atime}", SlotStartTime);



                InnerHTML = InnerHTML.Replace("{Lifts}", Lifts.ToString());

                InnerHTML = InnerHTML.Replace("{Pallets}", Pallets.ToString());

                InnerHTML = InnerHTML.Replace("{Cartons}", Cartoons.ToString());

                InnerHTML = InnerHTML.Replace("{Lines}", Lines.ToString());

                NewControl.InnerHtml = InnerHTML;
            }

            NewControl.Attributes.Add("display", "none");
            // Add the new HtmlGenericControl to the Controls collection of the
            // TableCell control. 
            divVolumeNew.Controls.Add(NewControl);

            tcLocal.Attributes.Add("onmouseover", "this.style.cursor = 'hand'; ddrivetip('ctl00_ContentPlaceHolder1_," + ControlID + ",','#ededed','400');");
            tcLocal.Attributes.Add("onmouseout", "hideddrivetip();");

        }
        catch { }
    }

    private void MakeCellClickableForOtherVendorNew(TableCell tclocal, string siTimeSlotID, string siSiteDoorID, string TimeSlot, string DoorNo, string SlotStartDay, string siTimeSlotOrderBYID)
    {
        string fixedSlotid = "-1";
        tclocal.Attributes.Add("onclick", "CheckAletrnateSlot('" + siTimeSlotID.ToString() + "','" + siSiteDoorID.ToString() + "','" + DoorNo + "','" + TimeSlot + "','" + fixedSlotid + "','" + "tue" + "','" + "89" + "');");
    }

    private void MakeCellClickableNew(TableCell tclocal, string siTimeSlotID, string siSiteDoorID, string TimeSlot, string DoorNo, string SlotStartDay, string siTimeSlotOrderBYID)
    {


        string fixedSlotid = "-1";
        CarrierDetails vd = new CarrierDetails();

        string[] StartTime = TimeSlot.Split(':');
        TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);

        if (SlotDoorCounter.ContainsKey(time1 + siSiteDoorID.ToString()))
        {
            vd = SlotDoorCounter[time1 + siSiteDoorID.ToString()];
            if (vd.CurrentFixedSlotID != null)
            {
                fixedSlotid = vd.CurrentFixedSlotID.ToString();
            }
        }
       // tclocal.Attributes.Add("name", fixedSlotid);
        tclocal.Attributes.Add("onclick", "CheckAletrnateSlot('" + siTimeSlotID.ToString() + "','" + siSiteDoorID.ToString() + "','" + DoorNo + "','" + TimeSlot + "','" + fixedSlotid + "','" + SlotStartDay + "','" + siTimeSlotOrderBYID + "');");
    }

    public void ShowHazordousItem()
    {
        MAS_VendorBAL mAS_VendorBAL = new MAS_VendorBAL();
        MAS_VendorBE mAS_VendorBE = new MAS_VendorBE();
        mAS_VendorBE.Action = "GetSiteSchedulingSettingsForHazardous";
        mAS_VendorBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue);
        DataSet ds = mAS_VendorBAL.GetSiteSchedulingSettingsForHazardousBAL(mAS_VendorBE);

        if (ds != null && ds.Tables.Count > 0)
        {
            var result = Convert.ToBoolean(ds.Tables[0].Rows[0][0].ToString());
            if (result == true)
            {
                divEnableHazardouesItemPrompt.Visible = true;
                divHazrdous.Visible = true;
            }
            else
            {
                divEnableHazardouesItemPrompt.Visible = false;
                divHazrdous.Visible = false;
            }
        }
    }

    protected void chkSiteSetting_CheckedChanged1(object sender, EventArgs e)
    {
        CountryData.Visible = chkSiteSetting.Checked;

        pnlPalletCheckforCountryDisplay.Visible = chkSiteSetting.Checked;

        lblYesNo.Text = chkSiteSetting.Checked ? "Yes" : "No"; 
    }
}

public class CarrierDetails
{
    public string CarrierName { get; set; }
    public Color CellColor { get; set; }
    public bool isCurrentSlotFixed { get; set; }
    public string CurrentFixedSlotID { get; set; }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities; using WebUtilities;
using System.Collections;
using Microsoft.Reporting.WebForms;


public partial class ModuleUI_Appointment_Booking_ReportViewer : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        SchedulingPrintOutreport();
      
    }

    public void SchedulingPrintOutreport()
    {
        //To set the path where Pdf will be generates
        // string root = Server.MapPath("~") + "\\Repository\\";
        //string Report = "Document";
        ReportViewer1.Visible = true;
        ReportDataSource rdsSchedulingPrintOut = new ReportDataSource();
        rdsSchedulingPrintOut = new ReportDataSource(Convert.ToString(Session["DataTableName"]), (DataTable)Session["DataSet"]);
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~") + Session["ReportPath"];
        this.ReportViewer1.LocalReport.Refresh();
        ReportViewer1.Width = Unit.Pixel(980);
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(rdsSchedulingPrintOut);


    }
}
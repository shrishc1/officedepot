﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="APPBok_ConfirmPurchaseOrderArrival.aspx.cs"
    Inherits="APPBok_ConfirmPurchaseOrderArrival" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%= btnSend.ClientID %>').click(function () {
                disablecheckbox();
            });
        });

        function disablecheckbox() {
            $('#<%=gvConfirmPOArrival.ClientID%>').find('input:checkbox[id$="chkNotDeliveredValue"]').each(function () {
                var iRowIndex = $(this).closest("tr").prevAll("tr").length;
                iRowIndex = parseInt(iRowIndex) + 1;
                if (parseInt(iRowIndex) < 10) {
                    iRowIndex = "0" + iRowIndex;
                }
                var hdnIsNotDelieveredEmailSent = $("#ctl00_ContentPlaceHolder1_gvConfirmPOArrival_ctl" + iRowIndex + "_hdnIsNotDelieveredEmailSent").val();
                if ($(this).is(':checked')) {
                    $(this).attr('disabled', true);
                }
            });
            var hdnEmailFlag = $('#<%=hdnEmailFlag.ClientID%>').val();
            $('#<%=gvConfirmPOArrival.ClientID %> td:nth-child(3)').each(function () {
                var iRowIndex = $(this).closest("tr").prevAll("tr").length;
                iRowIndex = parseInt(iRowIndex) + 1;
                if (parseInt(iRowIndex) < 10) {
                    iRowIndex = "0" + iRowIndex;
                }
                var chkNotDeliveredValue = $("#ctl00_ContentPlaceHolder1_gvConfirmPOArrival_ctl" + iRowIndex + "_chkNotDeliveredValue");
                var chkDeliveredValue = $("#ctl00_ContentPlaceHolder1_gvConfirmPOArrival_ctl" + iRowIndex + "_chkDeliveredValue");
                if ($(chkNotDeliveredValue).is(':checked') && hdnEmailFlag == "true") {
                    $(this).parent("tr").css("background-color", "green");
                    $(chkDeliveredValue).attr('disabled', true);
                }
            });
        }

        function PrintScreen() {
            document.getElementById('rwButtons').style.visibility = "hidden";
            window.print();
            document.getElementById('rwButtons').style.visibility = "visible";
            //setTimeout('self.close()', 1000);           
        }
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
        function EndRequest(sender, args) {
            var iRowIndex = 0;
            if (args.get_error() == undefined) {
                $('input:submit[id$="btncancel"]').addClass("button");
                $('#<%= btnSend.ClientID %>').click(function () {
                    $('#<%=gvConfirmPOArrival.ClientID%>').find('input:checkbox[id$="chkNotDeliveredValue"]').each(function () {
                        if ($(this).is(':checked')) {
                            $(this).attr('disabled', true);
                        }
                    });
                });
                var hdnEmailFlag = $('#<%=hdnEmailFlag.ClientID%>').val();
              //  debugger;
                $('#<%=gvConfirmPOArrival.ClientID %> td:nth-child(3)').each(function () {
                    var iRowIndex = $(this).closest("tr").prevAll("tr").length;
                    iRowIndex = parseInt(iRowIndex) + 1;
                    if (parseInt(iRowIndex) < 10) {
                        iRowIndex = "0" + iRowIndex;
                    }
                    var chkNotDeliveredValue = $("#ctl00_ContentPlaceHolder1_gvConfirmPOArrival_ctl" + iRowIndex + "_chkNotDeliveredValue");
                    var chkDeliveredValue = $("#ctl00_ContentPlaceHolder1_gvConfirmPOArrival_ctl" + iRowIndex + "_chkDeliveredValue");
                    if ($(chkNotDeliveredValue).is(':checked') && hdnEmailFlag == "true") {
                        $(this).parent("tr").css("background-color", "green");
                        $(chkDeliveredValue).attr('disabled', true);
                    }
                });
            }
        } 
       
           
    </script>
   
    <h2>
        <cc1:ucLabel ID="lblConfirmPurchaseOrderArrival" runat="server" Text="Confirm Purchase Order Arrival"></cc1:ucLabel>
    </h2>
    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
         
            <div class="right-shadow">
                <div class="formbox">
                    <div id="divMain" runat="server">
                        <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <%-- <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendorValue" runat="server"></cc1:ucLabel>
                                </td>--%>
                                <td style="font-weight: bold; width: 9%">
                                    <cc1:ucLabel ID="lblStatus" runat="server" Text="Status"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    :
                                </td>
                                <td style="width: 23%">
                                    <cc1:ucLabel ID="lblStatusValue" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 9%">
                                    <cc1:ucLabel ID="lblDate" Text="Date" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    :
                                </td>
                                <td style="width: 24%">
                                    <cc1:ucLabel ID="lblDateValue" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 9%">
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                </td>
                                <td style="font-weight: bold; width: 23%">
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblActualPallets" runat="server" Text="Pallets"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucLabel ID="lblPalletsValue" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblActualCartons" runat="server" Text="Cartons"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucLabel ID="lblCartonsValue" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblActualLines" runat="server" Text="Lines"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucLabel ID="lblLinesValue" runat="server"></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVehicleType" runat="server" Text="Vehicle Type"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucLabel ID="lblVehicleTypeValue" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblCarrier" Text="Carrier" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td colspan="4">
                                    <cc1:ucLabel ID="lblCarrierValue" runat="server"></cc1:ucLabel>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td align="center">
                                    <cc1:ucGridView ID="gvConfirmPOArrival" runat="server" AutoGenerateColumns="false"
                                        CssClass="grid gvclass" CellPadding="0" Width="700px" OnRowDataBound="gvConfirmPOArrival_RowDataBound">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField HeaderText="PO">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnPurchaseOrderID" runat="server" Value='<%#Eval("PurchaseOrderID") %>' />
                                                    <asp:HiddenField ID="hdnVendorID" runat="server" Value='<%#Eval("Vendor.VendorID") %>' />
                                                    <asp:HiddenField ID="hdnConfirmPOArrival" runat="server" Value='<%#Eval("ConfirmPOArrival") %>' />
                                                    <asp:HiddenField ID="hdnIsNotDelieveredEmailSent" runat="server" Value='<%#Eval("IsNotDelieveredEmailSent") %>' />
                                                    <cc1:ucLabel ID="lblPOValue" runat="server" Text='<%#Eval("PurchaseOrders") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vendor" HeaderStyle-HorizontalAlign="Left">
                                                <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblVendorNameValue" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delivered">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" CssClass="nobold radiobuttonlist" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="chkDeliveredValue" CssClass="radio-fix" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NotDelivered">
                                                <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" CssClass="nobold radiobuttonlist" />
                                                <ItemTemplate>
                                                    <cc1:ucCheckbox ID="chkNotDeliveredValue" CssClass="radio-fix" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </cc1:ucGridView>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table width="100%">
                            <tr>
                                <td align="right">
                                    <cc1:ucButton ID="EmailVendorNew" runat="server" Text="Email Vendor" CssClass="button"
                                        OnClick="btnEmailVendor_Click" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;<cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button"
                                        OnClick="btnBack_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center">
                            <asp:UpdateProgress runat="server" ID="PageUpdateProgress" AssociatedUpdatePanelID="up1">
                                <ProgressTemplate>
                                    <div align="center" style="background-color: #000; top: 0px; left: 0px; bottom: 0px;
                                        right: 0px; padding-top: 20%; margin: 0; width: 100%; height: 50%; overflow: hidden;
                                        position: absolute; z-index: 1000; filter: alpha(opacity=50); opacity: 0.5;">
                                        <asp:Image ID="imgWait" runat="server" ImageUrl="~/Images/Ajaxloading.gif" ImageAlign="Middle" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="EmailVendorNew" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="up2" runat="server" >
        <ContentTemplate>
         <asp:HiddenField ID="hdnEmailFlag" runat="server"/>
            <div id="divFade" runat="server" style="height: 100%; width: 100%; position: fixed;
                background: rgba(0, 0, 0,0.2); opacity: 1; top: 0px; left: 0px; z-index: 1000;"
                visible="False">
            </div>
            <div id="divpopup" runat="server" style="position: absolute; left: 200px !important;
                top: 200px !Important; z-index: 1001; background-color: #ffffff; border-radius: 5px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.2); float: left; margin: 0 0 20px 22px;
                padding: 10px; height: 600px; width: 800px;" visible="False">
                <div style="width: 100%; padding-left: 10px">
                    <table>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr id="rwButtons">
                            <td style="text-align: left; font-weight: bold;">
                                <cc1:ucLabel Text="To : " runat="server" ID="lblEmail"></cc1:ucLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                <cc1:ucDropdownList ID="ddlSentEmailIds" runat="server">
                                </cc1:ucDropdownList>
                                <cc1:ucTextbox ID="txtEmail" runat="server" Visible="false" Width="200px"></cc1:ucTextbox>&nbsp;&nbsp;&nbsp;
                                <asp:RegularExpressionValidator ID="revValidEmail" runat="server" Display="None"
                                    ValidationGroup="ReSendCommunication" ControlToValidate="txtEmail" ErrorMessage="Please enter valid email address"
                                    ValidationExpression="((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([,])*)*"></asp:RegularExpressionValidator>
                                <cc1:ucButton ID="btnSend" runat="server" Text="Send" class="button" OnClick="btnReSendCommunication_Click"
                                    ValidationGroup="ReSendCommunication" />
                                &nbsp;&nbsp;
                                <cc1:ucButton ID="btncancel" runat="server" OnClick="btncancel_Click" />
                                <cc1:ucButton ID="btnPrintLetter" runat="server" Visible="false" CssClass="button"
                                    Text="Print Letter" OnClientClick="PrintScreen();" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; font-weight: bold;">
                                <cc1:ucLabel Text="Language : " runat="server" ID="UcLabel1"></cc1:ucLabel>
                                <asp:DropDownList ID="drpLanguageId" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpLanguageId_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdSubject" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                    Style="color: Red" ValidationGroup="ReSendCommunication" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="POReconciliationHeader" runat="server">
                                    <b>
                                        <cc1:ucLabel ID="lblPOReconciliationHeader" runat="server" Text="The below mail will be sent to the vendor advising them that the below Purchase Orders were not delivered"></cc1:ucLabel></b>
                                    <b><span style="text-align: center; display: block; width: 600px">
                                        <cc1:ucLabel ID="lblPOReconciliationHeader1" runat="server" Text="Please note that once sent, these PO's cannot be reset as being delivered"></cc1:ucLabel></span></b>
                                    <b><span style="text-align: center; display: block; width: 600px">
                                        <cc1:ucLabel ID="lblPOReconciliationHeader2" runat="server" Text="Also, these PO's will be set to chased on the PO Reconcilation "></cc1:ucLabel></span></b>
                                    <br />
                                    <br />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="divReSendCommunication" runat="server">
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div id="innerData" style="display: none;">
                        <cc1:ucLabel ID="ltlable" runat="server"></cc1:ucLabel>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="drpLanguageId" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

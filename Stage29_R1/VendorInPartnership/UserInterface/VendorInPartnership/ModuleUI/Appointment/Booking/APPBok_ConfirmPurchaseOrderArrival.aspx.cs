﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BaseControlLibrary;
using Utilities;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.Languages.Languages;
using System.IO;
using System.Configuration;
using System.Data;
using System.Drawing;

public partial class APPBok_ConfirmPurchaseOrderArrival : CommonPage
{
    #region Declarations ...
    APPBOK_BookingBAL bookingBAL = null;
    APPBOK_BookingBE bookingBE = null;

    string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/Security/Login.aspx";
    string SelectDeliveredOrNotDelivered = WebCommon.getGlobalResourceValue("YouCanSelectDeliveredOrNotDeliveredForTheSamePO");
    string SelectDeliveredOrNotDeliverednew = WebCommon.getGlobalResourceValue("YouCanSelectDeliveredOrNotDeliveredForTheSamePOnew");
    string NotDeliveredPOErrorMessage = WebCommon.getGlobalResourceValue("NotDeliveredPOErrorMessage");
    //protected string hdnEmailFlag = string.Empty;
    DataTable dt;
    #endregion

    #region Events ...

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("BookId") != null)
            {
                var bookingId = Convert.ToInt32(GetQueryStringValue("BookId"));
                this.BindConfirmPOArrivalsData(bookingId);


            }
        }
    }

    protected void gvConfirmPOArrival_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (!e.Row.RowType.Equals(DataControlRowType.Header) && !e.Row.RowType.Equals(DataControlRowType.Footer))
        {
            var hdnConfirmPOArrival = e.Row.FindControl("hdnConfirmPOArrival") as HiddenField;
            var chkDeliveredValue = e.Row.FindControl("chkDeliveredValue") as ucCheckbox;
            var chkNotDeliveredValue = e.Row.FindControl("chkNotDeliveredValue") as ucCheckbox;
            var hdnIsNotDelieveredEmailSent = e.Row.FindControl("hdnIsNotDelieveredEmailSent") as HiddenField;
            
            if (hdnConfirmPOArrival != null)
            {
                switch (hdnConfirmPOArrival.Value)
                {
                    case "Delivered":
                        chkDeliveredValue.Checked = true;
                        break;
                    case "NotDelivered":
                        chkNotDeliveredValue.Checked = true;                       
                        break;
                    default:
                        break;
                }
            }
            if (hdnIsNotDelieveredEmailSent.Value == "True")
            {
                e.Row.BackColor = Color.Green;
                switch (hdnIsNotDelieveredEmailSent.Value)
                {
                    case "True":
                        chkDeliveredValue.Enabled = false;
                        chkNotDeliveredValue.Enabled = false;
                        break;
                    default:
                        break;
                } 
            }
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPBok_BookingOverview.aspx");
    }

    private void ChaseMailToVendor(string[] VendorData, string BookingType)
    {
        string[] sentTo;
        string[] language;


        ViewState["VendorEmailIds"] = VendorData[0];

        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
        {
            sentTo = VendorData[0].Split(',');
            language = VendorData[1].Split(',');

            for (int j = 0; j < sentTo.Length; j++)
            {
                if (sentTo[j] != " ")
                {
                    SendMailToVendors(sentTo[j], language[j], 1, BookingType);
                }
            }
        }

        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
        return;
    }

    private void BindLanguages()
    {
        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
        drpLanguageId.DataSource = oLanguages;
        drpLanguageId.DataValueField = "LanguageID";
        drpLanguageId.DataTextField = "Language";
        drpLanguageId.DataBind();
    }

    private void SendMailToVendors(string toAddress, string language, int languageid, string BookingType = "SNBI")
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.User = new SCT_UserBE();
            oBackOrderBE.Action = "GetVendorSiteAddress";
            oBackOrderBE.Vendor.VendorID = Convert.ToInt32(ViewState["VendorID"]);
            oBackOrderBE.SiteID = Convert.ToInt32(ViewState["SiteId"]);
            var lstBackOrderBE = oBackOrderBAL.GetVendorDetailBAL(oBackOrderBE);

            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {

                #region Logic to Save & Send email ...
                var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                //foreach (MAS_LanguageBE objLanguage in oLanguages)
                //{
                //    bool MailSentInLanguage = false;
                //    if (objLanguage.Language.ToLower() == language.ToLower())
                //        MailSentInLanguage = true;

                var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                var templateFile = string.Empty;
                if (BookingType == "SNBI")
                {
                    templateFile = string.Format(@"{0}emailtemplates/Appointment/POReconciliationSNBI.english.htm", path);
                }
                //else
                //{
                //    templateFile = string.Format(@"{0}emailtemplates/Appointment/POReconciliationNSBI.english.htm", path);
                //}

                #region Setting Region as per the language ...
                switch (languageid)
                {
                    case 1:
                        Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                        break;
                    case 2:
                        Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                        break;
                    case 3:
                        Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                        break;
                    case 4:
                        Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                        break;
                    case 5:
                        Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                        break;
                    case 6:
                        Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                        break;
                    case 7:
                        Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                        break;
                    default:
                        Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                        break;
                }

                #endregion

                #region  Prepairing html body format ...
                string htmlBody = null;
                using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                {
                    htmlBody = sReader.ReadToEnd();

                    htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));
                    htmlBody = htmlBody.Replace("{DateValue}", DateTime.Now.ToString("dd/MM/yyyy"));
                    htmlBody = htmlBody.Replace("{VendorCodeValue}", lstBackOrderBE[0].Vendor.Vendor_No);
                    htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
                    if (BookingType == "SNBI")
                    {
                        htmlBody = htmlBody.Replace("{BookingReference}", WebCommon.getGlobalResourceValue("BookingReference"));
                    }
                    htmlBody = htmlBody.Replace("{BookingRefValue}", Convert.ToString(ViewState["BookingRef"]));
                    htmlBody = htmlBody.Replace("{VendorNameValue}", lstBackOrderBE[0].Vendor.VendorName);
                    htmlBody = htmlBody.Replace("{PurchaseOrderNumber}", WebCommon.getGlobalResourceValue("PurchaseOrderNumber"));
                    if (BookingType == "SNBI")
                    {
                        htmlBody = htmlBody.Replace("{PurchaseOrderNumberValue}", Convert.ToString(ViewState["MultiPONo"]));
                    }
                    //else
                    //{
                    //    htmlBody = htmlBody.Replace("{PurchaseOrderNumberValue}", Convert.ToString(ViewState["PurchaseOrderNo"]));
                    //}
                    htmlBody = htmlBody.Replace("{VendorAddress1Value}", lstBackOrderBE[0].Vendor.address1);
                    htmlBody = htmlBody.Replace("{DeliveryDate}", WebCommon.getGlobalResourceValue("DeliveryDate"));
                    if (BookingType == "SNBI")
                    {
                        htmlBody = htmlBody.Replace("{DeliveryDateValue}", lblDateValue.Text);
                    }
                    //else
                    //{
                    //    htmlBody = htmlBody.Replace("{DeliveryDateValue}", Convert.ToString(ViewState["DeliveryDate"]));
                    //}

                    htmlBody = htmlBody.Replace("{VendorAddress2Value}", lstBackOrderBE[0].Vendor.address2);

                    if (BookingType == "SNBI")
                    {
                        htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier"));
                        htmlBody = htmlBody.Replace("{CarrierValue}", lblCarrierValue.Text);
                    }
                    if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.VMPPIN) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.VMPPIN))
                        htmlBody = htmlBody.Replace("{VMPPIN}", lstBackOrderBE[0].Vendor.VMPPIN);
                    else
                        htmlBody = htmlBody.Replace("{VMPPIN}", string.Empty);

                    if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.VMPPOU) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.VMPPOU))
                        htmlBody = htmlBody.Replace("{VMPPOU}", lstBackOrderBE[0].Vendor.VMPPOU);
                    else
                        htmlBody = htmlBody.Replace("{VMPPOU}", string.Empty);

                    if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.city) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.city))
                        htmlBody = htmlBody.Replace("{VendorCity}", lstBackOrderBE[0].Vendor.city);
                    else
                        htmlBody = htmlBody.Replace("{VendorCity}", string.Empty);

                    htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));

                    if (BookingType == "SNBI")
                    {
                        htmlBody = htmlBody.Replace("{POReconciliationSNBIText1}", WebCommon.getGlobalResourceValue("POReconciliationSNBIText1"));
                        htmlBody = htmlBody.Replace("{DeliveryDateValue}", Convert.ToString(ViewState["ScheduleDate"]));
                    }
                    //else
                    //{
                    //    htmlBody = htmlBody.Replace("{POReconciliationNSBIText1}", WebCommon.getGlobalResourceValue("POReconciliationNSBIText1"));
                    //    htmlBody = htmlBody.Replace("{POReconciliationNSBIText2}", WebCommon.getGlobalResourceValue("POReconciliationNSBIText2"));
                    //    htmlBody = htmlBody.Replace("{DeliveryDateValue}", Convert.ToString(ViewState["DeliveryDate"]));
                    //}


                    htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));


                    var apAddress = string.Empty;
                    if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                        && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4)
                        && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress5) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress6))
                    {
                        apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                            lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4, lstBackOrderBE[0].APAddress5, lstBackOrderBE[0].APAddress6);
                    }
                    else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                        && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4)
                        && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress5))
                    {
                        apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                            lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4, lstBackOrderBE[0].APAddress5);
                    }
                    else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                       && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4))
                    {
                        apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                            lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4);
                    }
                    else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                       && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3))
                    {
                        apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                            lstBackOrderBE[0].APAddress3);
                    }
                    else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2))
                    {
                        apAddress = string.Format("{0},&nbsp;{1}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2);
                    }
                    else
                    {
                        apAddress = lstBackOrderBE[0].APAddress1;
                    }

                    if (!string.IsNullOrEmpty(lstBackOrderBE[0].APPincode))
                        apAddress = string.Format("{0},&nbsp;{1}", apAddress, lstBackOrderBE[0].APPincode);

                    htmlBody = htmlBody.Replace("{SiteAddress}", apAddress);

                    divReSendCommunication.InnerHtml = htmlBody;
                    return;

                    // }
                #endregion
                }


                #endregion
            }
        }
    }
    protected void btnReSendCommunication_Click(object sender, EventArgs e)
    {
        string toAddress = ddlSentEmailIds.SelectedValue;
        string BookingType = "SNBI";
        if (!string.IsNullOrEmpty(toAddress))
        {
            sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.User = new SCT_UserBE();
            oBackOrderBE.Action = "GetVendorSiteAddress";
            oBackOrderBE.Vendor.VendorID = Convert.ToInt32(ViewState["VendorID"]);
            oBackOrderBE.SiteID = Convert.ToInt32(ViewState["SiteId"]);
            var lstBackOrderBE = oBackOrderBAL.GetVendorDetailBAL(oBackOrderBE);

            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {

                #region Logic to Save & Send email ...
                var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                foreach (MAS_LanguageBE objLanguage in oLanguages)
                {
                    bool MailSentInLanguage = false;
                    if (objLanguage.LanguageID == Convert.ToInt32(drpLanguageId.SelectedValue))
                        MailSentInLanguage = true;

                    var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                    var templateFile = string.Empty;
                    if (BookingType == "SNBI")
                    {
                        templateFile = string.Format(@"{0}emailtemplates/Appointment/POReconciliationSNBI.english.htm", path);
                    }
                    //else
                    //{
                    //    templateFile = string.Format(@"{0}emailtemplates/Appointment/POReconciliationNSBI.english.htm", path);
                    //}

                    #region Setting Region as per the language ...
                    switch (objLanguage.LanguageID)
                    {
                        case 1:
                            Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                            break;
                        case 2:
                            Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                            break;
                        case 3:
                            Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                            break;
                        case 4:
                            Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                            break;
                        case 5:
                            Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                            break;
                        case 6:
                            Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                            break;
                        case 7:
                            Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                            break;
                        default:
                            Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                            break;
                    }

                    #endregion

                    #region  Prepairing html body format ...
                    string htmlBody = null;
                    using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                    {
                        htmlBody = sReader.ReadToEnd();

                        htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));
                        htmlBody = htmlBody.Replace("{DateValue}", DateTime.Now.ToString("dd/MM/yyyy"));
                        htmlBody = htmlBody.Replace("{VendorCodeValue}", lstBackOrderBE[0].Vendor.Vendor_No);
                        if (BookingType == "SNBI")
                        {
                            htmlBody = htmlBody.Replace("{BookingReference}", WebCommon.getGlobalResourceValue("BookingReference"));
                        }
                        htmlBody = htmlBody.Replace("{BookingRefValue}", Convert.ToString(ViewState["BookingRef"]));
                        htmlBody = htmlBody.Replace("{VendorNameValue}", lstBackOrderBE[0].Vendor.VendorName);
                        htmlBody = htmlBody.Replace("{PurchaseOrderNumber}", WebCommon.getGlobalResourceValue("PurchaseOrderNumber"));
                        htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
                        if (BookingType == "SNBI")
                        {
                            htmlBody = htmlBody.Replace("{PurchaseOrderNumberValue}", Convert.ToString(ViewState["MultiPONo"]));
                        }
                        //else
                        //{
                        //    htmlBody = htmlBody.Replace("{PurchaseOrderNumberValue}", Convert.ToString(ViewState["PurchaseOrderNo"]));
                        //}
                        htmlBody = htmlBody.Replace("{VendorAddress1Value}", lstBackOrderBE[0].Vendor.address1);
                        htmlBody = htmlBody.Replace("{DeliveryDate}", WebCommon.getGlobalResourceValue("DeliveryDate"));
                        if (BookingType == "SNBI")
                        {
                            htmlBody = htmlBody.Replace("{DeliveryDateValue}", lblDateValue.Text);
                        }
                        //else
                        //{
                        //    htmlBody = htmlBody.Replace("{DeliveryDateValue}", Convert.ToString(ViewState["DeliveryDate"]));
                        //}

                        htmlBody = htmlBody.Replace("{VendorAddress2Value}", lstBackOrderBE[0].Vendor.address2);

                        if (BookingType == "SNBI")
                        {
                            htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier"));
                            htmlBody = htmlBody.Replace("{CarrierValue}", lblCarrierValue.Text);
                        }
                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.VMPPIN) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.VMPPIN))
                            htmlBody = htmlBody.Replace("{VMPPIN}", lstBackOrderBE[0].Vendor.VMPPIN);
                        else
                            htmlBody = htmlBody.Replace("{VMPPIN}", string.Empty);

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.VMPPOU) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.VMPPOU))
                            htmlBody = htmlBody.Replace("{VMPPOU}", lstBackOrderBE[0].Vendor.VMPPOU);
                        else
                            htmlBody = htmlBody.Replace("{VMPPOU}", string.Empty);

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.city) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.city))
                            htmlBody = htmlBody.Replace("{VendorCity}", lstBackOrderBE[0].Vendor.city);
                        else
                            htmlBody = htmlBody.Replace("{VendorCity}", string.Empty);

                        htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));

                        if (BookingType == "SNBI")
                        {
                            htmlBody = htmlBody.Replace("{POReconciliationSNBIText1}", WebCommon.getGlobalResourceValue("POReconciliationSNBIText1"));
                            htmlBody = htmlBody.Replace("{DeliveryDateValue}", Convert.ToString(ViewState["ScheduleDate"]));
                        }
                        //else
                        //{
                        //    htmlBody = htmlBody.Replace("{POReconciliationNSBIText1}", WebCommon.getGlobalResourceValue("POReconciliationNSBIText1"));
                        //    htmlBody = htmlBody.Replace("{POReconciliationNSBIText2}", WebCommon.getGlobalResourceValue("POReconciliationNSBIText2"));
                        //    htmlBody = htmlBody.Replace("{DeliveryDateValue}", Convert.ToString(ViewState["DeliveryDate"]));
                        //}


                        htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLink + " </a>");
                        htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));


                        var apAddress = string.Empty;
                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress5) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress6))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4, lstBackOrderBE[0].APAddress5, lstBackOrderBE[0].APAddress6);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress5))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4, lstBackOrderBE[0].APAddress5);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                           && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                           && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2))
                        {
                            apAddress = string.Format("{0},&nbsp;{1}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2);
                        }
                        else
                        {
                            apAddress = lstBackOrderBE[0].APAddress1;
                        }

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].APPincode))
                            apAddress = string.Format("{0},&nbsp;{1}", apAddress, lstBackOrderBE[0].APPincode);

                        htmlBody = htmlBody.Replace("{SiteAddress}", apAddress);



                    }
                    #endregion

                    #region Sending and saving email details and updating Chase Status ...


                    var sentToWithLink = new System.Text.StringBuilder();
                    sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("POReconciliation_ReSendCommunication.aspx.aspx?POChaseCommunicationId=" + ViewState["POChaseCommunicationId"]) + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + toAddress + "</a>" + System.Environment.NewLine);

                    var oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
                    oAPPBOK_CommunicationBE.Action = "AddPOChaseCommunication";

                    string SNBIEmailSubject = WebCommon.getGlobalResourceValue("SNBIEmailSubject");
                    string NSBIEmailSubject = WebCommon.getGlobalResourceValue("NSBIEmailSubject");

                    if (BookingType == "SNBI")
                    {
                        oAPPBOK_CommunicationBE.Subject = SNBIEmailSubject;
                    }
                    else
                    {
                        oAPPBOK_CommunicationBE.Subject = NSBIEmailSubject;
                    }
                    oAPPBOK_CommunicationBE.SentTo = toAddress;
                    oAPPBOK_CommunicationBE.POChaseSentDate = DateTime.Now;
                    oAPPBOK_CommunicationBE.Body = htmlBody;
                    oAPPBOK_CommunicationBE.SendByID = Convert.ToInt32(Session["UserID"]);
                    oAPPBOK_CommunicationBE.LanguageID = objLanguage.LanguageID;
                    oAPPBOK_CommunicationBE.MailSentInLanguage = MailSentInLanguage;
                    oAPPBOK_CommunicationBE.CommunicationStatus = "Initial";
                    oAPPBOK_CommunicationBE.SentToWithLink = sentToWithLink.ToString();
                    oAPPBOK_CommunicationBE.VendorEmailIds = Convert.ToString(ViewState["VendorEmailIds"]);
                    oAPPBOK_CommunicationBE.POChaseCommunicationId = Convert.ToInt32(ViewState["POChaseCommunicationId"]);

                    APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL1 = new APPBOK_CommunicationBAL();
                    var CommId = oAPPBOK_CommunicationBAL1.AddBookingCommunicationBAL(oAPPBOK_CommunicationBE);
                    if (MailSentInLanguage)
                    {
                        #region Update Chase Status and POChaseCommunicationId
                        //Update Chase Status and PO Chase CommunicationID
                        if (BookingType == "SNBI")
                        {
                            oAPPBOK_CommunicationBE.MultiPoIds = Convert.ToString(ViewState["MultiPOIDs"]);
                            oAPPBOK_CommunicationBE.Action = "UpdateChaseStatusSNBI";
                        }
                        else
                        {
                            oAPPBOK_CommunicationBE.Purchase_order = Convert.ToString(ViewState["PurchaseOrderNo"]);
                            oAPPBOK_CommunicationBE.SiteID = Convert.ToInt32(ViewState["SiteId"]);
                            oAPPBOK_CommunicationBE.OrderRaised = Convert.ToDateTime(ViewState["OrderRaised"]);
                            oAPPBOK_CommunicationBE.Country = Convert.ToString(ViewState["Country"]);
                            oAPPBOK_CommunicationBE.Action = "UpdateChaseStatusNSBI";
                        }
                        #endregion


                        #region Mail Send
                        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL2 = new APPBOK_CommunicationBAL();
                        var ChaseId = oAPPBOK_CommunicationBAL2.UpdateChaseStatusBAL(oAPPBOK_CommunicationBE);
                        //--------------------------------------------------------------------------

                        var emailToSubject = string.Empty;
                        var emailToAddress = toAddress;
                        var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                        if (BookingType == "SNBI")
                        {
                            emailToSubject = SNBIEmailSubject;
                        }
                        else
                        {
                            emailToSubject = NSBIEmailSubject;
                        }
                        var emailBody = htmlBody;
                        Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                        oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                        #endregion
                    }
                    #endregion
                }


                #endregion
            }
        }

        divFade.Visible = false;
        divpopup.Visible = false;
        ViewState["isEmailvendor"] = true;
        btnReSendSave_Click(sender, e);
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "MailToReSend", "disablecheckbox();", true);       
    }
    protected void drpLanguageId_SelectedIndexChanged(object sender, EventArgs e)
    {
        SendMailToVendors(ddlSentEmailIds.SelectedValue, drpLanguageId.SelectedItem.Text, Convert.ToInt32(drpLanguageId.SelectedValue));
    }

    protected void ddlSentEmailIds_SelectedIndexChanged(object sender, EventArgs e)
    {
        SendMailToVendors(ddlSentEmailIds.SelectedValue, drpLanguageId.SelectedItem.Text, Convert.ToInt32(drpLanguageId.SelectedValue));

    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        divFade.Visible = false;
        divpopup.Visible = false;
    }

    protected void btnEmailVendor_Click(object sender, EventArgs e)
    {
        bool blnStatus = true;
        for (int index = 0; index < gvConfirmPOArrival.Rows.Count; index++)
        {
            var chkDeliveredValue = gvConfirmPOArrival.Rows[index].FindControl("chkDeliveredValue") as ucCheckbox;
            var chkNotDeliveredValue = gvConfirmPOArrival.Rows[index].FindControl("chkNotDeliveredValue") as ucCheckbox;
            if (chkDeliveredValue != null && chkNotDeliveredValue != null)
            {
                if (chkDeliveredValue.Checked && chkNotDeliveredValue.Checked)
                {
                    blnStatus = false;
                    break;
                }
            }
            hdnEmailFlag.Value = "true";
        }

        if (blnStatus == false)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SelectDeliveredOrNotDeliverednew + "')", true);
            hdnEmailFlag.Value = "false";
            return;
        }

        string POs = string.Empty;
        string POIds = string.Empty;
        int count = 0;
        for (int index = 0; index < gvConfirmPOArrival.Rows.Count; index++)
        {
            var chkNotDeliveredValue = gvConfirmPOArrival.Rows[index].FindControl("chkNotDeliveredValue") as ucCheckbox;
            Label lblPOValue = gvConfirmPOArrival.Rows[index].FindControl("lblPOValue") as Label;
            HiddenField hdnPurchaseOrderID = gvConfirmPOArrival.Rows[index].FindControl("hdnPurchaseOrderID") as HiddenField;

            if (chkNotDeliveredValue != null)
            {
                if (chkNotDeliveredValue.Checked && chkNotDeliveredValue.Enabled == true)
                {
                    POs += lblPOValue.Text + ',';
                    POIds += hdnPurchaseOrderID.Value + ',';
                    count = count + 1;
                }
            }
        }

        if (count == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + NotDeliveredPOErrorMessage + "')", true);
            return;
        }

        ViewState["MultiPONo"] = POs.Trim().Substring(0, POs.Length - 1);
        ViewState["MultiPOIDs"] = POIds.Trim().Substring(0, POIds.Length - 1);
        #region Getting New Po Chase Communication ID and setting it in Viewstate
        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        int? POChaseCommunicationId;
        POChaseCommunicationId = oAPPBOK_CommunicationBAL.GetPOChaseCommunicationIDBAL();
        ViewState["POChaseCommunicationId"] = POChaseCommunicationId;
        #endregion
        BindLanguages();
        string[] VendorData = CommonPage.GetVendorEmailsWithLanguage(Convert.ToInt32(ViewState["VendorID"]), Convert.ToInt32(ViewState["SiteId"]));

        ddlSentEmailIds.DataSource = null;
        ddlSentEmailIds.DataSource = VendorData[0].Split(',').ToList();
        ddlSentEmailIds.DataBind();
        ListItem thisItem1 = (ListItem)ddlSentEmailIds.Items.FindByValue("");
        if (thisItem1 != null)
        {
            ddlSentEmailIds.Items.Remove(thisItem1);
        }
        ChaseMailToVendor(VendorData, "SNBI");
        divpopup.Visible = true;
        divFade.Visible = true;

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        /* Validation to check [Delivered] and [Not Delivered] both options should not be selected. */
        bool blnStatus = true;
        ViewState["isEmailvendor"] = null;
        for (int index = 0; index < gvConfirmPOArrival.Rows.Count; index++)
        {
            var chkDeliveredValue = gvConfirmPOArrival.Rows[index].FindControl("chkDeliveredValue") as ucCheckbox;
            var chkNotDeliveredValue = gvConfirmPOArrival.Rows[index].FindControl("chkNotDeliveredValue") as ucCheckbox;
            if (chkDeliveredValue != null && chkNotDeliveredValue != null)
            {
                if (chkDeliveredValue.Checked && chkNotDeliveredValue.Checked)
                {
                    blnStatus = false;
                    break;
                }
            }
        }

        if (blnStatus)
        {
            var bookingId = Convert.ToInt32(GetQueryStringValue("BookId")); ;
            this.SaveConfirmPOArrivals(bookingId);
            if (ViewState["isEmailvendor"] == null)
            {
                EncryptQueryString("APPBok_BookingOverview.aspx");
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SelectDeliveredOrNotDeliverednew + "')", true);
            return;
        }
    }


    protected void btnReSendSave_Click(object sender, EventArgs e)
    {
        /* Validation to check [Delivered] and [Not Delivered] both options should not be selected. */
        bool blnStatus = true;
        for (int index = 0; index < gvConfirmPOArrival.Rows.Count; index++)
        {
            var chkDeliveredValue = gvConfirmPOArrival.Rows[index].FindControl("chkDeliveredValue") as ucCheckbox;
            var chkNotDeliveredValue = gvConfirmPOArrival.Rows[index].FindControl("chkNotDeliveredValue") as ucCheckbox;
            if (chkDeliveredValue != null && chkNotDeliveredValue != null)
            {
                if (chkDeliveredValue.Checked && chkNotDeliveredValue.Checked)
                {
                    blnStatus = false;
                    break;
                }
            }
        }

        if (blnStatus)
        {
            var bookingId = Convert.ToInt32(GetQueryStringValue("BookId")); ;
            this.SaveConfirmPOArrivals(bookingId);
            //EncryptQueryString("APPBok_BookingOverview.aspx");         
            hdnEmailFlag.Value = "true";
        }
        else
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SelectDeliveredOrNotDeliverednew + "')", true);
            hdnEmailFlag.Value = "false";
            return;
        }
    }
    #endregion

    #region Methods ...

    private void BindConfirmPOArrivalsData(int bookingID)
    {
        bookingBE = new APPBOK_BookingBE();
        bookingBE.Action = "GetConfirmPurchaseOrderArrivalData";
        bookingBE.BookingID = bookingID;
        bookingBAL = new APPBOK_BookingBAL();
        var lstBookingDetails = bookingBAL.GetConfirmPurchaseOrderArrivalDataBAL(bookingBE);
        if (lstBookingDetails != null && lstBookingDetails.Count > 0)
        {
            lblStatusValue.Text = lstBookingDetails[0].BookingStatus;
            if (lstBookingDetails[0].ScheduleDate != null)
            {
                string[] dtArr = lstBookingDetails[0].ScheduleDate.ToString().Split('/');
                lblDateValue.Text = string.Format("{0}/{1}/{2}", dtArr[1], dtArr[0], dtArr[2].Substring(0, 4));
            }
            lblPalletsValue.Text = Convert.ToString(lstBookingDetails[0].NumberOfPallet);
            lblCartonsValue.Text = Convert.ToString(lstBookingDetails[0].NumberOfCartons);
            lblLinesValue.Text = Convert.ToString(lstBookingDetails[0].NumberOfLines);
            lblCarrierValue.Text = lstBookingDetails[0].Carrier.CarrierName;
            lblVehicleTypeValue.Text = lstBookingDetails[0].VehicleType.VehicleType;

            ViewState["SiteId"] = lstBookingDetails[0].SiteId;
            ViewState["VendorID"] = lstBookingDetails[0].Vendor.VendorID;
            ViewState["BookingRef"] = lstBookingDetails[0].BookingRef;
            ViewState["ScheduleDate"] = lblDateValue.Text;
            gvConfirmPOArrival.DataSource = lstBookingDetails;
        }
        else
            gvConfirmPOArrival.DataSource = null;

        gvConfirmPOArrival.DataBind();
        bookingBE = null;
        bookingBAL = null;
       // ViewState["isEmailvendor"] = true;
    }

    private void SaveConfirmPOArrivals(int bookingID)
    {
        bookingBE = new APPBOK_BookingBE();
        bookingBE.Action = "UpdateConfirmPurchaseOrderArrival";
        bookingBE.BookingID = bookingID;

        string[] DelNotDel = this.DeliveredNotDeliveredData();
        bookingBE.DeliveredPO = DelNotDel[0];
        bookingBE.DeliveredVendorIds = DelNotDel[1];
        bookingBE.NotDeliveredPO = DelNotDel[2];
        bookingBE.NotDeliveredVendorIds = DelNotDel[3];

        bookingBAL = new APPBOK_BookingBAL();
        bookingBAL.UpdateConfirmPurchaseOrderArrivalBAL(bookingBE);

        
        if (ViewState["isEmailvendor"] != null && Convert.ToBoolean(ViewState["isEmailvendor"])==true)
        {
            // those one's to which mail has been sent
            bookingBE.Action = "UpdateIsNotDelieveredEmailSent";
            bookingBE.BookingID = bookingID;
            bookingBE.NotDeliveredPO = DelNotDel[2];
            bookingBE.NotDeliveredVendorIds = DelNotDel[3];
           

            bookingBAL = new APPBOK_BookingBAL();
            bookingBAL.UpdateConfirmPurchaseOrderArrivalBAL(bookingBE);

          var bookingId = Convert.ToInt32(GetQueryStringValue("BookId"));
          this.BindConfirmPOArrivalsData(bookingId);            
        }
        bookingBE = null;
        bookingBAL = null;
    }

    /// <summary>
    /// Array output record sequence : 
    /// 0th Index : Delivered PO,
    /// 1st Index : Delivered Vendor Ids,
    /// 2nd Index : Not Delivered PO,
    /// 3rd Index : Not Delivered Vendor Ids
    /// </summary>
    /// <returns></returns>
    private string[] DeliveredNotDeliveredData()
    {
        string[] DelNotDel = new string[4];
        for (int index = 0; index < gvConfirmPOArrival.Rows.Count; index++)
        {
            var hdnVendorID = gvConfirmPOArrival.Rows[index].FindControl("hdnVendorID") as HiddenField;
            var lblPOValue = gvConfirmPOArrival.Rows[index].FindControl("lblPOValue") as ucLabel;
            var chkDeliveredValue = gvConfirmPOArrival.Rows[index].FindControl("chkDeliveredValue") as ucCheckbox;
            var chkNotDeliveredValue = gvConfirmPOArrival.Rows[index].FindControl("chkNotDeliveredValue") as ucCheckbox;
            if (hdnVendorID != null)
            {
                if (chkDeliveredValue.Checked && string.IsNullOrEmpty(DelNotDel[0]))
                {
                    DelNotDel[0] = lblPOValue.Text;
                    DelNotDel[1] = hdnVendorID.Value;
                }
                else if (chkDeliveredValue.Checked && !string.IsNullOrEmpty(DelNotDel[0]))
                {
                    DelNotDel[0] = string.Format("{0},{1}", DelNotDel[0], lblPOValue.Text);
                    DelNotDel[1] = string.Format("{0},{1}", DelNotDel[1], hdnVendorID.Value);
                }

                if (chkNotDeliveredValue.Checked && string.IsNullOrEmpty(DelNotDel[2]))
                {
                    DelNotDel[2] = lblPOValue.Text;
                    DelNotDel[3] = hdnVendorID.Value;
                }
                else if (chkNotDeliveredValue.Checked && !string.IsNullOrEmpty(DelNotDel[2]))
                {
                    DelNotDel[2] = string.Format("{0},{1}", DelNotDel[2], lblPOValue.Text);
                    DelNotDel[3] = string.Format("{0},{1}", DelNotDel[3], hdnVendorID.Value);
                }
            }
        }

        return DelNotDel;
    }

    #endregion
}
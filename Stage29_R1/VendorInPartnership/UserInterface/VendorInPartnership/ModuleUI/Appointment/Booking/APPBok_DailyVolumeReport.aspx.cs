﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.Booking;
using Utilities;
using System.Text;
using System.Data;
using WebUtilities;

public partial class APPBok_DailyVolumeReport : CommonPage {
    public static string PreviousSiteID {
        get {
            return Convert.ToString(HttpContext.Current.Session["PreviousSiteID"]);
        }
        set { HttpContext.Current.Session["PreviousSiteID"] = value; }
    }

    public static string PreviousDate {
        get {
            return Convert.ToString(HttpContext.Current.Session["PreviousDate"]);
        }
        set { HttpContext.Current.Session["PreviousDate"] = value; }
    }

    protected void Page_Init(object sender, EventArgs e) {
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = false;
    }

    protected void Page_Load(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
            txtDate.innerControltxtDate.Value = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy"));
            HttpContext.Current.Session["lstDailyVolumeDetail"] = null;
        }
    }
    
    protected void btnGenerateReport_Click(object sender, EventArgs e) {
        //List<APPBOK_BookingBE> lstDailyVolumeDetail = (List<APPBOK_BookingBE>)HttpContext.Current.Session["lstDailyVolumeDetail"];
        //GetSummeryonLoad();
        BindDailyVolume();
    }

    public void BindDailyVolume()
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);
        DataSet ds = oAPPBOK_BookingBAL.GetDailyVolumeTrackerBAL(oAPPBOK_BookingBE);
        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                ds.Tables[0].Rows.Add(dr.ItemArray);
            }
            ViewState["DataTable"] = ds.Tables[0];
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdBind.DataSource = ds.Tables[0];
                grdBind.DataBind();
                lblNoRecordsFound.Visible = false;
            }
            else
            {
                grdBind.DataSource = null;
                grdBind.DataBind();
                lblNoRecordsFound.Visible = true;
            }
        }
        else
        {
            grdBind.DataSource = null;
            grdBind.DataBind();
            lblNoRecordsFound.Visible = true;
        }
    }

    private void GetSummeryonLoad() {
        List<APPBOK_BookingBE> lstDailyVolumeDetail = (List<APPBOK_BookingBE>)HttpContext.Current.Session["lstDailyVolumeDetail"];

        ltTotalPallets.Text = lstDailyVolumeDetail.Sum(o => o.NumberOfPallet).ToString();
        ltTotalLines.Text = lstDailyVolumeDetail.Sum(o => o.NumberOfLines).ToString();
        ltTotalDeliveries.Text = lstDailyVolumeDetail.Sum(o => o.NumberOfDeliveries).ToString();

        ltConfirmedPallets.Text = lstDailyVolumeDetail.Where(vol => vol.GraphType == "1").Sum(o => o.NumberOfPallet).ToString();
        ltConfirmedLines.Text = lstDailyVolumeDetail.Where(vol => vol.GraphType == "1").Sum(o => o.NumberOfLines).ToString();
        ltConfirmedDeliveries.Text = lstDailyVolumeDetail.Where(vol => vol.GraphType == "1").Sum(o => o.NumberOfDeliveries).ToString();

        ltUnconfirmedPallets.Text = lstDailyVolumeDetail.Where(vol => vol.GraphType == "0").Sum(o => o.NumberOfPallet).ToString();
        ltUnconfirmedLines.Text = lstDailyVolumeDetail.Where(vol => vol.GraphType == "0").Sum(o => o.NumberOfLines).ToString();
        ltUnconfirmedDeliveries.Text = lstDailyVolumeDetail.Where(vol => vol.GraphType == "0").Sum(o => o.NumberOfDeliveries).ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string GetSummery(string SiteID, string Date) {
                
        string Volume = string.Empty;


        if (HttpContext.Current.Session["VolumeDetail"] == null) {
            List<APPBOK_BookingBE> lstDailyVolumeDetail = (List<APPBOK_BookingBE>)HttpContext.Current.Session["lstDailyVolumeDetail"];
            Volume += "[" + lstDailyVolumeDetail.Sum(o => o.NumberOfPallet).ToString();
            Volume += "," + lstDailyVolumeDetail.Sum(o => o.NumberOfLines).ToString();
            Volume += "," + lstDailyVolumeDetail.Sum(o => o.NumberOfDeliveries).ToString();

            Volume += "," + lstDailyVolumeDetail.Where(vol => vol.GraphType == "1").Sum(o => o.NumberOfPallet).ToString();
            Volume += "," + lstDailyVolumeDetail.Where(vol => vol.GraphType == "1").Sum(o => o.NumberOfLines).ToString();
            Volume += "," + lstDailyVolumeDetail.Where(vol => vol.GraphType == "1").Sum(o => o.NumberOfDeliveries).ToString();

            Volume += "," + lstDailyVolumeDetail.Where(vol => vol.GraphType == "0").Sum(o => o.NumberOfPallet).ToString();
            Volume += "," + lstDailyVolumeDetail.Where(vol => vol.GraphType == "0").Sum(o => o.NumberOfLines).ToString();
            Volume += "," + lstDailyVolumeDetail.Where(vol => vol.GraphType == "0").Sum(o => o.NumberOfDeliveries).ToString();
            Volume += "]";
            HttpContext.Current.Session["VolumeDetail"] = Volume;

        }
        else {
            Volume = Convert.ToString(HttpContext.Current.Session["VolumeDetail"]);
        }       

        return Volume;
    }


    private static void GetVolumeDetails(string SiteID, string Date) {

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(SiteID);
        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(Date);

        PreviousDate = Convert.ToString(Date);
        PreviousSiteID = SiteID;               
        List<APPBOK_BookingBE> lstDailyVolumeDetail = oAPPBOK_BookingBAL.GetDailyVolumeBAL(oAPPBOK_BookingBE);
        HttpContext.Current.Session["lstDailyVolumeDetail"] = lstDailyVolumeDetail;
    }


    [WebMethod(EnableSession = true)]
    public static string[] GetAllVolumeReportData(string GraphType, string VolumeType, string SiteID, string Date) {

        string[] VolDetails = new string[3];       
     
        if (HttpContext.Current.Session["lstDailyVolumeDetail"] == null || PreviousDate != Convert.ToString(Date) || PreviousSiteID != SiteID) {
            GetVolumeDetails(SiteID, Date);
            HttpContext.Current.Session["VolumeDetail"] = null;
        }

        List<APPBOK_BookingBE> lstDailyVolumeDetail = (List<APPBOK_BookingBE>)HttpContext.Current.Session["lstDailyVolumeDetail"];
        
        StringBuilder sb = new StringBuilder();
        sb.Append("[");

        StringBuilder sb2 = new StringBuilder();
        sb2.Append("[");

        var oDailyVolumeDetail = (dynamic)null; 

        if (GraphType == "Pallets") {
            oDailyVolumeDetail = lstDailyVolumeDetail.OrderBy(o => o.ScheduleDate).Select(c => new { Measure = c.NumberOfPallet, c.GraphType, SlotTime = c.SlotTime.SlotTime }).Distinct().ToList();
        }
        else if (GraphType == "Lines") {
            oDailyVolumeDetail = lstDailyVolumeDetail.OrderBy(o => o.ScheduleDate).Select(c => new { Measure = c.NumberOfLines, c.GraphType, SlotTime = c.SlotTime.SlotTime }).Distinct().ToList();
        }
        else if (GraphType == "Deliveries") {
            oDailyVolumeDetail = lstDailyVolumeDetail.OrderBy(o => o.ScheduleDate).Select(c => new { Measure = c.NumberOfDeliveries, c.GraphType, SlotTime = c.SlotTime.SlotTime }).Distinct().ToList();
        }


        for (int i = 0; i < oDailyVolumeDetail.Count; i++) {

            if (oDailyVolumeDetail[i].GraphType == "1") {
                sb.Append("{");
                sb.Append(string.Format("y :{0} , label: '{1}', color: '{2}'", Convert.ToString(oDailyVolumeDetail[i].Measure), oDailyVolumeDetail[i].SlotTime, ""));
                sb.Append("},");
            }
            else if (oDailyVolumeDetail[i].GraphType == "0") {
                sb2.Append("{");
                sb2.Append(string.Format("y :{0} , label: '{1}', color: '{2}'", Convert.ToString(oDailyVolumeDetail[i].Measure), oDailyVolumeDetail[i].SlotTime, ""));
                sb2.Append("},");
            }
        }        

        sb = sb.Remove(sb.Length - 1, 1);
        sb.Append("]");

        sb2 = sb2.Remove(sb2.Length - 1, 1);
        sb2.Append("]");             

        VolDetails[0] = sb.ToString();
        VolDetails[1] = sb2.ToString();


        string Volume = string.Empty;


        if (HttpContext.Current.Session["VolumeDetail"] == null) {
           
            Volume += "[" + lstDailyVolumeDetail.Sum(o => o.NumberOfPallet).ToString();
            Volume += "," + lstDailyVolumeDetail.Sum(o => o.NumberOfLines).ToString();
            Volume += "," + lstDailyVolumeDetail.Sum(o => o.NumberOfDeliveries).ToString();

            Volume += "," + lstDailyVolumeDetail.Where(vol => vol.GraphType == "1").Sum(o => o.NumberOfPallet).ToString();
            Volume += "," + lstDailyVolumeDetail.Where(vol => vol.GraphType == "1").Sum(o => o.NumberOfLines).ToString();
            Volume += "," + lstDailyVolumeDetail.Where(vol => vol.GraphType == "1").Sum(o => o.NumberOfDeliveries).ToString();

            Volume += "," + lstDailyVolumeDetail.Where(vol => vol.GraphType == "0").Sum(o => o.NumberOfPallet).ToString();
            Volume += "," + lstDailyVolumeDetail.Where(vol => vol.GraphType == "0").Sum(o => o.NumberOfLines).ToString();
            Volume += "," + lstDailyVolumeDetail.Where(vol => vol.GraphType == "0").Sum(o => o.NumberOfDeliveries).ToString();
            Volume += "]";
            HttpContext.Current.Session["VolumeDetail"] = Volume;

        }
        else {
            Volume = Convert.ToString(HttpContext.Current.Session["VolumeDetail"]);
        }

        VolDetails[2] = Volume;

        return VolDetails;
    }

    //[WebMethod(EnableSession = true)]
    //public static string GetVolumeReportDataConfORAll(string GraphType, string VolumeType, string SiteID, string Date) {

    //    string Volume = string.Empty;

    //    //if (VolumeType == "Confirmed" || VolumeType == "All") {

    //        if (HttpContext.Current.Session["lstDailyVolumeDetail"] == null || PreviousDate != Convert.ToString(Date) || PreviousSiteID != SiteID) {
    //            GetVolumeDetails(SiteID, Date);
    //            HttpContext.Current.Session["VolumeDetail"] = null;
    //        }           

    //        List<APPBOK_BookingBE> lstDailyVolumeDetail = (List<APPBOK_BookingBE>)HttpContext.Current.Session["lstDailyVolumeDetail"];

       
    //        lstDailyVolumeDetail = lstDailyVolumeDetail.Where(vol => vol.GraphType == "1").ToList();


    //        StringBuilder sb = new StringBuilder();
    //        sb.Append("[");

    //        var oDailyVolumeDetail = lstDailyVolumeDetail.OrderBy(o => o.ScheduleDate).Select(c => new { c.NumberOfPallet, c.NumberOfLines, c.NumberOfDeliveries, SlotTime = c.SlotTime.SlotTime }).Distinct().ToList();
    //        //var oDailyVolumeDetail = lstDailyVolumeDetail.OrderBy(o => o.SlotTime.SlotTime).Select(c => new { c.NumberOfPallet, c.NumberOfLines, SlotTime = c.SlotTime.SlotTime }).Distinct().ToList();

    //        if (GraphType == "Pallets") {

    //            for (int i = 0; i < oDailyVolumeDetail.Count; i++) {

    //                sb.Append("{");
    //                if (oDailyVolumeDetail[i].SlotTime != "NONTIME") {
    //                    sb.Append(string.Format("y :{0} , label: '{1}', color: '{2}'", Convert.ToString(oDailyVolumeDetail[i].NumberOfPallet), oDailyVolumeDetail[i].SlotTime, ""));
    //                }
    //                else {
    //                    sb.Append(string.Format("y :{0} , label: '{1}', color: '{2}', name : '{3}'", Convert.ToString(oDailyVolumeDetail[i].NumberOfPallet), oDailyVolumeDetail[i].SlotTime, "#FFCC00", "Non Time"));
    //                }
    //                sb.Append("},");
    //            }
    //        }
    //        else if (GraphType == "Lines") {

    //            for (int i = 0; i < oDailyVolumeDetail.Count; i++) {

    //                sb.Append("{");
    //                if (oDailyVolumeDetail[i].SlotTime != "NONTIME") {
    //                    sb.Append(string.Format("y :{0} , label: '{1}', color: '{2}'", Convert.ToString(oDailyVolumeDetail[i].NumberOfLines), oDailyVolumeDetail[i].SlotTime, ""));
    //                }
    //                else {
    //                    sb.Append(string.Format("y :{0} , label: '{1}', color: '{2}', name : '{3}'", Convert.ToString(oDailyVolumeDetail[i].NumberOfLines), oDailyVolumeDetail[i].SlotTime, "#FFCC00", "Non Time"));
    //                }
    //                sb.Append("},");
    //            }
    //        }
    //        else if (GraphType == "Deliveries") {

    //            for (int i = 0; i < oDailyVolumeDetail.Count; i++) {

    //                sb.Append("{");
    //                if (oDailyVolumeDetail[i].SlotTime != "NONTIME") {
    //                    sb.Append(string.Format("y :{0} , label: '{1}', color: '{2}'", Convert.ToString(oDailyVolumeDetail[i].NumberOfDeliveries), oDailyVolumeDetail[i].SlotTime, ""));
    //                }
    //                else {
    //                    sb.Append(string.Format("y :{0} , label: '{1}', color: '{2}', name : '{3}'", Convert.ToString(oDailyVolumeDetail[i].NumberOfDeliveries), oDailyVolumeDetail[i].SlotTime, "#FFCC00", "Non Time"));
    //                }
    //                sb.Append("},");
    //            }
    //        }
            

    //        sb = sb.Remove(sb.Length - 1, 1);
    //        sb.Append("]");


    //        Volume = sb.ToString();
    //    //}      

    //    return Volume;
    //}

    //[WebMethod(EnableSession = true)]
    //public static string GetVolumeReportDataUnConf(string GraphType, string VolumeType, string SiteID, string Date) {

    //    string Volume = string.Empty;

    //    //if (VolumeType == "UnConfirmed" || VolumeType == "All") {

    //        if (HttpContext.Current.Session["lstDailyVolumeDetail"] == null || PreviousDate != Convert.ToString(Date) || PreviousSiteID != SiteID) {
    //            GetVolumeDetails(SiteID, Date);
    //        }

    //        List<APPBOK_BookingBE> lstDailyVolumeDetail = (List<APPBOK_BookingBE>)HttpContext.Current.Session["lstDailyVolumeDetail"];


    //        lstDailyVolumeDetail = lstDailyVolumeDetail.Where(vol => vol.GraphType == "0").ToList();


    //        StringBuilder sb = new StringBuilder();
    //        sb.Append("[");

    //        //var oDailyVolumeDetail = lstDailyVolumeDetail.OrderBy(o => o.SlotTime.SlotTime).Select(c => new { c.NumberOfPallet, c.NumberOfLines, SlotTime = c.SlotTime.SlotTime }).Distinct().ToList();
    //        var oDailyVolumeDetail = lstDailyVolumeDetail.OrderBy(o => o.ScheduleDate).Select(c => new { c.NumberOfPallet, c.NumberOfLines, c.NumberOfDeliveries, SlotTime = c.SlotTime.SlotTime }).Distinct().ToList();
                   

    //        if (GraphType == "Pallets") {

    //            for (int i = 0; i < oDailyVolumeDetail.Count; i++) {

    //                sb.Append("{");
    //                if (oDailyVolumeDetail[i].SlotTime != "NONTIME") {
    //                    sb.Append(string.Format("y :{0} , label: '{1}', color: '{2}'", Convert.ToString(oDailyVolumeDetail[i].NumberOfPallet), oDailyVolumeDetail[i].SlotTime, ""));
    //                }
    //                else {
    //                    sb.Append(string.Format("y :{0} , label: '{1}', color: '{2}', name : '{3}'", Convert.ToString(oDailyVolumeDetail[i].NumberOfPallet), oDailyVolumeDetail[i].SlotTime, "#FFCC00", "Non Time"));
    //                }
    //                sb.Append("},");
    //            }
    //        }
    //        else if (GraphType == "Lines") {

    //            for (int i = 0; i < oDailyVolumeDetail.Count; i++) {

    //                sb.Append("{");
    //                if (oDailyVolumeDetail[i].SlotTime != "NONTIME") {
    //                    sb.Append(string.Format("y :{0} , label: '{1}', color: '{2}'", Convert.ToString(oDailyVolumeDetail[i].NumberOfLines), oDailyVolumeDetail[i].SlotTime, ""));
    //                }
    //                else {
    //                    sb.Append(string.Format("y :{0} , label: '{1}', color: '{2}', name : '{3}'", Convert.ToString(oDailyVolumeDetail[i].NumberOfLines), oDailyVolumeDetail[i].SlotTime, "#FFCC00", "Non Time"));
    //                }
    //                sb.Append("},");
    //            }
    //        }
    //        else if (GraphType == "Deliveries") {

    //            for (int i = 0; i < oDailyVolumeDetail.Count; i++) {

    //                sb.Append("{");
    //                if (oDailyVolumeDetail[i].SlotTime != "NONTIME") {
    //                    sb.Append(string.Format("y :{0} , label: '{1}', color: '{2}'", Convert.ToString(oDailyVolumeDetail[i].NumberOfDeliveries), oDailyVolumeDetail[i].SlotTime, ""));
    //                }
    //                else {
    //                    sb.Append(string.Format("y :{0} , label: '{1}', color: '{2}', name : '{3}'", Convert.ToString(oDailyVolumeDetail[i].NumberOfDeliveries), oDailyVolumeDetail[i].SlotTime, "#FFCC00", "Non Time"));
    //                }
    //                sb.Append("},");
    //            }
    //        }

    //        sb = sb.Remove(sb.Length - 1, 1);
    //        sb.Append("]");

    //        Volume = sb.ToString();
    //    //}

    //    return Volume;
    //}
    protected void grdBind_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            grdBind.Columns[4].HeaderText = "Pallets";
            grdBind.Columns[5].HeaderText = "Lines";
            grdBind.Columns[6].HeaderText = "Deliveries";
            grdBind.Columns[7].HeaderText = "Pallets";
            grdBind.Columns[8].HeaderText = "Lines";
            grdBind.Columns[9].HeaderText = "Deliveries";
        }

    }
    protected void grdBind_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            TableCell HeaderCell = new TableCell();
            HeaderCell.Text = "Total";
            HeaderCell.ColumnSpan = 4;
            HeaderCell.CssClass = "Gridheader";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);
          

            HeaderCell = new TableCell();
            HeaderCell.Text = "Confirmed";
            HeaderCell.ColumnSpan = 3;
            HeaderCell.CssClass = "Gridheader";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);
            grdBind.Controls[0].Controls.AddAt(0, HeaderGridRow);

            HeaderCell = new TableCell();
            HeaderCell.Text = "Unconfirmed";
            HeaderCell.CssClass = "Gridheader";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.ColumnSpan = 3;
            HeaderGridRow.Cells.Add(HeaderCell);           

        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        ExportToExcel((DataTable)ViewState["DataTable"], "DailyVolumeReport-Overview");
    }
    public void ExportToExcel(DataTable dtExport, string fileName)
    {
        HttpContext context = HttpContext.Current;
        string date = DateTime.Now.ToString("ddMMyyyy");
        string time = DateTime.Now.ToString("HH:mm");
        context.Response.ContentType = "text/vnd.ms-excel";
        context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}_{1}_{2}", fileName, date, time + ".xls"));
        if (dtExport.Rows.Count > 0)
        {
            // context.Response.Write("\t");
            for (int i = 0; i < dtExport.Columns.Count; i++)
            {
                switch (dtExport.Columns[i].ColumnName.ToString())
                {
                    case "SlotTime":
                        context.Response.Write("");
                        context.Response.Write("\t");
                        break;
                    case "Pallets":
                        context.Response.Write("TotalPallets");
                        context.Response.Write("\t");
                        break;
                    case "Lines":
                        context.Response.Write("TotalLines");
                        context.Response.Write("\t");
                        break;
                    case "Deliveries":
                        context.Response.Write("TotalDeliveries");
                        context.Response.Write("\t");
                        break;

                    case "Pallets1":
                        context.Response.Write("ConfirmedPallets");
                        context.Response.Write("\t");
                        break;
                    case "Lines1":
                        context.Response.Write("ConfirmedLines");
                        context.Response.Write("\t");
                        break;
                    case "Deliveries1":
                        context.Response.Write("ConfirmedDeliveries");
                        context.Response.Write("\t");
                        break;

                    case "Pallets2":
                        context.Response.Write("UnConfirmedPallets");
                        context.Response.Write("\t");
                        break;
                    case "Lines2":
                        context.Response.Write("UnConfirmedLines");
                        context.Response.Write("\t");
                        break;
                    case "Deliveries2":
                        context.Response.Write("UnConfirmedDeliveries");
                        context.Response.Write("\t");
                        break;
                }

            }
        }

        context.Response.Write(Environment.NewLine);

        //Write data
        foreach (DataRow row in dtExport.Rows)
        {

            for (int i = 0; i < dtExport.Columns.Count; i++)
            {
                context.Response.Write(row.ItemArray[i].ToString());
                context.Response.Write("\t");
            }
            context.Response.Write(Environment.NewLine);
        }
        context.Response.End();
    }
}
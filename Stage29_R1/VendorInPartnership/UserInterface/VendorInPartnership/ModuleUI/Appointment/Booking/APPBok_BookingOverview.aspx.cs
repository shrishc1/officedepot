﻿using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Languages.Languages;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Security;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;

public partial class APPBok_BookingOverview : CommonPage
{
    private DataTable dsSchedulingPrintOut = new DataTable();
    protected string strShowDeliveryMessage = WebCommon.getGlobalResourceValue("ShowDeliveryMessage");
    protected string siteRequired = WebCommon.getGlobalResourceValue("SiteRequired");
    protected string VendorPalletCheckRequired = WebCommon.getGlobalResourceValue("VendorPalletCheckRequired");
    protected string VendorPalletCheckDone = WebCommon.getGlobalResourceValue("VendorPalletCheckDone");
    protected string IsVendorPalletCheckingForISPM15 = WebCommon.getGlobalResourceValue("IsVendorPalletCheckingForISPM15");
    protected string EnableHazardouesItemPrompt = WebCommon.getGlobalResourceValue("HazardousItemCheckRequired");
    protected string EnableHazardouesItemPromptChecking = WebCommon.getGlobalResourceValue("EnableHazardouesItemPromptChecking");
    private string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    private string templatePathName = @"/EmailTemplates/Appointment/";
    private string strCountryId = string.Empty;
    private bool ImgWindowAlternateViewClicked = false;

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    public string GridViewSortExp
    {
        get
        {
            return Convert.ToString(ViewState["GridViewSortExp"]);
        }
        set { ViewState["GridViewSortExp"] = value; }
    }

    public string DeletionFixedSlotDate
    {
        get
        {
            return Convert.ToString(ViewState["DeletionFixedSlotDate"]);
        }
        set { ViewState["DeletionFixedSlotDate"] = value; }
    }

    public bool IsGridSort
    {
        get
        {
            if (ViewState["IsGridSort"] == null)
                ViewState["IsGridSort"] = false;
            return (bool)ViewState["IsGridSort"];
        }
        set { ViewState["IsGridSort"] = value; }
    }

    public DateTime dtSeletedDate
    {
        get
        {
            return (DateTime)ViewState["dtSeletedDate"];
        }
        set
        {
            ViewState["dtSeletedDate"] = value;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ucSeacrhVendor1.CurrentPage = this;

        ddlSiteCarrier.CurrentPage = this;
        ddlSiteCarrier.IsAllRequired = true;
        ddlSiteCarrier.SchedulingContact = true;

        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;

        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = true;
        ddlSite.IsFromBookingOverview = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ddlSiteCarrier.innerControlddlSite.AutoPostBack = true;
        ddlSite.ddlSite_DropChange += new EventHandler(ddlSite_ddlSite_DropChange);
        ddlSite.innerControlddlSite.AutoPostBack = true;

        if (GetQueryStringValue("frompage") == "recdash")
        {
            txtDate.innerControltxtDate.Value = GetQueryStringValue("Scheduledate");
            ddlSite.innerControlddlSite.SelectedValue = GetQueryStringValue("SiteID");
        }

        if (!Page.IsPostBack)
        {
            Session["BookingCreated"] = false;
            Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
            lblModuleText.Text = "Appointment Scheduling - Booking";
            string Role = string.Empty;

            if (Session["Role"] != null && Session["Role"].ToString() != "")
            {
                hdnRole.Value = Session["Role"].ToString().ToLower();
                Role = Session["Role"].ToString().Trim().ToLower();
            }
            //Not linked to radio button selection, Office Depot only
            if (Role == "vendor" || Role == "carrier")
            {
                divVendorBottons.Visible = true;
                divSearchButtons.Attributes.Add("style", "display:none;");
                divODButtons.Visible = false;
                btnVendorEditBooking.Enabled = false;
                btnDeleteBooking.Enabled = false;
                btnConfirmFixedSlot.Enabled = false;
                btnDeleteFixedSlot.Enabled = false;

                //Stage6 V2 point 23
                btnBookingHistory_1.Enabled = false;
                lblbookingsmadebyacarrierforyourordersText.Text = WebCommon.getGlobalResourceValue("bookingsmadebyacarrierforyourorders").Replace("##ST##", "<span style='color:#339966;font-weight:bold;'>").Replace("##ET##", "</span>");
            }
            else
            {
                ddlSiteCarrier.Visible = false;
                divVendorBottons.Visible = false;
                divSearchButtons.Visible = true;
                divODButtons.Visible = true;
                btnUnexpectedDelivery.Enabled = true;
                btnVolumeDetails.Enabled = true;

                GetScreen();
            }

            //Stage6 V2 point 23
            if (Role == "carrier")
            {
                lblbookingsmadebyacarrierforyourordersText.Visible = false;
            }

            txtDate.innerControltxtDate.Value = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy"));
            bindPlanner();

            EnableButtons();
        }

        btnExportToExcel1.CurrentPage = this;
        btnExportToExcel1.IsHideHidden = true;
        btnExportToExcel1.GridViewControl = ucGridViewExcel;
        btnExportToExcel1.FileName = "BookingOverview";

        btnExportNew.CurrentPage = this;
        btnExportNew.IsHideHidden = true;
        btnExportNew.GridViewControl = ucGridViewExcel;
        btnExportNew.FileName = "BookingOverview";
        pnlWindow.Visible = false;

        pager1.PageSize = 10;
        pager1.GenerateGoToSection = false;
        pager1.GeneratePagerInfoSection = false;
    }

    private void ddlSite_ddlSite_DropChange(object sender, EventArgs e)
    {
        MAS_SiteBAL objMasSitE = new MAS_SiteBAL();
        if (pnlVolumeDetails_1.Visible == true)
        {
            if (objMasSitE.GetSiteTimeSlotWindowFlagBAL(Convert.ToString(ddlSite.innerControlddlSite.SelectedItem.Value)) == "W")
            {
                if (ImgWindowAlternateViewClicked == true || imgWindowAlternateView.Visible == false)
                {
                    GenerateWindowVolumeDetails();
                    tdStatus1.Visible = false;
                    tdStatus2.Visible = false;
                    tdStatus3.Visible = false;
                    trAlternateHide.Visible = false;
                    tdVendor1.Visible = false;
                    tdVendor2.Visible = false;
                    tdVendor3.Visible = false;
                    tdBookingRef1.Visible = false;
                    tdBookingRef2.Visible = false;
                    tdBookingRef3.Visible = false;
                    tdIcon.Visible = false;
                    pnlLinks.Visible = true;
                    imgWindowAlternateView.Visible = true;
                    lnkGotoWindow.Visible = true;
                    lnkGoto.Visible = false;
                    lnkLegends.Style.Add("display", "none");
                    lnkCurrentBookingDetails.Visible = false;
                    lnkNonTimeBookingDetails.Visible = false;
                    lnkNonStandardWindowBookingDetails.Visible = true;
                }
            }
            else
            {
                GenerateVolumeDetails();
                tdStatus1.Visible = false;
                tdStatus2.Visible = false;
                tdStatus3.Visible = false;
                trAlternateHide.Visible = false;
                tdVendor1.Visible = false;
                tdVendor2.Visible = false;
                tdVendor3.Visible = false;
                tdBookingRef1.Visible = false;
                tdBookingRef2.Visible = false;
                tdBookingRef3.Visible = false;
                tdIcon.Visible = false;
                pnlLinks.Visible = true;
                imgWindowAlternateView.Visible = true;
                lnkGotoWindow.Visible = true;
                lnkGoto.Visible = false;
                lnkLegends.Style.Add("display", "block");
                lnkCurrentBookingDetails.Visible = true;
                lnkNonTimeBookingDetails.Visible = true;
                lnkNonStandardWindowBookingDetails.Visible = false;
            }
        }
        else
        {
            if (objMasSitE.GetSiteTimeSlotWindowFlagBAL(Convert.ToString(ddlSite.innerControlddlSite.SelectedItem.Value)) == "W")
            {
                lnkCurrentBookingDetails.Visible = false;
            }
            else
            {
                lnkCurrentBookingDetails.Visible = true;
            }
        }
    }

    private void EnableButtons()
    {
        string Role = string.Empty;
        if (Session["Role"] != null && Session["Role"].ToString() != "")
        {
            Role = Session["Role"].ToString().Trim().ToLower();
        }

        if (Role != "vendor" && Role != "carrier")
        {
            btnConfirmAFixedSlot.Enabled = false;
            btnEditABooking.Enabled = false;
            btnDeleteABooking.Enabled = false;
            btnQualityChecked.Enabled = false;
            btnDelArrived.Enabled = false;
            btnDelivaryUnload.Enabled = false;
            btnBookingHistory.Enabled = false;
            btnNoShow.Enabled = false;
            btnConfirmPOArrival.Enabled = false;

            btnConfirmAFixedSlot.CssClass = "button-readonly";
            btnEditABooking.CssClass = "button-readonly";
            btnDeleteABooking.CssClass = "button-readonly";
            btnQualityChecked.CssClass = "button-readonly";
            btnDelArrived.CssClass = "button-readonly";
            btnDelivaryUnload.CssClass = "button-readonly";
            btnBookingHistory.CssClass = "button-readonly";
            btnNoShow.CssClass = "button-readonly";
            btnConfirmPOArrival.CssClass = "button-readonly";
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "css", "<script>setReadonlyClass();</script>", false);
    }

    private void GetScreen()
    {
        SCT_UserScreenBE oSCT_UserScreenBE = new SCT_UserScreenBE();
        SCT_UserScreenBAL oSCT_UserScreenBAL = new SCT_UserScreenBAL();

        oSCT_UserScreenBE.Screen = new SCT_ScreenBE();
        oSCT_UserScreenBE.Action = "GetUserScreen";
        oSCT_UserScreenBE.Screen.ModuleID = 1;
        oSCT_UserScreenBE.UserID = Convert.ToInt32(Session["UserID"]);

        List<SCT_UserScreenBE> lstUserScreen = new List<SCT_UserScreenBE>();

        lstUserScreen = oSCT_UserScreenBAL.GetUserScreenBAL(oSCT_UserScreenBE);
        oSCT_UserScreenBAL = null;

        btnBookingHistory.Attributes.Add("style", "");
        btnVolumeDetails.Attributes.Add("style", "");

        if (lstUserScreen != null && lstUserScreen.Count > 0)
        {
            for (int iCount = 0; iCount < lstUserScreen.Count; iCount++)
            {
                if (lstUserScreen[iCount].Screen.ScreenName == "ManageBooking")
                {
                    btnAddBooking.Attributes.Add("style", "");
                    btnConfirmAFixedSlot.Attributes.Add("style", "");
                    btnEditABooking.Attributes.Add("style", "");
                    btnDeleteABooking.Attributes.Add("style", "");
                    btnUnexpectedDelivery.Attributes.Add("style", "");
                }
                if (lstUserScreen[iCount].Screen.ScreenName == "ManageDelivery")
                {
                    btnDelArrived.Attributes.Add("style", "");
                    btnDelivaryUnload.Attributes.Add("style", "");
                    btnNoShow.Attributes.Add("style", "");
                    btnQualityChecked.Attributes.Add("style", "");
                    btnConfirmPOArrival.Attributes.Add("style", "");
                }
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string Role = Session["Role"].ToString().Trim().ToLower();

        if (!IsPostBack)
        {
            //---------------Start Stage 14 R2 point 28 -------------------//
            if (!string.IsNullOrWhiteSpace(GetQueryStringValue("SVT")))
            {
                txtDate.innerControltxtDate.Value = GetQueryStringValue("ReportDate");
                ddlSite.innerControlddlSite.SelectedValue = GetQueryStringValue("SiteID");
                if (!string.IsNullOrWhiteSpace(GetQueryStringValue("BookingStatus")))
                {
                    ddlStatus.SelectedValue = "10";
                }
                Search();
            }//---------------End Stage 14 R2 point 28 -------------------//
            else
            {
                if (Session["BookingCritreria"] != null)
                {
                    Hashtable htBookingCriteria = (Hashtable)Session["BookingCritreria"];
                    txtDate.innerControltxtDate.Value = htBookingCriteria.ContainsKey("SchedulingDate") ? htBookingCriteria["SchedulingDate"].ToString() : "";
                    ddlStockPalnner.SelectedValue = (htBookingCriteria.ContainsKey("StockPlanner") && htBookingCriteria["StockPlanner"] != null) ? htBookingCriteria["StockPlanner"].ToString() : "";
                    ddlSite.innerControlddlSite.SelectedValue = htBookingCriteria.ContainsKey("site") ? htBookingCriteria["site"].ToString() : "";
                    ddlSite.PreCountryID = htBookingCriteria.ContainsKey("PreCountryID") ? Convert.ToInt32(htBookingCriteria["PreCountryID"].ToString()) : 0;

                    ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                    ddlSiteCarrier.innerControlddlSite.SelectedValue = htBookingCriteria.ContainsKey("site") ? htBookingCriteria["site"].ToString() : "";
                    ddlSiteCarrier.PreCountryID = htBookingCriteria.ContainsKey("PreCountryID") ? Convert.ToInt32(htBookingCriteria["PreCountryID"].ToString()) : 0;

                    txtBookingRef.Text = htBookingCriteria.ContainsKey("BookingRef") ? htBookingCriteria["BookingRef"].ToString() : "";
                    ucSeacrhVendor1.VendorNo = htBookingCriteria.ContainsKey("VendorID") ? htBookingCriteria["VendorID"].ToString() : "";
                    ucSeacrhVendor1.innerControlVendorNo.Text = htBookingCriteria.ContainsKey("VendorNo") ? htBookingCriteria["VendorNo"].ToString() : "";
                    ucSeacrhVendor1.innerControlVendorName.Text = htBookingCriteria.ContainsKey("VendorName") ? htBookingCriteria["VendorName"].ToString() : "";

                    if (Role == "vendor" || Role == "carrier")
                    {
                        if (Session["VendorCarrierSiteId"] != null)
                        {
                            if (ddlSiteCarrier.innerControlddlSite.Items.Count > 0)
                                ddlSiteCarrier.innerControlddlSite.SelectedIndex = ddlSiteCarrier.innerControlddlSite.Items.IndexOf(
                                    ddlSiteCarrier.innerControlddlSite.Items.FindByValue(Convert.ToString(Session["VendorCarrierSiteId"])));
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "css", "<script>setReadonlyClass();</script>", false);
                    }

                    txtPurchaseNumber.Text = htBookingCriteria.ContainsKey("PO") ? htBookingCriteria["PO"].ToString() : string.Empty;

                    ddlStatus.SelectedValue = (htBookingCriteria.ContainsKey("Status") && htBookingCriteria["Status"] != null) ? htBookingCriteria["Status"].ToString() : "";
                    if (htBookingCriteria.ContainsKey("ViewType") && htBookingCriteria["ViewType"] != null)
                    {
                        if (htBookingCriteria["ViewType"].ToString() == "ByBooking")
                        {
                            ddlDisplayBy.SelectedIndex = ddlDisplayBy.Items.IndexOf(ddlDisplayBy.Items.FindByValue("BYDELIVERY"));
                        }
                        else if (htBookingCriteria["ViewType"].ToString() == "ByVendor")
                        {
                            ddlDisplayBy.SelectedIndex = ddlDisplayBy.Items.IndexOf(ddlDisplayBy.Items.FindByValue("BYVENDOR"));
                        }
                        else if (htBookingCriteria["ViewType"].ToString() == "ByPurchaseOrder")
                        {
                            ddlDisplayBy.SelectedIndex = ddlDisplayBy.Items.IndexOf(ddlDisplayBy.Items.FindByValue("BYPO"));
                        }
                    }

                    if (GetQueryStringValue("frompage") == "recdash")
                    {
                        txtDate.innerControltxtDate.Value = GetQueryStringValue("Scheduledate");
                        ddlSite.innerControlddlSite.SelectedValue = GetQueryStringValue("SiteID");
                    }

                    BindBookings();
                }
                else if (Session["BookingCritreria"] == null)
                {
                    if (Session["SiteCountryID"] != null)
                    {
                        if (Role == "vendor" || Role == "carrier")
                        {
                            if (ddlSiteCarrier.innerControlddlSite.Items.Count > 0)
                                ddlSiteCarrier.PreCountryID = Convert.ToInt32(Session["SiteCountryID"]);
                        }
                        else
                        {
                            if (ddlSite.innerControlddlSite.Items.Count > 0)
                                ddlSite.PreCountryID = Convert.ToInt32(Session["SiteCountryID"]);
                        }
                    }

                    if (Session["SiteID"] != null)
                    {
                        if (Role == "vendor" || Role == "carrier")
                        {
                            ddlSiteCarrier.innerControlddlSite.SelectedIndex = ddlSiteCarrier.innerControlddlSite.Items.IndexOf(
                                ddlSiteCarrier.innerControlddlSite.Items.FindByValue(Convert.ToString(Session["SiteID"])));
                        }
                        else
                        {
                            ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(
                                ddlSite.innerControlddlSite.Items.FindByValue(Convert.ToString(Session["SiteID"])));
                        }
                    }
                }

                if (!string.IsNullOrWhiteSpace(GetQueryStringValue("RefusedDel")))
                {
                    if (ddlSite.innerControlddlSite.Items.Count > 0) { ddlSite.innerControlddlSite.SelectedIndex = 0; }
                    if (ddlStatus.Items.Count > 0)
                    {
                        ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText("Refused"));
                    }
                    if (ddlStockPalnner.Items.Count > 0)
                    {
                        ddlStockPalnner.SelectedIndex =
                            ddlStockPalnner.Items.IndexOf(ddlStockPalnner.Items.FindByValue(Convert.ToString(Session["UserID"])));
                    }
                    BindBookings();
                }

                if (!string.IsNullOrWhiteSpace(GetQueryStringValue("DashboardDeliverySchedule")))
                {
                    if (ddlSite.innerControlddlSite.Items.Count > 0) { ddlSite.innerControlddlSite.SelectedIndex = 0; }

                    if (ddlStockPalnner.Items.Count > 0)
                    {
                        ddlStockPalnner.SelectedIndex =
                            ddlStockPalnner.Items.IndexOf(ddlStockPalnner.Items.FindByValue(Convert.ToString(Session["UserID"])));
                    }
                    BindBookings();
                }

                if (GetQueryStringValue("frompage") == "recdash")
                {
                    txtDate.innerControltxtDate.Value = GetQueryStringValue("Scheduledate");
                    ddlSite.innerControlddlSite.SelectedValue = GetQueryStringValue("SiteID");
                    BindBookings();
                }

                if ((Role == "vendor" || Role == "carrier"))
                {
                    if (Session["BookingCritreria"] == null)
                    {
                        BindBookings();
                    }
                }
            }
        }

        //Emergency fix - temp as per Anthony's mail on 11 jan 2016

        if ((Role == "vendor" || Role == "carrier"))
        {
            if (!Page.IsPostBack)
            {
                CheckSchedulingClosedown();
            }
        }
        //----------------------------------------------------------

        MAS_SiteBAL objMasSitE = new MAS_SiteBAL();
        if (objMasSitE.GetSiteTimeSlotWindowFlagBAL(Convert.ToString(ddlSite.innerControlddlSite.SelectedItem.Value)) == "W")
        {
            imgAlternateView.Visible = false;
            imgWindowAlternateView.Visible = true;
        }
        else
        {
            pnlWindow.Visible = false;
            imgAlternateView.Visible = true;
            imgWindowAlternateView.Visible = false;
        }
    }

    public override void SiteSelectedIndexChanged()
    {
        string Role = Session["Role"].ToString().Trim().ToLower();
        if (Role == "vendor" || Role == "carrier")
        {
            Session["VendorCarrierSiteId"] = ddlSiteCarrier.innerControlddlSite.SelectedValue;

            hdnCurrentDate.Value = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString()).ToString();
            hdnSelectedDate.Value = txtDate.GetDate.ToString();

            Hashtable htBookingCritreria = new Hashtable();
            htBookingCritreria.Add("SchedulingDate", txtDate.GetDate.ToString("dd/MM/yyyy"));
            htBookingCritreria.Add("site", Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value));
            htBookingCritreria.Add("StockPlanner", ddlStockPalnner != null && !string.IsNullOrEmpty(ddlStockPalnner.SelectedValue) ? Convert.ToInt32(ddlStockPalnner.SelectedValue) : (int?)null);
            htBookingCritreria.Add("Status", ddlStatus != null && !string.IsNullOrEmpty(ddlStatus.SelectedValue) ? Convert.ToInt32(ddlStatus.SelectedValue) : (int?)null);
            htBookingCritreria.Add("ViewType", ddlDisplayBy.SelectedItem.Value == "BYDELIVERY" ? "ByBooking" : ddlDisplayBy.SelectedItem.Value == "BYVENDOR" ? "ByVendor" : ddlDisplayBy.SelectedItem.Value == "BYPO" ? "ByPurchaseOrder" : "");
            //Point 16 phase 8 ..for All Booking/Delivery search criteria should be held
            htBookingCritreria.Add("BookingRef", !string.IsNullOrEmpty(txtBookingRef.Text) ? txtBookingRef.Text.Trim() : string.Empty);
            htBookingCritreria.Add("VendorID", ucSeacrhVendor1.VendorNo.Trim() != string.Empty ? ucSeacrhVendor1.VendorNo.Trim() : string.Empty);
            htBookingCritreria.Add("VendorNo", ucSeacrhVendor1.innerControlVendorNo.Text != string.Empty ? ucSeacrhVendor1.innerControlVendorNo.Text.Trim() : string.Empty);
            htBookingCritreria.Add("VendorName", ucSeacrhVendor1.innerControlVendorName.Text != string.Empty ? ucSeacrhVendor1.innerControlVendorName.Text.Trim() : string.Empty);

            //END
            BindBookings();
            htBookingCritreria.Add("PreCountryID", ddlSiteCarrier.innerControlddlSite != null && !string.IsNullOrEmpty(ddlSiteCarrier.innerControlddlSite.SelectedValue) ? ddlSiteCarrier.PreCountryID : (int?)null);
            htBookingCritreria.Add("PageIndex", 0);
            htBookingCritreria.Add("PO", txtPurchaseNumber.Text);
            Session["BookingCritreria"] = htBookingCritreria;
            Session["SiteID"] = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            //Phase 15 R2 point 21
            Session["CurrentSiteId"] = ddlSiteCarrier.innerControlddlSite.SelectedValue;

            ucGridView1.PageIndex = 0;
            CheckSchedulingClosedown();
            //-----------------------------------------------------------

            ScriptManager.RegisterStartupScript(this, this.GetType(), "css", "<script>setReadonlyClass();</script>", false);
        }
        else
        {
            ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            Session["SiteID"] = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            //Phase 15 R2 point 21
            Session["CurrentSiteId"] = ddlSite.innerControlddlSite.SelectedValue;
        }
        txtDate.LoadDates();
    }

    private void bindPlanner()
    {
        SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
        oNewSCT_UserBE.Action = "GetStockPalnnerByUserId";
        oNewSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
        lstUser = oSCT_UserBAL.GetPlannerBAL(oNewSCT_UserBE);
        if (lstUser.Count > 0 && lstUser != null)
        {
            FillControls.FillDropDown(ref ddlStockPalnner, lstUser, "FullName", "UserID", "All");
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        MAS_SiteBAL objMasSitE = new MAS_SiteBAL();
        if (pnlVolumeDetails_1.Visible == true)
        {
            if (objMasSitE.GetSiteTimeSlotWindowFlagBAL(Convert.ToString(ddlSite.innerControlddlSite.SelectedItem.Value)) == "W")
            {
                if (ImgWindowAlternateViewClicked == true || imgWindowAlternateView.Visible == false)
                {
                    GenerateVolumeDetails();
                    pnlWindow.Visible = true;
                    GenerateWindowAlternateSlot();
                    divPrint.Visible = false;
                    pnlVolumeDetails_1.Visible = true;
                    tblAlternate.Visible = false;
                    tdStatus1.Visible = false;
                    tdStatus2.Visible = false;
                    tdStatus3.Visible = false;
                    trAlternateHide.Visible = false;
                    tdVendor1.Visible = false;
                    tdVendor2.Visible = false;
                    tdVendor3.Visible = false;
                    tdBookingRef1.Visible = false;
                    tdBookingRef2.Visible = false;
                    tdBookingRef3.Visible = false;
                    tdIcon.Visible = false;
                    pnlLinks.Visible = true;
                    imgWindowAlternateView.Visible = true;
                    lnkGotoWindow.Visible = true;
                    lnkGoto.Visible = false;
                    lnkLegends.Style.Add("display", "none");
                    lnkCurrentBookingDetails.Visible = false;
                    lnkNonStandardWindowBookingDetails.Visible = true;
                }
            }
            else
            {
                GenerateVolumeDetails();
                pnlWindow.Visible = false;
                GenerateAlternateSlot(false);
                tableDiv_GeneralNew.Visible = true;
                divPrint.Visible = false;
                pnlVolumeDetails_1.Visible = true;
                tblAlternate.Visible = true;
                tdStatus1.Visible = false;
                tdStatus2.Visible = false;
                tdStatus3.Visible = false;
                trAlternateHide.Visible = false;
                tdVendor1.Visible = false;
                tdVendor2.Visible = false;
                tdVendor3.Visible = false;
                tdBookingRef1.Visible = false;
                tdBookingRef2.Visible = false;
                tdBookingRef3.Visible = false;
                tdIcon.Visible = false;
                pnlLinks.Visible = true;
                imgWindowAlternateView.Visible = true;
                lnkGotoWindow.Visible = true;
                lnkGoto.Visible = false;
                lnkLegends.Style.Add("display", "block");
                lnkCurrentBookingDetails.Visible = true;
                lnkNonStandardWindowBookingDetails.Visible = false;
            }
        }
        if (imgAlternateView.Visible == true || imgWindowAlternateView.Visible == true)
        {
            this.Search();
        }
    }

    private void Search()
    {
        hdnCurrentDate.Value = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString()).ToString();
        DateTime selectedDate = txtDate.GetDate;
        hdnSelectedDate.Value = selectedDate.ToString();

        Hashtable htBookingCritreria = new Hashtable();
        htBookingCritreria.Add("SchedulingDate", selectedDate.ToString("dd/MM/yyyy"));
        htBookingCritreria.Add("site", Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value));
        htBookingCritreria.Add("StockPlanner", ddlStockPalnner != null && !string.IsNullOrEmpty(ddlStockPalnner.SelectedValue) ? Convert.ToInt32(ddlStockPalnner.SelectedValue) : (int?)null);
        htBookingCritreria.Add("Status", ddlStatus != null && !string.IsNullOrEmpty(ddlStatus.SelectedValue) ? Convert.ToInt32(ddlStatus.SelectedValue) : (int?)null);
        htBookingCritreria.Add("ViewType", ddlDisplayBy.SelectedItem.Value == "BYDELIVERY" ? "ByBooking" : ddlDisplayBy.SelectedItem.Value == "BYVENDOR" ? "ByVendor" : ddlDisplayBy.SelectedItem.Value == "BYPO" ? "ByPurchaseOrder" : "");
        //Point 16 phase 8 ..for All Booking/Delivery search criteria should be held
        htBookingCritreria.Add("BookingRef", !string.IsNullOrEmpty(txtBookingRef.Text) ? txtBookingRef.Text.Trim() : string.Empty);
        htBookingCritreria.Add("VendorID", ucSeacrhVendor1.VendorNo.Trim() != string.Empty ? ucSeacrhVendor1.VendorNo.Trim() : string.Empty);
        htBookingCritreria.Add("VendorNo", ucSeacrhVendor1.innerControlVendorNo.Text != string.Empty ? ucSeacrhVendor1.innerControlVendorNo.Text.Trim() : string.Empty);
        htBookingCritreria.Add("VendorName", ucSeacrhVendor1.innerControlVendorName.Text != string.Empty ? ucSeacrhVendor1.innerControlVendorName.Text.Trim() : string.Empty);
        //END

        htBookingCritreria.Add("PreCountryID", ddlSite.innerControlddlSite != null && !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? ddlSite.PreCountryID : (int?)null);
        htBookingCritreria.Add("PageIndex", 0);
        htBookingCritreria.Add("PO", txtPurchaseNumber.Text);
        Session["BookingCritreria"] = htBookingCritreria;
        Session["SiteID"] = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        //Phase 15 R2 point 21
        Session["CurrentSiteId"] = ddlSite.innerControlddlSite.SelectedValue;

        ucGridView1.PageIndex = 0;

        BindBookings();

        ScriptManager.RegisterStartupScript(this, this.GetType(), "css", "<script>setReadonlyClass();</script>", false);
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindBookings(currnetPageIndx - 1);
    }
    protected void BindBookings(int PageIndex = 0)
    {
        try
        {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            DateTime selectedDate = txtDate.GetDate;

            // setting current date and selected date value.
            hdnCurrentDate.Value = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString()).ToString();
            hdnSelectedDate.Value = selectedDate.ToString();

            oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
            oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
            oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
            oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

            string Role = Session["Role"].ToString().Trim().ToLower();
            if (Role != "vendor" && Role != "carrier")
            {
                oAPPBOK_BookingBE.Action = "GetAllBookings";
                if (!string.IsNullOrEmpty(ddlStockPalnner.SelectedValue))
                    oAPPBOK_BookingBE.StockPlannerID = Convert.ToInt32(ddlStockPalnner.SelectedValue);
                oAPPBOK_BookingBE.FixedSlot.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                oAPPBOK_BookingBE.FixedSlot.SiteIDs = ddlSite.UserSiteIDs;
                oAPPBOK_BookingBE.ScheduleDate = selectedDate;
                oAPPBOK_BookingBE.SelectedScheduleDate = selectedDate;

                //-------------Sprint 3a Start-----------------//
                if (ddlDisplayBy.SelectedItem.Value == "BYDELIVERY")
                    oAPPBOK_BookingBE.ViewType = "ByBooking";
                else if (ddlDisplayBy.SelectedItem.Value == "BYVENDOR")
                    oAPPBOK_BookingBE.ViewType = "ByVendor";
                else if (ddlDisplayBy.SelectedItem.Value == "BYPO")
                    oAPPBOK_BookingBE.ViewType = "ByPurchaseOrder";
                else
                    oAPPBOK_BookingBE.ViewType = "ByBooking";

                //-------------Sprint 3a End-----------------//

                //-------------Sprint 8 point 14 Start-----------------//
                oAPPBOK_BookingBE.BookingRef = txtBookingRef.Text.Trim();
                //-------------Sprint 8 point 14 End-----------------//
            }
            else
            {
                oAPPBOK_BookingBE.Action = "ShowVendorBooking";
                if (Role == "vendor")
                    oAPPBOK_BookingBE.SupplierType = "V";
                else if (Role == "carrier")
                    oAPPBOK_BookingBE.SupplierType = "C";

                oAPPBOK_BookingBE.FromDate = DateTime.Today;
                oAPPBOK_BookingBE.ToDate = DateTime.Today.AddDays(41);
                oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
                oAPPBOK_BookingBE.SelectedScheduleDate = DateTime.Today;
                oAPPBOK_BookingBE.FixedSlot = new MASSIT_FixedSlotBE();
                oAPPBOK_BookingBE.FixedSlot.SiteID = Convert.ToInt32(ddlSiteCarrier.innerControlddlSite.SelectedItem.Value);
                oAPPBOK_BookingBE.PageIndex = PageIndex;
                for (int index = 0; index < ddlSiteCarrier.innerControlddlSite.Items.Count; index++)
                {
                    if (Convert.ToInt32(ddlSiteCarrier.innerControlddlSite.Items[index].Value) > 0)
                    {
                        if (string.IsNullOrEmpty(oAPPBOK_BookingBE.FixedSlot.SiteIDs))
                            oAPPBOK_BookingBE.FixedSlot.SiteIDs = ddlSiteCarrier.innerControlddlSite.Items[index].Value;
                        else
                            oAPPBOK_BookingBE.FixedSlot.SiteIDs = string.Format("{0},{1}", oAPPBOK_BookingBE.FixedSlot.SiteIDs,
                                ddlSiteCarrier.innerControlddlSite.Items[index].Value);
                    }
                }
            }

            DataTable dtBookings = new DataTable();

            if (!string.IsNullOrEmpty(txtCATCode.Text))
            {
                oAPPBOK_BookingBE.CatCode = txtCATCode.Text;
            }
            if (!string.IsNullOrEmpty(txtSku.Text))
            {
                oAPPBOK_BookingBE.SKU = txtSku.Text;
            }

            if (GetQueryStringValue("frompage") == "recdash")
            {
                oAPPBOK_BookingBE.IssuesFound = GetQueryStringValue("BookingsWithIssues") == "BookingsWithIssues";
            }



            dtBookings = oAPPBOK_BookingBAL.GetAllBookingDetailBAL(oAPPBOK_BookingBE);

            if ((Role == "vendor" || Role == "carrier") && dtBookings.Rows.Count > 0)
            {
                pager1.CurrentIndex = PageIndex - 1;
                pager1.ItemCount = Convert.ToDouble(dtBookings.Rows[0]["TotalRecords"].ToString());
                pager1.Visible = true;
            }
            else
            {
                pager1.Visible = false;
            }


            if (Role == "vendor" || Role == "carrier")
            {
                dtBookings.AsEnumerable().Where(r => (r.Field<DateTime>("DeliveryDate")).Date < DateTime.Now.Date).ToList().ForEach(row => row.Delete());
                dtBookings.AcceptChanges();
            }

            DataColumn myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "ActWeekDay";
            dtBookings.Columns.Add(myDataColumn);

            if (Role != "vendor" && Role != "carrier")
            {
                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "LogicalDate";
                dtBookings.Columns.Add(myDataColumn);
            }

            if (Role == "vendor" || Role == "carrier")
            {
                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "LogicalDeliveryDate";
                dtBookings.Columns.Add(myDataColumn);
            }

            DataColumn dcIndex = new DataColumn();
            dcIndex.ColumnName = "AutoID";
            dcIndex.DataType = System.Type.GetType("System.Int32");
            dtBookings.Columns.Add(dcIndex);
            DataView lstBookings = new DataView(dtBookings.DefaultView.Table);
            int iWeekDayNo = 0;
            //string strWeekDaySel = selectedDate.DayOfWeek.ToString().ToUpper();
            switch (selectedDate.DayOfWeek.ToString().ToUpper())
            {
                case "SUNDAY":
                    iWeekDayNo = 1;
                    break;
                case "MONDAY":
                    iWeekDayNo = 2;
                    break;
                case "TUESDAY":
                    iWeekDayNo = 3;
                    break;
                case "WEDNESDAY":
                    iWeekDayNo = 4;
                    break;
                case "THURSDAY":
                    iWeekDayNo = 5;
                    break;
                case "FRIDAY":
                    iWeekDayNo = 6;
                    break;
                case "SATURDAY":
                    iWeekDayNo = 7;
                    break;
            }

            foreach (DataRowView rowView in lstBookings)
            {
                DataRow row = rowView.Row;
                row["ActWeekDay"] = row["WeekDay"].ToString().Trim() == iWeekDayNo.ToString() || (row["WeekDay"].ToString().Trim() == "8")
                    ? iWeekDayNo.ToString()
                    : row["WeekDay"];
            }

            List<MASSIT_WeekSetupBE> lstWeekSetup = new List<MASSIT_WeekSetupBE>();

            if (Role != "vendor" && Role != "carrier")
            {
                //-----------------Start Collect Basic Information --------------------------//
                DateTime dtSelectedDate = selectedDate;
                string WeekDay = dtSelectedDate.ToString("dddd");

                MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
                MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

                oMASSIT_WeekSetupBE.Action = "GetSpecificWeekShowAll";
                oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
                oMASSIT_WeekSetupBE.ScheduleDate = dtSelectedDate;

                lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

                // To fixed - to restrict to show fixed slots on week setup not defined
                if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
                {
                    if (lstBookings != null && lstBookings.Count > 0)
                    {
                        lstBookings.RowFilter = "WeekDay <> '8'";
                        lstBookings = lstBookings.ToTable().DefaultView;
                    }
                }
                //---------------------------------------------------------------//

                if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
                {
                    if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
                    {
                        //Filter Booking not in site timing range//
                        int iCount = 0;
                        foreach (DataRowView rowView in lstBookings)
                        {
                            DataRow dr = rowView.Row;

                            if (Convert.ToString(dr["BookingType"]).Trim() != "Non Timed Delivery")
                            {
                                /*
                                 * Date 25 April 2013 - 10:29 AM
                                 * This condition set because in case of Window booking [dr["ExpectedDeliveryTime"]]
                                 * column will be null.
                                */
                                if (!string.IsNullOrEmpty(Convert.ToString(dr["ExpectedDeliveryTime"])))
                                {
                                    string part1 = dr["ExpectedDeliveryTime"].ToString().Split('-')[0].Trim();

                                    DateTime dt = new DateTime(1900, 1, 1, Convert.ToInt32(part1.Split(':')[0]), Convert.ToInt32(part1.Split(':')[1]), 0);
                                    DateTime dt2 = new DateTime(1900, 1, 2, Convert.ToInt32(part1.Split(':')[0]), Convert.ToInt32(part1.Split(':')[1]), 0);

                                    if (dt > lstWeekSetup[0].EndTime && Convert.ToString(dr["BookingType"]).Trim() != "Unscheduled")
                                    {
                                        string strWeekDay = selectedDate.DayOfWeek.ToString().ToUpper();
                                        if (strWeekDay == "SUNDAY")
                                        {
                                            if (1 <= Convert.ToInt32(dr["Weekday"]))
                                                dr["Weekday"] = "10";
                                        }
                                        else if (strWeekDay == "MONDAY")
                                        {
                                            if (2 <= Convert.ToInt32(dr["Weekday"]))
                                                dr["Weekday"] = "10";
                                        }
                                        else if (strWeekDay == "TUESDAY")
                                        {
                                            if (3 <= Convert.ToInt32(dr["Weekday"]))
                                                dr["Weekday"] = "10";
                                        }
                                        else if (strWeekDay == "WEDNESDAY")
                                        {
                                            if (4 <= Convert.ToInt32(dr["Weekday"]))
                                                dr["Weekday"] = "10";
                                        }
                                        else if (strWeekDay == "THURSDAY")
                                        {
                                            if (5 <= Convert.ToInt32(dr["Weekday"]))
                                                dr["Weekday"] = "10";
                                        }
                                        else if (strWeekDay == "FRIDAY")
                                        {
                                            if (6 <= Convert.ToInt32(dr["Weekday"]))
                                                dr["Weekday"] = "10";
                                        }
                                        else if (strWeekDay == "SATURDAY")
                                        {
                                            if (7 <= Convert.ToInt32(dr["Weekday"]))
                                                dr["Weekday"] = "10";
                                        }
                                    }
                                }
                            }
                            iCount++;
                        }

                        if (lstBookings != null && lstBookings.Count > 0)
                        {
                            lstBookings.RowFilter = "Weekday <> '10'";

                            lstBookings = lstBookings.ToTable().DefaultView;
                        }
                        //-------------------------------------//

                        //Filter fixed slot not in site timing range//
                        oAPPBOK_BookingBE.ScheduleDate = selectedDate.AddDays(-1);

                        DataTable lstBookingsStartDay = oAPPBOK_BookingBAL.GetAllBookingDetailBAL(oAPPBOK_BookingBE);

                        myDataColumn = new DataColumn();
                        myDataColumn.ColumnName = "ActWeekDay";
                        lstBookingsStartDay.Columns.Add(myDataColumn);

                        DataTable dttemp = lstBookingsStartDay.Copy();
                        if (lstBookingsStartDay != null && lstBookingsStartDay.Rows.Count > 0)
                        {
                            iCount = 0;
                            foreach (DataRow dr in dttemp.Rows)
                            {
                                if (Convert.ToString(dr["BookingType"]).Trim() != "Non Timed Delivery")
                                {
                                    string part = dr["ExpectedDeliveryTime"].ToString().Split('-')[0].Trim();
                                    if (!string.IsNullOrEmpty(part.Trim()))
                                    {
                                        DateTime dt = new DateTime(1900, 1, 1, Convert.ToInt32(part.Split(':')[0]), Convert.ToInt32(part.Split(':')[1]), 0);
                                        DateTime dt2 = new DateTime(1900, 1, 2, Convert.ToInt32(part.Split(':')[0]), Convert.ToInt32(part.Split(':')[1]), 0);

                                        lstBookingsStartDay.Rows[iCount]["ActWeekDay"] = (iWeekDayNo - 1).ToString();

                                        if (dt < lstWeekSetup[0].StartTime || Convert.ToInt32(lstBookingsStartDay.Rows[iCount]["Weekday"]) < (iWeekDayNo - 1))
                                        {
                                            lstBookingsStartDay.Rows[iCount]["Weekday"] = "10";
                                        }
                                        else
                                        {
                                            string strWeekDay = selectedDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
                                            if (strWeekDay == "SUNDAY")
                                            {
                                                lstBookingsStartDay.Rows[iCount]["Weekday"] = "1";
                                            }
                                            else if (strWeekDay == "MONDAY")
                                            {
                                                lstBookingsStartDay.Rows[iCount]["Weekday"] = "2";
                                            }
                                            else if (strWeekDay == "TUESDAY")
                                            {
                                                lstBookingsStartDay.Rows[iCount]["Weekday"] = "3";
                                            }
                                            else if (strWeekDay == "WEDNESDAY")
                                            {
                                                lstBookingsStartDay.Rows[iCount]["Weekday"] = "4";
                                            }
                                            else if (strWeekDay == "THURSDAY")
                                            {
                                                lstBookingsStartDay.Rows[iCount]["Weekday"] = "5";
                                            }
                                            else if (strWeekDay == "FRIDAY")
                                            {
                                                lstBookingsStartDay.Rows[iCount]["Weekday"] = "6";
                                            }
                                            else if (strWeekDay == "SATURDAY")
                                            {
                                                lstBookingsStartDay.Rows[iCount]["Weekday"] = "7";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        lstBookingsStartDay.Rows[iCount]["Weekday"] = "10";
                                    }
                                }
                                iCount++;
                            }
                        }
                        if (lstBookingsStartDay != null && lstBookingsStartDay.Rows.Count > 0)
                        {
                            DataView dv = lstBookingsStartDay.DefaultView;
                            dv.RowFilter = "Weekday<>'10'";

                            if (lstBookings != null && lstBookings.Count > 0)
                            {
                                lstBookings.Table.Merge(dv.ToTable());
                                lstBookings.Sort = "ActWeekDay asc,OrderBY,DoorNumber,EndOrderBY";
                            }
                        }
                    }
                }
            }

            if (Role != "vendor" && Role != "carrier")
            {
                //-------------Sprint 3a Start-----------------//
                if (ddlStatus.SelectedIndex > 0)
                {
                    switch (ddlStatus.SelectedItem.Value)
                    {
                        case "1":
                            lstBookings.RowFilter = "BookingStatus = 'Confirmed Slot' OR " +
                                               "BookingStatus = 'Confirm Window'";

                            break;

                        case "2":
                            lstBookings.RowFilter = "BookingStatus = 'Delivery Arrived'";
                            break;

                        case "3":
                            lstBookings.RowFilter = "BookingStatus = 'Delivery Unloaded' OR " +
                                            "BookingStatus = 'Partially Refused'";
                            break;

                        case "4":
                            lstBookings.RowFilter = "BookingStatus = 'Partially Refused' OR " +
                                            "BookingStatus = 'Refused Delivery' OR " +
                                            "BookingStatus = 'QC for PR' ";
                            break;

                        case "5":
                            lstBookings.RowFilter = "BookingStatus = 'QC for PR' OR " +
                                            "BookingStatus = 'Quality Checked'";
                            break;

                        case "6":
                            lstBookings.RowFilter = "BookingStatus = 'Delivery Arrived' OR " +
                                            "BookingStatus = 'Delivery Unloaded' OR " +
                                            "BookingStatus = 'QC for PR' OR " +
                                            "BookingStatus = 'Quality Checked' OR " +
                                            "BookingStatus = 'Partially Refused'";
                            break;

                        case "7":
                            lstBookings.RowFilter = "BookingStatus = 'No Show' OR " +
                                            "BookingStatus = 'Refused Delivery' OR " +
                                             "BookingStatus = 'QC for PR' OR " +
                                            "BookingStatus = 'Partially Refused'";
                            break;

                        case "8":
                            lstBookings.RowFilter = "BookingType = 'Provisional'";
                            break;

                        case "9":
                            lstBookings.RowFilter = "BookingStatus = 'Deleted'";
                            break;

                        case "10":
                            lstBookings.RowFilter = "BookingStatus = 'Unconfirmed Fixed' OR " +
                                               "BookingStatus = 'Reserved Window'  OR " +
                                               "BookingStatus = 'Open Fixed'";
                            break;

                        default:
                            break;
                    }

                    lstBookings = lstBookings.ToTable().DefaultView;
                }

                //---Filter By PO------//
                if (txtPurchaseNumber.Text.Trim() != string.Empty && lstBookings.Count > 0)
                {
                    lstBookings.RowFilter = "POs LIKE '%" + "," + txtPurchaseNumber.Text.Trim() + "," + "%'";
                    lstBookings = lstBookings.ToTable().DefaultView;
                }
                //---------------------//
                //---Filter By Vendor------//
                if (ucSeacrhVendor1.VendorNo.Trim() != string.Empty && lstBookings.Count > 0)
                {
                    lstBookings.RowFilter = "VendorIDs LIKE '%" + "," + ucSeacrhVendor1.VendorNo.Trim() + "," + "%'";

                    lstBookings = lstBookings.ToTable().DefaultView;
                }
                //---------------------//

                //-------------Sprint 3a End-----------------//
            }

            if (!string.IsNullOrWhiteSpace(GetQueryStringValue("RefusedDel")) && !IsPostBack)
            {
                lstBookings.RowFilter = "BookingStatus = 'Refused Delivery' OR " +
                                       "BookingStatus = 'Partially Refused'";

                lstBookings = lstBookings.ToTable().DefaultView;
            }

            if (Role == "vendor" || Role == "carrier")
            {
                foreach (DataRowView rowView in lstBookings)
                {
                    DataRow dr = rowView.Row;

                    if (dr["ISCrossDay"].ToString() == "True")
                    {
                        if (dr["Weekday"] != DBNull.Value)
                        {
                            DateTime dtDeliveryDate = Convert.ToDateTime(dr["DeliveryDate"].ToString());

                            string strWeekDay = string.Format("{0:dddd}", dr["DeliveryDate"]).ToUpper();
                            switch (strWeekDay)
                            {
                                case "SUNDAY":
                                    if (1 != Convert.ToInt32(dr["Weekday"]))
                                        dr["DeliveryDate"] = dtDeliveryDate.AddDays(-1);
                                    break;
                                case "MONDAY":
                                    if (2 != Convert.ToInt32(dr["Weekday"]))
                                        dr["DeliveryDate"] = dtDeliveryDate.AddDays(-1);
                                    break;
                                case "TUESDAY":
                                    if (3 != Convert.ToInt32(dr["Weekday"]))
                                        dr["DeliveryDate"] = dtDeliveryDate.AddDays(-1);
                                    break;
                                case "WEDNESDAY":
                                    if (4 != Convert.ToInt32(dr["Weekday"]))
                                        dr["DeliveryDate"] = dtDeliveryDate.AddDays(-1);
                                    break;
                                case "THURSDAY":
                                    if (5 != Convert.ToInt32(dr["Weekday"]))
                                        dr["DeliveryDate"] = dtDeliveryDate.AddDays(-1);
                                    break;
                                case "FRIDAY":
                                    if (6 != Convert.ToInt32(dr["Weekday"]))
                                        dr["DeliveryDate"] = dtDeliveryDate.AddDays(-1);
                                    break;
                                case "SATURDAY":
                                    if (7 != Convert.ToInt32(dr["Weekday"]))
                                        dr["DeliveryDate"] = dtDeliveryDate.AddDays(-1);
                                    break;
                            }
                        }
                    }
                }
            }

            //Filte out Non Timed Delivery//
            DataTable lstBookingsBonTime = null;
            if (Role != "vendor" && Role != "carrier")
            {
                if (lstBookings != null && lstBookings.Count > 0)
                {
                    lstBookingsBonTime = lstBookings.ToTable();
                    DataView dv = lstBookingsBonTime.DefaultView;
                    dv.RowFilter = "BookingType = 'Non Timed Delivery' AND DeliveryDate='" + selectedDate + "'";
                    lstBookingsBonTime = dv.ToTable();
                }

                if (lstBookings != null && lstBookings.Count > 0)
                {
                    lstBookings.RowFilter = "BookingType <> 'Non Timed Delivery' OR BookingType is null";
                    lstBookings = lstBookings.ToTable().DefaultView;
                }
            }

            if (lstBookings == null || lstBookings.Count == 0)
            {
                if (lstBookingsBonTime != null && lstBookingsBonTime.Rows.Count > 0)
                {
                    lstBookings = lstBookingsBonTime.DefaultView;
                    lstBookingsBonTime = null;
                }
            }
            //----------------------------//

            if (lstBookings != null && lstBookings.Count > 0)
            {
                lblVendorNoBooking.Visible = false;
                if (Role == "vendor" || Role == "carrier")
                {
                    lstBookings.Sort = "DeliveryDate,OrderBY,EndOrderBY";
                    gvVendor.DataSource = lstBookings;
                    gvVendor.DataBind();
                    ViewState["lstSites"] = lstBookings.ToTable();
                    ucGridView1.Visible = false;
                    gvVendor.Visible = true;

                    if (string.IsNullOrEmpty(GridViewSortExp))
                        GridViewSortExp = "DeliveryDate,OrderBY,EndOrderBY";
                }
                else
                {
                    if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
                    {
                        if (lstWeekSetup[0].StartWeekday == lstWeekSetup[0].EndWeekday)
                        {
                            lstBookings.Sort = "OrderBY,EndOrderBY,PKID";
                            if (string.IsNullOrEmpty(GridViewSortExp))
                                GridViewSortExp = "OrderBY,EndOrderBY,PKID";
                        }
                        else
                        {
                            lstBookings.Sort = "ActWeekDay,OrderBY,EndOrderBY";
                            if (string.IsNullOrEmpty(GridViewSortExp))
                                GridViewSortExp = "ActWeekDay,OrderBY,EndOrderBY";
                        }
                    }

                    if (lstBookingsBonTime != null && lstBookingsBonTime.Rows.Count > 0)
                    {
                        DataView dv = lstBookingsBonTime.DefaultView;
                        dv.RowFilter = "Weekday = " + iWeekDayNo.ToString();

                        if (lstBookings != null && lstBookings.Count > 0)
                            lstBookings.Table.Merge(dv.ToTable());
                        else if (dv != null && dv.Table.Rows.Count > 0)
                        {
                            lstBookings = dv;
                        }
                    }

                    //-------------------------------------//

                    int iAutoId = 0;
                    //-------------------------------------//

                    foreach (DataRowView rowView in lstBookings)
                    {
                        DataRow dr = rowView.Row;
                        //For Sorting
                        if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
                        {
                            if (lstWeekSetup[0].StartWeekday == lstWeekSetup[0].EndWeekday)
                            {
                                dr["AutoID"] = dr["OrderBY"].ToString();
                            }
                            else
                            {
                                iAutoId++;
                                dr["AutoID"] = iAutoId.ToString();
                            }
                        }
                        //-----------//

                        if (dr["ActWeekDay"] != DBNull.Value)
                        {
                            if (dr["ActWeekDay"].ToString() != iWeekDayNo.ToString())
                            {
                                dr["LogicalDate"] = selectedDate.AddDays(-1).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                dr["LogicalDate"] = selectedDate.ToString("dd/MM/yyyy");
                            }
                        }
                    }

                    ViewState["lstSites"] = lstBookings.ToTable();

                    if (lstBookings != null && lstBookings.Count > 0)
                    {
                        dtSeletedDate = selectedDate;
                        ucGridView1.DataSource = lstBookings;
                        ucGridView1.DataBind();
                        skuCatCode.Visible = true;
                        ucGridView1.Visible = true;
                        gvVendor.Visible = false;
                    }
                    else
                    {
                        skuCatCode.Visible = false;
                        ucGridView1.Visible = false;
                        gvVendor.Visible = false;
                        lblVendorNoBooking.Visible = true;
                    }
                }
            }
            else
            {
                ucGridView1.Visible = false;
                gvVendor.Visible = false;
                lblVendorNoBooking.Visible = true;
            }

            divPrint.Visible = true;
            pnlVolumeDetails_1.Visible = false;
            pnlWindow.Visible = false;

            pnlLinks.Visible = false;

            tableDiv_GeneralNew.Visible = false;

            MAS_SiteBAL objMasSitE = new MAS_SiteBAL();
            if (objMasSitE.GetSiteTimeSlotWindowFlagBAL(Convert.ToString(ddlSite.innerControlddlSite.SelectedItem.Value)) == "W")
            {
                imgAlternateView.Visible = false;
                imgWindowAlternateView.Visible = true;
            }
            else
            {
                pnlWindow.Visible = false;
                imgAlternateView.Visible = true;
                imgWindowAlternateView.Visible = false;
            }
        }
        catch
        {
        }
    }

    public override bool PreExportToExcel()
    {
        BindBookings();
        DataTable lstBookings = (DataTable)ViewState["lstSites"];
        ucGridViewExcel.DataSource = lstBookings;
        ucGridViewExcel.DataBind();
        return true;
    }

    public bool PreExportToExcel_1()
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

        string Role = Session["Role"].ToString().Trim().ToLower();
        if (Role != "vendor" && Role != "carrier")
        {
            oAPPBOK_BookingBE.Action = "ShowAll";
            if (!string.IsNullOrEmpty(ddlStockPalnner.SelectedValue))
                oAPPBOK_BookingBE.StockPlannerID = Convert.ToInt32(ddlStockPalnner.SelectedValue);
            oAPPBOK_BookingBE.FixedSlot.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = txtDate.GetDate;
            oAPPBOK_BookingBE.SelectedScheduleDate = txtDate.GetDate;
        }
        else
        {
            oAPPBOK_BookingBE.Action = "ShowVendorBooking";
            if (Role == "vendor")
                oAPPBOK_BookingBE.SupplierType = "V";
            else if (Role == "carrier")
                oAPPBOK_BookingBE.SupplierType = "C";

            oAPPBOK_BookingBE.FromDate = DateTime.Today;
            oAPPBOK_BookingBE.ToDate = DateTime.Today.AddDays(13);
            oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
            oAPPBOK_BookingBE.SelectedScheduleDate = DateTime.Today;
        }
        DataTable lstBookings = lstBookings = oAPPBOK_BookingBAL.GetAllBookingDetailBAL(oAPPBOK_BookingBE);
        if (Role == "vendor" || Role == "carrier")
        {
            lstBookings.AsEnumerable().Where(r => (r.Field<DateTime>("DeliveryDate")).Date < DateTime.Now.Date).ToList().ForEach(row => row.Delete());
            lstBookings.AcceptChanges();
        }

        DataColumn myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "ActWeekDay";
        lstBookings.Columns.Add(myDataColumn);

        int iWeekDayNo = 0;
        string strWeekDaySel = txtDate.GetDate.DayOfWeek.ToString().ToUpper();
        if (strWeekDaySel == "SUNDAY")
        {
            iWeekDayNo = 1;
        }
        else if (strWeekDaySel == "MONDAY")
        {
            iWeekDayNo = 2;
        }
        else if (strWeekDaySel == "TUESDAY")
        {
            iWeekDayNo = 3;
        }
        else if (strWeekDaySel == "WEDNESDAY")
        {
            iWeekDayNo = 4;
        }
        else if (strWeekDaySel == "THURSDAY")
        {
            iWeekDayNo = 5;
        }
        else if (strWeekDaySel == "FRIDAY")
        {
            iWeekDayNo = 6;
        }
        else if (strWeekDaySel == "SATURDAY")
        {
            iWeekDayNo = 7;
        }

        foreach (DataRow row in lstBookings.Rows)
        {
            if (row["WeekDay"].ToString().Trim() == iWeekDayNo.ToString() || row["WeekDay"].ToString().Trim() == "8")
                row["ActWeekDay"] = iWeekDayNo.ToString();
            else
                row["ActWeekDay"] = row["WeekDay"];
        }

        //-----------------Start Collect Basic Information --------------------------//
        DateTime dtSelectedDate = txtDate.GetDate;
        string WeekDay = dtSelectedDate.ToString("dddd");

        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        oMASSIT_WeekSetupBE.Action = "GetSpecificWeekShowAll";
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
        oMASSIT_WeekSetupBE.ScheduleDate = dtSelectedDate;

        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        if (Role != "vendor" && Role != "carrier")
        {
            if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
            {
                if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
                {
                    //Filter Booking not in site timing range//
                    int iCount = 0;
                    foreach (DataRow dr in lstBookings.Rows)
                    {
                        if (Convert.ToString(dr["BookingType"]).Trim() != "Non Timed Delivery")
                        {
                            string part1 = dr["ExpectedDeliveryTime"].ToString().Split('-')[0].Trim();

                            DateTime dt = new DateTime(1900, 1, 1, Convert.ToInt32(part1.Split(':')[0]), Convert.ToInt32(part1.Split(':')[1]), 0);
                            DateTime dt2 = new DateTime(1900, 1, 2, Convert.ToInt32(part1.Split(':')[0]), Convert.ToInt32(part1.Split(':')[1]), 0);

                            if (dt > lstWeekSetup[0].EndTime)
                            {
                                string strWeekDay = txtDate.GetDate.DayOfWeek.ToString().ToUpper();
                                if (strWeekDay == "SUNDAY")
                                {
                                    if (1 <= Convert.ToInt32(dr["Weekday"]))
                                        lstBookings.Rows[iCount]["Weekday"] = "10";
                                }
                                else if (strWeekDay == "MONDAY")
                                {
                                    if (2 <= Convert.ToInt32(dr["Weekday"]))
                                        lstBookings.Rows[iCount]["Weekday"] = "10";
                                }
                                else if (strWeekDay == "TUESDAY")
                                {
                                    if (3 <= Convert.ToInt32(dr["Weekday"]))
                                        lstBookings.Rows[iCount]["Weekday"] = "10";
                                }
                                else if (strWeekDay == "WEDNESDAY")
                                {
                                    if (4 <= Convert.ToInt32(dr["Weekday"]))
                                        lstBookings.Rows[iCount]["Weekday"] = "10";
                                }
                                else if (strWeekDay == "THURSDAY")
                                {
                                    if (5 <= Convert.ToInt32(dr["Weekday"]))
                                        lstBookings.Rows[iCount]["Weekday"] = "10";
                                }
                                else if (strWeekDay == "FRIDAY")
                                {
                                    if (6 <= Convert.ToInt32(dr["Weekday"]))
                                        lstBookings.Rows[iCount]["Weekday"] = "10";
                                }
                                else if (strWeekDay == "SATURDAY")
                                {
                                    if (7 <= Convert.ToInt32(dr["Weekday"]))
                                        lstBookings.Rows[iCount]["Weekday"] = "10";
                                }
                            }
                        }
                        iCount++;
                    }

                    if (lstBookings != null && lstBookings.Rows.Count > 0)
                    {
                        DataView dv = lstBookings.DefaultView;
                        dv.RowFilter = "Weekday <> '10'";
                        lstBookings = dv.ToTable();
                    }
                    //-------------------------------------//

                    //Filter fixed slot not in site timing range//
                    oAPPBOK_BookingBE.ScheduleDate = txtDate.GetDate.AddDays(-1);
                    DataTable lstBookingsStartDay = oAPPBOK_BookingBAL.GetAllBookingDetailBAL(oAPPBOK_BookingBE);

                    myDataColumn = new DataColumn();
                    myDataColumn.ColumnName = "ActWeekDay";
                    lstBookingsStartDay.Columns.Add(myDataColumn);

                    DataTable dttemp = lstBookingsStartDay.Copy();
                    if (lstBookingsStartDay != null && lstBookingsStartDay.Rows.Count > 0)
                    {
                        iCount = 0;
                        foreach (DataRow dr in dttemp.Rows)
                        {
                            if (Convert.ToString(dr["BookingType"]).Trim() != "Non Timed Delivery")
                            {
                                string part = dr["ExpectedDeliveryTime"].ToString().Split('-')[0].Trim();
                                DateTime dt = new DateTime(1900, 1, 1, Convert.ToInt32(part.Split(':')[0]), Convert.ToInt32(part.Split(':')[1]), 0);
                                DateTime dt2 = new DateTime(1900, 1, 2, Convert.ToInt32(part.Split(':')[0]), Convert.ToInt32(part.Split(':')[1]), 0);

                                lstBookingsStartDay.Rows[iCount]["ActWeekDay"] = (iWeekDayNo - 1).ToString();

                                if (dt < lstWeekSetup[0].StartTime || Convert.ToInt32(lstBookingsStartDay.Rows[iCount]["Weekday"]) < (iWeekDayNo - 1))
                                {
                                    lstBookingsStartDay.Rows[iCount]["Weekday"] = "10";
                                }
                                else
                                {
                                    string strWeekDay = txtDate.GetDate.AddDays(-1).DayOfWeek.ToString().ToUpper();
                                    if (strWeekDay == "SUNDAY")
                                    {
                                        lstBookingsStartDay.Rows[iCount]["Weekday"] = "1";
                                    }
                                    else if (strWeekDay == "MONDAY")
                                    {
                                        lstBookingsStartDay.Rows[iCount]["Weekday"] = "2";
                                    }
                                    else if (strWeekDay == "TUESDAY")
                                    {
                                        lstBookingsStartDay.Rows[iCount]["Weekday"] = "3";
                                    }
                                    else if (strWeekDay == "WEDNESDAY")
                                    {
                                        lstBookingsStartDay.Rows[iCount]["Weekday"] = "4";
                                    }
                                    else if (strWeekDay == "THURSDAY")
                                    {
                                        lstBookingsStartDay.Rows[iCount]["Weekday"] = "5";
                                    }
                                    else if (strWeekDay == "FRIDAY")
                                    {
                                        lstBookingsStartDay.Rows[iCount]["Weekday"] = "6";
                                    }
                                    else if (strWeekDay == "SATURDAY")
                                    {
                                        lstBookingsStartDay.Rows[iCount]["Weekday"] = "7";
                                    }
                                }
                            }
                            iCount++;
                        }
                    }
                    if (lstBookingsStartDay != null && lstBookingsStartDay.Rows.Count > 0)
                    {
                        DataView dv = lstBookingsStartDay.DefaultView;
                        dv.RowFilter = "Weekday<>'10'";

                        if (lstBookings != null && lstBookings.Rows.Count > 0)
                            lstBookings.Merge(dv.ToTable());
                        else
                            lstBookings = dv.ToTable();
                        dv = lstBookings.DefaultView;
                        dv.Sort = "ActWeekDay asc,OrderBY,DoorNumber";
                        lstBookings = dv.ToTable();
                    }
                }
            }
        }

        //Filte out Non Timed Delivery//

        DataTable lstBookingsBonTime = null;
        if (lstBookings != null && lstBookings.Rows.Count > 0)
        {
            DataView dv = lstBookings.DefaultView;
            dv.RowFilter = "BookingType = 'Non Timed Delivery'";
            lstBookingsBonTime = dv.ToTable();
        }

        if (lstBookings != null && lstBookings.Rows.Count > 0)
        {
            DataView dv = lstBookings.DefaultView;
            dv.RowFilter = "BookingType <> 'Non Timed Delivery' OR BookingType is null";
            lstBookings = dv.ToTable();
        }

        if (lstBookingsBonTime != null && lstBookingsBonTime.Rows.Count > 0)
        {
            DataView dv = lstBookingsBonTime.DefaultView;
            dv.RowFilter = "Weekday = " + iWeekDayNo.ToString();
            if (lstBookings != null && lstBookings.Rows.Count > 0)
                lstBookings.Merge(dv.ToTable());
        }
        //----------------------------//

        if (lstBookings != null && lstBookings.Rows.Count > 0)
        {
            lblVendorNoBooking.Visible = false;
            if (Role == "vendor" || Role == "carrier")
            {
                gvVendor.DataSource = lstBookings;
                gvVendor.DataBind();
            }
            else
            {
                if (lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
                {
                    if (lstWeekSetup[0].StartWeekday == lstWeekSetup[0].EndWeekday)
                    {
                        lstBookings.DefaultView.Sort = "OrderBY";
                    }
                }

                ucGridViewExcel.DataSource = lstBookings;
                ucGridViewExcel.DataBind();
            }
        }
        else
        {
        }

        return true;
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<APPBOK_BookingBE>.SortList((List<APPBOK_BookingBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    //--Stage 3 Point 4----//
    private bool CheckSiteClosure(DateTime? sDate)
    {
        bool isSiteOpen = true;
        if (txtDate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
        {
            MASSIT_HolidayBE oMASSIT_HolidayBE = new MASSIT_HolidayBE();
            MASSIT_HolidayBAL oMASSIT_HolidayBAL = new MASSIT_HolidayBAL();

            oMASSIT_HolidayBE.Action = "ShowAll";
            oMASSIT_HolidayBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();

            string Role = Session["Role"].ToString().Trim().ToLower();
            if (Role != "vendor" && Role != "carrier")
            {
                if (ddlSite.innerControlddlSite.SelectedIndex > 0)
                    oMASSIT_HolidayBE.Site.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : 0;
            }
            else
            {
                if (ddlSiteCarrier.innerControlddlSite.SelectedIndex > 0)
                    oMASSIT_HolidayBE.Site.SiteID = !string.IsNullOrEmpty(ddlSiteCarrier.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSiteCarrier.innerControlddlSite.SelectedValue) : 0;
            }

            oMASSIT_HolidayBE.HolidayDate = sDate;

            List<MASSIT_HolidayBE> lstHoliday = oMASSIT_HolidayBAL.GetHolidaysBAL(oMASSIT_HolidayBE);

            if (lstHoliday != null && lstHoliday.Count > 0)
            {
                string errMsg = WebCommon.getGlobalResourceValue("BK_MS_36"); // Site is closed on this date. Please select a different date.
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
                isSiteOpen = false;
            }
        }
        return isSiteOpen;
    }

    //---------------------//

    protected void btnDelArrived_Click(object sender, EventArgs e)
    {
        if (hdnDliveryArrived.Value == "1")
        {
            EncryptQueryString("~/ModuleUI/Appointment/Receiving/APPRcv_DeliveryArrived.aspx?Scheduledate="
                + txtDate.innerControltxtDate.Value + "&ID=" + hdnRecordTable.Value.ToString() + "-"
                + hdnBookingFor.Value.ToString() + "-" + hdnPrimaryKey.Value + "-"
                + ddlSite.innerControlddlSite.SelectedItem.Value.ToString()
                + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString());
        }
        else
        {
            //undo delivery arrived
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.Action = "UndoDeliveryArrived";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(hdnPrimaryKey.Value);
            oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"].ToString());
            int? BookingDeatils = oAPPBOK_BookingBAL.UndoBookingDeliveryArrivedStatusBAL(oAPPBOK_BookingBE);
            EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
        }
        hdnDliveryArrived.Value = "1";
    }

    protected void btnUnexpectedDelivery_Click(object sender, EventArgs e)
    {
        strCountryId = string.Empty;
        if (ddlSite.CountryID != 0)
            strCountryId = Convert.ToString(ddlSite.CountryID);
        else if (ddlSite.PreCountryID.HasValue && ddlSite.PreCountryID != 0)
            strCountryId = Convert.ToString(ddlSite.PreCountryID);
        else
            strCountryId = "0";

        EncryptQueryString("~/ModuleUI/Appointment/Receiving/APPRcv_UnexpectedDelivery.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString()
            + "&SiteName=" + ddlSite.innerControlddlSite.SelectedItem.Text.ToString()
            + "&SiteCountryID=" + strCountryId);
    }

    protected void btnQualityChecked_Click(object sender, EventArgs e)
    {
        EncryptQueryString("~/ModuleUI/Appointment/Receiving/APPRcv_QualityCheck.aspx?Scheduledate="
                            + txtDate.innerControltxtDate.Value + "&ID=" + hdnRecordTable.Value.ToString() + "-"
                            + hdnBookingFor.Value.ToString() + "-" + hdnPrimaryKey.Value + "-" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString());
    }

    protected void btnBookingHistory_Click(object sender, EventArgs e)
    {
        EncryptQueryString("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?Scheduledate="
                            + txtDate.innerControltxtDate.Value + "&ID=" + hdnRecordTable.Value.ToString() + "-"
                            + hdnBookingFor.Value.ToString() + "-" + hdnPrimaryKey.Value + "-" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString());
    }

    protected void btnBookingHistory_1_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in gvVendor.Rows)
        {
            RadioButton rdo = row.FindControl("rdoBookingID") as RadioButton;
            if ((rdo != null) && rdo.Checked)
            {
                Label lbl = (Label)row.FindControl("lblExpectedDeliveryDate");
                if (lbl != null)
                {
                    txtDate.innerControltxtDate.Value = lbl.Text;
                }
            }
        }
        EncryptQueryString("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?PN=VENBO&Scheduledate="
                            + txtDate.innerControltxtDate.Value + "&ID=" + hdnRecordTable.Value.ToString() + "-"
                            + hdnBookingFor.Value.ToString() + "-" + hdnPrimaryKey.Value + "-" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString());
    }

    protected void btnDelivaryUnload_Click(object sender, EventArgs e)
    {
        //check Vender Checking Restriction//

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.Action = "CheckVendorRestriction";
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(hdnPrimaryKey.Value);
        int? BookingDeatils = oAPPBOK_BookingBAL.CheckVendorBookingBAL(oAPPBOK_BookingBE);
        if (hdnDliveryUnload.Value == "1")
        {
            if (BookingDeatils > 0)
            {
                string errMsg = string.Empty;
                if (hdnBookingFor.Value == "V")
                    errMsg = WebCommon.getGlobalResourceValue("CheckVendorRestriction"); // "Deliveries from this Vendor need to be checked in full before vehicle can be released.<br /> Please make sure that the driver is aware and that the delivery is checked off ASAP."; //
                else if (hdnBookingFor.Value == "C")
                    errMsg = WebCommon.getGlobalResourceValue("CheckCarrierRestriction"); // "Deliveries from this Vendor need to be checked in full before vehicle can be released.<br /> Please make sure that the driver is aware and that the delivery is checked off ASAP."; //
                ltMsgOK.Text = errMsg;
                btnMessageOk.CommandName = "DoNothing";

                mdlok.Show();
                UpdatePanel1.Update();
            } //---------------------------------//
            else
            {
                EncryptQueryString("~/ModuleUI/Appointment/Receiving/APPRcv_DeliveryUnloaded.aspx?Scheduledate="
                    + txtDate.innerControltxtDate.Value + "&ID=" + hdnRecordTable.Value.ToString() + "-"
                    + hdnBookingFor.Value.ToString() + "-" + hdnPrimaryKey.Value + "-" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString()
                    + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString());
            }
        }
        else
        {
            oAPPBOK_BookingBE.Action = "UndoDeliveryUnload";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(hdnPrimaryKey.Value);
            oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"].ToString());
            int? Result = oAPPBOK_BookingBAL.UndoBookingDeliveryUnLoadStatusBAL(oAPPBOK_BookingBE);
            EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
        }

        hdnDliveryUnload.Value = "1";
    }

    protected void btnEditBooking_Click(object sender, EventArgs e)
    {
        if (!GetToleratedDays(hdnLogicalDate.Value))
        {
            EditBookingForOD();
        }
        if (imgAlternateView.Visible == false)
        {
            GenerateAlternateSlot(false);
        }
    }

    private void EditBookingForOD()
    {
        MAS_SiteBAL objMasSitE = new MAS_SiteBAL();
        string TimeWindowFlag = objMasSitE.GetSiteTimeSlotWindowFlagBAL(Convert.ToString(ddlSite.innerControlddlSite.SelectedItem.Value));

        if (hdnBookingFor.Value.ToString() == "V")
        {
            if (TimeWindowFlag == "W")
                EncryptQueryString("APPBok_WindowBookingEdit.aspx?Mode=Edit&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnLogicalDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + ddlSite.PreCountryID.ToString());
            else
                EncryptQueryString("APPBok_BookingEdit.aspx?Mode=Edit&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnLogicalDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + ddlSite.PreCountryID.ToString());
        }
        else if (hdnBookingFor.Value.ToString() == "C")
        {
            if (TimeWindowFlag == "W")
                EncryptQueryString("APPBok_CarrierBookingWindowEdit.aspx?Mode=Edit&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnLogicalDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + ddlSite.PreCountryID.ToString());
            else
                EncryptQueryString("APPBok_CarrierBookingEdit.aspx?Mode=Edit&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnLogicalDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + ddlSite.PreCountryID.ToString());
        }
    }

    protected void btnEditABooking_Click(object sender, EventArgs e)
    {
        if (!GetToleratedDays(hdnbookingdate.Value))
        {
            MAS_SiteBAL objMasSitE = new MAS_SiteBAL();
            string Role = Session["Role"].ToString().Trim().ToLower();
            string TimeWindowFlag = string.Empty;
            if (Role != "vendor" && Role != "carrier")
                TimeWindowFlag = objMasSitE.GetSiteTimeSlotWindowFlagBAL(Convert.ToString(ddlSite.innerControlddlSite.SelectedItem.Value));
            else
                TimeWindowFlag = objMasSitE.GetSiteTimeSlotWindowFlagBAL(Convert.ToString(hdnsiteid.Value));

            if (hdnBookingFor.Value.ToString() == "V")
            {
                if (TimeWindowFlag == "W")
                    EncryptQueryString("APPBok_WindowBookingEdit.aspx?Mode=Edit&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnbookingdate.Value + "&Logicaldate=" + hdnLogicalDate.Value + "&SiteID=" + hdnsiteid.Value.ToString() + "&SiteCountryID=" + ddlSiteCarrier.PreCountryID.ToString());
                else
                    EncryptQueryString("APPBok_BookingEdit.aspx?Mode=Edit&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnbookingdate.Value + "&Logicaldate=" + hdnLogicalDate.Value + "&SiteID=" + hdnsiteid.Value.ToString() + "&SiteCountryID=" + ddlSiteCarrier.PreCountryID.ToString());
            }
            else if (hdnBookingFor.Value.ToString() == "C")
            {
                if (TimeWindowFlag == "W")
                    EncryptQueryString("APPBok_CarrierBookingWindowEdit.aspx?Mode=Edit&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnbookingdate.Value + "&Logicaldate=" + hdnLogicalDate.Value + "&SiteID=" + hdnsiteid.Value.ToString() + "&SiteCountryID=" + ddlSiteCarrier.PreCountryID.ToString());
                else
                    EncryptQueryString("APPBok_CarrierBookingEdit.aspx?Mode=Edit&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnbookingdate.Value + "&Logicaldate=" + hdnLogicalDate.Value + "&SiteID=" + hdnsiteid.Value.ToString() + "&SiteCountryID=" + ddlSiteCarrier.PreCountryID.ToString());
            }
        }
    }

    protected void btnAddBooking_Click(object sender, EventArgs e)
    {
        if (txtDate.GetDate.Date < DateTime.Now.Date)
        {
            string errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT"); // "Booking not allowed in past date.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
            return;
        }

        string Role = Session["Role"].ToString().Trim().ToLower();
        if (Role != "vendor" && Role != "carrier")
        {
            if (!CheckSiteClosure(txtDate.GetDate))
            {
                return;
            }
        }

        MAS_SiteBAL objMasSitE = new MAS_SiteBAL();
        string TimeWindowFlag = string.Empty;

        if (ddlSiteCarrier.Visible)
            TimeWindowFlag = objMasSitE.GetSiteTimeSlotWindowFlagBAL(Convert.ToString(ddlSiteCarrier.innerControlddlSite.SelectedItem.Value));
        else
            TimeWindowFlag = objMasSitE.GetSiteTimeSlotWindowFlagBAL(Convert.ToString(ddlSite.innerControlddlSite.SelectedItem.Value));

        MAS_SiteBE singleSiteSetting = null;
        if (ddlSiteCarrier.Visible)
            singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSiteCarrier.innerControlddlSite.SelectedValue));
        else
            singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));

        if (Role != "vendor" && Role != "carrier")
        {
            if (ddlSiteCarrier.Visible)
            {
                if (TimeWindowFlag == "W")
                {
                    string TimeWindowID = hdnWindowTimeID.Value;
                    if (TimeWindowID != "")
                    {
                        EncryptQueryString("APPBOK_Option.aspx?Page=APPBok_WindowBookingEdit.aspx&Mode=Add&Scheduledate=" + txtDate.innerControltxtDate.Value + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSiteCarrier.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + singleSiteSetting.SiteCountryID.ToString() + "&Flag=" + "true" + "&Time=" + hdnWindowTime.Value + "&SiteDoorNumberID=" + hdnSiteDoorNumberID.Value + "&DoorNumber=" + hdnDoorNumber.Value + "&Type=" + "");
                    }
                    else
                    {
                        EncryptQueryString("APPBOK_Option.aspx?Page=APPBok_WindowBookingEdit.aspx&Mode=Add&Scheduledate=" + txtDate.innerControltxtDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSiteCarrier.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + singleSiteSetting.SiteCountryID.ToString() + "&Flag=" + "false");
                    }
                }
                else
                    EncryptQueryString("APPBOK_Option.aspx?Page=APPBok_BookingEdit.aspx&Mode=Add&Scheduledate=" + txtDate.innerControltxtDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSiteCarrier.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + singleSiteSetting.SiteCountryID.ToString());
            }
            else
            {
                if (TimeWindowFlag == "W")
                {
                    string TimeWindowID1 = hdnWindowTimeID.Value;
                    if (TimeWindowID1 != "")
                    {
                        EncryptQueryString("APPBOK_Option.aspx?Page=APPBok_WindowBookingEdit.aspx&Mode=Add&Scheduledate=" + txtDate.innerControltxtDate.Value + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSiteCarrier.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + singleSiteSetting.SiteCountryID.ToString() + "&Flag=" + "true" + "&Time=" + hdnWindowTime.Value + "&SiteDoorNumberID=" + hdnSiteDoorNumberID.Value + "&DoorNumber=" + hdnDoorNumber.Value + "&Type=" + "");
                    }
                    else
                    {
                        EncryptQueryString("APPBOK_Option.aspx?Page=APPBok_WindowBookingEdit.aspx&Mode=Add&Scheduledate=" + txtDate.innerControltxtDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + singleSiteSetting.SiteCountryID.ToString() + "&Flag=" + "false");
                    }
                }
                else
                    EncryptQueryString("APPBOK_Option.aspx?Page=APPBok_BookingEdit.aspx&Mode=Add&Scheduledate=" + txtDate.innerControltxtDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + singleSiteSetting.SiteCountryID.ToString());
            }
        }
        else
        {
            if (txtDate.Visible)
            {
                if (Role == "vendor")
                {
                    if (TimeWindowFlag == "W")
                        EncryptQueryString("APPBok_WindowBookingEdit.aspx?Mode=Add&Scheduledate=" + txtDate.innerControltxtDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSiteCarrier.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + singleSiteSetting.SiteCountryID.ToString() + "&Flag=" + "true");
                    else
                        EncryptQueryString("APPBok_BookingEdit.aspx?Mode=Add&Scheduledate=" + txtDate.innerControltxtDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSiteCarrier.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + singleSiteSetting.SiteCountryID.ToString());
                }
                else
                {
                    if (TimeWindowFlag == "W")
                        EncryptQueryString("APPBok_CarrierBookingWindowEdit.aspx?Mode=Add&Scheduledate=" + txtDate.innerControltxtDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSiteCarrier.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + singleSiteSetting.SiteCountryID.ToString() + "&Flag=" + "false");
                    else
                        EncryptQueryString("APPBok_CarrierBookingEdit.aspx?Mode=Add&Scheduledate=" + txtDate.innerControltxtDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSiteCarrier.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + singleSiteSetting.SiteCountryID.ToString());
                }
            }
            else
            {
                if (Role == "vendor")
                {
                    if (TimeWindowFlag == "W")
                        EncryptQueryString("APPBok_WindowBookingEdit.aspx?Mode=Add");
                    else
                        EncryptQueryString("APPBok_BookingEdit.aspx?Mode=Add");
                }
                else
                {
                    if (TimeWindowFlag == "W")
                        EncryptQueryString("APPBok_CarrierBookingWindowEdit.aspx?Mode=Add");
                    else
                        EncryptQueryString("APPBok_CarrierBookingEdit.aspx?Mode=Add");
                }
            }
        }
    }

    private bool GetToleratedDays(string schedulingDate, string CommandName = "EditBooking")
    {
        bool isError = false;
        if (schedulingDate != string.Empty)
        {
            MAS_SiteBE oMAS_SITEBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SITEBAL = new MAS_SiteBAL();

            List<MAS_SiteBE> lstSites = new List<MAS_SiteBE>();
            string Role = Session["Role"].ToString().Trim().ToLower();
            if (Role == "vendor" || Role == "carrier")
            {
                oMAS_SITEBE.Action = "ShowAll"; //"ShowAllForVendor";
                oMAS_SITEBE.SiteID = Convert.ToInt32(hdnsiteid.Value);
            }
            else
            {
                oMAS_SITEBE.Action = "ShowAll";
                oMAS_SITEBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            }
            oMAS_SITEBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SITEBE.User.UserID = Convert.ToInt32(Session["UserID"]);

            lstSites = oMAS_SITEBAL.GetSiteMisSettingBAL(oMAS_SITEBE);

            if (lstSites != null && lstSites.Count > 0)
            {
                int? NoticePeriodDays = lstSites[0].BookingNoticePeriodInDays;

                DateTime SchDate = Utilities.Common.GetMM_DD_YYYY(schedulingDate); //' Convert.ToDateTime(schedulingDate);
                DateTime CurrentDate = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString());

                int DaysDiff = 0;
                if (SchDate >= CurrentDate)
                {
                    DaysDiff = Enumerable.Range(0, Convert.ToInt32(SchDate.Subtract(CurrentDate).TotalDays)).Select(i => new[] { DayOfWeek.Saturday, DayOfWeek.Sunday }.Contains(CurrentDate.AddDays(i).DayOfWeek) ? 0 : 1).Sum();
                }

                //---Stage 4 Point 4---
                //All deliveries scheduled into this site must be booked before 12:00 and 1 working day(s) before delivery, therefore we cannot accept this booking on the date you have requested. Please select another date or contact the site's booking in department.
                string errMsg = string.Empty;
                if (NoticePeriodDays == null || NoticePeriodDays == 0)
                {
                    if (lstSites[0].BookingNoticeTime != null)
                    {
                        string NoticeTime = Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Hour.ToString()) + ":" + Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Minute.ToString());
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_2").Replace("##Time##", NoticeTime) + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_4").Replace("##NoticePeriodInDays##", "1");
                    }
                }
                else
                {
                    if (lstSites[0].BookingNoticeTime != null)
                    {
                        string NoticeTime = Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Hour.ToString()) + ":" + Common.FormatTime(lstSites[0].BookingNoticeTime.Value.Minute.ToString());
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_2").Replace("##Time##", NoticeTime) + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_4").Replace("##NoticePeriodInDays##", (NoticePeriodDays).ToString());
                    }
                    else
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_1") + " " +
                            WebCommon.getGlobalResourceValue("BK_MS_05_EXT_NEW_4").Replace("##NoticePeriodInDays##", (NoticePeriodDays).ToString());
                    }
                }
                //-----------------------//

                bool isAllowed = false;

                if (NoticePeriodDays == null)
                {
                    if (DaysDiff == 0)
                    {
                        DateTime dt1, dt2;
                        dt1 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);
                        if (lstSites[0].BookingNoticeTime != null)
                            dt2 = new DateTime(1900, 01, 01, lstSites[0].BookingNoticeTime.Value.Hour, lstSites[0].BookingNoticeTime.Value.Minute, 00);
                        else
                            dt2 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);

                        if (dt1 <= dt2)
                        {
                            isAllowed = true;
                        }
                    }
                    else
                    {
                        isAllowed = true;
                    }
                }

                if (isAllowed == false && DaysDiff >= NoticePeriodDays + 1)
                    isAllowed = true;

                if (isAllowed == false && DaysDiff == NoticePeriodDays)
                {
                    DateTime dt1, dt2;
                    dt1 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);
                    if (lstSites[0].BookingNoticeTime != null)
                        dt2 = new DateTime(1900, 01, 01, lstSites[0].BookingNoticeTime.Value.Hour, lstSites[0].BookingNoticeTime.Value.Minute, 00);
                    else
                        dt2 = new DateTime(1900, 01, 01, DateTime.Now.Hour, DateTime.Now.Minute, 00);

                    if (dt1 <= dt2)
                    {
                        isAllowed = true;
                    }
                }

                if (isAllowed == false && NoticePeriodDays == 0 && SchDate > CurrentDate)
                {
                    isAllowed = true;
                }

                if (!isAllowed)
                {
                    //error
                    if (Role == "vendor" || Role == "carrier")
                    {
                        ltMsgOK.Text = errMsg;
                        btnMessageOk.CommandName = CommandName;
                        mdlok.Show();
                        return true;
                    }
                    else
                    {
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_INT"); // "There is insufficient notice given for this booking. Do you want to continue?";
                        ltConfirmationMsg.Text = errMsg;
                        btnConfirmationMsgYes.CommandName = CommandName;
                        mdlConfirmationMsg.Show();
                        isError = true;
                    }
                }
            }
        }
        return isError;
    }

    protected void btnConfirmAFixedSlot_Click(object sender, EventArgs e)
    {
        if (txtDate.GetDate.Date < DateTime.Now.Date)
        {
            string errMsg = WebCommon.getGlobalResourceValue("BK_MS_05_EXT"); // "Booking not allowed in past date.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
            return;
        }

        DateTime? sdate;
        sdate = DateTime.Now.Date;
        if (Session["BookingCritreria"] != null)
        {
            Hashtable htBookingCriteria = (Hashtable)Session["BookingCritreria"];
            sdate = htBookingCriteria.ContainsKey("SchedulingDate") ? Utilities.Common.TextToDateFormat(htBookingCriteria["SchedulingDate"].ToString()) : (DateTime?)null;
        }

        if (!CheckSiteClosure(sdate))
        {
            return;
        }

        if (hdnBookingFor.Value.ToString() == "V")
        {
            if (hdnRecordTable.Value != "TW")
                EncryptQueryString("APPBok_BookingEdit.aspx?FixedSlot=True&Mode=Add&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnLogicalDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + ddlSite.PreCountryID.ToString() + "&BookingWeekDay=" + hdnBookingWeekDay.Value.ToString());
            else
                EncryptQueryString("APPBok_WindowBookingEdit.aspx?WindowSlot=True&Mode=Add&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnLogicalDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + ddlSite.PreCountryID.ToString() + "&BookingWeekDay=" + hdnBookingWeekDay.Value.ToString() + "&Flag=" + "true");
        }
        else if (hdnBookingFor.Value.ToString() == "C")
        {
            if (hdnRecordTable.Value != "TW")
                EncryptQueryString("APPBok_CarrierBookingEdit.aspx?FixedSlot=True&Mode=Add&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnLogicalDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + ddlSite.PreCountryID.ToString() + "&BookingWeekDay=" + hdnBookingWeekDay.Value.ToString());
            else
                EncryptQueryString("APPBok_CarrierBookingWindowEdit.aspx?WindowSlot=True&Mode=Add&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnLogicalDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + ddlSite.PreCountryID.ToString() + "&BookingWeekDay=" + hdnBookingWeekDay.Value.ToString() + "&Flag=" + "true");
        }
    }

    protected void btnConfirmFixedSlot_Click(object sender, EventArgs e)
    {
        if (hdnBookingFor.Value.ToString() == "V")
        {
            if (hdnRecordTable.Value != "TW")
                EncryptQueryString("APPBok_BookingEdit.aspx?FixedSlot=True&Mode=Add&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnbookingdate.Value + "&Logicaldate=" + hdnLogicalDate.Value + "&SiteID=" + hdnsiteid.Value.ToString() + "&SiteCountryID=" + ddlSiteCarrier.PreCountryID.ToString());
            else
                EncryptQueryString("APPBok_WindowBookingEdit.aspx?WindowSlot=True&Mode=Add&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnbookingdate.Value + "&Logicaldate=" + hdnLogicalDate.Value + "&SiteID=" + hdnsiteid.Value.ToString() + "&SiteCountryID=" + ddlSiteCarrier.PreCountryID.ToString() + "&Flag=" + "true");
        }
        else if (hdnBookingFor.Value.ToString() == "C")
        {
            if (hdnRecordTable.Value != "TW")
                EncryptQueryString("APPBok_CarrierBookingEdit.aspx?FixedSlot=True&Mode=Add&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnbookingdate.Value + "&Logicaldate=" + hdnLogicalDate.Value + "&SiteID=" + hdnsiteid.Value.ToString() + "&SiteCountryID=" + ddlSiteCarrier.PreCountryID.ToString());
            else
                EncryptQueryString("APPBok_CarrierBookingWindowEdit.aspx?WindowSlot=True&Mode=Add&Type=" + hdnBookingFor.Value.ToString() + "&ID=" + hdnPrimaryKey.Value.ToString() + "&Scheduledate=" + hdnbookingdate.Value + "&Logicaldate=" + hdnLogicalDate.Value + "&SiteID=" + hdnsiteid.Value.ToString() + "&SiteCountryID=" + ddlSiteCarrier.PreCountryID.ToString() + "&Flag=" + "true");
        }
    }

    protected void btnDeleteABooking_Click(object sender, EventArgs e)
    {
        if (!GetToleratedDays(hdnLogicalDate.Value, "DeleteBooking"))
        {
            DeleteBooking();
        }
    }

    private void DeleteBooking()
    {
        string Role = Session["Role"].ToString().Trim().ToLower();
        if (Role != "vendor" && Role != "carrier")
            ltConfirmationMsg.Text = WebCommon.getGlobalResourceValue("DeleteBookingConfirmOD");
        else
            ltConfirmationMsg.Text = WebCommon.getGlobalResourceValue("DeleteBookingConfirm");

        if (hdnRecordTable.Value == "BK")
            btnConfirmationMsgYes.CommandName = "Booking";
        else if (hdnRecordTable.Value == "FS")
            btnConfirmationMsgYes.CommandName = "Fixedslot";
        else if (hdnRecordTable.Value == "TW")
            btnConfirmationMsgYes.CommandName = "TimeWindow";

        mdlConfirmationMsg.Show();
    }

    protected void btnHidden_Click(object sender, EventArgs e)
    {
        Session["VolumeDetailSiteName"] = ddlSite.innerControlddlSite.SelectedItem.Text;
        Session["VolumeDetailSiteID"] = ddlSite.innerControlddlSite.SelectedItem.Value;
        Session["VolumeDetailDate"] = txtDate.innerControltxtDate.Value;
        Session["VolumeDetailWeekDay"] = txtDate.GetDate.DayOfWeek.ToString().ToUpper();
        EncryptQueryString("APPBok_BookingVolumeDetail.aspx");
    }

    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        Session["DataTableName"] = null;
        Session["DataSet"] = null;
        Session["ReportPath"] = null;
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oAPPBOK_BookingBE.ScheduleDate = txtDate.GetDate;
        oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
        oAPPBOK_BookingBE.StockPlannerID = Convert.ToInt32(ddlStockPalnner.SelectedItem.Value);
        dsSchedulingPrintOut = oAPPBOK_BookingBAL.GetSchedulingPrintOutBAL(oAPPBOK_BookingBE);
        Session["DataTableName"] = "dsSchedulingPrintOut";
        Session["DataSet"] = dsSchedulingPrintOut;
        Session["ReportPath"] = "\\ModuleUI\\Appointment\\Reports\\RDLC\\APPBok_SchedulingPrintOut.rdlc";

        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", String.Format("<script>window.open('{0}');</script>", "ReportViewer.aspx"), false);
    }

    public void btnConfirmationMsgYes_Click(object sender, EventArgs e)
    {
        if (btnConfirmationMsgYes.CommandName == "EditBooking")
        {
            EditBookingForOD();
        }
        else if (btnConfirmationMsgYes.CommandName == "DeleteBooking")
        {
            DeleteBooking();
        }
        else
        {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
            string Role = Session["Role"].ToString().Trim().ToLower();
            GridView gridToParse = new GridView();

            if (Role != "vendor" && Role != "carrier")
            {
                gridToParse = ucGridView1;
            }
            else
            {
                gridToParse = gvVendor;
            }

            oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
            string CrossOverDay = string.Empty;
            if (btnConfirmationMsgYes.CommandName == "Booking")
            {
                ltMsgOK.Text = WebCommon.getGlobalResourceValue("BookingDeleted");
                mdlok.Show();
                oAPPBOK_BookingBE.Action = "DeleteBooking";

                if (ucGridView1.Visible)
                {
                    //Comment for alternate slot
                    if (gridToParse.Rows.Count > 0)
                    {
                        foreach (GridViewRow ODgrd in gridToParse.Rows)
                        {
                            RadioButton ob = (RadioButton)ODgrd.FindControl("rdoBookingID");
                            if (ob.Checked)
                            {
                                HiddenField lblDeleteBookingID = (HiddenField)ODgrd.FindControl("hdnBookingID");
                                oAPPBOK_BookingBE.BookingID = Convert.ToInt32(lblDeleteBookingID.Value.Substring(3));
                                break;
                            }
                        }
                    }
                }
                else
                {
                    oAPPBOK_BookingBE.BookingID = Convert.ToInt32(hdnPrimaryKey.Value);
                }
            }
            else if (btnConfirmationMsgYes.CommandName == "Fixedslot" || btnConfirmationMsgYes.CommandName == "TimeWindow")
            {
                ltMsgOK.Text = WebCommon.getGlobalResourceValue("FixedslotDeleted");
                mdlok.Show();
                if (ucGridView1.Visible)
                {
                    foreach (GridViewRow gv in gridToParse.Rows)
                    {
                        RadioButton ob = (RadioButton)gv.FindControl("rdoBookingID");
                        if (ob.Checked)
                        {
                            HiddenField lblDeleteFixedSlotID = (HiddenField)gv.FindControl("hdnBookingID");
                            HiddenField lblDeleteFixedSlotdate = (HiddenField)gv.FindControl("hdnExpectedDeliveryDate");
                            Literal ltCrossOverDay = (Literal)gv.FindControl("UcLiteral18");
                            CrossOverDay = ltCrossOverDay.Text;
                            if (Role != "vendor" && Role != "carrier")
                            {
                                HiddenField hdnWeekday = (HiddenField)gv.FindControl("hdnWeekday");
                                if (hdnWeekday != null)
                                {
                                    if (hdnWeekday.Value == "8")
                                        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);
                                    else
                                        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value).AddDays(-1);
                                }
                            }
                            else
                            {
                                oAPPBOK_BookingBE.ScheduleDate = Convert.ToDateTime(lblDeleteFixedSlotdate.Value);
                            }

                            oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(lblDeleteFixedSlotID.Value.Substring(3));

                            break;
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(hdnPrimaryKey.Value))
                    {
                        oAPPBOK_BookingBE.FixedSlot.FixedSlotID = Convert.ToInt32(hdnPrimaryKey.Value);
                    }
                    if (hdnBookingWeekDay.Value == "8")
                        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);
                    else
                        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value).AddDays(-1);
                }

                if (btnConfirmationMsgYes.CommandName == "Fixedslot")
                {
                    if (!ucGridView1.Visible)
                    {
                        CrossOverDay = oAPPBOK_BookingBE.ScheduleDate.Value.ToString("dddd").ToLower().Trim();
                    }

                    oAPPBOK_BookingBE.Action = "DeleteFixedSlot";
                    if ((Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value).ToString("dddddddddddddd").Substring(0,
                        Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value).ToString("dddddddddddddd").Length)).ToLower().Trim()
                        == CrossOverDay.ToLower().Trim())
                    {
                        oAPPBOK_BookingBE.IsCrossDayBefore = false;
                    }
                    else
                    {
                        oAPPBOK_BookingBE.IsCrossDayBefore = true;
                    }
                    if (Role == "vendor" || Role == "carrier")
                    {
                        if (!string.IsNullOrEmpty(DeletionFixedSlotDate))
                            oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(DeletionFixedSlotDate);
                    }
                }
                else if (btnConfirmationMsgYes.CommandName == "TimeWindow")
                    oAPPBOK_BookingBE.Action = "DeleteTimeWindowSlot";
            }

            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            int? IDelete = oAPPBOK_BookingBAL.DeleteBookingDetailBAL(oAPPBOK_BookingBE);
            if (IDelete > 0)
            {
                hdnsiteid.Value = string.Empty;
                hdnbookingdate.Value = string.Empty;
                hdnLogicalDate.Value = string.Empty;
                hdnBookingFor.Value = string.Empty;
                hdnPrimaryKey.Value = string.Empty;
                hdnRecordTable.Value = string.Empty;
            }

            //-----------Sprint 18 R1 Point 2-------------//
            if (btnConfirmationMsgYes.CommandName == "Booking")
            {
                sendDeletedBookingMail(oAPPBOK_BookingBE.BookingID);
            }
            //---------------------------------------------//
            if (IDelete > 0)
            {
                btnConfirmationMsgYes.CommandName = string.Empty;
            }
        }
    }

    private void sendDeletedBookingMail(int BookingID)
    {
        APPBOK_CommunicationBAL communicationBAL = new APPBOK_CommunicationBAL();
        List<MAS_LanguageBE> lstLanguages = communicationBAL.GetLanguages();

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.BookingID = BookingID;
        oAPPBOK_BookingBE.Action = "DeletedBookingDetails";

        List<APPBOK_BookingBE> lstDeletedBooking = oAPPBOK_BookingBAL.DeletedBookingDetailsBAL(oAPPBOK_BookingBE);

        string VendorEmails = string.Empty;
        string[] sentTo;
        string[] language;
        string[] VendorData = new string[2];
        bool isMailSend = false;

        if (lstDeletedBooking[0].SupplierType == "C")
        {
            //Send mail to Carrier
            isMailSend = false;
            VendorData = CommonPage.GetCarrierEmailsWithLanguage(lstDeletedBooking[0].Carrier.CarrierID, Convert.ToInt32(lstDeletedBooking[0].SiteId));
            if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
            {
                sentTo = VendorData[0].Split(',');
                language = VendorData[1].Split(',');
                for (int j = 0; j < sentTo.Length; j++)
                {
                    if (sentTo[j].Trim() != "")
                    {
                        SendMail(sentTo[j], lstDeletedBooking, lstLanguages, language[j], BookingID);
                        isMailSend = true;
                    }
                }
            }
            if (!isMailSend)
            {
                SendMail(ConfigurationManager.AppSettings["DefaultEmailAddress"], lstDeletedBooking, lstLanguages, "English", BookingID);
            }
        }
        else if (lstDeletedBooking[0].SupplierType == "V")
        {
            //send mail to vendor
            isMailSend = false;
            VendorData = CommonPage.GetVendorEmailsWithLanguage(lstDeletedBooking[0].VendorID, Convert.ToInt32(lstDeletedBooking[0].SiteId));

            if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
            {
                sentTo = VendorData[0].Split(',');
                language = VendorData[1].Split(',');
                for (int j = 0; j < sentTo.Length; j++)
                {
                    if (sentTo[j].Trim() != "")
                    {
                        SendMail(sentTo[j], lstDeletedBooking, lstLanguages, language[j], BookingID);
                        isMailSend = true;
                    }
                }
            }
            if (!isMailSend)
            {
                SendMail(ConfigurationManager.AppSettings["DefaultEmailAddress"], lstDeletedBooking, lstLanguages, "English", BookingID);
            }
        }
    }

    private void SendMail(string toAddress, List<APPBOK_BookingBE> lstBooking, List<MAS_LanguageBE> lstLanguages, string sentinLang, int BookingID)
    {
        string mailBody = string.Empty;
        string mailSubject = "Booking Deleted";
        string fromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

        clsEmail oclsEmail = new clsEmail();

        foreach (MAS_LanguageBE objLanguage in lstLanguages)
        {
            bool MailSentInLanguage = false;

            if (objLanguage.Language.ToLower() == sentinLang.ToLower())
            {
                MailSentInLanguage = true;
            }

            mailBody = GetDeltedMailHTML(lstBooking[0], objLanguage.LanguageID);

            #region First Saving data of [AddBookingCommunication] Then sending email.

            CommunicationType.Enum communicationType = CommunicationType.Enum.BookingDeleted;

            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            APPBOK_CommunicationBE oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
            oAPPBOK_CommunicationBE.Action = "AddDeletedBookingCommunication";
            oAPPBOK_CommunicationBE.CommunicationType = CommunicationType.Name(communicationType);
            oAPPBOK_CommunicationBE.BookingID = BookingID;
            oAPPBOK_CommunicationBE.Subject = mailSubject;
            oAPPBOK_CommunicationBE.SentFrom = fromAddress;
            oAPPBOK_CommunicationBE.SentTo = toAddress;
            oAPPBOK_CommunicationBE.Body = mailBody;
            oAPPBOK_CommunicationBE.SendByID = Convert.ToInt32(Session["UserID"]);
            oAPPBOK_CommunicationBE.IsMailSent = MailSentInLanguage;
            oAPPBOK_CommunicationBE.LanguageID = objLanguage.LanguageID;
            oAPPBOK_CommunicationBE.MailSentInLanguage = MailSentInLanguage;
            int intIsExist = oAPPBOK_CommunicationBAL.AddBookingCommunicationBAL(oAPPBOK_CommunicationBE) ?? 0;
            if (intIsExist.Equals(0) && MailSentInLanguage.Equals(true))
            {
                oclsEmail.sendMail(toAddress, mailBody, mailSubject, fromAddress, true);
            }

            #endregion First Saving data of [AddBookingCommunication] Then sending email.
        }
    }

    private string GetDeltedMailHTML(APPBOK_BookingBE oAPPBOK_BookingBE, int languageID)
    {
        string htmlBody = string.Empty;
        string templatePath = string.Empty;
        string LanguageFile = string.Empty;
        sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePathName;
        LanguageFile = "DeletedBooking.english.htm";

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            #region mailBody

            htmlBody = sReader.ReadToEnd();
            // OD logo.
            htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValueByLangID("DearSirMadam", languageID));
            htmlBody = htmlBody.Replace("{DeleteBookingText1}", WebCommon.getGlobalResourceValueByLangID("DeleteBookingText1", languageID)).Replace("##bookingref##", oAPPBOK_BookingBE.BookingRef);

            htmlBody = htmlBody.Replace("{BookingReference}", WebCommon.getGlobalResourceValueByLangID("BookingReference", languageID));
            htmlBody = htmlBody.Replace("{BookingRefValue}", oAPPBOK_BookingBE.BookingRef);

            string strScheduleDate = string.Empty;
            string strDeliveryTime = string.Empty;

            if (oAPPBOK_BookingBE.BookingTypeID != 3)
                strDeliveryTime = oAPPBOK_BookingBE.SlotTime.SlotTime.Replace("-", "/");
            else
                strDeliveryTime = WebCommon.getGlobalResourceValueByLangID("NonTimeDelivery", languageID); ;

            strScheduleDate = string.Format("{0} {1}", oAPPBOK_BookingBE.ScheduleDate.Value.ToString("dd/MM/yyyy"), strDeliveryTime);

            htmlBody = htmlBody.Replace("{BookingDateTime}", WebCommon.getGlobalResourceValueByLangID("BookingDateTime", languageID));
            htmlBody = htmlBody.Replace("{BookingDateTimeValue}", strScheduleDate);

            htmlBody = htmlBody.Replace("{DeletedBy}", WebCommon.getGlobalResourceValueByLangID("DeletedBy", languageID));
            htmlBody = htmlBody.Replace("{DeletedByValue}", oAPPBOK_BookingBE.UserName);

            htmlBody = htmlBody.Replace("{DeletedAt}", WebCommon.getGlobalResourceValueByLangID("DeletedAt", languageID));
            htmlBody = htmlBody.Replace("{DeletedAtValue}", oAPPBOK_BookingBE.DeletedDate.Value.ToString("dd/MM/yyyy HH:mm"));

            htmlBody = htmlBody.Replace("{PurchaseOrders}", WebCommon.getGlobalResourceValueByLangID("PurchaseOrders", languageID));
            htmlBody = htmlBody.Replace("{PurchaseOrdersValue}", oAPPBOK_BookingBE.PurchaseOrdersIDs.Trim(','));

            htmlBody = htmlBody.Replace("{ManyThanksBO}", WebCommon.getGlobalResourceValueByLangID("ManyThanksBO", languageID));
            htmlBody = htmlBody.Replace("{VIPAdmin}", WebCommon.getGlobalResourceValueByLangID("VIPAdmin", languageID));

            #endregion mailBody
        }
        return htmlBody;
    }

    public void btnConfirmationMsgNo_Click(object sender, EventArgs e)
    {
        MAS_SiteBAL objMasSitE = new MAS_SiteBAL();

        if (objMasSitE.GetSiteTimeSlotWindowFlagBAL(Convert.ToString(ddlSite.innerControlddlSite.SelectedItem.Value)) == "W")
        {
            if (ImgWindowAlternateViewClicked == true || imgWindowAlternateView.Visible == false)
            {
                GenerateWindowAlternateSlot();
            }
        }
        else if (imgAlternateView.Visible == false)
        {
            GenerateAlternateSlot(false);
        }
        mdlConfirmationMsg.Hide();
        if (btnConfirmationMsgYes.CommandName == "Booking")
        {
            btnConfirmFixedSlot.Enabled = false;
            btnDeleteFixedSlot.Enabled = false;
            btnVendorEditBooking.Enabled = true;
            btnDeleteBooking.Enabled = true;
        }
        else if (btnConfirmationMsgYes.CommandName == "Fixedslot")
        {
            btnVendorEditBooking.Enabled = false;
            btnDeleteBooking.Enabled = false;
            btnConfirmFixedSlot.Enabled = true;
            btnDeleteFixedSlot.Enabled = true;
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "css", "<script>setReadonlyClass();</script>", false);
    }

    protected void btnDeleteFixedSlot_Click(object sender, EventArgs e)
    {
        string str = WebCommon.getGlobalResourceValue("DeleteFixedslotConfirm");
        btnConfirmationMsgYes.CommandName = "Fixedslot";
        foreach (GridViewRow gv in gvVendor.Rows)
        {
            RadioButton ob = (RadioButton)gv.FindControl("rdoBookingID");
            if (ob.Checked)
            {
                string value = gv.Cells[11].Text;
                HiddenField hdnTime = (HiddenField)gv.FindControl("hdnTime");
                str = str.Replace("{time}", hdnTime.Value);
                Label lblDeleteFixedSlot = (Label)gv.FindControl("lblExpectedDeliveryDate");
                str = str.Replace("{date}", lblDeleteFixedSlot.Text);
                DeletionFixedSlotDate = lblDeleteFixedSlot.Text;
            }
        }
        ltConfirmationMsg.Text = str;
        mdlConfirmationMsg.Show();
    }

    protected void btnMessageOk_Click(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "EditBooking" || e.CommandName == "DeleteBooking")
        {
            mdlok.Hide();
        }
        else if (e.CommandName == "Rediract")
        {
            mdlok.Hide();
            MAS_SiteBAL objMasSitE = new MAS_SiteBAL();
            BindBookings();

            if (objMasSitE.GetSiteTimeSlotWindowFlagBAL(Convert.ToString(ddlSite.innerControlddlSite.SelectedItem.Value)) == "W")
            {
                if (imgWindowAlternateView.Visible == false)
                {
                    if (pnlVolumeDetails_1.Visible == true)
                    {
                        tableDiv_GeneralNew.Visible = true;
                        divPrint.Visible = false;
                        pnlVolumeDetails_1.Visible = true;
                    }
                    else
                    {
                        tableDiv_GeneralNew.Visible = true;
                        divPrint.Visible = false;
                        pnlVolumeDetails_1.Visible = true;
                    }
                    GenerateWindowAlternateSlot();
                    pnlLinks.Visible = true;
                    lnkNonStandardWindowBookingDetails.Visible = true;
                    lnkNonTimeBookingDetails.Visible = false;
                }
            }
            else
            {
                if (imgAlternateView.Visible == false)
                {
                    if (pnlVolumeDetails_1.Visible == true)
                    {
                        tableDiv_GeneralNew.Visible = true;
                        divPrint.Visible = false;
                        pnlVolumeDetails_1.Visible = true;
                    }
                    else
                    {
                        tableDiv_GeneralNew.Visible = true;
                        divPrint.Visible = false;
                        pnlVolumeDetails_1.Visible = true;
                    }
                    GenerateAlternateSlot(false);
                    pnlLinks.Visible = true;
                    lnkNonStandardWindowBookingDetails.Visible = false;
                    lnkNonTimeBookingDetails.Visible = true;
                }
            }
        }
        else
            EncryptQueryString("~/ModuleUI/Appointment/Receiving/APPRcv_DeliveryUnloaded.aspx?Scheduledate="
                           + txtDate.innerControltxtDate.Value + "&ID=" + hdnRecordTable.Value.ToString() + "-"
                           + hdnBookingFor.Value.ToString() + "-" + hdnPrimaryKey.Value + "-" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString());
    }

    protected void gridView_Sorting(Object sender, GridViewSortEventArgs e)
    {
        try
        {
            IsGridSort = true;
            string sortExpression = e.SortExpression;

            GridViewSortExp = sortExpression;
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                GridViewSortDirection = SortDirection.Descending;
                SortGridView(sortExpression, " ASC");
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
                SortGridView(sortExpression, " DESC");
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    private void SortGridView(string sortExpression, string direction)
    {
        DataTable dtSortingTbl;
        DataView dtView;
        try
        {
            if (ViewState["lstSites"] != null)
            {
                dtSortingTbl = (DataTable)ViewState["lstSites"];
                dtView = dtSortingTbl.DefaultView;
                if (sortExpression != "Weekday")
                {
                    int sortExpIndx = sortExpression.IndexOf(",");
                    if (sortExpIndx == -1)
                        dtView.Sort = sortExpression + direction;
                    else
                    {
                        dtView.Sort = sortExpression.Substring(0, sortExpIndx) + direction + sortExpression.Substring(sortExpIndx);
                    }
                }
                else
                {
                    if (direction.Trim() == "ASC")
                        dtView.Sort = "Weekday desc,OrderBY,DoorNumber";
                    else
                        dtView.Sort = "Weekday asc,OrderBY,DoorNumber";
                }
                if (sortExpression != "AutoID" && IsGridSort)
                    dtSortingTbl = GetSortByBooking(dtView.ToTable());
                else
                    dtSortingTbl = dtView.ToTable();

                string Role = Session["Role"].ToString().Trim().ToLower();
                GridView gridToParse = new GridView();
                if (Role != "vendor" && Role != "carrier")
                {
                    gridToParse = ucGridView1;
                }
                else
                {
                    gridToParse = gvVendor;
                }
                ViewState["lstSites"] = dtSortingTbl;
                gridToParse.DataSource = dtSortingTbl;
                gridToParse.DataBind();
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    private DataTable dtSortingTblByBooking = null;

    private DataTable GetSortByBooking(DataTable dtSortingTbl)
    {
        if (dtSortingTbl != null && dtSortingTbl.Rows.Count > 0)
        {
            if (dtSortingTblByBooking == null)
                dtSortingTblByBooking = dtSortingTbl.Clone();

            string bookingRef = dtSortingTbl.Rows[0]["BookingRef"].ToString();

            DataRow[] rows;
            rows = dtSortingTbl.Select("BookingRef = '" + bookingRef + "'");
            foreach (DataRow r in rows)
            {
                dtSortingTblByBooking.ImportRow(r);
                r.Delete();
            }
            dtSortingTblByBooking.AcceptChanges();

            GetSortByBooking(dtSortingTbl);
        }
        return dtSortingTblByBooking;
    }

    private string strPrevBKid = string.Empty;

    private string strPrevVendorPalletChecking = string.Empty;

    private string strPrevHazardousItemChecking = string.Empty;

    protected void ucGridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridViewRow gvr = e.Row;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (gvr.Cells[2].Text == "QC for PR")
            {
                gvr.Cells[2].Text = "Quality Checked";
            }

            HiddenField hdnBookingID = (HiddenField)e.Row.FindControl("hdnBookingID");
            HiddenField hdnWeekday = (HiddenField)e.Row.FindControl("hdnWeekday");
            RadioButton rdoBookingID = (RadioButton)e.Row.FindControl("rdoBookingID");
            HiddenField hdnConfirmPOArrival = (HiddenField)e.Row.FindControl("hdnConfirmPOArrival");

            #region Code to set visibility of [Time / Day] based on [Time Window Id] ..

            ucLiteral uclUcLiteral17 = (ucLiteral)e.Row.FindControl("UcLiteral17");
            ucLiteral uclUcLiteral17_1 = (ucLiteral)e.Row.FindControl("UcLiteral17_1");
            ucLiteral uclUcLiteral17_2 = (ucLiteral)e.Row.FindControl("UcLiteral17_2");

            if (uclUcLiteral17_1 != null && uclUcLiteral17_2 != null)
            {
                if (!string.IsNullOrEmpty(uclUcLiteral17_2.Text) && !string.IsNullOrWhiteSpace(uclUcLiteral17_2.Text))
                {
                    uclUcLiteral17_2.Visible = true;
                    uclUcLiteral17_1.Visible = false;
                    uclUcLiteral17.Visible = false;
                }
                else if (!string.IsNullOrEmpty(uclUcLiteral17_1.Text) && !string.IsNullOrWhiteSpace(uclUcLiteral17_1.Text))
                {
                    uclUcLiteral17_2.Visible = false;
                    uclUcLiteral17_1.Visible = true;
                    uclUcLiteral17.Visible = false;
                }
                else
                {
                    uclUcLiteral17_2.Visible = false;
                    uclUcLiteral17_1.Visible = false;
                    uclUcLiteral17.Visible = true;
                }
            }
            else { uclUcLiteral17.Visible = true; }

            #endregion Code to set visibility of [Time / Day] based on [Time Window Id] ..

            GridView gv = (GridView)sender;

            String ID = gv.ID;

            if (ID == "ucGridViewExcel")
            {
                string BookingStatus = e.Row.Cells[2].Text;
                Label lblVendorPalletCheckRequired1 = (Label)e.Row.FindControl("lblVendorPalletCheckingRequired");
                Label lblEnableHazardouesItemPrompt = (Label)e.Row.FindControl("lblEnableHazardouesItemPrompt");
                HiddenField hdnIsVendorPalletCheckingRequired = (HiddenField)e.Row.FindControl("hdnIsVendorPalletCheckingRequired");
                HiddenField hdnIsEnableHazardouesItemPrompt = (HiddenField)e.Row.FindControl("hdnIsEnableHazardouesItemPrompt");
                HiddenField hdnIsVenodrPalletChecking = (HiddenField)e.Row.FindControl("hdnIsVenodrPalletChecking_1");
                if (!string.IsNullOrEmpty(hdnIsVenodrPalletChecking.Value))
                {
                    if (strPrevBKid == hdnBookingID.Value.ToString().Trim() && strPrevVendorPalletChecking != hdnIsVenodrPalletChecking.Value.ToString().Trim())
                    {
                        if (ddlDisplayBy.SelectedItem.Value == "BYDELIVERY")
                        {
                            e.Row.Visible = false;
                        }
                    }
                }

                strPrevVendorPalletChecking = hdnIsVenodrPalletChecking.Value.ToString().Trim();

                if (hdnIsVendorPalletCheckingRequired.Value == "1")
                {
                    if (BookingStatus == "Confirmed Slot")
                    {
                        lblVendorPalletCheckRequired1.Text = IsVendorPalletCheckingForISPM15;
                    }
                    else if (BookingStatus == "Delivery Unloaded")
                    {
                        lblVendorPalletCheckRequired1.Text = VendorPalletCheckDone;
                    }
                }
                else
                {
                    lblVendorPalletCheckRequired1.Text = string.Empty;
                }
            }
            else
            {
                string BookingStatus = e.Row.Cells[2].Text;

                HiddenField hdnIsVenodrPalletChecking = (HiddenField)e.Row.FindControl("hdnIsVenodrPalletChecking");
                Label lblHazardousItemCheckRequired = (Label)e.Row.FindControl("lblHazardousItemCheckRequired");
                HiddenField hdnIsEnableHazardouesItemPrompt = (HiddenField)e.Row.FindControl("hdnIsEnableHazardouesItemPrompt");
                Label lblVendorPalletCheckRequired1 = (Label)e.Row.FindControl("lblVendorPalletCheckingRequired");

                 if (!string.IsNullOrEmpty(hdnIsVenodrPalletChecking.Value))
                {
                    if (strPrevBKid == hdnBookingID.Value.ToString().Trim() 
                        && strPrevVendorPalletChecking != hdnIsVenodrPalletChecking.Value.ToString().Trim()  
                        )
                    {
                        if (ddlDisplayBy.SelectedItem.Value == "BYDELIVERY")
                        {
                            e.Row.Visible = false;
                        }
                    }
                } 

                strPrevVendorPalletChecking = hdnIsVenodrPalletChecking.Value.ToString().Trim();
                strPrevHazardousItemChecking = hdnIsEnableHazardouesItemPrompt.Value;

                if (BookingStatus == "Confirmed Slot" || BookingStatus == "Confirm Window")
                {
                    lblVendorPalletCheckRequired1.Text = VendorPalletCheckRequired;
                    //if (hdnIsEnableHazardouesItemPrompt.Value == "True")
                    //    lblHazardousItemCheckRequired.Text = EnableHazardouesItemPrompt;
                }
                else if (BookingStatus == "Delivery Unloaded")
                {
                    lblVendorPalletCheckRequired1.Text = VendorPalletCheckDone;
                    if (hdnIsEnableHazardouesItemPrompt.Value == "True")
                        lblHazardousItemCheckRequired.Text = EnableHazardouesItemPromptChecking;
                }
                else
                {
                    lblVendorPalletCheckRequired1.Text = string.Empty;
                    lblHazardousItemCheckRequired.Text = string.Empty;
                }
            }

            ucGridView1.Columns[1].Visible = false;
            ucGridViewExcel.Columns[1].Visible = false;
            ucGridView1.Columns[19].Visible = false;
            ucGridViewExcel.Columns[19].Visible = false;

            Literal UcLiteral18 = (Literal)e.Row.FindControl("UcLiteral18");

            if (hdnWeekday.Value == "8")
            {
                UcLiteral18.Text = dtSeletedDate.ToString("dddd").ToUpper();
            }
            else if (hdnWeekday.Value == "9")
            {
                UcLiteral18.Text = dtSeletedDate.AddDays(-1).ToString("dddd").ToUpper();
            }
            else if (hdnWeekday.Value == "1")
            {
                UcLiteral18.Text = "SUNDAY";
            }
            else if (hdnWeekday.Value == "2")
            {
                UcLiteral18.Text = "MONDAY";
            }
            else if (hdnWeekday.Value == "3")
            {
                UcLiteral18.Text = "TUESDAY";
            }
            else if (hdnWeekday.Value == "4")
            {
                UcLiteral18.Text = "WEDNESDAY";
            }
            else if (hdnWeekday.Value == "5")
            {
                UcLiteral18.Text = "THURSDAY";
            }
            else if (hdnWeekday.Value == "6")
            {
                UcLiteral18.Text = "FRIDAY";
            }
            else if (hdnWeekday.Value == "7")
            {
                UcLiteral18.Text = "SATURDAY";
            }

            //-----------Sprint 3a Start ------------------//
            if (ddlDisplayBy.SelectedItem.Value == "BYPO")
            {
                ucGridView1.Columns[1].Visible = true;
                ucGridViewExcel.Columns[1].Visible = true;
                ucGridView1.Columns[19].Visible = true;
                ucGridViewExcel.Columns[19].Visible = true;

                switch (hdnConfirmPOArrival.Value)
                {
                    case "D":
                        for (int index = 0; index < e.Row.Cells.Count; index++)
                            e.Row.Cells[index].ForeColor = System.Drawing.Color.Green;

                        break;

                    case "ND":
                        for (int index = 0; index < e.Row.Cells.Count; index++)
                            e.Row.Cells[index].ForeColor = System.Drawing.Color.Red;
                        break;

                    default:
                        break;
                }
            }

            if (strPrevBKid == hdnBookingID.Value.ToString().Trim())
            {
                rdoBookingID.Visible = false;

                if (ID != "ucGridViewExcel")
                {
                    gvr.Cells[2].Text = string.Empty;
                }
                gvr.Cells[4].Text = string.Empty;
                gvr.Cells[5].Text = string.Empty;
                gvr.Cells[6].Text = string.Empty;
                gvr.Cells[7].Text = string.Empty;

                gvr.Cells[16].Text = string.Empty;
                gvr.Cells[17].Text = string.Empty;
                gvr.Cells[18].Text = string.Empty;

                gvr.Cells[20].Text = string.Empty;
                gvr.Cells[21].Text = string.Empty;
                gvr.Cells[22].Text = string.Empty;
            }

            strPrevBKid = hdnBookingID.Value.ToString().Trim();

            //-----------Sprint 3a End ------------------//

            string BookingComments = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "BookingComments"));
            if (string.IsNullOrWhiteSpace(BookingComments))
            {
                HtmlImage imgInfo = (HtmlImage)e.Row.FindControl("imgInfo");
                if (imgInfo != null)
                {
                    imgInfo.Visible = false;
                }
            }

            if (ddlSite.IsEnableHazardouesItemPrompt)
            {
                ucGridView1.Columns[4].Visible = true;
                ucGridViewExcel.Columns[4].Visible = true;
            }
            else
            {
                ucGridView1.Columns[4].Visible = false;
                ucGridViewExcel.Columns[4].Visible = false;
            }
        }
    }

    protected void ucGridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ucGridView1.PageIndex = e.NewPageIndex;

        //if (Session["BookingCritreria"] != null)
        //{
        //    Hashtable htBookingCriteria = (Hashtable)Session["BookingCritreria"];
        //    htBookingCriteria.Remove("PageIndex");
        //    htBookingCriteria.Add("PageIndex", e.NewPageIndex.ToString());
        //    Session["BookingCritreria"] = htBookingCriteria;
        //}

        try
        {
            BindBookings(e.NewPageIndex);
            //if (ViewState["lstSites"] != null)
            //{
            //    if (IsGridSort)
            //    {
            //        if (GridViewSortDirection == SortDirection.Ascending)
            //        {
            //            SortGridView(GridViewSortExp, " DESC");
            //        }
            //        else
            //        {
            //            SortGridView(GridViewSortExp, " ASC");
            //        }
            //    }
            //    else
            //    {
            //        if (GridViewSortDirection == SortDirection.Ascending)
            //        {
            //            SortGridView(GridViewSortExp, " ASC");
            //        }
            //        else
            //        {
            //            SortGridView(GridViewSortExp, " DESC");
            //        }
            //    }
            //}
            //else
            //{
            //    BindBookings(e.NewPageIndex);
            //}
        }
        catch { }
    }

    protected void gvVendor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvVendor.PageIndex = e.NewPageIndex;

        try
        {
            //if (ViewState["lstSites"] != null)
            //{
            //    if (GridViewSortDirection == SortDirection.Ascending && IsGridSort == true)
            //    {
            //        SortGridView(GridViewSortExp, " DESC");
            //    }
            //    else
            //    {
            //        SortGridView(GridViewSortExp, " ASC");
            //    }
            //}
            //else
            //{
            BindBookings(e.NewPageIndex);
            // }
        }
        catch { }
    }

    protected void gvVendor_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridViewRow gvr = e.Row;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (gvr.Cells[2].Text == "QC for PR")
            {
                gvr.Cells[2].Text = "Quality Checked";
            }

            #region Code to set visibility of [Time / Day] based on [Time Window Id] ..

            ucLiteral UcLiteral27 = (ucLiteral)e.Row.FindControl("UcLiteral27");
            ucLiteral UcLiteral27_1 = (ucLiteral)e.Row.FindControl("UcLiteral27_1");
            ucLiteral UcLiteral27_2 = (ucLiteral)e.Row.FindControl("UcLiteral27_2");

            if (!string.IsNullOrEmpty(UcLiteral27_1.Text) && !string.IsNullOrWhiteSpace(UcLiteral27_1.Text))
            {
                UcLiteral27_2.Visible = false;
                UcLiteral27_1.Visible = true;
                UcLiteral27.Visible = false;
            }
            else if (!string.IsNullOrEmpty(UcLiteral27_2.Text) && !string.IsNullOrWhiteSpace(UcLiteral27_2.Text))
            {
                UcLiteral27_2.Visible = true;
                UcLiteral27_1.Visible = false;
                UcLiteral27.Visible = false;
            }
            else
            {
                UcLiteral27_2.Visible = false;
                UcLiteral27_1.Visible = false;
                UcLiteral27.Visible = true;
            }

            #endregion Code to set visibility of [Time / Day] based on [Time Window Id] ..

            string BookingStatus = e.Row.Cells[2].Text;
            Label lblVendorPalletCheckRequired_1 = (Label)e.Row.FindControl("lblVendorPalletCheckingRequired_1");

            if (BookingStatus == "Confirmed Slot")
            {
                lblVendorPalletCheckRequired_1.Text = VendorPalletCheckRequired;
            }
            else if (BookingStatus == "Delivery Unloaded")
            {
                lblVendorPalletCheckRequired_1.Text = VendorPalletCheckDone;
            }
            else
            {
                lblVendorPalletCheckRequired_1.Text = string.Empty;
            }

            //Stage6 V2 point 23
            HiddenField hdnSupplierType = (HiddenField)e.Row.FindControl("hdnSupplierType");
            string Role = Session["Role"].ToString().Trim().ToLower();
            if (Role == "vendor" && hdnSupplierType.Value == "C")
            {
                e.Row.CssClass = "greencell";
            }

            string BookingComments = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "BookingComments"));
            if (string.IsNullOrWhiteSpace(BookingComments))
            {
                HtmlImage imgInfo = (HtmlImage)e.Row.FindControl("imgInfo");
                imgInfo.Visible = false;
            }
        }
    }

    protected void btnNoShow_Click(object sender, EventArgs e)
    {
        lblShowDeliveryMessage.Text = strShowDeliveryMessage;
        mdNoShow.Show();
        if (imgAlternateView.Visible == false)
        {
            GenerateAlternateSlot(false);
        }
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        APPBOK_BookingBE bookingBE = new APPBOK_BookingBE();
        int? iResponce = 0;
        this.mdNoShow.Hide();
        btnNoShow.Enabled = false;
        btnNoShow.CssClass = "button-readonly";

        if (!string.IsNullOrWhiteSpace(hdnPrimaryKey.Value))
        {
            bookingBE.Action = "UpdateBookingStatusById";
            bookingBE.BookingID = Convert.ToInt32(hdnPrimaryKey.Value);
            bookingBE.UserID = Convert.ToInt32(Session["UserID"]);
            APPBOK_BookingBAL appBOK_BookingBAL = new APPBOK_BookingBAL();
            iResponce = appBOK_BookingBAL.UpdateBookingStatusByIdBAL(bookingBE);

            this.BindBookings();

            if (iResponce > 0)
            {
                ShowInformation();
            }
        }

        if (imgAlternateView.Visible == false)
        {
            GenerateAlternateSlot(false);
        }
    }

    private void ShowInformation()
    {
        try
        {
            APPBOK_BookingBE noShowDeliveriesBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

            noShowDeliveriesBE.Action = "GetNoShowDeliveryLetterData";
            noShowDeliveriesBE.BookingID = Convert.ToInt32(hdnPrimaryKey.Value);
            noShowDeliveriesBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            List<APPBOK_BookingBE> lstRefusedDeliveries = oAPPBOK_BookingBAL.GetNoShowDeliveryLetterBAL(noShowDeliveriesBE);

            ViewState["lstRefusedDeliveries"] = lstRefusedDeliveries;

            List<APPBOK_BookingBE> lstCarrier = lstRefusedDeliveries.Where(c => c.Vendor.VendorID == 0).Take(1).ToList();
            if (lstCarrier.Count > 0)
            {
                chkCarrier.Text = lstCarrier[0].Carrier.CarrierName;
            }

            var lstVendor = lstRefusedDeliveries.Where(c => c.Vendor.VendorID != 0).Select(bok => new { VendorName = bok.Vendor.VendorName, VendorID = bok.Vendor.VendorID }).Distinct().ToList();

            if (lstVendor.Count > 0)
            {
                chkListVendors.DataTextField = "VendorName";
                chkListVendors.DataValueField = "VendorID";
                chkListVendors.DataSource = lstVendor;
                chkListVendors.DataBind();

                foreach (ListItem item in chkListVendors.Items)
                {
                    item.Selected = true;
                }
            }

            if (lstVendor.Count > 1)
            {
                mdInformation.Show();
            }
            else
            {
                InvokeSendMail();
            }
        }
        catch
        {
        }
    }

    public delegate void MethodInvoker();

    protected void btnOK_Click(object sender, EventArgs e)
    {
        mdInformation.Hide();

        InvokeSendMail();
    }

    private void InvokeSendMail()
    {
        MethodInvoker simpleDelegate = new MethodInvoker(SendNoShowMails);
        // Calling SendNoShowMails Async
        simpleDelegate.BeginInvoke(null, null);
    }

    private StringBuilder sbMessage = new StringBuilder();

    private void SendNoShowMails()
    {
        sbMessage.AppendLine("----------------------------------------");
        sbMessage.AppendLine("1.1. Start SendNoShowMails");

        APPBOK_BookingBE stockPlannerEmailBE = new APPBOK_BookingBE();
        stockPlannerEmailBE.BookingID = Convert.ToInt32(hdnPrimaryKey.Value);
        stockPlannerEmailBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        sbMessage.AppendLine("1.2.BookingID :- " + Convert.ToString(hdnPrimaryKey.Value));
        sbMessage.AppendLine("1.3.SiteId :- " + Convert.ToString(ddlSite.innerControlddlSite.SelectedItem.Value));

        APPBOK_CommunicationBAL communicationBAL = new APPBOK_CommunicationBAL();
        List<MAS_LanguageBE> lstLanguages = communicationBAL.GetLanguages();

        sbMessage.AppendLine("1.4. End SendNoShowMails");
        sbMessage.AppendLine("----------------------------------------");
        LogUtility.SaveTraceLogEntry(sbMessage);
        sbMessage.Clear();

        this.SendEmailToNoShowDeliveryUsers(lstLanguages);
        this.SendEmailToNoShowStockPlannerUsers(stockPlannerEmailBE, lstLanguages);
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        mdNoShow.Hide();
        if (imgAlternateView.Visible == false)
        {
            GenerateAlternateSlot(false);
        }
    }

    // In this method need to set data.
    private void SendEmailToNoShowDeliveryUsers(List<MAS_LanguageBE> lstLanguages)
    {
        try
        {
            string mailBody = string.Empty;
            string mailSubject = "No Show Delivery - Letter";
            string fromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

            List<APPBOK_BookingBE> lstRefusedDeliveries = (List<APPBOK_BookingBE>)ViewState["lstRefusedDeliveries"];

            bool isVendorMailSent = false;

            foreach (ListItem a in chkListVendors.Items)
            {
                if (a.Selected)
                {
                    isVendorMailSent = true;
                    break;
                }
            }

            clsEmail oclsEmail = new clsEmail();

            bool isCarrierMailSent = false;

            foreach (ListItem a in chkListVendors.Items)
            {
                {
                    List<APPBOK_BookingBE> lstCarreirList = new List<APPBOK_BookingBE>();

                    if (a.Selected || (isVendorMailSent == false && isCarrierMailSent == false && chkCarrier.Checked))
                    {
                        List<APPBOK_BookingBE> lstVendorList = new List<APPBOK_BookingBE>();

                        if (isVendorMailSent == true)
                            lstVendorList = lstRefusedDeliveries.Where(v => v.Vendor.VendorID == Convert.ToInt32(a.Value)).ToList();

                        if (isCarrierMailSent == false && chkCarrier.Checked)
                        {
                            lstCarreirList = lstRefusedDeliveries.Where(v => v.Vendor.VendorID == 0).ToList();
                            lstVendorList.AddRange(lstCarreirList);
                            isCarrierMailSent = true;
                        }

                        foreach (APPBOK_BookingBE ab in lstVendorList)
                        {
                            foreach (MAS_LanguageBE objLanguage in lstLanguages)
                            {
                                bool MailSentInLanguage = false;
                                string toAddress = string.Empty;

                                if (!string.IsNullOrWhiteSpace(ab.Vendor.VendorContactEmail))
                                {
                                    toAddress = ab.Vendor.VendorContactEmail;
                                    if (string.IsNullOrEmpty(ab.Language))
                                    {
                                        var varLanguage = lstLanguages.Find(x => x.Language.Equals("English"));
                                        ab.Language = varLanguage.Language;
                                        ab.LanguageID = varLanguage.LanguageID;
                                    }
                                }
                                else
                                {
                                    toAddress = ConfigurationManager.AppSettings["DefaultEmailAddress"];
                                    if (string.IsNullOrEmpty(ab.Language))
                                    {
                                        var varLanguage = lstLanguages.Find(x => x.Language.Equals("English"));
                                        ab.Language = varLanguage.Language;
                                        ab.LanguageID = varLanguage.LanguageID;
                                    }
                                }

                                if (objLanguage.Language.ToLower() == ab.Language.ToLower())
                                {
                                    MailSentInLanguage = true;
                                }

                                mailBody = this.NoShowDeliveryEmailBody(ab, objLanguage.Language.ToLower(), objLanguage.LanguageID);

                                CommunicationType.Enum communicationType = CommunicationType.Enum.NoShow;

                                #region First Saving data of [AddBookingCommunication] Then sending email.

                                APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                                APPBOK_CommunicationBE oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
                                oAPPBOK_CommunicationBE.Action = "AddBookingCommunication";
                                oAPPBOK_CommunicationBE.CommunicationType = CommunicationType.Name(communicationType);
                                oAPPBOK_CommunicationBE.BookingID = Convert.ToInt32(hdnPrimaryKey.Value);
                                oAPPBOK_CommunicationBE.Subject = mailSubject;
                                oAPPBOK_CommunicationBE.SentFrom = fromAddress;
                                oAPPBOK_CommunicationBE.SentTo = toAddress;
                                oAPPBOK_CommunicationBE.Body = mailBody;
                                oAPPBOK_CommunicationBE.SendByID = Convert.ToInt32(Session["UserID"]);
                                oAPPBOK_CommunicationBE.IsMailSent = MailSentInLanguage;
                                oAPPBOK_CommunicationBE.LanguageID = objLanguage.LanguageID;
                                oAPPBOK_CommunicationBE.MailSentInLanguage = MailSentInLanguage;
                                int intIsExist = oAPPBOK_CommunicationBAL.AddBookingCommunicationBAL(oAPPBOK_CommunicationBE) ?? 0;
                                if (intIsExist.Equals(0) && MailSentInLanguage.Equals(true))
                                {
                                    oclsEmail.sendMail(toAddress, mailBody, mailSubject, fromAddress, true);
                                }

                                #endregion First Saving data of [AddBookingCommunication] Then sending email.
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            string errorMessage = ex.Message;
        }
    }

    private void SendEmailToNoShowStockPlannerUsers(APPBOK_BookingBE oAPPBOK_BookingBE, List<MAS_LanguageBE> lstLanguages)
    {
        sbMessage.AppendLine("----------------------------------------");
        sbMessage.AppendLine("2.1. Start SendEmailToNoShowStockPlannerUsers");

        APPBOK_BookingBE noShowDeliveriesBE = null;
        APPBOK_BookingBE finalNoShowDeliveriesBE = null;
        APPBOK_BookingBAL oAPPBOK_BookingBAL = null;
        try
        {
            finalNoShowDeliveriesBE = new APPBOK_BookingBE();
            oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            string mailBody = string.Empty;
            string mailSubject = "No Show Delivery - Letter";
            string fromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

            noShowDeliveriesBE = new APPBOK_BookingBE();
            noShowDeliveriesBE.Action = "GetNoShowSPUsers";
            noShowDeliveriesBE.BookingID = oAPPBOK_BookingBE.BookingID;
            noShowDeliveriesBE.SiteId = oAPPBOK_BookingBE.SiteId;

            List<APPBOK_BookingBE> lstNoShowSPDeliveries = oAPPBOK_BookingBAL.GetNoShowSPUsersBAL(noShowDeliveriesBE);

            APPBOK_BookingBE objPurchaseOrderBE = new APPBOK_BookingBE();
            objPurchaseOrderBE.Action = "GetPurchaseOrderForBooking";
            objPurchaseOrderBE.BookingID = oAPPBOK_BookingBE.BookingID;

            List<APPBOK_BookingBE> lstPurchaseOrder = oAPPBOK_BookingBAL.GetPurchaseOrderForBookingBAL(objPurchaseOrderBE);

            clsEmail oclsEmail = new clsEmail();

            sbMessage.AppendLine("2.2. lstPurchaseOrder count :" + Convert.ToString(lstPurchaseOrder.Count));
            sbMessage.AppendLine("2.3. lstNoShowSPDeliveries count :" + Convert.ToString(lstNoShowSPDeliveries.Count));

            foreach (APPBOK_BookingBE item in lstNoShowSPDeliveries)
            {
                string StockPlannerEmailList = item.StockPlannerEmail.ToString();

                foreach (MAS_LanguageBE objLanguage in lstLanguages)
                {
                    bool MailSentInLanguage = false;

                    if (objLanguage.Language.ToLower() == item.Language.ToLower())
                    {
                        MailSentInLanguage = true;
                        mailBody = this.NoShowSPEmailBody(item, StockPlannerEmailList, lstPurchaseOrder, objLanguage.Language.ToLower(), objLanguage.LanguageID);
                    }

                    string toAddress = string.Empty;

                    if (MailSentInLanguage.Equals(true))
                    {
                        sbMessage.AppendLine("2.4. StockPlannerEmail :" + item.StockPlannerEmail);

                        if (!string.IsNullOrWhiteSpace(item.StockPlannerEmail))
                            toAddress = item.StockPlannerEmail;
                        else
                            toAddress = ConfigurationManager.AppSettings["DefaultEmailAddress"];

                        sbMessage.AppendLine("2.5. Send Email To :" + toAddress);

                        oclsEmail.sendMail(toAddress, mailBody, mailSubject, fromAddress, true);

                        sbMessage.AppendLine("2.6. Send Email to stock palnner completed");
                    }
                }
            }

            sbMessage.AppendLine("----------------------------------------");
            LogUtility.SaveTraceLogEntry(sbMessage);
            sbMessage.Clear();
        }
        catch (Exception ex)
        {
            string errorMessage = ex.Message;
        }
    }

    private string NoShowDeliveryEmailBody(APPBOK_BookingBE oAPPBOK_BookingBE, string language, int languageID)
    {
        string htmlBody = string.Empty;
        string templatePath = string.Empty;
        string LanguageFile = string.Empty;
        sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePathName;
        LanguageFile = "NoShowDelivery.english.htm";

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            #region mailBody

            htmlBody = sReader.ReadToEnd();
            // OD logo.
            htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
            // OD Address.
            htmlBody = htmlBody.Replace("{OfficeDepotAddressValue}", oAPPBOK_BookingBE.SiteAddress);
            // Current Date.
            htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValueByLangID("Date", languageID));
            DateTime date = oAPPBOK_BookingBE.DeliveryDate ?? DateTime.Now.Date;
            string strCurrentDate = date.ToString("dd-MM-yyyy").Replace("-", "/");
            htmlBody = htmlBody.Replace("{DateValue}", strCurrentDate);
            // Vendor Details.
            htmlBody = htmlBody.Replace("{VendorCodeValue}", oAPPBOK_BookingBE.Vendor.Vendor_No);
            htmlBody = htmlBody.Replace("{VendorNameValue}", oAPPBOK_BookingBE.Vendor.VendorName);
            string strVendorAddress = string.Format("{0}<br />{1}<br />{2}", oAPPBOK_BookingBE.Vendor.address1,
                oAPPBOK_BookingBE.Vendor.address2, oAPPBOK_BookingBE.Vendor.city);
            htmlBody = htmlBody.Replace("{VendorAddressValue}", strVendorAddress);

            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValueByLangID("DearSirMadam", languageID));
            htmlBody = htmlBody.Replace("{NoShowDelText1}", WebCommon.getGlobalResourceValueByLangID("NoShowDelText1", languageID));
            htmlBody = htmlBody.Replace("{NoShowDelText2}", WebCommon.getGlobalResourceValueByLangID("NoShowDelText2", languageID));
            htmlBody = htmlBody.Replace("{NoShowDelText3}", WebCommon.getGlobalResourceValueByLangID("NoShowDelText3", languageID));

            htmlBody = htmlBody.Replace("{BookingReference}", WebCommon.getGlobalResourceValueByLangID("BookingReference", languageID));
            htmlBody = htmlBody.Replace("{BookingRefValue}", oAPPBOK_BookingBE.BookingRef);

            htmlBody = htmlBody.Replace("{SiteName}", WebCommon.getGlobalResourceValueByLangID("SiteName", languageID));
            htmlBody = htmlBody.Replace("{SiteNameValue}", oAPPBOK_BookingBE.SiteName);

            #region Logic to set Schedule DateTime like Booking

            string strDeliveryDate = string.Empty;
            string strDeliveryTime = string.Empty;
            string strScheduleDate = string.Empty;
            if (oAPPBOK_BookingBE.ScheduleDate != null)
            {
                strDeliveryDate = oAPPBOK_BookingBE.ScheduleDate.Value.ToString("dd/MM/yyyy");
                string strWeekDay = oAPPBOK_BookingBE.ScheduleDate.Value.DayOfWeek.ToString().ToUpper();
                switch (strWeekDay)
                {
                    case "SUNDAY":
                        if (oAPPBOK_BookingBE.WeekDay != 1)
                            strDeliveryDate = oAPPBOK_BookingBE.ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;

                    case "MONDAY":
                        if (oAPPBOK_BookingBE.WeekDay != 2)
                            strDeliveryDate = oAPPBOK_BookingBE.ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;

                    case "TUESDAY":
                        if (oAPPBOK_BookingBE.WeekDay != 3)
                            strDeliveryDate = oAPPBOK_BookingBE.ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;

                    case "WEDNESDAY":
                        if (oAPPBOK_BookingBE.WeekDay != 4)
                            strDeliveryDate = oAPPBOK_BookingBE.ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;

                    case "THURSDAY":
                        if (oAPPBOK_BookingBE.WeekDay != 5)
                            strDeliveryDate = oAPPBOK_BookingBE.ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;

                    case "FRIDAY":
                        if (oAPPBOK_BookingBE.WeekDay != 6)
                            strDeliveryDate = oAPPBOK_BookingBE.ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;

                    case "SATURDAY":
                        if (oAPPBOK_BookingBE.WeekDay != 7)
                            strDeliveryDate = oAPPBOK_BookingBE.ScheduleDate.Value.AddDays(-1).ToString("dd/MM/yyyy");
                        break;
                }
            }

            if (oAPPBOK_BookingBE.BookingTypeID != 3)
                strDeliveryTime = oAPPBOK_BookingBE.SlotTime.SlotTime.Replace("-", "/");
            else
                strDeliveryTime = WebCommon.getGlobalResourceValueByLangID("NonTimeDelivery", languageID); ;

            #endregion Logic to set Schedule DateTime like Booking

            strScheduleDate = string.Format("{0} {1}", strDeliveryDate, strDeliveryTime);

            htmlBody = htmlBody.Replace("{ScheduledDateTime}", WebCommon.getGlobalResourceValueByLangID("ScheduledDateTime", languageID));
            htmlBody = htmlBody.Replace("{ScheduledDateTimeValue}", strScheduleDate);

            htmlBody = htmlBody.Replace("{YoursSincerely}", WebCommon.getGlobalResourceValueByLangID("YoursSincerely", languageID));
            htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValueByLangID("OfficeDepots", languageID));

            #endregion mailBody
        }
        return htmlBody;
    }

    private string NoShowSPEmailBody(APPBOK_BookingBE oAPPBOK_BookingBE, string StockPlannerEmailList, List<APPBOK_BookingBE> lstPurchaseOrder, string language, int languageID)
    {
        string htmlBody = string.Empty;
        string templatePath = string.Empty;
        string LanguageFile = string.Empty;
        sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();

        templatePath = path + templatePathName;
        LanguageFile = string.Format("NoShowToSP.{0}", "english.htm");

        string PurchaseOrder = WebCommon.getGlobalResourceValueByLangID("PurchaseOrder", languageID);
        string Vendor = WebCommon.getGlobalResourceValueByLangID("Vendor", languageID);

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            #region mailBody

            htmlBody = sReader.ReadToEnd();
            // OD logo.
            htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

            htmlBody = htmlBody.Replace("{NoShowToSPText1}", WebCommon.getGlobalResourceValueByLangID("NoShowToSPText1", languageID));
            htmlBody = htmlBody.Replace("{NoShowToSPText2}", WebCommon.getGlobalResourceValueByLangID("NoShowToSPText2", languageID));
            htmlBody = htmlBody.Replace("{EmailAddressesValue}", StockPlannerEmailList.TrimEnd(','));
            htmlBody = htmlBody.Replace("{DeliveryDetailsHistory}", WebCommon.getGlobalResourceValueByLangID("DeliveryDetailsHistory", languageID));

            htmlBody = htmlBody.Replace("{Site}", WebCommon.getGlobalResourceValueByLangID("Site", languageID));
            htmlBody = htmlBody.Replace("{SiteValue}", oAPPBOK_BookingBE.SiteName);

            htmlBody = htmlBody.Replace("{BookingReference}", WebCommon.getGlobalResourceValueByLangID("BookingReference", languageID));
            htmlBody = htmlBody.Replace("{BookingRefValue}", oAPPBOK_BookingBE.BookingRef);

            htmlBody = htmlBody.Replace("{Pallets}", WebCommon.getGlobalResourceValueByLangID("Pallets", languageID));
            htmlBody = htmlBody.Replace("{PalletsValue}", Convert.ToString(oAPPBOK_BookingBE.NumberOfPallet));

            htmlBody = htmlBody.Replace("{Cartons}", WebCommon.getGlobalResourceValueByLangID("Cartons", languageID));
            htmlBody = htmlBody.Replace("{CartonsValue}", Convert.ToString(oAPPBOK_BookingBE.NumberOfCartons));

            htmlBody = htmlBody.Replace("{Lines}", WebCommon.getGlobalResourceValueByLangID("Lines", languageID));
            htmlBody = htmlBody.Replace("{LinesValue}", Convert.ToString(oAPPBOK_BookingBE.NumberOfLines));

            htmlBody = htmlBody.Replace("{Vendor}", WebCommon.getGlobalResourceValueByLangID("Vendor", languageID));
            htmlBody = htmlBody.Replace("{VendorValue}", oAPPBOK_BookingBE.Vendor.VendorName);

            htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValueByLangID("Carrier", languageID));
            htmlBody = htmlBody.Replace("{CarrierValue}", oAPPBOK_BookingBE.Carrier.CarrierName);

            htmlBody = htmlBody.Replace("{Bookedinfor}", WebCommon.getGlobalResourceValueByLangID("Bookedinfor", languageID));
            // Current Date.
            DateTime date = oAPPBOK_BookingBE.ScheduleDate ?? DateTime.Now.Date;
            string strCurrentDate = date.ToString("dd-MM-yyyy").Replace("-", "/");
            htmlBody = htmlBody.Replace("{ScheduledDateTimeValue}", strCurrentDate);

            htmlBody = htmlBody.Replace("{NoShowsentat}", WebCommon.getGlobalResourceValueByLangID("NoShowsentat", languageID));
            if (!string.IsNullOrEmpty(oAPPBOK_BookingBE.HistoryDate))
            {
                htmlBody = htmlBody.Replace("{NoShowScheduledDateTimeValue}", oAPPBOK_BookingBE.HistoryDate);
            }
            else
            {
                htmlBody = htmlBody.Replace("{NoShowScheduledDateTimeValue}", "");
            }

            htmlBody = htmlBody.Replace("{SetBy}", WebCommon.getGlobalResourceValueByLangID("SetBy", languageID));
            htmlBody = htmlBody.Replace("{UserNameValue}", oAPPBOK_BookingBE.UserName);

            htmlBody = htmlBody.Replace("{PurchaseOrders}", WebCommon.getGlobalResourceValueByLangID("PurchaseOrders", languageID));
            StringBuilder sProductDetail = new StringBuilder();
            sProductDetail.Append("<table style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=50%'>");

            sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + PurchaseOrder + "</cc1:ucLabel></td><td width='85%'>" + Vendor + "</td></tr>");
            for (int i = 0; i < lstPurchaseOrder.Count; i++)
            {
                sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstPurchaseOrder[i].PurchaseOrders + "</td><td>" + lstPurchaseOrder[i].Vendor.VendorName + "</td><td></tr>");
            }

            sProductDetail.Append("</table>");

            if (!string.IsNullOrEmpty(sProductDetail.ToString()))
                htmlBody = htmlBody.Replace("{PurchaseOrdersValue}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br />" + sProductDetail.ToString() + "</td></tr>");
            else
                htmlBody = htmlBody.Replace("{PurchaseOrdersValue}", "");

            #endregion mailBody
        }
        return htmlBody;
    }

    protected void btnAddByPOBooking_Click(object sender, EventArgs e)
    {
        string Role = Session["Role"].ToString().Trim().ToLower();
        if (Role != "vendor" && Role != "carrier")
        {
            EncryptQueryString("APPBok_BookingByPOEdit.aspx?Mode=Add&Scheduledate=" + txtDate.innerControltxtDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + ddlSite.PreCountryID.ToString());
        }
        else
        {
            if (txtDate.Visible)
            {
                EncryptQueryString("APPBok_BookingByPOEdit.aspx?Mode=Add&Scheduledate=" + txtDate.innerControltxtDate.Value + "&Logicaldate=" + txtDate.innerControltxtDate.Value + "&SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&SiteCountryID=" + ddlSite.PreCountryID.ToString());
            }
            else
            {
                EncryptQueryString("APPBok_BookingByPOEdit.aspx?Mode=Add");
            }
        }
    }

    private MAS_SiteBE GetSingleSiteSetting(int siteId)
    {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSingleSiteSetting";
        oMAS_SiteBE.SiteID = siteId;
        MAS_SiteBE localMAS_SiteBE = oMAS_SiteBAL.GetSingleSiteSettingBAL(oMAS_SiteBE);
        return localMAS_SiteBE;
    }

    protected void btnConfirmPOArrival_Click(object sender, EventArgs e)
    {
        string Role = Session["Role"].ToString().Trim().ToLower();
        if (!Role.Equals("vendor") && !Role.Equals("carrier"))
            EncryptQueryString("APPBok_ConfirmPurchaseOrderArrival.aspx?BookId=" + Convert.ToInt32(hdnPrimaryKey.Value));
    }

    protected void imgReport_Click(object sender, ImageClickEventArgs e)
    {
        DateTime ScheduleDate = txtDate.GetDate;
        string SiteId = ddlSite.innerControlddlSite.SelectedValue.ToString();
        EncryptQueryString("../Reports/SchedulePOLinesReport.aspx?siteid=" + SiteId + "&scheduledate=" + ScheduleDate.ToString("dd/MM/yyyy"));
    }

    protected void imgAlternateReport_Click(object sender, ImageClickEventArgs e)
    {
        pnlWindow.Visible = false;
        hdnAlternateViewStatus.Value = "1";
        GenerateVolumeDetails();
        GenerateAlternateSlot(false);
        divPrint.Visible = false;
        pnlVolumeDetails_1.Visible = true;
        tblAlternate.Visible = true;
        pnlLinks.Visible = true;
        tdStatus1.Visible = false;
        tdStatus2.Visible = false;
        tdStatus3.Visible = false;
        trAlternateHide.Visible = false;
        tdVendor1.Visible = false;
        tdVendor2.Visible = false;
        tdVendor3.Visible = false;
        tdBookingRef1.Visible = false;
        tdBookingRef2.Visible = false;
        tdBookingRef3.Visible = false;
        tdIcon.Visible = false;
        pnlVolumeDetails_1.Visible = true;
        pnlLegend_5.Visible = true;
        tableDiv_GeneralNew.Visible = true;

        //LinkButton for dispaly pnllinks
        lnkGoto.Visible = true;
        lnkGotoWindow.Visible = false;
        lnkLegends.Style.Add("display", "block");
        lblVendorNoBooking.Visible = false;
        lnkNonStandardWindowBookingDetails.Visible = false;
        string Role = Session["Role"].ToString().Trim().ToLower();
        skuCatCode.Visible = false;
    }

    protected void btnOK_1_Click(object sender, EventArgs e)
    {
        mdAshtonClose.Hide();
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static List<string[]> getdisabledDays(int imonth, int iyear, bool isPageLoad)
    {
        return ScheduleAvailability.getdisabledDays(imonth, iyear, isPageLoad);
    }

    private void CheckSchedulingClosedown()
    {
        MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE = new MASSIT_SchedulingClosedownBE();
        MASSIT_SchedulingClosedownBAL oMASSIT_SchedulingClosedownBAL = new MASSIT_SchedulingClosedownBAL();
        List<MASSIT_SchedulingClosedownBE> lstSchedulingClosedownBE = new List<MASSIT_SchedulingClosedownBE>();

        if (ViewState["lstSchedulingClosedownBE"] == null)
        {
            oMASSIT_SchedulingClosedownBE.Action = "ShowAll";
            lstSchedulingClosedownBE = oMASSIT_SchedulingClosedownBAL.GetSchedulingClosedownBAL(oMASSIT_SchedulingClosedownBE);
        }
        else
        {
            lstSchedulingClosedownBE = (List<MASSIT_SchedulingClosedownBE>)ViewState["lstSchedulingClosedownBE"];
        }

        btnAddVendorBooking.Enabled = true;
        btnAddVendorBooking.CssClass = "button";

        bool isAnySiteClosedFound = false;

        string Role = Session["Role"].ToString().Trim().ToLower();

        hdnDisableSites.Value = "," + (string.Join(",", lstSchedulingClosedownBE.Select(x => x.SiteID.ToString()).ToArray())) + ",";

        if (lstSchedulingClosedownBE != null && lstSchedulingClosedownBE.Count > 0)
        {
            for (int icount = 0; icount < lstSchedulingClosedownBE.Count; icount++)
            {
                if (ddlSiteCarrier.innerControlddlSite.SelectedItem.Value == lstSchedulingClosedownBE[icount].SiteID.ToString())
                {
                    mdAshtonClose.Show();
                    ltwanrningmesaage.Text = lstSchedulingClosedownBE[icount].WarningMessage;

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "disable", "<script>dispablebuttons('" + ddlSiteCarrier.innerControlddlSite.SelectedItem.Value + "','" + Role + "');</script>", false);

                    isAnySiteClosedFound = true;

                    break;
                }
            }
        }

        DisableEnableBookingButtons(!isAnySiteClosedFound);
    }

    private void DisableEnableBookingButtons(bool MakeEnable)
    {
        btnAddVendorBooking.Enabled = btnVendorEditBooking.Enabled = btnDeleteBooking.Enabled = btnConfirmFixedSlot.Enabled = btnDeleteFixedSlot.Enabled = MakeEnable;
        btnAddVendorBooking.CssClass = btnVendorEditBooking.CssClass = btnDeleteBooking.CssClass = btnConfirmFixedSlot.CssClass = btnDeleteFixedSlot.CssClass = MakeEnable == false ? "button-readonly" : "button";

        ScriptManager.RegisterStartupScript(this, this.GetType(), "css", "<script>setReadonlyClass();</script>", false);
    }

    public DateTime LogicalSchedulDateTime
    {
        get
        {
            return ViewState["LogicalSchedulDateTime"] != null ? Convert.ToDateTime(ViewState["LogicalSchedulDateTime"].ToString()) : new DateTime(1900, 01, 01, 0, 0, 00);
        }
        set
        {
            ViewState["LogicalSchedulDateTime"] = value;
        }
    }

    public string strOpenDoorIds
    {
        get
        {
            return ViewState["strOpenDoorIds"] != null ? ViewState["strOpenDoorIds"].ToString() : ",";
        }
        set
        {
            ViewState["strOpenDoorIds"] = value;
        }
    }

    public int iSlotTimeLength = 15;//changes from 5 to 15
    //-----------------------------------//

    private DataTable dtAlternateSlot;


    private List<APPBOK_BookingBE> lstAllFixedDoor = new List<APPBOK_BookingBE>();
    private int iTimeSlotID, iSiteDoorID, iPallets, iCartons, iLifts, iLines, actSlotLength, iNextSlotLength, iNextSlotLength2, iTimeSlotOrderBYID;
    private bool isClickable, isCurrentSlotFixed, isNextSlotCovered, isCurrentSlotCovered;
    private string DoorIDs = ",";
    private string sTimeSlot = "";
    private string sDoorNo;
    private string sVendorCarrierName = "";
    private string NextRequiredSlot = string.Empty;
    private string sVendorCarrierNameNextSlot = string.Empty;
    private string sCurrentFixedSlotID = "-1";
    private string sSlotStartDay = string.Empty;
    private string sVendorCarrierNameNew = string.Empty;
    private string AllocationType = string.Empty;
    private string SiteDoorNumberID = string.Empty;

    private string GetDoorConstraints()
    {
        string OpenDoorIds = string.Empty;
        LogicalSchedulDateTime = Utilities.Common.TextToDateFormat(txtDate.innerControltxtDate.Value);
        if (txtDate.innerControltxtDate.Value != string.Empty && ddlSite.innerControlddlSite.SelectedItem.Value != string.Empty)
        {
            bool IsSpecificWeekDefined = false;

            DateTime dtSelectedDate = LogicalSchedulDateTime;
            string WeekDay = dtSelectedDate.ToString("dddd");
            DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);

            MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
            MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

            oMASSIT_WeekSetupBE.Action = "GetSpecificWeek";
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_WeekSetupBE.EndWeekday = WeekDay;
            oMASSIT_WeekSetupBE.ScheduleDate = dtSelectedDate;

            List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup != null && lstWeekSetup.Count > 0 && lstWeekSetup[0].StartTime != null)
            {
                IsSpecificWeekDefined = true;
            }

            //------Check for Specific Entry First--------//
            APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
            MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

            APPSIT_DoorOpenTimeSpecificBAL oAPPSIT_DoorOpenTimeSpecificBAL = new APPSIT_DoorOpenTimeSpecificBAL();
            MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE = new MASSIT_DoorOpenTimeSpecificBE();
            oMASSIT_DoorOpenTimeSpecificBE.Action = "GetDoorConstraintsSpecific";
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime = new MASSIT_DoorOpenTimeBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday = WeekDay;
            oMASSIT_DoorOpenTimeSpecificBE.WeekStartDate = dtMon;
            List<MASSIT_DoorOpenTimeSpecificBE> lstDoorConstraintsSpecific = oAPPSIT_DoorOpenTimeSpecificBAL.GetDoorConstraintsSpecificBAL(oMASSIT_DoorOpenTimeSpecificBE);
            if (IsSpecificWeekDefined == true)
            {
                if (lstDoorConstraintsSpecific.Count > 0)
                { //Get Specific settings
                    for (int iCount = 0; iCount < lstDoorConstraintsSpecific.Count; iCount++)
                    {
                        OpenDoorIds += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + ",";
                    }
                }
                else
                { //Get Generic settings
                    oMASSIT_DoorOpenTimeBE.Action = "GetDoorConstraints";
                    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
                    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                    oMASSIT_DoorOpenTimeBE.Weekday = WeekDay;
                    List<MASSIT_DoorOpenTimeBE> lstDoorConstraints = oMASSIT_DoorOpenTimeBAL.GetDoorConstraintsBAL(oMASSIT_DoorOpenTimeBE);
                    for (int iCount = 0; iCount < lstDoorConstraints.Count; iCount++)
                    {
                        OpenDoorIds += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";
                    }
                }
            }
            else
            {
                if (IsSpecificWeekDefined == false)
                {
                    //Get Generic settings
                    oMASSIT_DoorOpenTimeBE.Action = "GetDoorConstraints";
                    oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
                    oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                    oMASSIT_DoorOpenTimeBE.Weekday = WeekDay;
                    List<MASSIT_DoorOpenTimeBE> lstDoorConstraints = oMASSIT_DoorOpenTimeBAL.GetDoorConstraintsBAL(oMASSIT_DoorOpenTimeBE);
                    for (int iCount = 0; iCount < lstDoorConstraints.Count; iCount++)
                    {
                        OpenDoorIds += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";
                    }
                }
            }
        }

        strOpenDoorIds = OpenDoorIds;
        return OpenDoorIds;
    }

    private string GetFormatedTime(string TimeToFormat)
    {
        if (TimeToFormat != string.Empty)
        {
            string[] arrDate = TimeToFormat.Split(':');
            string HH, MM;
            if (arrDate[0].Length < 2)
                HH = "0" + arrDate[0].ToString();
            else
                HH = arrDate[0];

            if (arrDate[1].Length < 2)
                MM = "0" + arrDate[1].ToString();
            else
                MM = arrDate[1];

            return HH + ":" + MM;
        }
        else
        {
            return TimeToFormat;
        }
    }

    private int GetRequiredTimeSlotLengthForOthers(int? TotalPallets, int? TotalCartons, string SupplierType, int ID)
    {
        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;

        TotalPallets = TotalPallets == null ? 0 : TotalPallets;
        TotalCartons = TotalCartons == null ? 0 : TotalCartons;

        if (SupplierType == "V")
        {
            //Step1 >Check Vendor Processing Window
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {
                MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
                APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

                oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
                oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                oMASSIT_VendorProcessingWindowBE.VendorID = ID;
                oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
                lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);

                if (lstVendor != null)
                {  //Vendor Processing Window Not found
                    if (lstVendor.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstVendor.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------//
        }

        if (SupplierType == "C")
        {
            //Step1 >Check Carrier Processing Window
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {
                MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
                APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

                oMASSIT_CarrerProcessingWindowBE.Action = "GetConstraints";
                oMASSIT_CarrerProcessingWindowBE.CarrierID = ID;
                oMASSIT_CarrerProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                MASSIT_CarrierProcessingWindowBE lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetConstraintsBAL(oMASSIT_CarrerProcessingWindowBE);

                if (lstCarrier != null)
                {  //Carrier Processing Window Not found
                    if (lstCarrier.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstCarrier.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstCarrier.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstCarrier.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------//
        }

        //Step3 >Check Site Miscellaneous Settings
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

            if (Session["Role"].ToString() == "Vendor")
                oMAS_SiteBE.Action = "ShowAllForVendor";
            else
                oMAS_SiteBE.Action = "ShowAll";

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
            lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

            if (lstSiteMisSettings != null && lstSiteMisSettings.Count > 0)
            {
                if (lstSiteMisSettings[0].AveragePalletUnloadedPerManHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour));      // Convert.ToDecimal((Convert.ToInt32(TotalPallets) / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour) * 60);
                    isTimeCalculatedForPallets = true;
                }
                if (lstSiteMisSettings[0].AverageCartonUnloadedPerManHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour));      //Convert.ToDecimal((Convert.ToInt32(TotalCartons) / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour) * 60);
                    isTimeCalculatedForCartons = true;
                }
            }
        }
        //---------------------------------//

        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime % iSlotTimeLength;
        int intr = (int)TotalCalculatedTime / iSlotTimeLength;
        if (intResult > 0)
            intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;

        return intr;
    }

    private IDictionary<string, VendorDetails> SlotDoorCounter = new Dictionary<string, VendorDetails>();

    private void SetCallHeight(int SlotLength, Color CellBackColor, string SlotStartTime, string Pallets, string Cartoons, string Lines, string Lifts, string FixedSlotID = ""
        , string SlotTimeID = "", string SiteDoorNumberID = "", string SupplierType = "", string AllocationType = "")
    {
        if (SlotLength >= 1)
        {
            string[] StartTime = SlotStartTime.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            int iLen = 0;

            for (iLen = 1; iLen <= SlotLength; iLen++)
            {
                VendorDetails vd = new VendorDetails();

                if (time1.Hours == 23 && time1.Minutes == 45)
                {
                    time1 = new TimeSpan(00, 00, 00);
                }
                else
                {
                    time1 = time1.Add(time2);
                }
                vd.VendorName = sVendorCarrierName;
                if (Session["Role"].ToString() == "Vendor")
                {
                    vd.CellColor = Color.Black;
                    // new alternate slot logic
                    dtAlternateSlot.Rows.Add(new object[] {sSlotStartDay,iTimeSlotOrderBYID,
                    SlotStartTime,time1.Hours == 23 && time1.Minutes == 45 ? new TimeSpan(00, 00, 00) :time1.Add(time2),sVendorCarrierNameNew,
                    Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                    ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                    });
                }
                else
                {
                    vd.CellColor = CellBackColor;
                    // new alternate slot logic

                    dtAlternateSlot.Rows.Add(new object[] {sSlotStartDay,iTimeSlotOrderBYID,
                    SlotStartTime,time1.Hours == 23 && time1.Minutes == 45 ? new TimeSpan(00, 00, 00) :time1.Add(time2),sVendorCarrierNameNew,
                    Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                    ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                    });
                }
                vd.isCurrentSlotFixed = isCurrentSlotFixed;

                if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                {
                    SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
                }

                SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
            }
        }
    }

    private string SetCallHeight(int SlotLength, ref TableCell tclocal, Color CellBackColor, string SlotStartTime, string Pallets,
        string Cartoons, string Lines, string Lifts, string FixedSlotID = ""
        , string SlotTimeID = "", string SiteDoorNumberID = "", string SupplierType = "", string AllocationType = "")
    {
        string VendorName = string.Empty;
        string temp = string.Empty;

        if (iTimeSlotID == 1)
            temp = string.Empty;
        TimeSpan NewAlternateTime1;
        if (SlotLength >= 1)
        {
            string[] StartTime = SlotStartTime.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            int iLen = 0;
            for (iLen = 1; iLen <= SlotLength; iLen++)
            {
                VendorDetails vd = new VendorDetails();
                if (iLen == 1)
                {
                    NewAlternateTime1 = time1;
                    if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                    {
                        vd = SlotDoorCounter[time1.ToString() + iSiteDoorID.ToString()];

                        if (Session["Role"].ToString() == "Vendor")
                        {
                            if (isCurrentSlotFixed == false)
                            {
                                VendorName = vd.VendorName;
                                isCurrentSlotFixed = vd.isCurrentSlotFixed;
                                // new alternate slot logic
                                dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                                    NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2), sVendorCarrierNameNew,
                                    Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                                    ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                                    });
                                //}
                            }
                        }
                        else
                        {
                            VendorName = vd.VendorName;
                            isCurrentSlotFixed = vd.isCurrentSlotFixed;
                            // new alternate slot logic
                            dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                                NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                                Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                                ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                                });

                            // }
                        }
                    }
                    else
                    {
                        vd.VendorName = sVendorCarrierName;
                        vd.CellColor = CellBackColor;
                        vd.isCurrentSlotFixed = isCurrentSlotFixed;
                        SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);

                        // new alternate slot logic
                        dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                            NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                            Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                            ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                            });
                    }
                }
                else
                {
                    if (time1.Hours == 23 && time1.Minutes == 45)
                    {
                        time1 = new TimeSpan(00, 00, 00);
                        strWeekDay = objWeekSetup.EndWeekday;
                        sSlotStartDay = strWeekDay;
                        string time = time1.Hours.ToString("00") + ":" + time1.Minutes.ToString("00");
                        iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                    }
                    else
                    {
                        time1 = time1.Add(time2);
                        sSlotStartDay = strWeekDay;
                        string time = time1.Hours.ToString("00") + ":" + time1.Minutes.ToString("00");
                        iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                    }

                    if (!SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                    {
                        vd.VendorName = string.Empty; // sVendorCarrierName;
                        NewAlternateTime1 = time1;
                        if (Session["Role"].ToString() == "Vendor" && CellBackColor != Color.GreenYellow)
                        {
                            vd.CellColor = Color.Black;

                            // new alternate slot logic
                            dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                                NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                                Pallets,Cartoons,Lines,Lifts,Color.Black.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                                ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                                });
                        }
                        else
                        {
                            vd.CellColor = CellBackColor;
                            // new alternate slot logic
                            dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                                 NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                                 Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                                ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                                });

                            vd.isCurrentSlotFixed = isCurrentSlotFixed;
                            vd.CurrentFixedSlotID = sCurrentFixedSlotID;
                            SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
                        }
                    }
                    else
                    {
                        NewAlternateTime1 = time1;
                        dtAlternateSlot.Rows.Add(new object[] { sSlotStartDay,iTimeSlotOrderBYID,
                        NewAlternateTime1,NewAlternateTime1.Hours == 23 && NewAlternateTime1.Minutes == 45 ? new TimeSpan(00, 00, 00) : NewAlternateTime1.Add(time2),sVendorCarrierNameNew,
                        Pallets,Cartoons,Lines,Lifts,CellBackColor.ToString().Replace("Color","").Replace("[","").Replace("]",""),iSiteDoorID
                        ,FixedSlotID,SlotTimeID,SiteDoorNumberID,SupplierType,AllocationType,BookingID,BookingStatus,BookingStatusID
                        });
                    }
                }
            }
        }

        if (SlotLength >= 1 && NextRequiredSlot != string.Empty)
        {
            string[] StartTime = SlotStartTime.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);

            int iLen = 0;

            for (iLen = 1; iLen <= SlotLength; iLen++)
            {
                if (GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) != GetFormatedTime(NextRequiredSlot))
                {
                    if (time1.Hours == 23 && time1.Minutes == 45)
                    {
                        time1 = new TimeSpan(00, 00, 00);
                        strWeekDay = objWeekSetup.EndWeekday;
                        sSlotStartDay = strWeekDay;
                        string time = time1.Hours.ToString("00") + ":" + time1.Minutes.ToString("00");
                        iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                    }
                    else
                    {
                        time1 = time1.Add(time2);
                        sSlotStartDay = strWeekDay;
                        string time = time1.Hours.ToString("00") + ":" + time1.Minutes.ToString("00");
                        iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                    }
                }
                else if (GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) == GetFormatedTime(NextRequiredSlot))
                {
                    SetCallHeightOverSpill(SlotLength - iLen + 1, Color.Orange, time1.Subtract(time2).ToString(), Pallets, Cartoons, Lines, Lifts);
                    break;
                }
            }
            NextRequiredSlot = string.Empty;
        }

        sCurrentFixedSlotID = "-1";
        return VendorName;
    }

    private void SetCallHeightOverSpill()
    {
        if (iNextSlotLength >= 1)
        {
            string[] StartTime = NextRequiredSlot.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            time1 = time1.Subtract(time2);
            int iLen = 0;

            for (iLen = 1; iLen <= iNextSlotLength; iLen++)
            {
                VendorDetails vd = new VendorDetails();

                if (time1.Hours == 23 && time1.Minutes == 45)
                {
                    time1 = new TimeSpan(00, 00, 00);
                }
                else
                {
                    time1 = time1.Add(time2);
                }

                if (iLen <= iNextSlotLength2)
                    vd.VendorName = sVendorCarrierNameNextSlot + " / " + sVendorCarrierName;
                else
                    vd.VendorName = sVendorCarrierName;

                if (Session["Role"].ToString() == "Vendor")
                {
                    vd.CellColor = Color.Black;
                }
                else
                {
                    vd.CellColor = Color.Orange;
                }
                vd.isCurrentSlotFixed = isCurrentSlotFixed;

                if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                {
                    SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
                }

                SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
            }
        }
    }

    private void SetCallHeightOverSpill(int SlotLength, Color CellBackColor, string SlotStartTime, string Pallets, string Cartoons, string Lines, string Lifts)
    {
        if (SlotLength >= 1)
        {
            int LoopCounter;
            if (isNextSlotCovered)
                LoopCounter = iNextSlotLength;
            else
                LoopCounter = SlotLength;

            string[] StartTime = SlotStartTime.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            int iLen = 0;
            TimeSpan NewAlternateTime1;
            for (iLen = 1; iLen <= LoopCounter; iLen++)
            {
                VendorDetails vd = new VendorDetails();
                if (time1.Hours == 23 && time1.Minutes == 45)
                {
                    time1 = new TimeSpan(00, 00, 00);
                    strWeekDay = objWeekSetup.EndWeekday;
                    sSlotStartDay = strWeekDay;
                    string time = time1.Hours.ToString("00") + ":" + time1.Minutes.ToString("00");
                    iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                }
                else
                {
                    time1 = time1.Add(time2);
                    sSlotStartDay = strWeekDay;
                    string time = time1.Hours.ToString("00") + ":" + time1.Minutes.ToString("00");
                    iTimeSlotOrderBYID = lstNewSlotTime.Where(x => x.SlotTime == time).Select(x => x.OrderBYID).FirstOrDefault();
                }

                if (iLen <= iNextSlotLength2)
                    vd.VendorName = sVendorCarrierNameNextSlot + " / " + sVendorCarrierName;
                else
                    vd.VendorName = sVendorCarrierName;

                NewAlternateTime1 = time1;

                if (Session["Role"].ToString() == "Vendor")
                {
                    vd.CellColor = Color.Black;
                }
                else
                {
                    vd.CellColor = CellBackColor;
                }
                vd.isCurrentSlotFixed = isCurrentSlotFixed;

                if (SlotDoorCounter.ContainsKey(time1.ToString() + iSiteDoorID.ToString()))
                {
                    SlotDoorCounter.Remove(time1.ToString() + iSiteDoorID.ToString());
                }
                SlotDoorCounter.Add(time1.ToString() + iSiteDoorID.ToString(), vd);
            }
        }
    }

    private int GetRequiredTimeSlotLength(int? TotalPallets, int? TotalCartons)
    {
        int itempCartons, itempPallets;
        itempCartons = iCartons;
        itempPallets = iPallets;

        iPallets = TotalPallets != (int?)null ? Convert.ToInt32(TotalPallets) : 0;
        iCartons = TotalCartons != (int?)null ? Convert.ToInt32(TotalCartons) : 0;

        int reqSlotLength = GetTimeSlotLength(-1);

        iCartons = itempCartons;
        iPallets = itempPallets;

        return reqSlotLength;
    }

    private int GetTimeSlotLength(int VehicleTypeID)
    {
        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;

        //Step1 >Check Vendor Processing Window
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
            APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

            oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
            oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
            lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);

            if (lstVendor != null)
            {  //Vendor Processing Window Not found
                if (lstVendor.APP_PalletsUnloadedPerHour > 0 && iPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVendor.APP_CartonsUnloadedPerHour > 0 && iCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }
            }
        }
        //---------------------------------//

        //Step2 > Check Vehicle Type Setup
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
            APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

            oMASSIT_VehicleTypeBE.Action = "ShowById";
            oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);

            oMASSIT_VehicleTypeBE.VehicleTypeID = VehicleTypeID;

            MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType != null)
            {
                //Volume Capacity
                if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 && iPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVehicleType.APP_CartonsUnloadedPerHour > 0 && iCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }

                if (isTimeCalculatedForPallets == false && isTimeCalculatedForCartons == false)
                { //Time Based
                    if (lstVehicleType.TotalUnloadTimeInMinute > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
                        isTimeCalculatedForPallets = true;
                        isTimeCalculatedForCartons = true;
                    }
                }

                //---------------Stage 14 Point 15-------------//
                if (lstVehicleType.TotalUnloadTimeInMinute <= 0 && TotalCalculatedTime < lstVehicleType.MinimumTimePerDeliveryInMinute)
                {
                    TotalCalculatedTime = lstVehicleType.MinimumTimePerDeliveryInMinute;
                }
                //-------------------------------------------//
            }
        }

        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime % iSlotTimeLength;
        int intr = (int)TotalCalculatedTime / iSlotTimeLength;
        if (intResult > 0)
            intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;
        //if (intr == 1)
        //    intr--;

        return intr;
    }

    private int GetRequiredTimeSlotLengthForBookedSlotOfOthers(int? TotalPallets, int? TotalCartons, string SupplierType, int ID, int VehicleTypeID)
    {
        //--------start Calculate Unload Time-------------------//
        decimal TotalCalculatedTime = 0;  //In Min
        bool isTimeCalculatedForPallets = false;
        bool isTimeCalculatedForCartons = false;

        TotalPallets = TotalPallets == null ? 0 : TotalPallets;
        TotalCartons = TotalCartons == null ? 0 : TotalCartons;

        if (SupplierType == "V")
        {
            //Step1 >Check Vendor Processing Window
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {
                MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
                APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

                oMASSIT_VendorProcessingWindowBE.Action = "ShowAllConsolidate";
                oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                oMASSIT_VendorProcessingWindowBE.VendorID = ID;
                oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

                MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
                lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);

                if (lstVendor != null)
                {  //Vendor Processing Window Not found
                    if (lstVendor.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVendor.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstVendor.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVendor.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------//
        }

        if (SupplierType == "C")
        {
            //Step1 >Check Carrier Processing Window
            if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
            {
                MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
                APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

                oMASSIT_CarrerProcessingWindowBE.Action = "GetConstraints";
                oMASSIT_CarrerProcessingWindowBE.CarrierID = ID;
                oMASSIT_CarrerProcessingWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                MASSIT_CarrierProcessingWindowBE lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetConstraintsBAL(oMASSIT_CarrerProcessingWindowBE);

                if (lstCarrier != null)
                {  //Carrier Processing Window Not found
                    if (lstCarrier.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstCarrier.APP_PalletsUnloadedPerHour));
                        isTimeCalculatedForPallets = true;
                    }
                    if (lstCarrier.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstCarrier.APP_CartonsUnloadedPerHour));
                        isTimeCalculatedForCartons = true;
                    }
                }
            }
            //---------------------------------//
        }

        //Step2 > Check Vehicle Type Setup
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
            APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

            oMASSIT_VehicleTypeBE.Action = "ShowById";
            oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);

            oMASSIT_VehicleTypeBE.VehicleTypeID = VehicleTypeID;

            MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType != null)
            {
                //Volume Capacity
                if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstVehicleType.APP_PalletsUnloadedPerHour));
                    isTimeCalculatedForPallets = true;
                }
                if (lstVehicleType.APP_CartonsUnloadedPerHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstVehicleType.APP_CartonsUnloadedPerHour));
                    isTimeCalculatedForCartons = true;
                }

                if (isTimeCalculatedForPallets == false && isTimeCalculatedForCartons == false)
                { //Time Based
                    if (lstVehicleType.TotalUnloadTimeInMinute > 0)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToDecimal(lstVehicleType.TotalUnloadTimeInMinute));
                        isTimeCalculatedForPallets = true;
                        isTimeCalculatedForCartons = true;
                    }
                }

                //---------------Stage 14 Point 15-------------//
                if (lstVehicleType.TotalUnloadTimeInMinute <= 0 && TotalCalculatedTime < lstVehicleType.MinimumTimePerDeliveryInMinute)
                {
                    TotalCalculatedTime = lstVehicleType.MinimumTimePerDeliveryInMinute;
                }
                //-------------------------------------------//
            }
        }
        //---------------------------------//

        //Step3 >Check Site Miscellaneous Settings
        if (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false)
        {
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

            if (Session["Role"].ToString() == "Vendor")
                oMAS_SiteBE.Action = "ShowAllForVendor";
            else
                oMAS_SiteBE.Action = "ShowAll";

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
            lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

            if (lstSiteMisSettings != null && lstSiteMisSettings.Count > 0)
            {
                if (lstSiteMisSettings[0].AveragePalletUnloadedPerManHour > 0 && TotalPallets != 0 && isTimeCalculatedForPallets == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalPallets) * (60.0 / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour));      // Convert.ToDecimal((Convert.ToInt32(TotalPallets) / lstSiteMisSettings[0].AveragePalletUnloadedPerManHour) * 60);
                    isTimeCalculatedForPallets = true;
                }
                if (lstSiteMisSettings[0].AverageCartonUnloadedPerManHour > 0 && TotalCartons != 0 && isTimeCalculatedForCartons == false)
                {
                    TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(TotalCartons) * (60.0 / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour));      //Convert.ToDecimal((Convert.ToInt32(TotalCartons) / lstSiteMisSettings[0].AverageCartonUnloadedPerManHour) * 60);
                    isTimeCalculatedForCartons = true;
                }
            }
        }
        //---------------------------------//

        //Calculate Slot Length
        int intResult = (int)TotalCalculatedTime % iSlotTimeLength;
        int intr = (int)TotalCalculatedTime / iSlotTimeLength;
        if (intResult > 0)
            intr++;

        if (TotalCalculatedTime > 0 && TotalCalculatedTime < 1)
            intr = 1;

        if (TotalCalculatedTime == 0)
            intr++;

        return intr;
    }

    private string BookingID = string.Empty;
    private string BookingStatusID = string.Empty;
    private string BookingStatus = string.Empty;
    private string SlotTimeID = string.Empty;

    private List<AlternateSlot> lstSlot = new List<AlternateSlot>();

    private Literal SetCellColor(ref TableCell tclocal)
    {
        string VendorCarrierName = string.Empty;
        int SlotLength = 0;
        NextRequiredSlot = string.Empty;
        iNextSlotLength = 0;
        iNextSlotLength2 = 0;
        sVendorCarrierNameNextSlot = string.Empty;
        isCurrentSlotCovered = false;

        //Find Next Fixed Slot on the Same door No if seledted slot is a fixed slot

        List<APPBOK_BookingBE> lstNextFixedDoorLocal = lstAllFixedDoor.FindAll(delegate (APPBOK_BookingBE Bok) { return Bok.DoorNoSetup.SiteDoorNumberID == iSiteDoorID; });

        for (int LoopCount = 0; LoopCount < lstNextFixedDoorLocal.Count; LoopCount++)
        {
            if (lstNextFixedDoorLocal[LoopCount].SlotTime.SlotTimeID == iTimeSlotID && lstNextFixedDoorLocal[LoopCount].DoorNoSetup.SiteDoorNumberID == iSiteDoorID)
            {
                if (LoopCount < lstNextFixedDoorLocal.Count - 1)
                {
                    NextRequiredSlot = lstNextFixedDoorLocal[LoopCount + 1].SlotTime.SlotTime;
                    sVendorCarrierNameNextSlot = lstNextFixedDoorLocal[LoopCount + 1].Vendor.VendorName;
                    iNextSlotLength = 0;
                    BookingID = Convert.ToString(lstNextFixedDoorLocal[LoopCount + 1].BookingID);
                    BookingStatusID = Convert.ToString(lstNextFixedDoorLocal[LoopCount + 1].BookingStatusID);
                    BookingStatus = Convert.ToString(lstNextFixedDoorLocal[LoopCount + 1].BookingStatus);
                    if (lstNextFixedDoorLocal[LoopCount + 1].Status != "Booked")
                    {
                        if (lstNextFixedDoorLocal[LoopCount + 1].SupplierType == "C")
                            iNextSlotLength2 = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons, lstNextFixedDoorLocal[LoopCount + 1].SupplierType, lstNextFixedDoorLocal[LoopCount + 1].Carrier.CarrierID); // GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons);
                        else if (lstNextFixedDoorLocal[LoopCount + 1].SupplierType == "V")
                            iNextSlotLength2 = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons, lstNextFixedDoorLocal[LoopCount + 1].SupplierType, lstNextFixedDoorLocal[LoopCount + 1].VendorID);
                    }
                    else
                    {
                        iNextSlotLength2 = GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount + 1].NumberOfPallet, lstNextFixedDoorLocal[LoopCount + 1].NumberOfCartons);
                    }
                }
                break;
            }
        }
        //---------------------------------//

        //Find Next Slot on the Same door No if seledted slot is not a fixed slot

        if (NextRequiredSlot == string.Empty)
        {
            string[] StartTime = sTimeSlot.Split(':');
            TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
            TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
            int iLen = 0;
            for (iLen = 1; iLen <= actSlotLength; iLen++)
            {
                if (time1.Hours == 23 && time1.Minutes == 45)
                {
                    time1 = new TimeSpan(00, 00, 00);
                }
                else
                {
                    time1 = time1.Add(time2);
                }
                for (int LoopCount = 0; LoopCount < lstNextFixedDoorLocal.Count; LoopCount++)
                {
                    if (GetFormatedTime(lstNextFixedDoorLocal[LoopCount].SlotTime.SlotTime) == GetFormatedTime(time1.Hours.ToString() + ":" + time1.Minutes.ToString()) && lstNextFixedDoorLocal[LoopCount].DoorNoSetup.SiteDoorNumberID == iSiteDoorID)
                    {
                        if (lstNextFixedDoorLocal[LoopCount].Status != "Booked")
                        {
                            if (lstNextFixedDoorLocal[LoopCount].SupplierType == "C")
                                iNextSlotLength = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons, lstNextFixedDoorLocal[LoopCount].SupplierType, lstNextFixedDoorLocal[LoopCount].Carrier.CarrierID); // GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons);
                            else if (lstNextFixedDoorLocal[LoopCount].SupplierType == "V")
                                iNextSlotLength = GetRequiredTimeSlotLengthForOthers(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons, lstNextFixedDoorLocal[LoopCount].SupplierType, lstNextFixedDoorLocal[LoopCount].VendorID);
                        }
                        else
                        {
                            if (lstNextFixedDoorLocal[LoopCount].SupplierType == "C")
                                iNextSlotLength = GetRequiredTimeSlotLengthForBookedSlotOfOthers(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons, lstNextFixedDoorLocal[LoopCount].SupplierType, lstNextFixedDoorLocal[LoopCount].Carrier.CarrierID, lstNextFixedDoorLocal[LoopCount].VehicleType.VehicleTypeID); // GetRequiredTimeSlotLength(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons);
                            else if (lstNextFixedDoorLocal[LoopCount].SupplierType == "V")
                                iNextSlotLength = GetRequiredTimeSlotLengthForBookedSlotOfOthers(lstNextFixedDoorLocal[LoopCount].NumberOfPallet, lstNextFixedDoorLocal[LoopCount].NumberOfCartons, lstNextFixedDoorLocal[LoopCount].SupplierType, lstNextFixedDoorLocal[LoopCount].VendorID, lstNextFixedDoorLocal[LoopCount].VehicleType.VehicleTypeID);
                        }

                        NextRequiredSlot = lstNextFixedDoorLocal[LoopCount].SlotTime.SlotTime;
                        sVendorCarrierNameNextSlot = lstNextFixedDoorLocal[LoopCount].Vendor.VendorName;
                        break;
                    }
                }
                if (NextRequiredSlot != string.Empty)
                {
                    isNextSlotCovered = true;
                    if (actSlotLength - iLen < iNextSlotLength)
                    {
                        isNextSlotCovered = false;
                    }
                    break;
                }
            }
        }
        //---------------------------------//

        //Filter slot as per SlotTimeID and SiteDoorNumberID

        List<APPBOK_BookingBE> lstAllFixedDoorLocal = lstAllFixedDoor.FindAll(delegate (APPBOK_BookingBE Bok) { return Bok.SlotTime.SlotTimeID == iTimeSlotID && Bok.DoorNoSetup.SiteDoorNumberID == iSiteDoorID; });
        if (lstAllFixedDoorLocal.Count > 0)
        {
            for (int i = 0; i < lstAllFixedDoorLocal.Count(); i++)
            {
                isClickable = true;
                sVendorCarrierName = lstAllFixedDoorLocal[i].Vendor.VendorName;
                sVendorCarrierNameNew = lstAllFixedDoorLocal[i].Vendor.VendorName;
                string supplierType = lstAllFixedDoorLocal[i].SupplierType;
                string FixedSlotID = Convert.ToString(lstAllFixedDoorLocal[i].FixedSlot.FixedSlotID);
                SlotTimeID = Convert.ToString(lstAllFixedDoorLocal[i].SlotTime.SlotTimeID);

                AllocationType = Convert.ToString(lstAllFixedDoorLocal[i].FixedSlot.AllocationType);
                //commented Just for run Fixed slot
                iCartons = lstAllFixedDoorLocal[i].NumberOfCartons != null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfCartons) : 0;

                iPallets = lstAllFixedDoorLocal[i].NumberOfPallet != null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfPallet) : 0;

                iLines = lstAllFixedDoorLocal[i].NumberOfLines != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfLines) : 0;

                iLifts = lstAllFixedDoorLocal[i].NumberOfLift != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfLift) : 0;

                if (lstAllFixedDoorLocal[i].Status != "Booked")
                {
                    if (lstAllFixedDoorLocal[i].SupplierType == "C")
                    {
                        SlotLength = GetRequiredTimeSlotLengthForOthers(lstAllFixedDoorLocal[i].NumberOfPallet, lstAllFixedDoorLocal[i].NumberOfCartons, lstAllFixedDoorLocal[i].SupplierType, lstAllFixedDoorLocal[i].Carrier.CarrierID);
                        tclocal.Attributes.Add("abbr", "C-" + lstAllFixedDoorLocal[i].Carrier.CarrierID.ToString());
                    }
                    else if (lstAllFixedDoorLocal[i].SupplierType == "V")
                    {
                        SlotLength = GetRequiredTimeSlotLengthForOthers(lstAllFixedDoorLocal[i].NumberOfPallet, lstAllFixedDoorLocal[i].NumberOfCartons, lstAllFixedDoorLocal[i].SupplierType, lstAllFixedDoorLocal[i].VendorID);
                        tclocal.Attributes.Add("abbr", "V-" + lstAllFixedDoorLocal[i].VendorID.ToString());
                    }
                }
                else
                {
                    if (lstAllFixedDoorLocal[i].SupplierType == "C")
                    {
                        SlotLength = GetRequiredTimeSlotLengthForBookedSlotOfOthers(lstAllFixedDoorLocal[i].NumberOfPallet, lstAllFixedDoorLocal[i].NumberOfCartons, lstAllFixedDoorLocal[i].SupplierType, lstAllFixedDoorLocal[i].Carrier.CarrierID, lstAllFixedDoorLocal[i].VehicleType.VehicleTypeID);
                        tclocal.Attributes.Add("abbr", "C-" + lstAllFixedDoorLocal[i].Carrier.CarrierID.ToString());
                    }
                    else if (lstAllFixedDoorLocal[i].SupplierType == "V")
                    {
                        SlotLength = GetRequiredTimeSlotLengthForBookedSlotOfOthers(lstAllFixedDoorLocal[i].NumberOfPallet, lstAllFixedDoorLocal[i].NumberOfCartons, lstAllFixedDoorLocal[i].SupplierType, lstAllFixedDoorLocal[i].VendorID, lstAllFixedDoorLocal[i].VehicleType.VehicleTypeID);
                        tclocal.Attributes.Add("abbr", "V-" + lstAllFixedDoorLocal[i].VendorID.ToString());
                    }
                }

                BookingID = Convert.ToString(lstAllFixedDoorLocal[i].BookingID);
                BookingStatusID = Convert.ToString(lstAllFixedDoorLocal[i].BookingStatusID);
                BookingStatus = Convert.ToString(lstAllFixedDoorLocal[i].BookingStatus);
                //---Phase 19 point 1--//
                tclocal.Attributes.Add("prefix", lstAllFixedDoorLocal[i].FixedSlot.AllocationType + "-" + SlotLength.ToString());
                //---------------------//

                isCurrentSlotFixed = false;
                string tempVendorName = "";

                sCurrentFixedSlotID = lstAllFixedDoorLocal[i].FixedSlot.FixedSlotID.ToString();

                if (Session["Role"].ToString() == "Vendor")
                {
                    tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Black, lstAllFixedDoorLocal[i].SlotTime.SlotTime, iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);
                }
                else
                {
                    if (lstAllFixedDoorLocal[i].SlotType == "FIXED SLOT" && lstAllFixedDoorLocal[i].Status != "Booked")
                    {
                        isCurrentSlotFixed = true;
                        tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Red, lstAllFixedDoorLocal[i].SlotTime.SlotTime, iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);
                        if (!tempVendorName.Contains(sVendorCarrierName))
                        {
                            if (tempVendorName != string.Empty)
                            {
                                if (sVendorCarrierName != tempVendorName)
                                {
                                    tempVendorName = sVendorCarrierName + " / " + tempVendorName;
                                }
                            }
                            else
                            {
                                if (sVendorCarrierName != tempVendorName)
                                {
                                    tempVendorName = sVendorCarrierName;
                                }
                            }
                        }
                    }
                    else if (lstAllFixedDoorLocal[i].Status == "Booked")
                    {
                        sVendorCarrierNameNew = lstAllFixedDoorLocal[i].Vendor.VendorName;
                        tempVendorName = SetCallHeight(SlotLength, ref tclocal, Color.Purple, lstAllFixedDoorLocal[i].SlotTime.SlotTime, iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), FixedSlotID, SlotTimeID, SiteDoorNumberID, supplierType, AllocationType);

                        if (!tempVendorName.Contains(sVendorCarrierName))
                        {
                            if (tempVendorName != string.Empty)
                            {
                                if (sVendorCarrierName != tempVendorName)
                                {
                                    tempVendorName = sVendorCarrierName + " / " + tempVendorName;
                                }
                            }
                            else
                            {
                                if (sVendorCarrierName != tempVendorName)
                                {
                                    tempVendorName = sVendorCarrierName;
                                }
                            }
                        }
                    }
                }

                if (tempVendorName != string.Empty)
                {
                    VendorCarrierName = tempVendorName;
                }
                else
                {
                    VendorCarrierName = lstAllFixedDoorLocal[i].Vendor.VendorName;
                    iTimeSlotID = lstAllFixedDoorLocal[i].SlotTime.SlotTimeID;
                    iSiteDoorID = lstAllFixedDoorLocal[i].DoorNoSetup.SiteDoorNumberID;
                    iCartons = lstAllFixedDoorLocal[i].NumberOfCartons != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfCartons) : 0;
                    iLifts = lstAllFixedDoorLocal[i].NumberOfLift != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfLift) : 0;
                    iLines = lstAllFixedDoorLocal[i].NumberOfLines != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfLines) : 0;
                    iPallets = lstAllFixedDoorLocal[i].NumberOfPallet != (int?)null ? Convert.ToInt32(lstAllFixedDoorLocal[i].NumberOfPallet) : 0;
                }
                if (Session["Role"].ToString() == "Vendor")
                {
                    VendorCarrierName = "";
                }
            }
        }
        else
        {
            isCurrentSlotFixed = false;
            bool isLoopProcessed = false;
            if (DoorIDs.Contains(iTimeSlotID + "@" + iSiteDoorID))
            {  //Door Available
                isLoopProcessed = true;
                VendorCarrierName = SetCallHeight(1, ref tclocal, Color.White, sTimeSlot, iPallets.ToString(), iCartons.ToString(), iLines.ToString(), iLifts.ToString(), SiteDoorNumberID: SiteDoorNumberID);
                if (Session["Role"].ToString() == "Vendor")
                {
                    VendorCarrierName = "";
                }
            }
            else if (!DoorIDs.Contains(iTimeSlotID + "@" + iSiteDoorID) && Session["Role"].ToString() != "Vendor")
            {  //Door Not Available but User is internal user
                TimeSpan NewAlternateTime1;
                string[] StartTime = sTimeSlot.Split(':');
                TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);
                TimeSpan time2 = new TimeSpan(00, iSlotTimeLength, 00);
                NewAlternateTime1 = time1;
            }

            if (isLoopProcessed == false)
            { // Door Not Available and external user
                isCurrentSlotFixed = false;

                VendorDetails vd = new VendorDetails();

                string[] StartTime = sTimeSlot.Split(':');
                TimeSpan time1 = new TimeSpan(Convert.ToInt32(StartTime[0]), Convert.ToInt32(StartTime[1]), 00);

                if (SlotDoorCounter.ContainsKey(time1 + iSiteDoorID.ToString()))
                {
                    vd = SlotDoorCounter[time1 + iSiteDoorID.ToString()];
                    if (vd.CellColor == Color.GreenYellow)
                    {
                        VendorCarrierName = vd.VendorName;
                    }
                    else
                    {
                        VendorCarrierName = "";
                    }
                }
                else
                {
                    VendorCarrierName = "";
                }
            }
        }

        string[] StartTimeslot = sTimeSlot.Split(':');
        TimeSpan slottime1 = new TimeSpan(Convert.ToInt32(StartTimeslot[0]), Convert.ToInt32(StartTimeslot[1]), 00);
        TimeSpan slottime2 = new TimeSpan(00, iSlotTimeLength, 00);
        TimeSpan slotNewAlternateTime1 = slottime1;
        lstSlot.Add(new AlternateSlot()
        {
            SlotStartDay = sSlotStartDay,
            siTimeSlotOrderBYID = Convert.ToString(iTimeSlotOrderBYID),
            ArrSlotTime = slotNewAlternateTime1.ToString(),
            EstSlotTime = slotNewAlternateTime1.Hours.ToString() == "23"
            && slotNewAlternateTime1.Minutes.ToString() == "45" ? "00:00:00" : slotNewAlternateTime1.Add(slottime2).ToString(),
            SiteDoorNumberID = SiteDoorNumberID,
            SlotTimeID = Convert.ToString(SlotTimeID),
            DoorNo = Convert.ToString(iSiteDoorID)
        });

        BookingID = "";
        BookingStatus = "";
        BookingStatusID = "";
        Literal lt = new Literal();
        lt.Text = VendorCarrierName;
        return lt;
    }

    public string strVehicleDoorTypeMatrix
    {
        get
        {
            return ViewState["strVehicleDoorTypeMatrix"] != null ? ViewState["strVehicleDoorTypeMatrix"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strVehicleDoorTypeMatrix"] = value;
        }
    }

    public string strVehicleDoorTypeMatrixSKU
    {
        get
        {
            return ViewState["strVehicleDoorTypeMatrixSKU"] != null ? ViewState["strVehicleDoorTypeMatrixSKU"].ToString() : string.Empty;
        }
        set
        {
            ViewState["strVehicleDoorTypeMatrixSKU"] = value;
        }
    }

    public System.Collections.Specialized.NameValueCollection ArrDoorTypePriority
    {
        get
        {
            return ViewState["ArrDoorTypePriority"] != null ? (System.Collections.Specialized.NameValueCollection)ViewState["ArrDoorTypePriority"] : null;
        }
        set
        {
            ViewState["ArrDoorTypePriority"] = value;
        }
    }

    public System.Collections.Specialized.NameValueCollection ArrDoorTypePrioritySKU
    {
        get
        {
            return ViewState["ArrDoorTypePrioritySKU"] != null ? (System.Collections.Specialized.NameValueCollection)ViewState["ArrDoorTypePrioritySKU"] : null;
        }
        set
        {
            ViewState["ArrDoorTypePrioritySKU"] = value;
        }
    }

    private void VehicleTypeSelect()
    {
        //Get Door Type
        strVehicleDoorTypeMatrix = string.Empty;
        MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();

        APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();

        oMASSIT_VehicleDoorMatrixBE.Action = "ShowAll";
        oMASSIT_VehicleDoorMatrixBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_VehicleDoorMatrixBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMASSIT_VehicleDoorMatrixBE.SiteID = Convert.ToInt16(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<MASSIT_VehicleDoorMatrixBE> lstVehicleDoorMatrix = oMASSIT_VehicleDoorMatrixBAL.GetVehicleDoorMatrixDetailsBAL(oMASSIT_VehicleDoorMatrixBE);

        //---FOR SKU DOOR MATRIX, NEED TO SKIP VEHICLE DOOR MATIX CONSTRAINTS--- SPRINT 4 POINT 7
        System.Collections.Specialized.NameValueCollection collection = new System.Collections.Specialized.NameValueCollection();

        for (int iCount = 0; iCount < lstVehicleDoorMatrix.Count; iCount++)
        {
            strVehicleDoorTypeMatrixSKU += lstVehicleDoorMatrix[iCount].DoorTypeID.ToString() + ",";
            collection.Add(lstVehicleDoorMatrix[iCount].DoorTypeID.ToString(), lstVehicleDoorMatrix[iCount].Priority.ToString());
        }

        ArrDoorTypePrioritySKU = new System.Collections.Specialized.NameValueCollection();
        foreach (string key in collection.AllKeys)
        {
            string Val = collection.Get(key);
            string[] ArrVal = Val.Split(',');
            ArrDoorTypePrioritySKU.Add(key, ArrVal[0].ToString());
        }
        //---------------------------------------------------------------------------------------

        //Get Door Type as per selected Vehicle Type

        collection = new System.Collections.Specialized.NameValueCollection();

        for (int iCount = 0; iCount < lstVehicleDoorMatrix.Count; iCount++)
        {
            strVehicleDoorTypeMatrix += lstVehicleDoorMatrix[iCount].DoorTypeID.ToString() + ",";
            collection.Add(lstVehicleDoorMatrix[iCount].DoorTypeID.ToString(), lstVehicleDoorMatrix[iCount].Priority.ToString());
        }

        ArrDoorTypePriority = new System.Collections.Specialized.NameValueCollection();
        foreach (string key in collection.AllKeys)
        {
            string Val = collection.Get(key);
            string[] ArrVal = Val.Split(',');
            ArrDoorTypePriority.Add(key, ArrVal[0].ToString());
        }

        //-----------------//
    }

    private List<SYS_SlotTimeBE> lstNewSlotTime = new List<SYS_SlotTimeBE>();
    private MASSIT_WeekSetupBE objWeekSetup = new MASSIT_WeekSetupBE();

    private string strWeekDay = string.Empty;

    private void GenerateAlternateSlot(bool isStandardBookingRequired, int iSKUSiteDoorID = 0, int iSKUSiteDoorTypeID = 0)
    {
        tableDiv_GeneralNew.Controls.RemoveAt(0);
        GenerateNonTimeBooking();
        GetDoorConstraints();
        VehicleTypeSelect();
        LogicalSchedulDateTime = Utilities.Common.TextToDateFormat(txtDate.innerControltxtDate.Value);

        actSlotLength = 0;

        #region -------Collect Basic Information-----------

        //-----------------Start Collect Basic Information --------------------------//
        DateTime dtSelectedDate = LogicalSchedulDateTime;
        string WeekDay = dtSelectedDate.ToString("dddd");

        DateTime dtMon = Utilities.Common.GetFirstDayOfWeek(dtSelectedDate);


        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();
        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE
        {
            Action = "GetSpecificWeek",
            SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value),
            EndWeekday = WeekDay,
            ScheduleDate = LogicalSchedulDateTime
        };

        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        if (lstWeekSetup == null || lstWeekSetup.Count <= 0 || lstWeekSetup[0].StartTime == null)
        {
            oMASSIT_WeekSetupBE.Action = "ShowAll";
            lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

            if (lstWeekSetup.Count <= 0)
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "<script>alert('" + errorMeesage + "');</script>", false);
                return;
            }
            else if (lstWeekSetup[0].StartTime == null)
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "<script>alert('" + errorMeesage + "');</script>", false);
                return;
            }
        }

        APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
        MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

        DoorIDs = strOpenDoorIds != "," ? strOpenDoorIds : GetDoorConstraints();

        oMASSIT_DoorOpenTimeBE.Action = "GetDoorNoAndType";
        oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE
        {
            SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value)
        };

        List<MASSIT_DoorOpenTimeBE> lstDoorNoType = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

        int iDoorCount = lstDoorNoType.Count;

        int dblCellWidth = 165;
        int i1stColumnWidth = 160;
        //-------------------------End Collect Basic Information ------------------------------//

        #endregion -------Collect Basic Information-----------

        #region --------------Grid Header---------------

        dtAlternateSlot = new DataTable();
        dtAlternateSlot.Columns.Add("SlotStartDay", typeof(string));
        dtAlternateSlot.Columns.Add("TimeSlotOrderBYID", typeof(string));
        dtAlternateSlot.Columns.Add("ArrSlotTime", typeof(string));
        dtAlternateSlot.Columns.Add("EstSlotTime", typeof(string));
        dtAlternateSlot.Columns.Add("Vendor", typeof(string));
        dtAlternateSlot.Columns.Add("Pallets", typeof(string));
        dtAlternateSlot.Columns.Add("Cartoons", typeof(string));
        dtAlternateSlot.Columns.Add("Lines", typeof(string));
        dtAlternateSlot.Columns.Add("Lifts", typeof(string));
        dtAlternateSlot.Columns.Add("Color", typeof(string));
        dtAlternateSlot.Columns.Add("DoorNo", typeof(string));
        dtAlternateSlot.Columns.Add("FixedSlotID", typeof(string));
        dtAlternateSlot.Columns.Add("SlotTimeID", typeof(string));
        dtAlternateSlot.Columns.Add("SiteDoorNumberID", typeof(string));
        dtAlternateSlot.Columns.Add("SupplierType", typeof(string));
        dtAlternateSlot.Columns.Add("AllocationType", typeof(string));
        dtAlternateSlot.Columns.Add("BookingID", typeof(string));
        dtAlternateSlot.Columns.Add("BookingStatus", typeof(string));
        dtAlternateSlot.Columns.Add("BookingStatusID", typeof(string));
        dtAlternateSlot.Columns.Add("ArrSlotTime15min", typeof(string));
        dtAlternateSlot.Columns.Add("EstSlotTime15min", typeof(string));
        dtAlternateSlot.Columns.Add("DoorNameDisplay", typeof(string));
        dtAlternateSlot.Columns.Add("DoorNoDisplay", typeof(string));
        dtAlternateSlot.Columns.Add("VendorDisplay", typeof(string));
        dtAlternateSlot.Columns.Add("EstSlotTimeDisplay", typeof(string));
        dtAlternateSlot.Columns.Add("Weekday", typeof(string));

        //-------------Start Grid Header--------------//
        Table tblSlot = new Table();
        tblSlot.CellSpacing = 0;
        tblSlot.CellPadding = 0;
        tblSlot.CssClass = "FixedTables";
        tblSlot.ID = "Open_Text_General";
        tblSlot.Width = Unit.Pixel((dblCellWidth * iDoorCount) + i1stColumnWidth);

        TableRow trDoorNo = new TableRow();
        trDoorNo.TableSection = TableRowSection.TableHeader;
        TableHeaderCell tcDoorNo = new TableHeaderCell();
        tcDoorNo.Text = "Door Name / Door Type";
        tcDoorNo.BorderWidth = Unit.Pixel(1);
        tcDoorNo.Width = Unit.Pixel(i1stColumnWidth);
        trDoorNo.Cells.Add(tcDoorNo);

        TableRow trTime = new TableRow();
        trTime.TableSection = TableRowSection.TableHeader;
        TableHeaderCell tcTime = new TableHeaderCell();
        tcTime.Text = "Time";
        tcTime.BorderWidth = Unit.Pixel(1);
        tcTime.Width = Unit.Pixel(i1stColumnWidth);
        trTime.Cells.Add(tcTime);

        for (int iCount = 1; iCount <= iDoorCount; iCount++)
        {
            tcDoorNo = new TableHeaderCell();
            tcDoorNo.BorderWidth = Unit.Pixel(1);
            tcDoorNo.Text = lstDoorNoType[iCount - 1].DoorNo.DoorNumber.ToString() + " / " + lstDoorNoType[iCount - 1].DoorType.DoorType; ;
            tcDoorNo.Width = Unit.Pixel(dblCellWidth);
            trDoorNo.Cells.Add(tcDoorNo);

            tcTime = new TableHeaderCell();
            tcTime.BorderWidth = Unit.Pixel(1);
            tcTime.Text = "Vendor/Carrier";
            tcTime.Width = Unit.Pixel(dblCellWidth);
            trTime.Cells.Add(tcTime);
        }
        trDoorNo.BackColor = System.Drawing.Color.LightGray;
        tblSlot.Rows.Add(trDoorNo);

        trTime.BackColor = System.Drawing.Color.LightGray;
        tblSlot.Rows.Add(trTime);
        //-------------End Grid Header -------------//

        #endregion --------------Grid Header---------------

        #region --------------Slot Ploting---------------

        //-------------Start Slot Ploting--------------//

        //--------Find all Fixed Slots, Bookings For all Vendor/Carrier-------//

        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE
        {
            Action = "AllBookingFixedSlotDetails",
            SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value),
            ScheduleDate = LogicalSchedulDateTime,
            SelectedScheduleDate = LogicalSchedulDateTime
        };

        lstAllFixedDoor = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

        lstAllFixedDoor = lstAllFixedDoor.FindAll(delegate (APPBOK_BookingBE St)
        {
            DateTime? dt = (DateTime?)null;
            dt = St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty
                ? (DateTime?)new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0)
                : (DateTime?)new DateTime(1900, 1, 1, 0, 0, 0);

            return St.BookingRef == "Fixed booking" ? dt <= lstWeekSetup[0].EndTime : true;
        });

        //--------------------------------------//

        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        {
            oAPPBOK_BookingBE.Action = "AllBookingFixedSlotDetails";
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.SelectedScheduleDate = LogicalSchedulDateTime;
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1);

            List<APPBOK_BookingBE> lstAllFixedDoorStartWeek = oAPPBOK_BookingBAL.GetAllBookingFixedSlotDetailsBAL(oAPPBOK_BookingBE);

            int weekday = 0;

            weekday = ExtrcatWeekDay(lstWeekSetup, weekday);

            lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(x => (x.WeekDay == weekday && x.Status == "Booked") || (x.WeekDay == 0 && x.Status == string.Empty));

            //Filetr Non Time Delivery

            lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate (APPBOK_BookingBE St)
            {
                return St.SlotTime.SlotTime != "";
            });

            if (lstAllFixedDoorStartWeek != null && lstAllFixedDoorStartWeek.Count > 0)
            {
                lstAllFixedDoorStartWeek = lstAllFixedDoorStartWeek.FindAll(delegate (APPBOK_BookingBE St)
                {
                    DateTime? dt = (DateTime?)null;
                    dt = St.SlotTime.SlotTime != null && St.SlotTime.SlotTime != string.Empty
                        ? (DateTime?)new DateTime(1900, 1, 1, Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[0]), Convert.ToInt32(St.SlotTime.SlotTime.Split(':')[1]), 0)
                        : (DateTime?)new DateTime(1900, 1, 1, 0, 0, 0);
                    return dt >= lstWeekSetup[0].StartTime;
                });

                if (lstAllFixedDoorStartWeek.Count > 0)
                {
                    lstAllFixedDoor = Utility.MergeListCollections(lstAllFixedDoor, lstAllFixedDoorStartWeek);
                }
            }
        }

        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE
        {
            Action = "ShowAll"
        };
        List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);

        lstNewSlotTime = lstSlotTime;

        //loop for vertical
        for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
        {
            string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + "." + lstWeekSetup[0].StartTime.Value.Minute.ToString("00");
            if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
            {
                StartTime = "24." + lstWeekSetup[0].StartTime.Value.Minute.ToString("00");
            }

            string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString("00");
            if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
            {
                EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString("00");
            }

            decimal dSlotTime = Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.'));
            decimal dStartTime = Convert.ToDecimal(StartTime);
            decimal dEndTime = Convert.ToDecimal(EndTime);

            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
            {
                dEndTime = Convert.ToDecimal("24.00");
            }

            if (dSlotTime >= dStartTime && dSlotTime < dEndTime)
            {
                TableRow tr = new TableRow();
                tr.TableSection = TableRowSection.TableBody;
                TableCell tc = new TableCell();
                tc.BorderWidth = Unit.Pixel(1);

                tc.Text = lstSlotTime.Count - 1 > jCount
                    ? lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime)
                    : lstWeekSetup[0].StartWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);

                tc.Width = Unit.Pixel(i1stColumnWidth);
                tr.Cells.Add(tc);

                //loop for horizontal
                for (int iCount = 1; iCount <= iDoorCount; iCount++)
                {
                    tc = new TableCell();
                    tc.BorderWidth = Unit.Pixel(1);
                    tc.Width = Unit.Pixel(dblCellWidth);
                    iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
                    iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
                    sTimeSlot = lstSlotTime[jCount].SlotTime;
                    sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
                    sSlotStartDay = lstWeekSetup[0].StartWeekday;
                    SiteDoorNumberID = Convert.ToString(lstDoorNoType[iCount - 1].DoorType.DoorTypeID);
                    //----Sprint 3b------//
                    iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
                    //------------------//
                    //test  for an other day
                    objWeekSetup = lstWeekSetup[0];
                    strWeekDay = lstWeekSetup[0].StartWeekday;

                    Literal ddl = SetCellColor(ref tc);
                    tc.Controls.Add(ddl);
                    tr.Cells.Add(tc);
                }
                tblSlot.Rows.Add(tr);
            }
        }

        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        {
            for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
            {
                string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString("00");

                if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00")
                {
                    EndTime = "24." + lstWeekSetup[0].EndTime.Value.Minute.ToString("00");
                }
                if (Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.')) == Convert.ToDecimal(EndTime))
                {
                }
                if (Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.')) < Convert.ToDecimal(EndTime))
                {
                    TableRow tr = new TableRow();
                    tr.TableSection = TableRowSection.TableBody;
                    TableCell tc = new TableCell();
                    tc.BorderWidth = Unit.Pixel(1);

                    tc.Text = lstSlotTime.Count - 1 > jCount
                        ? lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[jCount + 1].SlotTime)
                        : lstWeekSetup[0].EndWeekday + " " + GetFormatedTime(lstSlotTime[jCount].SlotTime) + " - " + GetFormatedTime(lstSlotTime[0].SlotTime);

                    tc.Width = Unit.Pixel(i1stColumnWidth);
                    tr.Cells.Add(tc);

                    for (int iCount = 1; iCount <= iDoorCount; iCount++)
                    {
                        tc = new TableCell();
                        tc.Width = Unit.Pixel(dblCellWidth);
                        tc.BorderWidth = Unit.Pixel(1);
                        iTimeSlotID = lstSlotTime[jCount].SlotTimeID;
                        iSiteDoorID = lstDoorNoType[iCount - 1].SiteDoorNumberID;
                        sTimeSlot = lstSlotTime[jCount].SlotTime;
                        sDoorNo = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
                        sSlotStartDay = lstWeekSetup[0].EndWeekday;
                        SiteDoorNumberID = Convert.ToString(lstDoorNoType[iCount - 1].DoorType.DoorTypeID);
                        //----Sprint 3b------//
                        iTimeSlotOrderBYID = lstSlotTime[jCount].OrderBYID;
                        //------------------//

                        //test  for an other day
                        objWeekSetup = lstWeekSetup[0];
                        strWeekDay = lstWeekSetup[0].EndWeekday;

                        Literal ddl = SetCellColor(ref tc);
                        tc.Controls.Add(ddl);
                        tr.Cells.Add(tc);
                    }
                    tblSlot.Rows.Add(tr);
                }
            }
        }

        try
        {
            for (int i = 0; i < lstSlot.Count; i++)
            {
                bool isDataPresent = dtAlternateSlot.AsEnumerable().Any(x => x.Field<string>("ArrSlotTime") == lstSlot[i].ArrSlotTime.ToString()
                              && x.Field<string>("EstSlotTime") == lstSlot[i].EstSlotTime.ToString()
                              && x.Field<string>("DoorNo") == lstSlot[i].DoorNo.ToString());
                if (isDataPresent == false)
                {
                    if (lstSlot[i].ArrSlotTime != lstWeekSetup[0].EndTime.Value.TimeOfDay.ToString())
                    {
                        DataRow dr = dtAlternateSlot.NewRow();
                        dr["SlotStartDay"] = lstSlot[i].SlotStartDay;
                        dr["TimeSlotOrderBYID"] = lstSlot[i].siTimeSlotOrderBYID;
                        dr["ArrSlotTime"] = lstSlot[i].ArrSlotTime;
                        dr["EstSlotTime"] = lstSlot[i].EstSlotTime;
                        dr["SiteDoorNumberID"] = lstSlot[i].SiteDoorNumberID;
                        dr["SlotTimeID"] = lstSlot[i].SlotTimeID;
                        dr["DoorNo"] = lstSlot[i].DoorNo;
                        dr["Color"] = "Black";
                        dr["Pallets"] = "0";
                        dr["Cartoons"] = "0";
                        dr["Lines"] = "0";
                        dr["Lifts"] = "0";
                        dr["FixedSlotID"] = "";
                        dr["Vendor"] = "";
                        dr["SupplierType"] = "";
                        dr["AllocationType"] = "";
                        dr["ArrSlotTime15min"] = "";
                        dr["EstSlotTime15min"] = "";
                        dr["DoorNameDisplay"] = "";
                        dr["DoorNoDisplay"] = "";
                        dr["VendorDisplay"] = "";
                        dr["BookingID"] = "";
                        dr["BookingStatus"] = "Unconfirmed Fixed";
                        dr["BookingStatusID"] = "-1";
                        dtAlternateSlot.Rows.Add(dr);
                        dtAlternateSlot.AcceptChanges();
                    }
                }
            }

            if (dtAlternateSlot.Rows.Count > 0)
            {
                for (int i = 0; i < dtAlternateSlot.Rows.Count; i++)
                {
                    string weekday = string.Empty;
                    if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "SUN")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "1";
                    }
                    else if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "MON")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "2";
                    }
                    else if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "TUE")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "3";
                    }
                    else if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "WED")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "4";
                    }
                    else if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "THU")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "5";
                    }
                    else if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "FRI")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "6";
                    }
                    else if (dtAlternateSlot.Rows[i]["SlotStartDay"].ToString().ToUpper() == "SAT")
                    {
                        dtAlternateSlot.Rows[i]["Weekday"] = "7";
                    }

                    DataRow dr = dtAlternateSlot.Rows[i];
                    if (dr["Color"].ToString().Trim() == "Orange")
                    {
                        dr.Delete();
                        dtAlternateSlot.AcceptChanges();
                    }

                    if (dtAlternateSlot.Rows[i]["Color"].ToString().Trim() == "White")
                    {
                        dtAlternateSlot.Rows[i]["Vendor"] = "";
                        dtAlternateSlot.Rows[i]["Pallets"] = "-";
                        dtAlternateSlot.Rows[i]["Cartoons"] = "-";
                        dtAlternateSlot.Rows[i]["Lines"] = "-";
                        dtAlternateSlot.Rows[i]["Lifts"] = "-";
                    }
                    if (dtAlternateSlot.Rows[i]["Pallets"].ToString().Trim() == "-1" || dtAlternateSlot.Rows[i]["Pallets"].ToString().Trim() == "0")
                    {
                        dtAlternateSlot.Rows[i]["Pallets"] = "-";
                    }
                    if (dtAlternateSlot.Rows[i]["Cartoons"].ToString().Trim() == "-1" || dtAlternateSlot.Rows[i]["Cartoons"].ToString().Trim() == "0")
                    {
                        dtAlternateSlot.Rows[i]["Cartoons"] = "-";
                    }
                    if (dtAlternateSlot.Rows[i]["Lines"].ToString().Trim() == "-1" || dtAlternateSlot.Rows[i]["Lines"].ToString().Trim() == "0")
                    {
                        dtAlternateSlot.Rows[i]["Lines"] = "-";
                    }
                    if (dtAlternateSlot.Rows[i]["Lifts"].ToString().Trim() == "-1" || dtAlternateSlot.Rows[i]["Lifts"].ToString().Trim() == "0")
                    {
                        dtAlternateSlot.Rows[i]["Lifts"] = "-";
                    }

                    if (dtAlternateSlot.Rows[i]["AllocationType"].ToString().Trim() == "S" && dtAlternateSlot.Rows[i]["Color"].ToString().Trim() != "GreenYellow")
                    {
                        dtAlternateSlot.Rows[i]["Color"] = "Pink";
                    }

                    string[] ArrSlotTime = dtAlternateSlot.Rows[i]["ArrSlotTime"].ToString().Split(':');
                    string[] EstSlotTime = dtAlternateSlot.Rows[i]["EstSlotTime"].ToString().Split(':');

                    //commented for testing slot for 15 min

                    if (ArrSlotTime[1] == "00")
                    {
                        dtAlternateSlot.Rows[i]["ArrSlotTime15min"] = ArrSlotTime[0] + ":00";
                    }

                    if (EstSlotTime[1] == "15")
                    {
                        dtAlternateSlot.Rows[i]["EstSlotTime15min"] = EstSlotTime[0] + ":15";
                    }

                    if (ArrSlotTime[1] == "15")
                    {
                        dtAlternateSlot.Rows[i]["ArrSlotTime15min"] = ArrSlotTime[0] + ":15";
                    }

                    if (EstSlotTime[1] == "30")
                    {
                        dtAlternateSlot.Rows[i]["EstSlotTime15min"] = EstSlotTime[0] + ":30";
                    }

                    if (ArrSlotTime[1] == "30")
                    {
                        dtAlternateSlot.Rows[i]["ArrSlotTime15min"] = ArrSlotTime[0] + ":30";
                    }

                    if (EstSlotTime[1] == "45")
                    {
                        dtAlternateSlot.Rows[i]["EstSlotTime15min"] = EstSlotTime[0] + ":45";
                    }

                    if (ArrSlotTime[1] == "45")
                    {
                        dtAlternateSlot.Rows[i]["ArrSlotTime15min"] = ArrSlotTime[0] + ":45";
                    }

                    if (EstSlotTime[1] == "00")
                    {
                        if (EstSlotTime[0] != "23" && EstSlotTime[1] != "00")
                        {
                            int EstSlotTimeNew = Convert.ToInt32(EstSlotTime[0]) + 1;
                            dtAlternateSlot.Rows[i]["EstSlotTime15min"] = Convert.ToInt32(EstSlotTime[0]) + 1 + ":00";
                        }
                        else
                        {
                            // new logic for time
                            dtAlternateSlot.Rows[i]["EstSlotTime15min"] = EstSlotTime[0] == "23" && EstSlotTime[1] != "00" ? "00:00" : EstSlotTime[0] + ":00";
                        }
                    }

                    if (dtAlternateSlot.Rows[i]["EstSlotTime15min"].ToString().Split(':')[0].Length == 1)
                    {
                        dtAlternateSlot.Rows[i]["EstSlotTime15min"] = "0" + dtAlternateSlot.Rows[i]["EstSlotTime15min"].ToString().Split(':')[0] + ":00";
                    }
                    if (dtAlternateSlot.Rows[i]["ArrSlotTime"].ToString().Substring(0, 5).Split(':')[0].ToArray().Length == 1)
                    {
                        dtAlternateSlot.Rows[i]["ArrSlotTime"] = "0" + dtAlternateSlot.Rows[i]["ArrSlotTime"].ToString().Substring(0, 5);
                    }
                    else
                    {
                        dtAlternateSlot.Rows[i]["ArrSlotTime"] = dtAlternateSlot.Rows[i]["ArrSlotTime"].ToString().Substring(0, 5);
                    }
                    dtAlternateSlot.Rows[i]["EstSlotTime"] = dtAlternateSlot.Rows[i]["EstSlotTime"].ToString().Substring(0, 5).Split(':')[0].ToArray().Length == 1
                        ? "0" + dtAlternateSlot.Rows[i]["EstSlotTime"].ToString().Substring(0, 5)
                        : dtAlternateSlot.Rows[i]["EstSlotTime"].ToString().Substring(0, 5);

                    var SlotID = lstSlotTime.Where(x => x.SlotTime == dtAlternateSlot.Rows[i]["ArrSlotTime"].ToString().Trim());
                    if (SlotID != null)
                    {
                        dtAlternateSlot.Rows[i]["SlotTimeID"] = SlotID.Select(x => x.SlotTimeID).FirstOrDefault();
                    }
                    if (dtAlternateSlot.Rows[i]["EstSlotTime"].ToString() == "00:00")
                    {
                        dtAlternateSlot.Rows[i]["EstSlotTime"] = "00:00"; // to categories 00:00 in min and max
                    }
                    if (Convert.ToString(dtAlternateSlot.Rows[i]["BookingStatus"]) == "")
                    {
                        dtAlternateSlot.Rows[i]["BookingStatus"] = "Unconfirmed Fixed";
                    }
                    if (Convert.ToString(dtAlternateSlot.Rows[i]["BookingStatusID"]) == "" || Convert.ToString(dtAlternateSlot.Rows[i]["BookingStatusID"]) == "0")
                    {
                        dtAlternateSlot.Rows[i]["BookingStatusID"] = "-1";
                    }
                }

                for (int i = 0; i < dtAlternateSlot.Rows.Count; i++)
                {
                    // Remove multipe data due to 15 min slots
                    bool isDataPresent = dtAlternateSlot.AsEnumerable().Any(x => x.Field<string>("ArrSlotTime15min") == dtAlternateSlot.Rows[i]["ArrSlotTime15min"].ToString()
                                   && x.Field<string>("EstSlotTime15min") == dtAlternateSlot.Rows[i]["EstSlotTime15min"].ToString()
                                   && x.Field<string>("DoorNo") == dtAlternateSlot.Rows[i]["DoorNo"].ToString()
                                   && x.Field<string>("Color") == "Black");
                    if (isDataPresent)
                    {
                        var lst = dtAlternateSlot.AsEnumerable().Where(x => x.Field<string>("ArrSlotTime15min") == dtAlternateSlot.Rows[i]["ArrSlotTime15min"].ToString()
                                       && x.Field<string>("EstSlotTime15min") == dtAlternateSlot.Rows[i]["EstSlotTime15min"].ToString()
                                       && x.Field<string>("DoorNo") == dtAlternateSlot.Rows[i]["DoorNo"].ToString()).ToList();
                        if (lst.Count() >= 2)
                        {
                            int count = lst.Select(x => x.Field<string>("Color")).Distinct().Count();
                            if (count >= 2)
                            {
                                for (int j = 0; j < lst.Count(); j++)
                                {
                                    if (lst[j]["Color"].ToString() == "Black")
                                    {
                                        DataRow drDelete = dtAlternateSlot.AsEnumerable().Where(x => x.Field<string>("ArrSlotTime15min") == dtAlternateSlot.Rows[i]["ArrSlotTime15min"].ToString()
                                                   && x.Field<string>("EstSlotTime15min") == dtAlternateSlot.Rows[i]["EstSlotTime15min"].ToString()
                                                   && x.Field<string>("DoorNo") == dtAlternateSlot.Rows[i]["DoorNo"].ToString()
                                                   && x.Field<string>("Color") == "Black").FirstOrDefault();
                                        drDelete.Delete();
                                        dtAlternateSlot.AcceptChanges();
                                    }
                                }
                            }
                        }
                    }
                }

                dtAlternateSlot = (from c in dtAlternateSlot.AsEnumerable()
                                   orderby c.Field<string>("Weekday"), c.Field<string>("ArrSlotTime")
                                   select c).CopyToDataTable();

                //// setting dummy fixed slot id to get isolate record for pop up data
                var UpdateDummyFixedSLot = (from x in dtAlternateSlot.AsEnumerable()
                                            where (x.Field<string>("FixedSlotID").Trim() == ""
                                            // FOR GREEN OR CONFIRM BOX
                                            )
                                                  && x.Field<string>("Color").Trim() == "Purple"
                                            select x).ToList();

                var UpdateDummyFixedSlotTime = (from x in dtAlternateSlot.AsEnumerable()
                                                where (x.Field<string>("FixedSlotID").Trim() == "")
                                                      && x.Field<string>("Color").Trim() == "Purple"
                                                group x by new
                                                {
                                                    EstSlotTime15min = x.Field<string>("EstSlotTime15min"),
                                                    ArrSlotTime15min = x.Field<string>("ArrSlotTime15min"),
                                                    DoorNo = x.Field<string>("DoorNo")
                                                } into t
                                                select t).ToList();

                int dummy = 1000000;
                foreach (var item in UpdateDummyFixedSlotTime)
                {
                    var data = UpdateDummyFixedSLot.Where(x => x.Field<string>("ArrSlotTime15min") == item.Key.ArrSlotTime15min
                                                          && x.Field<string>("EstSlotTime15min") == item.Key.EstSlotTime15min
                                                          && x.Field<string>("DoorNo") == item.Key.DoorNo
                                                          ).ToList();
                    for (int i = 0; i < data.Count; i++)
                    {
                        data[i]["FixedSlotID"] = dummy.ToString();
                        dtAlternateSlot.AcceptChanges();
                    }
                    dummy = dummy + 1;
                }
                dummy = dummy + 1;
            }
            // Update Door Name

            foreach (var item in lstDoorNoType)
            {
                (from c in dtAlternateSlot.AsEnumerable()
                 where c.Field<string>("DoorNo") == Convert.ToString(item.SiteDoorNumberID)
                 select c).ToList().ForEach(items =>
                                                    {
                                                        items["DoorNameDisplay"] = item.DoorType.DoorType;
                                                        items["DoorNoDisplay"] = item.DoorNo.DoorNumber;
                                                    });
            }

            var dtAlternateSlotFinal = (from c in dtAlternateSlot.AsEnumerable()
                                        group c by new
                                        {
                                            Vendor = c.Field<string>("Vendor"),
                                            Pallets = c.Field<string>("Pallets"),
                                            Cartoons = c.Field<string>("Cartoons"),
                                            Lines = c.Field<string>("Lines"),
                                            Lifts = c.Field<string>("Lifts"),
                                            Color = c.Field<string>("Color"),
                                            DoorNo = c.Field<string>("DoorNo"),
                                            FixedSlotID = c.Field<string>("FixedSlotID"),

                                            SiteDoorNumberID = c.Field<string>("SiteDoorNumberID"),
                                            SupplierType = c.Field<string>("SupplierType"),
                                            AllocationType = c.Field<string>("AllocationType"),
                                            ArrSlotTime15min = c.Field<string>("ArrSlotTime15min"),
                                            EstSlotTime15min = c.Field<string>("EstSlotTime15min"),
                                            DoorNameDisplay = c.Field<string>("DoorNameDisplay"),
                                            DoorNoDisplay = c.Field<string>("DoorNoDisplay"),
                                            VendorDisplay = c.Field<string>("Vendor"),
                                            BookingID = c.Field<string>("BookingID"),
                                            BookingStatus = c.Field<string>("BookingStatus"),
                                            BookingStatusID = c.Field<string>("BookingStatusID"),
                                            Weekday = c.Field<string>("Weekday")///
                                        } into g
                                        select new AlternateSlot
                                        {
                                            Vendor = g.Key.Vendor,
                                            Pallets = g.Key.Pallets,
                                            Cartoons = g.Key.Cartoons,
                                            Lines = g.Key.Lines,
                                            Lifts = g.Key.Lifts,
                                            Color = g.Key.Color,
                                            DoorNo = g.Key.DoorNo,
                                            FixedSlotID = g.Key.FixedSlotID,
                                            SlotTimeID = "",
                                            SiteDoorNumberID = g.Key.SiteDoorNumberID,
                                            SupplierType = g.Key.SupplierType,
                                            AllocationType = g.Key.AllocationType,
                                            ArrSlotTime15min = g.Key.ArrSlotTime15min,
                                            EstSlotTime15min = g.Key.EstSlotTime15min,
                                            DoorNameDisplay = g.Key.DoorNameDisplay,
                                            DoorNoDisplay = g.Key.DoorNoDisplay,
                                            VendorDisplay = g.Key.Vendor,
                                            ArrSlotTime = "",
                                            EstSlotTime = "",
                                            BookingID = g.Key.BookingID,
                                            BookingStatus = g.Key.BookingStatus,
                                            BookingStatusID = g.Key.BookingStatusID,
                                            Weekday = g.Key.Weekday///
                                        }).ToList();
            // set Orange color
            var SlotFinal = (from c in dtAlternateSlot.AsEnumerable()
                             select new AlternateSlot
                             {
                                 DoorNo = c.Field<string>("DoorNo"),
                                 FixedSlotID = c.Field<string>("FixedSlotID"),
                                 ArrSlotTime = c.Field<string>("ArrSlotTime15min"),
                                 EstSlotTime = c.Field<string>("EstSlotTime15min"),
                                 Weekday = c.Field<string>("Weekday")///
                             }).Distinct();

            foreach (var item in SlotFinal)
            {
                var dataCheckExtraRow = dtAlternateSlotFinal.Where(x => x.DoorNo == item.DoorNo && x.Color.Trim() != "White" && x.ArrSlotTime15min == item.ArrSlotTime && x.EstSlotTime15min == item.EstSlotTime && x.Weekday == item.Weekday);
                if (dataCheckExtraRow.Count() > 0)
                {
                    // remove extra white if there is already other color data is present
                    AlternateSlot removedata = dtAlternateSlotFinal.ToList().Where(x => x.DoorNo == item.DoorNo && x.Color.Trim() == "White" && x.ArrSlotTime15min == item.ArrSlotTime && x.EstSlotTime15min == item.EstSlotTime && x.Weekday == item.Weekday).FirstOrDefault();
                    dtAlternateSlotFinal.Remove(removedata);

                    if (dataCheckExtraRow.Count() >= 1)
                    {
                        // having fixed slot side by side Remove count check if client need all
                        // orange color in slot overspill
                        int count = 1;
                        foreach (var itemdataCheckExtraRow in dataCheckExtraRow)
                        {
                            if (count != 1 && itemdataCheckExtraRow.Color.Trim() != "GreenYellow")
                            {
                                itemdataCheckExtraRow.Color = "Orange";
                            }
                            count++;
                        }
                    }
                }
            }

            // data for pop up
            var SetMinMaxTimeForPallets = (from x in dtAlternateSlot.AsEnumerable()
                                           where x.Field<string>("Color").Trim() != "White"
                                           group x by new
                                           {
                                               Vendor = x.Field<string>("Vendor"),
                                               Color = x.Field<string>("Color"),
                                               DoorNo = x.Field<string>("DoorNo"),
                                               FixedSlotID = x.Field<string>("FixedSlotID"),
                                               SupplierType = x.Field<string>("SupplierType"),
                                               AllocationType = x.Field<string>("AllocationType"),
                                               WeekDay = x.Field<string>("WeekDay"),
                                               BookingID = x.Field<string>("BookingID")
                                           } into g
                                           orderby WeekDay
                                           select new AlternateSlot
                                           {
                                               Color = g.Key.Color,
                                               Vendor = g.Key.Vendor,
                                               DoorNo = g.Key.DoorNo,
                                               FixedSlotID = g.Key.FixedSlotID,
                                               ArrSlotTime = g.Min(x => x.Field<string>("ArrSlotTime")),
                                               EstSlotTime = g.Max(x => x.Field<string>("EstSlotTime")),
                                               SlotStartDay = g.Min(x => x.Field<string>("SlotStartDay")),
                                               siTimeSlotOrderBYID = Convert.ToString(g.Min(x => Int32.Parse(x.Field<string>("TimeSlotOrderBYID")))),
                                               SlotTimeID = Convert.ToString(g.Min(x => Int32.Parse(x.Field<string>("SlotTimeID")))),// Minimun slot id for click
                                               Weekday = g.Key.WeekDay,
                                               BookingID = g.Key.BookingID
                                           }).ToList();

            string StartDay = string.Empty;

            string EndDay = string.Empty;

            if (lstWeekSetup[0].StartWeekday.ToUpper() == "SUN")
            {
                StartDay = "1";
            }
            else if (lstWeekSetup[0].StartWeekday.ToUpper() == "MON")
            {
                StartDay = "2";
            }
            else if (lstWeekSetup[0].StartWeekday.ToUpper() == "TUE")
            {
                StartDay = "3";
            }
            else if (lstWeekSetup[0].StartWeekday.ToUpper() == "WED")
            {
                StartDay = "4";
            }
            else if (lstWeekSetup[0].StartWeekday.ToUpper() == "THU")
            {
                StartDay = "5";
            }
            else if (lstWeekSetup[0].StartWeekday.ToUpper() == "FRI")
            {
                StartDay = "6";
            }
            else if (lstWeekSetup[0].StartWeekday.ToUpper() == "SAT")
            {
                StartDay = "7";
            }

            if (lstWeekSetup[0].EndWeekday.ToUpper() == "SUN")
            {
                EndDay = "1";
            }
            else if (lstWeekSetup[0].EndWeekday.ToUpper() == "MON")
            {
                EndDay = "2";
            }
            else if (lstWeekSetup[0].EndWeekday.ToUpper() == "TUE")
            {
                EndDay = "3";
            }
            else if (lstWeekSetup[0].EndWeekday.ToUpper() == "WED")
            {
                EndDay = "4";
            }
            else if (lstWeekSetup[0].EndWeekday.ToUpper() == "THU")
            {
                EndDay = "5";
            }
            else if (lstWeekSetup[0].EndWeekday.ToUpper() == "FRI")
            {
                EndDay = "6";
            }
            else if (lstWeekSetup[0].EndWeekday.ToUpper() == "SAT")
            {
                EndDay = "7";
            }

            var SetMinMaxTimeForPalletsOld = SetMinMaxTimeForPallets.Where(x => x.Weekday == StartDay).ToList();
            var SetMinMaxTimeForPalletsNew = SetMinMaxTimeForPallets.Where(x => x.Weekday == EndDay).ToList();

            //for across the day
            for (int i = 0; i < SetMinMaxTimeForPalletsOld.Count; i++)
            {
                if (SetMinMaxTimeForPalletsNew.FindAll(x => x.Color == SetMinMaxTimeForPalletsOld[i].Color && x.Vendor == SetMinMaxTimeForPalletsOld[i].Vendor && x.DoorNo == SetMinMaxTimeForPalletsOld[i].DoorNo
                && x.FixedSlotID == SetMinMaxTimeForPalletsOld[i].FixedSlotID
                && x.BookingID == SetMinMaxTimeForPalletsOld[i].BookingID).Count > 0)
                {
                    foreach (var z in SetMinMaxTimeForPalletsNew.FindAll(x => x.Color == SetMinMaxTimeForPalletsOld[i].Color && x.Vendor == SetMinMaxTimeForPalletsOld[i].Vendor && x.DoorNo == SetMinMaxTimeForPalletsOld[i].DoorNo
                    && x.FixedSlotID == SetMinMaxTimeForPalletsOld[i].FixedSlotID
                    && x.BookingID == SetMinMaxTimeForPalletsOld[i].BookingID))
                    {
                        z.ArrSlotTime = SetMinMaxTimeForPalletsOld[i].ArrSlotTime;
                        z.SlotStartDay = SetMinMaxTimeForPalletsOld[i].SlotStartDay;
                        z.siTimeSlotOrderBYID = SetMinMaxTimeForPalletsOld[i].siTimeSlotOrderBYID;
                        z.SlotTimeID = SetMinMaxTimeForPalletsOld[i].SlotTimeID;
                    }
                }
                else
                {
                    SetMinMaxTimeForPalletsNew.Add(SetMinMaxTimeForPalletsOld[i]);
                }
            }

            SetMinMaxTimeForPallets = SetMinMaxTimeForPalletsNew;
            //change for time having in both weekday

            foreach (var itemSetMinMaxTimeForPallets in SetMinMaxTimeForPallets)
            {
                dtAlternateSlotFinal.ToList().Where(x => x.DoorNo == itemSetMinMaxTimeForPallets.DoorNo &&
                x.Color.Trim() != "White"
                                          && x.Vendor == itemSetMinMaxTimeForPallets.Vendor
                                          && x.FixedSlotID == itemSetMinMaxTimeForPallets.FixedSlotID
                                          && x.BookingID == itemSetMinMaxTimeForPallets.BookingID
                                          ).ToList().ForEach(
                 items =>
                {
                    items.ArrSlotTime = itemSetMinMaxTimeForPallets.ArrSlotTime;
                    items.EstSlotTime = itemSetMinMaxTimeForPallets.EstSlotTime;
                    items.SlotStartDay = itemSetMinMaxTimeForPallets.SlotStartDay;
                    items.siTimeSlotOrderBYID = itemSetMinMaxTimeForPallets.siTimeSlotOrderBYID;
                    items.SlotTimeID = itemSetMinMaxTimeForPallets.SlotTimeID;
                    items.Vendor = itemSetMinMaxTimeForPallets.Vendor;
                });
            }
            // data for clickable
             (from x in dtAlternateSlot.AsEnumerable()
              where x.Field<string>("Color").Trim() == "White"
              group x by new
              {
                  Color = x.Field<string>("Color"),
                  DoorNo = x.Field<string>("DoorNo"),
                  ArrSlotTime15min = x.Field<string>("ArrSlotTime15min"),
                  EstSlotTime15min = x.Field<string>("EstSlotTime15min"),
                  Weekday = x.Field<string>("Weekday"),
                  BookingID = x.Field<string>("BookingID")
              } into g
              select new AlternateSlot
              {
                  DoorNo = g.Key.DoorNo,
                  SlotStartDay = g.Min(x => x.Field<string>("SlotStartDay")),
                  siTimeSlotOrderBYID = Convert.ToString(g.Min(x => Int32.Parse(x.Field<string>("TimeSlotOrderBYID")))),
                  SlotTimeID = Convert.ToString(g.Min(x => Int32.Parse(x.Field<string>("SlotTimeID")))),// Minimun slot id for click
                  ArrSlotTime15min = g.Key.ArrSlotTime15min,
                  EstSlotTime15min = g.Key.EstSlotTime15min,
                  ArrSlotTime = g.Min(x => x.Field<string>("ArrSlotTime")),
                  EstSlotTime = g.Max(x => x.Field<string>("EstSlotTime")),
                  Weekday = g.Max(x => x.Field<string>("Weekday")),
                  BookingID = g.Key.BookingID
              }).ToList().ForEach(itemSetMinMaxTimeForPallets =>
                {
                    dtAlternateSlotFinal.ToList().Where(x => x.DoorNo == itemSetMinMaxTimeForPallets.DoorNo
                                                              && x.Color.Trim() == "White"
                                                              && x.ArrSlotTime15min == itemSetMinMaxTimeForPallets.ArrSlotTime15min
                                                              && x.EstSlotTime15min == itemSetMinMaxTimeForPallets.EstSlotTime15min
                                                              && x.DoorNo == itemSetMinMaxTimeForPallets.DoorNo && x.Weekday == itemSetMinMaxTimeForPallets.Weekday
                                                              && x.BookingID == itemSetMinMaxTimeForPallets.BookingID).ToList().
                                                              ForEach(items =>
                                    {
                                        items.SlotStartDay = itemSetMinMaxTimeForPallets.SlotStartDay;
                                        items.siTimeSlotOrderBYID = itemSetMinMaxTimeForPallets.siTimeSlotOrderBYID;
                                        items.SlotTimeID = itemSetMinMaxTimeForPallets.SlotTimeID;
                                        items.ArrSlotTime = itemSetMinMaxTimeForPallets.ArrSlotTime;
                                        items.EstSlotTime = itemSetMinMaxTimeForPallets.EstSlotTime;
                                        items.BookingID = itemSetMinMaxTimeForPallets.BookingID;
                                    });
                });

            // data for pop up and blank consecutive name of vendor
            string PreviousVendorname = string.Empty;
            string PreviousFixedSlotID = string.Empty;
            string PreviousDoor = string.Empty;
            string PreviousSlotID = string.Empty;
            string PreviousBookingID = string.Empty;

            var filter = dtAlternateSlotFinal.ToList().OrderBy(x => x.Vendor).ThenBy(x => x.DoorNo).ThenBy(x => x.FixedSlotID);

            foreach (var items in filter)
            {
                string Door = items.DoorNo.ToString();
                string FixedSlotID = items.FixedSlotID.ToString();
                string Vendor = items.Vendor.ToString();
                string BookingID = items.BookingID;

                if (items.Vendor.ToString().Trim() == PreviousVendorname && PreviousFixedSlotID == FixedSlotID && PreviousDoor == Door
                  && PreviousBookingID == items.BookingID)
                {
                    items.VendorDisplay = items.Vendor;
                    items.Vendor = "";
                    items.SlotTimeID = items.SlotTimeID;
                }
                else
                {
                    PreviousVendorname = "";
                    PreviousFixedSlotID = "";
                    PreviousDoor = "";
                    PreviousSlotID = "";
                }
                PreviousVendorname = Vendor;
                PreviousFixedSlotID = FixedSlotID;
                PreviousDoor = Door;
                PreviousBookingID = BookingID;
            }

            SetMinMaxTimeForPallets = (from x in dtAlternateSlot.AsEnumerable()
                                       group x by new
                                       {
                                           ArrSlotTime15min = x.Field<string>("ArrSlotTime15min"),
                                           EstSlotTime15min = x.Field<string>("EstSlotTime15min"),
                                           BookingID = x.Field<string>("BookingID")
                                       } into g
                                       select new AlternateSlot
                                       {
                                           ArrSlotTime15min = g.Key.ArrSlotTime15min,
                                           EstSlotTime15min = g.Key.EstSlotTime15min,
                                           BookingID = g.Key.BookingID,
                                           SlotTimeID = Convert.ToString(g.Min(x => Int32.Parse(x.Field<string>("SlotTimeID")))),// Minimun slot id for click
                                       }).ToList();

            SetMinMaxTimeForPallets.ForEach(itemSetMinMaxTimeForPallets =>
            {
                dtAlternateSlotFinal.ToList().Where(x => x.Color.Trim() != "White"
                                         && x.ArrSlotTime15min == itemSetMinMaxTimeForPallets.ArrSlotTime15min
                                         && x.EstSlotTime15min == itemSetMinMaxTimeForPallets.EstSlotTime15min
                                         && x.BookingID == itemSetMinMaxTimeForPallets.BookingID).ToList().ForEach(items =>
                {
                    items.SlotTimeID = itemSetMinMaxTimeForPallets.SlotTimeID;

                    items.BookingID = itemSetMinMaxTimeForPallets.BookingID;
                });
            });

            oAPPBOK_BookingBE.Action = "AllTodayBookingWithVendorDetails";
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
            DataTable dtAlternateSlotFinalTable = new DataTable();
            DataTable dtAlternateSlotNewTable = new DataTable();

            dtAlternateSlotFinalTable.Columns.Add("SupplierType", typeof(string));
            dtAlternateSlotFinalTable.Columns.Add("BookingID", typeof(string));
            dtAlternateSlotFinalTable.Columns.Add("BookingTypeID", typeof(string));
            dtAlternateSlotFinalTable.Columns.Add("Issue", typeof(string));
            dtAlternateSlotFinalTable.Columns.Add("BookingStatus", typeof(string));
            dtAlternateSlotFinalTable.Columns.Add("Pallets", typeof(string));
            dtAlternateSlotFinalTable.Columns.Add("Lifts", typeof(string));
            dtAlternateSlotFinalTable.Columns.Add("Cartoons", typeof(string));
            dtAlternateSlotFinalTable.Columns.Add("Lines", typeof(string));
            dtAlternateSlotFinalTable.Columns.Add("Vendor", typeof(string));
            dtAlternateSlotFinalTable.Columns.Add("Carrier", typeof(string));

            DataTable dt = oAPPBOK_BookingBAL.GetAllTodayBookingWithVendorDetailsBAL(oAPPBOK_BookingBE);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dtAlternateSlotFinalTable.Rows.Add(new object[]
                {
                        Convert.ToString(dt.Rows[i]["SupplierType"]),
                        Convert.ToString(dt.Rows[i]["BookingID"]),
                        Convert.ToString(dt.Rows[i]["BookingTypeID"]),
                        Convert.ToString(dt.Rows[i]["Issue"]),
                        Convert.ToString(dt.Rows[i]["BookingStatus"]),
                        Convert.ToString(dt.Rows[i]["Pallets"]),
                        Convert.ToString(dt.Rows[i]["Lifts"]),
                        Convert.ToString(dt.Rows[i]["Cartoons"]),
                        Convert.ToString(dt.Rows[i]["Lines"]),
                        Convert.ToString(dt.Rows[i]["Vendor"]),
                        Convert.ToString(dt.Rows[i]["CarrierName"])
               });
            }

            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
            {
                oAPPBOK_BookingBE.Action = "AllTodayBookingWithVendorDetails";
                oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime.AddDays(-1);

                dtAlternateSlotNewTable.Columns.Add("SupplierType", typeof(string));
                dtAlternateSlotNewTable.Columns.Add("BookingID", typeof(string));
                dtAlternateSlotNewTable.Columns.Add("BookingTypeID", typeof(string));
                dtAlternateSlotNewTable.Columns.Add("Issue", typeof(string));
                dtAlternateSlotNewTable.Columns.Add("BookingStatus", typeof(string));
                dtAlternateSlotNewTable.Columns.Add("Pallets", typeof(string));
                dtAlternateSlotNewTable.Columns.Add("Lifts", typeof(string));
                dtAlternateSlotNewTable.Columns.Add("Cartoons", typeof(string));
                dtAlternateSlotNewTable.Columns.Add("Lines", typeof(string));
                dtAlternateSlotNewTable.Columns.Add("Vendor", typeof(string));
                dtAlternateSlotNewTable.Columns.Add("Carrier", typeof(string));

                DataTable dt1 = oAPPBOK_BookingBAL.GetAllTodayBookingWithVendorDetailsBAL(oAPPBOK_BookingBE);
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    dtAlternateSlotNewTable.Rows.Add(new object[]
                    {
                        Convert.ToString(dt1.Rows[i]["SupplierType"]),
                        Convert.ToString(dt1.Rows[i]["BookingID"]),
                        Convert.ToString(dt1.Rows[i]["BookingTypeID"]),
                        Convert.ToString(dt1.Rows[i]["Issue"]),
                        Convert.ToString(dt1.Rows[i]["BookingStatus"]),
                        Convert.ToString(dt1.Rows[i]["Pallets"]),
                        Convert.ToString(dt1.Rows[i]["Lifts"]),
                        Convert.ToString(dt1.Rows[i]["Cartoons"]),
                        Convert.ToString(dt1.Rows[i]["Lines"]),
                        Convert.ToString(dt1.Rows[i]["Vendor"]),
                        Convert.ToString(dt1.Rows[i]["CarrierName"])
                   });
                }
            }

            dtAlternateSlotFinalTable.Merge(dtAlternateSlotNewTable);

            //set logic for oRangeColor
            dtAlternateSlotFinal.ToList().Where(x => x.Color.Trim() == "Orange").ToList().ForEach(itemOrange =>
           {
               dtAlternateSlotFinal.ToList().Where(x => x.ArrSlotTime == itemOrange.ArrSlotTime && x.EstSlotTime
                   == itemOrange.EstSlotTime && x.DoorNo == itemOrange.DoorNo).ToList().ForEach(items =>
               {
                   items.Color = "Orange";
               });
           });

            DataTable dtAlternateSlotFinalTableBind = new DataTable();
            dtAlternateSlotFinalTableBind.Columns.Add("ArrSlotTime", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("EstSlotTime", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("Vendor", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("Pallets", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("Cartoons", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("Lines", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("Lifts", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("Color", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("DoorNo", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("FixedSlotID", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("SlotTimeID", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("SiteDoorNumberID", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("SupplierType", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("AllocationType", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("ArrSlotTime15min", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("EstSlotTime15min", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("DoorNameDisplay", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("DoorNoDisplay", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("VendorDisplay", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("SlotStartDay", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("iTimeSlotOrderBYID", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("BookingID", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("BookingStatus", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("BookingStatusID", typeof(string));
            dtAlternateSlotFinalTableBind.Columns.Add("WeekDay", typeof(string));///
            for (int i = 0; i < dtAlternateSlotFinal.Count(); i++)
            {
                dtAlternateSlotFinalTableBind.Rows.Add(new object[] {
                    dtAlternateSlotFinal[i].ArrSlotTime,
                    dtAlternateSlotFinal[i].EstSlotTime,
                    dtAlternateSlotFinal[i].Vendor,
                    dtAlternateSlotFinal[i].Pallets,
                    dtAlternateSlotFinal[i].Cartoons,
                    dtAlternateSlotFinal[i].Lines,
                    dtAlternateSlotFinal[i].Lifts,
                    dtAlternateSlotFinal[i].Color,
                    dtAlternateSlotFinal[i].DoorNo,
                    dtAlternateSlotFinal[i].FixedSlotID,
                    dtAlternateSlotFinal[i].SlotTimeID,
                    dtAlternateSlotFinal[i].SiteDoorNumberID,
                    dtAlternateSlotFinal[i].SupplierType,
                    dtAlternateSlotFinal[i].AllocationType,
                    dtAlternateSlotFinal[i].ArrSlotTime15min,
                    dtAlternateSlotFinal[i].EstSlotTime15min,
                    dtAlternateSlotFinal[i].DoorNameDisplay,
                    dtAlternateSlotFinal[i].DoorNoDisplay,
                    dtAlternateSlotFinal[i].VendorDisplay,
                    dtAlternateSlotFinal[i].SlotStartDay,
                    dtAlternateSlotFinal[i].siTimeSlotOrderBYID,
                    dtAlternateSlotFinal[i].BookingID,
                    dtAlternateSlotFinal[i].BookingStatus,
                    dtAlternateSlotFinal[i].BookingStatusID,
                    dtAlternateSlotFinal[i].Weekday
                    });
            }

            // Logic for Setting black/blue color
            decimal slotlength = Math.Ceiling(Convert.ToDecimal(actSlotLength * 15) / Convert.ToDecimal(15));
            // int Count = 0;
            string[] OpenDoor = strOpenDoorIds.Split(',');
            string[] OpenDoorVechileSetup = strVehicleDoorTypeMatrix.Split(',');
            string[] OpenVehicleDoorTypeMatrixSKU = strVehicleDoorTypeMatrixSKU.Split(',');

            var Distinctdoor = dtAlternateSlotFinalTableBind.AsEnumerable().Select(x => x.Field<string>("DoorNo")).Distinct();
            string FixHeader = "tableCol" + Convert.ToString(Distinctdoor.Count());
            int countID = 0;
            Table tblSlotNew = new Table();
            tblSlotNew.CellSpacing = 0;
            tblSlotNew.CellPadding = 0;
            tblSlotNew.CssClass = "tableData outerTable";
            tblSlotNew.ID = "Open_Text_GeneralNew";

            tblSlotNew.Attributes.Add("border-collapse", "collapse");
            TableRow trDoorNoNew = new TableRow();
            trDoorNoNew.TableSection = TableRowSection.TableHeader;
            TableCell tcDoorNoNew = new TableHeaderCell
            {
                CssClass = "timeCol",
                Text = "Time"
            };
            trDoorNoNew.Cells.Add(tcDoorNoNew);

            var DistinctdoorTable = (from c in dtAlternateSlotFinalTableBind.AsEnumerable()
                                     select new
                                     {
                                         DoorNameDisplay = c.Field<string>("DoorNameDisplay"),
                                         DoorNoDisplay = c.Field<string>("DoorNoDisplay"),
                                         DoorNo = c.Field<string>("DoorNo")
                                     }).Distinct().ToList();

            foreach (var item in DistinctdoorTable)
            {
                tcDoorNoNew = new TableHeaderCell
                {
                    Text = item.DoorNoDisplay + " / " + item.DoorNameDisplay,
                    CssClass = "borderRightMain"
                };
                trDoorNoNew.Cells.Add(tcDoorNoNew);
            }

            tblSlotNew.Rows.Add(trDoorNoNew);

            var DistinctSlotFinal = from c in SlotFinal.AsEnumerable()
                                    group c by new { ArrSlotTime = c.ArrSlotTime, Weekday = c.Weekday } into t
                                    select new { ArrSlotTime = t.Key.ArrSlotTime, Weekday = t.Key.Weekday };

            foreach (var item in DistinctSlotFinal)
            {
                TableRow tr = new TableRow
                {
                    TableSection = TableRowSection.TableBody
                };

                TableCell tc = new TableCell
                {
                    Text = item.ArrSlotTime.ToString(),
                    CssClass = "timeCol innerTable"
                };

                tr.Cells.Add(tc);

                foreach (var door in Distinctdoor)
                {
                    var filterTableTdRow = (from c in dtAlternateSlotFinalTableBind.AsEnumerable()
                                            where c.Field<string>("DoorNo") == door
                                            && c.Field<string>("ArrSlotTime15min")
                                            == item.ArrSlotTime && c.Field<string>("Weekday") == item.Weekday
                                            select new AlternateSlot
                                            {
                                                Vendor = c.Field<string>("Vendor"),
                                                Pallets = c.Field<string>("Pallets"),
                                                Cartoons = c.Field<string>("Cartoons"),
                                                Lines = c.Field<string>("Lines"),
                                                Lifts = c.Field<string>("Lifts"),
                                                Color = c.Field<string>("Color"),
                                                DoorNo = c.Field<string>("DoorNo"),
                                                FixedSlotID = c.Field<string>("FixedSlotID"),
                                                SlotTimeID = c.Field<string>("SlotTimeID"),
                                                SiteDoorNumberID = c.Field<string>("SiteDoorNumberID"),
                                                SupplierType = c.Field<string>("SupplierType"),
                                                AllocationType = c.Field<string>("AllocationType"),
                                                ArrSlotTime15min = c.Field<string>("ArrSlotTime15min"),
                                                EstSlotTime15min = c.Field<string>("EstSlotTime15min"),
                                                DoorNameDisplay = c.Field<string>("DoorNameDisplay"),
                                                DoorNoDisplay = c.Field<string>("DoorNoDisplay"),
                                                ArrSlotTime = c.Field<string>("ArrSlotTime"),
                                                EstSlotTime = c.Field<string>("EstSlotTime"),
                                                VendorDisplay = c.Field<string>("VendorDisplay"),
                                                SlotStartDay = c.Field<string>("SlotStartDay"),
                                                siTimeSlotOrderBYID = c.Field<string>("iTimeSlotOrderBYID"),
                                                BookingID = c.Field<string>("BookingID"),
                                                BookingStatus = c.Field<string>("BookingStatus"),
                                                BookingStatusID = c.Field<string>("BookingStatusID")
                                            }).ToList();

                    //need to have 4 cells for displaying data 
                    /*
                    if (filterTableTdRow.Where(x => x.Color == "Orange").Count() > 2)
                    {
                        // Remove consecutive data of orange with particular filter to avoid one
                        // more td
                        AlternateSlot RemoveData = filterTableTdRow.Where(x => x.Color.Trim() == "Orange" && x.Vendor == "").FirstOrDefault();
                        filterTableTdRow.Remove(RemoveData);
                    }

                    if (filterTableTdRow.Where(x => x.Color.Trim() == "Purple").Count() > 2)
                    {
                        // Remove consecutive data of Purple with particular filter to avoid one
                        // more td
                        AlternateSlot RemoveData = filterTableTdRow.Where(x => x.Color.Trim() == "Purple" && x.Vendor == "").FirstOrDefault();
                        filterTableTdRow.Remove(RemoveData);
                    }
                    */
                    tc = new TableCell
                    {
                        CssClass = "borderRightMain"
                    };

                    Table tblChild = new Table();

                    tblChild.CssClass = "tableData innerTable spetialtable";

                    TableRow trChild = new TableRow();

                    foreach (AlternateSlot filterTableTd in filterTableTdRow)
                    {
                        // count is used to create unique id of pop up
                        countID += 1;

                        TableCell tcChild = new TableCell();

                        tcChild.CssClass = "tableData spetialtable";

                        Literal ddl = new Literal
                        {
                            Text = filterTableTd.Vendor.ToString().Length > 10
                            ? filterTableTd.Vendor.ToString().Substring(0, 10)
                            : filterTableTd.Vendor.ToString()
                        };
                        if (filterTableTdRow.Count > 1)
                        {
                            if (filterTableTd.Color.Trim() == "Green")
                            {
                                tcChild.CssClass = "greenTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "Red")
                            {
                                tcChild.CssClass = "redTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "Orange")
                            {
                                tcChild.CssClass = "orangeTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "Black")
                            {
                                tcChild.CssClass = "blueTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "LightGray")
                            {
                                tcChild.CssClass = "greyTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "Pink")
                            {
                                tcChild.CssClass = "pinkTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "GreenYellow")
                            {
                                tcChild.CssClass = "greenYellowTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "Purple")
                            {
                                tcChild.CssClass = "greenTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else
                            {
                                tcChild.CssClass = "whiteTd ";
                            }
                        }
                        else
                        {
                            if (filterTableTd.Color.Trim() == "Green")
                            {
                                tcChild.CssClass = "greenTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "Red")
                            {
                                tcChild.CssClass = "redTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "Orange")
                            {
                                tcChild.CssClass = "orangeTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "Black")
                            {
                                tcChild.CssClass = "blueTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "LightGray")
                            {
                                tcChild.CssClass = "greyTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "Pink")
                            {
                                tcChild.CssClass = "pinkTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "GreenYellow")
                            {
                                tcChild.CssClass = "greenYellowTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else if (filterTableTd.Color.Trim() == "Purple")
                            {
                                tcChild.CssClass = "greenTd width" + filterTableTdRow.Count.ToString() + " td" + filterTableTdRow.Count.ToString();
                            }
                            else
                            {
                                tcChild.CssClass = "whiteTd ";
                            }
                        }

                        if (filterTableTd.Color.Trim() != "Black" && filterTableTd.Color.Trim() != "White" && filterTableTd.Color.Trim() != "LightGray" && filterTableTd.Color.Trim() != "GreenYellow" && filterTableTd.Color.Trim() != "White")
                        {
                            CreateDynamicDivNew(tcChild, filterTableTd.ArrSlotTime, filterTableTd.EstSlotTime == "25:00" ? "00:00" : filterTableTd.EstSlotTime, filterTableTd.Pallets, filterTableTd.Cartoons, filterTableTd.Lines, filterTableTd.Lifts, filterTableTd.VendorDisplay, countID, filterTableTd.BookingID, filterTableTd.BookingStatus, dtAlternateSlotFinalTable, filterTableTd.Color);
                        }

                        if (filterTableTd.FixedSlotID != "")
                        {
                            MakeCellClickableNew(tcChild, filterTableTd.DoorNo, filterTableTd.ArrSlotTime15min, filterTableTd.SlotStartDay, filterTableTd.BookingID, filterTableTd.SupplierType, filterTableTd.Color, filterTableTd.FixedSlotID, filterTableTd.BookingStatusID, filterTableTd.BookingStatus);
                        }

                        tcChild.Controls.Add(ddl);
                        trChild.Cells.Add(tcChild);
                        tblChild.Controls.Add(trChild);
                    }

                    if (filterTableTdRow.Count() == 0)
                    {
                        // if slot is not there in datatable then set it black
                        TableCell tcChild = new TableCell();
                        tcChild.CssClass = "tableData innerTable spetialtable td1 blueTd width170";
                        trChild.Cells.Add(tcChild);
                        tblChild.Controls.Add(trChild);
                    }

                    tc.Controls.Add(tblChild);
                    tr.Cells.Add(tc);
                    tblSlotNew.Rows.Add(tr);
                }
            }

            if (Session["Role"].ToString() != "Vendor" && Session["Role"].ToString() != "Carrier")
            {
                tableDiv_GeneralNew.Controls.Add(tblSlotNew);
            }
        }
        catch
        {
        }
        //-------------End Slot Ploting--------------//

        #endregion --------------Slot Ploting---------------
    }

    private static int ExtrcatWeekDay(List<MASSIT_WeekSetupBE> lstWeekSetup, int weekday)
    {
        if (lstWeekSetup[0].StartWeekday == "sun")
        {
            weekday = 1;
        }
        if (lstWeekSetup[0].StartWeekday == "mon")
        {
            weekday = 2;
        }
        if (lstWeekSetup[0].StartWeekday == "tue")
        {
            weekday = 3;
        }
        if (lstWeekSetup[0].StartWeekday == "wed")
        {
            weekday = 4;
        }
        if (lstWeekSetup[0].StartWeekday == "thu")
        {
            weekday = 5;
        }
        if (lstWeekSetup[0].StartWeekday == "fri")
        {
            weekday = 6;
        }
        if (lstWeekSetup[0].StartWeekday == "sat")
        {
            weekday = 7;
        }

        return weekday;
    }

    private void CreateDynamicDivNew(TableCell tcLocal, string SlotStartTime, string SlotEndTime, string Pallets, string Cartoons, string Lines, string Lifts, string Vendor, int UniqueID, string BookingID, string BookingStatus, DataTable dtAlternateSlotFinalTable, string color)
    {
        try
        {
            // Create a new HtmlGenericControl.
            HtmlGenericControl NewControl = new HtmlGenericControl("div");
            NewControl.ViewStateMode = ViewStateMode.Disabled;
            string ControlID = "VolumeNew" + Convert.ToString(UniqueID);
            HiddenField hdnEndTimeGenerated = new HiddenField();
            hdnEndTimeGenerated.ID = "hdnEndTimeGenerate" + Convert.ToString(UniqueID);
            hdnEndTimeGenerated.Value = SlotEndTime;

            NewControl.ID = "," + ControlID + ",";
            if (BookingStatus != "Unconfirmed Fixed")
            {
                var lstAlternateSlotFinalTable = dtAlternateSlotFinalTable.AsEnumerable().Where(x => x.Field<string>("BookingID") == BookingID).ToList();

                foreach (var door in lstAlternateSlotFinalTable)
                {
                    var filterTableRow = (from c in lstAlternateSlotFinalTable.AsEnumerable()
                                          select new AlternateSlot
                                          {
                                              Vendor = c.Field<string>("Vendor"),
                                              Pallets = c.Field<string>("Pallets"),
                                              Cartoons = c.Field<string>("Cartoons"),
                                              Lines = c.Field<string>("Lines"),
                                              Lifts = c.Field<string>("Lifts"),
                                              SupplierType = c.Field<string>("SupplierType"),
                                              BookingID = c.Field<string>("BookingID"),
                                              BookingStatus = c.Field<string>("BookingStatus"),
                                              Issue = c.Field<string>("Issue"),
                                              Carrier = c.Field<string>("Carrier")
                                          }).ToList();

                    string InnerHTML = "<table width='100%'>" +
                                  "<tr><td colspan = '5' align = 'center' ><strong>{VendorName}</strong></td></tr> " +
                                  "<tr> " +
                                  "<td colspan = '5'> " +
                                  " <table style = 'width:100%' >" +
                                  " <tr> " +
                                  "<td><strong> Status : </strong> {Status}<br/> <strong> Issue : </strong> {Issue} </td>" +
                                  "<td valign='top'> <strong> Arrival : </strong> {Atime}</td> " +
                                  "<td  valign='top'><strong> Est Depart: </strong>{DTime}</td> " +
                                  "</tr> " +
                                  "</table >" +
                                  "</td>" +
                                  " </tr> " +
                                  " <tr> " +
                                  "<td></td>" +
                                  "<td align='center'><strong> Lifts </strong></td> " +
                                  " <td align='center'><strong> Pallets </strong></td> " +
                                  "<td align='center'><strong> Cartons </strong></td> " +
                                  "<td align='center'><strong> Lines </strong></td> " +
                                  "</tr>" +
                                  "<tr> " +
                                  " <td> <strong> Total </strong></td> " +
                                  " <td align='center'>{LiftsTotal}</td> " +
                                  " <td align='center'>{PalletsTotal}</td> " +
                                  " <td align='center'>{CartonsTotal}</td> " +
                                  "<td align='center'>{LinesTotal}</td> " +
                                  "</tr> ";

                    InnerHTML = InnerHTML.Replace("{Atime}", SlotStartTime);
                    InnerHTML = InnerHTML.Replace("{DTime}", SlotEndTime);
                    InnerHTML = InnerHTML.Replace("{LiftsTotal}", Lifts.ToString());
                    InnerHTML = InnerHTML.Replace("{PalletsTotal}", Pallets.ToString());
                    InnerHTML = InnerHTML.Replace("{CartonsTotal}", Cartoons.ToString());
                    InnerHTML = InnerHTML.Replace("{LinesTotal}", Lines.ToString());

                    if (filterTableRow[0].SupplierType == "V")
                    {
                        InnerHTML = InnerHTML +
                                     "</table> ";

                        InnerHTML = InnerHTML.Replace("{Status}", filterTableRow[0].BookingStatus);
                        InnerHTML = InnerHTML.Replace("{Issue}", filterTableRow[0].Issue);
                        InnerHTML = InnerHTML.Replace("{VendorName}", filterTableRow[0].Vendor);
                    }
                    else
                    {
                        for (int i = 0; i < lstAlternateSlotFinalTable.Count; i++)
                        {
                            InnerHTML = InnerHTML + "<tr> " +
                                          "<td> <strong> {Vendor} </strong></td> " +
                                          "<td align='center'>{Lifts}</td>" +
                                          "<td align='center'>{Pallets}</td>" +
                                          "<td align='center'>{Cartons}</td> " +
                                          "<td align='center'>{Lines}</td> " +
                                          "</tr> ";

                            InnerHTML = InnerHTML.Replace("{Atime}", SlotStartTime);
                            InnerHTML = InnerHTML.Replace("{DTime}", SlotEndTime);
                            InnerHTML = InnerHTML.Replace("{Lifts}", filterTableRow[i].Lifts != "" ? filterTableRow[i].Lifts : "-");
                            InnerHTML = InnerHTML.Replace("{Pallets}", filterTableRow[i].Pallets != "" ? filterTableRow[i].Pallets : "-");
                            InnerHTML = InnerHTML.Replace("{Cartons}", filterTableRow[i].Cartoons != "" ? filterTableRow[i].Cartoons : "-");
                            InnerHTML = InnerHTML.Replace("{Lines}", filterTableRow[i].Lines != "" ? filterTableRow[i].Lines : "-");
                            InnerHTML = InnerHTML.Replace("{Status}", filterTableRow[i].BookingStatus);
                            InnerHTML = InnerHTML.Replace("{Issue}", filterTableRow[i].Issue);
                            InnerHTML = InnerHTML.Replace("{Vendor}", filterTableRow[i].Vendor);

                            if (filterTableRow[i].SupplierType == "C")
                            {
                                InnerHTML = InnerHTML.Replace("{VendorName}", filterTableRow[i].Carrier);
                            }
                            else
                            {
                                InnerHTML = InnerHTML.Replace("{VendorName}", filterTableRow[i].Vendor);
                            }

                            if (i == lstAlternateSlotFinalTable.Count - 1)
                            {
                                InnerHTML = InnerHTML +
                                          "</table> ";
                            }
                        }
                    }

                    NewControl.InnerHtml = InnerHTML;
                }
            }
            else
            {
                string InnerHTML = "<table width='100%'> " +
                                  "<tr><td colspan='5' align='center'><strong>{VendorName}</strong></td></tr> " +
                                  "<tr> " +
                                    "<td><strong>Arrival {Atime} </strong></td> " +
                                    "<td>Lifts</td> " +
                                    "<td>Pallets</td> " +
                                    "<td>Cartons</td> " +
                                    "<td>Lines</td> " +
                                  "</tr> " +
                                  "<tr> ";

                InnerHTML = InnerHTML + "<td><strong>Est Departure {DTime} </strong></td> ";
                InnerHTML = InnerHTML.Replace("{DTime}", SlotEndTime);

                InnerHTML = InnerHTML + "<td>{Lifts}</td> " +
                                        "<td>{Pallets}</td> " +
                                        "<td>{Cartons}" +
                                        "<td>{Lines}</td> " +
                                      "</tr> " +
                                    "</table>";

                InnerHTML = InnerHTML.Replace("{VendorName}", Vendor);
                InnerHTML = InnerHTML.Replace("{Atime}", SlotStartTime);

                InnerHTML = InnerHTML.Replace("{Lifts}", Lifts.ToString());

                InnerHTML = InnerHTML.Replace("{Pallets}", Pallets.ToString());

                InnerHTML = InnerHTML.Replace("{Cartons}", Cartoons.ToString());

                InnerHTML = InnerHTML.Replace("{Lines}", Lines.ToString());

                NewControl.InnerHtml = InnerHTML;
            }

            NewControl.Attributes.Add("display", "none");
            // Add the new HtmlGenericControl to the Controls collection of the TableCell control.
            divVolumeNew.Controls.Add(NewControl);

            divVolumeNew.Controls.Add(hdnEndTimeGenerated);
            tcLocal.Attributes.Add("onmouseover", "this.style.cursor = 'hand'; ddrivetip('ctl00_ContentPlaceHolder1_," + ControlID + ",','#ededed','400');");
            tcLocal.Attributes.Add("onmouseout", "hideddrivetip();");
        }
        catch (Exception e)
        {
            LogUtility.SaveErrorLogEntry(e);
        }
    }

    private void MakeCellClickableNew(TableCell tclocal, string siSiteDoorID, string TimeSlot, string SlotStartDay, string BookingID, string SupplierType, string Color, string fixedSlotid, string BookingStatusStatusID, string BookingStatus)
    {
        string Status = string.Empty;

        string WeekNo = string.Empty;
        if (SlotStartDay.Trim().ToUpper() == "SUN")
        {
            WeekNo = "1";
        }
        else if (SlotStartDay.Trim().ToUpper() == "MON")
        {
            WeekNo = "2";
        }
        else if (SlotStartDay.Trim().ToUpper() == "TUE")
        {
            WeekNo = "3";
        }
        else if (SlotStartDay.Trim().ToUpper() == "WED")
        {
            WeekNo = "4";
        }
        else if (SlotStartDay.Trim().ToUpper() == "THU")
        {
            WeekNo = "5";
        }
        else if (SlotStartDay.Trim().ToUpper() == "FRI")
        {
            WeekNo = "6";
        }
        else if (SlotStartDay.Trim().ToUpper() == "SAT")
        {
            WeekNo = "7";
        }

        string strWeekDay = string.Empty;
        string logicalDate = string.Empty;
        strWeekDay = Convert.ToDateTime(Common.GetYYYY_MM_DD(txtDate.innerControltxtDate.Value)).DayOfWeek.ToString();
        if (strWeekDay.ToUpper().Substring(0, 3).ToUpper() == SlotStartDay.Trim().ToUpper() && BookingStatus == "Unconfirmed Fixed")
        {
            WeekNo = "8";
        }

        logicalDate = strWeekDay.ToUpper().Substring(0, 3) != SlotStartDay.Trim().ToUpper()
            ? Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value).AddDays(-1).ToString("dd/MM/yyyy")
            : txtDate.innerControltxtDate.Value;

        VendorDetails vd = new VendorDetails();

        fixedSlotid = BookingStatus != "Unconfirmed Fixed" ? "BK-" + BookingID.ToString() : "FS-" + fixedSlotid.ToString();
        tclocal.Attributes.Add("name", fixedSlotid);
        tclocal.Attributes.Add("onclick", "javascript:EnableDisableButtons(this,'" + fixedSlotid + "','" + BookingStatusStatusID + "','" + SupplierType + "','" + WeekNo + "','" + logicalDate + "','" + BookingStatus + "');");
    }

    public void GenerateVolumeDetails()
    {
        string strWeekDay = string.Empty;
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        strWeekDay = Convert.ToDateTime(Common.GetYYYY_MM_DD(txtDate.innerControltxtDate.Value)).DayOfWeek.ToString();
        oAPPBOK_BookingBE.ScheduleDate = Convert.ToDateTime(Common.GetYYYY_MM_DD(txtDate.innerControltxtDate.Value));
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue);
        if (strWeekDay.ToUpper() == "SUNDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 1;
        }
        else if (strWeekDay.ToUpper() == "MONDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 2;
        }
        else if (strWeekDay.ToUpper() == "TUESDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 3;
        }
        else if (strWeekDay.ToUpper() == "WEDNESDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 4;
        }
        else if (strWeekDay.ToUpper() == "THURSDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 5;
        }
        else if (strWeekDay.ToUpper() == "FRIDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 6;
        }
        else if (strWeekDay.ToUpper() == "SATURDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 7;
        }

        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        DataSet ds = oAPPBOK_BookingBAL.etVolumeDetailsAlternateSlotBAL(oAPPBOK_BookingBE);
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblMaxDeliveriesValue.Text = ds.Tables[0].Rows[0]["MAX_Deliveries"].ToString();
                lblConfirmedDeliveriesValue.Text = ds.Tables[0].Rows[0]["Confirmed_Deliveries"].ToString();
                lblUnConfirmedDeliveriesValue.Text = ds.Tables[0].Rows[0]["UNConfirmed_Deliveries"].ToString();
                lblNonTimedDeliveriesValue.Text = ds.Tables[0].Rows[0]["NonTimed_Deliveries"].ToString();
                lblTotalDeliveriesValue.Text = ds.Tables[0].Rows[0]["Total_Deliveries"].ToString();
                lblSpaceDeliveriesValue.Text = ds.Tables[0].Rows[0]["Space_Deliveries"].ToString();
                lblArrivedDeliveriesValue.Text = ds.Tables[0].Rows[0]["Arrived_Deliveries"].ToString();
                lblUnloadedDeliveriesValue.Text = ds.Tables[0].Rows[0]["Unload_Deliveries"].ToString();
                lblRefusedNoShowDeliveriesValue.Text = ds.Tables[0].Rows[0]["NoShowAndRefused_Deliveries"].ToString();
                lblMaxPalletsValue.Text = ds.Tables[0].Rows[0]["MAX_Pallets"].ToString();
                lblConfirmedPalletsValue.Text = ds.Tables[0].Rows[0]["Confirmed_Pallets"].ToString();
                lblUnConfirmedPalletsValue.Text = ds.Tables[0].Rows[0]["UNConfirmed_Pallets"].ToString();
                lblNonTimedPalletsValue.Text = ds.Tables[0].Rows[0]["NonTimed_Pallets"].ToString();
                lblTotalPalletsValue.Text = ds.Tables[0].Rows[0]["Total_Pallets"].ToString();
                lblSpacePalletsValue.Text = ds.Tables[0].Rows[0]["Space_Pallets"].ToString();
                lblArrivedPalletsValue.Text = ds.Tables[0].Rows[0]["Arrived_Pallets"].ToString();
                lblUnloadedPalletsValue.Text = ds.Tables[0].Rows[0]["Unload_Pallets"].ToString();
                lblRefusedNoShowPalletsValue.Text = ds.Tables[0].Rows[0]["NoShowAndRefused_Pallets"].ToString();
                lblMaxLinesValue.Text = ds.Tables[0].Rows[0]["MAX_Lines"].ToString();
                lblConfirmedLinesValue.Text = ds.Tables[0].Rows[0]["Confirmed_Lines"].ToString();
                lblUnConfirmedLinesValue.Text = ds.Tables[0].Rows[0]["UNConfirmed_Lines"].ToString();
                lblNonTimedLinesValue.Text = ds.Tables[0].Rows[0]["NonTimed_Lines"].ToString();
                lblTotalLinesValue.Text = ds.Tables[0].Rows[0]["Total_Lines"].ToString();
                lblSpaceLinesValue.Text = ds.Tables[0].Rows[0]["Space_Lines"].ToString();
                lblArrivedLinesValue.Text = ds.Tables[0].Rows[0]["Arrived_Lines"].ToString();
                lblUnloadedLinesValue.Text = ds.Tables[0].Rows[0]["Unload_Lines"].ToString();
                lblRefusedNoShowLinesValue.Text = ds.Tables[0].Rows[0]["NoShowAndRefused_Lines"].ToString();
                lblMaxCartonsValue.Text = ds.Tables[0].Rows[0]["MAX_Cartoons"].ToString();
                lblConfirmedCartonsValue.Text = ds.Tables[0].Rows[0]["Confirmed_Cartoons"].ToString();
                lblUnConfirmedCartonsValue.Text = ds.Tables[0].Rows[0]["UNConfirmed_Cartoons"].ToString();
                lblNonTimedCartonsValue.Text = ds.Tables[0].Rows[0]["NonTimed_Cartoons"].ToString();
                lblTotalCartonsValue.Text = ds.Tables[0].Rows[0]["Total_Cartoons"].ToString();
                lblSpaceCartonsValue.Text = ds.Tables[0].Rows[0]["Space_Cartoons"].ToString();
                lblArrivedCartonsValue.Text = ds.Tables[0].Rows[0]["Arrived_Cartoons"].ToString();
                lblUnloadedCartonsValue.Text = ds.Tables[0].Rows[0]["Unload_Cartoons"].ToString();
                lblRefusedNoShowCartonsValue.Text = ds.Tables[0].Rows[0]["NoShowAndRefused_Cartoons"].ToString();
                if (ds.Tables[0].Rows[0]["Color_Deliveries"].ToString() == "Red")
                {
                    lblTotalDeliveriesValue.ForeColor = System.Drawing.Color.Red;
                }
                if (ds.Tables[0].Rows[0]["Color_Pallets"].ToString() == "Red")
                {
                    lblTotalPalletsValue.ForeColor = System.Drawing.Color.Red;
                }
                if (ds.Tables[0].Rows[0]["Color_Lines"].ToString() == "Red")
                {
                    lblTotalLinesValue.ForeColor = System.Drawing.Color.Red;
                }
                if (ds.Tables[0].Rows[0]["Color_Cartoons"].ToString() == "Red")
                {
                    lblTotalCartonsValue.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
    }

    protected void GenerateNonTimeBooking()
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oMASBOK_BookingBAL = new APPBOK_BookingBAL();
        LogicalSchedulDateTime = Utilities.Common.TextToDateFormat(txtDate.innerControltxtDate.Value);
        oAPPBOK_BookingBE.Action = "GetNonTimeBooking";
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<APPBOK_BookingBE> lstNonTimeBooking = oMASBOK_BookingBAL.GetNonTimeBookingBAL(oAPPBOK_BookingBE);

        if (lstNonTimeBooking.Count > 0)
        {
            gvNonTimeBooking.DataSource = lstNonTimeBooking;
            gvNonTimeBooking.DataBind();
        }
        else
        {
            gvNonTimeBooking.DataSource = null;
            gvNonTimeBooking.DataBind();
        }
    }

    protected void lnkGoto_Click(object sender, EventArgs e)
    {
        divPrint.Visible = true;
        pnlVolumeDetails_1.Visible = false;
        tblAlternate.Visible = false;
        pnlLinks.Visible = false;

        tdStatus1.Visible = true;
        tdStatus2.Visible = true;
        tdStatus3.Visible = true;
        trAlternateHide.Visible = true;
        tdVendor1.Visible = true;
        tdVendor2.Visible = true;
        tdVendor3.Visible = true;
        tdBookingRef1.Visible = true;
        tdBookingRef2.Visible = true;
        tdBookingRef3.Visible = true;
        tdIcon.Visible = true;
        pnlLinks.Visible = false;
        pnlVolumeDetails_1.Visible = false;
        //shrish wrote code..
        hdnAlternateViewStatus.Value = "0";
        BindBookings();
    }
    private void GenerateWindowAlternateSlot()
    {
        try
        {
            pnlVolumeDetails_1.Visible = true;
            GenerateWindowVolumeDetails();
            GenerateNonStandardWindowBooking();
            tableDiv_GeneralNew.Controls.RemoveAt(0);
            APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
            MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

            #region Get DoorNoType

            oMASSIT_DoorOpenTimeBE.Action = "GetDoorNoAndType";
            oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
            oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            List<MASSIT_DoorOpenTimeBE> lstDoorNoType = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);

            #endregion Get DoorNoType

            Table tblSlotNew = new Table();
            tblSlotNew.CellSpacing = 2;
            tblSlotNew.CellPadding = 2;
            tblSlotNew.CssClass = "FixedTables tableData";
            tblSlotNew.ID = "Open_Text_GeneralNew";

            TableRow trDoorNoNew = new TableRow();
            trDoorNoNew.TableSection = TableRowSection.TableHeader;
            TableHeaderCell tcDoorNoNew = new TableHeaderCell();
            tcDoorNoNew.CssClass = "timeCol";
            tcDoorNoNew.Text = "Door";
            trDoorNoNew.Cells.Add(tcDoorNoNew);

            for (int iCount = 1; iCount <= lstDoorNoType.Count; iCount++)
            {
                tcDoorNoNew = new TableHeaderCell();
                tcDoorNoNew.Text = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;
                tcDoorNoNew.CssClass = "borderRight minwindowWidth";
                trDoorNoNew.Cells.Add(tcDoorNoNew);
            }

            LogicalSchedulDateTime = Utilities.Common.TextToDateFormat(txtDate.innerControltxtDate.Value);

            string scheduleDate = LogicalSchedulDateTime.ToString("yyyy-MM-dd");

            APPBOK_BookingBAL oMASBOK_BookingBAL = new APPBOK_BookingBAL();

            DataTable dt = oMASBOK_BookingBAL.GetWindowAlternateSlot(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value),
               scheduleDate, Convert.ToDateTime(Common.GetYYYY_MM_DD(txtDate.innerControltxtDate.Value)).DayOfWeek.ToString().Substring(0, 3), "GetTimeSlotData");

            DataTable dtForHoverInfoBox = oMASBOK_BookingBAL.GetWindowSlotHoverData(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value),
             scheduleDate, "GetTimeWindowHoverData");

            tblSlotNew.Rows.Add(trDoorNoNew);

            if (dt.Rows.Count > 0)
            {
                var max = Convert.ToInt64(dt.AsEnumerable().Max(x => x.Field<Int64>("id")));
                int countID = 1;
                for (int i = 1; i <= max; i++)
                {
                    TableRow tr = new TableRow();
                    tr.TableSection = TableRowSection.TableBody;
                    TableCell tc = new TableCell();
                    tc.CssClass = "timeCol";
                    tr.Cells.Add(tc);
                    tblSlotNew.BorderWidth = new Unit(1);

                    for (int iCount = 1; iCount <= lstDoorNoType.Count; iCount++)
                    {
                        tc = new TableCell();
                        tc.CssClass = "borderRightMain2 minwindowWidth";
                        Table tblChild = new Table();
                        tblChild.CssClass = "tableData1";
                        TableRow trChild = new TableRow();
                        TableCell tcChild = new TableCell();
                        tcChild.CssClass = "tableData1";
                        Label window = new Label();
                        Literal booking = new Literal();
                        Literal pallets = new Literal();
                        Literal lines = new Literal();
                        Literal carton = new Literal();
                        Label period = new Label();
                        Literal litImage = new Literal();

                        HiddenField hdnSiteDoorNumberID = new HiddenField();
                        HiddenField hdnTime = new HiddenField();
                        HiddenField hdnPeriod = new HiddenField();
                        HiddenField hdnDoorNumber = new HiddenField();

                        string door = lstDoorNoType[iCount - 1].DoorNo.DoorNumber;

                        var dr = dt.AsEnumerable().Where(x => x.Field<Int64>("id") == i && x.Field<string>("DoorNumber") == door).FirstOrDefault();

                        if (dr != null)
                        {
                            window.Text = "<span style = 'font-weight:bold;font-size:13px;font-family:Calibri'> " + dr["WindowName"].ToString() + "</span>";
                            period.Text = dr["Period"].ToString() + "<br/><br/>";
                            hdnSiteDoorNumberID.ID = "hdnSiteDoorNumberID" + countID.ToString() + "_" + dr["TimeWindowID"].ToString();
                            hdnSiteDoorNumberID.Value = dr["SiteDoorNumberID"].ToString();
                            hdnTime.ID = "hdnTime" + countID.ToString() + "_" + dr["TimeWindowID"].ToString();
                            hdnTime.Value = dr["Period"].ToString();
                            hdnDoorNumber.ID = "hdnDoorNumber" + countID.ToString() + "_" + dr["TimeWindowID"].ToString();
                            hdnDoorNumber.Value = dr["DoorNumber"].ToString();
                            booking.Text = "Bookings " + dr["Bookings"].ToString() + "<br/>";
                            pallets.Text = "Pallets  " + dr["MaxOfPallet"].ToString() + "<br/>";
                            lines.Text = "Lines  " + dr["MaxOfLine"].ToString() + "<br/>";
                            carton.Text = "Cartons  " + dr["MaxOfCartons"].ToString() + "<br/>";
                            tcChild.CssClass = dr["Color"].ToString();
                            var TimeWindowID = dr["TimeWindowID"].ToString();

                            int bookingData = Convert.ToInt32(dr["booking"]);

                            if (Convert.ToString(dr["WindowName"]).Trim() == "Reserved Window" && bookingData == 0)
                            {
                                MakeCellClickableWindowNew(tcChild, dr["StartTimeWeekday"].ToString(), dr["SupplierType"].ToString(), dr["PKID"].ToString(), dr["BookingStatus"].ToString(), dr["WindowName"].ToString());
                            }
                            else if (Convert.ToString(dr["WindowName"]).Trim() != "Reserved Window")
                            {
                                MakeCellClickableWindowNew(tcChild, dr["StartTimeWeekday"].ToString(), dr["SupplierType"].ToString(), dr["PKID"].ToString(), dr["BookingStatus"].ToString(), dr["WindowName"].ToString());
                            }

                            countID = countID + 1;

                            CreateDynamicWindowDivNew(tcChild, Convert.ToInt32(countID), TimeWindowID, dtForHoverInfoBox);
                        }

                        tcChild.Controls.Add(window);
                        tcChild.Controls.Add(period);
                        tcChild.Controls.Add(booking);
                        tcChild.Controls.Add(pallets);
                        tcChild.Controls.Add(carton);
                        tcChild.Controls.Add(lines);
                        tcChild.Controls.Add(hdnSiteDoorNumberID);
                        tcChild.Controls.Add(hdnTime);
                        tcChild.Controls.Add(hdnDoorNumber);

                        trChild.Cells.Add(tcChild);
                        tblChild.Controls.Add(trChild);
                        tc.Controls.Add(tblChild);

                        tr.Cells.Add(tc);
                        tblSlotNew.Rows.Add(tr);
                    }
                }
            }

            divPrint.Visible = false;

            if (Session["Role"].ToString() != "Vendor" && Session["Role"].ToString() != "Carrier")
            {
                if (lstDoorNoType.Count > 0 && dt.Rows.Count > 0)
                {
                    pnlWindow.Controls.Add(tblSlotNew);
                    pnlWindow.Visible = true;
                }
            }
        }
        catch
        {
        }
    }

    protected void imgWindowAlternateView_Click(object sender, ImageClickEventArgs e)
    {
        ImgWindowAlternateViewClicked = true;
        GenerateVolumeDetails();
        pnlWindow.Visible = true;
        GenerateWindowAlternateSlot();
        divPrint.Visible = false;
        pnlVolumeDetails_1.Visible = true;
        tblAlternate.Visible = false;
        tdStatus1.Visible = false;
        tdStatus2.Visible = false;
        tdStatus3.Visible = false;
        trAlternateHide.Visible = false;
        tdVendor1.Visible = false;
        tdVendor2.Visible = false;
        tdVendor3.Visible = false;
        tdBookingRef1.Visible = false;
        tdBookingRef2.Visible = false;
        tdBookingRef3.Visible = false;
        tdIcon.Visible = false;
        pnlLinks.Visible = true;
        imgWindowAlternateView.Visible = true;
        lnkGotoWindow.Visible = true;
        lnkGoto.Visible = false;
        lnkLegends.Style.Add("display", "none");
        lnkCurrentBookingDetails.Visible = false;
        lblVendorNoBooking.Visible = false;
        lnkNonTimeBookingDetails.Visible = false;
        lnkNonStandardWindowBookingDetails.Visible = true;
        skuCatCode.Visible = false;
    }

    private void MakeCellClickableWindowNew(TableCell tclocal, string SlotStartDay, string SupplierType, string fixedSlotid, string BookingStatus, string WindowName)
    {
        string WeekNo = string.Empty;
        if (SlotStartDay.Trim().ToUpper() == "SUN")
        {
            WeekNo = "1";
        }
        else if (SlotStartDay.Trim().ToUpper() == "MON")
        {
            WeekNo = "2";
        }
        else if (SlotStartDay.Trim().ToUpper() == "TUE")
        {
            WeekNo = "3";
        }
        else if (SlotStartDay.Trim().ToUpper() == "WED")
        {
            WeekNo = "4";
        }
        else if (SlotStartDay.Trim().ToUpper() == "THU")
        {
            WeekNo = "5";
        }
        else if (SlotStartDay.Trim().ToUpper() == "FRI")
        {
            WeekNo = "6";
        }
        else if (SlotStartDay.Trim().ToUpper() == "SAT")
        {
            WeekNo = "7";
        }

        string strWeekDay = string.Empty;
        string logicalDate = string.Empty;
        strWeekDay = Convert.ToDateTime(Common.GetYYYY_MM_DD(txtDate.innerControltxtDate.Value)).DayOfWeek.ToString();

        if (strWeekDay.ToUpper().Substring(0, 3) != SlotStartDay.Trim().ToUpper())
        {
            logicalDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value).AddDays(-1).ToString("dd/MM/yyyy");
        }
        else
        {
            logicalDate = txtDate.innerControltxtDate.Value;
        }

        if (WindowName == "Reserved Window")
        {
            logicalDate = txtDate.innerControltxtDate.Value;
            WeekNo = "8";
        }

        BookingStatusID = "-1";
        tclocal.Attributes.Add("onclick", "javascript:EnableDisableButtons(this,'" + fixedSlotid + "','" + BookingStatusID + "','" + SupplierType + "','" + WeekNo + "','" + logicalDate + "','" + BookingStatus + "');");
    }

    private void CreateDynamicWindowDivNew(TableCell tcLocal, int UniqueID, string TimeWindowID, DataTable dtAlternateSlotFinalTable)
    {
        try
        {
            // Create a new HtmlGenericControl.
            HtmlGenericControl NewControl = new HtmlGenericControl("div");
            NewControl.ViewStateMode = ViewStateMode.Disabled;
            string ControlID = "VolumeNewWindow" + Convert.ToString(UniqueID);

            NewControl.ID = "," + ControlID + ",";

            var lstAlternateSlotFinalTable = dtAlternateSlotFinalTable.AsEnumerable().Where(x => x.Field<string>("TimeWindowID") == TimeWindowID).ToList();

            foreach (var door in lstAlternateSlotFinalTable)
            {
                var filterTableRow = (from c in lstAlternateSlotFinalTable.AsEnumerable()
                                      select new
                                      {
                                          Vendor = c.Field<string>("Vendor_Name"),
                                          Pallets = c.Field<string>("NumberOfPallet"),
                                          Cartoons = c.Field<string>("NumberOfCartons"),
                                          Lines = c.Field<string>("NumberOfLine"),
                                          Lifts = c.Field<string>("NumberOfLift"),
                                          Reference = c.Field<string>("Reference"),
                                          WindowName = c.Field<string>("WindowName"),
                                          Period = c.Field<string>("Period"),
                                          Carrier = c.Field<string>("CarrierName")
                                      }).ToList();
                string InnerHTML = "<table width='100%'>" +
                              "<tr><td colspan = '5' align = 'center' ><strong>{WindowName}</strong> {Period} </td></tr> " +
                              "<tr> " +
                              "<td align='center'><strong> Vendor </strong></td> " +
                              "<td align='center'><strong> Carrier </strong></td> " +
                              "<td align='center'><strong> Reference </strong></td> " +
                              "<td align='center'><strong> Plts </strong></td> " +
                              "<td align='center'><strong> Ctns </strong></td> " +
                              "<td align='center'><strong> Lns </strong></td> " +
                              "</tr>";
                if (lstAlternateSlotFinalTable.Count > 0)
                {
                    if (!string.IsNullOrEmpty(filterTableRow[0].WindowName))
                        InnerHTML = InnerHTML.Replace("{WindowName}", filterTableRow[0].WindowName);
                    else
                        InnerHTML = InnerHTML.Replace("{WindowName}", "Reserved Window");
                    InnerHTML = InnerHTML.Replace("{Period}", filterTableRow[0].Period);
                }
                for (int i = 0; i < lstAlternateSlotFinalTable.Count; i++)
                {
                    InnerHTML = InnerHTML + "<tr> " +
                              "<td>{Vendor}</td> " +
                              "<td align='center'>{Carrier}</td> " +
                              "<td align='center'>{Reference}</td> " +
                              "<td align='center'>{PalletsTotal}</td> " +
                              "<td align='center'>{CartonsTotal}</td> " +
                              "<td align='center'>{LinesTotal}</td> " +
                              "</tr> ";
                    InnerHTML = InnerHTML.Replace("{Vendor}", filterTableRow[i].Vendor);
                    InnerHTML = InnerHTML.Replace("{Carrier}", filterTableRow[i].Carrier);
                    InnerHTML = InnerHTML.Replace("{PalletsTotal}", filterTableRow[i].Pallets != "" ? filterTableRow[i].Pallets : "-");
                    InnerHTML = InnerHTML.Replace("{CartonsTotal}", filterTableRow[i].Cartoons != "" ? filterTableRow[i].Cartoons : "-");
                    InnerHTML = InnerHTML.Replace("{LinesTotal}", filterTableRow[i].Lines != "" ? filterTableRow[i].Lines : "-");
                    InnerHTML = InnerHTML.Replace("{Reference}", filterTableRow[i].Reference);
                    if (i == lstAlternateSlotFinalTable.Count - 1)
                    {
                        InnerHTML = InnerHTML + "</table> ";
                    }
                }
                NewControl.InnerHtml = InnerHTML;
                NewControl.Attributes.Add("display", "none");
                // Add the new HtmlGenericControl to the Controls collection of the TableCell control.
                divVolumeNew.Controls.Add(NewControl);
                tcLocal.Attributes.Add("onmouseover", "this.style.cursor = 'hand'; ddrivetip('ctl00_ContentPlaceHolder1_," + ControlID + ",','#ededed','400');");
                tcLocal.Attributes.Add("onmouseout", "hideddrivetip();");
            }
        }
        catch (Exception e)
        {
            LogUtility.SaveErrorLogEntry(e);
        }
    }

    protected void lnkGotoWindow_Click(object sender, EventArgs e)
    {
        this.Search();
        GenerateVolumeDetails();
        pnlWindow.Visible = false;
        divPrint.Visible = true;
        pnlVolumeDetails_1.Visible = false;
        tblAlternate.Visible = true;
        tdStatus1.Visible = true;
        tdStatus2.Visible = true;
        tdStatus3.Visible = true;
        trAlternateHide.Visible = true;
        tdVendor1.Visible = true;
        tdVendor2.Visible = true;
        tdVendor3.Visible = true;
        tdBookingRef1.Visible = true;
        tdBookingRef2.Visible = true;
        tdBookingRef3.Visible = true;
        tdIcon.Visible = true;
        pnlLinks.Visible = false;
        imgWindowAlternateView.Visible = true;
        lnkGotoWindow.Visible = true;
        lnkGoto.Visible = false;
    }

    public void GenerateWindowVolumeDetails()
    {
        string strWeekDay = string.Empty;
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        strWeekDay = Convert.ToDateTime(Common.GetYYYY_MM_DD(txtDate.innerControltxtDate.Value)).DayOfWeek.ToString();
        oAPPBOK_BookingBE.ScheduleDate = Convert.ToDateTime(Common.GetYYYY_MM_DD(txtDate.innerControltxtDate.Value));
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue);
        if (strWeekDay.ToUpper() == "SUNDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 1;
        }
        else if (strWeekDay.ToUpper() == "MONDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 2;
        }
        else if (strWeekDay.ToUpper() == "TUESDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 3;
        }
        else if (strWeekDay.ToUpper() == "WEDNESDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 4;
        }
        else if (strWeekDay.ToUpper() == "THURSDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 5;
        }
        else if (strWeekDay.ToUpper() == "FRIDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 6;
        }
        else if (strWeekDay.ToUpper() == "SATURDAY")
        {
            oAPPBOK_BookingBE.WeekDay = 7;
        }

        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        DataSet ds = oAPPBOK_BookingBAL.GetWindowVolumeDetailsAlternateSlotBAL(oAPPBOK_BookingBE);
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblNonTimed.Text = "Non Standard Window Slot";
                lblMaxDeliveriesValue.Text = ds.Tables[0].Rows[0]["MAX_Deliveries"].ToString();
                lblConfirmedDeliveriesValue.Text = ds.Tables[0].Rows[0]["Confirmed_Deliveries"].ToString();
                lblUnConfirmedDeliveriesValue.Text = ds.Tables[0].Rows[0]["UNConfirmed_Deliveries"].ToString();
                lblNonTimedDeliveriesValue.Text = ds.Tables[0].Rows[0]["NonTimed_Deliveries"].ToString();
                lblTotalDeliveriesValue.Text = ds.Tables[0].Rows[0]["Total_Deliveries"].ToString();
                lblSpaceDeliveriesValue.Text = ds.Tables[0].Rows[0]["Space_Deliveries"].ToString();
                lblArrivedDeliveriesValue.Text = ds.Tables[0].Rows[0]["Arrived_Deliveries"].ToString();
                lblUnloadedDeliveriesValue.Text = ds.Tables[0].Rows[0]["Unload_Deliveries"].ToString();
                lblRefusedNoShowDeliveriesValue.Text = ds.Tables[0].Rows[0]["NoShowAndRefused_Deliveries"].ToString();
                lblMaxPalletsValue.Text = ds.Tables[0].Rows[0]["MAX_Pallets"].ToString();
                lblConfirmedPalletsValue.Text = ds.Tables[0].Rows[0]["Confirmed_Pallets"].ToString();
                lblUnConfirmedPalletsValue.Text = ds.Tables[0].Rows[0]["UNConfirmed_Pallets"].ToString();
                lblNonTimedPalletsValue.Text = ds.Tables[0].Rows[0]["NonTimed_Pallets"].ToString();
                lblTotalPalletsValue.Text = ds.Tables[0].Rows[0]["Total_Pallets"].ToString();
                lblSpacePalletsValue.Text = ds.Tables[0].Rows[0]["Space_Pallets"].ToString();
                lblArrivedPalletsValue.Text = ds.Tables[0].Rows[0]["Arrived_Pallets"].ToString();
                lblUnloadedPalletsValue.Text = ds.Tables[0].Rows[0]["Unload_Pallets"].ToString();
                lblRefusedNoShowPalletsValue.Text = ds.Tables[0].Rows[0]["NoShowAndRefused_Pallets"].ToString();
                lblMaxLinesValue.Text = ds.Tables[0].Rows[0]["MAX_Lines"].ToString();
                lblConfirmedLinesValue.Text = ds.Tables[0].Rows[0]["Confirmed_Lines"].ToString();
                lblUnConfirmedLinesValue.Text = ds.Tables[0].Rows[0]["UNConfirmed_Lines"].ToString();
                lblNonTimedLinesValue.Text = ds.Tables[0].Rows[0]["NonTimed_Lines"].ToString();
                lblTotalLinesValue.Text = ds.Tables[0].Rows[0]["Total_Lines"].ToString();
                lblSpaceLinesValue.Text = ds.Tables[0].Rows[0]["Space_Lines"].ToString();
                lblArrivedLinesValue.Text = ds.Tables[0].Rows[0]["Arrived_Lines"].ToString();
                lblUnloadedLinesValue.Text = ds.Tables[0].Rows[0]["Unload_Lines"].ToString();
                lblRefusedNoShowLinesValue.Text = ds.Tables[0].Rows[0]["NoShowAndRefused_Lines"].ToString();
                lblMaxCartonsValue.Text = ds.Tables[0].Rows[0]["MAX_Cartoons"].ToString();
                lblConfirmedCartonsValue.Text = ds.Tables[0].Rows[0]["Confirmed_Cartoons"].ToString();
                lblUnConfirmedCartonsValue.Text = ds.Tables[0].Rows[0]["UNConfirmed_Cartoons"].ToString();
                lblNonTimedCartonsValue.Text = ds.Tables[0].Rows[0]["NonTimed_Cartoons"].ToString();
                lblTotalCartonsValue.Text = ds.Tables[0].Rows[0]["Total_Cartoons"].ToString();
                lblSpaceCartonsValue.Text = ds.Tables[0].Rows[0]["Space_Cartoons"].ToString();
                lblArrivedCartonsValue.Text = ds.Tables[0].Rows[0]["Arrived_Cartoons"].ToString();
                lblUnloadedCartonsValue.Text = ds.Tables[0].Rows[0]["Unload_Cartoons"].ToString();
                lblRefusedNoShowCartonsValue.Text = ds.Tables[0].Rows[0]["NoShowAndRefused_Cartoons"].ToString();
                if (ds.Tables[0].Rows[0]["Color_Deliveries"].ToString() == "Red")
                {
                    lblTotalDeliveriesValue.ForeColor = System.Drawing.Color.Red;
                }
                if (ds.Tables[0].Rows[0]["Color_Pallets"].ToString() == "Red")
                {
                    lblTotalPalletsValue.ForeColor = System.Drawing.Color.Red;
                }
                if (ds.Tables[0].Rows[0]["Color_Lines"].ToString() == "Red")
                {
                    lblTotalLinesValue.ForeColor = System.Drawing.Color.Red;
                }
                if (ds.Tables[0].Rows[0]["Color_Cartoons"].ToString() == "Red")
                {
                    lblTotalCartonsValue.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
    }

    protected void GenerateNonStandardWindowBooking()
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oMASBOK_BookingBAL = new APPBOK_BookingBAL();
        LogicalSchedulDateTime = Utilities.Common.TextToDateFormat(txtDate.innerControltxtDate.Value);
        oAPPBOK_BookingBE.Action = "GetNonStandardWindowBooking";
        oAPPBOK_BookingBE.ScheduleDate = LogicalSchedulDateTime;
        oAPPBOK_BookingBE.SiteId = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        List<APPBOK_BookingBE> lstNonStandardWindowBooking = oMASBOK_BookingBAL.GetNonStandardWindowBookingBAL(oAPPBOK_BookingBE);

        if (lstNonStandardWindowBooking.Count > 0)
        {
            gvNonStandardWindowBooking.DataSource = lstNonStandardWindowBooking;
            gvNonStandardWindowBooking.DataBind();
        }
        else
        {
            gvNonStandardWindowBooking.DataSource = null;
            gvNonStandardWindowBooking.DataBind();
        }
    }
}
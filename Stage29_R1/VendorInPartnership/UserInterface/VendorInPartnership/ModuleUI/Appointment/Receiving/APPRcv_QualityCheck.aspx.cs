﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;


public partial class APPRcv_QualityCheck : CommonPage
{
    protected Boolean checkingRequired = false;
    protected string DeliveryCheckedMessage = WebCommon.getGlobalResourceValue("DeliveryCheckedMessage");
    protected string PasswordReq = WebCommon.getGlobalResourceValue("PasswordReq");
    protected string OperatorNameRequired = WebCommon.getGlobalResourceValue("OperatorNameRequired");
    protected string PasswordNotMatch = WebCommon.getGlobalResourceValue("PasswordNotMatch");
    protected string CommentRequired = WebCommon.getGlobalResourceValue("CommentRequired");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("ID") != null && GetQueryStringValue("ID").ToString() != "")
            {
                int? SiteId = BookingDetails(Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id")));
                BindSuperUsers(SiteId.Value);
                IsCheckingRequired();
               
            }
        }
    }

    #region Methods

    public int? BookingDetails(int pPkID)
    {
        int? SiteId = 0;
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "DeliveryArrived";
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(GetQueryStringValue("Scheduledate").ToString());

        List<APPBOK_BookingBE> lstDeliveryArrived = oAPPBOK_BookingBAL.GetBookingDetailsBAL(oAPPBOK_BookingBE);
        if (lstDeliveryArrived != null && lstDeliveryArrived.Count > 0)
        {
            SiteId = lstDeliveryArrived[0].SiteId;
            if (ExtractInformation(GetQueryStringValue("ID").ToString(), "type").ToLower() == "c")
            {
                trCarrier.Style["display"] = "";
                trVendorCarrier.Style["display"] = "none";
                CarrierNameData1.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
            }
            else
            {
                trCarrier.Style["display"] = "none";
                trVendorCarrier.Style["display"] = "";
                VendorNameData.Text = lstDeliveryArrived[0].FixedSlot.Vendor.VendorName.ToString();
                CarrierNameData.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
                ViewState["VendorId"] = lstDeliveryArrived[0].FixedSlot.VendorID.ToString();
                ViewState["SiteId"] = lstDeliveryArrived[0].SiteId.ToString();
            }
            BookingDateData.Text = GetQueryStringValue("Scheduledate").ToString();
            if (!string.IsNullOrEmpty(lstDeliveryArrived[0].ExpectedDeliveryTime))
                BookingTimeData.Text = "- " + lstDeliveryArrived[0].ExpectedDeliveryTime.ToString();
            VehicleTypeData.Text = lstDeliveryArrived[0].VehicleType.VehicleType.ToString();
            DoorNumberData.Text = lstDeliveryArrived[0].DoorNoSetup.DoorNumber.ToString();
            OperatorNameData.Text = lstDeliveryArrived[0].DLYUNL_OperatorInital.ToString();
            if (lstDeliveryArrived.Count == 1)
            {
                BookingLiftsData.Text = lstDeliveryArrived[0].LiftsScheduled.ToString();
                BookingLinesData.Text = lstDeliveryArrived[0].FixedSlot.MaximumLines.ToString();
                BookingPalletsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumPallets.ToString();
                BookingCartonsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumCatrons.ToString();
            }
            else
            {
                int? iLifts = 0, iLines = 0, iPallets = 0, iCartons = 0;
                for (int i = 0; i < lstDeliveryArrived.Count; i++)
                {
                    iLifts += lstDeliveryArrived[i].LiftsScheduled;
                    iLines += lstDeliveryArrived[i].FixedSlot.MaximumLines;
                    iPallets += lstDeliveryArrived[i].FixedSlot.MaximumPallets;
                    iCartons += lstDeliveryArrived[i].FixedSlot.MaximumCatrons;
                }
                BookingLiftsData.Text = iLifts.ToString();
                BookingLinesData.Text = iLines.ToString();
                BookingPalletsData.Text = iPallets.ToString();
                BookingCartonsData.Text = iCartons.ToString();
            }
        }
        return SiteId;
    }
    public string ExtractInformation(string pFullData, string pReturnType)
    {
        int i = -1;
        if (pReturnType == "table")
            i = 0;
        else if (pReturnType == "type")
            i = 1;
        else if (pReturnType == "id")
            i = 2;
        else if (pReturnType == "siteid")
            i = 3;
        return (pFullData.Split('-')[i].ToString());
    }
    #endregion
    protected void btnOK_Click(object sender, EventArgs e)
    {
        string objSelectedUserPass = ddlUser.SelectedValue.Split('-')[1];
        if (!Common.VerifyPassword(txtPassword.Text, objSelectedUserPass) && ddlUser.Visible == true)
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "alert('" + PasswordNotMatch + "');", true);
            return;
        }

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "QualityCheck";
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        oAPPBOK_BookingBE.DLYQCHK_DeliveryBookedCorrectly = rdoDelivery.SelectedItem.Value == "1" ? true : false;
        oAPPBOK_BookingBE.DLYQCHK_IncorrectlyBookedComments = txtActionTaken.Text.Trim();
        oAPPBOK_BookingBE.BookingStatusID = Convert.ToInt32(Common.BookingStatus.QualityChecked);
        oAPPBOK_BookingBE.DLYARR_OperatorInital = txtOperatorInitials.Text.Trim();
        oAPPBOK_BookingBE.FixedSlot.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oAPPBOK_BookingBE.FixedSlot.User.UserID = Convert.ToInt32(Session["UserID"].ToString().Trim());

        oAPPBOK_BookingBAL.addEditQualityCheckBAL(oAPPBOK_BookingBE);

        EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx?Scheduledate="
            + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString());
    }

    public void BindSuperUsers(int SiteId)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "ShowAll";
        oSCT_UserBE.IsDeliveryRefusalAuthorization = 1;
        oSCT_UserBE.SiteId = SiteId;
        List<SCT_UserBE> lstSuperUsers = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
        if (lstSuperUsers != null && lstSuperUsers.Count > 0)
        {
            FillControls.FillDropDown(ref ddlUser, lstSuperUsers, "FullName", "UserIDPassword");
        }
    }
    private void IsCheckingRequired()
    {
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPBOK_BookingBAL = new APPSIT_VendorBAL();
        oMASSIT_VendorBE.Action = "GetVendorCheckingRequired";
        oMASSIT_VendorBE.SiteID = Convert.ToInt32(ViewState["SiteId"]);
        oMASSIT_VendorBE.VendorID = Convert.ToInt32(ViewState["VendorId"]);

        List<MASSIT_VendorBE> lstSiteVendor = oAPPBOK_BookingBAL.GetVendorCheckingRequiredBAL(oMASSIT_VendorBE);
        if (lstSiteVendor != null && lstSiteVendor.Count > 0)
        {
            if (lstSiteVendor[0].APP_CheckingRequired == true)
            {
                checkingRequired = true;
                trSupervisor.Visible = true;
                trOperator.Visible = false;              
            }
            else
            {
                checkingRequired = false;
                trSupervisor.Visible = false;
                trOperator.Visible = true;
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx?Scheduledate="
            + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString());
    }
}
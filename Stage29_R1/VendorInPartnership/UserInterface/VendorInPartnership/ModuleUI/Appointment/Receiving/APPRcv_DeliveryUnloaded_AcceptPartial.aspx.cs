﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using Utilities; using WebUtilities;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.IO;
using System.Configuration;


public partial class APPRcv_DeliveryUnloaded_AcceptPartial : CommonPage
{

    protected string VolumeDetailsValidation = WebCommon.getGlobalResourceValue("VolumeDetailsValidation");
    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    string templatePath = @"/EmailTemplates/Appointment/";
    int Siteid;
    protected void Page_Load(object sender, EventArgs e)
    {
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = "Appointment Scheduling - Booking";

        if (!IsPostBack)
        {
            BindVehicleType();
            if (GetQueryStringValue("ID") != null && GetQueryStringValue("ID").ToString() != "")
            {
                if (ExtractInformation(GetQueryStringValue("ID").ToString(), "type").ToLower() == "c")
                {
                    lblCarrierBooking.Style["display"] = "none";
                    CarrierNameData.Style["display"] = "none";
                }
                else
                {
                    lblCarrierBooking.Style["display"] = "";
                    CarrierNameData.Style["display"] = "";
                }
                BookingDetails(Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id")));
            }

            
        }
    }

    public string ExtractInformation(string pFullData, string pReturnType)
    {
        int i = -1;
        if (pReturnType == "table")
            i = 0;
        else if (pReturnType == "type")
            i = 1;
        else if (pReturnType == "id")
            i = 2;
        else if (pReturnType == "siteid")
            i = 3;
        return (pFullData.Split('-')[i].ToString());
    }

    public void BookingDetails(int pPkID)
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "DeliveryArrived";
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(GetQueryStringValue("Scheduledate").ToString());

        List<APPBOK_BookingBE> lstDeliveryArrived = oAPPBOK_BookingBAL.GetBookingDetailsBAL(oAPPBOK_BookingBE);
        grdAcceptPartial.DataSource = lstDeliveryArrived;
        grdAcceptPartial.DataBind();

        ViewState["lstSites"] = lstDeliveryArrived;

        if (lstDeliveryArrived != null && lstDeliveryArrived.Count > 0)
        {
            ddlVehicleType.SelectedIndex = ddlVehicleType.Items.IndexOf(ddlVehicleType.Items.FindByValue(lstDeliveryArrived[0].VehicleType.VehicleTypeID.ToString()));
            Siteid = Convert.ToInt32(lstDeliveryArrived[0].SiteId);
            if (ExtractInformation(GetQueryStringValue("ID").ToString(), "type").ToLower() == "c")
            {
                trCarrier.Style["display"] = "";
                trVendorCarrier.Style["display"] = "none";
                CarrierNameData1.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
            }
            else
            {
                trCarrier.Style["display"] = "none";
                trVendorCarrier.Style["display"] = "";
                VendorNameData.Text = lstDeliveryArrived[0].FixedSlot.Vendor.VendorName.ToString();
                CarrierNameData.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
            }
            BookingDateData.Text = GetQueryStringValue("Scheduledate").ToString();
            BookingTimeData.Text = lstDeliveryArrived[0].ExpectedDeliveryTime.ToString();
            VehicleTypeData.Text = lstDeliveryArrived[0].VehicleType.VehicleType.ToString();
            DoorNumberData.Text = lstDeliveryArrived[0].DoorNoSetup.DoorNumber.ToString();

            DateTime? Unloadtime = lstDeliveryArrived[0].DLYUNL_DateTime;

            UnloadDateValue.Text = Common.GetDD_MM_YYYY(lstDeliveryArrived[0].DLYUNL_DateTime.ToString().Substring(0, 10) + "-" + Unloadtime.Value.Hour.ToString() + ":" + Unloadtime.Value.Minute.ToString());


            /*if (lstDeliveryArrived.Count == 1)
            {
                BookingLiftsData.Text = lstDeliveryArrived[0].LiftsScheduled.ToString();
                BookingLinesData.Text = lstDeliveryArrived[0].FixedSlot.MaximumLines.ToString();
                BookingPalletsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumPallets.ToString();
                BookingCartonsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumCatrons.ToString();
            }
            else
            {*/
                int? iLifts = 0, iLines = 0, iPallets = 0, iCartons = 0;
                for (int i = 0; i < lstDeliveryArrived.Count; i++)
                {
                    iLifts += lstDeliveryArrived[i].LiftsScheduled;
                    iLines += lstDeliveryArrived[i].FixedSlot.MaximumLines;
                    iPallets += lstDeliveryArrived[i].FixedSlot.MaximumPallets;
                    iCartons += lstDeliveryArrived[i].FixedSlot.MaximumCatrons;
                }
                BookingLiftsData.Text = iLifts.ToString();
                BookingLinesData.Text = iLines.ToString();
                BookingPalletsData.Text = iPallets.ToString();
                BookingCartonsData.Text = iCartons.ToString();
                lblExpectedPalletsTotal.Text = iPallets.ToString();
                lblExpectedCartonsTotal.Text = iCartons.ToString();
                lblExpectedLiftsTotal.Text = iLifts.ToString();
                //lblTotalAcceptedPallets.Text = iPallets.ToString();
                //lblTotalAcceptedCartons.Text = iCartons.ToString();
            //}
        }
    }

    private void BindVehicleType()
    {
        MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

        oMASSIT_VehicleTypeBE.Action = "ShowAll";
        oMASSIT_VehicleTypeBE.SiteID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "siteid")); 

        List<MASSIT_VehicleTypeBE> lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsBAL(oMASSIT_VehicleTypeBE);

        if (lstVehicleType.Count > 0)
        {
            FillControls.FillDropDown(ref ddlVehicleType, lstVehicleType, "VehicleType", "VehicleTypeID");
        }
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        int iCount = 0;
        foreach (GridViewRow gv in grdAcceptPartial.Rows)
        {
            TextBox txtRefusedPallets = (TextBox)gv.FindControl("txtRefusedPallets");
            TextBox txtRefusedCartons = (TextBox)gv.FindControl("txtRefusedCartons");

            if (txtRefusedPallets.Text.Trim() != "")
            {
                iCount++;
            }
            if (txtRefusedCartons.Text.Trim() != "")
            {
                iCount++;
            }
            if (iCount > 0) break;
        }
        if (iCount == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('"+VolumeDetailsValidation+"')", true);
            return;
        }

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Action = "UpdateDeliveryUnloadedAcceptPartial";

        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        foreach (GridViewRow gv in grdAcceptPartial.Rows)
        {
            Literal ltlVendorid = (Literal)gv.FindControl("lblVendorId");
            TextBox txtAcceptedPallets = (TextBox)gv.FindControl("txtAcceptedPallets");
            TextBox txtAcceptedCartons = (TextBox)gv.FindControl("txtAcceptedCartons");
            TextBox txtRefusedPallets = (TextBox)gv.FindControl("txtRefusedPallets");
            TextBox txtRefusedCartons = (TextBox)gv.FindControl("txtRefusedCartons");


            string VendorId = ltlVendorid.Text.ToString();
            string AcceptedPallets = txtAcceptedPallets.Text.ToString();
            string AcceptedCartons = txtAcceptedCartons.Text.ToString();
            string RefusedPallets = txtRefusedPallets.Text.ToString();
            string RefusedCartons = txtRefusedCartons.Text.ToString();

            if (VendorId != string.Empty)
                oAPPBOK_BookingBE.FixedSlot.VendorID = Convert.ToInt32(VendorId);
            
            oAPPBOK_BookingBE.DLYUNL_NumberOfCartons = AcceptedCartons != string.Empty ? Convert.ToInt32(AcceptedCartons) : (int?)null;
            oAPPBOK_BookingBE.DLYUNL_NumberOfPallet = AcceptedPallets != string.Empty ? Convert.ToInt32(AcceptedPallets) : (int?)null;
            oAPPBOK_BookingBE.DLYUNL_RefusedCarton = RefusedCartons != string.Empty ? Convert.ToInt32(RefusedCartons) : (int?)null;
            oAPPBOK_BookingBE.DLYUNL_RefusedPallet = RefusedPallets != string.Empty ? Convert.ToInt32(RefusedPallets) : (int?)null;
            oAPPBOK_BookingBE.BookingStatusID = Convert.ToInt32(Common.BookingStatus.PartiallyDelivery);
            oAPPBOK_BookingBE.FixedSlot.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oAPPBOK_BookingBE.FixedSlot.User.UserID = Convert.ToInt32(Session["UserID"].ToString().Trim());

            // Sprint 1 - Point 8 - Start
            if (Session["TransactionComments"] != null && Session["TransactionComments"].ToString() != "")
            {
                oAPPBOK_BookingBE.TransactionComments = Session["TransactionComments"].ToString().Trim();
            }
            // Sprint 1 - Point 8 - End

            oAPPBOK_BookingBAL.updateDeliveryUnloadedAcceptAllBAL(oAPPBOK_BookingBE);

            // Sprint 1 - Point 8 - Start
            Session.Remove("TransactionComments");
        }
        // send mail for delivery confirmtion
       // SendMail(); // not neccessary as not template given by client..mail will be sent on Refused partial.

        string strDelUnloadPassAllow = string.Empty;
        if (GetQueryStringValue("IsDelUnloadPassAllow") != null)
            strDelUnloadPassAllow = GetQueryStringValue("IsDelUnloadPassAllow");

        EncryptQueryString("APPRcv_DeliveryUnloadedRejectAll.aspx?Scheduledate="
           + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString()
           + "&UNL=PAR" + "&TotalPallets=" + hdnTotalPallets.Value
           + "&IsDelUnloadPassAllow=" + strDelUnloadPassAllow);
    }
    protected void btnExit_Click(object sender, EventArgs e)
    {
        EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx?Scheduledate="
           + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString());
    }
    protected void grdAcceptPartial_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    totalExPallets += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FixedSlot.MaximumPallets"));
        //    totalExCartons += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FixedSlot.MaximumCatrons"));
        //    totalExLifts += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "LiftsScheduled"));
        //    totalAcceptedPallets += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FixedSlot.MaximumPallets"));
        //    totalAcceptedCartons += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FixedSlot.MaximumCatrons"));

        //}
        //if (e.Row.RowType == DataControlRowType.Footer)
        //{
        //    Label lblExpectedPalletsTotal = (Label)e.Row.FindControl("lblExpectedPalletsTotal");
        //    Label lblExpectedCartonsTotal = (Label)e.Row.FindControl("lblExpectedCartonsTotal");
        //    Label lblExpectedLiftsTotal = (Label)e.Row.FindControl("lblExpectedLiftsTotal");
        //    Label lblTotalAcceptedPallets = (Label)e.Row.FindControl("lblTotalAcceptedPallets");
        //    Label lblTotalAcceptedCartons = (Label)e.Row.FindControl("lblTotalAcceptedCartons");

        //    lblExpectedPalletsTotal.Text = totalExPallets.ToString();
        //    lblExpectedCartonsTotal.Text = totalExCartons.ToString();
        //    lblExpectedLiftsTotal.Text = totalExLifts.ToString();
        //    lblTotalAcceptedPallets.Text = totalAcceptedPallets.ToString();
        //    lblTotalAcceptedCartons.Text = totalAcceptedCartons.ToString();
        //}

    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<APPBOK_BookingBE>.SortList((List<APPBOK_BookingBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    private void SendMail()
    {
        try
        {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));
            List<APPBOK_BookingBE> lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);
            if (lstBooking.Count > 0)
            {

                MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
                APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
                string VendorEmail = string.Empty;
                string Language = string.Empty;
                string[] sentTo;
                string[] language;
                List<MASSIT_VendorBE> lstVendorDetails = null;

                if (lstBooking[0].SupplierType == "C")
                {
                    oMASSIT_VendorBE.Action = "GetCarrierEmail";
                    oMASSIT_VendorBE.SiteVendorID = lstBooking[0].Carrier.CarrierID;

                    lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
                    foreach (MASSIT_VendorBE item in lstVendorDetails)
                    {
                        VendorEmail += item.Vendor.VendorContactEmail.ToString() + ", ";
                        Language += item.Vendor.Language.ToString() + ",";
                    }
                    sentTo = VendorEmail.Split(',');
                    language = Language.Split(',');
                    for (int j = 0; j < sentTo.Length; j++) {
                        if (sentTo[j] != " ") {
                            sendMailToVendor(lstBooking, sentTo[j], language[j]);
                        }
                    }
                }
                else if (lstBooking[0].SupplierType == "V")
                {
                    oMASSIT_VendorBE.Action = "GetVendorEmail";
                    oMASSIT_VendorBE.SiteVendorID = lstBooking[0].VendorID;

                    lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
                    if (lstVendorDetails != null && lstVendorDetails.Count > 0)
                    {

                        foreach (MASSIT_VendorBE item in lstVendorDetails)
                        {
                            VendorEmail += item.Vendor.VendorContactEmail.ToString() + ", ";
                            Language += item.Vendor.Language.ToString() + ",";
                        }
                        sentTo = VendorEmail.Split(',');
                        language = Language.Split(',');
                        for (int j = 0; j < sentTo.Length; j++) {
                            if (sentTo[j] != " ") {
                                sendMailToVendor(lstBooking, sentTo[j], language[j]);
                            }
                        }
                    }
                    else
                    {
                        oMASSIT_VendorBE.Action = "GetContactDetails";
                        oMASSIT_VendorBE.SiteVendorID = lstBooking[0].VendorID;

                        lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
                    
                        foreach (MASSIT_VendorBE item in lstVendorDetails)
                        {
                            VendorEmail += item.Vendor.VendorContactEmail.ToString() + ", ";
                            Language += item.Vendor.Language.ToString() + ",";
                        }
                        sentTo = VendorEmail.Split(',');
                        language = Language.Split(',');
                        for (int j = 0; j < sentTo.Length; j++) {
                            if (sentTo[j] != " ") {
                                sendMailToVendor(lstBooking, sentTo[j], language[j]);
                            }
                        }
                    }
                }
             
              
            }
        }
        catch { }
    }

    private void sendMailToVendor(List<APPBOK_BookingBE> lstBooking, string toAddress, string language) 
    {
        //if (!string.IsNullOrEmpty(toAddress)) {
        //    string htmlBody = string.Empty;
        //    string templatesPath = "emailtemplates/Appointment";
        //    string templateFile = "~/" + templatesPath + "/";
        //    templateFile += "AcceptPartial."+language+".htm";
        //    sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
        //    if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower()))) {
        //        using (StreamReader sReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower()))) {
        //            #region mailBody
        //            htmlBody = sReader.ReadToEnd();
        //            htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
        //            htmlBody = htmlBody.Replace("{BookingRef}", lstBooking[0].BookingRef);
        //            htmlBody = htmlBody.Replace("{Date}", lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy"));
        //            htmlBody = htmlBody.Replace("{Site}", lstBooking[0].SiteName);
        //            //htmlBody = htmlBody.Replace("{Issue}", Issues);

        //            #endregion
        //        }
        //    }

        //    clsEmail mail = new clsEmail();
          
        //    string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
        //    mail.toAdd(toAddress);
        //    mail.EmailMessageText = htmlBody;
        //    mail.EmailSubjectText = "Your Delivery has been Accepted Partially";
        //    mail.FromEmail = sFromAddress;
        //    mail.FromName = "Office Depot Receiving - Do not reply.";
        //    mail.IsHTMLMail = true;
        //    mail.SendMail();
        //}
    }
}
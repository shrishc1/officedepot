﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using Utilities;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;



public partial class APPRcv_DeliveryUnloaded : CommonPage
{
    protected Boolean checkingRequired = false;
    protected string DeliveryCheckedMessage = WebCommon.getGlobalResourceValue("DeliveryCheckedMessage");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("ID") != null && GetQueryStringValue("ID").ToString() != "")
            {
                if (ExtractInformation(GetQueryStringValue("ID").ToString(), "type").ToLower() == "c")
                {
                    lblCarrierBooking.Style["display"] = "none";
                    CarrierNameData.Style["display"] = "none";
                }
                else
                {
                    lblCarrierBooking.Style["display"] = "";
                    CarrierNameData.Style["display"] = "";
                }
                BookingDetails(Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id")));
                BindAllPOGrid();
                IsCheckingRequired();
            }

            if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("SiteID") != "0")
            {
                
                MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(GetQueryStringValue("SiteID")));
                hdnIsDelUnloadPassAllow.Value = singleSiteSetting.DeliveryUnload.Trim().ToUpper();
            }
        }

        // Sprint 1 - Point 8 - Start
        if (!ddlOperators.SelectedValue.ToLower().Equals(Convert.ToString(Session["LoginID"]).ToLower()) && hdnIsDelUnloadPassAllow.Value == "Y")
        {
            tblPassword.Style.Remove("display");
            rfvOperatorPasswordRequired.Enabled = true;
        }
        else
        {
            tblPassword.Style.Add("display", "none");
            rfvOperatorPasswordRequired.Enabled = false;
        }
        // Sprint 1 - Point 8 - End
    }

    #region Methods

    public void BookingDetails(int pPkID)
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "DeliveryArrived";
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(GetQueryStringValue("Scheduledate").ToString());

        List<APPBOK_BookingBE> lstDeliveryArrived = oAPPBOK_BookingBAL.GetBookingDetailsBAL(oAPPBOK_BookingBE);
        if (lstDeliveryArrived != null && lstDeliveryArrived.Count > 0)
        {

            if (ExtractInformation(GetQueryStringValue("ID").ToString(), "type").ToLower() == "c")
            {
                trCarrier.Style["display"] = "";
                trVendorCarrier.Style["display"] = "none";
                CarrierNameData1.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
            }
            else
            {
                trCarrier.Style["display"] = "none";
                trVendorCarrier.Style["display"] = "";
                VendorNameData.Text = lstDeliveryArrived[0].FixedSlot.Vendor.VendorName.ToString();
                CarrierNameData.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
                ViewState["VendorId"] = lstDeliveryArrived[0].FixedSlot.VendorID.ToString();
                ViewState["SiteId"] = lstDeliveryArrived[0].SiteId.ToString();
            }
            BookingDateData.Text = GetQueryStringValue("Scheduledate").ToString();
            if (!string.IsNullOrEmpty(lstDeliveryArrived[0].ExpectedDeliveryTime))
                BookingTimeData.Text = "- " + lstDeliveryArrived[0].ExpectedDeliveryTime.ToString();
            VehicleTypeData.Text = lstDeliveryArrived[0].VehicleType.VehicleType.ToString();
            DoorNumberData.Text = lstDeliveryArrived[0].DoorNoSetup.DoorNumber.ToString();
            if (lstDeliveryArrived.Count == 1)
            {
                BookingLiftsData.Text = lstDeliveryArrived[0].LiftsScheduled.ToString();
                BookingLinesData.Text = lstDeliveryArrived[0].FixedSlot.MaximumLines.ToString();
                BookingPalletsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumPallets.ToString();
                BookingCartonsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumCatrons.ToString();
            }
            else
            {
                int? iLifts = 0, iLines = 0, iPallets = 0, iCartons = 0;
                for (int i = 0; i < lstDeliveryArrived.Count; i++)
                {
                    iLifts += lstDeliveryArrived[i].LiftsScheduled;
                    iLines += lstDeliveryArrived[i].FixedSlot.MaximumLines;
                    iPallets += lstDeliveryArrived[i].FixedSlot.MaximumPallets;
                    iCartons += lstDeliveryArrived[i].FixedSlot.MaximumCatrons;
                }
                BookingLiftsData.Text = iLifts.ToString();
                BookingLinesData.Text = iLines.ToString();
                BookingPalletsData.Text = iPallets.ToString();
                BookingCartonsData.Text = iCartons.ToString();
            }

            lblAdditionalInfo.Text = Convert.ToString(lstDeliveryArrived[0].BookingComments);

            pnlHazardousItemChecking.Visible= lstDeliveryArrived[0].IsEnableHazardouesItemPrompt;

            // Sprint 1 - Point 8 - Start
            SCT_UserBAL UserBAL = new SCT_UserBAL();
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            oSCT_UserBE.Action = "GetManageDeliveryUsersBySiteId";
            oSCT_UserBE.SiteId = lstDeliveryArrived[0].SiteId;
            List<SCT_UserBE> lstUsers = UserBAL.GetManageDeliveryUsersBAL(oSCT_UserBE);

            ddlOperators.DataSource = lstUsers;
            FillControls.FillDropDown(ref ddlOperators, lstUsers, "FullName", "LoginID");
            ddlOperators.DataBind();
            ddlOperators.SelectedValue = Convert.ToString(Session["LoginID"]);
            hdnLoggedInUserId.Value = Convert.ToString(Session["LoginID"]);
            // Sprint 1 - Point 8 - End
        }
    }

    private void IsCheckingRequired()
    {
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPBOK_BookingBAL = new APPSIT_VendorBAL();
        oMASSIT_VendorBE.Action = "GetVendorCheckingRequired";
        oMASSIT_VendorBE.SiteID = Convert.ToInt32(ViewState["SiteId"]);
        oMASSIT_VendorBE.VendorID = Convert.ToInt32(ViewState["VendorId"]);

        List<MASSIT_VendorBE> lstSiteVendor = oAPPBOK_BookingBAL.GetVendorCheckingRequiredBAL(oMASSIT_VendorBE);
        if (lstSiteVendor != null && lstSiteVendor.Count > 0)
        {
            if (lstSiteVendor[0].APP_CheckingRequired == true)
                checkingRequired = true;
            else
                checkingRequired = false;
        }

    }

    // Stage 8 - Point 6 Changes Begin
    protected void BindAllPOGrid()
    {
        DataTable myDataTable = null;
        myDataTable = new DataTable();


        DataColumn myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "PurchaseNumber";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Original_due_date";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "VendorName";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "OutstandingLines";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Qty_On_Hand";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "qty_on_backorder";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "SiteName";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "StockPlannerName";
        myDataTable.Columns.Add(myDataColumn);

        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        oUp_PurchaseOrderDetailBE.Action = "GetAllBookingPODetails";
        oUp_PurchaseOrderDetailBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));

        if (Session["Role"] != null && Session["Role"].ToString() != "")
        {
            if (Session["Role"].ToString() == "Vendor")
                oUp_PurchaseOrderDetailBE.UserID = Convert.ToInt32(Session["UserID"]);
        }

        List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllBookedProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);

        if (lstPO.Count > 0)
        {
            for (int iCount = 0; iCount < lstPO.Count; iCount++)
            {
                DataRow dataRow = myDataTable.NewRow();

                //oUp_PurchaseOrderDetailBE.Action = "GetBookingDetails";
                //oUp_PurchaseOrderDetailBE.Purchase_order = lstPO[iCount].Purchase_order;
                //oUp_PurchaseOrderDetailBE.Site = new MAS_SiteBE();
                //oUp_PurchaseOrderDetailBE.Site.SiteID = lstPO[iCount].Site.SiteID;  // Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "siteid"));
                //oUp_PurchaseOrderDetailBE.IsPOManuallyAdded = lstPO[iCount].IsPOManuallyAdded;
                //oUp_PurchaseOrderDetailBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                //oUp_PurchaseOrderDetailBE.Vendor.VendorID = lstPO[iCount].Vendor.VendorID;

                //List<Up_PurchaseOrderDetailBE> lstPOStock = oUP_PurchaseOrderDetailBAL.GetPriorityBAL(oUp_PurchaseOrderDetailBE);

                //if (lstPOStock != null && lstPOStock.Count > 0)
                //{

                //    List<Up_PurchaseOrderDetailBE> lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.Qty_On_Hand) == 0 && (Convert.ToInt32(p.Original_quantity) - p.Qty_receipted > 0); });

                //    if (lstPOTemp != null && lstPOTemp.Count > 0)
                //    {
                //        dataRow["Qty_On_Hand"] = lstPOTemp.Count.ToString();
                //    }
                //    else
                //    {
                //        dataRow["Qty_On_Hand"] = "0";
                //    }

                //    //lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.qty_on_backorder) > 0 && (Convert.ToInt32(p.Original_quantity) - p.Qty_receipted > 0); });
                //    lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p)
                //    {
                //        return (Convert.ToDecimal(p.SKU.Qty_On_Hand) - Convert.ToDecimal(p.SKU.qty_on_backorder) < 0);
                //    }
                //  );
                //    if (lstPOTemp != null && lstPOTemp.Count > 0)
                //    {
                //        dataRow["qty_on_backorder"] = lstPOTemp.Count.ToString();
                //    }
                //    else
                //    {
                //        dataRow["qty_on_backorder"] = "0";
                //    }

                //    dataRow["OutstandingLines"] = lstPOStock.Count.ToString();
                //    dataRow["StockPlannerName"] = lstPOStock[0].StockPlannerName;
                //}
                //else
                //{
                //    dataRow["Qty_On_Hand"] = "0";
                //    dataRow["qty_on_backorder"] = "0";
                //}

                dataRow["Qty_On_Hand"] = lstPO[iCount].Stockouts;
                dataRow["qty_on_backorder"] = lstPO[iCount].Backorders;
                dataRow["OutstandingLines"] = lstPO[iCount].OutstandingLines.ToString(); //lstPOStock.Count.ToString(); //
                dataRow["StockPlannerName"] = lstPO[iCount].StockPlannerName;

                dataRow["PurchaseNumber"] = lstPO[iCount].Purchase_order;
                dataRow["Original_due_date"] = lstPO[iCount].Original_due_date.Value.ToString("dd/MM/yyyy");
                dataRow["VendorName"] = lstPO[iCount].Vendor.VendorName;
                            
                dataRow["SiteName"] = lstPO[iCount].Site.SiteName;               
                                
                myDataTable.Rows.Add(dataRow);
            }
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            gvPO.DataSource = myDataTable;
            gvPO.DataBind();
        }

    }
    // Stage 8 - Point 6 Changes End

    public string ExtractInformation(string pFullData, string pReturnType)
    {
        int i = -1;
        if (pReturnType == "table")
            i = 0;
        else if (pReturnType == "type")
            i = 1;
        else if (pReturnType == "id")
            i = 2;
        else if (pReturnType == "siteid")
            i = 3;
        return (pFullData.Split('-')[i].ToString());
    }

    public void UpdateBookingStatus()
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        oAPPBOK_BookingBE.Action = "UpdateDeliveryUnloadedFirstStep";
        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");

        // Sprint 1 - Point 8 - Start
        oAPPBOK_BookingBE.DLYUNL_OperatorInital = ddlOperators.SelectedItem.Text.Trim();

        oAPPBOK_BookingBE.DLYUNL_DateTime = DateTime.Now;
        oAPPBOK_BookingBAL.updateDeliveryUnloadedFirstStepBAL(oAPPBOK_BookingBE);
    }

    #endregion

    protected void btnAcceptPartial_Click(object sender, EventArgs e)
    {
        // Sprint 1 - Point 8 - Apply a check to validate operator
        /* This point commented on client requirement Date : 23 July 2013 */
        /*if (ValidateOperator())
        {*/
            UpdateBookingStatus();

            // Sprint 1 - Point 8 - Start
            Session["TransactionComments"] = txtComments.Text;

            EncryptQueryString("APPRcv_DeliveryUnloaded_AcceptPartial.aspx?Scheduledate="
                + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString()
                + "&IsDelUnloadPassAllow=" + hdnIsDelUnloadPassAllow.Value);
        /*}
        else
        {
            string PasswordExp = WebCommon.getGlobalResourceValue("PasswordExp");
            ScriptManager.RegisterStartupScript(this.txtPassword, this.txtPassword.GetType(), "alert1", "alert('" + PasswordExp + "')", true);
            return;
        }*/
    }
    protected void btnRefuseAll_Click(object sender, EventArgs e)
    {
        // Sprint 1 - Point 8 - Apply a check to validate operator
        /* This point commented on client requirement Date : 23 July 2013 */
        /*if (ValidateOperator())
        {*/
            UpdateBookingStatus();

            // Sprint 1 - Point 8 - Start
            Session["TransactionComments"] = txtComments.Text;

            EncryptQueryString("APPRcv_DeliveryUnloadedRejectAll.aspx?Scheduledate="
                + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString() + "&UNL=REJ"
                + "&IsDelUnloadPassAllow=" + hdnIsDelUnloadPassAllow.Value);
        /*}
        else
        {
            string PasswordExp = WebCommon.getGlobalResourceValue("PasswordExp");
            ScriptManager.RegisterStartupScript(this.txtPassword, this.txtPassword.GetType(), "alert1", "alert('" + PasswordExp + "')", true);
            return;
        }*/
    }
    protected void btnExit_Click(object sender, EventArgs e)
    {
        EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx?Scheduledate="
            + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString());
    }
    protected void btnAcceptAll_Click(object sender, EventArgs e)
    {
        // Sprint 1 - Point 8 - Apply a check to validate operator
        /* This point commented on client requirement Date : 23 July 2013 */
        /*if (ValidateOperator())
        {*/
            UpdateBookingStatus();

            // Sprint 1 - Point 8 - Start
            Session["TransactionComments"] = txtComments.Text;

            EncryptQueryString("APPRcv_DeliveryUnloadedAcceptAll.aspx?Scheduledate="
                + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString()
                + "&IsDelUnloadPassAllow=" + hdnIsDelUnloadPassAllow.Value);
        /*}
        else
        {
            string PasswordExp = WebCommon.getGlobalResourceValue("PasswordExp");
            ScriptManager.RegisterStartupScript(this.txtPassword, this.txtPassword.GetType(), "alert1", "alert('" + PasswordExp + "')", true);
            return;
        }*/

    }

    // Sprint 1 - Point 8 - Start
    private bool ValidateOperator()
    {
        bool IsValidUser = true;
        if (!Convert.ToString(Session["LoginID"]).Equals(ddlOperators.SelectedValue) 
            && hdnIsDelUnloadPassAllow.Value == "Y")
        {
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

            oSCT_UserBE.Action = "GetDetails";
            oSCT_UserBE.LoginID = ddlOperators.SelectedValue;
            oSCT_UserBE.Password = txtPassword.Text.Trim();

            List<SCT_UserBE> lstSCT_UserBE = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);

            if (lstSCT_UserBE.Count <= 0)
            {
                IsValidUser = false;
            }
        }
        return IsValidUser;
    }
    // Sprint 1 - Point 8 - End

    private MAS_SiteBE GetSingleSiteSetting(int siteId)
    {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSingleSiteSetting";
        oMAS_SiteBE.SiteID = siteId;
        MAS_SiteBE localMAS_SiteBE = oMAS_SiteBAL.GetSingleSiteSettingBAL(oMAS_SiteBE);
        return localMAS_SiteBE;
    }
}
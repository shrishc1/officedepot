﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using BusinessEntities.ModuleBE.AdminFunctions;
using System.Text;

public partial class APPRcv_BookingHistory : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (GetQueryStringValue("ID") != null && GetQueryStringValue("ID").ToString() != "")
            {
                BookingDetails(Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id")));
                BindGrid(Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id")));
                BindAllPOGrid();
                string DivGallery = GetBookingUnloadImages(Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id")));
                if (string.IsNullOrEmpty(DivGallery))
                {
                    btnViewImage.Visible = pnlDeliveryUnloadImages.Visible = false;
                }
                else
                {
                    discrepancygallery.InnerHtml = DivGallery.ToString();
                    btnViewImage.Visible = pnlDeliveryUnloadImages.Visible = true;
                }
            }
        }
    }

    #region Methods

    public string ExtractInformation(string pFullData, string pReturnType)
    {
        int i = -1;
        switch (pReturnType)
        {
            case "table":
                i = 0;
                break;
            case "type":
                i = 1;
                break;
            case "id":
                i = 2;
                break;
            case "siteid":
                i = 3;
                break;
        }
        return (pFullData.Split('-')[i].ToString());
    }

    private void DeliveryDetails()
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "DeliveryUnloadDetails";
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));

        List<APPBOK_BookingBE> lstDeliveryUnload = oAPPBOK_BookingBAL.GetDeliveryUnloadDetailsBAL(oAPPBOK_BookingBE);
        if (lstDeliveryUnload != null && lstDeliveryUnload.Count > 0)
        {
            grdDeliveryDetails.DataSource = lstDeliveryUnload;
            grdDeliveryDetails.DataBind();

            if (lstDeliveryUnload.Count == 1)
            {
                VendorNameData.Text = lstDeliveryUnload[0].Vendor.VendorName;
            }
        }


        oAPPBOK_BookingBE.Action = "DeliveryUnloadTotal";
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));

        List<APPBOK_BookingBE> lstDeliveryTotal = oAPPBOK_BookingBAL.GetDeliveryUnloadDetailsBAL(oAPPBOK_BookingBE);
        if (lstDeliveryTotal != null && lstDeliveryTotal.Count > 0)
        {
            ltExpectedPalletsTotal.Text = lstDeliveryTotal[0].NumberOfPallet.ToString();
            ltExpectedCartonsTotal.Text = lstDeliveryTotal[0].NumberOfCartons.ToString();
            ltExpectedLiftsTotal.Text = lstDeliveryTotal[0].NumberOfLift.ToString();

            ltTotalAcceptedPallets.Text = lstDeliveryTotal[0].DLYUNL_NumberOfPallet.ToString();
            ltTotalAcceptedCartons.Text = lstDeliveryTotal[0].DLYUNL_NumberOfCartons.ToString();

            ltTotalRefusedPallets.Text = lstDeliveryTotal[0].DLYUNL_RefusedPallet.ToString();
            ltTotalRefusedCartons.Text = lstDeliveryTotal[0].DLYUNL_RefusedCarton.ToString();

        }
    }

    public string GetBookingUnloadImages(int BookingID)
    {
        StringBuilder sb = new StringBuilder();
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "GetBookingUnloadImages";
        oAPPBOK_BookingBE.BookingID = BookingID;

        List<string> lstImage = oAPPBOK_BookingBAL.GetUnloadImageDetailsBAL(oAPPBOK_BookingBE);

        if (lstImage != null && lstImage.Count > 0)
        {
            foreach (string item in lstImage)
            {
                sb.Append(@"<a href='../../../Images/DeliveryUnload/" + item + @"' rel='shadowbox[gal]' title='" + item + "' target='_blank'></a>");
            }
        }
        return sb.ToString(); ;
    }
    public void BookingDetails(int pPkID)
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "DeliveryArrived";
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);

        if (Session["Role"] != null && Session["Role"].ToString() != "")
        {
            switch (Session["Role"].ToString())
            {
                case "Carrier":
                    oAPPBOK_BookingBE.SupplierType = "C";
                    break;
                case "Vendor":
                    oAPPBOK_BookingBE.SupplierType = "V";
                    break;
            }
        }

        //oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(GetQueryStringValue("Scheduledate").ToString());

        List<APPBOK_BookingBE> lstDeliveryArrived = oAPPBOK_BookingBAL.GetBookingDetailsBAL(oAPPBOK_BookingBE);


        if (lstDeliveryArrived != null && lstDeliveryArrived.Count > 0)
        {

            trVendorCarrier.Style["display"] = ExtractInformation(GetQueryStringValue("ID").ToString(), "type").ToLower() == "c" ? "" : "";

            VendorNameData.Text = lstDeliveryArrived[0].FixedSlot.Vendor.VendorName.ToString();
            CarrierNameData.Text = !string.IsNullOrEmpty(lstDeliveryArrived[0].OtherCarrier)
                ? lstDeliveryArrived[0].Carrier.CarrierName.ToString() + " [" + lstDeliveryArrived[0].OtherCarrier + "] "
                : lstDeliveryArrived[0].Carrier.CarrierName.ToString();

            BookingDateData.Text = GetQueryStringValue("Scheduledate").ToString().Substring(0, 10);
            if (!string.IsNullOrEmpty(lstDeliveryArrived[0].ExpectedDeliveryTime))
                BookingTimeData.Text = "- " + lstDeliveryArrived[0].ExpectedDeliveryTime.ToString();

            //-------Stage 4 Point 12---------//
            VehicleTypeData.Text = !string.IsNullOrEmpty(lstDeliveryArrived[0].OtherVehicle.Trim())
                ? lstDeliveryArrived[0].VehicleType.VehicleType.ToString() + " [" + lstDeliveryArrived[0].OtherVehicle + "] "
                : lstDeliveryArrived[0].VehicleType.VehicleType.ToString();
            //-------------------------------//

            DoorNumberData.Text = lstDeliveryArrived[0].DoorNoSetup.DoorNumber.ToString();
            if (lstDeliveryArrived.Count == 1)
            {
                BookingLiftsData.Text = lstDeliveryArrived[0].LiftsScheduled.ToString();
                BookingLinesData.Text = lstDeliveryArrived[0].FixedSlot.MaximumLines.ToString();
                BookingPalletsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumPallets.ToString();
                BookingCartonsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumCatrons.ToString();
            }
            else
            {
                int? iLifts = 0, iLines = 0, iPallets = 0, iCartons = 0;
                for (int i = 0; i < lstDeliveryArrived.Count; i++)
                {
                    iLifts += lstDeliveryArrived[i].LiftsScheduled;
                    iLines += lstDeliveryArrived[i].FixedSlot.MaximumLines;
                    iPallets += lstDeliveryArrived[i].FixedSlot.MaximumPallets;
                    iCartons += lstDeliveryArrived[i].FixedSlot.MaximumCatrons;
                }
                BookingLiftsData.Text = iLifts.ToString();
                BookingLinesData.Text = iLines.ToString();
                BookingPalletsData.Text = iPallets.ToString();
                BookingCartonsData.Text = iCartons.ToString();
            }

            // Sprint 1 - Point 4 - Start - For Booking Comments.
            ltAdditionalInfo.Text = Convert.ToString(lstDeliveryArrived[0].BookingComments);
            // Sprint 1 - Point 4 - End - For Booking Comments.

            ltBookinRef.Text = "- " + lstDeliveryArrived[0].BookingRef;

            // Sprint 1 - Point 8 - Start
            if (lstDeliveryArrived[0].DLYREF_ReasonArrivedEarly || lstDeliveryArrived[0].DLYREF_ReasonArrivedLate ||
                lstDeliveryArrived[0].DLYREF_ReasonNoPaperworkOndisplay || lstDeliveryArrived[0].DLYREF_ReasonPalletsDamaged ||
                lstDeliveryArrived[0].DLYREF_ReasonPackagingDamaged || lstDeliveryArrived[0].DLYREF_ReasonUnsafeLoad ||
                lstDeliveryArrived[0].DLYREF_ReasonWrongAddress || lstDeliveryArrived[0].DLYREF_ReasonRefusedTowait ||
                lstDeliveryArrived[0].DLYREF_ReasonNottoODSpecification || lstDeliveryArrived[0].DLYREF_ReasonOther)
            {
                pnlIssues.Visible = true;
                chkLate.Checked = lstDeliveryArrived[0].DLYREF_ReasonArrivedLate;
                chkEarly.Checked = lstDeliveryArrived[0].DLYREF_ReasonArrivedEarly;
                chkNoPaperworkondisplay.Checked = lstDeliveryArrived[0].DLYREF_ReasonNoPaperworkOndisplay;
                chkPalletsDamaged.Checked = lstDeliveryArrived[0].DLYREF_ReasonPalletsDamaged;
                chkPackagingDamaged.Checked = lstDeliveryArrived[0].DLYREF_ReasonPackagingDamaged;
                chkunsafeload.Checked = lstDeliveryArrived[0].DLYREF_ReasonUnsafeLoad;
                chkWrongAddress.Checked = lstDeliveryArrived[0].DLYREF_ReasonWrongAddress;
                chkRefusedtowait.Checked = lstDeliveryArrived[0].DLYREF_ReasonRefusedTowait;
                chkNotToOdSpecification.Checked = lstDeliveryArrived[0].DLYREF_ReasonNottoODSpecification;
                chkOther.Checked = lstDeliveryArrived[0].DLYREF_ReasonOther;
                lblRefusalCommentsValue.Text = lstDeliveryArrived[0].DLYREF_ReasonComments;
            }
            else
            {
                pnlIssues.Visible = false;
            }
            // Sprint 1 - Point 8 - End
        }

        if (lstDeliveryArrived != null && lstDeliveryArrived.Count > 0)
        {
            if (lstDeliveryArrived[0].BookingStatusID == 3 || lstDeliveryArrived[0].BookingStatusID == 5 || lstDeliveryArrived[0].BookingStatusID == 6)
            {
                pnlDeliveryDetailsHistory.Visible = true;
                DeliveryDetails();
            }
        }
    }

    protected void BindGrid(int pPkID)
    {

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "GetBookingHistoryByBookingID";
        oAPPBOK_BookingBE.PKID = pPkID.ToString();
        List<APPBOK_BookingBE> lstBookingHistory = oAPPBOK_BookingBAL.GetBookingHistoryBAL(oAPPBOK_BookingBE);
        if (lstBookingHistory != null && lstBookingHistory.Count > 0)
        {
            grdBookingHistory.DataSource = lstBookingHistory;
            grdBookingHistory.DataBind();
            ViewState["lstSites"] = lstBookingHistory;
        }

        // Sprint 1 - Point 5 Start
        APPBOK_CommunicationBE oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
        oAPPBOK_CommunicationBE.Action = "GetItems";
        oAPPBOK_CommunicationBE.BookingID = pPkID;
        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        List<APPBOK_CommunicationBE> lstCommunications = oAPPBOK_CommunicationBAL.GetItemsBAL(oAPPBOK_CommunicationBE);
        if (lstCommunications != null && lstCommunications.Count > 0)
        {
            grdCommunications.DataSource = lstCommunications;
            grdCommunications.DataBind();
        }
        // Sprint 1 - Point 5 End

        // Phase 8 - Point 13 Start

        APPBOK_BookingBE oAPPBOK_BookingTimeslotBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingTimeSlotBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingTimeslotBE.Action = "GetTimeSlotWindow";
        oAPPBOK_BookingTimeslotBE.PKID = pPkID.ToString();
        List<APPBOK_BookingBE> lstTimeSlotInfo = oAPPBOK_BookingBAL.GetTimeSlotInfoBAL(oAPPBOK_BookingTimeslotBE);
        if (lstTimeSlotInfo != null && lstTimeSlotInfo.Count > 0)
        {
            if (lstTimeSlotInfo[0].TimeSlotWindow.ToString() == "W")
            {
                APPBOK_CommunicationBE oAPPBOK_CommunicationBE1 = new APPBOK_CommunicationBE();
                oAPPBOK_CommunicationBE1.Action = "GetItemsWithoutMailSent";
                oAPPBOK_CommunicationBE1.BookingID = pPkID;
                APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL1 = new APPBOK_CommunicationBAL();
                List<APPBOK_CommunicationBE> lstCommunications1 = oAPPBOK_CommunicationBAL1.GetItemsBAL(oAPPBOK_CommunicationBE1);
                if (lstCommunications1 != null && lstCommunications1.Count > 0)
                {
                    grdCommunicationsWithoutMail.DataSource = lstCommunications1;
                    grdCommunicationsWithoutMail.DataBind();
                }
                else
                {
                    pnlCommunicationsWithoutMailSent.Visible = false;
                }
            }
            else
            {
                pnlCommunicationsWithoutMailSent.Visible = false;
            }
        }


        // Phase 8 - Point 13 End
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<APPBOK_BookingBE>.SortList((List<APPBOK_BookingBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void SortGrid(object sender, GridViewSortEventArgs e)
    {
        ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";

        e.SortDirection = ViewState["sortDirection"].ToString() == "ASC" ? SortDirection.Ascending : SortDirection.Descending;

        grdBookingHistory.DataSource = SubSort(e);
        grdBookingHistory.DataBind();
    }

    protected void BindAllPOGrid()
    {
        DataTable myDataTable = null;
        myDataTable = new DataTable();


        DataColumn myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "PurchaseNumber";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Original_due_date";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "VendorName";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "OutstandingLines";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Qty_On_Hand";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "qty_on_backorder";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "SiteName";
        myDataTable.Columns.Add(myDataColumn);


        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        oUp_PurchaseOrderDetailBE.Action = "GetAllBookingPODetails";
        oUp_PurchaseOrderDetailBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));

        if (Session["Role"] != null && Session["Role"].ToString() != "")
        {
            if (Session["Role"].ToString() == "Vendor")
                oUp_PurchaseOrderDetailBE.UserID = Convert.ToInt32(Session["UserID"]);
        }

        List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllBookedProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);

        if (lstPO.Count > 0)
        {
            for (int iCount = 0; iCount < lstPO.Count; iCount++)
            {
                DataRow dataRow = myDataTable.NewRow();


                dataRow["PurchaseNumber"] = lstPO[iCount].Purchase_order;
                dataRow["Original_due_date"] = lstPO[iCount].Original_due_date.Value.ToString("dd/MM/yyyy");
                dataRow["VendorName"] = lstPO[iCount].Vendor.VendorName;


                dataRow["Qty_On_Hand"] = lstPO[iCount].Stockouts;
                dataRow["qty_on_backorder"] = lstPO[iCount].Backorders;
                dataRow["OutstandingLines"] = lstPO[iCount].OutstandingLines.ToString(); //lstPOStock.Count.ToString(); //



                dataRow["SiteName"] = lstPO[iCount].Site.SiteName;
                myDataTable.Rows.Add(dataRow);
            }
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            gvPO.DataSource = myDataTable;
            gvPO.DataBind();
        }

    }

    #endregion

    #region Events

    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PN") != null)
        {
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("VENBO"))
            {
                EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
            }
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POINQ"))
            {
                EncryptQueryString("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?ID=" + GetQueryStringValue("ID").ToString()
                    + "&Status=" + GetQueryStringValue("Status").ToString()
                    + "&PN=BookHist"
                    + "&PO=" + GetQueryStringValue("PO").ToString());
            }
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("SPOLR"))
            {
                EncryptQueryString("../Reports/SchedulePOLinesReport.aspx?siteid=" + GetQueryStringValue("siteid").ToString() + "&scheduledate=" +
                    GetQueryStringValue("scheduledate"));
            }
            if (GetQueryStringValue("PN").Trim().ToUpper().Equals("OPENPL"))
            {
                EncryptQueryString("~/ModuleUI/StockOverview/Expedite/OpenPurchaseListing.aspx?action=showlisting");
            }
            else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("BOINQ")) //For Booking Inquiry
            {
                EncryptQueryString("~/ModuleUI/Appointment/BookingInquiry.aspx");
            }
            else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("OB")) // For OverdueBooking
            {
                EncryptQueryString("~/ModuleUI/Appointment/Reports/OverdueBooking.aspx?PF=BH");
            }
            else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("PROBO"))
            {
                EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
            }

            else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("POTOUTBODATE"))
            {
                Response.Write("<script language='javascript'>self.close();</script>");
            }
            else if (GetQueryStringValue("PN").Trim().ToUpper().Equals("DELPERPORPT"))
            {
                Response.Write("<script language='javascript'>self.close();</script>");
            }
        }
        else
        {
            if (Session["Role"] != null && Session["Role"].ToString() != "")
            {
                if (Session["Role"].ToString() != "Carrier" && Session["Role"].ToString() != "Vendor")
                    EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
                else
                    EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_VendorBookingOverview.aspx");
            }
            else
            {
                EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
            }

        }
    }

    #endregion

    // Sprint 1 - Point 5 Start
    protected void grdCommunications_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Literal ltSentto = (Literal)e.Row.FindControl("ltSentto");

            ltSentto.Text = ltSentto.Text.Replace(",", ", ");

            System.Web.UI.HtmlControls.HtmlAnchor lnk = (System.Web.UI.HtmlControls.HtmlAnchor)e.Row.FindControl("lnk");
            lnk.Attributes.Add("href", EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_Communication.aspx?BookID=" + ((APPBOK_CommunicationBE)(e.Row.DataItem)).BookingID.ToString()
                + "&CommunicationID=" + ((APPBOK_CommunicationBE)(e.Row.DataItem)).CommunicationID.ToString()
                + "&CommType=" + ((APPBOK_CommunicationBE)(e.Row.DataItem)).CommunicationType.ToString()));

          }
    }
    // Sprint 1 - Point 5 End
}
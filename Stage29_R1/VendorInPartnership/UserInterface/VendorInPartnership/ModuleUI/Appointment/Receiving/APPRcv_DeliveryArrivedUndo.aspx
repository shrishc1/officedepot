﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="APPRcv_DeliveryArrivedUndo.aspx.cs"
      MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    Inherits="ModuleUI_Appointment_Receiving_APPRcv_DeliveryArrivedUndo" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--Sprint 1 - Point 8 - Start--%>
    <script type="text/javascript">
        function ShowHidePassword() {
            var LoggedInUserId = document.getElementById('<%= hdnLoggedInUserId.ClientID %>').value;
            var SelectedUserID = document.getElementById('<%= ddlOperators.ClientID %>').value;
            var IsDelArrivedPassAllow = document.getElementById('<%= hdnIsDelArrivedPassAllow.ClientID %>').value;

            /*if (SelectedUserID != LoggedInUserId && IsDelArrivedPassAllow == 'Y')*/
            if (SelectedUserID != LoggedInUserId) {
                document.getElementById('<%=tblPassword.ClientID%>').style.display = "";
                ValidatorEnable(document.getElementById('<%=rfvOperatorPasswordRequired.ClientID%>'), true);
            }
            else {
                document.getElementById('<%=tblPassword.ClientID%>').style.display = "none";
                ValidatorEnable(document.getElementById('<%=rfvOperatorPasswordRequired.ClientID%>'), false);
            }
        }
    </script>
    <%--Sprint 1 - Point 8 - End--%>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div>
                <%--Sprint 1 - Point 8 - Start--%>
                <asp:RequiredFieldValidator ID="rfvOperatorPasswordRequired" runat="server" ValidationGroup="a"
                    Display="None" ControlToValidate="txtPassword">
                </asp:RequiredFieldValidator>
                <%--Commented - No longer required this validation - Sprint 1 - Point 8--%>
                <%--<asp:RequiredFieldValidator ID="rfvOperatorNameRequired" 
                    runat="server" ValidationGroup="a" 
                    Display="None" ControlToValidate="txtOperatorInitials">
                </asp:RequiredFieldValidator>--%>
                <%--Sprint 1 - Point 8 - End--%>
                <asp:ValidationSummary ID="vs1" runat="server" ShowMessageBox="true" ValidationGroup="a"
                    ShowSummary="false" />
            </div>
            <h2>
                <cc1:ucLabel ID="lblDelArrivedUndo" runat="server" Text="Undo Delivery Arrived"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
               <div class="formbox" id="divPalletCheckMesg" runat="server" visible="false">

               </div>
               <br />
                    <cc1:ucPanel ID="pnlBookingDetailsArrival" runat="server" CssClass="fieldset-form">
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <tr id="trVendorCarrier" runat="server">
                                <td style="width: 4%;">
                                    <cc1:ucLabel ID="lblVendorBooking" runat="server" Text="Vendor" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 20%;" class="nobold">
                                    <cc1:ucLabel ID="VendorNameData" runat="server" />
                                </td>
                                <td style="width: 4%;">
                                    <cc1:ucLabel ID="lblCarrierBooking" runat="server" Text="Carrier" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 20%;" class="nobold" colspan="6">
                                    <cc1:ucLabel ID="CarrierNameData" runat="server" Text="ANC" />
                                </td>
                            </tr>
                            <tr id="trCarrier" runat="server">
                                <td style="width: 4%;">
                                    <cc1:ucLabel ID="lblCarrierBooking1" runat="server" Text="Carrier" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 20%;" class="nobold" colspan="9">
                                    <cc1:ucLabel ID="CarrierNameData1" runat="server" Text="ANC" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 4%;">
                                    <cc1:ucLabel ID="lblDateBooking" runat="server" Text="Date" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 20%;" class="nobold">
                                    <cc1:ucLabel ID="BookingDateData" runat="server" Text="" />
                                    &nbsp;
                                    <cc1:ucLabel ID="BookingTimeData" runat="server" Text="" />
                                </td>
                                <td style="width: 10%;">
                                    <cc1:ucLabel ID="lblVehicleType" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 14%;" class="nobold">
                                    <cc1:ucLabel ID="VehicleTypeData" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 10%;">
                                    <cc1:ucLabel ID="lblDoorName" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 14%;" class="nobold">
                                    <cc1:ucLabel ID="DoorNumberData" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 4%;">
                                </td>
                                <td style="width: 1%;">
                                </td>
                                <td style="width: 20%;" class="nobold">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblPalletsBooking" runat="server" Text="Pallets" />
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 100px;" class="nobold">
                                    <cc1:ucLabel ID="BookingPalletsData" runat="server" Text="20" />
                                </td>
                                <td>
                                    <cc1:ucLabel ID="lblCartons" runat="server" Text="Cartons" />
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 100px;" class="nobold">
                                    <cc1:ucLabel ID="BookingCartonsData" runat="server" Text="0" />
                                </td>
                                <td style="width: 100px;">
                                    <cc1:ucLabel ID="lblLines" runat="server" Text="Lines" />
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 100px;" class="nobold">
                                    <cc1:ucLabel ID="BookingLinesData" runat="server" Text="16" />
                                </td>
                                <td>
                                    <cc1:ucLabel ID="lblLiftsBooking" runat="server" Text="Lifts" />
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 100px;" class="nobold">
                                    <cc1:ucLabel ID="BookingLiftsData" runat="server" Text="31" />
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <%--Stage 8 - Point 6 - Start--%>
                    <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                        <tr>
                            <td style="width:10%">
                               <cc1:ucLabel ID="lblAdditionalInfoK" runat="server" Text="Additional Info" />
                            </td>
                             <td style="width:1%">
                                :
                            <td style="width:89%">
                               <cc1:ucLabel ID="lblAdditionalInfo" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                        <tr>
                            <td>
                              <cc1:ucGridView ID="gvPO" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                CellPadding="0" Width="100%">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:BoundField HeaderText="PO" DataField="PurchaseNumber" SortExpression="PurchaseNumber">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Original Due Date" DataField="Original_due_date" SortExpression="Original_due_date">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" SortExpression="VendorName">
                                        <HeaderStyle Width="20%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="# Outstanding Lines on PO" DataField="OutstandingLines"
                                        SortExpression="OutstandingLines">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="# Stock outs" DataField="Qty_On_Hand" SortExpression="Qty_On_Hand">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="# Backorders" DataField="qty_on_backorder" SortExpression="qty_on_backorder">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Destination" DataField="SiteName" SortExpression="SiteName">
                                        <HeaderStyle Width="20%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                     <asp:BoundField HeaderText="Stock Planner" DataField="StockPlannerName" SortExpression="StockPlannerName">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                            </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                    <%--Stage 8 - Point 6 - END--%>
                    <%--Sprint 1 - Point 8 - Start--%>
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                        <tr>
                            <td style="width: 10%;">
                                <cc1:ucLabel ID="lblOperatorInitials" runat="server" Text="Operator Name" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="width: 1%;">
                                :
                            </td>
                            <td class="nobold">
                                <%--<cc1:ucTextbox ID="txtOperatorInitials" runat="server" Width="250px" MaxLength="50"></cc1:ucTextbox>--%>
                                <%--<cc1:ucDropdownList ID="ddlOperators" runat="server" Width="250px" onchange="ShowHidePassword()"></cc1:ucDropdownList>--%>
                                <cc1:ucDropdownList ID="ddlOperators" runat="server" Width="250px">
                                </cc1:ucDropdownList>
                                <asp:HiddenField ID="hdnLoggedInUserId" runat="server" />
                                <asp:HiddenField ID="hdnIsDelArrivedPassAllow" runat="server" />
                            </td>
                            <td>
                                <table id="tblPassword" runat="server" width="100%" cellspacing="5" cellpadding="0"
                                    class="form-table">
                                    <tr>
                                        <td style="width: 10%;">
                                            <cc1:ucLabel ID="UcLabel1" runat="server" Text="Password" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 1%;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucTextbox ID="txtPassword" runat="server" Width="250px" MaxLength="50" TextMode="Password"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%;">
                                <cc1:ucLabel ID="lblComments" runat="server" Text="Comments"></cc1:ucLabel>
                            </td>
                            <td style="width: 1%;">
                                :
                            </td>
                            <td class="nobold" colspan="2">
                                <cc1:ucTextarea ID="txtComments" Rows="3" Columns="20" runat="server"></cc1:ucTextarea>
                            </td>
                        </tr>
                    </table>
                    <%--Sprint 1 - Point 8 - End--%>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnAccept" runat="server" Text="Accept" CssClass="button" OnClick="btnAccept_Click"
                    ValidationGroup="a" />
                <cc1:ucButton ID="btnReject" runat="server" Text="Reject" CssClass="button" OnClick="btnReject_Click"
                    ValidationGroup="a" />
                <cc1:ucButton ID="btnExit" runat="server" Text="Exit" CssClass="button" OnClick="btnExit_Click" />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAccept" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnReject" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

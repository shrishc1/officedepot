﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using BusinessEntities.ModuleBE.AdminFunctions;
using System.Configuration;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Languages.Languages;

public partial class APPRcv_Communication : CommonPage
{
    string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
    sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Role"] != null && Session["Role"].ToString() != "")
        {
            if (Session["Role"].ToString() == "Carrier" || Session["Role"].ToString() == "Vendor")
                trCoomunication.Visible = false;
        }

        if (!Page.IsPostBack)
        {

            InitLanguage();

            if (GetQueryStringValue("communicationid") != null && GetQueryStringValue("QueryDiscId") == null)
            {
                hdnCommunicationId.Value = GetQueryStringValue("communicationid");
                var oAPPBOK_CommunicationBE = getCommunication();
                divReSendCommunication.InnerHtml = oAPPBOK_CommunicationBE.Body.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
                txtEmail.Text = distictEmails(oAPPBOK_CommunicationBE.SentTo);

                if (drpLanguageId.Items.FindByValue(oAPPBOK_CommunicationBE.LanguageID.ToString()) != null)
                    drpLanguageId.Items.FindByValue(oAPPBOK_CommunicationBE.LanguageID.ToString()).Selected = true;
            }
            else if (GetQueryStringValue("DiscId") != null && GetQueryStringValue("QueryDiscId") != null)
            {
                trCoomunication.Visible = false;
                var discrepancyMail = this.GetQueryDiscrepancyCommunication(Convert.ToInt32(GetQueryStringValue("DiscId")),
                    Convert.ToInt32(GetQueryStringValue("QueryDiscId")));
                oSendCommunicationCommon = new sendCommunicationCommon();
                var path = oSendCommunicationCommon.getAbsolutePath();
                divReSendCommunication.InnerHtml = discrepancyMail.mailBody.Replace("{logoInnerPath}", path);
                txtEmail.Text = distictEmails(discrepancyMail.sentTo);
            }
            else
            {
                ShowErrorMessage();
            }
        }
    }

    public string distictEmails(string Emails)
    {
        string[] arrEMails = Emails.Split(new char[]{','});

        List<string> lstEmails = new List<string>();
        foreach (string email in arrEMails)
        {
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrWhiteSpace(email) && !lstEmails.Contains(email.Trim()))
                    lstEmails.Add(email.Trim());
        }
        return string.Join(",", lstEmails.ToArray());
    }


    private void InitLanguage()
    {
        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
        drpLanguageId.DataSource = oLanguages;
        drpLanguageId.DataValueField = "LanguageID";
        drpLanguageId.DataTextField = "Language";
        drpLanguageId.DataBind();
    }



    private APPBOK_CommunicationBE GetCommunicationItem(int LanguageId)
    {
        var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        var oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
        oAPPBOK_CommunicationBE.Action = "GetCommunicationItem";
        oAPPBOK_CommunicationBE.CommunicationID = Convert.ToInt32(hdnCommunicationId.Value);
        oAPPBOK_CommunicationBE.BookingID = Convert.ToInt32(GetQueryStringValue("BookID"));
        oAPPBOK_CommunicationBE.LanguageID = Convert.ToInt32(LanguageId);
        oAPPBOK_CommunicationBE.CommunicationType = Convert.ToString(GetQueryStringValue("CommType")).Replace(" Resent", string.Empty);
        oAPPBOK_CommunicationBE = oAPPBOK_CommunicationBAL.GetCommunicationDAL(oAPPBOK_CommunicationBE);
        return oAPPBOK_CommunicationBE;
    }

    private APPBOK_CommunicationBE getCommunication()
    {
        var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        var oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
        oAPPBOK_CommunicationBE.Action = "GetItem";
        oAPPBOK_CommunicationBE.CommunicationID = Convert.ToInt32(hdnCommunicationId.Value);
        oAPPBOK_CommunicationBE.BookingID = Convert.ToInt32(GetQueryStringValue("BookID"));
        //oAPPBOK_CommunicationBE.LanguageID = Convert.ToInt32(drpLanguageId.SelectedValue);
        oAPPBOK_CommunicationBE = oAPPBOK_CommunicationBAL.GetItemBAL(oAPPBOK_CommunicationBE);
        return oAPPBOK_CommunicationBE;
    }

    private DiscrepancyMailBE GetQueryDiscrepancyCommunication(int DiscId, int QueryDiscId)
    {
        var queryDiscrepancyBAL = new DISLog_QueryDiscrepancyBAL();
        var discrepancyMail = new DiscrepancyMailBE();
        discrepancyMail.Action = "GetQueryDiscrepancyEmailContent";
        discrepancyMail.QueryDiscrepancy = new DISLog_QueryDiscrepancyBE();
        discrepancyMail.QueryDiscrepancy.DiscrepancyLogID = DiscId;
        discrepancyMail.QueryDiscrepancy.QueryDiscrepancyID = QueryDiscId;

        discrepancyMail = queryDiscrepancyBAL.GetQueryDiscrepancyEmailContentBAL(discrepancyMail);
        return discrepancyMail;
    }

    private void ShowErrorMessage()
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "showNoMailToReSend", "alert('" + WebUtilities.WebCommon.getGlobalResourceValue("NoMailToResend") + "');", true);
    }

    protected void btnReSendCommunication_Click(object sender, EventArgs e)
    {
        var oAPPBOK_CommunicationBE = GetCommunicationItem(Convert.ToInt32(drpLanguageId.SelectedValue));
        if (oAPPBOK_CommunicationBE == null)
            oAPPBOK_CommunicationBE = GetCommunicationItem(1);
        var oclsEmail = new clsEmail();
        oAPPBOK_CommunicationBE.SentTo = txtEmail.Text.Trim();
        oclsEmail.sendMail(oAPPBOK_CommunicationBE.SentTo, oAPPBOK_CommunicationBE.Body, oAPPBOK_CommunicationBE.Subject, oAPPBOK_CommunicationBE.SentFrom, true);

        // Resending of Delivery mails
        var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        oAPPBOK_CommunicationBE.Action = "AddBookingCommunication";
        if (!oAPPBOK_CommunicationBE.CommunicationType.ToLower().Contains("resent"))
        {
            oAPPBOK_CommunicationBE.CommunicationType += " Resent";
        }
        oAPPBOK_CommunicationBE.SendByID = Convert.ToInt32(Session["UserID"]);
        oAPPBOK_CommunicationBE.IsMailSent = true;
        oAPPBOK_CommunicationBE.MailSentInLanguage = true;
        oAPPBOK_CommunicationBAL.AddItemBAL(oAPPBOK_CommunicationBE);
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "refreshParent", "window.opener.location.href = window.opener.location.href;", true);
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "close", "this.close();", true);
    }
    protected void drpLanguageId_SelectedIndexChanged(object sender, EventArgs e)
    {
            var oAPPBOK_CommunicationBE = GetCommunicationItem(Convert.ToInt32(drpLanguageId.SelectedValue));
            hdnCommunicationId.Value = oAPPBOK_CommunicationBE.CommunicationID.ToString();
            divReSendCommunication.InnerHtml = oAPPBOK_CommunicationBE.Body.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
           // txtEmail.Text = oAPPBOK_CommunicationBE.SentTo;
        

    }
}
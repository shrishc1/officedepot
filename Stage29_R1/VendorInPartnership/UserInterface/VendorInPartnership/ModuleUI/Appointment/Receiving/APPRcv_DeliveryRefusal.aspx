﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="APPRcv_DeliveryRefusal.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="APPRcv_DeliveryRefusal" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
    <script language="javascript" type="text/javascript">
        function checkvalidation() {
            var RefusalComments = document.getElementById('<%=txtRefusalComments.ClientID %>').value;
            var IsDelArrivedPassAllow = document.getElementById('<%= hdnIsDelArrivedPassAllow.ClientID %>').value;
            var SecondClickStatus = document.getElementById('<%= hdnSecondClickStatus.ClientID %>');
            if (parseInt(document.getElementById('<%=hdnisIssueChecked.ClientID %>').value) == 0) {
                alert("<%=AtLeastOnePoingFailure %>");
                return false;
            }
            if (RefusalComments.length <= 0) {
                alert("<%=RefusalCommentsValidation%>");
                return false;
            }
            if (RefusalComments.length > 0) {

                var divstyle = new String();
                divstyle = document.getElementById("divApproveBy").style.visibility;

                if (divstyle.toLowerCase() == "hidden") {
                    if (IsDelArrivedPassAllow == 'N') {
                        document.getElementById("divApproveBy").style.visibility = "hidden";
                        document.getElementById("divPassLabel").style.visibility = "hidden";
                        document.getElementById("divPassCollon").style.visibility = "hidden";
                        document.getElementById("divPassTextBox").style.visibility = "hidden";
                        SecondClickStatus.value = 'FIRSTCLICK';
                        return true;
                    }
                    else {
                        document.getElementById("divApproveBy").style.visibility = "visible";
                        document.getElementById("divPassLabel").style.visibility = "visible";
                        document.getElementById("divPassCollon").style.visibility = "visible";
                        document.getElementById("divPassTextBox").style.visibility = "visible";
                        return false;
                    }
                }
                else {
                    document.getElementById("divApproveBy").style.visibility = "visible";
                    var objddlUser = document.getElementById('<%=ddlUser.ClientID %>');

                    if (document.getElementById("divApproveBy").style.visibility == "visible") {
                        if (IsDelArrivedPassAllow == 'N') {
                            document.getElementById("divPassLabel").style.visibility = "hidden";
                            document.getElementById("divPassCollon").style.visibility = "hidden";
                            document.getElementById("divPassTextBox").style.visibility = "hidden";
                            SecondClickStatus.value = 'FIRSTCLICK';
                            return true;
                        }
                        else {
                            document.getElementById("divPassLabel").style.visibility = "visible";
                            document.getElementById("divPassCollon").style.visibility = "visible";
                            document.getElementById("divPassTextBox").style.visibility = "visible";

                            var SupervisorPassword = document.getElementById('<%=txtSupervisorPassword.ClientID %>').value;
                            if (SupervisorPassword.length <= 0) {
                                alert("<%=SupervisorPasswordValidation%>");
                                return false;
                            }
                        }
                    }
                }
            }
        }
        function CheckCheckboxes(chkbox) {
            if (chkbox.checked)
                document.getElementById('<%=hdnisIssueChecked.ClientID %>').value = parseInt(document.getElementById('<%=hdnisIssueChecked.ClientID %>').value) + 1;
            else
                document.getElementById('<%=hdnisIssueChecked.ClientID %>').value = parseInt(document.getElementById('<%=hdnisIssueChecked.ClientID %>').value) - 1;
        }
        document.onmousemove = positiontip
    </script>

    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>

    <!-- For Issues Detail -->
    <div id="divVolume" runat="server" class="balloonstyle">
    </div>
    <div id="dhtmltooltip" class="balloonstyle">
    </div>
    <iframe id="iframetop" scrolling="no" frameborder="0" class="iballoonstyle" width="0"
        height="0"></iframe>
    <div class="balloonstyle" id="ArrivedLate">
        <span class="bla8blue">
            <cc1:ucLabel ID="lblIssuesDesc" runat="server"></cc1:ucLabel>
        </span>
    </div>
    <!-- For Issues Detail -->
    <h2>
        <cc1:ucLabel ID="lblDeliveryRefusal" runat="server" Text="Delivery Refusal"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <asp:HiddenField ID="hdnisIssueChecked" runat="server" Value="0" />
        <div class="formbox">
            <%--  <table width="100%" cellspacing="5" cellpadding="0">
                        <tr>
                            <td width="50%" valign="top">--%>
            <cc1:ucPanel ID="pnlBookingDetailsArrival" runat="server" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr id="trVendorCarrier" runat="server">
                        <td style="width: 4%;">
                            <cc1:ucLabel ID="lblVendorBooking" runat="server" Text="Vendor" />
                        </td>
                        <td style="width: 1%;">:
                        </td>
                        <td style="width: 20%;" class="nobold">
                            <cc1:ucLabel ID="VendorNameData" runat="server" />
                        </td>
                        <td style="width: 4%;">
                            <cc1:ucLabel ID="lblCarrierBooking" runat="server" Text="Carrier" />
                        </td>
                        <td style="width: 1%;">:
                        </td>
                        <td style="width: 20%;" class="nobold" colspan="6">
                            <cc1:ucLabel ID="CarrierNameData" runat="server" Text="ANC" />
                        </td>
                    </tr>
                    <tr id="trCarrier" runat="server">
                        <td style="width: 4%;">
                            <cc1:ucLabel ID="lblCarrierBooking1" runat="server" Text="Carrier" />
                        </td>
                        <td style="width: 1%;">:
                        </td>
                        <td style="width: 20%;" class="nobold" colspan="9">
                            <cc1:ucLabel ID="CarrierNameData1" runat="server" Text="ANC" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 4%;">
                            <cc1:ucLabel ID="lblDateBooking" runat="server" Text="Date" />
                        </td>
                        <td style="width: 1%;">:
                        </td>
                        <td style="width: 20%;" class="nobold">
                            <cc1:ucLabel ID="BookingDateData" runat="server" Text="19/01/2011" />
                            &nbsp;-
                            <cc1:ucLabel ID="BookingTimeData" runat="server" Text="09:10" />
                        </td>
                        <td style="width: 10%;">
                            <cc1:ucLabel ID="lblVehicleType" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 1%;">:
                        </td>
                        <td style="width: 14%;" class="nobold">
                            <cc1:ucLabel ID="VehicleTypeData" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 10%;">
                            <cc1:ucLabel ID="lblDoorName" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 1%;">:
                        </td>
                        <td style="width: 14%;" class="nobold">
                            <cc1:ucLabel ID="DoorNumberData" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 4%;"></td>
                        <td style="width: 1%;"></td>
                        <td style="width: 20%;" class="nobold"></td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblPalletsBooking" runat="server" Text="Pallets" />
                        </td>
                        <td>:
                        </td>
                        <td style="width: 100px;" class="nobold">
                            <cc1:ucLabel ID="BookingPalletsData" runat="server" Text="20" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblCartons" runat="server" Text="Cartons" />
                        </td>
                        <td>:
                        </td>
                        <td style="width: 100px;" class="nobold">
                            <cc1:ucLabel ID="BookingCartonsData" runat="server" Text="0" />
                        </td>
                        <td style="width: 100px;">
                            <cc1:ucLabel ID="lblLines" runat="server" Text="Lines" />
                        </td>
                        <td>:
                        </td>
                        <td style="width: 100px;" class="nobold">
                            <cc1:ucLabel ID="BookingLinesData" runat="server" Text="16" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblLiftsBooking" runat="server" Text="Lifts" />
                        </td>
                        <td>:
                        </td>
                        <td style="width: 100px;" class="nobold">
                            <cc1:ucLabel ID="BookingLiftsData" runat="server" Text="31" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlIssues" runat="server" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td colspan="4" align="right">
                            <a href="javascript:void(0)" rel="ArrivedLate" tipwidth="700" onmouseover="ddrivetip('ArrivedLate','#ededed','300');"
                                onmouseout="hideddrivetip();">
                                <img src="../../../Images/info_button1.gif" align="absMiddle" border="0"></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkLate" runat="server" onClick="CheckCheckboxes(this);" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkEarly" runat="server" onClick="CheckCheckboxes(this);" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkNoPaperworkondisplay" runat="server" onClick="CheckCheckboxes(this);" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkPalletsDamaged" runat="server" onClick="CheckCheckboxes(this);" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkPackagingDamaged" runat="server" onClick="CheckCheckboxes(this);" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkunsafeload" runat="server" onClick="CheckCheckboxes(this);" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkWrongAddress" runat="server" onClick="CheckCheckboxes(this);" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkRefusedtowait" runat="server" onClick="CheckCheckboxes(this);" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkNotToOdSpecification" runat="server" onClick="CheckCheckboxes(this);" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkOther" runat="server" onClick="CheckCheckboxes(this);" />
                        </td>
                        <td style="width: 50%;" colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <cc1:ucLabel ID="RefusalComment" runat="server" Text="Comments : " />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;" colspan="4">
                            <cc1:ucTextbox ID="txtRefusalComments" runat="server" TextMode="MultiLine" Width="98%"></cc1:ucTextbox>
                            <asp:RequiredFieldValidator ID="rfvRefusalCommentsValidation" runat="server" Display="None"
                                ControlToValidate="txtRefusalComments" ErrorMessage="Please Enter refusal Comments."
                                ValidationGroup="a" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <div id="divApproveBy" style="visibility: hidden;" runat="server" clientidmode="Static">
                <cc1:ucPanel ID="pnlApproveBy" runat="server" GroupingText="Approve by" CssClass="fieldset-form">
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                        <tr>
                            <td style="width: 11%;">
                                <cc1:ucLabel ID="lblSupervisorName" runat="server" Text="Supervisor Name"></cc1:ucLabel>
                                <asp:HiddenField ID="hdnIsDelArrivedPassAllow" runat="server" />
                                <asp:HiddenField ID="hdnSecondClickStatus" runat="server" />
                            </td>
                            <td style="width: 1%;">:
                            </td>
                            <td style="width: 38%;">
                                <cc1:ucDropdownList ID="ddlUser" runat="server" Width="150px">
                                </cc1:ucDropdownList>
                            </td>
                            <td style="width: 14%;">
                                <div id="divPassLabel" runat="server" clientidmode="Static">
                                    <cc1:ucLabel ID="lblSupervisorPassword" runat="server" Text="Supervisor Password"></cc1:ucLabel>
                                </div>
                            </td>
                            <td style="width: 1%;">
                                <div id="divPassCollon" runat="server" clientidmode="Static">
                                    :
                                </div>
                            </td>
                            <td style="width: 35%;">
                                <div id="divPassTextBox" runat="server" clientidmode="Static">
                                    <cc1:ucTextbox ID="txtSupervisorPassword" runat="server" TextMode="Password" Width="100px"
                                        MaxLength="15"></cc1:ucTextbox>
                                </div>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
            </div>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnOK" runat="server" Text="OK" CssClass="button" OnClick="btnOK_2_Click"
            OnClientClick="return checkvalidation();" />
        <%--<cc1:ucButton ID="btnReject" runat="server" Text="Reject" CssClass="button" />--%>
        <cc1:ucButton ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" />
    </div>
    <!-- Phase 13 R3 Point 18 --->
    <asp:UpdatePanel ID="updInfromation" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnInfo" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdInformation" runat="server" TargetControlID="btnInfo"
                PopupControlID="pnlbtnInfo" BackgroundCssClass="modalBackground" BehaviorID="MsgbtnInfo"
                DropShadow="false" />

            <asp:Panel ID="pnlbtnInfo" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3>
                        <cc1:ucLabel ID="lblWarningInfo" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <cc1:ucLabel ID="lblSchedulingIssueCommunication" runat="server" Text="Please select which companies will receive mail relating to this issue"></cc1:ucLabel>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <cc1:ucLabel ID="lblCarrier" runat="server" Text="" Font-Bold="true"></cc1:ucLabel>
                                        <br />
                                        <cc1:ucCheckbox ID="chkCarrier" runat="server" Checked="true" Text="" TextAlign="Right" />
                                        <br />
                                        <br />
                                        <br />
                                        <cc1:ucLabel ID="lblVendors" runat="server" Text="" Font-Bold="true"></cc1:ucLabel>
                                        <cc1:ucCheckboxList ID="chkListVendors" runat="server" RepeatColumns="2">
                                        </cc1:ucCheckboxList>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <cc1:ucButton ID="btnOK_1" runat="server" Text="" CssClass="button"
                                        OnCommand="btnOK_1_Click" />

                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOK_1" />
        </Triggers>
    </asp:UpdatePanel>
    <!-- Phase 27 R1 Point 1 --->
    <asp:UpdatePanel ID="updInfromation_Messge" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnInfo_Messge" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="updInfromation_Messge_update" runat="server" TargetControlID="btnInfo_Messge"
                PopupControlID="Panel1" BackgroundCssClass="modalBackground" BehaviorID="btnInfo_Messge"
                DropShadow="false" />
            <asp:Panel ID="Panel1" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3>
                        <cc1:ucLabel ID="UcLabel1" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                               <div class="popup-innercontainer top-setting-Popup">
                                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer" style="background-color:white">
                                        <tr>
                                            <td colspan="2">
                                                <b>
                                                    <cc1:ucLabel ID="lblRejectArrivedDelivery" runat="server"
                                                        Text="">
                                                    </cc1:ucLabel>
                                                </b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblDateTimeBooking" runat="server"
                                                    Text="Date/time of booking:">
                                                </cc1:ucLabel></td>
                                            <td>
                                                <cc1:ucLabel ID="lblDateTimeBookingValue" runat="server">
                                                </cc1:ucLabel>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblVendor_" runat="server"
                                                    Text="Vendor #">
                                                </cc1:ucLabel></td>
                                            <td>

                                                <cc1:ucLabel ID="lblVendorValue" runat="server">
                                                </cc1:ucLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblVendor" runat="server"
                                                    Text="Vendor">
                                                </cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:ucLabel ID="lblVendorNameValue" runat="server">
                                                </cc1:ucLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblBookingReference" runat="server"
                                                    Text="Booking Reference">
                                                </cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:ucLabel ID="lblBookingReferenceValue" runat="server">
                                                </cc1:ucLabel>
                                            </td>
                                        </tr>
                                    </table>
                               </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <cc1:ucButton ID="btnConfirm" runat="server" Text="Confirm" CssClass="button"
                                        OnClick="btnConfirm_Click" />
                                    <cc1:ucButton ID="btnCancel1" runat="server" Text="Cancel" CssClass="button"
                                        OnClick="btnCancel_Click1" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnConfirm" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel1" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="APPRcv_QualityCheck.aspx.cs" Inherits="APPRcv_QualityCheck" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function ShowCheckingMessage() {            
            var checkingRequired = ('<%=checkingRequired%>');
            if (checkingRequired == "True") {
                var password = document.getElementById('<%=txtPassword.ClientID%>').value;
                var objddlUser = document.getElementById('<%=ddlUser.ClientID %>');
                var objSelectedUserPass = objddlUser.options[objddlUser.selectedIndex].value.split('-')[1];
            }
            else
            var OperatorName = document.getElementById('<%=txtOperatorInitials.ClientID %>').value;
            

            if (checkingRequired == "True" && password.length <= 0) {
                alert("<%=PasswordReq%>");
                return false;
            }

//            if (objSelectedUserPass != password) {
//                alert("<%=PasswordNotMatch%>");
//                return false;
//            }

            if (checkingRequired == "False" && OperatorName.length <= 0) {
                alert("<%=OperatorNameRequired%>");
                return false;
            }
            
            if (document.getElementById('<%=rdoDelivery.ClientID%>' + '_1').checked && document.getElementById('<%=txtActionTaken.ClientID%>').value == '') {
                alert('<%=CommentRequired%>');
                return false;
            }           
            if (checkingRequired == "True")
                alert('<%=DeliveryCheckedMessage%>');          
                
        }

    </script>
    <h2>
        <cc1:ucLabel ID="lblQualityCheck" runat="server" Text="Quality Check"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
       
        <div class="formbox">
            <cc1:ucPanel ID="pnlBookingDetailsArrival" runat="server" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td style="width: 10%;">
                            <cc1:ucLabel ID="lblOperatorName" runat="server" />
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 20%;" class="nobold" colspan="3">
                            <cc1:ucLabel ID="OperatorNameData" runat="server" />
                        </td>
                    </tr>
                    <tr id="trVendorCarrier" runat="server">
                        <td style="width: 4%;">
                            <cc1:ucLabel ID="lblVendorBooking" runat="server" Text="Vendor" />
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 20%;" class="nobold">
                            <cc1:ucLabel ID="VendorNameData" runat="server" />
                        </td>
                        <td style="width: 4%;">
                            <cc1:ucLabel ID="lblCarrierBooking" runat="server" Text="Carrier" />
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 20%;" class="nobold" colspan="6">
                            <cc1:ucLabel ID="CarrierNameData" runat="server" Text="ANC" />
                        </td>
                    </tr>
                    <tr id="trCarrier" runat="server">
                        <td style="width: 4%;">
                            <cc1:ucLabel ID="lblCarrierBooking1" runat="server" Text="Carrier" />
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 20%;" class="nobold" colspan="9">
                            <cc1:ucLabel ID="CarrierNameData1" runat="server" Text="ANC" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 4%;">
                            <cc1:ucLabel ID="lblDateBooking" runat="server" Text="Date" />
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 20%;" class="nobold">
                            <cc1:ucLabel ID="BookingDateData" runat="server" Text="" />
                            &nbsp;
                            <cc1:ucLabel ID="BookingTimeData" runat="server" Text="" />
                        </td>
                        <td style="width: 10%;">
                            <cc1:ucLabel ID="lblVehicleType" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 14%;" class="nobold">
                            <cc1:ucLabel ID="VehicleTypeData" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 10%;">
                            <cc1:ucLabel ID="lblDoorName" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 14%;" class="nobold">
                            <cc1:ucLabel ID="DoorNumberData" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 4%;">
                        </td>
                        <td style="width: 1%;">
                        </td>
                        <td style="width: 20%;" class="nobold">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblPalletsBooking" runat="server" Text="Pallets" />
                        </td>
                        <td>
                            :
                        </td>
                        <td style="width: 100px;" class="nobold">
                            <cc1:ucLabel ID="BookingPalletsData" runat="server" Text="20" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblCartons" runat="server" Text="Cartons" />
                        </td>
                        <td>
                            :
                        </td>
                        <td style="width: 100px;" class="nobold">
                            <cc1:ucLabel ID="BookingCartonsData" runat="server" Text="0" />
                        </td>
                        <td style="width: 100px;">
                            <cc1:ucLabel ID="lblLines" runat="server" Text="Lines" />
                        </td>
                        <td>
                            :
                        </td>
                        <td style="width: 100px;" class="nobold">
                            <cc1:ucLabel ID="BookingLinesData" runat="server" Text="16" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblLiftsBooking" runat="server" Text="Lifts" />
                        </td>
                        <td>
                            :
                        </td>
                        <td style="width: 100px;" class="nobold">
                            <cc1:ucLabel ID="BookingLiftsData" runat="server" Text="31" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <br />
            <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settings">
                <tr runat="server" id="trOperator" visible="true">
                    <td style="font-weight: bold; width: 28%">
                        <cc1:ucLabel ID="lblOperatorInitials" runat="server" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 21%">
                        <cc1:ucTextbox ID="txtOperatorInitials" runat="server" Width="200px" MaxLength="50"></cc1:ucTextbox>
                    </td>
                    <td style="font-weight: bold; width: 11%" align="center">
                    </td>
                    <td style="font-weight: bold; width: 1%">
                    </td>
                    <td style="font-weight: bold; width: 38%">
                    </td>
                </tr>
                <tr runat="server" id="trSupervisor" visible="false">
                    <td style="font-weight: bold; width: 28%">
                        <cc1:ucLabel ID="lblSupervisorName" runat="server" Text="Supervisor Name"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 21%">
                        <cc1:ucDropdownList ID="ddlUser" runat="server" Width="150px">
                        </cc1:ucDropdownList>
                    </td>
                    <td style="font-weight: bold; width: 11%" align="center">
                        <cc1:ucLabel ID="lblPassword" runat="server" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 38%">
                        <cc1:ucTextbox ID="txtPassword" runat="server" Width="200px" MaxLength="50" TextMode="Password"></cc1:ucTextbox>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 28%">
                        <cc1:ucLabel ID="lblDeliveryCorrect" runat="server" Text="Was the delivery booked in correctly"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td align="left" colspan="4">
                        <cc1:ucRadioButtonList runat="server" ID="rdoDelivery" Width="100px" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            <asp:ListItem Text="No" Selected="True" Value="0"></asp:ListItem>
                        </cc1:ucRadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="font-weight: bold;">
                        <cc1:ucLabel ID="lblActionQualityCheck" runat="server" Text="If incorrectly booked in please enter narrative of issue and action taken"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <cc1:ucTextbox ID="txtActionTaken" runat="server" MaxLength="1000" TextMode="MultiLine"
                            Width="98%"></cc1:ucTextbox>
                    </td>
                </tr>
            </table>
        </div>
        <div class="button-row">
            <cc1:ucButton ID="btnOK" Width="100px" runat="server" Text="OK" CssClass="button"
                OnClick="btnOK_Click" OnClientClick="return ShowCheckingMessage()" />
            <cc1:ucButton ID="btnCancel" Width="100px" runat="server" Text="Cancel" CssClass="button"
                OnClick="btnCancel_Click" />
        </div>
    </div>
</asp:Content>

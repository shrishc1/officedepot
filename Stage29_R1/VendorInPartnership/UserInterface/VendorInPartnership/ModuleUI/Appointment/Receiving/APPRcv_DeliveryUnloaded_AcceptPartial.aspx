﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="APPRcv_DeliveryUnloaded_AcceptPartial.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="APPRcv_DeliveryUnloaded_AcceptPartial" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">



        function calculate(obj, arg) {
           
                        if (arg == '4') {

                        var totalAcceptedPallets = document.getElementById('<%=lblTotalAcceptedPallets.ClientID %>');
                        var total = 0;
                        var Parent = document.getElementById("<%=grdAcceptPartial.ClientID%>");
                        var items = Parent.getElementsByTagName('input');
                        for (i = 0; i < items.length; i++) {
                            var len = items[i].id.split('_').length;
                            if (items[i].id.split('_')[len - 1] == 'txtAcceptedPallets') {

                                var objVal = document.getElementById(items[i].id).value;
                                if (document.getElementById(items[i].id).value != null && document.getElementById(items[i].id).value != '')
                                    total = parseInt(total) + parseInt(objVal);
                            }
                        }
                        totalAcceptedPallets.innerText = total;
                        document.getElementById('<%=hdnTotalPallets.ClientID %>').value = total;
                    }

                    if (arg == '5') {

                        var totalAcceptedCartons = document.getElementById('<%=lblTotalAcceptedCartons.ClientID %>');
                        var total = 0;
                        var Parent = document.getElementById("<%=grdAcceptPartial.ClientID%>");
                        var items = Parent.getElementsByTagName('input');
                        for (i = 0; i < items.length; i++) {
                            var len = items[i].id.split('_').length;
                            if (items[i].id.split('_')[len - 1] == 'txtAcceptedCartons') {

                                var objVal = document.getElementById(items[i].id).value;
                                if (document.getElementById(items[i].id).value != null && document.getElementById(items[i].id).value != '')
                                    total = parseInt(total) + parseInt(objVal);
                            }
                        }
                        totalAcceptedCartons.innerText = total;
                    }

                    if (arg == '6') {

                        var totalRefusedPallets = document.getElementById('<%=lblTotalRefusedPallets.ClientID %>');
                        var total = 0;
                        var Parent = document.getElementById("<%=grdAcceptPartial.ClientID%>");
                        var items = Parent.getElementsByTagName('input');
                        for (i = 0; i < items.length; i++) {
                            var len = items[i].id.split('_').length;
                            if (items[i].id.split('_')[len - 1] == 'txtRefusedPallets') {

                                var objVal = document.getElementById(items[i].id).value;
                                if (document.getElementById(items[i].id).value != null && document.getElementById(items[i].id).value != '')
                                    total = parseInt(total) + parseInt(objVal);
                            }
                        }
                        totalRefusedPallets.innerText = total;
                    }


                    if (arg == '7') {

                        var totalRefuseCartons = document.getElementById('<%=lblTotalRefusedCartons.ClientID %>');
                        var total = 0;
                        var Parent = document.getElementById("<%=grdAcceptPartial.ClientID%>");
                        var items = Parent.getElementsByTagName('input');
                        for (i = 0; i < items.length; i++) {
                            var len = items[i].id.split('_').length;
                            if (items[i].id.split('_')[len - 1] == 'txtRefusedCartons') {

                                var objVal = document.getElementById(items[i].id).value;
                                if (document.getElementById(items[i].id).value != null && document.getElementById(items[i].id).value != '')
                                    total = parseInt(total) + parseInt(objVal);
                                
                            }
                        }
                      
                        totalRefuseCartons.innerText = total;
                    }

                }
               

    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblDeliveryUnloadAcceptPartial" runat="server" Text="Delivery Unload - Accept Partial"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucPanel ID="pnlBookingDetailsArrival" GroupingText="Booking Details" runat="server"
                        CssClass="fieldset-form">
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <tr id="trVendorCarrier" runat="server">
                                <td style="width: 4%;">
                                    <cc1:ucLabel ID="lblVendorBooking" runat="server" Text="Vendor" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 20%;" class="nobold">
                                    <cc1:ucLabel ID="VendorNameData" runat="server" />
                                </td>
                                <td style="width: 4%;">
                                    <cc1:ucLabel ID="lblCarrierBooking" runat="server" Text="Carrier" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 20%;" class="nobold" colspan="6">
                                    <cc1:ucLabel ID="CarrierNameData" runat="server" Text="ANC" />
                                </td>
                            </tr>
                            <tr id="trCarrier" runat="server">
                                <td style="width: 4%;">
                                    <cc1:ucLabel ID="lblCarrierBooking1" runat="server" Text="Carrier" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 20%;" class="nobold" colspan="9">
                                    <cc1:ucLabel ID="CarrierNameData1" runat="server" Text="ANC" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 4%;">
                                    <cc1:ucLabel ID="lblDateBooking" runat="server" Text="Date" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 20%;" class="nobold">
                                    <cc1:ucLabel ID="BookingDateData" runat="server" Text="19/01/2011" />
                                    &nbsp;-
                                    <cc1:ucLabel ID="BookingTimeData" runat="server" Text="09:10" />
                                </td>
                                <td style="width: 10%;">
                                    <cc1:ucLabel ID="lblVehicleType" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 14%;" class="nobold">
                                    <cc1:ucLabel ID="VehicleTypeData" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 10%;">
                                    <cc1:ucLabel ID="lblDoorName" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 14%;" class="nobold">
                                    <cc1:ucLabel ID="DoorNumberData" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 4%;">
                                </td>
                                <td style="width: 1%;">
                                </td>
                                <td style="width: 20%;" class="nobold">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblPalletsBooking" runat="server" Text="Pallets" />
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 100px;" class="nobold">
                                    <cc1:ucLabel ID="BookingPalletsData" runat="server" Text="20" />
                                </td>
                                <td>
                                    <cc1:ucLabel ID="lblCartons" runat="server" Text="Cartons" />
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 100px;" class="nobold">
                                    <cc1:ucLabel ID="BookingCartonsData" runat="server" Text="0" />
                                </td>
                                <td style="width: 100px;">
                                    <cc1:ucLabel ID="lblLines" runat="server" Text="Lines" />
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 100px;" class="nobold">
                                    <cc1:ucLabel ID="BookingLinesData" runat="server" Text="16" />
                                </td>
                                <td>
                                    <cc1:ucLabel ID="lblLiftsBooking" runat="server" Text="Lifts" />
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 100px;" class="nobold">
                                    <cc1:ucLabel ID="BookingLiftsData" runat="server" Text="31" />
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <cc1:ucPanel ID="pnlArrivalDetails" GroupingText="Arrival Details" runat="server"
                        CssClass="fieldset-form">
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <tr>
                                <td style="width: 10%;">
                                    <cc1:ucLabel ID="lblVehicleTypeArrival" runat="server" Text="Vehicle Type" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 39%;" class="nobold">
                                    <cc1:ucDropdownList ID="ddlVehicleType" runat="server" Width="150px" AutoPostBack="true">
                                    </cc1:ucDropdownList>
                                </td>
                                <td style="width: 10%;">
                                    <cc1:ucLabel ID="lblDate" runat="server" Text="Date" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 39%;" class="nobold" colspan="6">
                                    <cc1:ucLabel ID="UnloadDateValue" runat="server" Text="" Width="100%" />
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <cc1:ucPanel ID="pnlVolumeDetails" GroupingText="Volume Details" runat="server" CssClass="fieldset-form">
                        <cc1:ucGridView ID="grdAcceptPartial" Width="100%" runat="server" CssClass="grid"
                            OnRowDataBound="grdAcceptPartial_RowDataBound" ShowFooter="true" onsorting="SortGrid" AllowSorting="true">
                            <Columns>
                                <asp:TemplateField HeaderText="Vendor" SortExpression="FixedSlot.Vendor.VendorName">
                                    <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral runat="server" ID="lblVendorName" Text='<%# Eval("FixedSlot.Vendor.VendorName") %>'></cc1:ucLiteral>
                                        <cc1:ucLiteral runat="server" ID="lblVendorId" Visible="false" Text='<%# Eval("FixedSlot.VendorID") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left"  />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expected Pallets" SortExpression="FixedSlot.MaximumPallets">
                                    <HeaderStyle Width="12%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="LBLExpectedPallets" Text='<%# Eval("FixedSlot.MaximumPallets")%>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expected Cartons" SortExpression="FixedSlot.MaximumCatrons">
                                    <HeaderStyle Width="12%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="LBLExpectedCartons" Text='<%# Eval("FixedSlot.MaximumCatrons") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expected Lifts" SortExpression="LiftsScheduled">
                                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="LBLExpectedLifts" Text='<%# Eval("LiftsScheduled") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Accepted Pallets">
                                    <HeaderStyle Width="12%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucTextbox runat="server" ID="txtAcceptedPallets" Width="30px" Text='' style="text-align:center;"
                                            MaxLength="3" onblur="calculate(this,'4');" onkeyup="AllowNumbersOnly(this);"></cc1:ucTextbox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Accepted Cartons">
                                    <HeaderStyle Width="12%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucTextbox runat="server" ID="txtAcceptedCartons" Width="30px" Text='' style="text-align:center;"
                                            MaxLength="4" onblur="calculate(this,'5');" onkeyup="AllowNumbersOnly(this);"></cc1:ucTextbox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Refused Pallets">
                                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucTextbox runat="server" Text="" ID="txtRefusedPallets" Width="30px" MaxLength="3" style="text-align:center;"
                                           onblur="calculate(this,'6');" onkeyup="AllowNumbersOnly(this);"></cc1:ucTextbox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Refused Cartons">
                                    <HeaderStyle Width="12%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucTextbox runat="server" Text="" ID="txtRefusedCartons" Width="30px" MaxLength="4" style="text-align:center;"
                                             onblur="calculate(this,'7');" onkeyup="AllowNumbersOnly(this);"></cc1:ucTextbox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                        <table width="100%" >
                            <tr>
                                <td style="width: 20%;">
                                    <cc1:ucLabel runat="server" ID="lblTotal" Text="Total" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                 
                               <td style="width: 12%;" align="center">
                                    <cc1:ucLabel runat="server" ID="lblExpectedPalletsTotal" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                 <td style="width: 12%;" align="center">
                                    <cc1:ucLabel runat="server" ID="lblExpectedCartonsTotal" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                <td style="width: 10%;" align="center">
                                    <cc1:ucLabel runat="server" ID="lblExpectedLiftsTotal" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                 <td style="width: 12%;" align="center">
                                    <cc1:ucLabel runat="server" ID="lblTotalAcceptedPallets" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                <td style="width: 12%;" align="center">
                                    <cc1:ucLabel runat="server" ID="lblTotalAcceptedCartons" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                <td style="width: 10%;" align="center">
                                    <cc1:ucLabel runat="server" ID="lblTotalRefusedPallets" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                 <td style="width: 12%;" align="center">
                                    <cc1:ucLabel runat="server" ID="lblTotalRefusedCartons" Font-Bold="true"></cc1:ucLabel>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnConfirm" runat="server" Text="Confirm" CssClass="button" OnClick="btnConfirm_Click" />
                <cc1:ucButton ID="btnExit" runat="server" Text="Exit" CssClass="button" OnClick="btnExit_Click" />
                 <asp:HiddenField ID="hdnTotalPallets" runat="server" Value="0" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

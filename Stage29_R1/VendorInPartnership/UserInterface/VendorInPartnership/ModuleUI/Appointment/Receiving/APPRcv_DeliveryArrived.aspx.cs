﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.IO;
using System.Configuration;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Web.UI.HtmlControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System.Drawing;


public partial class APPRcv_DeliveryArrived : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("ID") != null && GetQueryStringValue("ID").ToString() != "")
            {
                BookingDetails(Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id")));
                BindAllPOGrid();
            }

            if (GetQueryStringValue("SiteID") != null && Convert.ToString(GetQueryStringValue("SiteID")) != "0")
            {
                MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(GetQueryStringValue("SiteID")));
                hdnIsDelArrivedPassAllow.Value = singleSiteSetting.DeliveryArrived.Trim().ToUpper();
            }
        }

        // Sprint 1 - Point 8 - Start
        if (!ddlOperators.SelectedValue.Equals(Convert.ToString(Session["LoginID"]))
            && hdnIsDelArrivedPassAllow.Value == "Y")
        {
            tblPassword.Style.Remove("display");
            rfvOperatorPasswordRequired.Enabled = true;
        }
        else
        {
            tblPassword.Style.Add("display", "none");
            rfvOperatorPasswordRequired.Enabled = false;
        }
        // Sprint 1 - Point 8 - End
    }

    #region Methods

    public void BookingDetails(int pPkID)
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "DeliveryArrived";
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(GetQueryStringValue("Scheduledate").ToString());

        List<APPBOK_BookingBE> lstDeliveryArrived = oAPPBOK_BookingBAL.GetBookingDetailsBAL(oAPPBOK_BookingBE);

        Session["BookingDetail"] = lstDeliveryArrived;
        if (lstDeliveryArrived != null && lstDeliveryArrived.Count > 0)
        {
            if (ExtractInformation(GetQueryStringValue("ID").ToString(), "type").ToLower() == "c")
            {
                trCarrier.Style["display"] = "";
                trVendorCarrier.Style["display"] = "none";
                CarrierNameData1.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
            }
            else
            {
                trCarrier.Style["display"] = "none";
                trVendorCarrier.Style["display"] = "";
                CarrierNameData.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
                VendorNameData.Text = lstDeliveryArrived[0].FixedSlot.Vendor.VendorName.ToString();               
            }

            BookingDateData.Text = GetQueryStringValue("Scheduledate").ToString();

            if (!string.IsNullOrEmpty(lstDeliveryArrived[0].ExpectedDeliveryTime))
                BookingTimeData.Text = "- " + lstDeliveryArrived[0].ExpectedDeliveryTime.ToString();

            VehicleTypeData.Text = lstDeliveryArrived[0].VehicleType.VehicleType.ToString();
            DoorNumberData.Text = lstDeliveryArrived[0].DoorNoSetup.DoorNumber.ToString();

            if (lstDeliveryArrived.Count == 1)
            {
                BookingLiftsData.Text = lstDeliveryArrived[0].LiftsScheduled.ToString();
                BookingLinesData.Text = lstDeliveryArrived[0].FixedSlot.MaximumLines.ToString();
                BookingPalletsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumPallets.ToString();
                BookingCartonsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumCatrons.ToString();
            }
            else
            {
                int? iLifts = 0, iLines = 0, iPallets = 0, iCartons = 0;
                for (int i = 0; i < lstDeliveryArrived.Count; i++)
                {
                    iLifts += lstDeliveryArrived[i].LiftsScheduled;
                    iLines += lstDeliveryArrived[i].FixedSlot.MaximumLines;
                    iPallets += lstDeliveryArrived[i].FixedSlot.MaximumPallets;
                    iCartons += lstDeliveryArrived[i].FixedSlot.MaximumCatrons;
                }
                BookingLiftsData.Text = iLifts.ToString();
                BookingLinesData.Text = iLines.ToString();
                BookingPalletsData.Text = iPallets.ToString();
                BookingCartonsData.Text = iCartons.ToString();
            }

            lblAdditionalInfo.Text = Convert.ToString(lstDeliveryArrived[0].BookingComments);

            // Sprint 1 - Point 8 - Start
            SCT_UserBAL UserBAL = new SCT_UserBAL();
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            oSCT_UserBE.Action = "GetManageDeliveryUsersBySiteId";
            oSCT_UserBE.SiteId = lstDeliveryArrived[0].SiteId;
            List<SCT_UserBE> lstUsers = UserBAL.GetManageDeliveryUsersBAL(oSCT_UserBE);

            ddlOperators.DataSource = lstUsers;
            FillControls.FillDropDown(ref ddlOperators, lstUsers, "FullName", "LoginID");
            ddlOperators.DataBind();
            ddlOperators.SelectedValue = Convert.ToString(Session["LoginID"]);
            hdnLoggedInUserId.Value = Convert.ToString(Session["LoginID"]);
            // Sprint 1 - Point 8 - End

            if (lstDeliveryArrived[0].SupplierType != "C")
            {
                if (lstDeliveryArrived[0].IsVenodrPalletChecking)
                {
                    Label lblMesg = new Label
                    {
                        Text = WebCommon.getGlobalResourceValue("DeliveryArrivedPalletCheckingMesg"),
                        ForeColor = Color.Red
                    };
                    lblMesg.Font.Bold = true;
                    lblMesg.Font.Size = new FontUnit(12);
                    divPalletCheckMesg.Controls.Add(lblMesg);
                    divPalletCheckMesg.Visible = true;
                }
                if (lstDeliveryArrived[0].IsStandardPalletChecking)
                {
                    Label lblMesg = new Label
                    {
                        Text = WebCommon.getGlobalResourceValue("DeliveryArrivedPalletCheckingMsgForStandard"),
                        ForeColor = Color.Red
                    };

                    lblMesg.Font.Bold = true;
                    lblMesg.Font.Size = new FontUnit(12);
                    divPalletCheckMesg.Controls.Add(lblMesg);
                    divPalletCheckMesg.Visible = true;
                }
            }
            else
            {
                int count = 0, VenodrPalletChecking = 0,
                StandardPalletChecking = 0;

                string mesg = "<br />";
                for (int i = 0; i < lstDeliveryArrived.Count; i++)
                {
                    if (lstDeliveryArrived[i].IsVenodrPalletChecking)
                    {
                        mesg += lstDeliveryArrived[i].FixedSlot.Vendor.VendorName + "<br /><br />";
                        count += count + 1;
                        VenodrPalletChecking += VenodrPalletChecking + 1;
                    }
                    if (lstDeliveryArrived[i].IsStandardPalletChecking)
                    {
                        mesg += lstDeliveryArrived[i].FixedSlot.Vendor.VendorName + "<br /><br />";
                        count += count + 1;
                        StandardPalletChecking += StandardPalletChecking + 1;
                    }
                }

                if (count > 0)
                {
                    Label lblMesg = new Label
                    {
                        Text = VenodrPalletChecking > 0 ? WebCommon.getGlobalResourceValue("CarrierDeliveryArrivedPalletCheckingMesg")
                        : WebCommon.getGlobalResourceValue("CarrierDeliveryArrivedPalletCheckingMsgForStandard") + "<br /> ",
                        ForeColor = Color.Red
                    };
                    lblMesg.Font.Bold = true;
                    lblMesg.Font.Size = new FontUnit(12);
                    divPalletCheckMesg.Controls.Add(lblMesg);

                    Label lblVendor = new Label
                    {
                        Text = mesg,
                        ForeColor = Color.Black
                    };
                    lblVendor.Font.Bold = true;
                    divPalletCheckMesg.Controls.Add(lblVendor);
                    divPalletCheckMesg.Visible = true;
                }
            }

            if (lstDeliveryArrived[0].IsEnableHazardouesItemPrompt)
            {
                divPalletCheckMesg.Controls.Add(new LiteralControl("<br />"));
                divPalletCheckMesg.Controls.Add(new LiteralControl("<br />"));
                Label lblMesg = new Label
                {
                    Text = WebCommon.getGlobalResourceValue("DeliveryContainsHazardeousProducts"),
                    ForeColor = Color.Red
                };
                lblMesg.Font.Bold = true;
                lblMesg.Font.Size = new FontUnit(10);
                divPalletCheckMesg.Controls.Add(lblMesg);
                divPalletCheckMesg.Visible = true;
            }
        }
    }

    public string ExtractInformation(string pFullData, string pReturnType)
    {
        int i = -1;
        switch (pReturnType)
        {
            case "table":
                i = 0;
                break;
            case "type":
                i = 1;
                break;
            case "id":
                i = 2;
                break;
            case "siteid":
                i = 3;
                break;
        }
        return (pFullData.Split('-')[i].ToString());
    }

    protected void BindAllPOGrid()
    {
        DataTable myDataTable = new DataTable();
        DataColumn myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "PurchaseNumber";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Original_due_date";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "VendorName";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "OutstandingLines";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Qty_On_Hand";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "qty_on_backorder";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "SiteName";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "StockPlannerName";
        myDataTable.Columns.Add(myDataColumn);

        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

        oUp_PurchaseOrderDetailBE.Action = "GetAllBookingPODetails";
        oUp_PurchaseOrderDetailBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));

        if (Session["Role"] != null && Session["Role"].ToString() != "")
        {
            if (Session["Role"].ToString() == "Vendor")
                oUp_PurchaseOrderDetailBE.UserID = Convert.ToInt32(Session["UserID"]);
        }

        List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllBookedProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);

        if (lstPO.Count > 0)
        {
            for (int iCount = 0; iCount < lstPO.Count; iCount++)
            {
                DataRow dataRow = myDataTable.NewRow();

                dataRow["Qty_On_Hand"] = lstPO[iCount].Stockouts;
                dataRow["qty_on_backorder"] = lstPO[iCount].Backorders;
                dataRow["OutstandingLines"] = lstPO[iCount].OutstandingLines.ToString();
                dataRow["StockPlannerName"] = lstPO[iCount].StockPlannerName;

                dataRow["PurchaseNumber"] = lstPO[iCount].Purchase_order;
                dataRow["Original_due_date"] = lstPO[iCount].Original_due_date.Value.ToString("dd/MM/yyyy");
                dataRow["VendorName"] = lstPO[iCount].Vendor.VendorName;
                dataRow["SiteName"] = lstPO[iCount].Site.SiteName;

                myDataTable.Rows.Add(dataRow);
            }
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0)
        {
            gvPO.DataSource = myDataTable;
            gvPO.DataBind();
        }
    }

    public void UpdateBookingStatus(int pBookingStatusID)
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "UpdateDeliveryArrivedFirstStep";
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.BookingStatusID = pBookingStatusID;
        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");

        // Sprint 1 - Point 8 
        oAPPBOK_BookingBE.DLYARR_OperatorInital = ddlOperators.SelectedItem.Text.Trim();
        oAPPBOK_BookingBE.TransactionComments = txtComments.Text;

        oAPPBOK_BookingBE.FixedSlot.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oAPPBOK_BookingBE.FixedSlot.User.UserID = Convert.ToInt32(Session["UserID"].ToString().Trim());
        oAPPBOK_BookingBAL.addEditBookingDetailsBAL(oAPPBOK_BookingBE);

    }

    private MAS_SiteBE GetSingleSiteSetting(int siteId)
    {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSingleSiteSetting";
        oMAS_SiteBE.SiteID = siteId;
        MAS_SiteBE localMAS_SiteBE = oMAS_SiteBAL.GetSingleSiteSettingBAL(oMAS_SiteBE);
        return localMAS_SiteBE;
    }

    #endregion

    #region Events

    protected void btnReject_Click(object sender, EventArgs e)
    {

        //DO NOT UPDATE THE BOOKING STATUS ID HERE
        UpdateBookingStatus(Convert.ToInt16(Common.BookingStatus.NoStatusChange));

        // Sprint 1 - Point 8 - Start
        Session["RefuseDelivery"] = ddlOperators.SelectedItem.Text.Trim();
        Session["TransactionComments"] = txtComments.Text;
        // Sprint 1 - Point 8 - End

        EncryptQueryString("APPRcv_DeliveryRefusal.aspx?Scheduledate="
            + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString()
            + "&IsDelArriPassAllow=" + hdnIsDelArrivedPassAllow.Value);

    }
    protected void btnAccept_Click(object sender, EventArgs e)
    {

        // Sprint 1 - Point 8 - Start
        Session["TransactionComments"] = txtComments.Text;
        // Sprint 1 - Point 8 - End

        UpdateBookingStatus(Convert.ToInt16(Common.BookingStatus.DeliveryArrived));

        EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx?Scheduledate="
            + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString());

    }

    // Sprint 1 - Point 8 - Start
    private bool ValidateOperator()
    {
        bool IsValidUser = true;
        if (!Convert.ToString(Session["LoginID"]).Equals(ddlOperators.SelectedValue)
            && hdnIsDelArrivedPassAllow.Value == "Y")
        {
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

            oSCT_UserBE.Action = "GetDetails";
            oSCT_UserBE.LoginID = ddlOperators.SelectedValue;
            oSCT_UserBE.Password = txtPassword.Text.Trim();

            List<SCT_UserBE> lstSCT_UserBE = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);

            if (lstSCT_UserBE.Count <= 0) 
                IsValidUser = false; 
        }
        return IsValidUser;
    }
    // Sprint 1 - Point 8 - End

    protected void btnExit_Click(object sender, EventArgs e)
    {
        EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx?Scheduledate="
            + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString());
    }
    #endregion
}
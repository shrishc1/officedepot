﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Security;
using Utilities;
using WebUtilities;
using System.Linq;
using System.Web.UI.WebControls;

public partial class APPRcv_DeliveryRefusal : CommonPage
{

    protected string RefusalCommentsValidation = WebCommon.getGlobalResourceValue("RefusalCommentsValidation");
    protected string SupervisorPasswordValidation = WebCommon.getGlobalResourceValue("SupervisorPasswordValidation");
    protected string PasswordNotMatch = WebCommon.getGlobalResourceValue("PasswordNotMatch");
    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    string templatePathName = @"/EmailTemplates/Appointment/";
    protected string AtLeastOnePoingFailure = WebCommon.getGlobalResourceValue("AtLeastOnePoingFailure");
    string strDefaultEmailId = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int? SiteId = BookingDetails(Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id")));
            BindSuperUsers(SiteId.Value);

            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.Action = GetQueryStringValue("IsFromUDC") != null
                ? GetQueryStringValue("IsFromUDC") == "true" ? "GetBookingDeatilsByID_Unexpected" : "GetBookingDeatilsByID"
                : "GetBookingDeatilsByID";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));
            var  lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);
            if (lstBooking.Count > 0)
            {
                lblDateTimeBookingValue.Text = lstBooking[0].ScheduleDate.ToString();
                var vendor = lstBooking[0].Vendor.VendorName.ToString().Split('-');
                lblVendorValue.Text= vendor[0];
                lblVendorNameValue.Text = vendor[1];
                lblBookingReferenceValue.Text = lstBooking[0].BookingRef.ToString();
            }
        }

        if (GetQueryStringValue("IsDelArriPassAllow") != null)
        {
            hdnIsDelArrivedPassAllow.Value = GetQueryStringValue("IsDelArriPassAllow").Trim().ToUpper();
        }
    }

    #region Methods

    private bool CheckBoxCheckStatus()
    {
        if (chkLate.Checked || chkEarly.Checked || chkNoPaperworkondisplay.Checked || chkPalletsDamaged.Checked ||
            chkPackagingDamaged.Checked || chkunsafeload.Checked || chkWrongAddress.Checked || chkRefusedtowait.Checked ||
            chkNotToOdSpecification.Checked || chkOther.Checked)
            return true;
        else
            return false;
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        string objSelectedUserPass = ddlUser.SelectedValue.Split('-')[1];

        if (CheckBoxCheckStatus() == false)
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "alert('" + AtLeastOnePoingFailure + "');", true);
            return;
        }

        if (objSelectedUserPass != null && txtSupervisorPassword.Text != null && hdnIsDelArrivedPassAllow.Value == "Y")
        {
            divApproveBy.Attributes.Add("Style", "visibility:visible");
            divPassLabel.Attributes.Add("Style", "visibility:visible");
            divPassCollon.Attributes.Add("Style", "visibility:visible");
            divPassTextBox.Attributes.Add("Style", "visibility:visible");
        }
        else if (objSelectedUserPass != null && txtSupervisorPassword.Text != null && hdnIsDelArrivedPassAllow.Value == "N")
        {
            divApproveBy.Attributes.Add("Style", "visibility:hidden");
            divPassLabel.Attributes.Add("Style", "visibility:hidden");
            divPassCollon.Attributes.Add("Style", "visibility:hidden");
            divPassTextBox.Attributes.Add("Style", "visibility:hidden");
            //if (hdnSecondClickStatus.Value.Trim().ToUpper() != "SECONDCLICK")
            //    return;
        }

        if (!Common.VerifyPassword(txtSupervisorPassword.Text, objSelectedUserPass) && hdnIsDelArrivedPassAllow.Value == "Y")
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "alert('" + PasswordNotMatch + "');", true);
            return;
        }
        //UPDATE THE BOOKING STATUS ID HERE
       // UpdateBookingStatus(Convert.ToInt16(Common.BookingStatus.RefusedDelivery));

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "UpdateDeliveryRefusal";
        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        oAPPBOK_BookingBE.DLYREF_ReasonArrivedEarly = chkEarly.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonArrivedLate = chkLate.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonNoPaperworkOndisplay = chkNoPaperworkondisplay.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonPalletsDamaged = chkPalletsDamaged.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonPackagingDamaged = chkPackagingDamaged.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonWrongAddress = chkWrongAddress.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonRefusedTowait = chkRefusedtowait.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonNottoODSpecification = chkNotToOdSpecification.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonOther = chkOther.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonUnsafeLoad = chkunsafeload.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonComments = txtRefusalComments.Text;
        oAPPBOK_BookingBE.DLYREF_SupervisorID = Convert.ToInt32(ddlUser.SelectedItem.Value.Split('-')[0]);
       // oAPPBOK_BookingBAL.addEditDeliveryRefusalBAL(oAPPBOK_BookingBE);

        List<APPBOK_BookingBE> lstBooking = new List<APPBOK_BookingBE>();
        try {
            if (chkEarly.Checked || chkLate.Checked || chkNoPaperworkondisplay.Checked || chkNotToOdSpecification.Checked ||
               chkOther.Checked || chkPackagingDamaged.Checked || chkPalletsDamaged.Checked || chkRefusedtowait.Checked ||
               chkWrongAddress.Checked || chkunsafeload.Checked) {

                   if (GetQueryStringValue("IsFromUDC") != null)
                   {
                       if (GetQueryStringValue("IsFromUDC") == "true")
                       {
                           oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID_Unexpected";
                       }
                       else
                       {
                           oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
                       }
                   }
                   else
                   {
                       oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
                   }
                oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));
                lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);
                if (lstBooking.Count > 0) {
                    MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
                    APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
                    List<MASSIT_VendorBE> lstVendorDetails = null;


                    // Sprint 1 - Point 9 - Begin
                    if (lstBooking[0].BookingID > 0 && lstBooking[0].BookingID != 0) {
                        this.SendEmailToStockPlanner(lstBooking[0].BookingID);
                    }
                    // Sprint 1 - Point 9 - End


                    if (lstBooking[0].SupplierType == "C") {
                        oMASSIT_VendorBE.Action = "GetCarriersVendor";
                        oMASSIT_VendorBE.BookingID = oAPPBOK_BookingBE.BookingID;

                        lstVendorDetails = oAPPSIT_VendorBAL.GetCarriersVendorIDBAL(oMASSIT_VendorBE);

                        ViewState["lstVendorDetails"] = lstVendorDetails;
                        ViewState["lstBooking"] = lstBooking;

                        ShowInformation();


                    }
                    else if (lstBooking[0].SupplierType == "V") {
                        string VendorEmails = string.Empty;
                        string[] sentTo;
                        string[] language;
                        string[] VendorData = new string[2];
                        string[] CarrierData = new string[2];

                        VendorData = CommonPage.GetVendorEmailsWithLanguage(lstBooking[0].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0])) {
                            sentTo = VendorData[0].Split(',');
                            language = VendorData[1].Split(',');
                            for (int j = 0; j < sentTo.Length; j++) {
                                if (sentTo[j] != " ") {
                                    if (GetQueryStringValue("SlotTime") != null)
                                        Sendmail(lstBooking, null, sentTo[j], language[j]);
                                    else {
                                        SendmailRefusedAll(lstBooking, null, sentTo[j], language[j]);
                                        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                                    }
                                }
                            }
                        }
                        else {
                            if (GetQueryStringValue("SlotTime") != null)
                                Sendmail(lstBooking, null, ConfigurationManager.AppSettings["DefaultEmailAddress"], "English");
                            else {
                                SendmailRefusedAll(lstBooking, null, ConfigurationManager.AppSettings["DefaultEmailAddress"], "English");
                                Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                            }
                        }
                    }
                    

                }
            }
        }
        catch { }

        ///////////////////////////////////
        if (lstBooking.Count == 0 || lstBooking[0].SupplierType == "V")
        EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");

    }


    private void ShowInformation() {     
       
        List<APPBOK_BookingBE> lstBooking = (List<APPBOK_BookingBE>)ViewState["lstBooking"];
        try {

            List<APPBOK_BookingBE> lstCarrier = lstBooking.Take(1).ToList();
            if (lstCarrier.Count > 0) {
                chkCarrier.Text = lstCarrier[0].Carrier.CarrierName;
            }

            var lstVendor = lstBooking.Where(c => c.VendorID != 0).Select(bok => new { VendorName = bok.Vendor.VendorName, VendorID = bok.VendorID }).Distinct().ToList();

            if (lstVendor.Count > 0) {

                chkListVendors.DataTextField = "VendorName";
                chkListVendors.DataValueField = "VendorID";
                chkListVendors.DataSource = lstVendor;
                chkListVendors.DataBind();


                foreach (ListItem item in chkListVendors.Items) {
                    item.Selected = true;
                }
            }

            List<MASSIT_VendorBE> lstVendorDetails = (List<MASSIT_VendorBE>)ViewState["lstVendorDetails"];
            if (lstVendorDetails != null && lstVendorDetails.Count == 1) {
                SendCarrierMail();
                EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
            }
            else if (lstVendorDetails != null && lstVendorDetails.Count > 1) {
                mdInformation.Show();
            }          
           
        }
        catch (Exception ex) {
            //mdInformation.Show();
        }

        
    }

    protected void btnOK_2_Click(object sender, EventArgs e)
    {
        string objSelectedUserPass = ddlUser.SelectedValue.Split('-')[1];

        if (CheckBoxCheckStatus() == false)
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "alert('" + AtLeastOnePoingFailure + "');", true);
            return;
        }

        if (objSelectedUserPass != null && txtSupervisorPassword.Text != null && hdnIsDelArrivedPassAllow.Value == "Y")
        {
            divApproveBy.Attributes.Add("Style", "visibility:visible");
            divPassLabel.Attributes.Add("Style", "visibility:visible");
            divPassCollon.Attributes.Add("Style", "visibility:visible");
            divPassTextBox.Attributes.Add("Style", "visibility:visible");
        }
        else if (objSelectedUserPass != null && txtSupervisorPassword.Text != null && hdnIsDelArrivedPassAllow.Value == "N")
        {
            divApproveBy.Attributes.Add("Style", "visibility:hidden");
            divPassLabel.Attributes.Add("Style", "visibility:hidden");
            divPassCollon.Attributes.Add("Style", "visibility:hidden");
            divPassTextBox.Attributes.Add("Style", "visibility:hidden");
        }

        if (!Common.VerifyPassword(txtSupervisorPassword.Text, objSelectedUserPass) && hdnIsDelArrivedPassAllow.Value == "Y")
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "alert('" + PasswordNotMatch + "');", true);
            return;
        }

        updInfromation_Messge_update.Show();
    }
    protected void btnOK_1_Click(object sender, EventArgs e) {
        mdInformation.Hide();
        if (GetQueryStringValue("IsFromUDC") != null) // unexpected delievery check
        {
            if (GetQueryStringValue("IsFromUDC") == "true")
            {
                UnexpectedSendCarrierMail();
            }
            else
            {
                SendCarrierMail();
            }
        }
        else
        {
            SendCarrierMail();
        }
        EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
    }

    private void SendCarrierMail() {

        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        string VendorEmails = string.Empty;
        string[] sentTo;
        string[] language;
        string[] VendorData = new string[2];
        string[] CarrierData = new string[2];

        List<MASSIT_VendorBE> lstVendorDetails = (List<MASSIT_VendorBE>)ViewState["lstVendorDetails"];
        List<APPBOK_BookingBE> lstBooking = (List<APPBOK_BookingBE>)ViewState["lstBooking"];

        bool MailToBeSent = false;

        foreach (ListItem a in chkListVendors.Items) {
            if (a.Selected) {
                MailToBeSent = true;
                break;
            }
        }

        if (chkCarrier.Checked) {
            MailToBeSent = true;
        }

        if (lstVendorDetails != null && lstVendorDetails.Count > 0) {
            for (int i = 0; i < lstVendorDetails.Count; i++) {
                foreach (ListItem a in chkListVendors.Items) {
                    if (a.Selected && a.Value == Convert.ToString(lstVendorDetails[i].VendorID)) {
                        VendorData = CommonPage.GetVendorEmailsWithLanguage(lstVendorDetails[i].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                        break;
                    }
                }

                if (chkCarrier.Checked) {
                    /* Logic to adding the carrier user's email with Vendor Email. */
                    CarrierData = CommonPage.GetCarrierEmailsWithLanguage(Convert.ToInt32(lstBooking[0].Carrier.CarrierID), Convert.ToInt32(lstBooking[0].SiteId));
                    //for (int intIndex = 0; intIndex < CarrierData.Length; intIndex++)
                    if (CarrierData.Length > 0) {
                        VendorData[0] += CarrierData[0] + ",";
                        VendorData[1] += CarrierData[1] + ",";
                    }
                }

                if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0])) {
                    sentTo = VendorData[0].Split(',');
                    language = VendorData[1].Split(',');
                    for (int j = 0; j < sentTo.Length; j++) {
                        if (sentTo[j] != " ") {
                            //SendMailToVendor(sentTo[j], language[j], lstBooking);
                            if (GetQueryStringValue("SlotTime") != null)
                                Sendmail(lstBooking, lstVendorDetails[i], sentTo[j], language[j]);
                            else {
                                SendmailRefusedAll(lstBooking, lstVendorDetails[i], sentTo[j], language[j]);
                                Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                            }
                        }
                    }
                }
                else if (MailToBeSent) {
                    if (GetQueryStringValue("SlotTime") != null)
                        Sendmail(lstBooking, lstVendorDetails[i], ConfigurationManager.AppSettings["DefaultEmailAddress"], "English");
                    else {
                        SendmailRefusedAll(lstBooking, lstVendorDetails[i], ConfigurationManager.AppSettings["DefaultEmailAddress"], "English");
                        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                    }
                }
            }           
        }
    }

    private void UnexpectedSendCarrierMail()
    {

        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        string VendorEmails = string.Empty;
        string[] sentTo;
        string[] language;
        string[] VendorData = new string[2];
        string[] CarrierData = new string[2];
        string PO = string.Empty;
        List<MASSIT_VendorBE> lstVendorDetails = (List<MASSIT_VendorBE>)ViewState["lstVendorDetails"];
        List<APPBOK_BookingBE> lstBooking = (List<APPBOK_BookingBE>)ViewState["lstBooking"];

        bool MailToBeSent = false;

        foreach (ListItem a in chkListVendors.Items)
        {
            if (a.Selected)
            {
                MailToBeSent = true;
                break;
            }
        }

        if (chkCarrier.Checked)
        {
            MailToBeSent = true;
        }

        if (lstVendorDetails != null && lstVendorDetails.Count > 0)
        {
            for (int i = 0; i < lstVendorDetails.Count; i++)
            {
                foreach (ListItem a in chkListVendors.Items)
                {
                    if (a.Selected && a.Value == Convert.ToString(lstVendorDetails[i].VendorID))
                    {
                        VendorData = CommonPage.GetVendorEmailsWithLanguage(lstVendorDetails[i].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                        if (GetQueryStringValue("IsFromUDC") != null)
                        {
                            PO = lstBooking.Where(x => x.VendorID == lstVendorDetails[i].VendorID).Select(y => y.PO).FirstOrDefault();
                        }
                        break;
                    }
                }

                if (chkCarrier.Checked)
                {
                    /* Logic to adding the carrier user's email with Vendor Email. */
                    CarrierData = CommonPage.GetCarrierEmailsWithLanguage(Convert.ToInt32(lstBooking[0].Carrier.CarrierID), Convert.ToInt32(lstBooking[0].SiteId));
                    //for (int intIndex = 0; intIndex < CarrierData.Length; intIndex++)                    
                }
                
                if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                {
                    sentTo = VendorData[0].Split(',');
                    language = VendorData[1].Split(',');
                    for (int j = 0; j < sentTo.Length; j++)
                    {
                        if (sentTo[j] != " ")
                        {
                            //SendMailToVendor(sentTo[j], language[j], lstBooking);
                            if (GetQueryStringValue("SlotTime") != null)
                                Sendmail(lstBooking, lstVendorDetails[i], sentTo[j], language[j], PO);
                            else
                            {
                                SendmailRefusedAll(lstBooking, lstVendorDetails[i], sentTo[j], language[j]);
                                Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                            }
                        }
                    }
                }
                else if (MailToBeSent)
                {
                    if (GetQueryStringValue("SlotTime") != null)
                        Sendmail(lstBooking, lstVendorDetails[i], ConfigurationManager.AppSettings["DefaultEmailAddress"], "English");
                    else
                    {
                        SendmailRefusedAll(lstBooking, lstVendorDetails[i], ConfigurationManager.AppSettings["DefaultEmailAddress"], "English");
                        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                    }
                }
                if (GetQueryStringValue("IsFromUDC") != null)
                {
                    if (CarrierData.Length > 0 && !string.IsNullOrEmpty(CarrierData[0]))
                    {
                        PO = string.Empty;
                        sentTo = CarrierData[0].Split(',');
                        language = CarrierData[1].Split(',');
                        for (int j = 0; j < sentTo.Length; j++)
                        {
                            if (sentTo[j] != " ")
                            {
                                //SendMailToVendor(sentTo[j], language[j], lstBooking);
                                if (GetQueryStringValue("SlotTime") != null)
                                    Sendmail(lstBooking, lstVendorDetails[i], sentTo[j], language[j], isCarrier: true);
                                else
                                {
                                    SendmailRefusedAll(lstBooking, lstVendorDetails[i], sentTo[j], language[j]);
                                    Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    private void Sendmail(List<APPBOK_BookingBE> lstBooking, MASSIT_VendorBE lstVendorDetails, string toAddress, string language, string PO = "",bool isCarrier=false)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
           
            string htmlBody = string.Empty;
            sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();

            string templatePath = null;
            templatePath = path + templatePathName;

             var communicationBAL = new APPBOK_CommunicationBAL();
            var lstLanguages = communicationBAL.GetLanguages();
            foreach (BusinessEntities.ModuleBE.Languages.Languages.MAS_LanguageBE objLanguage in lstLanguages)
            {
                bool MailSentInLanguage = false;


                string Issues = string.Empty;
                if (chkEarly.Checked)
                    Issues += WebCommon.getGlobalResourceValue("Early", objLanguage.Language) + "<br/>";
                if (chkLate.Checked)
                    Issues += WebCommon.getGlobalResourceValue("Late", objLanguage.Language) + "<br/>";
                if (chkNoPaperworkondisplay.Checked)
                    Issues += WebCommon.getGlobalResourceValue("NoPaperworkondisplay", objLanguage.Language) + "<br/>";
                if (chkNotToOdSpecification.Checked)
                    Issues += WebCommon.getGlobalResourceValue("NotToOdSpecification", objLanguage.Language) + "<br/>";
                if (chkOther.Checked)
                    Issues += WebCommon.getGlobalResourceValue("Other", objLanguage.Language) + "<br/>";
                if (chkPackagingDamaged.Checked)
                    Issues += WebCommon.getGlobalResourceValue("PackagingDamaged", objLanguage.Language) + "<br/>";
                if (chkPalletsDamaged.Checked)
                    Issues += WebCommon.getGlobalResourceValue("PalletsDamaged", objLanguage.Language) + "<br/>";
                if (chkRefusedtowait.Checked)
                    Issues += WebCommon.getGlobalResourceValue("Refusedtowait", objLanguage.Language) + "<br/>";
                if (chkWrongAddress.Checked)
                    Issues += WebCommon.getGlobalResourceValue("WrongAddress", objLanguage.Language) + "<br/>";
                if (chkunsafeload.Checked)
                    Issues += WebCommon.getGlobalResourceValue("unsafeload", objLanguage.Language) + "<br/>";

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    MailSentInLanguage = true;
                }

                string LanguageFile = "UnexpectedRefused." + objLanguage.Language.ToLower() + ".htm";
                //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
                //{
                //    LanguageFile = "UnexpectedRefused." + "english.htm";
                //}
                LanguageFile = "UnexpectedRefused.english.htm";

                int iTotalPalllets = 0;
                int iTotalLines = 0;
                int iTotalCartons = 0;
                if (isCarrier == true)
                {                
                    if (lstBooking.Count > 0)
                    {
                        for (int iCount = 0; iCount < lstBooking.Count; iCount++)
                        {
                            iTotalPalllets += Convert.ToInt32(lstBooking[iCount].NumberOfPallet);
                            iTotalLines += Convert.ToInt32(lstBooking[iCount].NumberOfLines);
                            iTotalCartons += Convert.ToInt32(lstBooking[iCount].NumberOfCartons);
                        }
                    }
                }

                #region Setting reason as per the language ...
                switch (objLanguage.Language)
                {
                    case clsConstants.English:
                        Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                        break;
                    case clsConstants.French:
                        Page.UICulture = clsConstants.FranceISO; // // France
                        break;
                    case clsConstants.German:
                        Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                        break;
                    case clsConstants.Dutch:
                        Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                        break;
                    case clsConstants.Spanish:
                        Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                        break;
                    case clsConstants.Italian:
                        Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                        break;
                    case clsConstants.Czech:
                        Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                        break;
                    default:
                        Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                        break;
                }

                #endregion

                using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
                {
                    #region mailBody
                    htmlBody = sReader.ReadToEnd();
                    //htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                    htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                    htmlBody = htmlBody.Replace("{UnexpectedRefusedText1}", WebCommon.getGlobalResourceValue("UnexpectedRefusedText1"));
                    htmlBody = htmlBody.Replace("{UnexpectedRefusedText2}", WebCommon.getGlobalResourceValue("UnexpectedRefusedText2"));
                    htmlBody = htmlBody.Replace("{UnexpectedRefusedText3}", WebCommon.getGlobalResourceValue("UnexpectedRefusedText3"));

                    htmlBody = htmlBody.Replace("{BookingRef}", lstBooking[0].BookingRef);                    

                    htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));
                    htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy"));

                    htmlBody = htmlBody.Replace("{Time}", WebCommon.getGlobalResourceValue("Time"));
                    htmlBody = htmlBody.Replace("{TimeValue}", lstBooking[0].SlotTime.SlotTime);

                    htmlBody = htmlBody.Replace("{Site}", WebCommon.getGlobalResourceValue("Site"));
                    htmlBody = htmlBody.Replace("{SiteValue}", lstBooking[0].SiteName);

                    htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier"));
                    if (lstBooking[0].Carrier != null)
                        htmlBody = htmlBody.Replace("{CarrierValue}", lstBooking[0].Carrier.CarrierName);

                    htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValue("NumberofPallets"));
                    htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValue("NumberofCartons"));
                    if (lstBooking[0].SupplierType == "V")
                    {
                        htmlBody = htmlBody.Replace("{NumberofPalletsValue}", lstBooking[0].NumberOfPallet.ToString());
                        htmlBody = htmlBody.Replace("{NumberofCartonsValue}", lstBooking[0].NumberOfCartons.ToString());
                    }
                    else if (lstBooking[0].SupplierType == "C")
                    {
                        if (isCarrier == false)
                        {
                            htmlBody = htmlBody.Replace("{NumberofPalletsValue}", lstVendorDetails.NumberOfPallet.ToString());
                            htmlBody = htmlBody.Replace("{NumberofCartonsValue}", lstVendorDetails.NumberOfCartons.ToString());
                        }
                        else
                        {
                            htmlBody = htmlBody.Replace("{NumberofPalletsValue}", iTotalPalllets.ToString());
                            htmlBody = htmlBody.Replace("{NumberofCartonsValue}", iTotalCartons.ToString());
                        }
                    }

                    htmlBody = htmlBody.Replace("{PO}", WebCommon.getGlobalResourceValue("PO"));
                    if (!string.IsNullOrEmpty(PO))
                    {
                        htmlBody = htmlBody.Replace("{POValue}", PO);
                    }
                    else if (!string.IsNullOrEmpty(lstBooking[0].PurchaseOrders.Trim()))
                        htmlBody = htmlBody.Replace("{POValue}", lstBooking[0].PurchaseOrders.TrimEnd(',').TrimStart(','));
                    else
                        htmlBody = htmlBody.Replace("{POValue}", "-");

                    htmlBody = htmlBody.Replace("{ReasonforRefusal}", WebCommon.getGlobalResourceValue("ReasonforRefusal"));
                    htmlBody = htmlBody.Replace("{IssueValue}", Issues);

                    htmlBody = htmlBody.Replace("{Comments}", WebCommon.getGlobalResourceValue("Comments"));
                    htmlBody = htmlBody.Replace("{CommentsValue}", txtRefusalComments.Text);


                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                    #endregion
                }

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                #region Commented code
                //clsEmail oclsEmail = new clsEmail();
                //oclsEmail.sendMail(toAddress, htmlBody, "Unexpected Booking - Delivery Refused", sFromAddress, true);                                    
                //// Resending of Delivery mails
                //APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                //oAPPBOK_CommunicationBAL.AddItemBAL(lstBooking[0].BookingID, sFromAddress, toAddress, "Unexpected Booking - Delivery Refused", htmlBody, Convert.ToInt32(Session["UserID"]), CommunicationType.Enum.UnexpectedRefused);
                //// Send email to Stock Planner

                //// Sprint 1 - Point 9 - Begin
                //this.SendEmailToStockPlanner(lstBooking[0].BookingID, language);
                //// Sprint 1 - Point 9 - End
                #endregion

                // Resending of Delivery mails
                clsEmail oclsEmail = new clsEmail();
                string subject = "Unexpected Booking - Delivery Refused";
                CommunicationType.Enum communicationType = CommunicationType.Enum.UnexpectedRefused;
                APPBOK_CommunicationBAL oAPPBOK_AddCommunicationBAL = new APPBOK_CommunicationBAL();
                APPBOK_CommunicationBE oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
                oAPPBOK_CommunicationBE.Action = "AddBookingCommunication";
                oAPPBOK_CommunicationBE.CommunicationType = CommunicationType.Name(communicationType);
                oAPPBOK_CommunicationBE.BookingID = lstBooking[0].BookingID;
                oAPPBOK_CommunicationBE.Subject = subject;
                oAPPBOK_CommunicationBE.SentFrom = sFromAddress;
                oAPPBOK_CommunicationBE.SentTo = toAddress;
                oAPPBOK_CommunicationBE.Body = htmlBody;
                oAPPBOK_CommunicationBE.SendByID = Convert.ToInt32(Session["UserID"]);
                oAPPBOK_CommunicationBE.IsMailSent = MailSentInLanguage;
                oAPPBOK_CommunicationBE.LanguageID = objLanguage.LanguageID;
                oAPPBOK_CommunicationBE.MailSentInLanguage = MailSentInLanguage;
                int intIsExist = oAPPBOK_AddCommunicationBAL.AddBookingCommunicationBAL(oAPPBOK_CommunicationBE) ?? 0;
                if (intIsExist.Equals(0) && MailSentInLanguage.Equals(true))
                {
                    if (!string.IsNullOrWhiteSpace(toAddress))
                        oclsEmail.sendMail(toAddress, htmlBody, subject, sFromAddress, true);

                    #region Commented code ...
                    //// Sprint 1 - Point 9 - Begin
                    //    this.SendEmailToStockPlanner(lstBooking[0].BookingID, language);
                    //// Sprint 1 - Point 9 - End
                    #endregion
                }
            }
            Page.UICulture = Convert.ToString(Session["CultureInfo"]);
        }
    }

    // Sprint 1 - Point 9 - Begin
    private void SendEmailToStockPlanner(int intBookingID)
    {
        // Declareations
        APPBOK_BookingBE stockPlannerEmailBE = null;
        APPBOK_BookingBE refusedDeliveriesBE = null;
        APPBOK_BookingBE finalRefusedDeliveriesBE = null;
        APPBOK_BookingBAL oAPPBOK_BookingBAL = null;
        try
        {
            finalRefusedDeliveriesBE = new APPBOK_BookingBE();
            oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            string mailBody = string.Empty;
            string mailSubject = "Refused Delivery";
            string fromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

            // Logic to get data for Refused Deliveries
            refusedDeliveriesBE = new APPBOK_BookingBE();
            refusedDeliveriesBE.Action = "EmailDataForRefusedDeliveries";
            refusedDeliveriesBE.BookingID = intBookingID;
            List<APPBOK_BookingBE> lstRefusedDeliveries = oAPPBOK_BookingBAL.GetEmailDataForRefusedDeliveriesBAL(refusedDeliveriesBE);
            int intCount = 0;
            foreach (APPBOK_BookingBE ab in lstRefusedDeliveries)
            {
                #region Hoding actual refusal data in [APPBOK_BookingBE finalRefusedDeliveriesBE] object
                if (intCount == 0)
                {
                    finalRefusedDeliveriesBE.SiteName = ab.SiteName;
                    finalRefusedDeliveriesBE.BookingRef = ab.BookingRef;
                    finalRefusedDeliveriesBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    finalRefusedDeliveriesBE.Vendor.VendorName = ab.Vendor.VendorName;

                    finalRefusedDeliveriesBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    finalRefusedDeliveriesBE.Carrier.CarrierName = ab.Carrier.CarrierName;

                    finalRefusedDeliveriesBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    finalRefusedDeliveriesBE.SlotTime.SlotTime = ab.SlotTime.SlotTime;
                    finalRefusedDeliveriesBE.Status = ab.Status;
                    finalRefusedDeliveriesBE.Vendor.VendorContactEmail = ab.Vendor.VendorContactEmail;
                    finalRefusedDeliveriesBE.DLYREF_ReasonComments = ab.DLYREF_ReasonComments;

                    string reasonForRefusal = String.Empty;
                    if (ab.DLYREF_ReasonArrivedEarly == true)
                        reasonForRefusal = "Arrived Early";

                    if (ab.DLYREF_ReasonArrivedLate == true)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Arrived Late");

                    if (ab.DLYREF_ReasonNoPaperworkOndisplay == true)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "No Paper Work on Display");

                    if (ab.DLYREF_ReasonPalletsDamaged == true)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Pallets Damaged");

                    if (ab.DLYREF_ReasonPackagingDamaged == true)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Packaging Damaged");

                    if (ab.DLYREF_ReasonUnsafeLoad == true)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Unsafe Load");

                    if (ab.DLYREF_ReasonWrongAddress == true)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Wrong Address");

                    if (ab.DLYREF_ReasonRefusedTowait == true)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Refused to Wait");

                    if (ab.DLYREF_ReasonNottoODSpecification == true)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Not to OD Specification");

                    if (ab.DLYREF_ReasonOther == true)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Other");

                    finalRefusedDeliveriesBE.BookingComments = reasonForRefusal;
                    finalRefusedDeliveriesBE.PurchaseOrders = string.Format("{0}     {1}", ab.PurchaseOrders, ab.Prioirty);
                }
                else
                {
                    finalRefusedDeliveriesBE.PurchaseOrders = string.Format("{0}<br />{1}     {2}",
                        finalRefusedDeliveriesBE.PurchaseOrders, ab.PurchaseOrders, ab.Prioirty);
                }
                intCount++;
                #endregion
            }

            // Logic to sending email to Stock Planner.
            stockPlannerEmailBE = new APPBOK_BookingBE();
            stockPlannerEmailBE.Action = "EmailIdsForStockPlanner";
            stockPlannerEmailBE.BookingID = intBookingID;
            List<APPBOK_BookingBE> lstStockPlannerEmail = oAPPBOK_BookingBAL.GetEmailIdsForStockPlannerBAL(stockPlannerEmailBE);
            foreach (APPBOK_BookingBE ab in lstStockPlannerEmail)
            {
                // Here setting default [Stock Planner Email Id and Default Language] if not available in database.
                string toEmail = String.Empty;
                string strLanguage = String.Empty;
                if (!string.IsNullOrWhiteSpace(ab.Vendor.VendorContactEmail))
                {
                    toEmail = ab.Vendor.VendorContactEmail;
                    strLanguage = ab.Language;
                }
                else
                {
                    toEmail = strDefaultEmailId;
                    strLanguage = "English";
                }

                mailBody = this.GetStockPlannerEmailBody(finalRefusedDeliveriesBE, strLanguage);
                clsEmail oclsEmail = new clsEmail();
                oclsEmail.sendMail(toEmail, mailBody, mailSubject, fromAddress, true);
                Page.UICulture = Convert.ToString(Session["CultureInfo"]);
            }
        }
        catch (Exception ex)
        {
            string errorMessage = ex.Message;
        }
    }

    private string GetStockPlannerEmailBody(APPBOK_BookingBE oAPPBOK_BookingBE, string language)
    {
        string htmlBody = string.Empty;
        string templatePath = string.Empty;
        string LanguageFile = string.Empty;

        templatePath = path + templatePathName;
        //LanguageFile = "RefusalLetterToStockPlanner." + language + ".htm";
        //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
        //{
        //    LanguageFile = "RefusalLetterToStockPlanner." + "english.htm";
        //}
        LanguageFile = "RefusalLetterToStockPlanner.english.htm";

        #region Setting reason as per the language ...
        switch (language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            #region mailBody
            htmlBody = sReader.ReadToEnd();

            htmlBody = htmlBody.Replace("{RefusalLetterToSPText1}", WebCommon.getGlobalResourceValue("RefusalLetterToSPText1"));
            htmlBody = htmlBody.Replace("{DeliveryDetailsHistory}", WebCommon.getGlobalResourceValue("DeliveryDetailsHistory"));

            htmlBody = htmlBody.Replace("{ProvisionalBookingMLText1}", WebCommon.getGlobalResourceValue("ProvisionalBookingMLText1"));
            htmlBody = htmlBody.Replace("{ProvisionalBookingMLText1}", WebCommon.getGlobalResourceValue("ProvisionalBookingMLText1"));

            htmlBody = htmlBody.Replace("{Site}", WebCommon.getGlobalResourceValue("Site"));
            htmlBody = htmlBody.Replace("#SiteName#", oAPPBOK_BookingBE.SiteName);

            htmlBody = htmlBody.Replace("{BookingReference}", WebCommon.getGlobalResourceValue("BookingReference"));
            htmlBody = htmlBody.Replace("#BookingReference#", oAPPBOK_BookingBE.BookingRef);

            htmlBody = htmlBody.Replace("{Vendor}", WebCommon.getGlobalResourceValue("Vendor"));
            htmlBody = htmlBody.Replace("#Vendor#", oAPPBOK_BookingBE.Vendor.VendorName);

            htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier"));
            htmlBody = htmlBody.Replace("#Carrier#", oAPPBOK_BookingBE.Carrier.CarrierName);

            htmlBody = htmlBody.Replace("{Bookedinfor}", WebCommon.getGlobalResourceValue("Bookedinfor"));
            htmlBody = htmlBody.Replace("#BookedInFor#", oAPPBOK_BookingBE.SlotTime.SlotTime);

            htmlBody = htmlBody.Replace("{RefusedAt}", WebCommon.getGlobalResourceValue("RefusedAt"));
            htmlBody = htmlBody.Replace("#RefusedAt#", oAPPBOK_BookingBE.Status);

            htmlBody = htmlBody.Replace("{AuthorisedBy}", WebCommon.getGlobalResourceValue("AuthorisedBy"));
            htmlBody = htmlBody.Replace("#AuthorisedBy#", oAPPBOK_BookingBE.Vendor.VendorContactEmail);

            htmlBody = htmlBody.Replace("{PurchaseOrders}", WebCommon.getGlobalResourceValue("PurchaseOrders"));
            htmlBody = htmlBody.Replace("#PurchaseOrders#", oAPPBOK_BookingBE.PurchaseOrders);

            htmlBody = htmlBody.Replace("{ReasonforRefusal}", WebCommon.getGlobalResourceValue("ReasonforRefusal"));
            htmlBody = htmlBody.Replace("#ReasonForRefusal#", oAPPBOK_BookingBE.BookingComments);

            htmlBody = htmlBody.Replace("{CommentLabel}", WebCommon.getGlobalResourceValue("CommentLabel"));
            htmlBody = htmlBody.Replace("#Comment#", oAPPBOK_BookingBE.DLYREF_ReasonComments);
            #endregion
        }
        return htmlBody;
    }
    // Sprint 1 - Point 9 - End

    private void SendmailRefusedAll(List<APPBOK_BookingBE> lstBooking, MASSIT_VendorBE lstVendorDetails, string toAddress, string language)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            


            string htmlBody = string.Empty;
            sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();

            string templatePath = null;
            templatePath = path + templatePathName;

             var communicationBAL = new APPBOK_CommunicationBAL();
            var lstLanguages = communicationBAL.GetLanguages();
            foreach (BusinessEntities.ModuleBE.Languages.Languages.MAS_LanguageBE objLanguage in lstLanguages)
            {
                bool MailSentInLanguage = false;

                string Issues = string.Empty;
                if (chkEarly.Checked)
                    Issues += WebCommon.getGlobalResourceValue("Early", objLanguage.Language) + "<br/>";
                if (chkLate.Checked)
                    Issues += WebCommon.getGlobalResourceValue("Late", objLanguage.Language) + "<br/>";
                if (chkNoPaperworkondisplay.Checked)
                    Issues += WebCommon.getGlobalResourceValue("NoPaperworkondisplay", objLanguage.Language) + "<br/>";
                if (chkNotToOdSpecification.Checked)
                    Issues += WebCommon.getGlobalResourceValue("NotToOdSpecification", objLanguage.Language) + "<br/>";
                if (chkOther.Checked)
                    Issues += WebCommon.getGlobalResourceValue("Other", objLanguage.Language) + "<br/>";
                if (chkPackagingDamaged.Checked)
                    Issues += WebCommon.getGlobalResourceValue("PackagingDamaged", objLanguage.Language) + "<br/>";
                if (chkPalletsDamaged.Checked)
                    Issues += WebCommon.getGlobalResourceValue("PalletsDamaged", objLanguage.Language) + "<br/>";
                if (chkRefusedtowait.Checked)
                    Issues += WebCommon.getGlobalResourceValue("Refusedtowait", objLanguage.Language) + "<br/>";
                if (chkWrongAddress.Checked)
                    Issues += WebCommon.getGlobalResourceValue("WrongAddress", objLanguage.Language) + "<br/>";
                if (chkunsafeload.Checked)
                    Issues += WebCommon.getGlobalResourceValue("unsafeload", objLanguage.Language) + "<br/>";


                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    MailSentInLanguage = true;
                }

                string LanguageFile = "Refused." + objLanguage.Language.ToLower() + ".htm";
                //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
                //{
                //    LanguageFile = "Refused." + "english.htm";
                //}
                LanguageFile = "Refused.english.htm";

                #region Setting reason as per the language ...
                switch (objLanguage.Language)
                {
                    case clsConstants.English:
                        Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                        break;
                    case clsConstants.French:
                        Page.UICulture = clsConstants.FranceISO; // // France
                        break;
                    case clsConstants.German:
                        Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                        break;
                    case clsConstants.Dutch:
                        Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                        break;
                    case clsConstants.Spanish:
                        Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                        break;
                    case clsConstants.Italian:
                        Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                        break;
                    case clsConstants.Czech:
                        Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                        break;
                    default:
                        Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                        break;
                }

                #endregion

                using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
                {
                    #region mailBody
                    htmlBody = sReader.ReadToEnd();
                    //htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
                    htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));

                    htmlBody = htmlBody.Replace("{BookingReference}", WebCommon.getGlobalResourceValue("BookingReference"));
                    htmlBody = htmlBody.Replace("{BookingRefValue}", lstBooking[0].BookingRef);

                    htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));
                    htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy"));

                    htmlBody = htmlBody.Replace("{Site}", WebCommon.getGlobalResourceValue("Site"));
                    htmlBody = htmlBody.Replace("{SiteValue}", lstBooking[0].SiteName);


                    if (lstBooking[0].SupplierType == "V")
                    {
                        htmlBody = htmlBody.Replace("{POs}", WebCommon.getGlobalResourceValue("POs"));
                        htmlBody = htmlBody.Replace("{POsValue}", lstBooking[0].PurchaseOrders.TrimStart(',').TrimEnd(','));
                    }
                    else if (lstBooking[0].SupplierType == "C")
                    {
                        if (lstBooking.Count == 1)
                        {
                            htmlBody = htmlBody.Replace("{POs}", WebCommon.getGlobalResourceValue("POs"));
                            htmlBody = htmlBody.Replace("{POsValue}", lstBooking[0].PurchaseOrders.TrimStart(',').TrimEnd(','));
                        }
                        else
                        {
                            htmlBody = htmlBody.Replace("{POs}", string.Empty);
                            htmlBody = htmlBody.Replace("{POsValue}", string.Empty);
                        }
                    }

                    htmlBody = htmlBody.Replace("{RefusalText1}", WebCommon.getGlobalResourceValue("RefusalText1"));
                    htmlBody = htmlBody.Replace("{IssueValue}", Issues);

                    htmlBody = htmlBody.Replace("{Comments}", WebCommon.getGlobalResourceValue("Comments"));
                    htmlBody = htmlBody.Replace("{CommentsValue}", txtRefusalComments.Text);

                    htmlBody = htmlBody.Replace("{RefusalText2}", WebCommon.getGlobalResourceValue("RefusalText2"));
                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                    #endregion
                }

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                #region Commented Code...
                //clsEmail oclsEmail = new clsEmail();
                //oclsEmail.sendMail(toAddress, htmlBody, "Delivery Refused", sFromAddress, true);

                //// Resending of Delivery mails
                //APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                //oAPPBOK_CommunicationBAL.AddItemBAL(lstBooking[0].BookingID, sFromAddress, toAddress, "Delivery Refused", htmlBody, Convert.ToInt32(Session["UserID"]), CommunicationType.Enum.RefusedAll);
                //// Send email to Stock Planner

                //// Sprint 1 - Point 9 - Begin
                //this.SendEmailToStockPlanner(lstBooking[0].BookingID, language);
                //// Sprint 1 - Point 9 - End
                #endregion

                // Resending of Delivery mails
                clsEmail oclsEmail = new clsEmail();
                string subject = "Delivery Refused";
                CommunicationType.Enum communicationType = CommunicationType.Enum.RefusedAll;
                APPBOK_CommunicationBAL oAPPBOK_AddCommunicationBAL = new APPBOK_CommunicationBAL();
                APPBOK_CommunicationBE oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
                oAPPBOK_CommunicationBE.Action = "AddBookingCommunication";
                oAPPBOK_CommunicationBE.CommunicationType = CommunicationType.Name(communicationType);
                oAPPBOK_CommunicationBE.BookingID = lstBooking[0].BookingID;
                oAPPBOK_CommunicationBE.Subject = subject;
                oAPPBOK_CommunicationBE.SentFrom = sFromAddress;
                oAPPBOK_CommunicationBE.SentTo = toAddress;
                oAPPBOK_CommunicationBE.Body = htmlBody;
                oAPPBOK_CommunicationBE.SendByID = Convert.ToInt32(Session["UserID"]);
                oAPPBOK_CommunicationBE.IsMailSent = MailSentInLanguage;
                oAPPBOK_CommunicationBE.LanguageID = objLanguage.LanguageID;
                oAPPBOK_CommunicationBE.MailSentInLanguage = MailSentInLanguage;
                int intIsExist = oAPPBOK_AddCommunicationBAL.AddBookingCommunicationBAL(oAPPBOK_CommunicationBE) ?? 0;
                if (intIsExist.Equals(0) && MailSentInLanguage.Equals(true))
                {
                    if (!string.IsNullOrWhiteSpace(toAddress))
                        oclsEmail.sendMail(toAddress, htmlBody, subject, sFromAddress, true);

                    // Sprint 1 - Point 9 - Begin
                    //this.SendEmailToStockPlanner(lstBooking[0].BookingID, language);
                    // Sprint 1 - Point 9 - End
                }
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
    }

    #endregion

    #region Events

    public int? BookingDetails(int pPkID)
    {
        int? SiteId = 0;
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "DeliveryArrived";
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(GetQueryStringValue("Scheduledate").ToString());

        List<APPBOK_BookingBE> lstDeliveryArrived = oAPPBOK_BookingBAL.GetBookingDetailsBAL(oAPPBOK_BookingBE);
        if (lstDeliveryArrived != null && lstDeliveryArrived.Count > 0)
        {
            SiteId = lstDeliveryArrived[0].SiteId;
            if (ExtractInformation(GetQueryStringValue("ID").ToString(), "type").ToLower() == "c")
            {
                trCarrier.Style["display"] = "";
                trVendorCarrier.Style["display"] = "none";
                CarrierNameData1.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
            }
            else
            {
                trCarrier.Style["display"] = "none";
                trVendorCarrier.Style["display"] = "";
                VendorNameData.Text = lstDeliveryArrived[0].FixedSlot.Vendor.VendorName.ToString();
                CarrierNameData.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
            }
            BookingDateData.Text = GetQueryStringValue("Scheduledate").ToString();
            BookingTimeData.Text = lstDeliveryArrived[0].ExpectedDeliveryTime.ToString();
            VehicleTypeData.Text = lstDeliveryArrived[0].VehicleType.VehicleType.ToString();
            DoorNumberData.Text = lstDeliveryArrived[0].DoorNoSetup.DoorNumber.ToString();
            if (lstDeliveryArrived.Count == 1)
            {
                BookingLiftsData.Text = lstDeliveryArrived[0].LiftsScheduled.ToString();
                BookingLinesData.Text = lstDeliveryArrived[0].FixedSlot.MaximumLines.ToString();
                BookingPalletsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumPallets.ToString();
                BookingCartonsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumCatrons.ToString();
            }
            else
            {
                int? iLifts = 0, iLines = 0, iPallets = 0, iCartons = 0;
                for (int i = 0; i < lstDeliveryArrived.Count; i++)
                {
                    iLifts += lstDeliveryArrived[i].LiftsScheduled;
                    iLines += lstDeliveryArrived[i].FixedSlot.MaximumLines;
                    iPallets += lstDeliveryArrived[i].FixedSlot.MaximumPallets;
                    iCartons += lstDeliveryArrived[i].FixedSlot.MaximumCatrons;
                }
                BookingLiftsData.Text = iLifts.ToString();
                BookingLinesData.Text = iLines.ToString();
                BookingPalletsData.Text = iPallets.ToString();
                BookingCartonsData.Text = iCartons.ToString();
            }
        }
        return SiteId;
    }

    public string ExtractInformation(string pFullData, string pReturnType)
    {
        int i = -1;
        if (pReturnType == "table")
            i = 0;
        else if (pReturnType == "type")
            i = 1;
        else if (pReturnType == "id")
            i = 2;
        else if (pReturnType == "siteid")
            i = 3;
        return (pFullData.Split('-')[i].ToString());
    }

    public void BindSuperUsers(int SiteId)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "GetDeliveryRefusalUsersBySiteId";
        oSCT_UserBE.IsDeliveryRefusalAuthorization = 1;
        oSCT_UserBE.SiteId = SiteId;
        List<SCT_UserBE> lstSuperUsers = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
        if (lstSuperUsers != null && lstSuperUsers.Count > 0)
        {
            FillControls.FillDropDown(ref ddlUser, lstSuperUsers, "FullName", "UserIDPassword");
        }
    }

    public void UpdateBookingStatus(int pBookingStatusID)
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "UpdateDeliveryArrivedFirstStep";
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.BookingStatusID = pBookingStatusID;
        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        oAPPBOK_BookingBE.FixedSlot.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oAPPBOK_BookingBE.FixedSlot.User.UserID = Convert.ToInt32(Session["UserID"].ToString().Trim());
        if (Session["RefuseDelivery"] != null && Session["RefuseDelivery"].ToString() != "")
        {
            oAPPBOK_BookingBE.DLYARR_OperatorInital = Session["RefuseDelivery"].ToString().Trim();
        }
        if (Session["TransactionComments"] != null && Session["TransactionComments"].ToString() != "")
        {
            oAPPBOK_BookingBE.TransactionComments = Session["TransactionComments"].ToString().Trim();
        }
        oAPPBOK_BookingBAL.addEditBookingDetailsBAL(oAPPBOK_BookingBE);
        Session["RefuseDelivery"] = null;
        Session.Remove("TransactionComments");
    }

    #endregion

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        updInfromation_Messge_update.Hide();
       
        //UPDATE THE BOOKING STATUS ID HERE
        UpdateBookingStatus(Convert.ToInt16(Common.BookingStatus.RefusedDelivery));

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "UpdateDeliveryRefusal";
        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        oAPPBOK_BookingBE.DLYREF_ReasonArrivedEarly = chkEarly.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonArrivedLate = chkLate.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonNoPaperworkOndisplay = chkNoPaperworkondisplay.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonPalletsDamaged = chkPalletsDamaged.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonPackagingDamaged = chkPackagingDamaged.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonWrongAddress = chkWrongAddress.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonRefusedTowait = chkRefusedtowait.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonNottoODSpecification = chkNotToOdSpecification.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonOther = chkOther.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonUnsafeLoad = chkunsafeload.Checked;
        oAPPBOK_BookingBE.DLYREF_ReasonComments = txtRefusalComments.Text;
        oAPPBOK_BookingBE.DLYREF_SupervisorID = Convert.ToInt32(ddlUser.SelectedItem.Value.Split('-')[0]);
        oAPPBOK_BookingBAL.addEditDeliveryRefusalBAL(oAPPBOK_BookingBE);

        List<APPBOK_BookingBE> lstBooking = new List<APPBOK_BookingBE>();
        try
        {
            if (chkEarly.Checked || chkLate.Checked || chkNoPaperworkondisplay.Checked || chkNotToOdSpecification.Checked ||
               chkOther.Checked || chkPackagingDamaged.Checked || chkPalletsDamaged.Checked || chkRefusedtowait.Checked ||
               chkWrongAddress.Checked || chkunsafeload.Checked)
            {

                if (GetQueryStringValue("IsFromUDC") != null)
                {
                    if (GetQueryStringValue("IsFromUDC") == "true")
                    {
                        oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID_Unexpected";
                    }
                    else
                    {
                        oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
                    }
                }
                else
                {
                    oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
                }
                oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));
                lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);
                if (lstBooking.Count > 0)
                {
                    MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
                    APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
                    List<MASSIT_VendorBE> lstVendorDetails = null;


                    // Sprint 1 - Point 9 - Begin
                    if (lstBooking[0].BookingID > 0 && lstBooking[0].BookingID != 0)
                    {
                        this.SendEmailToStockPlanner(lstBooking[0].BookingID);
                    }
                    // Sprint 1 - Point 9 - End


                    if (lstBooking[0].SupplierType == "C")
                    {
                        oMASSIT_VendorBE.Action = "GetCarriersVendor";
                        oMASSIT_VendorBE.BookingID = oAPPBOK_BookingBE.BookingID;

                        lstVendorDetails = oAPPSIT_VendorBAL.GetCarriersVendorIDBAL(oMASSIT_VendorBE);

                        ViewState["lstVendorDetails"] = lstVendorDetails;
                        ViewState["lstBooking"] = lstBooking;

                        ShowInformation();


                    }
                    else if (lstBooking[0].SupplierType == "V")
                    {
                        string VendorEmails = string.Empty;
                        string[] sentTo;
                        string[] language;
                        string[] VendorData = new string[2];
                        string[] CarrierData = new string[2];

                        VendorData = CommonPage.GetVendorEmailsWithLanguage(lstBooking[0].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                        {
                            sentTo = VendorData[0].Split(',');
                            language = VendorData[1].Split(',');
                            for (int j = 0; j < sentTo.Length; j++)
                            {
                                if (sentTo[j] != " ")
                                {
                                    if (GetQueryStringValue("SlotTime") != null)
                                        Sendmail(lstBooking, null, sentTo[j], language[j]);
                                    else
                                    {
                                        SendmailRefusedAll(lstBooking, null, sentTo[j], language[j]);
                                        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (GetQueryStringValue("SlotTime") != null)
                                Sendmail(lstBooking, null, ConfigurationManager.AppSettings["DefaultEmailAddress"], "English");
                            else
                            {
                                SendmailRefusedAll(lstBooking, null, ConfigurationManager.AppSettings["DefaultEmailAddress"], "English");
                                Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                            }
                        }
                    }


                }
            }
        }
        catch { }

        ///////////////////////////////////
        if (lstBooking.Count == 0 || lstBooking[0].SupplierType == "V")
            EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
    }

    protected void btnCancel_Click1(object sender, EventArgs e)
    {
        updInfromation_Messge_update.Hide();       
    }
}
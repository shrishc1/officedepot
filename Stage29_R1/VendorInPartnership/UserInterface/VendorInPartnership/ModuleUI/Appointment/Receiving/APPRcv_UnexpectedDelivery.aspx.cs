﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.IO;
using System.Configuration;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Data;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System.Collections.Specialized;
using BusinessEntities.ModuleBE.OnTimeInFull.CountrySetting;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.CountrySetting;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Languages.Languages;


public partial class APPRcv_UnexpectedDelivery : CommonPage {
    string strPurchaseOrderExist = WebCommon.getGlobalResourceValue("PurchaseOrderExist");
    string strDefaultEmailId = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];
    #region PROPERTIES

    public string Lines {
        get {
            return ViewState["Lines"] != null ? ViewState["Lines"].ToString() : "XX";
        }
        set {
            ViewState["Lines"] = value;
        }
    }

    public string sAddedPurchaseOrders {
        get {
            return ViewState["vw_sAddedPurchaseOrders"] != null ? ViewState["vw_sAddedPurchaseOrders"].ToString() : string.Empty;
        }
        set {
            ViewState["vw_sAddedPurchaseOrders"] = value;
        }
    }

    public string AlreadyExistsPOs {
        get {
            return ViewState["AlreadyExistsPOs"] != null ? Convert.ToString(ViewState["AlreadyExistsPOs"]) : string.Empty;
        }
        set {
            ViewState["AlreadyExistsPOs"] = value;
        }
    }

    public bool? IsBookingValidationExcluded {
        get {
            return (Convert.ToBoolean(IsBookingValidationExcludedVendor) || Convert.ToBoolean(IsBookingValidationExcludedDelivery));
        }
        set {
            ViewState["IsBookingValidationExcluded"] = value;
        }
    }

    public bool? IsBookingValidationExcludedVendor {
        get {
            return ViewState["IsBookingValidationExcludedVendor"] != null ? Convert.ToBoolean(ViewState["IsBookingValidationExcludedVendor"]) : false;
        }
        set {
            ViewState["IsBookingValidationExcludedVendor"] = value;
        }
    }

    public bool? IsBookingValidationExcludedDelivery {
        get {
            return ViewState["IsBookingValidationExcludedDelivery"] != null ? Convert.ToBoolean(ViewState["IsBookingValidationExcludedDelivery"]) : false;
        }
        set {
            ViewState["IsBookingValidationExcludedDelivery"] = value;
        }
    }

    public string ucSeacrhVendorNo {
        get {
            return ViewState["vw_ucSeacrhVendorNo"] != null ? ViewState["vw_ucSeacrhVendorNo"].ToString() : string.Empty;
        }
        set {
            ViewState["vw_ucSeacrhVendorNo"] = value;
        }
    }

    public string ucSeacrhVendorName {
        get {
            return ViewState["vw_ucSeacrhVendorName"] != null ? ViewState["vw_ucSeacrhVendorName"].ToString() : string.Empty;
        }
        set {
            ViewState["vw_ucSeacrhVendorName"] = value;
        }
    }

    public string ucSeacrhVendorID {
        get {
            return ViewState["ucSeacrhVendorID"] != null ? ViewState["ucSeacrhVendorID"].ToString() : string.Empty;
        }
        set {
            ViewState["ucSeacrhVendorID"] = value;
        }
    }

    public string ucSeacrhVendorParentID {
        get {
            return ViewState["ucSeacrhVendorParentID"] != null ? ViewState["ucSeacrhVendorParentID"].ToString() : string.Empty;
        }
        set {
            ViewState["ucSeacrhVendorParentID"] = value;
        }
    }

    public DateTime ActualSchedulDateTime {
        get {
            return ViewState["ActualSchedulDateTime"] != null ? Convert.ToDateTime(ViewState["ActualSchedulDateTime"].ToString()) : new DateTime(1900, 01, 01, 0, 0, 00);
        }
        set {
            ViewState["ActualSchedulDateTime"] = value;
        }
    }

    //---------------------Start Phase 14 R2 Pint 21--------------//
    public bool? IsBookingConnected {
        get {
            return ViewState["IsBookingConnected"] != null ? Convert.ToBoolean(ViewState["IsBookingConnected"]) : false;
        }
        set {
            ViewState["IsBookingConnected"] = value;
        }
    }

    public int ConnectedBookingID {
        get {
            return ViewState["ConnectedBookingID"] != null ? Convert.ToInt32(ViewState["ConnectedBookingID"]) : -1;
        }
        set {
            ViewState["ConnectedBookingID"] = value;
        }
    }
    //---------------------End Phase 14 R2 Pint 21--------------//

    #endregion

    int siteCountryId = 0;

    protected void Page_InIt(object sender, EventArgs e) {
        ucSearchVendor1.CurrentPage = this;
        ucSearchVendor1.IsParentRequired = true;
        ucSearchVendor1.IsStandAloneRequired = true;
        ucSearchVendor1.IsChildRequired = true;
        ucSearchVendor1.IsGrandParentRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e) {
        if (!string.IsNullOrWhiteSpace(GetQueryStringValue("SiteID"))) {
            ucSearchVendor1.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
            //SetVendorDetailsByUserID(ucSearchVendor1.SiteID);
        }

        // Comming query string data...
        //GetQueryStringValue("SiteCountryID")
        //GetQueryStringValue("SiteName")

        // Sprint 1 - Point 8 - Start
        if (!IsPostBack) {
            SCT_UserBAL UserBAL = new SCT_UserBAL();
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            oSCT_UserBE.Action = "GetManageDeliveryUsersBySiteId";
            oSCT_UserBE.SiteId = ucSearchVendor1.SiteID;
            List<SCT_UserBE> lstUsers = UserBAL.GetManageDeliveryUsersBAL(oSCT_UserBE);

            ddlOperators.DataSource = lstUsers;
            FillControls.FillDropDown(ref ddlOperators, lstUsers, "FullName", "LoginID");
            ddlOperators.DataBind();
            ddlOperators.SelectedValue = Convert.ToString(Session["LoginID"]);
            hdnLoggedInUserId.Value = Convert.ToString(Session["LoginID"]);

            if (GetQueryStringValue("SiteID") != null) {
                if (Convert.ToInt32(GetQueryStringValue("SiteID")) != 0) {
                    MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(GetQueryStringValue("SiteID")));
                    hdnIsUnexpDelPassAllow.Value = singleSiteSetting.UnexpectedDelivery.Trim().ToUpper();
                    ltSiteName.Text = " - " + singleSiteSetting.SiteName;
                }
            }

            this.BindCarrier();
        }

        if (!ddlOperators.SelectedValue.Equals(Convert.ToString(Session["LoginID"]))
            && hdnIsUnexpDelPassAllow.Value == "Y") {
            tblPassword.Style.Remove("display");
            rfvOperatorPasswordRequired.Enabled = true;
        }
        else {
            tblPassword.Style.Add("display", "none");
            rfvOperatorPasswordRequired.Enabled = false;
        }
        // Sprint 1 - Point 8 - End

        if (!string.IsNullOrEmpty(GetQueryStringValue("BookingID"))) {
            btnVendor_Click(null, null);
            ConnectToBooking(GetQueryStringValue("BookingID"),true);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
            //ltCurrentDateTime.Text = System.DateTime.Now.ToString("dd/MM/yyyy") + "  @" + System.DateTime.Now.Hour.ToString() + ":" + System.DateTime.Now.ToString("mm");
            ActualSchedulDateTime = Utilities.Common.TextToDateFormat(System.DateTime.Now.ToString("dd/MM/yyyy"));
        }

        // Logic to clear the PO grid when user will change the Vendor.
        if (gvPO.Rows.Count > 0) {
            DataTable myDataTable = null;
            if (ViewState["myDataTable"] != null) {
                myDataTable = (DataTable)ViewState["myDataTable"];
                //DataTable dt = myDataTable.Clone();
                string strVenmdorName = Convert.ToString(myDataTable.Rows[0]["VendorName"]);
                if (!GetSelectedVenderName().Trim().ToUpper().Equals(strVenmdorName.Trim().ToUpper())) {
                    gvPO.DataSource = myDataTable.Clone();
                    gvPO.DataBind();
                    myDataTable = null;
                    ViewState["myDataTable"] = null;
                }
            }
        }
    }

    protected void btnUnknownVendor_Click(object sender, EventArgs e) {

        tdVendorControl.Attributes.Add("style", "display:none");
        tdUnknownVendor.Attributes.Add("style", "display:block");

    }

    protected void btnAccept_Click(object sender, EventArgs e) {
        // Sprint 1 - Point 8 - Apply a check to validate operator
        /* This point commented on client requirement Date : 23 July 2013 */
        /*if (ValidateOperator())
        {*/
        MakeBooking(2); // Delivery Arrived
        /*}
        else
        {
            string PasswordExp = WebCommon.getGlobalResourceValue("PasswordExp");
            ScriptManager.RegisterStartupScript(this.txtPassword, this.txtPassword.GetType(), "alert1", "alert('" + PasswordExp + "')", true);
            return;
        }*/
    }

    protected void btnReject_Click(object sender, EventArgs e) {
        // Sprint 1 - Point 8 - Apply a check to validate operator
        /* This point commented on client requirement Date : 23 July 2013 */
        /*if (ValidateOperator())
        {*/

        if (IsBookingConnected == false) {
            MakeBooking(4); // Refused Delivery    
        }
        else {

            MakeBooking(4); // Refused Delivery 

            PKID = Convert.ToInt32(Session["UnexpectedDeliveryPKID"]);
            UserID = Convert.ToInt32(Session["UserID"]);
            MethodInvoker simpleDelegate = new MethodInvoker(SendMail);

            simpleDelegate.BeginInvoke(4, null, null);
            EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
        }      
        /*}
        else
        {
            string PasswordExp = WebCommon.getGlobalResourceValue("PasswordExp");
            ScriptManager.RegisterStartupScript(this.txtPassword, this.txtPassword.GetType(), "alert1", "alert('" + PasswordExp + "')", true);
            return;
        }*/
    }

    // Sprint 1 - Point 8 - Start
    private bool ValidateOperator() {
        bool IsValidUser = true;
        if (!Convert.ToString(Session["LoginID"]).Equals(ddlOperators.SelectedValue)
            && hdnIsUnexpDelPassAllow.Value == "Y") {
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

            oSCT_UserBE.Action = "GetDetails";
            oSCT_UserBE.LoginID = ddlOperators.SelectedValue;
            oSCT_UserBE.Password = txtPassword.Text.Trim();

            List<SCT_UserBE> lstSCT_UserBE = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);

            if (lstSCT_UserBE.Count <= 0) {
                IsValidUser = false;
            }
        }
        return IsValidUser;
    }
    // Sprint 1 - Point 8 - End

    int PKID = 0;
    int UserID = 0;
    public delegate void MethodInvoker(int BookingStatusID);

    private void MakeBooking(int BookingStatusID) {

        List<UP_VendorBE> oUP_VendorBEList = ucSearchVendor1.IsValidVendorCode();
        string sValidVendorCode = WebCommon.getGlobalResourceValue("ValidVendorCode");

        if (oUP_VendorBEList.Count == 0) {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + sValidVendorCode + "')", true);
            return;
        }
        else {

            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

            oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
            oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
            oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
            oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

            //adding unknown vendor to database and assigning new vendor id to viewstate.
            if (!string.IsNullOrEmpty(txtUnknownVendor.Text)) {
                oAPPBOK_BookingBE.Action = "AddUnknownVendor";
                oAPPBOK_BookingBE.FixedSlot.Vendor.VendorName = txtUnknownVendor.Text.Trim();
                ViewState["NewVendorId"] = Convert.ToInt32(oAPPBOK_BookingBAL.addEditUnexpectedDeliveryBAL(oAPPBOK_BookingBE));
            }
            
            oAPPBOK_BookingBE.FixedSlot.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());

            if (!string.IsNullOrEmpty(ucSearchVendor1.VendorNo.Trim()))
                oAPPBOK_BookingBE.FixedSlot.Vendor.VendorID = Convert.ToInt32(ucSearchVendor1.VendorNo.Trim());
            else
                oAPPBOK_BookingBE.FixedSlot.Vendor.VendorID = Convert.ToInt32(ViewState["NewVendorId"]);


            oAPPBOK_BookingBE.ScheduleDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            oAPPBOK_BookingBE.FixedSlot.SlotTimeID = GetTimeSlotID();
            oAPPBOK_BookingBE.NumberOfPallet = !string.IsNullOrWhiteSpace(txtPallets.Text) ? Convert.ToInt32(txtPallets.Text.Trim()) : (int?)null;
            oAPPBOK_BookingBE.NumberOfCartons = !string.IsNullOrWhiteSpace(txtCartons.Text) ? Convert.ToInt32(txtCartons.Text.Trim()) : (int?)null;
            if (validationFunctions.IsNumeric(txtLines.Text))
                oAPPBOK_BookingBE.NumberOfLines = !string.IsNullOrWhiteSpace(txtLines.Text) ? Convert.ToInt32(txtLines.Text.Trim()) : (int?)null;

            // Sprint 1 - Point 8 
            oAPPBOK_BookingBE.DLYARR_OperatorInital = ddlOperators.SelectedItem.Text.Trim();

            oAPPBOK_BookingBE.SupplierType = "V";
            oAPPBOK_BookingBE.PreAdviseNotification = "Y";
            oAPPBOK_BookingBE.BookingTypeID = 2; //Unscheduled
            oAPPBOK_BookingBE.BookingStatusID = BookingStatusID;
            oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
            oAPPBOK_BookingBE.WeekDay = getWeekdayNo();

            //PO Details
            if (!string.IsNullOrEmpty(ucSearchVendor1.VendorNo.Trim())) {
                oAPPBOK_BookingBE.VendorID = Convert.ToInt32(ucSearchVendor1.VendorNo); // Convert.ToInt32(ucSeacrhVendor1.VendorNo);
            }
            else {
                oAPPBOK_BookingBE.VendorID = Convert.ToInt32(ViewState["NewVendorId"]);
            }
            DataTable myDataTable = null;
            if (ViewState["myDataTable"] != null)
                myDataTable = (DataTable)ViewState["myDataTable"];

            string strPoIds = string.Empty;
            string strPoNos = string.Empty;
            if (myDataTable != null) {
                for (int iCount = 0; iCount < myDataTable.Rows.Count; iCount++) {
                    if (strPoIds == string.Empty) {
                        strPoIds = myDataTable.Rows[iCount]["PurchaseOrderID"].ToString();
                        strPoNos = myDataTable.Rows[iCount]["PurchaseNumber"].ToString();
                    }
                    else {
                        strPoIds = strPoIds + "," + myDataTable.Rows[iCount]["PurchaseOrderID"].ToString();
                        strPoNos = strPoNos + "," + myDataTable.Rows[iCount]["PurchaseNumber"].ToString();
                    }
                }
            }
            oAPPBOK_BookingBE.PurchaseOrdersIDs = strPoIds;
            oAPPBOK_BookingBE.PurchaseOrders = strPoNos;

            oAPPBOK_BookingBE.TransactionComments = txtComments.Text;
            if (ddlCarrier.Items.Count > 0)
                if (ddlCarrier.SelectedIndex > 0)
                    oAPPBOK_BookingBE.VendorCarrierID = Convert.ToInt32(ddlCarrier.SelectedValue);

            oAPPBOK_BookingBE.Action = "CheckUnexpectedDelivery";
            if (Convert.ToInt32(oAPPBOK_BookingBAL.addEditUnexpectedDeliveryBAL(oAPPBOK_BookingBE)) == 1) {
                if (BookingStatusID == 2) {
                    EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
                }
                else if (BookingStatusID == 4) {
                    EncryptQueryString("~/ModuleUI/Appointment/Receiving/APPRcv_DeliveryRefusal.aspx?Scheduledate="
                        + oAPPBOK_BookingBE.ScheduleDate.Value.Day + "/"
                        + oAPPBOK_BookingBE.ScheduleDate.Value.Month + "/"
                        + oAPPBOK_BookingBE.ScheduleDate.Value.Year + "&ID=BK-V-" + Session["UnexpectedDeliveryPKID"].ToString()
                        + "-" + GetQueryStringValue("SiteID").ToString() + "&SlotTime="
                        + ViewState["sFinalSlotTime"].ToString()
                        + "&IsDelArriPassAllow=" + hdnIsUnexpDelPassAllow.Value);
                }
                return;
            }


            if (IsBookingConnected == true && BookingStatusID == 2) {
                oAPPBOK_BookingBE.Action = "LinkUnexpectedDelivery";
                oAPPBOK_BookingBE.BookingID = ConnectedBookingID;
            }
            else {
                oAPPBOK_BookingBE.Action = "InsertUnexpectedDelivery";
                if (IsBookingConnected == true) {
                    oAPPBOK_BookingBE.BookingID = ConnectedBookingID;
                }
                else { oAPPBOK_BookingBE.BookingID = -1; }
            }

            PKID = Convert.ToInt32(oAPPBOK_BookingBAL.addEditUnexpectedDeliveryBAL(oAPPBOK_BookingBE));

            Session["UnexpectedDeliveryPKID"] = PKID.ToString();

            // Sprint 1 - Point 8 - Start
            Session.Remove("TransactionComments");

            MethodInvoker simpleDelegate = new MethodInvoker(SendMail);
            UserID = Convert.ToInt32(Session["UserID"]);
            if (BookingStatusID == 2) {

                EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");

                // Calling SendMails Async
                simpleDelegate.BeginInvoke(BookingStatusID, null, null);

                //SendMail();

            }
            else if (BookingStatusID == 4) {
                EncryptQueryString("~/ModuleUI/Appointment/Receiving/APPRcv_DeliveryRefusal.aspx?Scheduledate="
                    + oAPPBOK_BookingBE.ScheduleDate.Value.Day + "/"
                    + oAPPBOK_BookingBE.ScheduleDate.Value.Month + "/"
                    + oAPPBOK_BookingBE.ScheduleDate.Value.Year + "&ID=BK-V-" + PKID.ToString()
                    + "-" + GetQueryStringValue("SiteID").ToString() + "&SlotTime="
                    + ViewState["sFinalSlotTime"].ToString()
                    + "&IsDelArriPassAllow=" + hdnIsUnexpDelPassAllow.Value);

                if (IsBookingConnected == true)
                    simpleDelegate.BeginInvoke(BookingStatusID, null, null);
            }
        }
    }

    private void SendMail(int BookingStatusID) {
        //Send Mail
        try {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

            oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
            oAPPBOK_BookingBE.BookingID = PKID;
            List<APPBOK_BookingBE> lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);

            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
            string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
            string templatePathName = @"/EmailTemplates/Appointment/";

            string templatePath = null;
            templatePath = path + templatePathName;

            string LanguageFile = string.Empty;

            if (BookingStatusID == 2) {
                if (IsBookingConnected == true)
                    LanguageFile = "LinkToUnexpectedAccept.english.htm";
                else
                    LanguageFile = "UnexpectedAccept.english.htm";
            }
            else if (BookingStatusID == 4) {
                LanguageFile = "LinkToUnexpectedReject.english.htm";
            }

            if (lstBooking.Count > 0) {

                MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
                APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
                string[] sentTo;
                string[] language;
                string[] VendorData = new string[2];
                bool isEmailSent = false;
                //Getting Vendor Email Ids.
                VendorData = CommonPage.GetVendorEmailsWithLanguage(lstBooking[0].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                sentTo = VendorData[0].Split(',');
                language = VendorData[1].Split(',');
                for (int j = 0; j < sentTo.Length; j++) {
                    if (!string.IsNullOrEmpty(sentTo[j].Trim()) && !string.IsNullOrWhiteSpace(sentTo[j].Trim())) {
                        SendmailToVendor(lstBooking, sentTo[j], language[j], oLanguages, templatePath, LanguageFile, BookingStatusID);
                        isEmailSent = true;
                    }
                }

                if (!isEmailSent) {
                    SendmailToVendor(lstBooking, strDefaultEmailId, "English", oLanguages, templatePath, LanguageFile, BookingStatusID);
                }

                //sending mail to Stock Planner (Phase 25 R1)

                oAPPBOK_BookingBE.Action = "GetUnexpectedBookingDataForStockPlanner";
                oAPPBOK_BookingBE.BookingID = PKID;
                List<APPBOK_BookingBE> lstUnexpectedBookingForSP = oAPPBOK_BookingBAL.GetUnexpectedBookingDataForStockPlannerBAL(oAPPBOK_BookingBE);

                if (lstUnexpectedBookingForSP.Count > 0)
                {
                    SendmailToStockPlanner(PKID, lstUnexpectedBookingForSP);
                }
            }
        }
        catch { }
    }

    private void SendmailToVendor(List<APPBOK_BookingBE> lstBooking, string toAddress, string language, List<MAS_LanguageBE> oLanguages, string templatePath, string LanguageFile, int BookingStatusID) {
        if (!string.IsNullOrEmpty(toAddress)) {

            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            //List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
            clsEmail oclsEmail = new clsEmail();

            foreach (MAS_LanguageBE objLanguage in oLanguages) {
                bool MailSentInLanguage = false;

                if (objLanguage.Language.ToLower() == language.ToLower()) {
                    MailSentInLanguage = true;
                }

                //string LanguageFile = "UnexpectedAccept." + language + ".htm";
                //if (!System.IO.File.Exists(templatePath + LanguageFile.ToLower()))
                //{
                //    LanguageFile = "UnexpectedAccept." + "english.htm";
                //}


                //#region Setting reason as per the language ...
                //switch (objLanguage.Language)
                //{
                //    case clsConstants.English:
                //        Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                //        break;
                //    case clsConstants.French:
                //        Page.UICulture = clsConstants.FranceISO; // // France
                //        break;
                //    case clsConstants.German:
                //        Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                //        break;
                //    case clsConstants.Dutch:
                //        Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                //        break;
                //    case clsConstants.Spanish:
                //        Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                //        break;
                //    case clsConstants.Italian:
                //        Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                //        break;
                //    case clsConstants.Czech:
                //        Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                //        break;
                //    default:
                //        Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                //        break;
                //}
                //#endregion

                string htmlBody = string.Empty;

                using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower())) {
                    #region mailBody
                    htmlBody = sReader.ReadToEnd();
                    // htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                    htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam", objLanguage.Language));

                    if (IsBookingConnected == true) {
                        if (BookingStatusID == 2) {
                            htmlBody = htmlBody.Replace("{LinkToUnexpectedAcceptText1}", WebCommon.getGlobalResourceValue("LinkToUnexpectedAcceptText1", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{LinkToUnexpectedAcceptText2}", WebCommon.getGlobalResourceValue("LinkToUnexpectedAcceptText2", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{LinkToUnexpectedAcceptText3}", WebCommon.getGlobalResourceValue("LinkToUnexpectedAcceptText3", objLanguage.Language));
                        }
                        else if (BookingStatusID == 4) {
                            htmlBody = htmlBody.Replace("{LinkToUnexpectedRefusedText1}", WebCommon.getGlobalResourceValue("LinkToUnexpectedRefusedText1", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{LinkToUnexpectedRefusedText2}", WebCommon.getGlobalResourceValue("LinkToUnexpectedRefusedText2", objLanguage.Language));
                        }

                        htmlBody = htmlBody.Replace("{TimeValue}", lstBooking[0].SlotTime.SlotTime.ToString());
                    }
                    else {
                        htmlBody = htmlBody.Replace("{UnexpectedAcceptText1}", WebCommon.getGlobalResourceValue("UnexpectedAcceptText1", objLanguage.Language));
                        htmlBody = htmlBody.Replace("{UnexpectedAcceptText2}", WebCommon.getGlobalResourceValue("UnexpectedAcceptText2", objLanguage.Language));
                        htmlBody = htmlBody.Replace("{UnexpectedAcceptText3}", WebCommon.getGlobalResourceValue("UnexpectedAcceptText3", objLanguage.Language));

                        htmlBody = htmlBody.Replace("{TimeValue}", ViewState["sFinalSlotTime"].ToString());
                    }

                    htmlBody = htmlBody.Replace("{BookingRef}", lstBooking[0].BookingRef);

                    htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy"));

                    htmlBody = htmlBody.Replace("{Time}", WebCommon.getGlobalResourceValue("Time", objLanguage.Language));
                    

                    htmlBody = htmlBody.Replace("{Site}", WebCommon.getGlobalResourceValue("Site", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{SiteValue}", lstBooking[0].SiteName);
                   

                    htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{CarrierValue}", lstBooking[0].Carrier.CarrierName);

                    htmlBody = htmlBody.Replace("{NumberofPallets}", WebCommon.getGlobalResourceValue("NumberofPallets", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{NumberofPalletsValue}", lstBooking[0].NumberOfPallet.ToString());

                    htmlBody = htmlBody.Replace("{NumberofCartons}", WebCommon.getGlobalResourceValue("NumberofCartons", objLanguage.Language));
                    htmlBody = htmlBody.Replace("{NumberofCartonsValue}", lstBooking[0].NumberOfCartons.ToString());

                    //Stage6 V2 point 2
                    htmlBody = htmlBody.Replace("{PO}", WebCommon.getGlobalResourceValue("PO", objLanguage.Language));
                    if (!string.IsNullOrEmpty(lstBooking[0].PurchaseOrders.Trim()))
                        htmlBody = htmlBody.Replace("{POValue}", lstBooking[0].PurchaseOrders.TrimEnd(',').TrimStart(','));
                    else
                        htmlBody = htmlBody.Replace("{POValue}", "-");

                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully", objLanguage.Language));

                    #endregion
                }


                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);


                if (objLanguage.Language == language) {
                    oclsEmail.sendMail(toAddress, htmlBody, "Unexpected Booking - Please investigate", sFromAddress, true);
                }
                // Resending of Delivery mails

                if (IsBookingConnected == true) {
                    if (BookingStatusID == 2) {
                        oAPPBOK_CommunicationBAL.AddItemBAL(lstBooking[0].BookingID, sFromAddress, toAddress, "Unexpected Booking - Please investigate", htmlBody, UserID, CommunicationType.Enum.UnexpectedAccepted, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);
                    }
                    else if (BookingStatusID == 4) {
                        oAPPBOK_CommunicationBAL.AddItemBAL(lstBooking[0].BookingID, sFromAddress, toAddress, "Unexpected Booking - Please investigate", htmlBody, UserID, CommunicationType.Enum.UnexpectedRefused, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);
                    }
                }
                else {
                    oAPPBOK_CommunicationBAL.AddItemBAL(lstBooking[0].BookingID, sFromAddress, toAddress, "Unexpected Booking - Please investigate", htmlBody, UserID, CommunicationType.Enum.UnexpectedAccepted, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);
                }
            }
            //Page.UICulture = Convert.ToString(Session["CultureInfo"]);
            //clsEmail mail = new clsEmail();

            //string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
            //mail.toAdd(toAddress);
            //mail.EmailMessageText = htmlBody;
            //mail.EmailSubjectText = "Unexpected Booking - Please investigate";
            //mail.FromEmail = sFromAddress;
            //mail.FromName = "Office Depot - Do not reply.";
            //mail.IsHTMLMail = true;
            //mail.SendMail();
        }
    }

    private void SendmailToStockPlanner(int BookingID, List<APPBOK_BookingBE> lstBooking)
    {

        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        string mailBody = string.Empty;
        string templatePath = string.Empty;
        string LanguageFile = string.Empty;


        string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
        string templatePathName = @"/EmailTemplates/Appointment/";

        templatePath = path + templatePathName;
        LanguageFile = "UnexpectedBookingLetterToSP.english.htm";

        var stockPlannerEmailBE = new APPBOK_BookingBE();
        stockPlannerEmailBE.Action = "GetEmailIdsForStockPlannerUnexpectedBooking";
        stockPlannerEmailBE.BookingID = BookingID;
        List<APPBOK_BookingBE> lstStockPlannerEmail = oAPPBOK_BookingBAL.GetEmailIdsForStockPlannerUnexpectedBookingBAL(stockPlannerEmailBE);

        lstStockPlannerEmail = lstStockPlannerEmail.Where(x => !string.IsNullOrEmpty(x.StockPlannerEmail)).ToList();

        //foreach (APPBOK_BookingBE ab in lstStockPlannerEmail)
        //{

        if (lstStockPlannerEmail.Count > 0)
        {
            string toEmail = String.Empty;
            string strLanguage = String.Empty;
            string[] StockPlannerData = new string[2];
            string[] sentTo;
            string[] language;



            foreach (var item in lstStockPlannerEmail)
            {
                if (!string.IsNullOrEmpty(item.StockPlannerEmail))
                {
                    StockPlannerData[0] += item.StockPlannerEmail.ToString() + ",";
                    StockPlannerData[1] += item.Language.ToString() + ",";
                }
            }

            if (StockPlannerData[0] != null)
            {
                sentTo = StockPlannerData[0].Split(',');
                language = StockPlannerData[1].Split(',');

                sentTo = sentTo.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                language = language.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            }
            else
            {
                sentTo = null;
                language = null;
            }

            clsEmail oclsEmail = new clsEmail();

            var communicationBAL = new APPBOK_CommunicationBAL();
            var lstLanguages = communicationBAL.GetLanguages();

            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            //List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

            if (sentTo != null)
            {
                for (int j = 0; j < sentTo.Length; j++)
                {
                    if (!string.IsNullOrEmpty(sentTo[j].Trim()) && !string.IsNullOrWhiteSpace(sentTo[j].Trim()))
                    {
                        foreach (MAS_LanguageBE objLanguage in lstLanguages)
                        {
                            bool MailSentInLanguage = false;

                            if (objLanguage.Language.ToLower() == language[j].Trim().ToLower())
                            {
                                MailSentInLanguage = true;
                            }

                            string htmlBody = string.Empty;

                            using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
                            {
                                #region mailBody

                                htmlBody = sReader.ReadToEnd();

                                htmlBody = htmlBody.Replace("{Hi}", WebCommon.getGlobalResourceValue("Hi", objLanguage.Language));
                                htmlBody = htmlBody.Replace("{VendorNameValue}", lstBooking[0].Vendor.VendorName);
                                //}

                                htmlBody = htmlBody.Replace("{UnexpectedBookingToSPText1}", WebCommon.getGlobalResourceValue("UnexpectedBookingToSPText1", objLanguage.Language));
                                htmlBody = htmlBody.Replace("{UnexpectedBookingToSPText2}", WebCommon.getGlobalResourceValue("UnexpectedBookingToSPText2", objLanguage.Language));


                                htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date", objLanguage.Language));
                                htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy"));

                                htmlBody = htmlBody.Replace("{Time}", WebCommon.getGlobalResourceValue("Time", objLanguage.Language));
                                htmlBody = htmlBody.Replace("{TimeValue}", lstBooking[0].SlotTime.SlotTime.ToString());


                                htmlBody = htmlBody.Replace("{Site}", WebCommon.getGlobalResourceValue("Site", objLanguage.Language));
                                htmlBody = htmlBody.Replace("{SiteValue}", lstBooking[0].SiteName);


                                htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier", objLanguage.Language));
                                htmlBody = htmlBody.Replace("{CarrierValue}", lstBooking[0].Carrier.CarrierName);

                                htmlBody = htmlBody.Replace("{Pallets}", WebCommon.getGlobalResourceValue("Pallets", objLanguage.Language));
                                htmlBody = htmlBody.Replace("{NumberofPalletsValue}", lstBooking[0].NumberOfPallet.ToString());

                                htmlBody = htmlBody.Replace("{Cartons}", WebCommon.getGlobalResourceValue("Cartons", objLanguage.Language));
                                htmlBody = htmlBody.Replace("{NumberofCartonsValue}", lstBooking[0].NumberOfCartons.ToString());


                                htmlBody = htmlBody.Replace("{PO}", WebCommon.getGlobalResourceValue("PO", objLanguage.Language));
                                if (!string.IsNullOrEmpty(lstBooking[0].PurchaseOrders.Trim()))
                                    htmlBody = htmlBody.Replace("{POValue}", lstBooking[0].PurchaseOrders.TrimEnd(',').TrimStart(','));
                                else
                                    htmlBody = htmlBody.Replace("{POValue}", "-");

                                htmlBody = htmlBody.Replace("{KindRegards}", WebCommon.getGlobalResourceValue("KindRegards", objLanguage.Language));
                                htmlBody = htmlBody.Replace("{VIPAdmin}", WebCommon.getGlobalResourceValue("VIPAdmin", objLanguage.Language));

                                #endregion
                            }
                            //mailBody = this.GetStockPlannerUnexpectedBookingEmailBody(lstBooking, strLanguage);

                            string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);


                            if (objLanguage.Language.ToLower() == language[j].Trim().ToLower())
                            {
                                if (!string.IsNullOrEmpty(sentTo[j].Trim()) && !string.IsNullOrWhiteSpace(sentTo[j].Trim()))
                                {
                                    oclsEmail.sendMail(sentTo[j], htmlBody, "Unexpected booking", sFromAddress, true);
                                }
                            }

                            //oAPPBOK_CommunicationBAL.AddItemBAL(lstBooking[0].BookingID, sFromAddress, toEmail, "Unexpected Booking", htmlBody, UserID, CommunicationType.Enum.UnexpectedBookingToSP, objLanguage.LanguageID, MailSentInLanguage, MailSentInLanguage);
                        }
                    }
                }
            }
        }
        //}
    }
    private int getWeekdayNo() {
        string Weekday = System.DateTime.Now.ToString("ddd").ToLower();
        int iWeekNo = 0;
        switch (Weekday) {
            case ("sun"):
            iWeekNo = 1;
            break;
            case ("mon"):
            iWeekNo = 2;
            break;
            case ("tue"):
            iWeekNo = 3;
            break;
            case ("wed"):
            iWeekNo = 4;
            break;
            case ("thu"):
            iWeekNo = 5;
            break;
            case ("fri"):
            iWeekNo = 6;
            break;
            case ("sat"):
            iWeekNo = 7;
            break;
            default:
            iWeekNo = 0;
            break;
        }
        return iWeekNo;
    }

    protected void btnCancel_Click(object sender, EventArgs e) {
        EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
    }

    protected void btnCarrier_Click(object sender, EventArgs e) {
        EncryptQueryString("APPRcv_UnexpectedDeliveryCarrier.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString()
            + "&SiteName=" + GetQueryStringValue("SiteName").ToString()
            + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString());
    }

    protected void btnVendor_Click(object sender, EventArgs e) {
        ucMvUnexpectedDelivery.ActiveViewIndex = 1;

        MAS_SiteBE singleSiteSetting = null;
        if (GetQueryStringValue("SiteID") != null)
            singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(GetQueryStringValue("SiteID")));

        if (singleSiteSetting != null) {
            //for uk
            if (singleSiteSetting.Country.CountryName.Equals("UKandIRE"))
                ltCurrentDateTime.Text = System.DateTime.Now.ToString("dd/MM/yyyy") + "  @" + System.DateTime.Now.Hour.ToString()  + ":" + System.DateTime.Now.ToString("mm");
            else
                ltCurrentDateTime.Text = System.DateTime.Now.ToString("dd/MM/yyyy") + "  @" +  System.DateTime.Now.AddHours(1).Hour.ToString() + ":" + System.DateTime.Now.ToString("mm");
        }
        else {
            //for other
            ltCurrentDateTime.Text = System.DateTime.Now.ToString("dd/MM/yyyy") + "  @" + System.DateTime.Now.AddHours(1).Hour.ToString() + ":" + System.DateTime.Now.ToString("mm");
        }
    }

    public int GetTimeSlotID() {
        int iTimeSlotID = 0;
        int iMinutes = Convert.ToInt32((ltCurrentDateTime.Text.Split('@')[1]).Split(':')[1].ToString());
        int iReminder = iMinutes % 15;
        int iFinalMinutes = iMinutes - iReminder;
        string sFinalSlotTime = string.Empty;
        if (iFinalMinutes < 10) {
            if (Convert.ToInt32((ltCurrentDateTime.Text.Split('@')[1]).Split(':')[0].ToString()) < 10)
                sFinalSlotTime = ("0" + ltCurrentDateTime.Text.Split('@')[1]).Split(':')[0].ToString() + ":0" + iFinalMinutes.ToString();
            else
                sFinalSlotTime = (ltCurrentDateTime.Text.Split('@')[1]).Split(':')[0].ToString() + ":0" + iFinalMinutes.ToString();
        }
        else {
            if (Convert.ToInt32((ltCurrentDateTime.Text.Split('@')[1]).Split(':')[0].ToString()) < 10)
                sFinalSlotTime = ("0" + ltCurrentDateTime.Text.Split('@')[1]).Split(':')[0].ToString() + ":" + iFinalMinutes.ToString();
            else
                sFinalSlotTime = (ltCurrentDateTime.Text.Split('@')[1]).Split(':')[0].ToString() + ":" + iFinalMinutes.ToString();
        }
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstSlotTimes = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);
        if (lstSlotTimes != null && lstSlotTimes.Count > 0) {
            iTimeSlotID = lstSlotTimes.Where(a => a.SlotTime == sFinalSlotTime.Trim()).SingleOrDefault<SYS_SlotTimeBE>().SlotTimeID;
        }
        ViewState["sFinalSlotTime"] = sFinalSlotTime;
        return iTimeSlotID;
    }

    //------------------------------------ Code For Add PO Section --------------------------------------------------------

    protected void btnAddPO_Click(object sender, EventArgs e) {
        string errMsg = string.Empty;
        if (txtPurchaseNumber.Text != string.Empty) {
            DataTable myDataTable = null;
            if (ViewState["myDataTable"] != null) {

                myDataTable = (DataTable)ViewState["myDataTable"];

                //Check Item alrady added
                DataRow[] drr = myDataTable.Select("PurchaseNumber='" + txtPurchaseNumber.Text.Trim() + "'");

                if (drr.Length > 0) {
                    // // Sprint 1 - Point 7 - Shown error message when adding PO using Add PO button                     

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" +
                        strPurchaseOrderExist + "');</script>", false);

                    return;
                }
            }

            IsPOValid();
        }
    }

    protected void gvPO_RowDataBound(object sender, GridViewRowEventArgs e) {

        string DelMsg = WebCommon.getGlobalResourceValue("RemovePO");

        if (e.Row.RowType == DataControlRowType.DataRow) {
            // loop all data rows
            sAddedPurchaseOrders = sAddedPurchaseOrders + e.Row.Cells[0].Text + ",";
            foreach (DataControlFieldCell cell in e.Row.Cells) {
                // check all cells in one row
                foreach (Control control in cell.Controls) {
                    LinkButton button = control as LinkButton;
                    if (button != null && button.CommandName == "Delete")
                        // Add delete confirmation
                        button.OnClientClick = "return (confirm('" + DelMsg + "')) ;";
                }
            }
        }
    }

    protected void gvPO_RowDeleting(object sender, GridViewDeleteEventArgs e) {

        DataTable myDataTable = null;

        if (ViewState["myDataTable"] != null) {
            myDataTable = (DataTable)ViewState["myDataTable"];
        }

        if (myDataTable != null) {
            myDataTable.Rows.RemoveAt(e.RowIndex); // (Convert.ToInt32(e.Keys[0]) - 1);
            myDataTable.AcceptChanges();
            gvPO.DataSource = myDataTable;
            gvPO.DataBind();
        }

        if (myDataTable != null && myDataTable.Rows.Count > 0) {
            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
            ViewState["myDataTable"] = myDataTable;

            int TotalLines = 0;
            foreach (DataRow dataRow in myDataTable.Rows) {
                TotalLines += Convert.ToInt32(dataRow["OutstandingLines"]);
            }
            //lblExpectedLines.Text = WebCommon.getGlobalResourceValue("ExpectedLines").Replace("XX", TotalLines.ToString()); 
            Lines = TotalLines.ToString();
        }
        else {
            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
            ViewState["myDataTable"] = null;
        }
    }

    protected void gvPO_Sorting(object sender, GridViewSortEventArgs e) {
        if (ViewState["myDataTable"] != null) {
            DataTable myDataTable = (DataTable)ViewState["myDataTable"];
            ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";
            DataView view = myDataTable.DefaultView;
            view.Sort = e.SortExpression + " " + Convert.ToString(ViewState["sortDirection"]);
            gvPO.DataSource = view;
            gvPO.DataBind();
        }
    }

    private void IsPOValid() {
        try {
            string errMsg = string.Empty;
            bool isWarning = false;

            //if (IsBookingValidationExcluded == false)
            //{

            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

            oUp_PurchaseOrderDetailBE.Action = "GetAllPODetails";
            oUp_PurchaseOrderDetailBE.Purchase_order = txtPurchaseNumber.Text.Trim();
            if (!string.IsNullOrWhiteSpace(GetQueryStringValue("SiteCountryID")))
                oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(GetQueryStringValue("SiteCountryID"));

            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllPODetailsBAL(oUp_PurchaseOrderDetailBE);
            if (lstPO.Count <= 0) {
                errMsg = WebCommon.getGlobalResourceValue("UnknownPurchaseOrder"); // "The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                ShowErrorMessage(errMsg, "UnknownPurchaseOrder"); // replaced by "UnknownPurchaseOrder"
                return;
            }

            if (lstPO.Count > 0) {
                if (lstPO[0].LastUploadedFlag == "N") {
                    errMsg = WebCommon.getGlobalResourceValue("UnknownPurchaseOrder"); //"The Purchase Order Number you have supplied is not valid, please enter correct Purchase Order Number.";
                    ShowErrorMessage(errMsg, "UnknownPurchaseOrder");
                    return;
                }
            }
            //}

            if (isWarning == true) {
                ShowWarningMessages();
            }
            else {
                //---------------------Start Phase 14 R2 Pint 21--------------//
                if (IsBookingConnected == true) {
                    this.AddPO(txtPurchaseNumber.Text, false, true);
                    txtPurchaseNumber.Text = string.Empty;
                }
                else {
                    #region Commented Code for Double Bookin Warning
                    // Checking PO is existing or not.
                    APPBOK_BookingBE appbok_BookingBE = new APPBOK_BookingBE();
                    appbok_BookingBE.Action = "CheckExistingBookings";
                    appbok_BookingBE.PurchaseOrders = txtPurchaseNumber.Text;

                    if (!string.IsNullOrEmpty(ucSearchVendor1.VendorNo.Trim()))
                        appbok_BookingBE.VendorID = Convert.ToInt32(ucSearchVendor1.VendorNo.Trim());
                    else
                        appbok_BookingBE.VendorID = Convert.ToInt32(ViewState["NewVendorId"]);


                    appbok_BookingBE.SiteId = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
                    APPBOK_BookingBAL appbok_BookingBAL = new APPBOK_BookingBAL();
                    List<APPBOK_BookingBE> lstAPPBOK_BookingBE = appbok_BookingBAL.CheckExistingBookingsBAL(appbok_BookingBE);

                    if (lstAPPBOK_BookingBE.Count == 0) {
                        this.AddPO(txtPurchaseNumber.Text, false, true);
                        //txtPurchaseNumber.Text = string.Empty;
                    }
                    else {

                        ucGridBooking.DataSource = lstAPPBOK_BookingBE;
                        ucGridBooking.DataBind();
                        mdlPOConfirmMsg.Show();   

                        //List<APPBOK_BookingBE> lstAPPBOK_TodayBookingBE = lstAPPBOK_BookingBE.Where(bok => bok.ScheduleDate == DateTime.Now.Date).ToList();

                        //if (lstAPPBOK_TodayBookingBE.Count == 0) {                           
                        //        ucGridBooking.DataSource = lstAPPBOK_BookingBE;
                        //        ucGridBooking.DataBind();
                        //        mdlPOConfirmMsg.Show();                            
                        //}
                        //else {
                        //    mdlTodayBookingMsg.Show();
                        //}
                    }
                    #endregion
                }
            }
            //---------------------Start Phase 14 R2 Pint 21--------------//                    
        }
        catch (Exception ex) {
        }
    }

    private void AddPO(string PurchaseNumber, bool Multiple = false, bool isValidPOChecked = false) {
        string errMsg = string.Empty;
        bool isWarning = false;
        if (PurchaseNumber != string.Empty) {
            //DataTable myDataTable = null;
            //if (ViewState["myDataTable"] != null)
            //{
            //    myDataTable = (DataTable)ViewState["myDataTable"];

            //    //Check Item alrady added
            //    DataRow[] drr = myDataTable.Select("PurchaseNumber='" + PurchaseNumber.Trim() + "'");

            //    if (drr.Length > 0)
            //    {
            //        // // Sprint 1 - Point 7 - Shown error message when adding PO using Add PO button 

            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" +
            //                strPurchaseOrderExist + "');</script>", false);

            //        return;
            //    }
            //}

            //if (IsBookingValidationExcluded == false)
            //{ 
            //If there is no Validation Exclusion for the vendor/carrier or booking type             
            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

            oUp_PurchaseOrderDetailBE.Action = "GetAllPODetails";
            oUp_PurchaseOrderDetailBE.Purchase_order = PurchaseNumber.Trim();
            if (!string.IsNullOrWhiteSpace(GetQueryStringValue("SiteCountryID")))
                oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(GetQueryStringValue("SiteCountryID"));

            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllPODetailsBAL(oUp_PurchaseOrderDetailBE);
            if (!isValidPOChecked) {
                if (lstPO.Count <= 0) {
                    errMsg = WebCommon.getGlobalResourceValue("UnknownPurchaseOrder"); // This PO is not valid. Are you sure you want to add it to the delivery
                    ShowErrorMessage(errMsg, "UnknownPurchaseOrder");
                    return;
                }
            }

            if (lstPO.Count > 0) {
                if (!isValidPOChecked) {
                    if (lstPO[0].LastUploadedFlag == "N") {
                        errMsg = WebCommon.getGlobalResourceValue("UnknownPurchaseOrder"); //This PO is not valid. Are you sure you want to add it to the delivery
                        ShowErrorMessage(errMsg, "UnknownPurchaseOrder");
                        return;
                    }
                }

                UP_VendorBE oUP_VendorBE = new UP_VendorBE();
                oUP_VendorBE.Action = "GetConsolidateVendors";

                if (!string.IsNullOrEmpty(ucSearchVendor1.VendorNo.Trim()))
                    oUP_VendorBE.VendorID = Convert.ToInt32(ucSearchVendor1.VendorNo.Trim());
                else
                    oUP_VendorBE.VendorID = Convert.ToInt32(ViewState["NewVendorId"]);

                List<UP_VendorBE> lstConsolidateVendors = oUP_PurchaseOrderDetailBAL.GetConsolidateVendorsBAL(oUP_VendorBE);

                bool isPoVendorExist = false;
                for (int index = 0; index < lstPO.Count; index++) {
                    if (lstConsolidateVendors.FindAll(x => x.VendorID == lstPO[index].Vendor.VendorID).ToList().Count <= 0)
                        isPoVendorExist = false;
                    else {
                        isPoVendorExist = true;
                        break;
                    }
                }

                //if (lstConsolidateVendors.FindAll(x => x.VendorID == lstPO[0].Vendor.VendorID).ToList().Count <= 0)
                if (isPoVendorExist == false) {
                    errMsg = WebCommon.getGlobalResourceValue("BK_MS_12_INT"); //"The Purchase Order Number you have entered is not for the entered vendor. Do you want to continue?";
                    AddWarningMessages(errMsg, "BK_MS_12_INT");
                    isWarning = true;
                }



                // Sprint 1 - Point 14 - Begin - Changing from [Original_due_date] to [Expected Date]
                /*
                   DateTime PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.Value.Day.ToString() + "/" + lstPO[0].Original_due_date.Value.Month.ToString() + "/" + lstPO[0].Original_due_date.Value.Year.ToString()));
                   //DateTime CurrentDate = Utilities.Common.TextToDateFormat(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString());
                */
                //DateTime PODueDate = DateTime.Now;
                //if (lstPO[0].Expected_date != null) // This condition is given, if [Expected Date] will come null then [Original due date] will be set like earlier.
                //{
                //    PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Expected_date.Value.Day.ToString() + "/" + lstPO[0].Expected_date.Value.Month.ToString() + "/" + lstPO[0].Expected_date.Value.Year.ToString()));
                //}
                //else
                //{
                //    PODueDate = Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.Value.Day.ToString() + "/" + lstPO[0].Original_due_date.Value.Month.ToString() + "/" + lstPO[0].Original_due_date.Value.Year.ToString()));
                //}
                //// Sprint 1 - Point 14 - End - Changing from [Original_due_date] to [Expected Date]

                //DateTime ScheduledDate = LogicalSchedulDateTime;  //SchDate; // Utilities.Common.TextToDateFormat(GetFormatedDate(lstPO[0].Original_due_date.ToString()));

                ////OTIF Future Date PO check//
                //bool isOTIFPO = false;
                //CntOTIF_FutureDateSetupBE oCntOTIF_FutureDateSetupBE = new CntOTIF_FutureDateSetupBE();
                //CntOTIF_FutureDateSetupBAL oCntOTIF_FutureDateSetupBAL = new CntOTIF_FutureDateSetupBAL();
                //oCntOTIF_FutureDateSetupBE.Action = "ShowAll";
                //oCntOTIF_FutureDateSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                //oCntOTIF_FutureDateSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                //oCntOTIF_FutureDateSetupBE.CountryID = Convert.ToInt32(PreSiteCountryID);
                //List<CntOTIF_FutureDateSetupBE> lstCountryFutureDatePO = oCntOTIF_FutureDateSetupBAL.GetCntOTIF_FutureDateSetupDetailsBAL(oCntOTIF_FutureDateSetupBE);
                //if (lstCountryFutureDatePO != null && lstCountryFutureDatePO.Count > 0)
                //{
                //    // This condition is given, if [Expected Date] will come null then [Original due date] will be set like earlier.
                //    if (lstPO[0].Expected_date != null)
                //    {
                //        // Sprint 1 - Point 14 - Begin - Changing from [Original_due_date] to [Expected Date]
                //        /*if (lstPO[0].Original_due_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Original_due_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth))*/
                //        if (lstPO[0].Expected_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Expected_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth))
                //        // Sprint 1 - Point 14 - End - Changing from [Original_due_date] to [Expected Date]
                //        {
                //            //This is OTIF PO Date
                //            isOTIFPO = true;
                //        }
                //    }
                //    else
                //    {
                //        if (lstPO[0].Original_due_date.Value.Day == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePODay) && lstPO[0].Original_due_date.Value.Month == Convert.ToInt32(lstCountryFutureDatePO[0].FutureDatePOMonth))
                //        {
                //            //This is OTIF PO Date
                //            isOTIFPO = true;
                //        }
                //    }
                //}
                ////------------------------//


                //bool isError = false;

                //if (isOTIFPO == false)
                //{
                //    if (lstPO[0].Site.ToleranceDueDay == 0)
                //    {
                //        if (ScheduledDate < PODueDate)
                //        {  //if (ScheduledDate < PODueDate) {
                //            isError = true;
                //        }
                //    }
                //    else if (lstPO[0].Site.ToleranceDueDay > 0)
                //    {
                //        if (PODueDate > ScheduledDate.AddDays(Convert.ToDouble(lstPO[0].Site.ToleranceDueDay)))
                //        {
                //            isError = true;
                //        }
                //    }

                //    if (isError)
                //    {           //error     
                //        if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                //        {
                //            errMsg = WebCommon.getGlobalResourceValue("BK_MS_13_EXT") + PODueDate.ToString("dd/MM/yyyy");  // "The  Purchase Order quoted is not due in on the date selected. Please rebook this Purchase Order for the correct date. <br> Due Date:" + PODueDate.ToString("dd/MM/yyyy"); 
                //            ShowErrorMessage(errMsg, "BK_MS_13_EXT");
                //            return;
                //        }
                //        else
                //        {
                //            errMsg = WebCommon.getGlobalResourceValue("BK_MS_13_INT").Replace("##dd/mm/yyyy##", PODueDate.ToString("dd/MM/yyyy")); // The Purchase Order quoted is not due in for this date.The correct due date is dd/mm/yyyy. Do you want to continue?
                //            AddWarningMessages(errMsg, "BK_MS_13_INT");
                //            isWarning = true;
                //        }
                //    }
                //}


                if (Convert.ToInt32(GetQueryStringValue("SiteID")) != lstPO[0].Site.SiteID) {
                    //Cross docking check

                    MASCNT_CrossDockBE oMASCNT_CrossDockBE = new MASCNT_CrossDockBE();
                    APPCNT_CrossDockBAL oMASCNT_CrossDockBAL = new APPCNT_CrossDockBAL();

                    oMASCNT_CrossDockBE.Action = "GetDestSiteById";
                    oMASCNT_CrossDockBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oMASCNT_CrossDockBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));

                    List<MASCNT_CrossDockBE> lstCrossDock = oMASCNT_CrossDockBAL.GetCrossDockDetailsBAL(oMASCNT_CrossDockBE);

                    if (lstCrossDock != null && lstCrossDock.Count > 0) {

                        lstCrossDock = lstCrossDock.FindAll(delegate(MASCNT_CrossDockBE cd) {
                            return cd.DestinationSiteID == lstPO[0].Site.SiteID;
                        });
                    }

                    if (lstCrossDock == null || lstCrossDock.Count == 0) {
                        //if (hdnCurrentRole.Value == "Vendor" || hdnCurrentRole.Value == "Carrier")
                        //{
                        //    errMsg = WebCommon.getGlobalResourceValue("BK_MS_14_EXT"); //"The Purchase Order you have quoted is not for the Office Depot site you have selected." +
                        //    //"Please recheck the entries made and resubmit the correct information";
                        //    ShowErrorMessage(errMsg, "BK_MS_14_EXT");
                        //    return;
                        //}
                        //else
                        //{
                        errMsg = WebCommon.getGlobalResourceValue("BK_MS_14_INT"); //"The Purchase Order you have quoted is not for the Office Depot site you have selected. Do you want to continue?";
                        AddWarningMessages(errMsg, "BK_MS_14_INT");
                        isWarning = true;
                        //}
                    }

                }
            }

            if (isWarning == false) {
                BindPOGrid(PurchaseNumber);

            }
            else {
                //if (!Multiple)
                //{
                ShowWarningMessages();
                //}
                //else
                //{
                //    BindPOGrid(PurchaseNumber);
                //}
            }
            //}
            //else if (IsBookingValidationExcluded == true)
            //{  //If there is Validation Exclusion for the vendor/carrier or booking type
            //    BindPOGrid(PurchaseNumber);

            //}
        }
        else {
            errMsg = WebCommon.getGlobalResourceValue("BK_MS_CorrectPO"); //"Please enter correct Purchase Order Number.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
        }
    }

    protected void BindPOGrid(string PurchaseNumber) {

        if (PurchaseNumber != string.Empty) {

            DataTable myDataTable = null;

            if (ViewState["myDataTable"] != null) {
                myDataTable = (DataTable)ViewState["myDataTable"];
                DataRow[] drr = myDataTable.Select("PurchaseNumber='" + PurchaseNumber.Trim() + "'");
                if (drr.Length > 0)
                    return;
            }
            else {
                myDataTable = new DataTable();
                DataColumn auto = new DataColumn("AutoID", typeof(System.Int32));
                auto.AutoIncrement = true;
                auto.AutoIncrementSeed = 1;
                auto.ReadOnly = true;
                auto.Unique = true;
                myDataTable.Columns.Add(auto);

                DataColumn myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseNumber";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Original_due_date";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "VendorName";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "OutstandingLines";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "Qty_On_Hand";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "qty_on_backorder";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "SiteName";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "PurchaseOrderID";
                myDataTable.Columns.Add(myDataColumn);

                myDataColumn = new DataColumn();
                myDataColumn.ColumnName = "ExpectedDate";
                myDataTable.Columns.Add(myDataColumn);
            }


            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
            oUp_PurchaseOrderDetailBE.Vendor = new UP_VendorBE();

            oUp_PurchaseOrderDetailBE.Action = "GetNewBookingPODetails";
            oUp_PurchaseOrderDetailBE.Purchase_order = PurchaseNumber;
            if (!string.IsNullOrWhiteSpace(GetQueryStringValue("SiteCountryID")))
                oUp_PurchaseOrderDetailBE.CountryID = Convert.ToInt32(GetQueryStringValue("SiteCountryID"));


            if (!string.IsNullOrEmpty(ucSearchVendor1.VendorNo.Trim()))
                oUp_PurchaseOrderDetailBE.Vendor.VendorID = Convert.ToInt32(ucSearchVendor1.VendorNo.Trim());
            else
                oUp_PurchaseOrderDetailBE.Vendor.VendorID = Convert.ToInt32(ViewState["NewVendorId"]);


            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);
            DataRow dataRow = myDataTable.NewRow();
            if (lstPO.Count > 0) {

                //oUp_PurchaseOrderDetailBE.Action = "GetStockDetails";
                //oUp_PurchaseOrderDetailBE.Purchase_order = PurchaseNumber;
                //oUp_PurchaseOrderDetailBE.Site = new MAS_SiteBE();
                //if (!string.IsNullOrWhiteSpace(GetQueryStringValue("SiteID")))
                //    oUp_PurchaseOrderDetailBE.Site.SiteID = lstPO[0].Site.SiteID; // Convert.ToInt32(GetQueryStringValue("SiteID"));

                //List<Up_PurchaseOrderDetailBE> lstPOStock = oUP_PurchaseOrderDetailBAL.GetPriorityBAL(oUp_PurchaseOrderDetailBE);

                //if (lstPOStock != null && lstPOStock.Count > 0)
                //{
                //    List<Up_PurchaseOrderDetailBE> lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p) { return Convert.ToDecimal(p.SKU.Qty_On_Hand) == 0 && (Convert.ToInt32(Convert.ToDecimal(p.Original_quantity)) - p.Qty_receipted > 0); });
                //    if (lstPOTemp != null && lstPOTemp.Count > 0)
                //        dataRow["Qty_On_Hand"] = lstPOTemp.Count.ToString();
                //    else
                //        dataRow["Qty_On_Hand"] = "0";

                //    lstPOTemp = lstPOStock.FindAll(delegate(Up_PurchaseOrderDetailBE p)
                //    { return (Convert.ToDecimal(p.SKU.Qty_On_Hand) - Convert.ToDecimal(p.SKU.qty_on_backorder) < 0); });

                //    if (lstPOTemp != null && lstPOTemp.Count > 0)
                //        dataRow["qty_on_backorder"] = lstPOTemp.Count.ToString();
                //    else
                //        dataRow["qty_on_backorder"] = "0";
                //}
                //else
                //{
                //    dataRow["Qty_On_Hand"] = "0";
                //    dataRow["qty_on_backorder"] = "0";
                //}


                dataRow["Qty_On_Hand"] = lstPO[0].Stockouts.ToString();
                dataRow["qty_on_backorder"] = lstPO[0].Backorders.ToString();
                dataRow["OutstandingLines"] = lstPO[0].OutstandingLines.ToString();

                dataRow["PurchaseNumber"] = PurchaseNumber;
                dataRow["Original_due_date"] = lstPO[0].Original_due_date.Value.ToString("dd/MM/yyyy");
                // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                // This condition is given, if [Expected Date] will come null then [Original due date] will be set like earlier.
                if (lstPO[0].Expected_date != null) {
                    dataRow["ExpectedDate"] = lstPO[0].Expected_date.Value.ToString("dd/MM/yyyy");
                }
                else {
                    dataRow["ExpectedDate"] = lstPO[0].Original_due_date.Value.ToString("dd/MM/yyyy");
                }
                // Sprint 1 - Point 14 - End - Adding [Expected Date]

                dataRow["VendorName"] = lstPO[0].Vendor.VendorName;
                //dataRow["OutstandingLines"] = lstPO.Count.ToString(); // lstPO[0].OutstandingLines.ToString();
                //dataRow["Qty_On_Hand"] = lstPO[0].SKU.Qty_On_Hand;
                //dataRow["qty_on_backorder"] = lstPO[0].SKU.qty_on_backorder;
                dataRow["SiteName"] = lstPO[0].Site.SiteName;
                dataRow["PurchaseOrderID"] = lstPO[0].PurchaseOrderID.ToString();

                myDataTable.Rows.Add(dataRow);
            }
            else {
                //if (IsBookingValidationExcluded == true)
                //{
                //ucLabel SelectedVendorName = (ucLabel)ucSearchVendor1.FindControl("SelectedVendorName");

                //#region Logic to get selected Vendor Name
                //string selectedVenderName = string.Empty;
                //if (!string.IsNullOrWhiteSpace(SelectedVendorName.Text))
                //{
                //    int index = -1;
                //    index = SelectedVendorName.Text.IndexOf("-");
                //    if (index != (-1))
                //    {
                //        selectedVenderName = SelectedVendorName.Text.Remove(0, index + 1).Trim().ToString();
                //    }
                //}
                //#endregion

                dataRow["PurchaseNumber"] = PurchaseNumber;
                dataRow["Original_due_date"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                dataRow["ExpectedDate"] = ActualSchedulDateTime.ToString("dd/MM/yyyy");
                dataRow["VendorName"] = GetSelectedVenderName();
                dataRow["OutstandingLines"] = "1";
                dataRow["Qty_On_Hand"] = "0";
                dataRow["qty_on_backorder"] = "0";
                dataRow["SiteName"] = GetQueryStringValue("SiteName");
                dataRow["PurchaseOrderID"] = 0;
                myDataTable.Rows.Add(dataRow);
                //}
            }

            if (myDataTable != null && myDataTable.Rows.Count > 0) {
                gvPO.DataSource = myDataTable;
                gvPO.DataBind();
                txtPurchaseNumber.Text = String.Empty;
            }

            if (myDataTable != null && myDataTable.Rows.Count > 0) {
                //ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnshow", "$get('" + btnProceedBooking.ClientID + "').style.display='';", true);
                ViewState["myDataTable"] = myDataTable;
            }
            else {
                //ScriptManager.RegisterStartupScript(Page, this.GetType(), "btnhide", "$get('" + btnProceedBooking.ClientID + "').style.display='none';", true);
                ViewState["myDataTable"] = null;
            }

            //int TotalLines = 0;
            //foreach (DataRow dr in myDataTable.Rows)
            //{
            //    if (dr["OutstandingLines"].ToString() != string.Empty)
            //        TotalLines += Convert.ToInt32(dr["OutstandingLines"]);
            //}


            //for (int iCount = 0; iCount < myDataTable.Rows.Count; iCount++) {
            //    TotalLines += Convert.ToInt32(lstPO[0].OutstandingLines.ToString()); //Convert.ToInt32(dataRow["OutstandingLines"]);
            //}
            //lblExpectedLines.Text = WebCommon.getGlobalResourceValue("ExpectedLines").Replace("XX", TotalLines.ToString()); //lblExpectedLines.Text.Replace(Lines, TotalLines.ToString());
            //Lines = TotalLines.ToString();

            //txtPurchaseNumber.Text = string.Empty;
        }

    }

    private void SetVendorDetailsByUserID(int siteId) {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetVendorDetailsByUserID";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oUP_VendorBE.Site = new MAS_SiteBE();
        oUP_VendorBE.Site.SiteID = siteId;

        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetVendorByUserIdBAL(oUP_VendorBE);
        if (lstUPVendor != null && lstUPVendor.Count > 0) {
            ucSeacrhVendorID = lstUPVendor[0].VendorID.ToString();
            ucSeacrhVendorName = lstUPVendor[0].VendorName;
            ucSeacrhVendorNo = lstUPVendor[0].Vendor_No;
            ucSeacrhVendorParentID = lstUPVendor[0].ParentVendorID.ToString();
        }
        //spVender.Style.Add("display", "none");

        //ltSearchVendorName.Visible = true;
        //ltSearchVendorName.Text = ucSeacrhVendorName;
    }

    private void AddWarningMessages(string ErrMessage, string CommandName) {
        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null) {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }
        if (ErrorMessages.Get(CommandName) == null) {
            ErrorMessages.Add(CommandName, ErrMessage);
            ViewState["ErrorMessages"] = ErrorMessages;
        }
    }

    private void ShowErrorMessage(string ErrMessage, string CommandName) {
        ltErrorMsg.Text = ErrMessage;
        btnBack.CommandName = CommandName;
        mdlErrorMsg.Show();
    }

    private void ShowWarningMessages() {
        NameValueCollection ErrorMessages = new NameValueCollection();
        if (ViewState["ErrorMessages"] != null) {
            ErrorMessages = (NameValueCollection)ViewState["ErrorMessages"];
        }
        if (ErrorMessages.Keys.Count > 0) {
            string errMsg = ErrorMessages.Get(0);
            string CommandName = ErrorMessages.GetKey(0);
            if (CommandName == "BK_MS_26_EXT") {
                errMsg = @"<span style=""color:Red;font-size:14px;"" >" + errMsg + "</span>";
            }
            ltConfirmMsg.Text = errMsg;
            btnErrContinue.CommandName = CommandName;
            btnErrBack.CommandName = CommandName;
            mdlConfirmMsg.Show();
            ErrorMessages.Remove(CommandName);
            ViewState["ErrorMessages"] = ErrorMessages;
        }
    }

    protected void btnErrorMsgOK_Click(object sender, EventArgs e) { }

    protected void btnInvalidPOContinue_Click(object sender, EventArgs e) {
        mdlErrorMsg.Hide();
        this.AddPO(txtPurchaseNumber.Text, false, true);
    }


    //---------------------Start Phase 14 R2 Pint 21--------------//

    protected void btnDoNotConnectBooking_Click(object sender, EventArgs e) {
        this.AddPO(txtPurchaseNumber.Text, false, true);
        txtPurchaseNumber.Text = string.Empty;
    }

    protected void btnConnectBooking_Click(object sender, EventArgs e) {

        for (int i = 0; i <= ucGridBooking.Rows.Count; i++) {
            GridViewRow row = ucGridBooking.Rows[i];
            RadioButton rdb = (RadioButton)row.FindControl("rdoBookingID");

            if (rdb.Checked == true) {
                HiddenField hdnBookingID = (HiddenField)row.FindControl("hdnBookingID");
                HiddenField hdnSupplierType = (HiddenField)row.FindControl("hdnSupplierType");
                HiddenField hdnBookingdate = (HiddenField)row.FindControl("hdnBookingdate");

                if (Convert.ToDateTime(hdnBookingdate.Value).Date == DateTime.Now.Date) {
                    mdlPOConfirmMsg.Hide();
                    mdlTodayBookingMsg.Show();                   
                }
                else {
                    string bookingid = hdnBookingID.Value;
                    if (hdnSupplierType.Value.ToLower() != "c") {
                        ConnectToBooking(bookingid);
                    }
                    else {
                        ChangeViewToCarrier(bookingid);
                        return;
                    }                   
                }
                break;
            }
        }      
    }

    private void ConnectToBooking(string bookingid, bool isViewChange = false) {
        BindBooking(bookingid, isViewChange);
        IsBookingConnected = true;
        ConnectedBookingID = Convert.ToInt32(bookingid);
        tblConnect.Style.Add("display", "");
    }

    private void BindBooking(string bookingid, bool isViewChange) {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "GetVendorBookingDeatilsByID";
        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(bookingid);

        List<APPBOK_BookingBE> lstBookingDeatils = new List<APPBOK_BookingBE>();
        lstBookingDeatils = oAPPBOK_BookingBAL.GetVendorBookingDetailsBAL(oAPPBOK_BookingBE);

        if (lstBookingDeatils != null && lstBookingDeatils.Count > 0) {

            ltBooking.Text = lstBookingDeatils[0].BookingRef;

            ucSeacrhVendorID = lstBookingDeatils[0].Vendor.VendorID.ToString();
            ucSeacrhVendorName = lstBookingDeatils[0].Vendor.VendorName;
            ucSeacrhVendorNo = lstBookingDeatils[0].Vendor.Vendor_No;
            ucSeacrhVendorParentID = lstBookingDeatils[0].Vendor.ParentVendorID.ToString();

            if (isViewChange) {
                ucSearchVendor1.innerControlVendorName.Text = ucSeacrhVendorNo + " - " + ucSeacrhVendorName;
                ucSearchVendor1.innerControlVendorNo.Text = ucSeacrhVendorNo;
                ucSearchVendor1.VendorNo = lstBookingDeatils[0].Vendor.VendorID.ToString();
                cusvVendorRequired.Enabled = false;
            }

            ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(lstBookingDeatils[0].Carrier.CarrierID.ToString()));

            txtPallets.Text = lstBookingDeatils[0].NumberOfPallet.ToString();
            txtCartons.Text = lstBookingDeatils[0].NumberOfCartons.ToString();
            txtLines.Text = lstBookingDeatils[0].NumberOfLines.ToString();

            BindAllPOGrid(bookingid);
        }
    }

    protected void BindAllPOGrid(string bookingid) {

        try {
            DataTable myDataTable = null;
            myDataTable = new DataTable();

            DataColumn auto = new DataColumn("AutoID", typeof(System.Int32));
            // specify it as auto increment field
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;
            auto.ReadOnly = true;
            auto.Unique = true;
            myDataTable.Columns.Add(auto);

            DataColumn myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "PurchaseNumber";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "Original_due_date";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "VendorName";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "OutstandingLines";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "Qty_On_Hand";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "qty_on_backorder";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "SiteName";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "PurchaseOrderID";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "ExpectedDate";
            myDataTable.Columns.Add(myDataColumn);

            Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
            UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

            oUp_PurchaseOrderDetailBE.Action = "GetAllBookingPODetails";
            oUp_PurchaseOrderDetailBE.BookingID = Convert.ToInt32(bookingid);

            List<Up_PurchaseOrderDetailBE> lstPO = oUP_PurchaseOrderDetailBAL.GetAllBookedProductOrderDetailsBAL(oUp_PurchaseOrderDetailBE);

            if (lstPO.Count > 0) {
                for (int iCount = 0; iCount < lstPO.Count; iCount++) {
                    DataRow dataRow = myDataTable.NewRow();


                    dataRow["Qty_On_Hand"] = lstPO[iCount].Stockouts;
                    dataRow["qty_on_backorder"] = lstPO[iCount].Backorders;
                    if (lstPO[iCount].OutstandingLines > 0) {
                        dataRow["OutstandingLines"] = lstPO[iCount].OutstandingLines.ToString(); //lstPOStock.Count.ToString(); //
                    }
                    else {
                        dataRow["OutstandingLines"] = 1;
                    }

                    dataRow["PurchaseNumber"] = lstPO[iCount].Purchase_order;
                    dataRow["Original_due_date"] = lstPO[iCount].Original_due_date.Value.ToString("dd/MM/yyyy");
                    dataRow["VendorName"] = lstPO[iCount].Vendor.VendorName;

                    dataRow["SiteName"] = lstPO[iCount].Site.SiteName;
                    dataRow["PurchaseOrderID"] = lstPO[iCount].PurchaseOrderID.ToString();

                    if (lstPO[iCount].Expected_date != null) {
                        dataRow["ExpectedDate"] = lstPO[iCount].Expected_date.Value.ToString("dd/MM/yyyy");
                    }
                    else {
                        dataRow["ExpectedDate"] = lstPO[iCount].Original_due_date.Value.ToString("dd/MM/yyyy");
                    }

                    myDataTable.Rows.Add(dataRow);
                }
            }

            if (myDataTable != null && myDataTable.Rows.Count > 0) {
                gvPO.DataSource = myDataTable;
                gvPO.DataBind();
            }

            int TotalLines = 0;
            foreach (DataRow dr in myDataTable.Rows) {
                if (dr["OutstandingLines"].ToString() != string.Empty)
                    TotalLines += Convert.ToInt32(dr["OutstandingLines"]);
            }

            ViewState["myDataTable"] = myDataTable;
            Lines = TotalLines.ToString();
        }
        catch (Exception ex) { }

    }

    protected void btnDisconnectBooking_Click(object sender, EventArgs e) {
        //ucSeacrhVendorID = string.Empty;
        //ucSeacrhVendorName = string.Empty;
        //ucSeacrhVendorNo = string.Empty;
        //ucSeacrhVendorParentID = string.Empty;
        ucSearchVendor1.ClearSearch();

        ddlCarrier.SelectedIndex = -1;
        txtPallets.Text = string.Empty;
        txtCartons.Text = string.Empty;
        txtLines.Text = string.Empty;

        ViewState["myDataTable"] = null;
        gvPO.DataSource = null;
        gvPO.DataBind();

        Lines = string.Empty;

        IsBookingConnected = false;
        ConnectedBookingID = -1;

        tblConnect.Style.Add("display", "none");

    }

    protected void btnPOBack_Click(object sender, EventArgs e) { }

    private void ChangeViewToCarrier(string bookingid) {
        EncryptQueryString("APPRcv_UnexpectedDeliveryCarrier.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString()
            + "&SiteName=" + GetQueryStringValue("SiteName").ToString()
            + "&SiteCountryID=" + GetQueryStringValue("SiteCountryID").ToString()
            + "&BookingID=" + bookingid
            );
    }
    //---------------------End Phase 14 R2 Pint 21--------------//      

    protected void btnBack_Click(object sender, CommandEventArgs e) {
        mdlConfirmMsg.Hide();
    }

    protected void btnContinue_Click(object sender, CommandEventArgs e) {
        BindPOGrid(txtPurchaseNumber.Text);
        mdlConfirmMsg.Hide();
    }

    private string GetSelectedVenderName() {
        string selectedVenderName = string.Empty;
        ucLabel SelectedVendorName = (ucLabel)ucSearchVendor1.FindControl("SelectedVendorName");
        if (!string.IsNullOrWhiteSpace(SelectedVendorName.Text)) {
            int index = -1;
            index = SelectedVendorName.Text.IndexOf("-");
            if (index != (-1)) {
                selectedVenderName = SelectedVendorName.Text.Remove(0, index + 1).Trim().ToString();
            }
        }//added by Gourvi to show Unknown Venodrname in grid
        else if (txtUnknownVendor.Text != null) {
            selectedVenderName = txtUnknownVendor.Text;
        }
        return selectedVenderName;
    }

    private MAS_SiteBE GetSingleSiteSetting(int siteId) {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSingleSiteSetting";
        oMAS_SiteBE.SiteID = siteId;
        MAS_SiteBE localMAS_SiteBE = oMAS_SiteBAL.GetSingleSiteSettingBAL(oMAS_SiteBE);
        return localMAS_SiteBE;
    }

    private void BindCarrier() {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        oMASCNT_CarrierBE.Action = "ShowAll";
        oMASCNT_CarrierBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        oMASCNT_CarrierBE.Site.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
        oMASCNT_CarrierBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_CarrierBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);

        if (lstCarrier.Count > 0) {
            FillControls.FillDropDown(ref ddlCarrier, lstCarrier, "CarrierName", "CarrierID", "--Select--");
        }
    }

    //private string GetStockPlannerUnexpectedBookingEmailBody(List<APPBOK_BookingBE> lstBookingBE, string language)
    //{
    //    string htmlBody = string.Empty;
    //    string templatePath = string.Empty;
    //    string LanguageFile = string.Empty;


    //    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    //    string templatePathName = @"/EmailTemplates/Appointment/";

    //    templatePath = path + templatePathName;
    //    LanguageFile = "UnexpectedBookingLetterToSP.english.htm";

    //    //List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
    //    clsEmail oclsEmail = new clsEmail();

    //    #region Setting reason as per the language ...
    //    switch (language)
    //    {
    //        case clsConstants.English:
    //            Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
    //            break;
    //        case clsConstants.French:
    //            Page.UICulture = clsConstants.FranceISO; // // France
    //            break;
    //        case clsConstants.German:
    //            Page.UICulture = clsConstants.GermanyISO; //"de"; // German
    //            break;
    //        case clsConstants.Dutch:
    //            Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
    //            break;
    //        case clsConstants.Spanish:
    //            Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
    //            break;
    //        case clsConstants.Italian:
    //            Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
    //            break;
    //        case clsConstants.Czech:
    //            Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
    //            break;
    //        default:
    //            Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
    //            break;
    //    }

    //    #endregion


    //    APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
    //    //List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
    //    clsEmail oclsEmail = new clsEmail();

    //    foreach (MAS_LanguageBE objLanguage in oLanguages)
    //    {
    //        bool MailSentInLanguage = false;

    //        if (objLanguage.Language.ToLower() == language.ToLower())
    //        {
    //            MailSentInLanguage = true;
    //        }


    //        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
    //    {
    //        htmlBody = sReader.ReadToEnd();
    //        // htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

    //        htmlBody = htmlBody.Replace("{Hi}", WebCommon.getGlobalResourceValue("Hi"));

    //        string vendorname = string.Empty;

    //        if (lstBookingBE != null && lstBookingBE.Count > 0)
    //        {
    //            //if (lstBookingBE[0].SupplierType == "C")
    //            //{
    //            //    for (int i = lstBookingBE.Count - 1; i >= 0; i--)
    //            //    {
    //            //        vendorname += lstBookingBE[i].Vendor.VendorName + "<br />";
    //            //    }
    //            //    //foreach (var item in lstBookingBE)
    //            //    //{
    //            //    //    vendorname += item.Vendor.VendorName + "<br />";
    //            //    //}
    //            //    htmlBody = htmlBody.Replace("{VendorNameValue}", vendorname);
    //            //}
    //            //else
    //            //{
    //            htmlBody = htmlBody.Replace("{VendorNameValue}", lstBookingBE[0].Vendor.VendorName);
    //            //}

    //            htmlBody = htmlBody.Replace("{UnexpectedBookingToSPText1}", WebCommon.getGlobalResourceValue("UnexpectedBookingToSPText1"));
    //            htmlBody = htmlBody.Replace("{UnexpectedBookingToSPText2}", WebCommon.getGlobalResourceValue("UnexpectedBookingToSPText2"));


    //            htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));
    //            htmlBody = htmlBody.Replace("{DateValue}", lstBookingBE[0].ScheduleDate.Value.ToString("dd/MM/yyyy"));

    //            htmlBody = htmlBody.Replace("{Time}", WebCommon.getGlobalResourceValue("Time"));
    //            htmlBody = htmlBody.Replace("{TimeValue}", lstBookingBE[0].SlotTime.SlotTime.ToString());


    //            htmlBody = htmlBody.Replace("{Site}", WebCommon.getGlobalResourceValue("Site"));
    //            htmlBody = htmlBody.Replace("{SiteValue}", lstBookingBE[0].SiteName);


    //            htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier"));
    //            htmlBody = htmlBody.Replace("{CarrierValue}", lstBookingBE[0].Carrier.CarrierName);

    //            htmlBody = htmlBody.Replace("{Pallets}", WebCommon.getGlobalResourceValue("Pallets"));
    //            htmlBody = htmlBody.Replace("{NumberofPalletsValue}", lstBookingBE[0].NumberOfPallet.ToString());

    //            htmlBody = htmlBody.Replace("{Cartons}", WebCommon.getGlobalResourceValue("Cartons"));
    //            htmlBody = htmlBody.Replace("{NumberofCartonsValue}", lstBookingBE[0].NumberOfCartons.ToString());


    //            htmlBody = htmlBody.Replace("{PO}", WebCommon.getGlobalResourceValue("PO"));
    //            if (!string.IsNullOrEmpty(lstBookingBE[0].PurchaseOrders.Trim()))
    //                htmlBody = htmlBody.Replace("{POValue}", lstBookingBE[0].PurchaseOrders.TrimEnd(',').TrimStart(','));
    //            else
    //                htmlBody = htmlBody.Replace("{POValue}", "-");

    //            htmlBody = htmlBody.Replace("{KindRegards}", WebCommon.getGlobalResourceValue("KindRegards"));
    //            htmlBody = htmlBody.Replace("{VIPAdmin}", WebCommon.getGlobalResourceValue("VIPAdmin"));


    //        }
    //        return htmlBody;
    //    }
    //}


    protected void btnAcceptandUnload_Click(object sender, EventArgs e)
    {
        int BookingStatusID = 2;
        List<UP_VendorBE> oUP_VendorBEList = ucSearchVendor1.IsValidVendorCode();
        string sValidVendorCode = WebCommon.getGlobalResourceValue("ValidVendorCode");

        if (oUP_VendorBEList.Count == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + sValidVendorCode + "')", true);
            return;
        }
        else
        {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
            oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
            oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
            oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

            //adding unknown vendor to database and assigning new vendor id to viewstate.
            if (!string.IsNullOrEmpty(txtUnknownVendor.Text))
            {
                oAPPBOK_BookingBE.Action = "AddUnknownVendor";
                oAPPBOK_BookingBE.FixedSlot.Vendor.VendorName = txtUnknownVendor.Text.Trim();
                ViewState["NewVendorId"] = Convert.ToInt32(oAPPBOK_BookingBAL.addEditUnexpectedDeliveryBAL(oAPPBOK_BookingBE));
            }
            oAPPBOK_BookingBE.FixedSlot.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
            if (!string.IsNullOrEmpty(ucSearchVendor1.VendorNo.Trim()))
                oAPPBOK_BookingBE.FixedSlot.Vendor.VendorID = Convert.ToInt32(ucSearchVendor1.VendorNo.Trim());
            else
                oAPPBOK_BookingBE.FixedSlot.Vendor.VendorID = Convert.ToInt32(ViewState["NewVendorId"]);

            oAPPBOK_BookingBE.ScheduleDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            oAPPBOK_BookingBE.FixedSlot.SlotTimeID = GetTimeSlotID();
            oAPPBOK_BookingBE.NumberOfPallet = !string.IsNullOrWhiteSpace(txtPallets.Text) ? Convert.ToInt32(txtPallets.Text.Trim()) : (int?)null;
            oAPPBOK_BookingBE.NumberOfCartons = !string.IsNullOrWhiteSpace(txtCartons.Text) ? Convert.ToInt32(txtCartons.Text.Trim()) : (int?)null;
            if (validationFunctions.IsNumeric(txtLines.Text))
                oAPPBOK_BookingBE.NumberOfLines = !string.IsNullOrWhiteSpace(txtLines.Text) ? Convert.ToInt32(txtLines.Text.Trim()) : (int?)null;

            // Sprint 1 - Point 8 
            oAPPBOK_BookingBE.DLYARR_OperatorInital = ddlOperators.SelectedItem.Text.Trim();
            oAPPBOK_BookingBE.SupplierType = "V";
            oAPPBOK_BookingBE.PreAdviseNotification = "Y";
            oAPPBOK_BookingBE.BookingTypeID = 2; //Unscheduled
            oAPPBOK_BookingBE.BookingStatusID = BookingStatusID;
            oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"]);
            oAPPBOK_BookingBE.WeekDay = getWeekdayNo();

            if (!string.IsNullOrEmpty(ucSearchVendor1.VendorNo.Trim()))
            {
                oAPPBOK_BookingBE.VendorID = Convert.ToInt32(ucSearchVendor1.VendorNo);
            }
            else
            {
                oAPPBOK_BookingBE.VendorID = Convert.ToInt32(ViewState["NewVendorId"]);
            }
            DataTable myDataTable = null;
            if (ViewState["myDataTable"] != null)
                myDataTable = (DataTable)ViewState["myDataTable"];

            string strPoIds = string.Empty;
            string strPoNos = string.Empty;
            if (myDataTable != null)
            {
                for (int iCount = 0; iCount < myDataTable.Rows.Count; iCount++)
                {
                    if (strPoIds == string.Empty)
                    {
                        strPoIds = myDataTable.Rows[iCount]["PurchaseOrderID"].ToString();
                        strPoNos = myDataTable.Rows[iCount]["PurchaseNumber"].ToString();
                    }
                    else
                    {
                        strPoIds = strPoIds + "," + myDataTable.Rows[iCount]["PurchaseOrderID"].ToString();
                        strPoNos = strPoNos + "," + myDataTable.Rows[iCount]["PurchaseNumber"].ToString();
                    }
                }
            }

            oAPPBOK_BookingBE.PurchaseOrdersIDs = strPoIds;
            oAPPBOK_BookingBE.PurchaseOrders = strPoNos;

            oAPPBOK_BookingBE.TransactionComments = txtComments.Text;
            if (ddlCarrier.Items.Count > 0)
                if (ddlCarrier.SelectedIndex > 0)
                    oAPPBOK_BookingBE.VendorCarrierID = Convert.ToInt32(ddlCarrier.SelectedValue);

            oAPPBOK_BookingBE.Action = "CheckUnexpectedDelivery";
            if (Convert.ToInt32(oAPPBOK_BookingBAL.addEditUnexpectedDeliveryBAL(oAPPBOK_BookingBE)) == 1)
            {
                if (BookingStatusID == 2)
                {
                    EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
                }               
                return;
            }

            if (IsBookingConnected == true && BookingStatusID == 2)
            {
                oAPPBOK_BookingBE.Action = "LinkUnexpectedDelivery";
                oAPPBOK_BookingBE.BookingID = ConnectedBookingID;
            }
            else
            {
                oAPPBOK_BookingBE.Action = "InsertUnexpectedDelivery";
                if (IsBookingConnected == true)
                {
                    oAPPBOK_BookingBE.BookingID = ConnectedBookingID;
                }
                else { oAPPBOK_BookingBE.BookingID = -1; }
            }

            PKID = Convert.ToInt32(oAPPBOK_BookingBAL.addEditUnexpectedDeliveryBAL(oAPPBOK_BookingBE));


            Session["UnexpectedDeliveryPKID"] = PKID.ToString();
            Session.Remove("TransactionComments");
            MethodInvoker simpleDelegate = new MethodInvoker(SendMail);
            UserID = Convert.ToInt32(Session["UserID"]);
            if (BookingStatusID == 2)
            {
                EncryptQueryString("~/ModuleUI/Appointment/Receiving/APPRcv_DeliveryUnloaded.aspx?Scheduledate="
                     + DateTime.Now.ToString("dd\\/MM\\/yyyy") + "&ID=" + "BK" + "-"
                     + "V" + "-" + PKID + "-" +GetQueryStringValue("SiteID").ToString()
                     + "&SiteID=" +GetQueryStringValue("SiteID").ToString());

                simpleDelegate.BeginInvoke(BookingStatusID, null, null);
            }           
        }
    }
}

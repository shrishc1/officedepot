﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="APPRcv_UnexpectedDeliveryCarrier.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" MaintainScrollPositionOnPostback="true"
    Inherits="APPRcv_UnexpectedDeliveryCarrier" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucVendor" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function ShowHidePassword() {
            var LoggedInUserId = document.getElementById('<%= hdnLoggedInUserId.ClientID %>').value;
            var SelectedUserID = document.getElementById('<%= ddlOperators.ClientID %>').value;
            var IsUnexpDelPassAllow = document.getElementById('<%= hdnIsUnexpDelPassAllow.ClientID %>').value;

            /*if (SelectedUserID != LoggedInUserId && IsUnexpDelPassAllow == 'Y')*/
            if (SelectedUserID != LoggedInUserId) {
                document.getElementById('<%=tblPassword.ClientID%>').style.display = "";
                ValidatorEnable(document.getElementById('<%=rfvOperatorPasswordRequired.ClientID%>'), true);
            }
            else {
                document.getElementById('<%=tblPassword.ClientID%>').style.display = "none";
                ValidatorEnable(document.getElementById('<%=rfvOperatorPasswordRequired.ClientID%>'), false);
            }
        }

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    return false;
                }
            });
        });

    </script>
    <h2>
        <cc1:ucLabel ID="lblUnexpectedDelivery" runat="server" Text="Unexpected Delivery"></cc1:ucLabel>
        <asp:Literal ID="ltSiteName" runat="server" Text="Unexpected Delivery"></asp:Literal>
    </h2>
    <asp:UpdatePanel ID="upCarrier" runat="server">
        <ContentTemplate>
            <div class="right-shadow">
                <div class="formbox">
                    <div>
                        <asp:RequiredFieldValidator ID="rfvCarrierRequired" runat="server" ControlToValidate="ddlCarrier"
                            Display="None" ValidationGroup="a" SetFocusOnError="true" InitialValue="0">
                        </asp:RequiredFieldValidator>
                        <%--Sprint 1 - Point 8 - Start--%>
                        <asp:RequiredFieldValidator ID="rfvOperatorPasswordRequired" runat="server" ValidationGroup="a"
                            Display="None" ControlToValidate="txtPassword">
                        </asp:RequiredFieldValidator>
                        <%--<asp:RequiredFieldValidator ID="rfvInitialsRequired" runat="server" ControlToValidate="txtInitials"
                            Display="None" ValidationGroup="a">
                        </asp:RequiredFieldValidator>--%>
                        <%--Sprint 1 - Point 8 - End--%>
                        <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="a" ShowMessageBox="true"
                            ShowSummary="false" />
                    </div>
                    <table width="70%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold; width: 19%">
                                <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 80%" colspan="4">
                                <cc1:ucDropdownList ID="ddlCarrier" runat="server" Width="150px" AutoPostBack="true">
                                </cc1:ucDropdownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblTimeSlot" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td>
                                <cc1:ucLiteral ID="ltCurrentDateTime" runat="server"></cc1:ucLiteral>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblTotalExpectedPallets" runat="server" Text="Total Pallets"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox runat="server" ID="txtTotalPallets" Width="70px" onkeyup="AllowNumbersOnly(this)"
                                    MaxLength="3"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblTotalCartons" runat="server" Text="Total Cartons"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td>
                                <cc1:ucNumericTextbox runat="server" ID="txtTotalCartons" Width="70px" onkeyup="AllowNumbersOnly(this)"
                                    MaxLength="4"></cc1:ucNumericTextbox>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblLines" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td>
                                <cc1:ucTextbox runat="server" ID="txtTotalLines" Width="70px" onkeyup="AllowNumbersOnly(this)"
                                    MaxLength="3"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblOperatorName" runat="server" isRequired="true" Text="Operator Name"></cc1:ucLabel>
                                <asp:HiddenField ID="hdnIsUnexpDelPassAllow" runat="server" />
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td>
                                <%--<cc1:ucTextbox runat="server" ID="txtInitials" Width="70px" MaxLength="50"></cc1:ucTextbox>--%>
                                <%--<cc1:ucDropdownList ID="ddlOperators" runat="server" Width="150px" onchange="ShowHidePassword()">--%>
                                <cc1:ucDropdownList ID="ddlOperators" runat="server" Width="150px">
                                </cc1:ucDropdownList>
                                <asp:HiddenField ID="hdnLoggedInUserId" runat="server" />
                            </td>
                            <td colspan="3">
                                <table id="tblPassword" runat="server" width="100%" cellspacing="5" cellpadding="0"
                                    class="form-table">
                                    <tr>
                                        <td style="width: 10%;">
                                            <cc1:ucLabel ID="UcLabel1" runat="server" Text="Password" isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 1%;">
                                            :
                                        </td>
                                        <td class="nobold">
                                            <cc1:ucTextbox ID="txtPassword" runat="server" Width="150px" MaxLength="50" TextMode="Password"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%;">
                                <cc1:ucLabel ID="lblComments" runat="server" Text="Comments"></cc1:ucLabel>
                            </td>
                            <td style="width: 1%;">
                                :
                            </td>
                            <td class="nobold" colspan="4">
                                <cc1:ucTextarea ID="txtComments" Rows="3" Columns="20" runat="server"></cc1:ucTextarea>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <%--<table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold; width: 10%" align="right">
                                <cc1:ucLabel ID="lblPallets" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 9%">
                                <cc1:ucTextbox runat="server" ID="txtPallets" Width="70px" onkeyup="AllowNumbersOnly(this)"
                                    MaxLength="3"></cc1:ucTextbox>
                            </td>
                            <td style="font-weight: bold; width: 10%" align="right">
                                <cc1:ucLabel ID="lblCartons" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 9%">
                                <cc1:ucNumericTextbox runat="server" ID="txtCartons" Width="70px" onkeyup="AllowNumbersOnly(this)"
                                    MaxLength="4"></cc1:ucNumericTextbox>
                            </td>
                            <td style="font-weight: bold; width: 10%" align="right">
                                <cc1:ucLabel ID="lblLines_1" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 9%">
                                <cc1:ucNumericTextbox runat="server" ID="txtLines" Width="70px" onkeyup="AllowNumbersOnly(this)"
                                    MaxLength="3"></cc1:ucNumericTextbox>
                            </td>
                        </tr>
                    </table>--%>
                    <cc1:ucPanel ID="pnlPurchaseOrderDetail" runat="server" CssClass="fieldset-form">
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                            <tr>
                                <td style="font-weight: bold;" width="29%" valign="top">
                                    <cc1:ucLabel ID="lblPurchaseNumber" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%" valign="top">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 10%" valign="top">
                                    <cc1:ucTextbox ID="txtPurchaseNumber" runat="server" Width="70px"></cc1:ucTextbox>
                                </td>
                                <td style="font-weight: bold;" width="10%" valign="top">
                                    <cc1:ucButton ID="btnAddPO" runat="server" CssClass="button" OnClick="btnAddPO_Click"
                                        ValidationGroup="a" />
                                </td>
                                <td style="font-weight: bold;" width="6%" valign="top">
                                    &nbsp;
                                </td>
                                <td style="font-weight: bold; width: 9%" align="left">
                                    <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    :
                                </td>
                                <td style="width: 24%">
                                    <cc2:ucVendor ID="ucSearchVendor1" runat="server" />
                                </td>
                                <td style="width: 10%">
                                    <cc1:ucButton ID="btnAddVendor" runat="server" Text="Add Vendor" CssClass="button"
                                        OnClick="btnAddVendor_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9">
                                    <cc1:ucLabel ID="lblAllPORequired" runat="server" Text="( Please note all Purchase Orders / Delivery references are required)"></cc1:ucLabel>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                            <tr>
                                <td>
                                    <cc1:ucGridView ID="gvPO" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                        CellPadding="0" Width="100%" OnRowDataBound="gvPO_RowDataBound" OnRowDeleting="gvPO_RowDeleting"
                                        DataKeyNames="AutoID" OnSorting="gvPO_Sorting" AllowSorting="true">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:BoundField HeaderText="PO" DataField="PurchaseNumber" SortExpression="PurchaseNumber">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Expected Date" DataField="ExpectedDate" SortExpression="ExpectedDate">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" SortExpression="VendorName">
                                                <HeaderStyle Width="20%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="# Outstanding Lines on PO" DataField="OutstandingLines"
                                                SortExpression="OutstandingLines">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="# Stock outs" DataField="Qty_On_Hand" SortExpression="Qty_On_Hand">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="# Backorders" DataField="qty_on_backorder" SortExpression="qty_on_backorder">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Destination" DataField="SiteName" SortExpression="SiteName">
                                                <HeaderStyle Width="20%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:CommandField HeaderText="Remove" CausesValidation="false" ButtonType="Link"
                                                ShowDeleteButton="True" DeleteText="Remove" ItemStyle-HorizontalAlign="Center">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:CommandField>
                                        </Columns>
                                    </cc1:ucGridView>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <cc1:ucPanel ID="pnlVendor" runat="server" CssClass="fieldset-form">
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                            <tr>
                                <cc1:ucGridView ID="grdAddVendors" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                    Width="70%" OnRowDeleting="grdAddVendors_RowDeleting" DataKeyNames="AutoID" OnRowDataBound="grdAddVendors_RowDataBound">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Vendor">
                                            <HeaderStyle Width="45%" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLiteral ID="ltVendor" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                                <asp:HiddenField ID="hdnVendorID" runat="server" Value='<%#Eval("VendorID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pallets">
                                            <HeaderStyle Width="15%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucNumericTextbox ID="txtVendorPallets" runat="server" onkeyup="AllowNumbersOnly(this);"
                                                    MaxLength="3" Width="40px" Text='<%#Eval("Pallets") %>'></cc1:ucNumericTextbox>
                                                <asp:HiddenField ID="hdngvVendorID" runat="server" Value='<%#Eval("VendorID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cartons">
                                            <HeaderStyle Width="15%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucNumericTextbox ID="txtVendorCartons" runat="server" onkeyup="AllowNumbersOnly(this);"
                                                    MaxLength="4" Width="40px" Text='<%#Eval("Cartons") %>'></cc1:ucNumericTextbox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lines">
                                            <HeaderStyle Width="15%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucNumericTextbox ID="txtLinesToBeDelivered" runat="server" onkeyup="AllowNumbersOnly(this);"
                                                    MaxLength="3" Width="40px" Text='<%#Eval("Lines") %>'></cc1:ucNumericTextbox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField HeaderText="Remove" CausesValidation="false" ButtonType="Link"
                                            ShowDeleteButton="True" DeleteText="Remove" ItemStyle-HorizontalAlign="Center">
                                            <HeaderStyle Width="10%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:CommandField>
                                    </Columns>
                                </cc1:ucGridView>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row" style="text-align:center;">
                  <cc1:ucButton ID="btnAcceptandUnload" Width="135px" runat="server" Text="Accept and unload" CssClass="button"
                                            OnClick="btnAcceptandUnload_Click" ValidationGroup="a" />
                <cc1:ucButton ID="btnAccept" Width="100px" runat="server" Text="Accept" CssClass="button"
                    OnClick="btnAccept_Click" ValidationGroup="a" />
                <cc1:ucButton ID="btnReject" Width="100px" runat="server" Text="Reject" CssClass="button"
                    OnClick="btnReject_Click" ValidationGroup="a" />
                <cc1:ucButton ID="btnCancel" Width="100px" runat="server" Text="Cancel" CssClass="button"
                    OnClick="btnCancel_Click" />
            </div>           
            <table width="70%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"
                id="tblConnect" runat="server" style="display: none;">
                <tr>
                    <td style="font-weight: bold; width: 15%">
                        <cc1:ucLabel ID="lblConnectedToBooking" runat="server" Text="This Delivery is now connected with booking - "
                            isRequired="true"></cc1:ucLabel>
                        <asp:Literal ID="ltBooking" runat="server"></asp:Literal>
                        &nbsp;&nbsp;
                        <cc1:ucButton ID="btnDisconnectBooking" runat="server" Text="Disconnect Booking"
                            CssClass="button" OnClick="btnDisconnectBooking_Click" />
                    </td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <asp:UpdateProgress runat="server" ID="PageUpdateProgress" AssociatedUpdatePanelID="upCarrier">
                            <ProgressTemplate>
                                <div align="center" style="background-color: #000; top: 0px; left: 0px; bottom: 0px;
                                    right: 0px; padding-top: 20%; margin: 0; width: 100%; height: 80%; overflow: hidden;
                                    position: absolute; z-index: 1000; filter: alpha(opacity=50); opacity: 0.5;">
                                    <asp:Image ID="imgWait" runat="server" ImageUrl="~/Images/Ajaxloading.gif" ImageAlign="Middle" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%---WARNING popup start--%>
    <asp:UpdatePanel ID="updpnlWarning" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnConfirmMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlConfirmMsg" runat="server" TargetControlID="btnConfirmMsg"
                PopupControlID="pnlConfirmMsg" BackgroundCssClass="modalBackground" BehaviorID="ConfirmMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlConfirmMsg" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLiteral ID="ltConfirmMsg" runat="server" Text=""></cc1:ucLiteral>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnErrContinue" runat="server" Text="CONTINUE" CssClass="button"
                                    OnCommand="btnContinue_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnErrBack" runat="server" Text="BACK" CssClass="button" OnCommand="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnErrContinue" />
            <asp:AsyncPostBackTrigger ControlID="btnErrBack" />
        </Triggers>
    </asp:UpdatePanel>
    <%---WARNING popup End--%>
    <%---ERROR popup start--%>
    <asp:UpdatePanel ID="updpnlError" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnErrorMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlErrorMsg" runat="server" TargetControlID="btnErrorMsg"
                PopupControlID="pnlErrorMsg" BackgroundCssClass="modalBackground" BehaviorID="ErrorMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlErrorMsg" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLiteral ID="ltErrorMsg" runat="server" Text=""></cc1:ucLiteral>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnErrorMsgOK" runat="server" Text="OK" CssClass="button" OnCommand="btnErrorMsgOK_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnErrorMsgOK" />
        </Triggers>
    </asp:UpdatePanel>
    <%---ERROR popup End--%>
    <%---PO WARNING popup start--%>
    <asp:UpdatePanel ID="updpnlPOWarning" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnPOConfirmMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlPOConfirmMsg" runat="server" TargetControlID="btnPOConfirmMsg"
                PopupControlID="pnlPOConfirmMsg" BackgroundCssClass="modalBackground" BehaviorID="POConfirmMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlPOConfirmMsg" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td align="center">
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <h3>
                                            <cc1:ucLabel ID="lblOutstandingPO" runat="server"></cc1:ucLabel></h3>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <cc1:ucGridView ID="ucGridBooking" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                                            CellPadding="0" Width="700px">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle Width="20px" HorizontalAlign="Left" />
                                                    <ItemStyle Width="20px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnBookingdate" runat="server" Value='<%#Eval("ScheduleDate") %>' />
                                                         <asp:HiddenField ID="hdnSupplierType" runat="server" Value='<%#Eval("SupplierType") %>' />
                                                        <asp:HiddenField ID="hdnBookingID" runat="server" Value='<%#Eval("BookingID") %>' />
                                                        <asp:RadioButton ID="rdoBookingID" runat="server" GroupName="BookingNo" OnClick="javascript:SelectSingleRadiobutton(this.id)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Booking Ref" DataField="BookingRef">
                                                    <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Vendor Name">
                                                    <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLiteral ID="UcLiteral2" runat="server" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLiteral>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Carrier Name">
                                                    <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLiteral ID="UcLiteral3" runat="server" Text='<%#Eval("Carrier.CarrierName") %>'></cc1:ucLiteral>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="NumberOfPallet" DataField="NumberOfPallet">
                                                    <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="NumberOfCartons" DataField="NumberOfCartons">
                                                    <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="NumberOfLines" DataField="NumberOfLines">
                                                    <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                        </cc1:ucGridView>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <cc1:ucButton ID="btnConnectBooking" runat="server" Text="Connect Booking" CssClass="button"
                                        OnCommand="btnConnectBooking_Click" />
                                    &nbsp;
                                    <cc1:ucButton ID="btnDoNotConnectBooking" runat="server" Text="Do Not Connect Booking"
                                        CssClass="button" OnCommand="btnDoNotConnectBooking_Click" />
                                    &nbsp;
                                    <cc1:ucButton ID="btnPOErrBack" runat="server" Text="BACK" CssClass="button" OnCommand="btnPOBack_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnConnectBooking" EventName="Command" />
            <asp:AsyncPostBackTrigger ControlID="btnDoNotConnectBooking" />
            <asp:AsyncPostBackTrigger ControlID="btnPOErrBack" />
        </Triggers>
    </asp:UpdatePanel>
    <%---WARNING popup End--%>
    <%---Today Booking info popup start--%>
    <asp:UpdatePanel ID="updpnlTodayBooking" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTodayBookingMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlTodayBookingMsg" runat="server" TargetControlID="btnTodayBookingMsg"
                PopupControlID="pnlTodayBookingMsg" BackgroundCssClass="modalBackground" BehaviorID="TodayBookingMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlTodayBookingMsg" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblTodayBooking" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <cc1:ucButton ID="btnOK" runat="server" Text="OK" CssClass="button" OnCommand="btnPOBack_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOK" />
        </Triggers>
    </asp:UpdatePanel>

     <asp:UpdatePanel ID="upClosedPOErrorPopup" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnClosedPOErrorPopup" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlClosedPOErrorMsg" runat="server" TargetControlID="btnClosedPOErrorPopup"
                PopupControlID="pnlClosedPOErrorPopup" BackgroundCssClass="modalBackground" BehaviorID="ClosedPOErrorMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlClosedPOErrorPopup" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: center;">
                    <table cellspacing="5" cellpadding="0" border="0"  class="top-setting-Popup">
                        <tr>
                        <td>
                         <table cellspacing="5" cellpadding="0" border="0"  class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLiteral ID="ltPurchaseOrderClosedMsg" runat="server" ></cc1:ucLiteral>
                            </td>
                        </tr>
                            <tr align="center">
                                <td>
                                    <table cellspacing="5" cellpadding="0" border="0"  class="top-setting-Popup">
                                     <tr>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLiteral ID="ltPO" runat="server" Text="PO"></cc1:ucLiteral>
                                        </td>
                                        <td style="font-weight: bold;">
                                            :
                                        </td>
                                        <td style="font-weight: bold;">
                                            <cc1:ucLiteral ID="ltPOvalue" runat="server" ></cc1:ucLiteral>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltVendor" runat="server" Text="Vendor"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                :
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltVendorName" runat="server" ></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltCreatedOn" runat="server" Text="Created On"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                :
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLiteral ID="ltCreatedDate" runat="server" ></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                    </table>
                               </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                        <cc1:ucLiteral ID="ltAddPOMsg" runat="server" ></cc1:ucLiteral>
                                    </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                        <cc1:ucLiteral ID="ltAddPoToVendorMsg" runat="server" ></cc1:ucLiteral>
                                    </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                        <cc1:ucLiteral ID="ltAddPoToUnknownVendorMsg" runat="server"></cc1:ucLiteral>
                                       
                                    </td>
                            </tr>  
                            </table>                                             
                         <tr>
                            <td align="center">
                                <div class="row">
                                 <br />
                                    <cc1:ucButton ID="btnAddPoPopup" runat="server" Text="Add PO" CssClass="button" OnCommand="btnAddPoPopup_Click" />&nbsp;&nbsp;&nbsp;
                                     <cc1:ucButton ID="btnNewVendor" runat="server" Text="New Vendor" CssClass="button" OnCommand="btnNewVendor_Click" />&nbsp;&nbsp;&nbsp;
                                      <cc1:ucButton ID="btnUnknownVendor" runat="server" Text="Unknown Vendor" CssClass="button" OnCommand="btnUnknownVendor_Click" />&nbsp;&nbsp;&nbsp;
                                       <cc1:ucButton ID="btnBack_1" runat="server" Text="Back" CssClass="button" OnCommand="btnBack_1_Click" />
                                </div>
                            </td>
                        </tr>
                        </td>
                        </tr>
                    </table>
                     
                </div>
            </asp:Panel>                 
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddPoPopup" />
            <asp:AsyncPostBackTrigger ControlID="btnNewVendor" />
            <asp:AsyncPostBackTrigger ControlID="btnUnknownVendor" />
            <asp:AsyncPostBackTrigger ControlID="btnBack_1" />
            
            <asp:AsyncPostBackTrigger ControlID="btnAddPO" />
            <asp:AsyncPostBackTrigger ControlID="btnAccept" />
            <asp:AsyncPostBackTrigger ControlID="btnReject" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel" />
        </Triggers>
    </asp:UpdatePanel>


     <asp:UpdatePanel ID="upUnknownPOErrorMsg" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnUnknownPOErrorMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlUnknownPOErrorMsg" runat="server" TargetControlID="btnUnknownPOErrorMsg"
                PopupControlID="pnlUnknownPOErrorMsg" BackgroundCssClass="modalBackground" BehaviorID="UnknownPOErrorMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlUnknownPOErrorMsg" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: center;">
                    <table cellspacing="5" cellpadding="0" border="0"  class="top-setting-Popup">
                        <tr>
                            <td>
                                <table cellspacing="5" cellpadding="0" border="0"  class="top-setting-Popup">
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLiteral ID="ltPOAddedIsUnknownMsg" runat="server" ></cc1:ucLiteral>                                       
                                    </td>
                                 </tr>
                                 </table>
                             </td>
                          </tr>
                          <tr>
                             <td align="center">
                                <div class="row">
                                <br />
                                    <cc1:ucButton ID="btnSelectVendor" runat="server" Text="Select Vendor" CssClass="button" OnCommand="btnSelectVendor_Click" />&nbsp;&nbsp;&nbsp;
                                        <cc1:ucButton ID="btnUnknownVendor_1" runat="server" Text="Unknown Vendor" CssClass="button" OnCommand="btnUnknownVendor_1_Click" />&nbsp;&nbsp;&nbsp;
                                        <cc1:ucButton ID="btnBack_2" runat="server" Text="Unknown Vendor" CssClass="button" OnCommand="btnBack_2_Click" />                                
                                </div>
                              </td>
                           </tr>
                        </table>   
                </div>
            </asp:Panel>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSelectVendor" />
            <asp:AsyncPostBackTrigger ControlID="btnUnknownVendor_1" />
            <asp:AsyncPostBackTrigger ControlID="btnBack_2" />            
            <asp:AsyncPostBackTrigger ControlID="btnAddPO" />
        </Triggers>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="upViewVendor" runat="server">
                <ContentTemplate>
                             <asp:Button ID="btnViewVendor_1" runat="Server" Style="display: none" />
                            <ajaxtoolkit:modalpopupextender ID="mdlVendorViewer_1" runat="server" TargetControlID="btnViewVendor_1"
                                PopupControlID="pnlbtnViewVendorViewer_1" 
                                 BackgroundCssClass="modalBackground" 
                                DropShadow="false" />


                             <asp:Panel ID="pnlbtnViewVendorViewer_1" runat="server"  Style="display:none; width: 500px;">
                                 <!--Romoved VendorViewer ancor tag from here-->
                                <div style="width: 100%; overflow-y: hidden; overflow-x: hidden; background-color: #fff;
                                    padding: 5px; border: 2px solid #ccc; text-align: left;">
                                    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                                        <tr>
                                            <td style="font-weight: bold;" width="25%">
                                                <cc1:ucLabel ID="lblVendorName" runat="server" Text="Vendor Name"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" width="5%">
                                                :
                                            </td>
                                            <td width="50%">
                                                <cc1:ucTextbox ID="txtVenderName2" runat="server" Width="147px"></cc1:ucTextbox>
                                            </td>
                                            <td width="20%">
                                                <cc1:ucButton OnClick="btnSearch_Click" ID="btnSearch" runat="server" 
                                                    Text="Search" CssClass="button" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                &nbsp;
                                            </td>
                                            <td style="font-weight: bold;" align="left" colspan="2">                           
                                                <cc1:ucListBox ID="lstVendor" runat="server" Width="99%" Height="200px" ondblclick="lstVendor_DoubleClick" ></cc1:ucListBox>
                                                </td>                      
                                            </tr>
                                            <tr>
                                                    <td colspan="4" align="center">
                                                <cc1:ucButton OnClick="btnSelect_Click" ID="btnSelect" runat="server"  
                                                    Text="Select" CssClass="button" Width="60px" />
                                                &nbsp;
                                                <cc1:ucButton OnClick="btnPopupCancel_Click" ID="btnPopupCancel" runat="server" 
                                                    Text="Cancel" CssClass="button" Width="60px" />
                                                </td>
                                            </tr>
                                                <tr>
                                                    <td colspan="4">&nbsp;</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </asp:Panel>
                                 </ContentTemplate>

                                 <Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="btnSearch" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSelect" />
                                    <asp:AsyncPostBackTrigger ControlID="btnPopupCancel" />
                                 </Triggers>
                          </asp:UpdatePanel>



    <%---WARNING popup End--%>
</asp:Content>

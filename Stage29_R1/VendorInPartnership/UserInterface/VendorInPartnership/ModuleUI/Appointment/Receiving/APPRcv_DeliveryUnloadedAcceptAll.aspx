﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPRcv_DeliveryUnloadedAcceptAll.aspx.cs" Inherits="ModuleUI_Appointment_Receiving_APPRcv_DeliveryUnloadedAcceptAll" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<script language="javascript" type="text/javascript">

 

    function calculate(obj, arg) {
        //if (obj.value != '') {

            if (arg == '1') {

                var totalAcceptedPallets = document.getElementById('<%=lblArrivedPalletsTotal.ClientID %>');
                var total = 0;
                var Parent = document.getElementById("<%=grdAcceptAll.ClientID%>");
                var items = Parent.getElementsByTagName('input');
                for (i = 0; i < items.length; i++) {
                    var len = items[i].id.split('_').length;
                    if (items[i].id.split('_')[len - 1] == 'txtArrivedPallets') {

                        var objVal = document.getElementById(items[i].id).value;
                        if (document.getElementById(items[i].id).value != null && document.getElementById(items[i].id).value != '')
                            total = parseInt(total) + parseInt(objVal);
                    }
                }
                totalAcceptedPallets.innerText = total;
                document.getElementById('<%=hdnTotalPallets.ClientID %>').value = total;
               
               
            }

            if (arg == '2') {

                var totalAcceptedCartons = document.getElementById('<%=lblArrivedCartonsTotal.ClientID %>');
                var total = 0;
                var Parent = document.getElementById("<%=grdAcceptAll.ClientID%>");
                var items = Parent.getElementsByTagName('input');
                for (i = 0; i < items.length; i++) {
                    var len = items[i].id.split('_').length;
                    if (items[i].id.split('_')[len - 1] == 'txtArrivedCartons') {

                        var objVal = document.getElementById(items[i].id).value;
                        if (document.getElementById(items[i].id).value != null && document.getElementById(items[i].id).value != '')
                            total = parseInt(total) + parseInt(objVal);
                    }
                }
                totalAcceptedCartons.innerText = total;
            }

            if (arg == '3') {

                var totalRefusedPallets = document.getElementById('<%=lblArrivedLiftsTotal.ClientID %>');
                var total = 0;
                var Parent = document.getElementById("<%=grdAcceptAll.ClientID%>");
                var items = Parent.getElementsByTagName('input');
                for (i = 0; i < items.length; i++) {
                    var len = items[i].id.split('_').length;
                    if (items[i].id.split('_')[len - 1] == 'txtArrivedLifts') {

                        var objVal = document.getElementById(items[i].id).value;
                        if (document.getElementById(items[i].id).value != null && document.getElementById(items[i].id).value != '')
                            total = parseInt(total) + parseInt(objVal);
                    }
                }
                totalRefusedPallets.innerText = total;
            }

        }
       

 
    


    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblDeliveryDetails" runat="server" Text="Delivery Uploaded - Accept All"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucPanel ID="pnlBookingDetailsArrival" GroupingText="Booking Details" runat="server"
                        CssClass="fieldset-form">
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <tr id="trVendorCarrier" runat="server">
                                <td style="width: 4%;">
                                    <cc1:ucLabel ID="lblVendorBooking" runat="server" Text="Vendor" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 20%;" class="nobold">
                                    <cc1:ucLabel ID="VendorNameData" runat="server" />
                                </td>
                                <td style="width: 4%;">
                                    <cc1:ucLabel ID="lblCarrierBooking" runat="server" Text="Carrier" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 20%;" class="nobold" colspan="6">
                                    <cc1:ucLabel ID="CarrierNameData" runat="server" Text="ANC" />
                                </td>
                            </tr>
                            <tr id="trCarrier" runat="server">
                                <td style="width: 4%;">
                                    <cc1:ucLabel ID="lblCarrierBooking1" runat="server" Text="Carrier" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 20%;" class="nobold" colspan="9">
                                    <cc1:ucLabel ID="CarrierNameData1" runat="server" Text="ANC" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 4%;">
                                    <cc1:ucLabel ID="lblDateBooking" runat="server" Text="Date" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 20%;" class="nobold">
                                    <cc1:ucLabel ID="BookingDateData" runat="server" Text="" />
                                    &nbsp;
                                    <cc1:ucLabel ID="BookingTimeData" runat="server" Text="" />
                                </td>
                                <td style="width: 10%;">
                                    <cc1:ucLabel ID="lblVehicleType" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 14%;" class="nobold">
                                    <cc1:ucLabel ID="VehicleTypeData" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 10%;">
                                    <cc1:ucLabel ID="lblDoorName" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 14%;" class="nobold">
                                    <cc1:ucLabel ID="DoorNumberData" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 4%;">
                                </td>
                                <td style="width: 1%;">
                                </td>
                                <td style="width: 20%;" class="nobold">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblPalletsBooking" runat="server" Text="Pallets" />
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 100px;" class="nobold">
                                    <cc1:ucLabel ID="BookingPalletsData" runat="server" Text="20" />
                                </td>
                                <td>
                                    <cc1:ucLabel ID="lblCartons" runat="server" Text="Cartons" />
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 100px;" class="nobold">
                                    <cc1:ucLabel ID="BookingCartonsData" runat="server" Text="0" />
                                </td>
                                <td style="width: 100px;">
                                    <cc1:ucLabel ID="lblLines" runat="server" Text="Lines" />
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 100px;" class="nobold">
                                    <cc1:ucLabel ID="BookingLinesData" runat="server" Text="16" />
                                </td>
                                <td>
                                    <cc1:ucLabel ID="lblLiftsBooking" runat="server" Text="Lifts" />
                                </td>
                                <td>
                                    :
                                </td>
                                <td style="width: 100px;" class="nobold">
                                    <cc1:ucLabel ID="BookingLiftsData" runat="server" Text="31" />
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <cc1:ucPanel ID="pnlArrivalDetails" GroupingText="Arrival Details" runat="server"
                        CssClass="fieldset-form">
                        <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                            <tr>
                                <td style="width: 10%;">
                                    <cc1:ucLabel ID="lblVehicleTypeArrival" runat="server" Text="Vehicle Type" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 39%;" class="nobold">
                                    <cc1:ucDropdownList ID="ddlVehicleType" runat="server" Width="150px" AutoPostBack="true">
                                    </cc1:ucDropdownList>
                                </td>
                                <td style="width: 10%;">
                                    <cc1:ucLabel ID="lblDate" runat="server" Text="Date" />
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td style="width: 39%;" class="nobold" colspan="6">
                                    <cc1:ucLabel ID="UnloadDateValue" runat="server" Text="" Width="100%" />
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <cc1:ucPanel ID="pnlVolumeDetails" GroupingText="Volume Details" runat="server" CssClass="fieldset-form">
                        <cc1:ucGridView ID="grdAcceptAll" Width="100%" runat="server" CssClass="grid" ShowFooter="true"  onsorting="SortGrid" AllowSorting="true">
                            <Columns>
                                <asp:TemplateField HeaderText="Vendor" SortExpression="FixedSlot.Vendor.VendorName">
                                    <HeaderStyle Width="25%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral runat="server" ID="lblVendorName" Text='<%# Eval("FixedSlot.Vendor.VendorName") %>'></cc1:ucLiteral>
                                        <cc1:ucLiteral runat="server" ID="lblVendorId" Visible="false" Text='<%# Eval("FixedSlot.VendorID") %>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expected Pallets" SortExpression="FixedSlot.MaximumPallets">
                                    <HeaderStyle Width="12%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="LBLExpectedPallets" Text='<%# Eval("FixedSlot.MaximumPallets") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expected Cartons" SortExpression="FixedSlot.MaximumCatrons">
                                    <HeaderStyle Width="12%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="LBLExpectedCartons" Text='<%# Eval("FixedSlot.MaximumCatrons") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expected Lifts" SortExpression="LiftsScheduled">
                                    <HeaderStyle Width="12%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="LBLExpectedLifts" Text='<%# Eval("LiftsScheduled") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Arrived Pallets">
                                    <HeaderStyle Width="13%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucTextbox runat="server" ID="txtArrivedPallets" Text='<%# Eval("FixedSlot.MaximumPallets") %>' style="text-align:center;"  Width="30px" MaxLength="3" onblur="calculate(this,'1');" onkeyup="AllowNumbersOnly(this);"></cc1:ucTextbox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Arrived Cartons">
                                    <HeaderStyle Width="13%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucTextbox runat="server" ID="txtArrivedCartons" Text='<%# Eval("FixedSlot.MaximumCatrons") %>' style="text-align:center;" Width="30px" MaxLength="4" onblur="calculate(this,'2');" onkeyup="AllowNumbersOnly(this);"></cc1:ucTextbox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Arrived Lifts">
                                    <HeaderStyle Width="13%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucTextbox runat="server" ID="txtArrivedLifts"  Text='<%# Eval("LiftsScheduled") %>' style="text-align:center;" Width="30px" MaxLength="3" onblur="calculate(this,'3');" onkeyup="AllowNumbersOnly(this);"></cc1:ucTextbox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                        <table width="100%" >
                            <tr>
                                <td style="width: 25%;">
                                    <cc1:ucLabel runat="server" ID="lblTotal" Text="Total" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                <td style="width: 12%;" align="center" >
                                    <cc1:ucLabel runat="server" ID="lblExpectedPalletsTotal" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                <td style="width: 12%;" align="center">
                                    <cc1:ucLabel runat="server" ID="lblExpectedCartonsTotal" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                <td style="width: 12%;" align="center">
                                    <cc1:ucLabel runat="server" ID="lblExpectedLiftsTotal" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                <td style="width: 13%;" align="center">
                                    <cc1:ucLabel runat="server" ID="lblArrivedPalletsTotal" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                <td style="width: 13%;" align="center">
                                    <cc1:ucLabel runat="server" ID="lblArrivedCartonsTotal" Font-Bold="true"></cc1:ucLabel>
                                </td>
                                <td style="width: 13%;" align="center">
                                    <cc1:ucLabel runat="server" ID="lblArrivedLiftsTotal" Font-Bold="true"></cc1:ucLabel>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnProceed" runat="server" Text="Proceed" CssClass="button" OnClick="btnProceed_Click" />
                <cc1:ucButton ID="btnExit" runat="server" Text="Exit" CssClass="button" 
                    onclick="btnExit_Click" />
                    <asp:HiddenField ID="hdnTotalPallets" runat="server" Value="0" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

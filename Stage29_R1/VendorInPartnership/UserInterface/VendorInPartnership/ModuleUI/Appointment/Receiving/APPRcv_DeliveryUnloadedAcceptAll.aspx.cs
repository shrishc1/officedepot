﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using Utilities; using WebUtilities;


using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.IO;
using System.Configuration;

public partial class ModuleUI_Appointment_Receiving_APPRcv_DeliveryUnloadedAcceptAll : CommonPage
{

    protected string VolumeDetailsValidation = WebCommon.getGlobalResourceValue("VolumeDetailsValidation");
    protected int Siteid;
    protected void Page_Load(object sender, EventArgs e)
    {
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = "Appointment Scheduling - Booking";
        if (!IsPostBack)
        {
            BindVehicleType();
            if (GetQueryStringValue("ID") != null && GetQueryStringValue("ID").ToString() != "")
            {
                if (ExtractInformation(GetQueryStringValue("ID").ToString(), "type").ToLower() == "c")
                {
                    lblCarrierBooking.Style["display"] = "none";
                    CarrierNameData.Style["display"] = "none";
                }
                else
                {
                    lblCarrierBooking.Style["display"] = "";
                    CarrierNameData.Style["display"] = "";
                }
                BookingDetails(Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id")));
            }           
        }
    }



    public string ExtractInformation(string pFullData, string pReturnType)
    {
        int i = -1;
        if (pReturnType == "table")
            i = 0;
        else if (pReturnType == "type")
            i = 1;
        else if (pReturnType == "id")
            i = 2;
        else if (pReturnType == "siteid")
            i = 3;
        return (pFullData.Split('-')[i].ToString());
    }

    public void BookingDetails(int pPkID)
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "DeliveryArrived";
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(GetQueryStringValue("Scheduledate").ToString());

        List<APPBOK_BookingBE> lstDeliveryArrived = oAPPBOK_BookingBAL.GetBookingDetailsBAL(oAPPBOK_BookingBE);
        grdAcceptAll.DataSource = lstDeliveryArrived;
        grdAcceptAll.DataBind();

        ViewState["lstSites"] = lstDeliveryArrived;

        if (lstDeliveryArrived != null && lstDeliveryArrived.Count > 0)
        {
            ddlVehicleType.SelectedIndex = ddlVehicleType.Items.IndexOf(ddlVehicleType.Items.FindByValue(lstDeliveryArrived[0].VehicleType.VehicleTypeID.ToString()));
            Siteid = Convert.ToInt32(lstDeliveryArrived[0].SiteId);
            if (ExtractInformation(GetQueryStringValue("ID").ToString(), "type").ToLower() == "c")
            {
                trCarrier.Style["display"] = "";
                trVendorCarrier.Style["display"] = "none";
                CarrierNameData1.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
            }
            else
            {
                trCarrier.Style["display"] = "none";
                trVendorCarrier.Style["display"] = "";
                VendorNameData.Text = lstDeliveryArrived[0].FixedSlot.Vendor.VendorName.ToString();
                CarrierNameData.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
            }
            BookingDateData.Text = GetQueryStringValue("Scheduledate").ToString();
            if (!string.IsNullOrEmpty(lstDeliveryArrived[0].ExpectedDeliveryTime))
                BookingTimeData.Text = "- " + lstDeliveryArrived[0].ExpectedDeliveryTime.ToString();
            VehicleTypeData.Text = lstDeliveryArrived[0].VehicleType.VehicleType.ToString();
            DoorNumberData.Text = lstDeliveryArrived[0].DoorNoSetup.DoorNumber.ToString();

            DateTime? Unloadtime = lstDeliveryArrived[0].DLYUNL_DateTime;
            if (Unloadtime != null)
            UnloadDateValue.Text = Common.GetDD_MM_YYYY(lstDeliveryArrived[0].DLYUNL_DateTime.ToString().Substring(0, 10) + "-" + Unloadtime.Value.Hour.ToString() + ":" + Unloadtime.Value.Minute.ToString());


            /*  if (lstDeliveryArrived.Count == 1)
              {
                  BookingLiftsData.Text = lstDeliveryArrived[0].LiftsScheduled.ToString();
                  BookingLinesData.Text = lstDeliveryArrived[0].FixedSlot.MaximumLines.ToString();
                  BookingPalletsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumPallets.ToString();
                  BookingCartonsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumCatrons.ToString();
              }
              else
              {*/
            int? iLifts = 0, iLines = 0, iPallets = 0, iCartons = 0;
            for (int i = 0; i < lstDeliveryArrived.Count; i++)
            {
                iLifts += lstDeliveryArrived[i].LiftsScheduled;
                iLines += lstDeliveryArrived[i].FixedSlot.MaximumLines;
                iPallets += lstDeliveryArrived[i].FixedSlot.MaximumPallets;
                iCartons += lstDeliveryArrived[i].FixedSlot.MaximumCatrons;
            }
            BookingLiftsData.Text = iLifts.ToString();
            BookingLinesData.Text = iLines.ToString();
            BookingPalletsData.Text = iPallets.ToString();
            BookingCartonsData.Text = iCartons.ToString();
            lblExpectedPalletsTotal.Text = iPallets.ToString();
            lblExpectedCartonsTotal.Text = iCartons.ToString();
            lblExpectedLiftsTotal.Text = iLifts.ToString();
            lblArrivedPalletsTotal.Text = iPallets.ToString();
            lblArrivedCartonsTotal.Text = iCartons.ToString();
            lblArrivedLiftsTotal.Text = iLifts.ToString();
            //}
        }
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        string ArrivedPallets = string.Empty;
        int iCount = 0;
        foreach (GridViewRow gv in grdAcceptAll.Rows)
        {
            TextBox txtArrivedPallets = (TextBox)gv.FindControl("txtArrivedPallets");
            TextBox txtArrivedCartons = (TextBox)gv.FindControl("txtArrivedCartons");
            TextBox txtArrivedLifts = (TextBox)gv.FindControl("txtArrivedLifts");
            if (txtArrivedPallets.Text.Trim() == ""
                && txtArrivedCartons.Text.Trim() == ""
                && txtArrivedLifts.Text.Trim() == "")
            {
                iCount++;
                break;
            }
        }
        if (iCount > 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + VolumeDetailsValidation + "')", true);
            return;
        }
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Action = "UpdateDeliveryUnloadedAcceptAll";

        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        foreach (GridViewRow gv in grdAcceptAll.Rows)
        {
            Literal ltlVendorid = (Literal)gv.FindControl("lblVendorId");
            TextBox txtArrivedPallets = (TextBox)gv.FindControl("txtArrivedPallets");
            TextBox txtArrivedCartons = (TextBox)gv.FindControl("txtArrivedCartons");
            TextBox txtArrivedLifts = (TextBox)gv.FindControl("txtArrivedLifts");

            string VendorId = ltlVendorid.Text.ToString();
            ArrivedPallets = txtArrivedPallets.Text.ToString();
            string ArrivedCartons = txtArrivedCartons.Text.ToString();
            string ArrivedLifts = txtArrivedLifts.Text.ToString();
            if (!string.IsNullOrEmpty(VendorId))
            oAPPBOK_BookingBE.FixedSlot.VendorID = Convert.ToInt32(VendorId);
            oAPPBOK_BookingBE.DLYUNL_NumberOfCartons = ArrivedCartons != string.Empty ? Convert.ToInt32(ArrivedCartons) : (int?)null;
            oAPPBOK_BookingBE.DLYUNL_NumberOfPallet = ArrivedPallets != string.Empty ? Convert.ToInt32(ArrivedPallets) : (int?)null;
            oAPPBOK_BookingBE.DLYUNL_NumberOfLift = ArrivedLifts != string.Empty ? Convert.ToInt32(ArrivedLifts) : (int?)null;
            oAPPBOK_BookingBE.BookingStatusID = Convert.ToInt32(Common.BookingStatus.DeliveryUnloaded);
            oAPPBOK_BookingBE.FixedSlot.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oAPPBOK_BookingBE.FixedSlot.User.UserID = Convert.ToInt32(Session["UserID"].ToString().Trim());
            
            // Sprint 1 - Point 8 - Start
            if (Session["TransactionComments"] != null && Session["TransactionComments"].ToString() != "")
            {
                oAPPBOK_BookingBE.TransactionComments = Session["TransactionComments"].ToString().Trim();
            }
            // Sprint 1 - Point 8 - End

            oAPPBOK_BookingBAL.updateDeliveryUnloadedAcceptAllBAL(oAPPBOK_BookingBE);

            // Sprint 1 - Point 8 - Start
            Session.Remove("TransactionComments");
        }
        // send mail for delivery confirmtion
        //  SendMail(); // not neccessary as no template given by client..mail will be sent on Delivery issues from Reject all page..

        string TotalPallets = string.Empty;
        if (hdnTotalPallets.Value != "0") {
            TotalPallets = hdnTotalPallets.Value;
        }
        else {
            TotalPallets = ArrivedPallets;
        }

        string strDelUnloadPassAllow = string.Empty;
        if (GetQueryStringValue("IsDelUnloadPassAllow") != null)
            strDelUnloadPassAllow = GetQueryStringValue("IsDelUnloadPassAllow");

        EncryptQueryString("APPRcv_DeliveryUnloadedRejectAll.aspx?Scheduledate="
           + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString()
           + "&UNL=FULL" + "&TotalPallets=" + TotalPallets
           + "&IsDelUnloadPassAllow=" + strDelUnloadPassAllow);
    }

    private void BindVehicleType()
    {
        MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

        oMASSIT_VehicleTypeBE.Action = "ShowAll";
        oMASSIT_VehicleTypeBE.SiteID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "siteid")); 

        List<MASSIT_VehicleTypeBE> lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsBAL(oMASSIT_VehicleTypeBE);

        if (lstVehicleType.Count > 0)
        {
            FillControls.FillDropDown(ref ddlVehicleType, lstVehicleType, "VehicleType", "VehicleTypeID");
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<APPBOK_BookingBE>.SortList((List<APPBOK_BookingBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnExit_Click(object sender, EventArgs e)
    {
        EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx?Scheduledate="
           + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString());
    }

    private void SendMail()
    {
        //try
        //{
        //    APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        //    APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
        //    oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
        //    oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));
        //    List<APPBOK_BookingBE> lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);
        //    if (lstBooking.Count > 0)
        //    {

        //        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        //        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
        //        string VendorEmail = string.Empty;
        //        List<MASSIT_VendorBE> lstVendorDetails = null;

        //        if (lstBooking[0].SupplierType == "C")
        //        {
        //            oMASSIT_VendorBE.Action = "GetCarrierEmail";
        //            oMASSIT_VendorBE.SiteVendorID = lstBooking[0].Carrier.CarrierID;

        //            lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        //            foreach (MASSIT_VendorBE item in lstVendorDetails)
        //            {
        //                VendorEmail += item.Vendor.VendorContactEmail.ToString() + ", ";
        //            }
        //        }
        //        else if (lstBooking[0].SupplierType == "V")
        //        {
        //            oMASSIT_VendorBE.Action = "GetVendorEmail";
        //            oMASSIT_VendorBE.SiteVendorID = lstBooking[0].VendorID;

        //            lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);
        //            if (lstVendorDetails != null && lstVendorDetails.Count > 0)
        //            {

        //                foreach (MASSIT_VendorBE item in lstVendorDetails)
        //                {
        //                    VendorEmail += item.Vendor.VendorContactEmail.ToString() + ", ";
        //                }
        //            }
        //            else
        //            {
        //                oMASSIT_VendorBE.Action = "GetContactDetails";
        //                oMASSIT_VendorBE.SiteVendorID = lstBooking[0].VendorID;

        //                lstVendorDetails = oAPPSIT_VendorBAL.GetVendorContactDetailsBAL(oMASSIT_VendorBE);

        //                foreach (MASSIT_VendorBE item in lstVendorDetails)
        //                {
        //                    VendorEmail += item.Vendor.VendorContactEmail.ToString() + ", ";
        //                }
        //            }
        //        }

        //        string htmlBody = string.Empty;
        //        string templatesPath = "emailtemplates/Appointment";
        //        string templateFile = "~/" + templatesPath + "/";
        //        templateFile += "AcceptAll.english.htm";
        //        sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
        //        if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower())))
        //        {
        //            using (StreamReader sReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(templateFile.ToLower())))
        //            {
        //                #region mailBody
        //                htmlBody = sReader.ReadToEnd();
        //                htmlBody = htmlBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
        //                htmlBody = htmlBody.Replace("{BookingRef}", lstBooking[0].BookingRef);
        //                htmlBody = htmlBody.Replace("{Date}", lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy"));
        //                htmlBody = htmlBody.Replace("{Site}", lstBooking[0].SiteName);
        //                //htmlBody = htmlBody.Replace("{Issue}", Issues);

        //                #endregion
        //            }
        //        }

        //        clsEmail mail = new clsEmail();
        //        System.Net.Mail.MailAddressCollection mailAddress = new System.Net.Mail.MailAddressCollection();
        //        string[] sMailAddress = VendorEmail.Split(',');
        //        for (int i = 0; i < sMailAddress.Length - 1; i++)
        //            mailAddress.Add(sMailAddress[i]);
        //        string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
        //        mail.toAdd(mailAddress);
        //        mail.EmailMessageText = htmlBody;
        //        mail.EmailSubjectText = "Your Delivery has been Accepted";
        //        mail.FromEmail = sFromAddress;
        //        mail.FromName = "Office Depot Receiving - Do not reply.";
        //        mail.IsHTMLMail = true;
        //        mail.SendMail();
        //    }
        //}
        //catch { }
    }
}
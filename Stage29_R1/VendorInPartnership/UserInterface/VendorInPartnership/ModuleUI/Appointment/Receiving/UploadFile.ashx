﻿<%@ WebHandler Language="C#" Class="UploadFile" %>

using System;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Collections.Generic;

public class UploadFile: IHttpHandler, IRequiresSessionState {
    public void ProcessRequest(HttpContext context) {
        string filedata = string.Empty;
        string strGuid = string.Empty;
        List<AttachedImagesList> lstImages = new List<AttachedImagesList>();
        if (context.Session["PathImage"] != null)
        {
            lstImages = (List<AttachedImagesList>)context.Session["PathImage"];
            strGuid = lstImages[0].Guid;
        }
        else
        {
            strGuid = Guid.NewGuid().ToString() + "_";
        }
        string sImageAlreadyAdded = WebUtilities.WebCommon.getGlobalResourceValue("ImageAlreadyAdded");
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                if (Path.GetExtension(file.FileName).ToLower() != ".jpg" &&
                    Path.GetExtension(file.FileName).ToLower() != ".png" &&
                    Path.GetExtension(file.FileName).ToLower() != ".gif" &&
                    Path.GetExtension(file.FileName).ToLower() != ".jpeg" &&
                    Path.GetExtension(file.FileName).ToLower() != ".pdf"
                )
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("Only jpg, png , gif, .jpeg, .pdf are allowed.!");
                    return;
                }

                if(lstImages.Exists(x => x.ImagePath == strGuid + file.FileName))
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(sImageAlreadyAdded);
                    return;
                }
            }


        }
        if (context.Request.Files.Count > 0) {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++) {
                HttpPostedFile file = files[i];

                string fname;
                if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER") {
                    string[] testfiles = file.FileName.Split(new char[] {
                        '\\'
                    });
                    fname = testfiles[testfiles.Length - 1];
                } else {
                    fname = file.FileName;
                }
                //here UploadFile is define my folder name , where files will be store.  
                string uploaddir = AppDomain.CurrentDomain.BaseDirectory.ToLower(); 
                filedata =  fname; 
                var FileImageName = strGuid + fname.Trim().Replace('+', '-').Replace("&", "and");
                file.SaveAs(uploaddir + "Images/DeliveryUnload/" +  FileImageName);

                lstImages.Add(new AttachedImagesList { ImagePath = FileImageName, Guid = strGuid });
            }
        }
        context.Response.ContentType = "text/plain";
        context.Response.Write("File Uploaded Successfully:" + filedata + "!");
        //if you want to use file path in aspx.cs page , then assign it in to session  
        context.Session["PathImage"] = lstImages;
        //return lstImages;
    }
    public bool IsReusable {
        get {
            return false;
        }
    }    
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="APPRcv_DeliveryUnloaded.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="APPRcv_DeliveryUnloaded" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript">
    function ShowHidePassword() {
        var LoggedInUserId = document.getElementById('<%= hdnLoggedInUserId.ClientID %>').value;
        var SelectedUserID = document.getElementById('<%= ddlOperators.ClientID %>').value;
        var IsDelUnloadPassAllow = document.getElementById('<%= hdnIsDelUnloadPassAllow.ClientID %>').value;

        /*if (SelectedUserID != LoggedInUserId && IsDelUnloadPassAllow == 'Y')*/
        if (SelectedUserID != LoggedInUserId) {
            document.getElementById('<%=tblPassword.ClientID%>').style.display = "";
            ValidatorEnable(document.getElementById('<%=rfvOperatorPasswordRequired.ClientID%>'), true);
        }
        else {
            document.getElementById('<%=tblPassword.ClientID%>').style.display = "none";
            ValidatorEnable(document.getElementById('<%=rfvOperatorPasswordRequired.ClientID%>'), false);
        }
    }
</script>

    <div>
        <asp:RequiredFieldValidator ID="rfvOperatorPasswordRequired" 
                    runat="server" ValidationGroup="Save" 
                    Display="None" ControlToValidate="txtPassword">
                </asp:RequiredFieldValidator>
        <%--<asp:RequiredFieldValidator ID="rfvOperatorNameRequired" runat="server" ValidationGroup="Save"
            Display="None" ControlToValidate="txtOperatorInitials">
        </asp:RequiredFieldValidator>--%>
        <asp:ValidationSummary ID="vs1" runat="server" ShowMessageBox="true" ValidationGroup="Save"
            ShowSummary="false" />
    </div>
    <script language="javascript" type="text/javascript">
        function ShowCheckingMessage() {

            var checkingRequired = ('<%=checkingRequired%>');
            if (checkingRequired == "True")
                alert('<%=DeliveryCheckedMessage%>');
        }
    </script>
    <h2>
        <cc1:ucLabel ID="lblDelivaryunload" runat="server" Text="Delivery Unload"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">

            <table id="pnlHazardousItemChecking" runat="server" visible="false" width="100%" cellspacing="5" cellpadding="0" class="form-table">
                <tr>
                    <td>
                        <cc1:ucLabel ID="lblCautionsHazardousItemDelivery" ForeColor="Red" Font-Bold="true" Font-Size="15px"
                            runat="server" Text="Cautions this delivery contains Hazardous products"></cc1:ucLabel>
                    </td>
                </tr>
            </table>
            <br />

            <cc1:ucPanel ID="pnlBookingDetailsArrival" runat="server" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr id="trVendorCarrier" runat="server">
                        <td style="width: 4%;">
                            <cc1:ucLabel ID="lblVendorBooking" runat="server" Text="Vendor" />
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 20%;" class="nobold">
                            <cc1:ucLabel ID="VendorNameData" runat="server" />
                        </td>
                        <td style="width: 4%;">
                            <cc1:ucLabel ID="lblCarrierBooking" runat="server" Text="Carrier" />
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 20%;" class="nobold" colspan="6">
                            <cc1:ucLabel ID="CarrierNameData" runat="server" Text="ANC" />
                        </td>
                    </tr>
                    <tr id="trCarrier" runat="server">
                        <td style="width: 4%;">
                            <cc1:ucLabel ID="lblCarrierBooking1" runat="server" Text="Carrier" />
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 20%;" class="nobold" colspan="9">
                            <cc1:ucLabel ID="CarrierNameData1" runat="server" Text="ANC" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 4%;">
                            <cc1:ucLabel ID="lblDateBooking" runat="server" Text="Date" />
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 20%;" class="nobold">
                            <cc1:ucLabel ID="BookingDateData" runat="server" Text="" />
                            &nbsp;
                            <cc1:ucLabel ID="BookingTimeData" runat="server" Text="" />
                        </td>
                        <td style="width: 10%;">
                            <cc1:ucLabel ID="lblVehicleType" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 14%;" class="nobold">
                            <cc1:ucLabel ID="VehicleTypeData" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 10%;">
                            <cc1:ucLabel ID="lblDoorName" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 1%;">
                            :
                        </td>
                        <td style="width: 14%;" class="nobold">
                            <cc1:ucLabel ID="DoorNumberData" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 4%;">
                        </td>
                        <td style="width: 1%;">
                        </td>
                        <td style="width: 20%;" class="nobold">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblPalletsBooking" runat="server" Text="Pallets" />
                        </td>
                        <td>
                            :
                        </td>
                        <td style="width: 100px;" class="nobold">
                            <cc1:ucLabel ID="BookingPalletsData" runat="server" Text="20" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblCartons" runat="server" Text="Cartons" />
                        </td>
                        <td>
                            :
                        </td>
                        <td style="width: 100px;" class="nobold">
                            <cc1:ucLabel ID="BookingCartonsData" runat="server" Text="0" />
                        </td>
                        <td style="width: 100px;">
                            <cc1:ucLabel ID="lblLines" runat="server" Text="Lines" />
                        </td>
                        <td>
                            :
                        </td>
                        <td style="width: 100px;" class="nobold">
                            <cc1:ucLabel ID="BookingLinesData" runat="server" Text="16" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblLiftsBooking" runat="server" Text="Lifts" />
                        </td>
                        <td>
                            :
                        </td>
                        <td style="width: 100px;" class="nobold">
                            <cc1:ucLabel ID="BookingLiftsData" runat="server" Text="31" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <%--  <cc1:ucPanel ID="pnlActualDetailsArrival" runat="server" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0"  class="form-table">
                    <tr>
                        <td style="width: 5%;">
                            <cc1:ucLabel ID="lblVendorActual" runat="server" Text="Vendor" />
                        </td>
                        <td style="width: 1%;">
                            <cc1:ucLabel ID="UcLabel21" runat="server" Text=":" />
                        </td>
                        <td class="nobold" style="width: 34%;">
                            <cc1:ucLabel ID="UcLabel7" runat="server" Text="Acme Ltd" />
                        </td>
                        <td style="width: 5%;">
                            <cc1:ucLabel ID="lblDateActual" runat="server" Text="Date" />
                        </td>
                        <td style="width: 1%;">
                            <cc1:ucLabel ID="UcLabel23" runat="server" Text=":" />
                        </td>
                        <td class="nobold" style="width: 14%;">
                            <cc1:ucLabel ID="UcLabel9" runat="server" Text="19/01/2011" />
                        </td>
                        <td style="width: 5%;">
                            <cc1:ucLabel ID="lblPalletsActual" runat="server" Text="Pallets" />
                        </td>
                        <td style="width: 1%;">
                            <cc1:ucLabel ID="UcLabel26" runat="server" Text=":" />
                        </td>
                        <td class="nobold" style="width: 14%;">
                            <cc1:ucTextbox ID="txtPalletsActual" runat="server" Width="30px" />
                        </td>
                        <td style="width: 5%;">
                            <cc1:ucLabel ID="lblLinesActual" runat="server" Text="Lines" />
                        </td>
                        <td style="width: 1%;">
                            <cc1:ucLabel ID="UcLabel28" runat="server" Text=":" />
                        </td>
                        <td class="nobold" style="width: 14%;">
                            <cc1:ucTextbox ID="txtLinesActual" runat="server" Width="30px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblCarrierActual" runat="server" Text="Carrier" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="UcLabel22" runat="server" Text=":" />
                        </td>
                        <td class="nobold">
                            <cc1:ucLabel ID="UcLabel8" runat="server" Text="ANC" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblTimeActual" runat="server" Text="Time" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="UcLabel24" runat="server" Text=":" />
                        </td>
                        <td class="nobold">
                            <cc1:ucLabel ID="UcLabel10" runat="server" Text="09:10" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblCartonsActual" runat="server" Text="Cartons" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="UcLabel27" runat="server" Text=":" />
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="txtCartonsActual" runat="server" Width="30px" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblLiftsActual" runat="server" Text="Lifts" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="UcLabel25" runat="server" Text=":" />
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="txtLiftsActual" runat="server" Width="30px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblVehicleTypeActual_1" runat="server" Text="Vehicle Type" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="UcLabel29" runat="server" Text=":" />
                        </td>
                        <td class="nobold" colspan="10">
                            <cc1:ucDropdownList ID="ddlVehicleType" runat="server" Width="150px">
                            </cc1:ucDropdownList>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>--%>
             <%--Stage 8 - Point 6 - Start--%>
                    <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                        <tr>
                            <td style="width:10%">
                               <cc1:ucLabel ID="lblAdditionalInfoK" runat="server" Text="Additional Info" />
                            </td>
                             <td style="width:1%">
                                :
                            <td style="width:89%">
                               <cc1:ucLabel ID="lblAdditionalInfo" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                        <tr>
                            <td>
                              <cc1:ucGridView ID="gvPO" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                CellPadding="0" Width="100%">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:BoundField HeaderText="PO" DataField="PurchaseNumber" SortExpression="PurchaseNumber">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Original Due Date" DataField="Original_due_date" SortExpression="Original_due_date">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" SortExpression="VendorName">
                                        <HeaderStyle Width="20%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="# Outstanding Lines on PO" DataField="OutstandingLines"
                                        SortExpression="OutstandingLines">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="# Stock outs" DataField="Qty_On_Hand" SortExpression="Qty_On_Hand">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="# Backorders" DataField="qty_on_backorder" SortExpression="qty_on_backorder">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Destination" DataField="SiteName" SortExpression="SiteName">
                                        <HeaderStyle Width="20%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                     <asp:BoundField HeaderText="Stock Planner" DataField="StockPlannerName" SortExpression="StockPlannerName">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                            </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                    <%--Stage 8 - Point 6 - END--%>
            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                <tr>
                    <td style="width: 10%;">
                        <cc1:ucLabel ID="lblOperatorInitials" runat="server" Text="Operator Name" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="width: 1%;">
                        :
                    </td>
                    <td class="nobold">
                        <%--<cc1:ucTextbox ID="txtOperatorInitials" runat="server" Width="250px" MaxLength="50"></cc1:ucTextbox>--%>
                        <%--<cc1:ucDropdownList ID="ddlOperators" runat="server" Width="250px" onchange="ShowHidePassword()">--%>
                        <cc1:ucDropdownList ID="ddlOperators" runat="server" Width="250px"></cc1:ucDropdownList>
                        <asp:HiddenField ID="hdnLoggedInUserId" runat="server" />
                        <asp:HiddenField ID="hdnIsDelUnloadPassAllow" runat="server" />
                    </td>
                    <td>
                        <table id="tblPassword" runat="server" width="100%" cellspacing="5" cellpadding="0"
                            class="form-table">
                            <tr>
                                <td style="width: 10%;">
                                    <cc1:ucLabel ID="UcLabel1" runat="server" Text="Password" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="width: 1%;">
                                    :
                                </td>
                                <td class="nobold">
                                    <cc1:ucTextbox ID="txtPassword" runat="server" Width="250px" MaxLength="50" TextMode="Password"></cc1:ucTextbox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%;">
                        <cc1:ucLabel ID="lblComments" runat="server" Text="Comments"></cc1:ucLabel>
                    </td>
                    <td style="width: 1%;">
                        :
                    </td>
                    <td class="nobold" colspan="2">
                        <cc1:ucTextarea ID="txtComments" Rows="3" Columns="20" runat="server"></cc1:ucTextarea>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnAcceptAll" runat="server" Text="Accept All" CssClass="button"
            ValidationGroup="Save" OnClick="btnAcceptAll_Click" OnClientClick="ShowCheckingMessage()" />
        <cc1:ucButton ID="btnAcceptPartial" runat="server" Text="Accept Partial" CssClass="button"
            ValidationGroup="Save" OnClick="btnAcceptPartial_Click" OnClientClick="ShowCheckingMessage()" />
        <cc1:ucButton ID="btnRefuseAll" runat="server" Text="Refuse All" CssClass="button"
            ValidationGroup="Save" OnClick="btnRefuseAll_Click" />
        <cc1:ucButton ID="btnExit" runat="server" Text="Exit" CssClass="button" OnClick="btnExit_Click" />
    </div>
</asp:Content>

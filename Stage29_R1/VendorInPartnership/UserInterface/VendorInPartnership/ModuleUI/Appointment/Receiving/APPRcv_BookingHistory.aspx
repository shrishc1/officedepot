﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="APPRcv_BookingHistory.aspx.cs" Inherits="APPRcv_BookingHistory" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%	  
        Response.WriteFile("../../Discrepancy/LogDiscrepancy/DiscrepancyImages.txt");
    %>
    <h2>
        <cc1:ucLabel ID="lblBookingHistory" runat="server" Text="Booking History"></cc1:ucLabel>
        &nbsp;
        <asp:Literal ID="ltBookinRef" runat="server"></asp:Literal>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlBookingDetailsArrival" runat="server" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr id="trVendorCarrier" runat="server">
                        <td>
                            <cc1:ucLabel ID="lblVendorBooking" runat="server" Text="Vendor" />
                        </td>
                        <td>:
                        </td>
                        <td class="nobold">
                            <cc1:ucLabel ID="VendorNameData" runat="server" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblCarrierBooking" runat="server" Text="Carrier" />
                        </td>
                        <td>:
                        </td>
                        <td class="nobold" colspan="6">
                            <cc1:ucLabel ID="CarrierNameData" runat="server" Text="ANC" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblDateBooking" runat="server" Text="Date" />
                        </td>
                        <td>:
                        </td>
                        <td class="nobold">
                            <cc1:ucLabel ID="BookingDateData" runat="server" Text="" />
                            &nbsp;
                            <cc1:ucLabel ID="BookingTimeData" runat="server" Text="" />
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblVehicleType" runat="server"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td class="nobold">
                            <cc1:ucLabel ID="VehicleTypeData" runat="server"></cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblDoorName" runat="server"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td class="nobold" colspan="4">
                            <cc1:ucLabel ID="DoorNumberData" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%;">
                            <cc1:ucLabel ID="lblPalletsBooking" runat="server" Text="Pallets" />
                        </td>
                        <td style="width: 1%;">:
                        </td>
                        <td style="width: 26%;" class="nobold">
                            <cc1:ucLabel ID="BookingPalletsData" runat="server" />
                        </td>
                        <td style="width: 10%;">
                            <cc1:ucLabel ID="lblCartons" runat="server" Text="Cartons" />
                        </td>
                        <td style="width: 1%;">:
                        </td>
                        <td style="width: 26%;" class="nobold">
                            <cc1:ucLabel ID="BookingCartonsData" runat="server" />
                        </td>
                        <td style="width: 8%;">
                            <cc1:ucLabel ID="lblLines" runat="server" Text="Lines" />
                        </td>
                        <td style="width: 1%;">:
                        </td>
                        <td style="width: 6%;" class="nobold">
                            <cc1:ucLabel ID="BookingLinesData" runat="server" />
                        </td>
                        <td style="width: 5%;">
                            <cc1:ucLabel ID="lblLiftsBooking" runat="server" Text="Lifts" />
                        </td>
                        <td style="width: 1%;">:
                        </td>
                        <td style="width: 5%;" class="nobold">
                            <cc1:ucLabel ID="BookingLiftsData" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblAdditionalInfo" runat="server" Text="Additional Info" />
                        </td>
                        <td>:
                        </td>
                        <td class="nobold" colspan="10">
                            <cc1:ucLabel ID="ltAdditionalInfo" runat="server" />
                        </td>
                    </tr>
                </table>
                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                    <tr>
                        <td>
                            <cc1:ucGridView ID="gvPO" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                CellPadding="0" Width="100%">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:BoundField HeaderText="PO" DataField="PurchaseNumber" SortExpression="PurchaseNumber">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Original Due Date" DataField="Original_due_date" SortExpression="Original_due_date">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" SortExpression="VendorName">
                                        <HeaderStyle Width="20%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="# Outstanding Lines on PO" DataField="OutstandingLines"
                                        SortExpression="OutstandingLines">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="# Stock outs" DataField="Qty_On_Hand" SortExpression="Qty_On_Hand">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="# Backorders" DataField="qty_on_backorder" SortExpression="qty_on_backorder">
                                        <HeaderStyle Width="10%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Destination" DataField="SiteName" SortExpression="SiteName">
                                        <HeaderStyle Width="20%" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlIssues" runat="server" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkLate" runat="server" Enabled="false" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkEarly" runat="server" Enabled="false" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkNoPaperworkondisplay" runat="server" Enabled="false" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkPalletsDamaged" runat="server" Enabled="false" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkPackagingDamaged" runat="server" Enabled="false" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkunsafeload" runat="server" Enabled="false" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkWrongAddress" runat="server" Enabled="false" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkRefusedtowait" runat="server" Enabled="false" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkNotToOdSpecification" runat="server" Enabled="false" />
                        </td>
                        <td style="width: 25%;">
                            <cc1:ucCheckbox ID="chkOther" runat="server" Enabled="false" />
                        </td>
                        <td style="width: 50%;" colspan="2"></td>
                    </tr>
                </table>
                <table width="100%" cellspacing="0" cellpadding="0" class="grid">
                   <tr>
                        <th align="left">
                            <cc1:ucLabel ID="RefusalComment" runat="server" Text="Comments : " />&nbsp;<span
                                style="font-weight: bold;"><cc1:ucLabel ID="lblRefusalCommentsValue" runat="server" /></span>
                        </th>
                    </tr>
                </table>
            </cc1:ucPanel>

            <cc1:ucPanel ID="pnlDeliveryUnloadImages" runat="server" CssClass="fieldset-form">
                <table width="100%" cellspacing="0" cellpadding="0" class="grid">                    
                    <tr>
                        <td align="right">
                            <a href="#" rel="shadowbox[gal]" target="_blank" style="padding-left: 0px;">
                                <input type="button" id="btnViewImage" value="View Images" runat="server" title="View Images"
                                    class="button" clientidmode="Static" /></a>
                            <div id="discrepancygallery" style="display: none" runat="server" clientidmode="Static"></div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>

            <cc1:ucPanel ID="pnlDeliveryDetailsHistory" GroupingText="Delivery Details" runat="server"
                CssClass="fieldset-form" Visible="false">
                <cc1:ucGridView ID="grdDeliveryDetails" Width="100%" runat="server" CssClass="grid"
                    ShowFooter="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Vendor">
                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral runat="server" ID="ltVendorName" Text='<%# Eval("Vendor.VendorName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Expected Pallets" DataField="NumberOfPallet">
                            <HeaderStyle Width="12%" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Expected Cartons" DataField="NumberOfCartons">
                            <HeaderStyle Width="12%" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Expected Lifts" DataField="NumberOfLift">
                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Accepted Pallets" DataField="DLYUNL_NumberOfPallet">
                            <HeaderStyle Width="12%" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Accepted Cartons" DataField="DLYUNL_NumberOfCartons">
                            <HeaderStyle Width="12%" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Refused Pallets" DataField="DLYUNL_RefusedPallet">
                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Refused Cartons" DataField="DLYUNL_RefusedCarton">
                            <HeaderStyle Width="12%" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </cc1:ucGridView>
                <table width="100%">
                    <tr>
                        <td style="width: 20%;">
                            <cc1:ucLabel runat="server" ID="lblTotal" Text="Total" Font-Bold="true"></cc1:ucLabel>
                        </td>
                        <td style="width: 12%;" align="center">
                            <cc1:ucLiteral runat="server" ID="ltExpectedPalletsTotal"></cc1:ucLiteral>
                        </td>
                        <td style="width: 12%;" align="center">
                            <cc1:ucLiteral runat="server" ID="ltExpectedCartonsTotal"></cc1:ucLiteral>
                        </td>
                        <td style="width: 10%;" align="center">
                            <cc1:ucLiteral runat="server" ID="ltExpectedLiftsTotal"></cc1:ucLiteral>
                        </td>
                        <td style="width: 12%;" align="center">
                            <cc1:ucLiteral runat="server" ID="ltTotalAcceptedPallets"></cc1:ucLiteral>
                        </td>
                        <td style="width: 12%;" align="center">
                            <cc1:ucLiteral runat="server" ID="ltTotalAcceptedCartons"></cc1:ucLiteral>
                        </td>
                        <td style="width: 10%;" align="center">
                            <cc1:ucLiteral runat="server" ID="ltTotalRefusedPallets"></cc1:ucLiteral>
                        </td>
                        <td style="width: 12%;" align="center">
                            <cc1:ucLiteral runat="server" ID="ltTotalRefusedCartons"></cc1:ucLiteral>
                        </td>
                    </tr>
                </table>

            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlBookingHistory" runat="server" CssClass="fieldset-form">
                <cc1:ucGridView ID="grdBookingHistory" runat="server" AutoGenerateColumns="false"
                    CssClass="grid" CellPadding="0" Width="100%" OnSorting="SortGrid" AllowSorting="true">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                    <Columns>
                        <asp:BoundField HeaderText="Status" DataField="BookingStatus" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="15%" SortExpression="BookingStatus" />
                        <asp:TemplateField HeaderText="User Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="18%"
                            SortExpression="FixedSlot.User.FullName">
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltUserName" runat="server" Text='<%#Eval("FixedSlot.User.FullName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Operator Initials" DataField="HistoryOperatorInitials"
                            HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="18%" SortExpression="HistoryOperatorInitials" />
                        <asp:BoundField HeaderText="Date" DataField="HistoryDate" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="14%" SortExpression="HistoryDate" />
                        <asp:BoundField HeaderText="Comments" DataField="HistoryComments" HeaderStyle-HorizontalAlign="Left"
                            HeaderStyle-Width="35%" SortExpression="HistoryComments" />
                    </Columns>
                </cc1:ucGridView>
            </cc1:ucPanel>
            <%--Sprint 1 - Point 5 Start--%>
            <cc1:ucPanel ID="pnlCommunicationsWithMailSent" runat="server" GroupingText="Communications"
                CssClass="fieldset-form">
                <cc1:ucGridView ID="grdCommunications" runat="server" AutoGenerateColumns="false"
                    CssClass="grid" CellPadding="0" Width="100%" OnRowDataBound="grdCommunications_RowDataBound">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                    <Columns>
                        <asp:BoundField HeaderText="Type" DataField="CommunicationType" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="25%" />
                        <%--<asp:BoundField HeaderText="Sent To" DataField="SentTo" HeaderStyle-HorizontalAlign="Left"  HeaderStyle-Width="25%"/>--%>
                        <asp:TemplateField HeaderText="Sent To" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="25%">
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSentto" runat="server" Text='<%#Eval("SentTo") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField HeaderText="User" DataField="FullName" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="25%" />
                        <asp:BoundField HeaderText="Date" DataField="SentDate" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="25%" />
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <b><a id="lnk" runat="server" target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=800px,height=600px,toolbar=0,scrollbars=0,status=0"); return false;'>Link</a></b>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlCommunicationsWithoutMailSent" runat="server" GroupingText="Communications"
                CssClass="fieldset-form">
                <cc1:ucGridView ID="grdCommunicationsWithoutMail" runat="server" AutoGenerateColumns="false"
                    CssClass="grid" CellPadding="0" Width="100%" OnRowDataBound="grdCommunications_RowDataBound">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                    <Columns>
                        <asp:BoundField HeaderText="Type" DataField="CommunicationType" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="25%" />
                        <asp:TemplateField HeaderText="Sent To" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="25%">
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSentto" runat="server" Text='<%#Eval("SentTo") %>' Visible="false"></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField HeaderText="User" DataField="FullName" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="25%" />
                        <asp:BoundField HeaderText="Date" DataField="SentDate" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="25%" />
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <b><a id="lnk" runat="server" target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=800px,height=600px,toolbar=0,scrollbars=0,status=0"); return false;'>Link</a></b>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </cc1:ucPanel>

            <%--Sprint 1 - Point 5 End--%>
        </div>
        <div class="bottom-shadow">
        </div>
        <div class="button-row">
            <cc1:ucButton ID="btnOK" Width="100px" runat="server" Text="OK" CssClass="button"
                OnClick="btnOK_Click" />
        </div>
    </div>
</asp:Content>

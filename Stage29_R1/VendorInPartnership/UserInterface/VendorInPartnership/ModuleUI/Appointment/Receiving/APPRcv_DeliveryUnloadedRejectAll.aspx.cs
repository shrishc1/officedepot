﻿using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;

public partial class APPRcv_DeliveryUnloadedRejectAll : CommonPage
{
    DataSet dsDummy = null;
    DataTable tbDummy = null;
    protected string RefusalCommentsValidation = WebCommon.getGlobalResourceValue("RefusalCommentsValidation");
    protected string SupervisorPasswordValidation = WebCommon.getGlobalResourceValue("SupervisorPasswordValidation");
    protected string PasswordNotMatch = WebCommon.getGlobalResourceValue("PasswordNotMatch");
    protected string EuroPalletesReturnedValidation = WebCommon.getGlobalResourceValue("EuroPalletesReturnedValidation");
    protected string EuroPalletesDeliveredValidation = WebCommon.getGlobalResourceValue("EuroPalletesDeliveredValidation");
    protected string EuroPalletesCommentsValidation = WebCommon.getGlobalResourceValue("EuroPalletesCommentsValidation");
    protected string UKStandardReturnedValidation = WebCommon.getGlobalResourceValue("UKStandardReturnedValidation");
    protected string UKStandardDeliveredValidation = WebCommon.getGlobalResourceValue("UKStandardDeliveredValidation");
    protected string UKStandardCommentsValidation = WebCommon.getGlobalResourceValue("UKStandardCommentsValidation");
    protected string CHEPReturnedValidation = WebCommon.getGlobalResourceValue("CHEPReturnedValidation");
    protected string CHEPDeliveredValidation = WebCommon.getGlobalResourceValue("CHEPDeliveredValidation");
    protected string CHEPCommentsValidation = WebCommon.getGlobalResourceValue("CHEPCommentsValidation");
    protected string OthersReturnedValidation = WebCommon.getGlobalResourceValue("OthersReturnedValidation");
    protected string OthersDeliveredValidation = WebCommon.getGlobalResourceValue("OthersDeliveredValidation");
    protected string OthersCommentsValidation = WebCommon.getGlobalResourceValue("OthersCommentsValidation");
    protected string AtleastOnePalletInformation = WebCommon.getGlobalResourceValue("AtleastOnePalletInformation");
    protected string AtLeastOnePoingFailure = WebCommon.getGlobalResourceValue("AtLeastOnePoingFailure");

    protected string DelieveryUnloadErrorMessage1 = WebCommon.getGlobalResourceValue("DelieveryUnloadErrorMessage1");
    protected string DelieveryUnloadErrorMessage2 = WebCommon.getGlobalResourceValue("DelieveryUnloadErrorMessage2");
    protected string ISPM15Alert1 = WebCommon.getGlobalResourceValue("ISPM15Alert1");
    protected string ISPM15Alert2 = WebCommon.getGlobalResourceValue("ISPM15Alert2");
    protected string DelieveryUnloadErrorMessage3 = WebCommon.getGlobalResourceValue("DelieveryUnloadErrorMessage3");
    protected string CommentsOnNoDeliveryCheckedAlertMsg = WebCommon.getGlobalResourceValue("CommentsOnNoDeliveryChecked");

    protected string ConfirmMessage = WebCommon.getGlobalResourceValue("ConfirmMessage");
    protected string VendorPalletIssueMsg = WebCommon.getGlobalResourceValue("VendorPalletIssueMsg");
    protected string RejectFullUnloadDelivery = WebCommon.getGlobalResourceValue("RejectUnLoadDelivery");
    protected string UnloadAllWithOneIssue = WebCommon.getGlobalResourceValue("UnloadAllWithOneIssue");
    readonly string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    readonly string templatePathName = @"/EmailTemplates/Appointment/";
    readonly string strDefaultEmailId = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"];

    public bool IsVenodrPalletChecking
    {
        get
        {
            if (ViewState["IsVenodrPalletChecking"] == null)
                ViewState["IsVenodrPalletChecking"] = false;
            return (bool)ViewState["IsVenodrPalletChecking"];
        }
        set
        {
            ViewState["IsVenodrPalletChecking"] = value;
        }
    }
    public bool IsStandardPalletChecking
    {
        get
        {
            if (ViewState["IsStandardPalletChecking"] == null)
                ViewState["IsStandardPalletChecking"] = false;
            return (bool)ViewState["IsStandardPalletChecking"];
        }
        set
        {
            ViewState["IsStandardPalletChecking"] = value;
        }
    }
    public bool ISPM15CountryPalletChecking
    {
        get
        {
            if (ViewState["ISPM15CountryPalletChecking"] == null)
                ViewState["ISPM15CountryPalletChecking"] = false;
            return (bool)ViewState["ISPM15CountryPalletChecking"];
        }
        set
        {
            ViewState["ISPM15CountryPalletChecking"] = value;
        }
    }
    public bool IsEnableHazardouesItemPrompt
    {
        get
        {
            if (ViewState["IsEnableHazardouesItemPrompt"] == null)
                ViewState["IsEnableHazardouesItemPrompt"] = false;
            return (bool)ViewState["IsEnableHazardouesItemPrompt"];
        }
        set
        {
            ViewState["IsEnableHazardouesItemPrompt"] = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            rfvRefusalCommentsValidation.Enabled = false;

            if (GetQueryStringValue("ID") != null && GetQueryStringValue("ID").ToString() != "")
            {
                if (ExtractInformation(GetQueryStringValue("ID").ToString(), "type").ToLower() == "c")
                {
                    lblCarrierBooking.Style["display"] = "none";
                    CarrierNameData.Style["display"] = "none";
                }
                else
                {
                    lblCarrierBooking.Style["display"] = "";
                    CarrierNameData.Style["display"] = "";
                }

                int? SiteId = BookingDetails(Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id")));
                BindSuperUsers(SiteId.Value);
            }

            if (GetQueryStringValue("UNL").ToString() == "FULL" || GetQueryStringValue("UNL").ToString() == "PAR")
            {
                IssueDetail.Text = "Addition Details";
                RefusalComment.Text = "Comments :";
                pnlIssues.GroupingText = "Issues";
                btnCancel.Visible = false;
            }

            hdnRefusalFlg.Value = GetQueryStringValue("UNL").ToString();

            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

            oAPPBOK_BookingBE.Action = GetQueryStringValue("IsFromUDC") != null
                ? GetQueryStringValue("IsFromUDC") == "true"
                ? "GetBookingDeatilsByID_Unexpected"
                : "GetBookingDeatilsByID"
                : "GetBookingDeatilsByID";

            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));
            var lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);

            if (lstBooking.Count > 0)
            {
                lblDateTimeBookingValue.Text = lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy");
                var vendor = lstBooking[0].Vendor.VendorName.ToString().Split('-');
                lblVendorValue.Text = vendor[0];
                lblVendorNameValue.Text = vendor[1];
                lblBookingReferenceValue.Text = lstBooking[0].BookingRef.ToString();

                litRejectUnLoadDelivery.Text = lstBooking[0].ISPM15CountryPalletChecking &&
                    (GetQueryStringValue("UNL").ToString() == "FULL" || GetQueryStringValue("UNL").ToString() == "PAR")
                    ? VendorPalletIssueMsg.ToString() : RejectFullUnloadDelivery.ToString();
            }
        }

        if (GetQueryStringValue("IsDelUnloadPassAllow") != null)
        {
            hdnIsDelUnloadPassAllow.Value = GetQueryStringValue("IsDelUnloadPassAllow").Trim().ToUpper();
        }

        if (GetQueryStringValue("UNL") != null && GetQueryStringValue("UNL").ToString().ToLower() == "rej")
        {
            rfvRefusalCommentsValidation.Enabled = true;
            trTotalPalletsDelivered.Visible = false;
        }
        if (GetQueryStringValue("TotalPallets") != null)
        {
            lblTotalDelivered.Text = GetQueryStringValue("TotalPallets");
        }

        this.Form.DefaultButton = btnOK.UniqueID;
        dsDummy = new DataSet();
        tbDummy = new DataTable();
    }
    public string ExtractInformation(string pFullData, string pReturnType)
    {
        int i = -1;
        switch (pReturnType)
        {
            case "table":
                i = 0;
                break;
            case "type":
                i = 1;
                break;
            case "id":
                i = 2;
                break;
            case "siteid":
                i = 3;
                break;
        }
        return (pFullData.Split('-')[i].ToString());
    }
    public int? BookingDetails(int pPkID)
    {
        int? SiteId = 0;
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

        oAPPBOK_BookingBE.Action = "DeliveryArrived";
        oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
        oAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
        oAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
        oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
        oAPPBOK_BookingBE.ScheduleDate = Common.GetMM_DD_YYYY(GetQueryStringValue("Scheduledate").ToString());

        List<APPBOK_BookingBE> lstDeliveryArrived = oAPPBOK_BookingBAL.GetBookingDetailsBAL(oAPPBOK_BookingBE);
        if (lstDeliveryArrived != null && lstDeliveryArrived.Count > 0)
        {
            SiteId = lstDeliveryArrived[0].SiteId;

            if (ExtractInformation(GetQueryStringValue("ID").ToString(), "type").ToLower() == "c")
            {
                trCarrier.Style["display"] = "";
                trVendorCarrier.Style["display"] = "none";
                CarrierNameData1.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
            }
            else
            {
                trCarrier.Style["display"] = "none";
                trVendorCarrier.Style["display"] = "";
                VendorNameData.Text = lstDeliveryArrived[0].FixedSlot.Vendor.VendorName.ToString();
                CarrierNameData.Text = lstDeliveryArrived[0].Carrier.CarrierName.ToString();
            }

            BookingDateData.Text = GetQueryStringValue("Scheduledate").ToString();

            if (!string.IsNullOrEmpty(lstDeliveryArrived[0].ExpectedDeliveryTime))
                BookingTimeData.Text = "- " + lstDeliveryArrived[0].ExpectedDeliveryTime.ToString();

            VehicleTypeData.Text = lstDeliveryArrived[0].VehicleType.VehicleType.ToString();

            DoorNumberData.Text = lstDeliveryArrived[0].DoorNoSetup.DoorNumber.ToString();

            if (lstDeliveryArrived.Count == 1)
            {
                BookingLiftsData.Text = lstDeliveryArrived[0].LiftsScheduled.ToString();
                BookingLinesData.Text = lstDeliveryArrived[0].FixedSlot.MaximumLines.ToString();
                BookingPalletsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumPallets.ToString();
                BookingCartonsData.Text = lstDeliveryArrived[0].FixedSlot.MaximumCatrons.ToString();
            }
            else
            {
                int? iLifts = 0, iLines = 0, iPallets = 0, iCartons = 0;
                for (int i = 0; i < lstDeliveryArrived.Count; i++)
                {
                    iLifts += lstDeliveryArrived[i].LiftsScheduled;
                    iLines += lstDeliveryArrived[i].FixedSlot.MaximumLines;
                    iPallets += lstDeliveryArrived[i].FixedSlot.MaximumPallets;
                    iCartons += lstDeliveryArrived[i].FixedSlot.MaximumCatrons;
                }
                BookingLiftsData.Text = iLifts.ToString();
                BookingLinesData.Text = iLines.ToString();
                BookingPalletsData.Text = iPallets.ToString();
                BookingCartonsData.Text = iCartons.ToString();
            }

            // Pallet Checking vendor
            IsVenodrPalletChecking = lstDeliveryArrived[0].IsVenodrPalletChecking;

            IsStandardPalletChecking = lstDeliveryArrived[0].IsStandardPalletChecking;

            ISPM15CountryPalletChecking = lstDeliveryArrived[0].ISPM15CountryPalletChecking;

            IsEnableHazardouesItemPrompt = lstDeliveryArrived[0].IsEnableHazardouesItemPrompt;

            if (lstDeliveryArrived[0].SupplierType != "C")
            {
                if (lstDeliveryArrived[0].IsVenodrPalletChecking || lstDeliveryArrived[0].ISPM15CountryPalletChecking)
                {
                    ListItem lst = new ListItem(lstDeliveryArrived[0].FixedSlot.Vendor.VendorName, Convert.ToString(lstDeliveryArrived[0].FixedSlot.Vendor.VendorID));
                    chkCarrierVendor.Items.Add(lst);
                    chkCarrierVendor.Style.Add("display", "none");
                    divPalletCheckMesg.Visible = true;
                    lblDelieveryUnloadPalletCheckingMesg1.Visible = true;
                    lblDelieveryUnloadPalletCheckingMesg2.Visible = true;
                    pnlVendorPalletChecking.Visible = false;
                    lst.Selected = true;
                }
                if (lstDeliveryArrived[0].IsStandardPalletChecking)
                {
                    ListItem lst = new ListItem(lstDeliveryArrived[0].FixedSlot.Vendor.VendorName, Convert.ToString(lstDeliveryArrived[0].FixedSlot.Vendor.VendorID));
                    chkCarrierVendor.Items.Add(lst);
                    chkCarrierVendor.Style.Add("display", "none");
                    divPalletCheckMesg.Visible = true;
                    lblDelieveryUnloadStandardPalletCheckingMesg1.Visible = true;
                    lblDelieveryUnloadStandardPalletCheckingMesg2.Visible = true;
                    pnlVendorPalletChecking.Visible = false;
                    lst.Selected = true;
                }
                if (lstDeliveryArrived[0].IsEnableHazardouesItemPrompt)
                {
                    ListItem lst = new ListItem(lstDeliveryArrived[0].FixedSlot.Vendor.VendorName, Convert.ToString(lstDeliveryArrived[0].FixedSlot.Vendor.VendorID));
                    chkCarrierHazardousVendor.Items.Add(lst);
                    chkCarrierHazardousVendor.Style.Add("display", "none");
                    divPalletCheckMesg.Visible = true;
                    lblHazardousItemCheckingDelivery.Visible = true;
                    lblHazardousItemCheckingDeliveryComments.Visible = true;
                    pnlVendorPalletChecking.Visible = false;
                    pnlHazardousItemChecking.Visible = false;
                    lst.Selected = true;                     
                }
            }
            else
            {
                for (int i = 0; i < lstDeliveryArrived.Count; i++)
                {
                    if (lstDeliveryArrived[i].IsVenodrPalletChecking || lstDeliveryArrived[0].ISPM15CountryPalletChecking)
                    {
                        ListItem lst = new ListItem(lstDeliveryArrived[i].FixedSlot.Vendor.VendorName, Convert.ToString(lstDeliveryArrived[i].FixedSlot.Vendor.VendorID));
                        chkCarrierVendor.Items.Add(lst);
                        chkCarrierVendor.Style.Add("display", "block"); 
                        lblDelieveryUnloadPalletCheckingMesg1.Visible = true;
                        lblDelieveryUnloadPalletCheckingMesg2.Visible = true;
                        divPalletCheckMesg.Visible = true;
                        pnlVendorPalletChecking.Visible = false;
                    }

                    if (lstDeliveryArrived[i].IsStandardPalletChecking)
                    {
                        ListItem lst = new ListItem(lstDeliveryArrived[i].FixedSlot.Vendor.VendorName, Convert.ToString(lstDeliveryArrived[i].FixedSlot.Vendor.VendorID));
                        chkCarrierVendor.Items.Add(lst);
                        chkCarrierVendor.Style.Add("display", "block");
                        lblDelieveryUnloadStandardPalletCheckingMesg1.Visible = true;
                        lblDelieveryUnloadStandardPalletCheckingMesg2.Visible = true;
                        divPalletCheckMesg.Visible = true;
                        pnlVendorPalletChecking.Visible = false;
                    }

                    if (lstDeliveryArrived[i].IsEnableHazardouesItemPrompt)
                    {
                        ListItem lst = new ListItem(lstDeliveryArrived[0].FixedSlot.Vendor.VendorName, Convert.ToString(lstDeliveryArrived[0].FixedSlot.Vendor.VendorID));
                        chkCarrierHazardousVendor.Items.Add(lst);
                        chkCarrierHazardousVendor.Style.Add("display", "block");

                        divHazardousMesg.Visible = true;
                        lblHazardousItemCheckingDelivery.Visible = true;
                        lblHazardousItemCheckingDeliveryComments.Visible = true;
                        pnlVendorPalletChecking.Visible = false;
                        pnlHazardousItemChecking.Visible = false;
                        lst.Selected = true;
                    }
                }
            }
        }
        return SiteId;
    }
    public void BindSuperUsers(int SiteId)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.Action = "GetDeliveryRefusalUsersBySiteId";
        oSCT_UserBE.IsDeliveryRefusalAuthorization = 1;
        oSCT_UserBE.SiteId = SiteId;
        List<SCT_UserBE> lstSuperUsers = oSCT_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
        if (lstSuperUsers != null && lstSuperUsers.Count > 0)
        {
            FillControls.FillDropDown(ref ddlUser, lstSuperUsers, "FullName", "UserIDPassword");
        }
    }
    private bool CheckBoxCheckStatus()
    {
        return chkLate.Checked || chkEarly.Checked || chkNoPaperworkondisplay.Checked || chkPalletsDamaged.Checked ||
            chkPackagingDamaged.Checked || chkunsafeload.Checked || chkWrongAddress.Checked || chkRefusedtowait.Checked ||
            chkNotToOdSpecification.Checked || chkOther.Checked || chkFailedToDeclareHazardousGoods.Checked;
    }
    protected void btnOK2_Click(object sender, EventArgs e)
    {
        string objSelectedUserPass = ddlUser.SelectedValue.Split('-')[1];

        if (GetQueryStringValue("UNL").ToString() != "FULL")
        {
            if ((GetQueryStringValue("UNL").ToString() == "REJ" || GetQueryStringValue("UNL").ToString() == "PAR") && CheckBoxCheckStatus() == false)
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "alert('" + AtLeastOnePoingFailure + "');", true);
                return;
            }
            if (objSelectedUserPass != null && txtSupervisorPassword.Text != null && hdnIsDelUnloadPassAllow.Value == "Y")
            {
                divApproveBy.Attributes.Add("Style", "visibility:visible");
                divPassLabel.Attributes.Add("Style", "visibility:visible");
                divPassCollon.Attributes.Add("Style", "visibility:visible");
                divPassTextBox.Attributes.Add("Style", "visibility:visible");
            }
            else if (objSelectedUserPass != null && txtSupervisorPassword.Text != null && hdnIsDelUnloadPassAllow.Value == "N")
            {
                divApproveBy.Attributes.Add("Style", "visibility:hidden");
                divPassLabel.Attributes.Add("Style", "visibility:hidden");
                divPassCollon.Attributes.Add("Style", "visibility:hidden");
                divPassTextBox.Attributes.Add("Style", "visibility:hidden");
            }
            if (!Common.VerifyPassword(txtSupervisorPassword.Text, objSelectedUserPass) && hdnIsDelUnloadPassAllow.Value == "Y")
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "alert('" + PasswordNotMatch + "');", true);
                return;
            }
        }

        if (IsVenodrPalletChecking || IsStandardPalletChecking || ISPM15CountryPalletChecking)
        {
            pnlVendorPalletChecking.Visible = true;

            btnOk3.Visible = IsEnableHazardouesItemPrompt;
            btnCancel3.Visible = IsEnableHazardouesItemPrompt;

            btnOK.Visible = !IsEnableHazardouesItemPrompt;
            btnCancel.Visible = !IsEnableHazardouesItemPrompt;

            divBookingDiv.Visible = false;
            divBookingButton.Visible = false;

            divBookingButton3.Visible= IsEnableHazardouesItemPrompt;

            lblDelieveryUnloadPalletCheckingMesg1.Visible = IsVenodrPalletChecking || ISPM15CountryPalletChecking;
            lblDelieveryUnloadPalletCheckingMesg2.Visible = IsVenodrPalletChecking || ISPM15CountryPalletChecking;

            lblDelieveryUnloadStandardPalletCheckingMesg1.Visible = IsStandardPalletChecking;
            lblDelieveryUnloadStandardPalletCheckingMesg2.Visible = IsStandardPalletChecking;
        }
        else
        {
            if (IsEnableHazardouesItemPrompt)
                btnOk3_Click(sender, e);
            else
                btnOK_Click(sender, e);
        }
    }
    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (CheckBoxCheckStatus())
        {
            litRejectUnLoadDelivery.Text = GetQueryStringValue("UNL").ToString() == "FULL"
                ? UnloadAllWithOneIssue.ToString()
                : RejectFullUnloadDelivery.ToString();

            if (GetQueryStringValue("UNL").ToString() == "PAR")
            {
                litRejectUnLoadDelivery.Text = VendorPalletIssueMsg.ToString();
            }

            updInfromation_Messge_update.Show();
        }
        else if ((IsVenodrPalletChecking || IsStandardPalletChecking || ISPM15CountryPalletChecking) && GetQueryStringValue("UNL").ToString() == "FULL")
        {
            litRejectUnLoadDelivery.Text = VendorPalletIssueMsg.ToString();

            if (rdoDelieveryUnload2_1.Checked)
            {
                updInfromation_Messge_update.Show();
            }
            if (rdoDelieveryUnload2_2.Checked)
            {
                updInfromation_Messge_update.Hide();
                SaveDeliveryUnload();
            }
        }
        else
        {
            string objSelectedUserPass = ddlUser.SelectedValue.Split('-')[1];
            if (GetQueryStringValue("UNL").ToString() != "FULL")
            {
                if ((GetQueryStringValue("UNL").ToString() == "REJ" || GetQueryStringValue("UNL").ToString() == "PAR") && !CheckBoxCheckStatus())
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "alert('" + AtLeastOnePoingFailure + "');", true);
                    return;
                }

                if (objSelectedUserPass != null && txtSupervisorPassword.Text != null && hdnIsDelUnloadPassAllow.Value == "Y")
                {
                    divApproveBy.Attributes.Add("Style", "visibility:visible");
                    divPassLabel.Attributes.Add("Style", "visibility:visible");
                    divPassCollon.Attributes.Add("Style", "visibility:visible");
                    divPassTextBox.Attributes.Add("Style", "visibility:visible");
                }
                else if (objSelectedUserPass != null && txtSupervisorPassword.Text != null && hdnIsDelUnloadPassAllow.Value == "N")
                {
                    divApproveBy.Attributes.Add("Style", "visibility:hidden");
                    divPassLabel.Attributes.Add("Style", "visibility:hidden");
                    divPassCollon.Attributes.Add("Style", "visibility:hidden");
                    divPassTextBox.Attributes.Add("Style", "visibility:hidden");
                }

                if (!Common.VerifyPassword(txtSupervisorPassword.Text, objSelectedUserPass) && hdnIsDelUnloadPassAllow.Value == "Y")
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "alert('" + PasswordNotMatch + "');", true);
                    return;
                }
            }
            SaveDeliveryUnload();
        }
    }
    protected void btnAddImage_Click(object sender, EventArgs e)
    {
        BindWithRepeater();
        lbl_smsg.Text = "File(s) Uploaded Successfully.";
    }
    private void AddColumns()
    {
        //Add 3 dummy coloumn, this can be increase on our need basis
        tbDummy.Columns.Add("ImageName");
        tbDummy.Columns.Add("ImageID");
    }
    private void BindWithRepeater()
    {
        List<AttachedImagesList> lstImages = new List<AttachedImagesList>();
        if (Session["PathImage"] != null)
        {
            lstImages = (List<AttachedImagesList>)Session["PathImage"];
        }
        int count = 1;
        for (int i = 0; i < lstImages.Count; i++)
        {
            if (i == 0)
            {
                AddColumns();
            }

            tbDummy.Rows.Add(lstImages[i].ImagePath, count++);
        }

        if (dsDummy.Tables.Count <= 0)
        {
            //add this table to dataset
            dsDummy.Tables.Add(tbDummy);
        }
        //bind this dataset to repeater
        rpImages.DataSource = dsDummy;
        rpImages.DataBind();
        tbDummy.Rows.Clear();
    }
    protected void RemoveImage_Click(object sender, CommandEventArgs e)
    {
        string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
        path = path.Replace(@"\bin\debug", "");

        List<AttachedImagesList> lstImages = new List<AttachedImagesList>();
        if (Session["PathImage"] != null)
        {
            lstImages = (List<AttachedImagesList>)Session["PathImage"];
        }

        if (e.CommandName == "Remove" && e.CommandArgument != null)
        {
            tbDummy = null;
            tbDummy = new DataTable();

            foreach (RepeaterItem item in rpImages.Items)
            {
                HiddenField hdImageID = ((HiddenField)item.FindControl("hdnImageID"));
                HtmlAnchor hylImageName = ((HtmlAnchor)item.FindControl("hylImageName"));

                if (e.CommandArgument.ToString().Trim() == hdImageID.Value.ToString().Trim())
                {
                    var itemToRemove = lstImages.SingleOrDefault(r => r.ImagePath == hylImageName.InnerText.Trim());
                    if (itemToRemove != null)
                    {
                        lstImages.Remove(itemToRemove);

                        Session["PathImage"] = lstImages;

                        //delete also from the Images folder
                        if (File.Exists(path + @"Images/DeliveryUnload/" + hylImageName.InnerText.Trim()))
                        {
                            File.Delete(path + @"Images/DeliveryUnload/" + hylImageName.InnerText.Trim());
                        }
                    }
                }
            }
            if (lstImages == null || lstImages.Count <= 0)
                Session["PathImage"] = null;
            BindWithRepeater();
        }
    }
    private void SendMail(bool IsImagesUploaded)
    {
        try
        {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));
            List<APPBOK_BookingBE> lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);
            if (lstBooking != null && lstBooking.Count > 0)
            {
                MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
                APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();
                List<MASSIT_VendorBE> lstVendorDetails = null;
                string[] sentTo;
                string[] language;
                string[] VendorData = new string[2];
                string[] CarrierData = new string[2];

                // Sprint 1 - Point 9 - Begin
                // in case of [Refused All] and [Refused Partial(accept partial)]
                if ((GetQueryStringValue("UNL").ToString() == "REJ" || GetQueryStringValue("UNL").ToString() == "PAR")
                    && lstBooking[0].BookingID != 0)
                {
                    this.SendEmailToStockPlanner(lstBooking[0].BookingID);
                    Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                }

                // Sprint 1 - Point 9 - End

                if (lstBooking[0].SupplierType == "C")
                {
                    oMASSIT_VendorBE.Action = "GetCarriersVendor";
                    oMASSIT_VendorBE.BookingID = oAPPBOK_BookingBE.BookingID;

                    lstVendorDetails = oAPPSIT_VendorBAL.GetCarriersVendorIDBAL(oMASSIT_VendorBE);

                    ViewState["lstVendorDetails"] = lstVendorDetails;
                    ViewState["lstBooking"] = lstBooking;

                    ShowInformation(IsImagesUploaded);
                }
                else if (lstBooking[0].SupplierType == "V")
                {
                    VendorData = CommonPage.GetVendorEmailsWithLanguage(lstBooking[0].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                    if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                    {
                        sentTo = VendorData[0].Split(',');
                        language = VendorData[1].Split(',');
                        for (int j = 0; j < sentTo.Length; j++)
                        {
                            if (sentTo[j] != " ")
                            {
                                SendmailToVendor(lstBooking, sentTo[j], language[j], IsImagesUploaded);
                                Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                            }
                        }
                    }
                    else
                    {
                        SendmailToVendor(lstBooking, ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", IsImagesUploaded);
                        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                    }
                    if (IsVenodrPalletChecking || IsStandardPalletChecking || ISPM15CountryPalletChecking)
                    {
                        // Insert ISPM15
                        oAPPBOK_BookingBE = new APPBOK_BookingBE();
                        oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

                        oAPPBOK_BookingBE.Action = "InsertISPM15";
                        oAPPBOK_BookingBE.VendorID = Convert.ToInt32(lstBooking[0].VendorID);
                        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));
                        oAPPBOK_BookingBE.IsVenodrPalletChecking = IsVenodrPalletChecking;
                        oAPPBOK_BookingBE.IsStandardPalletChecking = IsStandardPalletChecking;
                        oAPPBOK_BookingBE.ISPM15CountryPalletChecking = ISPM15CountryPalletChecking;

                        if (rdoDelieveryUnload1.Checked)
                        {
                            oAPPBOK_BookingBE.DeliveryChecked = true;
                            if (rdoDelieveryUnload2_1.Checked)
                            {
                                oAPPBOK_BookingBE.IssuesFound = true;
                                oAPPBOK_BookingBE.CheckedBy = txtChecked.Text;
                                oAPPBOK_BookingBE.ISPM15Comments = txtComments_1.Text;
                            }
                        }
                        else if (rdoDelieveryUnload2.Checked)
                        {
                            oAPPBOK_BookingBE.ISPM15Comments = txtCommentsOnNoDeliveryChecked.Text;
                            oAPPBOK_BookingBE.CheckedBy = Convert.ToString(Session["UserName"]);
                        }

                        oAPPBOK_BookingBAL.InsertISPM15BAL(oAPPBOK_BookingBE);

                        if (rdoDelieveryUnload2_1.Checked)
                        {
                            SendISPMIssueMail();
                        }
                    }

                    EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
    private void SendISPMIssueMail()
    {
        try
        {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
            oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));
            List<APPBOK_BookingBE> lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);
            if (lstBooking != null && lstBooking.Count > 0 && lstBooking[0].BookingID != 0)
            {
                this.SendISPMIssueEmailToStockPlanner(lstBooking[0].BookingID);
                Page.UICulture = Convert.ToString(Session["CultureInfo"]);
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
    private void ShowInformation(bool IsImagesUploaded = false)
    {
        List<APPBOK_BookingBE> lstBooking = (List<APPBOK_BookingBE>)ViewState["lstBooking"];
        try
        {
            List<APPBOK_BookingBE> lstCarrier = lstBooking.Take(1).ToList();

            if (lstCarrier.Count > 0)
            {
                chkCarrier.Text = lstCarrier[0].Carrier.CarrierName;
            }

            var lstVendor = lstBooking.Where(c => c.VendorID != 0).Select(bok => new { VendorName = bok.Vendor.VendorName, VendorID = bok.VendorID }).Distinct().ToList();

            if (lstVendor.Count > 0)
            {

                chkListVendors.DataTextField = "VendorName";
                chkListVendors.DataValueField = "VendorID";
                chkListVendors.DataSource = lstVendor;
                chkListVendors.DataBind();

                foreach (ListItem item in chkListVendors.Items)
                {
                    item.Selected = true;
                }
            }

            List<MASSIT_VendorBE> lstVendorDetails = (List<MASSIT_VendorBE>)ViewState["lstVendorDetails"];
            if (lstVendorDetails != null && lstVendorDetails.Count == 1)
            {
                SendCarrierMail(IsImagesUploaded);

                if (rdoDelieveryUnload2_1.Checked)
                {
                    SendISPMIssueMail();
                }

                EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
            }
            else if (lstVendorDetails != null && lstVendorDetails.Count > 1)
            {
                mdInformation.Show();
            }
        }
        catch
        {
            mdInformation.Show();
        }
    }
    protected void btnOK_1_Click(object sender, EventArgs e)
    {
        mdInformation.Hide();
        bool IsImagesUploaded = ViewState["IsImagesUploaded"] != null ? Convert.ToBoolean(ViewState["IsImagesUploaded"]) : false;
        SendCarrierMail(IsImagesUploaded);

        CheckBoxList listItemCollection = IsEnableHazardouesItemPrompt ? chkCarrierHazardousVendor : chkCarrierVendor;

        foreach (ListItem item in listItemCollection.Items)
        {
            if (item.Selected)
            {
                if (IsVenodrPalletChecking || IsStandardPalletChecking || ISPM15CountryPalletChecking ||IsEnableHazardouesItemPrompt)
                {
                    APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
                    APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
                     
                    oAPPBOK_BookingBE.VendorID = Convert.ToInt32(item.Value);
                    oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));
                    oAPPBOK_BookingBE.IsStandardPalletChecking = IsStandardPalletChecking;
                    oAPPBOK_BookingBE.IsVenodrPalletChecking = IsVenodrPalletChecking;
                    oAPPBOK_BookingBE.IsStandardPalletChecking = IsStandardPalletChecking;
                    oAPPBOK_BookingBE.ISPM15CountryPalletChecking = ISPM15CountryPalletChecking;
                    if (rdoDelieveryUnload1.Checked)
                    {
                        oAPPBOK_BookingBE.DeliveryChecked = true;
                        if (rdoDelieveryUnload2_1.Checked)
                        {
                            oAPPBOK_BookingBE.IssuesFound = true;
                            oAPPBOK_BookingBE.CheckedBy = txtChecked.Text;
                            oAPPBOK_BookingBE.ISPM15Comments = txtComments_1.Text;
                        }
                    }
                    else if (rdoDelieveryUnload2.Checked)
                    {
                        oAPPBOK_BookingBE.ISPM15Comments = txtCommentsOnNoDeliveryChecked.Text;
                        oAPPBOK_BookingBE.CheckedBy = Convert.ToString(Session["UserName"]);
                    }

                    if (rdoHDelieveryUnload1.Checked)
                    {
                        oAPPBOK_BookingBE.DeliveryChecked = true;
                        if (rdoDelieveryUnload2_1.Checked)
                        {
                            oAPPBOK_BookingBE.IssuesFound = true;
                            oAPPBOK_BookingBE.CheckedBy = txtHChecked.Text;
                            oAPPBOK_BookingBE.HazardousCommments = txtHComments_1.Text;
                        }
                    }
                    else if (rdoHDelieveryUnload2.Checked)
                    {
                        oAPPBOK_BookingBE.HazardousCommments = txtHCommentsOnNoDeliveryChecked.Text;
                        oAPPBOK_BookingBE.CheckedBy = Convert.ToString(Session["UserName"]);
                    }

                    if (IsVenodrPalletChecking || IsStandardPalletChecking || ISPM15CountryPalletChecking)
                    {
                        oAPPBOK_BookingBE.Action = "InsertISPM15";
                        oAPPBOK_BookingBAL.InsertISPM15BAL(oAPPBOK_BookingBE);
                    }

                    if (IsEnableHazardouesItemPrompt)
                    {
                        oAPPBOK_BookingBE.Action = "InsertHazardousItem";
                        oAPPBOK_BookingBAL.InsertHazardousItemDetailsBAL(oAPPBOK_BookingBE);
                    }                   
                }
            }
        }

        if (rdoDelieveryUnload2_1.Checked)
        {
            SendISPMIssueMail();
        }

        EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
    }
    private void SendCarrierMail(bool IsImagesUploaded = false)
    {
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oAPPSIT_VendorBAL = new APPSIT_VendorBAL();

        string[] sentTo;
        string[] language;
        string[] VendorData = new string[2];
        string[] CarrierData = new string[2];

        List<MASSIT_VendorBE> lstVendorDetails = (List<MASSIT_VendorBE>)ViewState["lstVendorDetails"];
        List<APPBOK_BookingBE> lstBooking = (List<APPBOK_BookingBE>)ViewState["lstBooking"];


        bool MailToBeSent = false;

        foreach (ListItem a in chkListVendors.Items)
        {
            if (a.Selected)
            {
                MailToBeSent = true;
                break;
            }
        }

        if (chkCarrier.Checked)
        {
            MailToBeSent = true;
        }


        string str = "=======================================================================================================================";
        StringBuilder sb = new StringBuilder();
        sb.Append(str + "\r\n");
        sb.Append("Start Carrier unexpected Booking Trace" + "\r\n");

        if (lstBooking != null && lstBooking.Count > 0)
        {
            sb.Append("BookingID :" + lstBooking[0].BookingID + " BookingRef" + lstBooking[0].BookingRef + "\r\n");
        }

        sb.Append("UserID :" + Session["UserID"] + "\r\n");
        sb.Append("User :" + Session["UserName"] + "\r\n");
        LogUtility.WriteTrace(sb);

        if (lstVendorDetails != null && lstVendorDetails.Count > 0)
        {
            for (int i = 0; i < lstVendorDetails.Count; i++)
            {
                foreach (ListItem a in chkListVendors.Items)
                {
                    if (a.Selected && a.Value == Convert.ToString(lstVendorDetails[i].VendorID))
                    {
                        VendorData = CommonPage.GetVendorEmailsWithLanguage(lstVendorDetails[i].VendorID, Convert.ToInt32(lstBooking[0].SiteId));
                        break;
                    }
                }

                if (chkCarrier.Checked)
                {
                    /* Logic to adding the carrier user's email with Vendor Email. */
                    CarrierData = CommonPage.GetCarrierEmailsWithLanguage(Convert.ToInt32(lstBooking[0].Carrier.CarrierID), Convert.ToInt32(lstBooking[0].SiteId));
                    if (CarrierData.Length > 0)
                    {
                        VendorData[0] += CarrierData[0] + ",";
                        VendorData[1] += CarrierData[1] + ",";
                    }

                    sb.Append("CarrierData[0] :" + CarrierData[0] + "\r\n");
                    sb.Append("CarrierData[1] :" + CarrierData[1] + "\r\n");

                    LogUtility.WriteTrace(sb);
                }

                if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                {
                    sentTo = VendorData[0].Split(',');
                    language = VendorData[1].Split(',');
                    sb = new StringBuilder();
                    sb.Append("(VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))");
                    sb.Append("VendorData[0] :" + VendorData[0] + "\r\n");
                    sb.Append("VendorData[1] :" + VendorData[1] + "\r\n");

                    LogUtility.WriteTrace(sb);
                    for (int j = 0; j < sentTo.Length; j++)
                    {
                        if (sentTo[j] != " ")
                        {
                            SendmailToVendor(lstBooking, sentTo[j], language[j], IsImagesUploaded);
                            Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                        }
                    }
                }
                else if (MailToBeSent)
                {
                    SendmailToVendor(lstBooking, ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", IsImagesUploaded);
                    Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                }
            }
        }
    }
    private void SendmailToVendor(List<APPBOK_BookingBE> lstBooking, string toAddress, string language, bool IsImagesUploaded = false)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            string htmlBody = string.Empty;
            string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
            string templatePathName = @"/EmailTemplates/Appointment/";
            string LanguageFile = string.Empty;
            string templatePath = path + templatePathName;
            var communicationBAL = new APPBOK_CommunicationBAL();
            var lstLanguages = communicationBAL.GetLanguages();
            foreach (BusinessEntities.ModuleBE.Languages.Languages.MAS_LanguageBE objLanguage in lstLanguages)
            {
                bool MailSentInLanguage = false;

                string Issues = string.Empty;
                if (chkEarly.Checked)
                    Issues += WebCommon.getGlobalResourceValue("Early", objLanguage.Language) + "<br/>";
                if (chkLate.Checked)
                    Issues += WebCommon.getGlobalResourceValue("Late", objLanguage.Language) + "<br/>";
                if (chkNoPaperworkondisplay.Checked)
                    Issues += WebCommon.getGlobalResourceValue("NoPaperworkondisplay", objLanguage.Language) + "<br/>";
                if (chkNotToOdSpecification.Checked)
                    Issues += WebCommon.getGlobalResourceValue("NotToOdSpecification", objLanguage.Language) + "<br/>";
                if (chkOther.Checked)
                    Issues += WebCommon.getGlobalResourceValue("Other", objLanguage.Language) + "<br/>";
                if (chkPackagingDamaged.Checked)
                    Issues += WebCommon.getGlobalResourceValue("PackagingDamaged", objLanguage.Language) + "<br/>";
                if (chkPalletsDamaged.Checked)
                    Issues += WebCommon.getGlobalResourceValue("PalletsDamaged", objLanguage.Language) + "<br/>";
                if (chkRefusedtowait.Checked)
                    Issues += WebCommon.getGlobalResourceValue("Refusedtowait", objLanguage.Language) + "<br/>";
                if (chkWrongAddress.Checked)
                    Issues += WebCommon.getGlobalResourceValue("WrongAddress", objLanguage.Language) + "<br/>";
                if (chkunsafeload.Checked)
                    Issues += WebCommon.getGlobalResourceValue("unsafeload", objLanguage.Language) + "<br/>";
                if (chkFailedToDeclareHazardousGoods.Checked)
                    Issues += WebCommon.getGlobalResourceValue("FailedToDeclareHazardousGoods", objLanguage.Language) + "<br/>";

                if (objLanguage.Language.ToLower() == language.ToLower())
                {
                    MailSentInLanguage = true;
                }


                switch (GetQueryStringValue("UNL").ToString()) // in case of Refused All
                {
                    case "REJ":
                        LanguageFile = "Refused.english.htm";
                        break;
                    case "PAR":
                        LanguageFile = "RefusedPartial.english.htm";
                        break;
                    case "FULL":
                        LanguageFile = "DeliveryIssues.english.htm";
                        break;
                }

                #region Setting reason as per the language ...
                switch (objLanguage.LanguageID)
                {
                    case 1:
                        Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                        break;
                    case 2:
                        Page.UICulture = clsConstants.FranceISO; // // France
                        break;
                    case 3:
                        Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                        break;
                    case 4:
                        Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                        break;
                    case 5:
                        Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                        break;
                    case 6:
                        Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                        break;
                    case 7:
                        Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                        break;
                    default:
                        Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                        break;
                }

                #endregion

                sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();

                using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
                {
                    #region mailBody
                    htmlBody = sReader.ReadToEnd();

                    /* Changed for [Refused.english.htm] file */
                    htmlBody = htmlBody.Replace("{RefusalText1}", WebCommon.getGlobalResourceValue("RefusalText1"));
                    htmlBody = htmlBody.Replace("{RefusalText2}", WebCommon.getGlobalResourceValue("RefusalText2"));

                    /* Changed for [RefusedPartial.english.htm] file */
                    htmlBody = htmlBody.Replace("{RefusedPartialText1}", WebCommon.getGlobalResourceValue("RefusedPartialText1"));
                    htmlBody = htmlBody.Replace("{DetailsOfIssue}", WebCommon.getGlobalResourceValue("DetailsOfIssue"));
                    htmlBody = htmlBody.Replace("{RefusedPartialText2}", WebCommon.getGlobalResourceValue("RefusedPartialText2"));
                    htmlBody = htmlBody.Replace("{RefusedPartialText3}", WebCommon.getGlobalResourceValue("RefusedPartialText3"));

                    /* Changed for [DeliveryIssues.english.htm] file */
                    htmlBody = htmlBody.Replace("{DeliveryIssuesText1}", WebCommon.getGlobalResourceValue("DeliveryIssuesText1"));
                    htmlBody = htmlBody.Replace("{DeliveryIssuesText2}", WebCommon.getGlobalResourceValue("DeliveryIssuesText2"));
                    htmlBody = htmlBody.Replace("{DeliveryIssuesText3}", WebCommon.getGlobalResourceValue("DeliveryIssuesText3"));
                    htmlBody = htmlBody.Replace("{DeliveryIssuesText4}", WebCommon.getGlobalResourceValue("DeliveryIssuesText4"));
                    htmlBody = htmlBody.Replace("{KindRegards}", WebCommon.getGlobalResourceValue("KindRegards"));

                    /* Changed For All letters files */
                    htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                    htmlBody = htmlBody.Replace("{ConfirmationText1}", WebCommon.getGlobalResourceValue("ConfirmationText1"));
                    htmlBody = htmlBody.Replace("{ConfirmationText1}", WebCommon.getGlobalResourceValue("ConfirmationText1"));
                    htmlBody = htmlBody.Replace("{ConfirmationText1}", WebCommon.getGlobalResourceValue("ConfirmationText1"));
                    htmlBody = htmlBody.Replace("{ConfirmationText1}", WebCommon.getGlobalResourceValue("ConfirmationText1"));


                    htmlBody = htmlBody.Replace("{BookingReference}", WebCommon.getGlobalResourceValue("BookingReference"));
                    htmlBody = htmlBody.Replace("{BookingRefValue}", lstBooking[0].BookingRef);

                    htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));
                    htmlBody = htmlBody.Replace("{DateValue}", lstBooking[0].ScheduleDate.Value.ToString("dd/MM/yyyy"));

                    htmlBody = htmlBody.Replace("{Site}", WebCommon.getGlobalResourceValue("Site"));
                    htmlBody = htmlBody.Replace("{SiteValue}", lstBooking[0].SiteName);

                    if (lstBooking[0].SupplierType == "V")
                    {
                        htmlBody = htmlBody.Replace("{POs}", WebCommon.getGlobalResourceValue("POs"));
                        htmlBody = htmlBody.Replace("{POsValue}", lstBooking[0].PurchaseOrders.TrimStart(',').TrimEnd(','));
                    }
                    else if (lstBooking[0].SupplierType == "C")
                    {
                        if (lstBooking.Count >= 1)
                        {
                            htmlBody = htmlBody.Replace("{POs}", WebCommon.getGlobalResourceValue("POs"));
                            htmlBody = htmlBody.Replace("{POsValue}", lstBooking[0].PurchaseOrders.TrimStart(',').TrimEnd(','));
                        }
                        else
                        {
                            htmlBody = htmlBody.Replace("{POs}", string.Empty);
                            htmlBody = htmlBody.Replace("{POsValue}", string.Empty);
                        }
                    }

                    htmlBody = htmlBody.Replace("{IssueValue}", Issues);
                    htmlBody = htmlBody.Replace("{Comments}", WebCommon.getGlobalResourceValue("Comments"));
                    htmlBody = htmlBody.Replace("{CommentsValue}", txtRefusalComments.Text);

                    if (IsImagesUploaded)
                    {
                        htmlBody = htmlBody.Replace("{ImageUploadText}", WebCommon.getGlobalResourceValue("ImageUploadText"));
                        htmlBody = htmlBody.Replace("{tblVisible}", "");
                    }
                    else
                    {
                        htmlBody = htmlBody.Replace("{tblVisible}", "none");
                    }


                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                    #endregion
                }

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                clsEmail oclsEmail = new clsEmail();

                CommunicationType.Enum communicationType;
                string subject = string.Empty;

                switch (GetQueryStringValue("UNL").ToString())
                {
                    case "FULL":
                        subject = "Delivery Issue";
                        communicationType = CommunicationType.Enum.DeliveryIssue;
                        break;
                    case "REJ":
                        subject = "Refused  Delivery - Urgent Response Required";
                        communicationType = CommunicationType.Enum.RefusedAll;
                        break;
                    default:
                        subject = "Refused  Delivery - Urgent Response Required";
                        communicationType = CommunicationType.Enum.RefusedPartial;
                        break;
                }


                // Resending of Delivery mails
                APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                APPBOK_CommunicationBE oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
                oAPPBOK_CommunicationBE.Action = "AddBookingCommunication";
                oAPPBOK_CommunicationBE.CommunicationType = CommunicationType.Name(communicationType);
                oAPPBOK_CommunicationBE.BookingID = lstBooking[0].BookingID;
                oAPPBOK_CommunicationBE.Subject = subject;
                oAPPBOK_CommunicationBE.SentFrom = sFromAddress;
                oAPPBOK_CommunicationBE.SentTo = toAddress;
                oAPPBOK_CommunicationBE.Body = htmlBody;
                oAPPBOK_CommunicationBE.SendByID = Convert.ToInt32(Session["UserID"]);
                oAPPBOK_CommunicationBE.IsMailSent = MailSentInLanguage;
                oAPPBOK_CommunicationBE.LanguageID = objLanguage.LanguageID;
                oAPPBOK_CommunicationBE.MailSentInLanguage = MailSentInLanguage;
                int intIsExist = oAPPBOK_CommunicationBAL.AddBookingCommunicationBAL(oAPPBOK_CommunicationBE) ?? 0;

                if (intIsExist.Equals(0) && MailSentInLanguage.Equals(true) && !string.IsNullOrWhiteSpace(toAddress))
                    oclsEmail.sendMail(toAddress, htmlBody, subject, sFromAddress, true);
            }
        }
    }
    // Sprint 1 - Point 9 - Begin
    private void SendEmailToStockPlanner(int intBookingID)
    {
        try
        {
            APPBOK_BookingBE finalRefusedDeliveriesBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            string mailBody = string.Empty;
            string mailSubject = "Refused Delivery";
            string fromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

            // Logic to get data for Refused Deliveries
            APPBOK_BookingBE refusedDeliveriesBE = new APPBOK_BookingBE();
            refusedDeliveriesBE.Action = "EmailDataForRefusedDeliveries";
            refusedDeliveriesBE.BookingID = intBookingID;

            List<APPBOK_BookingBE> lstRefusedDeliveries = oAPPBOK_BookingBAL.GetEmailDataForRefusedDeliveriesBAL(refusedDeliveriesBE);
            int intCount = 0;
            foreach (APPBOK_BookingBE ab in lstRefusedDeliveries)
            {
                #region Hoding actual refusal data in [APPBOK_BookingBE finalRefusedDeliveriesBE] object
                if (intCount == 0)
                {
                    finalRefusedDeliveriesBE.SiteName = ab.SiteName;
                    finalRefusedDeliveriesBE.BookingRef = ab.BookingRef;
                    finalRefusedDeliveriesBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    finalRefusedDeliveriesBE.Vendor.VendorName = ab.Vendor.VendorName;

                    finalRefusedDeliveriesBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    finalRefusedDeliveriesBE.Carrier.CarrierName = ab.Carrier.CarrierName;

                    finalRefusedDeliveriesBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    finalRefusedDeliveriesBE.SlotTime.SlotTime = ab.SlotTime.SlotTime;
                    finalRefusedDeliveriesBE.Status = ab.Status;
                    finalRefusedDeliveriesBE.Vendor.VendorContactEmail = ab.Vendor.VendorContactEmail;
                    finalRefusedDeliveriesBE.DLYREF_ReasonComments = ab.DLYREF_ReasonComments;

                    string reasonForRefusal = String.Empty;
                    if (ab.DLYREF_ReasonArrivedEarly)
                        reasonForRefusal = "Arrived Early";

                    if (ab.DLYREF_ReasonArrivedLate)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Arrived Late");

                    if (ab.DLYREF_ReasonNoPaperworkOndisplay)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "No Paper Work on Display");

                    if (ab.DLYREF_ReasonPalletsDamaged)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Pallets Damaged");

                    if (ab.DLYREF_ReasonPackagingDamaged)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Packaging Damaged");

                    if (ab.DLYREF_ReasonUnsafeLoad)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Unsafe Load");

                    if (ab.DLYREF_ReasonWrongAddress)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Wrong Address");

                    if (ab.DLYREF_ReasonRefusedTowait)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Refused to Wait");

                    if (ab.DLYREF_ReasonNottoODSpecification)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Not to OD Specification");

                    if (ab.DLYREF_ReasonOther)
                        reasonForRefusal = string.Format("{0}<br />{1}", reasonForRefusal, "Other");

                    finalRefusedDeliveriesBE.BookingComments = reasonForRefusal;

                    finalRefusedDeliveriesBE.PurchaseOrders = string.Format("{0}     {1}", ab.PurchaseOrders, ab.Prioirty);
                }
                else
                {
                    finalRefusedDeliveriesBE.PurchaseOrders = string.Format("{0}<br />{1}     {2}", finalRefusedDeliveriesBE.PurchaseOrders, ab.PurchaseOrders, ab.Prioirty);
                }
                intCount++;
                #endregion
            }

            // Declareations
            // Logic to sending email to Stock Planner.
            APPBOK_BookingBE stockPlannerEmailBE = new APPBOK_BookingBE
            {
                Action = "EmailIdsForStockPlanner",
                BookingID = intBookingID
            };

            List<APPBOK_BookingBE> lstStockPlannerEmail = oAPPBOK_BookingBAL.GetEmailIdsForStockPlannerBAL(stockPlannerEmailBE);

            foreach (APPBOK_BookingBE ab in lstStockPlannerEmail)
            {
                // Here setting default [Stock Planner Email Id and Default Language] if not available in database.
                string toEmail = !string.IsNullOrWhiteSpace(ab.Vendor.VendorContactEmail) ? ab.Vendor.VendorContactEmail : strDefaultEmailId;
                string strLanguage = !string.IsNullOrWhiteSpace(ab.Language) ? ab.Language : clsConstants.English;
                mailBody = this.GetStockPlannerEmailBody(finalRefusedDeliveriesBE, strLanguage);
                clsEmail oclsEmail = new clsEmail();
                oclsEmail.sendMail(toEmail, mailBody, mailSubject, fromAddress, true);
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
    private string GetStockPlannerEmailBody(APPBOK_BookingBE oAPPBOK_BookingBE, string language)
    {
        string htmlBody = string.Empty;
        string templatePath = path + templatePathName;
        string LanguageFile = "RefusalLetterToStockPlanner.english.htm";

        #region Setting reason as per the language ...
        switch (language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            #region mailBody
            htmlBody = sReader.ReadToEnd();

            htmlBody = htmlBody.Replace("{RefusalLetterToSPText1}", WebCommon.getGlobalResourceValue("RefusalLetterToSPText1"));
            htmlBody = htmlBody.Replace("{DeliveryDetailsHistory}", WebCommon.getGlobalResourceValue("DeliveryDetailsHistory"));

            htmlBody = htmlBody.Replace("{ProvisionalBookingMLText1}", WebCommon.getGlobalResourceValue("ProvisionalBookingMLText1"));
            htmlBody = htmlBody.Replace("{ProvisionalBookingMLText1}", WebCommon.getGlobalResourceValue("ProvisionalBookingMLText1"));

            htmlBody = htmlBody.Replace("{Site}", WebCommon.getGlobalResourceValue("Site"));
            htmlBody = htmlBody.Replace("#SiteName#", oAPPBOK_BookingBE.SiteName);

            htmlBody = htmlBody.Replace("{BookingReference}", WebCommon.getGlobalResourceValue("BookingReference"));
            htmlBody = htmlBody.Replace("#BookingReference#", oAPPBOK_BookingBE.BookingRef);

            htmlBody = htmlBody.Replace("{Vendor}", WebCommon.getGlobalResourceValue("Vendor"));
            htmlBody = htmlBody.Replace("#Vendor#", oAPPBOK_BookingBE.Vendor.VendorName);

            htmlBody = htmlBody.Replace("{Carrier}", WebCommon.getGlobalResourceValue("Carrier"));
            htmlBody = htmlBody.Replace("#Carrier#", oAPPBOK_BookingBE.Carrier.CarrierName);

            htmlBody = htmlBody.Replace("{Bookedinfor}", WebCommon.getGlobalResourceValue("Bookedinfor"));
            htmlBody = htmlBody.Replace("#BookedInFor#", oAPPBOK_BookingBE.SlotTime.SlotTime);

            htmlBody = htmlBody.Replace("{RefusedAt}", WebCommon.getGlobalResourceValue("RefusedAt"));
            htmlBody = htmlBody.Replace("#RefusedAt#", oAPPBOK_BookingBE.Status);

            htmlBody = htmlBody.Replace("{AuthorisedBy}", WebCommon.getGlobalResourceValue("AuthorisedBy"));
            htmlBody = htmlBody.Replace("#AuthorisedBy#", oAPPBOK_BookingBE.Vendor.VendorContactEmail);

            htmlBody = htmlBody.Replace("{PurchaseOrders}", WebCommon.getGlobalResourceValue("PurchaseOrders"));
            htmlBody = htmlBody.Replace("#PurchaseOrders#", oAPPBOK_BookingBE.PurchaseOrders);

            htmlBody = htmlBody.Replace("{ReasonforRefusal}", WebCommon.getGlobalResourceValue("ReasonforRefusal"));
            htmlBody = htmlBody.Replace("#ReasonForRefusal#", oAPPBOK_BookingBE.BookingComments);

            htmlBody = htmlBody.Replace("{CommentLabel}", WebCommon.getGlobalResourceValue("CommentLabel"));
            htmlBody = htmlBody.Replace("#Comment#", oAPPBOK_BookingBE.DLYREF_ReasonComments);
            #endregion
        }
        return htmlBody;
    }
    // Sprint 1 - Point 9 - End
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove("PathImage");
        RevertChanges();
        EncryptQueryString("APPRcv_DeliveryUnloaded.aspx?Scheduledate=" + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString());
    }
    private void SendISPMIssueEmailToStockPlanner(int intBookingID)
    {
        try
        {
            APPBOK_BookingBE finalRefusedDeliveriesBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            string mailBody = string.Empty;

            APPBOK_BookingBE refusedDeliveriesBE = new APPBOK_BookingBE();
            refusedDeliveriesBE.Action = "GetISPMIssueEmailData";
            refusedDeliveriesBE.BookingID = intBookingID;
            List<APPBOK_BookingBE> lstEmailData = oAPPBOK_BookingBAL.GetEmailDataForISPMIssueBAL(refusedDeliveriesBE);


            string fromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

            // Declareations
            // Logic to sending email to Stock Planner.
            APPBOK_BookingBE stockPlannerEmailBE = new APPBOK_BookingBE();
            stockPlannerEmailBE.Action = "EmailIdsForStockPlannerISPMIssue";
            stockPlannerEmailBE.BookingID = intBookingID;
            stockPlannerEmailBE.IsVenodrPalletChecking = IsVenodrPalletChecking;
            stockPlannerEmailBE.IsStandardPalletChecking = IsStandardPalletChecking;
            stockPlannerEmailBE.ISPM15CountryPalletChecking = ISPM15CountryPalletChecking;
            List<APPBOK_BookingBE> lstStockPlannerEmail = oAPPBOK_BookingBAL.GetEmailIdsForStockPlannerISPMIssueBAL(stockPlannerEmailBE);

            foreach (APPBOK_BookingBE ab in lstStockPlannerEmail)
            {
                // Here setting default [Stock Planner Email Id and Default Language] if not available in database.
                string toEmail = String.Empty;
                string strLanguage = String.Empty;
                if (!string.IsNullOrWhiteSpace(ab.StockPlannerEmail))
                    toEmail = ab.StockPlannerEmail;

                if (!string.IsNullOrWhiteSpace(ab.Language))
                    strLanguage = ab.Language;

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                clsEmail oclsEmail = new clsEmail();

                CommunicationType.Enum communicationType;
                string subject = string.Empty;

                if (lstEmailData[0].IsStandardPalletChecking)
                {
                    subject = "Standard Pallet Check Issue from VIP";
                }
                else
                {
                    subject = "ISPM15 ISSUE from VIP";
                }

                communicationType = CommunicationType.Enum.ISPM15Issue;

                var communicationBAL = new APPBOK_CommunicationBAL();
                var lstLanguages = communicationBAL.GetLanguages();

                if (!string.IsNullOrEmpty(toEmail))
                {
                    foreach (BusinessEntities.ModuleBE.Languages.Languages.MAS_LanguageBE objLanguage in lstLanguages)
                    {
                        bool MailSentInLanguage = objLanguage.Language.ToLower() == strLanguage.ToLower();
                        mailBody = this.GetStockPlannerISPMIssueEmailBody(lstEmailData, strLanguage);
                        // Resending of Delivery mails
                        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                        APPBOK_CommunicationBE oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE
                        {
                            Action = "AddBookingCommunication",
                            CommunicationType = CommunicationType.Name(communicationType),
                            BookingID = intBookingID,
                            Subject = subject,
                            SentFrom = sFromAddress,
                            SentTo = toEmail,
                            Body = mailBody,
                            SendByID = Convert.ToInt32(Session["UserID"]),
                            IsMailSent = MailSentInLanguage,
                            LanguageID = objLanguage.LanguageID,
                            MailSentInLanguage = MailSentInLanguage
                        };
                        int intIsExist = oAPPBOK_CommunicationBAL.AddBookingCommunicationBAL(oAPPBOK_CommunicationBE) ?? 0;
                        if (intIsExist.Equals(0) && MailSentInLanguage.Equals(true))
                        {
                            if (!string.IsNullOrWhiteSpace(toEmail))
                                oclsEmail.sendMail(toEmail, mailBody, subject, sFromAddress, true);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
    private string GetStockPlannerISPMIssueEmailBody(List<APPBOK_BookingBE> lstBookingBE, string language)
    {
        string htmlBody = string.Empty;
        string templatePath = path + templatePathName;

        string LanguageFile = "ISPM15IssueLetter.english.htm";

        #region Setting reason as per the language ...
        switch (language)
        {
            case clsConstants.English:
                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                break;
            case clsConstants.French:
                Page.UICulture = clsConstants.FranceISO; // // France
                break;
            case clsConstants.German:
                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                break;
            case clsConstants.Dutch:
                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                break;
            case clsConstants.Spanish:
                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                break;
            case clsConstants.Italian:
                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                break;
            case clsConstants.Czech:
                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                break;
            default:
                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                break;
        }

        #endregion

        List<MASSIT_VendorBE> lstVendorDetails = (List<MASSIT_VendorBE>)ViewState["lstVendorDetails"];
        List<APPBOK_BookingBE> lstBooking = (List<APPBOK_BookingBE>)ViewState["lstBooking"];

        using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
        {
            #region mailBody
            htmlBody = sReader.ReadToEnd();

            htmlBody = htmlBody.Replace("{Hi}", WebCommon.getGlobalResourceValue("Hi"));

            System.Text.StringBuilder vendorname = new System.Text.StringBuilder();

            if (lstBookingBE != null && lstBookingBE.Count > 0)
            {
                if (lstBookingBE[0].SupplierType == "C")
                {
                    for (int i = lstBookingBE.Count - 1; i >= 0; i--)
                    {
                        vendorname.Append(lstBookingBE[i].Vendor.VendorName + "<br />");
                    }

                    htmlBody = htmlBody.Replace("{VendorNameValue}", vendorname.ToString());
                }
                else
                {
                    htmlBody = htmlBody.Replace("{VendorNameValue}", lstBookingBE[0].Vendor.VendorName);
                }

                if (lstBookingBE[0].IsVenodrPalletChecking && !lstBookingBE[0].IsStandardPalletChecking)
                    htmlBody = htmlBody.Replace("{ISPM15IssueText}", WebCommon.getGlobalResourceValue("ISPM15IssueText"));
                else if (!lstBookingBE[0].IsVenodrPalletChecking && lstBookingBE[0].IsStandardPalletChecking)
                    htmlBody = htmlBody.Replace("{ISPM15IssueText}", WebCommon.getGlobalResourceValue("StandardPalletcheckIssueText"));
                else if (lstBookingBE[0].ISPM15CountryPalletChecking)
                    htmlBody = htmlBody.Replace("{ISPM15IssueText}", WebCommon.getGlobalResourceValue("ISPM15IssueText"));
                else
                    htmlBody = htmlBody.Replace("{ISPM15IssueText}", string.Empty);

                htmlBody = htmlBody.Replace("{BookingRef}", WebCommon.getGlobalResourceValue("BookingRef"));

                htmlBody = htmlBody.Replace("{BookingRefValue}", lstBookingBE[0].BookingRef);
                htmlBody = htmlBody.Replace("{VendorNameValue}", lstBookingBE[0].Vendor.VendorName);

                htmlBody = htmlBody.Replace("{Comments}", WebCommon.getGlobalResourceValue("Comments"));
                htmlBody = htmlBody.Replace("{CommentsValue}", lstBookingBE[0].ISPM15Comments);
                htmlBody = htmlBody.Replace("{CheckerName}", WebCommon.getGlobalResourceValue("CheckerName"));
                htmlBody = htmlBody.Replace("{CheckerNameValue}", lstBookingBE[0].CheckedBy);

                htmlBody = htmlBody.Replace("{POs}", WebCommon.getGlobalResourceValue("POs"));

                htmlBody = htmlBody.Replace("{ISPM15Messege1}", WebCommon.getGlobalResourceValue("ISPM15Messege1"));
                htmlBody = htmlBody.Replace("{ISPM15Messege2}", WebCommon.getGlobalResourceValue("ISPM15Messege2"));
                htmlBody = htmlBody.Replace("{ISPM15Messege3}", WebCommon.getGlobalResourceValue("ISPM15Messege3"));
                htmlBody = htmlBody.Replace("{ISPM15Messege4}", WebCommon.getGlobalResourceValue("ISPM15Messege4"));
                htmlBody = htmlBody.Replace("{ISPM15Messege5}", WebCommon.getGlobalResourceValue("ISPM15Messege5"));
                htmlBody = htmlBody.Replace("{ISPM15Messege6}", WebCommon.getGlobalResourceValue("ISPM15Messege6"));
                htmlBody = htmlBody.Replace("{ISPM15Messege7}", WebCommon.getGlobalResourceValue("ISPM15Messege7"));


                foreach (APPBOK_BookingBE lst in lstBookingBE)
                {
                    htmlBody = htmlBody.Replace("{POsValue}", lst.PurchaseOrders.Trim(','));
                }

                htmlBody = htmlBody.Replace("{KindRegards}", WebCommon.getGlobalResourceValue("KindRegards"));
                htmlBody = htmlBody.Replace("{VIPAdmin}", WebCommon.getGlobalResourceValue("VIPAdmin"));

            }
            #endregion
        }
        return htmlBody;
    }
    protected void BtnConfirm_Click(object sender, EventArgs e)
    {
        SaveDeliveryUnload();
        updInfromation_Messge_update.Hide();
    }
    protected void BtnCancel1_Click(object sender, EventArgs e)
    {
        Session.Remove("PathImage");
        RevertChanges();
        updInfromation_Messge_update.Hide();
    }
    public void RevertChanges()
    {
        try
        {
            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
            oAPPBOK_BookingBE.Action = "UndoDeliveryUnload";
            oAPPBOK_BookingBE.UserID = Convert.ToInt32(Session["UserID"].ToString());
            oAPPBOK_BookingBAL.UndoBookingDeliveryUnLoadStatusBAL(oAPPBOK_BookingBE);
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    public void SaveDeliveryUnload()
    {
        try
        {
            string objSelectedUserPass = ddlUser.SelectedValue.Split('-')[1];

            APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
            APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
            oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
            oAPPBOK_BookingBE.Action = "UpdateDeliveryUnloadedRefusalStep";
            oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");

            switch (GetQueryStringValue("UNL").ToString())
            {
                case "FULL":
                    oAPPBOK_BookingBE.BookingStatusID = Convert.ToInt32(Common.BookingStatus.DeliveryUnloaded);
                    break;
                case "PAR":
                    oAPPBOK_BookingBE.BookingStatusID = Convert.ToInt32(Common.BookingStatus.PartiallyDelivery);
                    break;
                default:
                    oAPPBOK_BookingBE.BookingStatusID = Convert.ToInt16(Common.BookingStatus.RefusedDelivery);
                    break;
            }

            oAPPBOK_BookingBE.DLYREF_ReasonArrivedEarly = chkEarly.Checked;
            oAPPBOK_BookingBE.DLYREF_ReasonArrivedLate = chkLate.Checked;
            oAPPBOK_BookingBE.DLYREF_ReasonNoPaperworkOndisplay = chkNoPaperworkondisplay.Checked;
            oAPPBOK_BookingBE.DLYREF_ReasonPalletsDamaged = chkPalletsDamaged.Checked;
            oAPPBOK_BookingBE.DLYREF_ReasonPackagingDamaged = chkPackagingDamaged.Checked;
            oAPPBOK_BookingBE.DLYREF_ReasonUnsafeLoad = chkunsafeload.Checked;
            oAPPBOK_BookingBE.DLYREF_ReasonWrongAddress = chkWrongAddress.Checked;
            oAPPBOK_BookingBE.DLYREF_ReasonRefusedTowait = chkRefusedtowait.Checked;
            oAPPBOK_BookingBE.DLYREF_ReasonNottoODSpecification = chkNotToOdSpecification.Checked;
            oAPPBOK_BookingBE.DLYREF_ReasonOther = chkOther.Checked;
            oAPPBOK_BookingBE.DLYREF_ReasonComments = txtRefusalComments.Text;
            oAPPBOK_BookingBE.DLYREF_FailedToDeclareHazardousGoods = chkFailedToDeclareHazardousGoods.Checked;
            oAPPBOK_BookingBE.DLYREF_EuroPalletsDelivered = txtEuroDelivered.Text != string.Empty ? Convert.ToInt16(txtEuroDelivered.Text) : (int?)null;
            oAPPBOK_BookingBE.DLYREF_EuroPalletsReturned = txtEuroReturned.Text != string.Empty ? Convert.ToInt16(txtEuroReturned.Text) : (int?)null;
            oAPPBOK_BookingBE.DLYREF_EuroPalletsComments = txtEuroComments.Text;
            oAPPBOK_BookingBE.DLYREF_UKStandardDelivered = txtUKDeliverd.Text != string.Empty ? Convert.ToInt16(txtUKDeliverd.Text) : (int?)null;
            oAPPBOK_BookingBE.DLYREF_UKStandardReturned = txtUKRetured.Text != string.Empty ? Convert.ToInt16(txtUKRetured.Text) : (int?)null;
            oAPPBOK_BookingBE.DLYREF_UKStandardComments = txtUKComments.Text;
            oAPPBOK_BookingBE.DLYREF_CHEPDelivered = txtCHEPDelivered.Text != string.Empty ? Convert.ToInt16(txtCHEPDelivered.Text) : (int?)null;
            oAPPBOK_BookingBE.DLYREF_CHEPReturned = txtCHEPReturned.Text != string.Empty ? Convert.ToInt16(txtCHEPReturned.Text) : (int?)null;
            oAPPBOK_BookingBE.DLYREF_CHEPComments = txtCHEPComments.Text;
            oAPPBOK_BookingBE.DLYREF_OthersDelivered = txtOthersDelivered.Text != string.Empty ? Convert.ToInt16(txtOthersDelivered.Text) : (int?)null;
            oAPPBOK_BookingBE.DLYREF_OthersReturned = txtOthersReturned.Text != string.Empty ? Convert.ToInt16(txtOthersReturned.Text) : (int?)null;
            oAPPBOK_BookingBE.DLYREF_OthersComments = txtOtherCommentsPallets.Text;
            oAPPBOK_BookingBE.FixedSlot.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

            oAPPBOK_BookingBE.FixedSlot.User.UserID = Convert.ToInt32(Session["UserID"].ToString().Trim());

            oAPPBOK_BookingBE.DLYREF_SupervisorID = objSelectedUserPass != null && txtSupervisorPassword.Text != null && hdnIsDelUnloadPassAllow.Value == "N"
                ? Convert.ToInt32(Session["UserID"].ToString().Trim())
                : ddlUser.SelectedIndex >= 0 ? Convert.ToInt32(ddlUser.SelectedItem.Value.Split('-')[0]) : 0;

            // Sprint 1 - Point 8 - Start
            if (Session["TransactionComments"] != null && Session["TransactionComments"].ToString() != "")
            {
                oAPPBOK_BookingBE.TransactionComments = Session["TransactionComments"].ToString().Trim();
            }
            // Sprint 1 - Point 8 - End

            List<AttachedImagesList> lstImages = new List<AttachedImagesList>();
            bool IsImagesUploaded = false;
            if (Session["PathImage"] != null && Session["PathImage"].ToString() != "")
            {
                if (Session["PathImage"] != null)
                {
                    lstImages = (List<AttachedImagesList>)Session["PathImage"];
                }

                if (lstImages != null && lstImages.Count > 0)
                {
                    oAPPBOK_BookingBE.IsUnloadImagesUploaded = true;
                    IsImagesUploaded = true;
                    ViewState["IsImagesUploaded"] = IsImagesUploaded;
                }
            }

            int? iResult = oAPPBOK_BookingBAL.updateDeliveryUnloadedFinalStepBAL(oAPPBOK_BookingBE);

            //Sprint 26 - point 1  - Start
            if (lstImages != null && lstImages.Count > 0)
            {
                oAPPBOK_BookingBE.Action = "UpdateDeliveryUnloadedImages";
                oAPPBOK_BookingBE.PKID = ExtractInformation(GetQueryStringValue("ID").ToString(), "id");
                for (int i = 0; i < lstImages.Count; i++)
                {
                    oAPPBOK_BookingBE.DLYUNL_UploadedFileName = lstImages[i].ImagePath;
                    oAPPBOK_BookingBAL.UpdateDeliveryUnloadedImagesBAL(oAPPBOK_BookingBE);
                }
                Session.Remove("PathImage");
            }
            //Sprint 26 - point 1  - End


            // Sprint 1 - Point 8 - Start
            Session.Remove("TransactionComments");

            if (CheckBoxCheckStatus())
            {
                SendMail(IsImagesUploaded);
            }
            else
            {
                if ((IsVenodrPalletChecking || IsStandardPalletChecking || ISPM15CountryPalletChecking) || IsEnableHazardouesItemPrompt)
                {
                    oAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
                    oAPPBOK_BookingBE.Action = "GetBookingDeatilsByID";
                    oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));
                    List<APPBOK_BookingBE> lstBooking = oAPPBOK_BookingBAL.GetBookingDetailsByIDBAL(oAPPBOK_BookingBE);

                    if (lstBooking != null && lstBooking.Count > 0 && lstBooking[0].SupplierType == "V")
                    {
                        // Insert ISPM15
                        oAPPBOK_BookingBE = new APPBOK_BookingBE();
                        oAPPBOK_BookingBAL = new APPBOK_BookingBAL();
                        
                        oAPPBOK_BookingBE.VendorID = Convert.ToInt32(lstBooking[0].VendorID);
                        oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));
                        oAPPBOK_BookingBE.IsStandardPalletChecking = IsStandardPalletChecking;
                        oAPPBOK_BookingBE.IsVenodrPalletChecking = IsVenodrPalletChecking;
                        oAPPBOK_BookingBE.ISPM15CountryPalletChecking = ISPM15CountryPalletChecking;
                        oAPPBOK_BookingBE.IsEnableHazardouesItemPrompt = IsEnableHazardouesItemPrompt;

                        if (rdoDelieveryUnload1.Checked)
                        {
                            oAPPBOK_BookingBE.DeliveryChecked = true;
                            if (rdoDelieveryUnload2_1.Checked)
                            {
                                oAPPBOK_BookingBE.IssuesFound = true;
                                oAPPBOK_BookingBE.CheckedBy = txtChecked.Text;
                                oAPPBOK_BookingBE.ISPM15Comments = txtComments_1.Text;
                            }
                        }
                        else if (rdoDelieveryUnload2.Checked)
                        {
                            oAPPBOK_BookingBE.ISPM15Comments = txtCommentsOnNoDeliveryChecked.Text;
                            oAPPBOK_BookingBE.CheckedBy = Convert.ToString(Session["UserName"]);
                        }

                        if (rdoHDelieveryUnload1.Checked)
                        {
                            oAPPBOK_BookingBE.DeliveryChecked = true;
                            if (rdoHDelieveryUnload2_1.Checked)
                            {
                                oAPPBOK_BookingBE.IssuesFound = true;
                                oAPPBOK_BookingBE.CheckedBy = txtHChecked.Text;
                                oAPPBOK_BookingBE.HazardousCommments = txtHComments_1.Text;
                            }
                        }
                        else if (rdoHDelieveryUnload2.Checked)
                        {
                            oAPPBOK_BookingBE.HazardousCommments = txtHCommentsOnNoDeliveryChecked.Text;
                            oAPPBOK_BookingBE.CheckedBy = Convert.ToString(Session["UserName"]);
                        }

                        if (IsVenodrPalletChecking || IsStandardPalletChecking || ISPM15CountryPalletChecking)
                        {
                            oAPPBOK_BookingBE.Action = "InsertISPM15";
                            oAPPBOK_BookingBAL.InsertISPM15BAL(oAPPBOK_BookingBE);
                        }

                        if( IsEnableHazardouesItemPrompt)
                        {
                            oAPPBOK_BookingBE.Action = "InsertHazardousItem";
                            oAPPBOK_BookingBAL.InsertHazardousItemDetailsBAL(oAPPBOK_BookingBE);
                        }

                    }
                    else if (lstBooking != null && lstBooking.Count > 0 && lstBooking[0].SupplierType == "C")
                    {
                        CheckBoxList listItemCollection = IsEnableHazardouesItemPrompt ? chkCarrierHazardousVendor : chkCarrierVendor;

                        foreach (ListItem item in listItemCollection.Items)
                        {
                            if (item.Selected)
                            {
                                if (IsVenodrPalletChecking || IsStandardPalletChecking || ISPM15CountryPalletChecking
                                    || IsEnableHazardouesItemPrompt)
                                {
                                    oAPPBOK_BookingBE = new APPBOK_BookingBE();
                                    oAPPBOK_BookingBAL = new APPBOK_BookingBAL();

                                    oAPPBOK_BookingBE.VendorID = Convert.ToInt32(item.Value);
                                    oAPPBOK_BookingBE.BookingID = Convert.ToInt32(ExtractInformation(GetQueryStringValue("ID").ToString(), "id"));
                                    oAPPBOK_BookingBE.IsStandardPalletChecking = IsStandardPalletChecking;
                                    oAPPBOK_BookingBE.IsVenodrPalletChecking = IsVenodrPalletChecking;
                                    oAPPBOK_BookingBE.ISPM15CountryPalletChecking = ISPM15CountryPalletChecking;
                                    oAPPBOK_BookingBE.IsEnableHazardouesItemPrompt = IsEnableHazardouesItemPrompt;

                                    if (rdoDelieveryUnload1.Checked)
                                    {
                                        oAPPBOK_BookingBE.DeliveryChecked = true;
                                        if (rdoDelieveryUnload2_1.Checked)
                                        {
                                            oAPPBOK_BookingBE.IssuesFound = true;
                                            oAPPBOK_BookingBE.CheckedBy = txtChecked.Text;
                                            oAPPBOK_BookingBE.ISPM15Comments = txtComments_1.Text;
                                        }
                                    }
                                    else if (rdoDelieveryUnload2.Checked)
                                    {
                                        oAPPBOK_BookingBE.ISPM15Comments = txtCommentsOnNoDeliveryChecked.Text;
                                        oAPPBOK_BookingBE.CheckedBy = Convert.ToString(Session["UserName"]);
                                    }

                                    if (rdoHDelieveryUnload1.Checked)
                                    {
                                        oAPPBOK_BookingBE.DeliveryChecked = true;
                                        if (rdoDelieveryUnload2_1.Checked)
                                        {
                                            oAPPBOK_BookingBE.IssuesFound = true;
                                            oAPPBOK_BookingBE.CheckedBy = txtHChecked.Text;
                                            oAPPBOK_BookingBE.HazardousCommments = txtHComments_1.Text;
                                        }
                                    }
                                    else if (rdoHDelieveryUnload2.Checked)
                                    {
                                        oAPPBOK_BookingBE.HazardousCommments = txtHCommentsOnNoDeliveryChecked.Text;
                                        oAPPBOK_BookingBE.CheckedBy = Convert.ToString(Session["UserName"]);
                                    }

                                    if (IsVenodrPalletChecking || IsStandardPalletChecking || ISPM15CountryPalletChecking)
                                    {
                                        oAPPBOK_BookingBE.Action = "InsertISPM15";
                                        oAPPBOK_BookingBAL.InsertISPM15BAL(oAPPBOK_BookingBE);
                                    }

                                    if (IsEnableHazardouesItemPrompt)
                                    {
                                        oAPPBOK_BookingBE.Action = "InsertHazardousItem";
                                        oAPPBOK_BookingBAL.InsertHazardousItemDetailsBAL(oAPPBOK_BookingBE);
                                    } 
                                }
                            }
                        }
                    }

                    if (rdoDelieveryUnload2_1.Checked)
                    {
                        SendISPMIssueMail();
                    }
                }

                EncryptQueryString("~/ModuleUI/Appointment/Booking/APPBok_BookingOverview.aspx");
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnOk3_Click(object sender, EventArgs e)
    {
        if (IsEnableHazardouesItemPrompt)
        {
            pnlHazardousItemChecking.Visible = true;
            pnlVendorPalletChecking.Visible = false;
            btnOK.Visible = true;
            btnCancel.Visible = true;
            divBookingButton3.Visible = false;
            divBookingButton.Visible = false;

            divBookingDiv.Visible = false;
            divBookingButton.Visible = false;

            lblHazardousItemCheckingDelivery.Visible = IsEnableHazardouesItemPrompt;
            lblHazardousItemCheckingDeliveryComments.Visible = IsEnableHazardouesItemPrompt;
        }
        else
        {
            btnOK_Click(sender, e);
        }
    }

    protected void btnCancel3_Click(object sender, EventArgs e)
    {
        /*Session.Remove("PathImage");
        RevertChanges();
        updInfromation_Messge_update.Hide();
        */
        Session.Remove("PathImage");
        RevertChanges();
        EncryptQueryString("APPRcv_DeliveryUnloaded.aspx?Scheduledate=" + GetQueryStringValue("Scheduledate").ToString() + "&ID=" + GetQueryStringValue("ID").ToString());
        
    }
}


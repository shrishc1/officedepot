﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPRcv_DeliveryUnloadedRejectAll.aspx.cs" Inherits="APPRcv_DeliveryUnloadedRejectAll" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<script type="text/javascript" src="../../../Scripts/1.12.4.jquery.min.js"></script>--%>
    <script language="javascript" type="text/javascript">       
        function onupload() {
            $(function () {
                var fileUpload = $('#<%=fileImage.ClientID%>').get(0);
                document.getElementById("<%=lbl_smsg.ClientID%>").innerText = '';
                document.getElementById("<%=lbl_emsg.ClientID%>").innerText = '';
                var files = fileUpload.files;
                var test = new FormData();
                for (var i = 0; i < files.length; i++) {
                    test.append(files[i].name, files[i]);
                }
                $.ajax({
                    url: "UploadFile.ashx",
                    type: "POST",
                    contentType: false,
                    processData: false,
                    data: test,
                    success: function (result) {
                        if (result.split(':')[0] == "File Uploaded Successfully") {
                            document.getElementById("<%=btnAdd.ClientID %>").click();
                            document.getElementById("<%=lbl_smsg.ClientID%>").innerText = result.split(':')[0];
                        } else {
                            document.getElementById("<%=lbl_emsg.ClientID%>").innerText = result;
                        }
                    },
                    error: function (err) {
                        alert(err.statusText);
                    }
                });
            })
        }

        function ConfirmMess() {
            var mess = window.confirm('<%=ConfirmMessage %>');
            if (mess == true) {
            }
            else {
                return false;
            }
        }

        function previewImage(arg) {
            var path = "../../Images/DeliveryUnload/" + arg;
            var myArguments = new Object();
            var answer = window.showModalDialog(path, myArguments, "dialogWidth:700px; dialogHeight:600px; center:yes");
            return false;
        }
        function checkvalidation() {
            if (!Page_ClientValidate('a')) {
                return false;
            }
            var RefusalComments = document.getElementById('<%=txtRefusalComments.ClientID %>').value;
            var IsDelUnloadPassAllow = document.getElementById('<%= hdnIsDelUnloadPassAllow.ClientID %>').value;
            var SecondClickStatus = document.getElementById('<%= hdnSecondClickStatus.ClientID %>');

            if (document.getElementById('<%=hdnRefusalFlg.ClientID %>').value == 'PAR' || document.getElementById('<%=hdnRefusalFlg.ClientID %>').value == 'REJ') {
                if (parseInt(document.getElementById('<%=hdnisIssueChecked.ClientID %>').value) == 0 || RefusalComments.length <= 0) {
                    alert("<%=AtLeastOnePoingFailure %>");
                    return false;
                }
                var divstyle = new String();
                divstyle = document.getElementById("divApproveBy").style.visibility;

                if (divstyle.toLowerCase() == "hidden") {
                    if (IsDelUnloadPassAllow == 'N') {
                        document.getElementById("divApproveBy").style.visibility = "hidden";
                        document.getElementById("divPassLabel").style.visibility = "hidden";
                        document.getElementById("divPassCollon").style.visibility = "hidden";
                        document.getElementById("divPassTextBox").style.visibility = "hidden";
                        SecondClickStatus.value = 'FIRSTCLICK';
                        return true;
                    }
                    else {
                        document.getElementById("divApproveBy").style.visibility = "visible";
                        document.getElementById("divPassLabel").style.visibility = "visible";
                        document.getElementById("divPassCollon").style.visibility = "visible";
                        document.getElementById("divPassTextBox").style.visibility = "visible";

                    }
                }
                else {
                    document.getElementById("divApproveBy").style.visibility = "visible";
                    var objddlUser = document.getElementById('<%=ddlUser.ClientID %>');

                    if (document.getElementById("divApproveBy").style.visibility == "visible") {
                        if (IsDelUnloadPassAllow == 'N') {
                            document.getElementById("divPassLabel").style.visibility = "hidden";
                            document.getElementById("divPassCollon").style.visibility = "hidden";
                            document.getElementById("divPassTextBox").style.visibility = "hidden";
                            SecondClickStatus.value = 'FIRSTCLICK';
                            return true;
                        }
                        else {
                            document.getElementById("divPassLabel").style.visibility = "visible";
                            document.getElementById("divPassCollon").style.visibility = "visible";
                            document.getElementById("divPassTextBox").style.visibility = "visible";

                            var SupervisorPassword = document.getElementById('<%=txtSupervisorPassword.ClientID %>').value;
                            if (SupervisorPassword.length <= 0) {
                                alert("<%=SupervisorPasswordValidation%>");
                                return false;
                            }
                        }
                    }
                }
            }

            if (document.getElementById('<%=hdnRefusalFlg.ClientID %>').value != 'PAR') {
                if (parseInt(document.getElementById('<%=hdnisIssueChecked.ClientID %>').value) > 0) {
                    if (RefusalComments.length <= 0) {
                        alert("<%=RefusalCommentsValidation%>");
                        return false;
                    }
                }
            }

            if (CheckPalletExchangeValidation() == 1) {

                if (RefusalComments.length > 0 && document.getElementById('<%=hdnRefusalFlg.ClientID %>').value != 'FULL') {

                    var divstyle = new String();
                    divstyle = document.getElementById("divApproveBy").style.visibility;

                    if (divstyle.toLowerCase() == "hidden") {
                        if (IsDelUnloadPassAllow == 'N') {
                            document.getElementById("divApproveBy").style.visibility = "visible";
                            document.getElementById("divPassLabel").style.visibility = "hidden";
                            document.getElementById("divPassCollon").style.visibility = "hidden";
                            document.getElementById("divPassTextBox").style.visibility = "hidden";
                            SecondClickStatus.value = 'FIRSTCLICK';
                            return true;
                        }
                        else {
                            document.getElementById("divApproveBy").style.visibility = "visible";
                            document.getElementById("divPassLabel").style.visibility = "visible";
                            document.getElementById("divPassCollon").style.visibility = "visible";
                            document.getElementById("divPassTextBox").style.visibility = "visible";
                        }
                    }
                    else {
                        document.getElementById("divApproveBy").style.visibility = "visible";
                        var objddlUser = document.getElementById('<%=ddlUser.ClientID %>');

                        if (document.getElementById("divApproveBy").style.visibility == "visible") {

                            if (IsDelUnloadPassAllow == 'N') {
                                document.getElementById("divPassLabel").style.visibility = "hidden";
                                document.getElementById("divPassCollon").style.visibility = "hidden";
                                document.getElementById("divPassTextBox").style.visibility = "hidden";
                                SecondClickStatus.value = 'FIRSTCLICK';
                                return true;
                            }
                            else {
                                document.getElementById("divPassLabel").style.visibility = "visible";
                                document.getElementById("divPassCollon").style.visibility = "visible";
                                document.getElementById("divPassTextBox").style.visibility = "visible";

                                var SupervisorPassword = document.getElementById('<%=txtSupervisorPassword.ClientID %>').value;
                                if (SupervisorPassword.length <= 0) {
                                    alert("<%=SupervisorPasswordValidation%>");
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            else {
                return false;
            }
        }

        function CheckPalletExchangeValidation() {
            var EuroDelivered = document.getElementById('<%=txtEuroDelivered.ClientID %>').value;
            var EuroReturned = document.getElementById('<%=txtEuroReturned.ClientID %>').value;
            var EuroComments = document.getElementById('<%=txtEuroComments.ClientID %>').value;

            var UKDelivered = document.getElementById('<%=txtUKDeliverd.ClientID %>').value;
            var UKReturned = document.getElementById('<%=txtUKRetured.ClientID %>').value;
            var UKComments = document.getElementById('<%=txtUKComments.ClientID %>').value;

            var CHEPDelivered = document.getElementById('<%=txtCHEPDelivered.ClientID %>').value;
            var CHEPReturned = document.getElementById('<%=txtCHEPReturned.ClientID %>').value;
            var CHEPComments = document.getElementById('<%=txtCHEPComments.ClientID %>').value;

            var OthersDelivered = document.getElementById('<%=txtOthersDelivered.ClientID %>').value;
            var OthersReturned = document.getElementById('<%=txtOthersReturned.ClientID %>').value;
            var OthersComments = document.getElementById('<%=txtOtherCommentsPallets.ClientID %>').value;

            if (EuroDelivered != "" && EuroReturned == "") {

                alert("<%=EuroPalletesReturnedValidation%>");
                return 0;
            }

            if (EuroDelivered == "" && EuroReturned != "") {

                alert("<%=EuroPalletesDeliveredValidation%>");
                return 0;
            }

            if (EuroDelivered != null && EuroReturned != null) {

                if (EuroReturned != EuroDelivered) {
                    if (EuroComments.length <= 0) {
                        alert("<%=EuroPalletesCommentsValidation%>");
                        return 0;
                    }
                }
            }

            if (UKDelivered != '' && UKReturned == '') {

                alert("<%=UKStandardReturnedValidation%>");
                return 0;
            }

            if (UKDelivered == '' && UKReturned != '') {

                alert("<%=UKStandardDeliveredValidation%>");
                return 0;
            }

            if (UKDelivered != null && UKReturned != null) {

                if (UKReturned != UKDelivered) {
                    if (UKComments.length <= 0) {
                        alert("<%=UKStandardCommentsValidation%>");
                        return 0;
                    }
                }
            }

            if (CHEPDelivered != '' && CHEPReturned == '') {

                alert("<%=CHEPReturnedValidation%>");
                return 0;
            }
            if (CHEPDelivered == '' && CHEPReturned != '') {

                alert("<%=CHEPDeliveredValidation%>");
                return 0;
            }


            if (CHEPDelivered != null && CHEPReturned != null) {

                if (CHEPReturned != CHEPDelivered) {
                    if (CHEPComments.length <= 0) {
                        alert("<%=CHEPCommentsValidation%>");
                        return 0;
                    }
                }
            }

            if (OthersDelivered != '' && OthersReturned == '') {

                alert("<%=OthersReturnedValidation%>");
                return 0;
            }

            if (OthersDelivered == '' && OthersReturned != '') {

                alert("<%=OthersDeliveredValidation%>");
                return 0;
            }

            if (OthersDelivered != null && OthersReturned != null) {

                if (OthersReturned != OthersDelivered) {
                    if (OthersComments.length <= 0) {
                        alert("<%=OthersCommentsValidation%>");
                        return 0;
                    }
                }
            }

            return 1;
        }

        function GetReturnedValues(obj, arg) {
            if (obj.value != '') {

                if (arg == '1') {
                    document.getElementById('<%=txtEuroReturned.ClientID %>').value = document.getElementById('<%=txtEuroDelivered.ClientID %>').value;
                    document.getElementById('<%=lblTotal.ClientID %>').innerText = document.getElementById('<%=txtEuroDelivered.ClientID %>').value;

                }
                if (arg == '2') {
                    document.getElementById('<%=txtUKRetured.ClientID %>').value = document.getElementById('<%=txtUKDeliverd.ClientID %>').value;
                    document.getElementById('<%=lblTotal.ClientID %>').innerText = parseInt(document.getElementById('<%=lblTotal.ClientID %>').innerText) + parseInt(document.getElementById('<%=txtUKDeliverd.ClientID %>').value);

                }
                if (arg == '3') {
                    document.getElementById('<%=txtCHEPReturned.ClientID %>').value = document.getElementById('<%=txtCHEPDelivered.ClientID %>').value;
                    document.getElementById('<%=lblTotal.ClientID %>').innerText = parseInt(document.getElementById('<%=lblTotal.ClientID %>').innerText) + parseInt(document.getElementById('<%=txtCHEPDelivered.ClientID %>').value);

                }
                else if (arg == '4') {
                    document.getElementById('<%=txtOthersReturned.ClientID %>').value = document.getElementById('<%=txtOthersDelivered.ClientID %>').value;
                    document.getElementById('<%=lblTotal.ClientID %>').innerText = parseInt(document.getElementById('<%=lblTotal.ClientID %>').innerText) + parseInt(document.getElementById('<%=txtOthersDelivered.ClientID %>').value);

                }
            }

        }

        function CheckCheckboxes(chkbox) {
            if (chkbox.checked)
                document.getElementById('<%=hdnisIssueChecked.ClientID %>').value = parseInt(document.getElementById('<%=hdnisIssueChecked.ClientID %>').value) + 1;
            else
                document.getElementById('<%=hdnisIssueChecked.ClientID %>').value = parseInt(document.getElementById('<%=hdnisIssueChecked.ClientID %>').value) - 1;
        }
        document.onmousemove = positiontip;

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
        function EndRequest(sender, args) {
            if (args.get_error() == undefined) {

                if ($('#<%= rdoDelieveryUnload1.ClientID %>').attr('checked') == true) {
                    $('#<%= pnlDelieveryUnload2.ClientID %>').show();
                    $('#<%= pnlDelieveryUnload3.ClientID %>').hide();
                    $('#<%= tblComments.ClientID %>').hide();
                    $('#<%= tblCommentsOnNoDeliveryChecked.ClientID %>').hide();
                    $('#<%= txtComments_1.ClientID %>').val("");
                    $('#<%= txtChecked.ClientID %>').val("");
                    $('#<%= txtCommentsOnNoDeliveryChecked.ClientID %>').val("");
                }
                if ($('#<%= rdoDelieveryUnload2.ClientID %>').attr('checked') == true) {
                    $('#<%= pnlDelieveryUnload2.ClientID %>').hide();
                    $('#<%= pnlDelieveryUnload3.ClientID %>').hide();
                    $('#<%= tblComments.ClientID %>').hide();
                    $('#<%= tblCommentsOnNoDeliveryChecked.ClientID %>').show();
                    $('#<%= txtComments_1.ClientID %>').val("");
                    $('#<%= txtChecked.ClientID %>').val("");
                    $('#<%= txtCommentsOnNoDeliveryChecked.ClientID %>').val("");

                }
                if ($('#<%= rdoDelieveryUnload2_1.ClientID %>').attr('checked') == true) {
                    $('#<%= pnlDelieveryUnload3.ClientID %>').show();
                    $('#<%= tblComments.ClientID %>').show();
                    $('#<%= tblCommentsOnNoDeliveryChecked.ClientID %>').hide();
                    $('#<%= txtComments_1.ClientID %>').val("");
                    $('#<%= txtChecked.ClientID %>').val("");
                    $('#<%= txtCommentsOnNoDeliveryChecked.ClientID %>').val("");
                }

                if ($('#<%= rdoDelieveryUnload2_2.ClientID %>').attr('checked') == true) {
                    $('#<%= pnlDelieveryUnload3.ClientID %>').hide();
                    $('#<%= tblComments.ClientID %>').hide();
                    $('#<%= tblCommentsOnNoDeliveryChecked.ClientID %>').hide();
                    $('#<%= txtComments_1.ClientID %>').val("");
                    $('#<%= txtChecked.ClientID %>').val("");
                    $('#<%= txtCommentsOnNoDeliveryChecked.ClientID %>').val("");
                }

                $('#<%= rdoDelieveryUnload1.ClientID %>').click(function () {

                    if ($('#<%= chkCarrierVendor.ClientID %> :checkbox:checked').length == 0) {
                        alert('<%= DelieveryUnloadErrorMessage3 %>');
                        return false;
                    }

                    $('#<%= pnlDelieveryUnload2.ClientID %>').show();
                    $('#<%= pnlDelieveryUnload3.ClientID %>').hide();
                    $('#<%= tblComments.ClientID %>').hide();
                    $('#<%= rdoDelieveryUnload2_1.ClientID %>').removeAttr('checked');
                    $('#<%= rdoDelieveryUnload2_2.ClientID %>').removeAttr('checked');
                    $('#<%= tblCommentsOnNoDeliveryChecked.ClientID %>').hide();
                    $('#<%= txtComments_1.ClientID %>').val("");
                    $('#<%= txtChecked.ClientID %>').val("");
                    $('#<%= txtCommentsOnNoDeliveryChecked.ClientID %>').val("");

                });

                $('#<%= rdoDelieveryUnload2.ClientID %>').click(function () {
                    if ($('#<%= chkCarrierVendor.ClientID %> :checkbox:checked').length == 0) {
                        alert('<%= DelieveryUnloadErrorMessage3 %>');
                        return false;
                    }
                    $('#<%= pnlDelieveryUnload2.ClientID %>').hide();
                    $('#<%= pnlDelieveryUnload3.ClientID %>').hide();
                    $('#<%= tblComments.ClientID %>').hide();
                    $('#<%= rdoDelieveryUnload2_1.ClientID %>').removeAttr('checked');
                    $('#<%= rdoDelieveryUnload2_2.ClientID %>').removeAttr('checked');
                    $('#<%= tblCommentsOnNoDeliveryChecked.ClientID %>').show();
                    $('#<%= txtComments_1.ClientID %>').val("");
                    $('#<%= txtChecked.ClientID %>').val("");
                    $('#<%= txtCommentsOnNoDeliveryChecked.ClientID %>').val("");
                });

                $('#<%= rdoDelieveryUnload2_1.ClientID %>').click(function () {
                    $('#<%= pnlDelieveryUnload3.ClientID %>').show();
                    $('#<%= tblComments.ClientID %>').show();
                    $('#<%= tblCommentsOnNoDeliveryChecked.ClientID %>').hide();
                    $('#<%= txtComments_1.ClientID %>').val("");
                    $('#<%= txtChecked.ClientID %>').val("");
                    $('#<%= txtCommentsOnNoDeliveryChecked.ClientID %>').val("");
                });
                $('#<%= rdoDelieveryUnload2_2.ClientID %>').click(function () {

                    $('#<%= pnlDelieveryUnload3.ClientID %>').hide();
                    $('#<%= tblComments.ClientID %>').hide();
                    $('#<%= tblCommentsOnNoDeliveryChecked.ClientID %>').hide();
                    $('#<%= txtComments_1.ClientID %>').val("");
                    $('#<%= txtChecked.ClientID %>').val("");
                    $('#<%= txtCommentsOnNoDeliveryChecked.ClientID %>').val("");
                });

                $('#<%= btnOK.ClientID %>').click(function () {

                    if ($('#<%= rdoDelieveryUnload1.ClientID %>').is(":visible")) {

                        if (!$('#<%= rdoDelieveryUnload1.ClientID %>').is(":checked") && !$('#<%= rdoDelieveryUnload2.ClientID %>').is(":checked")) {
                            alert('<%= DelieveryUnloadErrorMessage1%>');
                            return false;
                        }
                        else if (!$('#<%= rdoDelieveryUnload1.ClientID %>').is(":checked")
                            && $('#<%= rdoDelieveryUnload2.ClientID %>').is(":checked")
                            && $('#<%= txtCommentsOnNoDeliveryChecked.ClientID %>').val() == '') {
                            alert('- ' + '<%= CommentsOnNoDeliveryCheckedAlertMsg%>');
                            return false;
                        }
                        else if (($('#<%= rdoDelieveryUnload1.ClientID %>').is(":checked")
                            && !$('#<%= rdoDelieveryUnload2.ClientID %>').is(":checked")) &&
                            (!$('#<%= rdoDelieveryUnload2_1.ClientID %>').is(":checked")
                                && !$('#<%= rdoDelieveryUnload2_2.ClientID %>').is(":checked"))
                        ) {
                            alert('<%= DelieveryUnloadErrorMessage2%>');
                            return false;
                        }
                        else if (($('#<%= rdoDelieveryUnload1.ClientID %>').is(":checked")
                            && !$('#<%= rdoDelieveryUnload2.ClientID %>').is(":checked")) &&
                            ($('#<%= rdoDelieveryUnload2_1.ClientID %>').is(":checked")
                                || !$('#<%= rdoDelieveryUnload2_2.ClientID %>').is(":checked"))
                            && $('#<%= txtChecked.ClientID %>').val() == ''
                            && $('#<%= txtComments_1.ClientID %>').val() == '') {
                            alert('<%= ISPM15Alert1%> \n <%= ISPM15Alert2%> ');
                            return false;
                        }
                        else if (($('#<%= rdoDelieveryUnload1.ClientID %>').is(":checked")
                            && !$('#<%= rdoDelieveryUnload2.ClientID %>').is(":checked")) &&
                            ($('#<%= rdoDelieveryUnload2_1.ClientID %>').is(":checked")
                                && !$('#<%= rdoDelieveryUnload2_2.ClientID %>').is(":checked"))
                            && $('#<%= txtChecked.ClientID %>').val() !== ''
                            && $('#<%= txtComments_1.ClientID %>').val() == '') {
                            alert('<%= ISPM15Alert1%>');
                            return false;
                        }
                        else if (($('#<%= rdoDelieveryUnload1.ClientID %>').is(":checked")
                            && !$('#<%= rdoDelieveryUnload2.ClientID %>').is(":checked")) &&
                            ($('#<%= rdoDelieveryUnload2_1.ClientID %>').is(":checked")
                                && !$('#<%= rdoDelieveryUnload2_2.ClientID %>').is(":checked"))
                            && $('#<%= txtChecked.ClientID %>').val() == ''
                            && $('#<%= txtComments_1.ClientID %>').val() !== '') {
                            alert('<%= ISPM15Alert2%>');
                            return false;
                        }
                    }
                });

                $('#<%= rdoHDelieveryUnload1.ClientID %>').click(function () {
                    if ($('#<%= chkCarrierHazardousVendor.ClientID %> :checkbox:checked').length == 0) {
                        alert('<%= DelieveryUnloadErrorMessage3 %>');
                        return false;
                    }
                    $('#<%= pnlDelieveryHUnload2.ClientID %>').show();
                    $('#<%= pnlDelieveryHUnload3.ClientID %>').hide();
                    $('#<%= tblHComments.ClientID %>').hide();
                    $('#<%= rdoHDelieveryUnload2_1.ClientID %>').removeAttr('checked');
                    $('#<%= rdoHDelieveryUnload2_2.ClientID %>').removeAttr('checked');
                    $('#<%= tblHCommentsOnNoDeliveryChecked.ClientID %>').hide();
                    $('#<%= txtHComments_1.ClientID %>').val("");
                    $('#<%= txtHChecked.ClientID %>').val("");
                    $('#<%= txtHCommentsOnNoDeliveryChecked.ClientID %>').val("");
                });

                $('#<%= rdoHDelieveryUnload2.ClientID %>').click(function () {
                    if ($('#<%= chkCarrierHazardousVendor.ClientID %> :checkbox:checked').length == 0) {
                        alert('<%= DelieveryUnloadErrorMessage3 %>');
                        return false;
                    }
                    $('#<%= pnlDelieveryHUnload2.ClientID %>').hide();
                    $('#<%= pnlDelieveryHUnload3.ClientID %>').hide();
                    $('#<%= tblHComments.ClientID %>').hide();
                    $('#<%= rdoHDelieveryUnload2_1.ClientID %>').removeAttr('checked');
                    $('#<%= rdoHDelieveryUnload2_2.ClientID %>').removeAttr('checked');
                    $('#<%= tblHCommentsOnNoDeliveryChecked.ClientID %>').show();
                    $('#<%= txtHComments_1.ClientID %>').val("");
                    $('#<%= txtHChecked.ClientID %>').val("");
                    $('#<%= txtHCommentsOnNoDeliveryChecked.ClientID %>').val("");
                });

                $('#<%= rdoHDelieveryUnload2_1.ClientID %>').click(function () {
                    $('#<%= pnlDelieveryHUnload3.ClientID %>').show();
                    $('#<%= tblHComments.ClientID %>').show();
                    $('#<%= tblHCommentsOnNoDeliveryChecked.ClientID %>').hide();
                    $('#<%= txtHComments_1.ClientID %>').val("");
                    $('#<%= txtHChecked.ClientID %>').val("");
                    $('#<%= txtHCommentsOnNoDeliveryChecked.ClientID %>').val("");
                });

                $('#<%= rdoHDelieveryUnload2_2.ClientID %>').click(function () {

                    $('#<%= pnlDelieveryHUnload3.ClientID %>').hide();
                    $('#<%= tblHComments.ClientID %>').hide();
                    $('#<%= tblHCommentsOnNoDeliveryChecked.ClientID %>').hide();
                    $('#<%= txtHComments_1.ClientID %>').val("");
                    $('#<%= txtHChecked.ClientID %>').val("");
                    $('#<%= txtHCommentsOnNoDeliveryChecked.ClientID %>').val("");
                });
            }
        }


    </script>


    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <!-- For Issues Detail -->
    <div id="divVolume" runat="server" class="balloonstyle">
    </div>
    <div id="dhtmltooltip" class="balloonstyle">
    </div>
    <iframe id="iframetop" scrolling="no" frameborder="0" class="iballoonstyle" width="0"
        height="0"></iframe>
    <div class="balloonstyle" id="ArrivedLate">
        <span class="bla8blue">
            <cc1:ucLabel ID="lblIssuesDesc" runat="server"></cc1:ucLabel>
        </span>
    </div>


    <!-- For Issues Detail -->
    <h2>
        <cc1:ucLabel ID="IssueDetail" runat="server" Text="Issue Details "></cc1:ucLabel>
    </h2>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="right-shadow">
                <asp:HiddenField ID="hdnRefusalFlg" runat="server" />
                <asp:HiddenField ID="hdnisIssueChecked" runat="server" Value="0" />
                <div class="formbox" id="divBookingDiv" runat="server">
                    <div>
                        <cc1:ucPanel ID="pnlBookingDetailsArrival" GroupingText="Booking Details" runat="server"
                            CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <tr id="trVendorCarrier" runat="server">
                                    <td style="width: 10%;">
                                        <cc1:ucLabel ID="lblVendorBooking" runat="server" Text="Vendor" />
                                    </td>
                                    <td style="width: 1%;">:
                                    </td>
                                    <td style="width: 20%;" class="nobold">
                                        <cc1:ucLabel ID="VendorNameData" runat="server" />
                                    </td>
                                    <td style="width: 4%;">
                                        <cc1:ucLabel ID="lblCarrierBooking" runat="server" Text="Carrier" />
                                    </td>
                                    <td style="width: 1%;">:
                                    </td>
                                    <td style="width: 20%;" class="nobold" colspan="6">
                                        <cc1:ucLabel ID="CarrierNameData" runat="server" Text="ANC" />
                                    </td>
                                </tr>
                                <tr id="trCarrier" runat="server">
                                    <td style="width: 10%;">
                                        <cc1:ucLabel ID="lblCarrierBooking1" runat="server" Text="Carrier" />
                                    </td>
                                    <td style="width: 1%;">:
                                    </td>
                                    <td style="width: 20%;" class="nobold" colspan="9">
                                        <cc1:ucLabel ID="CarrierNameData1" runat="server" Text="ANC" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;">
                                        <cc1:ucLabel ID="lblDateBooking" runat="server" Text="Date" />
                                    </td>
                                    <td style="width: 1%;">:
                                    </td>
                                    <td style="width: 20%;" class="nobold">
                                        <cc1:ucLabel ID="BookingDateData" runat="server" Text="" />
                                        &nbsp;
                            <cc1:ucLabel ID="BookingTimeData" runat="server" Text="" />
                                    </td>
                                    <td style="width: 10%;">
                                        <cc1:ucLabel ID="lblVehicleType" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 1%;">:
                                    </td>
                                    <td style="width: 14%;" class="nobold">
                                        <cc1:ucLabel ID="VehicleTypeData" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 10%;">
                                        <cc1:ucLabel ID="lblDoorName" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 1%;">:
                                    </td>
                                    <td style="width: 14%;" class="nobold">
                                        <cc1:ucLabel ID="DoorNumberData" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 4%;"></td>
                                    <td style="width: 1%;"></td>
                                    <td style="width: 20%;" class="nobold"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblPalletsBooking" runat="server" Text="Pallets" />
                                    </td>
                                    <td>:
                                    </td>
                                    <td style="width: 100px;" class="nobold">
                                        <cc1:ucLabel ID="BookingPalletsData" runat="server" Text="20" />
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblCartons" runat="server" Text="Cartons" />
                                    </td>
                                    <td>:
                                    </td>
                                    <td style="width: 100px;" class="nobold">
                                        <cc1:ucLabel ID="BookingCartonsData" runat="server" Text="0" />
                                    </td>
                                    <td style="width: 100px;">
                                        <cc1:ucLabel ID="lblLines" runat="server" Text="Lines" />
                                    </td>
                                    <td>:
                                    </td>
                                    <td style="width: 100px;" class="nobold">
                                        <cc1:ucLabel ID="BookingLinesData" runat="server" Text="16" />
                                    </td>
                                    <td>
                                        <cc1:ucLabel ID="lblLiftsBooking" runat="server" Text="Lifts" />
                                    </td>
                                    <td>:
                                    </td>
                                    <td style="width: 100px;" class="nobold">
                                        <cc1:ucLabel ID="BookingLiftsData" runat="server" Text="31" />
                                    </td>
                                </tr>

                            </table>
                        </cc1:ucPanel>
                        <cc1:ucPanel ID="pnlIssues" runat="server" CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td colspan="4" align="right">
                                        <a href="javascript:void(0)" rel="ArrivedLate"  
                                            onmouseover="ddrivetip('ArrivedLate','#ededed','300');"
                                            onmouseout="hideddrivetip();">
                                            <img src="../../../Images/info_button1.gif" align="middle" border="0" alt=""></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <cc1:ucCheckbox ID="chkLate" runat="server" onClick="CheckCheckboxes(this);" />
                                    </td>
                                    <td style="width: 25%;">
                                        <cc1:ucCheckbox ID="chkEarly" runat="server" onClick="CheckCheckboxes(this);" />
                                    </td>
                                    <td style="width: 25%;">
                                        <cc1:ucCheckbox ID="chkNoPaperworkondisplay" runat="server" onClick="CheckCheckboxes(this);" />
                                    </td>
                                    <td style="width: 25%;">
                                        <cc1:ucCheckbox ID="chkPalletsDamaged" runat="server" onClick="CheckCheckboxes(this);" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <cc1:ucCheckbox ID="chkPackagingDamaged" runat="server" onClick="CheckCheckboxes(this);" />
                                    </td>
                                    <td style="width: 25%;">
                                        <cc1:ucCheckbox ID="chkunsafeload" runat="server" onClick="CheckCheckboxes(this);" />
                                    </td>
                                    <td style="width: 25%;">
                                        <cc1:ucCheckbox ID="chkWrongAddress" runat="server" onClick="CheckCheckboxes(this);" />
                                    </td>
                                    <td style="width: 25%;">
                                        <cc1:ucCheckbox ID="chkRefusedtowait" runat="server" onClick="CheckCheckboxes(this);" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <cc1:ucCheckbox ID="chkNotToOdSpecification" runat="server" onClick="CheckCheckboxes(this);" />
                                    </td>
                                    <td style="width: 25%;">
                                        <cc1:ucCheckbox ID="chkOther" runat="server" onClick="CheckCheckboxes(this);" />
                                    </td>
                                    <td style="width: 50%;" colspan="2">
                                        <cc1:ucCheckbox ID="chkFailedToDeclareHazardousGoods" runat="server" onClick="CheckCheckboxes(this);" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <cc1:ucLabel ID="RefusalComment" runat="server" Text="Comments : " />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;" colspan="4">
                                        <cc1:ucTextbox ID="txtRefusalComments" runat="server" TextMode="MultiLine" Width="98%"></cc1:ucTextbox>
                                        <asp:RequiredFieldValidator ID="rfvRefusalCommentsValidation" runat="server" Display="None"
                                            ControlToValidate="txtRefusalComments" ErrorMessage="Please Enter refusal Comments."
                                            ValidationGroup="a" />
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                        <cc1:ucPanel ID="pnlPalletExchange" runat="server" GroupingText="Pallet Exchange"
                            CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                                <tr id="trTotalPalletsDelivered" runat="server">
                                    <td style="font-weight: bold; width: 14%">
                                        <cc1:ucLabel ID="lblTotalPalletsDelivered" runat="server" Text="Total Pallets Delivered"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">:
                                    </td>
                                    <td style="width: 7%" class="nobold">
                                        <cc1:ucLabel ID="lblTotalDelivered" runat="server" Width="45px" Text="0"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 7%"></td>
                                    <td style="font-weight: bold; width: 71%"></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 14%">
                                        <cc1:ucLabel ID="lblTotalPalletsCount" runat="server" Text="Total Pallets Count"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">:
                                    </td>
                                    <td style="width: 7%" class="nobold">
                                        <cc1:ucLabel ID="lblTotal" runat="server" Width="45px" Text="0"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 7%"></td>
                                    <td style="font-weight: bold; width: 71%"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblDelivered" runat="server" Text="Delivered"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblReturned" runat="server" Text="Returned"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblComments" runat="server" Text="Comments"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblEuroPallets" runat="server" Text="Euro Pallets"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">:
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucTextbox ID="txtEuroDelivered" runat="server" Width="45px" onkeyup="AllowNumbersOnly(this);"
                                            onblur="javascript:return GetReturnedValues(this,'1');" MaxLength="3"></cc1:ucTextbox>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucTextbox ID="txtEuroReturned" runat="server" Width="45px" onkeyup="AllowNumbersOnly(this);"
                                            MaxLength="3"></cc1:ucTextbox>
                                    </td>
                                    <td style="font-weight: bold; width: 55%">
                                        <cc1:ucTextbox ID="txtEuroComments" runat="server" Width="97%"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblUKStandard" runat="server" Text="UK Standard"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">:
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucTextbox ID="txtUKDeliverd" runat="server" Width="45px" onkeyup="AllowNumbersOnly(this);"
                                            onblur="javascript:return GetReturnedValues(this,'2');" MaxLength="3"></cc1:ucTextbox>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucTextbox ID="txtUKRetured" runat="server" Width="45px" onkeyup="AllowNumbersOnly(this);"
                                            MaxLength="3"></cc1:ucTextbox>
                                    </td>
                                    <td style="font-weight: bold; width: 55%">
                                        <cc1:ucTextbox ID="txtUKComments" runat="server" Width="97%"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblCHEP" runat="server" Text="CHEP"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">:
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucTextbox ID="txtCHEPDelivered" runat="server" Width="45px" onkeyup="AllowNumbersOnly(this);"
                                            onblur="javascript:return GetReturnedValues(this,'3');" MaxLength="3"></cc1:ucTextbox>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucTextbox ID="txtCHEPReturned" runat="server" Width="45px" onkeyup="AllowNumbersOnly(this);"
                                            MaxLength="3"></cc1:ucTextbox>
                                    </td>
                                    <td style="font-weight: bold; width: 55%">
                                        <cc1:ucTextbox ID="txtCHEPComments" runat="server" Width="97%"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblOthers" runat="server" Text="Others"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">:
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucTextbox ID="txtOthersDelivered" runat="server" Width="45px" onkeyup="AllowNumbersOnly(this);"
                                            onblur="javascript:return GetReturnedValues(this,'4');" MaxLength="3"></cc1:ucTextbox>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucTextbox ID="txtOthersReturned" runat="server" Width="45px" onkeyup="AllowNumbersOnly(this);"
                                            MaxLength="3"></cc1:ucTextbox>
                                    </td>
                                    <td style="font-weight: bold; width: 55%">
                                        <cc1:ucTextbox ID="txtOtherCommentsPallets" runat="server" Width="97%"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                        <div id="divApproveBy" style="visibility: hidden;" runat="server" clientidmode="Static">
                            <cc1:ucPanel ID="pnlApproveBy" runat="server" GroupingText="Approve by" CssClass="fieldset-form">
                                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                    <tr>
                                        <td style="width: 11%;">
                                            <cc1:ucLabel ID="lblSupervisorName" runat="server" Text="Supervisor Name"></cc1:ucLabel>
                                            <asp:HiddenField ID="hdnIsDelUnloadPassAllow" runat="server" />
                                            <asp:HiddenField ID="hdnSecondClickStatus" runat="server" />
                                        </td>
                                        <td style="width: 1%;">:
                                        </td>
                                        <td style="width: 38%;">
                                            <cc1:ucDropdownList ID="ddlUser" runat="server" Width="150px">
                                            </cc1:ucDropdownList>
                                        </td>
                                        <td style="width: 14%;">
                                            <div id="divPassLabel" runat="server" clientidmode="Static">
                                                <cc1:ucLabel ID="UcLabel8" runat="server" Text="Supervisor Password"></cc1:ucLabel>
                                            </div>
                                        </td>
                                        <td style="width: 1%;">
                                            <div id="divPassCollon" runat="server" clientidmode="Static">
                                                :
                                            </div>
                                        </td>
                                        <td style="width: 35%;">
                                            <div id="divPassTextBox" runat="server" clientidmode="Static">
                                                <cc1:ucTextbox ID="txtSupervisorPassword" runat="server" TextMode="Password" Width="100px"></cc1:ucTextbox>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </div>

                        <cc1:ucPanel ID="pnlImagetobeattached" GroupingText="Image to be attached" runat="server"
                            CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblUploadFiles" runat="server" Text="Choose Image" /></td>
                                    <td>:
                                    </td>
                                    <td>
                                        <asp:FileUpload runat="server" ID="fileImage" CssClass="button" Width="400px" multiple="multiple" />
                                        &nbsp;&nbsp;<input type="button" id="btnUpload" value="Attach Image" class="button" onclick="onupload();" />

                                        <cc1:ucButton ID="btnAdd" runat="server" Text="Add" CssClass="button" Style="display: none;" OnClick="btnAddImage_Click" />

                                        <asp:Label ID="lbl_emsg" runat="server" ForeColor="Red"></asp:Label>
                                        <asp:Label ID="lbl_smsg" runat="server" ForeColor="Green"></asp:Label>
                                    </td>

                                </tr>
                                <tr>

                                    <td align="right" colspan="3">
                                        <table width="100%" cellpadding="5" cellspacing="0" class="form-tableGrid">
                                            <tr>
                                                <th>
                                                    <cc1:ucLabel runat="server" ID="lblTaggedImages" Text="Tagged Images" />
                                                    <%--Tagged Images--%>
                                                </th>
                                                <th>
                                                    <cc1:ucLabel runat="server" ID="lblRemove" Text="Remove" />
                                                    <%--Remove--%>
                                                </th>
                                            </tr>
                                            <asp:Repeater ID="rpImages" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <a onclick='<%# string.Format("previewImage(\"{0}\");", Eval("ImageName"))%>' id="hylImageName"
                                                                runat="server" style="cursor: pointer; text-decoration: underline; color: Blue">
                                                                <%#Eval("ImageName") %></a>
                                                            <asp:HiddenField ID="hdnImageID" runat="server" Value='<%#Eval("ImageID") %>' />
                                                        </td>
                                                        <td>
                                                            <cc1:ucButton ID="RemoveImage" runat="server" Text="Remove" CommandArgument='<%#Eval("ImageID") %>'
                                                                CommandName="Remove" OnCommand="RemoveImage_Click" CssClass="button" OnClientClick="return ConfirmMess();" />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </div>

                    <div class="button-row" id="divBookingButton" runat="server">
                        <cc1:ucButton ID="btnOK_2" runat="server" Text="OK" CssClass="button" OnClick="btnOK2_Click"
                            OnClientClick="return checkvalidation();" ValidationGroup="a" />
                        <cc1:ucButton ID="btnCancel_2" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" />
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="false" ShowMessageBox="true" ValidationGroup="a" />
                    </div>

                </div>

                <div class="bottom-shadow">
                </div>

                <cc1:ucPanel ID="pnlVendorPalletChecking" runat="server" CssClass="fieldset-form" Visible="false">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <cc1:ucLabel ID="lblDelieveryUnloadPalletCheckingMesg1" ForeColor="Red" Font-Bold="true" Font-Size="11px"
                    Visible="false" runat="server" Text="Please note, this delivery should have been checked for ISPM15 pallet issues."></cc1:ucLabel>

                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <cc1:ucLabel ID="lblDelieveryUnloadPalletCheckingMesg2" ForeColor="Red" Visible="false"
                    Font-Bold="true" Font-Size="11px" runat="server"
                    Text="Please state below if this delivery had an ISPM15 check. If there were issues, please describe these in the comments box"></cc1:ucLabel>

                    <cc1:ucLabel ID="lblDelieveryUnloadStandardPalletCheckingMesg1" ForeColor="Red" Font-Bold="true" Font-Size="11px"
                        Visible="false" runat="server" Text="Please note, this delivery should have been checked for standard pallet issues."></cc1:ucLabel>
                    <br />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <cc1:ucLabel ID="lblDelieveryUnloadStandardPalletCheckingMesg2" ForeColor="Red" Visible="false"
                    Font-Bold="true" Font-Size="11px" runat="server"
                    Text="Please state below if this delivery had an standard pallet check. If there were issues, please describe these in the comments box"></cc1:ucLabel>

                    <br />

                    <div id="divPalletCheckMesg" runat="server">
                        <asp:CheckBoxList runat="server" ID="chkCarrierVendor" Style="display: none;" RepeatDirection="Vertical">
                        </asp:CheckBoxList>
                        <br />

                        <cc1:ucPanel ID="pnlDelieveryUnload1" runat="server" CssClass="fieldset-form">
                            <table border="0" cellpadding="0" cellspacing="10">
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="rdoDelieveryUnload1" runat="server" Text="Yes" GroupName="Delievery" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rdoDelieveryUnload2" runat="server" Text="NO" GroupName="Delievery" />
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                        <br />
                        <cc1:ucPanel ID="pnlDelieveryUnload2" runat="server" CssClass="fieldset-form" Style="display: none">
                            <table border="0" cellpadding="0" cellspacing="10">
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="rdoDelieveryUnload2_1" Text="Yes" runat="server" GroupName="Delievery2" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rdoDelieveryUnload2_2" Text="NO" runat="server" GroupName="Delievery2" />
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                        <br />
                        <cc1:ucPanel ID="pnlDelieveryUnload3" runat="server" CssClass="fieldset-form" Style="display: none">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <cc1:ucTextbox ID="txtChecked" runat="server" Width="200px"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                        <br />
                        <table id="tblComments" runat="server" border="0" cellpadding="0" cellspacing="0" style="display: none">
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblcomments_1" runat="server"></cc1:ucLabel>:  </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" ID="txtComments_1" TextMode="MultiLine" Height="50px" Width="500px" /></td>
                            </tr>
                        </table>

                        <table id="tblCommentsOnNoDeliveryChecked" runat="server" border="0" cellpadding="0" cellspacing="0" style="display: none">
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblCommentsOnNoDeliveryChecked" runat="server"></cc1:ucLabel>:  </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" ID="txtCommentsOnNoDeliveryChecked" TextMode="MultiLine" Height="50px" Width="500px" /></td>
                            </tr>
                        </table>

                    </div>
                    <br />
                    <br />
                </cc1:ucPanel>

                <div class="bottom-shadow">
                </div>

                <div class="button-row" id="divBookingButton3" runat="server">
                    <cc1:ucButton ID="btnOk3" runat="server" Text="OK" Visible="false" CssClass="button" OnClick="btnOk3_Click" />
                    <cc1:ucButton ID="btnCancel3" runat="server" Text="Cancel" Visible="false" CssClass="button" OnClick="btnCancel3_Click" />
                </div>

                <cc1:ucPanel ID="pnlHazardousItemChecking" runat="server" CssClass="fieldset-form" Visible="false">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
                    <cc1:ucLabel ID="lblHazardousItemCheckingDelivery" ForeColor="Red" Font-Bold="true" Font-Size="11px"
                    Visible="false" runat="server" Text="Please note, this delivery should have been checked for Hazardous Item."></cc1:ucLabel>
                    
                    <br />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <cc1:ucLabel ID="lblHazardousItemCheckingDeliveryComments" ForeColor="Red" Visible="false"
                    Font-Bold="true" Font-Size="11px" runat="server"
                    Text="Please state below if this delivery had an Hazardous Item. If there were issues, please describe these in the comments box"></cc1:ucLabel>
 
                    <br /> 

                    <div id="divHazardousMesg" runat="server">
                        <asp:CheckBoxList runat="server" ID="chkCarrierHazardousVendor" Style="display: none;" RepeatDirection="Vertical">
                        </asp:CheckBoxList>
                        <br />

                        <cc1:ucPanel ID="pnlDelieveryHUnload1" runat="server" CssClass="fieldset-form">
                            <table border="0" cellpadding="0" cellspacing="10">
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="rdoHDelieveryUnload1" runat="server" Text="Yes" GroupName="Delievery3" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rdoHDelieveryUnload2" runat="server" Text="NO" GroupName="Delievery3" />
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                        <br />

                        <cc1:ucPanel ID="pnlDelieveryHUnload2" runat="server" CssClass="fieldset-form" Style="display: none">
                            <table border="0" cellpadding="0" cellspacing="10">
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="rdoHDelieveryUnload2_1" Text="Yes" runat="server" GroupName="Delievery4" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rdoHDelieveryUnload2_2" Text="NO" runat="server" GroupName="Delievery4" />
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                        <br />

                        <cc1:ucPanel ID="pnlDelieveryHUnload3" runat="server" CssClass="fieldset-form" Style="display: none">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <cc1:ucTextbox ID="txtHChecked" runat="server" Width="200px"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                        <br />

                        <table id="tblHComments" runat="server" border="0" cellpadding="0" cellspacing="0" style="display: none">
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblcomments_2" runat="server"></cc1:ucLabel>:  </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" ID="txtHComments_1" TextMode="MultiLine" Height="50px" Width="500px" /></td>
                            </tr>
                        </table>

                        <table id="tblHCommentsOnNoDeliveryChecked" runat="server" border="0" cellpadding="0" cellspacing="0" style="display: none">
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblHCommentsOnNoDeliveryChecked" runat="server" Text="Comments"></cc1:ucLabel>:  </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" ID="txtHCommentsOnNoDeliveryChecked" TextMode="MultiLine" Height="50px" Width="500px" /></td>
                            </tr>
                        </table>
                    </div>

                    <br />
                    <br />
                </cc1:ucPanel>

                <div class="button-row">
                    <cc1:ucButton ID="btnOK" runat="server" Text="OK" Visible="false" CssClass="button" OnClick="btnOK_Click" />
                    <cc1:ucButton ID="btnCancel" runat="server" Text="Cancel" Visible="false" CssClass="button" OnClick="btnCancel_Click" />
                </div>

            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnOK" />

        </Triggers>

    </asp:UpdatePanel>

    <!-- Phase 13 R3 Point 18 --->
    <asp:UpdatePanel ID="updInfromation" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnInfo" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdInformation" runat="server" TargetControlID="btnInfo"
                PopupControlID="pnlbtnInfo" BackgroundCssClass="modalBackground" BehaviorID="MsgbtnInfo"
                DropShadow="false" />
            <asp:Panel ID="pnlbtnInfo" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3>
                        <cc1:ucLabel ID="lblWarningInfo" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <cc1:ucLabel ID="lblSchedulingIssueCommunication" runat="server" Text="Please select which companies will receive mail relating to this issue"></cc1:ucLabel>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <cc1:ucLabel ID="lblCarrier" runat="server" Text="" Font-Bold="true"></cc1:ucLabel>
                                        <br />
                                        <cc1:ucCheckbox ID="chkCarrier" runat="server" Checked="true" Text="" TextAlign="Right" />
                                        <br />
                                        <br />
                                        <br />
                                        <cc1:ucLabel ID="lblVendors" runat="server" Text="" Font-Bold="true"></cc1:ucLabel>
                                        <cc1:ucCheckboxList ID="chkListVendors" runat="server" RepeatColumns="2">
                                        </cc1:ucCheckboxList>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <cc1:ucButton ID="btnOK_1" runat="server" Text="" CssClass="button"
                                        OnCommand="btnOK_1_Click" />

                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnOK_1" />
        </Triggers>
    </asp:UpdatePanel>


    <!-- Phase 27 R1 Point 1 --->
    <asp:UpdatePanel ID="updInfromation_Messge" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnInfo_Messge" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="updInfromation_Messge_update" runat="server" TargetControlID="btnInfo_Messge"
                PopupControlID="Panel1" BackgroundCssClass="modalBackground" BehaviorID="btnInfo_Messge"
                DropShadow="false" />
            <asp:Panel ID="Panel1" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3>
                        <cc1:ucLabel ID="UcLabel1" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer" style="background-color: white">
                                        <tr>
                                            <td colspan="2">
                                                <b>
                                                    <cc1:ucLiteral ID="litRejectUnLoadDelivery" runat="server"
                                                        Text="">
                                                    </cc1:ucLiteral>
                                                </b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblDateTimeBooking" runat="server"
                                                    Text="Date/time of booking:">
                                                </cc1:ucLabel></td>
                                            <td>
                                                <cc1:ucLabel ID="lblDateTimeBookingValue" runat="server">
                                                </cc1:ucLabel>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblVendor_" runat="server"
                                                    Text="Vendor #">
                                                </cc1:ucLabel></td>
                                            <td>

                                                <cc1:ucLabel ID="lblVendorValue" runat="server">
                                                </cc1:ucLabel>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblVendor" runat="server"
                                                    Text="Vendor">
                                                </cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:ucLabel ID="lblVendorNameValue" runat="server">
                                                </cc1:ucLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblBookingReference" runat="server"
                                                    Text="Booking Reference">
                                                </cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:ucLabel ID="lblBookingReferenceValue" runat="server">
                                                </cc1:ucLabel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <cc1:ucButton ID="btnConfirm" runat="server" Text="Confirm" CssClass="button"
                                        OnClick="BtnConfirm_Click" />
                                    <cc1:ucButton ID="btnCancel1" runat="server" Text="Cancel" CssClass="button"
                                        OnClick="BtnCancel1_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnConfirm" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel1" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" 
CodeFile="BookingTypeOverview.aspx.cs" 
Inherits="ModuleUI_GlobalSettings_BookingTypeOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<%@ Register src="../../CommonUI/UserControls/ucExportToExcel.ascx" tagname="ucExportToExcel" tagprefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h2>
    <cc1:ucLabel ID="lblBookingType" runat="server" Text="Booking Type" ></cc1:ucLabel>
</h2>
<div class="button-row">
 
    <cc2:ucExportToExcel ID="btnExportToExcel1" runat="server"/>
</div>
 <table width="100%">     
        <tr>
            <td align="center">
                <cc1:ucGridView ID="grdBookingType" Width="100%" runat="server" CssClass="grid" 
                    AutoGenerateColumns="False" CellPadding="0" GridLines="None" 
                    onsorting="grdBookingType_Sorting" AllowSorting="true">                                        
<AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                    <Columns>                        
                       <asp:TemplateField HeaderText="Booking Type" SortExpression="BookingTypeDescription">
                            <HeaderStyle HorizontalAlign="Left" Width="100px" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblBoolingType" runat="server" Text='<%# Eval("BookingTypeDescription")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>                                        

<RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
                </cc1:ucGridView>               
            </td>
        </tr>
       
    </table>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
</asp:Content>
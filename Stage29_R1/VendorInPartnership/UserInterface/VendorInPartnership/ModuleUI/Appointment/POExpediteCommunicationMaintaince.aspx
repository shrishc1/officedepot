﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="POExpediteCommunicationMaintaince.aspx.cs" Inherits="ModuleUI_Appointment_POExpediteCommunicationMaintaince"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        function chkSendMailChange(obj) {
            var confirmRes = confirm("Do you want to apply setting for ALL Vendors");
            if (confirmRes == true) {
                if ($(obj).attr('id').indexOf("chkSendDailyPO") >= 0) {
                    if ($(obj).is(":checked")) {
                        UpdateRecord('PO', true);
                    }
                    else {
                        UpdateRecord('PO', false);
                    }
                }
                else if ($(obj).attr('id').indexOf("chkSendWeeklyExpediteReminder") >= 0) {
                    if ($(obj).is(":checked")) {
                        UpdateRecord('ER', true);
                    }
                    else {
                        UpdateRecord('ER', false);
                    }
                }
            }
            else {
                if ($(obj).is(":checked")) {
                    $(obj).attr('checked', false);
                }
                else {
                    $(obj).attr('checked', true);
                }
            }
        }

        function UpdateRecord(colName, valToset) {
            var vendorIDs = $("[id$='hdnVendorIds']").val();
            $.ajax({
                type: "POST",
                url: "POExpediteCommunicationMaintaince.aspx/UpdatePOAndExpediteReminder",
                data: '{vendorIDs: "' + vendorIDs + '",colName:"' + colName + '",valToset:"' + valToset + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    if (response != null && response.d != null) {
                        alert('<%=UpdatedSuccessfully%>');
                        window.location.href = '<%=EncURL%>';
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblPOCommunicationSettings" runat="server"></cc1:ucLabel>
        <cc1:ucLabel ID="lblPOExpediteCommunication" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(UpdateRecord);
    </script>
    <asp:HiddenField ID="hdnVendorIds" runat="server" />
    <asp:HiddenField ID="hdnVendorId" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucMultiView ID="mvDiscrepancyList" runat="server" ActiveViewIndex="0">
                <cc1:ucView ID="vwSearchCreteria" runat="server">
                    <cc1:ucPanel ID="UcCarrierVendorPanel" runat="server">
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td align="right">
                                    <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td colspan="3">
                                    <table>
                                        <tr>
                                            <td style="font-weight: bold; width: 10%">
                                                <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 1%">
                                                <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; width: 10%">
                                                <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor Name"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 1%">
                                                <cc1:ucLabel ID="lblVendorCollon" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; width: 10%">
                                            </td>
                                            <td style="font-weight: bold; width: 1%">
                                            </td>
                                            <td>
                                                <table width="100%">
                                                    <tr style="height: 3em;">
                                                        <td width="10%" style="font-weight: bold; text-align: left;">
                                                            <cc1:ucRadioButton ID="rbAll" GroupName="Display" runat="server" Text=" All" />
                                                        </td>
                                                        <td width="10%" style="font-weight: bold; text-align: left;">
                                                            <cc1:ucRadioButton GroupName="Display" ID="rbPO" Width="120" runat="server" Text="Purchase Order" />
                                                        </td>
                                                        <td width="10%" style="font-weight: bold; text-align: left;">
                                                            <cc1:ucRadioButton GroupName="Display" ID="rdExpedite" Width="80" runat="server"
                                                                Text="Expedite" />
                                                        </td>
                                                        <td width="20%" style="font-weight: bold; text-align: left;">
                                                            <cc1:ucRadioButton GroupName="Display" ID="rbPOEx" Width="170" runat="server" Text="Purchase Order & Expedite" />
                                                        </td>
                                                        <td width="10%" style="font-weight: bold;">
                                                            <cc1:ucRadioButton GroupName="Display" ID="rbNone" runat="server" Text="None" />
                                                        </td>
                                                        <td width="43%">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </cc1:ucView>
                <cc1:ucView ID="vwSearchListing" runat="server">
                    <div class="button-row">
                        <uc1:ucExportButton ID="btnExportToExcel" runat="server" />
                    </div>
                    <cc1:ucPanel ID="pnlGrid" runat="server" ScrollBars="Both" CssClass="fieldset-form">
                        <cc1:ucGridView ID="gvPOExpedite" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid"
                            PageSize="50" AllowPaging="true" PagerStyle-HorizontalAlign="left" AllowSorting="true"
                            Style="overflow: auto;" OnPageIndexChanging="gvPOExpedite_PageIndexChanging">
                            <EmptyDataTemplate>
                                <div style="text-align: center">
                                    <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField ItemStyle-Wrap="false" HeaderText="CountryName" DataField="CountryName"
                                    AccessibleHeaderText="false" SortExpression="CountryName">
                                    <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField ItemStyle-Wrap="false" HeaderText="VendorNo" DataField="Vendor_No"
                                    AccessibleHeaderText="false" SortExpression="Vendor_No">
                                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="VendorName" SortExpression="VendorName">
                                    <HeaderStyle Width="25%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLinkButton ID="lnkVendorNames" runat="server" Text='<%# Eval("VendorName") %>'
                                            OnClick="lnkVendorName_Click" CommandArgument='<%# Eval("VendorID")+","+Eval("VendorName")+","+Eval("CountryName")+","+Eval("IsSendDailyPO")+","+Eval("IsSendWeeklyExpediteReminder") %>'></cc1:ucLinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SelectAllCheckBox">
                                    <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                    <ItemStyle Width="20%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <HeaderTemplate>
                                        <cc1:ucCheckbox runat="server" ID="chkSendDailyPO" CssClass="checkbox-input" onclick="chkSendMailChange(this);" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblIsSendDailyPO" runat="server" Text='<%# Eval("IsSendDailyPO").ToString().ToUpper() .Equals ("TRUE") ? "Y" : "N" %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SelectAllCheckBox">
                                    <HeaderStyle HorizontalAlign="Center" Width="30%" />
                                    <ItemStyle Width="30%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <HeaderTemplate>
                                        <cc1:ucCheckbox runat="server" ID="chkSendWeeklyExpediteReminder" CssClass="checkbox-input"
                                            onclick="chkSendMailChange(this);" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblIsSendWeeklyExpediteReminder" runat="server" Text='<%# Eval("IsSendWeeklyExpediteReminder").ToString().ToUpper() .Equals ("TRUE") ? "Y" : "N" %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </cc1:ucPanel>
                </cc1:ucView>
                <cc1:ucView ID="vwAdd" runat="server">
                    <asp:Panel ID="pnlAddPOExpediteCommunication" runat="server">
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="width: 5%">
                                </td>
                                <td style="font-weight: bold; width: 35%">
                                    <cc1:ucLabel ID="lblCountry_1" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 5%">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 50%">
                                    <cc1:ucLabel ID="lblCountryV" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 5%">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendorName" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendorNameV" runat="server"></cc1:ucLabel>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <cc1:ucPanel ID="pnlCommunicationRules" runat="server" CssClass="fieldset-form">
                                        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder form-table">
                                            <tr>
                                                <td style="font-weight: bold;">
                                                    <cc1:ucLabel ID="lblPleaseSelectCommunicationOptions" runat="server"></cc1:ucLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cc1:ucCheckbox ID="chkSendDailyPOFile" runat="server" Width="200px" />
                                                    <cc1:ucCheckbox ID="chkSendWeeklyExpediteReminder" runat="server" Width="250px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </cc1:ucPanel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </cc1:ucView>
            </cc1:ucMultiView>
        </div>
        <div class="button-row">
            <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Search" CssClass="button"
                OnClick="btnGenerateReport_Click" />
            <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Criteria" CssClass="button"
                OnClick="btnBack_Click" />
            <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
            <cc1:ucButton ID="btnBack_1" runat="server" Text="Back" CssClass="button" OnClick="btnBack1_Click" />
        </div>
    </div>
    <cc1:ucGridView ID="tempGridView" Visible="false" Width="100%" runat="server" CssClass="grid">
        <Columns>
            <asp:BoundField HeaderText="CountryName" DataField="CountryName" AccessibleHeaderText="false">
            </asp:BoundField>
            <asp:BoundField HeaderText="VendorNo" DataField="Vendor_No" AccessibleHeaderText="false">
            </asp:BoundField>
            <asp:BoundField HeaderText="VendorName" DataField="VendorName" AccessibleHeaderText="false">
            </asp:BoundField>
            <asp:TemplateField HeaderText="SendDailyPO">
                <ItemTemplate>
                    <cc1:ucLabel ID="lblIsSendDailyPO" runat="server" Text='<%# Eval("IsSendDailyPO").ToString().ToUpper() .Equals ("TRUE") ? "Y" : "N" %>'></cc1:ucLabel>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="SendWeeklyExpediteReminder">
                <ItemTemplate>
                    <cc1:ucLabel ID="lblIsSendWeeklyExpediteReminder" runat="server" Text='<%# Eval("IsSendWeeklyExpediteReminder").ToString().ToUpper() .Equals ("TRUE") ? "Y" : "N" %>'></cc1:ucLabel>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </cc1:ucGridView>
</asp:Content>

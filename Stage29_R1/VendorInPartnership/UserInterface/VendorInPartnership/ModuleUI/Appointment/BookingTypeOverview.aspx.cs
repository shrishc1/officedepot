﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class ModuleUI_GlobalSettings_BookingTypeOverview : CommonPage 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            BindBookingType();

        btnExportToExcel1.GridViewControl = grdBookingType;
        btnExportToExcel1.FileName = "BookingTypeOverview";
    }


    #region Methods

    protected void BindBookingType()
    {
        SqlConnection sqlCon = GetConnection();
        SqlDataAdapter sqlDa = new SqlDataAdapter(@"Select * from MAS_BookingType", sqlCon);
        DataSet dsLeaveData = new DataSet();

        sqlDa.Fill(dsLeaveData);
        grdBookingType.DataSource = dsLeaveData.Tables[0];
        grdBookingType.DataBind();
        sqlCon.Close();
        ViewState["myDataTable"] = dsLeaveData.Tables[0];
    }
    protected SqlConnection GetConnection()
    {
       
        return (new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["sConn"]));
    }

    #endregion
    protected void grdBookingType_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["myDataTable"] != null)
        {
            DataTable myDataTable = (DataTable)ViewState["myDataTable"];
            ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";
            DataView view = myDataTable.DefaultView;
            view.Sort = e.SortExpression + " " + Convert.ToString(ViewState["sortDirection"]);
            grdBookingType.DataSource = view;
            grdBookingType.DataBind();

        }
    }
}
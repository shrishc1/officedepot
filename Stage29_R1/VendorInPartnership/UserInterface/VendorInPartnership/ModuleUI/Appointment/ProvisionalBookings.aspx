﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ProvisionalBookings.aspx.cs" Inherits="ProvisionalBookings"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblProvisionalBookings" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSelectSiteCountry" GroupingText="Select Site/Country" runat="server" CssClass="fieldset-form">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                    <tr>
                        <td style="font-weight: bold; width: 13%">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="width: 36%">
                            <uc1:ucCountry ID="ddlCountry" runat="server" width="150px" />
                        </td>
                        <td style="font-weight: bold; width: 5%">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="width: 44%">
                            <uc2:ucSite ID="ddlSite" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div class="button-row" style="text-align: center;">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <div style="width: 99%; overflow: auto" id="divPrint" class="fixedTable" onscroll="getScroll1(this);">
        <table cellspacing="1" cellpadding="0" class="form-table">
            <tr>
                <td>
                    <cc1:ucGridView ID="gvVendor" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                        CellPadding="0" Width="980px" DataKeyNames="PKID" OnSorting="gridView_Sorting"
                        AllowSorting="true" AllowPaging="true" PageSize="30" OnPageIndexChanging="gvVendor_PageIndexChanging"
                        OnRowDataBound="gvVendor_RowDataBound" OnRowCommand="gvVendor_RowCommand">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <EmptyDataTemplate>
                            <div style="text-align: center">
                                <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                            </div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Site" SortExpression="SiteName">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="LBLSiteName" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Booking Ref #" SortExpression="BookingRef">
                                <HeaderStyle Width="170px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="ltHistory" runat="server" Text='<%#Eval("BookingRef") %>'></cc1:ucLiteral>
                                    <asp:HyperLink ID="hypHistory" runat="server" Text='<%#Eval("BookingRef") %>' ></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Time / Day" SortExpression="Weekday" >
                                <HeaderStyle Width="150px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnTimeWindowID" runat="server" Value='<%# Eval("TimeWindowID") %>' />
                                    <cc1:ucLiteral ID="UcLiteral27_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                        Visible="false"></cc1:ucLiteral>
                                    <cc1:ucLiteral ID="UcLiteral27_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                        Visible="false"></cc1:ucLiteral>
                                    <cc1:ucLiteral ID="UcLiteral27" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>&nbsp;
                                    <cc1:ucLiteral ID="UcLiteral28" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Date">
                                <HeaderStyle Width="200px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnExpectedDeliveryDate" runat="server" Value='<%# Eval("DeliveryDate") %>' />
                                    <cc1:ucLabel ID="lblExpectedDeliveryDate" runat="server" Text='<%# Eval("DeliveryDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Vendor Name" SortExpression="VendorName">
                                <HeaderStyle Width="200px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral2" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Carrier Name" SortExpression="CarrierName">
                                <HeaderStyle Width="180px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral3" runat="server" Text='<%#Eval("CarrierName") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NoOfPallets" SortExpression="PalletsScheduled">
                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral4" runat="server" Text='<%#Eval("PalletsScheduled") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NoOfCartons" SortExpression="CatronsScheduled">
                                <HeaderStyle Width="75px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral6" runat="server" Text='<%#Eval("CatronsScheduled") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="NoOfLifts" DataField="LiftsScheduled" SortExpression="LiftsScheduled">
                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="NoOfLines" SortExpression="LinesScheduled">
                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral5" runat="server" Text='<%#Eval("LinesScheduled") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                           
                            <asp:TemplateField HeaderText="Reason" SortExpression="ProvisionalReason">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>                                   
                                    <asp:HyperLink ID="hypReason" runat="server" Text='<%#Eval("ProvisionalReason") %>' NavigateUrl="#"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Confirm / Reject">
                                <HeaderStyle Width="160px" />
                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Font-Bold="true" />
                                <ItemTemplate>                                   
                                    <asp:LinkButton CommandName="Confirm" CommandArgument='<%#Eval("BookingID") %>' ID="hypConfirm" runat="server" Visible="false"></asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="hypReject" CommandName="Reject" CommandArgument='<%#Eval("BookingID") %>' runat="server" Visible="false"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </cc1:ucGridView>
                    <cc1:ucGridView ID="gvExport" OnRowDataBound="gvExport_RowDataBound" runat="server" AutoGenerateColumns="false" Style="display: none;">
                        <Columns>
                            <asp:TemplateField HeaderText="Site">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="LBLSiteName" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Booking Ref #">
                                <HeaderStyle Width="170px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="ltHistory" runat="server" Text='<%#Eval("BookingRef") %>'></cc1:ucLiteral>
                                    <asp:HyperLink ID="hypHistory" runat="server" Text='<%#Eval("BookingRef") %>' NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?PN=BOINQ&Scheduledate=" + Eval("DeliveryDate", "{0:dd/MM/yyyy}") + "&ID=" + Eval("PKID") + "-V-" + Eval("PKID") + "-" +  Eval("PKID")) %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Time / Day">
                                <HeaderStyle Width="150px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnTimeWindowID" runat="server" Value='<%# Eval("TimeWindowID") %>' />
                                    <cc1:ucLiteral ID="UcLiteral27_2" runat="server" Text='<%#Eval("NonWindowFromToTime") %>'
                                        Visible="false"></cc1:ucLiteral>
                                    <cc1:ucLiteral ID="UcLiteral27_1" runat="server" Text='<%#Eval("WindowStartEndTime") %>'
                                        Visible="false"></cc1:ucLiteral>
                                    <cc1:ucLiteral ID="UcLiteral27" runat="server" Text='<%#Eval("ExpectedDeliveryTime") %>'></cc1:ucLiteral>&nbsp;
                                    <cc1:ucLiteral ID="UcLiteral28" runat="server" Text='<%# Eval("DeliveryDate", "{0:dddd}")%>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Date">
                                <HeaderStyle Width="200px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnExpectedDeliveryDate" runat="server" Value='<%# Eval("DeliveryDate") %>' />
                                    <cc1:ucLabel ID="lblExpectedDeliveryDate" runat="server" Text='<%# Eval("DeliveryDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Vendor Name">
                                <HeaderStyle Width="200px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral2" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Carrier Name">
                                <HeaderStyle Width="180px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral3" runat="server" Text='<%#Eval("CarrierName") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NoOfPallets" SortExpression="PalletsScheduled">
                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral4" runat="server" Text='<%#Eval("PalletsScheduled") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NoOfCartons" SortExpression="CatronsScheduled">
                                <HeaderStyle Width="75px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral6" runat="server" Text='<%#Eval("CatronsScheduled") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="NoOfLifts" DataField="LiftsScheduled" SortExpression="LiftsScheduled">
                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="NoOfLines" SortExpression="LinesScheduled">
                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLiteral ID="UcLiteral5" runat="server" Text='<%#Eval("LinesScheduled") %>'></cc1:ucLiteral>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="ProvisionalReason" DataField="ProvisionalReason">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                    </cc1:ucGridView>
                </td>
            </tr>
        </table>
    </div>

          <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <%---WARNING popup Reject Provisional Booking start--%>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnRejectProvisional" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlRejectProvisional" runat="server" TargetControlID="btnRejectProvisional"
                PopupControlID="pnlRejectProvisional" BackgroundCssClass="modalBackground" BehaviorID="RejectProvisional"
                DropShadow="false" />
            <asp:Panel ID="pnlRejectProvisional" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3><cc1:ucLabel ID="lblWarningInfo" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                             <div  class="popup-innercontainer top-setting-Popup">
                               <div class="row1"> <cc1:ucLabel ID="lblRejectProvisionalMsg" runat="server" ></cc1:ucLabel></div>
                               </div>
                            </td>
                        </tr>
                         <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblAddComment" runat="server" ></cc1:ucLabel><br />
                                <asp:TextBox TextMode="MultiLine" ID="txtRejectProCmments" style="height:40px;width:480px;" runat="server"></asp:TextBox>
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:HiddenField ID="hdnBookingID" runat="server" />
                               <div class="row"> <cc1:ucButton ID="btnContinue_2" runat="server" Text="CONTINUE" CssClass="button"
                                    OnCommand="btnContinue_2_Click" />
                                &nbsp;
                                <cc1:ucButton ID="btnBack_2" runat="server" Text="BACK" CssClass="button" OnCommand="btnBack_2_Click" />
                           </div> </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnContinue_2" />
            <asp:AsyncPostBackTrigger ControlID="btnBack_2" />
        </Triggers>
    </asp:UpdatePanel>
    <%---WARNING popup Reject Provisional Booking End--%>

</asp:Content>

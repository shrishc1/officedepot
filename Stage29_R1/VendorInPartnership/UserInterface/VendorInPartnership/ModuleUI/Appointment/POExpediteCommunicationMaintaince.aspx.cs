﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.ReportRequest;
using System.Linq;
using WebUtilities;
using Utilities;

public partial class ModuleUI_Appointment_POExpediteCommunicationMaintaince : CommonPage
{
    #region Declarations ...
    protected string UpdatedSuccessfully = WebCommon.getGlobalResourceValue("UpdatedSuccessfully");
    protected string EncURL = "../Appointment/POExpediteCommunicationMaintaince.aspx?" + Encryption.Encrypt("FromPage=POExpediteCommunicationMaintaince");
    #endregion

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        //btnExportToExcel.GridViewControl = gvPOExpedite;
        btnExportToExcel.GridViewControl = tempGridView;
        btnExportToExcel.Page = this;
        btnExportToExcel.FileName = "POExpediteCommunicationMaintainceReport";
        //lblSearchResult.Text = "Purchase Order & Expedite Communication Maintaince Report";

        if (!IsPostBack)
        {

            lblPOCommunicationSettings.Visible = false;
            lblPOExpediteCommunication.Visible = true;

            btnBackSearch.Visible = false;
            btnSave.Visible = false;
            btnBack_1.Visible = false;

            mvDiscrepancyList.ActiveViewIndex = 0;
            rbAll.Checked = true;

            if (Session["oVendorBE"] != null)
            {
                if (GetQueryStringValue("FromPage") != null && GetQueryStringValue("FromPage").ToString().Trim() == "POExpediteCommunicationMaintaince"
                || Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].ToString().Trim() == "POExpediteCommunicationMaintaince")
                {
                    UP_VendorBE oVendorBE = (UP_VendorBE)Session["oVendorBE"];
                    BindGrid(oVendorBE);
                }
            }
        }

        msCountry.SetCountryOnPostBack();
        msVendor.setVendorsOnPostBack();
    }

    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {

        UP_VendorBE oVendorBE = new UP_VendorBE();
        oVendorBE = SetFilter();
        BindGrid(oVendorBE);
        if (gvPOExpedite.Rows.Count > 0)
        {
            gvPOExpedite.PageIndex = 0;
            gvPOExpedite.DataBind();
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 0;
        btnBackSearch.Visible = false;
        btnGenerateReport.Visible = true;

        btnSave.Visible = false;
        btnBack_1.Visible = false;


        lblPOCommunicationSettings.Visible = false;
        lblPOExpediteCommunication.Visible = true;
    }

    protected void gvPOExpedite_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (Cache["lstVendor"] != null)
        {
            List<UP_VendorBE> lsOpenPO = Cache["lstVendor"] as List<UP_VendorBE>;
            gvPOExpedite.PageIndex = e.NewPageIndex;
            gvPOExpedite.DataSource = lsOpenPO;
            gvPOExpedite.DataBind();
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<UP_VendorBE>.SortList((List<UP_VendorBE>)Cache["lstVendor"], e.SortExpression, e.SortDirection).ToArray();
    }

    [System.Web.Services.WebMethod]
    public static string UpdatePOAndExpediteReminder(string vendorIDs, string colName, bool valToset)
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        string columnName = string.Empty;
        try
        {
            if (colName == "PO")
            {
                oUP_VendorBE.IsSendDailyPO = valToset;
                oUP_VendorBE.SearchedVendorText = "PO";
            }
            else if (colName == "ER")
            {
                oUP_VendorBE.IsSendWeeklyExpediteReminder = valToset;
                oUP_VendorBE.SearchedVendorText = "Expedite";
            }

            oUP_VendorBE.VendorIDs = vendorIDs;
            oUP_VendorBE.Action = "UpdatePOCommunicationSetting";

            oUP_VendorBAL.AddPOExpediteCommunicationBAL(oUP_VendorBE);

            return "";
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void lnkVendorName_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 2;
        LinkButton lnk = (LinkButton)sender;
        var val = (lnk.CommandArgument).Split(',');
        hdnVendorId.Value = val[0];
        lblVendorNameV.Text = val[1];
        lblCountryV.Text = val[2];
        chkSendDailyPOFile.Checked = Convert.ToBoolean(val[3]);
        chkSendWeeklyExpediteReminder.Checked = Convert.ToBoolean(val[4]);

        btnSave.Visible = true;
        btnBack_1.Visible = true;
        btnBackSearch.Visible = false;
        //mdlAddPOExpedite.Show();

        lblPOExpediteCommunication.Visible = true;
        lblPOCommunicationSettings.Visible = false;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 1;
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        string columnName = string.Empty;
        try
        {
            oUP_VendorBE.IsSendDailyPO = (chkSendDailyPOFile.Checked ? true : false);
            oUP_VendorBE.IsSendWeeklyExpediteReminder = (chkSendWeeklyExpediteReminder.Checked ? true : false);

            oUP_VendorBE.VendorIDs = Convert.ToString(hdnVendorId.Value);
            oUP_VendorBE.Action = "UpdatePOCommunicationSetting";
            oUP_VendorBE.SearchedVendorText = "POExpedite";
            oUP_VendorBAL.AddPOExpediteCommunicationBAL(oUP_VendorBE);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + UpdatedSuccessfully + "');</script>", false);
        }
        catch (Exception)
        {
            throw;
        }

        if (Session["oVendorBE"] != null)
        {
            UP_VendorBE oVendorBE = (UP_VendorBE)Session["oVendorBE"];
            BindGrid(oVendorBE);
        }

        btnSave.Visible = false;
        btnBack_1.Visible = false;
        btnBackSearch.Visible = true;
        lblPOExpediteCommunication.Visible = false;
        lblPOCommunicationSettings.Visible = true;

    }

    protected void btnBack1_Click(object sender, EventArgs e)
    {
        mvDiscrepancyList.ActiveViewIndex = 1;
        btnSave.Visible = false;
        btnBack_1.Visible = false;
        btnBackSearch.Visible = true;

        lblPOExpediteCommunication.Visible = false;
        lblPOCommunicationSettings.Visible = true;
    }

    private void BindGrid(UP_VendorBE oVendorBE)
    {
        lblPOCommunicationSettings.Visible = true;
        lblPOExpediteCommunication.Visible = false;

        List<UP_VendorBE> lstVendor = null;
        UP_VendorBAL objVendorBal = new UP_VendorBAL();

        Session["oVendorBE"] = oVendorBE;


        oVendorBE.Action = "POCommunicationSetting";
        lstVendor = objVendorBal.GetAllVendorsBAL(oVendorBE);

        var vendorIds = lstVendor.Select(x => x.VendorID.ToString()).ToList();

        var IsAllSendDailyPOTrue = lstVendor.All(x => x.IsSendDailyPO == true);
        var IsSendWeeklyExpediteReminderTrue = lstVendor.All(x => x.IsSendWeeklyExpediteReminder == true);

        hdnVendorIds.Value = string.Join(",", vendorIds);

        gvPOExpedite.DataSource = lstVendor;
        gvPOExpedite.DataBind();

        btnExportToExcel.Visible = false;
        if (gvPOExpedite.Rows.Count > 0)
        {
            CheckBox chkSendDailyPO = (CheckBox)gvPOExpedite.HeaderRow.FindControl("chkSendDailyPO");
            CheckBox chkSendWeeklyExpediteReminder = (CheckBox)gvPOExpedite.HeaderRow.FindControl("chkSendWeeklyExpediteReminder");

            chkSendDailyPO.Checked = IsAllSendDailyPOTrue;
            chkSendWeeklyExpediteReminder.Checked = IsSendWeeklyExpediteReminderTrue;
            btnExportToExcel.Visible = true;
        }

        //for export to excel
        tempGridView.DataSource = lstVendor;
        tempGridView.DataBind();

        Cache["lstVendor"] = lstVendor;
        mvDiscrepancyList.ActiveViewIndex = 1;
        btnBackSearch.Visible = true;
        btnGenerateReport.Visible = false;
        rbAll.Checked = true;
    }

    private UP_VendorBE SetFilter()
    {
        UP_VendorBE oVendorBE = new UP_VendorBE();
        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            oVendorBE.VendorIDs = msVendor.SelectedVendorIDs;

        if (!string.IsNullOrEmpty(msCountry.SelectedCountryName))
            oVendorBE.CountryName = msCountry.SelectedCountryName;

        if (rbAll.Checked)
        {
            oVendorBE.SearchedVendorText = "ALL";
        }
        else if (rbPO.Checked)
        {
            oVendorBE.SearchedVendorText = "PO";
        }
        else if (rbPOEx.Checked)
        {
            oVendorBE.SearchedVendorText = "POExpedite";
        }
        else if (rdExpedite.Checked)
        {
            oVendorBE.SearchedVendorText = "Expedite";
        }
        else if (rbNone.Checked)
        {
            oVendorBE.SearchedVendorText = "None";
        }
        oVendorBE.Action = "POCommunicationSetting";

        return oVendorBE;
    }
}
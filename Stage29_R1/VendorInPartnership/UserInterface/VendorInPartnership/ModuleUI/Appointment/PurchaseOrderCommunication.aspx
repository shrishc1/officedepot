﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PurchaseOrderCommunication.aspx.cs" Inherits="PurchaseOrderCommunication" %>
<%@ PreviousPageType VirtualPath="PurchaseOrderInquiry.aspx" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../../../Css/style.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 100%; padding-left: 10px">
        <table>
            <tr>
                <td>
                    <asp:HiddenField ID="hdnCommunicationId" runat="server" />
                </td>
            </tr>
            <tr id="trCoomunication" runat="server">
                <td style="text-align: left;font-weight: bold;">
                    <cc1:ucLabel Text="To : " runat="server" ID="lblEmail"></cc1:ucLabel>&nbsp;&nbsp;&nbsp;
                    <cc1:ucTextbox ID="txtEmail" runat="server"  Width="500px" ></cc1:ucTextbox>&nbsp;&nbsp;&nbsp;
                    <asp:RequiredFieldValidator ID="rfvEmailRequired" runat="server" ControlToValidate="txtEmail"
                                Display="None" ValidationGroup="ReSendCommunication" SetFocusOnError="true" ErrorMessage="Please enter email address">
                            </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revValidEmail" runat="server"  Display="None" ValidationGroup="ReSendCommunication"
                        ControlToValidate="txtEmail" ErrorMessage="Please enter valid email address" 
                        ValidationExpression="(\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([, ]+)?)+"></asp:RegularExpressionValidator>
                    <cc1:ucButton ID="btnSendCommunication" runat="server" class="button" Text="Send"
                        OnClick="btnReSendCommunication_Click" ValidationGroup="ReSendCommunication"/>
                       
                </td>
            </tr>
            <tr>
                <%-- <td style="text-align: left;font-weight: bold;">
                   <cc1:ucLabel Text="Language : " runat="server" ID="UcLabel1"></cc1:ucLabel> <asp:DropDownList ID="drpLanguageId" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpLanguageId_SelectedIndexChanged"></asp:DropDownList>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail"
                                Display="None" ValidationGroup="ReSendCommunication" SetFocusOnError="true" ErrorMessage="Please enter email address">
                            </asp:RequiredFieldValidator>
                </td>--%>

            </tr>
            <tr>
                <td>
                     <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                            Style="color: Red" ValidationGroup="ReSendCommunication" />

                </td>

            </tr>
            <tr>
                <td>
                    <div id="divReSendCommunication" runat="server">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>


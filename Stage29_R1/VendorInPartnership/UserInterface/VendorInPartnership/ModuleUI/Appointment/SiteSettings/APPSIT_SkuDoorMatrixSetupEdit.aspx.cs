﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using WebUtilities;
using BusinessEntities.ModuleBE.AdminFunctions;

public partial class APPSIT_SkuDoorMatrixSetupEdit : CommonPage
{
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetQueryStringValue("SKUDoorMatrixID") != null)
        {
            //bind from DB
        }
    }
    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            if (GetQueryStringValue("SKUDoorMatrixID") == null)
            {
                ddlSite.innerControlddlSite.SelectedIndex = 0;
                ddlSite.innerControlddlSite.AutoPostBack = true;
                
                if (GetQueryStringValue("SiteID") != null)
                {
                    hdnSiteID.Value = GetQueryStringValue("SiteID").ToString();
                    ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Convert.ToString(hdnSiteID.Value)));
                }

                BindDoorName();
            }
            else
            {
                GetODSkuNumber();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    protected void BindDoorName()
    {
        MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE = new MASSIT_DoorNoSetupBE();
        MASSIT_ODSKUDoorMatrixSetupBAL oMASSIT_ODSKUDoorMatrixSetupBAL = new MASSIT_ODSKUDoorMatrixSetupBAL();

        ddlDoorName.Items.Clear();
        oMASSIT_DoorNoSetupBE.Action = "GET_DOORNAME";

        if (ddlSite.innerControlddlSite != null && ddlSite.innerControlddlSite.SelectedItem != null)
        {
            oMASSIT_DoorNoSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        }
        List<MASSIT_DoorNoSetupBE> lstDoorName = oMASSIT_ODSKUDoorMatrixSetupBAL.GetDoorNameBAL(oMASSIT_DoorNoSetupBE);

        if (lstDoorName.Count > 0)
            FillControls.FillDropDown(ref ddlDoorName, lstDoorName.ToList(), "DoorNumber", "SiteDoorNumberID", "---Select---");
        else
        {
            ddlDoorName.Items.Insert(0, new ListItem("--" + "---Select---" + "--", "0"));
        }
    }

    protected void GetODSkuNumber()
    {
        MASSIT_ODSKUDoorMatrixSetupBE oMASSIT_ODSKUDoorMatrixSetupBE = new MASSIT_ODSKUDoorMatrixSetupBE();
        MASSIT_ODSKUDoorMatrixSetupBAL oMASSIT_ODSKUDoorMatrixSetupBAL = new MASSIT_ODSKUDoorMatrixSetupBAL();

        if (GetQueryStringValue("SKUDoorMatrixID") != null)
        {
            ddlSite.innerControlddlSite.Enabled = false;

            oMASSIT_ODSKUDoorMatrixSetupBE.Action = "SHOW_BY_ID";
            oMASSIT_ODSKUDoorMatrixSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_ODSKUDoorMatrixSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_ODSKUDoorMatrixSetupBE.SkuDoorMatrixID = Convert.ToInt32(GetQueryStringValue("SKUDoorMatrixID"));

            MASSIT_ODSKUDoorMatrixSetupBE lstDoorMatrixSetup = oMASSIT_ODSKUDoorMatrixSetupBAL.GetDoorNameByIDBAL(oMASSIT_ODSKUDoorMatrixSetupBE);
            ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Convert.ToString(lstDoorMatrixSetup.Site.SiteID)));
            BindDoorName();
            ddlDoorName.SelectedIndex = ddlDoorName.Items.IndexOf(ddlDoorName.Items.FindByValue(lstDoorMatrixSetup.DoorNumber.SiteDoorNumberID.ToString()));
            txtOdSkuNumber.Text = Convert.ToString(lstDoorMatrixSetup.ODSkuNumber);
        }
        else
        {
            btnDelete.Visible = false;
        }

    }

    #region Events

    protected void btnSave_Click(object sender, EventArgs e)
    {
        MASSIT_ODSKUDoorMatrixSetupBE oMASSIT_ODSKUDoorMatrixSetupBE = new MASSIT_ODSKUDoorMatrixSetupBE();
        MASSIT_ODSKUDoorMatrixSetupBAL oMASSIT_ODSKUDoorMatrixSetupBAL = new MASSIT_ODSKUDoorMatrixSetupBAL();

        int intSKUDoorMatrixID = 0;
        if (GetQueryStringValue("SKUDoorMatrixID") != null)
        {
            intSKUDoorMatrixID = Convert.ToInt32(GetQueryStringValue("SKUDoorMatrixID"));
            oMASSIT_ODSKUDoorMatrixSetupBE.Action = "Update";
        }
        else oMASSIT_ODSKUDoorMatrixSetupBE.Action = "Add";

        oMASSIT_ODSKUDoorMatrixSetupBE.SkuDoorMatrixID = intSKUDoorMatrixID;
        MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE = new MASSIT_DoorNoSetupBE();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMASSIT_DoorNoSetupBE.SiteDoorNumberID = Convert.ToInt32(ddlDoorName.SelectedItem.Value);
        oMASSIT_ODSKUDoorMatrixSetupBE.DoorNumber = oMASSIT_DoorNoSetupBE;
        oMAS_SiteBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_ODSKUDoorMatrixSetupBE.Site = oMAS_SiteBE;
        oMASSIT_ODSKUDoorMatrixSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_ODSKUDoorMatrixSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMASSIT_ODSKUDoorMatrixSetupBE.ODSkuNumber = txtOdSkuNumber.Text;

        var result = oMASSIT_ODSKUDoorMatrixSetupBAL.addEditDoorNoSetupDetailsBAL(oMASSIT_ODSKUDoorMatrixSetupBE);

        if (result < 0)
        {
            string errorMessage = WebCommon.getGlobalResourceValue("UniqueCombinationExist");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
        }
        else
            EncryptQueryString("APPSIT_SkuDoorMatrixSetupOverview.aspx");
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        MASSIT_ODSKUDoorMatrixSetupBE oMASSIT_ODSKUDoorMatrixSetupBE = new MASSIT_ODSKUDoorMatrixSetupBE();
        MASSIT_ODSKUDoorMatrixSetupBAL oMASSIT_ODSKUDoorMatrixSetupBAL = new MASSIT_ODSKUDoorMatrixSetupBAL();

        oMASSIT_ODSKUDoorMatrixSetupBE.Action = "DELETE";
        if (GetQueryStringValue("SKUDoorMatrixID") != null)
        {
            oMASSIT_ODSKUDoorMatrixSetupBE.SkuDoorMatrixID = Convert.ToInt32(GetQueryStringValue("SKUDoorMatrixID"));
            oMASSIT_ODSKUDoorMatrixSetupBAL.addEditDoorNoSetupDetailsBAL(oMASSIT_ODSKUDoorMatrixSetupBE);
        }
        EncryptQueryString("APPSIT_SkuDoorMatrixSetupOverview.aspx");
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPSIT_SkuDoorMatrixSetupOverview.aspx");
    }

    public override void SiteSelectedIndexChanged()
    {
        BindDoorName();
        GetODSkuNumber();
    }

    #endregion Events
}
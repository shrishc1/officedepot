﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_ReservedWindowSetupOverview.aspx.cs" Inherits="APPSIT_ReservedWindowSetupOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblReservedWindowSetup" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="25%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td style="font-weight: bold;">
                        <uc2:ucSite ID="ddlSite" runat="server" />
                    </td>
                </tr>
            </table>
            <cc1:ucPanel ID="pnlMaximum" runat="server" GroupingText="Maximum" CssClass="fieldset-form">
                <table width="60%" cellspacing="0" cellpadding="0" class="form-table" align="center">
                    <tr align="center">
                        <th align="left">
                            &nbsp;
                        </th>
                        <th>
                            &nbsp;
                        </th>
                        <th>
                            <cc1:ucLabel ID="lblMonday" runat="server" Text="Monday" />
                        </th>
                        <th>
                            <cc1:ucLabel ID="lblTuesday" runat="server" Text="Tuesday" />
                        </th>
                        <th>
                            <cc1:ucLabel ID="lblWednesday" runat="server" Text="Wednesday" />
                        </th>
                        <th>
                            <cc1:ucLabel ID="lblThursday" runat="server" Text="Thursday" />
                        </th>
                        <th>
                            <cc1:ucLabel ID="lblFriday" runat="server" Text="Friday" />
                        </th>
                        <th>
                            <cc1:ucLabel ID="lblSaturday" runat="server" Text="Saturday" />
                        </th>
                        <th>
                            <cc1:ucLabel ID="lblSunday" runat="server" Text="Sunday" />
                        </th>
                    </tr>
                    <tr align="center" style="background-color: #fff;">
                        <td style="font-weight: bold;" align="left">
                            <cc1:ucLabel ID="lblMaximumPallets" runat="server" Text="Maximum Pallets"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5px">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletMon" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletTue" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletWed" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletThurs" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletFri" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletSat" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletSun" runat="server"></cc1:ucLiteral>
                        </td>
                    </tr>
                    <tr align="center" style="background-color: #fff;">
                        <td style="font-weight: bold;" align="left">
                            <cc1:ucLabel ID="lblScheduledPallets" runat="server" Text="Scheduled Pallets"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5px">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletMon_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletTue_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletWed_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletThurs_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletFri_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletSat_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltpalletSun_1" runat="server"></cc1:ucLiteral>
                        </td>
                    </tr>
                    <tr align="center" style="background-color: #f7f6f3;">
                        <td style="font-weight: bold;" align="left">
                            <cc1:ucLabel ID="lblScheduledCartons" runat="server" Text="Scheduled Cartons"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5px">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsMon_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsTue_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsWed_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsThurs_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsFri_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsSat_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsSun_1" runat="server"></cc1:ucLiteral>
                        </td>
                    </tr>
                    <tr align="center" style="background-color: #fff;">
                        <td style="font-weight: bold;" align="left">
                            <cc1:ucLabel ID="lblMaximumLines" runat="server" Text="Maximum Lines"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5px">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesMon" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesTue" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesWed" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesThurs" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesFri" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesSat" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesSun" runat="server"></cc1:ucLiteral>
                        </td>
                    </tr>
                    <tr align="center" style="background-color: #fff;">
                        <td style="font-weight: bold;" align="left">
                            <cc1:ucLabel ID="lblScheduledLines" runat="server" Text="Schedule Lines"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5px">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesMon_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesTue_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesWed_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesThurs_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesFri_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesSat_1" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltLinesSun_1" runat="server"></cc1:ucLiteral>
                        </td>
                    </tr>
                    <%--<tr align="center" style="background-color:#fff;">
                        <td style="font-weight: bold;" align="left">
                            <cc1:ucLabel ID="lblMaximumCartons" runat="server" Text="Maximum Cartons"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5px">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsMon" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsTue" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsWed" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsThurs" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsFri" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsSat" runat="server"></cc1:ucLiteral>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltCartonsSun" runat="server"></cc1:ucLiteral>
                        </td>
                    </tr>--%>
                </table>
            </cc1:ucPanel>
            <div class="button-row">
                <cc2:ucAddButton ID="btnAdd" runat="server" />
                <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
            </div>
            <table width="100%">
                <tr>
                    <td align="center" colspan="9">
                        <cc1:ucGridView ID="grdFixedSlot" Width="100%" runat="server" CssClass="grid" OnRowDataBound="grdFixedSlot_RowDataBound"
                            AlternatingRowStyle-CssClass="altcolor" OnSorting="SortGrid" AllowSorting="true">
                            <Columns>
                                <asp:TemplateField HeaderText="Code" SortExpression="VendorName">
                                    <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hpLink" runat="server" Text='<%# Eval("VendorName") %>' NavigateUrl='<%# EncryptQuery("APPSIT_ReservedWindowSetupEdit.aspx?TimeWindowID=" + Eval("TimeWindowID") + "&SiteID=" + Eval("SiteID"))%>'></asp:HyperLink>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Schedule Type" DataField="ScheduleType" SortExpression="ScheduleType">
                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Start Time" SortExpression="StartTime">
                                    <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltTimeSlot" runat="server" Text='<%# Eval("StartTime")%>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="End Time" SortExpression="EndTime">
                                    <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltEndSlot" runat="server" Text='<%# Eval("EndTime")%>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Door Name" SortExpression="DoorNumber">
                                    <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucLiteral ID="ltDoorNo" runat="server" Text='<%# Eval("DoorNumber")%>'></cc1:ucLiteral>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="# Deliveries" DataField="MaxDeliveries" SortExpression="MaxDeliveries">
                                    <HeaderStyle HorizontalAlign="Center" Width="12%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="# Pallets" DataField="MaximumPallets" SortExpression="MaximumPallets">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="# Lines" DataField="MaximumLines" SortExpression="MaximumLines">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Mon">
                                    <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucCheckbox ID="Monday1" Enabled="false" runat="server" Visible='<%# Eval("Monday")%>'
                                            Checked='<%# Eval("Monday")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tue">
                                    <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucCheckbox ID="Tuesday1" Enabled="false" runat="server" Visible='<%# Eval("Tuesday")%>'
                                            Checked='<%# Eval("Tuesday")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Wed">
                                    <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucCheckbox ID="Wednessday1" Enabled="false" runat="server" Visible='<%# Eval("Wednesday")%>'
                                            Checked='<%# Eval("Wednesday")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Thur">
                                    <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucCheckbox ID="Thrusday1" Enabled="false" runat="server" Visible='<%# Eval("Thursday")%>'
                                            Checked='<%# Eval("Thursday")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fri">
                                    <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucCheckbox ID="Friday1" Enabled="false" runat="server" Visible='<%# Eval("Friday")%>'
                                            Checked='<%# Eval("Friday")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sat">
                                    <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucCheckbox ID="Saturday1" Enabled="false" runat="server" Visible='<%# Eval("Saturday")%>'
                                            Checked='<%# Eval("Saturday")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sun">
                                    <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <cc1:ucCheckbox ID="Sunday1" Enabled="false" runat="server" Visible='<%# Eval("Sunday")%>'
                                            Checked='<%# Eval("Sunday")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

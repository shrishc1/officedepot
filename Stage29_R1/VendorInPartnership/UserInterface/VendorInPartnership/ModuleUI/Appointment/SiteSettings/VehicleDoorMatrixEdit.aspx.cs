﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Linq;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using Utilities; using WebUtilities;

using System.Collections.Generic;

public partial class VehicleDoorMappingsSetupEdit : CommonPage {
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            ddlDoorType.SelectedIndex = 0;
           // ddlDoorType.AutoPostBack = true;
            ddlVehicleType.SelectedIndex = 0;
            ddlVehicleType.AutoPostBack = true;
            ddlPriority.SelectedIndex = 0;
            BindVehicleType();
            BindDoorType();
            GetVehicleDoorMatrix();
        }
    }
    #region Methods

    protected void BindVehicleType() {
        MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

        oMASSIT_VehicleTypeBE.Action = "ShowAll";
        oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        if (Session["SelectedSiteID"] != null)
            oMASSIT_VehicleTypeBE.SiteID = Convert.ToInt32(Session["SelectedSiteID"]);
        else
            oMASSIT_VehicleTypeBE.SiteID = Convert.ToInt32(Session["SiteID"]);
            
        List<MASSIT_VehicleTypeBE> lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsBAL(oMASSIT_VehicleTypeBE);

        if (lstVehicleType.Count > 0)
        {
            FillControls.FillDropDown(ref ddlVehicleType, lstVehicleType.ToList(), "VehicleType", "VehicleTypeID", "---Select---");
        }
        else
        {
            ListItem vehtype = new ListItem("---Select---", "0");
            ddlVehicleType.Items.Add(vehtype);
        }

    }

    protected void BindDoorType() {
        MASCNT_DoorTypeBE oMASCNT_DoorTypeBE = new MASCNT_DoorTypeBE();
        APPCNT_DoorTypeBAL oMASCNT_DoorTypeBAL = new APPCNT_DoorTypeBAL();
        oMASCNT_DoorTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_DoorTypeBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        oMASCNT_DoorTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMASCNT_DoorTypeBE.Vehicle = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE();
        List<MASCNT_DoorTypeBE> lstDoorType = null;
        if (GetQueryStringValue("VehicleTypeDoorTypeID") != null)
        {
            oMASCNT_DoorTypeBE.Action = "ShowAll";            
            lstDoorType = oMASCNT_DoorTypeBAL.GetDoorTypeDetailsBAL(oMASCNT_DoorTypeBE);
        }
        else
        {
            oMASCNT_DoorTypeBE.Action = "ShowDoorType";
            if (Convert.ToInt32(ddlVehicleType.SelectedItem.Value) != 0)
            {
                oMASCNT_DoorTypeBE.Vehicle.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedItem.Value);
                lstDoorType = oMASCNT_DoorTypeBAL.GetDoorTypeBAL(oMASCNT_DoorTypeBE);
            }
        }       

        if (lstDoorType!=null ){
            if (lstDoorType.Count > 0) {
            FillControls.FillDropDown(ref ddlDoorType, lstDoorType.ToList(), "DoorType", "DoorTypeID", "---Select---");
            }
        }
        else
        {
            ListItem doortype = new ListItem("---Select---", "0");
            ddlDoorType.Items.Add(doortype);
        }

    }

    protected void GetVehicleDoorMatrix() {
        MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();
        APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();

        if (GetQueryStringValue("VehicleTypeDoorTypeID") != null)
        {
            btnSave.Visible = true;
            ddlDoorType.Enabled = false;
            ddlVehicleType.Enabled = false;
            oMASSIT_VehicleDoorMatrixBE.Action = "ShowById";
            oMASSIT_VehicleDoorMatrixBE.VehicleTypeDoorTypeID = Convert.ToInt32(GetQueryStringValue("VehicleTypeDoorTypeID"));
            MASSIT_VehicleDoorMatrixBE lstVehicleDoorMatrix = oMASSIT_VehicleDoorMatrixBAL.GetVehicleDoorMatrixDetailsByIdBAL(oMASSIT_VehicleDoorMatrixBE);

            ddlVehicleType.SelectedIndex = ddlVehicleType.Items.IndexOf(ddlVehicleType.Items.FindByValue(lstVehicleDoorMatrix.VehicleTypeID.ToString()));

            ddlDoorType.SelectedIndex = ddlDoorType.Items.IndexOf(ddlDoorType.Items.FindByValue(lstVehicleDoorMatrix.DoorTypeID.ToString()));

            ddlPriority.SelectedIndex = ddlPriority.Items.IndexOf(ddlPriority.Items.FindByValue(lstVehicleDoorMatrix.Priority.ToString()));

        } else {
            btnDelete.Visible = false;
        }

    }

    #endregion

    #region Events


    protected void btnSave_Click(object sender, EventArgs e) {       

        MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();
        APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();
        ddlDoorType.Enabled = true;
        ddlVehicleType.Enabled = true;
        ddlPriority.Enabled = true;
        string errorMessage;
        if (ddlVehicleType.SelectedValue == "0") {
            errorMessage = WebCommon.getGlobalResourceValue("VehicleTypeRequired");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
            return;
        } else {
            oMASSIT_VehicleDoorMatrixBE.VehicleTypeID = Convert.ToInt32(ddlVehicleType.SelectedValue);
        }
        oMASSIT_VehicleDoorMatrixBE.Priority = Convert.ToInt32(ddlPriority.SelectedItem.Value);
        if (ddlDoorType.SelectedValue=="0") {
            errorMessage = WebCommon.getGlobalResourceValue("DoorTypeRequired");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
            return;
        } else {
            oMASSIT_VehicleDoorMatrixBE.DoorTypeID = Convert.ToInt32(ddlDoorType.SelectedValue);
        }
        oMASSIT_VehicleDoorMatrixBE.Action = "CheckExistance";
        if (!string.IsNullOrEmpty(GetQueryStringValue("VehicleTypeDoorTypeID")))
        {
            oMASSIT_VehicleDoorMatrixBE.VehicleTypeDoorTypeID = Convert.ToInt32(GetQueryStringValue("VehicleTypeDoorTypeID"));
        }
        List<MASSIT_VehicleDoorMatrixBE> lstMASSIT_VehicleDoorMatrixBE = oMASSIT_VehicleDoorMatrixBAL.GetVehicleDoorMatrixDetailsBAL(oMASSIT_VehicleDoorMatrixBE);

        if (lstMASSIT_VehicleDoorMatrixBE.Count > 0) {
            errorMessage = WebCommon.getGlobalResourceValue("VehicleDoorTypeExist");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
        } else {
            oMASSIT_VehicleDoorMatrixBE.Action = "Add";

            oMASSIT_VehicleDoorMatrixBAL.addEditVehicleDoorMatrixDetailsBAL(oMASSIT_VehicleDoorMatrixBE);

            EncryptQueryString("VehicleDoorMatrixOverview.aspx");
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e) {
        MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();
        APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();

        oMASSIT_VehicleDoorMatrixBE.Action = "Edit";

        if (GetQueryStringValue("VehicleTypeDoorTypeID") != null) {
            oMASSIT_VehicleDoorMatrixBE.VehicleTypeDoorTypeID = Convert.ToInt32(GetQueryStringValue("VehicleTypeDoorTypeID"));

            oMASSIT_VehicleDoorMatrixBAL.addEditVehicleDoorMatrixDetailsBAL(oMASSIT_VehicleDoorMatrixBE);
        }

        EncryptQueryString("VehicleDoorMatrixOverview.aspx");
    }
    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("VehicleDoorMatrixOverview.aspx");
    }
    #endregion
    protected void ddlVehicleType_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDoorType();
    }
}
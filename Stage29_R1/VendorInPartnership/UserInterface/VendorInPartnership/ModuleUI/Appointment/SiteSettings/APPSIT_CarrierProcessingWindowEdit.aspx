﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_CarrierProcessingWindowEdit.aspx.cs" Inherits="APPSIT_CarrierProcessingWindowEdit" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblCarrierProcessing" runat="server" Text="Carrier Processing Window"></cc1:ucLabel>
    </h2>
    <input id="hidPalletsUnloadTime" type="hidden" runat="server" value="" />
    <input id="hidCartonsUnloadTime" type="hidden" runat="server" value="" />
    <input id="hidLineReceiptingTime" type="hidden" runat="server" value="" />
    <script language="javascript" type="text/javascript">
        function checkCorrectTimeFormat(sender, args) {
            var objTotlaUnloadTime = document.getElementById('<%=txtUploadPallet.ClientID %>');
            var objTotalUnloadTimePerCarton = document.getElementById('<%=txtUploadCarton.ClientID %>');
            var objTotlaReceiptTime = document.getElementById('<%=txtReceiptTime.ClientID %>');


            if (objTotlaUnloadTime.value != '' || objTotlaReceiptTime.value != '' || objTotalUnloadTimePerCarton.value != '') {
                var arr = new Array();
                arr = objTotlaUnloadTime.value.split(':');
                if (arr.length == 2) {
                    if (arr[0] != '' && arr[1] != '') {
                        if (IsNumeric(arr[0]) && IsNumeric(arr[1])) {
                            if (parseInt(arr[0] * 1) >= 0 && parseInt(arr[1] * 1) >= 0 && parseInt(arr[1] * 1) < 60) {
                                args.IsValid = true;
                            }
                            else {
                                args.IsValid = false;
                            }
                        }
                        else {
                            args.IsValid = false;
                        }
                    }
                    else {
                        args.IsValid = false;
                    }
                }
                else {
                    args.IsValid = false;
                }
            }
        }
        
    function confirmDelete() {
        return confirm('<%=deleteMessage%>');
    }
    function getMinutes(obj, arg) {
        if (obj.value != '') {
            if (IsNumeric(obj.value)) {
                if (parseInt(obj.value * 1) >= 0) {
                    if (arg == '1') {
                        document.getElementById('<%=lblPalletsUnloadTime.ClientID %>').textContent = document.getElementById('<%=lblPalletsUnloadTime.ClientID %>').innerText = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2);
                        document.getElementById('<%=hidPalletsUnloadTime.ClientID %>').value = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2);
                    }
                    else if (arg == '2') {
                        document.getElementById('<%=lblCartonsUnloadTime.ClientID %>').textContent = document.getElementById('<%=lblCartonsUnloadTime.ClientID %>').innerText = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2);
                        document.getElementById('<%=hidCartonsUnloadTime.ClientID %>').value = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2);
                    }
                    else if (arg == '3') {
                        document.getElementById('<%=lblLineReceiptingLineTime.ClientID %>').textContent = document.getElementById('<%=lblLineReceiptingLineTime.ClientID %>').innerText = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2);
                        document.getElementById('<%=hidLineReceiptingTime.ClientID %>').value = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2);
                    }
                }
                else {
                    alert('Please enter the correct value.');
                    return false;
                }
            }
            else {
                alert('Please enter the correct value.');
                return false;
            }
        }
    }

    function SaveValidation() {
        if (getMinutes(document.getElementById('<%=txtUploadPallet.ClientID %>'), '1') == false) {
            return false;
        }

        if (getMinutes(document.getElementById('<%=txtUploadCarton.ClientID %>'), '2') == false) {
            return false;
        }

        if (getMinutes(document.getElementById('<%=txtReceiptTime.ClientID %>'), '3') == false) {
            return false;
        }

        return true;
    }

    $(document).ready(function () {
        getMinutes(document.getElementById('<%=txtUploadPallet.ClientID %>'), '1');
        getMinutes(document.getElementById('<%=txtUploadCarton.ClientID %>'), '2');
        getMinutes(document.getElementById('<%=txtReceiptTime.ClientID %>'), '3');
    });
</script>
        
   
    <div class="right-shadow">
        <div>
            <asp:RequiredFieldValidator ID="rfvUploadPallets" runat="server" ControlToValidate="txtUploadPallet"
                Display="None" ValidationGroup="check" SetFocusOnError="true">
            </asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvUploadCartons" runat="server" ControlToValidate="txtUploadCarton"
                Display="None" ValidationGroup="check" SetFocusOnError="true">
            </asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvReceiptTime" runat="server" ControlToValidate="txtReceiptTime"
                Display="None" ValidationGroup="check" SetFocusOnError="true">
            </asp:RequiredFieldValidator>

            <asp:RequiredFieldValidator ID="rfvCarrierAdvice" runat="server" ControlToValidate="ddlCarrier"
                Display="None" ValidationGroup="check" SetFocusOnError="true" InitialValue="0">
            </asp:RequiredFieldValidator>

           <%-- <asp:RegularExpressionValidator ID="revUploadTime" runat="server" ControlToValidate="txtUploadPallet"
                ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$" ValidationGroup="check"                
                Display="None" SetFocusOnError="true"></asp:RegularExpressionValidator>

            <asp:RegularExpressionValidator ID="revUploadTimeCarton" runat="server" ControlToValidate="txtUploadCarton"
                ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$" ValidationGroup="check"                
                Display="None" SetFocusOnError="true"></asp:RegularExpressionValidator>
                
                 
            <asp:RegularExpressionValidator ID="revReceiptTimeLine" runat="server" ControlToValidate="txtReceiptTime"
                ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$" ValidationGroup="check" 
                Display="None" SetFocusOnError="true"></asp:RegularExpressionValidator>--%>
        </div>
        <div class="formbox">
            <table width="70%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width:3%"></td>
                    <td style="font-weight: bold; width: 40%">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 3%">
                                              :
                    </td>
                    <td style="font-weight: bold; width: 54%">
                        <cc2:ucSite ID="ucSite" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                      :
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucDropdownList ID="ddlCarrier" runat="server" Width="150px">
                        </cc1:ucDropdownList>
                      <cc1:ucLabel ID="lblCarrierValue" runat="server" Visible="false"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblAverageUnloding" runat="server" Text="Average Pallets unload time"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                       :
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox ID="txtUploadPallet" onblur="javascript:return getMinutes(this,'1');" runat="server" Width="40px" ValidationGroup="check" MaxLength="5"></cc1:ucTextbox>
                       &nbsp;<cc1:ucLabel ID="lblPerHour_1" runat="server" Text="Pallets Per Hour"></cc1:ucLabel>
                            &nbsp;&nbsp;&nbsp; &lt;<cc1:ucLabel ID="lblPalletsUnloadTime" runat="server" Text="xx"></cc1:ucLabel>&gt; &nbsp;<cc1:ucLabel
                                ID="lblPerMin_1" runat="server" Text="Minutes Per Pallet"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblAverageCartonsUnloding" runat="server" Text="Average Cartons unload time"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                       :
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox ID="txtUploadCarton" onblur="javascript:return getMinutes(this,'2');" runat="server" Width="40px" ValidationGroup="check" MaxLength="5"></cc1:ucTextbox>
                        &nbsp;<cc1:ucLabel ID="lblPerHour_2" runat="server" Text="Cartons Per Hour"></cc1:ucLabel>
                            &nbsp;&nbsp;&nbsp; &lt;<cc1:ucLabel ID="lblCartonsUnloadTime" runat="server" Text="xx"></cc1:ucLabel>&gt; &nbsp;<cc1:ucLabel
                                ID="lblPerMin_2" runat="server" Text="Minutes Per Cartons"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblAverageReceipting" runat="server" Text="Average Line Receipting Time"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                       :
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox ID="txtReceiptTime" onblur="javascript:return getMinutes(this,'3');" runat="server" Width="40px" ValidationGroup="check" MaxLength="5"></cc1:ucTextbox>
                         &nbsp;<cc1:ucLabel ID="lblPerHour_3" runat="server" Text="Lines Per Hour"></cc1:ucLabel>
                            &nbsp;&nbsp;&nbsp; &lt;<cc1:ucLabel ID="lblLineReceiptingLineTime" runat="server" Text="xx"></cc1:ucLabel>&gt; &nbsp;<cc1:ucLabel
                                ID="lblPerMin_3" runat="server" Text="Minutes Per Line"></cc1:ucLabel>
                    </td>
                </tr>
            </table>
        </div>
    </div>
  
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <asp:ValidationSummary ID="validationSummary" runat="server" ShowSummary="false"   ShowMessageBox="true" ValidationGroup="check" />
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
             OnClientClick="return SaveValidation();" ValidationGroup="check" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click" OnClientClick="return confirmDelete();"  />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" 
            onclick="btnBack_Click" />
    </div>
</asp:Content>

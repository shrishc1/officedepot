﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_TimeWindowEdit.aspx.cs" Inherits="APPSIT_TimeWindowEdit" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhnSelectVendor.ascx" TagName="ucSeacrhnSelectVendor"
    TagPrefix="cc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">

        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
    </script>
    <asp:UpdatePanel ID="updTimeWindowEdit" runat="server">
        <contenttemplate>
            <h2>
                <cc1:ucLabel ID="lblTimeWindow" runat="server" Text="Time Window"></cc1:ucLabel><cc1:ucLabel
                    ID="lblSiteNameT" runat="server"></cc1:ucLabel>
            </h2>
            <asp:ValidationSummary ID="vsAdd" runat="server" ValidationGroup="a" DisplayMode="BulletList"
                ShowMessageBox="true" ShowSummary="false" />
            <div class="right-shadow">
                <div class="formbox">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 100%;">
                                <cc1:ucPanel ID="pnlWindowDetails" runat="server" GroupingText="Window Details" CssClass="fieldset-form">
                                    <table width="80%" cellspacing="5" cellpadding="0" class="form-table">
                                        <tr>
                                            <td style="font-weight: bold; width: 20%;">
                                                <cc1:ucLabel ID="lblWindow" runat="server" Text="Window" isRequired="true"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 2%;">
                                                :
                                            </td>
                                            <td valign="top" style="width: 28%">
                                                <cc1:ucTextbox ID="txtWindow" MaxLength="200" runat="server" Width="100px"></cc1:ucTextbox>
                                            </td>
                                            <td style="font-weight: bold; width: 20%;">
                                                <cc1:ucLabel ID="lblDoor" runat="server" Text="Door"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 2%;">
                                                :
                                            </td>
                                            <td valign="top" style="width: 28%">
                                                <cc1:ucLiteral ID="ltDoor" runat="server"></cc1:ucLiteral>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblDoorType" runat="server" Text="DoorType"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                :
                                            </td>
                                            <td valign="top">
                                                <cc1:ucLiteral ID="ltDoorType" runat="server"></cc1:ucLiteral>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblPriority" runat="server" Text="Door"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                :
                                            </td>
                                            <td valign="top">
                                                <cc1:ucDropdownList ID="ddlPriority" runat="server" Width="50px">
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                </cc1:ucDropdownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblSiteWeekSetupStart" runat="server" Text="Start" isRequired="true"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                :
                                            </td>
                                            <td valign="top">
                                                <cc1:ucDropdownList ID="ddlStartTime" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                                                &nbsp;
                                                <cc1:ucLabel ID="lblHHMM1" runat="server"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblSiteWeekSetupEnd" runat="server" Text="End" isRequired="true"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                :
                                            </td>
                                            <td valign="top">
                                                <cc1:ucDropdownList ID="ddlEndTime" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                                                &nbsp;
                                                <cc1:ucLabel ID="lblHHMM2" runat="server"></cc1:ucLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblMaximumPallets" runat="server" Text="Maximum Pallets" Style="font-weight: bold"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold">
                                                :
                                            </td>
                                            <td>
                                                <cc1:ucNumericTextbox ID="txtMaxPallets" MaxLength="4" runat="server" Width="47px" onkeypress="return IsNumberKey(event, this);" onkeyup="AllowNumbersOnly(this)"></cc1:ucNumericTextbox>
                                            </td>
                                            <td style="font-weight: bold">
                                                <cc1:ucLabel ID="lblMaximumCartons" runat="server" Text="Maximum Cartons"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold">
                                                :
                                            </td>
                                            <td>
                                                <cc1:ucNumericTextbox ID="txtMaxCartons" MaxLength="4" runat="server" Width="47px" onkeypress="return IsNumberKey(event, this);" onkeyup="AllowNumbersOnly(this)"></cc1:ucNumericTextbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblMaximumLines" runat="server" Text="Maximum Lines" Style="font-weight: bold"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold">
                                                :
                                            </td>
                                            <td>
                                                <cc1:ucNumericTextbox ID="txtMaxLines" MaxLength="4" runat="server" Width="47px" onkeypress="return IsNumberKey(event, this);" onkeyup="AllowNumbersOnly(this)"></cc1:ucNumericTextbox>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblMaximumDeliveries" runat="server" Text="Maximum Deliveries"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold">
                                                :
                                            </td>
                                            <td>
                                                <cc1:ucNumericTextbox ID="txtMaxDeliveries" MaxLength="4" runat="server" Width="47px" onkeypress="return IsNumberKey(event, this);" onkeyup="AllowNumbersOnly(this)"></cc1:ucNumericTextbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <cc1:ucLabel ID="lblTotalHrsMinsAvailable" runat="server" Text="Total Hrs:Mins Available"
                                                    Style="font-weight: bold"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold">
                                                :
                                            </td>
                                            <td>
                                                <cc1:ucTextbox ID="txtTotalTime" runat="server" Width="40px" MaxLength="5"></cc1:ucTextbox>
                                                <cc1:ucLabel ID="lblHHMM1_1" runat="server"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;" colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                                <cc1:ucPanel ID="pnlCriteria" runat="server" GroupingText="Criteria" CssClass="fieldset-form">
                                    <table width="90%" align="left" cellspacing="5" cellpadding="0" border="0">
                                        <tr>
                                            <td>
                                                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="left" class="top-settings">
                                                    <tr>
                                                        <td style="font-weight: bold; width: 20%">
                                                            <cc1:ucLabel ID="lblCheckSku" runat="server" Text="Check SKU table"></cc1:ucLabel>
                                                        </td>
                                                        <td style="font-weight: bold; width: 5%">
                                                            :
                                                        </td>
                                                        <td style="font-weight: bold; width: 75%">
                                                            <cc1:ucCheckbox ID="chkSKU" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td>
                                                <cc1:ucPanel ID="pnlVolume" runat="server" CssClass="fieldset-form">
                                                    <table width="70%" align="left" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                                                        <tr>
                                                            <td style="font-weight: bold;" width="25%">
                                                                <cc1:ucLabel ID="lblLessPallets" runat="server" Text="< Pallets" Style="font-weight: bold"></cc1:ucLabel>
                                                            </td>
                                                            <td style="font-weight: bold;" width="1%">
                                                                :
                                                            </td>
                                                            <td width="34%">
                                                                <cc1:ucNumericTextbox ID="txtlessthenpallets" runat="server" Width="47px" onkeyup="AllowNumbersOnly(this)"
                                                                    MaxLength="3" onkeypress="return IsNumberKey(event, this);"></cc1:ucNumericTextbox>
                                                            </td>
                                                            <td width="15%">
                                                                <cc1:ucLabel ID="lblGreaterPallets" runat="server" Text="> Pallets" Style="font-weight: bold"></cc1:ucLabel>
                                                            </td>
                                                            <td style="font-weight: bold" width="1%">
                                                                :
                                                            </td>
                                                            <td width="24%">
                                                                <cc1:ucNumericTextbox ID="txtGreaterthenpallets" runat="server" Width="47px" onkeyup="AllowNumbersOnly(this)"
                                                                    MaxLength="3" onkeypress="return IsNumberKey(event, this);"></cc1:ucNumericTextbox>
                                                            </td>
                                                        </tr>     
                                                        <tr>
                                                            <td style="font-weight: bold;" width="25%">
                                                                <cc1:ucLabel ID="lblLessLines" runat="server" Text="< Lines" Style="font-weight: bold"></cc1:ucLabel>
                                                            </td>
                                                            <td style="font-weight: bold;" width="1%">
                                                                :
                                                            </td>
                                                            <td width="34%">
                                                                <cc1:ucNumericTextbox ID="txtLessthanLines" runat="server" Width="47px" onkeyup="AllowNumbersOnly(this)"
                                                                    MaxLength="3" onkeypress="return IsNumberKey(event, this);"></cc1:ucNumericTextbox>
                                                            </td>
                                                            <td width="15%">
                                                                <cc1:ucLabel ID="lblGreaterLines" runat="server" Text="> Lines" Style="font-weight: bold"></cc1:ucLabel>
                                                            </td>
                                                            <td style="font-weight: bold" width="1%">
                                                                :
                                                            </td>
                                                            <td width="24%">
                                                                <cc1:ucNumericTextbox ID="txtGreaterthanLines" runat="server" Width="47px" onkeyup="AllowNumbersOnly(this)"
                                                                    MaxLength="3" onkeypress="return IsNumberKey(event, this);"></cc1:ucNumericTextbox>
                                                            </td>
                                                        </tr>                                                   
                                                        <tr>
                                                            <td style="font-weight: bold;" width="25%">
                                                                <cc1:ucLabel ID="lblMaximumTime" runat="server" Style="font-weight: bold"></cc1:ucLabel>
                                                            </td>
                                                            <td style="font-weight: bold;" width="1%">
                                                                :
                                                            </td>
                                                            <td width="34%" style="font-weight: bold;">
                                                                <cc1:ucTextbox ID="txtMaximumTime" runat="server" Width="40px" MaxLength="5"></cc1:ucTextbox>
                                                                <cc1:ucLabel ID="lblHHMM1_2" runat="server"></cc1:ucLabel>
                                                            </td>
                                                            <td width="15%">
                                                            </td>
                                                            <td style="font-weight: bold" width="1%">
                                                            </td>
                                                            <td width="24%">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </cc1:ucPanel>
                                            </td>
                                        </tr>
                                        <%--  <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="up1" runat="server">
                                                    <ContentTemplate>
                                                        <cc1:ucPanel ID="pnlExcludeVendor" GroupingText="Exclude Vendor" runat="server" CssClass="fieldset-form">
                                                            <table width="100%" align="left" cellspacing="5" cellpadding="0" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <cc4:ucSeacrhnSelectVendor ID="ucSeacrhVendor1" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </cc1:ucPanel>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                       <tr>
                                            <td>
                                                <asp:UpdatePanel ID="up2" runat="server">
                                                    <ContentTemplate>
                                                        <cc1:ucPanel ID="pnlExcludeCarrier" GroupingText="Exclude Carrier" runat="server"
                                                            CssClass="fieldset-form">
                                                            <table width="100%" align="left" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                                                                <tr>
                                                                    <td style="font-weight: bold;" width="10%">
                                                                        <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier"></cc1:ucLabel>
                                                                    </td>
                                                                    <td style="font-weight: bold;" width="1%">
                                                                        :
                                                                    </td>
                                                                    <td width="30%">
                                                                        <cc1:ucListBox ID="UclstCarrier" runat="server" Height="200px" Width="320px">
                                                                        </cc1:ucListBox>
                                                                    </td>
                                                                    <td valign="middle" align="center" width="10%">
                                                                        <div>
                                                                            <cc1:ucButton ID="btnMoveRightAll" runat="server" Text=">>" CssClass="button" Width="35px"
                                                                                OnClick="btnMoveRightAll_Click" /></div>
                                                                        &nbsp;
                                                                        <div>
                                                                            <cc1:ucButton ID="btnMoveRight" runat="server" Text=">" CssClass="button" Width="35px"
                                                                                OnClick="btnMoveRight_Click" /></div>
                                                                        &nbsp;
                                                                        <div>
                                                                            <cc1:ucButton ID="btnMoveLeft" runat="server" Text="<" CssClass="button" Width="35px"
                                                                                OnClick="btnMoveLeft_Click" /></div>
                                                                        &nbsp;
                                                                        <div>
                                                                            <cc1:ucButton ID="btnMoveLeftAll" runat="server" Text="<<" CssClass="button" Width="35px"
                                                                                OnClick="btnMoveLeftAll_Click" /></div>
                                                                    </td>
                                                                    <td width="49%">
                                                                        <cc1:ucListBox ID="UclstRightCarrier" runat="server" Height="200px" Width="320px">
                                                                        </cc1:ucListBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </cc1:ucPanel>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnMoveRightAll" EventName="Click" />
                                                        <asp:AsyncPostBackTrigger ControlID="btnMoveRight" EventName="Click" />
                                                        <asp:AsyncPostBackTrigger ControlID="btnMoveLeft" EventName="Click" />
                                                        <asp:AsyncPostBackTrigger ControlID="btnMoveLeftAll" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="up3" runat="server">
                                                    <ContentTemplate>
                                                        <cc1:ucPanel ID="pnlIncludeVendor" GroupingText="Include Vendor" runat="server" CssClass="fieldset-form">
                                                            <table width="100%" align="left" cellspacing="5" cellpadding="0" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <cc4:ucSeacrhnSelectVendor ID="ucIncludeVendor" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </cc1:ucPanel>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="up4" runat="server">
                                                    <ContentTemplate>
                                                        <cc1:ucPanel ID="pnlIncludeCarrier" GroupingText="Include Carrier" runat="server"
                                                            CssClass="fieldset-form">
                                                            <table width="100%" align="left" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                                                                <tr>
                                                                    <td style="font-weight: bold;" width="10%">
                                                                        <cc1:ucLabel ID="lblCarrier_1" runat="server" Text="Carrier"></cc1:ucLabel>
                                                                    </td>
                                                                    <td style="font-weight: bold;" width="1%">
                                                                        :
                                                                    </td>
                                                                    <td width="30%">
                                                                        <cc1:ucListBox ID="uclstIncludeCarrier" runat="server" Height="200px" Width="320px">
                                                                        </cc1:ucListBox>
                                                                    </td>
                                                                    <td valign="middle" align="center" width="10%">
                                                                        <div>
                                                                            <cc1:ucButton ID="btnIncludeMoveRightAll" runat="server" Text=">>" CssClass="button" Width="35px"
                                                                                OnClick="btnIncludeMoveRightAll_Click" /></div>
                                                                        &nbsp;
                                                                        <div>
                                                                            <cc1:ucButton ID="btnIncludeMoveRight" runat="server" Text=">" CssClass="button" Width="35px"
                                                                                OnClick="btnIncludeMoveRight_Click" /></div>
                                                                        &nbsp;
                                                                        <div>
                                                                            <cc1:ucButton ID="btnIncludeMoveLeft" runat="server" Text="<" CssClass="button" Width="35px"
                                                                                OnClick="btnIncludeMoveLeft_Click" /></div>
                                                                        &nbsp;
                                                                        <div>
                                                                            <cc1:ucButton ID="btnIncludeLeftAll" runat="server" Text="<<" CssClass="button" Width="35px"
                                                                                OnClick="btnIncludeMoveLeftAll_Click" /></div>
                                                                    </td>
                                                                    <td width="49%">
                                                                        <cc1:ucListBox ID="uclstIncludeRightCarrier" runat="server" Height="200px" Width="320px">
                                                                        </cc1:ucListBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </cc1:ucPanel>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnIncludeMoveRightAll" EventName="Click" />
                                                        <asp:AsyncPostBackTrigger ControlID="btnIncludeMoveRight" EventName="Click" />
                                                        <asp:AsyncPostBackTrigger ControlID="btnIncludeMoveLeft" EventName="Click" />
                                                        <asp:AsyncPostBackTrigger ControlID="btnIncludeLeftAll" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;">
                                <div class="bottom-shadow">
                                </div>
                                <div class="button-row">
                                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />                                    
                                    <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click" OnClientClick="return confirmDelete();" />
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </contenttemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="updNoShow" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnNoShows" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdNoShow" runat="server" TargetControlID="btnNoShows"
                PopupControlID="pnlbtnNoShow" BackgroundCssClass="modalBackground" BehaviorID="MsgNoShow"
                DropShadow="false" />
            <asp:Panel ID="pnlbtnNoShow" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblTimeWindowDel" runat="server" Text=""></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnCancel" runat="server" OnCommand="btnCancel_Click"
                                    CssClass="button" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <cc1:ucButton ID="btnOK" runat="server" OnCommand="btnOK_Click"
                                    CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnCancel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

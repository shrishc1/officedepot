﻿<%@ Page Language="C#" AutoEventWireup="true" 
CodeFile="APPSIT_DeliveriesConstraintsSpecific.aspx.cs" 
Inherits="APPSIT_DeliveriesConstraintsSpecific"
 MaintainScrollPositionOnPostback="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc3" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc4" %>
<%@ Register Src="~/CommonUI/UserControls/ucDate.ascx" TagName="ucDate" TagPrefix="cc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblDeliveriesConstraints" runat="server" Text="Deliveries Constraints"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnWeekDay" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <table width="65%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width:29%"></td>
                    <td style="font-weight: bold;width:5%">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;width:1%">:</td>
                    <td style="font-weight: bold;width:65%">                        
                        <cc1:ucLabel ID="SiteNameForDCSpecific" runat="server"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td style="width:29%"></td>
                    <td style="font-weight: bold;width:5%">
                        <cc1:ucLabel ID="lblDate" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;width:1%">:</td>
                    <td >
                        <cc5:ucDate ID="txtDate" runat="server" />
                        <cc1:ucButton ID="btnGo" runat="server" CssClass="button" Width="100px" 
                            onclick="btnGo_Click" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="4">
                        <div>
                            <cc1:ucButton ID="btnMonday" runat="server" Text="Monday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Mon" />
                            <cc1:ucButton ID="btnTuesday" runat="server" Text="Tuesday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Tue" />
                            <cc1:ucButton ID="btnWednesday" runat="server" Text="Wednesday" CssClass="button"
                                Width="80px" OnClick="btnWeekday_Click" CommandArgument="Wed" />
                            <cc1:ucButton ID="btnThursday" runat="server" Text="Thursday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Thu" />
                            <cc1:ucButton ID="btnFriday" runat="server" Text="Friday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Fri" />
                            <cc1:ucButton ID="btnSaturday" runat="server" Text="Saturday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Sat" />
                            <cc1:ucButton ID="btnSunday" runat="server" Text="Sunday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Sun" />
                        </div>
                    </td>
                </tr>
            </table>
            <br />
            <table width="65%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    
                    <td style="font-weight: bold; width: 23%">
                        <cc1:ucLabel ID="lblTotalLines" runat="server" Text="Total Number of Lines"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 35%">
                        <asp:Literal ID="ltTotalLines" runat="server"></asp:Literal>
                    </td>
                    <td style="font-weight: bold; width: 23%">
                        <cc1:ucLabel ID="lblTotalNumberofLifts" runat="server" Text="Total Number of Lifts"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 19%">
                        <asp:Literal ID="ltTotalLifts" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
            <cc1:ucPanel ID="ucpnlTimeWindow" runat="server" GroupingText="" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td align="center">
                            <cc1:ucGridView ID="GridView1" runat="server" AutoGenerateColumns="False" ShowFooter="True"
                                OnRowCommand="GridView1_RowCommand" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing"
                                OnRowUpdating="GridView1_RowUpdating" OnRowCancelingEdit="GridView1_RowCancelingEdit"
                                CssClass="grid" CellPadding="0" Width="100%" GridLines="None" 
                                onsorting="GridView1_Sorting1" AllowSorting="true">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="Time Slot" InsertVisible="False" SortExpression="TimeSlot">
                                        <EditItemTemplate>
                                            <asp:Label ID="TimeSlotEdit" runat="server" Text='<%# Eval("TimeSlot") %>' Width="120px"></asp:Label>
                                             <asp:Label ID="StartWeekday_1" runat="server" Text='<%# Bind("StartWeekday") %>' Width="120px"></asp:Label>
                                            <asp:HiddenField ID="hdnStartTime" runat="server" Value='<%# Eval("StartTime") %>' />
                                            <asp:HiddenField ID="hdnStartWeekday" runat="server" Value='<%# Eval("StartWeekday") %>' />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="TimeSlot" runat="server" Text='<%# Bind("TimeSlot") %>' Width="120px"></asp:Label>
                                            <asp:Label ID="StartWeekday_2" runat="server" Text='<%# Bind("StartWeekday") %>' Width="120px"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MaximumNoofDeliveries">
                                        <EditItemTemplate>
                                            <asp:TextBox onkeyup="AllowNumbersOnly(this);" Width="40px" ID="txtRestrictDeliveries"
                                                runat="server" Text='<%# Bind("RestrictnumberofDeliveries") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label Width="40px" ID="RestrictDeliveries" runat="server" Text='<%# Bind("RestrictnumberofDeliveries") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Maximum Lines">
                                        <EditItemTemplate>
                                            <asp:TextBox onkeyup="AllowNumbersOnly(this);" Width="40px" ID="txtMaximumLines" runat="server"
                                                Text='<%# Bind("MaximumNumberofLines") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label Width="40px" ID="MaximumLines" runat="server" Text='<%# Bind("MaximumNumberofLines") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                         <%-- Here Lifts will considered as Pallets, no Lift Considrations , treat it as Pallets --%>

                                    <asp:TemplateField HeaderText="Maximum Pallets">
                                        <EditItemTemplate>
                                            <asp:TextBox onkeyup="AllowNumbersOnly(this);" Width="40px" ID="txtMaximumLifts" runat="server"
                                                Text='<%# Bind("MaximumNumberofLifts") %>' ></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label Width="40px" ID="MaximumLifts" runat="server" Text='<%# Bind("MaximumNumberofLifts") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Restrictsingledeliverypalletscountnothan">
                                        <EditItemTemplate>
                                            <asp:TextBox onkeyup="AllowNumbersOnly(this);" Width="40px" ID="txtRestrictsingledeliverypallets"
                                                runat="server" Text='<%# Bind("Restrictsingledeliverypallets") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label Width="40px" ID="Restrictsingledeliverypallets" runat="server" Text='<%# Bind("Restrictsingledeliverypallets") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Restrictsingledeliverylinescountnothan">
                                        <EditItemTemplate>
                                            <asp:TextBox onkeyup="AllowNumbersOnly(this);" Width="40px" ID="txtRestrictsingledeliverylines"
                                                runat="server" Text='<%# Bind("Restrictsingledeliverylines") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label Width="40px" ID="Restrictsingledeliverylines" runat="server" Text='<%# Bind("Restrictsingledeliverylines") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:CommandField ShowEditButton="True" HeaderText="Action">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Width="10%" HorizontalAlign="Left" />
                                    </asp:CommandField>
                                </Columns>
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
</asp:Content>


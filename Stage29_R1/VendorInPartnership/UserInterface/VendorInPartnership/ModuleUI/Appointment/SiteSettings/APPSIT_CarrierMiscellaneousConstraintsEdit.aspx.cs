﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Linq;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;

public partial class APPSIT_CarrierMiscellaneousConstraintsEdit : CommonPage
{
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    string strExistCarrierMiscConst = WebCommon.getGlobalResourceValue("ExistCarrierMiscConst");
    string strSelectCarrier = WebCommon.getGlobalResourceValue("CarrierSendingGoodsBackRequired");
    string strCheckBeforeAfterTime = WebCommon.getGlobalResourceValue("CheckBeforeAfterTime");    

    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ////ucSeacrhVendor1.CurrentPage = this;
        ////ucSeacrhVendor1.IsParentRequired = true;
        ////ucSeacrhVendor1.IsStandAloneRequired = true;
        ////ucSeacrhVendor1.IsChildRequired = true;
        ////ucSeacrhVendor1.IsGrandParentRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlSite.innerControlddlSite.AutoPostBack = true;
            BindTimes();

            if (string.IsNullOrWhiteSpace(GetQueryStringValue("SiteCarrierID")))
            {
                BindCarriers();
                ddlCarrier.Enabled = true;
            }
            else
            {
                BindVendorMicellaneousConstraints();
                ddlCarrier.Enabled = false;
            }
        }
    }

    public void BindCarriers()
    {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();
        List<MASCNT_CarrierBE> lstCarrier = null;
        int intSiteCountryID = 0;

        if (!string.IsNullOrWhiteSpace(GetQueryStringValue("SiteCarrierID")) || ddlSite.CountryID == 0)
            intSiteCountryID = GetCountryId(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue));
        else
            intSiteCountryID = ddlSite.CountryID;

        oMASCNT_CarrierBE.Action = "ShowAll";
        oMASCNT_CarrierBE.CountryID = intSiteCountryID;
        lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);
        ddlCarrier.Items.Clear();
        if (lstCarrier.Count > 0)
            FillControls.FillDropDown(ref ddlCarrier, lstCarrier, "CarrierName", "CarrierID", "--Select--");

        if (string.IsNullOrWhiteSpace(GetQueryStringValue("SiteCarrierID")))
            if (ddlCarrier.Items.Count > 0) { ddlCarrier.SelectedIndex = 0; }

        if(ddlCarrier.Items.Count==0)
            ddlCarrier.Items.Insert(0, new ListItem("--" + "--Select--" + "--", "0"));
    }

    private int GetCountryId(int siteId)
    {
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        List<MAS_SiteBE> lstSite = new List<MAS_SiteBE>();
        int intSiteCountryID = 0;

        oMAS_SiteBE.Action = "ShowAll";
        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMAS_SiteBE.SiteCountryID = 0;
        lstSite = oMAS_SiteBAL.GetSiteBAL(oMAS_SiteBE);

        MAS_SiteBE oSiteBE = lstSite.Find(s => s.SiteID == siteId);
        if (oSiteBE != null)
        {
            intSiteCountryID = Convert.ToInt32(oSiteBE.SiteCountryID);
        }
        return intSiteCountryID;
    }

    public void BindTimes()
    {
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstTimes = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);

        if (lstTimes != null && lstTimes.Count > 0)
        {
            FillControls.FillDropDown(ref ddlSlotAfterTime, lstTimes, "SlotTime", "SlotTimeID");
            ddlSlotAfterTime.Items.Insert(0, new ListItem("Select", "-1"));

            // remove 00:00 from the list for before time
            var v = from lst in lstTimes where (lst.SlotTimeID == 1) select lst;
            if (v != null && v.ToList()[0] != null)
                lstTimes.Remove(v.ToList()[0]);

            FillControls.FillDropDown(ref ddlSlotBeforeTime, lstTimes, "SlotTime", "SlotTimeID");
            ddlSlotBeforeTime.Items.Insert(0, new ListItem("Select", "-1"));
        }
    }

    private void BindVendorMicellaneousConstraints()
    {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oAPPCNT_CarrierBAL = new APPCNT_CarrierBAL();

        if (GetQueryStringValue("SiteCarrierID") != null)
        {
            oMASCNT_CarrierBE.Action = "GetAllCarrierMiscCons";
            oMASCNT_CarrierBE.SiteCarrierID = Convert.ToInt32(GetQueryStringValue("SiteCarrierID"));
            List<MASCNT_CarrierBE> lstCarrierBE = oAPPCNT_CarrierBAL.GetAllCarrierMiscConsBAL(oMASCNT_CarrierBE);
            if (lstCarrierBE != null)
            {
                ddlSite.CurrentPage = this;
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(lstCarrierBE[0].SiteID.ToString()));
                BindCarriers();
                ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByText(lstCarrierBE[0].CarrierName.ToString()));

                //ddlCarrier.Items.FindByValue(lstCarrierBE[0].CarrierID.ToString()).Selected = true;
                hdnSelectedVendorID.Value = lstCarrierBE[0].CarrierID.ToString();
                chkCarrierCheckingRestriction.Checked = Convert.ToBoolean(lstCarrierBE[0].APP_CheckingRequired);
                txtMaximumNumberofPallets.Text = lstCarrierBE[0].APP_MaximumPallets.ToString();
                txtMaximumNumberofLines.Text = lstCarrierBE[0].APP_MaximumLines.ToString();
                txtMaximumNumberDeliveriesPerDay.Text = lstCarrierBE[0].APP_MaximumDeliveriesInADay.ToString();
                chkMonday1.Checked = Convert.ToBoolean(lstCarrierBE[0].APP_CannotDeliveryOnMonday);
                chkTuesday1.Checked = Convert.ToBoolean(lstCarrierBE[0].APP_CannotDeliveryOnTuesday);
                chkWednesday1.Checked = Convert.ToBoolean(lstCarrierBE[0].APP_CannotDeliveryOnWednessday);
                chkThrusday1.Checked = Convert.ToBoolean(lstCarrierBE[0].APP_CannotDeliveryOnThrusday);
                chkFriday1.Checked = Convert.ToBoolean(lstCarrierBE[0].APP_CannotDeliveryOnFriday);
                chkSaturday1.Checked = Convert.ToBoolean(lstCarrierBE[0].APP_CannotDeliveryOnSaturday);
                chkSunday1.Checked = Convert.ToBoolean(lstCarrierBE[0].APP_CannotDeliveryOnSunday);

                //ddlSlotBeforeTime.Items.FindByValue(lstCarrierBE[0].BeforeSlotTimeID.ToString()).Selected = true;
                //ddlSlotAfterTime.Items.FindByValue(lstCarrierBE[0].AfterSlotTimeID.ToString()).Selected = true;

                if (lstCarrierBE[0].BeforeSlotTimeID != null && lstCarrierBE[0].BeforeSlotTimeID.ToString() != string.Empty)
                    ddlSlotBeforeTime.SelectedIndex = ddlSlotBeforeTime.Items.IndexOf(ddlSlotBeforeTime.Items.FindByValue(lstCarrierBE[0].BeforeSlotTimeID.ToString()));
                //txtBefore.Text = lstVendor[0].APP_CannotDeliverBefore.Value.Hour.ToString() + ":" + lstVendor[0].APP_CannotDeliverBefore.Value.Minute.ToString();
                if (lstCarrierBE[0].AfterSlotTimeID != null && lstCarrierBE[0].AfterSlotTimeID.ToString() != string.Empty)
                    ddlSlotAfterTime.SelectedIndex = ddlSlotAfterTime.Items.IndexOf(ddlSlotAfterTime.Items.FindByValue(lstCarrierBE[0].AfterSlotTimeID.ToString()));
                //txtAfter.Text = lstVendor[0].APP_CannotDeliverAfter.Value.Hour.ToString() + ":" + lstVendor[0].APP_CannotDeliverAfter.Value.Minute.ToString();
            }
            ddlSite.innerControlddlSite.Enabled = false;
        }
        else
        {
            btnDelete.Visible = false;
        }
    }

    private bool IsCarrierMiscConsExist(int siteID, int carrierID, int siteCarrierID)
    {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oAPPCNT_CarrierBAL = new APPCNT_CarrierBAL();
        oMASCNT_CarrierBE.Action = "IsCarrierMiscConsExist";
        oMASCNT_CarrierBE.SiteID = siteID;
        oMASCNT_CarrierBE.CarrierID = carrierID;
        oMASCNT_CarrierBE.SiteCarrierID = siteCarrierID;
        if (oAPPCNT_CarrierBAL.IsCarrierMiscConsExistBAL(oMASCNT_CarrierBE) == 0) { return true; }
        else { return false; }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (this.Page.IsValid)
        {
            if (ddlCarrier.SelectedIndex>0 || GetQueryStringValue("SiteCarrierID") != null)
            {
                int intSiteCarrierID = 0;
                if (!string.IsNullOrWhiteSpace(GetQueryStringValue("SiteCarrierID")))
                {
                    intSiteCarrierID = Convert.ToInt32(GetQueryStringValue("SiteCarrierID").ToString());
                }

                #region Validating the Before - After time 
                DateTime dtmBefore = DateTime.Now;
                DateTime dtmAftre = DateTime.Now;
                bool blnDateStatus = false;
                if (ddlSlotBeforeTime.SelectedIndex > 0 && ddlSlotAfterTime.SelectedIndex > 0)
                {
                    dtmBefore = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlSlotBeforeTime.SelectedItem));
                    dtmAftre = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlSlotAfterTime.SelectedItem));
                    blnDateStatus = true;
                }
                #endregion

                if (ddlCarrier.SelectedIndex == 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strSelectCarrier + "');</script>");
                    return;
                }
                else if (blnDateStatus == true && (dtmBefore > dtmAftre || dtmBefore == dtmAftre))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCheckBeforeAfterTime + "');</script>");
                    return;
                }
                else
                {                

                    if (this.IsCarrierMiscConsExist(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value), Convert.ToInt32(ddlCarrier.SelectedValue), intSiteCarrierID))
                    {
                        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
                        APPCNT_CarrierBAL oAPPCNT_CarrierBAL = new APPCNT_CarrierBAL();
                        DateTime? dtAfter = (DateTime?)null;
                        DateTime? dtBefore = (DateTime?)null;

                        string[] afterTime = ddlSlotAfterTime.SelectedItem.Text.Split(':');
                        if (afterTime.Length > 1)
                            dtAfter = new DateTime(1900, 1, 1, Convert.ToInt32(afterTime[0]), Convert.ToInt32(afterTime[1]), 0);

                        string[] beforeTime = ddlSlotBeforeTime.SelectedItem.Text.Split(':');
                        if (beforeTime.Length > 1)
                            dtBefore = new DateTime(1900, 1, 1, Convert.ToInt32(beforeTime[0]), Convert.ToInt32(beforeTime[1]), 0);

                        oMASCNT_CarrierBE.Action = "AddCarrierMiscCons";

                        string errorDay, errorBeforeTime, errorAfterTime, errorMaxDeliveriesDay = string.Empty;
                        int? errorMaxPallates, errorMaxLines;
                        if (checkFixedSlotDetails(ddlCarrier.SelectedValue, out errorDay, out errorBeforeTime, out errorAfterTime, out errorMaxPallates, out errorMaxLines, out errorMaxDeliveriesDay))
                        {
                            if (GetQueryStringValue("SiteCarrierID") != null)
                            {
                                oMASCNT_CarrierBE.SiteCarrierID = Convert.ToInt32(GetQueryStringValue("SiteCarrierID").ToString());
                                oMASCNT_CarrierBE.Action = "UpdateCarrierMiscCons";
                            }

                            oMASCNT_CarrierBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                            oMASCNT_CarrierBE.CarrierID = Convert.ToInt32(ddlCarrier.SelectedValue);
                            oMASCNT_CarrierBE.APP_CheckingRequired = chkCarrierCheckingRestriction.Checked;
                            oMASCNT_CarrierBE.APP_MaximumPallets = txtMaximumNumberofPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaximumNumberofPallets.Text) : (int?)null;
                            oMASCNT_CarrierBE.APP_MaximumLines = txtMaximumNumberofLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaximumNumberofLines.Text) : (int?)null;
                            oMASCNT_CarrierBE.APP_MaximumDeliveriesInADay = txtMaximumNumberDeliveriesPerDay.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaximumNumberDeliveriesPerDay.Text) : (int?)null;
                            oMASCNT_CarrierBE.APP_CannotDeliveryOnMonday = chkMonday1.Checked;
                            oMASCNT_CarrierBE.APP_CannotDeliveryOnTuesday = chkTuesday1.Checked;
                            oMASCNT_CarrierBE.APP_CannotDeliveryOnWednessday = chkWednesday1.Checked;
                            oMASCNT_CarrierBE.APP_CannotDeliveryOnThrusday = chkThrusday1.Checked;
                            oMASCNT_CarrierBE.APP_CannotDeliveryOnFriday = chkFriday1.Checked;
                            oMASCNT_CarrierBE.APP_CannotDeliveryOnSaturday = chkSaturday1.Checked;
                            oMASCNT_CarrierBE.APP_CannotDeliveryOnSunday = chkSunday1.Checked;
                            //oMASCNT_CarrierBE.APP_CannotDeliverAfter = dtAfter;
                            //oMASCNT_CarrierBE.APP_CannotDeliverBefore = dtBefore;

                            if (ddlSlotAfterTime.SelectedItem != null && ddlSlotAfterTime.SelectedItem.Value != "-1")
                                oMASCNT_CarrierBE.AfterSlotTimeID = Convert.ToInt32(ddlSlotAfterTime.SelectedItem.Value);
                            else
                                oMASCNT_CarrierBE.AfterSlotTimeID = null;

                            if (ddlSlotBeforeTime.SelectedItem != null && ddlSlotBeforeTime.SelectedItem.Value != "-1")
                                oMASCNT_CarrierBE.BeforeSlotTimeID = Convert.ToInt32(ddlSlotBeforeTime.SelectedItem.Value);
                            else
                                oMASCNT_CarrierBE.BeforeSlotTimeID = null;

                            int? intresult = oAPPCNT_CarrierBAL.addEditCarrierMiscConsBAL(oMASCNT_CarrierBE);
                            if (intresult == -1)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert3", "alert('" + strExistCarrierMiscConst + "')", true);
                                return;
                            }

                            EncryptQueryString("APPSIT_CarrierMiscellaneousConstraintsOverview.aspx");
                        }
                        else
                        {
                            string errorMessage = string.Empty;
                            if (!string.IsNullOrEmpty(errorDay))
                                errorMessage = "Fixed Slot Details not matched with Day Constraint for day - " + errorDay;
                            else if (!string.IsNullOrEmpty(errorBeforeTime))
                                errorMessage = "Fixed Slot Details not matched with before time constraint";
                            else if (!string.IsNullOrEmpty(errorAfterTime))
                                errorMessage = "Fixed Slot Details not matched with after time constraint";
                            else if (!string.IsNullOrEmpty(errorMaxDeliveriesDay))
                                errorMessage = "Fixed Slot Details not matched with Maximum Number Deliveries per day constraint ";
                            else if (errorMaxPallates != null && errorMaxPallates > 0)
                                errorMessage = "Fixed Slot Details not matched with Maximum Pallets constraint ";
                            else if (errorMaxLines != null && errorMaxLines > 0)
                                errorMessage = "Fixed Slot Details not matched with Maximum Lines constraint ";

                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + errorMessage + "');</script>");
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert3", "alert('" + strExistCarrierMiscConst + "')", true);
                    }
                }
            }
            else
            {
                //string errorMeesage = WebCommon.getGlobalResourceValue("VendorRequired");
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert3", "alert('" + errorMeesage + "')", true);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strSelectCarrier + "');</script>");
                return;
            }
        }
    }

    private bool checkFixedSlotDetails(string vendorID, out string errorDay, out string errorBeforeTime, 
        out string errorAfterTime, out int? errorMaxPallates, out int? errorMaxLines, out string errorMaxDeliveriesDay)
    {
        bool retValue = true;
        List<MASSIT_FixedSlotBE> lstFixedSlot = GetAllFixedSlotByCarrierId(vendorID);
        errorDay = errorBeforeTime = errorAfterTime = errorMaxDeliveriesDay = "";
        errorMaxPallates = errorMaxLines = null;
        int? errorMaxDeliveries;

        if (lstFixedSlot != null && lstFixedSlot.Count > 0)
        {
            for (int i = 0; i < lstFixedSlot.Count; i++)
            {
                if (chkMonday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Monday)) { retValue = false; errorDay = "Monday"; break; }
                else if (chkTuesday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Tuesday)) { retValue = false; errorDay = "Tuesday"; break; }
                else if (chkWednesday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Wednesday)) { retValue = false; errorDay = "Wednesday"; break; }
                else if (chkThrusday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Thursday)) { retValue = false; errorDay = "Thursday"; break; }
                else if (chkFriday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Friday)) { retValue = false; errorDay = "Friday"; break; }
                else if (chkSaturday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Saturday)) { retValue = false; errorDay = "Saturday"; break; }
                else if (chkSunday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Sunday)) { retValue = false; errorDay = "Sunday"; break; }

                /*This condition commented due to Anthony 26 Sep email, File Name-[DEV Retests 250913] and Sheet No-[6]*/
                //if (lstFixedSlot[i].SlotTime != null && !string.IsNullOrEmpty(lstFixedSlot[i].SlotTime.SlotTime))
                //    if ((ddlSlotBeforeTime.SelectedItem.Value != "-1") && (Convert.ToDateTime(lstFixedSlot[i].SlotTime.SlotTime) < Convert.ToDateTime(Convert.ToDateTime(lstFixedSlot[i].SlotTime.SlotTime).ToShortDateString() + " " + ddlSlotBeforeTime.SelectedItem.Text))) { retValue = false; errorBeforeTime = ddlSlotBeforeTime.SelectedItem.Text; break; }

                
                //if (lstFixedSlot[i].SlotTime != null && !string.IsNullOrEmpty(lstFixedSlot[i].SlotTime.SlotTime))
                //    if ((ddlSlotAfterTime.SelectedItem.Value != "-1") && (Convert.ToDateTime(lstFixedSlot[i].SlotTime.SlotTime) > Convert.ToDateTime(Convert.ToDateTime(lstFixedSlot[i].SlotTime.SlotTime).ToShortDateString() + " " + ddlSlotAfterTime.SelectedItem.Text))) { retValue = false; errorAfterTime = ddlSlotAfterTime.SelectedItem.Text; break; }
                /*---------------------------------------------*/

                if (lstFixedSlot[i] != null && lstFixedSlot[i].MaximumPallets != null && lstFixedSlot[i].MaximumPallets != 0 && !string.IsNullOrEmpty(txtMaximumNumberofPallets.Text) && Convert.ToInt16(txtMaximumNumberofPallets.Text) > 0)
                {
                    if (lstFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.MaximumPallets.Value > Convert.ToInt16(txtMaximumNumberofPallets.Text); }).Count > 1) { retValue = false; errorMaxPallates = Convert.ToInt16(txtMaximumNumberofPallets.Text); break; }
                    //if (lstFixedSlot[i].MaximumPallets.Value < Convert.ToInt16(txtMaximumNumberofPallets.Text)) { retValue = false; errorMaxPallates = lstFixedSlot[i].MaximumPallets.Value; break; }
                }
                if (lstFixedSlot[i] != null && lstFixedSlot[i].MaximumLines != null && lstFixedSlot[i].MaximumLines != 0 && !string.IsNullOrEmpty(txtMaximumNumberofLines.Text) && Convert.ToInt16(txtMaximumNumberofLines.Text) > 0)
                {
                    if (lstFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.MaximumLines.Value > Convert.ToInt16(txtMaximumNumberofLines.Text); }).Count > 1) { retValue = false; errorMaxLines = Convert.ToInt16(txtMaximumNumberofLines.Text); break; }
                    // if (lstFixedSlot[i].MaximumLines < Convert.ToInt16(txtMaximumNumberofLines.Text)) { retValue = false; errorMaxLines = lstFixedSlot[i].MaximumLines.Value; break; }
                }
                if (lstFixedSlot[i] != null && !string.IsNullOrEmpty(txtMaximumNumberDeliveriesPerDay.Text))
                {
                    /* Changed by Jai for [Fixed Slot is stopping us from editing a carrier miscellaneous constraint record] 29/May/2013 */
                    if (Convert.ToInt32(txtMaximumNumberDeliveriesPerDay.Text) > 0)
                    {
                        errorMaxDeliveries = Convert.ToInt32(txtMaximumNumberDeliveriesPerDay.Text);
                        if (lstFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Monday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Monday"; break; }
                        else if (lstFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Tuesday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Tuesday"; break; }
                        else if (lstFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Wednesday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Wednesday"; break; }
                        else if (lstFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Thursday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Thursday"; break; }
                        else if (lstFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Friday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Friday"; break; }
                        else if (lstFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Saturday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Saturday"; break; }
                        else if (lstFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Sunday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Sunday"; break; }
                    }
                }
            }
        }
        return retValue;
    }

    private List<MASSIT_FixedSlotBE> GetAllFixedSlotByCarrierId(string carrierID)
    {
        List<MASSIT_FixedSlotBE> lstFixedSlot = null;
        if (!string.IsNullOrEmpty(carrierID))
        {
            MASSIT_FixedSlotBE oMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();
            APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

            oMASSIT_FixedSlotBE.FixedSlotID = Convert.ToInt32(GetQueryStringValue("FixedSlotID"));
            oMASSIT_FixedSlotBE.Action = "GetFixedSlotDetailsForCarrier";
            oMASSIT_FixedSlotBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_FixedSlotBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_FixedSlotBE.CarrierID = Convert.ToInt32(carrierID);
            oMASSIT_FixedSlotBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            lstFixedSlot = oMASCNT_CarrierBAL.GetAllFixedSlotByCarrierIDBAL(oMASSIT_FixedSlotBE);
        }
        return lstFixedSlot;
    }
        
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPSIT_CarrierMiscellaneousConstraintsOverview.aspx");
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("SiteCarrierID") != null)
        {
            MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
            APPCNT_CarrierBAL oAPPCNT_CarrierBAL = new APPCNT_CarrierBAL();
            oMASCNT_CarrierBE.Action = "DeleteCarrierMiscCons";
            oMASCNT_CarrierBE.SiteCarrierID = Convert.ToInt32(GetQueryStringValue("SiteCarrierID"));
            oAPPCNT_CarrierBAL.DeleteCarrierMiscConsBAL(oMASCNT_CarrierBE);
        }
        EncryptQueryString("APPSIT_CarrierMiscellaneousConstraintsOverview.aspx");
    }

    public override void SiteSelectedIndexChanged()
    {
        base.SiteSelectedIndexChanged();
        this.BindCarriers();
    }
}
﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;

public partial class APPSIT_SpecificWeekSetupOverview : CommonPage {

    #region Events

    //public override void SitePost_Load() {
    //    base.SitePost_Load();
    //    if (!IsPostBack) {
    //        //ddlSite.innerControlddlSite.AutoPostBack = true;                
    //    }
    //}

    protected void btnCreateSpecificWeekSetup_Click(object sender, EventArgs e) {
        string errorMeesage = string.Empty;
        MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE = new MASSIT_SpecificWeekSetupBE();
        APPSIT_SpecificWeekSetupBAL oMASSIT_SpecificWeekSetupBAL = new APPSIT_SpecificWeekSetupBAL();

        if (txtDate.innerControltxtDate.Value != string.Empty) {
            DateTime dtSelectedDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);
            if (dtSelectedDate.DayOfWeek == DayOfWeek.Monday) {
                if (CheckWeekExistance()) {
                    //Week setup for the selected date is already created. Show it.               
                    ClearControls();
                    BindSpecificSiteWeekSetups();
                }
                else {
                    //copy the generic weeksetup records for the selected site and date in sepecific table
                    oMASSIT_SpecificWeekSetupBE.Action = "CreateSpecificWeek";
                    oMASSIT_SpecificWeekSetupBE.WeekStartDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);
                    oMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
                    oMASSIT_SpecificWeekSetupBAL.addEditSpecificWeekSetupBAL(oMASSIT_SpecificWeekSetupBE);

                    ClearControls();
                    BindSpecificSiteWeekSetups();
                }
            }
            else if (dtSelectedDate.DayOfWeek != DayOfWeek.Monday) {

                //Please select a date on which, day of the week is monday.
                errorMeesage = WebCommon.getGlobalResourceValue("SelectMondayWeekSetup");
                ClearControls();
                txtDate.innerControltxtDate.Value = string.Empty;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                return;

            }
            else {
                // Create the week setup for the selected date and site                
                //insert a record with the start date selected in the MASSIT_WeekSetupSpecific table
                oMASSIT_SpecificWeekSetupBE.Action = "InsertWeekSpecificRecord";
                oMASSIT_SpecificWeekSetupBE.WeekStartDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);
                oMASSIT_SpecificWeekSetupBAL.addEditSpecificWeekSetupBAL(oMASSIT_SpecificWeekSetupBE);

                ClearControls();
                BindSpecificSiteWeekSetups();
            }
        }
        else {
            //Week setup date required.
            errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupDateRequired");
            ClearControls();
            txtDate.innerControltxtDate.Value = string.Empty;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert4", "alert('" + errorMeesage + "')", true);
            return;
        }
    }

    protected void btnGo_Click(object sender, EventArgs e) {
        string errorMeesage = string.Empty;

        if (txtDate.innerControltxtDate.Value != string.Empty) {
            DateTime dtSelectedDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);
            if (dtSelectedDate.DayOfWeek == DayOfWeek.Monday) {
                if (CheckWeekExistance()) {
                    //show the week setup for the selected date and site
                    BindSpecificSiteWeekSetups();
                }
                else {
                    //Please first create a new week setup for the selected date and site.                    
                    errorMeesage = WebCommon.getGlobalResourceValue("CreateWeekSetup");
                    txtDate.innerControltxtDate.Value = string.Empty;
                    ClearControls();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
                    return;
                }
            }
            else {
                //Please select a date on which, day of the week is monday.
                errorMeesage = WebCommon.getGlobalResourceValue("SelectMondayWeekSetup");
                txtDate.innerControltxtDate.Value = string.Empty;
                ClearControls();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                //return;
            }
        }
        else {
            //Week setup date required.
            errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupDateRequired");
            txtDate.innerControltxtDate.Value = string.Empty;
            ClearControls();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert4", "alert('" + errorMeesage + "')", true);
            return;
        }
    }

    protected void Page_InIt(object sender, EventArgs e) {
        //ddlSite.CurrentPage = this;        
    }

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            ClearControls();
            if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("SiteID").ToString() != "") {
                SiteFromWeekSetup.Text = GetSiteName(Convert.ToInt32(GetQueryStringValue("SiteID")));
            }

            if (GetQueryStringValue("WeekStartDate") != null && GetQueryStringValue("WeekStartDate").ToString() != "") {
                txtDate.innerControltxtDate.Value = GetQueryStringValue("WeekStartDate");
                btnCreateSpecificWeekSetup_Click(null, null);
            }

        }

        ucExportToExcel.GridViewControl = UcGridView1;
        ucExportToExcel.FileName = "SpecificWeekSetupOverview";
    }

    protected void btnReleaseWeek_Click(object sender, EventArgs e) {

        string errorMeesage = string.Empty;
        if (txtDate.innerControltxtDate.Value != string.Empty) {
            DateTime dtSelectedDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);
            if (dtSelectedDate.DayOfWeek != DayOfWeek.Monday) {

                //Please select a date on which, day of the week is monday.
                errorMeesage = WebCommon.getGlobalResourceValue("SelectMondayWeekSetup");
                ClearControls();
                txtDate.innerControltxtDate.Value = string.Empty;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                return;

            }
        }
        else {
            //Week setup date required.
            errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupDateRequired");
            ClearControls();
            txtDate.innerControltxtDate.Value = string.Empty;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert4", "alert('" + errorMeesage + "')", true);
            return;
        }

        MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE = new MASSIT_SpecificWeekSetupBE();
        APPSIT_SpecificWeekSetupBAL oMASSIT_SpecificWeekSetupBAL = new APPSIT_SpecificWeekSetupBAL();
        oMASSIT_SpecificWeekSetupBE.WeekStartDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);        
        oMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
        oMASSIT_SpecificWeekSetupBE.Action = "ReleaseWeekSetup";
        int? iResult = oMASSIT_SpecificWeekSetupBAL.addEditSpecificWeekSetupBAL(oMASSIT_SpecificWeekSetupBE);
        ClearControls();
        txtDate.innerControltxtDate.Value = string.Empty;
    }

    #endregion
    

    #region Methods

    public string GetSiteName(int pSiteID) {
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        oMAS_SiteBE.Action = "ShowAll";

        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMAS_SiteBE.SiteCountryID = 0;

        List<MAS_SiteBE> lstSite = new List<MAS_SiteBE>();
        lstSite = oMAS_SiteBAL.GetSiteBAL(oMAS_SiteBE);
        string strSiteName = string.Empty;
        if (lstSite.Count > 0) {
            strSiteName = lstSite[lstSite.FindIndex(s => s.SiteID == pSiteID)].SiteDescription.ToString();
        }
        return strSiteName;
    }

    public void ClearControls() {
        DtMonday.Text = "(DD/MM/YYYY)";
        lblMonStartTime.Text = "-";
        lblMonEndTime.Text = "-";
        lblMonMaxLifts.Text = "-";
        lblMonMAxPallets.Text = "-";
        lblMonMaxLines.Text = "-";
        lblMonMaxContainers.Text = "-";
        lblMonMaxDeliveries.Text = "-";

        DtTuesday.Text = "(DD/MM/YYYY)";
        lblTueStartTime.Text = "-";
        lblTueEndTime.Text = "-";
        lblTueMaxLifts.Text = "-";
        lblTueMAxPallets.Text = "-";
        lblTueMaxLines.Text = "-";
        lblTueMaxContainers.Text = "-";
        lblTueMaxDeliveries.Text = "-";

        DtWednesday.Text = "(DD/MM/YYYY)";
        lblWedStartTime.Text = "-";
        lblWedEndTime.Text = "-";
        lblWedMaxLifts.Text = "-";
        lblWedMAxPallets.Text = "-";
        lblWedMaxLines.Text = "-";
        lblWedMaxContainers.Text = "-";
        lblWedMaxDeliveries.Text = "-";

        DtThursday.Text = "(DD/MM/YYYY)";
        lblThuStartTime.Text = "-";
        lblThuEndTime.Text = "-";
        lblThuMaxLifts.Text = "-";
        lblThuMAxPallets.Text = "-";
        lblThuMaxLines.Text = "-";
        lblThuMaxContainers.Text = "-";
        lblThuMaxDeliveries.Text = "-";

        DtFriday.Text = "(DD/MM/YYYY)";
        lblFriStartTime.Text = "-";
        lblFriEndTime.Text = "-";
        lblFriMaxLifts.Text = "-";
        lblFriMAxPallets.Text = "-";
        lblFriMaxLines.Text = "-";
        lblFriMaxContainers.Text = "-";
        lblFriMaxDeliveries.Text = "-";

        DtSaturday.Text = "(DD/MM/YYYY)";
        lblSatStartTime.Text = "-";
        lblSatEndTime.Text = "-";
        lblSatMaxLifts.Text = "-";
        lblSatMAxPallets.Text = "-";
        lblSatMaxLines.Text = "-";
        lblSatMaxContainers.Text = "-";
        lblSatMaxDeliveries.Text = "-";

        DtSunday.Text = "(DD/MM/YYYY)";
        lblSunStartTime.Text = "-";
        lblSunEndTime.Text = "-";
        lblSunMaxLifts.Text = "-";
        lblSunMAxPallets.Text = "-";
        lblSunMaxLines.Text = "-";
        lblSunMaxContainers.Text = "-";
        lblSunMaxDeliveries.Text = "-";

        lnkMonday.Enabled = false;
        lnkTuesday.Enabled = false;
        lnkWednesday.Enabled = false;
        lnkThursday.Enabled = false;
        lnkFriday.Enabled = false;
        lnkSaturday.Enabled = false;
        lnkSunday.Enabled = false;
    }

    public void BindSpecificSiteWeekSetups() {

        MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE = new MASSIT_SpecificWeekSetupBE();
        APPSIT_SpecificWeekSetupBAL oMASSIT_SpecificWeekSetupBAL = new APPSIT_SpecificWeekSetupBAL();

        oMASSIT_SpecificWeekSetupBE.WeekStartDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);
        //oMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
        oMASSIT_SpecificWeekSetupBE.Action = "ShowAll";        
        
        List<MASSIT_SpecificWeekSetupBE> lstSpecificWeekSetups = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);


        if (lstSpecificWeekSetups != null && lstSpecificWeekSetups.Count > 0) {

            UcGridView1.DataSource = lstSpecificWeekSetups;
            UcGridView1.DataBind();

            DateTime? dtWeekStartDate = null;
            for (int i = 0; i < lstSpecificWeekSetups.Count; i++) {
                dtWeekStartDate = lstSpecificWeekSetups[i].WeekStartDate;
                if (lstSpecificWeekSetups[i].EndWeekday == "mon") {                                        
                    if (dtWeekStartDate != null)
                        DtMonday.Text = GetDate(dtWeekStartDate.Value, 0);

                    if (lstSpecificWeekSetups[i].IsDayDisabled == false) {
                        lnkMonday.Enabled = true;
                        lnkMonday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?WeekStartDate=" + txtDate.innerControltxtDate.Value + "&SiteScheduleDiaryID=" + lstSpecificWeekSetups[i].SiteScheduleDiaryID.ToString() + "&weekday=mon" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                        if (lstSpecificWeekSetups[i].StartTime != null && lstSpecificWeekSetups[i].StartTime.ToString() != "")
                            lblMonStartTime.Text = lstSpecificWeekSetups[i].StartWeekday.ToUpper() + "-" + lstSpecificWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblMonStartTime.Text = "-";

                        if (lstSpecificWeekSetups[i].EndTime != null && lstSpecificWeekSetups[i].EndTime.ToString() != "")
                            lblMonEndTime.Text = lstSpecificWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblMonEndTime.Text = "-";
                        lblMonMaxLifts.Text = lstSpecificWeekSetups[i].MaximumLift != null ? lstSpecificWeekSetups[i].MaximumLift.ToString() : "-";
                        lblMonMAxPallets.Text = lstSpecificWeekSetups[i].MaximumPallet != null ? lstSpecificWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblMonMaxLines.Text = lstSpecificWeekSetups[i].MaximumLine != null ? lstSpecificWeekSetups[i].MaximumLine.ToString() : "-";
                        lblMonMaxContainers.Text = lstSpecificWeekSetups[i].MaximumContainer != null ? lstSpecificWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblMonMaxDeliveries.Text = lstSpecificWeekSetups[i].MaximumDeliveries != null ? lstSpecificWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkMonday.Enabled = false;
                    }
                }
                else if (lstSpecificWeekSetups[i].EndWeekday == "tue") {

                    if (dtWeekStartDate != null)
                        DtTuesday.Text = GetDate(dtWeekStartDate.Value, 1);

                    if (lstSpecificWeekSetups[i].IsDayDisabled == false) {
                        lnkTuesday.Enabled = true;
                        lnkTuesday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?WeekStartDate=" + txtDate.innerControltxtDate.Value + "&SiteScheduleDiaryID=" + lstSpecificWeekSetups[i].SiteScheduleDiaryID.ToString() + "&weekday=tue" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                        if (lstSpecificWeekSetups[i].StartTime != null && lstSpecificWeekSetups[i].StartTime.ToString() != "")
                            lblTueStartTime.Text = lstSpecificWeekSetups[i].StartWeekday.ToUpper() + "-" + lstSpecificWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblTueStartTime.Text = "-";

                        if (lstSpecificWeekSetups[i].EndTime != null && lstSpecificWeekSetups[i].EndTime.ToString() != "")
                            lblTueEndTime.Text = lstSpecificWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblTueEndTime.Text = "-";
                        lblTueMaxLifts.Text = lstSpecificWeekSetups[i].MaximumLift != null ? lstSpecificWeekSetups[i].MaximumLift.ToString() : "-";
                        lblTueMAxPallets.Text = lstSpecificWeekSetups[i].MaximumPallet != null ? lstSpecificWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblTueMaxLines.Text = lstSpecificWeekSetups[i].MaximumLine != null ? lstSpecificWeekSetups[i].MaximumLine.ToString() : "-";
                        lblTueMaxContainers.Text = lstSpecificWeekSetups[i].MaximumContainer != null ? lstSpecificWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblTueMaxDeliveries.Text = lstSpecificWeekSetups[i].MaximumDeliveries != null ? lstSpecificWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkTuesday.Enabled = false;
                    }
                }
                else if (lstSpecificWeekSetups[i].EndWeekday == "wed") {

                    if (dtWeekStartDate != null)
                        DtWednesday.Text = GetDate(dtWeekStartDate.Value, 2);

                    if (lstSpecificWeekSetups[i].IsDayDisabled == false) {
                        lnkWednesday.Enabled = true;
                        lnkWednesday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?WeekStartDate=" + txtDate.innerControltxtDate.Value + "&SiteScheduleDiaryID=" + lstSpecificWeekSetups[i].SiteScheduleDiaryID.ToString() + "&weekday=wed" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                        if (lstSpecificWeekSetups[i].StartTime != null && lstSpecificWeekSetups[i].StartTime.ToString() != "")
                            lblWedStartTime.Text = lstSpecificWeekSetups[i].StartWeekday.ToUpper() + "-" + lstSpecificWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblWedStartTime.Text = "-";

                        if (lstSpecificWeekSetups[i].EndTime != null && lstSpecificWeekSetups[i].EndTime.ToString() != "")
                            lblWedEndTime.Text = lstSpecificWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblWedEndTime.Text = "-";

                        lblWedMaxLifts.Text = lstSpecificWeekSetups[i].MaximumLift != null ? lstSpecificWeekSetups[i].MaximumLift.ToString() : "-";
                        lblWedMAxPallets.Text = lstSpecificWeekSetups[i].MaximumPallet != null ? lstSpecificWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblWedMaxLines.Text = lstSpecificWeekSetups[i].MaximumLine != null ? lstSpecificWeekSetups[i].MaximumLine.ToString() : "-";
                        lblWedMaxContainers.Text = lstSpecificWeekSetups[i].MaximumContainer != null ? lstSpecificWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblWedMaxDeliveries.Text = lstSpecificWeekSetups[i].MaximumDeliveries != null ? lstSpecificWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkWednesday.Enabled = false;
                    }
                }
                else if (lstSpecificWeekSetups[i].EndWeekday == "thu") {

                    if (dtWeekStartDate != null)
                        DtThursday.Text = GetDate(dtWeekStartDate.Value, 3);

                    if (lstSpecificWeekSetups[i].IsDayDisabled == false) {
                        lnkThursday.Enabled = true;
                        lnkThursday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?WeekStartDate=" + txtDate.innerControltxtDate.Value + "&SiteScheduleDiaryID=" + lstSpecificWeekSetups[i].SiteScheduleDiaryID.ToString() + "&weekday=thu" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                        if (lstSpecificWeekSetups[i].StartTime != null && lstSpecificWeekSetups[i].StartTime.ToString() != "")
                            lblThuStartTime.Text = lstSpecificWeekSetups[i].StartWeekday.ToUpper() + "-" + lstSpecificWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblThuStartTime.Text = "-";

                        if (lstSpecificWeekSetups[i].EndTime != null && lstSpecificWeekSetups[i].EndTime.ToString() != "")
                            lblThuEndTime.Text = lstSpecificWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblThuEndTime.Text = "-";

                        lblThuMaxLifts.Text = lstSpecificWeekSetups[i].MaximumLift != null ? lstSpecificWeekSetups[i].MaximumLift.ToString() : "-";
                        lblThuMAxPallets.Text = lstSpecificWeekSetups[i].MaximumPallet != null ? lstSpecificWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblThuMaxLines.Text = lstSpecificWeekSetups[i].MaximumLine != null ? lstSpecificWeekSetups[i].MaximumLine.ToString() : "-";
                        lblThuMaxContainers.Text = lstSpecificWeekSetups[i].MaximumContainer != null ? lstSpecificWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblThuMaxDeliveries.Text = lstSpecificWeekSetups[i].MaximumDeliveries != null ? lstSpecificWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkThursday.Enabled = false;
                    }
                }
                else if (lstSpecificWeekSetups[i].EndWeekday == "fri") {

                    if (dtWeekStartDate != null)
                        DtFriday.Text = GetDate(dtWeekStartDate.Value, 4);

                    if (lstSpecificWeekSetups[i].IsDayDisabled == false) {
                        lnkFriday.Enabled = true;
                        lnkFriday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?WeekStartDate=" + txtDate.innerControltxtDate.Value + "&SiteScheduleDiaryID=" + lstSpecificWeekSetups[i].SiteScheduleDiaryID.ToString() + "&weekday=fri" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                        if (lstSpecificWeekSetups[i].StartTime != null && lstSpecificWeekSetups[i].StartTime.ToString() != "")
                            lblFriStartTime.Text = lstSpecificWeekSetups[i].StartWeekday.ToUpper() + "-" + lstSpecificWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblFriStartTime.Text = "-";

                        if (lstSpecificWeekSetups[i].EndTime != null && lstSpecificWeekSetups[i].EndTime.ToString() != "")
                            lblFriEndTime.Text = lstSpecificWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblFriEndTime.Text = "-";

                        lblFriMaxLifts.Text = lstSpecificWeekSetups[i].MaximumLift != null ? lstSpecificWeekSetups[i].MaximumLift.ToString() : "-";
                        lblFriMAxPallets.Text = lstSpecificWeekSetups[i].MaximumPallet != null ? lstSpecificWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblFriMaxLines.Text = lstSpecificWeekSetups[i].MaximumLine != null ? lstSpecificWeekSetups[i].MaximumLine.ToString() : "-";
                        lblFriMaxContainers.Text = lstSpecificWeekSetups[i].MaximumContainer != null ? lstSpecificWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblFriMaxDeliveries.Text = lstSpecificWeekSetups[i].MaximumDeliveries != null ? lstSpecificWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkFriday.Enabled = false;
                    }
                }
                else if (lstSpecificWeekSetups[i].EndWeekday == "sat") {

                    if (dtWeekStartDate != null)
                        DtSaturday.Text = GetDate(dtWeekStartDate.Value, 5);

                    if (lstSpecificWeekSetups[i].IsDayDisabled == false) {
                        lnkSaturday.Enabled = true;
                        lnkSaturday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?WeekStartDate=" + txtDate.innerControltxtDate.Value + "&SiteScheduleDiaryID=" + lstSpecificWeekSetups[i].SiteScheduleDiaryID.ToString() + "&weekday=sat" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                        if (lstSpecificWeekSetups[i].StartTime != null && lstSpecificWeekSetups[i].StartTime.ToString() != "")
                            lblSatStartTime.Text = lstSpecificWeekSetups[i].StartWeekday.ToUpper() + "-" + lstSpecificWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblSatStartTime.Text = "-";

                        if (lstSpecificWeekSetups[i].EndTime != null && lstSpecificWeekSetups[i].EndTime.ToString() != "")
                            lblSatEndTime.Text = lstSpecificWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblSatEndTime.Text = "-";

                        lblSatMaxLifts.Text = lstSpecificWeekSetups[i].MaximumLift != null ? lstSpecificWeekSetups[i].MaximumLift.ToString() : "-";
                        lblSatMAxPallets.Text = lstSpecificWeekSetups[i].MaximumPallet != null ? lstSpecificWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblSatMaxLines.Text = lstSpecificWeekSetups[i].MaximumLine != null ? lstSpecificWeekSetups[i].MaximumLine.ToString() : "-";
                        lblSatMaxContainers.Text = lstSpecificWeekSetups[i].MaximumContainer != null ? lstSpecificWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblSatMaxDeliveries.Text = lstSpecificWeekSetups[i].MaximumDeliveries != null ? lstSpecificWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkSaturday.Enabled = false;
                    }
                }
                else if (lstSpecificWeekSetups[i].EndWeekday == "sun") {

                    if(dtWeekStartDate != null)
                        DtSunday.Text = GetDate(dtWeekStartDate.Value, 6);

                    if (lstSpecificWeekSetups[i].IsDayDisabled == false) {
                        lnkSunday.Enabled = true;
                        lnkSunday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?WeekStartDate=" + txtDate.innerControltxtDate.Value + "&SiteScheduleDiaryID=" + lstSpecificWeekSetups[i].SiteScheduleDiaryID.ToString() + "&weekday=sun" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                        if (lstSpecificWeekSetups[i].StartTime != null && lstSpecificWeekSetups[i].StartTime.ToString() != "")
                            lblSunStartTime.Text = lstSpecificWeekSetups[i].StartWeekday.ToUpper() + "-" + lstSpecificWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblSunStartTime.Text = "-";

                        if (lstSpecificWeekSetups[i].EndTime != null && lstSpecificWeekSetups[i].EndTime.ToString() != "")
                            lblSunEndTime.Text = lstSpecificWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblSunEndTime.Text = "-";

                        lblSunMaxLifts.Text = lstSpecificWeekSetups[i].MaximumLift != null ? lstSpecificWeekSetups[i].MaximumLift.ToString() : "-";
                        lblSunMAxPallets.Text = lstSpecificWeekSetups[i].MaximumPallet != null ? lstSpecificWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblSunMaxLines.Text = lstSpecificWeekSetups[i].MaximumLine != null ? lstSpecificWeekSetups[i].MaximumLine.ToString() : "-";
                        lblSunMaxContainers.Text = lstSpecificWeekSetups[i].MaximumContainer != null ? lstSpecificWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblSunMaxDeliveries.Text = lstSpecificWeekSetups[i].MaximumDeliveries != null ? lstSpecificWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkSunday.Enabled = false;
                    }
                }
            }

            string[] weekArray = { "mon", "tue", "wed", "thu", "fri", "sat", "sun" };
            string[] strSelected = txtDate.innerControltxtDate.Value.Split('/');
            DateTime dtSelected = Convert.ToDateTime(strSelected[1] + "/" + strSelected[0] + "/" + strSelected[2]);
            for (int i = 0; i < weekArray.Length; i++) {
                int count = 0;
                for (int j = 0; j < lstSpecificWeekSetups.Count; j++) {
                    if (weekArray[i].ToString() == lstSpecificWeekSetups[j].EndWeekday.ToString()) {
                        count = 0;
                        break;
                    }
                    else {
                        count++;
                    }
                }
                if (count > 0) {
                    if (weekArray[i].ToString() == "mon") {
                        DtMonday.Text = GetDate(dtSelected, 0);                       
                        lnkMonday.Enabled = true;
                        lnkMonday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=mon" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                    }
                    else if (weekArray[i].ToString() == "tue") {
                        DtTuesday.Text = GetDate(dtSelected, 1);                       
                        lnkTuesday.Enabled = true;
                        lnkTuesday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=tue" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                    }
                    else if (weekArray[i].ToString() == "wed") {
                        DtWednesday.Text = GetDate(dtSelected, 2);                        
                        lnkWednesday.Enabled = true;
                        lnkWednesday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=wed" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                    }
                    else if (weekArray[i].ToString() == "thu") {
                        DtThursday.Text = GetDate(dtSelected, 3);                        
                        lnkThursday.Enabled = true;
                        lnkThursday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=thu" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                    }
                    else if (weekArray[i].ToString() == "fri") {
                        DtFriday.Text = GetDate(dtSelected, 4);                        
                        lnkFriday.Enabled = true;
                        lnkFriday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=fri" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                    }
                    else if (weekArray[i].ToString() == "sat") {
                        DtSaturday.Text = GetDate(dtSelected, 5);                        
                        lnkSaturday.Enabled = true;
                        lnkSaturday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=sat" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                    }
                    else if (weekArray[i].ToString() == "sun") {
                        DtSunday.Text = GetDate(dtSelected, 6);
                        lnkSunday.Enabled = true;
                        lnkSunday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=sun" + "&WeekSpecificID=" + lstSpecificWeekSetups[0].WeekSetupSpecificID.ToString());
                    }
                }
            }
        }
        else {
            oMASSIT_SpecificWeekSetupBE.Action = "GetWeekSpecificID";
            List<MASSIT_SpecificWeekSetupBE> lstSpecificWeekSetupID = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);            
            if (lstSpecificWeekSetupID != null && lstSpecificWeekSetupID.Count > 0) {

                lnkMonday.Enabled = true;
                lnkTuesday.Enabled = true;
                lnkWednesday.Enabled = true;
                lnkThursday.Enabled = true;
                lnkFriday.Enabled = true;
                lnkSaturday.Enabled = true;
                lnkSunday.Enabled = true;

                lnkMonday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=mon" + "&WeekSpecificID=" + lstSpecificWeekSetupID[0].WeekSetupSpecificID.ToString());
                lnkTuesday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=tue" + "&WeekSpecificID=" + lstSpecificWeekSetupID[0].WeekSetupSpecificID.ToString());
                lnkWednesday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=wed" + "&WeekSpecificID=" + lstSpecificWeekSetupID[0].WeekSetupSpecificID.ToString());
                lnkThursday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=thu" + "&WeekSpecificID=" + lstSpecificWeekSetupID[0].WeekSetupSpecificID.ToString());
                lnkFriday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=fri" + "&WeekSpecificID=" + lstSpecificWeekSetupID[0].WeekSetupSpecificID.ToString());
                lnkSaturday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=sat" + "&WeekSpecificID=" + lstSpecificWeekSetupID[0].WeekSetupSpecificID.ToString());
                lnkSunday.PostBackUrl = EncryptURL("APPSIT_SpecificWeekSetupEdit.aspx?SiteID=" + GetQueryStringValue("SiteID").ToString() + "&weekday=sun" + "&WeekSpecificID=" + lstSpecificWeekSetupID[0].WeekSetupSpecificID.ToString());

                DtMonday.Text = GetDate(lstSpecificWeekSetupID[0].WeekStartDate.Value, 0);
                DtTuesday.Text = GetDate(lstSpecificWeekSetupID[0].WeekStartDate.Value, 1); 
                DtWednesday.Text = GetDate(lstSpecificWeekSetupID[0].WeekStartDate.Value, 2);
                DtThursday.Text = GetDate(lstSpecificWeekSetupID[0].WeekStartDate.Value, 3); 
                DtFriday.Text = GetDate(lstSpecificWeekSetupID[0].WeekStartDate.Value, 4); 
                DtSaturday.Text = GetDate(lstSpecificWeekSetupID[0].WeekStartDate.Value, 5);
                DtSunday.Text = GetDate(lstSpecificWeekSetupID[0].WeekStartDate.Value, 6);
            }            
        }
    }

    public bool CheckWeekExistance() {

        MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE = new MASSIT_SpecificWeekSetupBE();
        APPSIT_SpecificWeekSetupBAL oMASSIT_SpecificWeekSetupBAL = new APPSIT_SpecificWeekSetupBAL();
        oMASSIT_SpecificWeekSetupBE.Action = "ShowAll";
        //oMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
        oMASSIT_SpecificWeekSetupBE.WeekStartDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);

        List<MASSIT_SpecificWeekSetupBE> lstSpecificWeekSetups = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);
        if (lstSpecificWeekSetups != null && lstSpecificWeekSetups.Count > 0) {
            return true;
        }
        else
            return false;
    }

    public string GetDate(DateTime dtDate, int addDays) {
        return (dtDate.AddDays(addDays).ToString("dd") + "/" + dtDate.AddDays(addDays).ToString("MM") + "/" + dtDate.AddDays(addDays).ToString("yyyy"));
    }

    #endregion    
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_WeekSetupLogs.aspx.cs" Inherits="APPSIT_WeekSetupLogs" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc3" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblWeekSetup" runat="server" Text="Week Settings Changes Tracker"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
    <div class="formbox">
        <table width="30%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
            <tr>
                <td style="width: 20%">
                </td>
                <td style="font-weight: bold; width: 20%">
                    <cc1:ucLabel ID="UcLabel1" runat="server" Text="Site"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 5%">
                    :
                </td>
                <td style="font-weight: bold; width: 35%">
                    <cc4:ucSite ID="ddlSite" runat="server" />
                </td>
                <td style="width: 20%">
                       <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" ValidationGroup="a"
                    OnClick="btnSearch_Click" />
                </td>
            </tr>
        </table>
        <div class="button-row">
            
            <cc3:ucExportToExcel ID="ucExportToExcel" runat="server" />
        </div>

        <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" AutoGenerateColumns="false" Visible="false">
        <Columns>

                        <asp:BoundField HeaderText="Date/Time" DataField="LogTime" DataFormatString="{0: dd/MM/yyyy HH:mm }" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>

                        <asp:BoundField HeaderText="Site" DataField="SiteName" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>

                        <asp:BoundField HeaderText="Type" DataField="Type" DataFormatString="{0: dd/MM/yyyy}">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Start Time" DataField="StartTime" DataFormatString="{0: HH:mm }" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="End Time" DataField="EndTime" DataFormatString="{0: HH:mm }">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Max Lifts" DataField="MaximumLift" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                       <asp:BoundField HeaderText="Max Pallets" DataField="MaximumPallet" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                       <asp:BoundField HeaderText="Max Lines" DataField="MaximumLine" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                       <asp:BoundField HeaderText="Max Deliveries" DataField="MaximumDeliveries" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Max Containers" DataField="MaximumContainer" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="User Name" DataField="UserName" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        
        </Columns>
        </cc1:ucGridView>
    </div>
    </div>
   
</asp:Content>

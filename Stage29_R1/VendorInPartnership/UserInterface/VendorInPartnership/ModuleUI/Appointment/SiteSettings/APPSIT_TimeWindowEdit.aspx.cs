﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using System.Data;

using Utilities;
using WebUtilities;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;

public partial class APPSIT_TimeWindowEdit : CommonPage
{
    #region Global Declarations ...

    MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = null;
    MASSIT_TimeWindowBE oMASSIT_TimeWindowBE = null;
    string mustInsertWindow = WebCommon.getGlobalResourceValue("MustInsertWindow");
    string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
    string startEndTimeAlreadyExist = WebCommon.getGlobalResourceValue("StartEndTimeExist");
    string isStartEndTimeEqual = WebCommon.getGlobalResourceValue("IsStartEndTimeEqual");
    string isStartTimeGreaterThanEndTime = WebCommon.getGlobalResourceValue("IsStartTimeGreaterThanEndTime");
    string totalHrsMinsFormat = WebCommon.getGlobalResourceValue("ValidTotalHrsMinsTime");
    string isBookExistForWindowMessage = WebCommon.getGlobalResourceValue("IsBookExistForWindowMessage");
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    string includeExcludeVendorMessage = WebCommon.getGlobalResourceValue("IncludeExcludeVendorMessage");
    string includeExcludeCarrierMessage = WebCommon.getGlobalResourceValue("IncludeExcludeCarrierMessage");

    ucListBox lstSelectedVendor = new ucListBox();
    ucListBox lstIncludeSelectedVendor = new ucListBox();

    #endregion

    #region Page level Events ...

    #region Carrier Events ..

    protected void btnMoveRightAll_Click(object sender, EventArgs e) {
        if (UclstCarrier.Items.Count > 0)
            FillControls.MoveAllItems(UclstCarrier, UclstRightCarrier);
    }
    protected void btnMoveRight_Click(object sender, EventArgs e) {
        if (UclstCarrier.SelectedItem != null) {
            FillControls.MoveOneItem(UclstCarrier, UclstRightCarrier);
        }
    }
    protected void btnMoveLeft_Click(object sender, EventArgs e) {
        if (UclstRightCarrier.SelectedItem != null) {
            FillControls.MoveOneItem(UclstRightCarrier, UclstCarrier);
        }
    }
    protected void btnMoveLeftAll_Click(object sender, EventArgs e) {
        if (UclstRightCarrier.Items.Count > 0) {
            FillControls.MoveAllItems(UclstRightCarrier, UclstCarrier);
        }
    }

    #endregion

    #region Include Carrier Events ..

    protected void btnIncludeMoveRightAll_Click(object sender, EventArgs e)
    {
        if (uclstIncludeCarrier.Items.Count > 0)
            FillControls.MoveAllItems(uclstIncludeCarrier, uclstIncludeRightCarrier);
    }
    protected void btnIncludeMoveRight_Click(object sender, EventArgs e)
    {
        if (uclstIncludeCarrier.SelectedItem != null)
        {
            FillControls.MoveOneItem(uclstIncludeCarrier, uclstIncludeRightCarrier);
        }
    }
    protected void btnIncludeMoveLeft_Click(object sender, EventArgs e)
    {
        if (uclstIncludeRightCarrier.SelectedItem != null)
        {
            FillControls.MoveOneItem(uclstIncludeRightCarrier, uclstIncludeCarrier);
        }
    }
    protected void btnIncludeMoveLeftAll_Click(object sender, EventArgs e)
    {
        if (uclstIncludeRightCarrier.Items.Count > 0)
        {
            FillControls.MoveAllItems(uclstIncludeRightCarrier, uclstIncludeCarrier);
        }
    }

    #endregion

    protected void Page_InIt(object sender, EventArgs e) {
        //ucSeacrhVendor1.CurrentPage = this;
        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = true;

        //ucIncludeVendor.CurrentPage = this;
        ucIncludeVendor.IsParentRequired = true;
        ucIncludeVendor.IsStandAloneRequired = true;
        ucIncludeVendor.IsChildRequired = true;
        ucIncludeVendor.IsGrandParentRequired = true;

        if (GetQueryStringValue("SiteId") != null) {
            ucSeacrhVendor1.SiteID = Convert.ToInt32(GetQueryStringValue("SiteId"));
            ucIncludeVendor.SiteID = Convert.ToInt32(GetQueryStringValue("SiteId"));

            MAS_SiteBE singleSiteSetting = GetSingleSiteSetting(Convert.ToInt32(GetQueryStringValue("SiteId")));
            if (singleSiteSetting != null)
                lblSiteNameT.Text = string.Format(" : {0} - {1}", singleSiteSetting.SitePrefix, singleSiteSetting.SiteName);
        }

        if (GetQueryStringValue("TimeWindowID") == null) {
            btnDelete.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e) {
        int ItemCount = 0;
        if (!Page.IsPostBack) {
            if (GetQueryStringValue("SiteId") != null) {
                #region Get WeekSetupDetail
                MASSIT_TimeWindowBE oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
                oMASSIT_TimeWindowBE.Action = "GetTimeWindow";
                oMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteId"));
                oMASSIT_TimeWindowBE.Weekday = GetQueryStringValue("WeekDay");
                MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
                List<MASSIT_TimeWindowBE> lstMASSIT_TimeWindowBE = oMASSIT_TimeWindowBAL.GetTimeWindowBAL(oMASSIT_TimeWindowBE);

                if (lstMASSIT_TimeWindowBE != null) {
                    ItemCount = lstMASSIT_TimeWindowBE.Count;
                    ddlPriority.Items.Clear();
                    for (int iCount = 1; iCount <= ItemCount + 1; iCount++) {
                        ddlPriority.Items.Insert(iCount - 1, new ListItem(iCount.ToString(), iCount.ToString()));
                    }
                }
                #endregion
            }
        }
        if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro") {
            btnSave.Visible = false;
            btnDelete.Visible = false;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e) {
        if (!IsPostBack) {

            //Convert.ToString(GetQueryStringValue("SiteId"));
            //Convert.ToString(GetQueryStringValue("SiteDoorNoId"));
            //Convert.ToString(GetQueryStringValue("WeekDay"));

            this.BindTimes();

            ltDoor.Text = Convert.ToString(GetQueryStringValue("DoorNo"));
            ltDoorType.Text = Convert.ToString(GetQueryStringValue("DoorType"));


            int intTimeWindowID = 0;
            if (GetQueryStringValue("TimeWindowID") != null)
                intTimeWindowID = Convert.ToInt32(GetQueryStringValue("TimeWindowID"));

            if (intTimeWindowID != 0) {
                //txtWindow.Enabled = false;
                this.SetTimeWindowData(intTimeWindowID);
            }
            else {
                txtWindow.Enabled = true;
                this.BindCarriers();
                this.BindVendors();
                this.BindIncludeCarriers();
                this.BindIncludeVendors();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e) {
        if (GetQueryStringValue("TimeWindowID") != null)
            this.SaveData(Convert.ToInt32(GetQueryStringValue("TimeWindowID")));
        else
            this.SaveData();
    }

    protected void btnBack_Click(object sender, EventArgs e) {
        if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro")
            EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
        else {
            EncryptQueryString("APPSIT_TimeWindow.aspx");
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e) {
        if (GetQueryStringValue("TimeWindowID") != null) {
            oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
            oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();

            oMASSIT_TimeWindowBE.Action = "IsBookingExistForWindow";
            oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(GetQueryStringValue("TimeWindowID"));
            if (oMASSIT_TimeWindowBAL.IsBookingExistForWindowBAL(oMASSIT_TimeWindowBE) > 0) {          
                mdNoShow.Show();               
            }
            else {
                oMASSIT_TimeWindowBE.Action = "DeleteWindow";
                oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(GetQueryStringValue("TimeWindowID"));
                //oMASSIT_TimeWindowBAL.DeleteWindowBAL(oMASSIT_TimeWindowBE);
                EncryptQueryString("APPSIT_TimeWindow.aspx");
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        mdNoShow.Hide();
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {        
        DeleteWindow();
    }
    #endregion

    #region Page level Methods ...

    private void DeleteWindow()
    {
        oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
        oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        oMASSIT_TimeWindowBE.Action = "DeleteWindowSoft";
        oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(GetQueryStringValue("TimeWindowID"));
        oMASSIT_TimeWindowBAL.DeleteWindowBAL(oMASSIT_TimeWindowBE);
        EncryptQueryString("APPSIT_TimeWindow.aspx");        
    }
    public void BindVendors(string sExculdedVendorIDs = "", string sExculdedVendorName = "") {
        lstSelectedVendor.Items.Clear();

        lstSelectedVendor = (ucListBox)ucSeacrhVendor1.FindControl("lstSelectedVendor");

        DataTable dt1 = new DataTable();
        dt1.Columns.Add("VendorName", typeof(string));
        dt1.Columns.Add("SiteVendorID", typeof(string));

        string[] VendorNames = sExculdedVendorName.Split('=');

        if (VendorNames.Length > 0 && sExculdedVendorName != string.Empty) {
            for (int i = 0; i < VendorNames.Length; i++) {
                string values = VendorNames[i].Split('~')[0].Trim();
                string names = VendorNames[i].Split('~')[1].Trim();
                dt1.Rows.Add(new object[] { Common.HtmlDecode(names), values });
            }
        }

        //string[] names = sExculdedVendorName.Split(',');
        //string[] values = sExculdedVendorIDs.Split(',');

        //for (int i = 0; i < names.Length; i++)
        //    dt1.Rows.Add(new object[] { names[i], values[i] });


        if (dt1 != null && dt1.Rows.Count > 0) {
            FillControls.FillListBox(ref lstSelectedVendor, dt1, "VendorName", "SiteVendorID");
        }

        ucListBox lstVendor = (ucListBox)ucSeacrhVendor1.FindControl("lstVendor");

        if (lstSelectedVendor.Items.Count > 0) {
            for (int i = 0; i < lstSelectedVendor.Items.Count; i++) {
                if (lstVendor.Items.FindByValue(lstSelectedVendor.Items[i].Value) != null) {
                    lstVendor.Items.Remove(new ListItem(lstSelectedVendor.Items[i].Text, lstSelectedVendor.Items[i].Value));
                }
            }
        }

        if (lstSelectedVendor.Items.Count == 1)
            if (lstSelectedVendor.Items[0].Text.Trim().Length == 0)
                lstSelectedVendor.Items.Clear();

    }

    public void BindIncludeVendors(string sExculdedVendorIDs = "", string sExculdedVendorName = "")
    {
        //lstSelectedVendor.Items.Clear();

        lstSelectedVendor = (ucListBox)ucIncludeVendor.FindControl("lstSelectedVendor");

        DataTable dt1 = new DataTable();
        dt1.Columns.Add("VendorName", typeof(string));
        dt1.Columns.Add("SiteVendorID", typeof(string));

        string[] VendorNames = sExculdedVendorName.Split('=');

        if (VendorNames.Length > 0 && sExculdedVendorName != string.Empty) {
            for (int i = 0; i < VendorNames.Length; i++) {
                string values = VendorNames[i].Split('~')[0].Trim();
                string names = VendorNames[i].Split('~')[1].Trim();
                dt1.Rows.Add(new object[] { Common.HtmlDecode(names), values });
            }
        }


        if (dt1 != null && dt1.Rows.Count > 0)
        {
            FillControls.FillListBox(ref lstSelectedVendor, dt1, "VendorName", "SiteVendorID");
        }

        ucListBox lstVendor = (ucListBox)ucSeacrhVendor1.FindControl("lstVendor");

        if (lstSelectedVendor.Items.Count > 0)
        {
            for (int i = 0; i < lstSelectedVendor.Items.Count; i++)
            {
                if (lstVendor.Items.FindByValue(lstSelectedVendor.Items[i].Value) != null)
                {
                    lstVendor.Items.Remove(new ListItem(lstSelectedVendor.Items[i].Text, lstSelectedVendor.Items[i].Value));
                }
            }
        }

        if (lstSelectedVendor.Items.Count == 1)
            if (lstSelectedVendor.Items[0].Text.Trim().Length == 0)
                lstSelectedVendor.Items.Clear();
    }

    public void BindCarriers(string sExculdedCarriersIDs = "", string sExculdedCarriersName = "")
    {
        UclstCarrier.Items.Clear();
        UclstRightCarrier.Items.Clear();
        //DataTable dt1;
        DataTable dt2;


        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        oMASCNT_CarrierBE.Action = "ShowAll";
        oMASCNT_CarrierBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_CarrierBE.Site = new MAS_SiteBE();
        oMASCNT_CarrierBE.Site.SiteID = Convert.ToInt32(GetQueryStringValue("SiteId"));

        dt2 = oMASCNT_CarrierBAL.GetAllCarrierDetailsBAL(oMASCNT_CarrierBE);

        DataView dv = dt2.DefaultView;
        dv.Sort = "CarrierName";
        dt2 = dv.ToTable();

        DataTable dt1 = new DataTable();
        dt1.Columns.Add("CarrierName", typeof(string));
        dt1.Columns.Add("SiteCarrierID", typeof(string));

        string[] CarrierNames = sExculdedCarriersName.Split('=');

        if (CarrierNames.Length > 0 && sExculdedCarriersName != string.Empty) {
            for (int i = 0; i < CarrierNames.Length; i++) {
                string values = CarrierNames[i].Split('~')[0].Trim();
                string names = CarrierNames[i].Split('~')[1].Trim();
                dt1.Rows.Add(new object[] { Common.HtmlDecode(names), values });
            }
        }

        //string[] names = sExculdedCarriersName.Split(',');
        //string[] values = sExculdedCarriersIDs.Split(',');

        //for (int i = 0; i < names.Length; i++)
        //    dt1.Rows.Add(new object[] { Common.HtmlDecode(names[i]), values[i] });


        if (dt1 != null && dt1.Rows.Count > 0)
        {
            FillControls.FillListBox(ref UclstRightCarrier, dt1, "CarrierName", "SiteCarrierID");
        }
        if (dt2 != null && dt2.Rows.Count > 0)
        {
            FillControls.FillListBox(ref UclstCarrier, dt2, "CarrierName", "CarrierID");
            foreach (ListItem item in UclstCarrier.Items)
            {
                item.Text = Common.HtmlDecode(item.Text);
            }
            if (UclstRightCarrier.Items.Count > 0)
            {
                for (int i = 0; i < UclstRightCarrier.Items.Count; i++)
                {
                    int carrierIndex = UclstCarrier.Items.IndexOf(UclstCarrier.Items.FindByText(UclstRightCarrier.Items[i].Text.Trim()));
                    if (carrierIndex != (-1))
                        UclstCarrier.Items.RemoveAt(carrierIndex);
                    //if (UclstCarrier.Items.FindByValue(UclstRightCarrier.Items[i].Value) != null) {
                    //    UclstCarrier.Items.Remove(new ListItem(UclstRightCarrier.Items[i].Text.Trim(), UclstRightCarrier.Items[i].Value.Trim()));
                    //}
                }
            }
        }

        if (UclstRightCarrier.Items.Count == 1)
            if (UclstRightCarrier.Items[0].Text.Trim().Length == 0)
                UclstRightCarrier.Items.Clear();
    }

    public void BindIncludeCarriers(string sExculdedCarriersIDs = "", string sExculdedCarriersName = "")
    {
        uclstIncludeCarrier.Items.Clear();
        uclstIncludeRightCarrier.Items.Clear();
        DataTable dt2;
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();

        oMASCNT_CarrierBE.Action = "ShowAll";
        oMASCNT_CarrierBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_CarrierBE.Site = new MAS_SiteBE();
        oMASCNT_CarrierBE.Site.SiteID = Convert.ToInt32(GetQueryStringValue("SiteId"));
        dt2 = oMASCNT_CarrierBAL.GetAllCarrierDetailsBAL(oMASCNT_CarrierBE);

        DataView dv = dt2.DefaultView;
        dv.Sort = "CarrierName";
        dt2 = dv.ToTable();

        DataTable dt1 = new DataTable();
        dt1.Columns.Add("CarrierName", typeof(string));
        dt1.Columns.Add("SiteCarrierID", typeof(string));

        string[] CarrierNames = sExculdedCarriersName.Split('=');

        if (CarrierNames.Length > 0 && sExculdedCarriersName != string.Empty) {
            for (int i = 0; i < CarrierNames.Length; i++) {
                string values = CarrierNames[i].Split('~')[0].Trim();
                string names = CarrierNames[i].Split('~')[1].Trim();
                dt1.Rows.Add(new object[] { Common.HtmlDecode(names), values });
            }
        }
        //string[] names = sExculdedCarriersName.Split(',');
        //string[] values = sExculdedCarriersIDs.Split(',');

        //for (int i = 0; i < names.Length; i++)
        //    dt1.Rows.Add(new object[] { Common.HtmlDecode(names[i]), values[i] });


        if (dt1 != null && dt1.Rows.Count > 0)
        {
            FillControls.FillListBox(ref uclstIncludeRightCarrier, dt1, "CarrierName", "SiteCarrierID");
        }
        if (dt2 != null && dt2.Rows.Count > 0)
        {
            FillControls.FillListBox(ref uclstIncludeCarrier, dt2, "CarrierName", "CarrierID");
            foreach (ListItem item in uclstIncludeCarrier.Items)
            {
                item.Text = Common.HtmlDecode(item.Text);
            }

            if (uclstIncludeRightCarrier.Items.Count > 0)
            {
                for (int i = 0; i < uclstIncludeRightCarrier.Items.Count; i++)
                {
                    int carrierIndex = uclstIncludeCarrier.Items.IndexOf(uclstIncludeCarrier.Items.FindByText(uclstIncludeRightCarrier.Items[i].Text.Trim()));
                    if (carrierIndex != (-1))
                        uclstIncludeCarrier.Items.RemoveAt(carrierIndex);
                }
            }
        }

        if (uclstIncludeRightCarrier.Items.Count == 1)
            if (uclstIncludeRightCarrier.Items[0].Text.Trim().Length == 0)
                uclstIncludeRightCarrier.Items.Clear();
    }

    public void BindTimes() {
        #region Commented Code
        //SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
        //SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        //oSYS_SlotTimeBE.Action = "ShowAll";
        //List<SYS_SlotTimeBE> lstTimes = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);
        //lstTimes = lstTimes.FindAll(delegate(SYS_SlotTimeBE t) { return t.SlotTimeID <= 96; });
        //if (lstTimes != null && lstTimes.Count > 0)
        //{
        //    FillControls.FillDropDown(ref ddlStartTime, lstTimes, "SlotTime", "SlotTimeID");
        //    FillControls.FillDropDown(ref ddlEndTime, lstTimes, "SlotTime", "SlotTimeID");
        //}
        #endregion

        if (GetQueryStringValue("SiteId") != null) {
            List<SYS_SlotTimeBE> lstStartEndTime = new List<SYS_SlotTimeBE>();
            SYS_SlotTimeBE startEndTime = null;
            //------------------------------------------------------------------------------------------------------------------------------

            MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
            MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();
            oMASSIT_WeekSetupBE.Action = "ShowAll";
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteId"));
            oMASSIT_WeekSetupBE.EndWeekday = GetQueryStringValue("WeekDay");
            List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
            //------------------------------------------------------------------------------------------------------------------------------

            SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
            SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
            oSYS_SlotTimeBE.Action = "ShowAll";
            List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);
            lstSlotTime = lstSlotTime.FindAll(delegate(SYS_SlotTimeBE t) { return t.SlotTimeID <= 96; });
            //------------------------------------------------------------------------------------------------------------------------------

            for (int jCount = 0; jCount < lstSlotTime.Count; jCount++) {
                string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + "." + lstWeekSetup[0].StartTime.Value.Minute.ToString();
                string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();

                decimal dSlotTime = Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.'));
                decimal dStartTime = Convert.ToDecimal(StartTime);
                decimal dEndTime = Convert.ToDecimal(EndTime);

                if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
                    dEndTime = Convert.ToDecimal("24.00");

                if (dSlotTime >= dStartTime && dSlotTime < dEndTime) {
                    startEndTime = new SYS_SlotTimeBE();
                    startEndTime.SlotTime = lstSlotTime[jCount].SlotTime;
                    startEndTime.SlotTimeID = lstSlotTime[jCount].SlotTimeID;
                    lstStartEndTime.Add(startEndTime);

                }
            }
            //------------------------------------------------------------------------------------------------------------------------------

            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday) {
                for (int jCount = 0; jCount < lstSlotTime.Count; jCount++) {
                    string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();

                    if (Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.')) < Convert.ToDecimal(EndTime)) {
                        startEndTime = new SYS_SlotTimeBE();
                        startEndTime.SlotTime = lstSlotTime[jCount].SlotTime;
                        startEndTime.SlotTimeID = lstSlotTime[jCount].SlotTimeID;
                        lstStartEndTime.Add(startEndTime);
                    }
                }
            }
            //------------------------------------------------------------------------------------------------------------------------------

            FillControls.FillDropDown(ref ddlStartTime, lstStartEndTime, "SlotTime", "SlotTimeID");
            FillControls.FillDropDown(ref ddlEndTime, lstStartEndTime, "SlotTime", "SlotTimeID");

            ViewState["StartEndTime"] = lstStartEndTime;
        }
    }

    private TableCell getTableCell(string text, double cellWidth, int borderWidth, int columnSpan) {
        TableCell tc = new TableCell();
        tc.Text = text;
        //if (borderWidth != 0)
        tc.BorderWidth = Unit.Pixel(borderWidth);
        //if (cellWidth != 0)
        tc.Width = Unit.Percentage(cellWidth);
        if (columnSpan > 0)
            tc.ColumnSpan = columnSpan;

        return tc;
    }

    private bool IsListBoxItemExistInAnotherListBox(ucListBox fromListBox, ucListBox toListBox)
    {
        bool blnExist = false;
        for (int index = 0; index < fromListBox.Items.Count; index++)
        {
            var lstItem = new ListItem(fromListBox.Items[index].Text, fromListBox.Items[index].Value);
            if (toListBox.Items.Contains(lstItem))
            {
                blnExist = true;
                break;
            }
        }
        return blnExist;
    }

    private void SaveData(int intTimeWindowID = 0) 
    {
        if (string.IsNullOrEmpty(txtWindow.Text) || string.IsNullOrWhiteSpace(txtWindow.Text)) {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + mustInsertWindow + "');</script>", false);
            txtWindow.Focus();
            return;
        }

        #region Validating the Start - End time ..

        #region Commented Code
        /*
            DateTime dtmStartTime = DateTime.Now;
            DateTime dtmEndTIme = DateTime.Now;
            bool blnDateStatus = false;
            if (ddlStartTime.Items.Count > 0 && ddlEndTime.Items.Count > 0)
            {
                dtmStartTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlStartTime.SelectedItem));
                dtmEndTIme = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlEndTime.SelectedItem));
                blnDateStatus = true;
            }

            if (blnDateStatus == true && (dtmStartTime > dtmEndTIme || dtmStartTime == dtmEndTIme))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + startTimeCanNotBeGreaterEndTime + "');</script>", false);
                ddlStartTime.Focus();
                return;
            }
        */
        #endregion

        DateTime dtmDdlFirstTime = DateTime.Now;
        DateTime dtmDdlLastTime = DateTime.Now;
        DateTime dtmDdlSelStartTime = DateTime.Now;
        DateTime dtmDdlSelEndTime = DateTime.Now;

        if (ddlStartTime.Items.Count > 0) {
            //dtmCurrentDay12AMTime = Convert.ToDateTime("2013-01-01 00:00:00"); // Current Day 12 AM time.

            if (ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByText("00:00")) > 0)
                dtmDdlFirstTime = Convert.ToDateTime(string.Format("2012-01-01 {0}:00", ddlStartTime.Items[0].Text));
            else
                dtmDdlFirstTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlStartTime.Items[0].Text));

            dtmDdlLastTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlStartTime.Items[(ddlStartTime.Items.Count - 1)].Text));

            if (ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByText("00:00")) > ddlStartTime.SelectedIndex)
                dtmDdlSelStartTime = Convert.ToDateTime(string.Format("2012-01-01 {0}:00", ddlStartTime.SelectedItem));
            else
                dtmDdlSelStartTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlStartTime.SelectedItem));

            dtmDdlSelEndTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlEndTime.SelectedItem));


            if (dtmDdlFirstTime > dtmDdlLastTime && dtmDdlSelStartTime > dtmDdlLastTime && dtmDdlSelEndTime > dtmDdlLastTime) {
                if (dtmDdlSelStartTime > dtmDdlSelEndTime) {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartTimeGreaterThanEndTime + "');</script>", false);
                    ddlStartTime.Focus();
                    return;

                }
                else if (dtmDdlSelStartTime == dtmDdlSelEndTime) {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartEndTimeEqual + "');</script>", false);
                    ddlStartTime.Focus();
                    return;
                }
            }
            else if (dtmDdlFirstTime > dtmDdlLastTime && dtmDdlSelStartTime < dtmDdlLastTime && dtmDdlSelEndTime < dtmDdlLastTime) {
                if (dtmDdlSelStartTime > dtmDdlSelEndTime) {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartTimeGreaterThanEndTime + "');</script>", false);
                    ddlStartTime.Focus();
                    return;
                }
                if (dtmDdlSelStartTime == dtmDdlSelEndTime) {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartEndTimeEqual + "');</script>", false);
                    ddlStartTime.Focus();
                    return;
                }
            }
            else if (dtmDdlFirstTime > dtmDdlLastTime && dtmDdlSelStartTime > dtmDdlLastTime && dtmDdlSelEndTime < dtmDdlLastTime) {
                //blnStatus = true;
            }
            else if (dtmDdlFirstTime > dtmDdlLastTime && dtmDdlSelStartTime < dtmDdlLastTime && dtmDdlSelEndTime > dtmDdlLastTime) {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartTimeGreaterThanEndTime + "');</script>", false);
                ddlStartTime.Focus();
                return;
            }
            else if (dtmDdlSelStartTime == dtmDdlSelEndTime) {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartEndTimeEqual + "');</script>", false);
                ddlStartTime.Focus();
                return;
            }
            else if (dtmDdlSelStartTime > dtmDdlSelEndTime) {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartTimeGreaterThanEndTime + "');</script>", false);
                ddlStartTime.Focus();
                return;
            }
        }

        #endregion

        #region Logic to check Start End Time Existance ..
        oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
        oMASSIT_TimeWindowBE.Action = "GetStartEndTimeBasedOnSite";
        oMASSIT_TimeWindowBE.TimeWindowID = intTimeWindowID; //Convert.ToInt32(GetQueryStringValue("TimeWindowID"));

        if (GetQueryStringValue("SiteId") != null)
            oMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteId"));

        if (GetQueryStringValue("SiteDoorNoId") != null)
            oMASSIT_TimeWindowBE.SiteDoorNumberID = Convert.ToInt32(GetQueryStringValue("SiteDoorNoId"));

        if (GetQueryStringValue("WeekDay") != null)
            oMASSIT_TimeWindowBE.Weekday = Convert.ToString(GetQueryStringValue("WeekDay"));

        oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        List<MASSIT_TimeWindowBE> lstAllStartEndTime = oMASSIT_TimeWindowBAL.GetStartEndTimeBasedOnSiteBAL(oMASSIT_TimeWindowBE);
        if (lstAllStartEndTime != null) {
            for (int index = 0; index < lstAllStartEndTime.Count; index++) {
                // Three conditions are being checked here.
                // 1. UI Start time should not be in between or equal of Databse Start and End time.
                // 2. UI End time should not be in between or equal of Databse Start and End time.
                // 3. UI Start time should not be less than or equal to Databse start time and UI End time should not Greater than or equal to Database End time.
                if ( 
                        (Convert.ToInt32(ddlStartTime.SelectedValue) == lstAllStartEndTime[index].StartSlotTimeID)
                    || (Convert.ToInt32(ddlEndTime.SelectedValue) == lstAllStartEndTime[index].EndSlotTimeID)
                    || ((Convert.ToInt32(ddlStartTime.SelectedValue) > lstAllStartEndTime[index].StartSlotTimeID) && (Convert.ToInt32(ddlStartTime.SelectedValue) < lstAllStartEndTime[index].EndSlotTimeID)) // Condition 1
                    || ((Convert.ToInt32(ddlEndTime.SelectedValue) > lstAllStartEndTime[index].StartSlotTimeID) && (Convert.ToInt32(ddlEndTime.SelectedValue) < lstAllStartEndTime[index].EndSlotTimeID)) // Condition 2
                    || ((Convert.ToInt32(ddlStartTime.SelectedValue) < lstAllStartEndTime[index].StartSlotTimeID) && (Convert.ToInt32(ddlEndTime.SelectedValue) > lstAllStartEndTime[index].EndSlotTimeID))) // Condition 3
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + startEndTimeAlreadyExist + "');</script>", false);
                    ddlStartTime.Focus();
                    return;
                }
            }
        }
        #endregion

        #region Total Hrs:Mins Available validation for HH:MM format If textbox is not blank ..
        if (!string.IsNullOrEmpty(txtTotalTime.Text) && !string.IsNullOrWhiteSpace(txtTotalTime.Text)) {
            if (!IsHHMMFormat(txtTotalTime.Text.Trim())) {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + totalHrsMinsFormat + "');</script>", false);
                txtTotalTime.Focus();
                return;
            }

            #region Commented code ..
            //if (!validationFunctions.IsNumeric(txtTotalTime.Text.Trim().Replace(":", string.Empty)))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + totalHrsMinsFormat + "');</script>", false);
            //    txtTotalTime.Focus();
            //    return;
            //}

            //DateTime? dtBefore = (DateTime?)null;
            //if (txtTotalTime.Text.Trim() != string.Empty)
            //{
            //    int intCheckValidTime = -1;
            //    intCheckValidTime = txtTotalTime.Text.Trim().IndexOf(":");
            //    if (intCheckValidTime != (-1))
            //    {
            //        int intHours = Convert.ToInt32(txtTotalTime.Text.Split(':')[0]);
            //        int intMinutes = Convert.ToInt32(txtTotalTime.Text.Split(':')[1]);

            //        if (intHours > 24 || intMinutes > 59)
            //        {
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + totalHrsMinsFormat + "');</script>", false);
            //            txtTotalTime.Focus();
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + totalHrsMinsFormat + "');</script>", false);
            //        txtTotalTime.Focus();
            //        return;
            //    }
            //}
            #endregion
        }
        #endregion

        #region Maximum Time validation for HH:MM format If textbox is not blank ..
        if (!string.IsNullOrEmpty(txtMaximumTime.Text) && !string.IsNullOrWhiteSpace(txtMaximumTime.Text))
        {
            if (!IsHHMMFormat(txtMaximumTime.Text.Trim()))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + totalHrsMinsFormat + "');</script>", false);
                txtMaximumTime.Focus();
                return;
            }
        }
        #endregion

        #region Validation for Include-Exclude same Vendor ..
        ucListBox lstExcludedVendor = (ucListBox)ucSeacrhVendor1.FindControl("lstSelectedVendor");
        ucListBox lstIncludeedVendor = (ucListBox)ucIncludeVendor.FindControl("lstSelectedVendor");

        if (IsListBoxItemExistInAnotherListBox(lstExcludedVendor, lstIncludeedVendor))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + includeExcludeVendorMessage + "');</script>", false);
            return;
        }
        #endregion

        #region Validation for Include-Exclude same Carrier ..
        if (IsListBoxItemExistInAnotherListBox(UclstRightCarrier, uclstIncludeRightCarrier))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + includeExcludeCarrierMessage + "');</script>", false);
            return;
        }
        #endregion

        lstSelectedVendor = (ucListBox)ucSeacrhVendor1.FindControl("lstSelectedVendor");
        lstIncludeSelectedVendor = (ucListBox)ucIncludeVendor.FindControl("lstSelectedVendor");

        oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
        oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();

        if (GetQueryStringValue("TimeWindowID") != null) {
            oMASSIT_TimeWindowBE.Action = "Edit";
            oMASSIT_TimeWindowBE.TimeWindowID = intTimeWindowID;
        }
        else
            oMASSIT_TimeWindowBE.Action = "Add";

        if (GetQueryStringValue("SiteId") != null)
            oMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteId"));

        if (GetQueryStringValue("SiteDoorNoId") != null)
            oMASSIT_TimeWindowBE.SiteDoorNumberID = Convert.ToInt32(GetQueryStringValue("SiteDoorNoId"));

        if (GetQueryStringValue("WeekDay") != null)
            oMASSIT_TimeWindowBE.Weekday = Convert.ToString(GetQueryStringValue("WeekDay"));

        oMASSIT_TimeWindowBE.StartTimeWeekday = this.GetDayOnSelectedTime(ddlStartTime);
        oMASSIT_TimeWindowBE.EndTimeWeekday = this.GetDayOnSelectedTime(ddlEndTime);

        oMASSIT_TimeWindowBE.WindowName = txtWindow.Text.Trim();
        oMASSIT_TimeWindowBE.Priority = Convert.ToInt32(ddlPriority.SelectedItem.Value);
        oMASSIT_TimeWindowBE.StartSlotTimeID = Convert.ToInt32(ddlStartTime.SelectedItem.Value);
        oMASSIT_TimeWindowBE.EndSlotTimeID = Convert.ToInt32(ddlEndTime.SelectedItem.Value);

        if (!string.IsNullOrEmpty(txtMaxDeliveries.Text) && !string.IsNullOrWhiteSpace(txtMaxDeliveries.Text))
            oMASSIT_TimeWindowBE.MaxDeliveries = Convert.ToInt32(txtMaxDeliveries.Text);
        else
            oMASSIT_TimeWindowBE.MaxDeliveries = 0;

        if (!string.IsNullOrEmpty(txtMaxCartons.Text) && !string.IsNullOrWhiteSpace(txtMaxCartons.Text))
            oMASSIT_TimeWindowBE.MaximumCartons = Convert.ToInt32(txtMaxCartons.Text);
        else
            oMASSIT_TimeWindowBE.MaximumCartons = 0;

        if (!string.IsNullOrEmpty(txtMaxLines.Text) && !string.IsNullOrWhiteSpace(txtMaxLines.Text))
            oMASSIT_TimeWindowBE.MaximumLines = Convert.ToInt32(txtMaxLines.Text);
        else
            oMASSIT_TimeWindowBE.MaximumLines = 0;

        if (!string.IsNullOrEmpty(txtMaxPallets.Text) && !string.IsNullOrWhiteSpace(txtMaxPallets.Text))
            oMASSIT_TimeWindowBE.MaximumPallets = Convert.ToInt32(txtMaxPallets.Text);
        else
            oMASSIT_TimeWindowBE.MaximumPallets = 0;

        oMASSIT_TimeWindowBE.IsCheckSKUTable = Convert.ToBoolean(chkSKU.Checked);

        if (!string.IsNullOrEmpty(txtlessthenpallets.Text) && !string.IsNullOrWhiteSpace(txtlessthenpallets.Text))
            oMASSIT_TimeWindowBE.LessVolumePallets = Convert.ToInt32(txtlessthenpallets.Text);
        else

            oMASSIT_TimeWindowBE.LessVolumePallets = 0;

        if (!string.IsNullOrEmpty(txtGreaterthenpallets.Text) && !string.IsNullOrWhiteSpace(txtGreaterthenpallets.Text))
            oMASSIT_TimeWindowBE.GraterVolumePallets = Convert.ToInt32(txtGreaterthenpallets.Text);
        else
            oMASSIT_TimeWindowBE.GraterVolumePallets = 0;

        if (!string.IsNullOrEmpty(txtLessthanLines.Text) && !string.IsNullOrWhiteSpace(txtLessthanLines.Text))
            oMASSIT_TimeWindowBE.LessVolumeLines = Convert.ToInt32(txtLessthanLines.Text);
        else

            oMASSIT_TimeWindowBE.LessVolumeLines = 0;

        if (!string.IsNullOrEmpty(txtGreaterthanLines.Text) && !string.IsNullOrWhiteSpace(txtGreaterthanLines.Text))
            oMASSIT_TimeWindowBE.GraterVolumeLines = Convert.ToInt32(txtGreaterthanLines.Text);
        else
            oMASSIT_TimeWindowBE.GraterVolumeLines = 0;

        oMASSIT_TimeWindowBE.TotalHrsMinsAvailable = txtTotalTime.Text.Trim();
        oMASSIT_TimeWindowBE.MaximumTime = txtMaximumTime.Text.Trim();

        #region Multiple Excluded Vendor/Carrier Ids being set here ..

        /*Carrier Section*/
        if (UclstRightCarrier.Items.Count > 0) {
            if (UclstRightCarrier.Items.Count > 1) {
                oMASSIT_TimeWindowBE.ExcludedCarrierIDs = UclstRightCarrier.Items[0].Value + ",";
                for (int i = 1; i < UclstRightCarrier.Items.Count - 1; i++) {
                    oMASSIT_TimeWindowBE.ExcludedCarrierIDs += UclstRightCarrier.Items[i].Value + ",";
                }
                oMASSIT_TimeWindowBE.ExcludedCarrierIDs += UclstRightCarrier.Items[UclstRightCarrier.Items.Count - 1].Value;
            }
            else {
                oMASSIT_TimeWindowBE.ExcludedCarrierIDs = UclstRightCarrier.Items[0].Value;
            }
        }

        /*Vendor Section*/
        if (lstSelectedVendor.Items.Count > 0) {
            if (lstSelectedVendor.Items.Count > 1) {
                oMASSIT_TimeWindowBE.ExcludedVendorIDs = lstSelectedVendor.Items[0].Value + ",";
                for (int i = 1; i < lstSelectedVendor.Items.Count - 1; i++) {
                    oMASSIT_TimeWindowBE.ExcludedVendorIDs += lstSelectedVendor.Items[i].Value + ",";
                }
                oMASSIT_TimeWindowBE.ExcludedVendorIDs += lstSelectedVendor.Items[lstSelectedVendor.Items.Count - 1].Value;
            }
            else {
                oMASSIT_TimeWindowBE.ExcludedVendorIDs = lstSelectedVendor.Items[0].Value;
            }
        }
        #endregion

        #region Multiple Included Vendor/Carrier Ids being set here ..

        /*Carrier Section*/
        if (uclstIncludeRightCarrier.Items.Count > 0)
        {
            if (uclstIncludeRightCarrier.Items.Count > 1)
            {
                oMASSIT_TimeWindowBE.IncludedCarrierIDs = uclstIncludeRightCarrier.Items[0].Value + ",";
                for (int i = 1; i < uclstIncludeRightCarrier.Items.Count - 1; i++)
                {
                    oMASSIT_TimeWindowBE.IncludedCarrierIDs += uclstIncludeRightCarrier.Items[i].Value + ",";
                }
                oMASSIT_TimeWindowBE.IncludedCarrierIDs += uclstIncludeRightCarrier.Items[uclstIncludeRightCarrier.Items.Count - 1].Value;
            }
            else
            {
                oMASSIT_TimeWindowBE.IncludedCarrierIDs = uclstIncludeRightCarrier.Items[0].Value;
            }
        }

        /*Vendor Section*/
        if (lstIncludeSelectedVendor.Items.Count > 0)
        {
            if (lstIncludeSelectedVendor.Items.Count > 1)
            {
                oMASSIT_TimeWindowBE.IncludedVendorIDs = lstIncludeSelectedVendor.Items[0].Value + ",";
                for (int i = 1; i < lstIncludeSelectedVendor.Items.Count - 1; i++)
                {
                    oMASSIT_TimeWindowBE.IncludedVendorIDs += lstIncludeSelectedVendor.Items[i].Value + ",";
                }
                oMASSIT_TimeWindowBE.IncludedVendorIDs += lstIncludeSelectedVendor.Items[lstIncludeSelectedVendor.Items.Count - 1].Value;
            }
            else
            {
                oMASSIT_TimeWindowBE.IncludedVendorIDs = lstIncludeSelectedVendor.Items[0].Value;
            }
        }
        #endregion

        int? iResult = oMASSIT_TimeWindowBAL.addEditWeekSetupBAL(oMASSIT_TimeWindowBE);
        if (iResult > 0) {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }

        //BindVendors();
        //BindCarriers();

        EncryptQueryString("APPSIT_TimeWindow.aspx");
    }

    private string GetDayOnSelectedTime(ucDropdownList ddlSelectedTime) {
        string strDay = string.Empty;
        // DateTime dtmDdlFirstTime = DateTime.Now;
        //DateTime dtmDdlLastTime = DateTime.Now;
        //DateTime dtmDdlSelStartTime = DateTime.Now;
        //DateTime dtmDdlSelEndTime = DateTime.Now;

        if (ddlSelectedTime.Items.Count > 0) {

            if (ddlSelectedTime.Items.IndexOf(ddlSelectedTime.Items.FindByText("00:00")) > ddlSelectedTime.SelectedIndex)
                strDay = GetPreviousDay(GetQueryStringValue("WeekDay"));
            else
                strDay = GetQueryStringValue("WeekDay");
          
        }
        return strDay;
    }

    private string GetDayOnSelectedTime_old(ucDropdownList ddlSelectedTime) {
        string strDay = string.Empty;
        if (ddlSelectedTime.Items.Count > 0) {
            DateTime dtmCurrentDay12AMTime = DateTime.Now;
            DateTime dtmDdlFirstTime = DateTime.Now;
            DateTime dtmDdlLastTime = DateTime.Now;
            DateTime dtmDdlSelectedTime = DateTime.Now;

            DateTime dtmDdlWeekDay = DateTime.Now;

            dtmCurrentDay12AMTime = Convert.ToDateTime("2013-01-01 00:00:00"); // Current Day 12 AM time.
            dtmDdlFirstTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlSelectedTime.Items[0].Text));
            dtmDdlLastTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlSelectedTime.Items[(ddlSelectedTime.Items.Count - 1)].Text));
            dtmDdlSelectedTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlSelectedTime.SelectedItem));

            if (dtmDdlFirstTime > dtmDdlLastTime && dtmDdlSelectedTime > dtmDdlLastTime)
                dtmDdlWeekDay = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlSelectedTime.SelectedItem)).AddDays(-1);
            else
                dtmDdlWeekDay = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlSelectedTime.SelectedItem));

            if (dtmDdlWeekDay < dtmCurrentDay12AMTime)
                strDay = GetPreviousDay(GetQueryStringValue("WeekDay"));
            else
                strDay = GetQueryStringValue("WeekDay");

        }
        return strDay;
    }

    private bool GetSelectedTimeWithDate(ucDropdownList ddlSelStartTime, ucDropdownList ddlSelEndTime) {
        bool blnStatus = false;
        //DateTime dtmCurrentDay12AMTime = DateTime.Now;
        DateTime dtmDdlFirstTime = DateTime.Now;
        DateTime dtmDdlLastTime = DateTime.Now;
        DateTime dtmDdlSelStartTime = DateTime.Now;
        DateTime dtmDdlSelEndTime = DateTime.Now;

        DateTime dtmDdlWeekDay = DateTime.Now;

        if (ddlSelStartTime.Items.Count > 0) {
            //dtmCurrentDay12AMTime = Convert.ToDateTime("2013-01-01 00:00:00"); // Current Day 12 AM time.

            dtmDdlFirstTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlSelStartTime.Items[0].Text));
            dtmDdlLastTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlSelStartTime.Items[(ddlSelStartTime.Items.Count - 1)].Text));

            dtmDdlSelStartTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlSelStartTime.SelectedItem));
            dtmDdlSelEndTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlSelEndTime.SelectedItem));


            if (dtmDdlFirstTime > dtmDdlLastTime && dtmDdlSelStartTime > dtmDdlLastTime && dtmDdlSelEndTime > dtmDdlLastTime) {
                if (dtmDdlSelStartTime > dtmDdlSelEndTime || dtmDdlSelStartTime == dtmDdlSelEndTime)
                    blnStatus = false;
                else
                    blnStatus = true;
            }
            else if (dtmDdlFirstTime > dtmDdlLastTime && dtmDdlSelStartTime < dtmDdlLastTime && dtmDdlSelEndTime < dtmDdlLastTime) {
                if (dtmDdlSelStartTime > dtmDdlSelEndTime || dtmDdlSelStartTime == dtmDdlSelEndTime)
                    blnStatus = false;
                else
                    blnStatus = true;
            }
            else if (dtmDdlFirstTime > dtmDdlLastTime && dtmDdlSelStartTime > dtmDdlLastTime && dtmDdlSelEndTime < dtmDdlLastTime) {
                blnStatus = true;
            }
            else if (dtmDdlFirstTime > dtmDdlLastTime && dtmDdlSelStartTime < dtmDdlLastTime && dtmDdlSelEndTime > dtmDdlLastTime) {
                blnStatus = false;
            }
            else if (dtmDdlSelStartTime == dtmDdlSelEndTime) {
                blnStatus = false;
            }
        }

        return blnStatus;
    }

    private string GetPreviousDay(string shortDay) {
        string dayName = string.Empty;
        shortDay = shortDay.Trim().ToUpper();
        switch (shortDay) {
            case "MON":
                dayName = "Sun";
                break;
            case "TUE":
                dayName = "Mon";
                break;
            case "WED":
                dayName = "Tue";
                break;
            case "THU":
                dayName = "Wed";
                break;
            case "FRI":
                dayName = "Thu";
                break;
            case "SAT":
                dayName = "Fri";
                break;
            case "SUN":
                dayName = "Sat";
                break;
        }

        return dayName;
    }

    private void SetTimeWindowData(int intTimeWindowID) {
        oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
        oMASSIT_TimeWindowBE.Action = "GetTimeWindowData";
        oMASSIT_TimeWindowBE.TimeWindowID = intTimeWindowID; //Convert.ToInt32(GetQueryStringValue("TimeWindowID"));
        oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        List<MASSIT_TimeWindowBE> lstMASSIT_TimeWindowBE = oMASSIT_TimeWindowBAL.GetTimeWindowBAL(oMASSIT_TimeWindowBE);
        if (lstMASSIT_TimeWindowBE.Count > 0) {
            txtWindow.Text = lstMASSIT_TimeWindowBE[0].WindowName;

            ddlPriority.SelectedIndex =
                ddlPriority.Items.IndexOf(ddlPriority.Items.FindByValue(Convert.ToString(lstMASSIT_TimeWindowBE[0].Priority)));

            ddlStartTime.SelectedIndex =
                ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(Convert.ToString(lstMASSIT_TimeWindowBE[0].StartSlotTimeID)));

            ddlEndTime.SelectedIndex =
                ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(Convert.ToString(lstMASSIT_TimeWindowBE[0].EndSlotTimeID)));

            if (lstMASSIT_TimeWindowBE[0].MaxDeliveries != 0)
                txtMaxDeliveries.Text = Convert.ToString(lstMASSIT_TimeWindowBE[0].MaxDeliveries);
            else
                txtMaxDeliveries.Text = string.Empty;

            if (lstMASSIT_TimeWindowBE[0].MaximumCartons != 0)
                txtMaxCartons.Text = Convert.ToString(lstMASSIT_TimeWindowBE[0].MaximumCartons);
            else
                txtMaxCartons.Text = string.Empty;

            if (lstMASSIT_TimeWindowBE[0].MaximumLines != 0)
                txtMaxLines.Text = Convert.ToString(lstMASSIT_TimeWindowBE[0].MaximumLines);
            else
                txtMaxLines.Text = string.Empty;

            if (lstMASSIT_TimeWindowBE[0].MaximumPallets != 0)
                txtMaxPallets.Text = Convert.ToString(lstMASSIT_TimeWindowBE[0].MaximumPallets);
            else
                txtMaxPallets.Text = string.Empty;

            if (lstMASSIT_TimeWindowBE[0].IsCheckSKUTable)
                chkSKU.Checked = true;
            else
                chkSKU.Checked = false;

            if (lstMASSIT_TimeWindowBE[0].LessVolumePallets != 0)
                txtlessthenpallets.Text = Convert.ToString(lstMASSIT_TimeWindowBE[0].LessVolumePallets);
            else
                txtlessthenpallets.Text = string.Empty;

            if (lstMASSIT_TimeWindowBE[0].GraterVolumePallets != 0)
                txtGreaterthenpallets.Text = Convert.ToString(lstMASSIT_TimeWindowBE[0].GraterVolumePallets);
            else
                txtGreaterthenpallets.Text = string.Empty;


            if (lstMASSIT_TimeWindowBE[0].LessVolumeLines != 0)
                txtLessthanLines.Text = Convert.ToString(lstMASSIT_TimeWindowBE[0].LessVolumeLines);
            else
                txtLessthanLines.Text = string.Empty;

            if (lstMASSIT_TimeWindowBE[0].GraterVolumeLines != 0)
                txtGreaterthanLines.Text = Convert.ToString(lstMASSIT_TimeWindowBE[0].GraterVolumeLines);
            else
                txtGreaterthanLines.Text = string.Empty;


            txtTotalTime.Text = Convert.ToString(lstMASSIT_TimeWindowBE[0].TotalHrsMinsAvailable);
            txtMaximumTime.Text = Convert.ToString(lstMASSIT_TimeWindowBE[0].MaximumTime);

            BindCarriers(lstMASSIT_TimeWindowBE[0].ExcludedCarrierIDs, lstMASSIT_TimeWindowBE[0].ExcludedCarrierNames);
            BindVendors(lstMASSIT_TimeWindowBE[0].ExcludedVendorIDs, lstMASSIT_TimeWindowBE[0].ExcludedVendorNames);

            BindIncludeCarriers(lstMASSIT_TimeWindowBE[0].IncludedCarrierIDs, lstMASSIT_TimeWindowBE[0].IncludedCarrierNames);
            BindIncludeVendors(lstMASSIT_TimeWindowBE[0].IncludedVendorIDs, lstMASSIT_TimeWindowBE[0].IncludedVendorNames);

        }
    }

    private MAS_SiteBE GetSingleSiteSetting(int siteId) {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSingleSiteSetting";
        oMAS_SiteBE.SiteID = siteId;
        MAS_SiteBE localMAS_SiteBE = oMAS_SiteBAL.GetSingleSiteSettingBAL(oMAS_SiteBE);
        return localMAS_SiteBE;
    }

    private bool IsHHMMFormat(string strText, bool isMandatry = false) {
        bool status = true;

        if (!string.IsNullOrEmpty(strText) && !string.IsNullOrWhiteSpace(strText)) {
            if (!validationFunctions.IsNumeric(strText.Trim().Replace(":", string.Empty)))
                status = false;
            if (status) {
                if (strText.Trim() != string.Empty) {
                    int intCheckValidTime = -1;
                    intCheckValidTime = strText.Trim().IndexOf(":");
                    if (intCheckValidTime != (-1)) {
                        int intHours = Convert.ToInt32(strText.Split(':')[0]);
                        int intMinutes = Convert.ToInt32(strText.Split(':')[1]);

                        if (intHours > 99 || intMinutes > 59)
                            status = false;

                        if (intHours == 99 && intMinutes > 0)
                            status = false;
                    }
                    else
                        status = false;
                }
            }
        }
        else
        {
            if(isMandatry)
                status = false;
            else
                status = true;
        }

        return status;
    }

    #endregion

}
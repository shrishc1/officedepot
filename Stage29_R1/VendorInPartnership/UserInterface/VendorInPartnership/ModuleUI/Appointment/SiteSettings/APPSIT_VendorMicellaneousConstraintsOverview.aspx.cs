﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.Collections.Generic;

public partial class APPSIT_VendorMicellaneousConstraintsOverview : CommonPage {

    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ucExportToExcel1.GridViewControl = UcGridView1;
        ucExportToExcel1.FileName = "VendorMiscellaneousConstraintsOverview";
    }

    protected void Page_Load(object sender, EventArgs e) {
        //if (!IsPostBack) {
        //    BindVendorMicellaneousConstraints();
        //}
        //ucExportToExcel1.GridViewControl = UcGridView1;
        //ucExportToExcel1.FileName = "VendorMiscellaneousConstraintsOverview";
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            ddlSite.innerControlddlSite.AutoPostBack = true;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["SelectedSiteID"] != null && !IsPostBack)
        {
            ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Session["SelectedSiteID"].ToString()));
        }
        if (!IsPostBack)
        {
            BindVendorMicellaneousConstraints();
        }
    }

    public override void SiteSelectedIndexChanged()
    {
        Session["SelectedSiteID"] = ddlSite.innerControlddlSite.SelectedItem.Value;
        BindVendorMicellaneousConstraints();
    }

    #region Methods

    protected void BindVendorMicellaneousConstraints() {
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oMASSIT_VendorBAL = new APPSIT_VendorBAL();

        List<MASSIT_VendorBE> lstvendor = new List<MASSIT_VendorBE>();

        oMASSIT_VendorBE.Action = "ShowAll";
        oMASSIT_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        lstvendor = oMASSIT_VendorBAL.GetVendorConstraintsBAL(oMASSIT_VendorBE);
        if (lstvendor.Count > 0)
        {
            if (!string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedItem.Value))
            {
                var lstFilteredData = lstvendor.FindAll(v => v.SiteID == Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value));
                UcGridView1.DataSource = lstFilteredData;
                ViewState["lstSites"] = lstFilteredData;
            }
            else 
            { 
                UcGridView1.DataSource = lstvendor;
                ViewState["lstSites"] = lstvendor;
            }
        }
        else 
        {
            ViewState["lstSites"] = lstvendor;
            UcGridView1.DataSource = null; 
        }
        UcGridView1.DataBind();

        
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASSIT_VendorBE>.SortList((List<MASSIT_VendorBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
    #endregion
}
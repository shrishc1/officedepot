﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;


public partial class APPSIT_WeekSetupOverview : CommonPage {

    protected void Page_InIt(object sender, EventArgs e) {
        ddlSite.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            ClearControls();
        }

        ucExportToExcel.GridViewControl = UcGridView1;
        ucExportToExcel.FileName = "WeekSetupOverview";
    }

    protected void Page_PreRender(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
             if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("SiteID").ToString() != "") {
                 ClearControls();
                 ddlSite.innerControlddlSite.SelectedValue = Convert.ToString(GetQueryStringValue("SiteID"));
                 BindSiteWeekSetups();      
             }
        }
    }

    #region Methods

    public void ClearControls() {
        lblMonStartTime.Text = "-";
        lblMonEndTime.Text = "-";
        lblMonMaxLifts.Text = "-";
        lblMonMAxPallets.Text = "-";
        lblMonMaxLines.Text = "-";
        lblMonMaxContainers.Text = "-";
        lblMonMaxDeliveries.Text = "-";

        lblTueStartTime.Text = "-";
        lblTueEndTime.Text = "-";
        lblTueMaxLifts.Text = "-";
        lblTueMAxPallets.Text = "-";
        lblTueMaxLines.Text = "-";
        lblTueMaxContainers.Text = "-";
        lblTueMaxDeliveries.Text = "-";

        lblWedStartTime.Text = "-";
        lblWedEndTime.Text = "-";
        lblWedMaxLifts.Text = "-";
        lblWedMAxPallets.Text = "-";
        lblWedMaxLines.Text = "-";
        lblWedMaxContainers.Text = "-";
        lblWedMaxDeliveries.Text = "-";

        lblThuStartTime.Text = "-";
        lblThuEndTime.Text = "-";
        lblThuMaxLifts.Text = "-";
        lblThuMAxPallets.Text = "-";
        lblThuMaxLines.Text = "-";
        lblThuMaxContainers.Text = "-";
        lblThuMaxDeliveries.Text = "-";

        lblFriStartTime.Text = "-";
        lblFriEndTime.Text = "-";
        lblFriMaxLifts.Text = "-";
        lblFriMAxPallets.Text = "-";
        lblFriMaxLines.Text = "-";
        lblFriMaxContainers.Text = "-";
        lblFriMaxDeliveries.Text = "-";

        lblSatStartTime.Text = "-";
        lblSatEndTime.Text = "-";
        lblSatMaxLifts.Text = "-";
        lblSatMAxPallets.Text = "-";
        lblSatMaxLines.Text = "-";
        lblSatMaxContainers.Text = "-";
        lblSatMaxDeliveries.Text = "-";

        lblSunStartTime.Text = "-";
        lblSunEndTime.Text = "-";
        lblSunMaxLifts.Text = "-";
        lblSunMAxPallets.Text = "-";
        lblSunMaxLines.Text = "-";
        lblSunMaxContainers.Text = "-";
        lblSunMaxDeliveries.Text = "-";
    }

    public void BindSiteWeekSetups() {
        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();
           
        oMASSIT_WeekSetupBE.Action = "ShowAll";
        oMASSIT_WeekSetupBE.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : (int?)null;
        oMASSIT_WeekSetupBE.ScheduleDate = DateTime.Now.Date;

        List<MASSIT_WeekSetupBE> lstWeekSetups = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
        if (lstWeekSetups != null && lstWeekSetups.Count > 0) {

            UcGridView1.DataSource = lstWeekSetups;
            UcGridView1.DataBind();

            for (int i = 0; i < lstWeekSetups.Count; i++) {
                if (lstWeekSetups[i].EndWeekday == "mon") {
                    if (lstWeekSetups[i].IsDayDisabled == false) {
                        lnkMonday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteScheduleDiaryID=" + lstWeekSetups[i].SiteScheduleDiaryID.ToString()+ "&weekday=mon");
                        if (lstWeekSetups[i].StartTime != null && lstWeekSetups[i].StartTime.ToString() != "")
                            lblMonStartTime.Text = lstWeekSetups[i].StartWeekday.ToUpper() + "-" + lstWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblMonStartTime.Text = "-";

                        if (lstWeekSetups[i].EndTime != null && lstWeekSetups[i].EndTime.ToString() != "")
                            lblMonEndTime.Text = lstWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblMonEndTime.Text = "-";
                        lblMonMaxLifts.Text = lstWeekSetups[i].MaximumLift != null ? lstWeekSetups[i].MaximumLift.ToString() : "-";
                        lblMonMAxPallets.Text = lstWeekSetups[i].MaximumPallet != null ? lstWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblMonMaxLines.Text = lstWeekSetups[i].MaximumLine != null ? lstWeekSetups[i].MaximumLine.ToString() : "-";
                        lblMonMaxContainers.Text = lstWeekSetups[i].MaximumContainer != null ? lstWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblMonMaxDeliveries.Text = lstWeekSetups[i].MaximumDeliveries != null ? lstWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkMonday.Enabled = false;
                    }
                }
                else if (lstWeekSetups[i].EndWeekday == "tue") {
                    if (lstWeekSetups[i].IsDayDisabled == false) {
                        lnkTuesday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteScheduleDiaryID=" + lstWeekSetups[i].SiteScheduleDiaryID.ToString() + "&weekday=tue");
                        if (lstWeekSetups[i].StartTime != null && lstWeekSetups[i].StartTime.ToString() != "")
                            lblTueStartTime.Text = lstWeekSetups[i].StartWeekday.ToUpper() + "-" + lstWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblTueStartTime.Text = "-";

                        if (lstWeekSetups[i].EndTime != null && lstWeekSetups[i].EndTime.ToString() != "")
                            lblTueEndTime.Text = lstWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblTueEndTime.Text = "-";
                        lblTueMaxLifts.Text = lstWeekSetups[i].MaximumLift != null ? lstWeekSetups[i].MaximumLift.ToString() : "-";
                        lblTueMAxPallets.Text = lstWeekSetups[i].MaximumPallet != null ? lstWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblTueMaxLines.Text = lstWeekSetups[i].MaximumLine != null ? lstWeekSetups[i].MaximumLine.ToString() : "-";
                        lblTueMaxContainers.Text = lstWeekSetups[i].MaximumContainer != null ? lstWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblTueMaxDeliveries.Text = lstWeekSetups[i].MaximumDeliveries != null ? lstWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkTuesday.Enabled = false;
                    }
                }
                else if (lstWeekSetups[i].EndWeekday == "wed") {
                    if (lstWeekSetups[i].IsDayDisabled == false) {
                        lnkWednesday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteScheduleDiaryID=" + lstWeekSetups[i].SiteScheduleDiaryID.ToString() + "&weekday=wed");
                        if (lstWeekSetups[i].StartTime != null && lstWeekSetups[i].StartTime.ToString() != "")
                            lblWedStartTime.Text = lstWeekSetups[i].StartWeekday.ToUpper() + "-" + lstWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblWedStartTime.Text = "-";

                        if (lstWeekSetups[i].EndTime != null && lstWeekSetups[i].EndTime.ToString() != "")
                            lblWedEndTime.Text = lstWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblWedEndTime.Text = "-";

                        lblWedMaxLifts.Text = lstWeekSetups[i].MaximumLift != null ? lstWeekSetups[i].MaximumLift.ToString() : "-";
                        lblWedMAxPallets.Text = lstWeekSetups[i].MaximumPallet != null ? lstWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblWedMaxLines.Text = lstWeekSetups[i].MaximumLine != null ? lstWeekSetups[i].MaximumLine.ToString() : "-";
                        lblWedMaxContainers.Text = lstWeekSetups[i].MaximumContainer != null ? lstWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblWedMaxDeliveries.Text = lstWeekSetups[i].MaximumDeliveries != null ? lstWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkWednesday.Enabled = false;
                    }
                }
                else if (lstWeekSetups[i].EndWeekday == "thu") {
                    if (lstWeekSetups[i].IsDayDisabled == false) {
                        lnkThursday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteScheduleDiaryID=" + lstWeekSetups[i].SiteScheduleDiaryID.ToString() + "&weekday=thu");
                        if (lstWeekSetups[i].StartTime != null && lstWeekSetups[i].StartTime.ToString() != "")
                            lblThuStartTime.Text = lstWeekSetups[i].StartWeekday.ToUpper() + "-" + lstWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblThuStartTime.Text = "-";

                        if (lstWeekSetups[i].EndTime != null && lstWeekSetups[i].EndTime.ToString() != "")
                            lblThuEndTime.Text = lstWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblThuEndTime.Text = "-";

                        lblThuMaxLifts.Text = lstWeekSetups[i].MaximumLift != null ? lstWeekSetups[i].MaximumLift.ToString() : "-";
                        lblThuMAxPallets.Text = lstWeekSetups[i].MaximumPallet != null ? lstWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblThuMaxLines.Text = lstWeekSetups[i].MaximumLine != null ? lstWeekSetups[i].MaximumLine.ToString() : "-";
                        lblThuMaxContainers.Text = lstWeekSetups[i].MaximumContainer != null ? lstWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblThuMaxDeliveries.Text = lstWeekSetups[i].MaximumDeliveries != null ? lstWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkThursday.Enabled = false;
                    }
                }
                else if (lstWeekSetups[i].EndWeekday == "fri") {
                    if (lstWeekSetups[i].IsDayDisabled == false) {
                        lnkFriday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteScheduleDiaryID=" + lstWeekSetups[i].SiteScheduleDiaryID.ToString() + "&weekday=fri");
                        if (lstWeekSetups[i].StartTime != null && lstWeekSetups[i].StartTime.ToString() != "")
                            lblFriStartTime.Text = lstWeekSetups[i].StartWeekday.ToUpper() + "-" + lstWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblFriStartTime.Text = "-";

                        if (lstWeekSetups[i].EndTime != null && lstWeekSetups[i].EndTime.ToString() != "")
                            lblFriEndTime.Text = lstWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblFriEndTime.Text = "-";

                        lblFriMaxLifts.Text = lstWeekSetups[i].MaximumLift != null ? lstWeekSetups[i].MaximumLift.ToString() : "-";
                        lblFriMAxPallets.Text = lstWeekSetups[i].MaximumPallet != null ? lstWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblFriMaxLines.Text = lstWeekSetups[i].MaximumLine != null ? lstWeekSetups[i].MaximumLine.ToString() : "-";
                        lblFriMaxContainers.Text = lstWeekSetups[i].MaximumContainer != null ? lstWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblFriMaxDeliveries.Text = lstWeekSetups[i].MaximumDeliveries != null ? lstWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkFriday.Enabled = false;
                    }
                }
                else if (lstWeekSetups[i].EndWeekday == "sat") {
                    if (lstWeekSetups[i].IsDayDisabled == false) {
                        lnkSaturday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteScheduleDiaryID=" + lstWeekSetups[i].SiteScheduleDiaryID.ToString() + "&weekday=sat");
                        if (lstWeekSetups[i].StartTime != null && lstWeekSetups[i].StartTime.ToString() != "")
                            lblSatStartTime.Text = lstWeekSetups[i].StartWeekday.ToUpper() + "-" + lstWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblSatStartTime.Text = "-";

                        if (lstWeekSetups[i].EndTime != null && lstWeekSetups[i].EndTime.ToString() != "")
                            lblSatEndTime.Text = lstWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblSatEndTime.Text = "-";

                        lblSatMaxLifts.Text = lstWeekSetups[i].MaximumLift != null ? lstWeekSetups[i].MaximumLift.ToString() : "-";
                        lblSatMAxPallets.Text = lstWeekSetups[i].MaximumPallet != null ? lstWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblSatMaxLines.Text = lstWeekSetups[i].MaximumLine != null ? lstWeekSetups[i].MaximumLine.ToString() : "-";
                        lblSatMaxContainers.Text = lstWeekSetups[i].MaximumContainer != null ? lstWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblSatMaxDeliveries.Text = lstWeekSetups[i].MaximumDeliveries != null ? lstWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkSaturday.Enabled = false;
                    }
                }
                else if (lstWeekSetups[i].EndWeekday == "sun") {
                    if (lstWeekSetups[i].IsDayDisabled == false) {
                        lnkSunday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteScheduleDiaryID=" + lstWeekSetups[i].SiteScheduleDiaryID.ToString() + "&weekday=sun");
                        if (lstWeekSetups[i].StartTime != null && lstWeekSetups[i].StartTime.ToString() != "")
                            lblSunStartTime.Text = lstWeekSetups[i].StartWeekday.ToUpper() + "-" + lstWeekSetups[i].StartTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].StartTime.Value.ToString("mm");
                        else
                            lblSunStartTime.Text = "-";

                        if (lstWeekSetups[i].EndTime != null && lstWeekSetups[i].EndTime.ToString() != "")
                            lblSunEndTime.Text = lstWeekSetups[i].EndTime.Value.Hour.ToString() + ":" + lstWeekSetups[i].EndTime.Value.ToString("mm");
                        else
                            lblSunEndTime.Text = "-";

                        lblSunMaxLifts.Text = lstWeekSetups[i].MaximumLift != null ? lstWeekSetups[i].MaximumLift.ToString() : "-";
                        lblSunMAxPallets.Text = lstWeekSetups[i].MaximumPallet != null ? lstWeekSetups[i].MaximumPallet.ToString() : "-";
                        lblSunMaxLines.Text = lstWeekSetups[i].MaximumLine != null ? lstWeekSetups[i].MaximumLine.ToString() : "-";
                        lblSunMaxContainers.Text = lstWeekSetups[i].MaximumContainer != null ? lstWeekSetups[i].MaximumContainer.ToString() : "-";
                        lblSunMaxDeliveries.Text = lstWeekSetups[i].MaximumDeliveries != null ? lstWeekSetups[i].MaximumDeliveries.ToString() : "-";
                    }
                    else {
                        lnkSunday.Enabled = false;
                    }
                }
            }

            string[] weekArray = { "mon", "tue", "wed", "thu", "fri", "sat", "sun" };
            for (int i = 0; i < weekArray.Length; i++) {
                int count = 0;
                for (int j = 0; j < lstWeekSetups.Count; j++) {
                    if (weekArray[i].ToString() == lstWeekSetups[j].EndWeekday.ToString()) {
                        count = 0;
                        break;
                    }
                    else {
                        count++;
                    }
                }
                if (count > 0) {
                    if (weekArray[i].ToString() == "mon") {
                        lnkMonday.PostBackUrl =EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString()+"&weekday=mon");
                    }
                    else if (weekArray[i].ToString() == "tue") {
                        lnkTuesday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&weekday=tue");
                    }
                    else if (weekArray[i].ToString() == "wed") {
                        lnkWednesday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&weekday=wed");
                    }
                    else if (weekArray[i].ToString() == "thu") {
                        lnkThursday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&weekday=thu");
                    }
                    else if (weekArray[i].ToString() == "fri") {
                        lnkFriday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&weekday=fri");
                    }
                    else if (weekArray[i].ToString() == "sat") {
                        lnkSaturday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&weekday=sat");
                    }
                    else if (weekArray[i].ToString() == "sun") {
                        lnkSunday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&weekday=sun");
                    }
                }
            }        
        }
        else {
            lnkMonday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&weekday=mon");
            lnkTuesday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&weekday=tue");
            lnkWednesday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&weekday=wed");
            lnkThursday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&weekday=thu");
            lnkFriday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&weekday=fri");
            lnkSaturday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&weekday=sat");
            lnkSunday.PostBackUrl = EncryptURL("APPSIT_WeekSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value.ToString() + "&weekday=sun");
        }
    }
   
    #endregion

    #region Events

    public override void SitePost_Load() {
        base.SitePost_Load();
        if (!IsPostBack) {
            ddlSite.innerControlddlSite.AutoPostBack = true;
            if (GetQueryStringValue("SiteID") == null && string.IsNullOrEmpty(GetQueryStringValue("SiteID"))) {
                BindSiteWeekSetups();
            }
        }
    }

    public override void SiteSelectedIndexChanged() {        
        ClearControls();
        BindSiteWeekSetups();  
    }
    protected void btnWeekSetup_Click(object sender, CommandEventArgs e) {
        if (e.CommandArgument != null && e.CommandArgument.ToString() != "") {
            EncryptQueryString("APPSIT_WeekSetupEdit.aspx?SiteScheduleDiaryID=" + e.CommandArgument.ToString());
        }
    }    
    #endregion
    protected void btnSpecificWeekSetup_Click(object sender, EventArgs e) {
        EncryptQueryString("APPSIT_SpecificWeekSetupOverview.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value);
    }
}
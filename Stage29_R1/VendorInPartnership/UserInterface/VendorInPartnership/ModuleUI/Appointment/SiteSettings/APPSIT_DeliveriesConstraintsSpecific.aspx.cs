﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;


public partial class APPSIT_DeliveriesConstraintsSpecific : CommonPage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            hdnWeekDay.Value = "Mon";
            ucpnlTimeWindow.GroupingText = "Time Window For" + " " + "Monday";
            if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("SiteID").ToString() != "") {
                SiteNameForDCSpecific.Text = GetSiteName(Convert.ToInt32(GetQueryStringValue("SiteID")));
                //BindDeliveryConstraint();
                //BindTotals();
            }
        }
    }

    #region Methods

    public string GetSiteName(int pSiteID) {
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        oMAS_SiteBE.Action = "ShowAll";

        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMAS_SiteBE.SiteCountryID = 0;

        List<MAS_SiteBE> lstSite = new List<MAS_SiteBE>();
        lstSite = oMAS_SiteBAL.GetSiteBAL(oMAS_SiteBE);
        string strSiteName = string.Empty;
        if (lstSite.Count > 0) {
            strSiteName = lstSite[lstSite.FindIndex(s => s.SiteID == pSiteID)].SiteDescription.ToString();
        }
        return strSiteName;
    }

    private void BindDeliveryConstraint() {
        string errorMeesage = string.Empty;
        if (txtDate.innerControltxtDate.Value != string.Empty) {
            DateTime dtSelectedDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);
            if (dtSelectedDate.DayOfWeek != DayOfWeek.Monday) {

                //Choose monday
                errorMeesage = WebCommon.getGlobalResourceValue("SelectedDayMonday");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
                return;
            }
        }
        else if (txtDate.innerControltxtDate.Value == string.Empty) {

            //Select a date
            errorMeesage = WebCommon.getGlobalResourceValue("DateRequired");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
            return;
        }
        if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("SiteID").ToString() != "") {

            //check in the week setup specific table for the selected date and weekday.
            DateTime dtSelectedDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);

            MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE = new MASSIT_SpecificWeekSetupBE();
            APPSIT_SpecificWeekSetupBAL oAPPSIT_SpecificWeekSetupBAL = new APPSIT_SpecificWeekSetupBAL();

            oMASSIT_SpecificWeekSetupBE.Action = "ShowAll";

            oMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
            oMASSIT_SpecificWeekSetupBE.WeekStartDate = dtSelectedDate;
            oMASSIT_SpecificWeekSetupBE.EndWeekday = hdnWeekDay.Value;
            List<MASSIT_SpecificWeekSetupBE> lstWeekSetupSpecific = oAPPSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);

            if (lstWeekSetupSpecific.Count <= 0) {

                //specific week setup data does not exists
                errorMeesage = WebCommon.getGlobalResourceValue("SpecificWeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert3", "alert('" + errorMeesage + "')", true);
                return;
            }
            else if (lstWeekSetupSpecific[0].StartTime == null) {

                //specific week setup data does not exists because start time is not defined for the selected date and day.
                errorMeesage = WebCommon.getGlobalResourceValue("SpecificWeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert4", "alert('" + errorMeesage + "')", true);
                return;
            }

            //First copy the delivery constraint data for the specific delivery constraint if exists.
            APPSIT_DeliveryConstraintSpecificBAL oAPPSIT_DeliveryConstraintSpecificBAL = new APPSIT_DeliveryConstraintSpecificBAL();
            MASSIT_DeliveryConstraintSpecificBE oMASSIT_DeliveryConstraintSpecificBE = new MASSIT_DeliveryConstraintSpecificBE();

            oMASSIT_DeliveryConstraintSpecificBE.Action = "CopyGenericData";
            oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint = new MASSIT_DeliveryConstraintBE();
            oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
            oMASSIT_DeliveryConstraintSpecificBE.WeekStartDate = dtSelectedDate;
            oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.Weekday = hdnWeekDay.Value;

            oAPPSIT_DeliveryConstraintSpecificBAL.addEdiDeliveryConstraintSpecificBAL(oMASSIT_DeliveryConstraintSpecificBE);

            //---------------------------------------------------------
            DataTable myDataTable = new DataTable();

            DataColumn myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "TimeSlot";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "StartWeekday";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "StartTime";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "RestrictnumberofDeliveries";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "MaximumNumberofLines";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "MaximumNumberofLifts";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "Restrictsingledeliverypallets";
            myDataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.ColumnName = "Restrictsingledeliverylines";
            myDataTable.Columns.Add(myDataColumn);

            string sWeekDay = string.Empty;

            //---------------------------------------------------------
            for (int iCount = 0; iCount < 24; iCount++) {

                //----------------------------
                sWeekDay = lstWeekSetupSpecific[0].StartWeekday;
                int iStartTime = lstWeekSetupSpecific[0].StartTime.Value.Hour;
                int iCounterStart = iStartTime;
                if (iStartTime == 0) {
                    iStartTime = 24;
                    iCounterStart = 0;                    
                }

                int iEndTime = lstWeekSetupSpecific[0].EndTime.Value.Hour;
                int iCountEnd = iEndTime;
                if (iEndTime == 0) {
                    iEndTime = 24;
                    iCountEnd = 0;                    
                }

                if (lstWeekSetupSpecific[0].StartWeekday != lstWeekSetupSpecific[0].EndWeekday) {
                    iEndTime = 24;
                    iCountEnd = 0;                    
                }
                //-------------------------------------------------

                if (iCount >= iCounterStart && iCount < iEndTime) {
                    DataRow dataRow = myDataTable.NewRow();
                    dataRow["TimeSlot"] = GetHourFormat(iCount.ToString()) + " - " + GetHourFormat((iCount + 1).ToString());
                    dataRow["StartTime"] = iCount;
                    dataRow["StartWeekday"] = sWeekDay; 

                    oMASSIT_DeliveryConstraintSpecificBE.Action = "GetDeliveryConstraintSpecific";
                    oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint = new MASSIT_DeliveryConstraintBE();
                    oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
                    oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.Weekday = hdnWeekDay.Value;
                    oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartTime = iCount;
                    oMASSIT_DeliveryConstraintSpecificBE.WeekStartDate = dtSelectedDate;
                    oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartWeekday = lstWeekSetupSpecific[0].StartWeekday;

                    List<MASSIT_DeliveryConstraintSpecificBE> lstDeliveryConstraintSpecific = oAPPSIT_DeliveryConstraintSpecificBAL.GetDeliveryConstraintSpecificBAL(oMASSIT_DeliveryConstraintSpecificBE);

                    

                    if (lstDeliveryConstraintSpecific.Count > 0) {
                        dataRow["RestrictnumberofDeliveries"] = lstDeliveryConstraintSpecific[0].DeliveryConstraint.RestrictDelivery;
                        dataRow["MaximumNumberofLines"] = lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLine;
                        dataRow["MaximumNumberofLifts"] = lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLift;
                        dataRow["Restrictsingledeliverypallets"] = lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryPalletRestriction;
                        dataRow["Restrictsingledeliverylines"] = lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryLineRestriction;

                    }
                    myDataTable.Rows.Add(dataRow);
                }
            }

         
            //---------------------------------------------------------
            if (lstWeekSetupSpecific[0].StartWeekday != lstWeekSetupSpecific[0].EndWeekday) {
                for (int iCount = 0; iCount < 24; iCount++) {

                    //----------------------------
                    sWeekDay = lstWeekSetupSpecific[0].EndWeekday;
                    int iEndTime = lstWeekSetupSpecific[0].EndTime.Value.Hour;
                    if (iEndTime == 0) {
                        iEndTime = 00;
                        //sWeekDay = lstWeekSetupSpecific[0].EndWeekday;
                    }


                    //-------------------------------------------------

                    if (iCount < iEndTime) {
                        DataRow dataRow = myDataTable.NewRow();
                        dataRow["TimeSlot"] = GetHourFormat(iCount.ToString()) + " - " + GetHourFormat((iCount + 1).ToString());
                        dataRow["StartTime"] = iCount;
                        dataRow["StartWeekday"] = sWeekDay;

                        oMASSIT_DeliveryConstraintSpecificBE.Action = "GetDeliveryConstraintSpecific";
                        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint = new MASSIT_DeliveryConstraintBE();
                        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
                        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.Weekday = hdnWeekDay.Value;
                        oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartTime = iCount;
                        oMASSIT_DeliveryConstraintSpecificBE.WeekStartDate = dtSelectedDate;
                        //oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartWeekday = hdnWeekDay.Value;

                        List<MASSIT_DeliveryConstraintSpecificBE> lstDeliveryConstraintSpecific = oAPPSIT_DeliveryConstraintSpecificBAL.GetDeliveryConstraintSpecificBAL(oMASSIT_DeliveryConstraintSpecificBE);
                        
                        if (lstDeliveryConstraintSpecific.Count > 0) {                            
                            dataRow["RestrictnumberofDeliveries"] = lstDeliveryConstraintSpecific[0].DeliveryConstraint.RestrictDelivery;
                            dataRow["MaximumNumberofLines"] = lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLine;
                            dataRow["MaximumNumberofLifts"] = lstDeliveryConstraintSpecific[0].DeliveryConstraint.MaximumLift;
                            dataRow["Restrictsingledeliverypallets"] = lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryPalletRestriction;
                            dataRow["Restrictsingledeliverylines"] = lstDeliveryConstraintSpecific[0].DeliveryConstraint.SingleDeliveryLineRestriction;

                        }
                        myDataTable.Rows.Add(dataRow);
                    }
                }
            }
            //---------------------------------------------------------
            GridView1.DataSource = myDataTable;
            GridView1.DataBind();
            ViewState["myDataTable"] = myDataTable;
        }        
    }

    public string GetHourFormat(string HourMin) {
        if (HourMin == "24")
            HourMin = "00";

        if (HourMin.Length < 2)
            return "0" + HourMin + ":00";
        else
            return HourMin + ":00";
    }

    #endregion

    #region Events

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e) {
        if (e.CommandName == "Update") {
            APPSIT_DeliveryConstraintSpecificBAL oAPPSIT_DeliveryConstraintSpecificBAL = new APPSIT_DeliveryConstraintSpecificBAL();
            MASSIT_DeliveryConstraintSpecificBE oMASSIT_DeliveryConstraintSpecificBE = new MASSIT_DeliveryConstraintSpecificBE();

            int index = Convert.ToInt32(e.CommandArgument);

            oMASSIT_DeliveryConstraintSpecificBE.Action = "Add";
            oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint = new MASSIT_DeliveryConstraintBE();
            TextBox txtRestrictDeliveries = (TextBox)GridView1.Rows[index].FindControl("txtRestrictDeliveries");
            oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.RestrictDelivery = txtRestrictDeliveries.Text != string.Empty ? Convert.ToInt32(txtRestrictDeliveries.Text) : (int?)null;

            TextBox txtMaximumLines = (TextBox)GridView1.Rows[index].FindControl("txtMaximumLines");
            oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.MaximumLine = txtMaximumLines.Text != string.Empty ? Convert.ToInt32(txtMaximumLines.Text) : (int?)null;

            TextBox txtMaximumLifts = (TextBox)GridView1.Rows[index].FindControl("txtMaximumLifts");
            oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.MaximumLift = txtMaximumLifts.Text != string.Empty ? Convert.ToInt32(txtMaximumLifts.Text) : (int?)null;

            TextBox txtRestrictsingledeliverypallets = (TextBox)GridView1.Rows[index].FindControl("txtRestrictsingledeliverypallets");
            oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SingleDeliveryPalletRestriction = txtRestrictsingledeliverypallets.Text != string.Empty ? Convert.ToInt32(txtRestrictsingledeliverypallets.Text) : (int?)null;

            TextBox txtRestrictsingledeliverylines = (TextBox)GridView1.Rows[index].FindControl("txtRestrictsingledeliverylines");
            oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SingleDeliveryLineRestriction = txtRestrictsingledeliverylines.Text != string.Empty ? Convert.ToInt32(txtRestrictsingledeliverylines.Text) : (int?)null;

            HiddenField hdnStartTime = (HiddenField)GridView1.Rows[index].FindControl("hdnStartTime");
            oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartTime = Convert.ToInt32(hdnStartTime.Value);

            oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));

            oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.Weekday = hdnWeekDay.Value;

            oMASSIT_DeliveryConstraintSpecificBE.WeekStartDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);

            HiddenField hdnStartWeekday = (HiddenField)GridView1.Rows[index].FindControl("hdnStartWeekday");
            oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartWeekday = hdnStartWeekday.Value;

            oAPPSIT_DeliveryConstraintSpecificBAL.addEdiDeliveryConstraintSpecificBAL(oMASSIT_DeliveryConstraintSpecificBE);

        }
        GridView1.EditIndex = -1;
        BindDeliveryConstraint();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        GridView1.PageIndex = e.NewPageIndex;
        BindDeliveryConstraint();
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e) {
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e) {
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e) {
        GridView1.EditIndex = e.NewEditIndex;
        BindDeliveryConstraint();
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e) {

    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e) {
        GridView1.EditIndex = -1;     
    }

    protected void btnWeekday_Click(object sender, EventArgs e) {
        hdnWeekDay.Value = ((System.Web.UI.WebControls.Button)(sender)).CommandArgument;
        ucpnlTimeWindow.GroupingText = "Time Window For" + " " + ((Button)(sender)).Text;
        BindDeliveryConstraint();
    }

    private void BindTotals() {
        MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE = new MASSIT_SpecificWeekSetupBE();
        APPSIT_SpecificWeekSetupBAL oAPPSIT_SpecificWeekSetupBAL = new APPSIT_SpecificWeekSetupBAL();

        oMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        oMASSIT_SpecificWeekSetupBE.WeekStartDate = Common.GetMM_DD_YYYY(txtDate.innerControltxtDate.Value);
        List<MASSIT_SpecificWeekSetupBE> lstWeekSetupSpecific = oAPPSIT_SpecificWeekSetupBAL.GetTotlasBAL(oMASSIT_SpecificWeekSetupBE);

        if (lstWeekSetupSpecific.Count > 0) {
            ltTotalLifts.Text = lstWeekSetupSpecific[0].TOTAL_MaximumLift.ToString();
            ltTotalLines.Text = lstWeekSetupSpecific[0].TOTAL_MaximumLine.ToString();
        }
    }
    
    protected void btnGo_Click(object sender, EventArgs e) {
        BindDeliveryConstraint();
        BindTotals();
    }

    #endregion
    protected void GridView1_Sorting1(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["myDataTable"] != null)
        {
            DataTable myDataTable = (DataTable)ViewState["myDataTable"];
            ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";
            DataView view = myDataTable.DefaultView;
            view.Sort = e.SortExpression + " " + Convert.ToString(ViewState["sortDirection"]);
            GridView1.DataSource = view;
            GridView1.DataBind();

        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_WeekSetupEdit.aspx.cs" Inherits="APPSIT_WeekSetupEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="sm1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblWeekSetup" runat="server" Text="Week Setup"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div>
                    <%--<asp:RequiredFieldValidator ID="rfvStartTimeRequired"
                        runat="server"
                        ControlToValidate="txtStartTime"
                        Display="None" ValidationGroup="a">
                    </asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvEndTimeRequired"
                        runat="server"
                        ControlToValidate="txtEndTime"
                        Display="None" ValidationGroup="a">
                    </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revCorrectStartTime"
                        runat="server" ControlToValidate="txtStartTime"
                        Display="None" SetFocusOnError="true"
                        ValidationGroup="a"
                        ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$">
                    </asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="revCorrectEndTime"
                        runat="server" ControlToValidate="txtEndTime"
                        Display="None" SetFocusOnError="true"
                        ValidationGroup="a"
                        ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$">
                    </asp:RegularExpressionValidator>       --%>
                    <asp:CustomValidator ID="cusvEndTimeValidationNotZero" runat="server" ClientValidationFunction="CompareEndTimeNotZero"
                        Display="None" ValidationGroup="a">
                    </asp:CustomValidator>
                    <asp:CustomValidator ID="cusvStartEndTimeValidation" runat="server" ClientValidationFunction="CompareStartEndTime"
                        Display="None" ValidationGroup="a">
                    </asp:CustomValidator>
                    <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                        Style="color: Red" ValidationGroup="a" />
                </div>
                <div class="formbox">
                    <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold; width: 120px">
                                <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 5px">:
                            </td>
                            <td width="275px">
                                <cc1:ucLabel ID="LBLSite" runat="server" Text="Site Prefix Name"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblSiteWeekSetupStart" runat="server" Text="Start" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">:
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucDropdownList ID="ddlStartWeek" Width="100px" runat="server">
                                    <asp:ListItem Text="liMon" Value="mon">Monday</asp:ListItem>
                                    <asp:ListItem Text="liMon" Value="tue">Tuesday</asp:ListItem>
                                    <asp:ListItem Text="liMon" Value="wed">Wednesday</asp:ListItem>
                                    <asp:ListItem Text="liMon" Value="thu">Thursday</asp:ListItem>
                                    <asp:ListItem Text="liMon" Value="fri">Friday</asp:ListItem>
                                    <asp:ListItem Text="liMon" Value="sat">Saturday</asp:ListItem>
                                    <asp:ListItem Text="liMon" Value="sun">Sunday</asp:ListItem>
                                </cc1:ucDropdownList>
                                &nbsp;
                                <%--<cc1:ucTextbox ID="txtStartTime" runat="server" Width="40px"></cc1:ucTextbox>--%>
                                <cc1:ucDropdownList ID="ddlStartTime" runat="server" Width="60px">
                                </cc1:ucDropdownList>
                                &nbsp;
                                <cc1:ucLabel ID="lblHHMM1" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblSiteWeekSetupEnd" runat="server" Text="End" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">:
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucDropdownList ID="ddlEndWeek" runat="server" Width="100px">
                                    <asp:ListItem Value="mon">Monday</asp:ListItem>
                                    <asp:ListItem Value="tue">Tuesday</asp:ListItem>
                                    <asp:ListItem Value="wed">Wednesday</asp:ListItem>
                                    <asp:ListItem Value="thu">Thursday</asp:ListItem>
                                    <asp:ListItem Value="fri">Friday</asp:ListItem>
                                    <asp:ListItem Value="sat">Saturday</asp:ListItem>
                                    <asp:ListItem Value="sun">Sunday</asp:ListItem>
                                </cc1:ucDropdownList>
                                &nbsp;
                                <%--<cc1:ucTextbox ID="txtEndTime" runat="server" Width="40px"></cc1:ucTextbox>--%>
                                <cc1:ucDropdownList ID="ddlEndTime" runat="server" Width="60px">
                                </cc1:ucDropdownList>
                                &nbsp;
                                <cc1:ucLabel ID="lblHHMM2" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <cc1:ucLabel ID="lblMaximumLifts" runat="server" Text="Maximum Lifts"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold">:
                            </td>
                            <td>
                                <cc1:ucNumericTextbox ID="txtMaximumLift" runat="server" Width="47px" MaxLength="5"
                                    onkeyup="AllowNumbersOnly(this)"></cc1:ucNumericTextbox>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblMaximumPallets" runat="server" Text="Maximum Pallets" Style="font-weight: bold"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold">:
                            </td>
                            <td>
                                <cc1:ucNumericTextbox ID="txtMaximumPallets" runat="server" Width="47px" MaxLength="5"
                                    onkeyup="AllowNumbersOnly(this)"></cc1:ucNumericTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblMaximumLines" runat="server" Text="Maximum Lines" Style="font-weight: bold"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold">:
                            </td>
                            <td>
                                <cc1:ucNumericTextbox ID="txtMaxLines" runat="server" Width="47px" MaxLength="5"
                                    onkeyup="AllowNumbersOnly(this)"></cc1:ucNumericTextbox>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblMaxDeliveries" runat="server" Text="Max Deliveries"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold">:
                            </td>
                            <td>
                                <cc1:ucNumericTextbox ID="txtMaxDeliveries" runat="server" Width="47px" MaxLength="5"
                                    onkeyup="AllowNumbersOnly(this)"></cc1:ucNumericTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblMaxContainers" runat="server" Text="Maximum Containers" Style="font-weight: bold"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold">:
                            </td>
                            <td>
                                <cc1:ucNumericTextbox ID="txtMaxContainers" runat="server" Width="47px" onkeyup="AllowNumbersOnly(this)"></cc1:ucNumericTextbox>
                            </td>
                            <td colspan="3">&nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" ValidationGroup="a"
                    OnClick="btnSave_Click" />
                <cc1:ucButton ID="btnClear" runat="server" Text="Clear" CssClass="button" OnClick="btnClear_Click"
                    OnClientClick="javascript:return AreYouSure();" />
                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                <input type="hidden" id="hidSiteID" runat="server" value="" />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function CompareStartEndTime(sender, args) {
            var objddlStart = document.getElementById('<%=ddlStartWeek.ClientID %>');
            var objddlEnd = document.getElementById('<%=ddlEndWeek.ClientID %>');
            if (objddlStart.options[objddlStart.selectedIndex].value == objddlEnd.options[objddlEnd.selectedIndex].value) {
                obj1 = document.getElementById('<%=ddlStartTime.ClientID %>');
                obj2 = document.getElementById('<%=ddlEndTime.ClientID %>');
                val1 = obj1.options[obj1.selectedIndex].text;
                val2 = obj2.options[obj2.selectedIndex].text;
                if (dateCompare(val1, val2) == 1) {
                    args.IsValid = false;
                }
            }
        }
        function CompareEndTimeNotZero(sender, args) {
            var objddlEnd = document.getElementById('<%=ddlEndTime.ClientID %>');
            if (objddlEnd.options[objddlEnd.selectedIndex].text == '00:00') {
                args.IsValid = false;
            }
        }
        function AreYouSure() {
            var mess = window.confirm('<%=ConfirmMessage%>');
            if (mess == true) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="APPSIT_SkuDoorMatrixSetupEdit.aspx.cs" Inherits="APPSIT_SkuDoorMatrixSetupEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
        
    </script>
    <h2>
        <asp:HiddenField ID="hdnSiteID" runat="server" />
        <cc1:ucLabel ID="lblODSKUDoorMatrixSetup" runat="server" Text="OD SKU Door Matrix Setup"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div>
            <asp:RequiredFieldValidator ID="rfvOdSkuNumberRequired" runat="server" ControlToValidate="txtOdSkuNumber"
                Display="None" ValidationGroup="a" SetFocusOnError="true" ErrorMessage="Please enter OD SKU #.">
            </asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvDoorNameRequired" runat="server" ControlToValidate="ddlDoorName"
                Display="None" InitialValue="0" ValidationGroup="a" SetFocusOnError="true">
            </asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                Style="color: Red" ValidationGroup="a" />
        </div>
        <div class="formbox">
            <table width="35%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 10%">
                    </td>
                    <td style="font-weight: bold; width: 30%">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5%">
                        :
                    </td>
                    <td style="width: 45%">
                        <cc2:ucSite ID="ddlSite" runat="server" />
                    </td>
                    <td style="width: 10%">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblOdSkuNumber" runat="server" Text="OD SKU #" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucTextbox ID="txtOdSkuNumber" ValidationGroup="a" runat="server" Width="143px"
                            MaxLength="15"></cc1:ucTextbox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblDoorName" runat="server" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucDropdownList ID="ddlDoorName" ValidationGroup="a" runat="server" Width="143px">
                        </cc1:ucDropdownList>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
            ValidationGroup="a" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click"
            OnClientClick="return confirmDelete();" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_FixedSlotSetupEdit.aspx.cs" Inherits="APPSIT_MiscellaneousSettingsEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }


        function chkVendorID() {

            if (document.getElementById('<%=ucSeacrhVendor1.FindControl("txtVendorNo").ClientID%>') != null) {
                if (document.getElementById('<%=ucSeacrhVendor1.FindControl("txtVendorNo").ClientID%>').value == "") {
                    alert("<%=VendorReq %>");
                    return false;
                }
            }

            if (!isSomethingChecked()) {
                alert("<%=AtLeaseOneDay %>");
                return false;
            }
            var MaxPallets = document.getElementById('<%=txtMaxPallets.ClientID%>').value;
            var MaxCartoon = document.getElementById('<%=txtMaxCartons.ClientID%>').value;

            if (MaxPallets == "" && MaxCartoon == "") {
                alert("<%=FixedSlotsGreaterThan0 %>");
                return false;
            }

            if (parseInt(MaxPallets) <= 0 && parseInt(MaxCartoon) <= 0) {
                alert("<%=FixedSlotsGreaterThan0 %>");
                return false;
            }
            return true;
        }



        function isSomethingChecked() {
        
            var services = document.getElementById('chkDays');
            var checkboxes = services.getElementsByTagName('input');
            var checked = false;
            for (var i = 0, i0 = checkboxes.length; i < i0; i++) {
                if (checkboxes[i].type.toLowerCase() == "checkbox") {
                    if (checkboxes[i].checked)
                        checked = true;
                }
            }
            return checked;
        }


    </script>
    <h2>
        <cc1:ucLabel ID="lblFixedSlotSetup" runat="server"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:HiddenField ID="hdnSelectedVendorID" runat="server" />
    <asp:HiddenField ID="hdnSelectedCarrierID" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlScheduleType" runat="server" CssClass="fieldset-form">
                <table width="40%" cellspacing="5" cellpadding="0" align="center" class="top-settings">
                    <tr>
                        <td style="width: 50%;" class="nobold">
                            <cc1:ucRadioButton ID="rdoVendor" runat="server" GroupName="Vendor" 
                                Checked="true" oncheckedchanged="rdoVendor_CheckedChanged" AutoPostBack="true" />
                        </td>
                        <td style="width: 50%;" class="nobold">
                            <cc1:ucRadioButton ID="rdoCarrier" runat="server" GroupName="Vendor" 
                                oncheckedchanged="rdoCarrier_CheckedChanged" AutoPostBack="true" />
                        </td>
                    </tr>
                </table>
                <br />
                <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 30%">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor" isRequired="true"></cc1:ucLabel>
                            <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier" isRequired="true" Visible="false"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5px">
                            :
                        </td>
                        <td colspan="2" style="font-weight: bold;">
                            <cc1:ucLiteral ID="ltvendorName" runat="server"></cc1:ucLiteral>
                            <uc1:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                              <cc1:ucDropdownList ID="ddlCarrier" runat="server" Width="120px" Visible="false">
                            </cc1:ucDropdownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblTimeSlot" runat="server" Text="Time Slot" isRequired="true"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5%">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucDropdownList ID="ddlSlotTime" runat="server" Width="60px">
                            </cc1:ucDropdownList>
                            <cc1:ucLabel ID="lblHHMM1" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                     <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDoorName" runat="server" Text="DoorNo" isRequired="true"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5%">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucDropdownList ID="ddlDoorNo" runat="server" Width="60px">
                            </cc1:ucDropdownList>
                            
                        </td>
                    </tr>
                </table>
                <div id="chkDays">
                <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">                    
                    <tr>
                        <td>
                            <cc1:ucCheckbox ID="chkMon" Text="Monday" runat="server" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkTue" Text="Tuesday" runat="server" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkWed" Text="Wednesday" runat="server" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkThurs" Text="Thursday" runat="server" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkFri" Text="Friday" runat="server" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkSat" Text="Saturday" runat="server" />
                        </td>
                        <td>
                            <cc1:ucCheckbox ID="chkSun" Text="Sunday" runat="server" />
                        </td>
                    </tr>
                </table>
                </div>
                <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 30%">
                            <cc1:ucLabel ID="lblReservedPallets" runat="server" Text="Reserved Pallets"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5%">
                            :
                        </td>
                        <td class="nobold">
                            <cc1:ucNumericTextbox onkeypress="return IsNumberKey(event, this);" ID="txtMaxPallets" runat="server" MaxLength="3"
                                Width="40px"></cc1:ucNumericTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;" class="style1">
                            <cc1:ucLabel ID="lblReservedCartons" runat="server" Text="Reserved Cartons"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; " class="style2">
                            :
                        </td>
                        <td class="style3">
                            <cc1:ucNumericTextbox onkeypress="return IsNumberKey(event, this);" ID="txtMaxCartons" runat="server" MaxLength="3"
                                Width="40px"></cc1:ucNumericTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblReservedLines" runat="server" Text="Reserved Lines"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5%">
                            :
                        </td>
                        <td class="nobold">
                            <cc1:ucNumericTextbox onkeypress="return IsNumberKey(event, this);" ID="txtMaxLines" runat="server" MaxLength="3"
                                Width="40px"></cc1:ucNumericTextbox>
                        </td>
                    </tr>    
                    <tr> <td colspan="20">
                     <cc1:ucPanel ID="pnlAllocationType" runat="server" CssClass="fieldset-form">
                      <table width="40%" cellspacing="5" cellpadding="0" align="center" class="top-settings">
                    <tr>
                        <td style="width: 50%;" class="nobold">
                            <cc1:ucRadioButton ID="rdoHardAllocation" runat="server" GroupName="Allocation" 
                                Checked="true"/>
                        </td>
                        <td style="width: 50%;" class="nobold">
                            <cc1:ucRadioButton ID="rdoSoftAllocation" runat="server" GroupName="Allocation" 
                                 />
                        </td>
                    </tr>
                   </table>
                  <br />
                    </cc1:ucPanel>
                    </td>
                    </tr>             
                </table>
                  
            </cc1:ucPanel>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" OnClientClick="return chkVendorID();" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" 
            OnClientClick="return confirmDelete();" onclick="btnDelete_Click" Visible="false" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" 
            onclick="btnBack_Click" />
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .style1
        {
            height: 21px;
        }
        .style2
        {
            width: 5%;
            height: 21px;
        }
        .style3
        {
            font-weight: normal;
            color: #6b6b6b;
            height: 21px;
        }
    </style>
</asp:Content>


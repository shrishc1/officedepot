﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="VehicleDoorMatrixOverview.aspx.cs" Inherits="VehicleDoorMappingsSetupOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucAddButton" TagPrefix="uc" Src="~/CommonUI/UserControls/ucAddButton.ascx" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblVehicleDoorMappingSetup" runat="server" Text="Vehicle Door Matrix Setup"></cc1:ucLabel>
    </h2>
    <table class="top-settings" width="30%">
        <tr>
            <td style="font-weight: bold;" align="center">
                <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;
                <uc2:ucsite id="ddlSite" runat="server" />
            </td>
            
        </tr>
    </table>
    
    <table width="100%" >                
    <tr>
        <td>
            <div class="button-row">
                <uc:ucAddButton ID="btnAdd" runat="server" NavigateUrl="VehicleDoorMatrixEdit.aspx" />
                <uc1:ucExportButton ID="btnExportToExcel1" runat="server" />
            </div>
        </td>
    </tr>
        <tr>
            <td align="center">
                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" onsorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Vehicle Type " SortExpression="VehicleType.VehicleType">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hlVehicleType" runat="server" Text='<%#Eval("VehicleType.VehicleType")%>'
                                    NavigateUrl='<%# EncryptQuery("VehicleDoorMatrixEdit.aspx?VehicleTypeDoorTypeID=" + Eval("VehicleTypeDoorTypeID")) %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Door Type" SortExpression="DoorType.DoorType">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblDoorType" runat="server" Text='<%#Eval("DoorType.DoorType") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Priority" DataField="Priority" SortExpression="Priority">
                            <HeaderStyle HorizontalAlign="Left" Width="5%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                          <asp:TemplateField HeaderText="" >
                            <HeaderStyle HorizontalAlign="Left" Width="65%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="blank" runat="server" Text=""></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>

﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
public partial class APPSIT_VendorProcessingWindowOverview : CommonPage 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindVendor();
        }
        ucExportToExcel1.GridViewControl = UcGridView1;
        ucExportToExcel1.FileName = "VendorProcessingWindowOverview";
    }
    #region Methods

    protected void BindVendor() {
        MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
        APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

        oMASSIT_VendorProcessingWindowBE.Action = "Show";
        oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MASSIT_VendorProcessingWindowBE> lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsBAL(oMASSIT_VendorProcessingWindowBE);

        if (lstVendor.Count > 0) {
            UcGridView1.DataSource = lstVendor;
            UcGridView1.DataBind();
            ViewState["lstSites"] = lstVendor;
        }
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASSIT_VendorProcessingWindowBE>.SortList((List<MASSIT_VendorProcessingWindowBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    #endregion
}
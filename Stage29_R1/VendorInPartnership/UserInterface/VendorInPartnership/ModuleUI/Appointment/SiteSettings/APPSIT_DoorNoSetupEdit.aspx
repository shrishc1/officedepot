﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" CodeFile="APPSIT_DoorNoSetupEdit.aspx.cs"
    Inherits="APPSIT_DoorNoSetupEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
        
    </script>
    <h2>
        <cc1:ucLabel ID="lblDoorNameSetup" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div>            
            <asp:RequiredFieldValidator ID="rfvDoorNameRequired" runat="server" ControlToValidate="txtDoorName"
                Display="None" ValidationGroup="a" SetFocusOnError="true">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revDoorNameAlfanumericRequired" runat="server" ControlToValidate="txtDoorName"
                Display="None" ValidationExpression="^[A-Za-z0-9 ]{1,15}$" ValidationGroup="a" SetFocusOnError="true">
            </asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="rfvReferenceRequired" runat="server" ControlToValidate="txtReferenceText"
                Display="None" ValidationGroup="a" SetFocusOnError="true">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="rfvReferenceAlfanumericRequired" runat="server" ControlToValidate="txtReferenceText"
                Display="None" ValidationExpression="^[A-Za-z0-9 ]{1,15}$" ValidationGroup="a" SetFocusOnError="true">
            </asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="rfvDoorTypeRequired" runat="server" ControlToValidate="ddlDoorType"
                Display="None" InitialValue="0" ValidationGroup="a" SetFocusOnError="true">
            </asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                Style="color: Red" ValidationGroup="a" />
        </div>
        <div class="formbox">
            <table width="30%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 10%">
                    </td>
                    <td style="font-weight: bold; width: 30%">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5%">
                        :
                    </td>
                    <td style="width: 45%">
                        <cc2:ucSite ID="ucSite" runat="server" />
                    </td>
                    <td style="width: 10%">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblDoorName" runat="server" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucTextbox ID="txtDoorName" ValidationGroup="a" runat="server" Width="143px" MaxLength="15"></cc1:ucTextbox>
                    </td>
                    <td>
                    </td>
                </tr>                
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblReferenceText" ValidationGroup="a" runat="server" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucTextbox ID="txtReferenceText" runat="server" Width="25px" MaxLength="2"></cc1:ucTextbox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblDoorType" runat="server" Text="Door Type" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td>
                        <cc1:ucDropdownList ID="ddlDoorType" runat="server" ValidationGroup="a" Width="150px">
                        </cc1:ucDropdownList>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" ValidationGroup="a" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click" OnClientClick="return confirmDelete();" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_DoorConstraints.aspx.cs" Inherits="APPSIT_DoorConstraints"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
            <script language="javascript" type="text/javascript">

                function addRemoveDoor(CheckBoxID, j, i, startDay, endDay) {
                    //debugger;
                    if (startDay != endDay) {
                        var DoorIDs = document.getElementById('<%=hdnDoorID.ClientID%>').value;
                    }
                    else {
                        var DoorIDs = document.getElementById('<%=hdnDoorIDEndDay.ClientID%>').value;
                    }
                    if (CheckBoxID.checked == true) {
                         DoorIDs = DoorIDs + j + "@" + i + ",";      
                    }
                    else
                    {
                        DoorIDs = DoorIDs.replace("," + j + "@" + i + ",",",");
                    }
                    if (startDay != endDay)
                        document.getElementById('<%=hdnDoorID.ClientID%>').value = DoorIDs;
                    else
                        document.getElementById('<%=hdnDoorIDEndDay.ClientID%>').value = DoorIDs;

                }
                function openAllDoor(CheckBoxID, SiteDoorNumberID) {
                    //debugger;
                    var siteDoorNoID = ",";
                    document.getElementById('<%=hdnAllDoorOpen.ClientID%>').value = SiteDoorNumberID;
                    var bVal = false;
                    $('input[type=checkbox]').each(function () {
                        var s = this.id;
                        var len = 'ctl00_ContentPlaceHolder1_chkdooropenAll_';                       
                        if (s.indexOf('chkdooropenAll_' + SiteDoorNumberID) > -1) {
                            if (s.length == len.length + parseInt(SiteDoorNumberID.length)) {
                                bVal = this.checked;                               
                            }
                        }
                        else if (s.indexOf('_' + SiteDoorNumberID) > -1) {
                            var idsplit = s.split('_');                          
                            if (idsplit[3].length == parseInt(SiteDoorNumberID.length)) {
                                if (bVal != this.checked)
                                    this.click();
                            }
                        }
                    });                    
                }
            </script>
   
            <h2>
                <cc1:ucLabel ID="lblDoorConstraints" runat="server" Text="Door Constraints"></cc1:ucLabel>
            </h2>
   
            <asp:HiddenField ID="hdnDoorID" runat="server" value="," /> 
            <asp:HiddenField ID="hdnDoorIDEndDay" runat="server" value="," /> 
            <asp:HiddenField ID="hdnWeekDay" runat="server" />
            <asp:HiddenField ID="hdnStartWeekDay" runat="server" />
            <asp:HiddenField ID="hdnEndWeekDay" runat="server" />
            <asp:HiddenField ID="hdnAllDoorOpen" runat="server" />
            <div class="right-shadow">
                <div class="formbox">
                    <table width="65%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td style="font-weight: bold;">
                               <uc2:ucSite ID="ddlSite" runat="server" />
                            </td>
                            <td colspan="3" align="right">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="6">
                                <div>
                                    <cc1:ucButton ID="btnMonday" runat="server" Text="Monday" CssClass="button" Width="80px" onclick="btnWeekday_Click" CommandArgument="Mon" />
                                    <cc1:ucButton ID="btnTuesday" runat="server" Text="Tuesday" CssClass="button" Width="80px" onclick="btnWeekday_Click" CommandArgument="Tue"/>
                                    <cc1:ucButton ID="btnWednesday" runat="server" Text="Wednesday" CssClass="button" Width="80px" onclick="btnWeekday_Click" CommandArgument="Wed"/>
                                    <cc1:ucButton ID="btnThursday" runat="server" Text="Thursday" CssClass="button" Width="80px" onclick="btnWeekday_Click" CommandArgument="Thu"/>
                                    <cc1:ucButton ID="btnFriday" runat="server" Text="Friday" CssClass="button" Width="80px" onclick="btnWeekday_Click" CommandArgument="Fri"/>
                                    <cc1:ucButton ID="btnSaturday" runat="server" Text="Saturday" CssClass="button" Width="80px" onclick="btnWeekday_Click" CommandArgument="Sat"/>
                                    <cc1:ucButton ID="btnSunday" runat="server" Text="Sunday" CssClass="button" Width="80px" onclick="btnWeekday_Click" CommandArgument="Sun"/>
                                </div>
                            </td>
                        </tr>
                       
                    </table>
                    <cc1:ucPanel ID="pnlTimeWindowtop" runat="server" GroupingText="Time Window" CssClass="fieldset-form">
                        <asp:Panel ID="pnlHeaderWindow" runat="server" Style="width: 98.2%; overflow: auto;">
                        </asp:Panel>
                        <asp:Panel ID="pnlTimeWindow" runat="server" Style="width: 98%; height: 455px; overflow: auto">
                        </asp:Panel>
                    </cc1:ucPanel>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
                &nbsp;
                <cc1:ucButton ID="btnSpecificDoorConstraint" runat="server" 
                                    onclick="btnSpecificDoorConstraint_Click" CssClass="button" Width="160px" />
            </div>
      
</asp:Content>

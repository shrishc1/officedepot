﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_MiscellaneousSettingsOverview.aspx.cs" Inherits="APPSIT_MiscellaneousSettingsOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblApptMissSettings" runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <div class="right-shadow">
        <div class="formbox">            
                <cc1:ucPanel ID="pnlGrid" runat="server" ScrollBars="Both" CssClass="fieldset-form">
                    <cc1:ucGridView ID="grdApptSettings" runat="server" CssClass="grid" OnRowDataBound="grdApptSettings_RowDataBound"
                        OnSorting="SortGrid" AllowSorting="true" Width="1900" Style="overflow: auto;" >
                        <Columns>
                            <asp:TemplateField HeaderText="Site" SortExpression="SiteDescription">
                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpLink" runat="server" Text='<%# Eval("SiteDescription") %>' NavigateUrl='<%# EncryptQuery("APPSIT_MiscellaneousSettingsEdit.aspx?SiteID="+ Eval("SiteID") +"&SiteCountryID="+ Eval("SiteCountryID")) %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Booking Notification" DataField="BookingNoticePeriodInDays"
                                SortExpression="BookingNoticePeriodInDays">
                                <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="DueDateTolerance" DataField="ToleranceDueDay" SortExpression="ToleranceDueDay">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Lines Per FTEE" DataField="LinePerFTE" SortExpression="LinePerFTE">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="AvePalletsUnloadedperhour" DataField="AveragePalletUnloadedPerManHour"
                                SortExpression="AveragePalletUnloadedPerManHour">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="AveCartonsUnloadedperhour" DataField="AverageCartonUnloadedPerManHour"
                                SortExpression="AverageCartonUnloadedPerManHour">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="AveLinesReceivingperhour" DataField="AverageLinesReceiving"
                                SortExpression="AverageLinesReceiving">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="SlotsWindows" DataField="SlotWindow" SortExpression="SlotWindow">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="AlternateSlotText" DataField="AlternateSlot" SortExpression="AlternateSlot">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="DefaultDeliveryType" DataField="DeliveryType" SortExpression="DeliveryType">
                                <HeaderStyle HorizontalAlign="Center" Width="125px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="DefaultVehicleType" DataField="VehicleType" SortExpression="VehicleType">
                                <HeaderStyle HorizontalAlign="Center" Width="125px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="MaxPalletsPerDelivery" DataField="MaxPalletsPerDelivery"
                                SortExpression="MaxPalletsPerDelivery">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="MaxCartonsPerDelivery" DataField="MaxCartonsPerDelivery"
                                SortExpression="MaxCartonsPerDelivery">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="MaxLinesPerDelivery" DataField="MaxLinesPerDelivery"
                                SortExpression="MaxLinesPerDelivery">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="NonTimedStart" DataField="NonTimeStart" SortExpression="NonTimeStart">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="NonTimedEnd" DataField="NonTimeEnd" SortExpression="NonTimeEnd">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:CheckBoxField HeaderText="PreAdviceNote" DataField="IsPreAdviseNoteRequired"
                                SortExpression="IsPreAdviseNoteRequired" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" />
                            <asp:BoundField HeaderText="BookingExclusion" DataField="BookingExclusion" SortExpression="BookingExclusion">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                    </cc1:ucGridView>
                </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class APPSIT_VehicleTypeSetupOverview : CommonPage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack)
            BindVehicleTypeSetup();
        btnExportToExcel1.GridViewControl = UcGridView1;
        btnExportToExcel1.FileName = "VehicleTypeSetupOverview";
    }

    #region Methods

    protected void BindVehicleTypeSetup() {
        MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

        oMASSIT_VehicleTypeBE.Action = "ShowAll";
        oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MASSIT_VehicleTypeBE> lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsBAL(oMASSIT_VehicleTypeBE);

        if (lstVehicleType.Count > 0) {
            UcGridView1.DataSource = lstVehicleType;
            UcGridView1.DataBind();
            ViewState["lstSites"] = lstVehicleType;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASSIT_VehicleTypeBE>.SortList((List<MASSIT_VehicleTypeBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    #endregion
}
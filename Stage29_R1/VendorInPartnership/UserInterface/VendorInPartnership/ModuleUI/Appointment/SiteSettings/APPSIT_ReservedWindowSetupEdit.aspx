﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_ReservedWindowSetupEdit.aspx.cs" Inherits="APPSIT_ReservedWindowSetupEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="uc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }


        function chkVendorID() {

            if (document.getElementById('<%=ucSeacrhVendor1.FindControl("txtVendorNo").ClientID%>') != null) {
                if (document.getElementById('<%=ucSeacrhVendor1.FindControl("txtVendorNo").ClientID%>').value == "") {
                    alert("<%=VendorReq %>");
                    return false;
                }
            }

            if (!isSomethingChecked()) {
                alert("<%=AtLeaseOneDay %>");
                return false;
            }
            var MaxPallets = document.getElementById('<%=txtMaxPallets.ClientID%>').value;
            var MaxCartoon = document.getElementById('<%=txtMaxCartons.ClientID%>').value;

            if (MaxPallets == "" && MaxCartoon == "") {
                alert("<%=FixedSlotsGreaterThan0 %>");
                return false;
            }

            if (parseInt(MaxPallets) <= 0 && parseInt(MaxCartoon) <= 0) {
                alert("<%=FixedSlotsGreaterThan0 %>");
                return false;
            }
            return true;
        }



        function isSomethingChecked() {

            var services = document.getElementById('chkDays');
            var checkboxes = services.getElementsByTagName('input');
            var checked = false;
            for (var i = 0, i0 = checkboxes.length; i < i0; i++) {
                if (checkboxes[i].type.toLowerCase() == "checkbox") {
                    if (checkboxes[i].checked)
                        checked = true;
                }
            }
            return checked;
        }


    </script>
    <h2>
        <cc1:ucLabel ID="lblReservedWindowSetup" runat="server"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnSiteID" runat="server" />
    <asp:HiddenField ID="hdnSelectedVendorID" runat="server" />
    <asp:HiddenField ID="hdnSelectedCarrierID" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlScheduleType" runat="server" CssClass="fieldset-form">
                <table width="40%" cellspacing="5" cellpadding="0" align="center" class="top-settings">
                    <tr>
                        <td style="width: 50%;" class="nobold">
                            <cc1:ucRadioButton ID="rdoVendor" runat="server" GroupName="Vendor" Checked="true"
                                OnCheckedChanged="rdoVendor_CheckedChanged" AutoPostBack="true" />
                        </td>
                        <td style="width: 50%;" class="nobold">
                            <cc1:ucRadioButton ID="rdoCarrier" runat="server" GroupName="Vendor" OnCheckedChanged="rdoCarrier_CheckedChanged"
                                AutoPostBack="true" />
                        </td>
                    </tr>
                </table>
                <br />
                <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td style="font-weight: bold;">
                            <uc2:ucSite ID="ddlSite" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; width: 15%">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor" isRequired="true"></cc1:ucLabel>
                            <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier" isRequired="true" Visible="false"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="font-weight: bold; width: 39%">
                            <cc1:ucLiteral ID="ltvendorName" runat="server"></cc1:ucLiteral>
                            <uc1:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                            <cc1:ucDropdownList ID="ddlCarrier" runat="server" Width="120px" Visible="false">
                            </cc1:ucDropdownList>
                        </td>
                        <td style="font-weight: bold; width: 15%">
                            <cc1:ucLabel ID="lblDoorName" runat="server" Text="DoorNo" isRequired="true"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="font-weight: bold; width: 29%">
                            <cc1:ucDropdownList ID="ddlDoorNo" runat="server" Width="60px">
                            </cc1:ucDropdownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSiteWeekSetupStart" runat="server" Text="Start" isRequired="true"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td valign="top">
                            <cc1:ucDropdownList ID="ddlStartTime" runat="server" Width="60px">
                            </cc1:ucDropdownList>
                            &nbsp;
                            <cc1:ucLabel ID="UcLabel1" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSiteWeekSetupEnd" runat="server" Text="End" isRequired="true"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td valign="top" style="font-weight: bold;">
                            <cc1:ucDropdownList ID="ddlEndTime" runat="server" Width="60px">
                            </cc1:ucDropdownList>
                            &nbsp;
                            <cc1:ucLabel ID="lblHHMM2" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblMaximumPallets" runat="server" Text="Maximum Pallets" Style="font-weight: bold"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td>
                            <cc1:ucNumericTextbox ID="txtMaxPallets" MaxLength="4" runat="server" Width="47px"
                                onkeypress="return IsNumberKey(event, this);" onkeyup="AllowNumbersOnly(this)"></cc1:ucNumericTextbox>
                        </td>
                        <td style="font-weight: bold">
                            <cc1:ucLabel ID="lblMaximumCartons" runat="server" Text="Maximum Cartons"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td>
                            <cc1:ucNumericTextbox ID="txtMaxCartons" MaxLength="4" runat="server" Width="47px"
                                onkeypress="return IsNumberKey(event, this);" onkeyup="AllowNumbersOnly(this)"></cc1:ucNumericTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblMaximumLines" runat="server" Text="Maximum Lines" Style="font-weight: bold"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td>
                            <cc1:ucNumericTextbox ID="txtMaxLines" MaxLength="4" runat="server" Width="47px"
                                onkeypress="return IsNumberKey(event, this);" onkeyup="AllowNumbersOnly(this)"></cc1:ucNumericTextbox>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblMaximumDeliveries" runat="server" Text="Maximum Deliveries"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td>
                            <cc1:ucNumericTextbox ID="txtMaxDeliveries" MaxLength="4" runat="server" Width="47px"
                                onkeypress="return IsNumberKey(event, this);" onkeyup="AllowNumbersOnly(this)"></cc1:ucNumericTextbox>
                        </td>
                    </tr>
                </table>
                <div id="chkDays" style="padding-top:15Px;">
                    <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td>
                                <cc1:ucCheckbox ID="chkMon" Text="Monday" runat="server" />
                            </td>
                            <td>
                                <cc1:ucCheckbox ID="chkTue" Text="Tuesday" runat="server" />
                            </td>
                            <td>
                                <cc1:ucCheckbox ID="chkWed" Text="Wednesday" runat="server" />
                            </td>
                            <td>
                                <cc1:ucCheckbox ID="chkThurs" Text="Thursday" runat="server" />
                            </td>
                            <td>
                                <cc1:ucCheckbox ID="chkFri" Text="Friday" runat="server" />
                            </td>
                            <td>
                                <cc1:ucCheckbox ID="chkSat" Text="Saturday" runat="server" />
                            </td>
                            <td>
                                <cc1:ucCheckbox ID="chkSun" Text="Sunday" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                
            </cc1:ucPanel>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
            OnClientClick="return chkVendorID();" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClientClick="return confirmDelete();"
            OnClick="btnDelete_Click" Visible="false" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

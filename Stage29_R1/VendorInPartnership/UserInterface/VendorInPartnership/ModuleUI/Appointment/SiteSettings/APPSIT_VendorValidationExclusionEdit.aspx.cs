﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Utilities; using WebUtilities;


public partial class APPSIT_VendorValidationExclusionEdit : CommonPage {

    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            BindSites();
           // BindDeliveryTypes("DeliveryType");
            //BindVendors();
        }
    }

    #region Methods

    protected void BindSites() {
        SqlConnection sqlCon = GetConnection();
        SqlDataAdapter sqlDa = new SqlDataAdapter(@"select ltrim(rtrim(SitePrefix)) + ' - '+ SiteName as SiteDescription,SiteID from site", sqlCon);
        DataSet ds = new DataSet();
        sqlDa.Fill(ds, "Sites");
        ddlSite.DataSource = ds.Tables[0];
        ddlSite.DataValueField = "SiteID";
        ddlSite.DataTextField = "SiteDescription";
        ddlSite.DataBind();
    }

    //protected void BindDeliveryTypes(string strType)
    //{
    //    SqlConnection sqlCon = GetConnection();
    //    string strQuery = "select " +
    //                        "ParameterName , " +
    //                        "ParameterStringValue,  " +
    //                        "ParameterNumericValue, " +
    //                        "ParameterDecimalValue " +
    //                            "from sys_systemparameter sp " +
    //                                "inner join sys_systemparameterGroup spg " +
    //                        "on sp.SystemParameterGroupID = spg.SystemParameterGroupID " +
    //                            "where parametergroupname = '" + strType + "' " +
    //                        "order by SortOrder";
    //    SqlDataAdapter sqlDa = new SqlDataAdapter(strQuery, sqlCon);
    //    DataSet ds = new DataSet();
    //    sqlDa.Fill(ds, "DeliveryTypes");
    //    lstSelectedVendor.DataSource = ds.Tables[0];
    //    lstSelectedVendor.DataValueField = "ParameterStringValue";
    //    lstSelectedVendor.DataTextField = "ParameterName";
    //    lstSelectedVendor.DataBind();
    //}

    //protected void BindVendors() {
    //    SqlConnection sqlCon = GetConnection();
    //    string strQuery = "select " +
    //                            "VendorID , " +
    //                            "CompanyName  " +
    //                       "from Vendor ";
    //    SqlDataAdapter sqlDa = new SqlDataAdapter(strQuery, sqlCon);
    //    DataSet ds = new DataSet();
    //    sqlDa.Fill(ds, "Vendors");
    //    ddlVender.DataSource = ds.Tables[0];
    //    ddlVender.DataValueField = "VendorID";
    //    ddlVender.DataTextField = "CompanyName";
    //    ddlVender.DataBind();
    //}

    protected SqlConnection GetConnection() {
        return (new SqlConnection("Data Source=172.29.9.7;Initial Catalog=OfficeDepot;Persist Security Info=True;User ID=sa;Password=spice"));
    }

    #endregion

    #region Events

    #endregion
}
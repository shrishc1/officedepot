﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;

public partial class APPSIT_WeekSetupEdit : CommonPage
{
    protected string ConfirmMessage = WebCommon.getGlobalResourceValue("ConfirmMessage");

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnClear.Style["display"] = "none";
            ClearControls();
            BindTimes();
            if (GetQueryStringValue("weekday") != null && GetQueryStringValue("weekday").ToString() != "")
            {
                BindStartWeekDays(GetQueryStringValue("weekday").ToString());
            }
            GetWeekSetupForSelectedDay();
        }
        if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro")
        {
            btnSave.Visible = false;
            btnClear.Visible = false;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        if (GetQueryStringValue("SiteID") != null)
        {
            string Days = GetWeekDaysToBeDisabled(ddlStartWeek.SelectedItem.Value, ddlEndWeek.SelectedItem.Value);

            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
            if (Days != "")
                oMASSIT_WeekSetupBE.DayDisabledFor = ddlStartWeek.SelectedItem.Value + "," + Days;
            else
                oMASSIT_WeekSetupBE.DayDisabledFor = ddlStartWeek.SelectedItem.Value;

            oMASSIT_WeekSetupBE.Action = "CheckWeekSetups";
            List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
            if (lstWeekSetup != null && lstWeekSetup.Count > 0)
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("ClearWeekSetups");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
                return;
            }
        }

        //DateTime dtStart = new DateTime(1900, 1, 1, Convert.ToInt32(txtStartTime.Text.Split(':')[0]), Convert.ToInt32(txtStartTime.Text.Split(':')[1]), 0);
        //DateTime dtEnd = new DateTime(1900, 1, 1, Convert.ToInt32(txtEndTime.Text.Split(':')[0]), Convert.ToInt32(txtEndTime.Text.Split(':')[1]), 0);

        DateTime dtStart = new DateTime(1900, 1, 1, Convert.ToInt32(ddlStartTime.SelectedItem.Text.Split(':')[0]), Convert.ToInt32(ddlStartTime.SelectedItem.Text.Split(':')[1]), 0);
        DateTime dtEnd = new DateTime(1900, 1, 1, Convert.ToInt32(ddlEndTime.SelectedItem.Text.Split(':')[0]), Convert.ToInt32(ddlEndTime.SelectedItem.Text.Split(':')[1]), 0);

        //End time restriction
        oMASSIT_WeekSetupBE.Action = "ShowAll";
        oMASSIT_WeekSetupBE.SiteID = GetQueryStringValue("SiteID") != null ? Convert.ToInt32(GetQueryStringValue("SiteID")) : Convert.ToInt32(hidSiteID.Value);
        oMASSIT_WeekSetupBE.EndWeekday = ddlStartWeek.SelectedItem.Value;
        List<MASSIT_WeekSetupBE> lstWeekSetupConflictsStart = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
        if (lstWeekSetupConflictsStart != null && lstWeekSetupConflictsStart.Count > 0)
        {
            if (dtStart < lstWeekSetupConflictsStart[0].EndTime && lstWeekSetupConflictsStart[0].EndTime != null)
            {
                if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lstWeekSetupConflictsStart[0].SiteScheduleDiaryID)
                {
                    string errorMeesage = WebCommon.getGlobalResourceValue("EndTimeRestriction");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                    return;
                }
                else if (GetQueryStringValue("SiteScheduleDiaryID") == null)
                {
                    string errorMeesage = WebCommon.getGlobalResourceValue("EndTimeRestriction");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                    return;
                }
            }
        }

        //Start time restriction
        oMASSIT_WeekSetupBE.Action = "CheckStartTimesForEndDayTime";
        oMASSIT_WeekSetupBE.StartWeekday = ddlEndWeek.SelectedItem.Value;
        List<MASSIT_WeekSetupBE> lstWeekSetupConflictsEnd = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
        if (lstWeekSetupConflictsEnd != null && lstWeekSetupConflictsEnd.Count > 0)
        {
            if (lstWeekSetupConflictsEnd.Count == 1)
            {
                if (dtEnd > lstWeekSetupConflictsEnd[0].StartTime && lstWeekSetupConflictsEnd[0].StartTime != null)
                {
                    if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lstWeekSetupConflictsEnd[0].SiteScheduleDiaryID)
                    {
                        string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestriction");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert3", "alert('" + errorMeesage + "')", true);
                        return;
                    }
                    else if (GetQueryStringValue("SiteScheduleDiaryID") == null)
                    {
                        string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestriction");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert3", "alert('" + errorMeesage + "')", true);
                        return;
                    }
                }
            }
            else
            {
                for (int i = 0; i < lstWeekSetupConflictsEnd.Count; i++)
                {
                    if (lstWeekSetupConflictsEnd[i].StartTime != null)
                    {
                        if (dtEnd > lstWeekSetupConflictsEnd[i].StartTime && lstWeekSetupConflictsEnd[i].StartTime != null)
                        {
                            if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lstWeekSetupConflictsEnd[i].SiteScheduleDiaryID)
                            {
                                string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestriction");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert4", "alert('" + errorMeesage + "')", true);
                                return;
                            }
                            else if (GetQueryStringValue("SiteScheduleDiaryID") == null)
                            {
                                string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestriction");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert4", "alert('" + errorMeesage + "')", true);
                                return;
                            }
                        }
                    }
                }
            }
        }

        //Check Time window restriction

        MASSIT_TimeWindowBE oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
        oMASSIT_TimeWindowBE.Action = "GetMaxTimeWindow";
        oMASSIT_TimeWindowBE.SiteID = GetQueryStringValue("SiteID") != null ? Convert.ToInt32(GetQueryStringValue("SiteID")) : Convert.ToInt32(hidSiteID.Value);
        oMASSIT_TimeWindowBE.StartWeekday = ddlStartWeek.SelectedItem.Value; ;
        oMASSIT_TimeWindowBE.Weekday = ddlEndWeek.SelectedItem.Value; ;
        oMASSIT_TimeWindowBE.StartSlotTimeID = Convert.ToInt32(ddlStartTime.SelectedItem.Value);
        oMASSIT_TimeWindowBE.EndSlotTimeID = Convert.ToInt32(ddlEndTime.SelectedItem.Value);

        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        List<MASSIT_TimeWindowBE> lstMASSIT_TimeWindowBE = oMASSIT_TimeWindowBAL.GetMaxTimeWindowBAL(oMASSIT_TimeWindowBE);

        if (lstMASSIT_TimeWindowBE[0].TimeWindowID != 0)
        {
            string errorMeesage = WebCommon.getGlobalResourceValue("StartEndTimeWindow");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert5", "alert('" + errorMeesage + "')", true);
            return;
        }

        //----------------------------

        if (GetQueryStringValue("SiteID") != null)
        {
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        }
        else if (GetQueryStringValue("SiteScheduleDiaryID") != null)
        {
            //if editing the existing weeksetup then first clear all the related data
            ClearRecordsBeforeEdit();

            oMASSIT_WeekSetupBE.SiteScheduleDiaryID = Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID"));
        }
        oMASSIT_WeekSetupBE.Action = "AddEdit";
        oMASSIT_WeekSetupBE.StartWeekday = ddlStartWeek.SelectedItem.Value;
        oMASSIT_WeekSetupBE.EndWeekday = ddlEndWeek.SelectedItem.Value;

        oMASSIT_WeekSetupBE.StartTime = dtStart;
        oMASSIT_WeekSetupBE.EndTime = dtEnd;
        oMASSIT_WeekSetupBE.MaximumLift = txtMaximumLift.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaximumLift.Text.Trim()) : (int?)null;
        oMASSIT_WeekSetupBE.MaximumPallet = txtMaximumPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaximumPallets.Text.Trim()) : (int?)null;

        oMASSIT_WeekSetupBE.MaximumContainer = txtMaxContainers.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxContainers.Text.Trim()) : (int?)null;
        oMASSIT_WeekSetupBE.MaximumLine = txtMaxLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxLines.Text.Trim()) : (int?)null;

        //Stage 6 Point 19
        oMASSIT_WeekSetupBE.MaximumDeliveries = txtMaxDeliveries.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxDeliveries.Text.Trim()) : (int?)null;

        oMASSIT_WeekSetupBE.DayDisabledFor = GetWeekDaysToBeDisabled(ddlStartWeek.SelectedItem.Value, ddlEndWeek.SelectedItem.Value);

        oMASSIT_WeekSetupBE.StartSlotTimeID = Convert.ToInt32(ddlStartTime.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndSlotTimeID = Convert.ToInt32(ddlEndTime.SelectedItem.Value);

        oMASSIT_WeekSetupBAL.addEditWeekSetupBAL(oMASSIT_WeekSetupBE);

        string SiteID = GetQueryStringValue("SiteID") != null ? GetQueryStringValue("SiteID").ToString() : hidSiteID.Value;

        //Save Week Setup Log Entry
        APPSIT_SpecificWeekSetupBAL oMASSIT_SpecificWeekSetupBAL = new APPSIT_SpecificWeekSetupBAL();
        MASSIT_WeekSetupLogsBE oMASSIT_WeekSetupLogsBE = new MASSIT_WeekSetupLogsBE();
        oMASSIT_WeekSetupLogsBE.Action = "Add";
        oMASSIT_WeekSetupLogsBE.SiteID = Convert.ToInt32(SiteID);
        oMASSIT_WeekSetupLogsBE.StartTime = oMASSIT_WeekSetupBE.StartTime;
        oMASSIT_WeekSetupLogsBE.EndTime = oMASSIT_WeekSetupBE.EndTime;
        oMASSIT_WeekSetupLogsBE.MaximumLift = oMASSIT_WeekSetupBE.MaximumLift;
        oMASSIT_WeekSetupLogsBE.MaximumPallet = oMASSIT_WeekSetupBE.MaximumPallet;
        oMASSIT_WeekSetupLogsBE.MaximumContainer = oMASSIT_WeekSetupBE.MaximumContainer;
        oMASSIT_WeekSetupLogsBE.MaximumLine = oMASSIT_WeekSetupBE.MaximumLine;
        oMASSIT_WeekSetupLogsBE.MaximumDeliveries = oMASSIT_WeekSetupBE.MaximumDeliveries;
        oMASSIT_WeekSetupLogsBE.Type = "General";
        oMASSIT_WeekSetupLogsBE.UserID = System.Web.HttpContext.Current.Session["UserId"].ToString();
        oMASSIT_SpecificWeekSetupBAL.SaveWeekSetupLogsBAL(oMASSIT_WeekSetupLogsBE);

        // To open all doors
        //openAllDoor();

        EncryptQueryString("APPSIT_WeekSetupOverview.aspx?SiteID=" + hidSiteID.Value);
    }

    private void openAllDoor()
    {
        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        if (GetQueryStringValue("SiteID") != null)
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        else
            oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(hidSiteID.Value);
        oMASSIT_WeekSetupBE.Action = "OpenAllDoor";
        oMASSIT_WeekSetupBE.StartWeekday = ddlStartWeek.SelectedItem.Value;
        oMASSIT_WeekSetupBE.EndWeekday = ddlEndWeek.SelectedItem.Value;

        //oMASSIT_WeekSetupBE.StartTime = dtStart;
        //oMASSIT_WeekSetupBE.EndTime = dtEnd;
        oMASSIT_WeekSetupBE.MaximumLift = txtMaximumLift.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaximumLift.Text.Trim()) : (int?)null;
        oMASSIT_WeekSetupBE.MaximumPallet = txtMaximumPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaximumPallets.Text.Trim()) : (int?)null;

        oMASSIT_WeekSetupBE.MaximumContainer = txtMaxContainers.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxContainers.Text.Trim()) : (int?)null;
        oMASSIT_WeekSetupBE.MaximumLine = txtMaxLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxLines.Text.Trim()) : (int?)null;

        oMASSIT_WeekSetupBE.DayDisabledFor = GetWeekDaysToBeDisabled(ddlStartWeek.SelectedItem.Value, ddlEndWeek.SelectedItem.Value);

        oMASSIT_WeekSetupBE.StartSlotTimeID = Convert.ToInt32(ddlStartTime.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndSlotTimeID = Convert.ToInt32(ddlEndTime.SelectedItem.Value);

        oMASSIT_WeekSetupBAL.openAllDoorBAL(oMASSIT_WeekSetupBE);
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("SiteScheduleDiaryID") != null)
        {
            //ClearRecordsBeforeEdit();
            ClearRecordsWithDoorAndDeliveryData();
            EncryptQueryString("APPSIT_WeekSetupOverview.aspx?SiteID=" + hidSiteID.Value);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro")
            EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
        else
            EncryptQueryString("APPSIT_WeekSetupOverview.aspx?SiteID=" + hidSiteID.Value);
    }

    #endregion Events

    #region Methods

    public void ClearControls()
    {
        ddlStartWeek.SelectedIndex = 0;
        ddlEndWeek.SelectedIndex = 0;

        //txtStartTime.Text = string.Empty;
        //txtEndTime.Text = string.Empty;
        ddlStartTime.SelectedIndex = 0;
        ddlEndTime.SelectedIndex = 0;

        txtMaximumLift.Text = string.Empty;
        txtMaximumPallets.Text = string.Empty;
        txtMaxLines.Text = string.Empty;
        txtMaxContainers.Text = string.Empty;
        LBLSite.Text = string.Empty;
    }

    private string formatTime(string time)
    {
        if (time.Length == 1)
        {
            return "0" + time;
        }
        return time;
    }

    public void GetWeekSetupForSelectedDay()
    {
        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();
        if (GetQueryStringValue("SiteScheduleDiaryID") != null && GetQueryStringValue("SiteScheduleDiaryID").ToString() != "")
        {
            btnClear.Style["display"] = "";

            oMASSIT_WeekSetupBE.Action = "ShowAll";
            oMASSIT_WeekSetupBE.SiteScheduleDiaryID = Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID"));
            List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
            if (lstWeekSetup != null && lstWeekSetup.Count > 0)
            {
                LBLSite.Text = lstWeekSetup[0].Site.SiteName;
                hidSiteID.Value = lstWeekSetup[0].SiteID.ToString();

                ddlStartWeek.SelectedValue = lstWeekSetup[0].StartWeekday;
                if (lstWeekSetup[0].StartTime != null && lstWeekSetup[0].StartTime.ToString() != "")
                {
                    //txtStartTime.Text = lstWeekSetup[0].StartTime.Value.Hour.ToString() + ":" + lstWeekSetup[0].StartTime.Value.Minute.ToString();
                    if (ddlStartTime.Items.FindByText(formatTime(lstWeekSetup[0].StartTime.Value.Hour.ToString()) +
                        ":" + formatTime(lstWeekSetup[0].StartTime.Value.ToString("mm"))) != null)
                        ddlStartTime.SelectedValue = ddlStartTime.Items.FindByText(formatTime(lstWeekSetup[0].StartTime.Value.Hour.ToString()) + ":" + formatTime(lstWeekSetup[0].StartTime.Value.ToString("mm"))).Value;
                    else
                        ddlStartTime.SelectedIndex = 0;
                }
                else
                {
                    //txtStartTime.Text = "";
                    ddlStartTime.SelectedIndex = 0;
                }

                ddlEndWeek.Enabled = false;
                ddlEndWeek.SelectedValue = lstWeekSetup[0].EndWeekday;
                if (lstWeekSetup[0].EndTime != null && lstWeekSetup[0].EndTime.ToString() != "")
                {
                    //txtEndTime.Text = lstWeekSetup[0].EndTime.Value.Hour.ToString() + ":" + lstWeekSetup[0].EndTime.Value.Minute.ToString();
                    if (ddlEndTime.Items.FindByText(formatTime(lstWeekSetup[0].EndTime.Value.Hour.ToString()) + ":" + formatTime(lstWeekSetup[0].EndTime.Value.ToString("mm"))) != null)
                        ddlEndTime.SelectedValue = ddlEndTime.Items.FindByText(formatTime(lstWeekSetup[0].EndTime.Value.Hour.ToString()) + ":" + formatTime(lstWeekSetup[0].EndTime.Value.ToString("mm"))).Value;
                    else
                        ddlEndTime.SelectedIndex = 0;
                }
                else
                {
                    //txtEndTime.Text = "";
                    ddlEndTime.SelectedIndex = 0;
                }
                txtMaxLines.Text = Convert.ToString(lstWeekSetup[0].MaximumLine);
                txtMaximumPallets.Text = Convert.ToString(lstWeekSetup[0].MaximumPallet);
                txtMaximumLift.Text = Convert.ToString(lstWeekSetup[0].MaximumLift);
                txtMaxContainers.Text = Convert.ToString(lstWeekSetup[0].MaximumContainer);
                txtMaxDeliveries.Text = Convert.ToString(lstWeekSetup[0].MaximumDeliveries);
            }
        }
        else if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("SiteID").ToString() != "")
        {
            btnClear.Style["display"] = "none";
            hidSiteID.Value = GetQueryStringValue("SiteID").ToString();

            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

            oMAS_SiteBE.Action = "ShowAll";
            oMAS_SiteBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);

            List<MAS_SiteBE> lstSite = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
            if (lstSite != null && lstSite.Count > 0)
            {
                LBLSite.Text = lstSite[0].SitePrefix.ToString() + "-" + lstSite[0].SiteName.ToString();
            }
            if (GetQueryStringValue("weekday") != null && GetQueryStringValue("weekday").ToString() != "")
            {
                ddlEndWeek.SelectedValue = GetQueryStringValue("weekday").ToString();
                ddlEndWeek.Enabled = false;
            }
            else
            {
                ddlEndWeek.SelectedValue = "mon";
            }
            ddlStartWeek.SelectedValue = "mon";

            ddlStartTime.SelectedIndex = 0;
            ddlEndTime.SelectedIndex = 0;
        }
    }

    public string GetWeekDaysToBeDisabled(string startWeekDay, string endWeekDay)
    {
        //get the days of the week to be disabled
        ArrayList arrWeekDays = new ArrayList();
        arrWeekDays.Add("mon");
        arrWeekDays.Add("tue");
        arrWeekDays.Add("wed");
        arrWeekDays.Add("thu");
        arrWeekDays.Add("fri");
        arrWeekDays.Add("sat");
        arrWeekDays.Add("sun");

        ArrayList days = new ArrayList();
        int index = arrWeekDays.IndexOf(startWeekDay);

        if (index == 7) { index = 0; } else { index = index + 1; }
        int i;
        if (startWeekDay != endWeekDay)
        {
        loop: for (i = index; i < arrWeekDays.Count; i++)
            {
                if (endWeekDay != arrWeekDays[i].ToString())
                {
                    days.Add(arrWeekDays[i].ToString());
                }
                else if (endWeekDay == arrWeekDays[i].ToString())
                {
                    break;
                }
                else if (startWeekDay == arrWeekDays[i + 1].ToString())
                {
                    break;
                }
            }
            if (i == 7) { index = 0; goto loop; }
        }
        //if (txtStartTime.Text.Trim() == "00:00") {
        if (ddlStartTime.SelectedItem.Text.Trim() == "00:00")
        {
            if (ddlStartWeek.SelectedItem.Value == ddlEndWeek.SelectedItem.Value)
            {
            }
            else
                days.Add(startWeekDay);
        }
        //if (txtEndTime.Text.Trim() == "00:00") {
        if (ddlEndTime.SelectedItem.Text.Trim() == "00:00")
        {
            //days.Add(endWeekDay);
        }
        string DaysToDisabled = string.Empty;
        if (days.Count == 1) { DaysToDisabled += days[0].ToString(); }
        if (days.Count > 1)
        {
            DaysToDisabled += days[0].ToString();
            for (int j = 1; j < days.Count - 1; j++)
            {
                if (j < days.Count)
                    DaysToDisabled += "," + days[j].ToString();
            }
            DaysToDisabled += "," + days[days.Count - 1].ToString();
        }
        return DaysToDisabled;
    }

    public void ClearRecordsBeforeEdit()
    {
        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        oMASSIT_WeekSetupBE.Action = "ShowAll";

        oMASSIT_WeekSetupBE.SiteScheduleDiaryID = Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID"));
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(hidSiteID.Value);

        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        if (lstWeekSetup != null && lstWeekSetup.Count > 0)
        {
            oMASSIT_WeekSetupBE.DayDisabledFor = GetWeekDaysToBeDisabled(lstWeekSetup[0].StartWeekday, lstWeekSetup[0].EndWeekday) + "," + ddlEndWeek.SelectedItem.Value;
        }
        else
        {
            oMASSIT_WeekSetupBE.DayDisabledFor = GetWeekDaysToBeDisabled(ddlStartWeek.SelectedItem.Value, ddlEndWeek.SelectedItem.Value) + "," + ddlEndWeek.SelectedItem.Value;
        }
        oMASSIT_WeekSetupBE.Action = "ClearRecords";

        oMASSIT_WeekSetupBE.EndWeekday = ddlEndWeek.SelectedItem.Value;

        oMASSIT_WeekSetupBAL.addEditWeekSetupBAL(oMASSIT_WeekSetupBE);
    }

    public void ClearRecordsWithDoorAndDeliveryData()
    {
        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        oMASSIT_WeekSetupBE.Action = "ShowAll";

        oMASSIT_WeekSetupBE.SiteScheduleDiaryID = Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID"));
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(hidSiteID.Value);

        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        if (lstWeekSetup != null && lstWeekSetup.Count > 0)
        {
            oMASSIT_WeekSetupBE.DayDisabledFor = GetWeekDaysToBeDisabled(lstWeekSetup[0].StartWeekday, lstWeekSetup[0].EndWeekday) + "," + ddlEndWeek.SelectedItem.Value;
        }
        else
        {
            oMASSIT_WeekSetupBE.DayDisabledFor = GetWeekDaysToBeDisabled(ddlStartWeek.SelectedItem.Value, ddlEndWeek.SelectedItem.Value) + "," + ddlEndWeek.SelectedItem.Value;
        }

        oMASSIT_WeekSetupBE.Action = "ClearRecordsWithDoorAndDeliveryData";
        oMASSIT_WeekSetupBE.EndWeekday = ddlEndWeek.SelectedItem.Value;
        oMASSIT_WeekSetupBE.StartWeekday = ddlStartWeek.SelectedItem.Value;
        oMASSIT_WeekSetupBE.StartSlotTimeID = Convert.ToInt32(ddlStartTime.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndSlotTimeID = Convert.ToInt32(ddlEndTime.SelectedItem.Value);

        oMASSIT_WeekSetupBAL.addEditWeekSetupBAL(oMASSIT_WeekSetupBE);
    }

    public void BindStartWeekDays(string daySelected)
    {
        ArrayList weekArray = new ArrayList();
        ArrayList startWeekDays = new ArrayList();
        string[] sWeekArray = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

        weekArray.Add("mon"); weekArray.Add("tue");
        weekArray.Add("wed"); weekArray.Add("thu");
        weekArray.Add("fri"); weekArray.Add("sat");
        weekArray.Add("sun");
        int index = weekArray.IndexOf(daySelected);
        if (daySelected != "mon")
        {
            startWeekDays.Add(weekArray[index - 1]);
            startWeekDays.Add(weekArray[index]);
        }
        else if (daySelected == "mon")
        {
            startWeekDays.Add("sun");
            startWeekDays.Add("mon");
        }
        if (startWeekDays.Count > 0)
        {
            ddlStartWeek.Items.Clear();
            ddlStartWeek.Items.Add(new ListItem(sWeekArray[weekArray.IndexOf(startWeekDays[0].ToString())], startWeekDays[0].ToString()));
            ddlStartWeek.Items.Add(new ListItem(sWeekArray[weekArray.IndexOf(startWeekDays[1].ToString())], startWeekDays[1].ToString()));
        }
    }

    public void BindTimes()
    {
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstTimes = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);
        if (lstTimes != null && lstTimes.Count > 0)
        {
            FillControls.FillDropDown(ref ddlStartTime, lstTimes, "SlotTime", "SlotTimeID");
            FillControls.FillDropDown(ref ddlEndTime, lstTimes, "SlotTime", "SlotTimeID");
        }
    }

    #endregion Methods
}
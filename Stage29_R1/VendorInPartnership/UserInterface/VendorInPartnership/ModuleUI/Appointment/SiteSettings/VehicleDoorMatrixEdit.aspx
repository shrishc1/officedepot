﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="VehicleDoorMatrixEdit.aspx.cs" Inherits="VehicleDoorMappingsSetupEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
    </script>
    <h2>
        <cc1:ucLabel ID="lblVehicleDoorMappingSetup" runat="server" Text="Vehicle Door Matrix Setup"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="35%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width:5%"></td>
                    <td style="font-weight: bold; width: 35%">
                        <cc1:ucLabel ID="lblVehicleType" runat="server" Text="Vehicle Type" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5%;">
                        :
                    </td>
                    <td width="55%">
                        <cc1:ucDropdownList ID="ddlVehicleType" runat="server" Width="150px" 
                            onselectedindexchanged="ddlVehicleType_SelectedIndexChanged">
                        </cc1:ucDropdownList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblDoorType" runat="server" Text="Door Type" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td style="font-weight: bold" colspan="6">
                        <cc1:ucDropdownList ID="ddlDoorType" runat="server" Width="150px">
                        </cc1:ucDropdownList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblPriority" runat="server" Text="Priority"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td>
                        <cc1:ucDropdownList ID="ddlPriority" runat="server" Width="50px">
                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                        </cc1:ucDropdownList>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click"
            OnClientClick="return confirmDelete();" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

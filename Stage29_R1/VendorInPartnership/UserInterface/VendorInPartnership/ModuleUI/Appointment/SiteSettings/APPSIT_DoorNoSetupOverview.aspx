﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="~/ModuleUI/Appointment/SiteSettings/APPSIT_DoorNoSetupOverview.aspx.cs"
    Inherits="APPSIT_DoorNoSetupOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucAddButton" TagPrefix="uc" Src="~/CommonUI/UserControls/ucAddButton.ascx" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblDoorNameSetup" runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <uc:ucAddButton ID="btnAdd" runat="server" NavigateUrl="APPSIT_DoorNoSetupEdit.aspx" />
        <uc1:ucExportButton ID="btnExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center">
                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" onsorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblSiteDesc" runat="server" Text='<%#Eval("Site.SiteName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Door Name" SortExpression="DoorNumber">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpDelivery" runat="server" Text='<%# Eval("DoorNumber") %>' NavigateUrl='<%# EncryptQuery("APPSIT_DoorNoSetupEdit.aspx?SiteDoorNumberID="+ Eval("SiteDoorNumberID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reference" SortExpression="Reference">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblSiteDesc" runat="server" Text='<%#Eval("Reference") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Door Type" SortExpression="DoorType.DoorType">
                            <HeaderStyle Width="70%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblDoorType" runat="server" Text='<%#Eval("DoorType.DoorType") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>

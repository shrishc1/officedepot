﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_DeliveriesConstraints.aspx.cs" Inherits="APPSIT_DeliveriesConstraints"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblDeliveriesConstraints" runat="server" Text="Deliveries Constraints"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnWeekDay" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <table width="65%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 29%">
                    </td>
                    <td style="font-weight: bold; width: 5%">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 65%">
                        <uc2:ucSite ID="ddlSite" runat="server" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="5">
                        <div>
                            <cc1:ucButton ID="btnMonday" runat="server" Text="Monday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Mon" />
                            <cc1:ucButton ID="btnTuesday" runat="server" Text="Tuesday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Tue" />
                            <cc1:ucButton ID="btnWednesday" runat="server" Text="Wednesday" CssClass="button"
                                Width="80px" OnClick="btnWeekday_Click" CommandArgument="Wed" />
                            <cc1:ucButton ID="btnThursday" runat="server" Text="Thursday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Thu" />
                            <cc1:ucButton ID="btnFriday" runat="server" Text="Friday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Fri" />
                            <cc1:ucButton ID="btnSaturday" runat="server" Text="Saturday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Sat" />
                            <cc1:ucButton ID="btnSunday" runat="server" Text="Sunday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Sun" />
                        </div>
                    </td>
                </tr>
            </table>
            <br />
            <table width="65%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 23%">
                        <cc1:ucLabel ID="lblTotalLines" runat="server" Text="Total Number of Lines"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 35%">
                        <asp:Literal ID="ltTotalLines" runat="server"></asp:Literal>
                    </td>
                    <td style="font-weight: bold; width: 23%">
                        <cc1:ucLabel ID="lblTotalNumberofPallets" runat="server" Text="Total Number of Pallets"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 19%">
                        <asp:Literal ID="ltTotalPallets" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
            <cc1:ucPanel ID="ucpnlTimeWindow" runat="server" GroupingText="" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0">
                    <tr>
                        <td align="center">
                            <cc1:ucGridView ID="GridView1" runat="server" AutoGenerateColumns="False" ShowFooter="True"
                                OnRowCommand="GridView1_RowCommand" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing"
                                OnRowUpdating="GridView1_RowUpdating" OnRowCancelingEdit="GridView1_RowCancelingEdit"
                                CssClass="grid" CellPadding="0" Width="100%" GridLines="None" OnSorting="GridView1_Sorting1"
                                AllowSorting="true">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="Time Slot" InsertVisible="False" SortExpression="TimeSlot">
                                        <EditItemTemplate>
                                            <asp:Label ID="TimeSlotEdit" runat="server" Text='<%# Eval("TimeSlot") %>' Width="120px"></asp:Label>
                                            <asp:Label ID="StartWeekday_1" runat="server" Text='<%# Bind("StartWeekday") %>'
                                                Width="120px"></asp:Label>
                                            <asp:HiddenField ID="hdnStartTime" runat="server" Value='<%# Eval("StartTime") %>' />
                                            <asp:HiddenField ID="hdnStartWeekday" runat="server" Value='<%# Eval("StartWeekday") %>' />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="TimeSlot" runat="server" Text='<%# Bind("TimeSlot") %>' Width="120px"></asp:Label>
                                            <asp:Label ID="StartWeekday_2" runat="server" Text='<%# Bind("StartWeekday") %>'
                                                Width="120px"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MaximumNoofDeliveries" SortExpression="RestrictnumberofDeliveries">
                                        <EditItemTemplate>
                                            <asp:TextBox onkeypress="return IsNumberKey(event, this);" Width="40px" ID="txtRestrictDeliveries"
                                                runat="server" Text='<%# Bind("RestrictnumberofDeliveries") %>' MaxLength="5"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label Width="40px" ID="RestrictDeliveries" runat="server" Text='<%# Bind("RestrictnumberofDeliveries") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Maximum Lines" SortExpression="MaximumNumberofLines">
                                        <EditItemTemplate>
                                            <asp:TextBox onkeypress="return IsNumberKey(event, this);" Width="40px" ID="txtMaximumLines"
                                                runat="server" Text='<%# Bind("MaximumNumberofLines") %>' MaxLength="5"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label Width="40px" ID="MaximumLines" runat="server" Text='<%# Bind("MaximumNumberofLines") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <%-- Here Lifts will considered as Pallets, no Lift Considrations , treat it as Pallets --%>
                                    <asp:TemplateField HeaderText="Maximum Pallets" SortExpression="MaximumNumberofLifts">
                                        <EditItemTemplate>
                                            <asp:TextBox onkeypress="return IsNumberKey(event, this);" Width="40px" ID="txtMaximumLifts"
                                                runat="server" Text='<%# Bind("MaximumNumberofLifts") %>' MaxLength="5"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label Width="40px" ID="MaximumLifts" runat="server" Text='<%# Bind("MaximumNumberofLifts") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Restrictsingledeliverypalletscountnothan" SortExpression="Restrictsingledeliverypallets">
                                        <EditItemTemplate>
                                            <asp:TextBox onkeypress="return IsNumberKey(event, this);" Width="40px" ID="txtRestrictsingledeliverypallets"
                                                runat="server" Text='<%# Bind("Restrictsingledeliverypallets") %>' MaxLength="5"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label Width="40px" ID="Restrictsingledeliverypallets" runat="server" Text='<%# Bind("Restrictsingledeliverypallets") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Restrictsingledeliverylinescountnothan" SortExpression="Restrictsingledeliverylines">
                                        <EditItemTemplate>
                                            <asp:TextBox onkeypress="return IsNumberKey(event, this);" Width="40px" ID="txtRestrictsingledeliverylines"
                                                runat="server" Text='<%# Bind("Restrictsingledeliverylines") %>' MaxLength="5"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label Width="40px" ID="Restrictsingledeliverylines" runat="server" Text='<%# Bind("Restrictsingledeliverylines") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:CommandField ShowEditButton="True" HeaderText="Action">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle Width="10%" HorizontalAlign="Left" />
                                    </asp:CommandField>
                                </Columns>
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSpecificDeliveryConstraints" runat="server" Width="180px" CssClass="button"
            OnClick="btnSpecificDeliveryConstraints_Click" />
    </div>
</asp:Content>

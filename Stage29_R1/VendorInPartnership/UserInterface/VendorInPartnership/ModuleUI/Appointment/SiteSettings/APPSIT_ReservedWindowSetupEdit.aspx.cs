﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;


public partial class APPSIT_ReservedWindowSetupEdit : CommonPage
{
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected string VendorReq = WebCommon.getGlobalResourceValue("VendorReq");
    protected string AtLeaseOneDay = WebCommon.getGlobalResourceValue("AtLeaseOneDay");
    protected string FixedSlotsGreaterThan0 = WebCommon.getGlobalResourceValue("FixedSlotsGreaterThan0");
    string strCorrectValueValidation = WebCommon.getGlobalResourceValue("CorrectValueValidation");
    string startEndTimeAlreadyExist = WebCommon.getGlobalResourceValue("StartEndTimeExist");
    string isStartEndTimeEqual = WebCommon.getGlobalResourceValue("IsStartEndTimeEqual");
    string isStartTimeGreaterThanEndTime = WebCommon.getGlobalResourceValue("IsStartTimeGreaterThanEndTime");

    protected void Page_InIt(object sender, EventArgs e) {
        ucSeacrhVendor1.CurrentPage = this;
        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = true;
        ddlSite.CurrentPage = this;
        ddlSite.TimeSlotWindow = "W";
        ddlSite.innerControlddlSite.AutoPostBack = true;
    }

    protected void Page_Load(object sender, EventArgs e) {

        if (!IsPostBack) {
            if (GetQueryStringValue("SiteID") != null) {
                hdnSiteID.Value = GetQueryStringValue("SiteID").ToString();
                ucSeacrhVendor1.SiteID = Convert.ToInt32(hdnSiteID.Value);
            }
            BindTimes();
            BindDoorNo();
            BindCarrier();
            BindControls();

            if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro") {
                btnDelete.Visible = false;
                btnSave.Visible = false;
            }
        }       
    }

    public override void SitePost_Load() {
        base.SitePost_Load();
        if (!Page.IsPostBack) {
            ddlSite.innerControlddlSite.AutoPostBack = true;
            if (GetQueryStringValue("SiteID") != null)
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(GetQueryStringValue("SiteID")));
                        
        }
        
    }

    public override void SiteSelectedIndexChanged() {
        hdnSiteID.Value = ddlSite.innerControlddlSite.SelectedItem.Value;
        if (rdoVendor.Checked) {
            ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            ucSeacrhVendor1.ClearSearch();
        }
        else if (rdoCarrier.Checked) {         
            BindCarrier();
        }
    }

    private void BindCarrier() {
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();

        oMASCNT_CarrierBE.Action = "ShowAll";
        oMASCNT_CarrierBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_CarrierBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMASCNT_CarrierBE.Site = new MAS_SiteBE();
        oMASCNT_CarrierBE.Site.SiteID = Convert.ToInt32(hdnSiteID.Value);

        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);

        FillControls.FillDropDown(ref ddlCarrier, lstCarrier, "CarrierName", "CarrierID");
    }

    private void BindControls() {
        MASSIT_TimeWindowBE oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();

        //oMASSIT_FixedSlotBE.SiteID = Convert.ToInt32(hdnSiteID.Value);

        if (GetQueryStringValue("TimeWindowID") != null) {

            ucSeacrhVendor1.Visible = false;
            btnDelete.Visible = true;

            oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(GetQueryStringValue("TimeWindowID"));

            //if (GetQueryStringValue("BP") == null)
            //    oMASSIT_TimeWindowBE.Action = "ShowAll";
            //else if (GetQueryStringValue("BP") == "Pro")
                oMASSIT_TimeWindowBE.Action = "ShowAllReserved";

            //oMASSIT_TimeWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            //oMASSIT_TimeWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);

                List<MASSIT_TimeWindowBE> lstFixedSlot = oMASSIT_TimeWindowBAL.GetReservedWindowBAL(oMASSIT_TimeWindowBE);

            if (lstFixedSlot.Count > 0) {

                if (lstFixedSlot[0].ScheduleType == "Vendor") {
                    rdoCarrier.Checked = false;
                    rdoVendor.Checked = true;
                    lblCarrier.Visible = false;
                    lblVendor.Visible = true;
                }
                else if (lstFixedSlot[0].ScheduleType == "Carrier") {
                    rdoCarrier.Checked = true;
                    rdoVendor.Checked = false;
                    lblVendor.Visible = false;
                    lblCarrier.Visible = true;
                }

                hdnSelectedVendorID.Value = Convert.ToString(lstFixedSlot[0].IncludedVendorIDs);                
                ltvendorName.Text = lstFixedSlot[0].VendorName;
                
                ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(lstFixedSlot[0].StartSlotTimeID.ToString()));
                ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(lstFixedSlot[0].EndSlotTimeID.ToString()));
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(lstFixedSlot[0].SiteID.ToString()));
                ddlSite.innerControlddlSite.Enabled = false;
                ddlDoorNo.SelectedIndex = ddlDoorNo.Items.IndexOf(ddlDoorNo.Items.FindByValue(lstFixedSlot[0].SiteDoorNumberID.ToString()));
                chkFri.Checked = lstFixedSlot[0].Friday != null ? Convert.ToBoolean(lstFixedSlot[0].Friday) : false;
                chkMon.Checked = lstFixedSlot[0].Monday != null ? Convert.ToBoolean(lstFixedSlot[0].Monday) : false;
                chkSat.Checked = lstFixedSlot[0].Saturday != null ? Convert.ToBoolean(lstFixedSlot[0].Saturday) : false;
                chkSun.Checked = lstFixedSlot[0].Sunday != null ? Convert.ToBoolean(lstFixedSlot[0].Sunday) : false;
                chkThurs.Checked = lstFixedSlot[0].Thursday != null ? Convert.ToBoolean(lstFixedSlot[0].Thursday) : false;
                chkTue.Checked = lstFixedSlot[0].Tuesday != null ? Convert.ToBoolean(lstFixedSlot[0].Tuesday) : false;
                chkWed.Checked = lstFixedSlot[0].Wednesday != null ? Convert.ToBoolean(lstFixedSlot[0].Wednesday) : false;
                txtMaxPallets.Text = lstFixedSlot[0].MaximumPallets != null ? Convert.ToString(lstFixedSlot[0].MaximumPallets) : "";
                txtMaxLines.Text = lstFixedSlot[0].MaximumLines != null ? Convert.ToString(lstFixedSlot[0].MaximumLines) : "";
                txtMaxCartons.Text = lstFixedSlot[0].MaximumCartons != null ? Convert.ToString(lstFixedSlot[0].MaximumCartons) : "";
                txtMaxDeliveries.Text = lstFixedSlot[0].MaxDeliveries != null ? Convert.ToString(lstFixedSlot[0].MaxDeliveries) : "";
            }

            rdoCarrier.Enabled = false;
            rdoVendor.Enabled = false;

        }
    }
       

    public void BindTimes() {
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstTimes = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);
        lstTimes = lstTimes.FindAll(delegate(SYS_SlotTimeBE t) { return t.SlotTimeID <= 96; });
        if (lstTimes != null && lstTimes.Count > 0) {
            FillControls.FillDropDown(ref ddlStartTime, lstTimes, "SlotTime", "SlotTimeID");
            FillControls.FillDropDown(ref ddlEndTime, lstTimes, "SlotTime", "SlotTimeID");
        }

        //if (GetQueryStringValue("SiteId") != null) {
        //    List<SYS_SlotTimeBE> lstStartEndTime = new List<SYS_SlotTimeBE>();
        //    SYS_SlotTimeBE startEndTime = null;
        //    //------------------------------------------------------------------------------------------------------------------------------

        //    MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        //    MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();
        //    oMASSIT_WeekSetupBE.Action = "ShowAll";
        //    oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteId"));
        //    oMASSIT_WeekSetupBE.EndWeekday = GetQueryStringValue("WeekDay");
        //    List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
        //    //------------------------------------------------------------------------------------------------------------------------------

        //    SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        //    SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
        //    oSYS_SlotTimeBE.Action = "ShowAll";
        //    List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);
        //    lstSlotTime = lstSlotTime.FindAll(delegate(SYS_SlotTimeBE t) { return t.SlotTimeID <= 96; });
        //    //------------------------------------------------------------------------------------------------------------------------------

        //    for (int jCount = 0; jCount < lstSlotTime.Count; jCount++) {
        //        string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + "." + lstWeekSetup[0].StartTime.Value.Minute.ToString();
        //        string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();

        //        decimal dSlotTime = Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.'));
        //        decimal dStartTime = Convert.ToDecimal(StartTime);
        //        decimal dEndTime = Convert.ToDecimal(EndTime);

        //        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        //            dEndTime = Convert.ToDecimal("24.00");

        //        if (dSlotTime >= dStartTime && dSlotTime < dEndTime) {
        //            startEndTime = new SYS_SlotTimeBE();
        //            startEndTime.SlotTime = lstSlotTime[jCount].SlotTime;
        //            startEndTime.SlotTimeID = lstSlotTime[jCount].SlotTimeID;
        //            lstStartEndTime.Add(startEndTime);

        //        }
        //    }
        //    //------------------------------------------------------------------------------------------------------------------------------

        //    if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday) {
        //        for (int jCount = 0; jCount < lstSlotTime.Count; jCount++) {
        //            string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();

        //            if (Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.')) < Convert.ToDecimal(EndTime)) {
        //                startEndTime = new SYS_SlotTimeBE();
        //                startEndTime.SlotTime = lstSlotTime[jCount].SlotTime;
        //                startEndTime.SlotTimeID = lstSlotTime[jCount].SlotTimeID;
        //                lstStartEndTime.Add(startEndTime);
        //            }
        //        }
        //    }
        //    //------------------------------------------------------------------------------------------------------------------------------

        //    FillControls.FillDropDown(ref ddlStartTime, lstStartEndTime, "SlotTime", "SlotTimeID");
        //    FillControls.FillDropDown(ref ddlEndTime, lstStartEndTime, "SlotTime", "SlotTimeID");            
        //}
    }

    public void BindDoorNo() {
        MASSIT_DoorNoSetupBAL oMASSIT_DoorNoSetupBAL = new MASSIT_DoorNoSetupBAL();
        MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE = new MASSIT_DoorNoSetupBE();

        oMASSIT_DoorNoSetupBE.Action = "ShowAll";
        oMASSIT_DoorNoSetupBE.SiteID = Convert.ToInt32(hdnSiteID.Value);
        oMASSIT_DoorNoSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_DoorNoSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMASSIT_DoorNoSetupBE.IsActive = true;

        List<MASSIT_DoorNoSetupBE> lstDoorNo = oMASSIT_DoorNoSetupBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorNoSetupBE);

        FillControls.FillDropDown(ref ddlDoorNo, lstDoorNo, "DoorNumber", "SiteDoorNumberID");

    }  

    protected void btnSave_Click(object sender, EventArgs e) {

        if (txtMaxPallets.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtMaxPallets.Text.Trim().Replace(".", string.Empty))) {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtMaxPallets.Focus();
            return;
        }

        if (txtMaxCartons.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtMaxCartons.Text.Trim().Replace(".", string.Empty))) {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtMaxCartons.Focus();
            return;
        }

        if (txtMaxLines.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtMaxLines.Text.Trim().Replace(".", string.Empty))) {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtMaxLines.Focus();
            return;
        }

        if (txtMaxDeliveries.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtMaxDeliveries.Text.Trim().Replace(".", string.Empty))) {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtMaxDeliveries.Focus();
            return;
        }

        DateTime dtmDdlFirstTime = DateTime.Now;
        DateTime dtmDdlLastTime = DateTime.Now;
        DateTime dtmDdlSelStartTime = DateTime.Now;
        DateTime dtmDdlSelEndTime = DateTime.Now;

        if (ddlStartTime.Items.Count > 0) {
            //dtmCurrentDay12AMTime = Convert.ToDateTime("2013-01-01 00:00:00"); // Current Day 12 AM time.

            dtmDdlFirstTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlStartTime.Items[0].Text));
            dtmDdlLastTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlStartTime.Items[(ddlStartTime.Items.Count - 1)].Text));

            dtmDdlSelStartTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlStartTime.SelectedItem));
            dtmDdlSelEndTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlEndTime.SelectedItem));


            if (dtmDdlFirstTime > dtmDdlLastTime && dtmDdlSelStartTime > dtmDdlLastTime && dtmDdlSelEndTime > dtmDdlLastTime) {
                if (dtmDdlSelStartTime > dtmDdlSelEndTime) {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartTimeGreaterThanEndTime + "');</script>", false);
                    ddlStartTime.Focus();
                    return;

                }
                else if (dtmDdlSelStartTime == dtmDdlSelEndTime) {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartEndTimeEqual + "');</script>", false);
                    ddlStartTime.Focus();
                    return;
                }
            }
            else if (dtmDdlFirstTime > dtmDdlLastTime && dtmDdlSelStartTime < dtmDdlLastTime && dtmDdlSelEndTime < dtmDdlLastTime) {
                if (dtmDdlSelStartTime > dtmDdlSelEndTime) {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartTimeGreaterThanEndTime + "');</script>", false);
                    ddlStartTime.Focus();
                    return;
                }
                if (dtmDdlSelStartTime == dtmDdlSelEndTime) {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartEndTimeEqual + "');</script>", false);
                    ddlStartTime.Focus();
                    return;
                }
            }
            else if (dtmDdlFirstTime > dtmDdlLastTime && dtmDdlSelStartTime > dtmDdlLastTime && dtmDdlSelEndTime < dtmDdlLastTime) {
                //blnStatus = true;
            }
            else if (dtmDdlFirstTime > dtmDdlLastTime && dtmDdlSelStartTime < dtmDdlLastTime && dtmDdlSelEndTime > dtmDdlLastTime) {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartTimeGreaterThanEndTime + "');</script>", false);
                ddlStartTime.Focus();
                return;
            }
            else if (dtmDdlSelStartTime == dtmDdlSelEndTime) {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartEndTimeEqual + "');</script>", false);
                ddlStartTime.Focus();
                return;
            }
            else if (dtmDdlSelStartTime > dtmDdlSelEndTime) {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isStartTimeGreaterThanEndTime + "');</script>", false);
                ddlStartTime.Focus();
                return;
            }
        }



        //#region Check Vendor Micellaneous Constraints
        string errorMessage = string.Empty;
        //if (rdoVendor.Checked) {
        //    if (GetQueryStringValue("TimeWindowID") == null)   // when we add new fixed slot
        //    {
        //        PostVendorNo_Change();
        //        if (string.IsNullOrEmpty(hdnSelectedVendorID.Value)) {
        //            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('Vendor No. not valid');</script>");
        //            return;
        //        }
        //    }

        //}
        //#endregion
        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        MASSIT_TimeWindowBE oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

        #region Get Already Booked Slots
        oMASSIT_TimeWindowBE.Action = "ShowAllReserved";
        //if (GetQueryStringValue("TimeWindowID") != null) {
        //    oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(GetQueryStringValue("TimeWindowID"));
        //}
        oMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(hdnSiteID.Value);
        oMASSIT_TimeWindowBE.SiteDoorNumberID = Convert.ToInt32(ddlDoorNo.SelectedItem.Value);
             

        oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        List<MASSIT_TimeWindowBE> lstAllStartEndTime = oMASSIT_TimeWindowBAL.GetReservedWindowBAL(oMASSIT_TimeWindowBE);

        if (GetQueryStringValue("TimeWindowID") != null) {
            lstAllStartEndTime = lstAllStartEndTime.FindAll(delegate(MASSIT_TimeWindowBE St) { return St.TimeWindowID != Convert.ToInt32(GetQueryStringValue("TimeWindowID")); });
        }

        DateTime dtDBStartTime = DateTime.Now;
        DateTime dtDBEndTime = DateTime.Now;
        DateTime dtSelStartTime = DateTime.Now;
        DateTime dtSelEndTime = DateTime.Now;

        dtSelStartTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlStartTime.SelectedItem));
        dtSelEndTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlEndTime.SelectedItem));

        List<MASSIT_TimeWindowBE> lstAvailableSlots = new List<MASSIT_TimeWindowBE>();
        if (lstAllStartEndTime != null) {
            for (int index = 0; index < lstAllStartEndTime.Count; index++) {

                dtDBStartTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", lstAllStartEndTime[index].StartTime));
                dtDBEndTime = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", lstAllStartEndTime[index].EndTime));

                // Three conditions are being checked here.
                // 1. UI Start time should not be in between or equal of Databse Start and End time.
                // 2. UI End time should not be in between or equal of Databse Start and End time.
                // 3. UI Start time should not be less than or equal to Databse start time and UI End time should not Greater than or equal to Database End time.
                //if (
                //    (
                //    (Convert.ToInt32(ddlStartTime.SelectedValue) >= lstAllStartEndTime[index].StartSlotTimeID) && 
                //    (Convert.ToInt32(ddlStartTime.SelectedValue) <= lstAllStartEndTime[index].EndSlotTimeID)) // Condition 1

                //    || ((Convert.ToInt32(ddlEndTime.SelectedValue) >= lstAllStartEndTime[index].StartSlotTimeID) && (Convert.ToInt32(ddlEndTime.SelectedValue) <= lstAllStartEndTime[index].EndSlotTimeID)) // Condition 2
                //    || ((Convert.ToInt32(ddlStartTime.SelectedValue) <= lstAllStartEndTime[index].StartSlotTimeID) && (Convert.ToInt32(ddlEndTime.SelectedValue) >= lstAllStartEndTime[index].EndSlotTimeID))) // Condition 3

                if (
                        (
                            (dtDBStartTime >= dtSelStartTime) &&
                            (dtDBEndTime <= dtSelEndTime)
                        )
                        ||
                        (
                            (dtDBStartTime <= dtSelStartTime) &&
                            (dtDBEndTime <= dtSelEndTime) &&
                            (dtDBEndTime > dtSelStartTime) 
                        )
                        ||
                        (
                            (dtDBStartTime >= dtSelStartTime) &&
                            (dtDBEndTime >= dtSelEndTime) &&
                            (dtDBStartTime < dtSelEndTime)
                        )
                        ||
                        (
                            (dtDBStartTime <= dtSelStartTime) &&
                            (dtDBEndTime >= dtSelEndTime)
                        )
                    )
                {

                    if (chkMon.Checked) {
                        //lstAvailableSlots = lstAllStartEndTime.FindAll(delegate(MASSIT_TimeWindowBE St) { return St.Monday == true; });
                        if (lstAllStartEndTime[index].Monday == true) {
                            errorMessage = "This Window is already reserved for [" + lstAllStartEndTime[index].VendorName + "] for day - Monday";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                            return;
                        }
                    }
                    else if (chkTue.Checked) {
                        //lstAvailableSlots = lstAllStartEndTime.FindAll(delegate(MASSIT_TimeWindowBE St) { return St.Tuesday == true; });
                        if (lstAllStartEndTime[index].Tuesday == true) {
                            errorMessage = "This Window is already reserved for [" + lstAvailableSlots[0].VendorName + "] for day - Tuesday";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                            return;
                        }
                    }
                    else if (chkWed.Checked) {
                        //lstAvailableSlots = lstAllStartEndTime.FindAll(delegate(MASSIT_TimeWindowBE St) { return St.Wednesday == true; });
                        if (lstAllStartEndTime[index].Wednesday == true) {
                            errorMessage = "This Window is already reserved for [" + lstAvailableSlots[0].VendorName + "] for day - Wednesday";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                            return;
                        }
                    }
                    else if (chkThurs.Checked) {
                        //lstAvailableSlots = lstAllStartEndTime.FindAll(delegate(MASSIT_TimeWindowBE St) { return St.Thursday == true; });
                        if (lstAllStartEndTime[index].Thursday == true) {
                            errorMessage = "This Window is already reserved for [" + lstAvailableSlots[0].VendorName + "] for day - Thursday";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                            return;
                        }
                    }
                    else if (chkFri.Checked) {
                        //lstAvailableSlots = lstAllStartEndTime.FindAll(delegate(MASSIT_TimeWindowBE St) { return St.Friday == true; });
                        if (lstAllStartEndTime[index].Friday == true) {
                            errorMessage = "This Window is already reserved for [" + lstAvailableSlots[0].VendorName + "] for day - Friday";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                            return;
                        }
                    }
                    else if (chkSat.Checked) {
                        //lstAvailableSlots = lstAllStartEndTime.FindAll(delegate(MASSIT_TimeWindowBE St) { return St.Saturday == true; });
                        if (lstAllStartEndTime[index].Saturday == true) {
                            errorMessage = "This Window is already reserved for [" + lstAvailableSlots[0].VendorName + "] for day - Saturday";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                            return;
                        }
                    }
                    else if (chkSun.Checked) {
                        //lstAvailableSlots = lstAllStartEndTime.FindAll(delegate(MASSIT_TimeWindowBE St) { return St.Sunday == true; });
                        if (lstAllStartEndTime[index].Sunday == true) {
                            errorMessage = "This Window is already reserved for [" + lstAvailableSlots[0].VendorName + "] for day - Sunday";
                            ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                            return;
                        }
                    } 
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + startEndTimeAlreadyExist + "');</script>", false);
                    //ddlStartTime.Focus();
                    //return;
                }
            }
        }
        #endregion

        oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
        oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        if (GetQueryStringValue("TimeWindowID") == null) {

            oMASSIT_TimeWindowBE.Action = "AddReservedWindow";

            if (rdoVendor.Checked) {
                List<UP_VendorBE> oUP_VendorBEList = ucSeacrhVendor1.IsValidVendorCode();

                if (oUP_VendorBEList.Count == 0) {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('Please enter valid Vendor No.');</script>");
                    return;
                }
                else {
                    hdnSelectedVendorID.Value = oUP_VendorBEList[0].VendorID.ToString();
                }

                oMASSIT_TimeWindowBE.IncludedVendorIDs = hdnSelectedVendorID.Value;
                oMASSIT_TimeWindowBE.ScheduleType = "V";

            }
            else if (rdoCarrier.Checked) {
                oMASSIT_TimeWindowBE.IncludedCarrierIDs = ddlCarrier.SelectedItem.Value;
                oMASSIT_TimeWindowBE.ScheduleType = "C";
            }
            oMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(hdnSiteID.Value);
        }
        else {
            oMASSIT_TimeWindowBE.Action = "EditReservedWindow";
            oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(GetQueryStringValue("TimeWindowID").ToString());
        }      

       
        oMASSIT_TimeWindowBE.StartSlotTimeID = Convert.ToInt32(ddlStartTime.SelectedItem.Value);
        oMASSIT_TimeWindowBE.EndSlotTimeID = Convert.ToInt32(ddlEndTime.SelectedItem.Value);
        oMASSIT_TimeWindowBE.SiteDoorNumberID = Convert.ToInt32(ddlDoorNo.SelectedItem.Value);
        oMASSIT_TimeWindowBE.Monday = chkMon.Checked;
        oMASSIT_TimeWindowBE.Tuesday = chkTue.Checked;
        oMASSIT_TimeWindowBE.Wednesday = chkWed.Checked;
        oMASSIT_TimeWindowBE.Thursday = chkThurs.Checked;
        oMASSIT_TimeWindowBE.Friday = chkFri.Checked;
        oMASSIT_TimeWindowBE.Saturday = chkSat.Checked;
        oMASSIT_TimeWindowBE.Sunday = chkSun.Checked;
        oMASSIT_TimeWindowBE.MaximumCartons = txtMaxCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxCartons.Text.Trim()) : 0;
        oMASSIT_TimeWindowBE.MaximumLines = txtMaxLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxLines.Text.Trim()) : 0;
        oMASSIT_TimeWindowBE.MaximumPallets = txtMaxPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxPallets.Text.Trim()) : 0;
        oMASSIT_TimeWindowBE.MaxDeliveries = txtMaxDeliveries.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxDeliveries.Text.Trim()) : 0;

        oMASSIT_TimeWindowBAL.addEditReservedWindowBAL(oMASSIT_TimeWindowBE);

        EncryptQueryString("APPSIT_ReservedWindowSetupOverview.aspx?SiteID=" + hdnSiteID.Value);
    }

    protected void rdoVendor_CheckedChanged(object sender, EventArgs e) {
        ddlCarrier.Visible = false;
        ucSeacrhVendor1.Visible = true;
        lblVendor.Visible = true;
        lblCarrier.Visible = false;
        ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
    }

    protected void rdoCarrier_CheckedChanged(object sender, EventArgs e) {
        ddlCarrier.Visible = true;
        ucSeacrhVendor1.Visible = false;
        lblVendor.Visible = false;
        lblCarrier.Visible = true;
        BindCarrier();
    }

    protected void btnBack_Click(object sender, EventArgs e) {
        if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro")
            EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
        if (GetQueryStringValue("SiteID") != null)
            EncryptQueryString("APPSIT_ReservedWindowSetupOverview.aspx?SiteID=" + GetQueryStringValue("SiteID"));

    }

    protected void btnDelete_Click(object sender, EventArgs e) {
        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        MASSIT_TimeWindowBE oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

        if (GetQueryStringValue("TimeWindowID") != null) {
            oMASSIT_TimeWindowBE.TimeWindowID = Convert.ToInt32(GetQueryStringValue("TimeWindowID").ToString());
            oMASSIT_TimeWindowBE.Action = "DeleteReservedWindow";
            oMASSIT_TimeWindowBAL.DeleteWindowBAL(oMASSIT_TimeWindowBE);

            EncryptQueryString("APPSIT_ReservedWindowSetupOverview.aspx");
        }
    }

    public override void PostVendorNo_Change() {
        base.PostVendorNo_Change();
        //ucTextbox txtVendorNo = new ucTextbox();
        hdnSelectedVendorID.Value = ucSeacrhVendor1.VendorNo;
    }
}
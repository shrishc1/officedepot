﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="~/ModuleUI/Appointment/SiteSettings/APPSIT_VendorProcessingWindowOverview.aspx.cs"
    Inherits="APPSIT_VendorProcessingWindowOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblVendorProcessingWindow" runat="server" Text="Vendor Processing Window"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="APPSIT_VendorProcessingWindowEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" onsorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblSiteDesc" runat="server" Text='<%#Eval("Site.SiteName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor Name" SortExpression="Vendor.VendorName">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hlVendorName" runat="server" Text='<%#Eval("Vendor.VendorName") %>'
                                    NavigateUrl='<%# EncryptQuery("APPSIT_VendorProcessingWindowEdit.aspx?SiteVendorID=" + Eval("SiteVendorID"))%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="AverageNoofPalletsUnloadedperhour" DataField="APP_PalletsUnloadedPerHour"  SortExpression="APP_PalletsUnloadedPerHour">
                            <HeaderStyle HorizontalAlign="Center" Width="15%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                         <asp:TemplateField HeaderText="" >
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                               <cc1:ucLabel ID="blank" runat="server"></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="AverageNoofCartonsUnloadedperhour" DataField="APP_CartonsUnloadedPerHour" SortExpression="APP_CartonsUnloadedPerHour">
                            <HeaderStyle HorizontalAlign="Center" Width="15%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                         <asp:TemplateField HeaderText="" SortExpression="">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                               <cc1:ucLabel ID="blank" runat="server"></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="AverageNoofLinesReceivingperhour" DataField="APP_ReceiptingLinesPerHour" SortExpression="APP_ReceiptingLinesPerHour">
                            <HeaderStyle HorizontalAlign="Center" Width="15%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                          <asp:TemplateField HeaderText="">
                            <HeaderStyle Width="5%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                               <cc1:ucLabel ID="blank" runat="server"></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>

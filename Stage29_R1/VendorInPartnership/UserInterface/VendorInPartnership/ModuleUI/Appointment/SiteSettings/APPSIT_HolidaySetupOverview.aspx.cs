﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class APPSIT_HolidaySetupOverview : CommonPage
{
    protected void Page_InIt(object sender, EventArgs e) {
        ucCountry.CurrentPage = this;
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;
        ddlSite.IsAllDefault = true;
        ucCountry.IsAllRequired = true;
        ucCountry.IsAllDefault = true;
    }

    public override bool SitePrePage_Load() {
        return false;
    }

    public override void SitePost_Load() {
        base.SitePost_Load();
       
    }

    public override void CountryPost_Load() {
        if (!IsPostBack) {
            ucCountry.innerControlddlCountry.AutoPostBack = true;
            //ucCountry.innerControlddlCountry.SelectedItem.Value = "0";
            ddlSite.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);

            ddlSite.innerControlddlSite.AutoPostBack = true;
            ddlSite.BindSites();
            BindGrid();
        }
    }

    public override void SiteSelectedIndexChanged() {
        string URL = string.Empty;

        if (ddlSite.innerControlddlSite.SelectedItem != null)
            URL = "APPSIT_HolidaySetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value;
        else
            URL = "APPSIT_HolidaySetupEdit.aspx?SiteID=" + Session["SiteID"].ToString();
        btnAdd.NavigateUrl = URL;
    }

    public override void CountrySelectedIndexChanged() {
        ddlSite.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);       
        ddlSite.BindSites();        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Role"] != null && Session["Role"].ToString() != "") {
            if (Session["Role"].ToString() == "Carrier" || Session["Role"].ToString() == "Vendor")
                divbtnOpr.Visible = false;   
        }
        

        if (Session["SiteID"] == null) {
            EncryptQueryString("../../Security/Login.aspx");
        }

        string URL = string.Empty;

        if (ddlSite.innerControlddlSite.SelectedItem != null)
            URL = "APPSIT_HolidaySetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value;
        else
            URL = "APPSIT_HolidaySetupEdit.aspx?SiteID=" + Session["SiteID"].ToString();
        btnAdd.NavigateUrl = URL;

        ucExportToExcel1.GridViewControl = grdHoliday;
        ucExportToExcel1.FileName = "Site Holiday Closures";

        if (!Page.IsPostBack) {
           // ucCountry.innerControlddlCountry.SelectedItem.Value = "0";
           // ddlSite.innerControlddlSite.SelectedItem.Value = "0";
            txtYear.Text = DateTime.Now.Year.ToString();
        }
    }

    protected void grdHoliday_RowDataBound(object sender, GridViewRowEventArgs e) {
        GridViewRow gvr = e.Row;
        if (e.Row.RowType == DataControlRowType.DataRow) {
            HyperLink hpLink = (HyperLink)e.Row.FindControl("hpLink");
            Label ltHolidayDate = (Label)e.Row.FindControl("ltHolidayDate");
            if (Session["Role"] != null && Session["Role"].ToString() != "") {
                if (Session["Role"].ToString() == "Carrier" || Session["Role"].ToString() == "Vendor") {
                    hpLink.Visible = false;
                    ltHolidayDate.Visible = true;
                }
            }
        }
    }

    protected void btnGo_Click(object sender, EventArgs e) {
        BindGrid();
    }

    private void BindGrid() {
        MASSIT_HolidayBE oMASSIT_HolidayBE = new MASSIT_HolidayBE();
        MASSIT_HolidayBAL oMASSIT_HolidayBAL = new MASSIT_HolidayBAL();

        oMASSIT_HolidayBE.Action = "ShowAll";
        oMASSIT_HolidayBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        if (ddlSite.innerControlddlSite.SelectedIndex > 0)
            oMASSIT_HolidayBE.Site.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : 0;
        else {
            oMASSIT_HolidayBE.SiteIDs = ddlSite.UserSiteIDs;
        }

        if (!string.IsNullOrEmpty(txtYear.Text.Trim()))
            oMASSIT_HolidayBE.HolidayYear = new DateTime(Convert.ToInt32(txtYear.Text), 1, 1);


        List<MASSIT_HolidayBE> lstHoliday = oMASSIT_HolidayBAL.GetHolidaysBAL(oMASSIT_HolidayBE);

        if (lstHoliday != null && lstHoliday.Count > 0) {
            grdHoliday.Visible = true;
            grdHoliday.DataSource = lstHoliday;
            grdHoliday.DataBind();
            ViewState["lstHoliday"] = lstHoliday;
        }
        else {
            grdHoliday.Visible = false;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {
        return Utilities.GenericListHelper<MASSIT_HolidayBE>.SortList((List<MASSIT_HolidayBE>)ViewState["lstHoliday"], e.SortExpression, e.SortDirection).ToArray();
    }
}
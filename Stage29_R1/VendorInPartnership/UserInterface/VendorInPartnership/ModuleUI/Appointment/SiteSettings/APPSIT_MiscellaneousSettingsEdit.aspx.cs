﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using Utilities; 
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;

public partial class APPSIT_MiscellaneousSettingsEdit : CommonPage
{
    string strValidBeforeTime = WebCommon.getGlobalResourceValue("ValidBeforeTime");
    string strByDate = WebCommon.getGlobalResourceValue("ByDate");
    string strByPurchaseOrder = WebCommon.getGlobalResourceValue("ByPurchaseOrder");
    string strPerBooking = WebCommon.getGlobalResourceValue("PerBooking");
    string strPerVendor = WebCommon.getGlobalResourceValue("PerVendor");
    string strTimeSlot = WebCommon.getGlobalResourceValue("TimeSlot");
    string strTimeWindow = WebCommon.getGlobalResourceValue("TimeWindow");
    string strAlternateSlotTurnedOn = WebCommon.getGlobalResourceValue("AlternateSlotTurnedOn");
    string strAlternateSlotTurnedOff = WebCommon.getGlobalResourceValue("AlternateSlotTurnedOff");

    string strDelArrived = WebCommon.getGlobalResourceValue("DelArrived");
    string strDelivaryunload = WebCommon.getGlobalResourceValue("Delivaryunload");
    string strUnexpectedDelivery = WebCommon.getGlobalResourceValue("UnexpectedDelivery");

    string strBookingExcllusionYes = WebCommon.getGlobalResourceValue("Yes");
    string strBookingExcllusionNo = WebCommon.getGlobalResourceValue("No");

    MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = null;
    APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = null; 
    MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE = null;
    APPCNT_DeliveryTypeBAL oMASCNT_DeliveryTypeBAL = null;
    ListItem listItem = null;

    #region Events

    protected string CorrectValueValidation = WebCommon.getGlobalResourceValue("CorrectValueValidation");
    string strCorrectValueValidation = WebCommon.getGlobalResourceValue("CorrectValueValidation");


    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();

        if (!IsPostBack)
        {
            ddlSite.innerControlddlSite.SelectedIndex = 0;
            ddlSite.innerControlddlSite.AutoPostBack = true;
            this.BindVehicleType();
            this.BindDelivaryType();
            BindSiteMicellaneousSettings();            
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlSite.innerControlddlSite.AutoPostBack = true;
            this.SetResourceFilesText();
        }

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPSIT_MiscellaneousSettingsOverview.aspx");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        MAS_SiteBE oMAS_SITEBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SITEBAL = new MAS_SiteBAL();

        //DateTime? dtBefore = (DateTime?)null;
        //if (txtNoticePeriodTime.Text.Trim() != string.Empty)
        //   dtBefore = new DateTime(1900, 1, 1, Convert.ToInt32(txtNoticePeriodTime.Text.Split(':')[0]), Convert.ToInt32(txtNoticePeriodTime.Text.Split(':')[1]), 0);

        //txtToleratedDays

        if (txtToleratedDays.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtToleratedDays.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtToleratedDays.Focus();
            return;
        }

        if (txtLinesPerFTE.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtLinesPerFTE.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtLinesPerFTE.Focus();
            return;
        }

        if (txtAverageUnloading.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtAverageUnloading.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtAverageUnloading.Focus();
            return;
        }

        if (txtAverageCartonsUnloaded.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtAverageCartonsUnloaded.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtAverageCartonsUnloaded.Focus();
            return;
        }

        if (txtAverageReceipting.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtAverageReceipting.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtAverageReceipting.Focus();
            return;
        }

        //----------phase 14 point 28-------------------//
        if (txtAveragePutaway.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtAveragePutaway.Text.Trim().Replace(".", string.Empty))) {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtAveragePutaway.Focus();
            return;
        }
        //---------------------------------------------//

        if (txtNoticePeriodDays.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtNoticePeriodDays.Text.Trim()))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtNoticePeriodDays.Focus();
            return;
        }

        if (txtNoticePeriodTime.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtNoticePeriodTime.Text.Trim().Replace(":", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strValidBeforeTime + "');</script>");
            txtNoticePeriodTime.Focus();
            return;
        }

        DateTime? dtBefore = (DateTime?)null;
        if (txtNoticePeriodTime.Text.Trim() != string.Empty)
        {
            int intCheckValidTime = -1;
            intCheckValidTime = txtNoticePeriodTime.Text.Trim().IndexOf(":");
            if (intCheckValidTime != (-1))
            {
                int intHours = Convert.ToInt32(txtNoticePeriodTime.Text.Split(':')[0]);
                int intMinutes = Convert.ToInt32(txtNoticePeriodTime.Text.Split(':')[1]);

                if (intHours > 24 || intMinutes > 59)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strValidBeforeTime + "');</script>");
                    txtNoticePeriodTime.Focus();
                    return;
                }
                else
                {
                    dtBefore = new DateTime(1900, 1, 1, intHours, intMinutes, 0);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strValidBeforeTime + "');</script>");
                txtNoticePeriodTime.Focus();
                return;
            }
        }

        oMAS_SITEBE.Action = "UpdateSiteMiscellaneous";
        oMAS_SITEBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMAS_SITEBE.IsPreAdviseNoteRequired = chkPreAdviseNote1.Checked;
        if (!string.IsNullOrEmpty(txtToleratedDays.Text) && !string.IsNullOrWhiteSpace(txtToleratedDays.Text))
            oMAS_SITEBE.ToleranceDueDay = rdoToleranceDaysFixed.Checked ? 0 : (rdoToleranceDaysOpen.Checked ? -1 : Convert.ToInt32(txtToleratedDays.Text));
        else
            oMAS_SITEBE.ToleranceDueDay = rdoToleranceDaysFixed.Checked ? 0 : (rdoToleranceDaysOpen.Checked ? -1 : 0);

        oMAS_SITEBE.LinePerFTE = string.IsNullOrEmpty(txtLinesPerFTE.Text) ? (decimal?)null : Convert.ToDecimal(txtLinesPerFTE.Text);
        oMAS_SITEBE.AveragePalletUnloadedPerManHour = string.IsNullOrEmpty(txtAverageUnloading.Text) ? (int?)null : Convert.ToInt32(txtAverageUnloading.Text);  //Convert.ToInt32(txtAverageUnloading.Text);
        oMAS_SITEBE.AverageCartonUnloadedPerManHour = string.IsNullOrEmpty(txtAverageCartonsUnloaded.Text) ? (int?)null : Convert.ToInt32(txtAverageCartonsUnloaded.Text);  //Convert.ToInt32(txtAverageCartonsUnloaded.Text);
        oMAS_SITEBE.AverageLinesReceiving = string.IsNullOrEmpty(txtAverageReceipting.Text) ? (int?)null : Convert.ToInt32(txtAverageReceipting.Text);  // Convert.ToInt32(txtAverageReceipting.Text);
        //---Stage 14 R2 Point 28---//
        oMAS_SITEBE.APP_AveragePalletPutaway = string.IsNullOrEmpty(txtAveragePutaway.Text) ? (int?)null : Convert.ToInt32(txtAveragePutaway.Text);  
        //-------------------------//

        oMAS_SITEBE.BookingNoticePeriodInDays = txtNoticePeriodDays.Text != string.Empty ? Convert.ToInt32(txtNoticePeriodDays.Text) : (int?)null;
        oMAS_SITEBE.BookingNoticeTime = dtBefore;
        oMAS_SITEBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_SITEBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        if (rblByDatePO.SelectedIndex == 0)
            oMAS_SITEBE.ByDatePO = "D";
        else
            oMAS_SITEBE.ByDatePO = "P";

        if (rblPerBookingVendor.SelectedIndex == 0)
            oMAS_SITEBE.PerBookingVendor = "B";
        else
            oMAS_SITEBE.PerBookingVendor = "V";

        if (ddlDefaultDeliveryType.SelectedIndex != 0)
            oMAS_SITEBE.DeliveryTypeId = Convert.ToInt32(ddlDefaultDeliveryType.SelectedValue);
        else
            oMAS_SITEBE.DeliveryTypeId = 0;

        if (ddlDefaultVehicleType.SelectedIndex != 0)
            oMAS_SITEBE.VehicleTypeId = Convert.ToInt32(ddlDefaultVehicleType.SelectedValue);
        else
            oMAS_SITEBE.VehicleTypeId = 0;

        if (rblAlternateSlotTurnedOnOff.SelectedIndex == 0)
            oMAS_SITEBE.AlternateSlotOnOff = "Y";
        else
            oMAS_SITEBE.AlternateSlotOnOff = "N";

        if (rblTimeSlotWindow.SelectedIndex == 0)
            oMAS_SITEBE.TimeSlotWindow = "S";
        else
            oMAS_SITEBE.TimeSlotWindow = "W";

        oMAS_SITEBE.DeliveryArrived = "N";
        oMAS_SITEBE.DeliveryUnload = "N";
        oMAS_SITEBE.UnexpectedDelivery = "N";
        for (int index = 0; index < cblSupervisorConfirmationType.Items.Count; index++) 
        {
            if (cblSupervisorConfirmationType.Items[index].Selected)
            {
                switch (index)
                {
                    case 0:
                        oMAS_SITEBE.DeliveryArrived = "Y";
                        break;
                    case 1:
                        oMAS_SITEBE.DeliveryUnload = "Y";
                        break;
                    case 2:
                        oMAS_SITEBE.UnexpectedDelivery = "Y";
                        break;
                    default:
                        break;
                }
            }
        }

        //----Stage 4 Point 13----//
        oMAS_SITEBE.MaxCartonsPerDelivery = txtMaxCartons.Text != string.Empty ? Convert.ToInt32(txtMaxCartons.Text) : (int?)null;
        oMAS_SITEBE.MaxLinesPerDelivery = txtMaxLines.Text != string.Empty ? Convert.ToInt32(txtMaxLines.Text) : (int?)null;
        oMAS_SITEBE.MaxPalletsPerDelivery = txtMaxPallets.Text != string.Empty ? Convert.ToInt32(txtMaxPallets.Text) : (int?)null;
        //------------------------//

        //---Stage 5 Point 1-----//
        oMAS_SITEBE.IsBookingExclusions = rblBookingExclusions.SelectedIndex == 0 ? true : false;
        //-----------------------//

        //---Stage 7 Point 29-----//
        oMAS_SITEBE.NonTimeStart = txtStartTime.Text;
        oMAS_SITEBE.NonTimeEnd = txtEndTime.Text;
        //-----------------------//
        if(!string.IsNullOrEmpty(ucpalletPercentage.Text))
        oMAS_SITEBE.WindowVolumePrcentageSetting = Convert.ToInt32(ucpalletPercentage.Text);

        oMAS_SITEBAL.addEditSiteSettingsBAL(oMAS_SITEBE);

        EncryptQueryString("APPSIT_MiscellaneousSettingsOverview.aspx");
    }
    #endregion

    #region Methods

    private void BindSiteMicellaneousSettings()
    {
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        if (GetQueryStringValue("SiteID") != null)
        {
            oMAS_SiteBE.Action = "ShowAll";
            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));

            List<MAS_SiteBE> lstSiteMisSettings = new List<MAS_SiteBE>();
            lstSiteMisSettings = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);

            if (lstSiteMisSettings != null)
            {
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(lstSiteMisSettings[0].SiteID.ToString()));
                chkPreAdviseNote1.Checked = Convert.ToBoolean(lstSiteMisSettings[0].IsPreAdviseNoteRequired);

                if (lstSiteMisSettings[0].ToleranceDueDay == 0)
                    rdoToleranceDaysFixed.Checked = true;
                else if (lstSiteMisSettings[0].ToleranceDueDay == -1)
                    rdoToleranceDaysOpen.Checked = true;
                else {
                    rdoToleranceDays.Checked = true;
                    txtToleratedDays.Text = Convert.ToString(lstSiteMisSettings[0].ToleranceDueDay);
                }


                txtLinesPerFTE.Text = Convert.ToString(lstSiteMisSettings[0].LinePerFTE);
                txtAverageUnloading.Text = Convert.ToString(lstSiteMisSettings[0].AveragePalletUnloadedPerManHour);
                txtAverageCartonsUnloaded.Text = Convert.ToString(lstSiteMisSettings[0].AverageCartonUnloadedPerManHour);
                txtAverageReceipting.Text = Convert.ToString(lstSiteMisSettings[0].AverageLinesReceiving);
                //---Stage 14 R2 Point 28---//
                txtAveragePutaway.Text = Convert.ToString(lstSiteMisSettings[0].APP_AveragePalletPutaway);
                //------------------------//
                txtNoticePeriodDays.Text = Convert.ToString(lstSiteMisSettings[0].BookingNoticePeriodInDays);
                if (lstSiteMisSettings[0].AveragePalletUnloadedPerManHour != null)
                    lblPalletsUnloadTime.Text = Convert.ToString(Math.Round(Convert.ToDecimal(lstSiteMisSettings[0].AveragePalletUnloadedPerManHour.ToString()) / 60, 2));
                if (lstSiteMisSettings[0].AverageCartonUnloadedPerManHour != null)
                    lblCartonsUnloadTime.Text = Convert.ToString(Math.Round(Convert.ToDecimal(lstSiteMisSettings[0].AverageCartonUnloadedPerManHour.ToString()) / 60, 2));
                if (lstSiteMisSettings[0].AverageLinesReceiving != null)
                    lblLinesreceiving.Text = Convert.ToString(Math.Round(Convert.ToDecimal(lstSiteMisSettings[0].AverageLinesReceiving.ToString()) / 60, 2));

                //---Stage 14 R2 Point 28---//
                if (lstSiteMisSettings[0].APP_AveragePalletPutaway != null)
                lblpuatway.Text = Convert.ToString(Math.Round(Convert.ToDecimal(lstSiteMisSettings[0].APP_AveragePalletPutaway.ToString()) / 60, 2));
                //------------------------//
                if (lstSiteMisSettings[0].BookingNoticeTime != null && lstSiteMisSettings[0].BookingNoticeTime.ToString() != "")
                    txtNoticePeriodTime.Text = lstSiteMisSettings[0].BookingNoticeTime.Value.Hour.ToString() + ":" + lstSiteMisSettings[0].BookingNoticeTime.Value.Minute.ToString();

                if (lstSiteMisSettings[0].ByDatePO == String.Empty || lstSiteMisSettings[0].ByDatePO.Trim().ToUpper() == "D")
                    rblByDatePO.SelectedIndex = 0;
                else
                    rblByDatePO.SelectedIndex = 1;

                if (lstSiteMisSettings[0].PerBookingVendor == String.Empty || lstSiteMisSettings[0].PerBookingVendor.Trim().ToUpper() == "B")
                    rblPerBookingVendor.SelectedIndex = 0;
                else
                    rblPerBookingVendor.SelectedIndex = 1;

                if (lstSiteMisSettings[0].DeliveryTypeId != null)
                    ddlDefaultDeliveryType.SelectedIndex = ddlDefaultDeliveryType.Items.IndexOf(ddlDefaultDeliveryType.Items.FindByValue(Convert.ToString(lstSiteMisSettings[0].DeliveryTypeId)));
                else
                    ddlDefaultDeliveryType.SelectedIndex = 0;

                if (lstSiteMisSettings[0].VehicleTypeId != null)
                    ddlDefaultVehicleType.SelectedIndex = ddlDefaultVehicleType.Items.IndexOf(ddlDefaultVehicleType.Items.FindByValue(Convert.ToString(lstSiteMisSettings[0].VehicleTypeId)));
                else
                    ddlDefaultVehicleType.SelectedIndex = 0;

                if (lstSiteMisSettings[0].AlternateSlotOnOff == String.Empty || lstSiteMisSettings[0].AlternateSlotOnOff.Trim().ToUpper() == "Y")
                    rblAlternateSlotTurnedOnOff.SelectedIndex = 0;
                else
                    rblAlternateSlotTurnedOnOff.SelectedIndex = 1;

                #region CheckBoxList being set here.

                for (int index = 0; index < cblSupervisorConfirmationType.Items.Count; index++)
                {
                    cblSupervisorConfirmationType.Items[index].Selected = false;                    
                }

                if (lstSiteMisSettings[0].DeliveryArrived == String.Empty || lstSiteMisSettings[0].DeliveryArrived.Trim().ToUpper() == "Y")
                    cblSupervisorConfirmationType.Items[0].Selected = true;

                if (lstSiteMisSettings[0].DeliveryUnload == String.Empty || lstSiteMisSettings[0].DeliveryUnload.Trim().ToUpper() == "Y")
                    cblSupervisorConfirmationType.Items[1].Selected = true;

                if (lstSiteMisSettings[0].UnexpectedDelivery == String.Empty || lstSiteMisSettings[0].UnexpectedDelivery.Trim().ToUpper() == "Y")
                    cblSupervisorConfirmationType.Items[2].Selected = true;

                #endregion

                if (lstSiteMisSettings[0].TimeSlotWindow == String.Empty || lstSiteMisSettings[0].TimeSlotWindow.Trim().ToUpper() == "S")
                    rblTimeSlotWindow.SelectedIndex = 0;
                else
                    rblTimeSlotWindow.SelectedIndex = 1;

                //----Stage 4 Point 13----//
                txtMaxCartons.Text = Convert.ToString(lstSiteMisSettings[0].MaxCartonsPerDelivery);
                txtMaxLines.Text = Convert.ToString(lstSiteMisSettings[0].MaxLinesPerDelivery);
                txtMaxPallets.Text = Convert.ToString(lstSiteMisSettings[0].MaxPalletsPerDelivery);
                //------------------------//

                //---Stage 5 Point 1-----//
                rblBookingExclusions.SelectedIndex = lstSiteMisSettings[0].IsBookingExclusions == true ? 0 : 1;
                //------------------------//

                //---Stage 7 Point 29-----//
                txtEndTime.Text = lstSiteMisSettings[0].NonTimeEnd;
                txtStartTime.Text = lstSiteMisSettings[0].NonTimeStart;
                //------------------------//
                uclblpalletPercentage.Text = "Window Volume Prcentage Setting";
                ucpalletPercentage.Text = lstSiteMisSettings[0].WindowVolumePrcentageSetting.ToString();
            }

            ddlSite.innerControlddlSite.Enabled = false;
        }
        else
        {

        }
    }

    #endregion

    private void SetResourceFilesText()
    {
        // By Date and By PO being add here in radiobutton list.
        listItem = new ListItem(strByDate, strByDate);
        listItem.Selected = true;
        rblByDatePO.Items.Add(listItem);
        listItem = new ListItem(strByPurchaseOrder, strByPurchaseOrder);
        rblByDatePO.Items.Add(listItem);

        // Per Booking and Per Vendor being add here in radiobutton list.
        listItem = new ListItem(strPerBooking, strPerBooking);
        listItem.Selected = true;
        rblPerBookingVendor.Items.Add(listItem);
        listItem = new ListItem(strPerVendor, strPerVendor);
        rblPerBookingVendor.Items.Add(listItem);

        // Time Slot and Time Window being add here in radiobutton list.
        listItem = new ListItem(strTimeSlot, strTimeSlot);
        listItem.Selected = true;
        rblTimeSlotWindow.Items.Add(listItem);
        listItem = new ListItem(strTimeWindow, strTimeWindow);
        rblTimeSlotWindow.Items.Add(listItem);

        // Alternate Slot Turned On Off - being add here in radiobutton list.
        listItem = new ListItem(strAlternateSlotTurnedOn, strAlternateSlotTurnedOn);
        listItem.Selected = true;
        rblAlternateSlotTurnedOnOff.Items.Add(listItem);
        listItem = new ListItem(strAlternateSlotTurnedOff, strAlternateSlotTurnedOff);
        rblAlternateSlotTurnedOnOff.Items.Add(listItem);

        // Supervisor Confirmation [Delivery Arrived, Delivery Unload, Unexpected Delivery] - being add here in check box list.
        listItem = new ListItem(strDelArrived, strDelArrived);
        cblSupervisorConfirmationType.Items.Add(listItem);
        listItem = new ListItem(strDelivaryunload, strDelivaryunload);
        cblSupervisorConfirmationType.Items.Add(listItem);
        listItem = new ListItem(strUnexpectedDelivery, strUnexpectedDelivery);
        cblSupervisorConfirmationType.Items.Add(listItem);

        //Booking Exclusion setting
        listItem = new ListItem(strBookingExcllusionYes, "1");
        rblBookingExclusions.Items.Add(listItem);
        listItem = new ListItem(strBookingExcllusionNo, "0");
        listItem.Selected = true;
        rblBookingExclusions.Items.Add(listItem);
    }

    private void BindVehicleType()
    {
        oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

        if (GetQueryStringValue("SiteID") != null)
        {
            oMASSIT_VehicleTypeBE.Action = "ShowAll";
            oMASSIT_VehicleTypeBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID")); //Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            List<MASSIT_VehicleTypeBE> lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType.Count > 0)
            {
                FillControls.FillDropDown(ref ddlDefaultVehicleType, lstVehicleType, "VehicleType", "VehicleTypeID", "--Select--");
            }
            else
            {
                ddlDefaultVehicleType.Items.Clear();
                ddlDefaultVehicleType.Items.Add(new ListItem("---Select---", "0"));
            }
        }
        else
        {
            ddlDefaultVehicleType.Items.Clear();
            ddlDefaultVehicleType.Items.Add(new ListItem("---Select---", "0"));
        }

        //VehicleNarrativeRequired = string.Empty;

        //for (int iCount = 0; iCount < lstVehicleType.Count; iCount++)
        //{
        //    if (lstVehicleType[iCount].IsNarrativeRequired)
        //    {
        //        VehicleNarrativeRequired += lstVehicleType[iCount].VehicleTypeID.ToString() + ",";
        //    }
        //}
    }

    private void BindDelivaryType()
    {
        oMASCNT_DeliveryTypeBE = new MASCNT_DeliveryTypeBE();
        oMASCNT_DeliveryTypeBAL = new APPCNT_DeliveryTypeBAL();

        if (GetQueryStringValue("SiteCountryID") != null)
        {
            oMASCNT_DeliveryTypeBE.Action = "ShowAll";
            oMASCNT_DeliveryTypeBE.CountryID = Convert.ToInt32(GetQueryStringValue("SiteCountryID")); //ddlSite.CountryID;
            oMASCNT_DeliveryTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASCNT_DeliveryTypeBE.User.UserID = 0;
            List<MASCNT_DeliveryTypeBE> lstDeliveryType = oMASCNT_DeliveryTypeBAL.GetDeliveryTypeDetailsBAL(oMASCNT_DeliveryTypeBE);

            //if (hdnCurrentRole.Value == "Vendor")
            //{
            //    lstDeliveryType = lstDeliveryType.FindAll(delegate(MASCNT_DeliveryTypeBE p) { return p.IsEnabledAsVendorOption == true; });
            //}

            if (lstDeliveryType.Count > 0)
            {
                FillControls.FillDropDown(ref ddlDefaultDeliveryType, lstDeliveryType, "DeliveryType", "DeliveryTypeID", "--Select--");
            }
            else
            {
                ddlDefaultDeliveryType.Items.Clear();
                ddlDefaultDeliveryType.Items.Add(new ListItem("---Select---", "0"));
            }
        }
        else
        {
            ddlDefaultDeliveryType.Items.Clear();
            ddlDefaultDeliveryType.Items.Add(new ListItem("---Select---", "0"));
        }
    }

}
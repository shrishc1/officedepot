﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using Utilities;
using WebUtilities;


public partial class APPSIT_WeekSetupEdit : CommonPage {

    #region Events
    protected string ConfirmMessage = WebCommon.getGlobalResourceValue("ConfirmMessage");

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            btnClear.Style["display"] = "none";
            ClearControls();
            BindTimes();
            if (GetQueryStringValue("weekday") != null && GetQueryStringValue("weekday").ToString() != "") {
                BindStartWeekDays(GetQueryStringValue("weekday").ToString());
            }
            GetWeekSetupForSelectedDay();
        }
        if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro") {
            btnSave.Visible = false;
            btnClear.Visible = false;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e) {
        MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE = new MASSIT_SpecificWeekSetupBE();
        APPSIT_SpecificWeekSetupBAL oMASSIT_SpecificWeekSetupBAL = new APPSIT_SpecificWeekSetupBAL();

        //DateTime dtStart = new DateTime(1900, 1, 1, Convert.ToInt32(txtStartTime.Text.Split(':')[0]), Convert.ToInt32(txtStartTime.Text.Split(':')[1]), 0);
        //DateTime dtEnd = new DateTime(1900, 1, 1, Convert.ToInt32(txtEndTime.Text.Split(':')[0]), Convert.ToInt32(txtEndTime.Text.Split(':')[1]), 0);

        DateTime dtStart = new DateTime(1900, 1, 1, Convert.ToInt32(ddlStartTime.SelectedItem.Text.Split(':')[0]), Convert.ToInt32(ddlStartTime.SelectedItem.Text.Split(':')[1]), 0);
        DateTime dtEnd = new DateTime(1900, 1, 1, Convert.ToInt32(ddlEndTime.SelectedItem.Text.Split(':')[0]), Convert.ToInt32(ddlEndTime.SelectedItem.Text.Split(':')[1]), 0);

        #region Time Restrictions

        if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("WeekSpecificID") != null) {
            /*-----------------------------------------------------------------------------*/
            string Days = GetWeekDaysToBeDisabled(ddlStartWeek.SelectedItem.Value, ddlEndWeek.SelectedItem.Value);

            oMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
            oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID = Convert.ToInt32(GetQueryStringValue("WeekSpecificID"));
            oMASSIT_SpecificWeekSetupBE.DayDisabledFor = ddlStartWeek.SelectedItem.Value + "," + ddlEndWeek.SelectedItem.Value;
            oMASSIT_SpecificWeekSetupBE.Action = "CheckWeekSetups";
            List<MASSIT_SpecificWeekSetupBE> lstSpecificWeekSetup = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);
            if (lstSpecificWeekSetup != null && lstSpecificWeekSetup.Count > 0) {
                string errorMeesage = WebCommon.getGlobalResourceValue("ClearWeekSetupsSpecific");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
                return;
            }
            else {
                oMASSIT_SpecificWeekSetupBE.Action = "CheckGenericWeekSetups";
                oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID = 0;
                lstSpecificWeekSetup = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);
                if (lstSpecificWeekSetup != null && lstSpecificWeekSetup.Count > 0) {
                    string errorMeesage = WebCommon.getGlobalResourceValue("ClearWeekSetupsGeneric");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
                    return;
                }
            }
            /*-----------------------------------------------------------------------------*/
        }

        #endregion

        #region Time Restrictions1
        /*-----------------------------------------------------------------------------*/
        if (ddlEndWeek.SelectedItem.Value == "mon" && ddlStartWeek.SelectedItem.Value == "sun") {
            //check if back week exists
            if (hidIsBackWeekExists.Value == "true") {
                //check where end day = sunday either IsDayDisabled = true or 
                //time is conflicting with the start time entered. show error message  

                string[] date = StartDate.Text.Trim().Split('/');
                oMASSIT_SpecificWeekSetupBE.Action = "GetWeekSpecificID";
                oMASSIT_SpecificWeekSetupBE.WeekStartDate = Convert.ToDateTime(date[1] + "/" + date[0] + "/" + date[2]).AddDays(-7);
                List<MASSIT_SpecificWeekSetupBE> lst1 = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);
                if (lst1 != null && lst1.Count > 0) {
                    oMASSIT_SpecificWeekSetupBE.Action = "ShowAll";
                    oMASSIT_SpecificWeekSetupBE.SiteID = GetQueryStringValue("SiteID") != null ? Convert.ToInt32(GetQueryStringValue("SiteID")) : Convert.ToInt32(hidSiteID.Value);
                    oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID = lst1[0].WeekSetupSpecificID;
                    oMASSIT_SpecificWeekSetupBE.EndWeekday = ddlStartWeek.SelectedItem.Value;
                    List<MASSIT_SpecificWeekSetupBE> lst2 = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);
                    if (lst2 != null && lst2.Count > 0) {
                        if (dtStart < lst2[0].EndTime && lst2[0].EndTime != null) {
                            if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lst2[0].SiteScheduleDiaryID) {
                                string errorMeesage = WebCommon.getGlobalResourceValue("EndTimeRestrictionWithLastWeekSpecific");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert3", "alert('" + errorMeesage + "')", true);
                                return;
                            }
                            else if (GetQueryStringValue("SiteScheduleDiaryID") == null) {
                                string errorMeesage = WebCommon.getGlobalResourceValue("EndTimeRestrictionWithLastWeekSpecific");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert4", "alert('" + errorMeesage + "')", true);
                                return;
                            }
                        }
                    }
                }
            }
            else {
                //check in the generic week setup 

                //check where end day = sunday either IsDayDisabled = true or 
                //time is conflicting with the start time entered. show error message  

                oMASSIT_SpecificWeekSetupBE.Action = "CheckGenericWeekSetups";
                oMASSIT_SpecificWeekSetupBE.SiteID = GetQueryStringValue("SiteID") != null ? Convert.ToInt32(GetQueryStringValue("SiteID")) : Convert.ToInt32(hidSiteID.Value);
                oMASSIT_SpecificWeekSetupBE.EndWeekday = ddlStartWeek.SelectedItem.Value;
                oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID = 0;
                List<MASSIT_SpecificWeekSetupBE> lst3 = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);
                if (lst3 != null && lst3.Count > 0) {
                    if (dtStart < lst3[0].EndTime && lst3[0].EndTime != null) {
                        if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lst3[0].SiteScheduleDiaryID) {
                            string errorMeesage = WebCommon.getGlobalResourceValue("EndTimeRestrictionWithLastWeekGeneric");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert5", "alert('" + errorMeesage + "')", true);
                            return;
                        }
                        else if (GetQueryStringValue("SiteScheduleDiaryID") == null) {
                            string errorMeesage = WebCommon.getGlobalResourceValue("EndTimeRestrictionWithLastWeekGeneric");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert6", "alert('" + errorMeesage + "')", true);
                            return;
                        }
                    }
                }
            }
        }
        /*-----------------------------------------------------------------------------*/
        #endregion

        #region Time Restrictions2

        if (ddlEndWeek.SelectedItem.Value == "sun") {
            //check if next week exists
            if (hidIsNextWeekExists.Value == "true") {
                //check where end day = sunday either IsDayDisabled = true or 
                //time is conflicting with the start time entered. show error message  

                string[] date = StartDate.Text.Trim().Split('/');
                oMASSIT_SpecificWeekSetupBE.Action = "GetWeekSpecificID";
                oMASSIT_SpecificWeekSetupBE.WeekStartDate = Convert.ToDateTime(date[1] + "/" + date[0] + "/" + date[2]).AddDays(7);
                List<MASSIT_SpecificWeekSetupBE> lst4 = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);
                if (lst4 != null && lst4.Count > 0) {

                    oMASSIT_SpecificWeekSetupBE.Action = "CheckStartTimesForEndDayTime";
                    oMASSIT_SpecificWeekSetupBE.StartWeekday = "sun";
                    oMASSIT_SpecificWeekSetupBE.SiteID = GetQueryStringValue("SiteID") != null ? Convert.ToInt32(GetQueryStringValue("SiteID")) : Convert.ToInt32(hidSiteID.Value);
                    oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID = lst4[0].WeekSetupSpecificID;
                    List<MASSIT_SpecificWeekSetupBE> lst5 = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);
                    if (lst5 != null && lst5.Count > 0) {
                        if (lst5.Count == 1 && dtEnd > lst5[0].StartTime && lst5[0].StartTime != null) {
                            if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lst5[0].SiteScheduleDiaryID) {
                                string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionSpecific");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert7", "alert('" + errorMeesage + "')", true);
                                return;
                            }
                            else if (GetQueryStringValue("SiteScheduleDiaryID") == null) {
                                string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionSpecific");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert8", "alert('" + errorMeesage + "')", true);
                                return;
                            }
                        }
                        else {
                            for (int i = 0; i < lst5.Count; i++) {
                                if (lst5[i].StartTime != null) {
                                    if (dtEnd > lst5[i].StartTime) {
                                        if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lst5[i].SiteScheduleDiaryID) {
                                            string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionSpecific");
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert9", "alert('" + errorMeesage + "')", true);
                                            return;
                                        }
                                        else if (GetQueryStringValue("SiteScheduleDiaryID") == null) {
                                            string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionSpecific");
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert10", "alert('" + errorMeesage + "')", true);
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                //check in the generic week setup 

                //check where end day = sunday either IsDayDisabled = true or 
                //time is conflicting with the start time entered. show error message  

                oMASSIT_SpecificWeekSetupBE.Action = "CheckStartTimesForEndDayTimeForGeneric";
                oMASSIT_SpecificWeekSetupBE.StartWeekday = "sun";
                oMASSIT_SpecificWeekSetupBE.SiteID = GetQueryStringValue("SiteID") != null ? Convert.ToInt32(GetQueryStringValue("SiteID")) : Convert.ToInt32(hidSiteID.Value);
                List<MASSIT_SpecificWeekSetupBE> lst6 = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);
                if (lst6 != null && lst6.Count > 0) {
                    if (lst6.Count == 1 && dtEnd > lst6[0].StartTime && lst6[0].StartTime != null) {
                        if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lst6[0].SiteScheduleDiaryID) {
                            string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionGeneric");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert11", "alert('" + errorMeesage + "')", true);
                            return;
                        }
                        else if (GetQueryStringValue("SiteScheduleDiaryID") == null) {
                            string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionGeneric");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert12", "alert('" + errorMeesage + "')", true);
                            return;
                        }
                    }
                    else {
                        for (int i = 0; i < lst6.Count; i++) {
                            if (lst6[i].StartTime != null) {
                                if (dtEnd > lst6[i].StartTime) {
                                    if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lst6[i].SiteScheduleDiaryID) {
                                        string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionGeneric");
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert13", "alert('" + errorMeesage + "')", true);
                                        return;
                                    }
                                    else if (GetQueryStringValue("SiteScheduleDiaryID") == null) {
                                        string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionGeneric");
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert14", "alert('" + errorMeesage + "')", true);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Time Restrictions4

        //End time restriction
        if (ddlStartWeek.SelectedItem.Value != "mon") {
            //if (ddlStartWeek.SelectedItem.Value != "sun") {
            /*-----------------------------------------------------------------------------*/
            oMASSIT_SpecificWeekSetupBE.Action = "ShowAll";
            oMASSIT_SpecificWeekSetupBE.SiteID = GetQueryStringValue("SiteID") != null ? Convert.ToInt32(GetQueryStringValue("SiteID")) : Convert.ToInt32(hidSiteID.Value);
            oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID = GetQueryStringValue("WeekSpecificID") != null ? Convert.ToInt32(GetQueryStringValue("WeekSpecificID")) : Convert.ToInt32(hidWeekSpecificID.Value);
            oMASSIT_SpecificWeekSetupBE.EndWeekday = ddlStartWeek.SelectedItem.Value;
            List<MASSIT_SpecificWeekSetupBE> lstSpecificWeekSetupConflictsStart = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);

            if (lstSpecificWeekSetupConflictsStart != null && lstSpecificWeekSetupConflictsStart.Count > 0 && dtStart < lstSpecificWeekSetupConflictsStart[0].EndTime && lstSpecificWeekSetupConflictsStart[0].EndTime != null) {
                if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lstSpecificWeekSetupConflictsStart[0].SiteScheduleDiaryID) {
                    string errorMeesage = WebCommon.getGlobalResourceValue("EndTimeRestrictionSpecific");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert15", "alert('" + errorMeesage + "')", true);
                    return;
                }
                else if (GetQueryStringValue("SiteScheduleDiaryID") == null) {
                    string errorMeesage = WebCommon.getGlobalResourceValue("EndTimeRestrictionSpecific");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert16", "alert('" + errorMeesage + "')", true);
                    return;
                }

            }
        }
        #endregion

        #region Time Restrictions5

        //Start time restriction
        if (ddlEndWeek.SelectedItem.Value != "sun") {

            /*-----------------------------------------------------------------------------*/
            oMASSIT_SpecificWeekSetupBE.Action = "CheckStartTimesForEndDayTime";
            oMASSIT_SpecificWeekSetupBE.StartWeekday = ddlEndWeek.SelectedItem.Value;
            oMASSIT_SpecificWeekSetupBE.SiteID = GetQueryStringValue("SiteID") != null ? Convert.ToInt32(GetQueryStringValue("SiteID")) : Convert.ToInt32(hidSiteID.Value);
            oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID = Convert.ToInt32(GetQueryStringValue("WeekSpecificID"));

            List<MASSIT_SpecificWeekSetupBE> lstSpecificWeekSetupConflictsEnd = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);
            if (lstSpecificWeekSetupConflictsEnd != null && lstSpecificWeekSetupConflictsEnd.Count > 0) {
                if (lstSpecificWeekSetupConflictsEnd.Count == 1 && dtEnd > lstSpecificWeekSetupConflictsEnd[0].StartTime && lstSpecificWeekSetupConflictsEnd[0].StartTime != null) {
                    if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lstSpecificWeekSetupConflictsEnd[0].SiteScheduleDiaryID) {
                        string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionSpecific");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert19", "alert('" + errorMeesage + "')", true);
                        return;
                    }
                    else if (GetQueryStringValue("SiteScheduleDiaryID") == null) {
                        string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionSpecific");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert20", "alert('" + errorMeesage + "')", true);
                        return;
                    }
                }
                else {
                    for (int i = 0; i < lstSpecificWeekSetupConflictsEnd.Count; i++) {
                        if (lstSpecificWeekSetupConflictsEnd[i].StartTime != null) {
                            if (dtEnd > lstSpecificWeekSetupConflictsEnd[i].StartTime && lstSpecificWeekSetupConflictsEnd[i].StartTime != null) {
                                if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lstSpecificWeekSetupConflictsEnd[i].SiteScheduleDiaryID) {
                                    string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionSpecific");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert21", "alert('" + errorMeesage + "')", true);
                                    return;
                                }
                                else if (GetQueryStringValue("SiteScheduleDiaryID") == null) {
                                    string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionSpecific");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert22", "alert('" + errorMeesage + "')", true);
                                    return;
                                }
                            }
                        }
                    }
                }
                /*-----------------------------------------------------------------------------*/
            }
            //else {
            /*-----------------------------------------------------------------------------*/
            //End time restriction specific     
            oMASSIT_SpecificWeekSetupBE.Action = "ShowAll";
            oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID = Convert.ToInt32(GetQueryStringValue("WeekSpecificID"));
            oMASSIT_SpecificWeekSetupBE.EndWeekday = ddlStartWeek.SelectedItem.Value;
            List<MASSIT_SpecificWeekSetupBE> lstWeekSetupSpecificConflictsStart = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);

            if (lstWeekSetupSpecificConflictsStart != null && lstWeekSetupSpecificConflictsStart.Count > 0 && ddlStartWeek.SelectedItem.Value != "sun") {
                if (dtStart < lstWeekSetupSpecificConflictsStart[0].EndTime && lstWeekSetupSpecificConflictsStart[0].EndTime != null) {
                    if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lstWeekSetupSpecificConflictsStart[0].SiteScheduleDiaryID) {
                        string errorMeesage = WebCommon.getGlobalResourceValue("EndTimeRestrictionSpecific");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert23", "alert('" + errorMeesage + "')", true);
                        return;
                    }
                    else if (GetQueryStringValue("SiteScheduleDiaryID") == null) {
                        string errorMeesage = WebCommon.getGlobalResourceValue("EndTimeRestrictionSpecific");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert24", "alert('" + errorMeesage + "')", true);
                        return;
                    }
                }
            }
            /*-----------------------------------------------------------------------------*/
            if (ddlEndWeek.SelectedItem.Value == "mon" && ddlStartWeek.SelectedItem.Value == "sun") {
                oMASSIT_SpecificWeekSetupBE.Action = "CheckStartTimesForEndDayTimeForGeneric";
                oMASSIT_SpecificWeekSetupBE.StartWeekday = ddlEndWeek.SelectedItem.Value;
                oMASSIT_SpecificWeekSetupBE.SiteID = GetQueryStringValue("SiteID") != null ? Convert.ToInt32(GetQueryStringValue("SiteID")) : Convert.ToInt32(hidSiteID.Value);
                string[] date = StartDate.Text.Trim().Split('/');

                lstSpecificWeekSetupConflictsEnd = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);
                if (lstSpecificWeekSetupConflictsEnd != null && lstSpecificWeekSetupConflictsEnd.Count > 0) {
                    if (lstSpecificWeekSetupConflictsEnd.Count == 1 && dtEnd > lstSpecificWeekSetupConflictsEnd[0].StartTime && lstSpecificWeekSetupConflictsEnd[0].StartTime != null) {
                        if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lstSpecificWeekSetupConflictsEnd[0].SiteScheduleDiaryID) {
                            string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionGeneric");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert25", "alert('" + errorMeesage + "')", true);
                            return;
                        }
                        else if (GetQueryStringValue("SiteScheduleDiaryID") == null) {
                            string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionGeneric");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert25", "alert('" + errorMeesage + "')", true);
                            return;
                        }
                    }
                    else {
                        for (int i = 0; i < lstSpecificWeekSetupConflictsEnd.Count; i++) {
                            if (lstSpecificWeekSetupConflictsEnd[i].StartTime != null) {
                                if (dtEnd > lstSpecificWeekSetupConflictsEnd[i].StartTime && lstSpecificWeekSetupConflictsEnd[i].StartTime != null) {
                                    if (GetQueryStringValue("SiteScheduleDiaryID") != null && Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID")) != lstSpecificWeekSetupConflictsEnd[i].SiteScheduleDiaryID) {
                                        string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionGeneric");
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert26", "alert('" + errorMeesage + "')", true);
                                        return;
                                    }
                                    else if (GetQueryStringValue("SiteScheduleDiaryID") == null) {
                                        string errorMeesage = WebCommon.getGlobalResourceValue("StartTimeRestrictionGeneric");
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert25", "alert('" + errorMeesage + "')", true);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            /*-----------------------------------------------------------------------------*/
        }

        #endregion

        #region Save Data
        /*-----------------------------------------------------------------------------*/
        if (GetQueryStringValue("SiteID") != null) {
            oMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        }
        else if (GetQueryStringValue("SiteScheduleDiaryID") != null) {
            //if editing the existing weeksetup then first clear all the related data
            ClearRecordsBeforeEdit();

            oMASSIT_SpecificWeekSetupBE.SiteScheduleDiaryID = Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID"));
        }

        oMASSIT_SpecificWeekSetupBE.Action = "AddEdit";
        oMASSIT_SpecificWeekSetupBE.StartWeekday = ddlStartWeek.SelectedItem.Value;
        oMASSIT_SpecificWeekSetupBE.EndWeekday = ddlEndWeek.SelectedItem.Value;

        oMASSIT_SpecificWeekSetupBE.StartTime = dtStart;
        oMASSIT_SpecificWeekSetupBE.EndTime = dtEnd;

        oMASSIT_SpecificWeekSetupBE.MaximumLift = txtMaximumLift.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaximumLift.Text.Trim()) : (int?)null;
        oMASSIT_SpecificWeekSetupBE.MaximumPallet = txtMaximumPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaximumPallets.Text.Trim()) : (int?)null;

        oMASSIT_SpecificWeekSetupBE.MaximumContainer = txtMaxContainers.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxContainers.Text.Trim()) : (int?)null;
        oMASSIT_SpecificWeekSetupBE.MaximumLine = txtMaxLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxLines.Text.Trim()) : (int?)null;

        oMASSIT_SpecificWeekSetupBE.MaximumDeliveries = txtMaxDeliveries.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxDeliveries.Text.Trim()) : (int?)null;

        oMASSIT_SpecificWeekSetupBE.DayDisabledFor = GetWeekDaysToBeDisabled(ddlStartWeek.SelectedItem.Value, ddlEndWeek.SelectedItem.Value);


        oMASSIT_SpecificWeekSetupBE.StartSlotTimeID = Convert.ToInt32(ddlStartTime.SelectedItem.Value);
        oMASSIT_SpecificWeekSetupBE.EndSlotTimeID = Convert.ToInt32(ddlEndTime.SelectedItem.Value);

        oMASSIT_SpecificWeekSetupBAL.addEditSpecificWeekSetupBAL(oMASSIT_SpecificWeekSetupBE);
        string SiteID = GetQueryStringValue("SiteID") != null ? GetQueryStringValue("SiteID").ToString() : hidSiteID.Value;

        //Save Week Setup Log Entry
        MASSIT_WeekSetupLogsBE oMASSIT_WeekSetupLogsBE = new MASSIT_WeekSetupLogsBE();
        oMASSIT_WeekSetupLogsBE.Action = "Add";
        oMASSIT_WeekSetupLogsBE.SiteID = Convert.ToInt32(SiteID);
        oMASSIT_WeekSetupLogsBE.StartTime = oMASSIT_SpecificWeekSetupBE.StartTime;
        oMASSIT_WeekSetupLogsBE.EndTime = oMASSIT_SpecificWeekSetupBE.EndTime;
        oMASSIT_WeekSetupLogsBE.MaximumLift = oMASSIT_SpecificWeekSetupBE.MaximumLift;
        oMASSIT_WeekSetupLogsBE.MaximumPallet = oMASSIT_SpecificWeekSetupBE.MaximumPallet;
        oMASSIT_WeekSetupLogsBE.MaximumContainer = oMASSIT_SpecificWeekSetupBE.MaximumContainer;
        oMASSIT_WeekSetupLogsBE.MaximumLine = oMASSIT_SpecificWeekSetupBE.MaximumLine;
        oMASSIT_WeekSetupLogsBE.MaximumDeliveries = oMASSIT_SpecificWeekSetupBE.MaximumDeliveries;        
        oMASSIT_WeekSetupLogsBE.Type = WeekSetupStartDate.Text.Replace(")","").Replace("(","");
        
        oMASSIT_WeekSetupLogsBE.UserID = System.Web.HttpContext.Current.Session["UserId"].ToString();
        oMASSIT_SpecificWeekSetupBAL.SaveWeekSetupLogsBAL(oMASSIT_WeekSetupLogsBE);
        
        string WeekStartDate = GetQueryStringValue("WeekStartDate");
        EncryptQueryString("APPSIT_SpecificWeekSetupOverview.aspx?WeekStartDate=" + WeekStartDate + "&SiteID=" + SiteID);
        /*-----------------------------------------------------------------------------*/
        #endregion

    }

    protected void btnBack_Click(object sender, EventArgs e) {
        if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro")
            EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
        else {
            string SiteID = GetQueryStringValue("SiteID") != null ? GetQueryStringValue("SiteID").ToString() : hidSiteID.Value;
            string WeekStartDate = GetQueryStringValue("WeekStartDate");
            EncryptQueryString("APPSIT_SpecificWeekSetupOverview.aspx?WeekStartDate=" + WeekStartDate + "&SiteID=" + SiteID);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e) {
        if (GetQueryStringValue("SiteScheduleDiaryID") != null) {
            //ClearRecordsBeforeEdit();
            ClearRecordsWithDoorAndDeliveryData();
            string SiteID = GetQueryStringValue("SiteID") != null ? GetQueryStringValue("SiteID").ToString() : hidSiteID.Value;
            string WeekStartDate = GetQueryStringValue("WeekStartDate");
            EncryptQueryString("APPSIT_SpecificWeekSetupOverview.aspx?WeekStartDate=" + WeekStartDate + "&SiteID=" + SiteID);
        }
    }

    protected void ddlStartWeek_SelectedIndexChanged(object sender, EventArgs e) {
        string[] date = StartDate.Text.Trim().Split('/');
        DateTime dt = Convert.ToDateTime(date[1] + "/" + date[0] + "/" + date[2]);
        if (ddlEndWeek.SelectedItem.Value == "mon" && ddlStartWeek.SelectedItem.Value == "sun") {
            WeekSetupStartDate.Text = "(" + dt.AddDays(-1).ToString("dd") + "/" + dt.AddDays(-1).ToString("MM") + "/" + dt.AddDays(-1).ToString("yyyy") + ")";
        }
        else {
            WeekSetupStartDate.Text = GetWeekDate(dt, ddlStartWeek.SelectedItem.Value);
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// Clear controls
    /// </summary>
    public void ClearControls() {
        ddlStartWeek.SelectedIndex = 0;
        ddlEndWeek.SelectedIndex = 0;

        //txtStartTime.Text = string.Empty;
        //txtEndTime.Text = string.Empty;
        ddlStartTime.SelectedIndex = 0;
        ddlEndTime.SelectedIndex = 0;

        txtMaximumLift.Text = string.Empty;
        txtMaximumPallets.Text = string.Empty;
        txtMaxLines.Text = string.Empty;
        txtMaxContainers.Text = string.Empty;
        StartDate.Text = string.Empty;
        Site.Text = string.Empty;
    }

    private string formatTime(string time) {
        if (time.Length == 1) {
            return "0" + time;
        }
        return time;
    }
    /// <summary>
    /// Get Week Setup Fo rSelected Day
    /// </summary>
    public void GetWeekSetupForSelectedDay() {
        MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE = new MASSIT_SpecificWeekSetupBE();
        APPSIT_SpecificWeekSetupBAL oMASSIT_SpecificWeekSetupBAL = new APPSIT_SpecificWeekSetupBAL();
        if (GetQueryStringValue("SiteScheduleDiaryID") != null && GetQueryStringValue("SiteScheduleDiaryID").ToString() != "") {

            btnClear.Style["display"] = "";
            oMASSIT_SpecificWeekSetupBE.Action = "ShowAll";
            oMASSIT_SpecificWeekSetupBE.SiteScheduleDiaryID = Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID"));
            List<MASSIT_SpecificWeekSetupBE> lstSpecificWeekSetup = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);

            if (lstSpecificWeekSetup != null && lstSpecificWeekSetup.Count > 0) {

                Site.Text = lstSpecificWeekSetup[0].Site.SiteName;

                hidSiteID.Value = lstSpecificWeekSetup[0].SiteID.ToString();
                hidWeekSpecificID.Value = lstSpecificWeekSetup[0].WeekSetupSpecificID.ToString();
                hidIsBackWeekExists.Value = lstSpecificWeekSetup[0].IsBackWeekExists;
                hidIsNextWeekExists.Value = lstSpecificWeekSetup[0].IsNextWeekExists;

                StartDate.Text = lstSpecificWeekSetup[0].WeekStartDate.Value.ToString("dd") + "/" + lstSpecificWeekSetup[0].WeekStartDate.Value.ToString("MM") + "/" + lstSpecificWeekSetup[0].WeekStartDate.Value.ToString("yyyy");

                WeekSetupEndDate.Text = GetWeekDate(Convert.ToDateTime(lstSpecificWeekSetup[0].WeekStartDate), GetQueryStringValue("weekday").ToString());

                ddlStartWeek.SelectedValue = lstSpecificWeekSetup[0].StartWeekday;

                WeekSetupStartDate.Text = GetWeekDate(Convert.ToDateTime(lstSpecificWeekSetup[0].WeekStartDate), lstSpecificWeekSetup[0].StartWeekday);

                if (lstSpecificWeekSetup[0].StartTime != null && lstSpecificWeekSetup[0].StartTime.ToString() != "") {
                    //txtStartTime.Text = lstSpecificWeekSetup[0].StartTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetup[0].StartTime.Value.Minute.ToString();
                    if (ddlStartTime.Items.FindByText(formatTime(lstSpecificWeekSetup[0].StartTime.Value.Hour.ToString()) + ":" + formatTime(lstSpecificWeekSetup[0].StartTime.Value.ToString("mm"))) != null)
                        ddlStartTime.SelectedValue = ddlStartTime.Items.FindByText(formatTime(lstSpecificWeekSetup[0].StartTime.Value.Hour.ToString()) + ":" + formatTime(lstSpecificWeekSetup[0].StartTime.Value.ToString("mm"))).Value;
                    else
                        ddlStartTime.SelectedIndex = 0;
                }
                else {
                    //txtStartTime.Text = "";
                    ddlStartTime.SelectedIndex = 0;
                }

                ddlEndWeek.Enabled = false;
                ddlEndWeek.SelectedValue = lstSpecificWeekSetup[0].EndWeekday;
                if (lstSpecificWeekSetup[0].EndTime != null && lstSpecificWeekSetup[0].EndTime.ToString() != "") {
                    //txtEndTime.Text = lstSpecificWeekSetup[0].EndTime.Value.Hour.ToString() + ":" + lstSpecificWeekSetup[0].EndTime.Value.Minute.ToString();
                    if (ddlEndTime.Items.FindByText(formatTime(lstSpecificWeekSetup[0].EndTime.Value.Hour.ToString()) + ":" + formatTime(lstSpecificWeekSetup[0].EndTime.Value.ToString("mm"))) != null)
                        ddlEndTime.SelectedValue = ddlEndTime.Items.FindByText(formatTime(lstSpecificWeekSetup[0].EndTime.Value.Hour.ToString()) + ":" + formatTime(lstSpecificWeekSetup[0].EndTime.Value.ToString("mm"))).Value;
                    else
                        ddlEndTime.SelectedIndex = 0;
                }
                else {
                    //txtEndTime.Text = "";
                    ddlEndTime.SelectedIndex = 0;
                }

                txtMaxLines.Text = Convert.ToString(lstSpecificWeekSetup[0].MaximumLine);
                txtMaximumPallets.Text = Convert.ToString(lstSpecificWeekSetup[0].MaximumPallet);
                txtMaximumLift.Text = Convert.ToString(lstSpecificWeekSetup[0].MaximumLift);
                txtMaxContainers.Text = Convert.ToString(lstSpecificWeekSetup[0].MaximumContainer);
                txtMaxDeliveries.Text = Convert.ToString(lstSpecificWeekSetup[0].MaximumDeliveries);
            }

            else if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("SiteID").ToString() != "") {

                btnClear.Style["display"] = "none";
                hidSiteID.Value = GetQueryStringValue("SiteID").ToString();

                MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
                MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

                oMAS_SiteBE.Action = "ShowAll";
                oMAS_SiteBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
                oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);

                List<MAS_SiteBE> lstSite = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
                if (lstSite != null && lstSite.Count > 0) {
                    Site.Text = lstSite[0].SitePrefix.ToString() + "-" + lstSite[0].SiteName.ToString();
                }
                if (GetQueryStringValue("weekday") != null && GetQueryStringValue("weekday").ToString() != "") {
                    ddlEndWeek.SelectedValue = GetQueryStringValue("weekday").ToString();
                    ddlEndWeek.Enabled = false;
                }
                else {
                    ddlEndWeek.SelectedValue = "mon";
                }
                ddlStartWeek.SelectedValue = "mon";
                if (ddlEndWeek.SelectedValue == "mon")
                    ddlStartWeek.SelectedValue = "sun";

                oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID = Convert.ToInt32(GetQueryStringValue("WeekSpecificID").ToString());
                oMASSIT_SpecificWeekSetupBE.Action = "GetWeekSpecificStartDate";
                List<MASSIT_SpecificWeekSetupBE> lstSpecificWeekSetupDate = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);
                if (lstSpecificWeekSetupDate != null && lstSpecificWeekSetupDate.Count > 0) {
                    hidWeekSpecificID.Value = lstSpecificWeekSetupDate[0].WeekSetupSpecificID.ToString();
                    StartDate.Text = lstSpecificWeekSetupDate[0].WeekStartDate.Value.ToString("dd") + "/" + lstSpecificWeekSetupDate[0].WeekStartDate.Value.ToString("MM") + "/" + lstSpecificWeekSetupDate[0].WeekStartDate.Value.ToString("yyyy");
                    WeekSetupEndDate.Text = GetWeekDate(Convert.ToDateTime(lstSpecificWeekSetupDate[0].WeekStartDate), GetQueryStringValue("weekday").ToString());

                    WeekSetupStartDate.Text = GetWeekDate(Convert.ToDateTime(lstSpecificWeekSetupDate[0].WeekStartDate).AddDays(-1), GetQueryStringValue("weekday").ToString());

                    hidIsBackWeekExists.Value = lstSpecificWeekSetupDate[0].IsBackWeekExists;
                    hidIsNextWeekExists.Value = lstSpecificWeekSetupDate[0].IsNextWeekExists;
                }
                ddlStartTime.SelectedIndex = 0;
                ddlEndTime.SelectedIndex = 0;
            }
        }
        else {
            btnClear.Style["display"] = "none";
            hidSiteID.Value = GetQueryStringValue("SiteID").ToString();

            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

            oMAS_SiteBE.Action = "ShowAll";
            oMAS_SiteBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);

            List<MAS_SiteBE> lstSite = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
            if (lstSite != null && lstSite.Count > 0) {
                Site.Text = lstSite[0].SitePrefix.ToString() + "-" + lstSite[0].SiteName.ToString();
            }
            if (GetQueryStringValue("weekday") != null && GetQueryStringValue("weekday").ToString() != "") {
                ddlEndWeek.SelectedValue = GetQueryStringValue("weekday").ToString();
                ddlEndWeek.Enabled = false;
            }
            else {
                ddlEndWeek.SelectedValue = "mon";
            }
            ddlStartWeek.SelectedValue = "mon";
            if (ddlEndWeek.SelectedValue == "mon")
                ddlStartWeek.SelectedValue = "sun";

            oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID = Convert.ToInt32(GetQueryStringValue("WeekSpecificID").ToString());
            oMASSIT_SpecificWeekSetupBE.Action = "GetWeekSpecificStartDate";
            List<MASSIT_SpecificWeekSetupBE> lstSpecificWeekSetupDate = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);
            if (lstSpecificWeekSetupDate != null && lstSpecificWeekSetupDate.Count > 0) {
                hidWeekSpecificID.Value = lstSpecificWeekSetupDate[0].WeekSetupSpecificID.ToString();
                StartDate.Text = lstSpecificWeekSetupDate[0].WeekStartDate.Value.ToString("dd") + "/" + lstSpecificWeekSetupDate[0].WeekStartDate.Value.ToString("MM") + "/" + lstSpecificWeekSetupDate[0].WeekStartDate.Value.ToString("yyyy");
                WeekSetupEndDate.Text = GetWeekDate(Convert.ToDateTime(lstSpecificWeekSetupDate[0].WeekStartDate), GetQueryStringValue("weekday").ToString());

                WeekSetupStartDate.Text = GetWeekDate(Convert.ToDateTime(lstSpecificWeekSetupDate[0].WeekStartDate).AddDays(-1), GetQueryStringValue("weekday").ToString());

                hidIsBackWeekExists.Value = lstSpecificWeekSetupDate[0].IsBackWeekExists;
                hidIsNextWeekExists.Value = lstSpecificWeekSetupDate[0].IsNextWeekExists;
            }
            ddlStartTime.SelectedIndex = 0;
            ddlEndTime.SelectedIndex = 0;
        }
    }

    /// <summary>
    /// Get Week Days To Be Disabled
    /// </summary>
    /// <param name="startWeekDay"></param>
    /// <param name="endWeekDay"></param>
    /// <returns></returns>
    public string GetWeekDaysToBeDisabled(string startWeekDay, string endWeekDay) {
        //get the days of the week to be disabled
        ArrayList arrWeekDays = new ArrayList();
        arrWeekDays.Add("mon");
        arrWeekDays.Add("tue");
        arrWeekDays.Add("wed");
        arrWeekDays.Add("thu");
        arrWeekDays.Add("fri");
        arrWeekDays.Add("sat");
        arrWeekDays.Add("sun");

        ArrayList days = new ArrayList();
        int index = arrWeekDays.IndexOf(startWeekDay);

        if (index == 7) { index = 0; } else { index = index + 1; }
        int i;
        if (startWeekDay != endWeekDay) {
        loop: for (i = index; i < arrWeekDays.Count; i++) {
                if (endWeekDay != arrWeekDays[i].ToString()) {
                    days.Add(arrWeekDays[i].ToString());
                }
                else if (endWeekDay == arrWeekDays[i].ToString()) {
                    break;
                }
                else if (startWeekDay == arrWeekDays[i + 1].ToString()) {
                    break;
                }
            }
            if (i == 7) { index = 0; goto loop; }

        }
        //if (txtStartTime.Text.Trim() == "00:00") {
        if (ddlStartTime.SelectedItem.Text.Trim() == "00:00") {
            if (ddlStartWeek.SelectedItem.Value == ddlEndWeek.SelectedItem.Value) {
            }
            else
                days.Add(startWeekDay);
        }

        //if (txtEndTime.Text.Trim() == "00:00") {
        if (ddlEndTime.SelectedItem.Text.Trim() == "00:00") {
            //days.Add(endWeekDay);
        }
        string DaysToDisabled = string.Empty;
        if (days.Count == 1) { DaysToDisabled += days[0].ToString(); }
        if (days.Count > 1) {
            DaysToDisabled += days[0].ToString();
            for (int j = 1; j < days.Count - 1; j++) {
                if (j < days.Count)
                    DaysToDisabled += "," + days[j].ToString();
            }
            DaysToDisabled += "," + days[days.Count - 1].ToString();
        }
        return DaysToDisabled;
    }

    /// <summary>
    /// Clear records before edit
    /// </summary>
    public void ClearRecordsBeforeEdit() {
        MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE = new MASSIT_SpecificWeekSetupBE();
        APPSIT_SpecificWeekSetupBAL oMASSIT_SpecificWeekSetupBAL = new APPSIT_SpecificWeekSetupBAL();

        oMASSIT_SpecificWeekSetupBE.Action = "ShowAll";

        oMASSIT_SpecificWeekSetupBE.SiteScheduleDiaryID = Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID"));
        oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID = Convert.ToInt32(hidWeekSpecificID.Value);

        oMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(hidSiteID.Value);

        List<MASSIT_SpecificWeekSetupBE> lstSpecificWeekSetup = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);

        if (lstSpecificWeekSetup != null && lstSpecificWeekSetup.Count > 0) {
            oMASSIT_SpecificWeekSetupBE.DayDisabledFor = GetWeekDaysToBeDisabled(lstSpecificWeekSetup[0].StartWeekday, lstSpecificWeekSetup[0].EndWeekday) + "," + ddlEndWeek.SelectedItem.Value;
        }
        else {
            oMASSIT_SpecificWeekSetupBE.DayDisabledFor = GetWeekDaysToBeDisabled(ddlStartWeek.SelectedItem.Value, ddlEndWeek.SelectedItem.Value) + "," + ddlEndWeek.SelectedItem.Value;
        }
        oMASSIT_SpecificWeekSetupBE.Action = "ClearRecords";
        oMASSIT_SpecificWeekSetupBE.EndWeekday = ddlEndWeek.SelectedItem.Value;
        oMASSIT_SpecificWeekSetupBE.StartWeekday = ddlStartWeek.SelectedItem.Value;
        oMASSIT_SpecificWeekSetupBE.StartSlotTimeID = Convert.ToInt32(ddlStartTime.SelectedItem.Value);
        oMASSIT_SpecificWeekSetupBE.EndSlotTimeID = Convert.ToInt32(ddlEndTime.SelectedItem.Value);

        oMASSIT_SpecificWeekSetupBAL.addEditSpecificWeekSetupBAL(oMASSIT_SpecificWeekSetupBE);
    }

    public void ClearRecordsWithDoorAndDeliveryData() {
        MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE = new MASSIT_SpecificWeekSetupBE();
        APPSIT_SpecificWeekSetupBAL oMASSIT_SpecificWeekSetupBAL = new APPSIT_SpecificWeekSetupBAL();

        oMASSIT_SpecificWeekSetupBE.Action = "ShowAll";

        oMASSIT_SpecificWeekSetupBE.SiteScheduleDiaryID = Convert.ToInt32(GetQueryStringValue("SiteScheduleDiaryID"));
        oMASSIT_SpecificWeekSetupBE.WeekSetupSpecificID = Convert.ToInt32(hidWeekSpecificID.Value);

        oMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(hidSiteID.Value);

        List<MASSIT_SpecificWeekSetupBE> lstSpecificWeekSetup = oMASSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);

        if (lstSpecificWeekSetup != null && lstSpecificWeekSetup.Count > 0) {
            oMASSIT_SpecificWeekSetupBE.DayDisabledFor = GetWeekDaysToBeDisabled(lstSpecificWeekSetup[0].StartWeekday, lstSpecificWeekSetup[0].EndWeekday) + "," + ddlEndWeek.SelectedItem.Value;
        }
        else {
            oMASSIT_SpecificWeekSetupBE.DayDisabledFor = GetWeekDaysToBeDisabled(ddlStartWeek.SelectedItem.Value, ddlEndWeek.SelectedItem.Value) + "," + ddlEndWeek.SelectedItem.Value;
        }
        oMASSIT_SpecificWeekSetupBE.Action = "ClearRecordsWithDoorAndDeliveryData";
        oMASSIT_SpecificWeekSetupBE.EndWeekday = ddlEndWeek.SelectedItem.Value;
        oMASSIT_SpecificWeekSetupBE.StartWeekday = ddlStartWeek.SelectedItem.Value;
        oMASSIT_SpecificWeekSetupBE.StartSlotTimeID = Convert.ToInt32(ddlStartTime.SelectedItem.Value);
        oMASSIT_SpecificWeekSetupBE.EndSlotTimeID = Convert.ToInt32(ddlEndTime.SelectedItem.Value);

        oMASSIT_SpecificWeekSetupBAL.addEditSpecificWeekSetupBAL(oMASSIT_SpecificWeekSetupBE);
    }

    /// <summary>
    /// on the basis of selected day bind the start week days
    /// </summary>
    /// <param name="daySelected"></param>
    public void BindStartWeekDays(string daySelected) {
        ArrayList weekArray = new ArrayList();
        ArrayList startWeekDays = new ArrayList();
        string[] sWeekArray = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

        weekArray.Add("mon"); weekArray.Add("tue");
        weekArray.Add("wed"); weekArray.Add("thu");
        weekArray.Add("fri"); weekArray.Add("sat");
        weekArray.Add("sun");
        int index = weekArray.IndexOf(daySelected);
        if (daySelected != "mon") {
            startWeekDays.Add(weekArray[index - 1]);
            startWeekDays.Add(weekArray[index]);
        }
        else if (daySelected == "mon") {
            startWeekDays.Add("sun");
            startWeekDays.Add("mon");
        }
        if (startWeekDays.Count > 0) {
            ddlStartWeek.Items.Clear();
            ddlStartWeek.Items.Add(new ListItem(sWeekArray[weekArray.IndexOf(startWeekDays[0].ToString())], startWeekDays[0].ToString()));
            ddlStartWeek.Items.Add(new ListItem(sWeekArray[weekArray.IndexOf(startWeekDays[1].ToString())], startWeekDays[1].ToString()));
        }

    }

    /// <summary>
    /// Get date according to the start week date and selected weekday in the dropdown
    /// </summary>
    /// <param name="dtStartDate"></param>
    /// <param name="weekDay"></param>
    /// <returns></returns>
    public string GetWeekDate(DateTime dtStartDate, string weekDay) {
        ArrayList weekArray = new ArrayList();
        weekArray.Add("mon"); weekArray.Add("tue");
        weekArray.Add("wed"); weekArray.Add("thu");
        weekArray.Add("fri"); weekArray.Add("sat");
        weekArray.Add("sun");
        int index = weekArray.IndexOf(weekDay);


        if (weekDay == "sun" && ddlEndWeek.SelectedItem.Value == "mon" && ddlStartWeek.SelectedItem.Value == "sun") {
            return "(" + dtStartDate.AddDays(-1).ToString("dd") + "/" + dtStartDate.AddDays(-1).ToString("MM") + "/" + dtStartDate.AddDays(-1).ToString("yyyy") + ")";
        }
        else {
            return ("(" + dtStartDate.AddDays(index).ToString("dd") + "/" + dtStartDate.AddDays(index).ToString("MM") + "/" + dtStartDate.AddDays(index).ToString("yyyy") + ")");
        }

        //return ("(" + dtStartDate.AddDays(index).ToString("dd") + "/" + dtStartDate.AddDays(index).ToString("MM") + "/" + dtStartDate.AddDays(index).ToString("yyyy") + ")");
    }

    public void BindTimes() {
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstTimes = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);
        if (lstTimes != null && lstTimes.Count > 0) {
            FillControls.FillDropDown(ref ddlStartTime, lstTimes, "SlotTime", "SlotTimeID");
            FillControls.FillDropDown(ref ddlEndTime, lstTimes, "SlotTime", "SlotTimeID");
        }
    }

    #endregion
}
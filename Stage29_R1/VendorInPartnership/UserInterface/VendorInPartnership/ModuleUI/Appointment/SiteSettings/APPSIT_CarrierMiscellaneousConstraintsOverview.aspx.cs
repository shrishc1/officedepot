﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;

public partial class APPSIT_CarrierMiscellaneousConstraintsOverview : CommonPage
{
    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ucExportToExcel1.GridViewControl = UcGridView1;
        ucExportToExcel1.FileName = "CarrierMiscellaneousConstraintsOverview";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    BindCarrierMicellaneousConstraints();
        //}
        //ucExportToExcel1.GridViewControl = UcGridView1;
        //ucExportToExcel1.FileName = "CarrierMiscellaneousConstraintsOverview";
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            ddlSite.innerControlddlSite.AutoPostBack = true;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["SelectedSiteID"] != null && !IsPostBack)
        {
            ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Session["SelectedSiteID"].ToString()));
        }
        if (!IsPostBack)
        {
            BindCarrierMicellaneousConstraints();
        }
    }

    public override void SiteSelectedIndexChanged()
    {
        Session["SelectedSiteID"] = ddlSite.innerControlddlSite.SelectedItem.Value;
        BindCarrierMicellaneousConstraints();
    }

    #region Methods

    protected void BindCarrierMicellaneousConstraints()
    {
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();
        APPCNT_CarrierBAL oAPPCNT_CarrierBAL = new APPCNT_CarrierBAL();
        List<MASCNT_CarrierBE> lstCarrier = new List<MASCNT_CarrierBE>();

        oMASCNT_CarrierBE.Action = "GetAllCarrierMiscCons";
        oMASCNT_CarrierBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_CarrierBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        lstCarrier = oAPPCNT_CarrierBAL.GetAllCarrierMiscConsBAL(oMASCNT_CarrierBE);
        if (lstCarrier.Count > 0)
        {
            if (!string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedItem.Value))
            {
                var lstFilteredData = lstCarrier.FindAll(v => v.SiteID == Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value));
                UcGridView1.DataSource = lstFilteredData;
                ViewState["lstSites"] = lstFilteredData;
            }
            else 
            { 
                UcGridView1.DataSource = lstCarrier;
                ViewState["lstSites"] = lstCarrier;
            }
        }
        else 
        { 
            UcGridView1.DataSource = null;
            ViewState["lstSites"] = lstCarrier;
        }        
        UcGridView1.DataBind();        
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASCNT_CarrierBE>.SortList((List<MASCNT_CarrierBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
    #endregion
}
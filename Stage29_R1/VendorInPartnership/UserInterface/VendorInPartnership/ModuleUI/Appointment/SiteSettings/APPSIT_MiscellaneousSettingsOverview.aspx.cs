﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Collections.Generic;

public partial class APPSIT_MiscellaneousSettingsOverview : CommonPage
{
    #region Events

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            BindGrid();
        }
        ucExportToExcel1.GridViewControl = grdApptSettings;
        ucExportToExcel1.FileName = "MiscellaneousSettingsOverview";
    }

    protected void grdApptSettings_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string toleranceDuaDate = Convert.ToString( DataBinder.Eval(e.Row.DataItem, "ToleranceDueDay"));
            if (toleranceDuaDate == "0")
                e.Row.Cells[2].Text = "Fixed";
            else if (toleranceDuaDate == "-1")
                e.Row.Cells[2].Text = "Open";
            else
                e.Row.Cells[2].Text = toleranceDuaDate;



            string NoticePeriodDays = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "BookingNoticePeriodInDays"));
            string NoticePeriodTime = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "BookingNoticeTime"));
            System.DateTime dt ;
            if(NoticePeriodTime != null && NoticePeriodTime != "")            
                dt =  Convert.ToDateTime(NoticePeriodTime);
            else
                dt =  Convert.ToDateTime("01/01/1900");

            if (NoticePeriodDays != string.Empty && NoticePeriodTime != string.Empty)
            {
                string BookingNoticePeriod = NoticePeriodDays + " Days Before " + dt.Hour.ToString() + ":"+dt.Minute.ToString();
                e.Row.Cells[1].Text = BookingNoticePeriod;
            }

        }
    }

    #endregion 

    #region Methods

    private void BindGrid()
    {
        MAS_SiteBE oMAS_SITEBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SITEBAL = new MAS_SiteBAL();

        List<MAS_SiteBE> lstSites= new List<MAS_SiteBE>();

        oMAS_SITEBE.Action="ShowAll";
        oMAS_SITEBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_SITEBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMAS_SITEBE.TimeSlotWindow = String.Empty;

        lstSites = oMAS_SITEBAL.GetSiteMisSettingBAL(oMAS_SITEBE);
        grdApptSettings.DataSource=lstSites;
        grdApptSettings.DataBind();
        ViewState["lstSites"] = lstSites;
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MAS_SiteBE>.SortList((List<MAS_SiteBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    #endregion
    
}
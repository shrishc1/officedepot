﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="~/ModuleUI/Appointment/SiteSettings/APPSIT_CarrierProcessingWindowOverview.aspx.cs"
    Inherits="APPSIT_CarrierProcessingWindowOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblCarrierProcessingWindow" runat="server" Text="Carrier Processing Window"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="APPSIT_CarrierProcessingWindowEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" onsorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblSiteDesc" runat="server" Text='<%#Eval("Site.SiteName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Carrier" SortExpression="Carrier.CarrierName">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>                            
                             <asp:HyperLink ID="hlCarrier" runat="server" Text='<%#Eval("Carrier.CarrierName") %>'
                                    NavigateUrl='<%# EncryptQuery("APPSIT_CarrierProcessingWindowEdit.aspx?SiteCarrierID=" + Eval("SiteCarrierID"))%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="AverageNoofPalletsUnloadedperhour" DataField="APP_PalletsUnloadedPerHour"  SortExpression="APP_PalletsUnloadedPerHour">
                            <HeaderStyle HorizontalAlign="Center" Width="25%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="AverageNoofCartonsUnloadedperhour" DataField="APP_CartonsUnloadedPerHour" SortExpression="APP_CartonsUnloadedPerHour">
                            <HeaderStyle HorizontalAlign="Center" Width="25%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="AverageNoofLinesReceivingperhour" DataField="APP_ReceiptingLinesPerHour" SortExpression="APP_ReceiptingLinesPerHour">
                            <HeaderStyle HorizontalAlign="Center" Width="25%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                       
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="APPSIT_SkuDoorMatrixSetupOverview.aspx.cs" Inherits="APPSIT_SkuDoorMatrixSetupOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucAddButton" TagPrefix="uc" Src="~/CommonUI/UserControls/ucAddButton.ascx" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblSKUDoorMatrix" runat="server" Text="OD SKU Door Matrix Setup"></cc1:ucLabel>
    </h2>
    <table class="top-settings" width="30%">
        <tr>
            <td style="font-weight: bold;" align="center">
                <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;
                <uc2:ucSite ID="ddlSite" runat="server" />
            </td>
        </tr>
    </table>
    <div class="button-row">
        <uc:ucAddButton ID="btnAdd" runat="server" />
        <uc1:ucExportButton ID="btnExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center">
                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid"
                    AllowSorting="true">
                    <EmptyDataTemplate>
                        <cc1:ucLabel ID="lblNoData" Text="No data found" runat="server"></cc1:ucLabel>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="OD SKU #" SortExpression="ODSkuNumber">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpOdSkuNo" runat="server" Text='<%# Eval("ODSkuNumber") %>' NavigateUrl='<%# EncryptQuery("APPSIT_SkuDoorMatrixSetupEdit.aspx?SKUDoorMatrixID="+ Eval("SKUDoorMatrixID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DoorName" SortExpression="DoorNumber.DoorNumber">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblDoorName" runat="server" Text='<%#Eval("DoorNumber.DoorNumber") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>

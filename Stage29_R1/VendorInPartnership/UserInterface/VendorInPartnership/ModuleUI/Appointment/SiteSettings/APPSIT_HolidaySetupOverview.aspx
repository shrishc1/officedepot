﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_HolidaySetupOverview.aspx.cs" Inherits="APPSIT_HolidaySetupOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
    <%@ Register Src="../../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblSiteHolidayClosures" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>                    
                    <td style="font-weight: bold; width:10%">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" ></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">:</td>
                    <td style="font-weight: bold; width: 10%">
                        <uc1:ucCountry ID="ucCountry" runat="server" />
                    </td>
                    <td style="width:4%"></td>

                    <td style="font-weight: bold;width:10%">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold;width: 10%">
                        <uc2:ucSite ID="ddlSite" runat="server" />
                    </td>
                    <td style="width:4%"></td>

                    <td style="font-weight: bold;width:10%">
                        <cc1:ucLabel ID="lblYear" runat="server" Text="Year"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold;width: 10%">
                        <cc1:ucNumericTextbox ID="txtYear" runat="server" onkeyup="AllowNumbersOnly(this);"
                                                MaxLength="4" Width="60px"></cc1:ucNumericTextbox>
                    </td>
                    <td style="width:4%"></td>

                    <td style="width:25%">
                        <cc1:ucButton ID="btnGo" runat="server" Text="Go" onclick="btnGo_Click" />
                    </td>
                </tr>
            </table>
            <div class="button-row" id="divbtnOpr" runat="server">
                <cc2:ucAddButton ID="btnAdd" runat="server" />
                <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
            </div>
            <table width="100%">
                <tr>
                    <td align="center" colspan="9">
                        <cc1:ucGridView ID="grdHoliday" Width="100%" runat="server" CssClass="grid" OnRowDataBound="grdHoliday_RowDataBound"
                            AlternatingRowStyle-CssClass="altcolor" OnSorting="SortGrid" AllowSorting="true">
                            <Columns>
                                <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                                    <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="ltSiteName" runat="server" Text='<%# Eval("Site.SiteName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date" SortExpression="HolidayDate">
                                    <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hpLink" runat="server" Text='<%# Eval("HolidayDate", "{0:dd/MM/yyyy}") %>'
                                            NavigateUrl='<%# EncryptQuery("APPSIT_HolidaySetupEdit.aspx?SiteHolidayID=" + Eval("SiteHolidayID"))%>'></asp:HyperLink>
                                        <asp:Label ID="ltHolidayDate" runat="server" Text='<%# Eval("HolidayDate", "{0:dd/MM/yyyy}") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Reason" DataField="Reason" SortExpression="Reason">
                                    <HeaderStyle HorizontalAlign="Left" Width="65%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using Utilities;
using WebUtilities;
using System.Linq;
using System.Collections;
using System.Drawing;
using BaseControlLibrary;

public partial class APPSIT_TimeWindow : CommonPage
{
    protected string DoorName = WebCommon.getGlobalResourceValue("DoorName");

    public int iTimeSplit = 12;

    protected void Page_InIt(object sender, EventArgs e) {
        ddlSite.CurrentPage = this;
        ddlSite.TimeSlotWindow = "W";
    }

    public override void SitePost_Load() {
        base.SitePost_Load();
        if (!Page.IsPostBack) {
            if (Session["TimeDayAndSiteId"] == null)
            {
                ddlSite.innerControlddlSite.AutoPostBack = true;
                hdnWeekDay.Value = "Mon";
                pnlTimeWindowtop.GroupingText = "Time Window For" + " " + "Monday";
                GenerateGrid();
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e) {
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) 
        {
            if (Session["TimeDayAndSiteId"] != null)
            {
                /* Here getting Time window day and Site Id in array. */
                string timeWindowDay = Convert.ToString(Session["TimeDayAndSiteId"]);
                string[] TimeDayAndSiteId = new string[2];
                TimeDayAndSiteId = timeWindowDay.Split(',');

                /* Here getting Time window day and Site Id hidden field and drop down list. */
                hdnWeekDay.Value = TimeDayAndSiteId[0];
                ddlSite.innerControlddlSite.SelectedIndex =
                    ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(TimeDayAndSiteId[1]));

                /* Finally here setting pannel data and based on selection being called Generate grid method. */
                hdnDoorID.Value = ",";
                pnlTimeWindow.Controls.RemoveAt(0);
                pnlTimeWindowtop.GroupingText = string.Format("Time Window For {0}", this.GetDay(hdnWeekDay.Value));
                this.GenerateGrid();
                //Session["TimeDayAndSiteId"] = null;
            }
        }
    }

    private TableCell getTableCell(string text, double cellWidth, int borderWidth, int columnSpan) {
        TableCell tc = new TableCell();
        tc.Text = text;
        //if (borderWidth != 0)
            tc.BorderWidth = Unit.Pixel(borderWidth);
        //if (cellWidth != 0)
            tc.Width = Unit.Percentage(cellWidth);
        if (columnSpan > 0)
            tc.ColumnSpan = columnSpan;
       
        return tc;
    }

    private void GenerateGrid() {
        #region Get WeekSetupDetail
        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();
        oMASSIT_WeekSetupBE.Action = "ShowAll";
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndWeekday = hdnWeekDay.Value;
        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
        #endregion

        if (lstWeekSetup.Count <= 0) {
            string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
            return;
        }
        else if (lstWeekSetup[0].StartTime == null) {
            string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
            return;
        }

        #region Get DoorConstraints
        APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
        MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();
        oMASSIT_DoorOpenTimeBE.Action = "GetDoorConstraints";
        oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_DoorOpenTimeBE.Weekday = hdnWeekDay.Value;
        List<MASSIT_DoorOpenTimeBE> lstDoorConstraints = oMASSIT_DoorOpenTimeBAL.GetDoorConstraintsBAL(oMASSIT_DoorOpenTimeBE);
        #endregion

        string DoorIDs = ",";
        hdnDoorIDEndDay.Value = ",";

        for (int iCount = 0; iCount < lstDoorConstraints.Count; iCount++) {
            if (lstDoorConstraints[iCount].StartWeekday.ToLower() != lstDoorConstraints[iCount].Weekday.ToLower())
                DoorIDs += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";
            else
                hdnDoorIDEndDay.Value += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";
        }
        hdnDoorID.Value = DoorIDs;

        #region Get DoorNoType
        oMASSIT_DoorOpenTimeBE.Action = "GetDoorNoAndType";
        oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        List<MASSIT_DoorOpenTimeBE> lstDoorNoType = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);
        #endregion

        #region Table to show data

        int iDoorCount = lstDoorNoType.Count;
        double decCellWidth = (100 / (iDoorCount + 1));
        double dblCellWidth = Convert.ToDouble(Math.Ceiling(decCellWidth));

        Table tbl = new Table();
        tbl.CellSpacing = tbl.CellPadding = 0;
        tbl.CssClass = "form-tableGrid";
        tbl.Width = Unit.Percentage(100);

        pnlHeaderWindow.Controls.Add(tbl);

        TableCell tcDoorNo = getTableCell(DoorName, dblCellWidth, 1, 0);
        TableRow trDoorNo = new TableRow();
        trDoorNo.Cells.Add(tcDoorNo);

        TableCell tcDoorType = getTableCell("Door Type", dblCellWidth, 1, 0);
        TableRow trDoorType = new TableRow();
        trDoorType.Cells.Add(tcDoorType);
        
        //To show all doors
        for (int iCount = 1; iCount <= iDoorCount; iCount++) {
            tcDoorNo = getTableCell(string.Empty, dblCellWidth, 1, 0);
            Label lbl = new Label();
            lbl.Width = 60;
            lbl.Text = lstDoorNoType[iCount - 1].DoorNo.DoorNumber.ToString() + "     ";
            tcDoorNo.Controls.Add(lbl);
            trDoorNo.Cells.Add(tcDoorNo);

            tcDoorType = getTableCell(lstDoorNoType[iCount - 1].DoorType.DoorType, dblCellWidth, 1, 0);
            trDoorType.Cells.Add(tcDoorType);
        }
        trDoorNo.BackColor = trDoorType.BackColor  = System.Drawing.Color.LightGray;
        tbl.Rows.Add(trDoorNo);
        tbl.Rows.Add(trDoorType);   

        Table tblSlot = new Table();
        tblSlot.CellSpacing = tblSlot.CellPadding = 0;
        tblSlot.BorderWidth = 0;
        tblSlot.CssClass = "form-tableGrid";
        tblSlot.Width = Unit.Percentage(100);

        pnlTimeWindow.Controls.Add(tblSlot);

        #region Get SlotTime
        List<SYS_SlotTimeBE> lstSlotTimeFilter = new List<SYS_SlotTimeBE>();
        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);
        lstSlotTime = lstSlotTime.FindAll(delegate(SYS_SlotTimeBE t) { return t.SlotTimeID <= 96; });
        #endregion

        if (lstWeekSetup != null && lstWeekSetup.Count > 0) {
            hdnStartWeekDay.Value = lstWeekSetup[0].StartWeekday;
            hdnEndWeekDay.Value = lstWeekSetup[0].EndWeekday;
        }

        for (int jCount = 0; jCount < lstSlotTime.Count; jCount++) {
            string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + "." + lstWeekSetup[0].StartTime.Value.Minute.ToString();
            string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();

            decimal dSlotTime = Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.'));
            decimal dStartTime = Convert.ToDecimal(StartTime);
            decimal dEndTime = Convert.ToDecimal(EndTime);

            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
                dEndTime = Convert.ToDecimal("24.00");

            if (dSlotTime >= dStartTime && dSlotTime < dEndTime) {
                TableRow tr = new TableRow();
                TableCell tc = getTableCell(lstWeekSetup[0].StartWeekday + " - " + lstSlotTime[jCount].SlotTime, dblCellWidth, 1, 0);
                tr.Cells.Add(tc);

                for (int iCount = 1; iCount <= iDoorCount; iCount++) {
                    tc = getTableCell(string.Empty, dblCellWidth, 1, 0);                   
                    tr.Cells.Add(tc);
                }
                tblSlot.Rows.Add(tr);      
      
                SYS_SlotTimeBE SYS_SlotTimeBEFilter = new SYS_SlotTimeBE();
                SYS_SlotTimeBEFilter.SlotTime = lstSlotTime[jCount].SlotTime;
                SYS_SlotTimeBEFilter.SlotTimeID = lstSlotTime[jCount].SlotTimeID;
                lstSlotTimeFilter.Add(SYS_SlotTimeBEFilter);

            }
        }

        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday) {
            for (int jCount = 0; jCount < lstSlotTime.Count; jCount++) {
                string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();

                if (Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.')) < Convert.ToDecimal(EndTime)) {
                    TableRow tr = new TableRow();
                    TableCell tc = getTableCell(lstWeekSetup[0].EndWeekday + " - " + lstSlotTime[jCount].SlotTime, dblCellWidth, 1, 0);
                    tr.Cells.Add(tc);

                    for (int iCount = 1; iCount <= iDoorCount; iCount++) {                        
                        tc = getTableCell(string.Empty, dblCellWidth, 1, 0);                        
                        tr.Cells.Add(tc);
                    }
                    tblSlot.Rows.Add(tr);

                    SYS_SlotTimeBE SYS_SlotTimeBEFilter = new SYS_SlotTimeBE();
                    SYS_SlotTimeBEFilter.SlotTime = lstSlotTime[jCount].SlotTime;
                    SYS_SlotTimeBEFilter.SlotTimeID = lstSlotTime[jCount].SlotTimeID;
                    lstSlotTimeFilter.Add(SYS_SlotTimeBEFilter);
                }
            }
        }
        #endregion


        #region Get Time window details on the basis of selected site and selected day

        int iDoorId = -1;
        int iSlotID = -1;

        MASSIT_TimeWindowBE oMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
        oMASSIT_TimeWindowBE.Action = "GetTimeWindow";
        oMASSIT_TimeWindowBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_TimeWindowBE.Weekday = hdnWeekDay.Value;

        MASSIT_TimeWindowBAL oMASSIT_TimeWindowBAL = new MASSIT_TimeWindowBAL();
        List<MASSIT_TimeWindowBE> lstMASSIT_TimeWindowBE = oMASSIT_TimeWindowBAL.GetTimeWindowBAL(oMASSIT_TimeWindowBE);

        List<MASSIT_TimeWindowBE> lstMASSIT_TimeWindow = new List<MASSIT_TimeWindowBE>();

        #endregion

        #region rowspan

        //for (int iRow = 0; iRow < iDoorCount; iRow++) {
        //    for (int iCell = 1; iCell <= tblSlot.Rows.Count; iCell++) {
        //        tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Gray;
        //    }
        //}

        

        for (int iCell = 1; iCell <= iDoorCount; iCell++) {

            iDoorId = lstDoorNoType[iCell-1].SiteDoorNumberID;

            for (int iRow = 0; iRow < tblSlot.Rows.Count; iRow++) {

                iSlotID = lstSlotTimeFilter[iRow].SlotTimeID;

                lstMASSIT_TimeWindow = lstMASSIT_TimeWindowBE.FindAll(delegate(MASSIT_TimeWindowBE t) 
                { 
                    return t.SiteDoorNumberID == iDoorId && t.StartSlotTimeID == iSlotID ;
                });

                if (lstMASSIT_TimeWindow.Count > 0) {
                    int len = lstMASSIT_TimeWindow[0].EndSlotTimeID - lstMASSIT_TimeWindow[0].StartSlotTimeID; // +1;
                    if (!lstMASSIT_TimeWindow[0].StartTimeWeekday.Trim().ToUpper().Equals(lstMASSIT_TimeWindow[0].EndTimeWeekday.Trim().ToUpper()))
                        len = len + 96;

                    //else
                    //    len = lstMASSIT_TimeWindow[0].EndSlotTimeID - lstMASSIT_TimeWindow[0].StartSlotTimeID + 1;

                    int iTextRowNo = 1;
                    
                    if (len % 2 == 0)
                        iTextRowNo = Convert.ToInt32(len / 2);
                    else
                        iTextRowNo = Convert.ToInt32(len / 2) + 1;

                    for (int iCount = 1; iCount <= len; iCount++) {

                        try {
                            tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.Gray;
                            if (iCount == 1) {
                                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "2px");  // .BorderWidth = 0;                            
                            }
                            else {
                                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "0px");  // .BorderWidth = 0;                            
                            }

                            if (iCount == iTextRowNo) {


                                string path = "APPSIT_TimeWindowEdit.aspx"
                                   + "?SiteId=" + ddlSite.innerControlddlSite.SelectedItem.Value
                                   + "&SiteDoorNoId=" + lstDoorNoType[iCell - 1].SiteDoorNumberID.ToString()
                                   + "&DoorNo=" + lstDoorNoType[iCell - 1].DoorNo.DoorNumber.ToString()
                                   + "&DoorType=" + lstDoorNoType[iCell - 1].DoorType.DoorType.ToString()
                                   + "&WeekDay=" + hdnWeekDay.Value
                                   + "&TimeWindowID=" + lstMASSIT_TimeWindow[0].TimeWindowID;

                                HyperLink hyp = new HyperLink();
                                hyp.NavigateUrl = Common.EncryptQuery(path);
                                hyp.Text = lstMASSIT_TimeWindow[0].WindowName + "<br>(" + lstMASSIT_TimeWindow[0].Priority + ")";
                                hyp.Style.Add("font-size", "large");
                                tblSlot.Rows[iRow].Cells[iCell].Controls.Add(hyp);
                            }

                            if (iCount == len)
                                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "2px");
                            else
                                tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "0px");

                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "1px");  // .BorderWidth = 0;
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "1px");
                            //tblSlot.Rows[iRow].Cells[iCell].Attributes.Add("onclick", "alert('hi');");
                            tblSlot.Rows[iRow].Cells[iCell].Style.Add("text-align", "center");
                            iRow++;
                        }
                        catch {
                        }

                    }
                    iRow = iRow - 1;
                }
                else {
                    tblSlot.Rows[iRow].Cells[iCell].BackColor = Color.White;
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-top-width", "1px");
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-bottom-width", "1px");
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-left-width", "1px");  
                    tblSlot.Rows[iRow].Cells[iCell].Style.Add("border-right-width", "1px");
                }
            }
        }
        #endregion

        #region Add button window

        Table tblbutton = new Table();
        tblbutton.CellSpacing = tblSlot.CellPadding = 0;
        tblbutton.BorderWidth = 0; 
        tblbutton.CssClass = "form-tableGrid";
        tblbutton.Width = Unit.Percentage(100);

        pnlbuttons.Controls.Add(tblbutton);
        trDoorNo = new TableRow();

        tcDoorNo = getTableCell(string.Empty, dblCellWidth, 1, 0);
        trDoorNo = new TableRow();
        trDoorNo.Cells.Add(tcDoorNo);        

        for (int iCount = 1; iCount <= iDoorCount; iCount++) {
            string path = "APPSIT_TimeWindowEdit.aspx" + "?SiteId=" + ddlSite.innerControlddlSite.SelectedItem.Value
                + "&SiteDoorNoId=" + lstDoorNoType[iCount - 1].SiteDoorNumberID.ToString()
                + "&DoorNo=" + lstDoorNoType[iCount - 1].DoorNo.DoorNumber.ToString()
                + "&DoorType=" + lstDoorNoType[iCount - 1].DoorType.DoorType.ToString()
                + "&WeekDay=" + hdnWeekDay.Value;
                //+ "&TimeWindowID=8" ;    
           
            tcDoorNo = getTableCell(string.Empty, dblCellWidth, 1, 0);
            //ucButton btn = new ucButton();
            ucLinkButton btn = new ucLinkButton();
            btn.ID = "btnAdd_" + iCount.ToString();
            btn.Text = "Add";
            btn.CssClass = "button";
            btn.PostBackUrl = Common.EncryptQuery(path);            
            tcDoorNo.Style.Add("text-align", "center");
            tcDoorNo.Controls.Add(btn);

            
            //HiddenField hdnDetails = new HiddenField();
            //hdnDetails.ID = "hdnDetails_" + iCount.ToString();
            //hdnDetails.Value = lstDoorNoType[iCount - 1].DoorNo.DoorNumber.ToString() + "~" + lstDoorNoType[iCount - 1].DoorType.DoorType + "~" + lstDoorNoType[iCount - 1].SiteDoorNumberID.ToString();
            //tcDoorNo.Controls.Add(hdnDetails);
            trDoorNo.Cells.Add(tcDoorNo);
        }
        tblbutton.Rows.Add(trDoorNo);

        #endregion
    }

    protected void btnWeekday_Click(object sender, EventArgs e)
    {
        hdnDoorID.Value = ",";
        hdnWeekDay.Value = ((System.Web.UI.WebControls.Button)(sender)).CommandArgument;
        Session["TimeDayAndSiteId"] = string.Format("{0},{1}", hdnWeekDay.Value, ddlSite.innerControlddlSite.SelectedValue);
        pnlTimeWindowtop.GroupingText = "Time Window For" + " " + ((Button)(sender)).Text;
        pnlTimeWindow.Controls.RemoveAt(0);
        pnlHeaderWindow.Controls.RemoveAt(0);
        pnlbuttons.Controls.RemoveAt(0);
        GenerateGrid();
    }
    
    public override void SiteSelectedIndexChanged() {
        hdnDoorID.Value = ",";
        hdnWeekDay.Value = "Mon";
        Session["TimeDayAndSiteId"] = string.Format("{0},{1}", hdnWeekDay.Value, ddlSite.innerControlddlSite.SelectedValue);
        pnlTimeWindowtop.GroupingText = "Time Window For" + " " + "Monday";
        pnlTimeWindow.Controls.RemoveAt(0);
        pnlHeaderWindow.Controls.RemoveAt(0);
        pnlbuttons.Controls.RemoveAt(0);
        GenerateGrid();
    }

    private string GetDay(string shortDay)
    {
        string dayName = string.Empty;
        shortDay = shortDay.Trim().ToUpper();
        switch (shortDay)
        {
            case "MON":
                dayName = "Monday";
                break;
            case "TUE":
                dayName = "Tuesday";
                break;
            case "WED":
                dayName = "Wednesday";
                break;
            case "THU":
                dayName = "Thursday";
                break;
            case "FRI":
                dayName = "Friday";
                break;
            case "SAT":
                dayName = "Saturday";
                break;
            case "SUN":
                dayName = "Sunday";
                break;
            default:
                dayName = string.Empty;
                break;
        }

        return dayName;
    }
}
﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class APPSIT_DoorNoSetupOverview : CommonPage 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindSiteDoorNoSetup();
        }
        btnExportToExcel1.GridViewControl = UcGridView1;
        btnExportToExcel1.FileName = "DoorNoSetupOverview";
    }

    #region Methods

    protected void BindSiteDoorNoSetup()
    {
        MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE = new MASSIT_DoorNoSetupBE();

        MASSIT_DoorNoSetupBAL oMASSIT_DoorNoSetupBAL = new MASSIT_DoorNoSetupBAL();

        oMASSIT_DoorNoSetupBE.Action = "ShowAll";
        oMASSIT_DoorNoSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_DoorNoSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MASSIT_DoorNoSetupBE> lstDoorNoSetup = oMASSIT_DoorNoSetupBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorNoSetupBE);

        if (lstDoorNoSetup.Count > 0) {
            UcGridView1.DataSource = lstDoorNoSetup;
            UcGridView1.DataBind();
            ViewState["lstSites"] = lstDoorNoSetup;
        }
        
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASSIT_DoorNoSetupBE>.SortList((List<MASSIT_DoorNoSetupBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
    
  
    #endregion
}
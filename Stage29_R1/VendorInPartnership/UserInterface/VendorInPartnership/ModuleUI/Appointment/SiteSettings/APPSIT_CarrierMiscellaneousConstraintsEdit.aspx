﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" 
AutoEventWireup="true" CodeFile="APPSIT_CarrierMiscellaneousConstraintsEdit.aspx.cs" Inherits="APPSIT_CarrierMiscellaneousConstraintsEdit" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor" TagPrefix="uc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
    </script>
    <h2>
        <cc1:uclabel id="lblCarrierMiscellaneousConstraints" runat="server" text="Carrier Micellaneous Constraints"></cc1:uclabel>
    </h2>
    <asp:HiddenField ID="hdnSelectedVendorID" runat="server" />
    <asp:ValidationSummary ID="vsAdd" runat="server" ValidationGroup="a" DisplayMode="BulletList"
        ShowMessageBox="true" ShowSummary="false" />
    <div class="right-shadow">
        <div class="formbox">
            <table width="70%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 10%">
                    </td>
                    <td style="font-weight: bold; width: 29%">
                        <cc1:uclabel id="lblSitePrefixName" runat="server" text="Site" isrequired="true"></cc1:uclabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 60%">
                        <uc2:ucsite id="ddlSite" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:uclabel id="lblCarrier" runat="server" isrequired="true"></cc1:uclabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucDropdownList ID="ddlCarrier" runat="server"></cc1:ucDropdownList>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:uclabel id="lblCarrierCheckingRestriction" runat="server" text="Vender Checking Restriction"></cc1:uclabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:uccheckbox id="chkCarrierCheckingRestriction" runat="server" />
                    </td>
                </tr>
            </table>
            <cc1:ucpanel id="pnlVolumeConstraintSettings" runat="server" groupingtext="Volume Constraint Settings"
                cssclass="fieldset-form">
                <table width="50%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td style="font-weight: bold; width: 50%;">
                            <cc1:ucLabel ID="lblMaximumNumberofPallets" runat="server" Text="Maximum Number of Pallets"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5%;">
                            :
                        </td>
                        <td valign="top" style="width: 45%">
                            <cc1:ucNumericTextbox ID="txtMaximumNumberofPallets"  onkeypress="return IsNumberKey(event, this);" MaxLength="3" runat="server" Width="40px"></cc1:ucNumericTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblMaximumNumberofLines" runat="server" Text="Maximum Number of Lines"></cc1:ucLabel>
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <cc1:ucNumericTextbox ID="txtMaximumNumberofLines" onkeypress="return IsNumberKey(event, this);" MaxLength="3" runat="server" Width="40px"></cc1:ucNumericTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblMaximumNumberDeliveriesPerDay" runat="server" Text="Maximum Number Deliveries per day"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td valign="top">
                            <cc1:ucNumericTextbox ID="txtMaximumNumberDeliveriesPerDay" onkeypress="return IsNumberKey(event, this);" MaxLength="3" runat="server" Width="40px"></cc1:ucNumericTextbox>
                        </td>
                    </tr>
                </table>
            </cc1:ucpanel>
            <cc1:ucpanel id="pnlDayTimeWindowConstraintSettings" runat="server" groupingtext="Day / Time Window Constraint Settings"
                cssclass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblDayConstraintCanNotDeliverOn" runat="server" Text="Day Constraint( Can not Deliver on )"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 2%;">
                            :
                        </td>
                        <td class="nobold">
                            <cc1:ucCheckbox ID="chkMonday1" runat="server" />
                            <cc1:ucLabel ID="lblMonday" runat="server" Text="Monday"></cc1:ucLabel>&nbsp;
                            <cc1:ucCheckbox ID="chkTuesday1" runat="server" />
                            <cc1:ucLabel ID="lblTuesday" runat="server" Text="Tuesday"></cc1:ucLabel>&nbsp;
                            <cc1:ucCheckbox ID="chkWednesday1" runat="server" />
                            <cc1:ucLabel ID="lblWednesday" runat="server" Text="Wednesday"></cc1:ucLabel>&nbsp;
                            <cc1:ucCheckbox ID="chkThrusday1" runat="server" />
                            <cc1:ucLabel ID="lblThursday" runat="server" Text="Thursday"></cc1:ucLabel>&nbsp;
                            <cc1:ucCheckbox ID="chkFriday1" runat="server" />
                            <cc1:ucLabel ID="lblFriday" runat="server" Text="Friday"></cc1:ucLabel>&nbsp;
                            <cc1:ucCheckbox ID="chkSaturday1" runat="server" />
                            <cc1:ucLabel ID="lblSaturday" runat="server" Text="Saturday"></cc1:ucLabel>&nbsp;
                            <cc1:ucCheckbox ID="chkSunday1" runat="server" />
                            <cc1:ucLabel ID="lblSunday" runat="server" Text="Sunday"></cc1:ucLabel>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <h3>
                                Time Constraint (Can not Deliver)</h3>
                            <table style="width: 70%">
                                <tr>
                                    <td width="9%">
                                        <cc1:ucLabel ID="lblBefore" runat="server" Text="Before"></cc1:ucLabel>
                                    </td>
                                    <td width="1%">
                                        :
                                    </td>
                                    <td width="40%" class="nobold">
                                        <cc1:ucDropdownList ID="ddlSlotBeforeTime" runat="server" Width="60px"></cc1:ucDropdownList>
                                        <cc1:ucLabel ID="lblHHMM_1" runat="server" Text="HH:MM"></cc1:ucLabel>
                                        <%--<asp:RegularExpressionValidator ID="revtxtBefore" runat="server" ControlToValidate="txtBefore" Display="None" SetFocusOnError="true"
                                            ValidationGroup="a" ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$"></asp:RegularExpressionValidator>--%>
                                    </td>
                                    <td width="9%">
                                        <cc1:ucLabel ID="lblAfter" runat="server" Text="After"></cc1:ucLabel>
                                    </td>
                                    <td width="1%">
                                        :
                                    </td>
                                    <td width="40%" class="nobold">
                                         <cc1:ucDropdownList ID="ddlSlotAfterTime" runat="server" Width="60px"></cc1:ucDropdownList>
                                        <cc1:ucLabel ID="lblHHMM_2" runat="server" Text="HH:MM"></cc1:ucLabel>
                                        <%--<asp:RegularExpressionValidator ID="revtxtAfter" runat="server" ControlToValidate="txtAfter" Display="None" SetFocusOnError="true"
                                            ValidationGroup="a" ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$"></asp:RegularExpressionValidator>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucpanel>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucbutton id="btnSave" runat="server" text="Save" class="button" onclick="btnSave_Click"
            validationgroup="a" />
        <cc1:ucbutton id="btnDelete" runat="server" text="Delete" class="button" onclick="btnDelete_Click"
            onclientclick="return confirmDelete();" />
        <cc1:ucbutton id="btnBack" runat="server" text="Back" class="button" onclick="btnBack_Click" />
    </div>
</asp:Content>


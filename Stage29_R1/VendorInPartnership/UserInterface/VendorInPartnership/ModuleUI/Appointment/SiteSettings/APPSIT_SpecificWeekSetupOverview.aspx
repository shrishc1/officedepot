﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_SpecificWeekSetupOverview.aspx.cs" Inherits="APPSIT_SpecificWeekSetupOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc3" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc4" %>
<%@ Register Src="~/CommonUI/UserControls/ucDate.ascx" TagName="ucDate" TagPrefix="cc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblSpecificWeekSetup" runat="server" Text="Specific Week Setup"></cc1:ucLabel>
    </h2>
    <div class="formbox">
        <table width="40%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
            <tr>
                <td style="width: 5%">
                </td>
                <td style="font-weight: bold; width: 10%">
                    <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 5%">:</td>
                <td width="75%">
                    <%--<cc4:ucSite ID="ddlSite" runat="server" />--%>
                    <cc1:ucLabel ID="SiteFromWeekSetup" runat="server"></cc1:ucLabel>
                </td>
                <td style="width: 5%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="lblDate" runat="server" Text="Date"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;">:</td>
                <td >
                    <cc5:ucDate ID="txtDate" runat="server" />
                    <%--<cc1:ucButton ID="btnGo" runat="server" CssClass="button" Width="30px" 
                        onclick="btnGo_Click" />--%>
                    <cc1:ucButton ID="btnCreateSpecificWeekSetup" runat="server" CssClass="button" OnClick="btnCreateSpecificWeekSetup_Click"
                        Width="100px" />
                </td>
                <td>
                    <cc1:ucButton ID="btnReleaseWeek" runat="server" Text="Release week" CssClass="button" OnClick="btnReleaseWeek_Click" />                 
                </td>
            </tr>
        </table>
        <div class="button-row">
            <cc3:ucExportToExcel ID="ucExportToExcel" runat="server" />

        </div>
        <table width="100%" class="form-tableGrid" cellpadding="0" cellspacing="5">
            <tr>
                <th width="30%" style="border-style: none; background-color: White">
                    &nbsp;
                </th>
                <th width="10%" style="text-align: center">
                    <cc1:ucLinkButton ID="lnkMonday" runat="server" Text="Monday"></cc1:ucLinkButton>
                    <br />
                    <cc1:ucLabel ID="DtMonday" runat="server" Text="(DD/MM/YYYY)"></cc1:ucLabel>
                </th>
                <th width="10%" style="text-align: center">
                    <cc1:ucLinkButton ID="lnkTuesday" runat="server" Text="Tuesday"></cc1:ucLinkButton>
                    <br />
                    <cc1:ucLabel ID="DtTuesday" runat="server" Text="(DD/MM/YYYY)"></cc1:ucLabel>
                </th>
                <th width="10%" style="text-align: center">
                    <cc1:ucLinkButton ID="lnkWednesday" runat="server" Text="Wednesday"></cc1:ucLinkButton>
                    <br />
                    <cc1:ucLabel ID="DtWednesday" runat="server" Text="(DD/MM/YYYY)"></cc1:ucLabel>
                </th>
                <th width="10%" style="text-align: center">
                    <cc1:ucLinkButton ID="lnkThursday" runat="server" Text="Thursday"></cc1:ucLinkButton>
                    <br />
                    <cc1:ucLabel ID="DtThursday" runat="server" Text="(DD/MM/YYYY)"></cc1:ucLabel>
                </th>
                <th width="10%" style="text-align: center">
                    <cc1:ucLinkButton ID="lnkFriday" runat="server" Text="Friday"></cc1:ucLinkButton>
                    <br />
                    <cc1:ucLabel ID="DtFriday" runat="server" Text="(DD/MM/YYYY)"></cc1:ucLabel>
                </th>
                <th width="10%" style="text-align: center">
                    <cc1:ucLinkButton ID="lnkSaturday" runat="server" Text="Saturday"></cc1:ucLinkButton>
                    <br />
                    <cc1:ucLabel ID="DtSaturday" runat="server" Text="(DD/MM/YYYY)"></cc1:ucLabel>
                </th>
                <th width="10%" style="text-align: center">
                    <cc1:ucLinkButton ID="lnkSunday" runat="server" Text="Sunday"></cc1:ucLinkButton>
                    <br />
                    <cc1:ucLabel ID="DtSunday" runat="server" Text="(DD/MM/YYYY)"></cc1:ucLabel>
                </th>
            </tr>
            <tr>
                <td>
                    <cc1:ucLabel ID="lblStartTime" runat="server" Text="Start"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblMonStartTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblTueStartTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblWedStartTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblThuStartTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblFriStartTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSatStartTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSunStartTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:ucLabel ID="lblEndTime" runat="server" Text="End"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblMonEndTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblTueEndTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblWedEndTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblThuEndTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblFriEndTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSatEndTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSunEndTime" runat="server" Text="00:00"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:ucLabel ID="lblMaxLifts" runat="server" Text="Max Lifts"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblMonMaxLifts" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblTueMaxLifts" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblWedMaxLifts" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblThuMaxLifts" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblFriMaxLifts" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSatMaxLifts" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSunMaxLifts" runat="server" Text="0"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:ucLabel ID="lblMAxPallets" runat="server" Text="Max Pallets"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblMonMAxPallets" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblTueMAxPallets" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblWedMAxPallets" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblThuMAxPallets" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblFriMAxPallets" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSatMAxPallets" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSunMAxPallets" runat="server" Text="0"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:ucLabel ID="lblMaxLines" runat="server" Text="Max Lines"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblMonMaxLines" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblTueMaxLines" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblWedMaxLines" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblThuMaxLines" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblFriMaxLines" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSatMaxLines" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSunMaxLines" runat="server" Text="0"></cc1:ucLabel>
                </td>
            </tr>
              <tr>
                <td>
                    <cc1:ucLabel ID="lblMaxDeliveries" runat="server" Text="Max Deliveries"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblMonMaxDeliveries" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblTueMaxDeliveries" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblWedMaxDeliveries" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblThuMaxDeliveries" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblFriMaxDeliveries" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSatMaxDeliveries" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSunMaxDeliveries" runat="server" Text="0"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:ucLabel ID="lblMaxContainers" runat="server" Text="Max Containers"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblMonMaxContainers" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblTueMaxContainers" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblWedMaxContainers" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblThuMaxContainers" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblFriMaxContainers" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSatMaxContainers" runat="server" Text="0"></cc1:ucLabel>
                </td>
                <td style="text-align: center">
                    <cc1:ucLabel ID="lblSunMaxContainers" runat="server" Text="0"></cc1:ucLabel>
                </td>
            </tr>
        </table>

        <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" AutoGenerateColumns="false" style="display:none;">
        <Columns>
                <asp:TemplateField  HeaderText="Site">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Literal ID="ltStartTime_1" runat="server" Text='<%#Eval("Site.SiteName")%>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
              <asp:BoundField HeaderText="StartWeekday" DataField="StartWeekday" >
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField  HeaderText="StartTime">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Literal ID="ltStartTime_2" runat="server" Text='<%#Eval("StartTime", "{0:hh:mm}")%>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="End Week day" DataField="EndWeekday" >
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField  HeaderText="EndTime">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Literal ID="ltStartTime_3" runat="server" Text='<%#Eval("EndTime", "{0:hh:mm}")%>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:BoundField HeaderText="Maximum Lift" DataField="MaximumLift" >
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                         <asp:BoundField HeaderText="Maximum Pallet" DataField="MaximumPallet" >
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                         <asp:BoundField HeaderText="Maximum Lines" DataField="MaximumLine" >
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                        <asp:BoundField HeaderText="Maximum Deliveries" DataField="MaximumDeliveries" >
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
        </Columns>
        </cc1:ucGridView>
    </div>
</asp:Content>

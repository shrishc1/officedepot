﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.Collections.Generic;
using System.Linq;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;


public partial class APPSIT_HolidaySetupEdit : CommonPage
{
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_InIt(object sender, EventArgs e) {
        ucSite.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e) {
        txtDate.Attributes.Add("readonly", "readonly");
    }

    public override void SitePost_Load() {
        base.SitePost_Load();
        if (!Page.IsPostBack) {
            if (GetQueryStringValue("SiteHolidayID") != null) {
                BindHoliday();
                btnDelete.Visible = true;
            }
            if (GetQueryStringValue("SiteID") != null) {
                ucSite.innerControlddlSite.SelectedIndex = ucSite.innerControlddlSite.Items.IndexOf(ucSite.innerControlddlSite.Items.FindByValue(GetQueryStringValue("SiteID").ToString()));
            }
        }
    }

    private void BindHoliday() {
        MASSIT_HolidayBE oMASSIT_HolidayBE = new MASSIT_HolidayBE();
        MASSIT_HolidayBAL oMASSIT_HolidayBAL = new MASSIT_HolidayBAL();

        oMASSIT_HolidayBE.Action = "ShowAll";
        oMASSIT_HolidayBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        //oMASSIT_HolidayBE.Site.SiteID = !string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : 0;
        oMASSIT_HolidayBE.SiteHolidayID = Convert.ToInt32(GetQueryStringValue("SiteHolidayID"));
        List<MASSIT_HolidayBE> lstHoliday = oMASSIT_HolidayBAL.GetHolidaysBAL(oMASSIT_HolidayBE);
        if (lstHoliday != null && lstHoliday.Count > 0)
        {
            ucSite.innerControlddlSite.SelectedIndex = ucSite.innerControlddlSite.Items.IndexOf(ucSite.innerControlddlSite.Items.FindByValue(lstHoliday[0].Site.SiteID.ToString()));
            txtDate.innerControltxtDate.Value = Convert.ToDateTime(lstHoliday[0].HolidayDate).ToString("dd/MM/yyyy");
            txtReason.Text = lstHoliday[0].Reason;
        }
        ucSite.innerControlddlSite.Enabled = false;
        txtDate.innerControltxtDate.Disabled = true;
    }

    #region Events

    protected void btnSave_Click(object sender, EventArgs e) {
        MASSIT_HolidayBE oMASSIT_HolidayBE = new MASSIT_HolidayBE();
        MASSIT_HolidayBAL oMASSIT_HolidayBAL = new MASSIT_HolidayBAL();
        
        oMASSIT_HolidayBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        oMASSIT_HolidayBE.Reason = txtReason.Text;

        if (GetQueryStringValue("SiteHolidayID") != null) {
            oMASSIT_HolidayBE.Action = "Edit";
            oMASSIT_HolidayBE.SiteHolidayID = Convert.ToInt32(GetQueryStringValue("SiteHolidayID"));
        }
        else {
            oMASSIT_HolidayBE.Action = "Add";
            oMASSIT_HolidayBE.Site.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_HolidayBE.HolidayDate = txtDate.GetDate;
        }

        oMASSIT_HolidayBAL.addEditHolidayBAL(oMASSIT_HolidayBE);
        EncryptQueryString("APPSIT_HolidaysetupOverview.aspx");
    }
    protected void btnDelete_Click(object sender, EventArgs e) {
        MASSIT_HolidayBE oMASSIT_HolidayBE = new MASSIT_HolidayBE();
        MASSIT_HolidayBAL oMASSIT_HolidayBAL = new MASSIT_HolidayBAL();
        oMASSIT_HolidayBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        if (GetQueryStringValue("SiteHolidayID") != null) {
            oMASSIT_HolidayBE.Action = "Delete";
            oMASSIT_HolidayBE.SiteHolidayID = Convert.ToInt32(GetQueryStringValue("SiteHolidayID"));
            oMASSIT_HolidayBAL.addEditHolidayBAL(oMASSIT_HolidayBE);
            EncryptQueryString("APPSIT_HolidaysetupOverview.aspx");
        }
    }
    protected void btnBack_Click(object sender, EventArgs e) {

        EncryptQueryString("APPSIT_HolidaysetupOverview.aspx");
    }

    public override void SiteSelectedIndexChanged() {        
    }
    #endregion

}
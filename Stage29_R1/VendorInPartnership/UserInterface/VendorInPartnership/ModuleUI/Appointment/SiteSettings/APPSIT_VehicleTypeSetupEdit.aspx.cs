﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class APPSIT_VehicleTypeSetupEdit : CommonPage {
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected string CorrectValueValidation = WebCommon.getGlobalResourceValue("CorrectValueValidation");

    protected void Page_InIt(object sender, EventArgs e) {
        ddlSite.CurrentPage = this;
    }
    protected void Page_Load(object sender, EventArgs e) {
        
    }


    protected void Page_PreRender(object sender, EventArgs e) {
        if (!IsPostBack) {
            EnableDisableControls();
            GetVehicleType();
        }
    }
    #region Methods

    public void EnableDisableControls() {
        if (rdoVolumeBased.Checked == true) {
            txtVolumeBasedPallets.Enabled = true;
            txtVolumeBasedCartons.Enabled = true;

            txtMinTimePerDelivery.Enabled = true;

            txtTimebasedUnloadTime.Enabled = false;

            //Enable and disable the validators
            rfvVolumeBasedCartonsRequired.Enabled = true;
            rfvVolumeBasedPalletsRequired.Enabled = true;
            ranvVolumeBasedCartonsIntegerDataRequired.Enabled = true;
            ranvVolumeBasedPalletsIntegerDataRequired.Enabled = true;
            rfvTimebasedUnloadTimeRequired.Enabled = false;

            rfvMinTimePerDeliveryRequired.Enabled = true;
            revMinTimePerDeliveryCorrectFormatRequired.Enabled = true;

        } else {
            txtVolumeBasedPallets.Enabled = false;
            txtVolumeBasedCartons.Enabled = false;
            txtMinTimePerDelivery.Enabled = false;
            txtTimebasedUnloadTime.Enabled = true;

            //Enable and disable the validators
            rfvVolumeBasedCartonsRequired.Enabled = false;
            rfvVolumeBasedPalletsRequired.Enabled = false;
            ranvVolumeBasedCartonsIntegerDataRequired.Enabled = false;
            ranvVolumeBasedPalletsIntegerDataRequired.Enabled = false;
            rfvTimebasedUnloadTimeRequired.Enabled = true;

            rfvMinTimePerDeliveryRequired.Enabled = false;
            revMinTimePerDeliveryCorrectFormatRequired.Enabled = false;
        }
    }

    private void GetVehicleType() {

        MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

        if (GetQueryStringValue("VehicleTypeID") != null) {
            //btnSave.Visible = false;
            oMASSIT_VehicleTypeBE.Action = "ShowById";
            oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VehicleTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_VehicleTypeBE.VehicleTypeID = Convert.ToInt32(GetQueryStringValue("VehicleTypeID").ToString());
            MASSIT_VehicleTypeBE lstVehicleType = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsByIdBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleType != null) {
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(lstVehicleType.SiteID.ToString())); ;
                txtVehicleType.Text = lstVehicleType.VehicleType;
                if (lstVehicleType.IsNarrativeRequired)
                    chkIsNarrativeRequired.Checked = true;
                else
                    chkIsNarrativeRequired.Checked = false;

                if (lstVehicleType.IsContainer)
                    chkIsVehicleAContainer.Checked = true;
                else
                    chkIsVehicleAContainer.Checked = false;

                if (lstVehicleType.APP_PalletsUnloadedPerHour > 0 &&
                    lstVehicleType.APP_CartonsUnloadedPerHour > 0)
                {

                    rdoVolumeBased.Checked = true;
                    rdoTimeBased.Checked = false;
                    lblPalletsUnloadTime.Text = String.Format("{0:N2}", lstVehicleType.APP_PalletsUnloadedPerHour / 60.0);
                    lblCartonsUnloadTime.Text = String.Format("{0:N2}", lstVehicleType.APP_CartonsUnloadedPerHour / 60.0);
                    txtVolumeBasedPallets.Text = Convert.ToString(lstVehicleType.APP_PalletsUnloadedPerHour);
                    txtVolumeBasedCartons.Text = Convert.ToString(lstVehicleType.APP_CartonsUnloadedPerHour);
                    //hidPalletsUnloadTime.Value = lblPalletsUnloadTime.Text;
                    //hidCartonsUnloadTime.Value = lblCartonsUnloadTime.Text;
                    txtTimebasedUnloadTime.Text = string.Empty;

                    if (!string.IsNullOrEmpty(lstVehicleType.MinimumTimePerDeliveryString)) {
                        string[] strTime = lstVehicleType.MinimumTimePerDeliveryString.Split(':');
                        if (Convert.ToInt16(strTime[0]) < 9) {
                            txtMinTimePerDelivery.Text = "0" + strTime[0];
                        }
                        else {
                            txtMinTimePerDelivery.Text = strTime[0];
                        }
                        if (Convert.ToInt16(strTime[1]) < 9) {
                            txtMinTimePerDelivery.Text = txtMinTimePerDelivery.Text + ":" + "0" + strTime[1];
                        }
                        else {
                            txtMinTimePerDelivery.Text = txtMinTimePerDelivery.Text + ":" + strTime[1];
                        }
                    }

                } else {
                    rdoVolumeBased.Checked = false;
                    rdoTimeBased.Checked = true;
                    txtVolumeBasedPallets.Text = string.Empty;
                    txtVolumeBasedCartons.Text = string.Empty;
                    txtMinTimePerDelivery.Text = string.Empty;
                    if (!string.IsNullOrEmpty(lstVehicleType.TotalUnloadTimeString)) {
                        string[] strTime = lstVehicleType.TotalUnloadTimeString.Split(':');
                        if (Convert.ToInt16(strTime[0]) < 9) {
                            txtTimebasedUnloadTime.Text = "0" + strTime[0];
                        }
                        else {
                            txtTimebasedUnloadTime.Text = strTime[0];
                        }
                        if (Convert.ToInt16(strTime[1]) < 9) {
                            txtTimebasedUnloadTime.Text = txtTimebasedUnloadTime.Text + ":" + "0" + strTime[1];
                        }
                        else {
                            txtTimebasedUnloadTime.Text = txtTimebasedUnloadTime.Text + ":" + strTime[1];
                        }                        
                    }


                }
                EnableDisableControls();
            }
        } else {
            btnDelete.Visible = false;
        }
    }

    #endregion

    #region Events

    public override void SitePost_Load() {
        base.SitePost_Load();
        if (!IsPostBack) {

        }
    }

    protected void btnSave_Click(object sender, EventArgs e) {
        MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();


        oMASSIT_VehicleTypeBE.VehicleType = txtVehicleType.Text.Trim();
        oMASSIT_VehicleTypeBE.IsNarrativeRequired = chkIsNarrativeRequired.Checked == true ? true : false;
        oMASSIT_VehicleTypeBE.IsContainer = chkIsVehicleAContainer.Checked == true ? true : false;
        oMASSIT_VehicleTypeBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        if (GetQueryStringValue("VehicleTypeID") != null) {
            //  edit record
            oMASSIT_VehicleTypeBE.VehicleTypeID = Convert.ToInt32(GetQueryStringValue("VehicleTypeID").ToString());
            oMASSIT_VehicleTypeBE.Action = "Update";
            if (rdoVolumeBased.Checked == true) {
                oMASSIT_VehicleTypeBE.APP_PalletsUnloadedPerHour = Convert.ToInt32(txtVolumeBasedPallets.Text);
                oMASSIT_VehicleTypeBE.APP_CartonsUnloadedPerHour = Convert.ToInt32(txtVolumeBasedCartons.Text);
                oMASSIT_VehicleTypeBE.TotalUnloadTimeInMinute = 0;

                String[] arr = txtMinTimePerDelivery.Text.Split(':');
                oMASSIT_VehicleTypeBE.MinimumTimePerDeliveryInMinute = Convert.ToInt32(arr[0]) * 60 + Convert.ToInt32(arr[1]);

            } else if (rdoTimeBased.Checked == true) {
                oMASSIT_VehicleTypeBE.APP_PalletsUnloadedPerHour = 0;
                oMASSIT_VehicleTypeBE.APP_CartonsUnloadedPerHour = 0;
                String[] arr = txtTimebasedUnloadTime.Text.Split(':');
                oMASSIT_VehicleTypeBE.TotalUnloadTimeInMinute = Convert.ToInt32(arr[0]) * 60 + Convert.ToInt32(arr[1]);
            }
            oMASSIT_VehicleTypeBAL.addEditVehicleTypeDetailsBAL(oMASSIT_VehicleTypeBE);
            EncryptQueryString("APPSIT_VehicleTypeSetupOverview.aspx");

        } else {
            oMASSIT_VehicleTypeBE.Action = "CheckExistance";

            List<MASSIT_VehicleTypeBE> lstVehicleTypes = oMASSIT_VehicleTypeBAL.GetVehicleTypeDetailsBAL(oMASSIT_VehicleTypeBE);
            if (lstVehicleTypes != null && lstVehicleTypes.Count > 0) {
                string errorMessage = WebCommon.getGlobalResourceValue("VehicleTypeExists");
                if (rdoVolumeBased.Checked) {
                    txtTimebasedUnloadTime.Text = string.Empty;
                } else if (rdoTimeBased.Checked) {
                    txtVolumeBasedPallets.Text = string.Empty;
                    txtVolumeBasedCartons.Text = string.Empty;
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
            } else {
                oMASSIT_VehicleTypeBE.Action = "Add";
                if (rdoVolumeBased.Checked == true) {
                    oMASSIT_VehicleTypeBE.APP_PalletsUnloadedPerHour = Convert.ToInt32(txtVolumeBasedPallets.Text);
                    oMASSIT_VehicleTypeBE.APP_CartonsUnloadedPerHour = Convert.ToInt32(txtVolumeBasedCartons.Text);
                    oMASSIT_VehicleTypeBE.TotalUnloadTimeInMinute = 0;

                    String[] arr = txtMinTimePerDelivery.Text.Split(':');
                    oMASSIT_VehicleTypeBE.MinimumTimePerDeliveryInMinute = Convert.ToInt32(arr[0]) * 60 + Convert.ToInt32(arr[1]);

                } else if (rdoTimeBased.Checked == true) {
                    oMASSIT_VehicleTypeBE.APP_PalletsUnloadedPerHour = 0;
                    oMASSIT_VehicleTypeBE.APP_CartonsUnloadedPerHour = 0;
                    String[] arr = txtTimebasedUnloadTime.Text.Split(':');
                    oMASSIT_VehicleTypeBE.TotalUnloadTimeInMinute = Convert.ToInt32(arr[0]) * 60 + Convert.ToInt32(arr[1]);
                }
                oMASSIT_VehicleTypeBAL.addEditVehicleTypeDetailsBAL(oMASSIT_VehicleTypeBE);
                EncryptQueryString("APPSIT_VehicleTypeSetupOverview.aspx");
            }
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e) {
        MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
        APPSIT_VehicleTypeBAL oMASSIT_VehicleTypeBAL = new APPSIT_VehicleTypeBAL();

        oMASSIT_VehicleTypeBE.Action = "Edit";

        if (GetQueryStringValue("VehicleTypeID") != null) {
            oMASSIT_VehicleTypeBE.VehicleTypeID = Convert.ToInt32(GetQueryStringValue("VehicleTypeID"));

            oMASSIT_VehicleTypeBAL.addEditVehicleTypeDetailsBAL(oMASSIT_VehicleTypeBE);
        }

        EncryptQueryString("APPSIT_VehicleTypeSetupOverview.aspx");
    }
    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("APPSIT_VehicleTypeSetupOverview.aspx");
    }

    #endregion
}


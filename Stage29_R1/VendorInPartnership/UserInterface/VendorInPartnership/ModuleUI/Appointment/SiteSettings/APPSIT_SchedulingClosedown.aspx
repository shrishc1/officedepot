﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_SchedulingClosedown.aspx.cs" Inherits="APPSIT_SchedulingClosedown" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucAddButton" TagPrefix="uc" Src="~/CommonUI/UserControls/ucAddButton.ascx" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblSiteClosedownforScheduling" runat="server"></cc1:ucLabel>
    </h2>
    <table width="100%">
        <tr>
            <td>
                <div class="button-row">
                    <uc:ucAddButton ID="btnAdd" runat="server" NavigateUrl="APPSIT_SchedulingClosedownEdit.aspx" />
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <cc1:ucGridView ID="gvSchedulingClosedown" Width="100%" runat="server" CssClass="grid"
                    >
                    <Columns>
                        <asp:TemplateField HeaderText="Site" >
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hlSite" runat="server" Text='<%#Eval("SiteName")%>' NavigateUrl='<%# EncryptQuery("APPSIT_SchedulingClosedownEdit.aspx?SchedulingClosedownID=" + Eval("SchedulingClosedownID")) %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Message" >
                            <HeaderStyle Width="55%" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <asp:Literal ID="lblWarningMessage" runat="server" Text='<%#Eval("WarningMessage") %>'></asp:Literal>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CreateBy" >
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblCreateBy" runat="server" Text='<%#Eval("CreateBy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CreateOn" >
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblCreateDate" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>

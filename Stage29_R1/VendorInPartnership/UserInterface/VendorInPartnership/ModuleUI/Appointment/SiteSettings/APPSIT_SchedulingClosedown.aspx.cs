﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class APPSIT_SchedulingClosedown : CommonPage
{
    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            BindSchedulingClosedown();
        }
    }

    private void BindSchedulingClosedown() {
        MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE = new MASSIT_SchedulingClosedownBE();
        MASSIT_SchedulingClosedownBAL oMASSIT_SchedulingClosedownBAL = new MASSIT_SchedulingClosedownBAL();

        oMASSIT_SchedulingClosedownBE.Action = "ShowAll";
        
        List<MASSIT_SchedulingClosedownBE> lstSchedulingClosedownBE = oMASSIT_SchedulingClosedownBAL.GetSchedulingClosedownBAL(oMASSIT_SchedulingClosedownBE);

        if (lstSchedulingClosedownBE != null && lstSchedulingClosedownBE.Count > 0) {
            gvSchedulingClosedown.DataSource = lstSchedulingClosedownBE;
            gvSchedulingClosedown.DataBind();
        }
    }
}
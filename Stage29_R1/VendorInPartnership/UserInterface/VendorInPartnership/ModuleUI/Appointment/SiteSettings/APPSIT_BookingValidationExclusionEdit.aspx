﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_BookingValidationExclusionEdit.aspx.cs" Inherits="APPSIT_BookingValidationExclusionEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhnSelectVendor.ascx" TagName="ucSeacrhnSelectVendor"
    TagPrefix="cc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="up" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblBookingValidationExclusion" runat="server" Text="Booking Validation Exclusion"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <table width="30%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="width: 20%">
                            </td>
                            <td style="font-weight: bold; width: 20%">
                                <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 5%">
                                :
                            </td>
                            <td style="font-weight: bold; width: 35%">
                                <cc2:ucSite ID="ddlSite" runat="server" />
                            </td>
                            <td style="width: 20%">
                            </td>
                        </tr>
                    </table>
                    <asp:UpdatePanel ID="up1" runat="server">
                        <ContentTemplate>
                            <cc1:ucPanel ID="pnlVendor" runat="server" CssClass="fieldset-form">
                                <table width="90%" align="left" cellspacing="5" cellpadding="0" border="0">
                                    <tr>
                                        <td>
                                            <cc4:ucSeacrhnSelectVendor ID="ucSeacrhVendor1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="up2" runat="server">
                        <ContentTemplate>
                            <cc1:ucPanel ID="pnlCarrier" runat="server" CssClass="fieldset-form">
                                <table width="100%" align="left" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                                    <tr>
                                        <td style="font-weight: bold;" width="10%">
                                            <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" width="1px">
                                            :
                                        </td>
                                        <td width="30%">
                                            <cc1:ucListBox ID="UclstCarrier" runat="server" Height="200px" Width="320px">
                                            </cc1:ucListBox>
                                        </td>
                                        <td valign="middle" align="center" width="10%">
                                            <div>
                                                <cc1:ucButton ID="btnMoveRightAll" runat="server" Text=">>" CssClass="button" Width="35px"
                                                    OnClick="btnMoveRightAll_Click" /></div>
                                            &nbsp;
                                            <div>
                                                <cc1:ucButton ID="btnMoveRight" runat="server" Text=">" CssClass="button" Width="35px"
                                                    OnClick="btnMoveRight_Click" /></div>
                                            &nbsp;
                                            <div>
                                                <cc1:ucButton ID="btnMoveLeft" runat="server" Text="<" CssClass="button" Width="35px"
                                                    OnClick="btnMoveLeft_Click" /></div>
                                            &nbsp;
                                            <div>
                                                <cc1:ucButton ID="btnMoveLeftAll" runat="server" Text="<<" CssClass="button" Width="35px"
                                                    OnClick="btnMoveLeftAll_Click" /></div>
                                        </td>
                                        <td width="49%">
                                            <cc1:ucListBox ID="UclstRightCarrier" runat="server" Height="200px" Width="320px">
                                            </cc1:ucListBox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnMoveRightAll" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnMoveRight" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnMoveLeft" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnMoveLeftAll" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="up3" runat="server">
                        <ContentTemplate>
                            <cc1:ucPanel ID="pnlDeliveryType" runat="server" CssClass="fieldset-form">
                                <table width="100%" align="left" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                                    <tr>
                                        <td style="font-weight: bold;" width="10%">
                                            <cc1:ucLabel ID="lblDeliveryType" runat="server" Text="Delivery Type"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" width="1%">
                                            :
                                        </td>
                                        <td width="30%">
                                            <cc1:ucListBox ID="UclstDeliveryType" runat="server" Height="200px" Width="320px">
                                            </cc1:ucListBox>
                                        </td>
                                        <td valign="middle" align="center" width="10%">
                                            <div>
                                                <cc1:ucButton ID="btnMoveRightAll_1" runat="server" Text=">>" CssClass="button" Width="35px"
                                                    OnClick="btnMoveRightAll_1_Click" /></div>
                                            &nbsp;
                                            <div>
                                                <cc1:ucButton ID="btnMoveRight_1" runat="server" Text=">" CssClass="button" Width="35px"
                                                    OnClick="btnMoveRight_1_Click" /></div>
                                            &nbsp;
                                            <div>
                                                <cc1:ucButton ID="btnMoveLeft_1" runat="server" Text="<" CssClass="button" Width="35px"
                                                    OnClick="btnMoveLeft_1_Click" /></div>
                                            &nbsp;
                                            <div>
                                                <cc1:ucButton ID="btnMoveLeftAll_1" runat="server" Text="<<" CssClass="button" Width="35px"
                                                    OnClick="btnMoveLeftAll_1_Click" /></div>
                                        </td>
                                        <td width="49%">
                                            <cc1:ucListBox ID="UclstRightDeliveryType" runat="server" Height="200px" Width="320px">
                                            </cc1:ucListBox>
                                        </td>
                                    </tr>
                                </table>
                            </cc1:ucPanel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnMoveRightAll_1" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnMoveRight_1" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnMoveLeft_1" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnMoveLeftAll_1" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
                <%--<cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" />--%>
                <%--<cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" />--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_VendorValidationExclusionEdit.aspx.cs" Inherits="APPSIT_VendorValidationExclusionEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblVendorValidationExclusion" runat="server" Text="Vendor Validation Exclusion"></cc1:ucLabel>
    </h2>
    <script language="javascript" type="text/javascript">
        function showHideDiv() {
            var divstyle = new String();
            divstyle = document.getElementById("divVendor").style.visibility;

            if (divstyle.toLowerCase() == "visible") {
                document.getElementById("divVendor").style.visibility = "hidden";
            }
            else {
                document.getElementById("divVendor").style.visibility = "visible";
            }
            return false;
        }
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
    </script>
    <div class="right-shadow">
        <div class="formbox">
                <table width="30%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 40%">
                            <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 5%">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 55%">
                            <cc1:ucDropdownList ID="ddlSite" runat="server" Width="150px">
                            </cc1:ucDropdownList>                       
                    </tr>
                </table>
            <cc1:ucPanel ID="pnlVendor" runat="server" CssClass="fieldset-form">
                <table width="75%" align="left" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                    <tr>
                        <td style="font-weight: bold;" width="100px">
                            <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="10px">
                            <cc1:ucLabel ID="Label3" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucTextbox ID="txtVendorNo" runat="server" Width="33px"></cc1:ucTextbox>
                            &nbsp;
                            <input type="image" src="../../../Images/Search.png" style="height: 16px;" onclick="return showHideDiv();" />
                            &nbsp;&nbsp;&nbsp; xxx xxx
                        </td>
                        <td valign="middle" align="center">
                            <div>
                                <cc1:ucButton ID="btnSelect" runat="server" Text="Select" CssClass="button" Width="70px" /></div>
                            &nbsp;
                            <div>
                                <cc1:ucButton ID="btnRemove" runat="server" Text="Remove" CssClass="button" Width="70px" /></div>
                        </td>
                        <td>
                            <cc1:ucListBox ID="lstSelectedVendor" runat="server" Height="200px">
                            </cc1:ucListBox>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlCarrier" runat="server" CssClass="fieldset-form">
                <table width="75%" align="left" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                    <tr>
                        <td style="font-weight: bold;" width="100px">
                            <cc1:ucLabel ID="lblCarrier" runat="server" Text="Carrier"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="10px">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                        <td width="160px">
                            <cc1:ucListBox ID="UcListBox1" runat="server" Height="200px" Width="320px">
                            </cc1:ucListBox>
                        </td>
                        <td valign="middle" align="center" width="60px">
                            <div>
                                <cc1:ucButton ID="btnMoveRightAll" runat="server" Text=">>" CssClass="button" Width="35px" /></div>
                            &nbsp;
                            <div>
                                <cc1:ucButton ID="btnMoveRight" runat="server" Text=">" CssClass="button" Width="35px" /></div>
                            &nbsp;
                            <div>
                                <cc1:ucButton ID="btnMoveLeft" runat="server" Text="<" CssClass="button" Width="35px" /></div>
                            &nbsp;
                            <div>
                                <cc1:ucButton ID="btnMoveLeftAll" runat="server" Text="<<" CssClass="button" Width="35px" /></div>
                        </td>
                        <td width="160px">
                            <cc1:ucListBox ID="UclstCarrier" runat="server" Height="200px" Width="320px">
                            </cc1:ucListBox>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlDeliveryType" runat="server" CssClass="fieldset-form">
                <table width="75%" align="left" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                    <tr>
                        <td style="font-weight: bold;" width="100px">
                            <cc1:ucLabel ID="lblDeliveryType" runat="server" Text="Delivery Type"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="10px">
                            <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                        </td>
                        <td width="160px">
                            <cc1:ucListBox ID="UclstDeliveryType" runat="server" Height="200px" Width="320px">
                            </cc1:ucListBox>
                        </td>
                        <td valign="middle" align="center" width="60px">
                            <div>
                                <cc1:ucButton ID="btnMoveRightAll_1" runat="server" Text=">>" CssClass="button" Width="35px" /></div>
                            &nbsp;
                            <div>
                                <cc1:ucButton ID="btnMoveRight_1" runat="server" Text=">" CssClass="button" Width="35px" /></div>
                            &nbsp;
                            <div>
                                <cc1:ucButton ID="btnMoveLeft_1" runat="server" Text="<" CssClass="button" Width="35px" /></div>
                            &nbsp;
                            <div>
                                <cc1:ucButton ID="btnMoveLeftAll_1" runat="server" Text="<<" CssClass="button" Width="35px" /></div>
                        </td>
                        <td width="160px">
                            <cc1:ucListBox ID="UclstSelectedDelivery" runat="server" Height="200px" Width="320px">
                            </cc1:ucListBox>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
    <div id="divVendor" style="visibility: hidden; border-style: dotted; border-color: inherit;
        border-width: 1px; position: absolute; left: 300px; top: 375px; width: 400px;">
        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
            <tr>
                <td style="font-weight: bold;" nowarp>
                    Vendor Name
                </td>
                <td style="font-weight: bold;">
                    <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                </td>
                <td style="font-weight: bold;">
                    <cc1:ucTextbox ID="txtVenderName2" runat="server" Width="147px"></cc1:ucTextbox>
                </td>
                <td>
                    <div class="button-row">
                        <cc1:ucButton ID="btnSearch" runat="server" Width="60px" Text="Search" CssClass="button" />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
                <td style="font-weight: bold;" align="left">
                    <asp:ListBox ID="lstVendor" runat="server" Width="150px">
                        <asp:ListItem>Vendor1</asp:ListItem>
                        <asp:ListItem>Vendor2</asp:ListItem>
                        <asp:ListItem>Vendor3</asp:ListItem>
                    </asp:ListBox>
                </td>
            </tr>
        </table>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClientClick="return confirmDelete();" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" />
    </div>
</asp:Content>

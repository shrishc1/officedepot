﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.AdminFunctions;

public partial class APPSIT_SkuDoorMatrixSetupOverview : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var URL = string.Empty;
        if (ddlSite.innerControlddlSite.SelectedItem != null)
            URL = "APPSIT_SkuDoorMatrixSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value;
        btnAdd.NavigateUrl = URL;
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            ddlSite.innerControlddlSite.AutoPostBack = true;
        }

    }

    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        btnExportToExcel1.GridViewControl = UcGridView1;
        btnExportToExcel1.FileName = "SkuDoorMatrixSetupOverview";
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["SelectedSiteID"] != null && !IsPostBack)
        {
            ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Session["SelectedSiteID"].ToString()));
        }
        if (!IsPostBack)
        {
            BindSiteSkuDoorMatrixSetup();
        }
    }

    #region Methods

    protected void BindSiteSkuDoorMatrixSetup()
    {
        MASSIT_ODSKUDoorMatrixSetupBE oMASSIT_ODSKUDoorMatrixSetupBE = new MASSIT_ODSKUDoorMatrixSetupBE();

        MASSIT_ODSKUDoorMatrixSetupBAL oMASSIT_ODSKUDoorMatrixSetupBAL = new MASSIT_ODSKUDoorMatrixSetupBAL();

        oMASSIT_ODSKUDoorMatrixSetupBE.Action = "SHOW_ALL";

        MAS_SiteBE Site = new MAS_SiteBE();
        if (ddlSite.innerControlddlSite != null && ddlSite.innerControlddlSite.SelectedItem != null)
        {
            Site.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_ODSKUDoorMatrixSetupBE.Site = Site;
        }


        List<MASSIT_ODSKUDoorMatrixSetupBE> lstSkuDoorMatrixSetup = oMASSIT_ODSKUDoorMatrixSetupBAL.GetOdSkuDoorMatrixSetupDetailsBAL(oMASSIT_ODSKUDoorMatrixSetupBE);


        UcGridView1.DataSource = lstSkuDoorMatrixSetup;
        UcGridView1.DataBind();
        ViewState["lstSites"] = lstSkuDoorMatrixSetup;

    }

    public override void SiteSelectedIndexChanged()
    {
        Session["SelectedSiteID"] = ddlSite.innerControlddlSite.SelectedItem.Value;
        BindSiteSkuDoorMatrixSetup();
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASSIT_ODSKUDoorMatrixSetupBE>.SortList((List<MASSIT_ODSKUDoorMatrixSetupBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    #endregion
}
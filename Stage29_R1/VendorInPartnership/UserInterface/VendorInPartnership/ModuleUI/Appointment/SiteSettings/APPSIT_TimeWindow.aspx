﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_TimeWindow.aspx.cs" Inherits="APPSIT_TimeWindow" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
    <h2>
        <cc1:ucLabel ID="lblTimeWindow" runat="server" Text="Time Window"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnDoorID" runat="server" Value="," />
    <asp:HiddenField ID="hdnDoorIDEndDay" runat="server" Value="," />
    <asp:HiddenField ID="hdnWeekDay" runat="server" />
    <asp:HiddenField ID="hdnStartWeekDay" runat="server" />
    <asp:HiddenField ID="hdnEndWeekDay" runat="server" />
    <asp:HiddenField ID="hdnAllDoorOpen" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <table width="65%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td style="font-weight: bold;">
                        <uc2:ucSite ID="ddlSite" runat="server" />
                    </td>
                    <td colspan="3" align="right">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="6">
                        <div>
                            <cc1:ucButton ID="btnMonday" runat="server" Text="Monday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Mon" />
                            <cc1:ucButton ID="btnTuesday" runat="server" Text="Tuesday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Tue" />
                            <cc1:ucButton ID="btnWednesday" runat="server" Text="Wednesday" CssClass="button"
                                Width="80px" OnClick="btnWeekday_Click" CommandArgument="Wed" />
                            <cc1:ucButton ID="btnThursday" runat="server" Text="Thursday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Thu" />
                            <cc1:ucButton ID="btnFriday" runat="server" Text="Friday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Fri" />
                            <cc1:ucButton ID="btnSaturday" runat="server" Text="Saturday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Sat" />
                            <cc1:ucButton ID="btnSunday" runat="server" Text="Sunday" CssClass="button" Width="80px"
                                OnClick="btnWeekday_Click" CommandArgument="Sun" />
                        </div>
                    </td>
                </tr>
            </table>
            <cc1:ucPanel ID="pnlTimeWindowtop" runat="server" GroupingText="Time Window" CssClass="fieldset-form">
                <asp:Panel ID="pnlHeaderWindow" runat="server" Style="width: 98.2%; overflow: auto;">
                </asp:Panel>
                <asp:Panel ID="pnlTimeWindow" runat="server" Style="width: 100%; height: 455px; overflow: auto">
                </asp:Panel>
                <div class="bottom-shadow">
                </div>
                <div class="button-row">
                    <asp:Panel ID="pnlbuttons" runat="server" Style="width: 98.2%; overflow: auto">
                    </asp:Panel>
                </div>
            </cc1:ucPanel>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#ctl00_ContentPlaceHolder1_pnlTimeWindow').find('table td').css('max-width', '93px');
            $('#ctl00_ContentPlaceHolder1_pnlTimeWindow').find('table td a[href*="APPSIT_TimeWindowEdit.aspx"]').css({'max-width': '80px', 'display': 'block', 'word-break': 'break-word'});
           
        });        
    </script>
</asp:Content>

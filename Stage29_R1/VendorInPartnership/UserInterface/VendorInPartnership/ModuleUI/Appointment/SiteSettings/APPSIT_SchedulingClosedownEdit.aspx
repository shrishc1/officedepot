﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_SchedulingClosedownEdit.aspx.cs" Inherits="APPSIT_SchedulingClosedownEdit" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">    
    <h2>
        <cc1:ucLabel ID="lblSiteClosedownforScheduling" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <table width="99%" cellspacing="5" cellpadding="5" border="0" align="center" class="top-settingsNoBorder">
                <tr>
                    <td style="font-weight: bold; width: 4%" align="left">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 95%">
                        <uc2:ucSite ID="ddlSite" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <cc1:ucLabel ID="lblMessage" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" valign="top">
                        :
                    </td>
                    <td style="font-weight: bold;">
                        <CKEditor:CKEditorControl ID="txtWarning" Width="830px" Height="300px" runat="server"  ></CKEditor:CKEditorControl>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <cc1:ucButton ID="btnOpenSite" runat="server" Text="Open Site" CssClass="button"
            OnClick="btnOpenSite_Click" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
    <asp:UpdatePanel ID="updOpenSite" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnOpenSites" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdOpenSite" runat="server" TargetControlID="btnOpenSites"
                PopupControlID="pnlbtnOpenSite" BackgroundCssClass="modalBackground" BehaviorID="MsgOpenSite"
                DropShadow="false" />
            <asp:Panel ID="pnlbtnOpenSite" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3>
                        <cc1:ucLabel ID="btnWarningInfo" runat="server" Text="INFORMATION"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <asp:Literal ID="lblShowMessage" runat="server"></asp:Literal></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <cc1:ucButton ID="btnConfirm" CommandName="OpenSite" runat="server" OnCommand="btnConfirm_Click"
                                        CssClass="button" />
                                    &nbsp;
                                    <cc1:ucButton ID="btnCancel" CommandName="OpenSite" runat="server" OnCommand="btnCancel_Click"
                                        CssClass="button" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnConfirm" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

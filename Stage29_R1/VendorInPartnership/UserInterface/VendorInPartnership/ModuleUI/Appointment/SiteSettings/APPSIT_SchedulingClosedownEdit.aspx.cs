﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class APPSIT_SchedulingClosedownEdit : CommonPage
{
    protected void Page_InIt(object sender, EventArgs e) {
        ddlSite.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Page.IsPostBack) {
        //    if (GetQueryStringValue("SchedulingClosedownID") != null) {
        //        BindSchedulingClosedown(GetQueryStringValue("SchedulingClosedownID"));
        //    }
        //    else {
        //        btnOpenSite.Visible = false;                
        //    }
        //}
    }

    protected void Page_PreRender(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
            if (GetQueryStringValue("SchedulingClosedownID") == null) {
                BindSchedulingClosedown();
                btnOpenSite.Visible = false; 
            }
            else {
                BindSchedulingClosedown(GetQueryStringValue("SchedulingClosedownID"));
            }
        }
    }

    private void BindSchedulingClosedown() {
        MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE = new MASSIT_SchedulingClosedownBE();
        MASSIT_SchedulingClosedownBAL oMASSIT_SchedulingClosedownBAL = new MASSIT_SchedulingClosedownBAL();

        oMASSIT_SchedulingClosedownBE.Action = "ShowAll";

        List<MASSIT_SchedulingClosedownBE> lstSchedulingClosedownBE = oMASSIT_SchedulingClosedownBAL.GetSchedulingClosedownBAL(oMASSIT_SchedulingClosedownBE);

        if (lstSchedulingClosedownBE != null && lstSchedulingClosedownBE.Count > 0) {
            for (int icount = 0; icount < lstSchedulingClosedownBE.Count; icount++) {
                if (ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(lstSchedulingClosedownBE[icount].SiteID.ToString())) >= 0)
                    ddlSite.innerControlddlSite.Items.RemoveAt(ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(lstSchedulingClosedownBE[icount].SiteID.ToString())));
            }
        }
    }

    
    private void BindSchedulingClosedown(string SchedulingClosedownID) {
        MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE = new MASSIT_SchedulingClosedownBE();
        MASSIT_SchedulingClosedownBAL oMASSIT_SchedulingClosedownBAL = new MASSIT_SchedulingClosedownBAL();

        oMASSIT_SchedulingClosedownBE.Action = "ShowAll";
        oMASSIT_SchedulingClosedownBE.SchedulingClosedownID = Convert.ToInt32(SchedulingClosedownID);

        List<MASSIT_SchedulingClosedownBE> lstSchedulingClosedownBE = oMASSIT_SchedulingClosedownBAL.GetSchedulingClosedownBAL(oMASSIT_SchedulingClosedownBE);

        if (lstSchedulingClosedownBE != null && lstSchedulingClosedownBE.Count > 0) {
            ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Convert.ToString(lstSchedulingClosedownBE[0].SiteID)));
            txtWarning.Text = lstSchedulingClosedownBE[0].WarningMessage;
            ddlSite.innerControlddlSite.Enabled = false;
        }
    }

     #region Events

    protected void btnSave_Click(object sender, EventArgs e) {
        MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE = new MASSIT_SchedulingClosedownBE();
        MASSIT_SchedulingClosedownBAL oMASSIT_SchedulingClosedownBAL = new MASSIT_SchedulingClosedownBAL();

        if (GetQueryStringValue("SchedulingClosedownID") == null) {
            lblShowMessage.Text = WebCommon.getGlobalResourceValue("SiteClosedownMessage");
            mdOpenSite.Show();
        }
        else {
            oMASSIT_SchedulingClosedownBE.Action = "Edit";
            oMASSIT_SchedulingClosedownBE.SchedulingClosedownID = Convert.ToInt32(GetQueryStringValue("SchedulingClosedownID"));


            oMASSIT_SchedulingClosedownBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_SchedulingClosedownBE.WarningMessage = txtWarning.Text.Trim();
            oMASSIT_SchedulingClosedownBE.CreateById = Convert.ToInt32(Session["UserID"]);

            oMASSIT_SchedulingClosedownBAL.AddEditSchedulingClosedownBAL(oMASSIT_SchedulingClosedownBE);

            EncryptQueryString("APPSIT_SchedulingClosedown.aspx");
        }
    }

    protected void btnOpenSite_Click(object sender, EventArgs e) {
        MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE = new MASSIT_SchedulingClosedownBE();
        MASSIT_SchedulingClosedownBAL oMASSIT_SchedulingClosedownBAL = new MASSIT_SchedulingClosedownBAL();

        if (GetQueryStringValue("SchedulingClosedownID") != null) {
            lblShowMessage.Text = WebCommon.getGlobalResourceValue("SiteOpenMessage");
            mdOpenSite.Show();            
        }      
    }

    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("APPSIT_SchedulingClosedown.aspx");
    }

    protected void btnConfirm_Click(object sender, EventArgs e) {

        MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE = new MASSIT_SchedulingClosedownBE();
        MASSIT_SchedulingClosedownBAL oMASSIT_SchedulingClosedownBAL = new MASSIT_SchedulingClosedownBAL();

        if (GetQueryStringValue("SchedulingClosedownID") == null) {
            oMASSIT_SchedulingClosedownBE.Action = "Add";
            oMASSIT_SchedulingClosedownBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_SchedulingClosedownBE.WarningMessage = txtWarning.Text.Trim();
            oMASSIT_SchedulingClosedownBE.CreateById = Convert.ToInt32(Session["UserID"]);
            oMASSIT_SchedulingClosedownBAL.AddEditSchedulingClosedownBAL(oMASSIT_SchedulingClosedownBE);
        }
        else {
            oMASSIT_SchedulingClosedownBE.Action = "Delete";
            oMASSIT_SchedulingClosedownBE.SchedulingClosedownID = Convert.ToInt32(GetQueryStringValue("SchedulingClosedownID"));
            oMASSIT_SchedulingClosedownBAL.AddEditSchedulingClosedownBAL(oMASSIT_SchedulingClosedownBE);
        }

        EncryptQueryString("APPSIT_SchedulingClosedown.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e) {

        mdOpenSite.Hide();
    }
    
     #endregion
}
﻿using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;

public partial class APPSIT_VendorMicellaneousConstraintsEdit : CommonPage
{
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    string strExistVendorMiscConst = WebCommon.getGlobalResourceValue("ExistVendorMiscConst");
    string strCheckBeforeAfterTime = WebCommon.getGlobalResourceValue("CheckBeforeAfterTime");
    string strCorrectValueValidation = WebCommon.getGlobalResourceValue("CorrectValueValidation");
    string strValidVendorNo = WebCommon.getGlobalResourceValue("ValidVendorNo");


    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ucSeacrhVendor1.CurrentPage = this;
        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = true;

    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlSite.innerControlddlSite.AutoPostBack = true;
            BindTimes();
            BindVendorMicellaneousConstraints();

        }

        if (ddlSite.innerControlddlSite.SelectedIndex >= 0)
        {

            ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetVendorNo", "<script>setVendorName();</script>", false);
    }
    public void BindTimes()
    {
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstTimes = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);

        if (lstTimes != null && lstTimes.Count > 0)
        {
            FillControls.FillDropDown(ref ddlSlotAfterTime, lstTimes, "SlotTime", "SlotTimeID");
            ddlSlotAfterTime.Items.Insert(0, new ListItem("Select", "-1"));

            // remove 00:00 from the list for before time
            var v = from lst in lstTimes where (lst.SlotTimeID == 1) select lst;
            if (v != null && v.ToList()[0] != null)
                lstTimes.Remove(v.ToList()[0]);

            FillControls.FillDropDown(ref ddlSlotBeforeTime, lstTimes, "SlotTime", "SlotTimeID");
            ddlSlotBeforeTime.Items.Insert(0, new ListItem("Select", "-1"));
        }
    }
    private void BindVendorMicellaneousConstraints()
    {
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        APPSIT_VendorBAL oMASSIT_VendorBAL = new APPSIT_VendorBAL();

        if (GetQueryStringValue("SiteVendorID") != null)
        {

            ltvendorName.Visible = true;
            oMASSIT_VendorBE.Action = "ShowAll";
            oMASSIT_VendorBE.SiteVendorID = Convert.ToInt32(GetQueryStringValue("SiteVendorID"));
            oMASSIT_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);

            List<MASSIT_VendorBE> lstVendor = oMASSIT_VendorBAL.GetVendorConstraintsBAL(oMASSIT_VendorBE);

            if (lstVendor != null)
            {
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(lstVendor[0].SiteID.ToString()));
                ltvendorName.Text = lstVendor[0].Vendor.VendorName;
                hdnSelectedVendorID.Value = lstVendor[0].VendorID.ToString();
                chkVenderCheckingRestriction.Checked = Convert.ToBoolean(lstVendor[0].APP_CheckingRequired);
                txtMaximumNumberDeliveriesPerDay.Text = lstVendor[0].APP_MaximumDeliveriesInADay.ToString();
                txtMaximumNumberofLines.Text = lstVendor[0].APP_MaximumLines.ToString();
                txtMaximumNumberofPallets.Text = lstVendor[0].APP_MaximumPallets.ToString();

                chkFriday1.Checked = Convert.ToBoolean(lstVendor[0].APP_CannotDeliveryOnFriday);
                chkMonday1.Checked = Convert.ToBoolean(lstVendor[0].APP_CannotDeliveryOnMonday);
                chkSaturday1.Checked = Convert.ToBoolean(lstVendor[0].APP_CannotDeliveryOnSaturday);
                chkSunday1.Checked = Convert.ToBoolean(lstVendor[0].APP_CannotDeliveryOnSunday);
                chkThrusday1.Checked = Convert.ToBoolean(lstVendor[0].APP_CannotDeliveryOnThrusday);
                chkTuesday1.Checked = Convert.ToBoolean(lstVendor[0].APP_CannotDeliveryOnTuesday);
                chkWednesday1.Checked = Convert.ToBoolean(lstVendor[0].APP_CannotDeliveryOnWednessday);

                if (lstVendor[0].BeforeSlotTimeID != null && lstVendor[0].BeforeSlotTimeID.ToString() != "")
                    ddlSlotBeforeTime.SelectedIndex = ddlSlotBeforeTime.Items.IndexOf(ddlSlotBeforeTime.Items.FindByValue(lstVendor[0].BeforeSlotTimeID.ToString()));
                if (lstVendor[0].AfterSlotTimeID != null && lstVendor[0].AfterSlotTimeID.ToString() != "")
                    ddlSlotAfterTime.SelectedIndex = ddlSlotAfterTime.Items.IndexOf(ddlSlotAfterTime.Items.FindByValue(lstVendor[0].AfterSlotTimeID.ToString()));

            }

            ddlSite.innerControlddlSite.Enabled = false;
            ucSeacrhVendor1.Visible = false;



        }
        else
        {
            btnDelete.Visible = false;
        }

    }

    private bool IsMiscConstVendorExistBAL(int vendorID, int SiteID)
    {
        if (GetQueryStringValue("SiteVendorID") == null)
        {
            MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
            APPSIT_VendorBAL oMASSIT_VendorBAL = new APPSIT_VendorBAL();

            oMASSIT_VendorBE.Action = "IsMiscConstVendorExist";
            oMASSIT_VendorBE.VendorID = vendorID;
            oMASSIT_VendorBE.SiteID = SiteID;
            oMASSIT_VendorBE.IsConstraintsDefined = true;
            if (oMASSIT_VendorBAL.IsMiscConstVendorExistBAL(oMASSIT_VendorBE) == 0) { return true; }
            else { return false; }
        }
        else
        {
            return true;
        }
    }



    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (this.Page.IsValid)
        {
            if (!string.IsNullOrWhiteSpace(ucSeacrhVendor1.VendorNo) || GetQueryStringValue("SiteVendorID") != null)
            {
                int intVendorId = 0;
                if (!string.IsNullOrWhiteSpace(ucSeacrhVendor1.VendorNo))
                {
                    intVendorId = Convert.ToInt32(ucSeacrhVendor1.VendorNo);
                }
                else
                {
                    intVendorId = Convert.ToInt32(hdnSelectedVendorID.Value);
                }

                if (txtMaximumNumberDeliveriesPerDay.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtMaximumNumberDeliveriesPerDay.Text.Trim().Replace(".", string.Empty)))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
                    txtMaximumNumberDeliveriesPerDay.Focus();
                    return;
                }

                if (txtMaximumNumberofLines.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtMaximumNumberofLines.Text.Trim().Replace(".", string.Empty)))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
                    txtMaximumNumberofLines.Focus();
                    return;
                }

                if (txtMaximumNumberofPallets.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtMaximumNumberofPallets.Text.Trim().Replace(".", string.Empty)))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
                    txtMaximumNumberofPallets.Focus();
                    return;
                }

                if (this.IsMiscConstVendorExistBAL(intVendorId, Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value)))
                {
                    MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
                    APPSIT_VendorBAL oMASSIT_VendorBAL = new APPSIT_VendorBAL();

                    DateTime? dtAfter = (DateTime?)null;
                    DateTime? dtBefore = (DateTime?)null;

                    // if (txtAfter.Text.Trim() != string.Empty)
                    string[] afterTime = ddlSlotAfterTime.SelectedItem.Text.Split(':');
                    if (afterTime.Length > 1)
                        dtAfter = new DateTime(1900, 1, 1, Convert.ToInt32(afterTime[0]), Convert.ToInt32(afterTime[1]), 0);

                    string[] beforeTime = ddlSlotBeforeTime.SelectedItem.Text.Split(':');
                    if (beforeTime.Length > 1)
                        dtBefore = new DateTime(1900, 1, 1, Convert.ToInt32(beforeTime[0]), Convert.ToInt32(beforeTime[1]), 0);

                    #region Validating the Before - After time
                    DateTime dtmBefore = DateTime.Now;
                    DateTime dtmAftre = DateTime.Now;
                    bool blnDateStatus = false;
                    if (ddlSlotBeforeTime.SelectedIndex > 0 && ddlSlotAfterTime.SelectedIndex > 0)
                    {
                        dtmBefore = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlSlotBeforeTime.SelectedItem));
                        dtmAftre = Convert.ToDateTime(string.Format("2013-01-01 {0}:00", ddlSlotAfterTime.SelectedItem));
                        blnDateStatus = true;
                    }
                    if (blnDateStatus == true && (dtmBefore > dtmAftre || dtmBefore == dtmAftre))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCheckBeforeAfterTime + "');</script>");
                        return;
                    }
                    #endregion

                    List<UP_VendorBE> oUP_VendorBEList = null;
                    if (GetQueryStringValue("SiteVendorID") == null)
                    {
                        ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                        TextBox uctxtVendorNo = (TextBox)ucSeacrhVendor1.FindControl("txtVendorNo");
                        ucLabel ucSelectedVendorName = (ucLabel)ucSeacrhVendor1.FindControl("SelectedVendorName");

                        if (!string.IsNullOrEmpty(ucSelectedVendorName.Text))
                            oUP_VendorBEList = ucSeacrhVendor1.IsValidVendorCode();

                        if (oUP_VendorBEList == null)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strValidVendorNo + "');</script>");
                            return;
                        }
                        else if (oUP_VendorBEList.Count == 0)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strValidVendorNo + "');</script>");
                            return;
                        }
                        else
                        {
                            hdnSelectedVendorID.Value = oUP_VendorBEList[0].VendorID.ToString();
                        }
                        oMASSIT_VendorBE.Action = "Add";
                    }


                    string errorDay, errorBeforeTime, errorAfterTime, errorMaxDeliveriesDay = string.Empty;
                    int? errorMaxPallates, errorMaxLines;
                    if (checkFixedSlotDetails(hdnSelectedVendorID.Value, out errorDay, out errorBeforeTime, out errorAfterTime, out errorMaxPallates, out errorMaxLines, out errorMaxDeliveriesDay))
                    {
                        if (GetQueryStringValue("SiteVendorID") != null)
                        {
                            oMASSIT_VendorBE.SiteVendorID = Convert.ToInt32(GetQueryStringValue("SiteVendorID").ToString());
                            oMASSIT_VendorBE.Action = "Edit";
                        }

                        oMASSIT_VendorBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                        oMASSIT_VendorBE.VendorID = Convert.ToInt32(hdnSelectedVendorID.Value);

                        oMASSIT_VendorBE.APP_CheckingRequired = chkVenderCheckingRestriction.Checked;
                        oMASSIT_VendorBE.APP_CannotDeliverAfter = dtAfter;
                        oMASSIT_VendorBE.APP_CannotDeliverBefore = dtBefore;

                        if (ddlSlotAfterTime.SelectedItem != null && ddlSlotAfterTime.SelectedItem.Value != "-1")
                            oMASSIT_VendorBE.AfterSlotTimeID = Convert.ToInt32(ddlSlotAfterTime.SelectedItem.Value);
                        else
                            oMASSIT_VendorBE.AfterSlotTimeID = null;

                        if (ddlSlotBeforeTime.SelectedItem != null && ddlSlotBeforeTime.SelectedItem.Value != "-1")
                            oMASSIT_VendorBE.BeforeSlotTimeID = Convert.ToInt32(ddlSlotBeforeTime.SelectedItem.Value);
                        else
                            oMASSIT_VendorBE.BeforeSlotTimeID = null;

                        oMASSIT_VendorBE.APP_CannotDeliveryOnFriday = chkFriday1.Checked;
                        oMASSIT_VendorBE.APP_CannotDeliveryOnMonday = chkMonday1.Checked;
                        oMASSIT_VendorBE.APP_CannotDeliveryOnSaturday = chkSaturday1.Checked;
                        oMASSIT_VendorBE.APP_CannotDeliveryOnSunday = chkSunday1.Checked;
                        oMASSIT_VendorBE.APP_CannotDeliveryOnThrusday = chkThrusday1.Checked;
                        oMASSIT_VendorBE.APP_CannotDeliveryOnTuesday = chkTuesday1.Checked;
                        oMASSIT_VendorBE.APP_CannotDeliveryOnWednessday = chkWednesday1.Checked;
                        oMASSIT_VendorBE.APP_MaximumDeliveriesInADay = txtMaximumNumberDeliveriesPerDay.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaximumNumberDeliveriesPerDay.Text) : (int?)null;
                        oMASSIT_VendorBE.APP_MaximumLines = txtMaximumNumberofLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaximumNumberofLines.Text) : (int?)null;
                        oMASSIT_VendorBE.APP_MaximumPallets = txtMaximumNumberofPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaximumNumberofPallets.Text) : (int?)null;
                        oMASSIT_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                        oMASSIT_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);

                        int? intresult = oMASSIT_VendorBAL.addEditCarrierDetailsBAL(oMASSIT_VendorBE);

                        if (intresult == -1)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert3", "alert('" + strExistVendorMiscConst + "')", true);
                            return;
                        }

                        EncryptQueryString("APPSIT_VendorMicellaneousConstraintsOverview.aspx");
                    }
                    else
                    {
                        string errorMessage = string.Empty;
                        if (!string.IsNullOrEmpty(errorDay))
                            errorMessage = "Fixed Slot Details not matched with Day Constraint for day - " + errorDay;
                        else if (!string.IsNullOrEmpty(errorBeforeTime))
                            errorMessage = "Fixed Slot Details not matched with before time constraint";
                        else if (!string.IsNullOrEmpty(errorAfterTime))
                            errorMessage = "Fixed Slot Details not matched with after time constraint";
                        else if (!string.IsNullOrEmpty(errorMaxDeliveriesDay))
                            errorMessage = "Fixed Slot Details not matched with Maximum Number Deliveries per day constraint ";
                        else if (errorMaxPallates != null && errorMaxPallates > 0)
                            errorMessage = "Fixed Slot Details not matched with Maximum Pallets constraint ";
                        else if (errorMaxLines != null && errorMaxLines > 0)
                            errorMessage = "Fixed Slot Details not matched with Maximum Lines constraint ";

                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + errorMessage + "');</script>");
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert3", "alert('" + strExistVendorMiscConst + "')", true);
                }
            }
            else
            {
                string errorMeesage = WebCommon.getGlobalResourceValue("VendorRequired");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert3", "alert('" + errorMeesage + "')", true);
            }
        }
    }
    private bool checkFixedSlotDetails(string vendorID, out string errorDay, out string errorBeforeTime, out string errorAfterTime, out int? errorMaxPallates, out int? errorMaxLines, out string errorMaxDeliveriesDay)
    {
        bool retValue = true;
        errorDay = errorBeforeTime = errorAfterTime = errorMaxDeliveriesDay = "";
        errorMaxPallates = errorMaxLines = null;
        int? errorMaxDeliveries;

        MAS_SiteBE oSite = GetSingleSiteSetting(Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value));
        if (oSite.TimeSlotWindow == "W")
            return retValue;

        List<MASSIT_FixedSlotBE> lstFixedSlot = getFixedSlotDetails(vendorID);

        if (lstFixedSlot != null && lstFixedSlot.Count > 0)
        {
            for (int i = 0; i < lstFixedSlot.Count; i++)
            {


                if (chkMonday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Monday)) { retValue = false; errorDay = "Monday"; break; }
                else if (chkTuesday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Tuesday)) { retValue = false; errorDay = "Tuesday"; break; }
                else if (chkWednesday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Wednesday)) { retValue = false; errorDay = "Wednesday"; break; }
                else if (chkThrusday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Thursday)) { retValue = false; errorDay = "Thursday"; break; }
                else if (chkFriday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Friday)) { retValue = false; errorDay = "Friday"; break; }
                else if (chkSaturday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Saturday)) { retValue = false; errorDay = "Saturday"; break; }
                else if (chkSunday1.Checked && Convert.ToBoolean(lstFixedSlot[i].Sunday)) { retValue = false; errorDay = "Sunday"; break; }

                /*This condition commented due to Anthony 26 Sep email, File Name-[DEV Retests 250913] and Sheet No-[6]*/
                //if (lstFixedSlot[i].SlotTime != null && !string.IsNullOrEmpty(lstFixedSlot[i].SlotTime.SlotTime))
                //    if ((ddlSlotBeforeTime.SelectedItem.Value != "-1") && (Convert.ToDateTime(lstFixedSlot[i].SlotTime.SlotTime) < Convert.ToDateTime(Convert.ToDateTime(lstFixedSlot[i].SlotTime.SlotTime).ToShortDateString() + " " + ddlSlotBeforeTime.SelectedItem.Text))) { retValue = false; errorBeforeTime = ddlSlotBeforeTime.SelectedItem.Text; break; }


                //if (lstFixedSlot[i].SlotTime != null && !string.IsNullOrEmpty(lstFixedSlot[i].SlotTime.SlotTime))
                //    if ((ddlSlotAfterTime.SelectedItem.Value != "-1") && (Convert.ToDateTime(lstFixedSlot[i].SlotTime.SlotTime) > Convert.ToDateTime(Convert.ToDateTime(lstFixedSlot[i].SlotTime.SlotTime).ToShortDateString() + " " + ddlSlotAfterTime.SelectedItem.Text))) { retValue = false; errorAfterTime = ddlSlotAfterTime.SelectedItem.Text; break; }
                /*----------------------------------------------------------------------*/

                //if (lstFixedSlot[i] != null && lstFixedSlot[i].MaximumPallets != null && !string.IsNullOrEmpty(txtMaximumNumberofPallets.Text))
                if (lstFixedSlot[i] != null && lstFixedSlot[i].MaximumPallets != null && lstFixedSlot[i].MaximumPallets != 0 && !string.IsNullOrEmpty(txtMaximumNumberofPallets.Text) && Convert.ToInt16(txtMaximumNumberofPallets.Text) > 0)
                {
                    if (lstFixedSlot.FindAll(delegate (MASSIT_FixedSlotBE St) { return St.MaximumPallets == (int?)null || St.MaximumPallets.Value > Convert.ToInt16(txtMaximumNumberofPallets.Text); }).Count > 1) { retValue = false; errorMaxPallates = Convert.ToInt16(txtMaximumNumberofPallets.Text); break; }
                    //if (lstFixedSlot[i].MaximumPallets.Value < Convert.ToInt16(txtMaximumNumberofPallets.Text)) { retValue = false; errorMaxPallates = lstFixedSlot[i].MaximumPallets.Value; break; }
                }
                //if (lstFixedSlot[i] != null && lstFixedSlot[i].MaximumLines != null && !string.IsNullOrEmpty(txtMaximumNumberofLines.Text))
                if (lstFixedSlot[i] != null && lstFixedSlot[i].MaximumLines != null && lstFixedSlot[i].MaximumLines != 0 && !string.IsNullOrEmpty(txtMaximumNumberofLines.Text) && Convert.ToInt16(txtMaximumNumberofLines.Text) > 0)
                {
                    if (lstFixedSlot.FindAll(delegate (MASSIT_FixedSlotBE St) { return St.MaximumLines == (int?)null || St.MaximumLines.Value > Convert.ToInt16(txtMaximumNumberofLines.Text); }).Count > 1) { retValue = false; errorMaxLines = Convert.ToInt16(txtMaximumNumberofLines.Text); break; }
                    // if (lstFixedSlot[i].MaximumLines < Convert.ToInt16(txtMaximumNumberofLines.Text)) { retValue = false; errorMaxLines = lstFixedSlot[i].MaximumLines.Value; break; }
                }
                if (lstFixedSlot[i] != null && !string.IsNullOrEmpty(txtMaximumNumberDeliveriesPerDay.Text))
                {
                    if (Convert.ToInt32(txtMaximumNumberDeliveriesPerDay.Text) > 0)
                    {
                        errorMaxDeliveries = Convert.ToInt32(txtMaximumNumberDeliveriesPerDay.Text);
                        if (lstFixedSlot.FindAll(delegate (MASSIT_FixedSlotBE St) { return St.Monday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Monday"; break; }
                        else if (lstFixedSlot.FindAll(delegate (MASSIT_FixedSlotBE St) { return St.Tuesday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Tuesday"; break; }
                        else if (lstFixedSlot.FindAll(delegate (MASSIT_FixedSlotBE St) { return St.Wednesday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Wednesday"; break; }
                        else if (lstFixedSlot.FindAll(delegate (MASSIT_FixedSlotBE St) { return St.Thursday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Thursday"; break; }
                        else if (lstFixedSlot.FindAll(delegate (MASSIT_FixedSlotBE St) { return St.Friday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Friday"; break; }
                        else if (lstFixedSlot.FindAll(delegate (MASSIT_FixedSlotBE St) { return St.Saturday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Saturday"; break; }
                        else if (lstFixedSlot.FindAll(delegate (MASSIT_FixedSlotBE St) { return St.Sunday == true; }).Count > errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Sunday"; break; }
                    }
                }
            }
        }
        return retValue;
    }

    private MAS_SiteBE GetSingleSiteSetting(int siteId)
    {
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        oMAS_SiteBE.Action = "GetSingleSiteSetting";
        oMAS_SiteBE.SiteID = siteId;
        MAS_SiteBE localMAS_SiteBE = oMAS_SiteBAL.GetSingleSiteSettingBAL(oMAS_SiteBE);
        return localMAS_SiteBE;
    }

    private List<MASSIT_FixedSlotBE> getFixedSlotDetails(string vendorID)
    {

        List<MASSIT_FixedSlotBE> lstFixedSlot = null;
        if (!string.IsNullOrEmpty(vendorID))
        {
            MASSIT_FixedSlotBE oMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();
            APPSIT_FixedSlotBAL oMASSIT_FixedSlotBAL = new APPSIT_FixedSlotBAL();

            oMASSIT_FixedSlotBE.FixedSlotID = Convert.ToInt32(GetQueryStringValue("FixedSlotID"));
            oMASSIT_FixedSlotBE.Action = "GetFixedSlotDetails";
            oMASSIT_FixedSlotBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_FixedSlotBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_FixedSlotBE.VendorID = Convert.ToInt32(vendorID);
            oMASSIT_FixedSlotBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            lstFixedSlot = oMASSIT_FixedSlotBAL.GetAllFixedSlotByVendorIDBAL(oMASSIT_FixedSlotBE);
        }
        return lstFixedSlot;
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPSIT_VendorMicellaneousConstraintsOverview.aspx");
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("SiteVendorID") != null)
        {
            MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
            APPSIT_VendorBAL oMASSIT_VendorBAL = new APPSIT_VendorBAL();
            oMASSIT_VendorBE.Action = "Delete";
            oMASSIT_VendorBE.SiteVendorID = Convert.ToInt32(GetQueryStringValue("SiteVendorID").ToString());
            oMASSIT_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_VendorBAL.addEditCarrierDetailsBAL(oMASSIT_VendorBE);

        }
        EncryptQueryString("APPSIT_VendorMicellaneousConstraintsOverview.aspx");
    }

    public override void SiteSelectedIndexChanged()
    {
        base.SiteSelectedIndexChanged();
        if (ddlSite.innerControlddlSite.SelectedIndex >= 0)
        {
            ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            TextBox uctxtVendorNo = (TextBox)ucSeacrhVendor1.FindControl("txtVendorNo");
            ucLabel ucSelectedVendorName = (ucLabel)ucSeacrhVendor1.FindControl("SelectedVendorName");
            HiddenField hdnVendorName = (HiddenField)ucSeacrhVendor1.FindControl("hdnVendorName");
            if (uctxtVendorNo != null)
            {
                hdnVendorName.Value = string.Empty;
                uctxtVendorNo.Text = String.Empty;
                ucSelectedVendorName.Text = string.Empty;
            }
        }
    }
}
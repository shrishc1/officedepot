﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class VehicleDoorMappingsSetupOverview : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
         
            ddlSite.innerControlddlSite.AutoPostBack = true;
            
        }
       
    }
    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        btnExportToExcel1.GridViewControl = UcGridView1;
        btnExportToExcel1.FileName = "VehicleDoorMatrixOverview";
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["SelectedSiteID"] != null && !IsPostBack)
        {
            ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(Session["SelectedSiteID"].ToString()));
        }
        if (!IsPostBack) {
            GetVehicleDoorMatrix();
        }
    }
    #region Methods

    protected void GetVehicleDoorMatrix()
    {
        MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();
        APPSIT_VehicleDoorMatrixBAL oMASSIT_VehicleDoorMatrixBAL = new APPSIT_VehicleDoorMatrixBAL();
        oMASSIT_VehicleDoorMatrixBE.Action = "ShowAll";
        oMASSIT_VehicleDoorMatrixBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_VehicleDoorMatrixBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        if (ddlSite.innerControlddlSite != null && ddlSite.innerControlddlSite.SelectedItem != null)
            oMASSIT_VehicleDoorMatrixBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        List<MASSIT_VehicleDoorMatrixBE> lstVehicleDoorMatrix = oMASSIT_VehicleDoorMatrixBAL.GetVehicleDoorMatrixDetailsBAL(oMASSIT_VehicleDoorMatrixBE);
        if (lstVehicleDoorMatrix != null)
        {
            UcGridView1.DataSource = lstVehicleDoorMatrix;
            UcGridView1.DataBind();
            ViewState["lstSites"] = lstVehicleDoorMatrix;
        }

    }
    public override void SiteSelectedIndexChanged()
    {
        Session["SelectedSiteID"] = ddlSite.innerControlddlSite.SelectedItem.Value;
        GetVehicleDoorMatrix();
    }


    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASSIT_VehicleDoorMatrixBE>.SortList((List<MASSIT_VehicleDoorMatrixBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
    #endregion
}
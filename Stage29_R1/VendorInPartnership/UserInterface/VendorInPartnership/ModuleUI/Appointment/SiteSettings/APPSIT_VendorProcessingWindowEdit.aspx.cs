﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.Collections.Generic;

using Utilities; using WebUtilities;
using BaseControlLibrary;


public partial class APPSIT_VendorProcessingWindowEdit : CommonPage {
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected string CorrectValueValidation = WebCommon.getGlobalResourceValue("CorrectValueValidation");
    string strExistVendorProcessing = WebCommon.getGlobalResourceValue("ExistVendorMiscConst");

    protected void Page_InIt(object sender, EventArgs e) {
        ucSite.CurrentPage = this;
        ucSeacrhVendor1.CurrentPage = this;
        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = true;
        
    }
    protected void Page_Load(object sender, EventArgs e) {
           
    }
    
    #region Methods
    
    protected void GetVendor() {
        MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
        APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();

        if (GetQueryStringValue("SiteVendorID") != null) {

            ltvendorName.Visible = true;
            oMASSIT_VendorProcessingWindowBE.Action = "ShowById";
            oMASSIT_VendorProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_VendorProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_VendorProcessingWindowBE.SiteVendorID = Convert.ToInt32(GetQueryStringValue("SiteVendorID"));
            
            MASSIT_VendorProcessingWindowBE lstVendor = new MASSIT_VendorProcessingWindowBE();
            lstVendor = oMASSIT_VendorProcessingWindowBAL.GetVendorDetailsByIdBAL(oMASSIT_VendorProcessingWindowBE);

            if (lstVendor != null) {
                ucSite.innerControlddlSite.SelectedIndex = ucSite.innerControlddlSite.Items.IndexOf(ucSite.innerControlddlSite.Items.FindByValue(lstVendor.SiteID.ToString()));
                ltvendorName.Text = lstVendor.Vendor.VendorName;
                hdnVendorID.Value= lstVendor.VendorID.ToString();

                hidPalletsUnloadTime.Value = lblPalletsUnloadTime.Text = String.Format("{0:N2}", lstVendor.APP_PalletsUnloadedPerHour/60);
                hidCartonsUnloadTime.Value = lblCartonsUnloadTime.Text = String.Format("{0:N2}", lstVendor.APP_CartonsUnloadedPerHour/60);
                hidLineReceiptingTime.Value = lblLineReceiptingLineTime.Text = String.Format("{0:N2}", lstVendor.APP_ReceiptingLinesPerHour/60);

                txtUploadPallet.Text = Convert.ToString(lstVendor.APP_PalletsUnloadedPerHour);
                txtUploadCarton.Text = Convert.ToString(lstVendor.APP_CartonsUnloadedPerHour);
                txtReceiptTime.Text = Convert.ToString(lstVendor.APP_ReceiptingLinesPerHour); 
            
            }

            ucSite.innerControlddlSite.Enabled = false;
            ucSeacrhVendor1.Visible = false;
       }
       else {
           btnDelete.Visible = false;
       }   

        
    }

    //public static String ConvertMinutesToHours(int mins) {
    //    int hours = (mins - mins % 60) / 60;
    //    int min = mins - hours * 60;
    //    if (hours < 10 && min < 10)

    //        return "0" + hours + ":" + "0" + min;

    //    else if (hours < 10 && min > 10)
    //        return "0" + hours + ":" + min;

    //    else if (hours > 10 && min < 10)

    //        return "" + hours + ":" + "0" + min;

    //    else
    //        return "" + hours + ":" + min;

    //}

    //public int ConvertHHMMtoMin(string Min) {
    //    //string[] splitString = Min.Split(':');
    //    //return ((Convert.ToInt32(splitString[0]) * 60) + Convert.ToInt32(splitString[1]));
    //    return Convert.ToInt32(lblAverageUnloadingTimePerPallet.Text);

    //}

    #endregion  

    #region Events


    public override void SitePost_Load() {
        base.SitePost_Load();
        if (!IsPostBack) {
            txtUploadPallet.Text = "";
            txtReceiptTime.Text = "";
            ucSite.innerControlddlSite.SelectedIndex = 0;
            ucSite.innerControlddlSite.AutoPostBack = true;
            ucSeacrhVendor1.SiteID = !string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : 0;
            GetVendor();

        }
    }
    public override void SiteSelectedIndexChanged()
    {
        ucSeacrhVendor1.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
    }

    private bool IsMiscConstVendorExistBAL(int vendorID, int SiteID)
    {
        if (GetQueryStringValue("SiteVendorID") == null) {
            MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
            APPSIT_VendorBAL oMASSIT_VendorBAL = new APPSIT_VendorBAL();

            oMASSIT_VendorBE.Action = "IsMiscConstVendorExist";
            oMASSIT_VendorBE.VendorID = vendorID;
            oMASSIT_VendorBE.SiteID = SiteID;
            oMASSIT_VendorBE.IsProcessingDefined = true;
            if (oMASSIT_VendorBAL.IsMiscConstVendorExistBAL(oMASSIT_VendorBE) == 0) { return true; }
            else { return false; }
        }
        else {
            return true;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e) {
        if (this.Page.IsValid) {
            if (!string.IsNullOrWhiteSpace(ucSeacrhVendor1.VendorNo) || !string.IsNullOrEmpty(hdnVendorID.Value))
            {
                int intVendorId = 0;
                if (!string.IsNullOrWhiteSpace(ucSeacrhVendor1.VendorNo))
                {
                    intVendorId = Convert.ToInt32(ucSeacrhVendor1.VendorNo);
                }
                else
                {
                    intVendorId = Convert.ToInt32(hdnVendorID.Value);
                }
                if (this.IsMiscConstVendorExistBAL(intVendorId, Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value)))
                {
                    MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
                    APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();
                    btnSave.Visible = false;
                    if (GetQueryStringValue("SiteVendorID") == null)
                    {
                        List<UP_VendorBE> oUP_VendorBEList = ucSeacrhVendor1.IsValidVendorCode();

                        if (oUP_VendorBEList.Count == 0)
                        {
                            //Page.RegisterStartupScript("alert","<script>alert('Please enter valid Vendor No.');</script>"); 
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('Please enter valid Vendor No.');</script>");
                            return;
                        }
                        else
                        {
                            hdnVendorID.Value = oUP_VendorBEList[0].VendorID.ToString();
                        }
                    }
                    oMASSIT_VendorProcessingWindowBE.Action = "Add";
                    oMASSIT_VendorProcessingWindowBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
                    oMASSIT_VendorProcessingWindowBE.VendorID = Convert.ToInt32(hdnVendorID.Value);
                    if (txtUploadPallet.Text.IsNumeric())
                        oMASSIT_VendorProcessingWindowBE.APP_PalletsUnloadedPerHour = Convert.ToInt32(txtUploadPallet.Text); // ConvertHHMMtoMin(txtUploadPallet.Text);
                    if (txtUploadCarton.Text.IsNumeric())
                        oMASSIT_VendorProcessingWindowBE.APP_CartonsUnloadedPerHour = Convert.ToInt32(txtUploadCarton.Text);// ConvertHHMMtoMin(txtUploadCarton.Text);
                    if (txtReceiptTime.Text.IsNumeric())
                        oMASSIT_VendorProcessingWindowBE.APP_ReceiptingLinesPerHour = Convert.ToInt32(txtReceiptTime.Text);// ConvertHHMMtoMin(txtReceiptTime.Text);

                    oMASSIT_VendorProcessingWindowBAL.addEditVendorDetailsBAL(oMASSIT_VendorProcessingWindowBE);

                    EncryptQueryString("APPSIT_VendorProcessingWindowOverview.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert3", "alert('" + strExistVendorProcessing + "')", true);
                }
            }
        } else {
            string errorMeesage = WebCommon.getGlobalResourceValue("VendorRequired");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert3", "alert('" + errorMeesage + "')", true);
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e) {
        MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
        APPSIT_VendorProcessingWindowBAL oMASSIT_VendorProcessingWindowBAL = new APPSIT_VendorProcessingWindowBAL();


        oMASSIT_VendorProcessingWindowBE.Action = "Edit";

        if (GetQueryStringValue("SiteVendorID") != null) {
            oMASSIT_VendorProcessingWindowBE.SiteVendorID = Convert.ToInt32(GetQueryStringValue("SiteVendorID"));

            oMASSIT_VendorProcessingWindowBAL.addEditVendorDetailsBAL(oMASSIT_VendorProcessingWindowBE);
        }

        EncryptQueryString("APPSIT_VendorProcessingWindowOverview.aspx");
    }

    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("APPSIT_VendorProcessingWindowOverview.aspx");
    }
    protected void btnHid_Click(object sender, EventArgs e) {
       // GetVendor();
    }
    
    #endregion
    
}
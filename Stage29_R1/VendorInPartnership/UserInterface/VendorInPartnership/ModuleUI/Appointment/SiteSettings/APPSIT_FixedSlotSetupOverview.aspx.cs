﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.Collections.Generic;


public partial class APPSIT_FixedSlotSetupOverview : CommonPage {
    protected void Page_InIt(object sender, EventArgs e) {
        ddlSite.CurrentPage = this;
        ddlSite.TimeSlotWindow = "S";
    }

    public override void SitePost_Load() {
        base.SitePost_Load();
        if (!Page.IsPostBack) {
            ddlSite.innerControlddlSite.AutoPostBack = true;
            if(GetQueryStringValue("SiteID") != null)
                ddlSite.innerControlddlSite.SelectedIndex = ddlSite.innerControlddlSite.Items.IndexOf(ddlSite.innerControlddlSite.Items.FindByValue(GetQueryStringValue("SiteID")));
                
            Bindmaximum();
            BindGrid();
        }        
    }

   

    protected void Page_Load(object sender, EventArgs e) {

        if (Session["SiteID"] == null) {
            EncryptQueryString("../../Security/Login.aspx");
        }

        if (!IsPostBack) {
        }

        string URL = string.Empty;

        if (ddlSite.innerControlddlSite.SelectedItem != null)
            URL = "APPSIT_FixedSlotSetupEdit.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value;
        else
            URL = "APPSIT_FixedSlotSetupEdit.aspx?SiteID=" + Session["SiteID"].ToString();
        btnAdd.NavigateUrl = URL;

        ucExportToExcel1.GridViewControl = grdFixedSlot;
        ucExportToExcel1.FileName = "FixedSlotSetupOverview";
    }

    #region Methods
    private void Bindmaximum() {

        //Get Week setup maximums
        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        oMASSIT_WeekSetupBE.Action = "ShowAll";
        oMASSIT_WeekSetupBE.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : (int?)null; // 11;

        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);

        for (int iCount = 0; iCount < lstWeekSetup.Count; iCount++) {
            if (lstWeekSetup[iCount].EndWeekday.ToUpper() == "MON") {
                ltpalletMon.Text = lstWeekSetup[iCount].MaximumPallet.ToString();
                ltLinesMon.Text = lstWeekSetup[iCount].MaximumLine.ToString();
                //ltCartonsMon.Text = lstWeekSetup[iCount].MaximumContainer.ToString();

                
            }
            else if (lstWeekSetup[iCount].EndWeekday.ToUpper() == "TUE") {
                ltpalletTue.Text = lstWeekSetup[iCount].MaximumPallet.ToString();
                ltLinesTue.Text = lstWeekSetup[iCount].MaximumLine.ToString();
                //ltCartonsTue.Text = lstWeekSetup[iCount].MaximumContainer.ToString();


            }
            else if (lstWeekSetup[iCount].EndWeekday.ToUpper() == "WED") {
                ltpalletWed.Text = lstWeekSetup[iCount].MaximumPallet.ToString();
                ltLinesWed.Text = lstWeekSetup[iCount].MaximumLine.ToString();
                //ltCartonsWed.Text = lstWeekSetup[iCount].MaximumContainer.ToString();


            }
            else if (lstWeekSetup[iCount].EndWeekday.ToUpper() == "THU") {
                ltpalletThurs.Text = lstWeekSetup[iCount].MaximumPallet.ToString();
                ltLinesThurs.Text = lstWeekSetup[iCount].MaximumLine.ToString();
                //ltCartonsThurs.Text = lstWeekSetup[iCount].MaximumContainer.ToString();


            }
            else if (lstWeekSetup[iCount].EndWeekday.ToUpper() == "FRI") {
                ltpalletFri.Text = lstWeekSetup[iCount].MaximumPallet.ToString();
                ltLinesFri.Text = lstWeekSetup[iCount].MaximumLine.ToString();
                //ltCartonsFri.Text = lstWeekSetup[iCount].MaximumContainer.ToString();


            }
            else if (lstWeekSetup[iCount].EndWeekday.ToUpper() == "SAT") {
                ltpalletSat.Text = lstWeekSetup[iCount].MaximumPallet.ToString();
                ltLinesSat.Text = lstWeekSetup[iCount].MaximumLine.ToString();
                //ltCartonsSat.Text = lstWeekSetup[iCount].MaximumContainer.ToString();


            }
            else if (lstWeekSetup[iCount].EndWeekday.ToUpper() == "SUN") {
                ltpalletSun.Text = lstWeekSetup[iCount].MaximumPallet.ToString();
                ltLinesSun.Text = lstWeekSetup[iCount].MaximumLine.ToString();
                //ltCartonsSun.Text = lstWeekSetup[iCount].MaximumContainer.ToString();


            }
        }


        //get scheduled maximum
        MASSIT_FixedSlotBE oMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();
        APPSIT_FixedSlotBAL oMASSIT_FixedSlotBAL = new APPSIT_FixedSlotBAL();

        oMASSIT_FixedSlotBE.Action = "GetMaximumWeekWise"; // "GetMaximum";
        oMASSIT_FixedSlotBE.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : 0;

        List<MASSIT_FixedSlotBE> lstFixedSlot = new List<MASSIT_FixedSlotBE>();

        oMASSIT_FixedSlotBE.WeekDay = "Monday";
        oMASSIT_FixedSlotBE.AllocationType = "H";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);

        if (lstFixedSlot.Count > 0) {
            ltpalletMon_1.Text = lstFixedSlot[0].MaximumPallets.ToString() ;
            ltLinesMon_1.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsMon_1.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }

        oMASSIT_FixedSlotBE.WeekDay = "Tuesday";
        oMASSIT_FixedSlotBE.AllocationType = "H";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);
        if (lstFixedSlot.Count > 0) {
            ltpalletTue_1.Text = lstFixedSlot[0].MaximumPallets.ToString();
            ltLinesTue_1.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsTue_1.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }

        oMASSIT_FixedSlotBE.WeekDay = "Wednesday";
        oMASSIT_FixedSlotBE.AllocationType = "H";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);
        if (lstFixedSlot.Count > 0) {
            ltpalletWed_1.Text = lstFixedSlot[0].MaximumPallets.ToString();
            ltLinesWed_1.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsWed_1.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }

        oMASSIT_FixedSlotBE.WeekDay = "Thursday";
        oMASSIT_FixedSlotBE.AllocationType = "H";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);
        if (lstFixedSlot.Count > 0) {
            ltpalletThurs_1.Text = lstFixedSlot[0].MaximumPallets.ToString();
            ltLinesThurs_1.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsThurs_1.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }

        oMASSIT_FixedSlotBE.WeekDay = "Friday";
        oMASSIT_FixedSlotBE.AllocationType = "H";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);
        if (lstFixedSlot.Count > 0) {
            ltpalletFri_1.Text = lstFixedSlot[0].MaximumPallets.ToString();
            ltLinesFri_1.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsFri_1.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }

        oMASSIT_FixedSlotBE.WeekDay = "Saturday";
        oMASSIT_FixedSlotBE.AllocationType = "H";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);
        if (lstFixedSlot.Count > 0) {
            ltpalletSat_1.Text = lstFixedSlot[0].MaximumPallets.ToString();
            ltLinesSat_1.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsSat_1.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }

        oMASSIT_FixedSlotBE.WeekDay = "Sunday";
        oMASSIT_FixedSlotBE.AllocationType = "H";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);
        if (lstFixedSlot.Count > 0) {
            ltpalletSun_1.Text = lstFixedSlot[0].MaximumPallets.ToString();
            ltLinesSun_1.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsSun_1.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }


        oMASSIT_FixedSlotBE.WeekDay = "Monday";
        oMASSIT_FixedSlotBE.AllocationType = "S";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);

        if (lstFixedSlot.Count > 0)
        {
            ltpalletMon_2.Text = lstFixedSlot[0].MaximumPallets.ToString();
            ltLinesMon_2.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsMon_2.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }

        oMASSIT_FixedSlotBE.WeekDay = "Tuesday";
        oMASSIT_FixedSlotBE.AllocationType = "S";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);
        if (lstFixedSlot.Count > 0)
        {
            ltpalletTue_2.Text = lstFixedSlot[0].MaximumPallets.ToString();
            ltLinesTue_2.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsTue_2.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }

        oMASSIT_FixedSlotBE.WeekDay = "Wednesday";
        oMASSIT_FixedSlotBE.AllocationType = "S";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);
        if (lstFixedSlot.Count > 0)
        {
            ltpalletWed_2.Text = lstFixedSlot[0].MaximumPallets.ToString();
            ltLinesWed_2.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsWed_2.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }

        oMASSIT_FixedSlotBE.WeekDay = "Thursday";
        oMASSIT_FixedSlotBE.AllocationType = "S";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);
        if (lstFixedSlot.Count > 0)
        {
            ltpalletThurs_2.Text = lstFixedSlot[0].MaximumPallets.ToString();
            ltLinesThurs_2.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsThurs_2.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }

        oMASSIT_FixedSlotBE.WeekDay = "Friday";
        oMASSIT_FixedSlotBE.AllocationType = "S";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);
        if (lstFixedSlot.Count > 0)
        {
            ltpalletFri_2.Text = lstFixedSlot[0].MaximumPallets.ToString();
            ltLinesFri_2.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsFri_2.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }

        oMASSIT_FixedSlotBE.WeekDay = "Saturday";
        oMASSIT_FixedSlotBE.AllocationType = "S";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);
        if (lstFixedSlot.Count > 0)
        {
            ltpalletSat_2.Text = lstFixedSlot[0].MaximumPallets.ToString();
            ltLinesSat_2.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsSat_2.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }

        oMASSIT_FixedSlotBE.WeekDay = "Sunday";
        oMASSIT_FixedSlotBE.AllocationType = "S";
        lstFixedSlot = oMASSIT_FixedSlotBAL.GetMaximumBAL(oMASSIT_FixedSlotBE);
        if (lstFixedSlot.Count > 0)
        {
            ltpalletSun_2.Text = lstFixedSlot[0].MaximumPallets.ToString();
            ltLinesSun_2.Text = lstFixedSlot[0].MaximumLines.ToString();
            ltCartonsSun_2.Text = lstFixedSlot[0].MaximumCatrons.ToString();
        }

        CheckOverload("Mon");
        CheckOverload("Tue");
        CheckOverload("Wed");
        CheckOverload("Thurs");
        CheckOverload("Fri");
        CheckOverload("Sat");
        CheckOverload("Sun");
    }


    private void CheckOverload(string WeekDay) {
        Literal ltpalletweekMax = (Literal)this.Master.FindControl("ContentPlaceHolder1").FindControl("ltpallet" + WeekDay);
        Literal ltpalletSchMax = (Literal)this.Master.FindControl("ContentPlaceHolder1").FindControl("ltpallet" + WeekDay + "_1");
        Literal ltpalletSchMax2 = (Literal)this.Master.FindControl("ContentPlaceHolder1").FindControl("ltpallet" + WeekDay + "_2");
        //if (ltpalletSchMax2.Text == string.Empty)
        //{
        //    ltpalletSchMax2.Text = "0";
        //}
        string PalletSchedule1 = string.Empty;
        string PalletSchedule2 = string.Empty;
        PalletSchedule1 = ltpalletSchMax.Text == string.Empty ? "0" : ltpalletSchMax.Text;
        PalletSchedule2 = ltpalletSchMax2.Text == string.Empty ? "0" : ltpalletSchMax2.Text;
        if (ltpalletweekMax.Text != string.Empty && ltpalletSchMax.Text != string.Empty) {
            if (Convert.ToInt32(PalletSchedule1) + Convert.ToInt32(PalletSchedule2) > Convert.ToInt32(ltpalletweekMax.Text))
            {
                ltpalletSchMax.Text = "<span style='color: red;'>" + ltpalletSchMax.Text + "</span>";
                ltpalletSchMax2.Text = "<span style='color: red;'>" + ltpalletSchMax2.Text + "</span>";
            }
        }

       
        Literal ltLinesweekMax = (Literal)this.Master.FindControl("ContentPlaceHolder1").FindControl("ltLines" + WeekDay);
        Literal ltLinesSchMax = (Literal)this.Master.FindControl("ContentPlaceHolder1").FindControl("ltLines" + WeekDay + "_1");
        Literal ltLinesSchMax2 = (Literal)this.Master.FindControl("ContentPlaceHolder1").FindControl("ltLines" + WeekDay + "_2");
        //if (ltLinesSchMax2.Text == string.Empty)
        //{
        //    ltLinesSchMax2.Text = "0";
        //}
        string lineSchedule2 = string.Empty;
        lineSchedule2 = ltLinesSchMax2.Text == string.Empty ? "0" : ltLinesSchMax2.Text;
        string lineSchedule1 = string.Empty;
        lineSchedule1 = ltLinesSchMax.Text == string.Empty ? "0" : ltLinesSchMax.Text;
        if (ltLinesweekMax.Text != string.Empty && ltLinesSchMax.Text != string.Empty) {
            if (Convert.ToInt32(lineSchedule1) + Convert.ToInt32(lineSchedule2) > Convert.ToInt32(ltLinesweekMax.Text))
            {
                ltLinesSchMax.Text = "<span style='color: red;'>" + ltLinesSchMax.Text + "</span>";
                ltLinesSchMax2.Text = "<span style='color: red;'>" + ltLinesSchMax2.Text + "</span>";
            }
          
        }        
        //Literal ltCartonsweekMax = (Literal)this.Master.FindControl("ContentPlaceHolder1").FindControl("ltCartons" + WeekDay);
        //Literal ltCartonsSchMax = (Literal)this.Master.FindControl("ContentPlaceHolder1").FindControl("ltCartons" + WeekDay + "_1");

        //if (ltCartonsweekMax.Text != string.Empty && ltCartonsSchMax.Text != string.Empty) {
        //    if (Convert.ToInt32(ltCartonsSchMax.Text) > Convert.ToInt32(ltCartonsweekMax.Text))
        //        ltCartonsSchMax.Text = "<span style='color: red;'>" + ltCartonsSchMax.Text + "</span>";
        //}
    }

    private void BindGrid() {
        MASSIT_FixedSlotBE oMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();
        APPSIT_FixedSlotBAL oMASSIT_FixedSlotBAL = new APPSIT_FixedSlotBAL();

        oMASSIT_FixedSlotBE.Action = "ShowAll";
        oMASSIT_FixedSlotBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_FixedSlotBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        oMASSIT_FixedSlotBE.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue) : 0;

        List<MASSIT_FixedSlotBE> lstFixedSlot = oMASSIT_FixedSlotBAL.GetAllFixedSlotBAL(oMASSIT_FixedSlotBE);

        if (lstFixedSlot.Count > 0) {
            grdFixedSlot.Visible = true;
            grdFixedSlot.DataSource = lstFixedSlot;
            grdFixedSlot.DataBind();
            ViewState["lstSites"] = lstFixedSlot;
        }
        else {
            grdFixedSlot.Visible = false;

        }


    }

    public override void SiteSelectedIndexChanged() {
        Bindmaximum();
        BindGrid();
    }
    #endregion

    protected void btnAddSchedule_Click(object sender, EventArgs e) {
        EncryptQueryString("APPSIT_FixedSlotSetupEdit.aspx");
    }
    protected void grdFixedSlot_RowDataBound(object sender, GridViewRowEventArgs e) {
        if (e.Row.RowType == DataControlRowType.DataRow) {
            if (e.Row.Cells[1].Text == "V")
                e.Row.Cells[1].Text = "Vendor";
            else if (e.Row.Cells[1].Text == "C")
                e.Row.Cells[1].Text = "Carrier";
        }
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASSIT_FixedSlotBE>.SortList((List<MASSIT_FixedSlotBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
    
}
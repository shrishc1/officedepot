﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
public partial class APPSIT_CarrierProcessingWindowOverview : CommonPage 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRegionCarrier();
        }
        ucExportToExcel1.GridViewControl = UcGridView1;
        ucExportToExcel1.FileName = "CarrierProcessingWindowOverview";
    }
    #region Methods

    protected void BindRegionCarrier() {
        MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
        APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();


        oMASSIT_CarrerProcessingWindowBE.Action = "ShowAll";
        oMASSIT_CarrerProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_CarrerProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MASSIT_CarrierProcessingWindowBE> lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetCarrierDetailsBAL(oMASSIT_CarrerProcessingWindowBE);

        if (lstCarrier.Count > 0) {
            UcGridView1.DataSource = lstCarrier;
            UcGridView1.DataBind();
            ViewState["lstSites"] = lstCarrier;
           
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASSIT_CarrierProcessingWindowBE>.SortList((List<MASSIT_CarrierProcessingWindowBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
    
    #endregion
}
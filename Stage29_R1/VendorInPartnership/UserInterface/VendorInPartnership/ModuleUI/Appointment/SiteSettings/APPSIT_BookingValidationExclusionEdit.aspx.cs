﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using System.Data;

using Utilities; using WebUtilities;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;



public partial class APPSIT_BookingValidationExclusionEdit : CommonPage {
    public ucListBox lstSelectedVendor;
    protected void Page_InIt(object sender, EventArgs e) {
        ddlSite.CurrentPage = this;   
       
        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = true;
    }
    protected void Page_Load(object sender, EventArgs e) {
        if (ddlSite.innerControlddlSite.SelectedIndex > 0) {
            ucSeacrhVendor1.SiteID =Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        }
        lstSelectedVendor = (ucListBox)ucSeacrhVendor1.FindControl("lstSelectedVendor");
    }

    #region Methods

   

    public void BindDeliveryTypes() {
        UclstDeliveryType.Items.Clear();
        UclstRightDeliveryType.Items.Clear();
        DataTable dt1;
        DataTable dt2;

        MASSIT_BookingTypeExclusionBE oMASCNT_BookingTypeExclusionBE = new MASSIT_BookingTypeExclusionBE();
        APPSIT_BookingTypeExclusionBAL oMASCNT_BookingTypeExclusionBAL = new APPSIT_BookingTypeExclusionBAL();

        oMASCNT_BookingTypeExclusionBE.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value) : 0;
        oMASCNT_BookingTypeExclusionBE.Action = "GetDeliveries";
        dt1 = oMASCNT_BookingTypeExclusionBAL.GetBookingTypeExclusionDetailsBAL(oMASCNT_BookingTypeExclusionBE,"");
        
        oMASCNT_BookingTypeExclusionBE.Action = "GetAllDeliveryTypes";
        dt2 = oMASCNT_BookingTypeExclusionBAL.GetBookingTypeExclusionDetailsBAL(oMASCNT_BookingTypeExclusionBE,"");

        if (dt1 != null && dt1.Rows.Count > 0) {           
            FillControls.FillListBox(ref UclstRightDeliveryType, dt1, "DeliveryType", "SiteDeliveryID");
        }
        if (dt2 != null && dt2.Rows.Count > 0) {            
            FillControls.FillListBox(ref UclstDeliveryType, dt2, "DeliveryType", "SiteDeliveryID");
            if (UclstRightDeliveryType.Items.Count > 0) {
                for (int i = 0; i < UclstRightDeliveryType.Items.Count; i++) {
                    if (UclstDeliveryType.Items.FindByValue(UclstRightDeliveryType.Items[i].Value) != null) {
                        UclstDeliveryType.Items.Remove(new ListItem(UclstRightDeliveryType.Items[i].Text, UclstRightDeliveryType.Items[i].Value));
                    }
                }
            }
        }             
    }

    public void BindCarriers() {
        UclstCarrier.Items.Clear();
        UclstRightCarrier.Items.Clear();
        DataTable dt1;
        DataTable dt2;

        MASSIT_BookingTypeExclusionBE oMASCNT_BookingTypeExclusionBE = new MASSIT_BookingTypeExclusionBE();
        APPSIT_BookingTypeExclusionBAL oMASCNT_BookingTypeExclusionBAL = new APPSIT_BookingTypeExclusionBAL();


        oMASCNT_BookingTypeExclusionBE.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value) : 0;
        oMASCNT_BookingTypeExclusionBE.Action = "GetCarriers";
        dt1 = oMASCNT_BookingTypeExclusionBAL.GetBookingTypeExclusionDetailsBAL(oMASCNT_BookingTypeExclusionBE,"");

        oMASCNT_BookingTypeExclusionBE.Action = "GetAllCarrierNames";
        dt2 = oMASCNT_BookingTypeExclusionBAL.GetBookingTypeExclusionDetailsBAL(oMASCNT_BookingTypeExclusionBE,"");
        if (dt1 != null && dt1.Rows.Count > 0) {
            FillControls.FillListBox(ref UclstRightCarrier, dt1, "CarrierName", "SiteCarrierID");
        }
        if (dt2 != null && dt2.Rows.Count > 0) {           
            FillControls.FillListBox(ref UclstCarrier, dt2, "CarrierName", "SiteCarrierID");
            if (UclstRightCarrier.Items.Count > 0) {
                for (int i = 0; i < UclstRightCarrier.Items.Count; i++) {
                    if (UclstCarrier.Items.FindByValue(UclstRightCarrier.Items[i].Value) != null) {
                        UclstCarrier.Items.Remove(new ListItem(UclstRightCarrier.Items[i].Text, UclstRightCarrier.Items[i].Value));
                    }
                }
            }
        }             
    }

    public void BindVendors() {
        lstSelectedVendor.Items.Clear();
        
        MASSIT_BookingTypeExclusionBE oMASCNT_BookingTypeExclusionBE = new MASSIT_BookingTypeExclusionBE();
        APPSIT_BookingTypeExclusionBAL oMASCNT_BookingTypeExclusionBAL = new APPSIT_BookingTypeExclusionBAL();
        DataTable dt;
        oMASCNT_BookingTypeExclusionBE.SiteID = !string.IsNullOrEmpty(ddlSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value) : 0;
        oMASCNT_BookingTypeExclusionBE.Action = "GetVendors";

        dt = oMASCNT_BookingTypeExclusionBAL.GetBookingTypeExclusionDetailsBAL(oMASCNT_BookingTypeExclusionBE,"");

        if (dt != null && dt.Rows.Count > 0) {
            FillControls.FillListBox(ref lstSelectedVendor, dt, "VendorName", "SiteVendorID");
        }
    }
    
    #endregion

    #region Events
    
    public override void SitePost_Load() {
        base.SitePost_Load();
        if (!IsPostBack) {
            ddlSite.innerControlddlSite.AutoPostBack = true;
            BindCarriers();
            BindDeliveryTypes();
            BindVendors();
        }
    }

    public override void SiteSelectedIndexChanged()
    {
        ucListBox lstVendor = (ucListBox)ucSeacrhVendor1.FindControl("lstVendor");
        if (lstVendor != null)
        {
            if (lstVendor.Items.Count > 0)
                lstVendor.Items.Clear();
        }
        ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        BindCarriers();
        BindDeliveryTypes();
        BindVendors();
    }

    protected void btnSave_Click(object sender, EventArgs e) {
        MASSIT_BookingTypeExclusionBE oMASCNT_BookingTypeExclusionBE = new MASSIT_BookingTypeExclusionBE();
        APPSIT_BookingTypeExclusionBAL oMASCNT_BookingTypeExclusionBAL = new APPSIT_BookingTypeExclusionBAL();
        oMASCNT_BookingTypeExclusionBE.Action = "Edit";
        oMASCNT_BookingTypeExclusionBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

        if (UclstRightCarrier.Items.Count > 0) {
            if (UclstRightCarrier.Items.Count > 1) {
                oMASCNT_BookingTypeExclusionBE.SiteCarrierIDs = UclstRightCarrier.Items[0].Value + ",";
                for (int i = 1; i < UclstRightCarrier.Items.Count - 1; i++) {
                    oMASCNT_BookingTypeExclusionBE.SiteCarrierIDs += UclstRightCarrier.Items[i].Value + ",";
                }
                oMASCNT_BookingTypeExclusionBE.SiteCarrierIDs += UclstRightCarrier.Items[UclstRightCarrier.Items.Count - 1].Value;
            }
            else {
                oMASCNT_BookingTypeExclusionBE.SiteCarrierIDs = UclstRightCarrier.Items[0].Value;
            }
        }
        if (UclstRightDeliveryType.Items.Count > 0) {
            if (UclstRightDeliveryType.Items.Count > 1) {
                oMASCNT_BookingTypeExclusionBE.SiteDeliveryIDs = UclstRightDeliveryType.Items[0].Value + ",";
                for (int i = 1; i < UclstRightDeliveryType.Items.Count - 1; i++) {
                    oMASCNT_BookingTypeExclusionBE.SiteDeliveryIDs += UclstRightDeliveryType.Items[i].Value + ",";
                }
                oMASCNT_BookingTypeExclusionBE.SiteDeliveryIDs += UclstRightDeliveryType.Items[UclstRightDeliveryType.Items.Count - 1].Value;
            }
            else {
                oMASCNT_BookingTypeExclusionBE.SiteDeliveryIDs = UclstRightDeliveryType.Items[0].Value;
            }
        }

        if (lstSelectedVendor.Items.Count > 0) {
            if (lstSelectedVendor.Items.Count > 1) {
                oMASCNT_BookingTypeExclusionBE.SiteVendorIDs = lstSelectedVendor.Items[0].Value + ",";
                for (int i = 1; i < lstSelectedVendor.Items.Count - 1; i++) {
                    oMASCNT_BookingTypeExclusionBE.SiteVendorIDs += lstSelectedVendor.Items[i].Value + ",";
                }
                oMASCNT_BookingTypeExclusionBE.SiteVendorIDs += lstSelectedVendor.Items[lstSelectedVendor.Items.Count - 1].Value;
            }
            else {
                oMASCNT_BookingTypeExclusionBE.SiteVendorIDs = lstSelectedVendor.Items[0].Value;
            }
        }

       int? iResult = oMASCNT_BookingTypeExclusionBAL.addEditBookingTypeExclusionDetailsBAL(oMASCNT_BookingTypeExclusionBE);
        
        BindCarriers();
        BindDeliveryTypes();
        BindVendors();
        if (iResult > 0) {
            string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
            ScriptManager.RegisterClientScriptBlock(this,this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
    }
    #endregion

    #region Carrier Button Events

    protected void btnMoveRightAll_Click(object sender, EventArgs e) {
        if (UclstCarrier.Items.Count > 0)
         FillControls.MoveAllItems(UclstCarrier, UclstRightCarrier);
    }
    protected void btnMoveRight_Click(object sender, EventArgs e) {
        if (UclstCarrier.SelectedItem != null) {
            FillControls.MoveOneItem(UclstCarrier, UclstRightCarrier);
        }
    }
    protected void btnMoveLeft_Click(object sender, EventArgs e) {
        if (UclstRightCarrier.SelectedItem != null) {
            FillControls.MoveOneItem(UclstRightCarrier, UclstCarrier);
        }
    }
    protected void btnMoveLeftAll_Click(object sender, EventArgs e) {
        if (UclstRightCarrier.Items.Count > 0) {
            FillControls.MoveAllItems(UclstRightCarrier, UclstCarrier);
        }
    }

    #endregion 

    #region Delivery Button Events

    protected void btnMoveRightAll_1_Click(object sender, EventArgs e) {
        if (UclstDeliveryType.Items.Count > 0)
            FillControls.MoveAllItems(UclstDeliveryType, UclstRightDeliveryType);
    }
    protected void btnMoveRight_1_Click(object sender, EventArgs e) {
        if (UclstDeliveryType.SelectedItem != null) {
            FillControls.MoveOneItem(UclstDeliveryType, UclstRightDeliveryType);
        }
    }
    protected void btnMoveLeft_1_Click(object sender, EventArgs e) {
        if (UclstRightDeliveryType.SelectedItem != null) {
            FillControls.MoveOneItem(UclstRightDeliveryType, UclstDeliveryType);
        }
    }
    protected void btnMoveLeftAll_1_Click(object sender, EventArgs e) {
        if (UclstRightDeliveryType.Items.Count > 0) {
            FillControls.MoveAllItems(UclstRightDeliveryType, UclstDeliveryType);
        }
    }    
    #endregion

}
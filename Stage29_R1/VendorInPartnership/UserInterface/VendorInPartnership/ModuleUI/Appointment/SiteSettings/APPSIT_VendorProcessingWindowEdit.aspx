﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" CodeFile="APPSIT_VendorProcessingWindowEdit.aspx.cs"
    Inherits="APPSIT_VendorProcessingWindowEdit" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblVendorProcessing" runat="server" Text="Vendor Processing Window"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnVendorID" runat="server" />
    <input id="hidPalletsUnloadTime" type="hidden" runat="server" value="" />
    <input id="hidCartonsUnloadTime" type="hidden" runat="server" value="" />
    <input id="hidLineReceiptingTime" type="hidden" runat="server" value="" />
    <script language="javascript" type="text/javascript">
        function showHideDiv() {            
            var divstyle = new String();
            divstyle = document.getElementById("divVendor").style.visibility;

            if (divstyle.toLowerCase() == "visible") {
                document.getElementById("divVendor").style.visibility = "hidden";
            }
            else {
                document.getElementById("divVendor").style.visibility = "visible";
            }
            return false;
        }

        function confirmDelete() {            
            return confirm('<%=deleteMessage%>');
        }

        function getMinutes(obj, arg) {              
            if (obj.value != '') {
                if (IsNumeric(obj.value)) {
                    if (parseInt(obj.value * 1) >= 0) {
                        if (arg == '1') {
                            document.getElementById('<%=lblPalletsUnloadTime.ClientID %>').innerText=document.getElementById('<%=lblPalletsUnloadTime.ClientID %>').textContent = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2); ;
                            document.getElementById('<%=hidPalletsUnloadTime.ClientID %>').value = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2);
                        }
                        else if (arg == '2') {
                            document.getElementById('<%=lblCartonsUnloadTime.ClientID %>').textContent = document.getElementById('<%=lblCartonsUnloadTime.ClientID %>').innerText = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2);
                            document.getElementById('<%=hidCartonsUnloadTime.ClientID %>').value = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2);
                        }
                        else if (arg == '3') {
                            document.getElementById('<%=lblLineReceiptingLineTime.ClientID %>').textContent = document.getElementById('<%=lblLineReceiptingLineTime.ClientID %>').innerText = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2);
                            document.getElementById('<%=hidLineReceiptingTime.ClientID %>').value = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2);
                        }
                    }
                    else {
                        alert("<%=CorrectValueValidation %>");
                        return false;
                    }
                }
                else {
                    alert("<%=CorrectValueValidation %>");
                    return false;
                }
            }
        }

        function SaveValidation() {
            if (getMinutes(document.getElementById('<%=txtUploadPallet.ClientID %>'), '1') == false) {
                return false;
            }

            if (getMinutes(document.getElementById('<%=txtUploadCarton.ClientID %>'), '2') == false) {
                return false;
            }

            if (getMinutes(document.getElementById('<%=txtReceiptTime.ClientID %>'), '3') == false) {
                return false;
            }

            return true;
        }

        $(document).ready(function () {
            getMinutes(document.getElementById('<%=txtUploadPallet.ClientID %>'), '1');
            getMinutes(document.getElementById('<%=txtUploadCarton.ClientID %>'), '2');
            getMinutes(document.getElementById('<%=txtReceiptTime.ClientID %>'), '3');
        });
    </script>
    <div class="right-shadow">
        <div>
            <asp:RequiredFieldValidator ID="rfvUploadPallets" runat="server" ControlToValidate="txtUploadPallet" Display="None" ValidationGroup="check"
                SetFocusOnError="true"> </asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvReceiptTime" runat="server" ControlToValidate="txtReceiptTime" Display="None" ValidationGroup="check"
                SetFocusOnError="true"> </asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvUploadCarton" runat="server" ControlToValidate="txtUploadCarton" Display="None" ValidationGroup="check"
                SetFocusOnError="true"> </asp:RequiredFieldValidator>
            <%--<asp:RegularExpressionValidator ID="revUploadTime" runat="server" ControlToValidate="txtUploadPallet" ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$"
                ValidationGroup="check" Display="None" SetFocusOnError="true"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="revReceiptTimeLine" runat="server" ControlToValidate="txtReceiptTime" ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$"
                ValidationGroup="check" Display="None" SetFocusOnError="true"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="revUploadCarton" runat="server" ControlToValidate="txtUploadCarton" ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$"
                ValidationGroup="check" Display="None" SetFocusOnError="true"></asp:RegularExpressionValidator>--%>
            <asp:ValidationSummary runat="server" ID="vsCheck" ShowMessageBox="true" ShowSummary="false" DisplayMode="BulletList" ValidationGroup="check" />
        </div>
        <div class="formbox">
            <table width="70%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 5%">
                    </td>
                    <td style="font-weight: bold; width: 35%">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5%">
                        :
                    </td>
                    <td style="font-weight: bold; width: 55%">
                        <cc2:ucSite ID="ucSite" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td style="font-weight: bold;">
                        <asp:Literal ID="ltvendorName" runat="server" Visible="false"></asp:Literal>
                        <uc1:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblAveragePalletsUnloadTime" runat="server" Text="Average Pallets unload time"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td style="font-weight: bold;">
                        <%--    <cc1:ucNumericTextbox ID="txtUploadPallet" onkeyup="AllowNumbersOnly(this);"
                                MaxLength="4" runat="server" Width="40px"></cc1:ucNumericTextbox>--%>
                        <cc1:ucTextbox ID="txtUploadPallet" onblur="javascript:return getMinutes(this,'1');" runat="server" Width="40px" ValidationGroup="check" MaxLength="5"></cc1:ucTextbox>
                        
                        &nbsp;<cc1:ucLabel ID="lblPerHour_1" runat="server" Text="Pallets Per Hour"></cc1:ucLabel>
                            &nbsp;&nbsp;&nbsp; &lt;<cc1:ucLabel ID="lblPalletsUnloadTime" runat="server" Text="xx"></cc1:ucLabel>&gt; &nbsp;<cc1:ucLabel
                                ID="lblPerMin_1" runat="server" Text="Minutes Per Pallet"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblAverageCartonsUnloadTime" runat="server" Text="Average Cartons unload time"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td style="font-weight: bold;">
                        <%-- <cc1:ucNumericTextbox ID="txtUploadCarton" onkeyup="AllowNumbersOnly(this);"
                                MaxLength="4" runat="server" Width="40px"></cc1:ucNumericTextbox>--%>
                        <cc1:ucTextbox ID="txtUploadCarton" onblur="javascript:return getMinutes(this,'2');" runat="server" Width="40px" ValidationGroup="check" MaxLength="5"></cc1:ucTextbox>
                        
                         &nbsp;<cc1:ucLabel ID="lblPerHour_2" runat="server" Text="Cartons Per Hour"></cc1:ucLabel>
                            &nbsp;&nbsp;&nbsp; &lt;<cc1:ucLabel ID="lblCartonsUnloadTime" runat="server" Text="xx"></cc1:ucLabel>&gt; &nbsp;<cc1:ucLabel
                                ID="lblPerMin_2" runat="server" Text="Minutes Per Cartons"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblAverageReceiptingTimeperline" runat="server" Text="Average Line Receipting Time"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td style="font-weight: bold;">
                        <%--  <cc1:ucNumericTextbox ID="txtReceiptTime" onkeyup="AllowNumbersOnly(this);"
                                MaxLength="4" runat="server" Width="40px"></cc1:ucNumericTextbox>--%>
                        <cc1:ucTextbox ID="txtReceiptTime" onblur="javascript:return getMinutes(this,'3');" runat="server" Width="40px" ValidationGroup="check" MaxLength="5"></cc1:ucTextbox>                        
                        &nbsp;<cc1:ucLabel ID="lblPerHour_3" runat="server" Text="Lines Per Hour"></cc1:ucLabel>
                            &nbsp;&nbsp;&nbsp; &lt;<cc1:ucLabel ID="lblLineReceiptingLineTime" runat="server" Text="xx"></cc1:ucLabel>&gt; &nbsp;<cc1:ucLabel
                                ID="lblPerMin_3" runat="server" Text="Minutes Per Line"></cc1:ucLabel>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="divVendor" style="visibility: hidden; border-style: dotted; border-color: inherit; border-width: 1px; position: absolute;
        left: 300px; top: 275px; width: 400px;">
        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
            <tr>
                <td style="font-weight: bold;" nowarp>
                    Vendor Name
                </td>
                <td style="font-weight: bold;">
                    :
                </td>
                <td style="font-weight: bold;">
                    <cc1:ucTextbox ID="txtVenderName2" runat="server" Width="147px"></cc1:ucTextbox>
                </td>
                <td>
                    <div class="button-row">
                        <cc1:ucButton ID="btnSearch" runat="server" Width="60px" Text="Search" CssClass="button" />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
                <td style="font-weight: bold;" align="left">
                    <asp:ListBox ID="lstVendor" runat="server" Width="150px">
                        <asp:ListItem>Vendor1</asp:ListItem>
                        <asp:ListItem>Vendor2</asp:ListItem>
                        <asp:ListItem>Vendor3</asp:ListItem>
                    </asp:ListBox>
                </td>
            </tr>
        </table>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClientClick="return SaveValidation();"
            OnClick="btnSave_Click" ValidationGroup="check" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button"  OnClientClick="return confirmDelete();"
            OnClick="btnDelete_Click" />
        <cc1:ucButton ID="btnHid" runat="server" OnClick="btnHid_Click" Style="display: none" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" 
            onclick="btnBack_Click" />
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_MiscellaneousSettingsEdit.aspx.cs" Inherits="APPSIT_MiscellaneousSettingsEdit" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">

        function getMinutes(obj, arg) {
            if (obj.value != '') {
                if (IsNumeric(obj.value)) {
                    if (parseInt(obj.value * 1) >= 0) {
                        if (arg == '1') {
                            document.getElementById('<%=lblPalletsUnloadTime.ClientID %>').textContent = document.getElementById('<%=lblPalletsUnloadTime.ClientID %>').innerText = roundNumber(parseFloat(obj.value / 60), 2);
                        }
                        if (arg == '2') {
                            document.getElementById('<%=lblCartonsUnloadTime.ClientID %>').textContent = document.getElementById('<%=lblCartonsUnloadTime.ClientID %>').innerText = roundNumber(parseFloat(obj.value / 60), 2);
                        }
                        else if (arg == '3') {
                            document.getElementById('<%=lblLinesreceiving.ClientID %>').textContent = document.getElementById('<%=lblLinesreceiving.ClientID %>').innerText = roundNumber(parseFloat(obj.value / 60), 2);
                        }
                        else if (arg == '4') {
                            document.getElementById('<%=lblpuatway.ClientID %>').textContent = document.getElementById('<%=lblpuatway.ClientID %>').innerText = roundNumber(parseFloat(obj.value / 60), 2);
                        }
                    }
                    else {
                        alert("<%=CorrectValueValidation %>");
                        return false;
                    }
                }
                else {
                    alert("<%=CorrectValueValidation %>");
                    return false;
                }
            }
        }


        function CheckFormat() {
            var StartTime = $.trim($("#<%=txtStartTime.ClientID%>").val());
            var EndTime = $.trim($("#<%=txtEndTime.ClientID%>").val());

            var re = /^([0-1][0-9]|[2][0-3]):([0-5][0-9])$/;

            var isvalid = true;

            if (StartTime == '' && EndTime == '') {
                return isvalid;
            }

            if (StartTime != '' && EndTime == '') {
                alert('If Non Timed Opening Start Time has a value then so must Non Timed Opening End Time.');
                $("#<%=txtEndTime.ClientID%>").focus();
                isvalid = false;
            } else if (StartTime == '' && EndTime != '') {
                alert('If Non Timed Opening End Time has a value then so must Non Timed Opening Start Time.');
                $("#<%=txtStartTime.ClientID%>").focus();
                isvalid = false;
            } else if (!re.test(StartTime)) {
                alert('Invalid Non Timed Opening Start Time');
                $("#<%=txtStartTime.ClientID%>").focus();
                isvalid = false;
            } else if (!re.test(EndTime)) {
                alert('Invalid Non Timed Opening End Time');
                $("#<%=txtEndTime.ClientID%>").focus();
                isvalid = false;
            } else if (StartTime == EndTime) {
                alert('Non Timed Opening Start Time and Non Timed Opening End Time should not be same.');
                $("#<%=txtStartTime.ClientID%>").focus();
                isvalid = false;
            }
            return isvalid;
        }

        function SaveValidation() {
            if (getMinutes(document.getElementById('<%=txtAverageUnloading.ClientID %>'), '1') == false) {
                return false;
            }

            if (getMinutes(document.getElementById('<%=txtAverageCartonsUnloaded.ClientID %>'), '2') == false) {
                return false;
            }

            if (getMinutes(document.getElementById('<%=txtAverageReceipting.ClientID %>'), '3') == false) {
                return false;
            }

            if (getMinutes(document.getElementById('<%=txtAveragePutaway.ClientID %>'), '4') == false) {
                return false;
            }

            if (!CheckFormat())
                return false;

            return true;
        }
    </script>
    <h2>
        <cc1:ucLabel ID="lblApptMissSettings" runat="server"></cc1:ucLabel>
    </h2>
    <script type="text/javascript">
        function CheckNoofDays(oSource, args) {
            var $days = $('#txtToleratedDays').val();
            if ($days == "" && $('#rdoToleranceDays').is(':checked'))
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    </script>
    <div class="right-shadow">
        <div class="formbox">
            <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 210px">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5px">:
                    </td>
                    <td>
                        <uc2:ucSite ID="ddlSite" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblPreAdviseNote" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">:
                    </td>
                    <td>
                        <cc1:ucCheckbox ID="chkPreAdviseNote1" runat="server" CssClass="checkboxlist" />
                    </td>
                </tr>
            </table>
            <br />
            <cc1:ucPanel ID="pnlBookingMethod" runat="server" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td style="width: 600px;" colspan="3" class="nobold">
                            <cc1:ucLabel ID="lblBookingMethodByDatePO" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 600px;" colspan="3" class="nobold">
                            <cc1:ucRadioButtonList ID="rblByDatePO" RepeatColumns="2" RepeatDirection="Horizontal"
                                runat="server" GroupName="BookingMethod" CssClass="checkboxlist" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 600px;" colspan="3" class="nobold">
                            <cc1:ucLabel ID="lblTimeSlotWindowText" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 600px;" colspan="3" class="nobold">
                            <cc1:ucRadioButtonList ID="rblTimeSlotWindow" RepeatColumns="2" RepeatDirection="Horizontal"
                                runat="server" GroupName="BookingMethod" CssClass="checkboxlist" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 600px;" colspan="3" class="nobold">
                            <cc1:ucLabel ID="lblBookingMethodPerBookingVendor" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 600px;" colspan="3" class="nobold">
                            <cc1:ucRadioButtonList ID="rblPerBookingVendor" RepeatColumns="2" RepeatDirection="Horizontal"
                                runat="server" GroupName="BookingMethod" CssClass="checkboxlist" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <br />
            <cc1:ucPanel ID="pnlAdviseTolratedDays" runat="server" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td style="width: 200px;" class="nobold">
                            <cc1:ucRadioButton ID="rdoToleranceDaysFixed" runat="server" GroupName="ToleranceDays"
                                CssClass="checkboxlist" Checked="true" />
                        </td>
                        <td style="width: 200px;" class="nobold">
                            <cc1:ucRadioButton ID="rdoToleranceDaysOpen" runat="server" GroupName="ToleranceDays"
                                CssClass="checkboxlist" />
                        </td>
                        <td style="width: 200px" valign="top" class="nobold">
                            <cc1:ucRadioButton ID="rdoToleranceDays" ClientIDMode="Static" runat="server" GroupName="ToleranceDays"
                                CssClass="checkboxlist" />
                            <cc1:ucTextbox ID="txtToleratedDays" ClientIDMode="Static" runat="server" Width="12px"
                                MaxLength="3" onkeypress="return IsNumberKey(event, this);"></cc1:ucTextbox>
                            <cc1:ucLabel ID="lblDays" runat="server" onkeyup="AllowNumbersOnly(this);"></cc1:ucLabel>
                            <asp:CustomValidator ID="cusvDaysRequired" ClientValidationFunction="CheckNoofDays"
                                runat="server" ValidationGroup="Save" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGoodsInLinesPerManHours" runat="server" CssClass="fieldset-form">
                <table width="80%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td style="width: 60%">
                            <cc1:ucLabel ID="lblLinesPerFTE" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 5%">:
                        </td>
                        <td style="width: 55%">
                            <cc1:ucTextbox ID="txtLinesPerFTE" runat="server" Width="35px" onkeyup="AllowDecimalOnly(this);"
                                MaxLength="6"></cc1:ucTextbox>
                            <asp:RegularExpressionValidator ID="revLinesPerFTEExpression" ValidationExpression="^\d+(\.\d{0,2})?$"
                                runat="server" ControlToValidate="txtLinesPerFTE" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblAverageUnloding" runat="server"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="txtAverageUnloading" runat="server" Width="27px" onblur="javascript:return getMinutes(this,'1');"
                                onkeyup="AllowNumbersOnly(this);" MaxLength="3"></cc1:ucTextbox>
                            &nbsp;<cc1:ucLabel ID="lblPerHour_1" runat="server" Text="PerHour"></cc1:ucLabel>
                            &nbsp;&nbsp;&nbsp;&lt;<cc1:ucLabel ID="lblPalletsUnloadTime" runat="server" Text="xx"></cc1:ucLabel>&gt;
                            &nbsp;<cc1:ucLabel ID="lblPerMin_1" runat="server" Text="PerMin"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblAverageCartonsUnloding" runat="server" Text="Average # of Cartons Unloaded"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="txtAverageCartonsUnloaded" runat="server" Width="27px" onblur="javascript:return getMinutes(this,'2');"
                                onkeyup="AllowNumbersOnly(this);" MaxLength="3"></cc1:ucTextbox>
                            &nbsp;<cc1:ucLabel ID="lblPerHour_2" runat="server" Text="PerHour"></cc1:ucLabel>
                            &nbsp;&nbsp;&nbsp;&lt;<cc1:ucLabel ID="lblCartonsUnloadTime" runat="server" Text="xx"></cc1:ucLabel>&gt;
                            &nbsp;<cc1:ucLabel ID="lblPerMin_2" runat="server" Text="PerMin"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblAverageReceipting" runat="server"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="txtAverageReceipting" runat="server" Width="27px" onblur="javascript:return getMinutes(this,'3');"
                                onkeyup="AllowNumbersOnly(this);" MaxLength="3"></cc1:ucTextbox>
                            &nbsp;<cc1:ucLabel ID="lblPerHour_3" runat="server" Text="PerHour"></cc1:ucLabel>
                            &nbsp;&nbsp;&nbsp;&lt;<cc1:ucLabel ID="lblLinesreceiving" runat="server" Text="xx"></cc1:ucLabel>&gt;
                            &nbsp;<cc1:ucLabel ID="lblPerMin_3" runat="server" Text="PerMin"></cc1:ucLabel>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <cc1:ucLabel ID="lblAveragePutaway" runat="server"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="txtAveragePutaway" runat="server" Width="27px" onblur="javascript:return getMinutes(this,'4');"
                                onkeyup="AllowNumbersOnly(this);" MaxLength="3"></cc1:ucTextbox>
                            &nbsp;<cc1:ucLabel ID="lblPerHour_4" runat="server" Text="PerHour"></cc1:ucLabel>
                            &nbsp;&nbsp;&nbsp;&lt;<cc1:ucLabel ID="lblpuatway" runat="server" Text="xx"></cc1:ucLabel>&gt;
                            &nbsp;<cc1:ucLabel ID="lblPerMin_4" runat="server" Text="PerMin"></cc1:ucLabel>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                Style="color: Red" ValidationGroup="Save" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlBookingNoticePeriod" runat="server" CssClass="fieldset-form">
                <table width="30%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td class="nobold">
                            <cc1:ucLabel ID="lblInDays1" runat="server" Text="In Days"></cc1:ucLabel>
                        </td>
                        <td class="nobold">:
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="txtNoticePeriodDays" runat="server" Width="12px" MaxLength="2"
                                onkeypress="return IsNumberKey(event, this);"></cc1:ucTextbox>
                        </td>
                        <td class="nobold">
                            <cc1:ucLabel ID="lblBefore" runat="server" Text="Before"></cc1:ucLabel>
                        </td>
                        <td class="nobold">:
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="txtNoticePeriodTime" runat="server" Width="40px" MaxLength="5"></cc1:ucTextbox>
                            <cc1:ucLabel ID="lblTimeFormat" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlDeliveriesAndVehicles" runat="server" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td style="width: 70px;" class="nobold">
                            <cc1:ucLabel ID="lblDefaultDeliveryType" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 100px;" class="nobold" align="left">
                            <cc1:ucDropdownList ID="ddlDefaultDeliveryType" Style="width: 150px;" runat="server">
                            </cc1:ucDropdownList>
                        </td>
                        <td style="width: 70px;" class="nobold">
                            <cc1:ucLabel ID="lblDefaultVehicleType" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 100px;" class="nobold" align="left">
                            <cc1:ucDropdownList ID="ddlDefaultVehicleType" Style="width: 150px;" runat="server">
                            </cc1:ucDropdownList>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlAlternateSlotAvailability" runat="server" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td style="width: 600px;" class="nobold">
                            <cc1:ucRadioButtonList ID="rblAlternateSlotTurnedOnOff" RepeatColumns="2" RepeatDirection="Horizontal"
                                CssClass="checkboxlist" runat="server">
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlSupervisorConfirmationTypes" runat="server" CssClass="fieldset-form">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td style="width: 600px;" class="nobold">
                            <cc1:ucCheckboxList ID="cblSupervisorConfirmationType" RepeatColumns="3" RepeatDirection="Horizontal"
                                CssClass="checkboxlist" runat="server">
                            </cc1:ucCheckboxList>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlMaxVolumeperdelivery" runat="server" CssClass="fieldset-form">
                <table width="70%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td colspan="6" class="nobold">
                            <cc1:ucLabel ID="lblMaxVolumeperdeliveryNotice" runat="server" Text="Enter the maximum volumes a vendor or carrier can add per delivery if required"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td class="nobold" style="width: 100px;">
                            <cc1:ucLabel ID="lblMaximumPallets" runat="server" Text="Maximum Pallets"></cc1:ucLabel>
                        </td>
                        <td class="nobold">:
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="txtMaxPallets" runat="server" Width="40px" MaxLength="3" onkeypress="return IsNumberKey(event, this);"></cc1:ucTextbox>
                        </td>
                        <td class="nobold" style="width: 100px;">
                            <cc1:ucLabel ID="lblMaximumCartons" runat="server" Text="Maximum Cartons"></cc1:ucLabel>
                        </td>
                        <td class="nobold">:
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="txtMaxCartons" runat="server" Width="40px" MaxLength="3" onkeypress="return IsNumberKey(event, this);"></cc1:ucTextbox>
                        </td>
                        <td class="nobold" style="width: 100px;">
                            <cc1:ucLabel ID="lblMaximumLines" runat="server" Text="Maximum Lines"></cc1:ucLabel>
                        </td>
                        <td class="nobold">:
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="txtMaxLines" runat="server" Width="40px" MaxLength="3" onkeypress="return IsNumberKey(event, this);"></cc1:ucTextbox>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlBookingExclusions" runat="server" CssClass="fieldset-form" GroupingText="Booking Exclusions">
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td style="width: 600px;" class="nobold">
                            <cc1:ucLabel ID="lblBookingExclusionsMsg" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 600px;" class="nobold">
                            <cc1:ucRadioButtonList ID="rblBookingExclusions" RepeatColumns="2" RepeatDirection="Horizontal"
                                CssClass="checkboxlist" runat="server">
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>

            <cc1:ucPanel ID="pnlNonTimedOpeningTimes" runat="server" CssClass="fieldset-form">
                <table width="50%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td class="nobold">
                            <cc1:ucLabel ID="lblStartTime" runat="server"></cc1:ucLabel>
                        </td>
                        <td class="nobold">:
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="txtStartTime" runat="server" Width="40px" MaxLength="5"></cc1:ucTextbox>
                            <cc1:ucLabel ID="lblTimeFormat_2" runat="server"></cc1:ucLabel>
                        </td>
                        <td class="nobold">
                            <cc1:ucLabel ID="lblEndTime" runat="server"></cc1:ucLabel>
                        </td>
                        <td class="nobold">:
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="txtEndTime" runat="server" Width="40px" MaxLength="5"></cc1:ucTextbox>
                            <cc1:ucLabel ID="lblTimeFormat_1" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>

            <cc1:ucPanel ID="ucWindowMisSettingforPercentage" runat="server" CssClass="fieldset-form" GroupingText="Window Volume Prcentage Setting">
                <table width="50%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td class="nobold">
                            <cc1:ucLabel ID="uclblpalletPercentage" runat="server"></cc1:ucLabel>
                        </td>
                        <td class="nobold">:
                        </td>
                        <td class="nobold">
                            <cc1:ucTextbox ID="ucpalletPercentage" runat="server" Width="40px" MaxLength="3" onkeypress="return IsNumberKey(event, this);"></cc1:ucTextbox>
                            <cc1:ucLabel ID="UcLabel2" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>        
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" ValidationGroup="Save"
            OnClientClick="return SaveValidation();" OnClick="btnSave_Click" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

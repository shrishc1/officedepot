﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;

public partial class APPSIT_WeekSetupLogs : CommonPage
{
    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        //ddlSite.innerControlddlSite.AutoPostBack = true;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ucExportToExcel.GridViewControl = UcGridView1;
        ucExportToExcel.FileName = "Week Settings Changes";
        //SiteSelectedIndexChanged();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindSiteWeekSetupLogs();
    }

    //public override void SiteSelectedIndexChanged()
    //{        
    //    BindSiteWeekSetupLogs();
    //}

    private void BindSiteWeekSetupLogs()
    {
        APPSIT_SpecificWeekSetupBAL oMASSIT_SpecificWeekSetupBAL = new APPSIT_SpecificWeekSetupBAL();
        MASSIT_WeekSetupLogsBE oMASSIT_WeekSetupLogsBE = new MASSIT_WeekSetupLogsBE();
        oMASSIT_WeekSetupLogsBE.Action = "GetBySiteID";
        oMASSIT_WeekSetupLogsBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedValue);
        List<MASSIT_WeekSetupLogsBE> oMASSIT_WeekSetupLogsBEList = oMASSIT_SpecificWeekSetupBAL.GetWeekSetupLogsBAL(oMASSIT_WeekSetupLogsBE);
        UcGridView1.DataSource = oMASSIT_WeekSetupLogsBEList;
        UcGridView1.DataBind();
        UcGridView1.Visible = true;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;


public partial class APPSIT_DoorConstraintsSpecific : CommonPage
{
    protected string DoorName = WebCommon.getGlobalResourceValue("DoorName");

    public int iTimeSplit = 12;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {

            hdnWeekDay.Value = "mon";

            if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("SiteID").ToString() != "") {
                SiteNameForDCSpecific.Text = GetSiteName(Convert.ToInt32(GetQueryStringValue("SiteID")));
                pnlTimeWindowtop.GroupingText = "Time Window For" + " " + "Monday";
                //GenerateGrid();
            }
        }
    }

    #region Methods

    public string GetSiteName(int pSiteID) {
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        oMAS_SiteBE.Action = "ShowAll";

        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMAS_SiteBE.SiteCountryID = 0;

        List<MAS_SiteBE> lstSite = new List<MAS_SiteBE>();
        lstSite = oMAS_SiteBAL.GetSiteBAL(oMAS_SiteBE);
        string strSiteName = string.Empty;
        if (lstSite.Count > 0) {
            strSiteName = lstSite[lstSite.FindIndex(s => s.SiteID == pSiteID)].SiteDescription.ToString();
        }
        return strSiteName;
    }

    private DropDownList CreateOpenTypeDropDown(int jCount, int iCount) {
        DropDownList dl = new DropDownList();
        dl.ID = "ddlOpenType" + jCount.ToString() + iCount.ToString();
        dl.Width = Unit.Pixel(50);
        dl.Items.Add("Yes");
        dl.Items.Add("No");

        //dl.AutoPostBack = true;
        //dl.SelectedIndexChanged += new EventHandler(dl_SelectedIndexChanged);


        return dl;
    }

    private CheckBox CreateDoorOpenCheckBox(int jCount, int iCount, string pStartWeekday) {
        CheckBox chk = new CheckBox();
        chk.ID = "chkdooropen" + jCount.ToString() + "_" + iCount.ToString() + "_" + pStartWeekday;
        chk.Attributes.Add("onclick", "addRemoveDoor(this,'" + jCount + "','" + iCount + "');");
        if (hdnDoorID.Value.Contains("," + jCount + "@" + iCount)) {
            chk.Checked = true;
        }
        return chk;
    }

    private void GenerateGrid() {
        string sSlotDay = string.Empty;
        string errorMeesage = string.Empty;
        if (txtDate.innerControltxtDate.Value != string.Empty) {
            DateTime dtSelectedDate = txtDate.GetDate;
            if (dtSelectedDate.DayOfWeek != DayOfWeek.Monday) {
                
                //Choose monday
                errorMeesage = WebCommon.getGlobalResourceValue("SelectedDayMonday");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
                return;
            }
        }
        else if (txtDate.innerControltxtDate.Value == string.Empty) {

            //Select a date
            errorMeesage = WebCommon.getGlobalResourceValue("DateRequired");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert2", "alert('" + errorMeesage + "')", true);
            return;
        }

        //-----------------------------------------------------------------
        if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("SiteID").ToString() != "") {

            //check in the week setup specific table for the selected date and weekday.
            DateTime dtSelectedDate = txtDate.GetDate;

            MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE = new MASSIT_SpecificWeekSetupBE();
            APPSIT_SpecificWeekSetupBAL oAPPSIT_SpecificWeekSetupBAL = new APPSIT_SpecificWeekSetupBAL();

            oMASSIT_SpecificWeekSetupBE.Action = "ShowAll";

            oMASSIT_SpecificWeekSetupBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
            oMASSIT_SpecificWeekSetupBE.WeekStartDate = dtSelectedDate;
            oMASSIT_SpecificWeekSetupBE.EndWeekday = hdnWeekDay.Value;
            List<MASSIT_SpecificWeekSetupBE> lstWeekSetupSpecific = oAPPSIT_SpecificWeekSetupBAL.GetSpecificWeekSetupDetailsBAL(oMASSIT_SpecificWeekSetupBE);

            if (lstWeekSetupSpecific.Count <= 0) {

                //specific week setup data does not exists
                errorMeesage = WebCommon.getGlobalResourceValue("SpecificWeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert3", "alert('" + errorMeesage + "')", true);
                return;
            }
            else if (lstWeekSetupSpecific[0].StartTime == null) {

                //specific week setup data does not exists because start time is not defined for the selected date and day.
                errorMeesage = WebCommon.getGlobalResourceValue("SpecificWeekSetupSettingNotDefined");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert4", "alert('" + errorMeesage + "')", true);
                return;
            }
            //----------------------------------------------------------------------
            //first copy the door open time generic data 
            APPSIT_DoorOpenTimeSpecificBAL oAPPSIT_DoorOpenTimeSpecificBAL = new APPSIT_DoorOpenTimeSpecificBAL();
            MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE = new MASSIT_DoorOpenTimeSpecificBE();
            oMASSIT_DoorOpenTimeSpecificBE.Action = "CopyGenericData";
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime = new MASSIT_DoorOpenTimeBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
            oMASSIT_DoorOpenTimeSpecificBE.WeekStartDate = txtDate.GetDate;
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday = hdnWeekDay.Value;
            oAPPSIT_DoorOpenTimeSpecificBAL.AddEditDoorConstraintsSpecificBAL(oMASSIT_DoorOpenTimeSpecificBE);

            //----------------------------------------------------------------------

            oMASSIT_DoorOpenTimeSpecificBE.Action = "GetDoorConstraintsSpecific";
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime = new MASSIT_DoorOpenTimeBE();            
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday = hdnWeekDay.Value;
            oMASSIT_DoorOpenTimeSpecificBE.WeekStartDate = txtDate.GetDate;
            //if (lstWeekSetupSpecific[0].StartWeekday != lstWeekSetupSpecific[0].EndWeekday) {
            //    oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.StartWeekday = lstWeekSetupSpecific[0].StartWeekday;
            //}
            //else{
            //    oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.StartWeekday = hdnWeekDay.Value;
            //}

            List<MASSIT_DoorOpenTimeSpecificBE> lstDoorConstraintsSpecific = oAPPSIT_DoorOpenTimeSpecificBAL.GetDoorConstraintsSpecificBAL(oMASSIT_DoorOpenTimeSpecificBE);

            string DoorIDs = ",";
            string StartWeekDays = ",";
            for (int iCount = 0; iCount < lstDoorConstraintsSpecific.Count; iCount++) {
                DoorIDs += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + ",";
                StartWeekDays += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + "-" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.StartWeekday + ",";
                //if (lstWeekSetupSpecific[0].StartWeekday != lstWeekSetupSpecific[0].EndWeekday) {
                //    StartWeekDays += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + "-" + lstWeekSetupSpecific[0].StartWeekday + ",";
                //}
                //else {
                //    StartWeekDays += lstDoorConstraintsSpecific[iCount].DoorOpenTime.SlotTimeID + "@" + lstDoorConstraintsSpecific[iCount].DoorOpenTime.SiteDoorNumberID + "-" + hdnWeekDay.Value + ",";
                //}
            }

            hdnDoorID.Value = DoorIDs;
            hdnStartWeekDays.Value = StartWeekDays;

            oMASSIT_DoorOpenTimeSpecificBE.Action = "GetDoorNoAndType";
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime = new MASSIT_DoorOpenTimeBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
            
            oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));

            List<MASSIT_DoorOpenTimeSpecificBE> lstDoorNoType = oAPPSIT_DoorOpenTimeSpecificBAL.GetDoorConstraintsSpecificBAL(oMASSIT_DoorOpenTimeSpecificBE);

            int iDoorCount = lstDoorNoType.Count;

            decimal decCellWidth = (100 / (iDoorCount + 1));
            double dblCellWidth = Convert.ToDouble(Math.Ceiling(decCellWidth));

            Table tbl = new Table();
            tbl.CellSpacing = 0;
            tbl.CellPadding = 0;
            tbl.CssClass = "form-tableGrid";
            tbl.Width = Unit.Percentage(100);

            pnlHeaderWindow.Controls.Add(tbl);


            TableRow trDoorNo = new TableRow();
            TableCell tcDoorNo = new TableCell();
            tcDoorNo.Text = DoorName;
            tcDoorNo.BorderWidth = Unit.Pixel(1);
            tcDoorNo.Width = Unit.Percentage(dblCellWidth);
            trDoorNo.Cells.Add(tcDoorNo);

            TableRow trDoorType = new TableRow();
            TableCell tcDoorType = new TableCell();
            tcDoorType.Text = "Door Type";
            tcDoorType.BorderWidth = Unit.Pixel(1);
            tcDoorType.Width = Unit.Percentage(dblCellWidth);
            trDoorType.Cells.Add(tcDoorType);


            TableRow trTime = new TableRow();
            TableCell tcTime = new TableCell();
            tcTime.Text = "Time";
            tcTime.BorderWidth = Unit.Pixel(1);
            tcTime.Width = Unit.Percentage(dblCellWidth);
            trTime.Cells.Add(tcTime);

            for (int iCount = 1; iCount <= iDoorCount; iCount++) {
                tcDoorNo = new TableCell();
                tcDoorNo.BorderWidth = Unit.Pixel(1);

                Label lbl = new Label();
                lbl.Width = 60;
                lbl.Text = lstDoorNoType[iCount - 1].DoorOpenTime.DoorNo.DoorNumber.ToString() + "     ";
                tcDoorNo.Controls.Add(lbl);

                //tcDoorNo.Text = lstDoorNoType[iCount - 1].DoorOpenTime.DoorNo.DoorNumber.ToString();
                tcDoorNo.Width = Unit.Percentage(dblCellWidth);


                CheckBox ddl = CreateAllDoorOpenCheckBox(lstDoorNoType[iCount - 1].DoorOpenTime.SiteDoorNumberID);
                tcDoorNo.Controls.Add(ddl);

                Label lblOpenAll = new Label();
                lblOpenAll.Text = " Open All";
                tcDoorNo.Controls.Add(lblOpenAll);


                trDoorNo.Cells.Add(tcDoorNo);


                tcDoorType = new TableCell();
                tcDoorType.BorderWidth = Unit.Pixel(1);
                tcDoorType.Text = lstDoorNoType[iCount - 1].DoorOpenTime.DoorType.DoorType;
                tcDoorType.Width = Unit.Percentage(dblCellWidth);
                trDoorType.Cells.Add(tcDoorType);

                tcTime = new TableCell();
                tcTime.BorderWidth = Unit.Pixel(1);
                tcTime.Text = "Open?";
                tcTime.Width = Unit.Percentage(dblCellWidth);
                trTime.Cells.Add(tcTime);
            }
            trDoorNo.BackColor = System.Drawing.Color.LightGray;
            tbl.Rows.Add(trDoorNo);

            trDoorType.BackColor = System.Drawing.Color.LightGray;
            tbl.Rows.Add(trDoorType);

            trTime.BackColor = System.Drawing.Color.LightGray;
            tbl.Rows.Add(trTime);


            Table tblSlot = new Table();
            tblSlot.CellSpacing = 0;
            tblSlot.CellPadding = 0;

            tblSlot.CssClass = "form-tableGrid";
            tblSlot.Width = Unit.Percentage(100);

            pnlTimeWindow.Controls.Add(tblSlot);

            SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
            SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();

            oSYS_SlotTimeBE.Action = "ShowAll";
            List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);

            for (int jCount = 0; jCount < lstSlotTime.Count; jCount++) {
                string StartTime = lstWeekSetupSpecific[0].StartTime.Value.Hour.ToString() + "." + lstWeekSetupSpecific[0].StartTime.Value.Minute.ToString();
                if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00") {
                    StartTime = "24." + lstWeekSetupSpecific[0].StartTime.Value.Minute.ToString();
                }

                string EndTime = lstWeekSetupSpecific[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetupSpecific[0].EndTime.Value.Minute.ToString();
                if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00") {
                    EndTime = "24." + lstWeekSetupSpecific[0].EndTime.Value.Minute.ToString();
                }

                decimal dSlotTime = Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.'));
                decimal dStartTime = Convert.ToDecimal(StartTime);
                decimal dEndTime = Convert.ToDecimal(EndTime);
               
                if (lstWeekSetupSpecific[0].StartWeekday != lstWeekSetupSpecific[0].EndWeekday) {
                    dEndTime = Convert.ToDecimal("24.00");
                    sSlotDay = lstWeekSetupSpecific[0].StartWeekday;
                }
                
                if (dSlotTime >= dStartTime && dSlotTime < dEndTime) {
                    TableRow tr = new TableRow();
                    TableCell tc = new TableCell();
                    tc.BorderWidth = Unit.Pixel(1);
                    tc.Text = lstSlotTime[jCount].SlotTime;
                    tr.Cells.Add(tc);

                    for (int iCount = 1; iCount <= iDoorCount; iCount++) {
                        tc = new TableCell();
                        //tc.Text = "";
                        CheckBox ddl = CreateDoorOpenCheckBox(lstSlotTime[jCount].SlotTimeID, lstDoorNoType[iCount - 1].DoorOpenTime.SiteDoorNumberID, sSlotDay);
                        tc.BorderWidth = Unit.Pixel(1);
                        tc.Controls.Add(ddl);
                        tc.Width = Unit.Percentage(dblCellWidth);
                        tr.Cells.Add(tc);
                    }
                    tblSlot.Rows.Add(tr);

                    if ((jCount + 1) % iTimeSplit == 0) {

                        tr = new TableRow();
                        tr.BackColor = System.Drawing.Color.LightGray;
                        tc = new TableCell();
                        tc.BorderWidth = Unit.Pixel(1);
                        tc.ColumnSpan = iDoorCount + 1;
                        tr.Cells.Add(tc);
                        tblSlot.Rows.Add(tr);
                    }
                }
            }

            if (lstWeekSetupSpecific[0].StartWeekday != lstWeekSetupSpecific[0].EndWeekday) {
                sSlotDay = lstWeekSetupSpecific[0].EndWeekday;
                for (int jCount = 0; jCount < lstSlotTime.Count; jCount++) {
                    string EndTime = lstWeekSetupSpecific[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetupSpecific[0].EndTime.Value.Minute.ToString();
                    if (lstSlotTime[jCount].SlotTime.Split(new char[] { ':' })[0] == "00") {
                        EndTime = "00." + lstWeekSetupSpecific[0].EndTime.Value.Minute.ToString();
                    }
                    if (Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.')) < Convert.ToDecimal(EndTime)) {
                        TableRow tr = new TableRow();
                        TableCell tc = new TableCell();
                        tc.BorderWidth = Unit.Pixel(1);
                        tc.Text = lstSlotTime[jCount].SlotTime;
                        tr.Cells.Add(tc);

                        for (int iCount = 1; iCount <= iDoorCount; iCount++) {
                            tc = new TableCell();
                            //tc.Text = "";
                            CheckBox ddl = CreateDoorOpenCheckBox(lstSlotTime[jCount].SlotTimeID, lstDoorNoType[iCount - 1].DoorOpenTime.SiteDoorNumberID, sSlotDay);
                            tc.BorderWidth = Unit.Pixel(1);
                            tc.Controls.Add(ddl);
                            tc.Width = Unit.Percentage(dblCellWidth);
                            tr.Cells.Add(tc);
                        }
                        tblSlot.Rows.Add(tr);

                        if ((jCount + 1) % iTimeSplit == 0) {

                            tr = new TableRow();
                            tr.BackColor = System.Drawing.Color.LightGray;
                            tc = new TableCell();
                            tc.BorderWidth = Unit.Pixel(1);
                            tc.ColumnSpan = iDoorCount + 1;
                            tr.Cells.Add(tc);
                            tblSlot.Rows.Add(tr);
                        }
                    }
                }
            }
        }
    }

    private CheckBox CreateAllDoorOpenCheckBox(int SiteDoorNumberID) {
        CheckBox chk = new CheckBox();
        chk.ID = "chkdooropenAll" + "_" + SiteDoorNumberID;
        chk.Attributes.Add("onclick", "openAllDoor(this,'" + SiteDoorNumberID + "');");
        return chk;
    }
    #endregion

    #region Events

    protected void btnSave_Click(object sender, EventArgs e) {
        if (hdnWeekDay.Value == string.Empty) {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('please select day.');", true);
            return;
        }

        string DoorConstraintIDs = hdnDoorID.Value;
        string StartWeekDays = hdnStartWeekDays.Value;

        APPSIT_DoorOpenTimeSpecificBAL oAPPSIT_DoorOpenTimeSpecificBAL = new APPSIT_DoorOpenTimeSpecificBAL();
        MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE = new MASSIT_DoorOpenTimeSpecificBE();

        oMASSIT_DoorOpenTimeSpecificBE.Action = "Add";
        oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime = new MASSIT_DoorOpenTimeBE();
        oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
        oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
            
        oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorConstraintIDs = DoorConstraintIDs.TrimStart(new char[] { ',' }).TrimEnd(new char[] { ',' });
        string[] strSplit = (StartWeekDays.TrimStart(new char[] { ',' }).TrimEnd(new char[] { ',' })).Split(',');
        string strWeekDays = ",";

        for (int i = 0; i < strSplit.Length; i++) {
            if(strSplit[i] != string.Empty)
                strWeekDays += (strSplit[i].Split('-'))[1] + ",";
        }
        oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.StartWeekDays = strWeekDays.TrimStart(new char[] { ',' }).TrimEnd(new char[] { ',' });  
  
        oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday = hdnWeekDay.Value;
        oMASSIT_DoorOpenTimeSpecificBE.WeekStartDate = txtDate.GetDate;
        oAPPSIT_DoorOpenTimeSpecificBAL.AddEditDoorConstraintsSpecificBAL(oMASSIT_DoorOpenTimeSpecificBE);

        GenerateGrid();
    }

    protected void btnWeekday_Click(object sender, EventArgs e) {
        hdnWeekDay.Value = ((System.Web.UI.WebControls.Button)(sender)).CommandArgument;
        pnlTimeWindowtop.GroupingText = "Time Window For" + " " + ((Button)(sender)).Text;
        GenerateGrid();
    }
    
    protected void btnGo_Click(object sender, EventArgs e) {
        GenerateGrid();
    }

    #endregion
}
﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.Collections.Generic;
using System.Linq;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;


public partial class APPSIT_DoorNoSetupEdit : CommonPage {
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_InIt(object sender, EventArgs e) {
        ucSite.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e) {
                  
    }

    public override void SitePost_Load() {
        base.SitePost_Load();
        if (!IsPostBack) {
            if (GetQueryStringValue("SiteDoorNumberID") == null)
            {
                ucSite.innerControlddlSite.SelectedIndex = 0;
                //else
                //    ucSite.innerControlddlSite.SelectedIndex = ucSite.innerControlddlSite.Items.IndexOf(ucSite.innerControlddlSite.Items.FindByText(
                //        ucSite.innerControlddlSite.SelectedItem.Text));

                ucSite.innerControlddlSite.AutoPostBack = true;
                BindDoorType();
            }
            else
            {
                GetDoorNumber();
            }
        }
    }

    protected void BindDoorType() {

        MASCNT_DoorTypeBE oMASCNT_DoorTypeBE = new MASCNT_DoorTypeBE();
        APPCNT_DoorTypeBAL oMASCNT_DoorTypeBAL = new APPCNT_DoorTypeBAL();

        ddlDoorType.Items.Clear();
        oMASCNT_DoorTypeBE.Action = "ShowDoorType";
        oMASCNT_DoorTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_DoorTypeBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        if (ucSite.innerControlddlSite != null && ucSite.innerControlddlSite.SelectedItem != null)
        {
            oMASCNT_DoorTypeBE.Site.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
        }
        oMASCNT_DoorTypeBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMASCNT_DoorTypeBE.Vehicle = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE();
        List<MASCNT_DoorTypeBE> lstDoorType = oMASCNT_DoorTypeBAL.GetDoorTypeBAL(oMASCNT_DoorTypeBE);

        if (lstDoorType.Count > 0)
            FillControls.FillDropDown(ref ddlDoorType, lstDoorType.ToList(), "DoorType", "DoorTypeID", "--Select--");
        else
        {
            ddlDoorType.Items.Insert(0, new ListItem("--" + "--Select--" + "--", "0"));            
        }
    }

    protected void GetDoorNumber() {
        MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE = new MASSIT_DoorNoSetupBE();
        MASSIT_DoorNoSetupBAL oMASSIT_DoorNoSetupBAL = new MASSIT_DoorNoSetupBAL();

        if (GetQueryStringValue("SiteDoorNumberID") != null) {
            //btnSave.Visible = false;
            ucSite.innerControlddlSite.Enabled = false;
            //ddlDoorType.Enabled = false;
            //txtDoorName.Enabled = false;
            //txtReferenceText.Enabled = false;
            oMASSIT_DoorNoSetupBE.Action = "ShowById";
            oMASSIT_DoorNoSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_DoorNoSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_DoorNoSetupBE.SiteDoorNumberID = Convert.ToInt32(GetQueryStringValue("SiteDoorNumberID"));
            MASSIT_DoorNoSetupBE lstDoorNoSetup = oMASSIT_DoorNoSetupBAL.GetDoorNoSetupDetailsByIdBAL(oMASSIT_DoorNoSetupBE);

            //ucSite.innerControlddlSite.SelectedValue = Convert.ToString(lstDoorNoSetup.SiteID);
            ucSite.innerControlddlSite.SelectedIndex = ucSite.innerControlddlSite.Items.IndexOf(ucSite.innerControlddlSite.Items.FindByValue(
                 Convert.ToString(lstDoorNoSetup.SiteID)));
            BindDoorType();

            
            ddlDoorType.SelectedIndex = ddlDoorType.Items.IndexOf(ddlDoorType.Items.FindByValue(lstDoorNoSetup.SiteDoorTypeID.ToString()));
            txtDoorName.Text = Convert.ToString(lstDoorNoSetup.DoorNumber);
            txtReferenceText.Text = Convert.ToString(lstDoorNoSetup.Reference);
        }
        else {
            btnDelete.Visible = false;
        }

    }

    #region Events

    protected void btnSave_Click(object sender, EventArgs e) {
        MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE = new MASSIT_DoorNoSetupBE();        
        MASSIT_DoorNoSetupBAL oMASSIT_DoorNoSetupBAL = new MASSIT_DoorNoSetupBAL();

        int intSiteDoorNumberID = 0;
        if (GetQueryStringValue("SiteDoorNumberID") != null)
        {
            intSiteDoorNumberID = Convert.ToInt32(GetQueryStringValue("SiteDoorNumberID"));
        }

        oMASSIT_DoorNoSetupBE.SiteDoorNumberID = intSiteDoorNumberID;
        oMASSIT_DoorNoSetupBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_DoorNoSetupBE.SiteDoorTypeID = Convert.ToInt32(ddlDoorType.SelectedValue);
        oMASSIT_DoorNoSetupBE.DoorNumber = txtDoorName.Text;
        oMASSIT_DoorNoSetupBE.Reference = txtReferenceText.Text;
        oMASSIT_DoorNoSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_DoorNoSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMASSIT_DoorNoSetupBE.Action = "CheckExistance";
        
        List<MASSIT_DoorNoSetupBE> lstMASSIT_DoorNoSetupBE = oMASSIT_DoorNoSetupBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorNoSetupBE);
        if (lstMASSIT_DoorNoSetupBE.Count > 0) 
        {
            string errorMessage = WebCommon.getGlobalResourceValue("DoorNameExist");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
        }
        else{
            if (GetQueryStringValue("SiteDoorNumberID") != null)
                oMASSIT_DoorNoSetupBE.Action = "Update";
            else
                oMASSIT_DoorNoSetupBE.Action = "Add";

            oMASSIT_DoorNoSetupBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
            oMASSIT_DoorNoSetupBE.SiteDoorTypeID = Convert.ToInt32(ddlDoorType.SelectedValue);
            oMASSIT_DoorNoSetupBAL.addEditDoorNoSetupDetailsBAL(oMASSIT_DoorNoSetupBE);
            EncryptQueryString("APPSIT_DoorNoSetupOverview.aspx");
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e) {

        MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE = new MASSIT_DoorNoSetupBE();
        MASSIT_DoorNoSetupBAL oMASSIT_DoorNoSetupBAL = new MASSIT_DoorNoSetupBAL();

        oMASSIT_DoorNoSetupBE.Action = "Delete";
        if (GetQueryStringValue("SiteDoorNumberID") != null) {
            oMASSIT_DoorNoSetupBE.SiteDoorNumberID = Convert.ToInt32(GetQueryStringValue("SiteDoorNumberID"));
            oMASSIT_DoorNoSetupBAL.addEditDoorNoSetupDetailsBAL(oMASSIT_DoorNoSetupBE);
        }
        EncryptQueryString("APPSIT_DoorNoSetupOverview.aspx");
    }
    protected void btnBack_Click(object sender, EventArgs e) {

        EncryptQueryString("APPSIT_DoorNoSetupOverview.aspx");
    }

    public override void SiteSelectedIndexChanged() {
        BindDoorType();
        GetDoorNumber();

    }
    #endregion

}
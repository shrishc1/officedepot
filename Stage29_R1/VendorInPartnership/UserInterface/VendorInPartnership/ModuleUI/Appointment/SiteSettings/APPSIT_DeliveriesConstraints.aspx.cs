﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using Utilities; using WebUtilities;

public partial class APPSIT_DeliveriesConstraints : CommonPage 
{
    string strCorrectValueValidation = WebCommon.getGlobalResourceValue("CorrectValueValidation");

    protected void Page_InIt(object sender, EventArgs e) {
        ddlSite.CurrentPage = this;
        ddlSite.TimeSlotWindow = "S";
    }

    public override void SitePost_Load() {
        base.SitePost_Load();
        if (!Page.IsPostBack) {
            ddlSite.innerControlddlSite.AutoPostBack = true;
            hdnWeekDay.Value = "Mon";
            
            BindDeliveryConstraint();
            BindTotals();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            ucpnlTimeWindow.GroupingText = "Time Window For" + " " + "Monday";
        }
    }

    private void BindDeliveryConstraint() {

        //-----------------------------------------------------------------
        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        oMASSIT_WeekSetupBE.Action = "ShowAll";

        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndWeekday = hdnWeekDay.Value;
        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);



        if (lstWeekSetup.Count <= 0) {
            string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
            ClearControls();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
            return;
        }
        else if (lstWeekSetup[0].StartTime == null) {
            string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
            ClearControls();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
            return;
        }

        //----------------------------------------------------------------------

        DataTable myDataTable = new DataTable();

        DataColumn myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "TimeSlot";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "StartWeekday";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "StartTime";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "RestrictnumberofDeliveries";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "MaximumNumberofLines";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "MaximumNumberofLifts";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Restrictsingledeliverypallets";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.ColumnName = "Restrictsingledeliverylines";
        myDataTable.Columns.Add(myDataColumn);


        APPSIT_DeliveryConstraintBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DeliveryConstraintBAL();
        MASSIT_DeliveryConstraintBE oMASSIT_DeliveryConstraintBE = new MASSIT_DeliveryConstraintBE();

        //oMASSIT_DeliveryConstraintBE.Action = "GetOpenDoorSlotTime";
        //oMASSIT_DeliveryConstraintBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        //oMASSIT_DeliveryConstraintBE.Weekday = hdnWeekDay.Value;

        //List<MASSIT_DeliveryConstraintBE> lstOpenDoor = oMASSIT_DoorOpenTimeBAL.GetOpenDoorSlotTimeBAL(oMASSIT_DeliveryConstraintBE);


        string sWeekDay = string.Empty;

        //for (int iCount = 0; iCount < lstOpenDoor.Count; iCount++) {
        for (int iCount = 0; iCount < 24; iCount++) {

            //----------------------------
            sWeekDay = lstWeekSetup[0].StartWeekday;
            int iStartTime = lstWeekSetup[0].StartTime.Value.Hour;
            int iCounterStart = iStartTime;
            if (iStartTime == 0) {
                iStartTime = 24;
                iCounterStart = 0;
            }

            int iEndTime = lstWeekSetup[0].EndTime.Value.Hour;
            int iCountEnd = iEndTime;
            if (iEndTime == 0) {
                iEndTime = 24;
                iCountEnd = 0;
            }

            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday) {
                iEndTime = 24;
                iCountEnd = 0;
            }
            //-------------------------------------------------

            if (iCount >= iCounterStart && iCount < iEndTime) {
                DataRow dataRow = myDataTable.NewRow();
                dataRow["TimeSlot"] = GetHourFormat(iCount.ToString()) + " - " + GetHourFormat((iCount + 1).ToString());
                dataRow["StartTime"] = iCount;

                oMASSIT_DeliveryConstraintBE.Action = "GetDeliveryConstraint";
                oMASSIT_DeliveryConstraintBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                oMASSIT_DeliveryConstraintBE.Weekday = hdnWeekDay.Value;
                oMASSIT_DeliveryConstraintBE.StartWeekday = lstWeekSetup[0].StartWeekday; 
                oMASSIT_DeliveryConstraintBE.StartTime = iCount;

                List<MASSIT_DeliveryConstraintBE> lstDeliveryConstraint = oMASSIT_DoorOpenTimeBAL.GetDeliveryConstraintBAL(oMASSIT_DeliveryConstraintBE);

                dataRow["StartWeekday"] = lstWeekSetup[0].StartWeekday; 
                if (lstDeliveryConstraint.Count > 0) {
                    dataRow["RestrictnumberofDeliveries"] = lstDeliveryConstraint[0].RestrictDelivery;
                    dataRow["MaximumNumberofLines"] = lstDeliveryConstraint[0].MaximumLine;
                    dataRow["MaximumNumberofLifts"] = lstDeliveryConstraint[0].MaximumLift;
                    dataRow["Restrictsingledeliverypallets"] = lstDeliveryConstraint[0].SingleDeliveryPalletRestriction;
                    dataRow["Restrictsingledeliverylines"] = lstDeliveryConstraint[0].SingleDeliveryLineRestriction;
                    
                }

                myDataTable.Rows.Add(dataRow);
            }
        }


        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday) {
            for (int iCount = 0; iCount < 24; iCount++) {

                //----------------------------
                sWeekDay = lstWeekSetup[0].EndWeekday;
                int iEndTime = lstWeekSetup[0].EndTime.Value.Hour;
                if (iEndTime == 0) {
                    iEndTime = 24;
                }

               
                //-------------------------------------------------

                if (iCount < iEndTime) {
                    DataRow dataRow = myDataTable.NewRow();
                    dataRow["TimeSlot"] = GetHourFormat(iCount.ToString()) + " - " + GetHourFormat((iCount + 1).ToString());
                    dataRow["StartTime"] = iCount;

                    oMASSIT_DeliveryConstraintBE.Action = "GetDeliveryConstraint";
                    oMASSIT_DeliveryConstraintBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
                    oMASSIT_DeliveryConstraintBE.Weekday = hdnWeekDay.Value;
                    oMASSIT_DeliveryConstraintBE.StartWeekday = hdnWeekDay.Value;
                    oMASSIT_DeliveryConstraintBE.StartTime = iCount;

                    List<MASSIT_DeliveryConstraintBE> lstDeliveryConstraint = oMASSIT_DoorOpenTimeBAL.GetDeliveryConstraintBAL(oMASSIT_DeliveryConstraintBE);

                    dataRow["StartWeekday"] = hdnWeekDay.Value;
                    if (lstDeliveryConstraint.Count > 0) {
                        dataRow["RestrictnumberofDeliveries"] = lstDeliveryConstraint[0].RestrictDelivery;
                        dataRow["MaximumNumberofLines"] = lstDeliveryConstraint[0].MaximumLine;
                        dataRow["MaximumNumberofLifts"] = lstDeliveryConstraint[0].MaximumLift;
                        dataRow["Restrictsingledeliverypallets"] = lstDeliveryConstraint[0].SingleDeliveryPalletRestriction;
                        dataRow["Restrictsingledeliverylines"] = lstDeliveryConstraint[0].SingleDeliveryLineRestriction;
                        
                    }

                    myDataTable.Rows.Add(dataRow);
                }
            }
        }



        GridView1.DataSource = myDataTable;

        GridView1.DataBind();
        ViewState["myDataTable"] = myDataTable;

    }

    public string GetHourFormat(string HourMin) {
        if (HourMin == "24")
            HourMin = "00";

        if (HourMin.Length < 2)
            return "0" + HourMin + ":00";
        else
            return HourMin + ":00";
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Update") {
            APPSIT_DeliveryConstraintBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DeliveryConstraintBAL();
            MASSIT_DeliveryConstraintBE oMASSIT_DeliveryConstraintBE = new MASSIT_DeliveryConstraintBE();

            int index = Convert.ToInt32(e.CommandArgument);

            oMASSIT_DeliveryConstraintBE.Action = "Add";

            TextBox txtRestrictDeliveries = (TextBox)GridView1.Rows[index].FindControl("txtRestrictDeliveries");
            if (txtRestrictDeliveries.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtRestrictDeliveries.Text.Trim().Replace(".", string.Empty)))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
                txtRestrictDeliveries.Focus();
                return;
            }
            oMASSIT_DeliveryConstraintBE.RestrictDelivery = txtRestrictDeliveries.Text != string.Empty ? Convert.ToInt32(txtRestrictDeliveries.Text) : (int?)null;

            TextBox txtMaximumLines = (TextBox)GridView1.Rows[index].FindControl("txtMaximumLines");
            if (txtMaximumLines.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtMaximumLines.Text.Trim().Replace(".", string.Empty)))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
                txtMaximumLines.Focus();
                return;
            }
            oMASSIT_DeliveryConstraintBE.MaximumLine = txtMaximumLines.Text != string.Empty ? Convert.ToInt32(txtMaximumLines.Text) : (int?)null;

            TextBox txtMaximumLifts = (TextBox)GridView1.Rows[index].FindControl("txtMaximumLifts");
            if (txtMaximumLifts.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtMaximumLifts.Text.Trim().Replace(".", string.Empty)))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
                txtMaximumLifts.Focus();
                return;
            }
            oMASSIT_DeliveryConstraintBE.MaximumLift = txtMaximumLifts.Text != string.Empty ? Convert.ToInt32(txtMaximumLifts.Text) : (int?)null;

            TextBox txtRestrictsingledeliverypallets = (TextBox)GridView1.Rows[index].FindControl("txtRestrictsingledeliverypallets");
            if (txtRestrictsingledeliverypallets.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtRestrictsingledeliverypallets.Text.Trim().Replace(".", string.Empty)))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
                txtRestrictsingledeliverypallets.Focus();
                return;
            }
            oMASSIT_DeliveryConstraintBE.SingleDeliveryPalletRestriction = txtRestrictsingledeliverypallets.Text != string.Empty ? Convert.ToInt32(txtRestrictsingledeliverypallets.Text) : (int?)null;

            TextBox txtRestrictsingledeliverylines = (TextBox)GridView1.Rows[index].FindControl("txtRestrictsingledeliverylines");
            if (txtRestrictsingledeliverylines.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtRestrictsingledeliverylines.Text.Trim().Replace(".", string.Empty)))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
                txtRestrictsingledeliverylines.Focus();
                return;
            }
            oMASSIT_DeliveryConstraintBE.SingleDeliveryLineRestriction = txtRestrictsingledeliverylines.Text != string.Empty ? Convert.ToInt32(txtRestrictsingledeliverylines.Text) : (int?)null;

            HiddenField hdnStartTime = (HiddenField)GridView1.Rows[index].FindControl("hdnStartTime");
            oMASSIT_DeliveryConstraintBE.StartTime = Convert.ToInt32(hdnStartTime.Value);

            HiddenField hdnStartWeekday = (HiddenField)GridView1.Rows[index].FindControl("hdnStartWeekday");
            oMASSIT_DeliveryConstraintBE.StartWeekday = hdnStartWeekday.Value;

            oMASSIT_DeliveryConstraintBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);

            oMASSIT_DeliveryConstraintBE.Weekday = hdnWeekDay.Value;

            oMASSIT_DoorOpenTimeBAL.addEdiDeliveryConstraintBAL(oMASSIT_DeliveryConstraintBE);          
            
        }
        GridView1.EditIndex = -1;
        BindDeliveryConstraint();
        BindTotals();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindDeliveryConstraint();
        BindTotals();
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        BindDeliveryConstraint();
        BindTotals();
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        //BindDeliveryConstraint();

    }

    protected void btnWeekday_Click(object sender, EventArgs e) {
        hdnWeekDay.Value = ((System.Web.UI.WebControls.Button)(sender)).CommandArgument;
        ucpnlTimeWindow.GroupingText = "Time Window For" + " " + ((Button)(sender)).Text;
        BindDeliveryConstraint();
        BindTotals();
    }

    public override void SiteSelectedIndexChanged() {
        //hdnDoorID.Value = ",";
        hdnWeekDay.Value = "Mon";
        ucpnlTimeWindow.GroupingText = "Time Window For" + " " + "Monday";
        BindDeliveryConstraint();
        BindTotals();
    }

    private void BindTotals() {

        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();

        oMASSIT_WeekSetupBE.Action = "ShowAll";
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        
        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
               
        lstWeekSetup = lstWeekSetup.FindAll(delegate(MASSIT_WeekSetupBE WS) {
                return WS.EndWeekday == hdnWeekDay.Value.ToLower();
            });

        if (lstWeekSetup.Count > 0) {
            ltTotalPallets.Text = lstWeekSetup[0].MaximumPallet != null ? lstWeekSetup[0].MaximumPallet.ToString() : "-";
            ltTotalLines.Text = lstWeekSetup[0].MaximumLine != null ? lstWeekSetup[0].MaximumLine.ToString() : "-";
        }        
    }

    protected void btnSpecificDeliveryConstraints_Click(object sender, EventArgs e) {
        EncryptQueryString("APPSIT_DeliveriesConstraintsSpecific.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value);
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MASSIT_CarrierProcessingWindowBE>.SortList((List<MASSIT_CarrierProcessingWindowBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void GridView1_Sorting1(object sender, GridViewSortEventArgs e)
    {
        if (ViewState["myDataTable"] != null)
        {
            DataTable myDataTable = (DataTable)ViewState["myDataTable"];
            ViewState["sortDirection"] = ViewState["sortDirection"] == null || Convert.ToString(ViewState["sortDirection"]) == "DESC" ? "ASC" : "DESC";
            DataView view = myDataTable.DefaultView;
            view.Sort = e.SortExpression + " " + Convert.ToString(ViewState["sortDirection"]);
            GridView1.DataSource = view;
            GridView1.DataBind();

        }
    }

    public void ClearControls() {
        GridView1.DataSource = null;
        GridView1.DataBind();
        ViewState["myDataTable"] = null;

        ltTotalPallets.Text = string.Empty;
        ltTotalLines.Text = string.Empty;
    }
}
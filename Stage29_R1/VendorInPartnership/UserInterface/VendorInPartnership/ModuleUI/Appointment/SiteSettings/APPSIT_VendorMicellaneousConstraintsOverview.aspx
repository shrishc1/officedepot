﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_VendorMicellaneousConstraintsOverview.aspx.cs" Inherits="APPSIT_VendorMicellaneousConstraintsOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblVendorMiscellaneousConstraints" runat="server" Text="Vendor Micellaneous Constraints"></cc1:ucLabel>
    </h2>
    <table class="top-settings" width="30%">
        <tr>
            <td style="font-weight: bold;" align="center">
                <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;
                <uc2:ucSite ID="ddlSite" runat="server" />
            </td>

        </tr>
    </table>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="APPSIT_VendorMicellaneousConstraintsEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" style="width: 100%;">
                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <%-- <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltSiteName" runat="server" Text='<%# Eval("Site.SiteName") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>--%>

                        <asp:TemplateField HeaderText="Vendor" SortExpression="Vendor.VendorName">
                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpLink" runat="server" Text='<%# Eval("Vendor.VendorName") %>'
                                    NavigateUrl='<%# EncryptQuery("APPSIT_VendorMicellaneousConstraintsEdit.aspx?SiteVendorID="+ Eval("SiteVendorID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="MaxPallets" DataField="APP_MaximumPallets" SortExpression="APP_MaximumPallets">
                            <HeaderStyle HorizontalAlign="Center" Width="7.28%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="MaxLines" DataField="APP_MaximumLines" SortExpression="APP_MaximumLines">
                            <HeaderStyle HorizontalAlign="Center" Width="7.28%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="MaxDeliveries " DataField="APP_MaximumDeliveriesInADay" SortExpression="APP_MaximumDeliveriesInADay">
                            <HeaderStyle HorizontalAlign="Center" Width="7.28%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="CannotDeliverBeforeH">
                            <HeaderStyle HorizontalAlign="Center" Width="7.28%" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Literal ID="ltbeforhour" runat="server" Text='<%# Eval("APP_CannotDeliverBefore.Hour") %>'></asp:Literal>:<asp:Literal ID="ltbeformin" runat="server" Text='<%# Eval("APP_CannotDeliverBefore.Minute") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CannotDeliverAfterH">
                            <HeaderStyle HorizontalAlign="Center" Width="7.28%" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Literal ID="ltAfterhour" runat="server" Text='<%# Eval("APP_CannotDeliverAfter.Hour") %>'></asp:Literal>:<asp:Literal ID="ltltAftermin" runat="server" Text='<%# Eval("APP_CannotDeliverAfter.Minute") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CheckBoxField HeaderText="CannotDeliverOnMon" DataField="APP_CannotDeliveryOnMonday" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7.28%" />
                        <asp:CheckBoxField HeaderText="CannotDeliverOnTue" DataField="APP_CannotDeliveryOnTuesday" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7.28%" />
                        <asp:CheckBoxField HeaderText="CannotDeliverOnWed" DataField="APP_CannotDeliveryOnWednessday" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7.28%" />
                        <asp:CheckBoxField HeaderText="CannotDeliverOnThu" DataField="APP_CannotDeliveryOnThrusday" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7.28%" />
                        <asp:CheckBoxField HeaderText="CannotDeliverOnFri" DataField="APP_CannotDeliveryOnFriday" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7.28%" />
                        <asp:CheckBoxField HeaderText="CheckingRestriction" DataField="APP_CheckingRequired" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7.28%" />
                       
                        <asp:BoundField HeaderText="UpdatedBy" DataField="UserName">
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="UpdatedOn">
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Literal ID="ltUpdatedOn" runat="server" Text='<%# Eval("UpdatedOn", "{0:dd, MMM yyyy}") %>'>

                                </asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>

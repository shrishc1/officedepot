﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" CodeFile="APPSIT_VehicleTypeSetupEdit.aspx.cs"
    Inherits="APPSIT_VehicleTypeSetupEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
    </script>
    <h2>
        <cc1:ucLabel ID="lblVehicleTypeSetup" runat="server" Text="Vehicle Type Setup"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div>
            <asp:RequiredFieldValidator ID="rfvVehicleTypeRequired" runat="server" ControlToValidate="txtVehicleType" Display="None"
                ValidationGroup="a">
            </asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvVolumeBasedPalletsRequired" runat="server" ControlToValidate="txtVolumeBasedPallets"
                Display="None" ValidationGroup="a">
            </asp:RequiredFieldValidator>
            <asp:RangeValidator ID="ranvVolumeBasedPalletsIntegerDataRequired" runat="server" ControlToValidate="txtVolumeBasedPallets"
                Display="None" ValidationGroup="a" Type="Integer" MinimumValue="1" MaximumValue="999"></asp:RangeValidator>
            <asp:RequiredFieldValidator ID="rfvVolumeBasedCartonsRequired" runat="server" ControlToValidate="txtVolumeBasedCartons"
                Display="None" ValidationGroup="a">
            </asp:RequiredFieldValidator>
            <asp:RangeValidator ID="ranvVolumeBasedCartonsIntegerDataRequired" runat="server" ControlToValidate="txtVolumeBasedCartons"
                Display="None" ValidationGroup="a" Type="Integer" MinimumValue="1" MaximumValue="999"></asp:RangeValidator>
            

            <asp:RequiredFieldValidator ID="rfvMinTimePerDeliveryRequired" runat="server" ControlToValidate="txtMinTimePerDelivery"
                Display="None" ValidationGroup="a">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revMinTimePerDeliveryCorrectFormatRequired" runat="server" ControlToValidate="txtMinTimePerDelivery"
                Display="None" SetFocusOnError="true" ValidationGroup="a" ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$">
            </asp:RegularExpressionValidator>

            <asp:RequiredFieldValidator ID="rfvTimebasedUnloadTimeRequired" runat="server" ControlToValidate="txtTimebasedUnloadTime"
                Display="None" ValidationGroup="a">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revTimebasedUnloadTimeCoorectFormatRequired" runat="server" ControlToValidate="txtTimebasedUnloadTime"
                Display="None" SetFocusOnError="true" ValidationGroup="a" ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$">
            </asp:RegularExpressionValidator>
            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false" Style="color: Red" ValidationGroup="a" />
        </div>
        <div class="formbox">
            <table width="40%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 5%">
                    </td>
                    <td style="font-weight: bold; width: 35%;">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5%">
                        :
                    </td>
                    <td style="width: 50%;">
                        <cc2:ucSite ID="ddlSite" runat="server" />
                    </td>
                    <td style="width: 5%">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVehicleType" runat="server" Text="Vehicle Type" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtVehicleType" runat="server" Width="220px" MaxLength="30"></cc1:ucTextbox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <cc1:ucLabel ID="lblIsNarrativeRequired" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:ucCheckbox ID="chkIsNarrativeRequired" runat="server" />
                    </td>
                    <td>
                    </td>
                </tr>
                 <tr>
                    <td>
                    </td>
                    <td>
                        <cc1:ucLabel ID="lblIsVehicleAContainer" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td>
                        <cc1:ucCheckbox ID="chkIsVehicleAContainer" runat="server" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <cc1:ucPanel ID="pnlVolumeBased" runat="server" CssClass="fieldset-form">
                <table width="60%" cellspacing="5" cellpadding="0" class="form-table">
                    <tr>
                        <td colspan="4">
                            <cc1:ucRadioButton onclick="javascript:return EnableDisableControls('1');" ID="rdoVolumeBased" runat="server" GroupName="VolumeBased"
                                Text="Volume Based" Checked="True" />
                        </td>
                    </tr>
                    <tr>
                        <td width="10%">
                            &nbsp;
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblAveragePalletsUnloadTime" runat="server" Text="Average Pallets unload time" isRequired="true"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td class="nobold" valign="top">
                            <cc1:ucTextbox ID="txtVolumeBasedPallets" onblur="javascript:return getMinutes(this,'1');" runat="server" Width="27px"
                                MaxLength="3"></cc1:ucTextbox>
                            &nbsp;<cc1:ucLabel ID="lblPerHour_1" runat="server" Text="Pallets Per Hour"></cc1:ucLabel>
                            &nbsp;&nbsp;&nbsp; &lt;<cc1:ucLabel ID="lblPalletsUnloadTime" runat="server" Text="xx"></cc1:ucLabel>&gt; &nbsp;<cc1:ucLabel
                                ID="lblPerMin_1" runat="server" Text="Minutes Per Pallet"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblAverageCartonsUnloadTime" runat="server" Text="Average Cartons unload time" isRequired="true"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td class="nobold" valign="top">
                            <cc1:ucTextbox ID="txtVolumeBasedCartons" onblur="javascript:return getMinutes(this,'2');" runat="server" Width="27px"
                                MaxLength="3"></cc1:ucTextbox>
                            &nbsp;<cc1:ucLabel ID="lblPerHour_2" runat="server" Text="Cartons Per Hour"></cc1:ucLabel>
                            &nbsp;&nbsp;&nbsp; &lt;<cc1:ucLabel ID="lblCartonsUnloadTime" runat="server" Text="xx"></cc1:ucLabel>&gt; &nbsp;<cc1:ucLabel
                                ID="lblPerMin_2" runat="server" Text="Minutes Per Cartons"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblMinimumTimePerDelivery" runat="server" Text="Minimum Time Per Delivery" isRequired="true"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td class="nobold" valign="top">
                            <cc1:ucTextbox ID="txtMinTimePerDelivery" runat="server" Width="32px" MaxLength="5"></cc1:ucTextbox>
                            &nbsp;<cc1:ucLabel ID="UcLabel2" runat="server" Text="HH:MM"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <cc1:ucRadioButton onclick="javascript:return EnableDisableControls('2');" ID="rdoTimeBased" runat="server" GroupName="VolumeBased"
                                Text="Time Based" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblTotalUnloadTime" runat="server" Text="Total unload time" isRequired="true"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td class="nobold" valign="top">
                            <cc1:ucTextbox ID="txtTimebasedUnloadTime" runat="server" Width="32px" MaxLength="5"></cc1:ucTextbox>
                            &nbsp;<cc1:ucLabel ID="UcLabel9" runat="server" Text="HH:MM"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" ValidationGroup="a" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" OnClientClick="return confirmDelete();" CssClass="button" OnClick="btnDelete_Click" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
        <input id="hidPalletsUnloadTime" type="hidden" runat="server" value="" />
        <input id="hidCartonsUnloadTime" type="hidden" runat="server" value="" />
    </div>
    <script type="text/javascript">
        function EnableDisableControls(arg) {

            var objVolumePallets = document.getElementById('<%=txtVolumeBasedPallets.ClientID %>');
            var lblPerHourPallets = document.getElementById('<%=lblPalletsUnloadTime.ClientID %>');
            var objVolumeCartons = document.getElementById('<%=txtVolumeBasedCartons.ClientID %>');
            var lblPerHourCartons = document.getElementById('<%=lblCartonsUnloadTime.ClientID %>');
            var objTotlaUnloadTime = document.getElementById('<%=txtTimebasedUnloadTime.ClientID %>');

            var objMinTimePerDelivery = document.getElementById('<%=txtMinTimePerDelivery.ClientID %>');
            var rfvMinTimePerDeliveryRequired = document.getElementById('<%=rfvMinTimePerDeliveryRequired.ClientID %>');
            var revMinTimePerDeliveryCorrectFormatRequired = document.getElementById('<%=revMinTimePerDeliveryCorrectFormatRequired.ClientID %>');

            var rfvVolumeBasedPallets = document.getElementById('<%=rfvVolumeBasedPalletsRequired.ClientID %>');
            var rfvVolumeBasedCartons = document.getElementById('<%=rfvVolumeBasedCartonsRequired.ClientID %>');
            var rfvTimebasedUnloadTime = document.getElementById('<%=rfvTimebasedUnloadTimeRequired.ClientID %>');

            var ranvVolumeBasedCartonsIntegerDataRequired = document.getElementById('<%=ranvVolumeBasedCartonsIntegerDataRequired.ClientID %>');
            var ranvVolumeBasedPalletsIntegerDataRequired = document.getElementById('<%=ranvVolumeBasedPalletsIntegerDataRequired.ClientID %>');
            
            var lblPerMin_1 = document.getElementById('<%=lblPerMin_1.ClientID %>');
            var lblPerMin_2 = document.getElementById('<%=lblPerMin_2.ClientID %>');

            if (arg == '1') {
                objVolumePallets.value = '';
                objVolumePallets.disabled = false;
                objVolumeCartons.value = '';
                objVolumeCartons.disabled = false;
                lblPerHourPallets.innerText = 'xx';
                lblPerHourCartons.innerText = 'xx';
                objTotlaUnloadTime.value = '';
                objTotlaUnloadTime.disabled = true;

                objMinTimePerDelivery.value = '';
                objMinTimePerDelivery.disabled = false;

                //Enable and disable the validators
                ValidatorEnable(rfvVolumeBasedPallets, true);
                ValidatorEnable(rfvVolumeBasedCartons, true);
                ValidatorEnable(rfvMinTimePerDeliveryRequired, true);
                ValidatorEnable(rfvTimebasedUnloadTime, false);

                ValidatorEnable(ranvVolumeBasedCartonsIntegerDataRequired, true);
                ValidatorEnable(ranvVolumeBasedPalletsIntegerDataRequired, true);
                ValidatorEnable(revMinTimePerDeliveryCorrectFormatRequired, true);
            }
            else if (arg == '2') {
                objVolumePallets.value = '';
                objVolumePallets.disabled = true;
                objVolumeCartons.value = '';
                objVolumeCartons.disabled = true;
                lblPerHourPallets.innerText = 'xx';
                lblPerHourCartons.innerText = 'xx';

                objMinTimePerDelivery.value = '';
                objMinTimePerDelivery.disabled = true;

                objTotlaUnloadTime.value = '';
                objTotlaUnloadTime.disabled = false;

                //Enable and disable the validators
                ValidatorEnable(rfvVolumeBasedPallets, false);
                ValidatorEnable(rfvVolumeBasedCartons, false);
                ValidatorEnable(rfvMinTimePerDeliveryRequired, false);
                ValidatorEnable(rfvTimebasedUnloadTime, true);

                ValidatorEnable(ranvVolumeBasedCartonsIntegerDataRequired, false);
                ValidatorEnable(ranvVolumeBasedPalletsIntegerDataRequired, false);
                ValidatorEnable(revMinTimePerDeliveryCorrectFormatRequired, false);
            }
        }

        function checkCorrectTimeFormat(sender, args) {
            var objTotlaUnloadTime = document.getElementById('<%=txtTimebasedUnloadTime.ClientID %>');
            if (objTotlaUnloadTime.value != '') {
                var arr = new Array();
                arr = objTotlaUnloadTime.value.split(':');
                if (arr.length == 2) {
                    if (arr[0] != '' && arr[1] != '') {
                        if (IsNumeric(arr[0]) && IsNumeric(arr[1])) {
                            if (parseInt(arr[0] * 1) >= 0 && parseInt(arr[1] * 1) >= 0 && parseInt(arr[1] * 1) < 60) {
                                args.IsValid = true;
                            }
                            else {
                                args.IsValid = false;
                            }
                        }
                        else {
                            args.IsValid = false;
                        }
                    }
                    else {
                        args.IsValid = false;
                    }
                }
                else {
                    args.IsValid = false;
                }
            }
        }

        function getMinutes(obj, arg) {
            if (obj.value != '') {
                if (IsNumeric(obj.value)) {
                    if (parseInt(obj.value * 1) >= 0) {
                        if (arg == '1') {
                            document.getElementById('<%=lblPalletsUnloadTime.ClientID %>').innerText = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2);
                            //document.getElementById('<%=hidPalletsUnloadTime.ClientID %>').value = roundNumber(parseFloat((60 / parseFloat(obj.value * 1))), 2);
                        }
                        else if (arg == '2') {
                            document.getElementById('<%=lblCartonsUnloadTime.ClientID %>').innerText = roundNumber(parseFloat((parseFloat(obj.value * 1)/60.0)), 2);
                            //document.getElementById('<%=hidCartonsUnloadTime.ClientID %>').value = roundNumber(parseFloat((60 / parseFloat(obj.value * 1))), 2);
                        }
                    }
                    else {
                        alert("<%=CorrectValueValidation %>");
                        return false;
                    }
                }
                else {
                    alert("<%=CorrectValueValidation %>");
                    return false;
                }
            }
        }
    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using Utilities;
using WebUtilities;
using System.Linq;
using System.Collections;

public partial class APPSIT_DoorConstraints : CommonPage
{

    protected string DoorName = WebCommon.getGlobalResourceValue("DoorName");

    public int iTimeSplit = 12;

    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ddlSite.TimeSlotWindow = "S";
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!Page.IsPostBack)
        {
            ddlSite.innerControlddlSite.AutoPostBack = true;
            hdnWeekDay.Value = "Mon";
            pnlTimeWindowtop.GroupingText = "Time Window For" + " " + "Monday";
            GenerateGrid();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private TableCell getTableCell(string text, double cellWidth, int borderWidth, int columnSpan)
    {
        TableCell tc = new TableCell();
        tc.Text = text;// lstDoorNoType[iCount - 1].DoorType.DoorType;
        if (borderWidth != 0)
            tc.BorderWidth = Unit.Pixel(borderWidth);
        if (cellWidth != 0)
            tc.Width = Unit.Percentage(cellWidth);
        if (columnSpan > 0)
            tc.ColumnSpan = columnSpan;
        return tc;
    }
    private void GenerateGrid()
    {
        #region Get WeekSetupDetail
        MASSIT_WeekSetupBE oMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
        MASSIT_WeekSetupBAL oMASSIT_WeekSetupBAL = new MASSIT_WeekSetupBAL();
        oMASSIT_WeekSetupBE.Action = "ShowAll";
        oMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_WeekSetupBE.EndWeekday = hdnWeekDay.Value;
        List<MASSIT_WeekSetupBE> lstWeekSetup = oMASSIT_WeekSetupBAL.GetWeekSetupDetailsBAL(oMASSIT_WeekSetupBE);
        #endregion

        if (lstWeekSetup.Count <= 0)
        {
            string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
            return;
        }
        else if (lstWeekSetup[0].StartTime == null)
        {
            string errorMeesage = WebCommon.getGlobalResourceValue("WeekSetupSettingNotDefined");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
            return;
        }

        #region Get DoorConstraints
        APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
        MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();
        oMASSIT_DoorOpenTimeBE.Action = "GetDoorConstraints";
        oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_DoorOpenTimeBE.Weekday = hdnWeekDay.Value;
        List<MASSIT_DoorOpenTimeBE> lstDoorConstraints = oMASSIT_DoorOpenTimeBAL.GetDoorConstraintsBAL(oMASSIT_DoorOpenTimeBE);
        #endregion

        string DoorIDs = ",";
        hdnDoorIDEndDay.Value = ",";

        for (int iCount = 0; iCount < lstDoorConstraints.Count; iCount++)
        {
            if (lstDoorConstraints[iCount].StartWeekday.ToLower() != lstDoorConstraints[iCount].Weekday.ToLower())
                DoorIDs += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";
            else
                hdnDoorIDEndDay.Value += lstDoorConstraints[iCount].SlotTimeID + "@" + lstDoorConstraints[iCount].SiteDoorNumberID + ",";
        }
        hdnDoorID.Value = DoorIDs;

        #region Get DoorNoType
        oMASSIT_DoorOpenTimeBE.Action = "GetDoorNoAndType";
        oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        List<MASSIT_DoorOpenTimeBE> lstDoorNoType = oMASSIT_DoorOpenTimeBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorOpenTimeBE);
        #endregion

        #region Table to show data

        int iDoorCount = lstDoorNoType.Count;
        double decCellWidth = (100 / (iDoorCount + 1));
        double dblCellWidth = Convert.ToDouble(Math.Ceiling(decCellWidth));

        Table tbl = new Table();
        tbl.CellSpacing = tbl.CellPadding = 0;
        tbl.CssClass = "form-tableGrid";
        tbl.Width = Unit.Percentage(100);

        pnlHeaderWindow.Controls.Add(tbl);

        TableCell tcDoorNo = getTableCell(DoorName, dblCellWidth, 1, 0);
        TableRow trDoorNo = new TableRow();
        trDoorNo.Cells.Add(tcDoorNo);

        TableCell tcDoorType = getTableCell("Door Type", dblCellWidth, 1, 0);
        TableRow trDoorType = new TableRow();
        trDoorType.Cells.Add(tcDoorType);

        TableCell tcTime = getTableCell("Time", dblCellWidth, 1, 0);
        TableRow trTime = new TableRow();
        trTime.Cells.Add(tcTime);

        //To show all doors
        for (int iCount = 1; iCount <= iDoorCount; iCount++)
        {
            tcDoorNo = getTableCell(string.Empty, dblCellWidth, 1, 0);
            Label lbl = new Label();
            lbl.Width = 60;
            lbl.Text = lstDoorNoType[iCount - 1].DoorNo.DoorNumber.ToString() + "     ";
            tcDoorNo.Controls.Add(lbl);

            CheckBox ddl = CreateAllDoorOpenCheckBox(lstDoorNoType[iCount - 1].SiteDoorNumberID);
            tcDoorNo.Controls.Add(ddl);

            Label lblOpenAll = new Label();
            lblOpenAll.Text = " Open All";
            tcDoorNo.Controls.Add(lblOpenAll);

            trDoorNo.Cells.Add(tcDoorNo);

            tcDoorType = getTableCell(lstDoorNoType[iCount - 1].DoorType.DoorType, dblCellWidth, 1, 0);
            trDoorType.Cells.Add(tcDoorType);

            tcTime = getTableCell("Open?", dblCellWidth, 1, 0);
            trTime.Cells.Add(tcTime);

        }
        trDoorNo.BackColor = trDoorType.BackColor = trTime.BackColor = System.Drawing.Color.LightGray;
        tbl.Rows.Add(trDoorNo);
        tbl.Rows.Add(trDoorType);
        tbl.Rows.Add(trTime);

        Table tblSlot = new Table();
        tblSlot.CellSpacing = tblSlot.CellPadding = 0;

        tblSlot.CssClass = "form-tableGrid";
        tblSlot.Width = Unit.Percentage(100);

        pnlTimeWindow.Controls.Add(tblSlot);

        #region Get SlotTime
        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstSlotTime = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);
        #endregion

        if (lstWeekSetup != null && lstWeekSetup.Count > 0)
        {
            hdnStartWeekDay.Value = lstWeekSetup[0].StartWeekday;
            hdnEndWeekDay.Value = lstWeekSetup[0].EndWeekday;
        }

        for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
        {
            string StartTime = lstWeekSetup[0].StartTime.Value.Hour.ToString() + "." + lstWeekSetup[0].StartTime.Value.Minute.ToString();
            string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();

            decimal dSlotTime = Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.'));
            decimal dStartTime = Convert.ToDecimal(StartTime);
            decimal dEndTime = Convert.ToDecimal(EndTime);

            if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
                dEndTime = Convert.ToDecimal("24.00");

            if (dSlotTime >= dStartTime && dSlotTime < dEndTime)
            {
                TableRow tr = new TableRow();
                TableCell tc = getTableCell(lstWeekSetup[0].StartWeekday + " - " + lstSlotTime[jCount].SlotTime, dblCellWidth, 1, 0);
                tr.Cells.Add(tc);

                for (int iCount = 1; iCount <= iDoorCount; iCount++)
                {
                    tc = getTableCell(string.Empty, dblCellWidth, 1, 0);
                    CheckBox ddl = CreateDoorOpenCheckBox(lstSlotTime[jCount].SlotTimeID, lstDoorNoType[iCount - 1].SiteDoorNumberID, lstWeekSetup[0].StartWeekday, lstWeekSetup[0].EndWeekday);
                    tc.Controls.Add(ddl);
                    tr.Cells.Add(tc);
                }
                tblSlot.Rows.Add(tr);

                if ((jCount + 1) % iTimeSplit == 0)
                {
                    tr = new TableRow();
                    tr.BackColor = System.Drawing.Color.LightGray;
                    tc = getTableCell(string.Empty, 0, 1, iDoorCount + 1);
                    tr.Cells.Add(tc);
                    tblSlot.Rows.Add(tr);
                }
            }
        }

        if (lstWeekSetup[0].StartWeekday != lstWeekSetup[0].EndWeekday)
        {
            for (int jCount = 0; jCount < lstSlotTime.Count; jCount++)
            {
                string EndTime = lstWeekSetup[0].EndTime.Value.Hour.ToString() + "." + lstWeekSetup[0].EndTime.Value.Minute.ToString();

                if (Convert.ToDecimal(lstSlotTime[jCount].SlotTime.Replace(':', '.')) < Convert.ToDecimal(EndTime))
                {
                    TableRow tr = new TableRow();
                    TableCell tc = getTableCell(lstWeekSetup[0].EndWeekday + " - " + lstSlotTime[jCount].SlotTime, dblCellWidth, 1, 0);
                    tr.Cells.Add(tc);

                    for (int iCount = 1; iCount <= iDoorCount; iCount++)
                    {
                        CheckBox ddl = CreateDoorOpenCheckBox(lstSlotTime[jCount].SlotTimeID, lstDoorNoType[iCount - 1].SiteDoorNumberID, lstWeekSetup[0].EndWeekday, lstWeekSetup[0].EndWeekday);
                        tc = getTableCell(string.Empty, dblCellWidth, 1, 0);
                        tc.Controls.Add(ddl);
                        tr.Cells.Add(tc);
                    }
                    tblSlot.Rows.Add(tr);

                    if ((jCount + 1) % iTimeSplit == 0)
                    {
                        tr = new TableRow();
                        tr.BackColor = System.Drawing.Color.LightGray;
                        tc = getTableCell(string.Empty, 0, 1, iDoorCount + 1);
                        tr.Cells.Add(tc);
                        tblSlot.Rows.Add(tr);
                    }
                }
            }
        }
        #endregion
    }

    private DropDownList CreateOpenTypeDropDown(int jCount, int iCount)
    {
        DropDownList dl = new DropDownList();
        dl.ID = "ddlOpenType" + jCount.ToString() + iCount.ToString();
        dl.Width = Unit.Pixel(50);
        dl.Items.Add("Yes");
        dl.Items.Add("No");

        //dl.AutoPostBack = true;
        //dl.SelectedIndexChanged += new EventHandler(dl_SelectedIndexChanged);
        return dl;
    }
    private CheckBox CreateAllDoorOpenCheckBox(int SiteDoorNumberID)
    {
        CheckBox chk = new CheckBox();
        chk.ID = "chkdooropenAll" + "_" + SiteDoorNumberID;
        chk.Attributes.Add("onclick", "openAllDoor(this,'" + SiteDoorNumberID + "');");
        return chk;
    }
    private CheckBox CreateDoorOpenCheckBox(int jCount, int iCount, string startDay, string endDay)
    {
        CheckBox chk = new CheckBox();
        chk.ID = "chkdooropen" + jCount.ToString() + "_" + iCount.ToString();
        chk.Attributes.Add("onclick", "addRemoveDoor(this,'" + jCount + "','" + iCount + "','" + startDay.ToLower() + "','" + endDay.ToLower() + "');");

        if (startDay.ToLower() != endDay.ToLower() && hdnDoorID.Value.Contains("," + jCount + "@" + iCount))
        {
            chk.Checked = true;
        }
        else if (startDay.ToLower() == endDay.ToLower() && hdnDoorIDEndDay.Value.Contains("," + jCount + "@" + iCount))
        {
            chk.Checked = true;
        }
        return chk;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (hdnWeekDay.Value == string.Empty)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('please select day.');", true);
            return;
        }

        string DoorConstraintIDs = hdnDoorID.Value;
        string DoorConstraintIDsForEndDay = hdnDoorIDEndDay.Value;
        APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
        MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();

        oMASSIT_DoorOpenTimeBE.Action = "Add";
        oMASSIT_DoorOpenTimeBE.DoorConstraintIDs = DoorConstraintIDs.TrimStart(new char[] { ',' }).TrimEnd(new char[] { ',' });
        oMASSIT_DoorOpenTimeBE.DoorConstraintIDsForEndDay = DoorConstraintIDsForEndDay.TrimStart(new char[] { ',' }).TrimEnd(new char[] { ',' });
        oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
        oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_DoorOpenTimeBE.Weekday = hdnWeekDay.Value;
        oMASSIT_DoorOpenTimeBE.StartWeekday = hdnStartWeekDay.Value;
        oMASSIT_DoorOpenTimeBE.EndWeekday = hdnEndWeekDay.Value;
        oMASSIT_DoorOpenTimeBAL.addEditVehicleTypeDetailsBAL(oMASSIT_DoorOpenTimeBE);

        GenerateGrid();
    }

    protected void btnWeekday_Click(object sender, EventArgs e)
    {
        hdnWeekDay.Value = ((System.Web.UI.WebControls.Button)(sender)).CommandArgument;
        pnlTimeWindowtop.GroupingText = "Time Window For" + " " + ((Button)(sender)).Text;
        GenerateGrid();
    }

    public override void SiteSelectedIndexChanged()
    {

        hdnDoorID.Value = ",";
        hdnWeekDay.Value = "Mon";
        pnlTimeWindowtop.GroupingText = "Time Window For" + " " + "Monday";
        GenerateGrid();
    }

    protected void btnSpecificDoorConstraint_Click(object sender, EventArgs e)
    {
        EncryptQueryString("APPSIT_DoorConstraintsSpecific.aspx?SiteID=" + ddlSite.innerControlddlSite.SelectedItem.Value);
    }
}
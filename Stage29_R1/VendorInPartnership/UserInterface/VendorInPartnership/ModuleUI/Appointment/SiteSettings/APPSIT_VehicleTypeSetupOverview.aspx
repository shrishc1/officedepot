﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="APPSIT_VehicleTypeSetupOverview.aspx.cs" Inherits="APPSIT_VehicleTypeSetupOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblVehicleTypeSetup" runat="server" Text="Vehicle Type Setup"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="APPSIT_VehicleTypeSetupEdit.aspx" />
        <cc2:ucExportToExcel ID="btnExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center">
                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid"
                    AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                            <ItemStyle HorizontalAlign="Left" Width="14%" />
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblSite" runat="server" Text='<%#Eval("Site.SiteName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vehicle Type" SortExpression="VehicleType">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpDelivery" runat="server" Text='<%# Eval("VehicleType") %>' NavigateUrl='<%# EncryptQuery("APPSIT_VehicleTypeSetupEdit.aspx?VehicleTypeID="+ Eval("VehicleTypeID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Narrative Required">
                            <HeaderStyle HorizontalAlign="Left" Width="8%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucCheckbox ID="NarrativeRequired" Enabled="false" runat="server" Visible='<%# Eval("IsNarrativeRequired ")%>'
                                    Checked='<%# Eval("IsNarrativeRequired ")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Is Container">
                            <HeaderStyle HorizontalAlign="Left" Width="8%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucCheckbox ID="IsContainer" Enabled="false" runat="server" Visible='<%# Eval("IsContainer ")%>'
                                    Checked='<%# Eval("IsContainer ")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="AveragePalletsunloadtimeinminutes" DataField="APP_PalletsUnloadedPerHour"
                            SortExpression="APP_PalletsUnloadedPerHour">
                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="AverageCartonsunloadtimeinminutes" DataField="APP_CartonsUnloadedPerHour"
                            SortExpression="APP_CartonsUnloadedPerHour">
                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="MinimumTimePerDelivery" DataField="MinimumTimePerDeliveryString"
                            SortExpression="MinimumTimePerDeliveryString">
                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Totalunloadtimeinhours" DataField="TotalUnloadTimeString"
                            SortExpression="TotalUnloadTimeString">
                            <HeaderStyle HorizontalAlign="Left" Width="23%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>

﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;

using Utilities; using WebUtilities;
using BaseControlLibrary;


public partial class APPSIT_CarrierProcessingWindowEdit : CommonPage {
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_InIt(object sender, EventArgs e) {
        ucSite.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e) {
        
    }

    protected void Page_PreRender(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
            if (GetQueryStringValue("SiteCarrierID") != null) {
                GetCarrierSite();
            }
        }
    }

       
    #region Methods

    protected void GetCarrier() {
        MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
        APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

        oMASSIT_CarrerProcessingWindowBE.Action = "GetCarriers";
        oMASSIT_CarrerProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_CarrerProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMASSIT_CarrerProcessingWindowBE.SiteID = !string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : 0;

        DataTable dtCarrier = oMASSIT_CarrerProcessingWindowBAL.GetCarrierDetailsBAL(oMASSIT_CarrerProcessingWindowBE,"");
        if (dtCarrier !=null && dtCarrier.Rows.Count > 0) {
            FillControls.FillDropDown(ref ddlCarrier, dtCarrier, "CarrierName", "CarrierID", "---Select---");
        }
    }

    protected void GetCarrierSite() {
        MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
        APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

        if (GetQueryStringValue("SiteCarrierID") != null) {
            
            oMASSIT_CarrerProcessingWindowBE.Action = "ShowById";
            oMASSIT_CarrerProcessingWindowBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_CarrerProcessingWindowBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMASSIT_CarrerProcessingWindowBE.SiteCarrierID = Convert.ToInt32(GetQueryStringValue("SiteCarrierID"));
            MASSIT_CarrierProcessingWindowBE lstCarrier = oMASSIT_CarrerProcessingWindowBAL.GetCarrierDetailsByIdBAL(oMASSIT_CarrerProcessingWindowBE);
            ucSite.innerControlddlSite.SelectedValue = Convert.ToString(lstCarrier.SiteID);
            lblCarrierValue.Visible = true;
            lblCarrierValue.Text = lstCarrier.Carrier.CarrierName;
            ddlCarrier.Visible = false;
            //ddlCarrier.SelectedIndex = ddlCarrier.Items.IndexOf(ddlCarrier.Items.FindByValue(lstCarrier.CarrierID.ToString()));

            hidPalletsUnloadTime.Value = lblPalletsUnloadTime.Text = String.Format("{0:N2}", lstCarrier.APP_PalletsUnloadedPerHour/60);
            hidCartonsUnloadTime.Value = lblCartonsUnloadTime.Text = String.Format("{0:N2}", lstCarrier.APP_CartonsUnloadedPerHour / 60);
            hidLineReceiptingTime.Value = lblLineReceiptingLineTime.Text = String.Format("{0:N2}", lstCarrier.APP_ReceiptingLinesPerHour / 60);
            
            txtUploadPallet.Text = Convert.ToString(lstCarrier.APP_PalletsUnloadedPerHour);            
            txtUploadCarton.Text = Convert.ToString(lstCarrier.APP_CartonsUnloadedPerHour);            
            txtReceiptTime.Text = Convert.ToString(lstCarrier.APP_ReceiptingLinesPerHour);

            //btnSave.Visible = false;
            ucSite.innerControlddlSite.Enabled = false;
            ddlCarrier.Enabled = false;
            //txtReceiptTime.Enabled = false;
            //txtUploadPallet.Enabled = false;
            //txtUploadCarton.Enabled = false;
        }
        else {
            btnDelete.Visible = false;
        }
    }

    //public string ConvertMinutesToHours(int mins) {
      
    //        int hours = (mins - mins % 60) / 60;
    //        int min = mins - hours * 60;
    //        if (hours < 10 && min < 10)

    //            return "0" + hours + ":" + "0" + min;

    //        else if (hours < 10 && min > 10)
    //            return "0" + hours + ":" + min;

    //        else if (hours > 10 && min < 10)

    //            return "" + hours + ":" + "0" + min;

    //        else
    //            return "" + hours + ":" + min;               
    //}

    //public int ConvertHHMMtoMin(string Min) {
    //    string[] splitString = Min.Split(':');
    //    return ((Convert.ToInt32(splitString[0]) * 60) + Convert.ToInt32(splitString[1]));

    //}

    #endregion
    #region Events

    protected void btnSave_Click(object sender, EventArgs e) {
        MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
        APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

        if (GetQueryStringValue("SiteCarrierID") == null)
            oMASSIT_CarrerProcessingWindowBE.Action = "Add";
        else {
            oMASSIT_CarrerProcessingWindowBE.Action = "EditCarrier";
            oMASSIT_CarrerProcessingWindowBE.SiteCarrierID = Convert.ToInt32(GetQueryStringValue("SiteCarrierID"));
        }

        oMASSIT_CarrerProcessingWindowBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
        oMASSIT_CarrerProcessingWindowBE.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
        if(txtUploadPallet.Text.IsNumeric())
            oMASSIT_CarrerProcessingWindowBE.APP_PalletsUnloadedPerHour = Convert.ToInt32(txtUploadPallet.Text);
        if (txtUploadCarton.Text.IsNumeric())
            oMASSIT_CarrerProcessingWindowBE.APP_CartonsUnloadedPerHour = Convert.ToInt32(txtUploadCarton.Text);
        if (txtReceiptTime.Text.IsNumeric())
            oMASSIT_CarrerProcessingWindowBE.APP_ReceiptingLinesPerHour = Convert.ToInt32(txtReceiptTime.Text);

        int? result=oMASSIT_CarrerProcessingWindowBAL.addEditCarrierDetailsBAL(oMASSIT_CarrerProcessingWindowBE);

        if (result == null || result == 0)
            ClientScript.RegisterStartupScript(this.GetType(), "CarrierAlreadyExists", "alert('Combination of Site & Carrier alrady exists.');", true);
        else
            EncryptQueryString("APPSIT_CarrierProcessingWindowOverview.aspx");

    }
    protected void btnDelete_Click(object sender, EventArgs e) {
        MASSIT_CarrierProcessingWindowBE oMASSIT_CarrerProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
        APPSIT_CarrierProcessingWindowBAL oMASSIT_CarrerProcessingWindowBAL = new APPSIT_CarrierProcessingWindowBAL();

        oMASSIT_CarrerProcessingWindowBE.Action = "Edit";

        if (GetQueryStringValue("SiteCarrierID") != null) {
            oMASSIT_CarrerProcessingWindowBE.SiteCarrierID = Convert.ToInt32(GetQueryStringValue("SiteCarrierID"));

            oMASSIT_CarrerProcessingWindowBAL.addEditCarrierDetailsBAL(oMASSIT_CarrerProcessingWindowBE);
        }

        EncryptQueryString("APPSIT_CarrierProcessingWindowOverview.aspx");

    }
    public override void SitePost_Load()
       {
        base.SitePost_Load();
        if (!IsPostBack) {
            txtUploadPallet.Text = "";
            txtUploadCarton.Text = "";
            txtReceiptTime.Text = "";
            ucSite.innerControlddlSite.SelectedIndex = 0;
            ucSite.innerControlddlSite.AutoPostBack = true;
            ddlCarrier.AutoPostBack = true;
            GetCarrier();
            GetCarrierSite();
        }
    }
       public override void SiteSelectedIndexChanged()
       {
           GetCarrier();
       }

    #endregion
    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("APPSIT_CarrierProcessingWindowOverview.aspx");
    }
}
﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;


public partial class APPSIT_MiscellaneousSettingsEdit : CommonPage {
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected string VendorReq = WebCommon.getGlobalResourceValue("VendorReq");
    protected string AtLeaseOneDay = WebCommon.getGlobalResourceValue("AtLeaseOneDay");
    protected string FixedSlotsGreaterThan0 = WebCommon.getGlobalResourceValue("FixedSlotsGreaterThan0");
    string strCorrectValueValidation = WebCommon.getGlobalResourceValue("CorrectValueValidation");
    
    protected void Page_InIt(object sender, EventArgs e) {        
        ucSeacrhVendor1.CurrentPage = this;
        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e) {
        
        if (!IsPostBack) {
            if (GetQueryStringValue("SiteID") != null) {
                hdnSiteID.Value = GetQueryStringValue("SiteID").ToString();
                ucSeacrhVendor1.SiteID = Convert.ToInt32(hdnSiteID.Value);
            }
            BindTimes();
            BindDoorNo();
            BindCarrier();
            BindControls();
        }
        if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro") {
            btnDelete.Visible = false;
            btnSave.Visible = false;
        }
    }


    private void BindCarrier() {
        APPCNT_CarrierBAL oMASCNT_CarrierBAL = new APPCNT_CarrierBAL();
        MASCNT_CarrierBE oMASCNT_CarrierBE = new MASCNT_CarrierBE();

        oMASCNT_CarrierBE.Action = "ShowAll";
        oMASCNT_CarrierBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASCNT_CarrierBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMASCNT_CarrierBE.Site = new MAS_SiteBE();
        oMASCNT_CarrierBE.Site.SiteID = Convert.ToInt32(hdnSiteID.Value);

        List<MASCNT_CarrierBE> lstCarrier = oMASCNT_CarrierBAL.GetCarrierDetailsBAL(oMASCNT_CarrierBE);

        FillControls.FillDropDown(ref ddlCarrier, lstCarrier, "CarrierName", "CarrierID");
    }

    private void BindControls() {
        MASSIT_FixedSlotBE oMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();
        APPSIT_FixedSlotBAL oMASSIT_FixedSlotBAL = new APPSIT_FixedSlotBAL();
         

        if (GetQueryStringValue("FixedSlotID") != null) {

            ucSeacrhVendor1.Visible = false;
            btnDelete.Visible = true;

            oMASSIT_FixedSlotBE.FixedSlotID = Convert.ToInt32(GetQueryStringValue("FixedSlotID"));
            
           if (GetQueryStringValue("BP") == null) 
                oMASSIT_FixedSlotBE.Action = "ShowAll";
           else if (GetQueryStringValue("BP") == "Pro") 
                oMASSIT_FixedSlotBE.Action = "ShowById";

            oMASSIT_FixedSlotBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_FixedSlotBE.User.UserID = Convert.ToInt32(Session["UserID"]);

            List<MASSIT_FixedSlotBE> lstFixedSlot = oMASSIT_FixedSlotBAL.GetAllFixedSlotBAL(oMASSIT_FixedSlotBE);

            if (lstFixedSlot.Count > 0) {

                if (lstFixedSlot[0].ScheduleType == "V") {
                    rdoCarrier.Checked = false;
                    rdoVendor.Checked = true;
                    lblCarrier.Visible = false;
                    lblVendor.Visible = true;
                }
                else if (lstFixedSlot[0].ScheduleType == "C") {
                    rdoCarrier.Checked = true;
                    rdoVendor.Checked = false;
                    lblVendor.Visible = false;
                    lblCarrier.Visible = true;
                }

                hdnSelectedVendorID.Value  = Convert.ToString(lstFixedSlot[0].VendorID);
                oMASSIT_FixedSlotBE.Vendor = new UP_VendorBE();
                ltvendorName.Text = lstFixedSlot[0].Vendor.VendorName;
                ddlSlotTime.SelectedIndex = ddlSlotTime.Items.IndexOf(ddlSlotTime.Items.FindByValue(lstFixedSlot[0].SlotTimeID.ToString()));
                ddlDoorNo.SelectedIndex = ddlDoorNo.Items.IndexOf(ddlDoorNo.Items.FindByValue(lstFixedSlot[0].SiteDoorNumberID.ToString()));
                chkFri.Checked = lstFixedSlot[0].Friday != null ? Convert.ToBoolean(lstFixedSlot[0].Friday) : false ;
                chkMon.Checked = lstFixedSlot[0].Monday != null ? Convert.ToBoolean(lstFixedSlot[0].Monday) : false;
                chkSat.Checked = lstFixedSlot[0].Saturday != null ? Convert.ToBoolean(lstFixedSlot[0].Saturday) : false;
                chkSun.Checked = lstFixedSlot[0].Sunday != null ? Convert.ToBoolean(lstFixedSlot[0].Sunday) : false;
                chkThurs.Checked = lstFixedSlot[0].Thursday != null ? Convert.ToBoolean(lstFixedSlot[0].Thursday) : false;
                chkTue.Checked = lstFixedSlot[0].Tuesday != null ? Convert.ToBoolean(lstFixedSlot[0].Tuesday) : false;
                chkWed.Checked = lstFixedSlot[0].Wednesday != null ? Convert.ToBoolean(lstFixedSlot[0].Wednesday) : false;
                txtMaxPallets.Text = lstFixedSlot[0].MaximumPallets != null ? Convert.ToString(lstFixedSlot[0].MaximumPallets) : "";
                txtMaxLines.Text = lstFixedSlot[0].MaximumLines != null ? Convert.ToString(lstFixedSlot[0].MaximumLines) : "";
                txtMaxCartons.Text = lstFixedSlot[0].MaximumCatrons != null ? Convert.ToString(lstFixedSlot[0].MaximumCatrons) : "";

                if (lstFixedSlot[0].AllocationType == "H")
                {
                    rdoHardAllocation.Checked = true;
                }
                else if (lstFixedSlot[0].AllocationType == "S")
                { rdoSoftAllocation.Checked = true; }
            }

            rdoCarrier.Enabled = false;
            rdoVendor.Enabled = false;

        }
    }

    

    public void BindTimes() {
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstTimes = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);
        if (lstTimes != null && lstTimes.Count > 0) {
            FillControls.FillDropDown(ref ddlSlotTime, lstTimes, "SlotTime", "SlotTimeID");           
        }
    }

    public void BindDoorNo() {
        MASSIT_DoorNoSetupBAL oMASSIT_DoorNoSetupBAL = new MASSIT_DoorNoSetupBAL();
        MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE = new MASSIT_DoorNoSetupBE();

        oMASSIT_DoorNoSetupBE.Action = "ShowAll";
        oMASSIT_DoorNoSetupBE.SiteID = Convert.ToInt32(hdnSiteID.Value);
        oMASSIT_DoorNoSetupBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMASSIT_DoorNoSetupBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oMASSIT_DoorNoSetupBE.IsActive = true;

        List<MASSIT_DoorNoSetupBE> lstDoorNo  = oMASSIT_DoorNoSetupBAL.GetDoorNoSetupDetailsBAL(oMASSIT_DoorNoSetupBE);

        FillControls.FillDropDown(ref ddlDoorNo, lstDoorNo, "DoorNumber", "SiteDoorNumberID");
        
    }
    private void checkUnloadTime()
    {
        if (GetQueryStringValue("SiteID") != null && Session["UserID"] != null)
        {
            #region Get Unload Time
            APPSIT_FixedSlotBAL oMASSIT_FixedSlotBAL = new APPSIT_FixedSlotBAL();
            MASSIT_FixedSlotBE oMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();
            oMASSIT_FixedSlotBE.Action = "GetUnloadTime";
            oMASSIT_FixedSlotBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMASSIT_FixedSlotBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            if (GetQueryStringValue("SiteID") != null)
                oMASSIT_FixedSlotBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
            if(rdoVendor.Checked)
            oMASSIT_FixedSlotBE.VendorID =hdnSelectedVendorID.Value!="" ?Convert.ToInt32(hdnSelectedVendorID.Value):(int?)null;
            if(rdoCarrier.Checked)
                oMASSIT_FixedSlotBE.CarrierID = hdnSelectedCarrierID.Value != "" ? Convert.ToInt32(hdnSelectedCarrierID.Value) : (int?)null;
            DataSet ds=oMASSIT_FixedSlotBAL.getUnloadTimeBAL(oMASSIT_FixedSlotBE);
            #endregion

            #region Variable
            decimal TotalCalculatedTime = 0;  //In Min
            bool isTimeCalculatedForPallets = false;
            bool isTimeCalculatedForCartons = false;
            int iPallets = Convert.ToInt32(txtMaxPallets.Text);
            int iCartons = Convert.ToInt32(txtMaxCartons.Text);
            #endregion
                          
            #region Step1 - Check Carrier Processing Window
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count>0 && (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false))
                {
                    DataTable dtVendor = ds.Tables[0];
                    if (dtVendor.Rows[0]["APP_PalletsUnloadedPerHour"] != DBNull.Value && Convert.ToInt32(dtVendor.Rows[0]["APP_PalletsUnloadedPerHour"]) > 0 && iPallets != 0 && isTimeCalculatedForPallets == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iPallets) * (60 / Convert.ToInt32(dtVendor.Rows[0]["APP_PalletsUnloadedPerHour"])));
                        isTimeCalculatedForPallets = true;
                    }
                    if (dtVendor.Rows[0]["APP_CartonsUnloadedPerHour"] != DBNull.Value && Convert.ToInt32(dtVendor.Rows[0]["APP_CartonsUnloadedPerHour"]) > 0 && iCartons != 0 && isTimeCalculatedForCartons == false)
                    {
                        TotalCalculatedTime += Convert.ToDecimal(Convert.ToInt32(iCartons) * (60 / Convert.ToInt32(dtVendor.Rows[0]["APP_CartonsUnloadedPerHour"])));
                        isTimeCalculatedForCartons = true;
                    }
                }
            #endregion

            #region Step2 - Check Site Miscellaneous Settings, if Vendor/Carrier unload time is not defined
                if (ds != null && ds.Tables.Count > 1 && ds.Tables[0].Rows.Count>0 && (isTimeCalculatedForPallets == false || isTimeCalculatedForCartons == false))
                    {                
                        DataTable dtMiscellaneous = ds.Tables[0];                        
                        if (dtMiscellaneous.Rows[0]["AveragePalletUnloadedPerManHour"] != DBNull.Value && Convert.ToInt32(dtMiscellaneous.Rows[0]["AveragePalletUnloadedPerManHour"]) > 0 && iPallets != 0 && isTimeCalculatedForPallets == false)
                        {
                            TotalCalculatedTime += Convert.ToDecimal((Convert.ToInt32(iPallets) / Convert.ToInt32(dtMiscellaneous.Rows[0]["AveragePalletUnloadedPerManHour"])) * 60);
                            isTimeCalculatedForPallets = true;
                        }
                        if (dtMiscellaneous.Rows[0]["AverageCartonUnloadedPerManHour"] != DBNull.Value &&  Convert.ToInt32(dtMiscellaneous.Rows[0]["AverageCartonUnloadedPerManHour"]) > 0 && iCartons != 0 && isTimeCalculatedForCartons == false)
                        {
                            TotalCalculatedTime += Convert.ToDecimal((Convert.ToInt32(iCartons) / Convert.ToInt32(dtMiscellaneous.Rows[0]["AverageCartonUnloadedPerManHour"])) * 60);
                            isTimeCalculatedForCartons = true;
                        }
                    }
                #endregion

            #region Calculate Slot Length
                int intResult = (int)TotalCalculatedTime % 15;
            int intr = (int)TotalCalculatedTime / 15;
            if (intResult > 0)
                intr++;

            //return intr;
            #endregion
        }
    }
    protected void btnSave_Click(object sender, EventArgs e) {

        if (txtMaxPallets.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtMaxPallets.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtMaxPallets.Focus();
            return;
        }

        if (txtMaxCartons.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtMaxCartons.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtMaxCartons.Focus();
            return;
        }

        if (txtMaxLines.Text.Trim() != string.Empty && !validationFunctions.IsNumeric(txtMaxLines.Text.Trim().Replace(".", string.Empty)))
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + strCorrectValueValidation + "');</script>");
            txtMaxLines.Focus();
            return;
        }
         
        APPSIT_FixedSlotBAL oMASSIT_FixedSlotBAL = new APPSIT_FixedSlotBAL();
        MASSIT_FixedSlotBE oMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();

        if (GetQueryStringValue("FixedSlotID") == null)
        {
            if (rdoVendor.Checked)
            {
                List<UP_VendorBE> oUP_VendorBEList = ucSeacrhVendor1.IsValidVendorCode();

                if (oUP_VendorBEList.Count == 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('Please enter valid Vendor No.');</script>");
                    return;
                }
                else
                {
                    hdnSelectedVendorID.Value = oUP_VendorBEList[0].VendorID.ToString();
                }

                oMASSIT_FixedSlotBE.VendorID = Convert.ToInt32(hdnSelectedVendorID.Value);
                oMASSIT_FixedSlotBE.ScheduleType = "V";

            }
            else if (rdoCarrier.Checked)
            {
                oMASSIT_FixedSlotBE.CarrierID = Convert.ToInt32(ddlCarrier.SelectedItem.Value);
                oMASSIT_FixedSlotBE.ScheduleType = "C";
            }

        }
        else
        {
            oMASSIT_FixedSlotBE.FixedSlotID = Convert.ToInt32(GetQueryStringValue("FixedSlotID").ToString());
        }

        #region Check Vendor Micellaneous Constraints
        string errorMessage = string.Empty;
        if (rdoVendor.Checked)
        {
            if (GetQueryStringValue("FixedSlotID") == null)   // when we add new fixed slot
            {
                PostVendorNo_Change();
                if (string.IsNullOrEmpty(hdnSelectedVendorID.Value))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('Vendor No. not valid');</script>");
                    return;
                }
            }

          
            #region COMMENTED FOR SPRINT - 3 [Vendor / Carrier Constraints conflict with Fixed Slot]

            //string errorDay, errorBeforeTime, errorAfterTime, errorMaxDeliveriesDay;
            //int? errorMaxPallates, errorMaxLines;

            //if (!checkVendorMicellaneousConstraintsDetals(hdnSelectedVendorID.Value, out errorDay, out errorBeforeTime, out errorAfterTime, out errorMaxPallates, out errorMaxLines, out errorMaxDeliveriesDay))
            //{
            //    errorMessage = string.Empty;
            //    if (!string.IsNullOrEmpty(errorDay))
            //        errorMessage = "Fixed Slot Details not matched with Vendor Micellaneous Constraints for day - " + errorDay;
            //    else if (!string.IsNullOrEmpty(errorBeforeTime))
            //        errorMessage = "Fixed Slot Details not matched with Vendor Micellaneous Constraint for before time";
            //    else if (!string.IsNullOrEmpty(errorAfterTime))
            //        errorMessage = "Fixed Slot Details not matched with Vendor Micellaneous Constraint for after time";
            //    else if (!string.IsNullOrEmpty(errorMaxDeliveriesDay))
            //        errorMessage = "Maximum Number Deliveries per day exceed for - " + errorMaxDeliveriesDay;
            //    else if (errorMaxPallates != null && errorMaxPallates > 0)
            //        errorMessage = "Maximum Pallets defined for this vendor is " + errorMaxPallates;
            //    else if (errorMaxLines != null && errorMaxLines > 0)
            //        errorMessage = "Maximum Lines defined for this vendor is " + errorMaxLines;
            //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('" + errorMessage + "');</script>");
            //    return;
            //}
            #endregion
        }
        #endregion
        #region Check Week-door Constraints
            APPSIT_DoorOpenTimeBAL oMASSIT_DoorOpenTimeBAL = new APPSIT_DoorOpenTimeBAL();
            MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();
            oMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
            oMASSIT_DoorOpenTimeBE.Action = "GetDoorOpenTimeConstraint";
            oMASSIT_DoorOpenTimeBE.DoorNo.SiteID = Convert.ToInt32(hdnSiteID.Value);
            List<MASSIT_DoorOpenTimeBE> lstFixedSlot = oMASSIT_DoorOpenTimeBAL.GetDoorOpenTimeBAL(oMASSIT_DoorOpenTimeBE);
            List<MASSIT_DoorOpenTimeBE> lstCheckSlot = null;
            if (chkMon.Checked)
            {
                string WeekDay = "Monday";
                string weekDayChk = WeekDay.Substring(0, 3);
                lstCheckSlot = lstFixedSlot.FindAll(delegate(MASSIT_DoorOpenTimeBE St) { return St.Weekday.ToUpper() == weekDayChk.ToUpper(); });
                if (!checkWeekDoorConstraintsDetals(lstCheckSlot, WeekDay))
                {
                    return;
                }             
            }
            if (chkTue.Checked)
            {
                string WeekDay = "Tuesday";
                string weekDayChk = WeekDay.Substring(0, 3);
                lstCheckSlot = lstFixedSlot.FindAll(delegate(MASSIT_DoorOpenTimeBE St) { return St.Weekday.ToUpper() == weekDayChk.ToUpper(); });
                if (!checkWeekDoorConstraintsDetals(lstCheckSlot, WeekDay))
                {
                    return;
                }              
            }
            if (chkWed.Checked)
            {
                string WeekDay = "Wednesday";
                string weekDayChk = WeekDay.Substring(0, 3);
                lstCheckSlot = lstFixedSlot.FindAll(delegate(MASSIT_DoorOpenTimeBE St) { return St.Weekday.ToUpper() == weekDayChk.ToUpper(); });
                if (!checkWeekDoorConstraintsDetals(lstCheckSlot, WeekDay))
                {
                    return;
                }  
            }
            if (chkThurs.Checked)
            {
                string WeekDay = "Thursday";
                string weekDayChk = WeekDay.Substring(0, 3);
                lstCheckSlot = lstFixedSlot.FindAll(delegate(MASSIT_DoorOpenTimeBE St) { return St.Weekday.ToUpper() == weekDayChk.ToUpper(); });
                if (!checkWeekDoorConstraintsDetals(lstCheckSlot, WeekDay))
                {
                    return;
                }  
            }
            if (chkFri.Checked)
            {
                string WeekDay = "Friday";
                string weekDayChk = WeekDay.Substring(0, 3);
                lstCheckSlot = lstFixedSlot.FindAll(delegate(MASSIT_DoorOpenTimeBE St) { return St.Weekday.ToUpper() == weekDayChk.ToUpper(); });
                if (!checkWeekDoorConstraintsDetals(lstCheckSlot, WeekDay))
                {
                    return;
                }  
            }
            if (chkSat.Checked)
            {
                string WeekDay = "Saturday";
                string weekDayChk = WeekDay.Substring(0, 3);
                lstCheckSlot = lstFixedSlot.FindAll(delegate(MASSIT_DoorOpenTimeBE St) { return St.Weekday.ToUpper() == weekDayChk.ToUpper(); });
                if (!checkWeekDoorConstraintsDetals(lstCheckSlot, WeekDay))
                {
                    return;
                } 
            }
            if (chkSun.Checked)
            {
                string WeekDay = "Sunday";
                string weekDayChk = WeekDay.Substring(0, 3);
                lstCheckSlot = lstFixedSlot.FindAll(delegate(MASSIT_DoorOpenTimeBE St) { return St.Weekday.ToUpper() == weekDayChk.ToUpper(); });
                if (!checkWeekDoorConstraintsDetals(lstCheckSlot, WeekDay))
                {
                    return;
                }
            }
            #endregion

            #region Get Already Booked Slots
            oMASSIT_FixedSlotBE.Action = "GetAlradyBookedFixedSlots";
            oMASSIT_FixedSlotBE.SiteID = Convert.ToInt32(hdnSiteID.Value);
            oMASSIT_FixedSlotBE.SlotTimeID = Convert.ToInt32(ddlSlotTime.SelectedItem.Value);
            oMASSIT_FixedSlotBE.SiteDoorNumberID = Convert.ToInt32(ddlDoorNo.SelectedItem.Value);
            List<MASSIT_FixedSlotBE> lstAlradyBookedFixedSlot = oMASSIT_FixedSlotBAL.GetAllFixedSlotBAL(oMASSIT_FixedSlotBE);
            List<MASSIT_FixedSlotBE> lstAvailableSlots = null;
            if (GetQueryStringValue("FixedSlotID") != null) {   //Filter current Record in case of edit
                lstAlradyBookedFixedSlot = lstAlradyBookedFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.FixedSlotID != Convert.ToInt32(GetQueryStringValue("FixedSlotID").ToString()); });
            }

            if (lstAlradyBookedFixedSlot.Count > 0) {
                //filter Slots which are alrady fixed on weekday
                if (chkMon.Checked) {                    
                    lstAvailableSlots = lstAlradyBookedFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Monday == true; });
                    if (lstAvailableSlots.Count > 0) {
                        errorMessage = "This Slot is already fixed for [" + lstAvailableSlots[0].Vendor.VendorName + "] for day - Monday";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                        return;
                    }
                }
                else if (chkTue.Checked) {
                    lstAvailableSlots = lstAlradyBookedFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Tuesday == true; });
                    if (lstAvailableSlots.Count > 0) {
                        errorMessage = "This Slot is already fixed for [" + lstAvailableSlots[0].Vendor.VendorName + "] for day - Tuesday";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                        return;
                    }
                }
                else if (chkWed.Checked) {
                    lstAvailableSlots = lstAlradyBookedFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Wednesday == true; });
                    if (lstAvailableSlots.Count > 0) {
                        errorMessage = "This Slot is already fixed for [" + lstAvailableSlots[0].Vendor.VendorName + "] for day - Wednesday";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                        return;
                    }
                }
                else if (chkThurs.Checked) {
                    lstAvailableSlots = lstAlradyBookedFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Thursday == true; });
                    if (lstAvailableSlots.Count > 0) {
                        errorMessage = "This Slot is already fixed for [" + lstAvailableSlots[0].Vendor.VendorName + "] for day - Thursday";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                        return;
                    }
                }
                else if (chkFri.Checked) {
                    lstAvailableSlots = lstAlradyBookedFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Friday == true; });
                    if (lstAvailableSlots.Count > 0) {
                        errorMessage = "This Slot is already fixed for [" + lstAvailableSlots[0].Vendor.VendorName + "] for day - Friday";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                        return;
                    }
                }
                else if (chkSat.Checked) {
                    lstAvailableSlots = lstAlradyBookedFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Saturday == true; });
                    if (lstAvailableSlots.Count > 0) {
                        errorMessage = "This Slot is already fixed for [" + lstAvailableSlots[0].Vendor.VendorName + "] for day - Saturday";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                        return;
                    }
                }
                else if (chkSun.Checked) {
                    lstAvailableSlots = lstAlradyBookedFixedSlot.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Sunday == true; });
                    if (lstAvailableSlots.Count > 0) {
                        errorMessage = "This Slot is already fixed for [" + lstAvailableSlots[0].Vendor.VendorName + "] for day - Sunday";
                        ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                        return;
                    }
                } 
            }
            #endregion          

            oMASSIT_FixedSlotBE.Action = GetQueryStringValue("FixedSlotID") == null ? "Add" : "Edit";

        oMASSIT_FixedSlotBE.SiteID = Convert.ToInt32(hdnSiteID.Value);       
        oMASSIT_FixedSlotBE.SlotTimeID = Convert.ToInt32(ddlSlotTime.SelectedItem.Value);
        oMASSIT_FixedSlotBE.SiteDoorNumberID = Convert.ToInt32(ddlDoorNo.SelectedItem.Value);
        oMASSIT_FixedSlotBE.Monday = chkMon.Checked;
        oMASSIT_FixedSlotBE.Tuesday = chkTue.Checked;
        oMASSIT_FixedSlotBE.Wednesday = chkWed.Checked;
        oMASSIT_FixedSlotBE.Thursday = chkThurs.Checked;
        oMASSIT_FixedSlotBE.Friday = chkFri.Checked;
        oMASSIT_FixedSlotBE.Saturday = chkSat.Checked;
        oMASSIT_FixedSlotBE.Sunday = chkSun.Checked;
        oMASSIT_FixedSlotBE.MaximumCatrons = txtMaxCartons.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxCartons.Text.Trim()) : (int?)null;
        oMASSIT_FixedSlotBE.MaximumLines = txtMaxLines.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxLines.Text.Trim()) : (int?)null;
        oMASSIT_FixedSlotBE.MaximumPallets = txtMaxPallets.Text.Trim() != string.Empty ? Convert.ToInt32(txtMaxPallets.Text.Trim()) : (int?)null;

        if (rdoHardAllocation.Checked)
        {
            oMASSIT_FixedSlotBE.AllocationType = "H";
        }
        else if(rdoSoftAllocation.Checked)
        {
            oMASSIT_FixedSlotBE.AllocationType = "S";
        }
        oMASSIT_FixedSlotBAL.addEdiFixedSlotBAL(oMASSIT_FixedSlotBE);

        EncryptQueryString("APPSIT_FixedSlotSetupOverview.aspx");
    }
    private bool checkVendorMicellaneousConstraintsDetals(string VendorID, out string errorDay, out string errorBeforeTime, out string errorAfterTime, out int? errorMaxPallates, out int? errorMaxLines, out string errorMaxDeliveriesDay)
    {
        bool retValue = true;
        List<MASSIT_VendorBE> lstVendor = getVendorMicellaneousConstraintsDetails(VendorID);
        APPSIT_FixedSlotBAL oMASSIT_FixedSlotBAL = new APPSIT_FixedSlotBAL();
        MASSIT_FixedSlotBE oMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();
        oMASSIT_FixedSlotBE.Action = "GetMaxDeliveriesPerDay";
        oMASSIT_FixedSlotBE.VendorID = Convert.ToInt32(VendorID);
        List<MASSIT_FixedSlotBE> lstMaxDeliveriesPerDay = oMASSIT_FixedSlotBAL.GetMaxDeliveriesPerDayBAL(oMASSIT_FixedSlotBE);

        errorDay = errorBeforeTime = errorAfterTime = errorMaxDeliveriesDay="";
        errorMaxPallates = errorMaxLines=null;
        int? errorMaxDeliveries;
        if (lstVendor != null && lstVendor.Count > 0)
        {
            for (int i = 0; i < lstVendor.Count; i++)
            {
               
                if (chkMon.Checked && Convert.ToBoolean(lstVendor[i].APP_CannotDeliveryOnMonday)) { retValue = false; errorDay = "Monday"; break; }
                else if (chkTue.Checked && Convert.ToBoolean(lstVendor[i].APP_CannotDeliveryOnTuesday)) { retValue = false; errorDay = "Tuesday"; break; }
                else if (chkWed.Checked && Convert.ToBoolean(lstVendor[i].APP_CannotDeliveryOnWednessday)) { retValue = false; errorDay = "Wednesday"; break; }
                else if (chkThurs.Checked && Convert.ToBoolean(lstVendor[i].APP_CannotDeliveryOnThrusday)) { retValue = false; errorDay = "Thursday"; break; }
                else if (chkFri.Checked && Convert.ToBoolean(lstVendor[i].APP_CannotDeliveryOnFriday)) { retValue = false; errorDay = "Friday"; break; }
                else if (chkSat.Checked && Convert.ToBoolean(lstVendor[i].APP_CannotDeliveryOnSaturday)) { retValue = false; errorDay = "Saturday"; break; }
                else if (chkSun.Checked && Convert.ToBoolean(lstVendor[i].APP_CannotDeliveryOnSunday)) { retValue = false; errorDay = "Sunday"; break; }

                if (lstVendor != null && lstVendor[i] != null && lstVendor[i].APP_CannotDeliverBefore != null && lstVendor[i].APP_CannotDeliverBefore.Value != null) {
                    if (Convert.ToDateTime(lstVendor[i].APP_CannotDeliverBefore.Value) > Convert.ToDateTime(Convert.ToDateTime(lstVendor[i].APP_CannotDeliverBefore.Value).ToShortDateString() + " " + ddlSlotTime.SelectedItem.Text)) { retValue = false; errorBeforeTime = ddlSlotTime.SelectedItem.Text; break; }
                }
                if (lstVendor != null && lstVendor[i] != null && lstVendor[i].APP_CannotDeliverAfter != null && lstVendor[i].APP_CannotDeliverAfter.Value != null) {
                    if (Convert.ToDateTime(lstVendor[i].APP_CannotDeliverAfter.Value) < Convert.ToDateTime(Convert.ToDateTime(lstVendor[i].APP_CannotDeliverAfter.Value).ToShortDateString() + " " + ddlSlotTime.SelectedItem.Text)) { retValue = false; errorAfterTime = ddlSlotTime.SelectedItem.Text; break; }
                }
              

                if (lstVendor[i] != null && lstVendor[i].APP_MaximumPallets != null && !string.IsNullOrEmpty(txtMaxPallets.Text))
                {
                    if (lstVendor[i].APP_MaximumPallets.Value< Convert.ToInt16(txtMaxPallets.Text)) { retValue = false; errorMaxPallates = lstVendor[i].APP_MaximumPallets.Value; break; }
                }
                if (lstVendor[i] != null && lstVendor[i].APP_MaximumLines != null && !string.IsNullOrEmpty(txtMaxLines.Text))
                {
                    if (lstVendor[i].APP_MaximumLines < Convert.ToInt16(txtMaxLines.Text)) { retValue = false; errorMaxLines = lstVendor[i].APP_MaximumLines.Value; break; }
                }
                if (lstVendor[i] != null && lstVendor[i].APP_MaximumDeliveriesInADay != null && GetQueryStringValue("FixedSlotID")==null)
                {
                    errorMaxDeliveries = lstVendor[i].APP_MaximumDeliveriesInADay.Value;
                    if (chkMon.Checked && lstMaxDeliveriesPerDay.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Monday == true; }).Count >= errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Monday"; break; }
                    else if (chkTue.Checked && lstMaxDeliveriesPerDay.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Tuesday == true; }).Count >= errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Tuesday"; break; }
                    else if (chkWed.Checked && lstMaxDeliveriesPerDay.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Wednesday == true; }).Count >= errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Wednesday"; break; }
                    else if (chkThurs.Checked && lstMaxDeliveriesPerDay.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Thursday == true; }).Count >= errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Thursday"; break; }
                    else if (chkFri.Checked && lstMaxDeliveriesPerDay.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Friday == true; }).Count >= errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Friday"; break; }
                    else if (chkSat.Checked && lstMaxDeliveriesPerDay.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Saturday == true; }).Count >= errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Saturday"; break; }
                    else if (chkSun.Checked && lstMaxDeliveriesPerDay.FindAll(delegate(MASSIT_FixedSlotBE St) { return St.Sunday == true; }).Count >= errorMaxDeliveries) { retValue = false; errorMaxDeliveriesDay = "Sunday"; break; }                    
                }
            }
        }
        return retValue;
    }
    private bool checkWeekDoorConstraintsDetals(List<MASSIT_DoorOpenTimeBE> lstCheckSlot, string WeekDay)        
    {
        bool retValue = true;
        List<MASSIT_DoorOpenTimeBE> lstCheckDoor = null;
        List<MASSIT_DoorOpenTimeBE> lstCheckSlotTime = null;
        string errorMessage = "";       
        if (lstCheckSlot.Count > 0)
        {
            lstCheckDoor = lstCheckSlot.FindAll(delegate(MASSIT_DoorOpenTimeBE St) { return St.SiteDoorNumberID == Convert.ToInt32(ddlDoorNo.SelectedItem.Value); });
            if (lstCheckDoor.Count > 0)
            {
                lstCheckSlotTime = lstCheckDoor.FindAll(delegate(MASSIT_DoorOpenTimeBE St) { return St.SlotTimeID == Convert.ToInt32(ddlSlotTime.SelectedItem.Value); });
                if (lstCheckSlotTime.Count == 0)
                {
                    errorMessage = WebCommon.getGlobalResourceValue("DoorClosedMsg");
                    ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                    retValue = false;
                }
            }
            else
            {
                errorMessage = WebCommon.getGlobalResourceValue("DoorSetupMsg") + WebCommon.getGlobalResourceValue(WeekDay);
                ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
                retValue = false;
            }
        }
        else
        {
            errorMessage = WebCommon.getGlobalResourceValue("WeekSetupMsg") + WebCommon.getGlobalResourceValue(WeekDay);
            ClientScript.RegisterStartupScript(this.GetType(), "alert2", "<script>alert('" + errorMessage + "');</script>");
            retValue = false;
        }      
        return retValue;
    }
    private List<MASSIT_VendorBE> getVendorMicellaneousConstraintsDetails(string VendorID)
    {
        MASSIT_VendorBE oMASSIT_VendorBE = new MASSIT_VendorBE();
        oMASSIT_VendorBE.Action = "GetVendorMicellaneousConstraints";
        oMASSIT_VendorBE.VendorID = Convert.ToInt32(VendorID);
        if(GetQueryStringValue("SiteID") != null)
            oMASSIT_VendorBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        oMASSIT_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        if(Session != null && Session["UserID"] != null)
            oMASSIT_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        APPSIT_VendorBAL oMASSIT_VendorBAL = new APPSIT_VendorBAL();
        List<MASSIT_VendorBE> lstVendor = oMASSIT_VendorBAL.GetVendorMicellaneousConstraintsDetailsBAL(oMASSIT_VendorBE);       
        return lstVendor;
    }

    protected void rdoVendor_CheckedChanged(object sender, EventArgs e) {
        ddlCarrier.Visible = false;
        ucSeacrhVendor1.Visible = true;
        lblVendor.Visible = true;
        lblCarrier.Visible = false;
    }
    protected void rdoCarrier_CheckedChanged(object sender, EventArgs e) {
        ddlCarrier.Visible = true;
        ucSeacrhVendor1.Visible = false;
        lblVendor.Visible = false;
        lblCarrier.Visible = true;
    }
    protected void btnBack_Click(object sender, EventArgs e) {
        if (GetQueryStringValue("BP") != null && GetQueryStringValue("BP") == "Pro")
            EncryptQueryString("~/ModuleUI/Appointment/ProvisionalBookings.aspx");
        if (GetQueryStringValue("SiteID") != null)
            EncryptQueryString("APPSIT_FixedSlotSetupOverview.aspx?SiteID=" + GetQueryStringValue("SiteID"));

    }
    protected void btnDelete_Click(object sender, EventArgs e) {
        APPSIT_FixedSlotBAL oMASSIT_FixedSlotBAL = new APPSIT_FixedSlotBAL();
        MASSIT_FixedSlotBE oMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();

        if (GetQueryStringValue("FixedSlotID") != null) {
            oMASSIT_FixedSlotBE.FixedSlotID = Convert.ToInt32(GetQueryStringValue("FixedSlotID").ToString());
            oMASSIT_FixedSlotBE.Action = "Delete";
            oMASSIT_FixedSlotBAL.addEdiFixedSlotBAL(oMASSIT_FixedSlotBE);

            EncryptQueryString("APPSIT_FixedSlotSetupOverview.aspx");
        }
    }

    public override void PostVendorNo_Change()
    {
        base.PostVendorNo_Change();
        hdnSelectedVendorID.Value = ucSeacrhVendor1.VendorNo;
    }
}
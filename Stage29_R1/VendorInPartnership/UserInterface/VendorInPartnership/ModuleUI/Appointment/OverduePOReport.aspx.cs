﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using Utilities;
using System.Data;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BaseControlLibrary;
using System.Collections;
using Microsoft.Reporting.WebForms;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;

public partial class ModuleUI_Appointment_OverduePOReport : CommonPage
{
    #region"page Load Event"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
        }

        if (IsPostBack)
        {
            txtFromDate.Text = hdnJSFromDt.Value;
            txtToDate.Text = hdnJSToDt.Value;
           
        }

        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvExport;
        ucExportToExcel1.FileName = "OverduePOReport";
    }
    #endregion
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    #region"btnGenerateReport_Click"
    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        try
        {
            gvVendor.PageIndex = 0;
            BindBookings();

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
       
    }
    #endregion
    
    protected void BindBookings()
    {
        APPBOK_BookingBE oAPPBOK_BookingBE = new APPBOK_BookingBE();
        APPBOK_BookingBAL oAPPBOK_BookingBAL = new APPBOK_BookingBAL();


        //oAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();  
        //oAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oAPPBOK_BookingBE.FromDate = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);
        oAPPBOK_BookingBE.ToDate = Utilities.Common.TextToDateFormat(hdnJSToDt.Value);

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
            hdnSiteIDs.Value = msSite.SelectedSiteIDs;
   
        DataTable lstBookings = new DataTable();
       //lstBookings = oAPPBOK_BookingBAL.GetBookingInqueryBAL(oAPPBOK_BookingBE);
        lstBookings.DefaultView.Sort = "DeliveryDate,OrderBY";
        gvVendor.DataSource = lstBookings;
        gvVendor.DataBind();
        gvExport.DataSource = lstBookings;
        gvExport.DataBind();
        ViewState["lstSites"] = lstBookings;
    }
    protected void gridView_Sorting(Object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortExpression = e.SortExpression;
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                GridViewSortDirection = SortDirection.Descending;
                SortGridView(sortExpression, " ASC");
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
                SortGridView(sortExpression, " DESC");
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
    private void SortGridView(string sortExpression,string direction)
    {
        DataTable dtSortingTbl;
        DataView dtView;
        try
        {
            if (ViewState["lstSites"] != null)
            {
                dtSortingTbl = (DataTable)ViewState["lstSites"];
                dtView = dtSortingTbl.DefaultView;
                //dtView =  new DataView(dtSortingTbl);
                if (sortExpression != "Weekday")
                {
                    int sortExpIndx = sortExpression.IndexOf(",");
                    if (sortExpIndx == -1)
                        dtView.Sort = sortExpression + direction;
                    else
                    {
                        dtView.Sort = sortExpression.Substring(0, sortExpIndx) + direction + sortExpression.Substring(sortExpIndx);
                    }
                }
                else
                {
                    if (direction.Trim() == "ASC")
                        dtView.Sort = "Weekday desc,OrderBY";
                    else
                        dtView.Sort = "Weekday asc,OrderBY";
                }
                if (sortExpression != "AutoID")
                    dtSortingTbl = GetSortByBooking(dtView.ToTable());
                else
                    dtSortingTbl = dtView.ToTable();

                string Role = Session["Role"].ToString().Trim().ToLower();
                GridView gridToParse = new GridView();

                gridToParse = gvVendor;

                gridToParse.DataSource = dtSortingTbl;
                gridToParse.DataBind();
                ViewState["lstSites"] = dtSortingTbl;http://localhost:4005/VendorInPartnership/ModuleUI/Appointment/Reports/DeliveryTimingIssuesReport.aspx
                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(gridToParse);
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }
    DataTable dtSortingTblByBooking = null;
    private DataTable GetSortByBooking(DataTable dtSortingTbl)
    {
        if (dtSortingTbl != null && dtSortingTbl.Rows.Count > 0)
        {
            //DataTable lstBookings = dtSortingTbl;
            if (dtSortingTblByBooking == null)
                dtSortingTblByBooking = dtSortingTbl.Clone();

            string bookingRef = dtSortingTbl.Rows[0]["BookingRef"].ToString();

            DataRow[] rows;
            rows = dtSortingTbl.Select("BookingRef = '" + bookingRef + "'");
            foreach (DataRow r in rows)
            {
                dtSortingTblByBooking.ImportRow(r);
                r.Delete();
            }
            dtSortingTblByBooking.AcceptChanges();

            GetSortByBooking(dtSortingTbl);
        }
        return dtSortingTblByBooking;
    }
    protected void gvVendor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvVendor.PageIndex = e.NewPageIndex;
        if (Session["BookingInquery"] != null)
        {
            Hashtable htBookingCriteria = (Hashtable)Session["BookingInquery"];

            htBookingCriteria.Remove("PageIndex");
            htBookingCriteria.Add("PageIndex", e.NewPageIndex);

            Session["BookingInquery"] = htBookingCriteria;
        }
        //BindBookingPageWise();
    }
}
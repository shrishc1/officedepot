﻿<%@ Page Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true"
    CodeFile="StockPlanarDiscrepancyTrackerReportEdit.aspx.cs"
    Inherits="ModuleUI_GlobalSettings_StockPlanarDiscrepancyTrackerReportEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
        <tr>
            <td style="font-weight: bold; width: 15%;">
                <cc1:ucLabel ID="lblPleaseenterSubject" runat="server" Text="Please enter Subject"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold; width: 2%;">
                <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
            </td>
            <td style="font-weight: bold; width: 83%;">
                <cc1:ucTextbox ID="txtSubjectText" runat="server" Width="400px"></cc1:ucTextbox>

                <div style="float: right">
                    <asp:LinkButton ID="Link" Text="Click to Download" OnClick="DownloadButton_Click" runat="server" />
                </div>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; width: 100%;" colspan="3">
                <cc1:ucLabel ID="lblPleaseenteremailcommunicationbelow" runat="server" Text="Please enter email communication below"></cc1:ucLabel>&nbsp;:
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; width: 100%;" colspan="3">
                <CKEditor:CKEditorControl ID="txtEmailCommunication" Height="400px" runat="server"></CKEditor:CKEditorControl>
            </td>
        </tr>
        <tr runat="server" visible="false">
            <td style="font-weight: bold; width: 15%;">
                <cc1:ucLabel ID="lblSentOnBehalfOf" runat="server" Text="Sent on Behalf of"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold; width: 2%;">
                <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
            </td>
            <td style="font-weight: bold; width: 83%;">
                <cc1:ucTextbox ID="txtSentonBehalfofText" runat="server" Width="260px"></cc1:ucTextbox>
            </td>
        </tr>

        <tr>
            <td align="right" colspan="3">
                <div class="button-row">
                    <cc1:ucButton ID="btnSubmit" runat="server" Text="Submit" CssClass="button" OnClick="btnSubmit_Click" />
                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

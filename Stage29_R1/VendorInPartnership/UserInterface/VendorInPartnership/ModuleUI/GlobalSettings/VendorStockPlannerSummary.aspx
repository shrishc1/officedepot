﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VendorStockPlannerSummary.aspx.cs"
MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="ModuleUI_GlobalSettings_VendorStockPlannerSummary" %>


<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>


    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblVendorStockPlannerSummary" runat="server" ></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center">
                <cc1:ucGridView ID="grdVendorSPSummary" Width="60%" runat="server" CssClass="grid" onsorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Country" SortExpression="Country">
                            <HeaderStyle HorizontalAlign="Left"  />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpCountry" runat="server" Text='<%# Eval("Country") %>'  NavigateUrl='<%# EncryptQuery("VendorStockPlannerDetail.aspx?CountryID="+ Eval("CountryID")) %>'></asp:HyperLink>
                          
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="# Planners" SortExpression="NoofSPUser">
                            <HeaderStyle Width="30%" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate >
                               <cc1:ucLiteral ID="ltWithContact" runat="server" Text='<%# Eval("NoofSPUser") %>'></cc1:ucLiteral>
                            </ItemTemplate>                           
                        </asp:TemplateField> 
                        

                        <asp:TemplateField HeaderText="% with Contact" SortExpression="Weight">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                 <cc1:ucLiteral ID="ltWithContact1" runat="server" Text='<%# Eval("Weight") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>

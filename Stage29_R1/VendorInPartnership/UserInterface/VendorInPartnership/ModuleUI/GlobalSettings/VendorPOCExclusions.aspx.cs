﻿using System;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.GlobalSettings;
using BusinessLogicLayer.ModuleBAL.GlobalSettings;
using System.Web.UI;

public partial class ModuleUI_GlobalSettings_VendorPOCExclusions : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            VendorPOCExclusionBAL vendorPOCExclusionBAL = new VendorPOCExclusionBAL();
            VendorPOCExclusionBE vendorPOCExclusionBE = new VendorPOCExclusionBE();
            vendorPOCExclusionBE.Action = "GetAllVendorPOCExclusion";
            gvVendorPOC.DataSource= vendorPOCExclusionBAL.GetVendorPOCExclusionBAL(vendorPOCExclusionBE);
            gvVendorPOC.DataBind();
        }

        ucExportToExcel1.FileName = "AllVendorPOCExclusion";
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvVendorPOC;
    }
}
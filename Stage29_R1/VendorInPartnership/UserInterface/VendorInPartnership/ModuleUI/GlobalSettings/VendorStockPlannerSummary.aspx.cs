﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.GlobalSettings;
using BusinessLogicLayer.ModuleBAL.GlobalSettings;

public partial class ModuleUI_GlobalSettings_VendorStockPlannerSummary : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = grdVendorSPSummary;
        // ucExportToExcel1.IsAutoGenratedGridview = true;
        ucExportToExcel1.FileName = "VendorStockPlannerSummary";

        if (!IsPostBack)
        {
            BindVendorStockPlannerSummary();
        }
    }

    protected void BindVendorStockPlannerSummary()
    {
        PointsofContactSummaryBE SPContactBE = new PointsofContactSummaryBE();
        PointsofContactSummaryBAL SPContactBAL = new PointsofContactSummaryBAL();

        SPContactBE.Action = "ShowStockPlannerSummary";

        List<PointsofContactSummaryBE> lstSPContactSummary = SPContactBAL.GetVendorSPSummaryBAL(SPContactBE);

        if (lstSPContactSummary != null && lstSPContactSummary.Count > 0)
        {
            grdVendorSPSummary.DataSource = lstSPContactSummary;
            grdVendorSPSummary.DataBind();
            ViewState["lstSPContactSummary"] = lstSPContactSummary;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<PointsofContactSummaryBE>.SortList((List<PointsofContactSummaryBE>)ViewState["lstSPContactSummary"], e.SortExpression, e.SortDirection).ToArray();
    }


}
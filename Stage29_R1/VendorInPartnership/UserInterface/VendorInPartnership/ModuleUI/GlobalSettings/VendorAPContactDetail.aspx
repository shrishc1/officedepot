﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VendorAPContactDetail.aspx.cs" 
MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"  Inherits="ModuleUI_GlobalSettings_VendorAPContactDetail" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc4" %>


    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <script type="text/javascript">

        $(document).ready(function () {

            
           
            $("input[type='checkbox'][name$='chkSelectAllText']").click(function () {

                var table = $(this).parent().parent().parent().parent().parent();
                if ($(this).is(":checked")) {
                    $.each(table.find("input[type='checkbox']:gt(0)"), function (index, element) {
                        if ($(this).attr("disabled") == false) {
                            $(this).attr("checked", "checked");
                        }
                    });
                }
                else {
                    table.find("input[type='checkbox']:gt(0)").removeAttr('checked');
                }
            });

            $("[id*=chkRow]").live("click", function () {
                var grid = $(this).closest("table");
                var chkHeader = $("[id*=chkSelectAllText]", grid);
                if (!$(this).is(":checked")) {
                    $("td", $(this).closest("tr")).removeClass("selected");
                    chkHeader.removeAttr("checked");
                } else {
                    $("td", $(this).closest("tr")).addClass("selected");
                    if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                        chkHeader.attr("checked", "checked");
                    }
                }
            });

        });   
</script>

    <h2>
        <cc1:ucLabel ID="lblVendorAPContactDetail" runat="server" ></cc1:ucLabel>
    </h2>
   
     <div>      
        <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
            <tr >
                 <td style="font-weight: bold; width:15%;">
                    <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td style="width:34%">
                    <uc1:ucCountry ID="ddlCountry" runat="server"   />
                </td>
                <td style="font-weight: bold; width:15%;">
                    <cc1:ucLabel ID="lblStatus" runat="server" Text="Status"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td  style="font-weight: bold;width:34%">
                     <asp:DropDownList ID="ddlAPStatus" runat="server" Width="120px">
                                    <asp:ListItem Text="ALL" Value="0" />
                                    <asp:ListItem Text="NO CONTACT" Value="1" />
                     </asp:DropDownList>
      &nbsp;&nbsp;   
      <%--<div class="button-row" style="float: right;">--%>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <cc1:ucButton ID="btnGo" Text="Go" runat="server" CssClass="button" 
                             onclick="btnGo_Click" />
                    <%-- </div>--%>
                </td>
            </tr>
            <tr >
                <td style="font-weight: bold; width:15%;">
                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;">
                    :
                </td>
                <td colspan="5">
                    <cc3:ucSeacrhVendor runat="server" ID="ddlSeacrhVendor" Visible="false"  />
                    <cc4:MultiSelectVendor runat="server" ID="msVendor" EnableViewState="true" />
                </td>
            </tr>
        </table>
    </div>
     <div class="button-row">
     
        <cc1:ucButton  id="btnBack" CssClass="button"  runat="server" 
            onclick="btnBack_Click" />
    
        &nbsp;&nbsp;<cc2:ucexporttoexcel ID="ucExportToExcel1" runat="server" />
    </div>
    <div id="AssignAPClerk" runat="server" visible="false">
    <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
            <tr  runat="server">
                 <td style="font-weight: bold; width:10%;">
                    <cc1:ucLabel ID="lblAPClerk" runat="server" Text="AP Clerk"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td style="width:74%">
                   <cc1:ucDropdownList ID="ddlAPClerk" runat="server"></cc1:ucDropdownList> 
                                          
      &nbsp;&nbsp;
      <%--<div class="button-row" style="float: right;">--%>
                        <cc1:ucButton ID="btnAssignAPClerk" Text="Assign AP Clerk" runat="server" 
                            CssClass="button" onclick="btnAssignAPClerk_Click" />
                <%--   </div>--%>
                </td>
             </tr>
        </table>
    </div>
    </br>
    <cc1:ucPanel ID="pnlStatusALL" runat="server">
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="grdAPStatusAll" Width="90%" runat="server" CssClass="grid"
                     AllowSorting="true" OnSorting="SortGrid" >
                     <emptydatatemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblRecordNotFound" runat="server" Text="No record found"></cc1:ucLabel>
                                    </div>
                                </emptydatatemplate>
                    <Columns>
                       <asp:TemplateField HeaderText="Vendor" SortExpression="VendorName">
                            <HeaderStyle HorizontalAlign="Left" Width="25%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                 <cc1:ucLabel ID="lblVendorNew" runat="server" Text='<%# Eval("VendorName") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Lines" SortExpression="NoOfLines">
                            <HeaderStyle  HorizontalAlign="Left" Width="6%" />
                            <ItemTemplate>
                                   <cc1:ucLiteral ID="ltlines" runat="server" Text='<%# Eval("NoOfLines") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField> 
                        

                        <asp:TemplateField HeaderText="% Weight" SortExpression="Weight">
                            <HeaderStyle HorizontalAlign="center" Width="12%" />
                            <ItemStyle HorizontalAlign="center" Width="12%" />
                            <ItemTemplate>
                                 <cc1:ucLiteral ID="ltWeight" runat="server" Text='<%# Eval("Weight") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Contact Name" SortExpression="ContactName">
                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                 <cc1:ucLabel ID="lblContactName" runat="server" Text='<%# Eval("ContactName") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Tel #" SortExpression="VendorPhoneNumber">
                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                 <cc1:ucLiteral ID="ltPhoneNo" runat="server" Text='<%# Eval("VendorPhoneNumber") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
    </cc1:ucPanel>
     <cc1:ucPanel ID="pnlStatusNoContact" runat="server">
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="grdAPStatusNoContact" Width="90%" runat="server" CssClass="grid"
                     AllowSorting="true" OnSorting="SortGrid" >
                     <emptydatatemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblRecordNotFound" runat="server" Text="No record found"></cc1:ucLabel>
                                    </div>
                                </emptydatatemplate>
                    <Columns>
                    <asp:TemplateField HeaderText="SelectAllCheckBox">
                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                    <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderTemplate>
                                        <cc1:ucCheckbox runat="server" ID="chkSelectAllText" CssClass="checkbox-input"  />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkRow"   />
                                        <asp:HiddenField ID="hdnVendorID" runat="server" Value='<%# Eval("VendorID") %>' />
                                    </ItemTemplate>
                       </asp:TemplateField>

                       <asp:TemplateField HeaderText="Vendor" SortExpression="VendorName">
                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                 <cc1:ucLabel ID="lblVendor" runat="server" Text='<%# Eval("VendorName") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Lines" SortExpression="NoOfLines">
                            <HeaderStyle  HorizontalAlign="Left" Width="20%" />
                            <ItemTemplate>
                                   <cc1:ucLiteral ID="ltlines" runat="server" Text='<%# Eval("NoOfLines") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField> 
                        

                        <asp:TemplateField HeaderText="% Weight" SortExpression="Weight">
                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                 <cc1:ucLiteral ID="ltWeight" runat="server" Text='<%# Eval("Weight") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                       
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
    </cc1:ucPanel>
     
</asp:Content>
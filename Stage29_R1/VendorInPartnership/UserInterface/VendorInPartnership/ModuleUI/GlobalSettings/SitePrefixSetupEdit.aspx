﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="SitePrefixSetupEdit.aspx.cs" Inherits="SitePrefixSetupEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucCountrySite.ascx" TagName="ucCountry" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=UnDeclaredAsBannerMessage%>');
        }
        function checkSitesSelected(source, args) {
            var lstSelectedSites = document.getElementById('<%=UclstSiteSelected.ClientID%>');
            if (lstSelectedSites.options.length < 2) {
                args.IsValid = false;
            }
        }
    </script>
    <h2>
        <cc1:ucLabel ID="lblSiteSetup" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table id="trEdit" runat="server" style="display: none" width="100%" cellspacing="5"
                cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 110px">
                        <cc1:ucLabel ID="lblSiteNumber" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5px;">
                        <cc1:ucLabel ID="Label3" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="width: 290px">
                        <cc1:ucLabel ID="lblSiteNumberValue" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 440px">
                    </td>
                    <td style="font-weight: bold; width: 5px;">
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <table id="trAdd" runat="server" width="30%" cellspacing="5" cellpadding="0" border="0"
                align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold;" width="5%">
                        <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" width="5%">
                        :
                    </td>
                    <td width="30%">
                        <cc1:ucListBox ID="UclstSiteList" runat="server" Height="150px" Width="90%">
                        </cc1:ucListBox>
                    </td>
                    <td valign="middle" align="center" width="30%">
                        <div>
                            <cc1:ucButton ID="btnMoveRightAll" runat="server" Text=">>" CssClass="button" Width="35px"
                                OnClick="btnMoveRightAll_Click" /></div>
                        &nbsp;
                        <div>
                            <cc1:ucButton ID="btnMoveRight" runat="server" Text=">" CssClass="button" Width="35px"
                                OnClick="btnMoveRight_Click" /></div>
                        &nbsp;
                        <div>
                            <cc1:ucButton ID="btnMoveLeft" runat="server" Text="<" CssClass="button" Width="35px"
                                OnClick="btnMoveLeft_Click" /></div>
                        &nbsp;
                        <div>
                            <cc1:ucButton ID="btnMoveLeftAll" runat="server" Text="<<" CssClass="button" Width="35px"
                                OnClick="btnMoveLeftAll_Click" /></div>
                    </td>
                    <td width="30%">
                        <cc1:ucListBox ID="UclstSiteSelected" runat="server" Height="150px" Width="90%">
                        </cc1:ucListBox>
                        <asp:CustomValidator ID="cusvNoSelectedSites" runat="server" ClientValidationFunction="checkSitesSelected"
                            ValidationGroup="a" Display="None"></asp:CustomValidator>
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblSiteName" runat="server" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="Label1" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucTextbox ID="txtSiteName" runat="server" Width="250px" MaxLength="30"></cc1:ucTextbox>
                        <asp:RequiredFieldValidator ID="rfvSiteNameRequired" runat="server" ControlToValidate="txtSiteName"
                            Display="None" ValidationGroup="a">
                        </asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <cc1:ucLabel ID="lblSitePrefix" runat="server" Style="font-weight: 700" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="Label9" runat="server">:</cc1:ucLabel>
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtSitePrefix" runat="server" Width="47px" MaxLength="3"></cc1:ucTextbox>
                        <asp:RequiredFieldValidator ID="rfvSitePrefixRequired" runat="server" ControlToValidate="txtSitePrefix"
                            Display="None" ValidationGroup="a">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblPhoneNumbers" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtPhoneNumbers" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblFaxNumbers" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="Label7" runat="server">:</cc1:ucLabel>
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtFaxNumbers" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblEmail" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="Label5" runat="server">:</cc1:ucLabel>
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtEmail" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                        <asp:RegularExpressionValidator ID="revEmailreq" ControlToValidate="txtEmail" runat="server" ValidationGroup="a"
                            ErrorMessage="Please enter Correct User ID/Email" ValidationExpression="^[\w-\.\']{1,}\@([\da-zA-Z-]{1,}\.){1,3}[\da-zA-Z-]{2,6}$" />
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblManagerName" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="Label8" runat="server">:</cc1:ucLabel>
                    </td>
                    <td>
                        <cc1:ucDropdownList ID="ddlManagerName" runat="server" Width="150px">
                        </cc1:ucDropdownList>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblCurrency" runat="server" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td >
                         <cc1:ucDropdownList ID="ddlCurrency" runat="server" Width="150px">
                        </cc1:ucDropdownList>
                        <asp:RequiredFieldValidator ID="rfvCurrencyRequired" runat="server" ControlToValidate="ddlCurrency"
                            Display="None" ValidationGroup="a" InitialValue="0" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                            Style="color: Red" ValidationGroup="a" />
                    </td>
                     <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblOfficeHours" Text="Office Hours"
                            runat="server" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td  >
                       <cc1:ucTextbox ID="txtOfficeHour" runat="server"></cc1:ucTextbox>  
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td width="50%">
                        <cc1:ucPanel ID="pnlSiteAddress" runat="server" GroupingText="Site Address" CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblAddress1_1" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="Label6" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtAddress1" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblAddress2_1" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtAddress2" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblAddress3_1" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtAddress3" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblAddress4_1" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtAddress4" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblAddress5_1" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel10" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtAddress5" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblAddress6_1" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel12" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtAddress6" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblCountry_1" runat="server" Text="Country"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <uc1:ucCountry ID="ucSiteCountry" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblPostCode_1" runat="server" Text="PostCode"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtPostalCode" runat="server" Width="250px" MaxLength="15"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                    <td width="50%">
                        <cc1:ucPanel ID="pnlAccountPaybleAddress" runat="server" GroupingText="Account Payble Address"
                            CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblAddress1_2" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel9" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtAPAddress1" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblAddress2_2" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel13" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtAPAddress2" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblAddress3_2" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel15" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtAPAddress3" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblAddress4_2" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel17" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtAPAddress4" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblAddress5_2" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel19" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtAPAddress5" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblAddress6_2" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel21" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtAPAddress6" runat="server" Width="250px" MaxLength="100"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblCountry_2" runat="server" Text="Country"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel23" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <uc1:ucCountry ID="ucAPCountry" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblPostCode_2" runat="server" Text="PostCode"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="UcLabel25" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td colspan="4">
                                        <cc1:ucTextbox ID="txtAPPostcode" runat="server" Width="250px" MaxLength="15"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 1%">
                        <cc1:ucLabel ID="lblRemarks_1" runat="server" Text="Remarks"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <cc1:ucTextbox TextMode="MultiLine" Rows="2" ID="txtRemarks" runat="server" Width="90%"
                            MaxLength="500"></cc1:ucTextbox>
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblCostCenter" runat="server" Text="Cost Centre"></cc1:ucLabel>
                    </td>                    
                </tr>
                <tr>
                    <td colspan="2">
                        <cc1:ucTextbox ID="txtCostCenter" runat="server" Width="20%"></cc1:ucTextbox>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click"
            ValidationGroup="a" />
        <cc1:ucButton ID="btnUnDeclaredAsBanner" runat="server" OnClientClick="return confirmDelete();"
            CssClass="button" OnClick="btnDelete_Click" />
        <cc1:ucButton ID="btnBack" runat="server" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

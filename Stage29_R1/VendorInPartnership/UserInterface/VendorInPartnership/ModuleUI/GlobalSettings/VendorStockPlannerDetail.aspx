﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VendorStockPlannerDetail.aspx.cs"
MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="ModuleUI_GlobalSettings_VendorStockPlannerDetail" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc4" %>


    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
//        $(document).ready(function () {
//            $('#ctl00_ContentPlaceHolder1_ucExportToExcel1_btnExportToExcel').val('Export To Excel');
//        });
//       
</script>

    <h2>
        <cc1:ucLabel ID="lblVendorStockPlannerDetail" runat="server" ></cc1:ucLabel>
    </h2>
   
     <div>      
        <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
            <tr >
                 <td style="font-weight: bold; width:15%;">
                    <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td style="width:34%">
                    <uc1:ucCountry ID="ddlCountry" runat="server"   />
                </td>
                <td style="font-weight: bold; width:15%;">
                    <cc1:ucLabel ID="lblStatus" runat="server" Text="Status"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td  style="font-weight: bold;width:34%">
                     <asp:DropDownList ID="ddlAPStatus" runat="server" Width="120px">
                                    <asp:ListItem Text="ALL" Value="1" />
                                    <asp:ListItem Text="NO PLANNER" Value="0" />
                     </asp:DropDownList>
      &nbsp;&nbsp;   
      <%--<div class="button-row" style="float: right;">--%>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <cc1:ucButton ID="btnGo" Text="Go" runat="server" CssClass="button" 
                             onclick="btnGo_Click" />
                    <%-- </div>--%>
                </td>
            </tr>
            <tr >
                <td style="font-weight: bold; width:15%;">
                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;">
                    :
                </td>
                <td colspan="5">
                    <cc3:ucSeacrhVendor runat="server" ID="ddlSeacrhVendor" Visible="false"  />
                    <cc4:MultiSelectVendor runat="server" ID="msVendor" EnableViewState="true" />
                </td>
            </tr>
        </table>
    </div>
     <div class="button-row">
     <cc1:ucButton  id="btnBack" CssClass="button"  runat="server" 
            onclick="btnBack_Click" />&nbsp;&nbsp;
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    
    <table width="100%">
        <tr>
            <td align="center" >
                <cc1:ucGridView ID="grdStockPlannerDetail" Width="90%" runat="server" CssClass="grid"
                     AllowSorting="true" OnSorting="SortGrid" >
                     <emptydatatemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblRecordNotFound" runat="server" Text="No record found"></cc1:ucLabel>
                                    </div>
                     </emptydatatemplate>
                    <Columns>
                       <asp:TemplateField HeaderText="Vendor" SortExpression="VendorName">
                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                            <asp:HyperLink ID="hplVendorName" runat="server" Text='<%# Eval("VendorName") %>'  NavigateUrl='<%# EncryptQuery("VendorEdit.aspx?VendorID="+ Eval("VendorID")+"&SPVendorID=" + Eval("VendorName") ) %>'></asp:HyperLink>
                                 
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Lines" SortExpression="NoOfLines">
                            <HeaderStyle  HorizontalAlign="Left" Width="10%" />
                            <ItemTemplate>
                                   <cc1:ucLiteral ID="ltlinesNew" runat="server" Text='<%# Eval("NoOfLines") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField> 
                        

                        <asp:TemplateField HeaderText="% Weight" SortExpression="Weight">
                            <HeaderStyle HorizontalAlign="center" Width="20%" />
                            <ItemStyle HorizontalAlign="center" Width="15%"/>
                            <ItemTemplate>
                                 <cc1:ucLiteral ID="ltWeightNew" runat="server" Text='<%# Eval("Weight") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Planner #" SortExpression="StockPlannerNo">
                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                 <cc1:ucLabel ID="lblPlannerNo" runat="server" Text='<%# Eval("StockPlannerNo") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Planner Name" SortExpression="ContactName">
                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                 <cc1:ucLiteral ID="ltPlannerName" runat="server" Text='<%# Eval("ContactName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Planner Tel #" SortExpression="VendorPhoneNumber">
                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                 <cc1:ucLiteral ID="ltPlannerPhoneNo" runat="server" Text='<%# Eval("VendorPhoneNumber") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>

    
   
   
</asp:Content>
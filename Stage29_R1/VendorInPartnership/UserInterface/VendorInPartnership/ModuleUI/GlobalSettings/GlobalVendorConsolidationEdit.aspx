﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" CodeFile="GlobalVendorConsolidationEdit.aspx.cs"
    Inherits="GlobalVendorConsolidationEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry" TagPrefix="uc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucSeacrhnSelectVendorNew.ascx" TagName="ucSeacrhnSelectVendorNew"
    TagPrefix="cc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblGlobalVendorConsolidation" runat="server" Text="Global Vendor Consolidation"></cc1:ucLabel>
    </h2>
    <script language="javascript" type="text/javascript">
        function checkVendorSelected(source, args) {
            if (document.getElementById('<%=ucSeacrhVendor1.FindControl("lstSelectedVendor").ClientID%>') != null) {
                var lstSelectedProductCode = document.getElementById('<%=ucSeacrhVendor1.FindControl("lstSelectedVendor").ClientID%>');

                if (lstSelectedProductCode.options.length < 1) {

                    args.IsValid = false;

                }
            }
        }

        function DeleteConfirmation() {
            if (confirm('<%=DeleteConsoConf%>')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <asp:ScriptManager ID="SM" runat="server">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 15%">
                        <cc1:ucLabel ID="lblConsVendorCode" runat="server" Text="Cons. Vendor Code"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 84%">
                        <cc1:ucTextbox ID="txtConsVendorCode" runat="server" Width="60px" MaxLength="15"></cc1:ucTextbox>
                        <asp:RequiredFieldValidator ID="rfvConsVendorCodeReq" runat="server" ControlToValidate="txtConsVendorCode"
                            Display="None" ValidationGroup="a">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revConsVendorCodeValidate" runat="server" ErrorMessage="please input alphabets/numbers only."
                            ControlToValidate="txtConsVendorCode" ValidationExpression="^[a-zA-Z0-9]+$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblConsVendorName" runat="server" Text="Cons Vendor Name"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="UcLabel10" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox ID="txtConsVendorNam" runat="server" Width="150px" MaxLength="200"></cc1:ucTextbox>
                        <asp:RequiredFieldValidator ID="rfvConsVendorNamReq" runat="server" ControlToValidate="txtConsVendorNam"
                            Display="None" ValidationGroup="a">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revConsVendorNamValidate" runat="server" ErrorMessage="please input alphabets only."
                            ControlToValidate="txtConsVendorNam" ValidationExpression="^[a-zA-Z0-9 ]+$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                    </td>
                    <td>
                        <cc4:ucSeacrhnSelectVendorNew ID="ucSeacrhVendor1" runat="server" />
                        <asp:CustomValidator ID="cusvNoSelectedVendor" runat="server" ClientValidationFunction="checkVendorSelected"
                            ValidationGroup="a" Display="None"></asp:CustomValidator>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <div class="button-row">
                <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" OnClientClick="return DeleteConfirmation();" CssClass="button" OnClick="btnDelete_Click"/>
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
                    ValidationGroup="a" />
                <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                    Style="color: Red" ValidationGroup="a" />
                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;

   public partial class ModuleUI_GlobalSettings_SiteEnabledForSchedulingSettings : CommonPage
{
    #region Declarations ...

    protected string SiteSchedulingSettingsSaveMsg = WebCommon.getGlobalResourceValue("SiteSchedulingSettingsSaveMsg");
    List<MAS_VendorBE> lstVendor = new List<MAS_VendorBE>();
    
    #endregion Declarations ...

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            BindSiteSchedulingData();
        }
        ucExportToExcel1.GridViewControl = gvSiteEnableForScheduling;
        ucExportToExcel1.FileName = "Site Enabled For Scheduling";
    }

    public void BindSiteSchedulingData()
    {
        MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
        MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();
        List<MAS_VendorBE> lstVendor = new List<MAS_VendorBE>();

        oMAS_VendorBE.Action="GetSiteSchedulingData";

        lstVendor = oMAS_VendorBAL.GetSiteSchedulingDataBAL(oMAS_VendorBE);

        if (lstVendor != null && lstVendor.Count > 0)
        {
            gvSiteEnableForScheduling.DataSource = lstVendor;
            gvSiteEnableForScheduling.DataBind();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
        MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();

        oMAS_VendorBE.Action = "UpdateSiteSchedulingAppSettings";

        foreach (GridViewRow row in gvSiteEnableForScheduling.Rows)
        {
            CheckBox chk = row.Cells[2].FindControl("chkSiteSchedulingEnabled") as CheckBox;
            HiddenField hdnSiteID = (HiddenField)row.FindControl("hdnSiteID");
            if (chk.Checked)
            {
                oMAS_VendorBE.SiteID = Convert.ToInt32((row.Cells[1].FindControl("hdnSiteID") as HiddenField).Value);
                oMAS_VendorBE.IsSiteSchedulingEnabled = true;               
            }
            else 
            {
                oMAS_VendorBE.SiteID = Convert.ToInt32((row.Cells[1].FindControl("hdnSiteID") as HiddenField).Value);
                oMAS_VendorBE.IsSiteSchedulingEnabled = false;
            }

            CheckBox chkEnableHazardouesItemPrompt = row.Cells[2].FindControl("checkBoxEnableHazardouesItemPrompt") as CheckBox;
            if (chkEnableHazardouesItemPrompt.Checked)
            {
                oMAS_VendorBE.IsEnableHazardouesItemPrompt = true;
            }
            else
            {              
                oMAS_VendorBE.IsEnableHazardouesItemPrompt = false;
            }


            int? result =  oMAS_VendorBAL.UpdateSiteSchedulingSettingsBAL(oMAS_VendorBE);
          if (result == 1)
          {
              ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + SiteSchedulingSettingsSaveMsg + "')", true);
          }

        }
        BindSiteSchedulingData();
    }
}
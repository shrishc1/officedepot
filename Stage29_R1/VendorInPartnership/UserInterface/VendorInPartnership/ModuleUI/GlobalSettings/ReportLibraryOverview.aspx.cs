﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Text;
using BaseControlLibrary;
using Utilities;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;


public partial class ReportLibraryOverview : CommonPage
{
    #region Declarations ...    
    readonly string ReportLibViewValidate = WebCommon.getGlobalResourceValue("ReportLibViewValidate");
    #endregion

    #region Properties

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["GridViewSortDirection"] == null)
                ViewState["GridViewSortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["GridViewSortDirection"];
        }
        set { ViewState["GridViewSortDirection"] = value; }
    }

    public string SortDir
    {
        get
        {
            return GridViewSortDirection == SortDirection.Ascending ? "DESC" : "ASC";
        }
    }

    public string GridViewSortExp
    {
        get
        {
            if (ViewState["GridViewSortExp"] == null)
                ViewState["GridViewSortExp"] = "ModuleName";
            return (string)ViewState["GridViewSortExp"];
        }
        set { ViewState["GridViewSortExp"] = value; }
    }

    #endregion

    #region Events ...

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.BindReportLibrary();
        }

        btnExportToExcel1.CurrentPage = this;
        btnExportToExcel1.GridViewControl = gvReportLibrary;
        btnExportToExcel1.FileName = "ReportLibraryOverview";
    }

    protected void gvReportLibrary_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("CHECKREPORT"))
        {
            var row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            var hdnScreenUrlText = (HiddenField)row.FindControl("hdnScreenUrlText");
            var hdnScreenIdText = (HiddenField)row.FindControl("hdnScreenIdText");
            if (hdnScreenUrlText != null && hdnScreenIdText != null)
            {
                var templateBAL = new SCT_TemplateBAL();
                var templateBE = new SCT_TemplateBE();
                templateBE.Action = "GetTemplateForReportLib";

                if (Session["UserID"] != null)
                    templateBE.UserId = Convert.ToInt32(Session["UserID"]);

                if (Session["UserRoleId"] != null)
                    templateBE.RoleId = Convert.ToInt32(Session["UserRoleId"]);

                if (hdnScreenIdText.Value != string.Empty)
                    templateBE.ScreenID = Convert.ToInt32(hdnScreenIdText.Value);

                var getTemplateData = templateBAL.GetTemplateForReportLibBAL(templateBE);
                if (getTemplateData != null)
                {
                    if (Convert.ToInt32(getTemplateData.TemplateScreenID) > 0 || Convert.ToInt32(getTemplateData.TemplateID) > 0)
                    {
                        EncryptQueryString(hdnScreenUrlText.Value);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReportLibViewValidate + "')", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReportLibViewValidate + "')", true);
                    return;
                }
            }
        }
    }

    #endregion

    #region Methods ...

    private void BindReportLibrary()
    {
        var reportLibraryBE = new ReportLibraryBE();
        var reportLibraryBAL = new ReportLibraryBAL();
        reportLibraryBE.Action = "GetAllReportLibrary";
        var lstReportLibrary = reportLibraryBAL.GetReportLibraryBAL(reportLibraryBE);

        #region Logic to set the multilingual report name ...
        foreach(var reportLibrary in lstReportLibrary)
        { 
            reportLibrary.ReportName = this.GetReportName(reportLibrary.ScreenName);            
            reportLibrary.Description = this.GetDescription(reportLibrary.ScreenName);
            reportLibrary.FurtherInfo = this.GetFurtherInfo(reportLibrary.ScreenName);
            if(reportLibrary.ScreenName == "StockPlannerOTIFReport")
            {
                reportLibrary.ModuleName = this.GetModuleName("OTIFStockOverview");
            }
            else
            {
                string module = this.GetModuleName(reportLibrary.ModuleName);
                reportLibrary.ModuleName = !string.IsNullOrEmpty(module) ? module : WebCommon.getGlobalResourceValue(reportLibrary.ModuleName); ;
            }                
        }
        #endregion

        ViewState["ReportLibrary"] = lstReportLibrary;
        gvReportLibrary.DataSource = lstReportLibrary;
        gvReportLibrary.DataBind();
    }
    private string GetReportName(string reportText)
    {
        reportText = reportText.Trim().ToUpper();
        string reportName;
        switch (reportText)
        {
            case "REPORTS":
                reportName = WebCommon.getGlobalResourceValue("VendorScorecard");
                break;
            default:
                reportName = WebCommon.getGlobalResourceValue(reportText);
                break;
        }
        return reportName;
    }
    private string GetModuleName(string moduleText)
    {
        moduleText = moduleText.Trim().ToUpper();
        var moduleName = string.Empty;
        switch (moduleText)
        {
            case "DELIVERYREPORTS":
                moduleName = WebCommon.getGlobalResourceValue("DeliveryReports");
                break;
            case "BOOKINGREPORTS":
                moduleName = WebCommon.getGlobalResourceValue("BookingReports");
                break;
            case "PLANNINGREPORTS":
                moduleName = WebCommon.getGlobalResourceValue("PlanningReports");
                break;
            case "SCHEDULINGREPORTS":
                moduleName = WebCommon.getGlobalResourceValue("Scheduling");
                break;
            case "RECEIVINGACTIONS":
                moduleName = WebCommon.getGlobalResourceValue("ReceivingActions");
                break;
            case "DISCREPORTS":
                moduleName = WebCommon.getGlobalResourceValue("Discrepancies");
                break;
            case "OTIFREPORTS":
                moduleName = WebCommon.getGlobalResourceValue("OTIF");
                break;
            case "STOCKOVERVIEWREPORTS":
                moduleName = WebCommon.getGlobalResourceValue("StockOverview");
                break;
            case "VENDORSCORECARD":
                moduleName = WebCommon.getGlobalResourceValue("VendorScoreCard");
                break;
            case "APPSCHEDULING":
                moduleName = WebCommon.getGlobalResourceValue("Scheduling");
                break;
            case "DISCREPANCIES":
                moduleName = WebCommon.getGlobalResourceValue("Discrepancies");
                break;
            case "OTIF":
                moduleName = WebCommon.getGlobalResourceValue("OTIF");
                break;
            case "ADMINSETTINGS":
                moduleName = WebCommon.getGlobalResourceValue("AdminSettings");
                break;
            case "VENDORMAINTENANCE":
                moduleName = WebCommon.getGlobalResourceValue("AdminSettings");
                break;
            case "VENDORCONSOLIDATION":
                moduleName = WebCommon.getGlobalResourceValue("AdminSettings");
                break;
            case "VENDORSETUP":
                moduleName = WebCommon.getGlobalResourceValue("AdminSettings");
                break;
            case "OTIFSTOCKOVERVIEW":
                moduleName = WebCommon.getGlobalResourceValue("OTIFStockOverview");
                break;
        }
        return moduleName;
    }
    private string GetDescription(string descriptionText)
    {
        descriptionText = descriptionText.Trim().ToUpper();
        string description;
       
        switch (descriptionText)
        {
            case "REPORTS":
                descriptionText = WebCommon.getGlobalResourceValue("VendorScorecard");
                descriptionText = descriptionText.Replace("-", string.Empty);
                descriptionText = descriptionText.Replace("/", string.Empty);
                descriptionText = descriptionText.Replace(" ", string.Empty);
                descriptionText = descriptionText.Trim();
                description = WebCommon.getGlobalResourceValue(string.Format("{0}Description", descriptionText));
                break;
            default:
                //descriptionText = WebCommon.getGlobalResourceValue(descriptionText);
                //descriptionText = descriptionText.Replace("-", string.Empty);
                //descriptionText = descriptionText.Replace("/", string.Empty);
                //descriptionText = descriptionText.Replace(" ", string.Empty);
                //descriptionText = descriptionText.Trim();
                description = WebCommon.getGlobalResourceValue(string.Format("{0}Description", descriptionText));
                break;
        }        

        return description;
    }

    private string GetFurtherInfo(string furtherInfoText)
    {
        furtherInfoText = furtherInfoText.Trim().ToUpper();
        string furtherInfo;
        switch (furtherInfoText)
        {
            case "REPORTS":
                furtherInfoText = WebCommon.getGlobalResourceValue("VendorScorecard");
                furtherInfoText = furtherInfoText.Replace("-", string.Empty);
                furtherInfoText = furtherInfoText.Replace("/", string.Empty);
                furtherInfoText = furtherInfoText.Replace(" ", string.Empty);
                furtherInfoText = furtherInfoText.Trim();
                furtherInfo = WebCommon.getGlobalResourceValue(string.Format("{0}FurtherInfo", furtherInfoText));
                break;
            default:
                furtherInfoText = WebCommon.getGlobalResourceValue(furtherInfoText);
                furtherInfoText = furtherInfoText.Replace("-", string.Empty);
                furtherInfoText = furtherInfoText.Replace("/", string.Empty);
                furtherInfoText = furtherInfoText.Replace(" ", string.Empty);
                furtherInfoText = furtherInfoText.Trim();
                furtherInfo = WebCommon.getGlobalResourceValue(string.Format("{0}FurtherInfo", furtherInfoText));
                break;
        }
        return furtherInfo;
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        GridViewSortExp = e.SortExpression;
        GridViewSortDirection = SortDir == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
        return Utilities.GenericListHelper<ReportLibraryBE>.SortList((List<ReportLibraryBE>)ViewState["ReportLibrary"], e.SortExpression, GridViewSortDirection).ToArray();
    }

    private void SortGridView(string sortExpression, SortDirection direction)
    {
        var unused = new List<ReportLibraryBE>();
        try
        {
            if (ViewState["ReportLibrary"] != null)
            {
                List<ReportLibraryBE> lstReportLibrary = Utilities.GenericListHelper<ReportLibraryBE>.SortList((List<ReportLibraryBE>)ViewState["ReportLibrary"], sortExpression, direction);
                gvReportLibrary.DataSource = lstReportLibrary;
                gvReportLibrary.DataBind();
                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(gvReportLibrary);
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    #endregion   
}
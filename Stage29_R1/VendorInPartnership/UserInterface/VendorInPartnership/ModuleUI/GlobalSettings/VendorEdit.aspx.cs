﻿using System;
using System.Collections.Generic;
using System.Text;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Discrepancy.CountrySetting;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Data;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;
using System.Linq;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Web.UI;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.StockOverview;

public partial class VendorEdit : CommonPage
{
    protected string CurrencySaveMesg = WebCommon.getGlobalResourceValue("CurrencySaveMesg");
    protected string CurrencyRequired = WebCommon.getGlobalResourceValue("CurrencyRequired");
    protected string CurrencyErrorMesg = WebCommon.getGlobalResourceValue("CurrencyErrorMesg");
    protected string SuccessMesg = WebCommon.getGlobalResourceValue("SuccessMesg");
    protected string ReturnAddressSaveMsg = WebCommon.getGlobalResourceValue("ReturnAddressSaveMsg");
    protected string VendorId = string.Empty;

    #region Declarations ...

    Dictionary<int, string> dicVendorActiveSites = new Dictionary<int, string>();

    #endregion

    #region Events ...

    protected void Page_Init(object sender, EventArgs e)
    {
        // ucSiteCountry.CurrentPage = this;
        //VendorId = GetQueryStringValue("VendorID");
        //showhide.HRef = EncryptURL("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?VendorID=" + VendorId + "&Mode=Add");
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("ReadOnly") == "1")
            {

                lblConsolidatedCode.Visible = true;
                hpConsolidatedCode.Visible = false;
                //btnMoveRightAll.Visible = false;
                //btnMoveRight.Visible = false;
                //btnMoveLeft.Visible = false;
                //btnMoveLeftAll.Visible = false;
                btnSave.Visible = false;
                btnProceed.Visible = true;
            }
            else
            {
                lblConsolidatedCode.Visible = false;
                hpConsolidatedCode.Visible = true;
                //btnMoveRightAll.Visible = true;
                //btnMoveRight.Visible = true;
                //btnMoveLeft.Visible = true;
                //btnMoveLeftAll.Visible = true;
                btnSave.Visible = true;
                btnProceed.Visible = false;
            }
            if (GetQueryStringValue("VendorID") != null)
            {
                this.BindCurrency();
                GetVendorDetail();

                List<SCT_UserBE> Scorecardlist = new List<SCT_UserBE>();
                List<SCT_UserBE> Schedulinglist = new List<SCT_UserBE>();
                List<SCT_UserBE> OTIFlist = new List<SCT_UserBE>();
                List<SCT_UserBE> Discrepancylist = new List<SCT_UserBE>();
                List<SCT_UserBE> InvoiceIssuelist = new List<SCT_UserBE>();

                Schedulinglist = GetSchedulingContact();
                Discrepancylist = GetDiscrepancyContact();
                OTIFlist = GetOTIFContact();
                Scorecardlist = GetScorecardContact();
                InvoiceIssuelist = GetInvoiceIssueContact();

                //--Add element to list to make grid equal in size
                List<int> lstCount = new List<int>();
                lstCount.Add(Schedulinglist.Count);
                lstCount.Add(Discrepancylist.Count);
                lstCount.Add(OTIFlist.Count);
                lstCount.Add(Scorecardlist.Count);

                int max = lstCount[0];
                foreach (var item in lstCount)
                {
                    if (item > max)
                    {
                        max = item;
                    }
                }

                // Binding Data to grids of Contact Point
                while (Scorecardlist.Count < max || Scorecardlist.Count < 4)
                {
                    SCT_UserBE item = new SCT_UserBE();
                    Scorecardlist.Add(item);
                }
                gvScorecardContact.DataSource = Scorecardlist;
                gvScorecardContact.DataBind();

                while (OTIFlist.Count < max || OTIFlist.Count < 4)
                {
                    SCT_UserBE item = new SCT_UserBE();
                    OTIFlist.Add(item);
                }
                gvOTIFContact.DataSource = OTIFlist;
                gvOTIFContact.DataBind();

                while (Discrepancylist.Count < max || Discrepancylist.Count < 4)
                {
                    SCT_UserBE item = new SCT_UserBE();
                    Discrepancylist.Add(item);
                }
                gvDiscrepancyContact.DataSource = Discrepancylist;
                gvDiscrepancyContact.DataBind();

                while (InvoiceIssuelist.Count < max || InvoiceIssuelist.Count < 4)
                {
                    SCT_UserBE item = new SCT_UserBE();
                    InvoiceIssuelist.Add(item);
                }
                gvInvoiceIssueContact.DataSource = InvoiceIssuelist;
                gvInvoiceIssueContact.DataBind();

                while (Schedulinglist.Count < max || Schedulinglist.Count < 4)
                {
                    SCT_UserBE item = new SCT_UserBE();
                    Schedulinglist.Add(item);
                }
                gvSchedulingContact.DataSource = Schedulinglist;
                gvSchedulingContact.DataBind();

                // Account Payable

                DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE = new DISCnt_AccountPayableContactBE();
                DISCNT_AccountPayableContactBAL oDISCnt_AccountPayableContactBAL = new DISCNT_AccountPayableContactBAL();

                oDISCnt_AccountPayableContactBE.Action = "ShowAll";
                oDISCnt_AccountPayableContactBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
                List<DISCnt_AccountPayableContactBE> lstExistance = oDISCnt_AccountPayableContactBAL.GetAccountPayableContactDetailsBAL(oDISCnt_AccountPayableContactBE);
                oDISCnt_AccountPayableContactBAL = null;
                rptAccountPayable.DataSource = lstExistance;
                rptAccountPayable.DataBind();

                //
                //BindSites();
                //BindSelectedSites();
                this.BindVendorOfContacts();

                BindVendorPenalty();
                GetOverstockReturnsDetails();

            }
            // ucSiteCountry.innerControlddlCountry.AutoPostBack = true;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        // updateVendorActiveSites();
        //if (!string.IsNullOrWhiteSpace(txtReturnAddress.Text))
        //{
            UP_VendorBE oUP_VendorBE = new UP_VendorBE();
            UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
            oUP_VendorBE.Action = "UpdateReturnAddress";
            oUP_VendorBE.ReturnAddress = txtReturnAddress.Text;
            oUP_VendorBE.DeliveryInstructions = txtDeliveryInstructions.Text;
            oUP_VendorBE.PaymentTerms = txtPaymentTerms.Text;
            oUP_VendorBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

            if (GetQueryStringValue("VendorID") != null)
            {
                oUP_VendorBAL.UpdateReturnAddress(oUP_VendorBE);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SuccessMesg + "')", true);
                oUP_VendorBAL = null;
            }
        //}
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PageFrom") != null)
        {
            if (GetQueryStringValue("PageFrom").ToString() == "UserList")
                EncryptQueryString("../Security/SCT_UsersOverview.aspx");
            else if (GetQueryStringValue("PageFrom").ToString() == "vendorPOCList")
            {
                Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");
                return;
            }
            if (GetQueryStringValue("PageFrom").ToString() == "VendorOverview" || GetQueryStringValue("PageFrom").ToString() == "VendorMaintainance"
                || GetQueryStringValue("PageFrom").ToString() == "OverstockAgreementEdit" || GetQueryStringValue("PageFrom").ToString() == "ReturnsAgreementEdit")
            {
                EncryptQueryString("VendorOverview.aspx");
            }
        }

        if (GetQueryStringValue("PageComeFrom") != null)
        {
            if (GetQueryStringValue("PageComeFrom").ToString() == "UserOverview")
            {
                EncryptQueryString("../Security/SCT_UsersOverview.aspx");
            }
            else
            {
                EncryptQueryString("VendorOverview.aspx");
            }
        }

        if (GetQueryStringValue("PreviousPageFrom") != null)
        {
            if (GetQueryStringValue("PreviousPageFrom").ToString() == "UserList")
            {
                EncryptQueryString("../Security/SCT_UsersOverview.aspx");
            }
        }

        if (GetQueryStringValue("SPVendorID") != null)
        {
            EncryptQueryString("VendorStockPlannerDetail.aspx?IsBackToSP=1");
        }

        if (GetQueryStringValue("PreviousPageWas") != null) // from MaintainVendorPointsOfContact page
        {
            if (GetQueryStringValue("PreviousPageWas").Equals("UserList"))
            {
                EncryptQueryString("../Security/SCT_UsersOverview.aspx");
            }
            else if (GetQueryStringValue("PreviousPageWas").Equals("VendorOverview"))
            {
                EncryptQueryString("VendorOverview.aspx");
            }
        }


    }

    // #region Site Button Events

    //protected void btnMoveRightAll_Click(object sender, EventArgs e)
    //{
    //    if (UclstLeftSite.Items.Count > 0)
    //        FillControls.MoveAllItemsLeftToRight(UclstLeftSite, UclstRightSite, ucSiteCountry.innerControlddlCountry.SelectedItem.Text);
    //}

    //protected void btnMoveRight_Click(object sender, EventArgs e)
    //{
    //    if (UclstLeftSite.SelectedItem != null)
    //    {
    //        FillControls.MoveItemLeftToRight(UclstLeftSite, UclstRightSite, ucSiteCountry.innerControlddlCountry.SelectedItem.Text);
    //    }
    //}

    //protected void btnMoveLeft_Click(object sender, EventArgs e)
    //{
    //    if (UclstRightSite.SelectedItem != null)
    //    {
    //        FillControls.MoveItemRightToLeft(UclstRightSite, UclstLeftSite, ucSiteCountry.innerControlddlCountry.SelectedItem.Text);
    //    }
    //}

    //protected void btnMoveLeftAll_Click(object sender, EventArgs e)
    //{
    //    if (UclstRightSite.Items.Count > 0)
    //    {
    //        FillControls.MoveAllItemsRightToLeft(UclstRightSite, UclstLeftSite, ucSiteCountry.innerControlddlCountry.SelectedItem.Text);
    //    }
    //}

    //#endregion 

    #endregion

    #region Custom Methods ...

    private List<SCT_UserBE> GetScorecardContact()
    {
        int VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

        List<SCT_UserBE> oSCT_UserBElist = new List<SCT_UserBE>();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        oUP_VendorBE.Action = "GetVendorContact";
        oUP_VendorBE.VendorID = VendorID;
        oUP_VendorBE.User = new SCT_UserBE();
        oUP_VendorBE.User.ScorecardContact = 'Y';

        oUP_VendorBE.User.OTIFContact = null;
        oUP_VendorBE.User.SchedulingContact = null;
        oUP_VendorBE.User.DiscrepancyContact = null;
        oUP_VendorBE.User.InvoiceIssueContact = null;

        oSCT_UserBElist = oUP_VendorBAL.GetVendorContact(oUP_VendorBE);
        oUP_VendorBAL = null;
        ViewState["lstUser"] = oSCT_UserBElist;
        return oSCT_UserBElist;
    }

    private List<SCT_UserBE> GetOTIFContact()
    {
        int VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

        List<SCT_UserBE> oSCT_UserBElist = new List<SCT_UserBE>();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        oUP_VendorBE.Action = "GetVendorContact";
        oUP_VendorBE.VendorID = VendorID;
        oUP_VendorBE.User = new SCT_UserBE();

        oUP_VendorBE.User.OTIFContact = 'Y';
        oUP_VendorBE.User.SchedulingContact = null;
        oUP_VendorBE.User.DiscrepancyContact = null;
        oUP_VendorBE.User.ScorecardContact = null;
        oUP_VendorBE.User.InvoiceIssueContact = null;


        oSCT_UserBElist = oUP_VendorBAL.GetVendorContact(oUP_VendorBE);
        oUP_VendorBAL = null;
        ViewState["lstUser"] = oSCT_UserBElist;
        return oSCT_UserBElist;

    }

    private List<SCT_UserBE> GetDiscrepancyContact()
    {
        int VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

        List<SCT_UserBE> oSCT_UserBElist = new List<SCT_UserBE>();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        oUP_VendorBE.Action = "GetVendorContact";
        oUP_VendorBE.VendorID = VendorID;
        oUP_VendorBE.User = new SCT_UserBE();

        oUP_VendorBE.User.SchedulingContact = null;
        oUP_VendorBE.User.DiscrepancyContact = 'Y';
        oUP_VendorBE.User.OTIFContact = null;
        oUP_VendorBE.User.ScorecardContact = null;
        oUP_VendorBE.User.InvoiceIssueContact = null;

        oSCT_UserBElist = oUP_VendorBAL.GetVendorContact(oUP_VendorBE);
        oUP_VendorBAL = null;
        ViewState["lstUser"] = oSCT_UserBElist;
        return oSCT_UserBElist;
    }

    private List<SCT_UserBE> GetInvoiceIssueContact()
    {
        int VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

        List<SCT_UserBE> oSCT_UserBElist = new List<SCT_UserBE>();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        oUP_VendorBE.Action = "GetVendorContact";
        oUP_VendorBE.VendorID = VendorID;
        oUP_VendorBE.User = new SCT_UserBE();

        oUP_VendorBE.User.SchedulingContact = null;
        oUP_VendorBE.User.DiscrepancyContact = null;
        oUP_VendorBE.User.OTIFContact = null;
        oUP_VendorBE.User.ScorecardContact = null;
        oUP_VendorBE.User.InvoiceIssueContact = 'Y';

        oSCT_UserBElist = oUP_VendorBAL.GetVendorContact(oUP_VendorBE);
        oUP_VendorBAL = null;
        ViewState["lstUser"] = oSCT_UserBElist;
        return oSCT_UserBElist;
    }

    private List<SCT_UserBE> GetSchedulingContact()
    {

        int VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

        List<SCT_UserBE> oSCT_UserBElist = new List<SCT_UserBE>();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        oUP_VendorBE.Action = "GetVendorContact";
        oUP_VendorBE.VendorID = VendorID;
        oUP_VendorBE.User = new SCT_UserBE();
        oUP_VendorBE.User.SchedulingContact = 'Y';
        oUP_VendorBE.User.DiscrepancyContact = null;
        oUP_VendorBE.User.OTIFContact = null;
        oUP_VendorBE.User.ScorecardContact = null;
        oUP_VendorBE.User.InvoiceIssueContact = null;

        oSCT_UserBElist = oUP_VendorBAL.GetVendorContact(oUP_VendorBE);
        oUP_VendorBAL = null;
        ViewState["lstUser"] = oSCT_UserBElist;
        return oSCT_UserBElist;

    }

    private void GetVendorDetail()
    {

        int VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        oUP_VendorBE.Action = "ShowVendorByID";
        oUP_VendorBE.VendorID = VendorID;
        oUP_VendorBE = oUP_VendorBAL.GetVendorDetailBAL(oUP_VendorBE);
        ddlCurrency.Items.RemoveAt(0);
        if (oUP_VendorBE.NewCurrencyID != null)
        {
            ddlCurrency.SelectedValue = Convert.ToString(oUP_VendorBE.NewCurrencyID);
        }


        oUP_VendorBAL = null;
        lblDefaultFaxNumberData.Text = oUP_VendorBE.VendorContactFax;
        lblDefaultLanguageData.Text = oUP_VendorBE.Language;
        lblDefaultPhoneNumberData.Text = oUP_VendorBE.VendorContactNumber;
        lblVendorNameData.Text = oUP_VendorBE.VendorName;
        lblVendorNumberData.Text = oUP_VendorBE.Vendor_No;
        lblDefaultLanguageData.Text = oUP_VendorBE.Language;
        lblVendorCountryData.Text = oUP_VendorBE.CountryName;
        lblStockPlannerNumberData.Text = oUP_VendorBE.StockPlannerNumber;
        lblPlannerContactNoData.Text = oUP_VendorBE.StockPlannerContact;
        lblStockPlannerNameData.Text = oUP_VendorBE.StockPlannerName;
        txtDefaultAddress.Text = GetAddressString(oUP_VendorBE).ToString();
        txtReturnAddress.Text = oUP_VendorBE.ReturnAddress;
        txtDeliveryInstructions.Text = oUP_VendorBE.DeliveryInstructions;
        txtPaymentTerms.Text = oUP_VendorBE.PaymentTerms;
        lblMasterPurchasingAgreementLink.NavigateUrl = oUP_VendorBE.MasterPurchasingAgreement;
        lblMasterPurchasingAgreementLink.Text = oUP_VendorBE.MasterPurchasingAgreement;

        GetConsolidateVendor(oUP_VendorBE.VendorFlag);
    }

    private void GetConsolidateVendor(char? vFlag)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        List<UP_VendorBE> lstVendorDetails = new List<UP_VendorBE>();
        oUP_VendorBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

        bool IsConsildationReq = false;
        if (!string.IsNullOrWhiteSpace(Convert.ToString(vFlag)))
            if (vFlag == 'P')
            {
                oUP_VendorBE.Action = "GetConsolidateVendor";
                lstVendorDetails = oUP_VendorBAL.GetVendorConsolidationBAL(oUP_VendorBE);
            }
            else
            {
                IsConsildationReq = true;
            }
        else
        {
            IsConsildationReq = true;
        }

        if (IsConsildationReq)
        {
            oUP_VendorBE.Action = "GetVendorConsolidation";
            lstVendorDetails = oUP_VendorBAL.GetVendorConsolidationBAL(oUP_VendorBE);
            if (lstVendorDetails != null && lstVendorDetails.Count > 0 && lstVendorDetails[0].ParentVendorID != null)
            {
                oUP_VendorBE.Action = "GetConsolidateVendor";
                oUP_VendorBE.VendorID = Convert.ToInt32(lstVendorDetails[0].ParentVendorID);
                lstVendorDetails = oUP_VendorBAL.GetVendorConsolidationBAL(oUP_VendorBE);
            }
            else
            {
                lstVendorDetails = null;
            }
        }


        oUP_VendorBAL = null;
        if (lstVendorDetails != null && lstVendorDetails.Count > 0)
        {

            hpConsolidatedCode.Text = lstVendorDetails[0].ConsolidatedCode;
            lblConsolidatedCode.Text = lstVendorDetails[0].ConsolidatedCode;

            if (lstVendorDetails[0].ConsVendorFlag == 'P')
            {
                hpConsolidatedCode.NavigateUrl = EncryptQuery("VendorConsolidationEdit.aspx?VendorId=" + lstVendorDetails[0].ParentVendorID.ToString());
            }
            else if (lstVendorDetails[0].ConsVendorFlag == 'G')
            {
                hpConsolidatedCode.NavigateUrl = EncryptQuery("GlobalVendorConsolidationEdit.aspx?VendorId=" + lstVendorDetails[0].ParentVendorID.ToString());
            }

            ltConsolidationName.Text = lstVendorDetails[0].ConsolidatedName;


            lstVendorDetails = lstVendorDetails.Select(c => { c.VendorName = c.Vendor_No + " - " + c.VendorName; return c; }).ToList();
            rptLinkedVendor.DataSource = lstVendorDetails;
            rptLinkedVendor.DataBind();
        }
        else
        {
            pnlConsolidation.Visible = false;
        }
    }

    private StringBuilder GetAddressString(UP_VendorBE lstVendor)
    {
        StringBuilder Address = new StringBuilder();

        Address.AppendLine(lstVendor.address1);

        if (lstVendor.address2 != string.Empty)
        {
            Address.AppendLine(lstVendor.address2);
        }
        if (lstVendor.city != string.Empty)
        {
            Address.AppendLine(lstVendor.city);
        }
        if (lstVendor.county != string.Empty)
        {
            Address.AppendLine(lstVendor.county);
        }
        if (lstVendor.VMPPIN != string.Empty)
        {
            Address.AppendLine(lstVendor.VMPPIN);
        }
        if (lstVendor.VMPPOU != string.Empty)
        {
            Address.AppendLine(lstVendor.VMPPOU);
        }

        return Address;

    }

    //private void updateVendorActiveSites()
    //{
    //    if (GetQueryStringValue("VendorID") != null)
    //    {
    //        string VendorActiveSiteIDs = string.Empty;
    //        if (UclstRightSite.Items.Count > 0)
    //        {
    //            if (UclstRightSite.Items.Count > 1)
    //            {
    //                VendorActiveSiteIDs = UclstRightSite.Items[0].Value + ",";
    //                for (int i = 1; i < UclstRightSite.Items.Count - 1; i++)
    //                {
    //                    VendorActiveSiteIDs += UclstRightSite.Items[i].Value + ",";
    //                }
    //                VendorActiveSiteIDs += UclstRightSite.Items[UclstRightSite.Items.Count - 1].Value;
    //            }
    //            else
    //                VendorActiveSiteIDs = UclstRightSite.Items[0].Value;
    //        }
    //        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
    //        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
    //        oUP_VendorBE.Action = "UpdateVendorActiveSites";
    //        oUP_VendorBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
    //        oUP_VendorBE.VendorActiveSiteIDs = VendorActiveSiteIDs;
    //        oUP_VendorBAL.UpdateVendorActiveSites(oUP_VendorBE);
    //        oUP_VendorBAL = null;
    //    }
    //}

    //private void BindSites()
    //{
    //    if (GetQueryStringValue("VendorID").IsNumeric() && Session["UserID"].IsNumeric())
    //    {
    //        int CountryID = 0;
    //        if (ucSiteCountry.innerControlddlCountry.SelectedIndex != -1)
    //            CountryID = Convert.ToInt32(ucSiteCountry.innerControlddlCountry.SelectedItem.Value);
    //        else
    //            CountryID = Convert.ToInt32(Session["SiteCountryID"].ToString());

    //        #region to Show all sites in left listbox
    //        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
    //        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

    //        oMAS_SiteBE.Action = "ShowAll";

    //        oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
    //        oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
    //        oMAS_SiteBE.SiteCountryID = CountryID;
    //        List<MAS_SiteBE> lstSite = oMAS_SiteBAL.GetSiteBAL(oMAS_SiteBE);
    //        oMAS_SiteBAL = null;
    //        if (lstSite.Count > 0)
    //        {
    //            FillControls.FillListBox(ref UclstLeftSite, lstSite, "SiteDescription", "SiteID");
    //        }
    //        if (UclstRightSite.Items.Count > 0)
    //        {
    //            for (int i = 0; i < UclstRightSite.Items.Count; i++)
    //            {
    //                if (UclstLeftSite.Items.FindByValue(UclstRightSite.Items[i].Value) != null)
    //                {
    //                    string CurrentItemText = UclstRightSite.Items[i].Text;
    //                    if (CurrentItemText.IndexOf("(") > -1 && CurrentItemText.IndexOf(")") > -1)
    //                    {
    //                        CurrentItemText = CurrentItemText.Remove(CurrentItemText.IndexOf("("), CurrentItemText.IndexOf(")") - CurrentItemText.IndexOf("(") + 1);
    //                        UclstLeftSite.Items.Remove(new ListItem(CurrentItemText.Trim(), UclstRightSite.Items[i].Value));
    //                    }
    //                }
    //            }
    //        }
    //        #endregion
    //    }
    //}

    //private void BindSelectedSites()
    //{
    //    if (GetQueryStringValue("VendorID").IsNumeric() && Session["UserID"].IsNumeric())
    //    {
    //        int CountryID = 0;
    //        if (ucSiteCountry.innerControlddlCountry.SelectedIndex != -1)
    //            CountryID = Convert.ToInt32(ucSiteCountry.innerControlddlCountry.SelectedItem.Value);
    //        else
    //            CountryID = Convert.ToInt32(Session["SiteCountryID"].ToString());

    //        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
    //        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
    //        oUP_VendorBE.Action = "GetVendorActiveSites";
    //        oUP_VendorBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

    //        oUP_VendorBE.CountryID = CountryID;

    //        DataTable dtVendorActiveSites = oUP_VendorBAL.GetVendorActiveSitesBAL(oUP_VendorBE);
    //        oUP_VendorBAL = null;
    //        if (dtVendorActiveSites != null && dtVendorActiveSites.Rows.Count > 0)
    //        {
    //            FillControls.FillListBox(ref UclstRightSite, dtVendorActiveSites, "SiteDescriptionWithCountry", "SiteID");
    //            if (UclstRightSite.Items.Count > 0)
    //            {
    //                for (int i = 0; i < UclstRightSite.Items.Count; i++)
    //                {
    //                    if (UclstLeftSite.Items.FindByValue(UclstRightSite.Items[i].Value) != null)
    //                    {
    //                        string CurrentItemText = UclstRightSite.Items[i].Text;
    //                        if (CurrentItemText.IndexOf("(") > -1 && CurrentItemText.IndexOf(")") > -1)
    //                        {
    //                            CurrentItemText = CurrentItemText.Remove(CurrentItemText.IndexOf("("), CurrentItemText.IndexOf(")") - CurrentItemText.IndexOf("(") + 1);
    //                            UclstLeftSite.Items.Remove(new ListItem(CurrentItemText.Trim(), UclstRightSite.Items[i].Value));
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }
    //}

    public override void CountrySelectedIndexChanged()
    {
        //BindSites();
        //BindSelectedSites();
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MAS_VendorBE>.SortList((List<MAS_VendorBE>)ViewState["lstUser"], e.SortExpression, e.SortDirection).ToArray();
    }

    private List<MAS_MaintainVendorPointsContactBE> GetVendorPointsContacts()
    {
        var vendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
        var lstVendorPointsContact = new List<MAS_MaintainVendorPointsContactBE>();
        var vendorPointsContactBAL = new MAS_MaintainVendorPointsContactBAL();
        var VendorPointsContact = new MAS_MaintainVendorPointsContactBE();

        VendorPointsContact.Action = "GetMaintainVendorPointsContact";
        VendorPointsContact.Vendor = new MAS_VendorBE();
        VendorPointsContact.Vendor.VendorID = vendorID;
        lstVendorPointsContact = vendorPointsContactBAL.GetVendorPointsContactsBAL(VendorPointsContact);
        vendorPointsContactBAL = null;
        return lstVendorPointsContact;
    }

    private void BindVendorOfContacts()
    {
        var lstVendorPOC = new List<MAS_MaintainVendorPointsContactBE>() { new MAS_MaintainVendorPointsContactBE { FirstName = string.Empty, SecondName = string.Empty } };
        var lstVendorPointsContact = this.GetVendorPointsContacts();
        if (lstVendorPointsContact != null)
        {
            gvInventoryPOC.DataSource = lstVendorPointsContact.FindAll(vc => vc.InventoryPOC.Equals("Y"));
            gvInventoryPOC.DataBind();

            gvProcurementPOC.DataSource = lstVendorPointsContact.FindAll(vc => vc.ProcurementPOC.Equals("Y"));
            gvProcurementPOC.DataBind();

            gvMerchandisingPOC.DataSource = lstVendorPointsContact.FindAll(vc => vc.MerchandisingPOC.Equals("Y"));
            gvMerchandisingPOC.DataBind();

            gvExecutivePOC.DataSource = lstVendorPointsContact.FindAll(vc => vc.ExecutivePOC.Equals("Y"));
            gvExecutivePOC.DataBind();
        }

        if (gvInventoryPOC.Rows.Count.Equals(0))
        {
            gvInventoryPOC.DataSource = lstVendorPOC;
            gvInventoryPOC.DataBind();
        }

        if (gvProcurementPOC.Rows.Count.Equals(0))
        {
            gvProcurementPOC.DataSource = lstVendorPOC;
            gvProcurementPOC.DataBind();
        }

        if (gvMerchandisingPOC.Rows.Count.Equals(0))
        {
            gvMerchandisingPOC.DataSource = lstVendorPOC;
            gvMerchandisingPOC.DataBind();
        }

        if (gvExecutivePOC.Rows.Count.Equals(0))
        {
            gvExecutivePOC.DataSource = lstVendorPOC;
            gvExecutivePOC.DataBind();
        }
    }

    #endregion   
    protected void gvSchedulingContact_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (GetQueryStringValue("ReadOnly") == "1")
            {
                HyperLink hplink = (HyperLink)e.Row.FindControl("HLSchedulingVendor");
                if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "AccountStatus")).ToLower() == "registered awaiting approval")
                {
                    hplink.NavigateUrl = EncryptQuery("~/ModuleUI/Security/SCT_ExternalUserApprovalEdit.aspx?Readonly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, ("UserID")) + "&VendorID=" + GetQueryStringValue("VendorID"));
                }
                else
                {
                    hplink.NavigateUrl = EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?Readonly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, ("UserID")) + "&VendorID=" + GetQueryStringValue("VendorID"));
                }
            }
        }
    }
    protected void gvDiscrepancyContact_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (GetQueryStringValue("ReadOnly") == "1")
            {
                HyperLink hplink = (HyperLink)e.Row.FindControl("hlLoginID");
                if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "AccountStatus")).ToLower() == "registered awaiting approval")
                {
                    hplink.NavigateUrl = EncryptQuery("~/ModuleUI/Security/SCT_ExternalUserApprovalEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID") + "&VendorID=" + GetQueryStringValue("VendorID"));
                }
                else
                {
                    hplink.NavigateUrl = EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID") + "&VendorID=" + GetQueryStringValue("VendorID"));
                }
            }
        }
    }

    protected void gvInvoiceIssueContact_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (GetQueryStringValue("ReadOnly") == "1")
            {
                HyperLink hplink = (HyperLink)e.Row.FindControl("hlLoginID");
                if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "AccountStatus")).ToLower() == "registered awaiting approval")
                {
                    hplink.NavigateUrl = EncryptQuery("~/ModuleUI/Security/SCT_ExternalUserApprovalEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID") + "&VendorID=" + GetQueryStringValue("VendorID"));
                }
                else
                {
                    hplink.NavigateUrl = EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID") + "&VendorID=" + GetQueryStringValue("VendorID"));
                }
            }
        }
    }

    protected void gvOTIFContact_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (GetQueryStringValue("ReadOnly") == "1")
            {
                HyperLink hplink = (HyperLink)e.Row.FindControl("HLOTIFVendor");
                if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "AccountStatus")).ToLower() == "registered awaiting approval")
                {
                    hplink.NavigateUrl = EncryptQuery("~/ModuleUI/Security/SCT_ExternalUserApprovalEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID") + "&VendorID=" + GetQueryStringValue("VendorID"));
                }
                else
                {
                    hplink.NavigateUrl = EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID") + "&VendorID=" + GetQueryStringValue("VendorID"));
                }
            }
        }
    }
    protected void gvScorecardContact_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (GetQueryStringValue("ReadOnly") == "1")
            {
                HyperLink hplink = (HyperLink)e.Row.FindControl("HLScorecardVendor");
                if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "AccountStatus")).ToLower() == "registered awaiting approval")
                {
                    hplink.NavigateUrl = EncryptQuery("~/ModuleUI/Security/SCT_ExternalUserApprovalEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID") + "&VendorID=" + GetQueryStringValue("VendorID"));
                }
                else
                {
                    hplink.NavigateUrl = EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID") + "&VendorID=" + GetQueryStringValue("VendorID"));
                }
            }
        }
    }
    protected void gvInventoryPOC_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (GetQueryStringValue("ReadOnly") == "1")
            {
                HyperLink hplink = (HyperLink)e.Row.FindControl("hlInventoryPOC");
                hplink.NavigateUrl = EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?ReadOnly=1&venPointId=" + DataBinder.Eval(e.Row.DataItem, "VendorPointID") + "&VendorID=" + DataBinder.Eval(e.Row.DataItem, "Vendor.VendorID"));

            }
        }
    }
    protected void gvProcurementPOC_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (GetQueryStringValue("ReadOnly") == "1")
            {
                HyperLink hplink = (HyperLink)e.Row.FindControl("hlProcurementPOC");
                hplink.NavigateUrl = EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?ReadOnly=1&venPointId=" + DataBinder.Eval(e.Row.DataItem, "VendorPointID") + "&VendorID=" + DataBinder.Eval(e.Row.DataItem, "Vendor.VendorID"));
            }
        }
    }
    protected void gvMerchandisingPOC_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (GetQueryStringValue("ReadOnly") == "1")
            {
                HyperLink hplink = (HyperLink)e.Row.FindControl("hlMerchandisingPOC");
                hplink.NavigateUrl = EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?ReadOnly=1&venPointId=" + DataBinder.Eval(e.Row.DataItem, "VendorPointID") + "&VendorID=" + DataBinder.Eval(e.Row.DataItem, "Vendor.VendorID"));
            }
        }
    }
    protected void gvExecutivePOC_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (GetQueryStringValue("ReadOnly") == "1")
            {
                HyperLink hplink = (HyperLink)e.Row.FindControl("hlExecutivePOC");
                hplink.NavigateUrl = EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?ReadOnly=1&venPointId=" + DataBinder.Eval(e.Row.DataItem, "VendorPointID") + "&VendorID=" + DataBinder.Eval(e.Row.DataItem, "Vendor.VendorID"));
            }
        }
    }
    protected void btnProceed_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PageFrom") != null)
        {
            if (GetQueryStringValue("PageFrom").ToString() == "UserList")
                EncryptQueryString("../Security/SCT_UsersOverview.aspx");
            else if (GetQueryStringValue("PageFrom").ToString() == "vendorPOCList")
            {
                Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");
                return;
            }
        }
        else
            EncryptQueryString("VendorOverview.aspx");
    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        lblMPAagreement.Visible = false;
        lblMasterPurchasingAgreementLink.Visible = false;
        lblMPAagreementEdit.Visible = true;
        txtMPAagreement.Visible = true;
        btnSave_1.Visible = true;
        btn_back_1.Visible = true;
        btnEdit.Visible = false;
        txtMPAagreement.Text = lblMasterPurchasingAgreementLink.Text;
    }
    protected void btnSave_1_Click(object sender, EventArgs e)
    {
        lblMPAagreement.Visible = true;
        lblMasterPurchasingAgreementLink.Visible = true;
        lblMPAagreementEdit.Visible = false;
        txtMPAagreement.Visible = false;
        btnSave_1.Visible = false;
        btn_back_1.Visible = false;
        btnEdit.Visible = true;

        int VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        oUP_VendorBE.Action = "UpdateMPA";
        oUP_VendorBE.VendorID = VendorID;
        oUP_VendorBE.MasterPurchasingAgreement = txtMPAagreement.Text;
        oUP_VendorBAL.UpdateMPEBAL(oUP_VendorBE);
        oUP_VendorBAL = null;
        lblMasterPurchasingAgreementLink.Text = txtMPAagreement.Text;
        lblMasterPurchasingAgreementLink.NavigateUrl = txtMPAagreement.Text;
    }
    protected void btn_back_1_Click(object sender, EventArgs e)
    {
        lblMPAagreement.Visible = true;
        lblMasterPurchasingAgreementLink.Visible = true;
        lblMPAagreementEdit.Visible = false;
        txtMPAagreement.Visible = false;
        btnSave_1.Visible = false;
        btn_back_1.Visible = false;
        btnEdit.Visible = true;
    }

    private void BindCurrency()
    {

        var currencyBAL = new MAS_CurrencyBAL();
        var currencyBE = new CurrencyBE();
        currencyBE.Action = "ShowAll";
        var lstCurrency = currencyBAL.GetCurrenyBAL(currencyBE);
        if (lstCurrency != null && lstCurrency.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCurrency, lstCurrency, "CurrencyName", "CurrencyID", "Select");
            FillControls.FillDropDown(ref ddlCurrencyList, lstCurrency, "CurrencyName", "CurrencyID", "Select");
        }
    }

    protected void btnSave_2_Click(object sender, EventArgs e)
    {
        int VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        oUP_VendorBE.Action = "SetCurrency";

        oUP_VendorBE.NewCurrencyID = Convert.ToInt32(ddlCurrency.SelectedValue);


        oUP_VendorBE.VendorID = VendorID;

        int? res = oUP_VendorBAL.SetCurrencyBAL(oUP_VendorBE);
        if (res == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CurrencyErrorMesg + "')", true);
        }

        else if (res == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CurrencySaveMesg + "')", true);
        }
    }


    protected void btnPenaltyChargesEdit_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("VendorID") != null)
        {
            EncryptQueryString("../StockOverview/BackOrder/VendorPenaltyMaintenanceEdit.aspx?VendorID=" + GetQueryStringValue("VendorID") + "&IsVendorSubscribeToPenalty=" + GetQueryStringValue("IsVendorSubscribeToPenalty") + "&PageFrom=" + "VendorEdit");

        }
        if (GetQueryStringValue("PageFrom") != null)
        {
            if (GetQueryStringValue("PageFrom").ToString() == "UserList")
            {
                EncryptQueryString("../StockOverview/BackOrder/VendorPenaltyMaintenanceEdit.aspx?VendorID=" + GetQueryStringValue("VendorID") + "&IsVendorSubscribeToPenalty=" + GetQueryStringValue("IsVendorSubscribeToPenalty") + "&PageFrom=" + "VendorEdit" + "&PreviousPageFrom=" + "UserList");
            }
        }
    }

    protected void btnOverstockDetailsEdit_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("VendorID") != null)
        {
            EncryptQueryString("OverstockAgreementEdit.aspx?VendorID=" + GetQueryStringValue("VendorID") + "&PageFrom=" + "VendorEdit");

        }
    }

    protected void btnReturnsDetailsEdit_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("VendorID") != null)
        {
            EncryptQueryString("ReturnsAgreementEdit.aspx?VendorID=" + GetQueryStringValue("VendorID") + "&PageFrom=" + "VendorEdit");

        }
    }



    private void BindVendorPenalty(int Page = 1)
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.Action = "GetVendorPenalty";

            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

            oBackOrderBE.Vendor.VendorID = 0;

            if (GetQueryStringValue("PageFrom") != null && GetQueryStringValue("VendorID") != null)
            {
                //if (GetQueryStringValue("PageFrom").ToString() == "UserList" && GetQueryStringValue("VendorID") != null)
                //{
                //    oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
                //}
                //if (GetQueryStringValue("PageFrom").ToString() == "OverstockAgreementEdit" && GetQueryStringValue("VendorID") != null)
                //{

                oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

            }

            if (GetQueryStringValue("PageComeFrom") != null)
            {
                oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
            }

            if (GetQueryStringValue("VendorID") != null && GetQueryStringValue("IsVendorSubscribeToPenalty") != null && GetQueryStringValue("PageFrom").Equals("VendorOverview"))
            {
                oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
                oBackOrderBE.IsVendorSubscribeToPenalty = GetQueryStringValue("IsVendorSubscribeToPenalty");
            }

            if (GetQueryStringValue("IsPageFrom") != null && GetQueryStringValue("VendorID") != null) // from MaintainVendorPointsOfContact page
            {
                oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
            }

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetVendorPenaltyBAL(oBackOrderBE);

            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                string IsVendorSubscribeToPenalty = !string.IsNullOrEmpty(lstBackOrderBE[0].IsVendorSubscribeToPenalty) ? lstBackOrderBE[0].IsVendorSubscribeToPenalty : "";

                if (IsVendorSubscribeToPenalty.Equals("Yes"))
                {
                    rdoYes.Checked = true;
                }
                else if (IsVendorSubscribeToPenalty.Equals("No"))
                {
                    rdoNo.Checked = true;
                }

                string PenaltyType = !string.IsNullOrEmpty(lstBackOrderBE[0].PenaltyType) ? lstBackOrderBE[0].PenaltyType : "";

                if (PenaltyType.Equals("BO"))
                {
                    rdoIsPenaltyBO.Visible = true;
                    rdoIsPenaltyOTIF.Visible = true;
                    rdoIsPenaltyBO.Checked = true;
                    lblValueperBO.Visible = true;
                    txtValueBO.Visible = true;
                }
                else if (PenaltyType.Equals("OTIF"))
                {
                    rdoIsPenaltyBO.Visible = true;
                    rdoIsPenaltyOTIF.Visible = true;
                    rdoIsPenaltyOTIF.Checked = true;
                    lblValueperOTIF.Visible = true;
                    txtValueOTIF.Visible = true;
                    lblValueOTIFLine.Visible = true;
                    txtValueOTIFLine.Visible = true;
                }

                int ValueOfBO = Convert.ToInt32(lstBackOrderBE[0].ValueOfBackOrder > 0 ? lstBackOrderBE[0].ValueOfBackOrder : 0);

                if (ValueOfBO != 0)
                {
                    txtValueBO.Text = Convert.ToString(ValueOfBO);
                }
                else if (lstBackOrderBE[0].OTIFLineValuePenalty != null)
                {
                    txtValueOTIF.Text = Convert.ToString(lstBackOrderBE[0].OTIFRate);
                    txtValueOTIFLine.Text = Convert.ToString(lstBackOrderBE[0].OTIFLineValuePenalty);
                }

                string CurrencyName = !string.IsNullOrEmpty(lstBackOrderBE[0].Currency.CurrencyName) ? lstBackOrderBE[0].Currency.CurrencyName : string.Empty;

                if (CurrencyName != null)
                {
                    lblCurrency_1.Visible = true;
                    ddlCurrencyList.Visible = true;
                    ddlCurrencyList.SelectedIndex = ddlCurrencyList.Items.IndexOf(
                        ddlCurrencyList.Items.FindByText(!string.IsNullOrEmpty(lstBackOrderBE[0].Currency.CurrencyName) ? lstBackOrderBE[0].Currency.CurrencyName : string.Empty));
                }
                else
                {
                    lblCurrency_1.Visible = false;
                    ddlCurrencyList.Visible = false;
                }
            }

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnOverstockDetailsSave_Click(object sender, EventArgs e)
    {
        MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
        MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();

        oMAS_VendorBE.Action = "UpdateOverstockDetails";

        if (GetQueryStringValue("VendorID") != null)
        {
            oMAS_VendorBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
        }

        if (rdoYes_1.Checked)
        {
            oMAS_VendorBE.IsVendorOverstockAgreement = "Y";
        }
        else if (rdoNo_1.Checked)
        {
            oMAS_VendorBE.IsVendorOverstockAgreement = "N";
        }

        oMAS_VendorBE.VendorOverstockAgreementDetails = txtOverstockDetails.Text;

        int? result = oMAS_VendorBAL.UpdateOverstockReturnsDetailsBAL(oMAS_VendorBE);
        if (result == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SuccessMesg + "')", true);
        }
        oMAS_VendorBAL = null;
        GetOverstockReturnsDetails();
    }

    protected void btnReturnsDetailsSave_Click(object sender, EventArgs e)
    {
        MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
        MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();

        oMAS_VendorBE.Action = "UpdateReturnsDetails";
        if (GetQueryStringValue("VendorID") != null)
        {
            oMAS_VendorBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
        }

        if (rdoYes_2.Checked)
        {
            oMAS_VendorBE.IsVendorReturnsAgreement = "Y";
        }
        else if (rdoNo_2.Checked)
        {
            oMAS_VendorBE.IsVendorReturnsAgreement = "N";
        }

        oMAS_VendorBE.VendorReturnsAgreementDetails = txtReturnsDetails.Text;

        int? result = oMAS_VendorBAL.UpdateOverstockReturnsDetailsBAL(oMAS_VendorBE);
        if (result == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + SuccessMesg + "')", true);
        }
        oMAS_VendorBAL = null;
        GetOverstockReturnsDetails();
    }

    private void GetOverstockReturnsDetails()
    {
        MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
        MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();

        oMAS_VendorBE.Action = "GetOverstockReturnsDetails";

        if (GetQueryStringValue("VendorID") != null)
        {
            oMAS_VendorBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
        }

        List<MAS_VendorBE> lstOverstockReturnsDetails = oMAS_VendorBAL.GetOverstockReturnsDetailsBAL(oMAS_VendorBE);

        if (lstOverstockReturnsDetails != null && lstOverstockReturnsDetails.Count > 0)
        {
            string IsVendorOverstockAgreement = lstOverstockReturnsDetails[0].IsVendorOverstockAgreement;
            string VendorOverstockAgreementDetails = lstOverstockReturnsDetails[0].VendorOverstockAgreementDetails;

            if (IsVendorOverstockAgreement.Equals("Y"))
            {
                rdoYes_1.Checked = true;
            }
            else if (IsVendorOverstockAgreement.Equals("N"))
            {
                rdoNo_1.Checked = true;
            }

            if (VendorOverstockAgreementDetails != null)
            {
                txtOverstockDetails.Text = VendorOverstockAgreementDetails;
            }

            string IsVendorReturnsAgreement = lstOverstockReturnsDetails[0].IsVendorReturnsAgreement;
            string VendorReturnsAgreementDetails = lstOverstockReturnsDetails[0].VendorReturnsAgreementDetails;


            if (IsVendorReturnsAgreement.Equals("Y"))
            {
                rdoYes_2.Checked = true;
            }
            else if (IsVendorReturnsAgreement.Equals("N"))
            {
                rdoNo_2.Checked = true;
            }

            if (VendorReturnsAgreementDetails != null)
            {
                txtReturnsDetails.Text = VendorReturnsAgreementDetails;
            }


        }

        oMAS_VendorBAL = null;

    }






    protected void lnkbtnAdd_Click(object sender, EventArgs e)
    {
        int VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));

        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        oUP_VendorBE.Action = "ShowVendorByID";
        oUP_VendorBE.VendorID = VendorID;
        oUP_VendorBE = oUP_VendorBAL.GetVendorDetailBAL(oUP_VendorBE);

        if (oUP_VendorBE != null)
        {
            if (GetQueryStringValue("PageFrom") != null) // first time when Vendor edit page opens - PageFrom is used
            {
                if (GetQueryStringValue("PageFrom").Equals("UserList"))
                {
                    EncryptQueryString("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?VendorID=" + GetQueryStringValue("VendorId")
                                + "&Vendor_No=" + oUP_VendorBE.Vendor_No + "&VendorName=" + oUP_VendorBE.VendorName
                                + "&CountryName=" + oUP_VendorBE.CountryName + "&Mode=AddInventoryPOC" + "&PreviousPageWas=UserList");
                }
                else if (GetQueryStringValue("PageFrom").Equals("VendorOverview"))
                {
                    EncryptQueryString("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?VendorID=" + GetQueryStringValue("VendorId")
                                + "&Vendor_No=" + oUP_VendorBE.Vendor_No + "&VendorName=" + oUP_VendorBE.VendorName
                                + "&CountryName=" + oUP_VendorBE.CountryName + "&Mode=AddInventoryPOC" + "&PreviousPageWas=VendorOverview");
                }
                else
                {
                    EncryptQueryString("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?VendorID=" + GetQueryStringValue("VendorId")
                               + "&Vendor_No=" + oUP_VendorBE.Vendor_No + "&VendorName=" + oUP_VendorBE.VendorName
                               + "&CountryName=" + oUP_VendorBE.CountryName + "&Mode=AddInventoryPOC");
                }
            }
            else if (GetQueryStringValue("IsPageFrom") != null && GetQueryStringValue("VendorID") != null) // from MaintainVendorPointsOfContact page
            {
                if (GetQueryStringValue("PreviousPageWas").Equals("UserList"))
                {
                    EncryptQueryString("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?VendorID=" + GetQueryStringValue("VendorId")
                               + "&Vendor_No=" + oUP_VendorBE.Vendor_No + "&VendorName=" + oUP_VendorBE.VendorName
                               + "&CountryName=" + oUP_VendorBE.CountryName + "&Mode=AddInventoryPOC" + "&PreviousPageWas=UserList");
                }
                else if (GetQueryStringValue("PreviousPageWas").Equals("VendorOverview"))
                {
                    EncryptQueryString("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?VendorID=" + GetQueryStringValue("VendorId")
                                + "&Vendor_No=" + oUP_VendorBE.Vendor_No + "&VendorName=" + oUP_VendorBE.VendorName
                                + "&CountryName=" + oUP_VendorBE.CountryName + "&Mode=AddInventoryPOC" + "&PreviousPageWas=VendorOverview");
                }
                else
                {
                    EncryptQueryString("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?VendorID=" + GetQueryStringValue("VendorId")
                               + "&Vendor_No=" + oUP_VendorBE.Vendor_No + "&VendorName=" + oUP_VendorBE.VendorName
                               + "&CountryName=" + oUP_VendorBE.CountryName + "&Mode=AddInventoryPOC");
                }
            }
        }
    }
}
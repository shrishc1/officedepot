﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using WebUtilities;

public partial class ModuleUI_GlobalSettings_TradeEntitiesEdit : CommonPage
{
    protected string AtleastOneFieldFilled = WebCommon.getGlobalResourceValue("AtleastOneFieldFilled");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetTradeEntitiesOnID();
        }
    }

    #region Methods

    private void GetTradeEntitiesOnID()
    {

        MAS_TradeEntitiesBE oMAS_TradeEntitiesBE = new MAS_TradeEntitiesBE();
        MAS_TradeEntitiesBAL oMAS_TradeEntitiesBAL = new MAS_TradeEntitiesBAL();


        if (GetQueryStringValue("TradeEntitiesID") != null)
        {

            oMAS_TradeEntitiesBE.Action = "ShowAll";

            oMAS_TradeEntitiesBE.TradeEntitiesID = Convert.ToInt32(GetQueryStringValue("TradeEntitiesID"));

            List<MAS_TradeEntitiesBE> lstTradeEntities = oMAS_TradeEntitiesBAL.GetTradeEntitiesBAL(oMAS_TradeEntitiesBE);

            if (lstTradeEntities != null)
            {
                txtRecordNo.Text = Convert.ToString(GetQueryStringValue("RecordNo"));
                txtArea.Text = lstTradeEntities[0].Area;
                txtCountry.Text = lstTradeEntities[0].OD_Country;
                txtCompany.Text = lstTradeEntities[0].Company;
                txtSendPdfInvoicesTo.Text = lstTradeEntities[0].SendPdfInvoicesTo;
                txtInvoiceAddress.Text = lstTradeEntities[0].InvoiceAddress;
                txtVatNo.Text = lstTradeEntities[0].VAT_No;
                txtCOCNo.Text = lstTradeEntities[0].COC_No;
                txtType.Text = lstTradeEntities[0].Type;
                txtExampleOrderNo.Text = lstTradeEntities[0].ExampleOrderNo;
            }
        }
        else
        {
            if (GetQueryStringValue("RecordNo") != null)
            {
                txtRecordNo.Text = GetQueryStringValue("RecordNo");
            }
            
            btnDelete.Visible = false;
        }
    }

    #endregion


    #region Events


    protected void btnSave_Click(object sender, EventArgs e)
    {
        MAS_TradeEntitiesBE oMAS_TradeEntitiesBE = new MAS_TradeEntitiesBE();
        MAS_TradeEntitiesBAL oMAS_TradeEntitiesBAL = new MAS_TradeEntitiesBAL();

        oMAS_TradeEntitiesBE.Area = txtArea.Text;
        oMAS_TradeEntitiesBE.OD_Country = txtCountry.Text;
        oMAS_TradeEntitiesBE.Company = txtCompany.Text;
        oMAS_TradeEntitiesBE.SendPdfInvoicesTo = txtSendPdfInvoicesTo.Text;
        oMAS_TradeEntitiesBE.InvoiceAddress = txtInvoiceAddress.Text;
        oMAS_TradeEntitiesBE.VAT_No = txtVatNo.Text;
        oMAS_TradeEntitiesBE.COC_No = txtCOCNo.Text;
        oMAS_TradeEntitiesBE.Type = txtType.Text;
        oMAS_TradeEntitiesBE.ExampleOrderNo = txtExampleOrderNo.Text;

        if (GetQueryStringValue("TradeEntitiesID") != null)
        {
            oMAS_TradeEntitiesBE.Action = "UPDATE";
            oMAS_TradeEntitiesBE.TradeEntitiesID = Convert.ToInt32(GetQueryStringValue("TradeEntitiesID"));
        }
        else
        {           
                oMAS_TradeEntitiesBE.Action = "ADD";            
        }

        oMAS_TradeEntitiesBAL.addEditTradeEntitiesDetailsBAL(oMAS_TradeEntitiesBE);

        EncryptQueryString("TradeEntitiesOverview.aspx");

    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        MAS_TradeEntitiesBE oMAS_TradeEntitiesBE = new MAS_TradeEntitiesBE();
        MAS_TradeEntitiesBAL oMAS_TradeEntitiesBAL = new MAS_TradeEntitiesBAL();

        oMAS_TradeEntitiesBE.Action = "DELETE";

        if (GetQueryStringValue("TradeEntitiesID") != null)
        {
            oMAS_TradeEntitiesBE.TradeEntitiesID = Convert.ToInt32(GetQueryStringValue("TradeEntitiesID"));
            oMAS_TradeEntitiesBAL.addEditTradeEntitiesDetailsBAL(oMAS_TradeEntitiesBE);
        }

        EncryptQueryString("TradeEntitiesOverview.aspx");
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("TradeEntitiesOverview.aspx");
    }

    #endregion

}
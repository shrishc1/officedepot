﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.AdminFunctions;
using BaseControlLibrary;


public partial class ShowCommunication  : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (GetQueryStringValue("CommId") != null)
                this.SetEmailInfo(Convert.ToInt32(GetQueryStringValue("CommId")));
        }
    }

    private void SetEmailInfo(int communicationId)
    {
        var commLibraryBAL = new CommunicationLibraryBAL();
        var commLibraryBE = new CommunicationLibraryBE();
        commLibraryBE.Action = "GetCommunicationDetails";
        commLibraryBE.CommunicationID = communicationId;
        var emilDetails = commLibraryBAL.GetEmailCommunicationBAL(commLibraryBE);
        if (emilDetails != null && emilDetails.Count > 0)
        {
            dvDisplayLetter.InnerHtml = emilDetails[0].Body;            
        }
    }
}
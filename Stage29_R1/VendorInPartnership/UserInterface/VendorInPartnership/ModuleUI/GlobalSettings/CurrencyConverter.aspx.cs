﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.IO;

public partial class CurrencyConverter : CommonPage {
    CurrencyConverterBAL o_currencyBAL = new CurrencyConverterBAL();
    CurrencyConverterBE o_CurrencyBE = new CurrencyConverterBE();

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            btnExportToExcel.Visible = false;
            BindCurrencyddl();
            BindCurrencygrid();
        }

        //ucExportToExcel.CurrentPage = this;
        //ucExportToExcel.GridViewControl = gvCurrentCurrency;
        //ucExportToExcel.FileName = "Currency Convert";
    }

    private void BindCurrencyddl() {
        o_CurrencyBE.Action = "ShowAllCurrency";
        List<CurrencyConverterBE> lstCurrencyNamesBE = o_currencyBAL.BindCurrencyddlBAL(o_CurrencyBE);

        if (lstCurrencyNamesBE.Count > 0 && lstCurrencyNamesBE != null) {
            FillControls.FillDropDown(ref ddlCurrency, lstCurrencyNamesBE, "Currency", "CurrencyID", "--All--");
        }

    }

    private void BindCurrencygrid() {

        List<CurrencyConverterBE> lstCurrencyBE = new List<CurrencyConverterBE>();

        if (ViewState["lstCurrencyBE"] != null) {
            lstCurrencyBE = (List<CurrencyConverterBE>)ViewState["lstCurrencyBE"];

            if (lstCurrencyBE == null || lstCurrencyBE.Count == 0) {
                o_CurrencyBE.Action = "ShowAll";
                lstCurrencyBE = o_currencyBAL.BindCurrencyBAL(o_CurrencyBE);
            }
        }
        else {
            o_CurrencyBE.Action = "ShowAll";
            lstCurrencyBE = o_currencyBAL.BindCurrencyBAL(o_CurrencyBE);
            ViewState["lstCurrencyBE"] = lstCurrencyBE;
        }

        if (ddlCurrency.SelectedIndex > 0) {
            lstCurrencyBE = lstCurrencyBE.Where(cc => cc.CurrencyID == Convert.ToInt32(ddlCurrency.SelectedItem.Value)).ToList();
        }

        List<CurrencyConverterBE> lstCurrencyFilterBE = new List<CurrencyConverterBE>();
        if (lstCurrencyBE != null && lstCurrencyBE.Count > 0) {            
            btnExportToExcel.Visible = true;
            lstCurrencyFilterBE = lstCurrencyBE.Where(cc => cc.IsPrevious == false).ToList();
            gvCurrentCurrency.DataSource = lstCurrencyFilterBE;
            gvCurrentCurrency.DataBind();
            lstCurrencyFilterBE = null;
            lstCurrencyFilterBE = lstCurrencyBE.Where(cc => cc.IsPrevious == true).ToList();
            gvPreviousCurrency.DataSource = lstCurrencyFilterBE;
            gvPreviousCurrency.DataBind();
        }
        else {
            btnExportToExcel.Visible = false;
            lstCurrencyFilterBE = null;
            gvCurrentCurrency.DataSource = gvPreviousCurrency.DataSource = lstCurrencyFilterBE;
            gvCurrentCurrency.DataBind();           
            gvPreviousCurrency.DataBind();
        }
    }

    protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e) {
        BindCurrencygrid();
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {
        return Utilities.GenericListHelper<CurrencyConverterBE>.SortList((List<CurrencyConverterBE>)ViewState["lstCurrencyBE"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnExport_Click(object sender, EventArgs e) {
        //Response.ClearContent();
        //Response.Buffer = true;
        //Response.ContentType = "application/force-download";
        //Response.AddHeader("content-disposition", "attachment; filename=VIPMgmtReport.xls");
        //Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
        //Response.Write("<head>");
        //Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
        //Response.Write("<!--[if gte mso 9]><xml>");
        //Response.Write("<x:ExcelWorkbook>");
        //Response.Write("<x:ExcelWorksheets>");
        //Response.Write("<x:ExcelWorksheet>");
        //Response.Write("<x:Name>VIP Management Report Overview</x:Name>");
        //Response.Write("<x:WorksheetOptions>");
        //Response.Write("<x:Print>");
        //Response.Write("<x:ValidPrinterInfo/>");
        //Response.Write("</x:Print>");
        //Response.Write("</x:WorksheetOptions>");
        //Response.Write("</x:ExcelWorksheet>");
        //Response.Write("</x:ExcelWorksheets>");
        //Response.Write("</x:ExcelWorkbook>");
        //Response.Write("</xml>");
        //Response.Write("<![endif]--> ");
        //StringWriter tw = new StringWriter(); HtmlTextWriter hw = new HtmlTextWriter(tw); tblExport.RenderControl(hw);
        //Response.Write(tw.ToString());
        //Response.Write("</head>");
        //Response.End();

        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=Details.xls");
        Response.Charset = "";
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htm = new HtmlTextWriter(sw);
        tblExport.RenderControl(htm);
        Response.Write(sw.ToString());
        Response.End();       
    }

    public override void VerifyRenderingInServerForm(Control control) {
        return;
    }
}

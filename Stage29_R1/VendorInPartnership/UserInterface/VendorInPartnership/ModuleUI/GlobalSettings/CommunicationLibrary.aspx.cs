﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.AdminFunctions;
using BaseControlLibrary;
using Utilities;


public partial class CommunicationLibrary : CommonPage
{
    #region Events ...

    protected void Page_Load(object sender, EventArgs e)
    {
        btnExcelCommLib.CurrentPage = this;
        btnExcelCommLib.GridViewControl = gvCommLibraryExcel;
        btnExcelCommLib.FileName = "CommunicationLibrary";

        if (!IsPostBack)
        {
            this.BindCommunicationLibGrid();
        }
    }

    protected void gvCommLibrary_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName.Equals("DateSentInfo"))
        //{
        //    GridViewRow oGridViewRow = (GridViewRow)(((ucLinkButton)e.CommandSource).NamingContainer);
        //    int intRowIndex = oGridViewRow.RowIndex;

        //    string strPONo = Convert.ToString(e.CommandArgument);
        //    HiddenField hdnCommunicationIDText = (HiddenField)gvCommLibrary.Rows[intRowIndex].FindControl("hdnCommunicationIDText");

        //    if (!string.IsNullOrEmpty(hdnCommunicationIDText.Value))
        //        SetEmailInfo(Convert.ToInt32(hdnCommunicationIDText.Value), true);
        //}
        //else 
        if (e.CommandName.Equals("SentToInfo"))
        {
            GridViewRow oGridViewRow = (GridViewRow)(((ucLinkButton)e.CommandSource).NamingContainer);
            int intRowIndex = oGridViewRow.RowIndex;

            string strPONo = Convert.ToString(e.CommandArgument);
            HiddenField hdnCommunicationIDText = (HiddenField)gvCommLibrary.Rows[intRowIndex].FindControl("hdnCommunicationIDText");

            if (!string.IsNullOrEmpty(hdnCommunicationIDText.Value))
                SetEmailInfo(Convert.ToInt32(hdnCommunicationIDText.Value));
        }
    }

    protected void btnBack_Click(object sender, EventArgs e) { mdlCommDetail.Hide(); }

    protected void gvCommLibrary_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["CommLibrary"] != null)
        {
            gvCommLibrary.PageIndex = e.NewPageIndex;
            gvCommLibrary.DataSource = (List<CommunicationLibraryBE>)ViewState["CommLibrary"];
            gvCommLibrary.DataBind();
        }
    }

    #endregion

    #region Methods ...

    private void BindCommunicationLibGrid()
    {

        var dateTo = txtToDate.Text;
        var dateFrom = txtFromDate.Text;
        var CommunicationIds = msTypeofCommunication.SelectedBindCommunicationValues;

        var commLibraryBAL = new CommunicationLibraryBAL();
        var commLibraryBE = new CommunicationLibraryBE();

        if (!string.IsNullOrEmpty(msTypeofCommunication.SelectedBindCommunicationValues) && !string.IsNullOrWhiteSpace(msTypeofCommunication.SelectedBindCommunicationValues))
            commLibraryBE.CommunicationIds = msTypeofCommunication.SelectedBindCommunicationValues;

        commLibraryBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtFromDate.Text);
        commLibraryBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Common.GetMM_DD_YYYY(txtToDate.Text);

        commLibraryBE.Action = "GetCommunicationLibrary";
        var lstAllCommunicationLib= commLibraryBAL.GetAllCommunicationLibBAL(commLibraryBE);
        gvCommLibrary.DataSource = null;
        gvCommLibraryExcel.DataSource = null;
        if( lstAllCommunicationLib!=null && lstAllCommunicationLib.Count>0)
        {
            gvCommLibrary.DataSource =lstAllCommunicationLib;
            gvCommLibraryExcel.DataSource = lstAllCommunicationLib;
            ViewState["CommLibrary"] = lstAllCommunicationLib;
        }        
        gvCommLibrary.DataBind();
        gvCommLibraryExcel.DataBind();
        msTypeofCommunication.SetVendorRoleOnPostBack();
    }

    private void SetEmailInfo(int communicationId)
    {
        var commLibraryBAL = new CommunicationLibraryBAL();
        var commLibraryBE = new CommunicationLibraryBE();
        commLibraryBE.Action = "GetCommunicationDetails";
        commLibraryBE.CommunicationID = communicationId;
        var emilDetails = commLibraryBAL.GetEmailCommunicationBAL(commLibraryBE);
        if (emilDetails != null && emilDetails.Count > 0)
        {
            lblSentTo.Visible = true;
            lblSentToIds.Visible = true;
            dvDisplayLetter.Visible = false;
            dvDisplayLetter.InnerHtml = string.Empty;
            lblSentToIds.Text = emilDetails[0].Users.EmailId.Replace(",", "<br/>");                

            //if (!IsEmailShow)
            //{
            //    lblSentTo.Visible = true;
            //    lblSentToIds.Visible = true;
            //    dvDisplayLetter.Visible = false;
            //    dvDisplayLetter.InnerHtml = string.Empty;
            //    lblSentToIds.Text = emilDetails[0].Users.EmailId.Replace(",","<br/>");                
            //}
            //else
            //{
            //    lblSentTo.Visible = false;
            //    lblSentToIds.Visible = false;
            //    dvDisplayLetter.Visible = true;
            //    lblSentToIds.Text = string.Empty;
            //    dvDisplayLetter.InnerHtml = emilDetails[0].Body;
            //}
        }
        mdlCommDetail.Show();
    }

    #endregion    
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindCommunicationLibGrid();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using WebUtilities;
using Utilities;

public partial class VendorVatCodeEdit : CommonPage
{
    protected string VatCodeAlreadyExist = WebCommon.getGlobalResourceValue("VatCodeAlreadyExist");
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");

    #region Events



    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();

        if (!IsPostBack)
        {
            GetVendorVatCode();
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string Addresult = string.Empty;
        int? EditResult = 0;
        try
        {
            MAS_VatCodeBAL oMAS_VatCodeBAL = new MAS_VatCodeBAL();
            MAS_VatCodeBE oMAS_VatCodeBE = new MAS_VatCodeBE();


            if (GetQueryStringValue("VendorID") != null)
            {
                oMAS_VatCodeBE.Action = "UpdateVendorVatCode";
                oMAS_VatCodeBE.Vendor = new MAS_VendorBE();
                oMAS_VatCodeBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
                oMAS_VatCodeBE.Vendor.VatCode = txtVatCode.Text.Trim();
                EditResult = oMAS_VatCodeBAL.EditDeleteVendorVatCodeBAL(oMAS_VatCodeBE);
            }

            EncryptQueryString("VendorVatCodeOverview.aspx?PreviousPage=VendorVat");

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            MAS_VatCodeBAL oMAS_VatCodeBAL = new MAS_VatCodeBAL();
            MAS_VatCodeBE oMAS_VatCodeBE = new MAS_VatCodeBE();


            if (GetQueryStringValue("VendorID") != null)
            {
                oMAS_VatCodeBE.Action = "DeleteVendorVat";
                oMAS_VatCodeBE.Vendor = new MAS_VendorBE();
                oMAS_VatCodeBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
                int? result = oMAS_VatCodeBAL.EditDeleteVendorVatCodeBAL(oMAS_VatCodeBE);

            }
            EncryptQueryString("VendorVatCodeOverview.aspx?PreviousPage=VendorVat");
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("VendorVatCodeOverview.aspx?PreviousPage=VendorVat");
    }



    #endregion

    #region Methods


    private void GetVendorVatCode()
    {
        try
        {
            if (GetQueryStringValue("VendorID") != null)
            {
                MAS_VatCodeBAL oMAS_VatCodeBAL = new MAS_VatCodeBAL();
                MAS_VatCodeBE oMAS_VatCodeBE = new MAS_VatCodeBE();

                oMAS_VatCodeBE.Action = "GetVendorVatById";
                oMAS_VatCodeBE.Vendor = new MAS_VendorBE();

                oMAS_VatCodeBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());

                List<MAS_VatCodeBE> lstMAS_VatCodeBE = oMAS_VatCodeBAL.GetVendorVatCodeBAL(oMAS_VatCodeBE);


                if (lstMAS_VatCodeBE != null && lstMAS_VatCodeBE.Count > 0)
                {
                    txtCountry.Text = lstMAS_VatCodeBE[0].Vendor.Vendor_Country;
                    txtVendorNo.Text = lstMAS_VatCodeBE[0].Vendor.Vendor_No;
                    txtVendorName.Text = lstMAS_VatCodeBE[0].Vendor.Vendor_Name;
                    txtVatCode.Text = lstMAS_VatCodeBE[0].Vendor.VatCode;
                }

                if(! string.IsNullOrEmpty(txtVatCode.Text))
                btnDelete.Visible = true;
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    #endregion
}
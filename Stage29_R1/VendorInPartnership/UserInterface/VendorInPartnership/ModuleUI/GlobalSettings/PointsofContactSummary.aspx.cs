﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.GlobalSettings;
using BusinessLogicLayer.ModuleBAL.GlobalSettings;

public partial class PointsofContactSummary : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            PointsofContactSummaryBE oPointsofContactSummaryBE = new PointsofContactSummaryBE();
            PointsofContactSummaryBAL oPointsofContactSummaryBAL = new PointsofContactSummaryBAL();
            List<PointsofContactSummaryBE> lstPOC = new List<PointsofContactSummaryBE>();
            oPointsofContactSummaryBE.Action = "GetReportSummary";
            if (Session["UserID"] != null)
            {
                oPointsofContactSummaryBE.UserID = Convert.ToInt32(Session["UserID"]);
                oPointsofContactSummaryBE.IsOpen = rblVendors.SelectedValue == "0" ? true : false;
                oPointsofContactSummaryBE.IsExcludeVendor = chkExcludeVendors.Checked == true ? true : false;
                //all + include
                if (oPointsofContactSummaryBE.IsOpen && oPointsofContactSummaryBE.IsExcludeVendor)
                    oPointsofContactSummaryBE.IsAll = 2;
                //all + not include
                else if (oPointsofContactSummaryBE.IsOpen && !oPointsofContactSummaryBE.IsExcludeVendor)
                    oPointsofContactSummaryBE.IsAll = 1;
                //--open po +  include
                else if (!oPointsofContactSummaryBE.IsOpen && oPointsofContactSummaryBE.IsExcludeVendor)
                    oPointsofContactSummaryBE.IsAll = 4;
                //open po + not include
                else if (!oPointsofContactSummaryBE.IsOpen && !oPointsofContactSummaryBE.IsExcludeVendor)
                    oPointsofContactSummaryBE.IsAll = 3;

                lstPOC = oPointsofContactSummaryBAL.GetPOCSummaryBAL(oPointsofContactSummaryBE);
                if (lstPOC != null && lstPOC.Count > 0)
                {
                    gvVendorPOC.DataSource = lstPOC;
                    gvVendorPOC.DataBind();
                    ViewState["lstPOC"] = lstPOC;
                    ucExportToExcel1.Visible = true;
                }
            }
        }

        ucExportToExcel1.GridViewControl = gvVendorPOC;
        ucExportToExcel1.FileName = "VendorPOCSummary";
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {
        return Utilities.GenericListHelper<PointsofContactSummaryBE>.SortList((List<PointsofContactSummaryBE>)ViewState["lstPOC"], e.SortExpression, e.SortDirection).ToArray();
    }
   
    protected void gvVendorPOC_RowDataBound(object sender, GridViewRowEventArgs e) {
        if (e.Row.RowType == DataControlRowType.DataRow) {
            for (int iCount = 1; iCount < e.Row.Cells.Count; iCount++)
                e.Row.Cells[iCount].Text = e.Row.Cells[iCount].Text + "%";
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        PointsofContactSummaryBE oPointsofContactSummaryBE = new PointsofContactSummaryBE();
        PointsofContactSummaryBAL oPointsofContactSummaryBAL = new PointsofContactSummaryBAL();
        List<PointsofContactSummaryBE> lstPOC = new List<PointsofContactSummaryBE>();
        oPointsofContactSummaryBE.Action = "GetReportSummary";
        if (Session["UserID"] != null)
        {
            oPointsofContactSummaryBE.UserID = Convert.ToInt32(Session["UserID"]);
            oPointsofContactSummaryBE.IsOpen = rblVendors.SelectedValue == "0" ? true : false;
            oPointsofContactSummaryBE.IsExcludeVendor = chkExcludeVendors.Checked == true ? true : false;
            //all + include
            if (oPointsofContactSummaryBE.IsOpen && oPointsofContactSummaryBE.IsExcludeVendor)
                oPointsofContactSummaryBE.IsAll = 2;
            //all + not include
            else if (oPointsofContactSummaryBE.IsOpen && !oPointsofContactSummaryBE.IsExcludeVendor)
                oPointsofContactSummaryBE.IsAll = 1;
            //--open po +  include
            else if (!oPointsofContactSummaryBE.IsOpen && oPointsofContactSummaryBE.IsExcludeVendor)
                oPointsofContactSummaryBE.IsAll = 4;
            //open po + not include
            else if (!oPointsofContactSummaryBE.IsOpen && !oPointsofContactSummaryBE.IsExcludeVendor)
                oPointsofContactSummaryBE.IsAll = 3;

            lstPOC = oPointsofContactSummaryBAL.GetPOCSummaryBAL(oPointsofContactSummaryBE);
            if (lstPOC != null && lstPOC.Count > 0)
            {
                gvVendorPOC.DataSource = lstPOC;
                gvVendorPOC.DataBind();
                ViewState["lstPOC"] = lstPOC;
            }
        }
    }
}
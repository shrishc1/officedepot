﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.GlobalSettings;
using BusinessLogicLayer.ModuleBAL.GlobalSettings;


public partial class ModuleUI_GlobalSettings_VendorAPContactSummary : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = grdVendorAPContactSummary;
       // ucExportToExcel1.IsAutoGenratedGridview = true;
        ucExportToExcel1.FileName = "VendorAccountsPayableSummary";

        if (!IsPostBack)
        {
            BindVendorAPContactSummaryDetails();
        }
    }

    protected void BindVendorAPContactSummaryDetails()
    {
        PointsofContactSummaryBE APContactBE = new PointsofContactSummaryBE();
        PointsofContactSummaryBAL APContactBAL = new PointsofContactSummaryBAL();

        APContactBE.Action = "ShowAPContactSummary";

        List<PointsofContactSummaryBE> lstAPContactSummary = APContactBAL.GetVendorAPContactSummaryDetailsBAL(APContactBE);

        if (lstAPContactSummary != null && lstAPContactSummary.Count > 0)
        {
            grdVendorAPContactSummary.DataSource = lstAPContactSummary;
            grdVendorAPContactSummary.DataBind();
            ViewState["lstAPContactSummary"] = lstAPContactSummary;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<PointsofContactSummaryBE>.SortList((List<PointsofContactSummaryBE>)ViewState["lstAPContactSummary"], e.SortExpression, e.SortDirection).ToArray();
    }


}
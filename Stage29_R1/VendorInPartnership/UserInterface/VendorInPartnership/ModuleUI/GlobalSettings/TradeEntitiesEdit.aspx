﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TradeEntitiesEdit.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
 Inherits="ModuleUI_GlobalSettings_TradeEntitiesEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
      <script type="text/javascript">

          function IsAtleastOneFieldFilled() {
             
              var txtArea = document.getElementById('<%=txtArea.ClientID %>').value;
              var txtCountry = document.getElementById('<%=txtCountry.ClientID %>').value;
              var txtCompany = document.getElementById('<%=txtCompany.ClientID %>').value;
              var txtSendPdfInvoicesTo = document.getElementById('<%=txtSendPdfInvoicesTo.ClientID %>').value;
              var txtInvoiceAddress = document.getElementById('<%=txtInvoiceAddress.ClientID %>').value;
              var txtVatNo = document.getElementById('<%=txtVatNo.ClientID %>').value;
              var txtCOCNo = document.getElementById('<%=txtCOCNo.ClientID %>').value;
              var txtType = document.getElementById('<%=txtType.ClientID %>').value;
              var txtExampleOrderNo = document.getElementById('<%=txtExampleOrderNo.ClientID %>').value;
              if (txtArea == "" && txtCountry == "" && txtCompany == "" && txtSendPdfInvoicesTo == "" && txtInvoiceAddress == ""
                  && txtVatNo == "" && txtCOCNo == "" && txtType == "" && txtExampleOrderNo == "") {
                  alert('<%=AtleastOneFieldFilled%>');
                  return false;
              }
              else {
                  return true;
              }
          }
        </script>
    <h2>
        <cc1:ucLabel ID="lblTradeEntities" runat="server" Text="Carrier Setup"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
       <div class="formbox">
                    
               
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width:1%"></td>
                    <td style="font-weight: bold;width:14%">
                        <cc1:ucLabel ID="lblRecordNo" runat="server" Text="Record #" ></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width:2%; text-align:center">
                        :
                    </td>
                    <td style="font-weight: bold; width:15%">
                        <cc1:ucTextbox ID="txtRecordNo" runat="server" Width="40px" Enabled="false"></cc1:ucTextbox>
                    </td>                     
                    <td style="font-weight: bold;text-align:center; width:3%">
                        <cc1:ucLabel ID="lblArea" runat="server" Text="Area" ></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" align="center">
                        :
                    </td>
                    <td style="font-weight: bold; width:12%">
                        <cc1:ucTextbox ID="txtArea" runat="server" Width="380px"></cc1:ucTextbox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" align="center">
                        :
                    </td>
                    <td style="font-weight: bold;"">
                        <cc1:ucTextbox ID="txtCountry" runat="server" Width="280px" ></cc1:ucTextbox>
                    </td>
                     <td style="font-weight: bold;text-align:center">
                        <cc1:ucLabel ID="lblCompany" runat="server" Text="Company" ></cc1:ucLabel>
                    </td>
                   <td style="font-weight: bold;" align="center">
                        :
                    </td>
                    <td style="font-weight: bold;" >
                        <cc1:ucTextbox ID="txtCompany" runat="server" Width="380px"></cc1:ucTextbox>
                    </td>
                </tr>
                 <tr>
                    <td></td>
                    <td style="font-weight: bold ;">
                        <cc1:ucLabel ID="lblSendPdfInvoicesto" runat="server" Text="Send PDF Invoices To"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" align="center">
                        :
                    </td>
                    <td colspan="5">
                        <cc1:ucTextbox ID="txtSendPdfInvoicesTo" runat="server"  Width="750px"></cc1:ucTextbox>
                    </td>
                 </tr>
                       <tr>
                    <td style="width:1%" rowspan="3"></td>
                    <td style="font-weight: bold;" rowspan="3" valign="top">
                        <cc1:ucLabel ID="lblInvoiceAddress" runat="server" Text="Invoice Address" ></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width:2%"; align="center" rowspan="3">
                        :
                    </td>
                    <td style="font-weight: bold;" rowspan="3" valign="top">
                        <cc1:ucTextbox ID="txtInvoiceAddress" runat="server" Width="280px" Height="60px" TextMode="MultiLine"></cc1:ucTextbox>
                    </td>                     
                    <td style="font-weight: bold;text-align:center">
                        <cc1:ucLabel ID="lblVatNo" runat="server" Text="VAT#" ></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" align="center">
                        :
                    </td>
                    <td style="font-weight: bold;"">
                        <cc1:ucTextbox ID="txtVatNo" runat="server" Width="380px"></cc1:ucTextbox>
                    </td>
                </tr>
                      <tr>                    
                    <td style="font-weight: bold;text-align:center" align="left">
                        <cc1:ucLabel ID="lblCOCNo" runat="server" Text="CoC #" ></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" align="center">
                        :
                    </td>
                    <td style="font-weight: bold;"">
                        <cc1:ucTextbox ID="txtCOCNo" runat="server" Width="380px"></cc1:ucTextbox>
                    </td>               
                    
                </tr>
                  <tr>                    
                    <td style="font-weight: bold;text-align:center" align="left">
                        <cc1:ucLabel ID="lblType" runat="server" Text="Type" ></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" align="center">
                        :
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox ID="txtType" runat="server" Width="380px"></cc1:ucTextbox>
                    </td>               
                    
                </tr>
                 <tr>   
                 <td></td>                 
                    <td style="font-weight: bold;" align="left">
                        <cc1:ucLabel ID="lblExampleOrderNo" runat="server" Text="Example Order#" ></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" align="center">
                        :
                    </td>
                    <td style="font-weight: bold;" colspan="4">
                        <cc1:ucTextbox ID="txtExampleOrderNo" runat="server" Width="750px"></cc1:ucTextbox>
                    </td>               
                    
                </tr>
            </table>

             <%--</td>
                <td>&nbsp;</td>
            </tr>
        </table>--%>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" OnClientClick="return IsAtleastOneFieldFilled();"
            ValidationGroup="a" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click"
            OnClientClick="return confirmDelete();" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

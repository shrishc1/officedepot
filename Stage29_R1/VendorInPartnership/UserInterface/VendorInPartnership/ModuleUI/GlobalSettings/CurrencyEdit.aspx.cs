﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;

public partial class CurrencyEdit :CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        if (!IsPostBack)
        {
            GetCurrencyType();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        MAS_CurrencyBAL oMAS_CurrencyBAL = new MAS_CurrencyBAL();

        CurrencyBE oCurrencyBE = new CurrencyBE();
        oCurrencyBE.Action = "AddCurrency";
        oCurrencyBE.CurrencyName = txtCurrencyType.Text.Trim();
        oCurrencyBE.CreatedBy = Session["Username"].ToString();

        int? result = oMAS_CurrencyBAL.addEditCurrencyBAL(oCurrencyBE);
        

        EncryptQueryString("Currency.aspx");
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("Currency.aspx");
    }

    private void GetCurrencyType()
    {
        if (GetQueryStringValue("CurrencyID") != null)
        {
            MAS_CurrencyBAL oMAS_CurrencyBAL = new MAS_CurrencyBAL();

            CurrencyBE oCurrencyBE = new CurrencyBE();
            oCurrencyBE.Action = "ShowAll";
            oCurrencyBE.CurrencyId = Convert.ToInt32(GetQueryStringValue("CurrencyID").ToString());

            List<CurrencyBE> lstCurrencyBE = oMAS_CurrencyBAL.GetCurrenyBAL(oCurrencyBE);


            if (lstCurrencyBE != null && lstCurrencyBE.Count > 0)
            {
                txtCurrencyType.Text = lstCurrencyBE[0].CurrencyName;
            }
            btnSave.Visible = false;
            
        }
    }

   
}
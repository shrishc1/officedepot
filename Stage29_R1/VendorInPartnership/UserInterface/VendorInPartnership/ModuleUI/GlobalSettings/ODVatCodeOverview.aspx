﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ODVatCodeOverview.aspx.cs" Inherits="ODVatCodeOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblVATcodemaintananceOD" runat="server" ></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="ODVatCodeEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="gvODVatCode" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid" AutoGenerateColumns="false"
                    AllowSorting="true" >
                    <EmptyDataTemplate>
                        <div style="text-align: center">
                            <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                        </div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Site" SortExpression="SiteName">
                            <HeaderStyle Width="50%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpSite" runat="server" Text='<%# Eval("SiteName") %>' NavigateUrl='<%# EncryptQuery("ODVatCodeEdit.aspx?ODVatCodeID="+ Eval("ODVatCodeID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="VatCode" ItemStyle-Wrap="false" DataField="VatCode" SortExpression="VatCode">
                            <HeaderStyle Width="50%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>

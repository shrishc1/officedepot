﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="PointsofContactDetail.aspx.cs" Inherits="PointsofContactDetail"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry" TagPrefix="uc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblVendorPointsofContactDetail" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <div class="right-shadow">
                <div class="formbox">
                    <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold;" width="5%">
                                <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;" width="1%" align="center">:
                            </td>
                            <td style="font-weight: bold;" width="29%">
                                <uc1:ucCountry ID="ucCountry" runat="server" />
                            </td>
                            <td style="font-weight: bold;" width="10%">
                                <cc1:ucLabel ID="lblModule" runat="server" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;" width="1%" align="center">:
                            </td>
                            <td style="font-weight: bold;" width="19%">
                                <cc1:ucDropdownList ID="ddlModule" runat="server" Width="100px" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlModule_SelectedIndexChanged">
                                </cc1:ucDropdownList>
                            </td>
                            <td style="font-weight: bold;" width="10%">
                                <cc1:ucLabel ID="lblCaptured" runat="server" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;" width="1%" align="center">:
                            </td>
                            <td style="font-weight: bold;" width="10%">
                                <cc1:ucDropdownList ID="ddlCaptured" runat="server" Width="50px">
                                    <asp:ListItem Text="ALL" Value="ALL"></asp:ListItem>
                                    <asp:ListItem Text="Y" Value="Y"></asp:ListItem>
                                    <asp:ListItem Text="N" Value="N"></asp:ListItem>
                                </cc1:ucDropdownList>
                            </td>
                            <td width="10%"></td>
                            <td width="15%"></td>

                        </tr>

                        <tr>
                            <td style="font-weight: bold;" width="35%" colspan="3">
                                <asp:RadioButtonList ID="rblVendors" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="0" Selected="True">All Vendors</asp:ListItem>
                                    <asp:ListItem Value="1">Only Vendors with open PO's</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td style="font-weight: bold;" width="10%" colspan="3">

                                <asp:CheckBox ID="chkExcludeVendors" runat="server" Text="Include Excluded Vendors" />
                            </td>


                            <td style="font-weight: bold;" width="1%" align="center"></td>
                            <td style="font-weight: bold;" width="10%"></td>
                            <td width="10%">
                                <cc1:ucButton ID="btnSearch" runat="server" CssClass="button" OnClick="btnSearch_Click" />
                            </td>
                            <td width="15%">
                                <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                                    Width="109px" Height="20px" Visible="false" />
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <span id="spCountry" runat="server">
                                    <cc1:ucGridView ID="gvVendorPOC" AllowSorting="true" OnSorting="SortGrid" runat="server"
                                        AutoGenerateColumns="false" CssClass="grid gvclass" CellPadding="0" Width="960px"
                                        PageSize="50" AllowPaging="true" OnPageIndexChanging="gvVendorPOC_PageIndexChanging">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">
                                                <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                            </div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <%--<asp:BoundField HeaderText="VendorName" DataField="VendorName" SortExpression="VendorName">
                                            <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>--%>
                                            <asp:TemplateField HeaderText="VendorName" SortExpression="VendorName">
                                                <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hpVendorLink" Target="_blank" runat="server" NavigateUrl='<%# EncryptQuery("../GlobalSettings/VendorEdit.aspx?PageFrom=vendorPOCList&VendorID="+Eval("VendorID")) %>'
                                                        Text='<%#Eval("VendorName") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Lines" DataField="Lines" SortExpression="Lines">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Weight" DataField="Weightage" SortExpression="Weightage">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Scheduling" DataField="SchedulingPOC" SortExpression="SchedulingPOC">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Discrepancies" DataField="DiscrepancyPOC" SortExpression="DiscrepancyPOC">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="OTIF" DataField="OTIFPOC" SortExpression="OTIFPOC">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Scorecard" DataField="ScorecardPOC" SortExpression="ScorecardPOC">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Inventory" DataField="InventoryPOC" SortExpression="InventoryPOC">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Procurement" DataField="ProcurementPOC" SortExpression="ProcurementPOC">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Accounts Payable" DataField="AccountsPayablePOC" SortExpression="AccountsPayablePOC">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Executive" DataField="ExecutivePOC" SortExpression="ExecutivePOC">
                                                <HeaderStyle Width="40px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                    </cc1:ucGridView>
                                    <cc1:ucGridView ID="gvVendorPOCExport" runat="server" AutoGenerateColumns="false"
                                        CssClass="grid gvclass" CellPadding="0" Width="960px" Style="display: none;">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">
                                                <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                            </div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:BoundField HeaderText="VendorName" DataField="VendorName" SortExpression="VendorName">
                                                <HeaderStyle Width="260px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Lines" DataField="Lines" SortExpression="Lines">
                                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Weight" DataField="Weightage" SortExpression="Weightage">
                                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Scheduling" DataField="SchedulingPOC" SortExpression="SchedulingPOC">
                                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Discrepancies" DataField="DiscrepancyPOC" SortExpression="DiscrepancyPOC">
                                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="OTIFC" DataField="OTIFPOC" SortExpression="OTIFPOC">
                                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Scorecard" DataField="ScorecardPOC" SortExpression="ScorecardPOC">
                                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Inventory" DataField="InventoryPOC" SortExpression="InventoryPOC">
                                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Procurement" DataField="ProcurementPOC" SortExpression="ProcurementPOC">
                                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Accounts Payable" DataField="AccountsPayablePOC" SortExpression="AccountsPayablePOC">
                                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Executive" DataField="ExecutivePOC" SortExpression="ExecutivePOC">
                                                <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                    </cc1:ucGridView>
                                </span><span id="spSite" runat="server" visible="false">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <table border="0" cellspacing="0" cellpadding="0" class="grid gvclass" width="100%">
                                                    <tr>
                                                        <th style="width: 150px; text-align: left;">
                                                            <cc1:ucLabel ID="lblVendor" runat="server"></cc1:ucLabel>
                                                        </th>
                                                        <th style="width: 25px; text-align: center;">
                                                            <cc1:ucLabel ID="lblLines" runat="server"></cc1:ucLabel>
                                                        </th>
                                                        <th style="width: 25px; text-align: center;">
                                                            <cc1:ucLabel ID="lblWeight" runat="server"></cc1:ucLabel>
                                                        </th>
                                                        <th style="width: 45px; text-align: center;">
                                                            <cc1:ucLabel ID="ltPOC" runat="server"></cc1:ucLabel>
                                                        </th>
                                                        <asp:Repeater ID="rptSiteHeader" runat="server">
                                                            <ItemTemplate>
                                                                <th style="width: 45px; text-align: center;">
                                                                    <asp:Literal ID="ltSiteName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SiteName") %>'></asp:Literal>
                                                                </th>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tr>
                                                    <asp:Repeater ID="rptPOReports" runat="server" OnItemDataBound="rptPOCReports_OnItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="color: #333333; background-color: #F7F6F3; text-align: left;">
                                                                    <%--<asp:Literal ID="ltVendor" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "VendorName") %>'></asp:Literal>--%>

                                                                    <asp:HyperLink ID="hpVendorLink" Target="_blank" runat="server" NavigateUrl='<%# EncryptQuery("../GlobalSettings/VendorEdit.aspx?PageFrom=vendorPOCList&VendorID="+Eval("VendorID")) %>'
                                                                        Text='<%#Eval("VendorName") %>'></asp:HyperLink>
                                                                </td>
                                                                <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                                                                    <asp:Literal ID="ltLines" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Lines") %>'></asp:Literal>
                                                                </td>
                                                                <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                                                                    <asp:Literal ID="ltWeight" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Weightage") %>'></asp:Literal>
                                                                </td>
                                                                <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                                                                    <asp:Literal ID="ltPOCCountry" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "POCCountry") %>'></asp:Literal>
                                                                </td>
                                                                <asp:Repeater ID="rptSiteValue" runat="server">
                                                                    <ItemTemplate>
                                                                        <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                                                                            <asp:Literal ID="ltPOCSite" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "POC") %>'></asp:Literal>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <AlternatingItemTemplate>
                                                            <tr>
                                                                <td style="color: #333333; text-align: left;">
                                                                    <%--<asp:Literal ID="ltVendor" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "VendorName") %>'></asp:Literal>--%>
                                                                    <asp:HyperLink ID="hpVendorLink" Target="_blank" runat="server" NavigateUrl='<%# EncryptQuery("../GlobalSettings/VendorEdit.aspx?PageFrom=vendorPOCList&VendorID="+Eval("VendorID")) %>'
                                                                        Text='<%#Eval("VendorName") %>'></asp:HyperLink>
                                                                </td>
                                                                <td style="color: #333333; text-align: center;">
                                                                    <asp:Literal ID="ltLines" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Lines") %>'></asp:Literal>
                                                                </td>
                                                                <td style="color: #333333; text-align: center;">
                                                                    <asp:Literal ID="ltWeight" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Weightage") %>'></asp:Literal>
                                                                </td>
                                                                <td style="color: #333333; text-align: center;">
                                                                    <asp:Literal ID="ltPOCCountry" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "POCCountry") %>'></asp:Literal>
                                                                </td>
                                                                <asp:Repeater ID="rptSiteValue" runat="server">
                                                                    <ItemTemplate>
                                                                        <td style="color: #333333; text-align: center;">
                                                                            <asp:Literal ID="ltPOCSite" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "POC") %>'></asp:Literal>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </AlternatingItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellspacing="0" cellpadding="0" align="left" class="form-table">
                                        <tr>
                                            <td align="left">
                                                <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false"></cc1:PagerV2_8>
                                            </td>
                                        </tr>
                                    </table>

                                    <table style="border: 1px solid #d9d9d9; display: none;" cellpadding="1" cellspacing="1" id="tblSite"
                                        runat="server" width="100%">
                                        <tr>
                                            <td>
                                                <table border="1" cellspacing="0" cellpadding="0" class="grid gvclass searchgrid-1" width="100%">
                                                    <tr>
                                                        <th style="width: 150px; text-align: left; background-color: #f5f1e8; color: #934500!important;">
                                                            <cc1:ucLabel ID="lblVendor_1" runat="server"></cc1:ucLabel>
                                                        </th>
                                                        <th style="width: 25px; text-align: center; background-color: #f5f1e8; color: #934500!important;">
                                                            <cc1:ucLabel ID="lblLines_1" runat="server"></cc1:ucLabel>
                                                        </th>
                                                        <th style="width: 25px; text-align: center; background-color: #f5f1e8; color: #934500!important;">
                                                            <cc1:ucLabel ID="lblWeight_1" runat="server"></cc1:ucLabel>
                                                        </th>
                                                        <th style="width: 45px; text-align: center; background-color: #f5f1e8; color: #934500!important;">
                                                            <cc1:ucLabel ID="ltPOCExport" runat="server"></cc1:ucLabel>
                                                        </th>
                                                        <asp:Repeater ID="rptSiteHeaderExport" runat="server">
                                                            <ItemTemplate>
                                                                <th style="width: 45px; text-align: center; background-color: #f5f1e8; color: #934500!important;">
                                                                    <asp:Literal ID="ltSiteName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SiteName") %>'></asp:Literal>
                                                                </th>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tr>
                                                    <asp:Repeater ID="rptPOCReportsExport" runat="server" OnItemDataBound="rptPOCReports_OnItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="color: #333333; background-color: #F7F6F3; text-align: left;">
                                                                    <asp:Literal ID="ltVendor" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "VendorName") %>'></asp:Literal>
                                                                </td>
                                                                <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                                                                    <asp:Literal ID="ltLines" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Lines") %>'></asp:Literal>
                                                                </td>
                                                                <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                                                                    <asp:Literal ID="ltWeight" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Weightage") %>'></asp:Literal>
                                                                </td>
                                                                <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                                                                    <asp:Literal ID="ltPOCCountry" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "POCCountry") %>'></asp:Literal>
                                                                </td>
                                                                <asp:Repeater ID="rptSiteValue" runat="server">
                                                                    <ItemTemplate>
                                                                        <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                                                                            <asp:Literal ID="ltPOCSite" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "POC") %>'></asp:Literal>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <AlternatingItemTemplate>
                                                            <tr>
                                                                <td style="color: #333333; text-align: left;">
                                                                    <asp:Literal ID="ltVendor" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "VendorName") %>'></asp:Literal>
                                                                </td>
                                                                <td style="color: #333333; text-align: center;">
                                                                    <asp:Literal ID="ltLines" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Lines") %>'></asp:Literal>
                                                                </td>
                                                                <td style="color: #333333; text-align: center;">
                                                                    <asp:Literal ID="ltWeight" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Weightage") %>'></asp:Literal>
                                                                </td>
                                                                <td style="color: #333333; text-align: center;">
                                                                    <asp:Literal ID="ltPOCCountry" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "POCCountry") %>'></asp:Literal>
                                                                </td>
                                                                <asp:Repeater ID="rptSiteValue" runat="server">
                                                                    <ItemTemplate>
                                                                        <td style="color: #333333; text-align: center;">
                                                                            <asp:Literal ID="ltPOCSite" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "POC") %>'></asp:Literal>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </AlternatingItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using WebUtilities;
using Utilities;

public partial class ODVatCodeEdit : CommonPage
{

    protected string VatCodeAlreadyExist = WebCommon.getGlobalResourceValue("VatCodeAlreadyExist");
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    #region Events

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
        ucSite.IsSelectRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        
    }
    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            if (GetQueryStringValue("ODVatCodeID") == null)
            {
                ucSite.innerControlddlSite.SelectedIndex = 0;

            }
            else
            {
                GetODVatCode();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string Addresult = string.Empty;
        int? EditResult = 0;
        try
        {
            MAS_VatCodeBAL oMAS_VatCodeBAL = new MAS_VatCodeBAL();
            MAS_VatCodeBE oMAS_VatCodeBE = new MAS_VatCodeBE();
            
            oMAS_VatCodeBE.SiteId = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
            oMAS_VatCodeBE.VatCode = txtVatCode.Text.ToString();
            oMAS_VatCodeBE.IsActive = true;


            if (GetQueryStringValue("ODVatCodeID") != null)
            {
                oMAS_VatCodeBE.Action = "EditODVatCode";
                oMAS_VatCodeBE.ODVatCodeID = Convert.ToInt32(GetQueryStringValue("ODVatCodeID"));
                EditResult = oMAS_VatCodeBAL.EditDeleteODVatCodeBAL(oMAS_VatCodeBE);
            }
            else
            {
                oMAS_VatCodeBE.Action = "AddODVatCode";
                Addresult = oMAS_VatCodeBAL.AddODVatCodeDAL(oMAS_VatCodeBE);
            }



             if (Addresult == "AlreadyExist")
             {
                 ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + VatCodeAlreadyExist + "')", true);
                 return;
             }
             else
             {
                 EncryptQueryString("ODVatCodeOverview.aspx");
             }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            MAS_VatCodeBAL oMAS_VatCodeBAL = new MAS_VatCodeBAL();
            MAS_VatCodeBE oMAS_VatCodeBE = new MAS_VatCodeBE();

            oMAS_VatCodeBE.Action = "DeleteODVatCode";
            if (GetQueryStringValue("ODVatCodeID") != null)
            {
                oMAS_VatCodeBE.ODVatCodeID = Convert.ToInt32(GetQueryStringValue("ODVatCodeID"));
                int? result = oMAS_VatCodeBAL.EditDeleteODVatCodeBAL(oMAS_VatCodeBE);

            }
            EncryptQueryString("ODVatCodeOverview.aspx");
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("ODVatCodeOverview.aspx");
    }



    #endregion

    #region Methods


    private void GetODVatCode()
    {
        try
        {
            if (GetQueryStringValue("ODVatCodeID") != null)
            {
                MAS_VatCodeBAL oMAS_VatCodeBAL = new MAS_VatCodeBAL();
                MAS_VatCodeBE oMAS_VatCodeBE = new MAS_VatCodeBE();

                oMAS_VatCodeBE.Action = "ShowAllODVatCode";
                oMAS_VatCodeBE.ODVatCodeID = Convert.ToInt32(GetQueryStringValue("ODVatCodeID").ToString());

                List<MAS_VatCodeBE> lstMAS_VatCodeBE = oMAS_VatCodeBAL.GetODVatCodeBAL(oMAS_VatCodeBE);


                if (lstMAS_VatCodeBE != null && lstMAS_VatCodeBE.Count > 0)
                {
                    ucSite.innerControlddlSite.SelectedValue = Convert.ToString(lstMAS_VatCodeBE[0].SiteId);
                    txtVatCode.Text = lstMAS_VatCodeBE[0].VatCode;
                }
                ucSite.innerControlddlSite.Enabled = false;
                btnDelete.Visible = true;
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    #endregion
}
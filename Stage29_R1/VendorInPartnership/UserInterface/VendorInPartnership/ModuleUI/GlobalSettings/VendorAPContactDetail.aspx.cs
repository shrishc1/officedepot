﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.GlobalSettings;
using BusinessLogicLayer.ModuleBAL.GlobalSettings;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using WebUtilities;
using BaseControlLibrary;

public partial class ModuleUI_GlobalSettings_VendorAPContactDetail : CommonPage
{
    protected string CheckBoxRequired = "-" + WebCommon.getGlobalResourceValue("CheckboxSelect");
    protected string SuccessMesg = "-" + WebCommon.getGlobalResourceValue("SuccessMesg");
    protected void Page_Init(object sender, EventArgs e)
    {
        ddlCountry.CurrentPage = this;
        ucExportToExcel1.CurrentPage = this;
       // ddlCountry.innerControlddlCountry.AutoPostBack = true;
        //ucExportToExcel1.IsHideHidden = true;     

        //ddlCountry.IsAllDefault = true;
        //ddlCountry.IsAllRequired = true;
    }
    bool ispostback=false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("CountryID") != null)
            {
                ViewState["CountryID"] = true;
            }
            ispostback = true;
            //BindVendorAPContactDetails();
        }

        //if (ddlAPStatus.SelectedValue.Equals("0"))
        //{
            ucExportToExcel1.GridViewControl = grdAPStatusAll;
            ucExportToExcel1.FileName = "VendorAccountsPayableAllContactDetail";
          //  ucExportToExcel1.IsHideHidden = false;
       // }
        //else
        //{
        //    ucExportToExcel1.GridViewControl = grdAPStatusNoContact;
        //    ucExportToExcel1.IsHideHidden = true;
        //    ucExportToExcel1.FileName = "VendorAccountsPayableNoContactDetail";
        //}

        // ucExportToExcel1.IsAutoGenratedGridview = true;
        
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {    
        
        BindVendorAPContactDetails();
        setData();
    }

    public void setData()
    {
        msVendor.setVendorsOnPostBack();
        ////************************* Vendor ***************************
        string IncludeVendorId = msVendor.SelectedVendorIDs;
        TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;
        string txtVendorIdText = txtVendorId.Text;
        ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
        // lstRightVendor.Items.Clear();
        if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(IncludeVendorId) || !string.IsNullOrEmpty(txtVendorIdText)))
        {
            lstLeftVendor.Items.Clear();
            //lstRightVendor.Items.Clear();
            msVendor.SearchVendorClick(txtVendorIdText);
            if (!string.IsNullOrEmpty(IncludeVendorId))
            {
                string[] strIncludeVendorIDs = IncludeVendorId.Split(',');
                for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                    if (listItem != null)
                    {
                        // lstRightVendor.Items.Add(listItem);
                        lstLeftVendor.Items.Remove(listItem);
                    }
                }
            }
        }

    }

    

    protected void BindVendorAPContactDetails()
    {
        PointsofContactSummaryBE APContactBE = new PointsofContactSummaryBE();
        PointsofContactSummaryBAL APContactBAL = new PointsofContactSummaryBAL();

        APContactBE.Action = "BindVendorAPContactDetails";
        if (ViewState["CountryID"] != null)
        {
            APContactBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
            ddlCountry.innerControlddlCountry.SelectedIndex = ddlCountry.innerControlddlCountry.Items.IndexOf(ddlCountry.innerControlddlCountry.Items.FindByValue(GetQueryStringValue("CountryID")));
            APContactBE.IsAllContact = false;
        }
        else
        {
            if (ispostback==false)
            {
                APContactBE.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue);
                APContactBE.IsAllContact = ddlAPStatus.SelectedValue == "0" ? false : true;
            }
            else
            {
                return;
            }
           
        }

         APContactBE.SCSelectedVendorIDs = msVendor.SelectedVendorIDs;

        List<PointsofContactSummaryBE> lstAPContactDetails = APContactBAL.GetVendorAPContactDetailsBAL(APContactBE);
        if (ViewState["CountryID"] != null)
        {
            ViewState["CountryID"] = null;            
        }
        ispostback = false;
        if (lstAPContactDetails != null && lstAPContactDetails.Count > 0)
        {
           
            if (ddlAPStatus.SelectedIndex == 0)
            {
                ucExportToExcel1.Visible = true;
                grdAPStatusAll.Visible = true;
                grdAPStatusAll.DataSource = lstAPContactDetails;
                grdAPStatusAll.DataBind();
                grdAPStatusNoContact.DataSource = null;
                grdAPStatusNoContact.DataBind();
                grdAPStatusNoContact.Visible = false;
                AssignAPClerk.Visible = false;
                ViewState["lstAPContactDetails"] = lstAPContactDetails;

            }
            else
            {
                ucExportToExcel1.Visible = false;
                AssignAPClerk.Visible = true;
                grdAPStatusNoContact.Visible = true;
                grdAPStatusNoContact.DataSource = lstAPContactDetails;
                grdAPStatusNoContact.DataBind();
                ViewState["lstAPContactDetails"] = lstAPContactDetails;
                grdAPStatusAll.DataSource = null;
                grdAPStatusAll.Visible = false;

                SCT_UserBE oSCT_UserBE = new SCT_UserBE();
                SCT_UserBAL oMAS_UserBAL = new SCT_UserBAL();
        
                oSCT_UserBE.Action = "ShowAll";
                oSCT_UserBE.RoleName = "OD - Accounts Payable";

                List<SCT_UserBE> lstUsers = oMAS_UserBAL.GetUserDetailsBAL(oSCT_UserBE);
                if (lstUsers != null && lstUsers.Count > 0)
                {
                    ddlAPClerk.DataSource = lstUsers;
                    ddlAPClerk.DataTextField = "APUserName";
                    ddlAPClerk.DataValueField = "UserID";
                    ddlAPClerk.DataBind();

                }
            }
        }
        else
        {
            grdAPStatusAll.DataSource = null;
            grdAPStatusAll.DataBind();
            grdAPStatusNoContact.DataSource = null;
            grdAPStatusNoContact.DataBind();
            grdAPStatusNoContact.Visible = false;
            AssignAPClerk.Visible = false;
            ucExportToExcel1.Visible = false;
            //if (ddlAPStatus.SelectedIndex == 0)
            //{
            //    AssignAPClerk.Visible = false;
            //}
            //else
            //{
            //    AssignAPClerk.Visible = true;
            //}
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<PointsofContactSummaryBE>.SortList((List<PointsofContactSummaryBE>)ViewState["lstAPContactDetails"], e.SortExpression, e.SortDirection).ToArray();
    }



    protected void btnAssignAPClerk_Click(object sender, EventArgs e)
    {
        int count = 0;
        PointsofContactSummaryBE APContactBE = new PointsofContactSummaryBE();
        PointsofContactSummaryBAL APContactBAL = new PointsofContactSummaryBAL();

        APContactBE.Action = "AssignAPClerkIds";

        foreach (GridViewRow row in grdAPStatusNoContact.Rows)
        {
            CheckBox chk = (CheckBox)row.FindControl("chkRow");
                       
            if ((chk != null) && chk.Checked)
            {
                count = count + 1;
                HiddenField hdn = (HiddenField)row.FindControl("hdnVendorID");
                int vendorid =Convert.ToInt32(hdn.Value);
                int userid=Convert.ToInt32(ddlAPClerk.SelectedValue);
                APContactBE.UserID = userid;
                APContactBE.VendorID = vendorid;                
                APContactBAL.SaveAPClerk(APContactBE);
                // Do something with each row here...
            }
        }
        if (count == 0)
        {
            ScriptManager.RegisterClientScriptBlock((Page)HttpContext.Current.CurrentHandler, this.GetType(), "Message", "alert('" + CheckBoxRequired + "')", true);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock((Page)HttpContext.Current.CurrentHandler, this.GetType(), "Message", "alert('" + SuccessMesg + "')", true);
            BindVendorAPContactDetails();
           
        }
        setData();
    }

    //public override void CountrySelectedIndexChanged()
    //{
    //    if (ddlCountry.innerControlddlCountry.SelectedIndex > 0)
    //    {
    //        ViewState["CountryID"] = ddlCountry.innerControlddlCountry.SelectedValue;
    //        grdAPStatusAll.DataSource = null;
    //        grdAPStatusAll.DataBind();
    //        grdAPStatusNoContact.DataSource = null;
    //        grdAPStatusNoContact.DataBind();
    //    }
       
    //}
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("VendorAPContactSummary.aspx");
    }

    public override void CountryPost_Load()
    {
        if (!IsPostBack)
        {
            BindVendorAPContactDetails();
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Resources;
using System.Collections;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Xml.Linq;
using System.Linq;
using WebUtilities;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;
using DataAccessLayer;




public partial class LanguageTagsoverview : CommonPage
{

    bool IsExportClicked = false;

    #region Methods

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            GetFilteredResult();
        }
    }

   

    private void GetFilteredResult()
    {

        string EnglishPath = string.Empty;
        string DutchPath = string.Empty;
        string ItalianPath = string.Empty;
        string FrenchPath = string.Empty;
        string SpanishPath = string.Empty;
        string GermanPath = string.Empty;     

        EnglishPath = AppDomain.CurrentDomain.BaseDirectory.ToLower() + @"App_GlobalResources\Officedepot.resx";     
        DutchPath = AppDomain.CurrentDomain.BaseDirectory.ToLower() + @"App_GlobalResources\Officedepot.nl.resx";
        ItalianPath = AppDomain.CurrentDomain.BaseDirectory.ToLower() + @"App_GlobalResources\Officedepot.it.resx";
        FrenchPath = AppDomain.CurrentDomain.BaseDirectory.ToLower() + @"App_GlobalResources\Officedepot.fr.resx";
        SpanishPath = AppDomain.CurrentDomain.BaseDirectory.ToLower() + @"App_GlobalResources\Officedepot.es.resx";
        GermanPath = AppDomain.CurrentDomain.BaseDirectory.ToLower() + @"App_GlobalResources\Officedepot.de.resx";
        

        //Loading the XML file using the Load method of XElement. 
        var resourceElementsEng = XElement.Load(EnglishPath);
        var resourceElementsDutchPath = XElement.Load(DutchPath);
        var resourceElementsItalianPath = XElement.Load(ItalianPath);
        var resourceElementsFrenchPath = XElement.Load(FrenchPath);
        var resourceElementsSpanishPath = XElement.Load(SpanishPath);
        var resourceElementsGermanPath = XElement.Load(GermanPath);
       

        //LINQ to load the resource items 
        var lstEnglish = (from resElem in resourceElementsEng.Elements("data")

                          select new
                          {
                              Key = resElem.Attribute("name").Value,
                              Value = resElem.Element("value").Value
                          }).ToList();


        var lstDutchLang = (from resElem in resourceElementsDutchPath.Elements("data")
                               select new
                               {
                                   DutchKey = resElem.Attribute("name").Value,
                                   DutchValue = resElem.Element("value").Value
                               }).ToList();
        var lstItalyLang = (from resElem in resourceElementsItalianPath.Elements("data")
                            select new
                            {
                                ItalyKey = resElem.Attribute("name").Value,
                               ItalyValue = resElem.Element("value").Value
                            }).ToList();

        var lstFrenchLang = (from resElem in resourceElementsFrenchPath.Elements("data")
                            select new
                            {
                                FrenchKey = resElem.Attribute("name").Value,
                                FrenchValue = resElem.Element("value").Value
                            }).ToList();

        var lstSpanishLang = (from resElem in resourceElementsSpanishPath.Elements("data")
                            select new
                            {
                                SpanishKey = resElem.Attribute("name").Value,
                                SpanishValue = resElem.Element("value").Value
                            }).ToList();

        var lstGermanLang = (from resElem in resourceElementsGermanPath.Elements("data")
                            select new
                            {
                                GermanKey = resElem.Attribute("name").Value,
                                GermanValue = resElem.Element("value").Value
                            }).ToList();



        ////////

        //var missingNames = lstSelectedLang.Select(t => t.SelectedLangKey)
        //                  .Except(lstEnglish.Select(e => e.Key))
        //                  .ToList();

        //////////



       



        var query = (from E in lstEnglish
                     join D in lstDutchLang on E.Key equals D.DutchKey
                     join I in lstItalyLang on E.Key equals I.ItalyKey
                     join F in lstFrenchLang on E.Key equals F.FrenchKey
                     join S in lstSpanishLang on E.Key equals S.SpanishKey
                     join G in lstGermanLang on E.Key equals G.GermanKey

                     select new ResourceResult
                     {
                         LanguageKey = E.Key,
                         English = E.Value,
                         French = F.FrenchValue,
                         German=G.GermanValue,
                         Dutch = D.DutchValue,
                         Spanish = S.SpanishValue,
                         Italian = I.ItalyValue

                        
                     }).ToList();

     

        BulkInsert(DBConnection.Connection.ToString(), "MAS_LanguageTags", query);

      
    }

    public static void BulkInsert<T>(string connection, string tableName, IList<T> list)
    {
        using (var bulkCopy = new SqlBulkCopy(connection))
        {
            bulkCopy.BatchSize = list.Count;
            bulkCopy.DestinationTableName = tableName;

            var table = new DataTable();
            var props = TypeDescriptor.GetProperties(typeof(T))
                //Dirty hack to make sure we only have system data types
                //i.e. filter out the relationships/collections
            .Cast<PropertyDescriptor>()
            .Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System"))
            .ToArray();
            foreach (var propertyInfo in props)
            {
                bulkCopy.ColumnMappings.Add(propertyInfo.Name, propertyInfo.Name);
                table.Columns.Add(propertyInfo.Name, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType);
            }

            var values = new object[props.Length];
            foreach (var item in list)
            {
                for (var i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }

                table.Rows.Add(values);
            }

            bulkCopy.WriteToServer(table);
        }
    }

    #endregion

   
 

    private class ResourceResult
    {
        public string LanguageKey { get; set; }
        //public string Value { get; set; }
        //public string Translation { get; set; }

        public string English { get; set; }
        public string French { get; set; }
        public string German { get; set; }
        public string Dutch { get; set; }
        public string Spanish { get; set; }
        public string Italian { get; set; }
       
        
      

    }

}




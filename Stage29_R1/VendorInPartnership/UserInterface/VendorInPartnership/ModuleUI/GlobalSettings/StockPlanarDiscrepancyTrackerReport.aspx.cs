﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.AdminFunctions;
public partial class ModuleUI_GlobalSettings_StockPlanarDiscrepancyTrackerReport : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CommunicationLibraryBAL communicationLibraryBAL = new CommunicationLibraryBAL();
            DiscrepancyNotification discrepancyNotification = new DiscrepancyNotification();
            discrepancyNotification.Action = "GetDiscrepancyTrackerCommunication";
            GvStockPlanarDiscrepancyTrackerReport.DataSource = communicationLibraryBAL.GetStockPlanarDiscrepancyTrackerBAL(discrepancyNotification); ;
            GvStockPlanarDiscrepancyTrackerReport.DataBind();
        }
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = GvStockPlanarDiscrepancyTrackerReport;
        ucExportToExcel1.FileName = "StockPlannarDiscrepancyTrackerReport";
        ucExportToExcel1.Visible = true;
    }

    protected void GvStockPlanarDiscrepancyTrackerReport_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Dwn")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GvStockPlanarDiscrepancyTrackerReport.Rows[index];
            HiddenField hdnWeekFile = (HiddenField)row.FindControl("hdnWeekFile");
            string fName = hdnWeekFile.Value;
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + fName);
            Response.TransmitFile(fName);
            Response.End();
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OverstockAgreementEdit.aspx.cs"  MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
 Inherits="ModuleUI_GlobalSettings_OverstockAgreementEdit" %>


<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<script language="javascript" type="text/javascript">
    function IsNotesEnteredOnYes() {              
        if ($('input[id$=rdoYes]').attr('checked')) {
            var Notes = $("#<%=txtNotes.ClientID %>").val();
            if (Notes.trim() == "") {
                alert('<%=PleaseEnterNotes%>');
                return false;
            }
            else {
                return true;
            }
        }
    }

</script>
<h2>
 <cc1:ucLabel ID="lblOverstockAgreement" runat="server"></cc1:ucLabel>
</h2>


 <div class="formbox">
            <table width="80%" cellspacing="7" cellpadding="0" border="0" align="center" class="top-settings">
              
                <tr>                    
                    <td style="font-weight: bold;width:60px;">
                        <cc1:ucLabel ID="lblCountry" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold ; width:5px;">
                        :
                    </td>
                    <td style="font-weight: bold;width:100px;">
                    
                        <cc1:ucTextbox ID="txtCountry" ValidationGroup="a" runat="server" Width="200px" Enabled="false"></cc1:ucTextbox>
                    </td>
                   
                </tr>
                <tr>                   
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblVendor" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucTextbox ID="txtVendor" ValidationGroup="a" runat="server" Width="200px" Enabled="false"></cc1:ucTextbox>
                    </td>
                    
                </tr>
                <tr>                    
                    <td style="font-weight: bold" colspan="4">
                        <cc1:ucLabel ID="lblIsVendorOverstockAgreement" runat="server"></cc1:ucLabel>
                    </td> 
                </tr>
                 <tr>                    
                    <td style="font-weight: bold" colspan="4">
                        <cc1:ucRadioButton ID="rdoYes" runat="server" GroupName="abcd"></cc1:ucRadioButton> &nbsp;&nbsp;
                        
                        <cc1:ucRadioButton ID="rdoNo" runat="server" GroupName="abcd"></cc1:ucRadioButton> 
                        <br />
                        <br />
                    </td> 
                </tr>
                 <tr>                    
                    <td style="font-weight: bold" colspan="4">
                        <cc1:ucLabel ID="lblNotes"  runat="server"></cc1:ucLabel>
                    </td>                      
                </tr>
                <tr>
                <td style="font-weight: bold" colspan="4">
                        <cc1:ucTextbox ID="txtNotes" ValidationGroup="a" runat="server" Width="780px" ></cc1:ucTextbox>
                    </td> 
                </tr>
            </table>
        </div>

    <div class="bottom-shadow">
    </div>
    <div class="button-row">
       <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"   OnClientClick="return IsNotesEnteredOnYes();" />
    </div>


</asp:Content>

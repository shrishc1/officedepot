﻿using System;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.GlobalSettings;
using BusinessLogicLayer.ModuleBAL.GlobalSettings;
using System.Web.UI;

public partial class ModuleUI_GlobalSettings_VendorPOCExclusionsEdit : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("Mode") == "Edit" && !string.IsNullOrEmpty(GetQueryStringValue("VendorExclusionID")))
            {
                VendorPOCExclusionBAL vendorPOCExclusionBAL = new VendorPOCExclusionBAL();
                VendorPOCExclusionBE vendorPOCExclusionBE = new VendorPOCExclusionBE();
                vendorPOCExclusionBE.Action = "GetVendorPOCExclusionEdit";
                vendorPOCExclusionBE.VendorExclusionID = Convert.ToInt32(GetQueryStringValue("VendorExclusionID"));
                VendorPOCExclusionBE objPOCExclusion = vendorPOCExclusionBAL.GetEditVendorPOCExclusionBAL(vendorPOCExclusionBE);
                if (objPOCExclusion != null)
                {
                    hdnLocalvendorID.Value = objPOCExclusion.VendorID.ToString();
                    txtSelectedVendorValue.Text = "( " + objPOCExclusion.CountryName + " )" + objPOCExclusion.VendorNo + "- " + objPOCExclusion.VendorName.ToString();
                    txtReason.Text = objPOCExclusion.ReasonForExclusion;
                }
                btnSubmit.Visible = false;
                btnDelete.Visible = true;
            }
            else
            {
                btnSubmit.Visible = true;
                btnDelete.Visible = false;
            }
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SearchVendor(ref lstLeft, txtVendorNo.Text, "Local");
    }
    private void SearchVendor(ref ucListBox lstList, string VendorName, string ReportType)
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetConsVendorByName";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["Userid"]);
        oUP_VendorBE.VendorName = VendorName;
        if (ReportType == "Local")
            oUP_VendorBE.VendorFlag = 'P';
        else if (ReportType == "Global")
            oUP_VendorBE.VendorFlag = 'G';

        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetVendorByUserCountryBAL(oUP_VendorBE);


        oUP_VendorBAL = null;
        if (lstUPVendor.Count > 0)
        {

            FillControls.FillListBox(ref lstList, lstUPVendor, "VendorName", "VendorID");

            /// --- Added for Showing Inative Vendors --- 27 Feb 2013
            for (int i = 0; i < lstUPVendor.Count; i++)
            {
                if (lstUPVendor[i].VendorID > 0)
                    if (Convert.ToInt32(lstList.Items[i].Value) == lstUPVendor[i].VendorID && lstUPVendor[i].IsActiveVendor == "N")
                        lstList.Items[i].Attributes.Add("Style", "background-color:RED;");
            }
            /// --- Added for Showing Inative Vendors --- 27 Feb 2013
        }
        else
        {
            lstList.Items.Clear();
        }
    }
    protected void btnSearchByVendorNo_Click(object sender, EventArgs e)
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetAllLevelVendorDetailsByVendorNo";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["Userid"]);
        oUP_VendorBE.Vendor_No = txtVendorNo.Text;

        //oUP_VendorBE.VendorFlag = 'P';

        //oUP_VendorBE.VendorFlag = 'G';


        List<UP_VendorBE> lstUPVendor = new List<UP_VendorBE>();
        lstUPVendor = oUP_VendorBAL.GetVendorByUserCountryBAL(oUP_VendorBE);
        if (lstUPVendor.Count > 0)
        {
            txtVendorNo.Text = oUP_VendorBE.Vendor_No;
            FillControls.FillListBox(ref lstLeft, lstUPVendor, "VendorName", "VendorID");
            for (int i = 0; i < lstUPVendor.Count; i++)
            {
                if (lstUPVendor[i].VendorID > 0)
                    if (Convert.ToInt32(lstLeft.Items[i].Value) == lstUPVendor[i].VendorID && lstUPVendor[i].IsActiveVendor == "N")
                        this.lstLeft.Items[i].Attributes.Add("Style", "background-color:RED;");
            }
        }
        else
        {
            lstLeft.Items.Clear();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        VendorPOCExclusionBAL vendorPOCExclusionBAL = new VendorPOCExclusionBAL();
        VendorPOCExclusionBE vendorPOCExclusionBE = new VendorPOCExclusionBE();

        vendorPOCExclusionBE.Action = "CreateVendorPOCExclusion";
        vendorPOCExclusionBE.CreatedBy = Convert.ToInt32(Session["UserID"].ToString());
        vendorPOCExclusionBE.VendorID = Convert.ToInt32(hdnLocalvendorID.Value);
        vendorPOCExclusionBE.ReasonForExclusion = txtReason.Text;
        int? result = vendorPOCExclusionBAL.SaveVendorPOCExclusionBAL(vendorPOCExclusionBE);
        if (result > 0)
        {
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "Saved", "<script>Saved Successfully..</script>", false);
            EncryptQueryString("VendorPOCExclusions.aspx");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "problem", "<script>Some problem in saving..</script>", false);
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        VendorPOCExclusionBAL vendorPOCExclusionBAL = new VendorPOCExclusionBAL();
        VendorPOCExclusionBE vendorPOCExclusionBE = new VendorPOCExclusionBE();
        vendorPOCExclusionBE.Action = "DeleteVendorPOCExclusion";
        vendorPOCExclusionBE.CreatedBy = Convert.ToInt32(Session["UserID"].ToString());
        vendorPOCExclusionBE.VendorExclusionID = Convert.ToInt32(GetQueryStringValue("VendorExclusionID"));

        int? result = vendorPOCExclusionBAL.DeleteVendorPOCExclusionBAL(vendorPOCExclusionBE);
        if (result > 0)
        {
           // ScriptManager.RegisterStartupScript(this, this.GetType(), "Saved", "<script>Deleted Successfully..</script>", false);
            EncryptQueryString("VendorPOCExclusions.aspx");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "problem", "<script>Some problem in deleting..</script>", false);
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("VendorPOCExclusions.aspx");
    }
}
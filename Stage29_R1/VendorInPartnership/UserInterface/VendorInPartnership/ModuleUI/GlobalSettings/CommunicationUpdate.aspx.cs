﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using BaseControlLibrary;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.AdminFunctions;
using System.Collections;
using System.IO;

public partial class CommunicationUpdate : CommonPage
{
    #region Declarations ...

    string Vendors = WebCommon.getGlobalResourceValue("Vendors");
    string Carriers = WebCommon.getGlobalResourceValue("Carriers");
    string OfficeDepots = WebCommon.getGlobalResourceValue("OfficeDepots");
    string SPDiscrepancyScedular = WebCommon.getGlobalResourceValue("SPDiscrepancyScedular");

    string emailSubjectMandatory = WebCommon.getGlobalResourceValue("EmailSubjectMandatory");
    string emailCommunicationMandatory = WebCommon.getGlobalResourceValue("EmailBodyMandatory");
    string emailSentByMandatory = WebCommon.getGlobalResourceValue("EmailSentByMandatory");
    string sentonBehalfofMandatory = WebCommon.getGlobalResourceValue("SentonBehalfofMandatory");
    string emailTitleMandatory = WebCommon.getGlobalResourceValue("EmailTitleMandatory");
    protected string doYouWantToSubmit = WebCommon.getGlobalResourceValue("DoYouWantToSubmit");
    protected string commLibBackDataLostConfirmation = WebCommon.getGlobalResourceValue("CommLibBackDataLostConfirmation");
    protected string commLibProceedMessage = WebCommon.getGlobalResourceValue("CommLibProceedMessage");
    protected string ValidEmail = WebCommon.getGlobalResourceValue("ValidEmail");
    string errMsg = WebCommon.getGlobalResourceValue("DownLoadFileNotExist");
    string DefaultEmailAddress = "Office Depot";
    ArrayList lst = new ArrayList();
    int check = 0;
    SCT_UserBAL userBAL = null;
    SCT_UserBE userBE = null;
    #endregion

    #region Events ...

    protected void Page_Load(object sender, EventArgs e)
    {
        btnExportVendors.CurrentPage = this;
        btnExportVendors.GridViewControl = gvVendorsExcel;
        btnExportVendors.FileName = "CommunicationUpdateVendors";

        btnExportCarriers.CurrentPage = this;
        btnExportCarriers.GridViewControl = gvCarriersExcel;
        btnExportCarriers.FileName = "CommunicationUpdateCarriers";

        btnExportOfficeDepots.CurrentPage = this;
        btnExportOfficeDepots.GridViewControl = gvOfficeDepotsExcel;
        btnExportOfficeDepots.FileName = "CommunicationUpdateOfficeDepot";

        btnExportOfficeDepot_Sps.CurrentPage = this;
        btnExportOfficeDepot_Sps.GridViewControl = gvOfficeDepotsExcel;
        btnExportOfficeDepot_Sps.FileName = "CommunicationUpdateOfficeDepotSps";

        if (!IsPostBack)
        {
            this.BindCommunicationType();
        }

        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = GvStockPlanarDiscrepancyTrackerReport;
        ucExportToExcel1.FileName = "StockPlannarDiscrepancyTrackerReport";
        ucExportToExcel1.Visible = true;

        msCountry.SetCountryOnPostBack();
        if (rblCommunicationType.SelectedIndex.Equals(0))
            msVendorRole.SetVendorRoleOnPostBack();
        if (rblCommunicationType.SelectedIndex.Equals(1))
            msCarrier.setCarriersOnPostBack();
        if (rblCommunicationType.SelectedIndex.Equals(2))
            msOfficeDepoteRole.SetODRoleOnPostBack();
    }

    protected void rblCommunicationType_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = rblCommunicationType.SelectedIndex;
        this.LoadDesign(index);
        this.SubFolderVisibility(index, true);
        this.ClearGrid(index);
        hdnFldSelectedValues.Value = "";
    }

    #region Vendor Events...

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.ClearGrid(0);
        this.VendorGridBind();
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        if (gvVendors.Visible == true)
            GetCheckedRecords(gvVendors);
        else if (gvCarriers.Visible == true)
            GetCheckedRecords(gvCarriers);
        else if (gvOfficeDepots.Visible == true)
            GetCheckedRecords(gvOfficeDepots);

        txtEmailCommunication.Text = string.Empty;
        txtSentonBehalfofText.Text = DefaultEmailAddress;
        this.ProceedFunctionality(true, false);
    }

    protected void gvVendors_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["SearchedVendor"] != null)
        {
            GridViewRow header = gvVendors.HeaderRow;
            ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
            if (ch.Checked)
                check = 1;
            else
                check = 0;
            GetCheckedRecords(gvVendors);
            gvVendors.PageIndex = e.NewPageIndex;
            gvVendors.DataSource = (List<SCT_UserBE>)ViewState["SearchedVendor"];
            gvVendors.DataBind();
            TicCheckedRecords(gvVendors);


        }
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        this.SubFolderVisibility(rblCommunicationType.SelectedIndex, true);
    }

    #endregion  

    #region Carrier Events ...

    protected void btnSearch_1_Click(object sender, EventArgs e)
    {
        this.ClearGrid(1);
        this.CarrierGridBind();
    }

    protected void btnProceed_1_Click(object sender, EventArgs e)
    {
        if (gvVendors.Visible == true)
            GetCheckedRecords(gvVendors);
        else if (gvCarriers.Visible == true)
            GetCheckedRecords(gvCarriers);
        else if (gvOfficeDepots.Visible == true)
            GetCheckedRecords(gvOfficeDepots);

        txtEmailCommunication.Text = string.Empty;
        txtSentonBehalfofText.Text = DefaultEmailAddress;
        this.ProceedFunctionality(true, false);
    }

    protected void gvCarriers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["SearchedCarrier"] != null)
        {
            GridViewRow header = gvCarriers.HeaderRow;
            ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
            if (ch.Checked)
                check = 1;
            else
                check = 0;
            GetCheckedRecords(gvCarriers);
            gvCarriers.PageIndex = e.NewPageIndex;
            gvCarriers.DataSource = (List<SCT_UserBE>)ViewState["SearchedCarrier"];
            gvCarriers.DataBind();
            TicCheckedRecords(gvCarriers);
        }
    }

    protected void btnBack_2_Click(object sender, EventArgs e)
    {
        this.SubFolderVisibility(rblCommunicationType.SelectedIndex, true);
    }

    #endregion

    #region Office Depot Events ...

    protected void btnSearch_2_Click(object sender, EventArgs e)
    {
        this.ClearGrid(2);
        this.OfficeDepotGridBind();
    }

    protected void btnProceed_2_Click(object sender, EventArgs e)
    {
        if (gvVendors.Visible == true)
            GetCheckedRecords(gvVendors);
        else if (gvCarriers.Visible == true)
            GetCheckedRecords(gvCarriers);
        else if (gvOfficeDepots.Visible == true)
            GetCheckedRecords(gvOfficeDepots);

        txtEmailCommunication.Text = string.Empty;
        txtSentonBehalfofText.Text = DefaultEmailAddress;
        this.ProceedFunctionality(true, false);
    }

    protected void gvOfficeDepots_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["SearchedOD"] != null)
        {
            GridViewRow header = gvOfficeDepots.HeaderRow;
            ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
            if (ch.Checked)
                check = 1;
            else
                check = 0;
            GetCheckedRecords(gvOfficeDepots);
            gvOfficeDepots.PageIndex = e.NewPageIndex;
            gvOfficeDepots.DataSource = (List<SCT_UserBE>)ViewState["SearchedOD"];
            gvOfficeDepots.DataBind();
            TicCheckedRecords(gvOfficeDepots);
        }
    }

    protected void btnBack_3_Click(object sender, EventArgs e)
    {
        this.SubFolderVisibility(rblCommunicationType.SelectedIndex, true);
    }

    #endregion

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtSubjectText.Text) || string.IsNullOrWhiteSpace(txtSubjectText.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + emailSubjectMandatory + "')", true);
            txtSubjectText.Focus();
            return;
        }

        if (string.IsNullOrEmpty(txtEmailCommunication.Text) || string.IsNullOrWhiteSpace(txtEmailCommunication.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + emailCommunicationMandatory + "')", true);
            txtEmailCommunication.Focus();
            return;
        }

        if (string.IsNullOrEmpty(txtSentonBehalfofText.Text) || string.IsNullOrWhiteSpace(txtSentonBehalfofText.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + sentonBehalfofMandatory + "')", true);
            txtSentonBehalfofText.Focus();
            return;
        }

        pnlCommunication.Visible = true;
        pnlManageLetter.Visible = false;
        pnlDisplayLetter.Visible = true;

        var logoText = "<table align=\"center\" width=\"100%\"><tr><td style=\"text-align: center; font-family: Arial\" colspan=\"2\"><img src=\"{logoInnerPath}\" width=\"100%\" /></td></tr></table>";
        var sendComm = new sendCommunicationCommon();
        logoText = logoText.Replace("{logoInnerPath}", sendComm.getAbsolutePath());

        dvDisplayLetter.InnerHtml = string.Format("{0}{1}", logoText, txtEmailCommunication.Text);
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        this.ProceedFunctionality(false, true);
    }

    protected void btnYes_Click(object sender, EventArgs e)
    {
        var sendComm = new sendCommunicationCommon();
        dvDisplayLetter.InnerHtml = dvDisplayLetter.InnerHtml.Replace("<img src=\"cid:logoinner\" width=\"100%\" />", "<img src=\"cid:logoinner\" />");
        var commLibraryBAL = new CommunicationLibraryBAL();
        var commLibraryBE = new CommunicationLibraryBE
        {
            Action = "SaveCommunicationLibrary",
            Subject = txtSubjectText.Text,
            Body = dvDisplayLetter.InnerHtml,
            SentBy = txtSentonBehalfofText.Text,
            Title = txtTitleText.Text,
            TypeofCommunication = this.GetTypeofCommunication(rblCommunicationType.SelectedIndex),
            UserIds = this.GetEmailUserIds(),
            CreatedByID = Convert.ToInt32(Session["UserID"])
        };

        if (commLibraryBAL.AddCommunicationLibBAL(commLibraryBE) > 0)
        {
            var emailFromAddress = Convert.ToString(Session["LoginID"]);
            var emailToSubject = txtSubjectText.Text;
            var emailBody = dvDisplayLetter.InnerHtml;
            Utilities.clsEmail oclsEmail = new Utilities.clsEmail();

            string[] EmailAddress = null;

            var emailToAddress = this.GetEmailAddress();

            if (commLibraryBE.UserIds != null)
            {
                if (emailToAddress != null)
                {
                    EmailAddress = emailToAddress.Split(',');

                    for (int j = 0; j < EmailAddress.Length; j++)
                    {
                        if (EmailAddress[j] != " ")
                        {
                            oclsEmail.sendMail(EmailAddress[j], emailBody, emailToSubject, emailFromAddress, true);
                        }
                    }
                }
            }
        }

        this.Clear();
        this.BindCommunicationType();
    }

    protected void btnNo_Click(object sender, EventArgs e)
    {
        pnlCommunication.Visible = true;
        pnlManageLetter.Visible = true;
        pnlDisplayLetter.Visible = false;
        dvDisplayLetter.InnerHtml = string.Empty;
    }

    #endregion

    #region Methods ...

    private void BindCommunicationType()
    {
        rblCommunicationType.Items.Clear();
        rblCommunicationType.Enabled = true;
        ListItem lstItem = new ListItem(Vendors, "1");
        rblCommunicationType.Items.Add(lstItem);
        lstItem = new ListItem(Carriers, "2");
        rblCommunicationType.Items.Add(lstItem);
        lstItem = new ListItem(OfficeDepots, "3");

        rblCommunicationType.Items.Add(lstItem);
        lstItem = new ListItem(SPDiscrepancyScedular, "4");

        rblCommunicationType.Items.Add(lstItem);
        rblCommunicationType.SelectedIndex = 0;
        this.LoadDesign(rblCommunicationType.SelectedIndex);
        this.SubFolderVisibility(rblCommunicationType.SelectedIndex, true);
    }

    private void LoadDesign(int commSelectedIndex)
    {
        pnlCommonCommunication.Visible = true;
        switch (commSelectedIndex)
        {
            case 0:
                pnlVendorsCommunication.Visible = true;
                pnlCarriersCommunication.Visible = false;
                pnlOfficeDepotCommunication.Visible = false;
                break;
            case 1:
                pnlVendorsCommunication.Visible = false;
                pnlCarriersCommunication.Visible = true;
                pnlOfficeDepotCommunication.Visible = false;
                break;
            case 2:
                pnlVendorsCommunication.Visible = false;
                pnlCarriersCommunication.Visible = false;
                pnlOfficeDepotCommunication.Visible = true;
                break;
            case 3:
                pnlVendorsCommunication.Visible = false;
                pnlCarriersCommunication.Visible = false;
                pnlOfficeDepotCommunication.Visible = true;
                break;
            default:
                pnlVendorsCommunication.Visible = true;
                pnlCarriersCommunication.Visible = false;
                pnlOfficeDepotCommunication.Visible = false;
                break;
        }
    }

    private void ClearGrid(int displayIndex)
    {
        switch (displayIndex)
        {
            case 0:
                gvCarriers.DataSource = null;
                gvCarriers.DataBind();
                btnProceed_1.Visible = false;
                btnExportCarriers.Visible = false;

                gvOfficeDepots.DataSource = null;
                gvOfficeDepots.DataBind();
                btnProceed_2.Visible = false;
                btnExportOfficeDepots.Visible = false;
                break;
            case 1:
                gvVendors.DataSource = null;
                gvVendors.DataBind();
                btnProceed.Visible = false;
                btnExportVendors.Visible = false;

                gvOfficeDepots.DataSource = null;
                gvOfficeDepots.DataBind();
                btnProceed_2.Visible = false;
                btnExportOfficeDepots.Visible = false;
                break;
            case 2:
                gvVendors.DataSource = null;
                gvVendors.DataBind();
                btnProceed.Visible = false;
                btnExportVendors.Visible = false;

                gvCarriers.DataSource = null;
                gvCarriers.DataBind();
                btnProceed_1.Visible = false;
                btnExportCarriers.Visible = false;
                break;
        }
    }

    private void ExcelButtonVisibility(int commSelectedIndex, bool isVisible)
    {
        if (isVisible)
        {
            switch (commSelectedIndex)
            {
                case 0:
                    btnExportVendors.Visible = true;
                    btnExportCarriers.Visible = false;
                    btnExportOfficeDepots.Visible = false;
                    btnExportOfficeDepot_Sps.Visible = false;
                    break;
                case 1:
                    btnExportVendors.Visible = false;
                    btnExportCarriers.Visible = true;
                    btnExportOfficeDepots.Visible = false;
                    btnExportOfficeDepot_Sps.Visible = false;
                    break;
                case 2:
                    btnExportVendors.Visible = false;
                    btnExportCarriers.Visible = false;
                    btnExportOfficeDepots.Visible = true;
                    btnExportOfficeDepot_Sps.Visible = false;
                    break;
                case 3:
                    btnExportVendors.Visible = false;
                    btnExportCarriers.Visible = false;
                    btnExportOfficeDepots.Visible = false;
                    btnExportOfficeDepot_Sps.Visible = true;
                    break;
            }
        }
        else
        {
            btnExportVendors.Visible = false;
            btnExportCarriers.Visible = false;
            btnExportOfficeDepots.Visible = false;
            btnExportOfficeDepot_Sps.Visible = false;
        }
    }

    private string GetSelectedVendorIds(ucMultiSelectVendor multiSelectVendor)
    {
        ucListBox lstSelectedVendor = (ucListBox)multiSelectVendor.FindControl("lstSelectedVendor");
        var vendorIds = string.Empty;
        for (int index = 0; index < lstSelectedVendor.Items.Count; index++)
        {
            if (index.Equals(0))
                vendorIds = lstSelectedVendor.Items[index].Value;
            else
                vendorIds = string.Format("{0},{1}", vendorIds, lstSelectedVendor.Items[index].Value);
        }
        return vendorIds;
    }

    private void VendorGridBind()
    {
        userBAL = new SCT_UserBAL();
        userBE = new SCT_UserBE();
        userBE.Action = "GetCommunicationUpdateVendors";
        userBE.CountryIds = msCountry.SelectedCountryIDs;
        userBE.VendorIDs = this.GetSelectedVendorIds(msVendor);
        userBE.Roles = msVendorRole.SelectedVendorRoleIDs;
        userBE.EuropeanLocal = Convert.ToInt32(ddlEuropeanLocal.SelectedValue);
        var lstCommVendor = userBAL.GetCommunicationUpdateVendorsBAL(userBE);
        gvVendors.DataSource = null;
        gvVendorsExcel.DataSource = null;
        btnExportVendors.Visible = false;
        btnProceed.Visible = false;
        if (lstCommVendor.Count > 0 && lstCommVendor != null)
        {
            gvVendors.DataSource = lstCommVendor;
            gvVendorsExcel.DataSource = lstCommVendor;
            ViewState["SearchedVendor"] = lstCommVendor;
            btnExportVendors.Visible = true;
            btnProceed.Visible = true;
        }
        gvVendors.DataBind();
        gvVendorsExcel.DataBind();
        this.SubFolderVisibility(rblCommunicationType.SelectedIndex, false);
    }

    private void CarrierGridBind()
    {
        userBAL = new SCT_UserBAL();
        userBE = new SCT_UserBE();
        userBE.Action = "GetCommunicationUpdateCarriers";
        userBE.CountryIds = msCountry.SelectedCountryIDs;
        userBE.CarrierIDs = msCarrier.SelectedCarrierIDs;
        var lstCommCarrier = userBAL.GetCommunicationUpdateCarriersBAL(userBE);
        gvCarriers.DataSource = null;
        gvCarriersExcel.DataSource = null;
        btnExportCarriers.Visible = false;
        btnProceed_1.Visible = false;
        if (lstCommCarrier.Count > 0 && lstCommCarrier != null)
        {
            gvCarriers.DataSource = lstCommCarrier;
            gvCarriersExcel.DataSource = lstCommCarrier;
            ViewState["SearchedCarrier"] = lstCommCarrier;
            btnExportCarriers.Visible = true;
            btnProceed_1.Visible = true;
        }
        gvCarriers.DataBind();
        gvCarriersExcel.DataBind();
        this.SubFolderVisibility(rblCommunicationType.SelectedIndex, false);
    }

    private void OfficeDepotGridBind()
    {
        userBAL = new SCT_UserBAL();
        userBE = new SCT_UserBE();
        userBE.Action = "GetCommunicationUpdateOfficeDepot";
        userBE.CountryIds = msCountry.SelectedCountryIDs;
        userBE.Roles = msOfficeDepoteRole.SelectedOfficeDepotRoleIDs;
        var lstCommOfficeDepot = userBAL.GetCommunicationUpdateOfficeDepotBAL(userBE);
        gvOfficeDepots.DataSource = null;
        gvOfficeDepotsExcel.DataSource = null;
        btnExportOfficeDepots.Visible = false;
        btnProceed_2.Visible = false;
        if (lstCommOfficeDepot.Count > 0 && lstCommOfficeDepot != null)
        {
            gvOfficeDepots.DataSource = lstCommOfficeDepot;
            gvOfficeDepotsExcel.DataSource = lstCommOfficeDepot;
            ViewState["SearchedOD"] = lstCommOfficeDepot;
            btnExportOfficeDepots.Visible = true;
            btnProceed_2.Visible = true;
        }
        gvOfficeDepots.DataBind();
        gvOfficeDepotsExcel.DataBind();
        this.SubFolderVisibility(rblCommunicationType.SelectedIndex, false);
    }

    private void ProceedFunctionality(bool proceed, bool back)
    {
        if (proceed)
        {
            pnlCommonCommunication.Visible = false;
            pnlVendorsCommunication.Visible = false;
            pnlCarriersCommunication.Visible = false;
            pnlOfficeDepotCommunication.Visible = false;

            pnlCommunication.Visible = true;
            pnlManageLetter.Visible = true;
            pnlDisplayLetter.Visible = false;

            rblCommunicationType.Enabled = false;
            this.ExcelButtonVisibility(rblCommunicationType.SelectedIndex, false);
        }
        else if (back)
        {
            pnlCommunication.Visible = false;
            txtSubjectText.Text = string.Empty;
            txtEmailCommunication.Text = string.Empty;
            txtSentonBehalfofText.Text = string.Empty;
            txtTitleText.Text = string.Empty;
            this.ExcelButtonVisibility(rblCommunicationType.SelectedIndex, true);
            this.LoadDesign(rblCommunicationType.SelectedIndex);
            this.SubFolderVisibility(rblCommunicationType.SelectedIndex, false);
        }
    }

    private string GetTypeofCommunication(int commSelectedIndex)
    {
        var typeofComm = string.Empty;
        switch (commSelectedIndex)
        {
            case 0:
                typeofComm = "V";
                break;
            case 1:
                typeofComm = "C";
                break;
            case 2:
                typeofComm = "OD";
                break;
            case 3:
                typeofComm = "ODSP";
                break;
        }

        return typeofComm;
    }

    private string GetEmailAddress()
    {
        var stringBuilder = new System.Text.StringBuilder();
        var lstRecords = new List<SCT_UserBE>();
        switch (rblCommunicationType.SelectedIndex)
        {
            case 0:
                {
                    lstRecords = (List<SCT_UserBE>)ViewState["SearchedVendor"];
                    break;
                }
            case 1:
                {
                    lstRecords = (List<SCT_UserBE>)ViewState["SearchedCarrier"];
                    break;
                }
            case 2:
                {
                    lstRecords = (List<SCT_UserBE>)ViewState["SearchedOD"];
                    break;
                }
        }

        var rowNum = hdnFldSelectedValues.Value.ToString().TrimEnd(',').Split(',').Select(int.Parse).ToList();
        var result = (from myType in lstRecords
                      where rowNum.Contains(myType.RowNum)
                      select new
                      {
                          email = myType.EmailId
                      }).Distinct().ToList();

        foreach (var val in result)
            stringBuilder.Append(string.Format(",{0}", val.email));

        return stringBuilder.ToString().TrimStart(',').TrimEnd(',');
    }

    private string GetEmailUserIds()
    {
        var stringBuilder = new System.Text.StringBuilder();
        var lstRecords = new List<SCT_UserBE>();
        switch (rblCommunicationType.SelectedIndex)
        {
            case 0:
                {
                    lstRecords = (List<SCT_UserBE>)ViewState["SearchedVendor"];
                    break;
                }
            case 1:
                {
                    lstRecords = (List<SCT_UserBE>)ViewState["SearchedCarrier"];
                    break;
                }
            case 2:
                {
                    lstRecords = (List<SCT_UserBE>)ViewState["SearchedOD"];
                    break;
                }
        }

        List<int> rowNum = hdnFldSelectedValues.Value.ToString().TrimEnd(',').Split(',').Select(int.Parse).ToList();
        var result = (from myType in lstRecords
                      where rowNum.Contains(myType.RowNum)
                      select new
                      {
                          UserID = myType.UserID
                      }).Distinct().ToList();

        foreach (var val in result)
        {
            stringBuilder.Append(string.Format(",{0}", val.UserID));
        }

        return stringBuilder.ToString().TrimStart(',').TrimEnd(',');
    }

    private void Clear()
    {
        txtEmailCommunication.Text = String.Empty;
        txtSentonBehalfofText.Text = String.Empty;
        txtSubjectText.Text = String.Empty;
        txtTitleText.Text = String.Empty;
        dvDisplayLetter.InnerHtml = String.Empty;
        pnlDisplayLetter.Visible = false;
    }

    private void SetCountry()
    {
        var lstLeft = ((ucListBox)msCountry.FindControl("lstLeft"));
        var lstRight = ((ucListBox)msCountry.FindControl("lstRight"));
        var hiddenSelectedIDs = ((HiddenField)msCountry.FindControl("hiddenSelectedIDs"));

        string[] CountryIds = hiddenSelectedIDs.Value.TrimEnd(',').Split(',');
        for (int index = 0; index < CountryIds.Length; index++)
        {
            var listItem = lstLeft.Items.FindByValue(CountryIds[index]);
            if (listItem != null)
            {
                lstRight.Items.Add(listItem);
                lstLeft.Items.Remove(listItem);
            }
        }
    }

    private void SetVendorRole()
    {
        var lstVendor = ((ucListBox)msVendorRole.FindControl("lstVendor"));
        var lstSelectedVendor = ((ucListBox)msVendorRole.FindControl("lstSelectedVendor"));
        string[] VendorRoleIds = msVendorRole.SelectedVendorRoleIDs.Split(',');

        if (lstSelectedVendor.Items.Count.Equals(0))
        {
            for (int index = 0; index < VendorRoleIds.Length; index++)
            {
                var listItem = lstVendor.Items.FindByValue(VendorRoleIds[index]);
                lstSelectedVendor.Items.Add(listItem);
                lstVendor.Items.Remove(listItem);
            }
        }
    }

    private void SubFolderVisibility(int commSelectedIndex, bool search)
    {
        switch (commSelectedIndex)
        {
            case 0:
                if (search)
                {
                    rblCommunicationType.Enabled = true;
                    pnlCommonCommunication.Visible = true;
                    pnlSubVendComm1.Visible = true;
                    pnlSubVendComm2.Visible = false;
                    pnlSubCarrComm1.Visible = false;
                    pnlSubCarrComm2.Visible = false;
                    pnlSubODComm1.Visible = false;
                    pnlSubODComm2.Visible = false;
                    btnExportVendors.Visible = false;
                    pnlSubODComm_StockPlanner.Visible = false;
                }
                else
                {
                    rblCommunicationType.Enabled = false;
                    pnlCommonCommunication.Visible = false;
                    pnlSubVendComm1.Visible = false;
                    pnlSubVendComm2.Visible = true;
                    pnlSubCarrComm1.Visible = false;
                    pnlSubCarrComm2.Visible = false;
                    pnlSubODComm1.Visible = false;
                    pnlSubODComm2.Visible = false;
                    pnlSubODComm_StockPlanner.Visible = false;
                }
                break;
            case 1:
                if (search)
                {
                    rblCommunicationType.Enabled = true;
                    pnlCommonCommunication.Visible = true;
                    pnlSubVendComm1.Visible = false;
                    pnlSubVendComm2.Visible = false;
                    pnlSubCarrComm1.Visible = true;
                    pnlSubCarrComm2.Visible = false;
                    pnlSubODComm1.Visible = false;
                    pnlSubODComm2.Visible = false;
                    btnExportCarriers.Visible = false;
                    pnlSubODComm_StockPlanner.Visible = false;
                }
                else
                {
                    rblCommunicationType.Enabled = false;
                    pnlCommonCommunication.Visible = false;
                    pnlSubVendComm1.Visible = false;
                    pnlSubVendComm2.Visible = false;
                    pnlSubCarrComm1.Visible = false;
                    pnlSubCarrComm2.Visible = true;
                    pnlSubODComm1.Visible = false;
                    pnlSubODComm2.Visible = false;
                    pnlSubODComm_StockPlanner.Visible = false;
                }
                break;
            case 2:
                if (search)
                {
                    rblCommunicationType.Enabled = true;
                    pnlCommonCommunication.Visible = true;
                    pnlSubVendComm1.Visible = false;
                    pnlSubVendComm2.Visible = false;
                    pnlSubCarrComm1.Visible = false;
                    pnlSubCarrComm2.Visible = false;
                    pnlSubODComm1.Visible = true;
                    pnlSubODComm2.Visible = false;
                    btnExportOfficeDepots.Visible = false;
                    pnlSubODComm_StockPlanner.Visible = false;
                }
                else
                {
                    rblCommunicationType.Enabled = false;
                    pnlCommonCommunication.Visible = false;
                    pnlSubVendComm1.Visible = false;
                    pnlSubVendComm2.Visible = false;
                    pnlSubCarrComm1.Visible = false;
                    pnlSubCarrComm2.Visible = false;
                    pnlSubODComm1.Visible = false;
                    pnlSubODComm_StockPlanner.Visible = false;
                    pnlSubODComm2.Visible = true;
                }
                break;
            case 3:
                if (search)
                {
                    rblCommunicationType.Enabled = true;
                    pnlCommonCommunication.Visible = false;
                    pnlSubVendComm1.Visible = false;
                    pnlSubVendComm2.Visible = false;
                    pnlSubCarrComm1.Visible = false;
                    pnlSubCarrComm2.Visible = false;
                    pnlSubODComm1.Visible = false;
                    pnlSubODComm2.Visible = false;
                    btnExportOfficeDepots.Visible = false;
                    pnlSubODComm_StockPlanner.Visible = true;                  

                    CommunicationLibraryBAL communicationLibraryBAL = new CommunicationLibraryBAL();
                    DiscrepancyNotification discrepancyNotification = new DiscrepancyNotification
                    {
                        Action = "GetDiscrepancyTrackerCommunication"
                    };

                    GvStockPlanarDiscrepancyTrackerReport.DataSource = communicationLibraryBAL.GetStockPlanarDiscrepancyTrackerBAL(discrepancyNotification); ;
                    GvStockPlanarDiscrepancyTrackerReport.DataBind();
                }
                break;
        }
    }

    private void GetCheckedRecords(GridView grd)
    {
        GridViewRow header = grd.HeaderRow;
        ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
        if (ch.Checked && string.IsNullOrEmpty(hdnFldSelectedValues.Value.ToString()))
        {
            var lstRecords = new List<SCT_UserBE>();
            if (ViewState["SearchedVendor"] != null)
            {
                lstRecords = (List<SCT_UserBE>)ViewState["SearchedVendor"];
            }
            else if (ViewState["SearchedCarrier"] != null)
            {
                lstRecords = (List<SCT_UserBE>)ViewState["SearchedCarrier"];
            }
            else
            {
                lstRecords = (List<SCT_UserBE>)ViewState["SearchedOD"];
            }

            foreach (var item in lstRecords)
            {
                hdnFldSelectedValues.Value = hdnFldSelectedValues.Value + item.RowNum + ",";
            }
        }
        else
        {
            foreach (GridViewRow gvrow in grd.Rows)
            {
                CheckBox chk = (CheckBox)gvrow.FindControl("chkSelect");
                if (chk != null & chk.Checked)
                {
                    HiddenField hdnRowNum = (HiddenField)gvrow.FindControl("hdnRowNum");
                    if (!hdnFldSelectedValues.Value.ToString().Contains(hdnRowNum.Value.ToString()))
                        hdnFldSelectedValues.Value = hdnFldSelectedValues.Value + hdnRowNum.Value + ",";
                }
            }
        }
    }

    private void TicCheckedRecords(GridView grd)
    {
        string str = string.Empty;
        if (!string.IsNullOrEmpty(hdnFldSelectedValues.Value.ToString()))
        {
            var ids = hdnFldSelectedValues.Value.ToString().TrimEnd(',').Split(',');
            foreach (GridViewRow gvrow in grd.Rows)
            {
                CheckBox chk = (CheckBox)gvrow.FindControl("chkSelect");
                HiddenField hdnRowNum = (HiddenField)gvrow.FindControl("hdnRowNum");
                if (chk != null)
                {
                    if (ids.Contains(hdnRowNum.Value))
                    {
                        chk.Checked = true;
                    }
                }
            }
        }
    }

    #endregion

    protected void chkSelectAllText_CheckedChanged(object sender, EventArgs e)
    {
        GridViewRow header = gvVendors.HeaderRow;
        ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
        if (ch.Checked)
        {
            check = 1;
        }
        else
        {
            check = 0;
            hdnFldSelectedValues.Value = "";
        }
        VendorGridBind();
    }
    protected void gvVendors_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (check == 1)
            {
                GridViewRow header = gvVendors.HeaderRow;
                ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
                ch.Checked = true;
                CheckBox chk = (CheckBox)e.Row.FindControl("chkSelect");
                chk.Checked = true;
            }
        }
    }
    protected void UcCheckbox1_CheckedChanged(object sender, EventArgs e)
    {
        GridViewRow header = gvCarriers.HeaderRow;
        ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
        if (ch.Checked)
        {
            check = 1;
        }
        else
        {
            check = 0;
            hdnFldSelectedValues.Value = "";
        }
        CarrierGridBind();
    }
    protected void gvCarriers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (check == 1)
            {
                GridViewRow header = gvCarriers.HeaderRow;
                ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
                ch.Checked = true;
                CheckBox chk = (CheckBox)e.Row.FindControl("chkSelect");
                chk.Checked = true;
            }
        }
    }
    protected void chkSelectAllText_CheckedChanged1(object sender, EventArgs e)
    {
        GridViewRow header = gvOfficeDepots.HeaderRow;
        ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
        if (!ch.Checked)
        {
            check = 0;
            hdnFldSelectedValues.Value = "";
        }
        else
        {
            check = 1;
        }

        OfficeDepotGridBind();
    }
    protected void gvOfficeDepots_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (check == 1)
            {
                GridViewRow header = gvOfficeDepots.HeaderRow;
                ucCheckbox ch = (ucCheckbox)header.FindControl("chkSelectAllText");
                ch.Checked = true;
                CheckBox chk = (CheckBox)e.Row.FindControl("chkSelect");
                chk.Checked = true;
            }
            else
            {
            }
        }
    }
    protected void BtnCancel_Click(object sender, EventArgs e)
    {
        this.SubFolderVisibility(rblCommunicationType.SelectedIndex, true);
    }
    protected void GvStockPlanarDiscrepancyTrackerReport_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Dwn")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GvStockPlanarDiscrepancyTrackerReport.Rows[index];
            HiddenField hdnWeekFile = (HiddenField)row.FindControl("hdnWeekFile");
            string fName = hdnWeekFile.Value;
            
            if (File.Exists(fName))
            {
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + fName);
                Response.TransmitFile(fName);
                Response.End();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
            }
        }
    }
}
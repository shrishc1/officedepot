﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;

public partial class Currency : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        if (!IsPostBack)
        {
            ucExportToExcel1.Visible = false;
            BindDebitReasonSetUpGrid();
        }
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvCurrency;
        ucExportToExcel1.FileName = "Currency";
    }

    private void BindDebitReasonSetUpGrid()
    {
        MAS_CurrencyBAL oMAS_CurrencyBAL = new MAS_CurrencyBAL();
        
        CurrencyBE oCurrencyBE = new CurrencyBE();
        oCurrencyBE.Action = "ShowAll";


        List<CurrencyBE> lstCurrencyBE = oMAS_CurrencyBAL.GetCurrenyBAL(oCurrencyBE);

        if (lstCurrencyBE != null && lstCurrencyBE.Count > 0)
        {
            ucExportToExcel1.Visible = true;
            gvCurrency.DataSource = lstCurrencyBE;
            gvCurrency.DataBind();
            ViewState["lstCurrencyBE"] = lstCurrencyBE;
        }


    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<CurrencyBE>.SortList((List<CurrencyBE>)ViewState["lstCurrencyBE"], e.SortExpression, e.SortDirection).ToArray();
    }
}
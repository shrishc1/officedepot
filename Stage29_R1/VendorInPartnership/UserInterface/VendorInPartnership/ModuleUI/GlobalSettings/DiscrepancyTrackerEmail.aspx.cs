﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using WebUtilities;

public partial class ModuleUI_Discrepancy_DiscrepancyTrackerEmail : CommonPage
{
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DiscrepancyEmailTracker discrepancyEmailTracker = new DiscrepancyEmailTracker();
            CommunicationLibraryBAL discrepancyEmailTrackerBAL = new CommunicationLibraryBAL();
            discrepancyEmailTracker.Action = "ALL";
            discrepancyEmailTracker.UserID = Convert.ToInt32(Session["UserID"].ToString());
            List<DiscrepancyEmailTracker> result = discrepancyEmailTrackerBAL.GetALLDiscrepancyTrackerEmaiBAL(discrepancyEmailTracker);

            gvDiscrepancyTrackerEmail.DataSource = result;
            gvDiscrepancyTrackerEmail.DataBind();
            gvDiscrepancyTrackerEmailExcel.DataSource = result;
            gvDiscrepancyTrackerEmailExcel.DataBind();
        }
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvDiscrepancyTrackerEmailExcel;
        ucExportToExcel1.FileName = "DiscrepancyTrackerEmailEscalation";
    } 

    protected void gvDiscrepancyTrackerEmail_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "dede")
        {
            int ID = Convert.ToInt32(e.CommandArgument);
            DiscrepancyEmailTracker discrepancyEmailTracker = new DiscrepancyEmailTracker();
            CommunicationLibraryBAL discrepancyEmailTrackerBAL = new CommunicationLibraryBAL();
            discrepancyEmailTracker.Action = "Delete";
            discrepancyEmailTracker.ID = ID;
            discrepancyEmailTracker.UserID = Convert.ToInt32(Session["UserID"].ToString());
            string result = discrepancyEmailTrackerBAL.AddDiscrepancyTrackerEmailBAL(discrepancyEmailTracker);
            if (result.Contains("Successfully"))
            {
                EncryptQueryString("DiscrepancyTrackerEmail.aspx");
            }
        }
    }
}
﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Linq.Expressions;

public partial class SiteSetupOverview : CommonPage {
    private int i = 0;
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            BindGrid();                        
        }
        btnExportToExcel1.GridViewControl = grdSiteOverview;
        btnExportToExcel1.FileName = "SiteOverview";
    }

    #region Methods
    private void BindGrid()
    {
        MAS_SiteBE oMAS_SITEBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SITEBAL = new MAS_SiteBAL();

        List<MAS_SiteBE> lstSites = new List<MAS_SiteBE>();

        oMAS_SITEBE.Action = "ShowMaster";
        oMAS_SITEBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_SITEBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        lstSites = oMAS_SITEBAL.GetSiteMisSettingBAL(oMAS_SITEBE);
        oMAS_SITEBAL = null;
        grdSiteOverview.DataSource = lstSites;
        grdSiteOverview.DataBind();
        ViewState["lstSites"] = lstSites;
    }

    #endregion
    protected void grdSiteOverview_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //string SiteNumber = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SiteNumber"));
            //string SiteNumberList = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SiteNumberList"));
            //if (SiteNumber == "")
            //    e.Row.Cells[1].Text = SiteNumberList;
            //else
            //    e.Row.Cells[1].Text = SiteNumber;
        }

    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MAS_SiteBE>.SortList((List<MAS_SiteBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
    
        
}
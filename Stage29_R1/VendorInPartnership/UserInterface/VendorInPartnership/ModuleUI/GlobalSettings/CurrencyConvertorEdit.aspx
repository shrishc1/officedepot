﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="CurrencyConvertorEdit.aspx.cs" Inherits="CurrencyConvertorEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lbl_currency" runat="server" Text="Currency Converter"></cc1:ucLabel>
    </h2>
    <div class="formbox">
        <%--<table width="30%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
            <tr>
                <td>&nbsp;</td>
                <td>--%>
        <asp:RegularExpressionValidator ID="revExchangeRateError" ControlToValidate="txtExchangeRates"
            SetFocusOnError="true" runat="server" ValidationGroup="a" Display="None" ValidationExpression="^(-?\d{0,13}\.\d{0,5}|\d{0,13})$"></asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="rfvCurrencyRequired" runat="server" ControlToValidate="ddlCurrency"
            SetFocusOnError="true" Display="None" ValidationGroup="a" InitialValue="0"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="rfvCurrencyToRequired" runat="server" ControlToValidate="ddlConvertCurrency"
            SetFocusOnError="true" Display="None" ValidationGroup="a" InitialValue="0"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="rfvExchangerate" runat="server" ControlToValidate="txtExchangeRates"
            SetFocusOnError="true" Display="None" ValidationGroup="a"></asp:RequiredFieldValidator>
        <asp:CompareValidator ID="cmpvCurrencyConvertToError" runat="server" ControlToCompare="ddlCurrency"
            SetFocusOnError="true" Display="None" ControlToValidate="ddlConvertCurrency"
            ValidationGroup="a" Operator="NotEqual"></asp:CompareValidator>
        <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
            Style="color: Red" ValidationGroup="a" />
        <table width="65%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
            <tr>
                <td class="style1">
                </td>
                <td style="font-weight: bold;" width="28%" class="style2">
                    <cc1:ucLabel ID="lblCurrency" runat="server" Text="Currency" isRequired="true"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;" width="5%" align="center" class="style2">
                    :
                </td>
                <td style="font-weight: bold;" width="57%" class="style2">
                    <cc1:ucDropdownList ID="ddlCurrency" runat="server" CausesValidation="True" ValidationGroup="a"
                        OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged">
                    </cc1:ucDropdownList>
                </td>
                <td class="style1">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="font-weight: bold">
                    <cc1:ucLabel ID="lblConvertCurrency" runat="server" Text="Convert To" isRequired="true"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;" align="center">
                    :
                </td>
                <td>
                    <cc1:ucDropdownList ID="ddlConvertCurrency" runat="server" CausesValidation="True"
                        ValidationGroup="a">
                    </cc1:ucDropdownList>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="font-weight: bold">
                    <cc1:ucLabel ID="lblExchangeRates" runat="server" Text="Exchange Rate" isRequired="true"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;" align="center">
                    :
                </td>
                <td>
                    <cc1:ucNumericTextbox ID="txtExchangeRates" runat="server" Width="65px" MaxLength="7"
                        onkeyup="AllowDecimalOnly(this);" ValidationGroup="a"></cc1:ucNumericTextbox>
                </td>
            </tr>
        </table>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
            ValidationGroup="a" />
        <cc1:ucButton ID="btnback" runat="server" Text="Back" CssClass="button" OnClick="btnback_Click"
            CausesValidation="false" />
    </div>
</asp:Content>

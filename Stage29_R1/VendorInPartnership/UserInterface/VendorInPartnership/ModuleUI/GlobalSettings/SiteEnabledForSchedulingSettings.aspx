﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SiteEnabledForSchedulingSettings.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="ModuleUI_GlobalSettings_SiteEnabledForSchedulingSettings" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblSiteEnabledForScheduling" Text="Site Enabled For Scheduling"
            runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row">

        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center">
                <cc1:ucGridView ID="gvSiteEnableForScheduling" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Country" SortExpression="Country.CountryName">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltCountryName" runat="server" Text='<%# Eval("CountryName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Site" SortExpression="SiteName">
                            <HeaderStyle HorizontalAlign="Left" Width="16%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSite" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLiteral>
                                <asp:HiddenField ID="hdnSiteID" runat="server" Value='<%# Eval("SiteID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Enabled">
                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucCheckbox ID="chkSiteSchedulingEnabled" runat="server"
                                    Checked='<%# Eval("IsSiteSchedulingEnabled ")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Enable Hazardoues Item Prompt">
                            <HeaderStyle HorizontalAlign="Left" Width="60%" />                         
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucCheckbox ID="checkBoxEnableHazardouesItemPrompt" runat="server"
                                    Checked='<%# Eval("IsEnableHazardouesItemPrompt")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
                <div class="button-row">
                    <cc1:ucButton ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />&nbsp;&nbsp;&nbsp;            
                </div>

            </td>
        </tr>
    </table>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="VendorVatCodeOverview.aspx.cs" Inherits="VendorVatCodeOverview" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblVATcodemaintananceVendor" runat="server" Text="Vendor Overview"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSelection" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="width: 90%">
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDisplay" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" align="center">
                            :
                        </td>
                        <td>
                            <table width="60%" cellspacing="3" cellpadding="0" align="left">
                                <tr>
                                    <td style="width: 20%;" class="nobold">
                                        <cc1:ucRadioButton ID="rdoAllOnly" runat="server" GroupName="Vat" Checked="true" />
                                    </td>
                                    <td style="font-weight: bold; width: 4em; text-align: center">
                                        &nbsp;&nbsp;
                                    </td>
                                    <td style="width: 30%;" class="nobold">
                                        <cc1:ucRadioButton ID="rdoMaintainedValue" runat="server" GroupName="Vat" />
                                    </td>
                                    <td style="font-weight: bold; width: 4em; text-align: center">
                                        &nbsp;&nbsp;
                                    </td>
                                    <td style="width: 40%;" class="nobold">
                                        <cc1:ucRadioButton ID="rdoNotMaintainedValue" runat="server" GroupName="Vat" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGrid" runat="server" Visible="false">
                <div class="button-row">
                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                        Width="109px" Height="20px" />
                </div>
                <div style="width: 100%; overflow: auto" id="divPrint">
                    <table cellspacing="1" cellpadding="0" border="0" align="center" width="100%">
                        <tr>
                            <td>
                                <cc1:ucGridView ID="gvVendorVatCode" Width="100%" runat="server" CssClass="grid"
                                    OnSorting="SortGrid" AutoGenerateColumns="false" AllowSorting="true" OnPageIndexChanging="gvVendorVatCode_PageIndexChanging">
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">
                                            <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                        </div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Country" SortExpression="Vendor.Vendor_Country">
                                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVendorCountryValue" Text='<%# Eval("Vendor.Vendor_Country") %>'
                                                    runat="server"></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VendorNo" SortExpression="Vendor.Vendor_No">
                                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVendorVendorNoValue" Text='<%# Eval("Vendor.Vendor_No") %>' runat="server"></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.Vendor_Name">
                                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hpVendorName" runat="server" Text='<%# Eval("Vendor.Vendor_Name") %>'
                                                    NavigateUrl='<%# EncryptQuery("VendorVatCodeEdit.aspx?VendorID="+ Eval("Vendor.VendorID")) %>'></asp:HyperLink>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VatCode" SortExpression="Vendor.VatCode">
                                            <HeaderStyle Width="40%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVatCodeValue" Text='<%# Eval("Vendor.VatCode") %>' runat="server"></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                </cc1:ucGridView>
                                <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                                </cc1:PagerV2_8>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                                    OnClick="btnBackToSearch_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="StockPlanarDiscrepancyTrackerReport.aspx.cs" Inherits="ModuleUI_GlobalSettings_StockPlanarDiscrepancyTrackerReport" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/MultiSelectTypeofCommunication.ascx"
    TagName="ucMultiSelectTypeofCommunication" TagPrefix="cc3" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h2>
        <cc1:ucLabel ID="lblStockPlanarDiscrepancyTrackerReport" runat="server" Text=""></cc1:ucLabel>
    </h2>

    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>

    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="GvStockPlanarDiscrepancyTrackerReport" Width="100%" runat="server" CssClass="grid"
                    OnSorting="SortGrid" AllowSorting="true" OnRowCommand="GvStockPlanarDiscrepancyTrackerReport_RowCommand">
                    <Columns>
                        <asp:TemplateField HeaderText="Week" SortExpression="Week">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpWeek" runat="server" Text='<%# Eval("Week") %>'
                                    NavigateUrl='<%# EncryptQuery("StockPlanarDiscrepancyTrackerReportEdit.aspx?DiscrepancyTrackerId="+ Eval("DiscrepancyTrackerId") + "&MailSent=" + Eval("MailSent")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Year" ItemStyle-Wrap="false" DataField="Year"
                            SortExpression="Year">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="EmailSubject" ItemStyle-Wrap="false" DataField="EmailSubject"
                            SortExpression="EmailSubject">
                            <HeaderStyle Width="40%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="MailSent" SortExpression="MailSent">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblWeek" runat="server" Text='<%#Convert.ToBoolean( Eval("MailSent")) ==true ?"Yes" :"No" %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Date Created" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                            DataField="CreatedDate" SortExpression="CreatedDate">
                            <HeaderStyle HorizontalAlign="Center" Width="45%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:ButtonField ButtonType="Link" Text="Download" CommandName="Dwn" HeaderText="Files" Visible="false"/>
                        <asp:TemplateField HeaderText="WeekFile" Visible="false">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnWeekFile" runat="server" Value='<%# Eval("ExcelPath") %>'></asp:HiddenField>                              
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
    </table>


</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.GlobalSettings;
using BusinessLogicLayer.ModuleBAL.GlobalSettings;
using Utilities;
using WebUtilities;
using System.IO;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Data;
using BusinessEntities.ModuleBE.Security;

public partial class ModuleUI_GlobalSettings_PointsofContactDetailReport : CommonPage
{
    public SortDirection GridViewSortDirection
    {

        get
        {
            if (ViewState["GridViewSortDirection"] == null)
                ViewState["GridViewSortDirection"] = SortDirection.Descending;
            return (SortDirection)ViewState["GridViewSortDirection"];
        }
        set { ViewState["GridViewSortDirection"] = value; }
    }

    public string SortDir
    {
        get
        {
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                return "DESC";
            }
            else
            {
                return "ASC";
            }
        }
    }

    public string GridViewSortExp
    {
        get
        {
            if (ViewState["GridViewSortExp"] == null)
                ViewState["GridViewSortExp"] = "Lines";
            return (string)ViewState["GridViewSortExp"];
        }
        set { ViewState["GridViewSortExp"] = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ucCountry.CurrentPage = this;
        //pager1.PageSize = 50;
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        //pager1.CurrentIndex = currnetPageIndx;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
        }
    }

    public override void CountryPost_Load()
    {

    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        GridViewSortExp = e.SortExpression;
        if (SortDir == "ASC")
        {
            GridViewSortDirection = SortDirection.Ascending;
        }
        else
        {
            GridViewSortDirection = SortDirection.Descending;
        }
        return Utilities.GenericListHelper<PointsofContactSummaryBE>.SortList((List<PointsofContactSummaryBE>)ViewState["lstPOC"], e.SortExpression, e.SortDirection).ToArray();
    }
    protected void gvVendorPOC_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int iCount = 1; iCount < e.Row.Cells.Count; iCount++)
                e.Row.Cells[iCount].Text = e.Row.Cells[iCount].Text + "%";
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        PointsofContactSummaryBE oPointsofContactSummaryBE = new PointsofContactSummaryBE();
        SCT_UserBAL sCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        oPointsofContactSummaryBE.Action = "PointofContactUsers";
        oPointsofContactSummaryBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
        oPointsofContactSummaryBE.VendorIDs = msVendor.SelectedVendorIDs;
        DataSet ds = sCT_UserBAL.GetPointOfContactBAL(oPointsofContactSummaryBE);
        ViewState["lstPOC"] = ds;
        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            btnExportToExcel.Visible = true;
            gvPointOfContact.DataSource = ds;
            gvPointOfContact.DataBind();
        }
        else
        {
            btnExportToExcel.Visible = true;
        }

        msVendor.ClearSearch();
    }
    protected void gvVendorPOC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPointOfContact.PageIndex = e.NewPageIndex;
        SortGridView(GridViewSortExp, GridViewSortDirection);
    }
    private void SortGridView(string sortExpression, SortDirection direction)
    {
        try
        {
            if (ViewState["lstPOC"] != null)
            {
                gvPointOfContact.DataSource = (DataSet)ViewState["lstPOC"];
                gvPointOfContact.DataBind();

                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(gvPointOfContact);
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            if (ViewState["lstPOC"] != null)
            {
                gvPointOfContactReport.DataSource = (DataSet)ViewState["lstPOC"];
                gvPointOfContactReport.DataBind();

                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(gvPointOfContactReport);

                WebCommon.Export("VendorPOCDetails", gvPointOfContactReport);
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="CommunicationLibrary.aspx.cs" Inherits="CommunicationLibrary" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/MultiSelectTypeofCommunication.ascx"
    TagName="ucMultiSelectTypeofCommunication" TagPrefix="cc3" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function PrintScreen() {
            document.getElementById('rwButtons').style.visibility = "hidden";
            window.print();
            document.getElementById('rwButtons').style.visibility = "visible";
            //setTimeout('self.close()', 1000);
        }
        function HideModel() {
            $find('mdlCommDetail').hide();
        }

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblCommunicationLibrary" Text="Communication Library" runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row  ">
        <cc2:ucExportToExcel ID="btnExcelCommLib" runat="server" style="text-align: left!important;" />
    </div>
   <%-- <asp:UpdatePanel ID="updSearch" runat="server">
        <ContentTemplate>--%>
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 15%">
                        <cc1:ucLabel ID="lblTypeofCommunication" Text="Type of Communication" runat="server"></cc1:ucLabel>:
                    </td>
                    <td>
                        <cc3:ucMultiSelectTypeofCommunication runat="server" ID="msTypeofCommunication" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 6em;">
                        <cc1:ucLabel ID="lblDatesent" Text="Date Sent" runat="server"></cc1:ucLabel>:
                    </td>
                    <td>
                        <table width="25%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-weight: bold; width: 6em;">
                                    <asp:TextBox ID="txtFromDate" runat="server" ClientIDMode="Static" class="date" />
                                    <%--<cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue1(this)" ReadOnly="True" Width="70px" />--%>
                                </td>
                                <td style="font-weight: bold; width: 4em; text-align: center">
                                    <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 6em;">
                                    <asp:TextBox ID="txtToDate" runat="server" ClientIDMode="Static" class="date" />
                                    <%--<cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                        onchange="setValue2(this)" ReadOnly="True" Width="70px" />--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="3">
                        <div class="button-row">
                            <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
    <asp:UpdatePanel ID="updCommLib" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center" colspan="9">
                        <cc1:ucGridView ID="gvCommLibrary" Width="100%" runat="server" CssClass="grid" OnRowCommand="gvCommLibrary_RowCommand"
                            OnPageIndexChanging="gvCommLibrary_PageIndexChanging" AllowPaging="true" PageSize="50"
                            EmptyDataText="No record Found.">
                            <Columns>
                                <asp:TemplateField HeaderText="DateSent" SortExpression="SentDate">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnCommunicationIDText" Value='<%#Eval("CommunicationID") %>'
                                            runat="server" />
                                        <%--<cc1:ucLinkButton ID="lnkDateSentText" Text='<%# Eval("SentDate") != DBNull.Value ?  Convert.ToDateTime(Eval("SentDate").ToString()).ToString("dd/MM/yyyy") : ""%>'
                                            runat="server"  CommandName="DateSentInfo"></cc1:ucLinkButton>--%>
                                        <%--<asp:HyperLink ID="hlDateSentText" runat="server" Target="_blank" Text='<%# Eval("SentDate") != DBNull.Value ?  Convert.ToDateTime(Eval("SentDate").ToString()).ToString("dd/MM/yyyy") : ""%>'
                                            NavigateUrl='<%# EncryptQuery("ShowCommunication.aspx?CommId=" + Eval("CommunicationID"))%>'></asp:HyperLink>--%>
                                        <a id="lnkDateSentText" href='<%# EncryptQuery("ShowCommunication.aspx?CommId=" + Eval("CommunicationID"))%>'
                                            runat="server" target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1000px,height=620px,toolbar=0,scrollbars=0,status=0"); return false;'>
                                            <%# Eval("SentDate") != DBNull.Value ?  Convert.ToDateTime(Eval("SentDate").ToString()).ToString("dd/MM/yyyy") : ""%></a>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SentBy" SortExpression="CreatedByEmail">
                                    <HeaderStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblSentOnBehalfOfValue" Text='<%#Eval("CreatedByEmail")%>' runat="server"></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SentOnBehalfOf" SortExpression="SentBy">
                                    <HeaderStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblSentByText" Text='<%#Eval("SentBy")%>' runat="server"></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TypeofCommunication" SortExpression="TypeofCommunication">
                                    <HeaderStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblTypeofCommunicationText" Text='<%#Eval("TypeofCommunication")%>'
                                            runat="server"></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Subject" SortExpression="Subject">
                                    <HeaderStyle Width="300px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblSubjectText" Text='<%#Eval("Subject")%>' runat="server"></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sentto" SortExpression="Sentto">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <cc1:ucLinkButton ID="lnkSentToText" Text='<%#Eval("SentTo")%>' runat="server" CommandName="SentToInfo"></cc1:ucLinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                        <cc1:ucGridView ID="gvCommLibraryExcel" Width="100%" runat="server" CssClass="grid"
                            Visible="false">
                            <Columns>
                                <asp:TemplateField HeaderText="DateSent" SortExpression="SentDate">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnCommunicationIDText" Value='<%#Eval("CommunicationID") %>'
                                            runat="server" />
                                        <cc1:ucLinkButton ID="lnkDateSentText" Text='<%# Eval("SentDate") != DBNull.Value ?  Convert.ToDateTime(Eval("SentDate").ToString()).ToString("dd/MM/yyyy") : ""%>'
                                            runat="server" CommandName="DateSentInfo"></cc1:ucLinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SentBy" SortExpression="CreatedByEmail">
                                    <HeaderStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblSentOnBehalfOfValue" Text='<%#Eval("CreatedByEmail")%>' runat="server"></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SentOnBehalfOf" SortExpression="SentBy">
                                    <HeaderStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblSentByText" Text='<%#Eval("SentBy")%>' runat="server"></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TypeofCommunication" SortExpression="TypeofCommunication">
                                    <HeaderStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblTypeofCommunicationText" Text='<%#Eval("TypeofCommunication")%>'
                                            runat="server"></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Subject" SortExpression="Subject">
                                    <HeaderStyle Width="300px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblSubjectText" Text='<%#Eval("Subject")%>' runat="server"></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sentto" SortExpression="Sentto">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <ItemTemplate>
                                        <cc1:ucLinkButton ID="lnkSentToText" Text='<%#Eval("SentTo")%>' runat="server" CommandName="SentToInfo"></cc1:ucLinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updCommDetail" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnCommDetailMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlCommDetail" runat="server" TargetControlID="btnCommDetailMPE"
                PopupControlID="pnlCommDetail"  BackgroundCssClass="modalBackground" BehaviorID="CommDetail"
                DropShadow="false" />
            <asp:Panel ID="pnlCommDetail" runat="server" Style="display: none; width: 450px;
                min-height: 300px;">
                <div style="overflow-y: scroll; height: 500px; overflow-x: hidden; background-color: #fff;
                    padding: 5px; border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder-popup"
                        style="width: 100%">
                        <tr>
                            <td>
                                <cc1:ucButton ID="btnClose" runat="server" Text="Close" CssClass="button" OnClientClick="HideModel();" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%;">
                                <cc1:ucLabel ID="lblSentTo" runat="server" Text="Sent To"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;">
                                <cc1:ucLabel ID="lblSentToIds" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;">
                                <div id="dvDisplayLetter" runat="server">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <%--<Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave_1" />
            <asp:AsyncPostBackTrigger ControlID="btnBack_2" />
            <asp:AsyncPostBackTrigger ControlID="btnAddComment" />
        </Triggers>--%>
    </asp:UpdatePanel>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.GlobalSettings;
using BusinessLogicLayer.ModuleBAL.GlobalSettings;
using BaseControlLibrary;

public partial class ModuleUI_GlobalSettings_VendorStockPlannerDetail : CommonPage
{
    bool ispostback = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        ddlCountry.CurrentPage = this;
        ucExportToExcel1.CurrentPage = this;
    //  ucExportToExcel1.IsHideHidden = true;     

    //  ddlCountry.innerControlddlCountry.AutoPostBack = true;
    //  ddlCountry.IsAllDefault = true;
    //  ddlCountry.IsAllRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("CountryID") != null)
            {
                ViewState["CountryID"] = true;
            }            
            ispostback = true;
           // BindVendorSPDetails();
        }

        ucExportToExcel1.GridViewControl = grdStockPlannerDetail;
        ucExportToExcel1.FileName = "VendorStockPlannerContactDetail";      

    }

    protected void BindVendorSPDetails()
    {
        PointsofContactSummaryBE APContactBE = new PointsofContactSummaryBE();
        PointsofContactSummaryBAL APContactBAL = new PointsofContactSummaryBAL();

        APContactBE.Action = "BindVendorSPDetails";

        if (ViewState["CountryID"] != null)
        {
            APContactBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
            ddlCountry.innerControlddlCountry.SelectedIndex = ddlCountry.innerControlddlCountry.Items.IndexOf(ddlCountry.innerControlddlCountry.Items.FindByValue                       (GetQueryStringValue("CountryID")));
            APContactBE.IsAllContact = true;
        }
        else
        {
            if (ispostback == false)
            {
                APContactBE.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedValue);
                APContactBE.IsAllContact = ddlAPStatus.SelectedValue == "0" ? false : true;
            }
            else
            {
                if (GetQueryStringValue("IsBackToSP") == null)
                {
                    return;
                }
               
            }

        }

        APContactBE.SCSelectedVendorIDs = msVendor.SelectedVendorIDs;
         List<PointsofContactSummaryBE> lstSPDetails =new List<PointsofContactSummaryBE> ();
        if (GetQueryStringValue("IsBackToSP") != null)
        {
            if (Session["APContactBE"] != null)
            {
                APContactBE = (PointsofContactSummaryBE)Session["APContactBE"];
                lstSPDetails = APContactBAL.GetVendorSPDetailBAL(APContactBE);
            }
        }
        else
        {
           lstSPDetails= APContactBAL.GetVendorSPDetailBAL(APContactBE);
        }

        Session["APContactBE"] = APContactBE;
        if (ViewState["CountryID"] != null)
        {
            ViewState["CountryID"] = null;            
        }
        ispostback = false;
        if (lstSPDetails != null && lstSPDetails.Count > 0)
        {
            ucExportToExcel1.Visible = true;
            grdStockPlannerDetail.DataSource = lstSPDetails;
            grdStockPlannerDetail.DataBind();
            ViewState["lstSPDetails"] = lstSPDetails;
        }
        else 
        {
            ucExportToExcel1.Visible = false;
            grdStockPlannerDetail.DataSource = null;
            grdStockPlannerDetail.DataBind();
            ViewState["lstSPDetails"] = null;
        }
      
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<PointsofContactSummaryBE>.SortList((List<PointsofContactSummaryBE>)ViewState["lstSPDetails"], e.SortExpression,                       e.SortDirection).ToArray();
    }



    protected void btnGo_Click(object sender, EventArgs e)
    {
        BindVendorSPDetails();
        setData();
    }

    public void setData()
    {
        msVendor.setVendorsOnPostBack();
        ////************************* Vendor ***************************
        string IncludeVendorId = msVendor.SelectedVendorIDs;
        TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;
        string txtVendorIdText = txtVendorId.Text;
        ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
        // lstRightVendor.Items.Clear();
        if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(IncludeVendorId) || !string.IsNullOrEmpty(txtVendorIdText)))
        {
            lstLeftVendor.Items.Clear();
            //lstRightVendor.Items.Clear();
            msVendor.SearchVendorClick(txtVendorIdText);
            if (!string.IsNullOrEmpty(IncludeVendorId))
            {
                string[] strIncludeVendorIDs = IncludeVendorId.Split(',');
                for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                    if (listItem != null)
                    {
                        // lstRightVendor.Items.Add(listItem);
                        lstLeftVendor.Items.Remove(listItem);
                    }
                }
            }
        }

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("VendorStockPlannerSummary.aspx");
    }

    public override void CountryPost_Load()
    {
        if (!IsPostBack)
        {
            BindVendorSPDetails();
        }
    }
}
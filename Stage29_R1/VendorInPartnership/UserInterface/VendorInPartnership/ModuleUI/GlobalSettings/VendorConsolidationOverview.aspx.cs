﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using System.Collections.Generic;
using BaseControlLibrary;
using Utilities;

public partial class VendorConsolidationOverview : CommonPage
{
    #region Events ...

    public SortDirection GridViewSortDirection {
        get {
            if (ViewState["GridViewSortDirection"] == null)
                ViewState["GridViewSortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["GridViewSortDirection"];
        }
        set { ViewState["GridViewSortDirection"] = value; }
    }

    public string SortDir {
        get {
            if (GridViewSortDirection == SortDirection.Ascending) {
                return "DESC";
            }
            else {
                return "ASC";
            }
        }
    }


    public string GridViewSortExp {
        get {
            if (ViewState["GridViewSortExp"] == null)
                ViewState["GridViewSortExp"] = "ConsolidatedCode";
            return (string)ViewState["GridViewSortExp"];
        }
        set { ViewState["GridViewSortExp"] = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ddlCountry.CurrentPage = this;
        ddlCountry.innerControlddlCountry.AutoPostBack = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ddlCountry.innerControlddlCountry.Items.Insert(0, new ListItem("--Select--", "-1"));
            //ddlCountry.innerControlddlCountry.SelectedIndex = 0;
            if (Session["LocalVendorSearch"] != null)
            {
                UP_VendorBE oUP_VendorBE = new UP_VendorBE();
                oUP_VendorBE = (UP_VendorBE)Session["LocalVendorSearch"];
                msVendor.SearchCountryID = Convert.ToInt32(oUP_VendorBE.CountryID);
            }
            else if (Session["SiteCountryID"] != null)
            {
                msVendor.SearchCountryID = Convert.ToInt32(Session["SiteCountryID"]);
            }

            if (GetQueryStringValue("PageFrom") != null && GetQueryStringValue("PageFrom") == "VendorOverview") {
                BindGrid();
            }

            if (SortDir == "ASC") {
                GridViewSortDirection = SortDirection.Ascending;
            }
            else {
                GridViewSortDirection = SortDirection.Descending;
            }
        }
        else if (IsPostBack)
        {
            if (ddlCountry.innerControlddlCountry.Items.Count > 0)
                if (!string.IsNullOrEmpty(ddlCountry.innerControlddlCountry.SelectedItem.Value))
                    msVendor.SearchCountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
        }
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = grdVendorConsExcel;
        ucExportToExcel1.FileName = "VendorConsolidationOverview"; 
    }

    protected void grdVendorCons_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdVendorCons.PageIndex = e.NewPageIndex;
        //BindGrid();
        
        SortGridView(GridViewSortExp, GridViewSortDirection);
    }

    public override void CountrySelectedIndexChanged()
    {
        base.CountrySelectedIndexChanged();
        ucListBox lstRight = msVendor.FindControl("lstRight") as ucListBox;
        if (lstRight != null)
            lstRight.Items.Clear();

        ucListBox lstLeft = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
        if (lstLeft != null)
            lstLeft.Items.Clear();

        HiddenField hiddenSelectedIDs = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
        if (hiddenSelectedIDs != null)
            hiddenSelectedIDs.Value = string.Empty;

        HiddenField hiddenSelectedName = msVendor.FindControl("hiddenSelectedName") as HiddenField;
        if (hiddenSelectedName != null)
            hiddenSelectedName.Value = string.Empty;

    }

    public override void CountryPost_Load()
    {
        if (!IsPostBack)
        {
            //ddlCountry.innerControlddlCountry.AutoPostBack = true;
            //int countryId = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);

            if (Session["LocalVendorSearch"] != null)
            {
                UP_VendorBE oUP_VendorBE = new UP_VendorBE();
                oUP_VendorBE = (UP_VendorBE)Session["LocalVendorSearch"];

                /* Country .. */
                if (ddlCountry.innerControlddlCountry.Items.Count > 0 && oUP_VendorBE.CountryID != null)
                {
                    ddlCountry.innerControlddlCountry.SelectedIndex =
                        ddlCountry.innerControlddlCountry.Items.IndexOf(ddlCountry.innerControlddlCountry.Items.FindByValue(
                        Convert.ToString(oUP_VendorBE.CountryID)));
                }
                Session["LocalVendorSearch"] = null;
            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        List<UP_VendorBE> lstConsolidatesVendor = new List<UP_VendorBE>();
        oUP_VendorBE.Action = "GetVendorConsolidation";
        if (ddlCountry.innerControlddlCountry.Items.Count > 0)
            if (!string.IsNullOrEmpty(ddlCountry.innerControlddlCountry.SelectedItem.Value))
                oUP_VendorBE.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);

        #region Vendor search related ..

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            oUP_VendorBE.VendorIDs = msVendor.SelectedVendorIDs;

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorName))
            oUP_VendorBE.VendorName = msVendor.SelectedVendorName;

        TextBox txtSearchedVendor = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;
        if (txtSearchedVendor != null)
            if (!string.IsNullOrEmpty(txtSearchedVendor.Text) && !string.IsNullOrWhiteSpace(txtSearchedVendor.Text))
                oUP_VendorBE.SearchedVendorText = txtSearchedVendor.Text;

        #endregion

        msVendor.setVendorsOnPostBack();

        lstConsolidatesVendor = oUP_VendorBAL.GetVendorConsolidationBAL(oUP_VendorBE).FindAll(x => x.ConsVendorFlag != 'G');
        oUP_VendorBAL = null;
        grdVendorCons.PageIndex = 0;
        grdVendorCons.DataSource = lstConsolidatesVendor;
        grdVendorCons.DataBind();

        //-- Add By Abhinav
        grdVendorConsExcel.DataSource = lstConsolidatesVendor;
        grdVendorConsExcel.DataBind();
        //--------------

        ViewState["lstSites"] = lstConsolidatesVendor;

        
        GridViewSortExp = "ConsolidatedCode";
        GridViewSortDirection = SortDirection.Descending;

        Session["LocalVendorSearch"] = oUP_VendorBE;
    }


    #endregion

    #region Methods ...

    //public override bool PreExportToExcel()
    //{
    //    UP_VendorBE oUP_VendorBE = new UP_VendorBE();
    //    UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
    //    List<UP_VendorBE> lstConsolidatesVendor = new List<UP_VendorBE>();
    //    oUP_VendorBE.Action = "GetVendorConsolidation";
    //    lstConsolidatesVendor = oUP_VendorBAL.GetVendorConsolidationBAL(oUP_VendorBE);
    //    oUP_VendorBAL = null;
    //    grdVendorConsExcel.DataSource = null;
    //    grdVendorConsExcel.DataSource = lstConsolidatesVendor;
    //    grdVendorConsExcel.DataBind();
    //    return true;
    //}

    private void SortGridView(string sortExpression, SortDirection direction) {
        List<UP_VendorBE> lstConsolidatesVendor = new List<UP_VendorBE>();
        try {
            if (ViewState["lstSites"] != null) {

                lstConsolidatesVendor = Utilities.GenericListHelper<UP_VendorBE>.SortList((List<UP_VendorBE>)ViewState["lstSites"], sortExpression, direction);

                grdVendorCons.DataSource = lstConsolidatesVendor;
                grdVendorCons.DataBind();

                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(grdVendorCons);
            }
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }

    protected void BindGrid()
    {
        //UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        //UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        //List<UP_VendorBE> lstConsolidatesVendor = new List<UP_VendorBE>();
        //oUP_VendorBE.Action = "GetVendorConsolidation";
        //lstConsolidatesVendor = oUP_VendorBAL.GetVendorConsolidationBAL(oUP_VendorBE);
        //oUP_VendorBAL = null;
        //grdVendorCons.DataSource = lstConsolidatesVendor;
        //grdVendorCons.DataBind();
        //ViewState["lstSites"] = lstConsolidatesVendor;

        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        //UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        List<UP_VendorBE> lstConsolidatesVendor = new List<UP_VendorBE>();

        if (Session["LocalVendorSearch"] != null)
        {
            oUP_VendorBE = (UP_VendorBE)Session["LocalVendorSearch"];

            #region Setting controls of Searched Criteria ..

            /* Country .. */
            if (ddlCountry.innerControlddlCountry.Items.Count > 0 && oUP_VendorBE.CountryID != null)
            {
                ddlCountry.innerControlddlCountry.SelectedIndex =
                    ddlCountry.innerControlddlCountry.Items.IndexOf(ddlCountry.innerControlddlCountry.Items.FindByValue(
                    Convert.ToString(oUP_VendorBE.CountryID)));
            }

            /* Vendor .. */
            HiddenField hiddenSelectedIDs = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
            if (hiddenSelectedIDs != null && !string.IsNullOrEmpty(oUP_VendorBE.VendorIDs))
                hiddenSelectedIDs.Value = oUP_VendorBE.VendorIDs;

            HiddenField hiddenSelectedName = msVendor.FindControl("hiddenSelectedName") as HiddenField;
            if (hiddenSelectedName != null && !string.IsNullOrEmpty(oUP_VendorBE.VendorName))
                hiddenSelectedName.Value = oUP_VendorBE.VendorName;

            ucListBox lstRight = msVendor.FindControl("lstRight") as ucListBox;
            ucListBox lstLeft = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
            if (lstLeft != null && lstRight != null && !string.IsNullOrEmpty(oUP_VendorBE.SearchedVendorText)
                && !string.IsNullOrEmpty(oUP_VendorBE.VendorIDs))
            {
                lstLeft.Items.Clear();
                lstRight.Items.Clear();
                msVendor.SearchVendorClick(oUP_VendorBE.SearchedVendorText);

                string[] strVendorIDs = oUP_VendorBE.VendorIDs.Split(',');
                for (int index = 0; index < strVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeft.Items.FindByValue(strVendorIDs[index]);
                    if (listItem != null)
                    {
                        lstRight.Items.Add(listItem);
                        lstLeft.Items.Remove(listItem);
                    }
                }
            }

            #endregion
        }
        else
        {
            oUP_VendorBE.Action = "GetVendorConsolidation";

            if (Session["SiteCountryID"] != null && ddlCountry.innerControlddlCountry.Items.Count == 0)
                oUP_VendorBE.CountryID = Convert.ToInt32(Session["SiteCountryID"]);
            else
                oUP_VendorBE.CountryID = ddlCountry.innerControlddlCountry.SelectedIndex > 0 ? Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value) : (int?)null;

            #region Vendor search related ..

            if (GetQueryStringValue("VendorID") != null)
            {
                oUP_VendorBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
            }


            if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
                oUP_VendorBE.VendorIDs = msVendor.SelectedVendorIDs;

            if (!string.IsNullOrEmpty(msVendor.SelectedVendorName))
                oUP_VendorBE.VendorName = msVendor.SelectedVendorName;

            TextBox txtSearchedVendor = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;
            if (txtSearchedVendor != null)
                oUP_VendorBE.SearchedVendorText = txtSearchedVendor.Text;

            #endregion
        }


        lstConsolidatesVendor = GetVendor(oUP_VendorBE);
        
        grdVendorCons.DataSource = lstConsolidatesVendor;
        grdVendorCons.DataBind();

        //-- Add By Abhinav
        grdVendorConsExcel.DataSource = lstConsolidatesVendor;
        grdVendorConsExcel.DataBind();
        //--------------

        ViewState["lstSites"] = lstConsolidatesVendor;
        //Session["LocalVendorSearch"] = null; /* THis code is being null in [CountryPost_Load()] method in Event region senction. */
    }

    //Add By Abhinav -- 17 July 2013 -- To get Vendors from existing Viewstste
    private List<UP_VendorBE> GetVendor(UP_VendorBE oUP_VendorBE) {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        List<UP_VendorBE> lstConsolidatesVendor = new List<UP_VendorBE>();

        if (ViewState["lstSites"] == null) {
            lstConsolidatesVendor = oUP_VendorBAL.GetVendorConsolidationBAL(oUP_VendorBE).FindAll(x => x.ConsVendorFlag != 'G'); ;
            oUP_VendorBAL = null;            
        }
        else {
            lstConsolidatesVendor = (List<UP_VendorBE>)ViewState["lstSites"];
        }
        return lstConsolidatesVendor;
    }
    //----------------------------//

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        GridViewSortExp = e.SortExpression;
        if (SortDir == "ASC") {
            GridViewSortDirection = SortDirection.Ascending;
        }
        else {
            GridViewSortDirection = SortDirection.Descending;
        }
        return Utilities.GenericListHelper<UP_VendorBE>.SortList((List<UP_VendorBE>)ViewState["lstSites"], e.SortExpression, GridViewSortDirection).ToArray();
    }

    #endregion
    
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BaseControlLibrary;
using System.Collections;

public partial class ModuleUI_GlobalSettings_ReturnsAgreementOverView : CommonPage
{
    private bool IsGoClicked = false;

    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected override void OnPreRender(EventArgs e)
    {       
        if (!IsPostBack)
        {
            if (GetQueryStringValue("PageFrom") == null)
            {
                BindGrid();
            }
            
            else if (Session["ReturnsAgreementSearch"] != null)
            {
                if (IsGoClicked == false)
                {
                    GetSession();
                    RetainSearchData();
                }
            }
        }
        base.OnPreRender(e);
    }

    protected void Page_Init(object sender, EventArgs e)
    {  
        ucCountry.CurrentPage = this;
        ucCountry.IsAllRequired = true;
        btnExportToExcel1.CurrentPage = this;
        btnExportToExcel1.GridViewControl = gvExport;
        btnExportToExcel1.FileName = "Returns Agreement View";        
    }

     private void BindGrid()
    {
        MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
        MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();
        List<MAS_VendorBE> lstVendor = new List<MAS_VendorBE>();
        oMAS_VendorBE.Action = "GetReturnsAgreementView";

        oMAS_VendorBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);

        #region Vendor search related ..
        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            oMAS_VendorBE.VendorIDs = msVendor.SelectedVendorIDs;


        if (!string.IsNullOrEmpty(msVendor.SelectedVendorName))
            oMAS_VendorBE.Vendor_Name = msVendor.SelectedVendorName;

        TextBox txtSearchedVendor = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;
        if (txtSearchedVendor != null)
            oMAS_VendorBE.SearchedVendorText = txtSearchedVendor.Text;
        #endregion

        // msVendor.setVendorsOnPostBack();
        //}

        if (rdoAgreementinPlace.Checked)
        {
            oMAS_VendorBE.AgreementType = "AgreementinPlace";
        }
        else if (rdoNoAgreement.Checked)
        {
            oMAS_VendorBE.AgreementType = "NoAgreement";
        }
        else if(rdoAllVendors.Checked)
        {
            oMAS_VendorBE.AgreementType = "All";
        }


        this.SetSession(oMAS_VendorBE);

        lstVendor = oMAS_VendorBAL.GetOverstockReturnsAgreementOverviewBAL(oMAS_VendorBE);
        oMAS_VendorBAL = null;

        gvReturnsAgreementOverview.PageIndex = 0;
        gvReturnsAgreementOverview.DataSource = lstVendor;
        gvReturnsAgreementOverview.DataBind();
        gvExport.DataSource = lstVendor;
        gvExport.DataBind();

        if(lstVendor != null && lstVendor.Count>0)
        {
            btnExportToExcel1.Visible = true;
        }
        else
        {
            btnExportToExcel1.Visible = false;
        }


        ViewState["lstSites"] = lstVendor;       
    }

     protected void btnShow_Click(object sender, EventArgs e)
     {
         IsGoClicked = true;
         BindGrid();
         RetainSearchData();   
     }

     protected void gvRetursAgreementOverview_PageIndexChanging(object sender, GridViewPageEventArgs e)
     {
         gvReturnsAgreementOverview.PageIndex = e.NewPageIndex;
         gvReturnsAgreementOverview.DataSource = ViewState["lstSites"];
         gvReturnsAgreementOverview.DataBind();
     }


     private void SetSession(MAS_VendorBE oMAS_VendorBE)
     {
         Session.Remove("ReturnsAgreementSearch");
         Session["ReturnsAgreementSearch"] = null;

         Hashtable htReturnsAgreementView = new Hashtable();
         TextBox txtVendor = (TextBox)msVendor.FindControl("ucVendor").FindControl("txtVendorNo");

         htReturnsAgreementView.Add("Vendor_Name", oMAS_VendorBE.Vendor_Name);
         htReturnsAgreementView.Add("CountryID", oMAS_VendorBE.CountryID);
         htReturnsAgreementView.Add("SelectedVendorIDs", oMAS_VendorBE.VendorIDs);
         htReturnsAgreementView.Add("txtVendor", txtVendor.Text);
         htReturnsAgreementView.Add("IsSearchedByVendorNo", msVendor.IsSearchedByVendorNo);
         htReturnsAgreementView.Add("SelectedVendorName", msVendor.SelectedVendorName);

         htReturnsAgreementView.Add("SearchedVendorText", oMAS_VendorBE.SearchedVendorText);
        //  htOverstockAgreementView.Add("SearchedVendorText", oMAS_VendorBE.SearchedVendorText);

        htReturnsAgreementView.Add("AgreementType", oMAS_VendorBE.AgreementType);

        Session["ReturnsAgreementSearch"] = htReturnsAgreementView;
     }

     private void GetSession()
     {
         MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
         MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();
         List<MAS_VendorBE> lstVendor = new List<MAS_VendorBE>();

           TextBox txtSearchedVendor = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;

         if (Session["ReturnsAgreementSearch"] != null)
         {
             Hashtable htReturnsAgreementView = (Hashtable)Session["ReturnsAgreementSearch"];

             oMAS_VendorBE.CountryID = (htReturnsAgreementView.ContainsKey("CountryID") && htReturnsAgreementView["CountryID"] != null) ? Convert.ToInt32(htReturnsAgreementView["CountryID"].ToString()) : 0;

             ucCountry.innerControlddlCountry.SelectedIndex = ucCountry.innerControlddlCountry.Items.IndexOf(ucCountry.innerControlddlCountry.Items.FindByValue(htReturnsAgreementView["CountryID"].ToString()));

             oMAS_VendorBE.VendorIDs = (htReturnsAgreementView.ContainsKey("SelectedVendorIDs") && htReturnsAgreementView["SelectedVendorIDs"] != null) ? htReturnsAgreementView["SelectedVendorIDs"].ToString() : null;

            oMAS_VendorBE.AgreementType = (htReturnsAgreementView.ContainsKey("AgreementType") && htReturnsAgreementView["AgreementType"] != null) ? htReturnsAgreementView["AgreementType"].ToString() : null;

            //  txtSearchedVendor.Text = (htOverstockAgreementView.ContainsKey("SearchedVendorText") && htOverstockAgreementView["SearchedVendorText"] != null) ? htOverstockAgreementView["SearchedVendorText"].ToString() : null;

            //if (htOverstockAgreementView["VendorIDs"] != null)
            //{
            //    msVendor.SelectedVendorIDs = "";
            //    msVendor.SelectedVendorName = "";
            //    msVendor.SelectedVendorIDs = htOverstockAgreementView["VendorIDs"].ToString();
            //    msVendor.SelectedVendorName = htOverstockAgreementView["Vendor_Name"].ToString();
            //    msVendor.setVendorsOnPostBack();
            //}

            oMAS_VendorBE.Action = "GetReturnsAgreementView";

             lstVendor = oMAS_VendorBAL.GetOverstockReturnsAgreementOverviewBAL(oMAS_VendorBE);

             if (lstVendor != null && lstVendor.Count > 0)
             {
                 gvReturnsAgreementOverview.DataSource = lstVendor;
                 gvExport.DataSource = lstVendor;
               

             }
             else
             {
                 gvReturnsAgreementOverview.DataSource = null;
                 gvExport.DataSource = null;
             }

             gvReturnsAgreementOverview.DataBind();
             gvExport.DataBind();

         }
     }

     private void RetainSearchData()
     {

         Hashtable htReturnsAgreementView = (Hashtable)Session["ReturnsAgreementSearch"];

         // ********** Vendor ***************
         string txtVendor = (htReturnsAgreementView.ContainsKey("txtVendor") && htReturnsAgreementView["txtVendor"] != null) ? htReturnsAgreementView["txtVendor"].ToString() : "";
         string VendorId = (htReturnsAgreementView.ContainsKey("SelectedVendorIDs") && htReturnsAgreementView["SelectedVendorIDs"] != null) ? htReturnsAgreementView["SelectedVendorIDs"].ToString() : "";
         TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;

         bool IsSearchedByVendorNo = (htReturnsAgreementView.ContainsKey("IsSearchedByVendorNo") && htReturnsAgreementView["IsSearchedByVendorNo"] != null) ? Convert.ToBoolean(htReturnsAgreementView["IsSearchedByVendorNo"]) : false;
         string txtVendorIdText = txtVendorId.Text;
         ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
         ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
         string strVendorId = string.Empty;
         int value;
         lstLeftVendor.Items.Clear();
         if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(VendorId) || !string.IsNullOrEmpty(VendorId)))
         {
             lstLeftVendor.Items.Clear();
             lstRightVendor.Items.Clear();

             if (IsSearchedByVendorNo == true)
             {
                 msVendor.SearchVendorNumberClick(txtVendor);
                 //parsing successful 
             }
             else
             {
                 msVendor.SearchVendorClick(txtVendor);
                 //parsing failed. 
             }

             if (!string.IsNullOrEmpty(VendorId))
             {
                 string[] strIncludeVendorIDs = VendorId.Split(',');
                 for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                 {
                     ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                     if (listItem != null)
                     {

                         strVendorId += strIncludeVendorIDs[index] + ",";
                         lstRightVendor.Items.Add(listItem);
                         lstLeftVendor.Items.Remove(listItem);
                     }
                 }
             }

             HiddenField hdnSelectedVendor = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
             hdnSelectedVendor.Value = strVendorId;
         }
         else
         {
             lstRightVendor.Items.Clear();
         }

        string AgreementType = (htReturnsAgreementView.ContainsKey("AgreementType") && htReturnsAgreementView["AgreementType"] != null) ? htReturnsAgreementView["AgreementType"].ToString() : "";

        if (AgreementType.Equals("AgreementinPlace"))
        {
            rdoAgreementinPlace.Checked = true;
        }
        else if (AgreementType.Equals("NoAgreement"))
        {
            rdoNoAgreement.Checked = true;
        }
        else if (AgreementType.Equals("All"))
        {
            rdoAllVendors.Checked = true;
        }
    }


}
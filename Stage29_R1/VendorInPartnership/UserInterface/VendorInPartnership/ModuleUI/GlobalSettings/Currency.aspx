﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" 
AutoEventWireup="true" CodeFile="Currency.aspx.cs" Inherits="Currency" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblCurrencySetUp" runat="server" Text="Currency Type Set Up"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="CurrencyEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="gvCurrency" Width="100%" runat="server" CssClass="grid"
                    OnSorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Currency" SortExpression="CurrencyName">
                            <HeaderStyle Width="40%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpCurrency" runat="server" Text='<%# Eval("CurrencyName") %>' NavigateUrl='<%# EncryptQuery("CurrencyEdit.aspx?CurrencyID="+ Eval("CurrencyID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Created By" ItemStyle-Wrap="false" DataField="CreatedBy"
                            SortExpression="CreatedBy">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Date Created" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                            DataField="DateCreated" SortExpression="DateCreated">
                            <HeaderStyle HorizontalAlign="Center" Width="45%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;

public partial class ModuleUI_GlobalSettings_TradeEntitiesOverview : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            BindTradeEntities();
        }
        //ucExportToExcel1.GridViewControl = gvTradeEntitiesOverview;
        //ucExportToExcel1.FileName = "Trade Entities Overview";
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvExport;
        ucExportToExcel1.FileName = "Trade Entities Overview";
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (ViewState["RecordNo"] != null)
        {
            EncryptQueryString("TradeEntitiesEdit.aspx?RecordNo=" + ViewState["RecordNo"]);
        }
        else
        {
            EncryptQueryString("TradeEntitiesEdit.aspx?RecordNo=1");
        }
    }


    protected void gvTradeEntitiesOverview_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink hplink = (HyperLink)e.Row.FindControl("hpTradeEntitiesID");

            if (Convert.ToString(Session["Role"]) == "Vendor")
            {
                hplink.Enabled = false;
                hplink.ForeColor = System.Drawing.Color.Black;
                btnAdd.Visible = false;
            }
            else
            {
                hplink.Enabled = true;                
                btnAdd.Visible = true;
            } 
        }
    }

    #region Methods

    protected void BindTradeEntities()
    {
        MAS_TradeEntitiesBE oMAS_TradeEntitiesBE = new MAS_TradeEntitiesBE();
        MAS_TradeEntitiesBAL oMAS_TradeEntitiesBAL = new MAS_TradeEntitiesBAL();

        oMAS_TradeEntitiesBE.Action = "ShowAll";

        List<MAS_TradeEntitiesBE> lstTradeEntities = oMAS_TradeEntitiesBAL.GetTradeEntitiesBAL(oMAS_TradeEntitiesBE);

        if (lstTradeEntities.Count > 0)
        {
            ViewState["RecordNo"] = lstTradeEntities.Count+1;

            gvTradeEntitiesOverview.DataSource = lstTradeEntities;
            gvTradeEntitiesOverview.DataBind();
            gvExport.DataSource = lstTradeEntities;
            gvExport.DataBind();
            ViewState["lstTradeEntities"] = lstTradeEntities;
        }

       


    }
    #endregion
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" ValidateRequest="false" CodeFile="SiteSetupOverview.aspx.cs" Inherits="SiteSetupOverview" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<%@ Register src="../../CommonUI/UserControls/ucAddButton.ascx" tagname="ucAddButton" tagprefix="cc2" %>
<%@ Register src="../../CommonUI/UserControls/ucExportToExcel.ascx" tagname="ucExportToExcel" tagprefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">        
    <h2><cc1:ucLabel ID="lblSiteSetUp" runat="server"></cc1:ucLabel>    
  
    </h2>
<div class="button-row">
    <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="SitePrefixSetupEdit.aspx" />  
    <cc2:ucExportToExcel ID="btnExportToExcel1" runat="server"/>    
</div>
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="grdSiteOverview"  Width="100%"  runat="server" 
                    CssClass="grid" onrowdatabound="grdSiteOverview_RowDataBound" 
                    AllowSorting="True"   onsorting="SortGrid">                                        
                    <Columns>                                                 
                         <asp:BoundField HeaderText="Site" DataField="SiteDescription" SortExpression="SiteDescription">
                            <HeaderStyle Width="25%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>  

                         <asp:TemplateField HeaderText="SiteNumber" SortExpression="SiteNumberList">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpSiteno" runat="server" Text='<%# Eval("SiteNumberList") %>' NavigateUrl='<%# EncryptQuery("SitePrefixSetupEdit.aspx?SiteID="+ Eval("SiteID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField> 
                        

                        <asp:BoundField HeaderText="Phone" DataField="PhoneNumbers" SortExpression="PhoneNumbers">
                        <HeaderStyle HorizontalAlign="Left" Width="14%" />
                        <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Fax" DataField="FaxNumber" SortExpression="FaxNumber">
                        <HeaderStyle HorizontalAlign="Left" Width="14%" />
                        <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Email" DataField="EmailID" SortExpression="EmailID">
                        <HeaderStyle HorizontalAlign="Left" Width="22%" />
                        <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="ManagerName" DataField="SiteManagerName" SortExpression="SiteManagerName">
                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                        <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>                    
                    
                </cc1:ucGridView>               
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td> 
            <td></td> 
        </tr>        
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SelectSQL.aspx.cs" ValidateRequest="false"
    MasterPageFile="~/CommonUI/CMN_MasterPages/NoUser_MasterPage.Master" Inherits="ShowData"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table style="width:100%;">
            <tr>
                <td style="width: 100%; height: 100%;">
                    <asp:Label ID="lblErrorMessage" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100%; height: 100%;">
                    <table>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkShowText" runat="server" AutoPostBack="True" OnCheckedChanged="chkShowText_CheckedChanged" />
                            </td>
                            <td>
                                <asp:Panel ID="pnlShowTextBox" Visible="false" runat="server">
                                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server"></asp:TextBox>
                                    <cc1:ucButton ID="btnValidateUser" Text="Validate User" runat="server" OnClick="btnValidateUser_Click" />
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Panel ID="pnlDoQuerySql" runat="server" Visible="false">
                        <asp:TextBox ID="txtQueryResult" runat="server" TextMode="MultiLine" Height="250px"
                            Width="800px"></asp:TextBox>
                        <br />
                        <div>
                        <cc1:ucButton CssClass="button" ID="btnShowData" Text=" Show Data " runat="server" OnClick="btnShowData_Click" />
                        <cc1:ucButton CssClass="button" ID="btnClearData" Text=" Clear Data " runat="server" OnClick="btnClearData_Click" />
                        <cc1:ucButton CssClass="button" ID="btnDownloadScript" Text=" Download Script " Visible="false" runat="server" OnClick="btnDownloadScript_Click" />
                        
                            </div>
                        
                    </asp:Panel>
                </td>
            </tr>
        </table>

       
    </div>

     <div style="width:960px;        overflow: scroll;" >
        <br />
                        <asp:GridView ID="gvShowData" runat="server" CssClass="grid" onpageindexchanging="gvShowData_PageIndexChanging" 
                            AllowPaging="true" PageSize="50" Font-Size="8px" Font-Names="verdana" >
                           <%-- <AlternatingRowStyle BackColor="White" ForeColor="Black" />                            
                            <FooterStyle BackColor="Blue" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="Blue" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Left" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" /> --%>
                        </asp:GridView>
        </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Data;
using System.Data.SqlClient;
using WebUtilities;

public partial class CurrencyConvertorEdit : CommonPage {
    CurrencyConverterBAL o_currencyBAL = new CurrencyConverterBAL();
    CurrencyConverterBE o_CurrencyBE = new CurrencyConverterBE();

    #region Events

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            BindCurrencyddl();
            BindConvertToCurrencyddl();
            GetCurrencyType();
        }
    }
    protected void btnback_Click(object sender, EventArgs e) {
        EncryptQueryString("CurrencyConverter.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e) {
        int? resultType = null;

        o_CurrencyBE.CurrencyID = Convert.ToInt32(ddlCurrency.SelectedValue);
        o_CurrencyBE.ConvertCurrencyID = Convert.ToInt32(ddlConvertCurrency.SelectedValue);
        o_CurrencyBE.ExchangeRate = Convert.ToDouble(txtExchangeRates.Text.Trim());
        o_CurrencyBE.UserID = Convert.ToInt32(Session["UserID"].ToString());


        //Update Currency type.
        if (GetQueryStringValue("CurrencyConverterID") != null && (!string.IsNullOrEmpty(GetQueryStringValue("CurrencyConverterID")))) {
            o_CurrencyBE.Action = "UpdateCurrencyConversion";
            o_CurrencyBE.CurrencyConverterID = Convert.ToInt32(GetQueryStringValue("CurrencyConverterID"));

        }
        else {

            //Check the duplicacy of Currency.
            o_CurrencyBE.Action = "CheckExistance";
           int? isExists = o_currencyBAL.CheckCurrencyBAL(o_CurrencyBE);
            if (Convert.ToInt16(isExists) == 1) {
                string errorMessage = WebCommon.getGlobalResourceValue("CurrencyCurrencyToExists");
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
                return;
            }

            //Add new Currency type.
            o_CurrencyBE.Action = "AddCurrencyConversion";
        }


        resultType = o_currencyBAL.addEditCurrencyConverterBAL(o_CurrencyBE);
        if (resultType == 0)
            EncryptQueryString("CurrencyConverter.aspx");
    }

    #endregion

    #region Methods

    private void GetCurrencyType() {
        if (GetQueryStringValue("CurrencyConverterID") != null) {

            o_CurrencyBE.Action = "ShowAll";
            o_CurrencyBE.CurrencyConverterID = Convert.ToInt32(GetQueryStringValue("CurrencyConverterID"));
            //CurrencyConverterBAL o_currencyBAL = new CurrencyConverterBAL();
            List<CurrencyConverterBE> lstCurrencyBE = o_currencyBAL.BindCurrencyBAL(o_CurrencyBE);


            if (lstCurrencyBE != null && lstCurrencyBE.Count > 0) {
                ddlCurrency.SelectedIndex = ddlCurrency.Items.IndexOf(ddlCurrency.Items.FindByValue(lstCurrencyBE[0].CurrencyID.ToString()));
                ddlConvertCurrency.SelectedIndex = ddlConvertCurrency.Items.IndexOf(ddlConvertCurrency.Items.FindByValue(lstCurrencyBE[0].ConvertCurrencyID.ToString()));
                txtExchangeRates.Text = (Convert.ToDouble(lstCurrencyBE[0].ExchangeRate)).ToString();
            }
            btnSave.Visible = true;
            ddlConvertCurrency.Enabled = false;
            ddlCurrency.Enabled = false;
        }
    }
    private void BindCurrencyddl() {
        o_CurrencyBE.Action = "ShowAllCurrency";
        List<CurrencyConverterBE> lstCurrencyNamesBE = o_currencyBAL.BindCurrencyddlBAL(o_CurrencyBE);

        if (lstCurrencyNamesBE.Count > 0 && lstCurrencyNamesBE != null) {
            FillControls.FillDropDown(ref ddlCurrency, lstCurrencyNamesBE, "Currency", "CurrencyID", "--All--");
        }


    }
    private void BindConvertToCurrencyddl() {
        o_CurrencyBE.Action = "ShowAllCurrency";
        List<CurrencyConverterBE> lstCurrencyNamesBE = o_currencyBAL.BindCurrencyddlBAL(o_CurrencyBE);

        if (lstCurrencyNamesBE.Count > 0 && lstCurrencyNamesBE != null) {
            FillControls.FillDropDown(ref ddlConvertCurrency, lstCurrencyNamesBE, "Currency", "CurrencyID", "--All--");
        }
    }

    #endregion







    protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e) {

    }
}

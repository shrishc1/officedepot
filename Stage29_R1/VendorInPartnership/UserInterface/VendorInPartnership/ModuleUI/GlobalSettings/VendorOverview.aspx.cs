﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System.Text;
using BaseControlLibrary;
using Utilities;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Data;

public partial class VendorOverview : CommonPage
{

    public SortDirection GridViewSortDirection {
        get {
            if (ViewState["GridViewSortDirection"] == null)
                ViewState["GridViewSortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["GridViewSortDirection"];
        }
        set { ViewState["GridViewSortDirection"] = value; }
    }

    public string SortDir {
        get {
            if (GridViewSortDirection == SortDirection.Ascending) {
                return "DESC";
            }
            else {
                return "ASC";
            }
        }
    }

    public string GridViewSortExp {
        get {
            if (ViewState["GridViewSortExp"] == null)
                ViewState["GridViewSortExp"] = "Vendor_No";
            return (string)ViewState["GridViewSortExp"];
        }
        set { ViewState["GridViewSortExp"] = value; }
    }

    #region Events ...

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            //ucSite.innerControlddlSite.SelectedIndex = 0;
            //ucSite.innerControlddlSite.AutoPostBack = true;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //ucSite.CurrentPage = this;
        ucCountry.CurrentPage = this;
        ucCountry.IsAllRequired = true;
        msVendor.IsConsolidatedVendorRequired = false;
        btnExportToExcel1.GridViewControl = gvVendorExportExcel;
        btnExportToExcel1.FileName = "VendorOverview";
        Label lblModuleText = (Label)Page.Master.FindControl("lblModuleText");
        lblModuleText.Text = "Global Setting-Vendor Setup";
    }

    protected void Page_Load(object sender, EventArgs e)
    {        

    }    

    protected override void OnPreRender(EventArgs e)
    {
        if (!IsPostBack)
        {
            GetReadOnlyModule();
            //ucCountry.innerControlddlCountry.Items.Insert(0, new ListItem("--Select--", "-1"));
            //ucCountry.innerControlddlCountry.SelectedIndex = 1;
            //ucSite.innerControlddlSite.Items.Insert(0, new ListItem("--Select--", "-1"));
            //ucSite.innerControlddlSite.SelectedIndex = 0;
            BindGrid();
        }
        base.OnPreRender(e);
    }

    protected void gvVendorOverview_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName == "SAVE")
        //{
        //    GridViewRow row = (GridViewRow)((Button)e.CommandSource).NamingContainer;
        //    DropDownList ddl = (DropDownList)row.FindControl("ddlActiveVendor1");
        //    var ddlvalue = ddl.SelectedItem.Value;
        //    ucDropdownList ddlEuropeanorLocal = (ucDropdownList)row.FindControl("ddlEuropeanorLocal");

        //    MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
        //    MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();
        //    List<MAS_VendorBE> lstVendor = new List<MAS_VendorBE>();

        //    oMAS_VendorBE.Action = "UPDATE";
        //    oMAS_VendorBE.VendorID = Convert.ToInt32(e.CommandArgument);
        //    oMAS_VendorBE.IsActiveVendor = ddlvalue;
        //    //------Stage 7 Point 1------//
        //    oMAS_VendorBE.EuropeanorLocal = Convert.ToInt32(ddlEuropeanorLocal.SelectedItem.Value);
            //--------------------------//

            //if (row.Cells[5].Text == "Y" && ddlvalue == "N")
            //{
            //    //hfIsActiveVendor.Value = oMAS_VendorBE.IsActiveVendor.ToString();
            //    //hfVendorID.Value = oMAS_VendorBE.VendorID.ToString();
            //    //ltNoShow.Text = "Contact is Defined for this Vendor.Are you sure to Deactivate?";
            //    //mdNoShow.Show();
            //    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "continuePursing", "return continuePursing('UPDATE','" + oMAS_VendorBE.IsActiveVendor.ToString() + "','" + oMAS_VendorBE.VendorID.ToString() + "');", true);
            //    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "message", "confirm('Contact is Defined for this Vendor.Are you sure to Deactivate?')", true);
            //    //return;
            //}
            //else
        //    oMAS_VendorBAL.UpdateVendorBAL(oMAS_VendorBE);
        //}
    }

    protected void gvVendorOverview_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //    //MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
            //    //MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();
            //    DropDownList ddl = (DropDownList)e.Row.FindControl("ddlActiveVendor1");
            //    Button btn = (Button)e.Row.FindControl("btnSaveVendor");
            //    HiddenField hdnIsActiveVendor = (HiddenField)e.Row.FindControl("hdnIsActiveVendor");
            //    ucLabel lblContactDefinedVO = (ucLabel)e.Row.FindControl("lblContactDefinedVO");

            //    //------Stage 7 Point 1------//
            //    ucDropdownList ddlEuropeanorLocal = (ucDropdownList)e.Row.FindControl("ddlEuropeanorLocal");
            //    HiddenField hdnEuropeanorLocal = (HiddenField)e.Row.FindControl("hdnEuropeanorLocal");
            //    if (!string.IsNullOrWhiteSpace(hdnEuropeanorLocal.Value))
            //        ddlEuropeanorLocal.SelectedIndex = ddlEuropeanorLocal.Items.IndexOf(ddlEuropeanorLocal.Items.FindByValue(hdnEuropeanorLocal.Value));
            //    else
            //        ddlEuropeanorLocal.SelectedIndex = 1;
            //    //-----------------------------//

            //    //oMAS_VendorBE.Action = "ShowMaster";
            //    //oMAS_VendorBE.VendorIDs = btn.CommandArgument;
            //    //oMAS_VendorBE = oMAS_VendorBAL.GetVendorBALByID(oMAS_VendorBE);

            //    if (!string.IsNullOrWhiteSpace(hdnIsActiveVendor.Value))
            //        ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(hdnIsActiveVendor.Value));
            //    else
            //        ddl.SelectedIndex = 0;



            //    ddl.Attributes.Add("onchange", "return myFunction();");

            //if (lblContactDefinedVO.Text == "Y" && ddl.SelectedItem.Value == "Y")
            //    btn.Attributes.Add("onclick", "return continuePurzing('" + ddl.SelectedItem.Value + "');");
            if (ViewState["ReadOnly"] != null && Convert.ToBoolean(ViewState["ReadOnly"]) == true)
            {
                //if (gvVendorOverview.HeaderRow != null)
                //{
                //    gvVendorOverview.HeaderRow.Cells[7].Visible = false;
                //    gvVendorOverview.HeaderRow.Cells[8].Visible = false;
                //    e.Row.Cells[7].Visible = false;
                //    e.Row.Cells[8].Visible = false;
                //}

                HyperLink hplink = (HyperLink)e.Row.FindControl("VendorLink");
                hplink.NavigateUrl = EncryptQuery("VendorEdit.aspx?VendorID=" + DataBinder.Eval(e.Row.DataItem, ("VendorID")) + "&Readonly=1");
            }

        }
    }

    protected void gvVendorExportExcel_RowDataBound(object sender, GridViewRowEventArgs e) {
        //if (e.Row.RowType == DataControlRowType.DataRow) {
        //    ucLabel ltEuropeanorLocal = (ucLabel)e.Row.FindControl("ltEuropeanorLocal");
        //    //------Stage 7 Point 1------//
        //    if (ltEuropeanorLocal.Text == "1")
        //        ltEuropeanorLocal.Text = "European";
        //    else
        //        ltEuropeanorLocal.Text = "Local";
        //    //-----------------------------//         
        //    if (ViewState["ReadOnly"] != null && Convert.ToBoolean(ViewState["ReadOnly"]) == true)
        //    {
        //        if (gvVendorExportExcel.HeaderRow != null)
        //        {
        //            gvVendorOverview.HeaderRow.Cells[7].Visible = false;
        //            gvVendorOverview.HeaderRow.Cells[8].Visible = false;
        //            e.Row.Cells[7].Visible = false;
        //            e.Row.Cells[8].Visible = false;
        //        }          
        //    }
        //}
    }

    protected void gvVendorOverview_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvVendorOverview.PageIndex = e.NewPageIndex;
        //this.BindGrid();
        SortGridView(GridViewSortExp,GridViewSortDirection);
    }

    protected void UcButtonShow_Click(object sender, EventArgs e)
    {
        //BindGrid();
        MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
        MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();
        List<MAS_VendorBE> lstVendor = new List<MAS_VendorBE>();

        oMAS_VendorBE.Action = "ShowMaster";
        // if (ucCountry.innerControlddlCountry.SelectedItem.Value > 0)
        oMAS_VendorBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);

        //if (ddlRegistrationStatus.SelectedValue != "--Select--")
        //    oMAS_VendorBE.RegistrationStatus = ddlRegistrationStatus.SelectedValue;

        //if (ucSite.innerControlddlSite.SelectedIndex > 0)
        //    oMAS_VendorBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);

        #region Vendor search related ..
        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            oMAS_VendorBE.VendorIDs = msVendor.SelectedVendorIDs;

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorName))
            oMAS_VendorBE.Vendor_Name = msVendor.SelectedVendorName;

        TextBox txtSearchedVendor = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;
        if (txtSearchedVendor != null)
            oMAS_VendorBE.SearchedVendorText = txtSearchedVendor.Text;
        #endregion

        msVendor.setVendorsOnPostBack();

        //if (ddlActiveVendor.SelectedIndex > 0)
        //    oMAS_VendorBE.IsActiveVendor = ddlActiveVendor.SelectedItem.Value;

        //------Stage 7 Point 1------//
        //if (ddlEuroOrLoc.SelectedIndex > 0)
        //    oMAS_VendorBE.EuropeanorLocal = Convert.ToInt32(ddlEuroOrLoc.SelectedItem.Value);
        //---------------------------//

        if (GetQueryStringValue("VendorsMissingDetails") != null && GetQueryStringValue("VendorsMissingDetails").ToString() == "Y")
            oMAS_VendorBE.VendorsMissingDetails = 'Y';

        lstVendor = oMAS_VendorBAL.GetVendorBAL(oMAS_VendorBE);
        oMAS_VendorBAL = null;
        for (int i = 0; i < lstVendor.Count; i++)
        {
            if (!string.IsNullOrEmpty(lstVendor[i].VendorSite) && lstVendor[i].VendorSite.IndexOf(",") > -1)
            {
                lstVendor[i].VendorSite = lstVendor[i].VendorSite.Replace(",", "<br/>");
            }
        }
        gvVendorOverview.PageIndex = 0;
        gvVendorOverview.DataSource = lstVendor;
        gvVendorOverview.DataBind();
        gvVendorExportExcel.DataSource = lstVendor;
        gvVendorExportExcel.DataBind();
        ViewState["lstSites"] = lstVendor;

       
        GridViewSortExp = "Vendor_No";
        GridViewSortDirection = SortDirection.Ascending;

        Session["VendorSetupSearch"] = oMAS_VendorBE;

        GlobalResourceFields(this.Page);
    }    

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {

        GridViewSortExp = e.SortExpression;

        if (SortDir == "ASC") {
            GridViewSortDirection = SortDirection.Ascending;           
        }
        else {
            GridViewSortDirection = SortDirection.Descending;           
        }

        return Utilities.GenericListHelper<MAS_VendorBE>.SortList((List<MAS_VendorBE>)ViewState["lstSites"], e.SortExpression, GridViewSortDirection).ToArray();
    }

    private void SortGridView(string sortExpression, SortDirection direction) {
        List<MAS_VendorBE> lstVendor = new List<MAS_VendorBE>();
        try {
            if (ViewState["lstSites"] != null) {

                lstVendor = Utilities.GenericListHelper<MAS_VendorBE>.SortList((List<MAS_VendorBE>)ViewState["lstSites"], sortExpression, direction);

                gvVendorOverview.DataSource = lstVendor;
                gvVendorOverview.DataBind();

                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(gvVendorOverview);
            }
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }

    #endregion

    #region Methods ...

    private void BindGrid()
    {
        MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
        //MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();
        List<MAS_VendorBE> lstVendor = new List<MAS_VendorBE>();

        if (Session["VendorSetupSearch"] != null)
        {
            oMAS_VendorBE = (MAS_VendorBE)Session["VendorSetupSearch"];

            #region Setting controls of Searched Criteria ..

            /* Country .. */
            if (ucCountry.innerControlddlCountry.Items.Count > 0 && oMAS_VendorBE.CountryID != null)
            {
                ucCountry.innerControlddlCountry.SelectedIndex =
                    ucCountry.innerControlddlCountry.Items.IndexOf(ucCountry.innerControlddlCountry.Items.FindByValue(
                    Convert.ToString(oMAS_VendorBE.CountryID)));
            }

            /* Select Site .. */
            //if (ucSite.innerControlddlSite.Items.Count > 0 && oMAS_VendorBE.SiteID != null)
            //{
            //    ucSite.innerControlddlSite.SelectedIndex = ucSite.innerControlddlSite.Items.IndexOf(
            //        ucSite.innerControlddlSite.Items.FindByValue(Convert.ToString(oMAS_VendorBE.SiteID)));
            //}

            /* Registration Status .. */
            //if (ddlRegistrationStatus.Items.Count > 0 && !string.IsNullOrEmpty(oMAS_VendorBE.RegistrationStatus))
            //{
            //    ddlRegistrationStatus.SelectedIndex = ddlRegistrationStatus.Items.IndexOf(
            //        ddlRegistrationStatus.Items.FindByValue(oMAS_VendorBE.RegistrationStatus));
            //}

            /* Active Vendor .. */
            //if (ddlActiveVendor.Items.Count > 0 && !string.IsNullOrEmpty(oMAS_VendorBE.IsActiveVendor))
            //{
            //    ddlActiveVendor.SelectedIndex = ddlActiveVendor.Items.IndexOf(
            //        ddlActiveVendor.Items.FindByValue(oMAS_VendorBE.IsActiveVendor));
            //}

            /* Vendor .. */
            HiddenField hiddenSelectedIDs = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
            if (hiddenSelectedIDs != null && !string.IsNullOrEmpty(oMAS_VendorBE.VendorIDs))
                hiddenSelectedIDs.Value = oMAS_VendorBE.VendorIDs;

            HiddenField hiddenSelectedName = msVendor.FindControl("hiddenSelectedName") as HiddenField;
            if (hiddenSelectedName != null && !string.IsNullOrEmpty(oMAS_VendorBE.Vendor_Name))
                hiddenSelectedName.Value = oMAS_VendorBE.Vendor_Name;

            ucListBox lstRight = msVendor.FindControl("lstRight") as ucListBox;
            ucListBox lstLeft = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
            if (lstLeft != null && lstRight != null && !string.IsNullOrEmpty(oMAS_VendorBE.SearchedVendorText) 
                && !string.IsNullOrEmpty(oMAS_VendorBE.VendorIDs))
            {
                lstLeft.Items.Clear();
                lstRight.Items.Clear();
                msVendor.SearchVendorClick(oMAS_VendorBE.SearchedVendorText);

                string[] strVendorIDs = oMAS_VendorBE.VendorIDs.Split(',');
                for (int index = 0; index < strVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeft.Items.FindByValue(strVendorIDs[index]);
                    if (listItem != null)
                    {
                        lstRight.Items.Add(listItem);
                        lstLeft.Items.Remove(listItem);
                    }
                }
            }

            #endregion
        }
        else
        {
            oMAS_VendorBE.Action = "ShowMaster";
            // if (ucCountry.innerControlddlCountry.SelectedItem.Value > 0)
            oMAS_VendorBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);

            //if (ddlRegistrationStatus.SelectedValue != "--Select--")
            //    oMAS_VendorBE.RegistrationStatus = ddlRegistrationStatus.SelectedValue;

            //if (ucSite.innerControlddlSite.SelectedIndex > 0)
            //    oMAS_VendorBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);

            #region Vendor search related ..
            if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
                oMAS_VendorBE.VendorIDs = msVendor.SelectedVendorIDs;

            if (!string.IsNullOrEmpty(msVendor.SelectedVendorName))
                oMAS_VendorBE.Vendor_Name = msVendor.SelectedVendorName;

            TextBox txtSearchedVendor = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;
            if (txtSearchedVendor != null)
                oMAS_VendorBE.SearchedVendorText = txtSearchedVendor.Text;
            #endregion

            //if (ddlActiveVendor.SelectedIndex > 0)
            //    oMAS_VendorBE.IsActiveVendor = ddlActiveVendor.SelectedItem.Value;

            //------Stage 7 Point 1------//
            //if (ddlEuroOrLoc.SelectedIndex > 0)
            //    oMAS_VendorBE.EuropeanorLocal = Convert.ToInt32(ddlEuroOrLoc.SelectedItem.Value);
            //---------------------------//

            if (GetQueryStringValue("VendorsMissingDetails") != null && GetQueryStringValue("VendorsMissingDetails").ToString() == "Y")
                oMAS_VendorBE.VendorsMissingDetails = 'Y';
        }

        lstVendor  = GetVendor(oMAS_VendorBE);

        gvVendorOverview.DataSource = lstVendor;
        gvVendorOverview.DataBind();
        gvVendorExportExcel.DataSource = lstVendor;
        gvVendorExportExcel.DataBind();


        ViewState["lstSites"] = lstVendor;
        //Session["VendorSetupSearch"] = oMAS_VendorBE;
    }

    //Add By Abhinav -- 17 July 2013 -- To get Vendors from existing Viewstste
    private List<MAS_VendorBE>  GetVendor(MAS_VendorBE oMAS_VendorBE) {
        MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();
        List<MAS_VendorBE> lstVendor = new List<MAS_VendorBE>();

        if (ViewState["lstSites"] == null) {
            lstVendor = oMAS_VendorBAL.GetVendorBAL(oMAS_VendorBE);
            oMAS_VendorBAL = null;
            for (int i = 0; i < lstVendor.Count; i++) {
                if (!string.IsNullOrEmpty(lstVendor[i].VendorSite) && lstVendor[i].VendorSite.IndexOf(",") > -1) {
                    lstVendor[i].VendorSite = lstVendor[i].VendorSite.Replace(",", "<br/>");
                }
            }
        }
        else {
            lstVendor = (List<MAS_VendorBE>)ViewState["lstSites"];
        }
        return lstVendor;
    }
    //----------------------------//

    public void GetReadOnlyModule()
    {
        SCT_ModuleBAL oSCT_ModuleBAL = new SCT_ModuleBAL();
        SCT_ModuleBE oSCT_ModuleBE = new SCT_ModuleBE();

        oSCT_ModuleBE.Action = "GetReadOnlyModule";
        oSCT_ModuleBE.ScreenName = "VendorSetUp";
        DataTable dt = oSCT_ModuleBAL.GetReadOnlyModuleBAL(oSCT_ModuleBE);
        if (dt.Rows.Count > 0)
        {
            ViewState["ReadOnly"] = null;
        }
        else
        {
            ViewState["ReadOnly"] = true;
        }
    }

    #endregion
    
}
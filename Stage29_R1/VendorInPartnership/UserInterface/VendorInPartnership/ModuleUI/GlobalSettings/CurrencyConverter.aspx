﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="CurrencyConverter.aspx.cs" Inherits="CurrencyConverter" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblCurrencyConverter" runat="server" Text="Currency Converter"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="UcAddButton1" runat="server" NavigateUrl="CurrencyConvertorEdit.aspx" />
        <cc1:ucButton ID="btnExportToExcel" runat="server" OnClick="btnExport_Click" CssClass="exporttoexcel"
            Width="109px" Height="20px" />
    </div>
    <table class="top-settings" width="75%" id="tblHeader" runat="server">
        <tr>
            <td style="font-weight: bold;" align="center">
                <table width="100%" cellspacing="5" cellpadding="0" border="0">
                    <tr>
                        <td style="font-weight: bold;" class="style1">
                            <cc1:ucLabel ID="lblCurrency" runat="server" Text="Currency"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" class="style1">
                            :
                        </td>
                        <td style="font-weight: bold;" align="left" class="style1">
                            <cc1:ucDropdownList ID="ddlCurrency" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged">
                            </cc1:ucDropdownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table border="0" cellspacing="0" cellpadding="0" id="tblExport" width="100%" runat="server">
        <tr>
            <td>
                <cc1:ucPanel ID="pnlCurrent" runat="server" CssClass="fieldset-form">
                    <cc1:ucGridView ID="gvCurrentCurrency" Width="100%" runat="server" CssClass="grid"
                        OnSorting="SortGrid" AllowSorting="false">
                        <AlternatingRowStyle BackColor="#FFFFFF" ForeColor="#284775"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField HeaderText="Currency" SortExpression="Currency">
                                <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpCurrency" runat="server" Text='<%# Eval("Currency") %>' NavigateUrl='<%# EncryptQuery("CurrencyConvertorEdit.aspx?CurrencyConverterID="+ Eval("CurrencyConverterID")) %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Convert To" ItemStyle-Wrap="false" DataField="CurrencyConvertTo"
                                SortExpression="CurrencyConvertTo">
                                <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Exchange Rate" ItemStyle-Wrap="false" DataField="ExchangeRate"
                                SortExpression="ExchangeRate">
                                <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Date Set">
                                <ItemTemplate>
                                    <cc1:ucLabel ID="lbl_Date" runat="server" Text='<%#Eval("DateApplied", "{0:dd/MM/yyyy hh:mm}")%>'></cc1:ucLabel>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User">
                                <ItemTemplate>
                                    <cc1:ucLabel ID="lbl_user" runat="server" Text='<%# Eval("UserName") %>'></cc1:ucLabel>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                            </asp:TemplateField>
                        </Columns>
                    </cc1:ucGridView>
                </cc1:ucPanel>
            </td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <cc1:ucPanel ID="pnlPrevious" runat="server" CssClass="fieldset-form">
                    <cc1:ucGridView ID="gvPreviousCurrency" Width="100%" runat="server" CssClass="grid"
                        OnSorting="SortGrid" AllowSorting="false" AutoGenerateColumns="False" CellPadding="0"
                        GridLines="None">
                        <AlternatingRowStyle BackColor="#FFFFFF" ForeColor="#284775"></AlternatingRowStyle>
                        <Columns>
                            <asp:BoundField HeaderText="Currency" ItemStyle-Wrap="false" DataField="Currency"
                                SortExpression="Currency">
                                <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Convert To" ItemStyle-Wrap="false" DataField="CurrencyConvertTo"
                                SortExpression="CurrencyConvertTo">
                                <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Exchange Rate" ItemStyle-Wrap="false" DataField="ExchangeRate"
                                SortExpression="ExchangeRate">
                                <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Date Set">
                                <ItemTemplate>
                                    <cc1:ucLabel ID="lbl_Date" runat="server" Text='<%#Eval("DateApplied", "{0:dd/MM/yyyy hh:mm}")%>'></cc1:ucLabel>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User">
                                <ItemTemplate>
                                    <cc1:ucLabel ID="lbl_user" runat="server" Text='<%# Eval("UserName") %>'></cc1:ucLabel>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                            </asp:TemplateField>
                        </Columns>
                    </cc1:ucGridView>
                </cc1:ucPanel>
            </td>
        </tr>
    </table>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using Utilities;
public partial class ODVatCodeOverview : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        if (!IsPostBack)
        {
            ucExportToExcel1.Visible = false;
            BindODVAtCode();
        }
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvODVatCode;
        ucExportToExcel1.FileName = "OfficeDepotVatCode";
    }


    private void BindODVAtCode()
    {
        try
        {
            MAS_VatCodeBAL oMAS_VatCodeBAL = new MAS_VatCodeBAL();

            MAS_VatCodeBE oMAS_VatCodeBE = new MAS_VatCodeBE();
            oMAS_VatCodeBE.Action = "ShowAllODVatCode";


            List<MAS_VatCodeBE> lstMAS_VatCodeBE = oMAS_VatCodeBAL.GetODVatCodeBAL(oMAS_VatCodeBE);

            if (lstMAS_VatCodeBE != null && lstMAS_VatCodeBE.Count > 0)
            {
                ucExportToExcel1.Visible = true;
                gvODVatCode.DataSource = lstMAS_VatCodeBE;
                ViewState["lstMAS_VatCodeBE"] = lstMAS_VatCodeBE;
            }
            else
            {
                oMAS_VatCodeBAL = null;
                ucExportToExcel1.Visible = false;
            }

            gvODVatCode.DataBind();
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MAS_VatCodeBE>.SortList((List<MAS_VatCodeBE>)ViewState["lstMAS_VatCodeBE"], e.SortExpression, e.SortDirection).ToArray();
    }
}
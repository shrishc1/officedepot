﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TradeEntitiesOverview.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
 Inherits="ModuleUI_GlobalSettings_TradeEntitiesOverview" %>



<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblTradeEntities" runat="server" Text="Carrier Setup"></cc1:ucLabel>
    </h2>
    <div class="button-row">
    <cc1:ucButton ID="btnAdd" runat="server" CssClass="add" onclick="btnAdd_Click" Width="40px" />
       <%-- <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="TradeEntitiesEdit.aspx" />--%>
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center">
                <cc1:ucGridView ID="gvTradeEntitiesOverview" Width="100%" runat="server" CssClass="grid" OnRowDataBound="gvTradeEntitiesOverview_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>                                
                                 <asp:HyperLink ID="hpTradeEntitiesID" runat="server" Text='<%# Eval("RowNo") %>'  NavigateUrl='<%# EncryptQuery("TradeEntitiesEdit.aspx?TradeEntitiesID="+ Eval("TradeEntitiesID")+"&RecordNo="+Eval("RowNo")) %>'></asp:HyperLink>
                            </ItemTemplate>                           
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Area">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                            <cc1:ucLiteral ID="ltArea" runat="server" Text='<%# Eval("Area") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="OD-Country">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltODCountry" runat="server" Text='<%# Eval("OD_Country") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Send pdf invoices to">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSendPdfInvoicesTo" runat="server" Text='<%# Eval("SendPdfInvoicesTo") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Company">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltCompany" runat="server" Text='<%# Eval("Company") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Invoice-address">
                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltInvoiceAddress" runat="server" Text='<%# Eval("InvoiceAddress") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VAT #">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltVATNo" runat="server" Text='<%# Eval("VAT_No") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CoC #">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltCOCNo" runat="server" Text='<%# Eval("COC_No") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltType" runat="server" Text='<%# Eval("Type") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Example-Order Nr.">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltExampleOrderNo" runat="server" Text='<%# Eval("ExampleOrderNo").ToString().Replace(",","<br />") %>'></cc1:ucLiteral>                                
                            </ItemTemplate>

                        </asp:TemplateField>
                        
                    </Columns>
                </cc1:ucGridView>
                 <cc1:ucGridView ID="gvExport" runat="server" AutoGenerateColumns="false" Style="display: none;">
                        <Columns>
                        <asp:TemplateField HeaderText="Record #">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>                                
                                 <asp:HyperLink ID="hpTradeEntitiesID" runat="server" Text='<%# Eval("RowNo") %>'  NavigateUrl='<%# EncryptQuery("TradeEntitiesEdit.aspx?TradeEntitiesID="+ Eval("TradeEntitiesID")) %>'></asp:HyperLink>
                            </ItemTemplate>                           
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Area">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                            <cc1:ucLiteral ID="ltArea" runat="server" Text='<%# Eval("Area") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="OD-Country">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltODCountry" runat="server" Text='<%# Eval("OD_Country") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Send pdf invoices to">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSendPdfInvoicesTo" runat="server" Text='<%# Eval("SendPdfInvoicesTo") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Company">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltCompany" runat="server" Text='<%# Eval("Company") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Invoice-address">
                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltInvoiceAddress" runat="server" Text='<%# Eval("InvoiceAddress") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VAT #">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltVATNo" runat="server" Text='<%# Eval("VAT_No") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CoC #">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltCOCNo" runat="server" Text='<%# Eval("COC_No") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltType" runat="server" Text='<%# Eval("Type") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Example-Order Nr.">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltExampleOrderNo" runat="server" Text='<%# Eval("ExampleOrderNo").ToString().Replace(",","<br />") %>'></cc1:ucLiteral>
                            </ItemTemplate>

                        </asp:TemplateField>
                        
                    </Columns>
                    </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BaseControlLibrary;
using System.Collections;

public partial class ModuleUI_GlobalSettings_OverstockAgreementView : CommonPage
{
    private bool IsGoClicked = false;

    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected override void OnPreRender(EventArgs e)
    {       
        if (!IsPostBack)
        {
            if (GetQueryStringValue("PageFrom") == null)
            {
                 BindGrid();
            }

            if (Session["OverstockAgreementSearch"] != null)
            {
                if (IsGoClicked == false)
                {
                    GetSession();
                    RetainSearchData();
                }
            }
        }
        base.OnPreRender(e);
    }

    protected void Page_Init(object sender, EventArgs e)
    {        
        ucCountry.CurrentPage = this;
        ucCountry.IsAllRequired = true;
        btnExportToExcel1.CurrentPage = this;
        btnExportToExcel1.GridViewControl = gvExport;
        btnExportToExcel1.FileName = "Overstock Agreement View";
    }

    private void BindGrid()
    {
        MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
        MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();
        List<MAS_VendorBE> lstVendor = new List<MAS_VendorBE>();
       
        oMAS_VendorBE.Action = "GetOverstockAgreementView";

       
        oMAS_VendorBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);

        #region Vendor search related ..
        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            oMAS_VendorBE.VendorIDs = msVendor.SelectedVendorIDs;


        if (!string.IsNullOrEmpty(msVendor.SelectedVendorName))
            oMAS_VendorBE.Vendor_Name = msVendor.SelectedVendorName;

        TextBox txtSearchedVendor = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;
        if (txtSearchedVendor != null)
            oMAS_VendorBE.SearchedVendorText = txtSearchedVendor.Text;
        #endregion

        if (rdoAgreementinPlace.Checked)
        {
            oMAS_VendorBE.AgreementType = "AgreementinPlace";
        }
        else if (rdoNoAgreement.Checked)
        {
            oMAS_VendorBE.AgreementType = "NoAgreement";
        }
        else if (rdoAllVendors.Checked)
        {
            oMAS_VendorBE.AgreementType = "All";
        }

        this.SetSession(oMAS_VendorBE);

        lstVendor = oMAS_VendorBAL.GetOverstockReturnsAgreementOverviewBAL(oMAS_VendorBE);
        oMAS_VendorBAL = null;

        gvOverstockAgreementOverview.PageIndex = 0;
        gvOverstockAgreementOverview.DataSource = lstVendor;
        gvOverstockAgreementOverview.DataBind();
        gvExport.DataSource = lstVendor;
        gvExport.DataBind();

        if (lstVendor.Count > 0)
        {
            btnExportToExcel1.Visible = true;
        }
        else
        {
            btnExportToExcel1.Visible = false;
        }

        ViewState["lstSites"] = lstVendor;
      
    }

    protected void gvOverstockAgreementOverview_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvOverstockAgreementOverview.PageIndex = e.NewPageIndex;
        gvOverstockAgreementOverview.DataSource = ViewState["lstSites"];
        gvOverstockAgreementOverview.DataBind();

    }

    protected void UcButtonShow_Click(object sender, EventArgs e)
    {

        IsGoClicked = true;
        BindGrid();
        RetainSearchData();   
    }

    private void SetSession(MAS_VendorBE oMAS_VendorBE)
    {
        Session.Remove("OverstockAgreementSearch");
        Session["OverstockAgreementSearch"] = null;

        TextBox txtVendor = (TextBox)msVendor.FindControl("ucVendor").FindControl("txtVendorNo");

        Hashtable htOverstockAgreementView = new Hashtable();
       
        htOverstockAgreementView.Add("Vendor_Name", oMAS_VendorBE.Vendor_Name);
        htOverstockAgreementView.Add("CountryID", oMAS_VendorBE.CountryID);
        htOverstockAgreementView.Add("SelectedVendorIDs", oMAS_VendorBE.VendorIDs);
        htOverstockAgreementView.Add("txtVendor", txtVendor.Text);
        htOverstockAgreementView.Add("IsSearchedByVendorNo", msVendor.IsSearchedByVendorNo);
        htOverstockAgreementView.Add("SelectedVendorName", msVendor.SelectedVendorName); 

        htOverstockAgreementView.Add("SearchedVendorText", oMAS_VendorBE.SearchedVendorText);
        htOverstockAgreementView.Add("AgreementType", oMAS_VendorBE.AgreementType);

        Session["OverstockAgreementSearch"] = htOverstockAgreementView;
    }


    private void GetSession()
    {
        MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
        MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();
        List<MAS_VendorBE> lstVendor = new List<MAS_VendorBE>();

        if (Session["OverstockAgreementSearch"] != null)
        {
            Hashtable htOverstockAgreementView = (Hashtable)Session["OverstockAgreementSearch"];
            
            oMAS_VendorBE.CountryID = (htOverstockAgreementView.ContainsKey("CountryID") && htOverstockAgreementView["CountryID"] != null) ? Convert.ToInt32(htOverstockAgreementView["CountryID"].ToString()) : 0;

            ucCountry.innerControlddlCountry.SelectedIndex = ucCountry.innerControlddlCountry.Items.IndexOf(ucCountry.innerControlddlCountry.Items.FindByValue(htOverstockAgreementView["CountryID"].ToString()));


            oMAS_VendorBE.VendorIDs = (htOverstockAgreementView.ContainsKey("SelectedVendorIDs") && htOverstockAgreementView["SelectedVendorIDs"] != null) ? htOverstockAgreementView["SelectedVendorIDs"].ToString() : null;

            oMAS_VendorBE.AgreementType = (htOverstockAgreementView.ContainsKey("AgreementType") && htOverstockAgreementView["AgreementType"] != null) ? htOverstockAgreementView["AgreementType"].ToString() : null;


            //if (htOverstockAgreementView["SelectedVendorIDs"] != null)
            //{
            //    msVendor.SelectedVendorIDs = htOverstockAgreementView["SelectedVendorIDs"].ToString();
            //    msVendor.SelectedVendorName = htOverstockAgreementView["Vendor_Name"].ToString();
            //   // msVendor.setVendorsOnPostBack();
            //}

            oMAS_VendorBE.Action = "GetOverstockAgreementView";

            lstVendor = oMAS_VendorBAL.GetOverstockReturnsAgreementOverviewBAL(oMAS_VendorBE);

            if (lstVendor != null && lstVendor.Count > 0)
            {
                gvOverstockAgreementOverview.DataSource = lstVendor;
                gvExport.DataSource = lstVendor;               
                         
            }
            else
            {
                gvOverstockAgreementOverview.DataSource = null;
                gvExport.DataSource = null;
            }

            gvOverstockAgreementOverview.DataBind();
            gvExport.DataBind();

        }
    }

    private void RetainSearchData()
    {

        Hashtable htOverstockAgreementView = (Hashtable)Session["OverstockAgreementSearch"];
            
        // ********** Vendor ***************
        string txtVendor = (htOverstockAgreementView.ContainsKey("txtVendor") && htOverstockAgreementView["txtVendor"] != null) ? htOverstockAgreementView["txtVendor"].ToString() : "";
        string VendorId = (htOverstockAgreementView.ContainsKey("SelectedVendorIDs") && htOverstockAgreementView["SelectedVendorIDs"] != null) ? htOverstockAgreementView["SelectedVendorIDs"].ToString() : "";
        TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;

        bool IsSearchedByVendorNo = (htOverstockAgreementView.ContainsKey("IsSearchedByVendorNo") && htOverstockAgreementView["IsSearchedByVendorNo"] != null) ? Convert.ToBoolean(htOverstockAgreementView["IsSearchedByVendorNo"]) : false;
        string txtVendorIdText = txtVendorId.Text;
        ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
        string strVendorId = string.Empty;
        int value;
        lstLeftVendor.Items.Clear();
        if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(VendorId) || !string.IsNullOrEmpty(VendorId)))
        {
            lstLeftVendor.Items.Clear();
            lstRightVendor.Items.Clear();

            if (IsSearchedByVendorNo == true)
            {
                msVendor.SearchVendorNumberClick(txtVendor);
                //parsing successful 
            }
            else
            {
                msVendor.SearchVendorClick(txtVendor);
                //parsing failed. 
            }

            if (!string.IsNullOrEmpty(VendorId))
            {
                string[] strIncludeVendorIDs = VendorId.Split(',');
                for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                    if (listItem != null)
                    {

                        strVendorId += strIncludeVendorIDs[index] + ",";
                        lstRightVendor.Items.Add(listItem);
                        lstLeftVendor.Items.Remove(listItem);
                    }
                }
            }

            HiddenField hdnSelectedVendor = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedVendor.Value = strVendorId;
        }
        else
        {
            lstRightVendor.Items.Clear();
        }

        string AgreementType = (htOverstockAgreementView.ContainsKey("AgreementType") && htOverstockAgreementView["AgreementType"] != null) ? htOverstockAgreementView["AgreementType"].ToString() : "";

        if(AgreementType.Equals("AgreementinPlace"))
        {
            rdoAgreementinPlace.Checked = true;
        }
        else if(AgreementType.Equals("NoAgreement"))
        {
            rdoNoAgreement.Checked = true;
        }
        else if (AgreementType.Equals("All"))
        {
            rdoAllVendors.Checked = true;
        }

    }
}



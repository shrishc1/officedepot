﻿<%@ Page Language="C#" AutoEventWireup="true"
    CodeFile="DiscrepancyTrackerEmailEddit.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    Inherits="ModuleUI_GlobalSettings_DiscrepancyTrackerEmailEddit" %>


<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
    </script>
    <h2>
        <cc1:ucLabel ID="lblDiscreapncyEscalationEmailRecipients" Text="Discreapncy Escalation Email Recipients" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div>
            <asp:RequiredFieldValidator ID="rfvCarrierRequired" runat="server"
                ControlToValidate="txtEmail"
                Display="None"
                ValidationGroup="a">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server"
                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                ControlToValidate="txtEmail"
                ErrorMessage="Invalid Email Format"
                ValidationGroup="a"></asp:RegularExpressionValidator>
            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                Style="color: Red" ValidationGroup="a" />
        </div>
        <div class="formbox">
            <table width="65%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td></td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblEmail" runat="server" Text="Carrier" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" align="center">:
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtEmail" runat="server" Width="340px" MaxLength="50"></cc1:ucTextbox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%"></td>
                    <td style="font-weight: bold;" width="28%">
                        <cc1:ucLabel ID="lblType" runat="server" Text="Type" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" width="5%" align="center">:
                    </td>
                    <td style="font-weight: bold;" width="57%">
                        <cc1:ucDropdownList ID="ddlType" runat="server" Width="130px">
                            <asp:ListItem Text="TO" Value="TO"></asp:ListItem>
                            <asp:ListItem Text="CC" Value="CC"></asp:ListItem>
                            <asp:ListItem Text="BCC" Value="BCC"></asp:ListItem>
                        </cc1:ucDropdownList>
                    </td>
                    <td style="width: 5%"></td>
                </tr>

            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSubmit" runat="server" CssClass="button" OnClick="btnSave_Click"
            ValidationGroup="a" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click"
            OnClientClick="return confirmDelete();" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

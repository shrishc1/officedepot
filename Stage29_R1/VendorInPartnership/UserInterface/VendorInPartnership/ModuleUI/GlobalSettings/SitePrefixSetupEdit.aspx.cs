﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;

using Utilities;
using WebUtilities;
public partial class SitePrefixSetupEdit : CommonPage {

    protected string UnDeclaredAsBannerMessage = WebCommon.getGlobalResourceValue("UnDeclaredAsBannerMessage");

    protected void Page_Init(object sender, EventArgs e) {
        ucSiteCountry.CurrentPage = this;
        ucAPCountry.CurrentPage = this;
    }
    public int? CountryIdSC
    { get { return ViewState["CountryIdSC"] !=null ? Convert.ToInt32(ViewState["CountryIdSC"]) :0; }
        set { ViewState["CountryIdSC"] = value; } }
    public int? CountryIdAP
    {
        get { return ViewState["CountryIdAP"] != null ? Convert.ToInt32(ViewState["CountryIdAP"]) : 0; }
        set { ViewState["CountryIdAP"] = value; }
    }
    protected void Page_Prerender(object sender, EventArgs e) {
        if (!IsPostBack) {
            bindSiteList();
            bindManager();
            //---Stage 11 R3 Point 17---//
            GetCurrencyType();
            //-------------------------//
            getSiteDetail();            
        }
    }

    protected void Page_Load(object sender, EventArgs e) {
        txtSiteName.Focus();
    }

    #region Methods

    private void GetCurrencyType() {
        CurrencyConverterBAL o_currencyBAL = new CurrencyConverterBAL();
        CurrencyConverterBE o_CurrencyBE = new CurrencyConverterBE();

        o_CurrencyBE.Action = "ShowAllCurrency";
        List<CurrencyConverterBE> lstCurrencyNamesBE = o_currencyBAL.BindCurrencyddlBAL(o_CurrencyBE);

        if (lstCurrencyNamesBE.Count > 0 && lstCurrencyNamesBE != null) {
            FillControls.FillDropDown(ref ddlCurrency, lstCurrencyNamesBE, "Currency", "CurrencyID", "--Select--");
        }       
    }

    //Binds only at Add mode
    private void bindSiteList() {
        if (GetQueryStringValue("SiteID") == null) {
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();
            oMAS_SiteBE.Action = "ShowMasterSiteList";

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.SiteCountryID = 0;
            List<MAS_SiteBE> lstSite = new List<MAS_SiteBE>();
            lstSite = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
            oMAS_SiteBAL = null;
            if (lstSite.Count > 0 && lstSite != null) {
                FillControls.FillListBox(ref UclstSiteList, lstSite, "SiteNumber", "SiteID");
            }
        }

    }

    private void bindManager() {
        SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
        oNewSCT_UserBE.Action = "GetManagers";
        lstUser = oSCT_UserBAL.GetMangersBAL(oNewSCT_UserBE);
        oSCT_UserBAL = null;
        FillControls.FillDropDown(ref ddlManagerName, lstUser, "FirstName", "UserID", "--Select--");
    }
    private void getSiteDetail() {
        MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

        if (GetQueryStringValue("SiteID") != null) {
            cusvNoSelectedSites.Enabled = false;
            trEdit.Attributes.Add("style", "display:block");
            trAdd.Attributes.Add("style", "display:none");
            oMAS_SiteBE.Action = "ShowMaster";

            oMAS_SiteBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();

            oMAS_SiteBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oMAS_SiteBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
           
            List<MAS_SiteBE> lstSiteDetails = new List<MAS_SiteBE>();
            lstSiteDetails = oMAS_SiteBAL.GetSiteMisSettingBAL(oMAS_SiteBE);
            oMAS_SiteBAL = null;
            if (lstSiteDetails != null) {

                lblSiteNumberValue.Text = lstSiteDetails[0].SiteNumber != "" ? Convert.ToString(lstSiteDetails[0].SiteNumber) : Convert.ToString(lstSiteDetails[0].SiteNumberList);
                txtSiteName.Text = Convert.ToString(lstSiteDetails[0].SiteName);
                txtSitePrefix.Text = Convert.ToString(lstSiteDetails[0].SitePrefix);
                txtPhoneNumbers.Text = Convert.ToString(lstSiteDetails[0].PhoneNumbers);
                txtFaxNumbers.Text = Convert.ToString(lstSiteDetails[0].FaxNumber);
                txtEmail.Text = Convert.ToString(lstSiteDetails[0].EmailID);
                if (ddlManagerName.Items.FindByValue(lstSiteDetails[0].SiteMangerUserID.ToString()) != null)
                    ddlManagerName.SelectedValue = lstSiteDetails[0].SiteMangerUserID.ToString();


                //---Stage 11 R3 Point 17---//
                ddlCurrency.SelectedIndex = ddlCurrency.Items.IndexOf(ddlCurrency.Items.FindByValue(lstSiteDetails[0].CurrencyID.ToString()));
                //-------------------------//


                txtAddress1.Text = Convert.ToString(lstSiteDetails[0].SiteAddressLine1);
                txtAddress2.Text = Convert.ToString(lstSiteDetails[0].SiteAddressLine2);
                txtAddress3.Text = Convert.ToString(lstSiteDetails[0].SiteAddressLine3);
                txtAddress4.Text = Convert.ToString(lstSiteDetails[0].SiteAddressLine4);
                txtAddress5.Text = Convert.ToString(lstSiteDetails[0].SiteAddressLine5);
                txtAddress6.Text = Convert.ToString(lstSiteDetails[0].SiteAddressLine6);
                if (ucSiteCountry.innerControlddlCountry.Items.FindByValue(lstSiteDetails[0].SiteCountryID.ToString()) != null)
                    ucSiteCountry.innerControlddlCountry.SelectedValue = lstSiteDetails[0].SiteCountryID.ToString();

                CountryIdSC = lstSiteDetails[0].SiteCountryID;

                CountryIdAP = lstSiteDetails[0].SiteCountryID;

                txtPostalCode.Text = Convert.ToString(lstSiteDetails[0].SitePincode);
                txtAPAddress1.Text = Convert.ToString(lstSiteDetails[0].APAddressLine1);
                txtAPAddress2.Text = Convert.ToString(lstSiteDetails[0].APAddressLine2);
                txtAPAddress3.Text = Convert.ToString(lstSiteDetails[0].APAddressLine3);
                txtAPAddress4.Text = Convert.ToString(lstSiteDetails[0].APAddressLine4);
                txtAPAddress5.Text = Convert.ToString(lstSiteDetails[0].APAddressLine5);
                txtAPAddress6.Text = Convert.ToString(lstSiteDetails[0].APAddressLine6);
                if (ucAPCountry.innerControlddlCountry.Items.FindByValue(lstSiteDetails[0].APCountryID.ToString()) != null)
                    ucAPCountry.innerControlddlCountry.SelectedValue = lstSiteDetails[0].APCountryID.ToString();
                txtAPPostcode.Text = Convert.ToString(lstSiteDetails[0].APPincode);
                txtRemarks.Text = Convert.ToString(lstSiteDetails[0].Remarks);
                txtCostCenter.Text = Convert.ToString(lstSiteDetails[0].CostCenter);

                txtOfficeHour.Text= Convert.ToString(lstSiteDetails[0].OfficeHour);
            }


        }
        else {

        }
    }


    #endregion

    #region Events

    protected void btnSave_Click(object sender, EventArgs e) {
        MAS_SiteBE oMAS_SITEBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SITEBAL = new MAS_SiteBAL();

        oMAS_SITEBE.SiteName = txtSiteName.Text;
        oMAS_SITEBE.SitePrefix = txtSitePrefix.Text;
        oMAS_SITEBE.PhoneNumbers = txtPhoneNumbers.Text;
        oMAS_SITEBE.FaxNumber = txtFaxNumbers.Text;
        oMAS_SITEBE.EmailID = txtEmail.Text;
        oMAS_SITEBE.SiteMangerUserID = ddlManagerName.SelectedIndex == 0 ? (int?)null : Convert.ToInt32(ddlManagerName.SelectedValue);

        //---Stage 11 R3 Point 17---//
        oMAS_SITEBE.CurrencyID = ddlCurrency.SelectedIndex == 0 ? (int?)null : Convert.ToInt32(ddlCurrency.SelectedItem.Value);
        //-------------------------//

        oMAS_SITEBE.SiteAddressLine1 = txtAddress1.Text;
        oMAS_SITEBE.SiteAddressLine2 = txtAddress2.Text;
        oMAS_SITEBE.SiteAddressLine3 = txtAddress3.Text;
        oMAS_SITEBE.SiteAddressLine4 = txtAddress4.Text;
        oMAS_SITEBE.SiteAddressLine5 = txtAddress5.Text;
        oMAS_SITEBE.SiteAddressLine6 = txtAddress6.Text;

        if (CountryIdSC == 1 && Convert.ToInt32(ucSiteCountry.innerControlddlCountry.SelectedValue) == 0 )
        {
            oMAS_SITEBE.SiteCountryID = CountryIdSC;
        }
        else
        {
            oMAS_SITEBE.SiteCountryID = Convert.ToInt32(ucSiteCountry.innerControlddlCountry.SelectedValue);
        }
        
        oMAS_SITEBE.SitePincode = txtPostalCode.Text;

        oMAS_SITEBE.APAddressLine1 = txtAPAddress1.Text;
        oMAS_SITEBE.APAddressLine2 = txtAPAddress2.Text;
        oMAS_SITEBE.APAddressLine3 = txtAPAddress3.Text;
        oMAS_SITEBE.APAddressLine4 = txtAPAddress4.Text;
        oMAS_SITEBE.APAddressLine5 = txtAPAddress5.Text;
        oMAS_SITEBE.APAddressLine6 = txtAPAddress6.Text;
 
        if (CountryIdAP == 1 && Convert.ToInt32(ucAPCountry.innerControlddlCountry.SelectedValue) == 0)
        {
            oMAS_SITEBE.APCountryID = CountryIdAP;
        }
        else
        {
            oMAS_SITEBE.APCountryID = Convert.ToInt32(ucAPCountry.innerControlddlCountry.SelectedValue);
        }

        oMAS_SITEBE.APPincode = txtAPPostcode.Text;
        oMAS_SITEBE.Remarks = txtRemarks.Text;

        oMAS_SITEBE.CostCenter = txtCostCenter.Text.Trim();

        oMAS_SITEBE.User = new SCT_UserBE();
        oMAS_SITEBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        oMAS_SITEBE.OfficeHour = txtOfficeHour.Text;

        if (GetQueryStringValue("SiteID") != null) {
            oMAS_SITEBE.Action = "UpdateMaster";
            oMAS_SITEBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        }
        else {
            oMAS_SITEBE.Action = "AddMaster";
            oMAS_SITEBE.IsActive = true;
            if (UclstSiteSelected.Items.Count > 0) {
                if (UclstSiteSelected.Items.Count > 1) {
                    oMAS_SITEBE.SiteNumberList = UclstSiteSelected.Items[0].Value + ",";
                    for (int i = 1; i < UclstSiteSelected.Items.Count - 1; i++) {
                        oMAS_SITEBE.SiteNumberList += UclstSiteSelected.Items[i].Value + ",";
                    }
                    oMAS_SITEBE.SiteNumberList += UclstSiteSelected.Items[UclstSiteSelected.Items.Count - 1].Value;
                }
                else {
                    oMAS_SITEBE.SiteNumberList = UclstSiteSelected.Items[0].Value;
                }
            }
        }


        int? iResult = oMAS_SITEBAL.addEditSiteSettingsBAL(oMAS_SITEBE);
        oMAS_SITEBAL = null;
        if (iResult == 0) {
            string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            EncryptQueryString("SiteSetupOverview.aspx");
        }

    }
    protected void btnDelete_Click(object sender, EventArgs e) {
        MAS_SiteBE oMAS_SITEBE = new MAS_SiteBE();
        MAS_SiteBAL oMAS_SITEBAL = new MAS_SiteBAL();
        oMAS_SITEBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        oMAS_SITEBE.Action = "DeleteMaster";
        int? iResult = oMAS_SITEBAL.DeleteSiteSettingsBAL(oMAS_SITEBE);
        oMAS_SITEBAL = null;
        if (iResult == 0) {
            EncryptQueryString("SiteSetupOverview.aspx");
        }
    }
    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("SiteSetupOverview.aspx");
    }
    protected void btnMoveRightAll_Click(object sender, EventArgs e) {
        if (UclstSiteList.Items.Count > 0)
            FillControls.MoveAllItems(UclstSiteList, UclstSiteSelected);
    }
    protected void btnMoveRight_Click(object sender, EventArgs e) {
        if (UclstSiteList.SelectedItem != null) {
            FillControls.MoveOneItem(UclstSiteList, UclstSiteSelected);
        }
    }
    protected void btnMoveLeft_Click(object sender, EventArgs e) {
        if (UclstSiteSelected.SelectedItem != null) {
            FillControls.MoveOneItem(UclstSiteSelected, UclstSiteList);
        }
    }
    protected void btnMoveLeftAll_Click(object sender, EventArgs e) {
        if (UclstSiteSelected.Items.Count > 0) {
            FillControls.MoveAllItems(UclstSiteSelected, UclstSiteList);
        }
    }

    #endregion


}
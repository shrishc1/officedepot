﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="GlobalVendorConsolidationOverview.aspx.cs" Inherits="GlobalVendorConsolidationOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%--<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>--%>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblGlobalVendorConsolidation" runat="server" Text="Global Vendor Consolidation"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="GlobalVendorConsolidationEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="85%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
        <%--<tr>
            <td width="15%" style="font-weight: bold;">
                <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold; width: 1%">
                :
            </td>
            <td width="84%">
                <uc1:ucCountry ID="ddlCountry" runat="server" Width="150px" />
            </td>
        </tr>--%>
        <tr>
            <td width="15%" style="font-weight: bold;">
                <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor" />
            </td>
            <td style="font-weight: bold; width: 1%">
                :
            </td>
            <td width="84%">
                <cc2:ucSeacrhVendor runat="server" ID="ddlSeacrhVendor" Visible="false" />
                <cc1:MultiSelectVendor runat="server" ID="msVendor" />
            </td>
        </tr>
        <tr align="center">
            <td colspan="3">
                <cc1:ucButton runat="server" ID="btnSearch" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
            </td>
        </tr>
    </table>
    <div>
        <br />
    </div>
    <div style="width: 100%; height: 800px; overflow: auto" id="divPrint">
        <table width="100%">
            <tr>
                <td align="center">
                    <cc1:ucGridView ID="grdVendorCons" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid"
                        AllowSorting="true" AllowPaging="true" PageSize="30" OnPageIndexChanging="grdVendorCons_PageIndexChanging">
                        <Columns>
                            <asp:BoundField HeaderText="Vendor Country" DataField="CountryName" SortExpression="CountryName">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Vendor Code" DataField="Vendor_No" SortExpression="Vendor_No">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" SortExpression="VendorName">
                                <HeaderStyle Width="30%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <%--<asp:BoundField HeaderText="Consolidated Country" DataField="ConsolidatedCountryName">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>--%>
                            <asp:TemplateField HeaderText="Consolidated Code" SortExpression="ConsolidatedCode">
                                <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpConsolidatedCode" runat="server" Text='<%# Eval("ConsolidatedCode") %>'
                                        NavigateUrl='<%# EncryptQuery("GlobalVendorConsolidationEdit.aspx?VendorId="+ Eval("ParentVendorID")) %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Consolidated Name" DataField="ConsolidatedName" SortExpression="ConsolidatedName">
                                <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                    </cc1:ucGridView>
                </td>
            </tr>
        </table>
    </div>
    <cc1:ucGridView ID="grdVendorConsExcel" Width="100%" runat="server" CssClass="grid"
        Visible="false">
        <Columns>
            <asp:BoundField HeaderText="Vendor Country" DataField="CountryName" SortExpression="CountryName">
                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Vendor Code" DataField="Vendor_No" SortExpression="Vendor_No">
                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" SortExpression="VendorName">
                <HeaderStyle Width="30%" HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Consolidated Code" DataField="ConsolidatedCode">
                <HeaderStyle Width="15%" HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Consolidated Name" DataField="ConsolidatedName" SortExpression="ConsolidatedName">
                <HeaderStyle Width="20%" HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
        </Columns>
    </cc1:ucGridView>
</asp:Content>

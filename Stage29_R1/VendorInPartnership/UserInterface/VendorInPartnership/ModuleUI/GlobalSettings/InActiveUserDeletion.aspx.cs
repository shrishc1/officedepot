﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Collections.Generic;
using System.Linq;
using WebUtilities;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BaseControlLibrary;
using Utilities;
using System.Data;
using System.Web;

public partial class ModuleUI_GlobalSettings_InActiveUserDeletion : CommonPage
{
    #region Declarations ...    
    protected string selectCheckboxToAccept = WebCommon.getGlobalResourceValue("SelectCheckboxToAction");
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["GridViewSortDirection"] == null)
                ViewState["GridViewSortDirection"] = SortDirection.Ascending;
            return (SortDirection)ViewState["GridViewSortDirection"];
        }
        set { ViewState["GridViewSortDirection"] = value; }
    }
    private void BindRole()
    {
        SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
        SCT_UserRoleBE oSCT_UserRoleBE = new SCT_UserRoleBE();
        oSCT_UserRoleBE.Action = "GetAllUsers";

        List<SCT_UserRoleBE> lstRole = oSCT_TemplateBAL.GetUserRolesBAL(oSCT_UserRoleBE);
        oSCT_TemplateBAL = null;
        if (lstRole.Count > 0)
        {
            //lstRole = lstRole.Where(item => item.RoleName.Contains("OD")).ToList();
            FillControls.FillDropDown(ref ddlRole, lstRole, "RoleName", "UserRoleID", "Select");
        }
    }
    public string SortDir
    {
        get
        {
            return GridViewSortDirection == SortDirection.Ascending ? "DESC" : "ASC";
        }
    }

    public int PageNumber {
        get
        {
            if (ViewState["PageNumber"] == null)
                ViewState["PageNumber"] = 0;
            return (int)ViewState["PageNumber"];
        }
        set { ViewState["PageNumber"] = value; }
    }
 
    public List<SCT_UserBE> LstUsers
    {
        get
        {
            if (ViewState["lstUsers"] == null)
                ViewState["lstUsers"] = null;
            return (List<SCT_UserBE>)ViewState["lstUsers"];
        }
        set { ViewState["lstUsers"] = value; }
    }

    public string GridViewSortExp
    {
        get
        {
            if (ViewState["GridViewSortExp"] == null)
                ViewState["GridViewSortExp"] = "LoginID";
            return (string)ViewState["GridViewSortExp"];
        }
        set
        {
            ViewState["GridViewSortExp"] = value;
        }
    }

    public bool IsGridSort
    {
        get
        {
            if (ViewState["IsGridSort"] == null)
                ViewState["IsGridSort"] = false;
            return (bool)ViewState["IsGridSort"];
        }
        set { ViewState["IsGridSort"] = value; }
    }

    protected string AllRejectedUserdeleteMessage = WebCommon.getGlobalResourceValue("AllRejectedUserdeleteMessage");
    
    const int DefaultPageIndex = 1;
    List<SCT_UserBE> lstUserSettings = new List<SCT_UserBE>();
    List<SCT_UserBE> lstCountrySettings = new List<SCT_UserBE>();

    #endregion

    #region Events ...

    protected void Page_InIt(object sender, EventArgs e)
    {
        if (ScriptManager.GetCurrent(Page) == null)
        {
            ScriptManager sMgr = new ScriptManager();
            Page.Form.Controls.AddAt(0, sMgr);
        }

        ddlCountry.CurrentPage = this;
        ddlCountry.IsAllRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BtnDeleteUser.Visible = LstUsers !=null && LstUsers.Count > 0;
            BindRole();
       
            Pager1.PageSize = 20;
            Pager1.GenerateGoToSection = false;
            Pager1.GeneratePagerInfoSection = false;
        }
    }
 
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
           
            oSCT_UserBE.Action = "SearchLostLoginUsers";
            oSCT_UserBE.CountryId = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
            oSCT_UserBE.LastLoginDays = !string.IsNullOrEmpty(txtLastLoginDays.Text) ? Convert.ToInt32(txtLastLoginDays.Text) : 0;
            oSCT_UserBE.UserSearchText = txtUserNameValue.Text.Trim();
            oSCT_UserBE.UserRoleID = Convert.ToInt32(ddlRole.SelectedValue);
            oSCT_UserBE.PageIndex = 1;
            oSCT_UserBE.PageSize = 20;
            switch (oSCT_UserBE.UserRoleID)
            {
                case 2:
                    oSCT_UserBE.ReportType = "VendorOverview";
                    break;
                case 3:
                    oSCT_UserBE.ReportType = "CarrierOverview";
                    break;
                default:
                    oSCT_UserBE.ReportType = "OfficeDepotUserOverview";
                    break;
            }

            LstUsers = oSCT_UserBAL.GetLastLoginUsersBAL(oSCT_UserBE);
            oSCT_UserBAL = null;

            if (LstUsers.Count > 0)
            {                                
                gvUser.PageIndex = 1;
                Pager1.PageSize = 20;
                Pager1.CurrentIndex = 1;
                PageNumber = 1;
                Pager1.ItemCount = LstUsers.Count > 0 ? LstUsers[0].TotalRecords : 0;
                Pager1.Visible = true;
                  
                gvUser.DataSource = null;
                gvUser.DataBind();

                BtnDeleteUser.Visible = LstUsers.Count > 0;

                gvUser.DataSource = LstUsers;
                gvUser.DataBind();

                GridViewSortExp = "LoginID";
                GridViewSortDirection = SortDirection.Ascending;
                SortGridView(GridViewSortExp, GridViewSortDirection);
            }
            else
            {
                Pager1.Visible = false;
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    public void pager_Command(object sender, CommandEventArgs e)
    { 
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument); 
        Pager1.PageSize = 20;
        Pager1.CurrentIndex = currnetPageIndx;
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();

        oSCT_UserBE.Action = "SearchLostLoginUsers";
        oSCT_UserBE.CountryId = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
        oSCT_UserBE.LastLoginDays = !string.IsNullOrEmpty(txtLastLoginDays.Text) ? Convert.ToInt32(txtLastLoginDays.Text) : 0;
        oSCT_UserBE.UserSearchText = txtUserNameValue.Text.Trim();
        oSCT_UserBE.UserRoleID = Convert.ToInt32(ddlRole.SelectedValue);
        oSCT_UserBE.PageIndex = currnetPageIndx;
        oSCT_UserBE.PageSize = 20;
        PageNumber = currnetPageIndx;
        switch (oSCT_UserBE.UserRoleID)
        {
            case 2:
                oSCT_UserBE.ReportType = "VendorOverview";
                break;
            case 3:
                oSCT_UserBE.ReportType = "CarrierOverview";
                break;
            default:
                oSCT_UserBE.ReportType = "OfficeDepotUserOverview";
                break;
        }

        LstUsers = oSCT_UserBAL.GetLastLoginUsersBAL(oSCT_UserBE);
        oSCT_UserBAL = null;

        if (LstUsers.Count > 0)
        {
            Pager1.ItemCount = LstUsers.Count > 0 ? LstUsers[0].TotalRecords : 0;
            Pager1.Visible = true;
            gvUser.DataSource = LstUsers;
            gvUser.DataBind();
        }
    }

    private void SortGridView(string sortExpression, SortDirection direction)
    { 
        try
        {
            if (LstUsers != null)
            { 
                LstUsers = Utilities.GenericListHelper<SCT_UserBE>.SortList(LstUsers, sortExpression, direction);
                Pager1.ItemCount = LstUsers.Count > 0 ? LstUsers[0].TotalRecords : 0;
                Pager1.Visible = true;
                gvUser.DataSource = LstUsers;
                gvUser.DataBind();   
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
     
    protected void btnDeleteRejectedUser_Click(object sender, EventArgs e)
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        oSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
        oSCT_UserBE.Action = "DeleteRejectedUser";
        int? iResult = oSCT_UserBAL.addEditUserBAL(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (iResult == 0)
        {
            EncryptQueryString("SCT_UsersOverview.aspx");
        }
    }

    protected void gvUser_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink hlLoginID = (HyperLink)e.Row.FindControl("hlLoginID");
            if (((SCT_UserBE)e.Row.DataItem).LoginID == "--NA--")
            {
                hlLoginID.Visible = false;
            }
            if (ViewState["ReadOnly"] != null && Convert.ToBoolean(ViewState["ReadOnly"]) == true)
            {
                HyperLink hplink = (HyperLink)e.Row.FindControl("hpVendorLink");
                hplink.NavigateUrl = EncryptQuery("../GlobalSettings/VendorEdit.aspx?ReadOnly=1&PageFrom=UserList&VendorID=" + DataBinder.Eval(e.Row.DataItem, "Vendor.VendorID"));

                hlLoginID.NavigateUrl = DataBinder.Eval(e.Row.DataItem, "UserRoleID").ToString() == "2" || DataBinder.Eval(e.Row.DataItem, "UserRoleID").ToString() == "3"
                    ? DataBinder.Eval(e.Row.DataItem, "AccountStatus").ToString().ToLower() == "registered awaiting approval" || DataBinder.Eval(e.Row.DataItem, "AccountStatus").ToString().ToLower() == "edited awaiting approval"
                        ? EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID") + "&UStatus=RAA")
                        : EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID"))
                    : EncryptQuery("~/ModuleUI/Security/SCT_UserEdit.aspx?ReadOnly=1&UserID=" + DataBinder.Eval(e.Row.DataItem, "UserID"));
            }
        }
    }

    protected void gvUser_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "confirm('Are you sure you want to delete?')", true);
        if (e.CommandName == "DeleteUser")
        { 
            string UserID = Convert.ToString(e.CommandArgument);
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            SCT_UserBE oSCT_UserBE = new SCT_UserBE();
            List<SCT_UserBE> lstUsers = new List<SCT_UserBE>();
            oSCT_UserBE.Action = "DeleteSelectedUsers";
            oSCT_UserBE.UserIds = UserID;
            oSCT_UserBE.UserID = Session["UserID"] != null ? Convert.ToInt32(Session["UserID"]) : 0;
            lstUsers = oSCT_UserBAL.GetLastLoginUsersBAL(oSCT_UserBE);
            oSCT_UserBAL = null;
        }
    }
    
    #endregion

    #region Methods ...
      
    protected void gridView_Sorting(Object sender, GridViewSortEventArgs e)
    {
        try
        {
            IsGridSort = true;
            string sortExpression = e.SortExpression;

            GridViewSortExp = sortExpression;
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                GridViewSortDirection = SortDirection.Descending;
                SortGridView(sortExpression, GridViewSortDirection);
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
                SortGridView(sortExpression, GridViewSortDirection);
            }

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
     
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        GridViewSortExp = e.SortExpression;
        GridViewSortDirection = SortDir == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
        return Utilities.GenericListHelper<SCT_UserBE>.SortList((List<SCT_UserBE>)ViewState["lstUsers"], e.SortExpression, GridViewSortDirection).ToArray();
    }
     
    #endregion

    protected void BtnDeleteUser_Click(object sender, EventArgs e)
    {
        try
        {
            SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
            SCT_UserBE oSCT_UserBE = new SCT_UserBE(); 

            string UserIds = string.Empty;

            foreach (GridViewRow row in gvUser.Rows)
            {
                CheckBox chkSelect = (CheckBox)row.FindControl("chkSelect");
                Literal ltUserID = (Literal)row.FindControl("ltUserID");

                if (chkSelect.Checked)
                {
                    UserIds = UserIds + "," + ltUserID.Text;
                }
            }

            oSCT_UserBE.Action = "DeleteSelectedUsers";
            oSCT_UserBE.UserIds = UserIds;
            oSCT_UserBE.UserID = Session["UserID"] != null ? Convert.ToInt32(Session["UserID"]) : 0;
            var lstUsers = oSCT_UserBAL.GetLastLoginUsersBAL(oSCT_UserBE);
            oSCT_UserBAL = null;
            SortGridView(GridViewSortExp, GridViewSortDirection);

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
}
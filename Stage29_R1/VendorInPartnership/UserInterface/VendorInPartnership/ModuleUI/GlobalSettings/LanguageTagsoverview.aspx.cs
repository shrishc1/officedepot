﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Resources;
using System.Collections;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Xml.Linq;
using System.Linq;
using WebUtilities;
using System.Reflection;
using BusinessEntities.ModuleBE.Languages;
using BusinessLogicLayer.ModuleBAL.Languages;
using System.Web;
using System.Web.UI;





public partial class LanguageTagsoverview : CommonPage
{

    bool IsExportClicked = false;

    #region Methods

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindLanguage();
            GetFilteredResult();
        }
    }

    private void BindLanguage()
    {
        SCT_UserBE oSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();

        oSCT_UserBE.Action = "GetLanguage";
        List<SCT_UserBE> lstLanguage = oSCT_UserBAL.GetLanguagesBAL(oSCT_UserBE);
        oSCT_UserBAL = null;
        if (lstLanguage.Count > 0)
        {
            FillControls.FillDropDown(ref ddlLanguage, lstLanguage, "Language", "ISOLanguageName");
        }
        //Add by abhinav to set default seletion of language dropdown as per the current user language
        ddlLanguage.SelectedIndex = ddlLanguage.Items.IndexOf(ddlLanguage.Items.FindByText(WebLocalize.Resource.getUserLanguage()));
        
    }

    private void GetFilteredResult()
    {
        MAS_LanguageTagsBE oMAS_LanguageTagsBE = new MAS_LanguageTagsBE();
        MAS_LanguageTagsBAL oMAS_LanguageTagsBAL = new MAS_LanguageTagsBAL();

        oMAS_LanguageTagsBE.Action = "GetAllLanguageTags";
        List<MAS_LanguageTagsBE> lstLanguageTags = oMAS_LanguageTagsBAL.GetAllLanguageTagsBAL(oMAS_LanguageTagsBE);
        string SelectedLanguage = ddlLanguage.SelectedItem.Text;
        var lstEnglish = (from resElem in lstLanguageTags

                          select new ResourceResult
                          {
                              LanguageTagsId = resElem.LanguageTagsId,
                              Key = resElem.LanguageKey,
                              Value = resElem.English,
                              Translation = (string)resElem.GetType().GetProperty(SelectedLanguage).GetValue(resElem,null)
                          }) .ToList();

        lstEnglish = lstEnglish.Where(x => x.Translation != null).ToList();

        string sSearchEngTransaltion = txtSearchEnglish.Text.ToLower();
        string sSearchSelectedLangTranslation = txtSearchTranslation.Text.ToLower();

        if (!string.IsNullOrEmpty(sSearchEngTransaltion.Trim()))
        {
            sSearchEngTransaltion = sSearchEngTransaltion.Replace("©", "&copy;");
            sSearchEngTransaltion = sSearchEngTransaltion.Replace("&lt;br/&gt;", "<br/>");
            sSearchEngTransaltion = sSearchEngTransaltion.Replace("\r\n", "<br/>");
        }
        string sLangEng = sSearchEngTransaltion.Replace("<br/>", "<br /> ");

        if (!string.IsNullOrEmpty(sSearchSelectedLangTranslation.Trim()))
        {
            sSearchSelectedLangTranslation = sSearchSelectedLangTranslation.Replace("©", "&copy;");
            sSearchSelectedLangTranslation = sSearchSelectedLangTranslation.Replace("&lt;br/&gt;", "<br/>");
            sSearchSelectedLangTranslation = sSearchSelectedLangTranslation.Replace("\r\n", "<br/>");
        }

        string sLangTran = sSearchSelectedLangTranslation.Replace("<br/>", "<br /> ");


        List<ResourceResult> result = lstEnglish.Where(x =>(x.Value.ToLower().Contains(sSearchEngTransaltion)||  x.Value.ToLower().Contains(sLangEng)) && (x.Translation.ToLower().Contains(sSearchSelectedLangTranslation) || x.Translation.ToLower().Contains(sLangTran))).ToList();
         ViewState["LanguageTags"] = result;
        gvResourceEditor.DataSource = result;
        gvResourceEditor.DataBind();
        if (IsExportClicked)
        {
            grdResourceEditorExcel.DataSource = result;
            grdResourceEditorExcel.DataBind();
        }

        if (gvResourceEditor.Rows.Count == 0)
        {
            btnExportToExcel.Visible = false;
        }
        else
        {
            btnExportToExcel.Visible = true;
        }
    }

    #endregion


    #region Events

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        gvResourceEditor.PageIndex = 0;
        GetFilteredResult();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtSearchEnglish.Text = string.Empty;
        txtSearchTranslation.Text = string.Empty;
        GetFilteredResult();

    }

    protected void btnApplyChanges_Click(object sender, EventArgs e)
    {
        //Get All LanguageTags From DB
        //MAS_LanguageTagsBE oMAS_LanguageTagsBE = new MAS_LanguageTagsBE();
        //MAS_LanguageTagsBAL oMAS_LanguageTagsBAL = new MAS_LanguageTagsBAL();

        //oMAS_LanguageTagsBE.Action = "GetAllLanguageTags";
        //List<MAS_LanguageTagsBE> lstLanguageTags = oMAS_LanguageTagsBAL.GetAllLanguageTagsBAL(oMAS_LanguageTagsBE);
        //if (lstLanguageTags.Count > 0)
        //{
        //    WebLocalize.Resource.lstTags = null;
        //    Cache.Remove("LanguageTagslist");
        //    Cache.Insert("LanguageTagslist", lstLanguageTags, null,
        //               System.Web.Caching.Cache.NoAbsoluteExpiration,
        //               System.Web.Caching.Cache.NoSlidingExpiration);
        //}
        WebLocalize.Resource.dictionaryCzech.Clear();
        WebLocalize.Resource.dictionaryDutch.Clear();
        WebLocalize.Resource.dictionaryEnglish.Clear();
        WebLocalize.Resource.dictionaryFrench.Clear();
        WebLocalize.Resource.dictionaryGerman.Clear();
        WebLocalize.Resource.dictionaryItalian.Clear();
        WebLocalize.Resource.dictionarySpanish.Clear();

        System.Web.HttpRuntime.Cache.Remove("LanguageTagsEnglish");
        System.Web.HttpRuntime.Cache.Remove("LanguageTagsFrench");
        System.Web.HttpRuntime.Cache.Remove("LanguageTagsGerman");
        System.Web.HttpRuntime.Cache.Remove("LanguageTagsDutch");
        System.Web.HttpRuntime.Cache.Remove("LanguageTagsSpanish");
        System.Web.HttpRuntime.Cache.Remove("LanguageTagsItalian");
        System.Web.HttpRuntime.Cache.Remove("LanguageTagsCzech");

        Utility.LoadLanguageTags();

    }

    protected void gvResourceEditor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvResourceEditor.PageIndex = e.NewPageIndex;
        GetFilteredResult();
        //if (ViewState["LanguageTags"] != null)
        //{
           
        //    grdResourceEditorExcel.DataSource = (List<ResourceResult>)ViewState["LanguageTags"];
        //    grdResourceEditorExcel.DataBind();
        //}
    }

    protected void gvResourceEditor_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    protected void gvResourceEditor_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //if (e.Row.RowType == DataControlRowType.DataRow) //check for RowType
        //{


        //    foreach (DataControlFieldCell cell in e.Row.Cells)
        //    {

        //        foreach (Control control in cell.Controls)
        //        {

        //            LinkButton button = control as LinkButton;
        //            if (button != null && button.CommandName == "Update")
        //                // Add delete confirmation
        //                button.OnClientClick = "if (!confirm('Are you sure " +
        //                       "you want to delete this record?')) return;";
        //        }
        //    }
        //}

    }

    protected void gvResourceEditor_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "Update")
        {

            int index = Convert.ToInt32(e.CommandArgument);
            TextBox txtTranslation = (TextBox)gvResourceEditor.Rows[index].FindControl("txtTranslation1");
            Label lblKey = (Label)gvResourceEditor.Rows[index].FindControl("lblKey1");
            HiddenField hdnLangTagsId = (HiddenField)gvResourceEditor.Rows[index].FindControl("hdnLangTagId");

            string sLanguageKey = lblKey.Text;
            string sTranslationText = txtTranslation.Text;
            int iLangTagsId = Convert.ToInt32(hdnLangTagsId.Value);
            string sSelectedLanguage = ddlLanguage.SelectedItem.Text;


            sTranslationText = sTranslationText.Replace("'", "''");

            MAS_LanguageTagsBE oMAS_LanguageTagsBE = new MAS_LanguageTagsBE();
            MAS_LanguageTagsBAL oMAS_LanguageTagsBAL = new MAS_LanguageTagsBAL();


            oMAS_LanguageTagsBE.Action = "UpdateLanguageTags";
            oMAS_LanguageTagsBE.LanguageTagsId = iLangTagsId;
            oMAS_LanguageTagsBE.LanguageName = sSelectedLanguage;
            oMAS_LanguageTagsBE.LanguageText = sTranslationText;

            int? iResult = oMAS_LanguageTagsBAL.EditLanguageTagBAL(oMAS_LanguageTagsBE);

        }

        gvResourceEditor.EditIndex = -1;
        GetFilteredResult();
    }





    protected void gvResourceEditor_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvResourceEditor.EditIndex = e.NewEditIndex;
        GetFilteredResult();
    }

    protected void gvResourceEditor_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvResourceEditor.EditIndex = -1;
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        GetFilteredResult();
        LocalizeGridHeader(grdResourceEditorExcel);
        if (grdResourceEditorExcel.Rows.Count > 0)
            WebCommon.Export("LanguageTags", grdResourceEditorExcel);
    }
    #endregion

      [Serializable]
    private class ResourceResult
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string Translation { get; set; }
        public int LanguageTagsId { get; set; }
    }

}




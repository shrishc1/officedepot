﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using WebUtilities;

public partial class ModuleUI_GlobalSettings_OverstockAgreementEdit : CommonPage
{
    protected string SuccessMesg = WebCommon.getGlobalResourceValue("SuccessMesg");
    protected string PleaseEnterNotes = WebCommon.getGlobalResourceValue("PleaseEnterNotes");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
            {
                GetOverstockAgreementDetailsforEdit();
            }
        }

    }

    private void GetOverstockAgreementDetailsforEdit()
    {
        MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
        MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();

        oMAS_VendorBE.Action = "GetOverstockAgreementView";
        oMAS_VendorBE.VendorIDs = Convert.ToString(GetQueryStringValue("VendorID"));
        oMAS_VendorBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));

        List<MAS_VendorBE> lstMAS_VendorBE = oMAS_VendorBAL.GetOverstockReturnsAgreementOverviewBAL(oMAS_VendorBE);
        oMAS_VendorBAL = null;

        if (lstMAS_VendorBE != null && lstMAS_VendorBE.Count > 0)
        {

            txtCountry.Text = Convert.ToString(lstMAS_VendorBE[0].Vendor_Country);
            txtVendor.Text = lstMAS_VendorBE[0].Vendor;
            if (lstMAS_VendorBE[0].IsVendorOverstockAgreement == "N")
            {
                rdoNo.Checked = true;               
            }
            else
            {
                rdoYes.Checked = true;
            }

            txtNotes.Text = lstMAS_VendorBE[0].VendorOverstockAgreementDetails;
        }



    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        MAS_VendorBE oMAS_VendorBE = new MAS_VendorBE();
        MAS_VendorBAL oMAS_VendorBAL = new MAS_VendorBAL();

        oMAS_VendorBE.Action = "UpdateOverstockDetails";

        if (GetQueryStringValue("VendorID") != null)
        {
            oMAS_VendorBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
        }

        if (rdoYes.Checked)
        {
            oMAS_VendorBE.IsVendorOverstockAgreement = "Y";
        }
        else if (rdoNo.Checked)
        {
            oMAS_VendorBE.IsVendorOverstockAgreement = "N";
        }

        oMAS_VendorBE.VendorOverstockAgreementDetails = txtNotes.Text;

        int? result = oMAS_VendorBAL.UpdateOverstockReturnsDetailsBAL(oMAS_VendorBE);

        if (result == 1)
        {
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('" + SuccessMesg + "');", true);

            if (GetQueryStringValue("PageFrom") != null)
            {
                if (GetQueryStringValue("PageFrom").Equals("OverstockAgreementView"))
                {
                    EncryptQueryString("OverstockAgreementView.aspx?PageFrom=OverstockAgreementEdit");                    
                }

                if (GetQueryStringValue("PageFrom").Equals("VendorEdit"))
                {
                    EncryptQueryString("VendorEdit.aspx?VendorID=" + GetQueryStringValue("VendorID") + "&PageFrom=" + "OverstockAgreementEdit");
                }
            }
           
           
        }

        oMAS_VendorBAL = null;

    }   
    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PageFrom") != null)
        {
            if (GetQueryStringValue("PageFrom").Equals("OverstockAgreementView"))
            {
                EncryptQueryString("OverstockAgreementView.aspx?PageFrom=OverstockAgreementEdit");
            }

            if (GetQueryStringValue("PageFrom").Equals("VendorEdit"))
            {
                EncryptQueryString("VendorEdit.aspx?VendorID=" + GetQueryStringValue("VendorID") + "&PageFrom=" + "OverstockAgreementEdit");
            }
        }
    }
}
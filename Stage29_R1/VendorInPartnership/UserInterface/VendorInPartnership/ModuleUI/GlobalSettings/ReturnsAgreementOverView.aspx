﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReturnsAgreementOverView.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
Inherits="ModuleUI_GlobalSettings_ReturnsAgreementOverView" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
 <h2>
        <cc1:uclabel ID="lblReturnsAgreement" runat="server"></cc1:uclabel>
        </h2>    
        <br />

    <div>        
        <table width="90%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
            <tr>
                <td width="15%" style="font-weight: bold;">
                    <cc1:uclabel ID="lblCountry" runat="server" Text="Country"></cc1:uclabel>
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td width="34%">
                    <uc1:ucCountry ID="ucCountry" runat="server" Width="150px" />
                </td>              
            </tr>
           
            <tr>
               <td width="15%" style="font-weight: bold;">
                    <cc1:ucLabel ID="lblVendor" runat="server" />
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td width="84%" colspan="4">                  
                    <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                </td>
            </tr>  
             <tr>  
                 <td width="15%" style="font-weight: bold;">                    
                </td>
                <td style="font-weight: bold; width: 1%">
                </td>             
                <td width="84%" colspan="4">                  
                   <cc1:ucRadioButton ID="rdoAgreementinPlace" runat="server" Text="Agreement in place" Checked="true"   GroupName="AgreementType"  />&nbsp;
                    <cc1:ucRadioButton ID="rdoNoAgreement" runat="server" Text="No Agreement" GroupName="AgreementType"  />    &nbsp;
                    <cc1:ucRadioButton ID="rdoAllVendors" runat="server" Text="All Vendors"   GroupName="AgreementType"  />
                </td>
            </tr>           
            <tr align="right">
                <td colspan="6">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <cc1:ucbutton ID="btnShow" ValidationGroup="Show" runat="server" Text="Show" CssClass="button"
                        OnClick="btnShow_Click" />
                </td>
            </tr>

        </table>
    </div>

     <br />
       <br />
    <div class="button-row" style="float:right;" >       
        <cc2:ucExportToExcel ID="btnExportToExcel1" runat="server" />
    </div>
    <br />
    <br />

    <table width="100%">
        <tr>
            <td align="center" colspan="5">
                <cc1:ucgridview ID="gvReturnsAgreementOverview" Width="100%" runat="server" CssClass="grid"
                     AllowPaging="True" OnPageIndexChanging="gvRetursAgreementOverview_PageIndexChanging"
                    PageSize="30">

                    <EmptyDataTemplate>
                            <div style="text-align: center">
                                <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                            </div>
                    </EmptyDataTemplate>
                    <Columns>
                      
                        <asp:BoundField HeaderText="Country" DataField="Vendor_Country" >
                            <HeaderStyle HorizontalAlign="Left" Width="8%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                         <%-- <asp:BoundField HeaderText="Vendor No" DataField="Vendor_No" >
                            <HeaderStyle HorizontalAlign="Left" Width="6%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>--%>

                        <asp:BoundField HeaderText="Lines Rcvd" DataField="Lines">
                            <HeaderStyle HorizontalAlign="Center" Width="9%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Weight" DataField="Weightage">
                            <HeaderStyle HorizontalAlign="Center" Width="9%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>

                        <asp:TemplateField HeaderText="Vendor"  ItemStyle-Width="9%">
                            <HeaderStyle Width="8%" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:HyperLink ID="VendorLink" runat="server" NavigateUrl='<%# EncryptQuery("ReturnsAgreementEdit.aspx?VendorID="+Eval("VendorID")+"&CountryID="+Eval("CountryID")+"&PageFrom="+"ReturnsAgreementView") %>'
                                    Text='<%#Eval("Vendor") %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>                        
                       
                        <asp:BoundField HeaderText="Returns Agreement Y/N" DataField="IsVendorReturnsAgreement">
                            <HeaderStyle HorizontalAlign="Center" Width="9%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Notes" DataField="VendorReturnsAgreementDetails">
                            <HeaderStyle HorizontalAlign="Left" Width="14%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </cc1:ucgridview>
                 <cc1:ucGridView ID="gvExport" runat="server" AutoGenerateColumns="false" Style="display: none;">
                        <Columns>
                            <asp:BoundField HeaderText="Country" DataField="Vendor_Country" >
                            <HeaderStyle HorizontalAlign="Left" Width="8%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                          <%--<asp:BoundField HeaderText="Vendor No" DataField="Vendor_No" >
                            <HeaderStyle HorizontalAlign="Left" Width="6%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>--%>

                            <asp:BoundField HeaderText="Lines Rcvd" DataField="Lines">
                            <HeaderStyle HorizontalAlign="Center" Width="9%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Weight" DataField="Weightage">
                            <HeaderStyle HorizontalAlign="Center" Width="9%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>

                        <asp:TemplateField HeaderText="Vendor"  ItemStyle-Width="9%">
                            <HeaderStyle Width="8%" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:HyperLink ID="VendorLink" runat="server" NavigateUrl='<%# EncryptQuery("ReturnsAgreementEdit.aspx?VendorID="+Eval("VendorID")+"&CountryID="+Eval("CountryID")+"&PageFrom="+"ReturnsAgreementView") %>'
                                    Text='<%#Eval("Vendor") %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>                        
                       
                        <asp:BoundField HeaderText="Returns Agreement Y/N" DataField="IsVendorReturnsAgreement">
                            <HeaderStyle HorizontalAlign="Center" Width="9%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Notes" DataField="VendorReturnsAgreementDetails">
                            <HeaderStyle HorizontalAlign="Left" Width="14%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        </Columns>
                    </cc1:ucGridView>
             </td>
          </tr>
      </table>

    </asp:Content>

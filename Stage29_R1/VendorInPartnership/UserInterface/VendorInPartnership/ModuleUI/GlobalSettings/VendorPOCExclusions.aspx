﻿<%@ Page Language="C#" AutoEventWireup="true"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    MaintainScrollPositionOnPostback="true"
    CodeFile="VendorPOCExclusions.aspx.cs"
    Inherits="ModuleUI_GlobalSettings_VendorPOCExclusions" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:ScriptManager runat="server" />
    <h2>
        <cc1:ucLabel ID="VendorPOCExclusions" runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="VendorPOCExclusionsEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <div style="width: 99%; overflow: auto" id="divPrint" class="fixedTable" onscroll="getScroll1(this);">
        <table cellspacing="1" cellpadding="0" class="form-table">
            <tr>
                <td>
                    <cc1:ucGridView ID="gvVendorPOC" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                        CellPadding="0" Width="980px"
                        AllowSorting="false" AllowPaging="true" PageSize="30">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <EmptyDataTemplate>
                            <div style="text-align: center">
                                <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                            </div>
                        </EmptyDataTemplate>
                        <Columns>
                            <%--     <asp:TemplateField HeaderText="Country">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="lblCountry" runat="server" Text='<%# Eval("CountryName") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                            <asp:BoundField DataField="CountryName" HeaderText="Country">
                                <ItemStyle  HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="left" Width="120px" />                            
                            </asp:BoundField>


                            <asp:TemplateField HeaderText="Vendor">
                                <HeaderStyle Width="150px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypHistory" runat="server" Text='<%#Eval("VendorName") %>' NavigateUrl='<%# EncryptQuery("~/ModuleUI/GlobalSettings/VendorPOCExclusionsEdit.aspx?Mode=Edit&VendorExclusionID=" + Eval("VendorExclusionID") ) %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reason for Exclusion">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="lblReasonForExclusion" runat="server" Text='<%# Eval("ReasonForExclusion") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Exluded By">
                                <HeaderStyle Width="120px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <cc1:ucLabel ID="lblExcludedBy" runat="server" Text='<%# Eval("UserName") %>'></cc1:ucLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </cc1:ucGridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="PointsofContactSummary.aspx.cs" Inherits="PointsofContactSummary"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblVendorPointsofContactSummary" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <div class="button-row">
                <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" Visible="false" />
            </div>
            <table width="100%">
                <tr>
                    <td style="font-weight: bold;" width="35%">
                        <asp:RadioButtonList ID="rblVendors" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Value="0" Selected="True">All Vendors</asp:ListItem>
                            <asp:ListItem Value="1">Only Vendors with open PO's</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td style="font-weight: bold;" width="20%">
                        <asp:CheckBox ID="chkExcludeVendors" runat="server" Text="Include Excluded Vendors" />
                    </td>
                    <td width="45%">
                        <cc1:ucButton ID="btnSearch" runat="server" CssClass="button" OnClick="btnSearch_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <cc1:ucGridView ID="gvVendorPOC" AllowSorting="true" OnSorting="SortGrid" runat="server"
                            AutoGenerateColumns="false" CssClass="grid gvclass" CellPadding="0" Width="960px"
                            OnRowDataBound="gvVendorPOC_RowDataBound">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                            <EmptyDataTemplate>
                                <div style="text-align: center">
                                    <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Country" SortExpression="Country">
                                    <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hpCountry" runat="server" Text='<%# Eval("Country") %>' NavigateUrl='<%# EncryptQuery("PointsofContactDetail.aspx?CountryID="+ Eval("CountryID")) %>'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Scheduling" DataField="Scheduling" SortExpression="Scheduling">
                                    <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Discrepancies" DataField="Discrepancies" SortExpression="Discrepancies">
                                    <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="OTIF" DataField="OTIF" SortExpression="OTIF">
                                    <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Scorecard" DataField="Scorecard" SortExpression="Scorecard">
                                    <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Inventory" DataField="Inventory" SortExpression="Inventory">
                                    <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Procurement" DataField="Procurement" SortExpression="Procurement">
                                    <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="AccountsPayable" DataField="AccountsPayable" SortExpression="AccountsPayable">
                                    <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Executive" DataField="Executive" SortExpression="Executive">
                                    <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

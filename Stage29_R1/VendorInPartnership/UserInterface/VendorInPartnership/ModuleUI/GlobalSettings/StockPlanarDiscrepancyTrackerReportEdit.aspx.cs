﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;

public partial class ModuleUI_GlobalSettings_StockPlanarDiscrepancyTrackerReportEdit : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var trackerId = GetQueryStringValue("DiscrepancyTrackerId");

            if (!string.IsNullOrEmpty(trackerId))
            {
                CommunicationLibraryBAL communicationLibraryBAL = new CommunicationLibraryBAL();
                DiscrepancyNotification discrepancyNotification = new DiscrepancyNotification();
                discrepancyNotification.DiscrepancyTrackerId = Convert.ToInt64(trackerId);
                discrepancyNotification.Action = "GetDiscrepancyTrackerCommunicationByTrackerId";
                var ds = communicationLibraryBAL.GetStockPlanarDiscrepancyTrackerByTrackerIdBAL(discrepancyNotification);
                if (ds != null)
                {
                    txtSubjectText.Text = ds.Tables[0].Rows[0]["EmailSubject"].ToString();
                    txtEmailCommunication.Text = ds.Tables[0].Rows[0]["EmailContent"].ToString();
                }
            }
             

            btnSubmit.Visible = Convert.ToBoolean(GetQueryStringValue("MailSent")) == true ? false : true;
        }
    }

    string emailSubjectMandatory = WebCommon.getGlobalResourceValue("EmailSubjectMandatory");
    string emailCommunicationMandatory = WebCommon.getGlobalResourceValue("EmailBodyMandatory");
    string errMsg= WebCommon.getGlobalResourceValue("DownLoadFileNotExist");
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty(txtSubjectText.Text) || string.IsNullOrWhiteSpace(txtSubjectText.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + emailSubjectMandatory + "')", true);
            txtSubjectText.Focus();
            return;
        }

        if (string.IsNullOrEmpty(txtEmailCommunication.Text) || string.IsNullOrWhiteSpace(txtEmailCommunication.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + emailCommunicationMandatory + "')", true);
            txtEmailCommunication.Focus();
            return;
        }

        CommunicationLibraryBAL communicationLibraryBAL = new CommunicationLibraryBAL();
        DiscrepancyNotification discrepancyNotification = new DiscrepancyNotification();
        discrepancyNotification.DiscrepancyTrackerId = Convert.ToInt64(GetQueryStringValue("DiscrepancyTrackerId"));
        discrepancyNotification.Action = "UpdateDiscrepancyTrackerCommunication";
        discrepancyNotification.EmailContent = txtEmailCommunication.Text;
        discrepancyNotification.EmailSubject = txtSubjectText.Text;
        discrepancyNotification.EditedBy = Convert.ToInt32(Session["UserID"].ToString());
        var ds = communicationLibraryBAL.UpdateStockPlanarDiscrepancyTrackerByTrackerIdBAL(discrepancyNotification);

        EncryptQueryString("StockPlanarDiscrepancyTrackerReport.aspx");
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("StockPlanarDiscrepancyTrackerReport.aspx");
    }


    public void DownloadButton_Click(Object sender, EventArgs e)
    {
        string fName = GetQueryStringValue("File");
        if (!string.IsNullOrEmpty(fName))
        {
            if (File.Exists(fName))
            {
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + fName);
                Response.TransmitFile(fName);
                Response.End();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + errMsg + "');</script>", false);
            }
        }
    }
}
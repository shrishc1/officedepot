﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.GlobalSettings;
using BusinessLogicLayer.ModuleBAL.GlobalSettings;
using Utilities;
using WebUtilities;
using System.IO;

public partial class PointsofContactDetail : CommonPage
{

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["GridViewSortDirection"] == null)
                ViewState["GridViewSortDirection"] = SortDirection.Descending;
            return (SortDirection)ViewState["GridViewSortDirection"];
        }
        set { ViewState["GridViewSortDirection"] = value; }
    }

    public string SortDir
    {
        get
        {
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                return "DESC";
            }
            else
            {
                return "ASC";
            }
        }
    }

    public string GridViewSortExp
    {
        get
        {
            if (ViewState["GridViewSortExp"] == null)
                ViewState["GridViewSortExp"] = "Lines";
            return (string)ViewState["GridViewSortExp"];
        }
        set { ViewState["GridViewSortExp"] = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ucCountry.CurrentPage = this;
        pager1.PageSize = 50;
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindVendorPOCdetails(currnetPageIndx);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindModule();
            ddlCaptured.Enabled = false;
        }
    }

    public override void CountryPost_Load()
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("CountryID") != null)
            {
                ucCountry.innerControlddlCountry.SelectedIndex = ucCountry.innerControlddlCountry.Items.IndexOf(ucCountry.innerControlddlCountry.Items.FindByValue(GetQueryStringValue("CountryID")));
                BindVendorPOCdetails();
            }
        }
    }

    private void BindVendorPOCdetails(int Page = 1)
    {
        PointsofContactSummaryBE oPointsofContactSummaryBE = new PointsofContactSummaryBE();
        PointsofContactSummaryBAL oPointsofContactSummaryBAL = new PointsofContactSummaryBAL();
        List<PointsofContactSummaryBE> lstPOC = new List<PointsofContactSummaryBE>();

        oPointsofContactSummaryBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
        oPointsofContactSummaryBE.Module = Convert.ToString(ddlModule.SelectedItem.Value);
        oPointsofContactSummaryBE.Captured = Convert.ToString(ddlCaptured.SelectedItem.Value);
        //new changes
        //oPointsofContactSummaryBE.NoOfLines = txtNoLines.Text;
        //oPointsofContactSummaryBE.PoCreated = txtPOCreatedDate.innerControltxtDate.Value != "" ? Common.GetYYYY_MM_DD(txtPOCreatedDate.innerControltxtDate.Value) : "";


        oPointsofContactSummaryBE.IsOpen = rblVendors.SelectedValue == "0" ? false : true;
        oPointsofContactSummaryBE.IsExcludeVendor = chkExcludeVendors.Checked == true ? true : false;
        oPointsofContactSummaryBE.IsAll = rblVendors.SelectedValue == "0" ? -1 : 0;

        if (oPointsofContactSummaryBE.Module == "Scheduling" || oPointsofContactSummaryBE.Module == "Discrepancy")
        {

            spCountry.Visible = false;
            spSite.Visible = true;

            ltPOC.Text = WebCommon.getGlobalResourceValue(oPointsofContactSummaryBE.Module);

            List<PointsofContactSummaryBE> oPointsofContactDetailBEList = new List<PointsofContactSummaryBE>();
            List<PointsofContactSummaryBE> oPointsofContactdetailSiteBEList = new List<PointsofContactSummaryBE>();

            if (ViewState["oPointsofContactdetailSiteBEList"] == null)
            {
                oPointsofContactSummaryBE.Action = "GetReportDetails" + oPointsofContactSummaryBE.Module;
                Dictionary<string, List<PointsofContactSummaryBE>> myLists = oPointsofContactSummaryBAL.GetPOCDetailsSiteBAL(oPointsofContactSummaryBE);

                oPointsofContactDetailBEList = myLists["PointsofContactDetailBEList"];
                oPointsofContactdetailSiteBEList = myLists["PointsofContactdetailSiteBEList"];

                ViewState["oPointsofContactDetailBEList"] = oPointsofContactDetailBEList;
                ViewState["oPointsofContactdetailSiteBEList"] = oPointsofContactdetailSiteBEList;
            }
            else
            {
                oPointsofContactDetailBEList = (List<PointsofContactSummaryBE>)ViewState["oPointsofContactDetailBEList"];
                oPointsofContactdetailSiteBEList = (List<PointsofContactSummaryBE>)ViewState["oPointsofContactdetailSiteBEList"];
            }


            pager1.ItemCount = oPointsofContactDetailBEList.Count;
            oPointsofContactDetailBEList = oPointsofContactDetailBEList.Skip((50 * (Page - 1))).Take((50)).ToList();
            pager1.CurrentIndex = Page;

            rptSiteHeader.DataSource = oPointsofContactdetailSiteBEList.OrderBy(o => o.SiteName).Select(c => new { c.SiteName }).Distinct().ToList();
            rptSiteHeader.DataBind();

            rptPOReports.DataSource = oPointsofContactDetailBEList;
            rptPOReports.DataBind();

            if (oPointsofContactDetailBEList != null && oPointsofContactDetailBEList.Count > 0)
            {
                btnExportToExcel.Visible = true;
                pager1.Visible = true;
            }
            else
            {
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
            }

        }
        else
        {

            spCountry.Visible = true;
            spSite.Visible = false;

            oPointsofContactSummaryBE.Action = "GetReportDetails";
            lstPOC = oPointsofContactSummaryBAL.GetPOCDetailsBAL(oPointsofContactSummaryBE);

            if (lstPOC != null && lstPOC.Count > 0)
            {
                ViewState["lstPOC"] = lstPOC;
                SortGridView(GridViewSortExp, GridViewSortDirection);
                btnExportToExcel.Visible = true;
            }
            else
            {
                ViewState["lstPOC"] = null;
                gvVendorPOC.DataSource = null;
                gvVendorPOC.DataBind();
                btnExportToExcel.Visible = false;
            }
        }
    }

    private void BindModule()
    {
        ddlModule.Items.Add(new ListItem("ALL", "ALL"));
        ddlModule.Items.Add(new ListItem("Scheduling", "Scheduling"));
        ddlModule.Items.Add(new ListItem("Discrepancies", "Discrepancy"));
        ddlModule.Items.Add(new ListItem("OTIF", "OTIF"));
        ddlModule.Items.Add(new ListItem("Scorecard", "Scorecard"));
        ddlModule.Items.Add(new ListItem("Inventory", "Inventory"));
        ddlModule.Items.Add(new ListItem("Procurement", "Procurement"));
        ddlModule.Items.Add(new ListItem("Accounts Payable", "AccountsPayable"));
        ddlModule.Items.Add(new ListItem("Executive", "Executive"));
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        GridViewSortExp = e.SortExpression;
        if (SortDir == "ASC")
        {
            GridViewSortDirection = SortDirection.Ascending;
        }
        else
        {
            GridViewSortDirection = SortDirection.Descending;
        }
        return Utilities.GenericListHelper<PointsofContactSummaryBE>.SortList((List<PointsofContactSummaryBE>)ViewState["lstPOC"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void gvVendorPOC_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int iCount = 1; iCount < e.Row.Cells.Count; iCount++)
                e.Row.Cells[iCount].Text = e.Row.Cells[iCount].Text + "%";
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        gvVendorPOC.PageIndex = 0;
        GridViewSortExp = "Lines";
        GridViewSortDirection = SortDirection.Descending;

        ViewState["oPointsofContactDetailBEList"] = null;
        ViewState["oPointsofContactdetailSiteBEList"] = null;

        BindVendorPOCdetails();

    }

    protected void ddlModule_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlCaptured.SelectedIndex = 0;
        if (ddlModule.SelectedItem.Value == "ALL")
        {
            ddlCaptured.Enabled = false;
        }
        else
        {
            ddlCaptured.Enabled = true;
        }
    }

    protected void gvVendorPOC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvVendorPOC.PageIndex = e.NewPageIndex;
        SortGridView(GridViewSortExp, GridViewSortDirection);
    }

    private void SortGridView(string sortExpression, SortDirection direction)
    {
        List<PointsofContactSummaryBE> lstPOC = new List<PointsofContactSummaryBE>();
        try
        {
            if (ViewState["lstPOC"] != null)
            {

                lstPOC = Utilities.GenericListHelper<PointsofContactSummaryBE>.SortList((List<PointsofContactSummaryBE>)ViewState["lstPOC"], sortExpression, direction);


                gvVendorPOC.DataSource = lstPOC;
                gvVendorPOC.DataBind();

                CommonPage c = new CommonPage();
                c.LocalizeGridHeader(gvVendorPOC);
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    private void BindVendorPOCdetailsExport()
    {
        PointsofContactSummaryBE oPointsofContactSummaryBE = new PointsofContactSummaryBE();
        PointsofContactSummaryBAL oPointsofContactSummaryBAL = new PointsofContactSummaryBAL();

        if (Convert.ToString(ddlModule.SelectedItem.Value) == "Scheduling" || Convert.ToString(ddlModule.SelectedItem.Value) == "Discrepancy")
        {


            ltPOCExport.Text = WebCommon.getGlobalResourceValue(Convert.ToString(ddlModule.SelectedItem.Value));

            List<PointsofContactSummaryBE> oPointsofContactDetailBEList = new List<PointsofContactSummaryBE>();
            List<PointsofContactSummaryBE> oPointsofContactdetailSiteBEList = new List<PointsofContactSummaryBE>();

            if (ViewState["oPointsofContactdetailSiteBEList"] != null)
            {

                oPointsofContactDetailBEList = (List<PointsofContactSummaryBE>)ViewState["oPointsofContactDetailBEList"];
                oPointsofContactdetailSiteBEList = (List<PointsofContactSummaryBE>)ViewState["oPointsofContactdetailSiteBEList"];
            }


            rptSiteHeaderExport.DataSource = oPointsofContactdetailSiteBEList.OrderBy(o => o.SiteName).Select(c => new { c.SiteName }).Distinct().ToList();
            rptSiteHeaderExport.DataBind();

            rptPOCReportsExport.DataSource = oPointsofContactDetailBEList;
            rptPOCReportsExport.DataBind();

        }

    }

    protected void btnExport_Click(object sender, EventArgs e)
    {


        List<PointsofContactSummaryBE> lstPOC = new List<PointsofContactSummaryBE>();
        try
        {
            if (Convert.ToString(ddlModule.SelectedItem.Value) == "Scheduling" || Convert.ToString(ddlModule.SelectedItem.Value) == "Discrepancy")
            {

                BindVendorPOCdetailsExport();

                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "application/force-download";
                Response.AddHeader("content-disposition", "attachment; filename=POCSitewiseReport.xls");
                Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
                Response.Write("<head>");
                Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
                Response.Write("<!--[if gte mso 9]><xml>");
                Response.Write("<x:ExcelWorkbook>");
                Response.Write("<x:ExcelWorksheets>");
                Response.Write("<x:ExcelWorksheet>");
                Response.Write("<x:Name>POC report</x:Name>");
                Response.Write("<x:WorksheetOptions>");
                Response.Write("<x:Print>");
                Response.Write("<x:ValidPrinterInfo/>");
                Response.Write("</x:Print>");
                Response.Write("</x:WorksheetOptions>");
                Response.Write("</x:ExcelWorksheet>");
                Response.Write("</x:ExcelWorksheets>");
                Response.Write("</x:ExcelWorkbook>");
                Response.Write("</xml>");
                Response.Write("<![endif]--> ");
                StringWriter tw = new StringWriter(); HtmlTextWriter hw = new HtmlTextWriter(tw); tblSite.RenderControl(hw);
                Response.Write(tw.ToString());
                Response.Write("</head>");
                Response.End();


            }
            else
            {
                if (ViewState["lstPOC"] != null)
                {

                    lstPOC = Utilities.GenericListHelper<PointsofContactSummaryBE>.SortList((List<PointsofContactSummaryBE>)ViewState["lstPOC"], GridViewSortExp, GridViewSortDirection);

                    gvVendorPOCExport.DataSource = lstPOC;
                    gvVendorPOCExport.DataBind();

                    CommonPage c = new CommonPage();
                    c.LocalizeGridHeader(gvVendorPOCExport);

                    WebCommon.Export("VendorPOCDetails", gvVendorPOCExport);
                }
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }


    protected void rptPOCReports_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            List<PointsofContactSummaryBE> oPointsofContactdetailSiteBEList = (List<PointsofContactSummaryBE>)ViewState["oPointsofContactdetailSiteBEList"];

            Repeater rptSiteValue = (Repeater)e.Item.FindControl("rptSiteValue");

            int POCtrackerDetailID = ((PointsofContactSummaryBE)e.Item.DataItem).POCtrackerDetailID;

            if (Convert.ToString(ddlModule.SelectedItem.Value) == "Scheduling")
            {
                rptSiteValue.DataSource = oPointsofContactdetailSiteBEList.Where(w => w.POCtrackerDetailID == POCtrackerDetailID).OrderBy(o => o.SiteName).Select(c => new { POC = c.SchedulingPOC }).ToList();

            }
            else if (Convert.ToString(ddlModule.SelectedItem.Value) == "Discrepancy")
            {
                rptSiteValue.DataSource = oPointsofContactdetailSiteBEList.Where(w => w.POCtrackerDetailID == POCtrackerDetailID).OrderBy(o => o.SiteName).Select(c => new { POC = c.DiscrepancyPOC }).ToList();
            }
            rptSiteValue.DataBind();
        }
    }
}
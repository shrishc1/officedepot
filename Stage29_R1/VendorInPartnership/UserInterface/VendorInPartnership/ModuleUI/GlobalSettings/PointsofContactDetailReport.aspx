﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" AutoEventWireup="true" CodeFile="PointsofContactDetailReport.aspx.cs" Inherits="ModuleUI_GlobalSettings_PointsofContactDetailReport" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry" TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblPointsofContactDetailReport" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <div class="right-shadow">
                <div class="formbox">
                    <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold;" width="5%">
                                <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;" width="1%" align="center">:
                            </td>
                            <td style="font-weight: bold;" width="29%">
                                <uc1:ucCountry ID="ucCountry" runat="server" />
                            </td>

                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor">
                                </cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                            </td>
                        </tr>
                        <tr>


                            <td style="font-weight: bold;" width="1%" align="center"></td>
                            <td style="font-weight: bold;" width="10%"></td>
                            <td width="10%">
                                <cc1:ucButton ID="btnSearch" runat="server" CssClass="button" OnClick="btnSearch_Click" />
                            </td>
                            <td width="15%">

                                <cc1:ucButton ID="btnExportToExcel"
                                    runat="server"
                                    CssClass="exporttoexcel"
                                    OnClick="btnExport_Click"
                                    Width="109px" Height="20px" Visible="false" />
                            </td>
                        </tr>
                    </table>

                    <br />

                    <table width="100%">
                        <tr>
                            <td align="center">

                                <cc1:ucGridView ID="gvPointOfContact" runat="server"
                                    AutoGenerateColumns="false" CssClass="grid gvclass" CellPadding="0" Width="960px"
                                    PageSize="20" AllowPaging="true" OnPageIndexChanging="gvVendorPOC_PageIndexChanging">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">
                                            <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                        </div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:BoundField HeaderText="Vendor Name" DataField="Vendor_Name" SortExpression="VendorName">
                                            <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="POCType" DataField="POCType" SortExpression="POCType">
                                            <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="UserName" DataField="UserName" SortExpression="UserName">
                                            <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="EmailId" DataField="EmailId" SortExpression="EmailId">
                                            <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                </cc1:ucGridView>

                                <cc1:ucGridView ID="gvPointOfContactReport" runat="server" AutoGenerateColumns="false"
                                    CssClass="grid gvclass" CellPadding="0" Width="960px" Style="display: none;">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">
                                            <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                        </div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:BoundField HeaderText="Vendor Name" DataField="Vendor_Name" SortExpression="VendorName">
                                            <HeaderStyle Width="260px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="POCType" DataField="POCType" SortExpression="POCType">
                                            <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="UserName" DataField="UserName" SortExpression="UserName">
                                            <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="EmailId" DataField="EmailId" SortExpression="EmailId">
                                            <HeaderStyle Width="70px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                </cc1:ucGridView>

                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

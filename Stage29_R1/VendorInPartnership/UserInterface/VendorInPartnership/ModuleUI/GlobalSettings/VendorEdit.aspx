﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VendorEdit.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    Inherits="VendorEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblVendorSetup" runat="server" Text="Vendor Set Up"></cc1:ucLabel>
    </h2>
    <div class="formbox right-shadow">
        <cc1:ucPanel ID="pnlVendorDetail" runat="server" CssClass="fieldset-form">
            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                <tr>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td style="width: 15%;">
                                    <cc1:ucLabel ID="lblVendorNumber" runat="server" Text="Vendor #" />
                                </td>
                                <td style="width: 1%;">:
                                </td>
                                <td class="nobold" style="width: 14%;">
                                    <cc1:ucLabel ID="lblVendorNumberData" runat="server" />
                                </td>
                                <td style="width: 17%;">
                                    <cc1:ucLabel ID="lblVendorName" runat="server" Text="Vendor Name" />
                                </td>
                                <td style="width: 1%;">:
                                </td>
                                <td class="nobold" style="width: 18%;">
                                    <cc1:ucLabel ID="lblVendorNameData" runat="server" />
                                </td>
                                <td style="width: 18%;">
                                    <cc1:ucLabel ID="lblVendorCountry" runat="server" Text="Country" />
                                </td>
                                <td style="width: 1%;">:
                                </td>
                                <td class="nobold" style="width: 15%;">
                                    <cc1:ucLabel ID="lblVendorCountryData" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:ucLabel ID="lblDefaultLanguage" runat="server" Text="Default Language" />
                                </td>
                                <td>:
                                </td>
                                <td class="nobold">
                                    <cc1:ucLabel ID="lblDefaultLanguageData" runat="server" />
                                </td>
                                <td>
                                    <cc1:ucLabel ID="lblDefaultPhoneNumber" runat="server" Text="Default Phone #" />
                                </td>
                                <td>:
                                </td>
                                <td class="nobold">
                                    <cc1:ucLabel ID="lblDefaultPhoneNumberData" runat="server" />
                                </td>
                                <td>
                                    <cc1:ucLabel ID="lblDefaultFaxNumber" runat="server" Text="Default Fax #" />
                                </td>
                                <td>:
                                </td>
                                <td class="nobold">
                                    <cc1:ucLabel ID="lblDefaultFaxNumberData" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </cc1:ucPanel>
        <cc1:ucPanel ID="pnlStockPlanner" runat="server" CssClass="fieldset-form">
            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                <tr>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td style="width: 15%;">
                                    <cc1:ucLabel ID="lblStockPlannerNumber" runat="server" Text="Stock Planner #"></cc1:ucLabel>
                                </td>
                                <td style="width: 1%;">:
                                </td>
                                <td class="nobold" style="width: 14%;">
                                    <cc1:ucLabel ID="lblStockPlannerNumberData" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 17%;">
                                    <cc1:ucLabel ID="lblStockPlannerName" runat="server" Text="Stock Planner Name"></cc1:ucLabel>
                                </td>
                                <td style="width: 1%;">:
                                </td>
                                <td class="nobold" style="width: 18%;">
                                    <cc1:ucLabel ID="lblStockPlannerNameData" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="width: 18%;">
                                    <cc1:ucLabel ID="lblPlannerContactNo" runat="server" Text="Stock Planner Contact #"></cc1:ucLabel>
                                </td>
                                <td style="width: 1%;">:
                                </td>
                                <td class="nobold" style="width: 15%;">
                                    <cc1:ucLabel ID="lblPlannerContactNoData" runat="server"></cc1:ucLabel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </cc1:ucPanel>
        <cc1:ucPanel ID="pnlAccountsPayable" runat="server" CssClass="fieldset-form">
            <table width="100%" cellspacing="5" cellpadding="0" class="form-table">
                <tr>
                    <td style="width: 100%;">
                        <asp:Repeater ID="rptAccountPayable" runat="server">
                            <ItemTemplate>
                                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="width: 15%;" valign="top">
                                            <cc1:ucLabel ID="lblAccountsPayableName" runat="server" Text="Accounts Payable Name"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 1%;" valign="top">:
                                        </td>
                                        <td class="nobold" style="width: 14%;" valign="top">
                                            <cc1:ucLiteral ID="lblAccountsPayableNameT" runat="server" Text='<%#Eval("UserName") %>'></cc1:ucLiteral>
                                        </td>
                                        <td style="width: 17%;" valign="top">
                                            <cc1:ucLabel ID="lblAccountsPayableContact" runat="server" Text="Accounts Payable Contact"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 1%;" valign="top">:
                                        </td>
                                        <td class="nobold" style="width: 18%;" valign="top">
                                            <cc1:ucLiteral ID="ltAccountsPayableContactT" runat="server" Text='<%#Eval("User.PhoneNumber") %>'></cc1:ucLiteral>
                                        </td>
                                        <td style="width: 18%;" valign="top">
                                            <%--<cc1:ucLabel ID="lblAccountsPayableHash" runat="server" Text="Accounts Payable #"></cc1:ucLabel>--%>
                                        </td>
                                        <td style="width: 1%;" valign="top">
                                            <%--:--%>
                                        </td>
                                        <td class="nobold" style="width: 15%;" valign="top">
                                            <%--<cc1:ucLiteral ID="ltAccountsPayableHashT" runat="server" Text='<%#Eval("UserID") %>'></cc1:ucLiteral>--%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width: 15%;" valign="top">
                                            <cc1:ucLabel ID="lblAccountsPayableName" runat="server" Text="Accounts Payable Name"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 1%;" valign="top">:
                                        </td>
                                        <td class="nobold" style="width: 14%;" valign="top">
                                            <cc1:ucLiteral ID="lblAccountsPayableNameT" runat="server" Text='<%#Eval("UserName") %>'></cc1:ucLiteral>
                                        </td>
                                        <td style="width: 17%;" valign="top">
                                            <cc1:ucLabel ID="lblAccountsPayableContact" runat="server" Text="Accounts Payable Contact"></cc1:ucLabel>
                                        </td>
                                        <td style="width: 1%;" valign="top">:
                                        </td>
                                        <td class="nobold" style="width: 18%;" valign="top">
                                            <cc1:ucLiteral ID="ltAccountsPayableContactT" runat="server" Text='<%#Eval("User.PhoneNumber") %>'></cc1:ucLiteral>
                                        </td>
                                        <td style="width: 18%;" valign="top">
                                            <%--<cc1:ucLabel ID="lblAccountsPayableHash" runat="server" Text="Accounts Payable #"></cc1:ucLabel>--%>
                                        </td>
                                        <td style="width: 1%;" valign="top">
                                            <%--:--%>
                                        </td>
                                        <td class="nobold" style="width: 15%;" valign="top">
                                            <%--<cc1:ucLiteral ID="ltAccountsPayableHashT" runat="server" Text='<%#Eval("UserID") %>'></cc1:ucLiteral>--%>
                                        </td>
                                    </tr>
                                </table>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
        </cc1:ucPanel>

        <asp:UpdatePanel ID="upPenaltyCharges" runat="server">
            <ContentTemplate>
                <cc1:ucPanel ID="pnlPenaltyCharges" runat="server" GroupingText="Penalty Charges" CssClass="fieldset-form">
                    <table border="0" width="95%" cellspacing="10" cellpadding="0" class="form-table">
                        <tr>
                            <td style="width: 12%;" valign="top">
                                <cc1:ucLabel ID="lblIsVendorSubscribetoPenaltyCharge" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="width: 18%;" valign="top" colspan="2">
                                <cc1:ucRadioButton ID="rdoYes" runat="server" Enabled="false"></cc1:ucRadioButton>
                                &nbsp;&nbsp;
                                       
                                             <cc1:ucRadioButton ID="rdoNo" runat="server" Enabled="false"></cc1:ucRadioButton>
                            </td>
                        </tr>

                        <tr>
                            <td valign="top">
                                <cc1:ucRadioButton ID="rdoIsPenaltyBO" runat="server" Enabled="false" Visible="false"></cc1:ucRadioButton>
                            </td>
                            <td colspan="2" valign="top">
                                <cc1:ucRadioButton ID="rdoIsPenaltyOTIF" runat="server" Enabled="false" Visible="false"></cc1:ucRadioButton>
                            </td>
                        </tr>

                        <tr>
                            <td valign="top">
                                <cc1:ucLabel ID="lblValueperBO" runat="server" Visible="false"></cc1:ucLabel>&nbsp;&nbsp;
                                          <cc1:ucTextbox ID="txtValueBO" runat="server" Height="100%" Width="12%" Enabled="false" Visible="false"></cc1:ucTextbox>
                                <cc1:ucLabel ID="lblValueperOTIF" runat="server" Text="OTIF % below which a penalty will be applied" Visible="false"></cc1:ucLabel>&nbsp;&nbsp;
                                          <cc1:ucTextbox ID="txtValueOTIF" runat="server" Height="100%" Width="12%" Enabled="false" Visible="false"></cc1:ucTextbox>&nbsp;&nbsp;<br />
                                <br />
                                &nbsp;&nbsp;
                                          <cc1:ucLabel ID="lblValueOTIFLine" runat="server" Text="% of line value if penalty is to be applied" Visible="false"></cc1:ucLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          <cc1:ucTextbox ID="txtValueOTIFLine" runat="server" Height="100%" Width="12%" Enabled="false" Visible="false"></cc1:ucTextbox>
                            </td>
                            <td valign="top">
                                <cc1:ucLabel ID="lblCurrency_1" runat="server" Visible="false"></cc1:ucLabel>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <cc1:ucDropdownList ID="ddlCurrencyList" runat="server" Width="120px" Enabled="false" Visible="false">
                                            </cc1:ucDropdownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            
                            </td>
                            <td valign="top">
                                <cc1:ucButton ID="btnEdit_1" runat="server" CssClass="button"
                                    OnClick="btnPenaltyChargesEdit_Click" Text="EDIT" />

                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnEdit_1" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="upOverstock" runat="server">
            <ContentTemplate>
                <cc1:ucPanel ID="pnlOverstock" runat="server" GroupingText="Overstock" CssClass="fieldset-form">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" class="form-table">
                        <tr>
                            <td style="width: 29%;" valign="top">
                                <cc1:ucLabel ID="lblIsVendorOverstockAgreement" runat="server"></cc1:ucLabel>
                            </td>
                            <td valign="top">
                                <cc1:ucRadioButton ID="rdoYes_1" runat="server" GroupName="abcd" Enabled="false"></cc1:ucRadioButton>
                                &nbsp;&nbsp;
                                       
                                             <cc1:ucRadioButton ID="rdoNo_1" runat="server" GroupName="abcd" Enabled="false"></cc1:ucRadioButton>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 29%;" valign="top">
                                <cc1:ucLabel ID="lblDetail" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucTextbox ID="txtOverstockDetails" runat="server" Height="100%" Width="92%" Enabled="false"></cc1:ucTextbox>&nbsp;&nbsp;
                                           <cc1:ucButton ID="btnEdit_2" runat="server" CssClass="button"
                                               OnClick="btnOverstockDetailsEdit_Click" Text="EDIT" />
                            </td>
                        </tr>


                    </table>
                </cc1:ucPanel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnEdit_2" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <cc1:ucPanel ID="pnlReturns" runat="server" GroupingText="Returns" CssClass="fieldset-form">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" class="form-table">
                        <tr>
                            <td style="width: 28%;" valign="top">
                                <cc1:ucLabel ID="lblIsVendorReturnsAgreement" runat="server"></cc1:ucLabel>
                            </td>
                            <td valign="top">
                                <cc1:ucRadioButton ID="rdoYes_2" runat="server" GroupName="abc" Enabled="false"></cc1:ucRadioButton>
                                &nbsp;&nbsp;
                                       
                                             <cc1:ucRadioButton ID="rdoNo_2" runat="server" GroupName="abc" Enabled="false"></cc1:ucRadioButton>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 28%;" valign="top">
                                <cc1:ucLabel ID="lblDetail_1" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <cc1:ucTextbox ID="txtReturnsDetails" runat="server" Height="100%" Width="92%" Enabled="false"></cc1:ucTextbox>&nbsp;&nbsp;
                                            <cc1:ucButton ID="btnEdit_3" runat="server" CssClass="button"
                                                OnClick="btnReturnsDetailsEdit_Click" Text="EDIT" />
                            </td>
                        </tr>


                    </table>
                </cc1:ucPanel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnEdit_3" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>


        <cc1:ucPanel ID="pnlVendorPointsOfContact" runat="server" CssClass="fieldset-form">
            <table width="100%" cellspacing="5" cellpadding="0" align="center" class="top-settingsNoBorder">
                <thead>
                    <tr>
                        <td style="width: 20%; text-align: center; font-weight: bold;">
                            <cc1:ucLabel ID="lblAppointmentScheduling" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 20%; text-align: center; font-weight: bold;">
                            <cc1:ucLabel ID="lblDiscrepancies" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 20%; text-align: center; font-weight: bold;">
                            <cc1:ucLabel ID="lblInvoiceIssue" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 20%; text-align: center; font-weight: bold;">
                            <cc1:ucLabel ID="lblOTIF" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 20%; text-align: center; font-weight: bold;">
                            <cc1:ucLabel ID="lblScorecard" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td valign="top">
                            <cc1:ucGridView ID="gvSchedulingContact" Width="100%" CssClass="grid" runat="server"
                                AutoGenerateColumns="false"
                                OnRowDataBound="gvSchedulingContact_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name" HeaderStyle-Width="40%" SortExpression="VendorName"
                                        HeaderStyle-HorizontalAlign="Left">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HLSchedulingVendor" runat="server" Text='<%#Eval("UserName") %>'
                                                NavigateUrl='<%# Convert.ToString(Eval("AccountStatus")).ToLower()=="registered awaiting approval" ? (EncryptQuery("~/ModuleUI/Security/SCT_ExternalUserApprovalEdit.aspx?UserID="+ Eval("UserID")+"&VendorID="+GetQueryStringValue("VendorID"))) :(EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?UserID="+ Eval("UserID")+"&VendorID="+GetQueryStringValue("VendorID")))  %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Status" HeaderStyle-Width="60%" DataField="AccountStatus"
                                        HeaderStyle-HorizontalAlign="Left" />
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                        <td valign="top">
                            <cc1:ucGridView ID="gvDiscrepancyContact" Width="100%" CssClass="grid" runat="server"
                                AutoGenerateColumns="false"
                                OnRowDataBound="gvDiscrepancyContact_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name" HeaderStyle-Width="40%" SortExpression="VendorName">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlLoginID" runat="server" Text='<%#Eval("UserName") %>' NavigateUrl='<%# Convert.ToString(Eval("AccountStatus")).ToLower()=="registered awaiting approval" ? (EncryptQuery("~/ModuleUI/Security/SCT_ExternalUserApprovalEdit.aspx?UserID="+ Eval("UserID")+"&VendorID="+GetQueryStringValue("VendorID"))) :(EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?UserID="+ Eval("UserID")+"&VendorID="+GetQueryStringValue("VendorID")))  %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Status" HeaderStyle-Width="60%" DataField="AccountStatus"
                                        HeaderStyle-HorizontalAlign="Left" />
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                        <td valign="top">
                            <cc1:ucGridView ID="gvInvoiceIssueContact" Width="100%" CssClass="grid" runat="server"
                                AutoGenerateColumns="false"
                                OnRowDataBound="gvInvoiceIssueContact_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name" HeaderStyle-Width="40%" SortExpression="VendorName">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlLoginID" runat="server" Text='<%#Eval("UserName") %>' NavigateUrl='<%# Convert.ToString(Eval("AccountStatus")).ToLower()=="registered awaiting approval" ? (EncryptQuery("~/ModuleUI/Security/SCT_ExternalUserApprovalEdit.aspx?UserID="+ Eval("UserID")+"&VendorID="+GetQueryStringValue("VendorID"))) :(EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?UserID="+ Eval("UserID")+"&VendorID="+GetQueryStringValue("VendorID")))  %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Status" HeaderStyle-Width="60%" DataField="AccountStatus"
                                        HeaderStyle-HorizontalAlign="Left" />
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                        <td valign="top">
                            <cc1:ucGridView ID="gvOTIFContact" Width="100%" CssClass="grid" runat="server"
                                AutoGenerateColumns="false" OnRowDataBound="gvOTIFContact_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name" HeaderStyle-Width="40%" SortExpression="VendorName">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HLOTIFVendor" runat="server" Text='<%#Eval("UserName") %>' NavigateUrl='<%# Convert.ToString(Eval("AccountStatus")).ToLower()=="registered awaiting approval" ?(EncryptQuery("~/ModuleUI/Security/SCT_ExternalUserApprovalEdit.aspx?UserID="+ Eval("UserID")+"&VendorID="+GetQueryStringValue("VendorID"))) :(EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?UserID="+ Eval("UserID")+"&VendorID="+GetQueryStringValue("VendorID")))  %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Status" HeaderStyle-Width="60%" DataField="AccountStatus"
                                        HeaderStyle-HorizontalAlign="Left" />
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                        <td valign="top">
                            <cc1:ucGridView ID="gvScorecardContact" Width="100%" CssClass="grid" runat="server"
                                AutoGenerateColumns="false" DataKeyNames="UserID"
                                OnRowDataBound="gvScorecardContact_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name" HeaderStyle-Width="40%" SortExpression="VendorName">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HLScorecardVendor" runat="server" Text='<%#Eval("UserName") %>'
                                                NavigateUrl='<%# Convert.ToString(Eval("AccountStatus")).ToLower()=="registered awaiting approval" ?(EncryptQuery("~/ModuleUI/Security/SCT_ExternalUserApprovalEdit.aspx?UserID="+ Eval("UserID")+"&VendorID="+GetQueryStringValue("VendorID"))) :(EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?UserID="+ Eval("UserID")+"&VendorID="+GetQueryStringValue("VendorID")))  %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Status" HeaderStyle-Width="60%" DataField="AccountStatus"
                                        HeaderStyle-HorizontalAlign="Left" />
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" cellspacing="5" cellpadding="0" align="center" class="top-settingsNoBorder">
                <thead>
                    <tr>
                        <td style="width: 25%; text-align: center; font-weight: bold;">
                            <cc1:ucLabel ID="lblInventoryContact" Text="Inventory Contact" runat="server"></cc1:ucLabel>&nbsp;
                            <asp:LinkButton class="showAddImage" Text="Add" ID="lnkbtnAdd" runat="server" OnClick="lnkbtnAdd_Click" />
                            <%--<a   id="showhide"  runat="server">hide</a>--%>
                        </td>
                        <td style="width: 25%; text-align: center; font-weight: bold;">
                            <cc1:ucLabel ID="lblProcurementContact" Text="Procurement Contact" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 25%; text-align: center; font-weight: bold;">
                            <cc1:ucLabel ID="lblAccountsPayableContact" Text="Accounts Payable Contact" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 25%; text-align: center; font-weight: bold;">
                            <cc1:ucLabel ID="lblExecutiveContact" Text="Executive Contact" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td valign="top">
                            <cc1:ucGridView ID="gvInventoryPOC" Width="100%" CssClass="grid" runat="server"
                                AutoGenerateColumns="false" OnRowDataBound="gvInventoryPOC_RowDataBound" ShowFooter="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name" SortExpression="VendorName" HeaderStyle-HorizontalAlign="Left">
                                        <HeaderStyle Width="17%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlInventoryPOC" runat="server" Text='<%# Eval("FirstName","") + " " + Eval("SecondName","") %>'
                                                NavigateUrl='<%# (!string.IsNullOrEmpty(GetQueryStringValue("PageFrom"))) ? GetQueryStringValue("PageFrom").Equals("UserList") ? (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID") + "&PreviousPageWas=UserList")) 
                                                    : (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID") + "&PreviousPageWas=VendorOverview")) 
                                                    : (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID"))) %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                        <%--<footertemplate>
                                             <asp:HyperLink ID="hlAddInventoryPOC" runat="server" Text="Add"
                                                NavigateUrl='<%# (!string.IsNullOrEmpty(GetQueryStringValue("PageFrom"))) ? GetQueryStringValue("PageFrom").Equals("UserList") ? (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID") + "&PreviousPageWas=UserList")) 
                                                    : (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID") + "&PreviousPageWas=VendorOverview")) 
                                                    : (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID"))) %>'>
                                         </asp:HyperLink>
                                                 </footertemplate> --%>                                      
                                    </asp:TemplateField>

                                </Columns>
                            </cc1:ucGridView>
                        </td>
                        <td valign="top">
                            <cc1:ucGridView ID="gvProcurementPOC" Width="100%" CssClass="grid" runat="server"
                                AutoGenerateColumns="false" OnRowDataBound="gvProcurementPOC_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name" SortExpression="VendorName" HeaderStyle-HorizontalAlign="Left">
                                        <HeaderStyle Width="17%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlProcurementPOC" runat="server" Text='<%# Eval("FirstName","") + " " + Eval("SecondName","") %>'
                                                NavigateUrl='<%# (!string.IsNullOrEmpty(GetQueryStringValue("PageFrom"))) ? GetQueryStringValue("PageFrom").Equals("UserList") ? (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID") + "&PreviousPageWas=UserList")) 
                                                    : (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID") + "&PreviousPageWas=VendorOverview")) 
                                                    : (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID"))) %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                        <td valign="top">
                            <cc1:ucGridView ID="gvMerchandisingPOC" Width="100%" CssClass="grid" runat="server"
                                AutoGenerateColumns="false"
                                OnRowDataBound="gvMerchandisingPOC_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name" SortExpression="VendorName" HeaderStyle-HorizontalAlign="Left">
                                        <HeaderStyle Width="17%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlMerchandisingPOC" runat="server" Text='<%# Eval("FirstName","") + " " + Eval("SecondName","") %>'
                                                NavigateUrl='<%# (!string.IsNullOrEmpty(GetQueryStringValue("PageFrom"))) ? GetQueryStringValue("PageFrom").Equals("UserList") ? (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID") + "&PreviousPageWas=UserList")) 
                                                    : (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID") + "&PreviousPageWas=VendorOverview")) 
                                                    : (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID"))) %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                        <td valign="top">
                            <cc1:ucGridView ID="gvExecutivePOC" Width="100%" CssClass="grid" runat="server"
                                AutoGenerateColumns="false" OnRowDataBound="gvExecutivePOC_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name" SortExpression="VendorName" HeaderStyle-HorizontalAlign="Left">
                                        <HeaderStyle Width="17%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlExecutivePOC" runat="server" Text='<%# Eval("FirstName","") + " " + Eval("SecondName","") %>'
                                                NavigateUrl='<%# (!string.IsNullOrEmpty(GetQueryStringValue("PageFrom"))) ? GetQueryStringValue("PageFrom").Equals("UserList") ? (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID") + "&PreviousPageWas=UserList")) 
                                                    : (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID") + "&PreviousPageWas=VendorOverview")) 
                                                    : (EncryptQuery("~/ModuleUI/GlobalSettings/MaintainVendorPointsofContactEdit.aspx?venPointId="+Eval("VendorPointID")+"&VendorID="+Eval("Vendor.VendorID"))) %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                </tbody>
            </table>
        </cc1:ucPanel>
        <cc1:ucPanel ID="pnlConsolidation" runat="server" GroupingText="Consolidation" CssClass="fieldset-form">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                <tr>
                    <td width="25%" valign="top">
                        <cc1:ucPanel ID="pnlConsolidationCode" runat="server" GroupingText="Consolidation Code"
                            CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="left" class="top-settingsNoBorder">
                                <tr>
                                    <td width="100%">
                                        <asp:HyperLink ID="hpConsolidatedCode" runat="server" NavigateUrl="#"></asp:HyperLink>
                                        <asp:Label ID="lblConsolidatedCode" runat="server" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                    <td width="25%" valign="top">
                        <cc1:ucPanel ID="pnlConsolidationName" runat="server" GroupingText="Consolidation Name"
                            CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="left" class="top-settingsNoBorder">
                                <tr>
                                    <td width="100%">
                                        <cc1:ucLiteral ID="ltConsolidationName" runat="server"></cc1:ucLiteral>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                    <td width="50%" valign="top">
                        <cc1:ucPanel ID="pnlLinkedVendors" runat="server" GroupingText="Linked Vendors" CssClass="fieldset-form">
                            <asp:Repeater ID="rptLinkedVendor" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="left" class="top-settingsNoBorder">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td width="100%">
                                            <% if (GetQueryStringValue("ReadOnly") == "1")
                                                {
                                                    if (GetQueryStringValue("PageFrom") == "VendorOverview")
                                                    {
                                            %>
                                            <asp:HyperLink ID="VendorLinkReadonly" runat="server" NavigateUrl='<%# EncryptQuery("VendorEdit.aspx?ReadOnly=1&VendorID="+Eval("VendorID")+"&PageComeFrom=VendorOverview") %>'
                                                Text='<%#Eval("VendorName") %>'></asp:HyperLink>
                                            <%
                                                }
                                                else if (GetQueryStringValue("PageFrom") == "UserList")
                                                {
                                            %>
                                            <asp:HyperLink ID="VendorLinkReadonly1" runat="server" NavigateUrl='<%# EncryptQuery("VendorEdit.aspx?ReadOnly=1&VendorID="+Eval("VendorID")+"&PageComeFrom=UserOverview") %>'
                                                Text='<%#Eval("VendorName") %>'></asp:HyperLink>
                                            <% }
                                                }

                                                else
                                                {
                                                    if (GetQueryStringValue("PageFrom") == "VendorOverview")
                                                    {
                                            %>
                                            <asp:HyperLink ID="VendorLink" runat="server" NavigateUrl='<%# EncryptQuery("VendorEdit.aspx?VendorID="+Eval("VendorID")+"&PageComeFrom=VendorOverview")%>'
                                                Text='<%#Eval("VendorName") %>'></asp:HyperLink>
                                            <%
                                                }
                                                else if (GetQueryStringValue("PageFrom") == "UserList")
                                                {
                                            %>
                                            <asp:HyperLink ID="VendorLink1" runat="server" NavigateUrl='<%# EncryptQuery("VendorEdit.aspx?VendorID="+Eval("VendorID")+"&PageComeFrom=UserOverview")%>'
                                                Text='<%#Eval("VendorName") %>'></asp:HyperLink>
                                            <% }

                                                } %> 
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </cc1:ucPanel>
                    </td>
                </tr>
            </table>
        </cc1:ucPanel>
        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
            <tr>
                <td valign="top" width="100%">
                    <cc1:ucPanel ID="pnlMPA" runat="server" GroupingText="Master Purchasing Agreement (MPA)"
                        CssClass="fieldset-form">
                        <cc1:ucLabel ID="lblMPAagreement" runat="server"></cc1:ucLabel><br />
                        <asp:HyperLink ID="lblMasterPurchasingAgreementLink" runat="server" Target="_blank"></asp:HyperLink>
                        <cc1:ucLabel ID="lblMPAagreementEdit" runat="server" Visible="false"></cc1:ucLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <cc1:ucButton ID="btnEdit" runat="server" CssClass="button"
                            OnClick="btnEdit_Click" Text="EDIT" />


                        <cc1:ucTextbox ID="txtMPAagreement" runat="server" Height="100%"
                            Visible="false" Width="68%"></cc1:ucTextbox>
                        <%--   <div class="button-row">--%><%--   </div>--%>
                        <%-- <div class="button-row">--%>
                        <cc1:ucButton ID="btnSave_1" runat="server" CssClass="button"
                            OnClick="btnSave_1_Click" Text="Save" Visible="false" />
                        &nbsp;&nbsp;&nbsp;
                        <cc1:ucButton ID="btn_back_1" runat="server" CssClass="button"
                            OnClick="btn_back_1_Click" Text="Back" Visible="false" />
                        <%-- </div>--%>
                    </cc1:ucPanel>
                </td>
            </tr>
        </table>
        <cc1:ucPanel ID="pnlAddress" runat="server" GroupingText="Address" CssClass="fieldset-form">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                <tr>
                    <td width="50%">
                        <cc1:ucPanel ID="pnlDefaultAddress" runat="server" GroupingText="Default Address"
                            CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="left" class="top-settingsNoBorder">
                                <tr>
                                    <td width="100%">
                                        <cc1:ucTextbox ID="txtDefaultAddress" runat="server" Width="98%" Height="150px" TextMode="MultiLine"
                                            Enabled="False"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                    <td width="50%">
                        <cc1:ucPanel ID="pnlReturnAddress" runat="server" GroupingText="Return Address" CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="left" class="top-settingsNoBorder">
                                <tr>
                                    <td width="100%">
                                        <cc1:ucTextbox ID="txtReturnAddress" runat="server" Width="98%" Height="150px" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                            TextMode="MultiLine"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <cc1:ucPanel ID="UcPanel1" runat="server" GroupingText="Delivery Instructions"
                            CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="left" class="top-settingsNoBorder">
                                <tr>
                                    <td width="100%">
                                        <cc1:ucTextbox ID="txtDeliveryInstructions" runat="server" Width="98%" Height="50px"
                                            onkeyup="checkTextLengthOnKeyUp(this,100);" MaxLength="100" TextMode="MultiLine"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                    <td width="50%">
                        <cc1:ucPanel ID="UcPanel2" runat="server" GroupingText="Payment Terms" CssClass="fieldset-form">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="left" class="top-settingsNoBorder">
                                <tr>
                                    <td width="100%">
                                        <cc1:ucTextbox ID="txtPaymentTerms" runat="server" Width="98%" Height="50px" MaxLength="100"
                                            TextMode="MultiLine"
                                            onkeyup="checkTextLengthOnKeyUp(this,100);"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                </tr>

                <tr>
                    <td align="right" colspan="2">
                        <cc1:ucButton ID="btnSave" runat="server" CssClass="button" Text="Save" Visible="false" OnClick="btnSave_Click" />
                    </td>
                </tr>
            </table>
        </cc1:ucPanel>
        <asp:UpdatePanel ID="up3" runat="server">
            <ContentTemplate>
                <cc1:ucPanel ID="pnlCurrencySettings" runat="server" GroupingText="Currency Settings" CssClass="fieldset-form">
                    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                        <tr>
                            <td style="font-weight: bold; text-align: left;">
                                <cc1:ucLabel ID="lblPleaseSelectCurrency_1" isRequired="true" runat="server"></cc1:ucLabel>&nbsp;&nbsp;:&nbsp;&nbsp;
                                <cc1:ucDropdownList ID="ddlCurrency" runat="server" Width="170px">
                                </cc1:ucDropdownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <cc1:ucButton ID="btnSave_2" runat="server" CssClass="button"
                                    OnClick="btnSave_2_Click" />
                            </td>

                        </tr>
                        <%-- <tr>
                        <td style="font-weight: bold; text-align: left;">
                                 <cc1:ucLabel ID="lblDefaultCurrency" isRequired="true" runat="server"></cc1:ucLabel>&nbsp;&nbsp;:&nbsp;&nbsp;
                                  <cc1:ucLabel ID="lblShowDefaultCurrency" isRequired="true" runat="server"></cc1:ucLabel>

                             </td>
                        </tr>--%>
                    </table>
                </cc1:ucPanel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSave_2" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <%-- <asp:UpdatePanel ID="up2" runat="server">
            <ContentTemplate>
                <cc1:ucPanel ID="pnlAppointmentScheduling" runat="server" CssClass="fieldset-form">
                    <table width="80%" align="left" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                        <tr>
                            <td style="font-weight: bold; text-align: left;">
                                <cc1:ucLabel ID="lblCountry_1" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                                &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
                            </td>
                            <td colspan="3">
                                <uc1:ucCountry ID="ucSiteCountry" runat="server" width="170px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; text-align: center;" colspan="4">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="left" style="font-weight: bold">
                                <cc1:ucLabel ID="lblDisabled" runat="server" Text="Disabled"></cc1:ucLabel>
                            </td>
                            <td>
                            </td>
                            <td align="left" style="font-weight: bold">
                                <cc1:ucLabel ID="lblEnabled" runat="server" Text="Enabled"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;" width="10%">
                                <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                &nbsp;&nbsp;&nbsp;:
                            </td>
                            <td width="30%">
                                <cc1:ucListBox ID="UclstLeftSite" runat="server" Height="200px" Width="200px">
                                </cc1:ucListBox>
                            </td>
                            <td valign="middle" align="center" width="10%">
                                <div>
                                    <cc1:ucButton ID="btnMoveRightAll" runat="server" Text=">>" CssClass="button" Width="35px"
                                        OnClick="btnMoveRightAll_Click" />
                                </div>
                                &nbsp;
                                <div>
                                    <cc1:ucButton ID="btnMoveRight" runat="server" Text=">" CssClass="button" Width="35px"
                                        OnClick="btnMoveRight_Click" />
                                </div>
                                &nbsp;
                                <div>
                                    <cc1:ucButton ID="btnMoveLeft" runat="server" Text="<" CssClass="button" Width="35px"
                                        OnClick="btnMoveLeft_Click" />
                                </div>
                                &nbsp;
                                <div>
                                    <cc1:ucButton ID="btnMoveLeftAll" runat="server" Text="<<" CssClass="button" Width="35px"
                                        OnClick="btnMoveLeftAll_Click" />
                                </div>
                            </td>
                            <td width="50%">
                                <cc1:ucListBox ID="UclstRightSite" runat="server" Height="200px" Width="200px">
                                </cc1:ucListBox>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnMoveRightAll" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnMoveRight" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnMoveLeft" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnMoveLeftAll" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>--%>
        <div class="button-row">
            <table width="100%">
                <tr align="right">
                    <td style="padding-right: 10px;">
                        <cc1:ucButton ID="btnProceed" runat="server" CssClass="button" Text="Proceed"
                            OnClick="btnProceed_Click" />

                        <cc1:ucButton ID="btnBack" runat="server" CssClass="button" Text="Back" OnClick="btnBack_Click" />
                    </td>
                </tr>
            </table>

        </div>
    </div>
    <script type="text/javascript">

        // debugger;
        var h1, h2, h3, h4;
        h1 = $('#ctl00_ContentPlaceHolder1_gvSchedulingContact').height();
        h2 = $('#ctl00_ContentPlaceHolder1_gvDiscrepancyContact').height();
        h3 = $('#ctl00_ContentPlaceHolder1_gvOTIFContact').height();
        h4 = $('#ctl00_ContentPlaceHolder1_gvScorecardContact').height();
        var array = new Array();
        array.push(h1);
        array.push(h2);
        array.push(h3);
        array.push(h4);
        array.sort(function (a, b) { return a - b });
        array.reverse();

        $('#ctl00_ContentPlaceHolder1_gvSchedulingContact').height(array[0]);
        $('#ctl00_ContentPlaceHolder1_gvDiscrepancyContact').height(array[0]);
        $('#ctl00_ContentPlaceHolder1_gvOTIFContact').height(array[0]);
        $('#ctl00_ContentPlaceHolder1_gvScorecardContact').height(array[0]);

        h1, h2, h3, h4 = null;

        array = null;
        array = new Array();

        h1 = $('#ctl00_ContentPlaceHolder1_gvInventoryPOC').height();
        h2 = $('#ctl00_ContentPlaceHolder1_gvProcurementPOC').height();
        h3 = $('#ctl00_ContentPlaceHolder1_gvMerchandisingPOC').height();
        h4 = $('#ctl00_ContentPlaceHolder1_gvExecutivePOC').height();

        array.push(h1);
        array.push(h2);
        array.push(h3);
        array.push(h4);
        array.sort(function (a, b) { return a - b });
        array.reverse();

        $('#ctl00_ContentPlaceHolder1_gvInventoryPOC').height(array[0]);
        $('#ctl00_ContentPlaceHolder1_gvProcurementPOC').height(array[0]);
        $('#ctl00_ContentPlaceHolder1_gvMerchandisingPOC').height(array[0]);
        $('#ctl00_ContentPlaceHolder1_gvExecutivePOC').height(array[0]);

    </script>

</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="head">
</asp:Content>

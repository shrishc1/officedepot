﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Collections;
using Utilities;
using WebUtilities;

public partial class VendorVatCodeOverview : CommonPage
{

    #region Declarations ...
    private const int gridPageSize = 50;
    bool IsExportClicked = false;
    #endregion

    #region Events

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "VendorVat")
            {
                if (Session["VendorVatCode"] != null)
                {
                    GetSession();
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
        }
    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindVendorVatCode();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            IsExportClicked = true;
            BindVendorVatCode();
            LocalizeGridHeader(gvVendorVatCode);
            WebCommon.Export("VendorVatCode", gvVendorVatCode);
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnBackToSearch_Click(object sender, EventArgs e)
    {

        EncryptQueryString("VendorVatCodeOverview.aspx");
    }

    protected void gvVendorVatCode_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstMAS_VatCodeBE"] != null)
        {
            gvVendorVatCode.DataSource = (List<MAS_VatCodeBE>)ViewState["lstMAS_VatCodeBE"];
            gvVendorVatCode.PageIndex = e.NewPageIndex;
            if (Session["VendorVatCode"] != null)
            {
                Hashtable htVendorVatCode = (Hashtable)Session["VendorVatCode"];
                htVendorVatCode.Remove("PageIndex");
                htVendorVatCode.Add("PageIndex", e.NewPageIndex);
                Session["VendorVatCode"] = htVendorVatCode;
            }
            gvVendorVatCode.DataBind();
        }
    }

    #endregion

    #region Methods

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindVendorVatCode(currnetPageIndx);
    }

    private void BindVendorVatCode(int Page = 1)
    {
        try
        {
            MAS_VatCodeBAL oMAS_VatCodeBAL = new MAS_VatCodeBAL();
            MAS_VatCodeBE oMAS_VatCodeBE = new MAS_VatCodeBE();

            oMAS_VatCodeBE.Action = "GetAllVendorVatCodes";
            oMAS_VatCodeBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
            oMAS_VatCodeBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
            oMAS_VatCodeBE.IsVendorVatCodeMaintained = rdoMaintainedValue.Checked ? 1 : (rdoNotMaintainedValue.Checked ? 2 : 0);

            if (IsExportClicked)
            {
                oMAS_VatCodeBE.GridCurrentPageNo = 0;
                oMAS_VatCodeBE.GridPageSize = 0;
            }
            else
            {
                oMAS_VatCodeBE.GridCurrentPageNo = Page;
                oMAS_VatCodeBE.GridPageSize = gridPageSize;
            }

            this.SetSession(oMAS_VatCodeBE);

            oMAS_VatCodeBE.Vendor = new BusinessEntities.ModuleBE.AdminFunctions.MAS_VendorBE();
            oMAS_VatCodeBE.Vendor.VendorID = (int?)null;

            List<MAS_VatCodeBE> lstMAS_VatCodeBE = oMAS_VatCodeBAL.GetVendorVatCodeBAL(oMAS_VatCodeBE);

            if (lstMAS_VatCodeBE != null && lstMAS_VatCodeBE.Count > 0)
            {
                btnExportToExcel.Visible = true;
                gvVendorVatCode.DataSource = null;
                gvVendorVatCode.DataSource = lstMAS_VatCodeBE;
                pager1.ItemCount = Convert.ToDouble(lstMAS_VatCodeBE[0].TotalRecords.ToString());
                ViewState["lstMAS_VatCodeBE"] = lstMAS_VatCodeBE;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else
            {
                gvVendorVatCode.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
            }

            gvVendorVatCode.PageIndex = Page;
            pager1.CurrentIndex = Page;
            gvVendorVatCode.DataBind();
            oMAS_VatCodeBAL = null;

            pnlGrid.Visible = true;
            pnlSelection.Visible = false;
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }


    private void SetSession(MAS_VatCodeBE oMAS_VatCodeBE)
    {
        Session["VendorVatCode"] = null;
        Session.Remove("VendorVatCode");

        Hashtable htVendorVatCode = new Hashtable();
        htVendorVatCode.Add("SelectedCountryId", oMAS_VatCodeBE.SelectedCountryIDs);
        htVendorVatCode.Add("SelectedVendorIDs", oMAS_VatCodeBE.SelectedVendorIDs);
        htVendorVatCode.Add("SelectedViewMode", oMAS_VatCodeBE.IsVendorVatCodeMaintained);
        htVendorVatCode.Add("PageIndex", oMAS_VatCodeBE.GridCurrentPageNo);

        Session["VendorVatCode"] = htVendorVatCode;

    }

    private void GetSession()
    {
        MAS_VatCodeBAL oMAS_VatCodeBAL = new MAS_VatCodeBAL();
        MAS_VatCodeBE oMAS_VatCodeBE = new MAS_VatCodeBE();

        if (Session["VendorVatCode"] != null)
        {
            Hashtable htVendorVatCode = (Hashtable)Session["VendorVatCode"];

            oMAS_VatCodeBE.SelectedCountryIDs = (htVendorVatCode.ContainsKey("SelectedCountryId") && htVendorVatCode["SelectedCountryId"] != null) ? htVendorVatCode["SelectedCountryId"].ToString() : null;
            oMAS_VatCodeBE.SelectedVendorIDs = (htVendorVatCode.ContainsKey("SelectedVendorIDs") && htVendorVatCode["SelectedVendorIDs"] != null) ? htVendorVatCode["SelectedVendorIDs"].ToString() : null;
            oMAS_VatCodeBE.IsVendorVatCodeMaintained = Convert.ToInt32(htVendorVatCode["SelectedViewMode"]);
            oMAS_VatCodeBE.Action = "GetAllVendorVatCodes";

            oMAS_VatCodeBE.GridCurrentPageNo = Convert.ToInt32(htVendorVatCode["PageIndex"].ToString());
            oMAS_VatCodeBE.GridPageSize = gridPageSize;

            oMAS_VatCodeBE.Vendor = new BusinessEntities.ModuleBE.AdminFunctions.MAS_VendorBE();
            oMAS_VatCodeBE.Vendor.VendorID = (int?)null;

            List<MAS_VatCodeBE> lstMAS_VatCodeBE = oMAS_VatCodeBAL.GetVendorVatCodeBAL(oMAS_VatCodeBE);

            ViewState["lstMAS_VatCodeBE"] = lstMAS_VatCodeBE;
            if (lstMAS_VatCodeBE != null && lstMAS_VatCodeBE.Count > 0)
            {
                btnExportToExcel.Visible = true;
                gvVendorVatCode.DataSource = null;
                gvVendorVatCode.DataSource = lstMAS_VatCodeBE;
                pager1.ItemCount = Convert.ToDouble(lstMAS_VatCodeBE[0].TotalRecords.ToString());
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else
            {
                gvVendorVatCode.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
            }
            pager1.CurrentIndex = Convert.ToInt32(htVendorVatCode["PageIndex"].ToString()); ;
            gvVendorVatCode.DataBind();
            oMAS_VatCodeBAL = null;

            pnlGrid.Visible = true;
            pnlSelection.Visible = false;


        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        int pageindex = pager1.CurrentIndex;
        BindVendorVatCode(pageindex);
        return Utilities.GenericListHelper<MAS_VatCodeBE>.SortList((List<MAS_VatCodeBE>)ViewState["lstMAS_VatCodeBE"], e.SortExpression, e.SortDirection).ToArray();
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }
    #endregion


}
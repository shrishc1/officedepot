﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Utilities; using WebUtilities;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;

public partial class GlobalVendorConsolidationEdit : CommonPage
{
    protected string DeleteConsoConf = WebCommon.getGlobalResourceValue("DeleteConsolidationConfirmation");

    #region Events

    protected void Page_Init(object sender, EventArgs e)
    {
        //ucCountry.CurrentPage = this;
    }
    protected void Page_Prerender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ucSeacrhVendor1.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
            ucSeacrhVendor1.IsParentRequired = true;
            ucSeacrhVendor1.IsStandAloneRequired = true;
            ucSeacrhVendor1.IsCountryRequiredWithVendor = true;
            ucSeacrhVendor1.CountryID = -1;
            ucSeacrhVendor1.BindVendor();
            getVendorDetail();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        txtConsVendorCode.Focus();
        if (!IsPostBack)
        {
            //ucCountry.innerControlddlCountry.AutoPostBack = true;

            if (GetQueryStringValue("VendorID") != null)
                btnDelete.Visible = true;
            else
                btnDelete.Visible = false;
        }
    }
   

    
    protected void btnSave_Click(object sender, EventArgs e)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        // To check whether Vendor_No already exist
        if (CheckForVendorExistance()) {
            string VendorNoExist = WebCommon.getGlobalResourceValue("VendorNoExist");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + VendorNoExist + "');", true);
            return;
        }

        oUP_VendorBE.Vendor_No = txtConsVendorCode.Text;
        oUP_VendorBE.VendorName = txtConsVendorNam.Text;
        oUP_VendorBE.CountryID = (int?)null;
        oUP_VendorBE.VendorFlag = 'G';
        oUP_VendorBE.ConsolidatedVendors = ucSeacrhVendor1.SelectedVendorIDs;

        if (GetQueryStringValue("VendorID") != null)
        {
            oUP_VendorBE.Action = "UpdateVendorConsolidation";
            oUP_VendorBE.ParentVendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
        }
        else
        {
            oUP_VendorBE.Action = "AddVendorConsolidation";
        }

        int? iResult = oUP_VendorBAL.addEditConsolidateVendorBAL(oUP_VendorBE);
        oUP_VendorBAL = null;
        if (iResult == 0)
        {
            string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
        EncryptQueryString("GlobalVendorConsolidationOverview.aspx");

    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();

        if (GetQueryStringValue("VendorID") != null)
        {
            oUP_VendorBE.Action = "DeleteVendorConsolidation";
            oUP_VendorBE.ParentVendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
            int? iResult = oUP_VendorBAL.DeleteConsolidateVendorBAL(oUP_VendorBE);           
            EncryptQueryString("GlobalVendorConsolidationOverview.aspx");
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("GlobalVendorConsolidationOverview.aspx");
    }

    #endregion
    
    #region Methods

    private bool CheckForVendorExistance() {
        bool IsVendorNoExist = false;
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        oUP_VendorBE.Action = "CheckVendorNoExistance";
        oUP_VendorBE.Vendor_No = txtConsVendorCode.Text.Trim();
        if (GetQueryStringValue("VendorID") != null)
            oUP_VendorBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
        IsVendorNoExist = oUP_VendorBAL.CheckVendorNoAlreadyExistBAL(oUP_VendorBE);
        oUP_VendorBAL = null;
        return IsVendorNoExist;
    }

    public override bool CountryPrePage_Load() {
        return true;
    }

    //public override void CountryCustomAction() {
        //ucCountry.BindAllCountry();
        //ucSeacrhVendor1.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
    //}



    private void getVendorDetail()
    {
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        if (GetQueryStringValue("VendorId") != null )
        {
            //txtConsVendorCode.Enabled = false;
            //txtConsVendorNam.Enabled = false;
            //ucCountry.innerControlddlCountry.Enabled = false;

            oUP_VendorBE.Action = "GetVendorConsolidation";
            oUP_VendorBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorId"));
            oUP_VendorBE.VendorFlag = 'G';
            List<UP_VendorBE> lstVendorDetails = new List<UP_VendorBE>();
            lstVendorDetails = oUP_VendorBAL.GetVendorConsolidationBAL(oUP_VendorBE);

            if (lstVendorDetails != null && lstVendorDetails.Count > 0)
            {
                txtConsVendorCode.Text = lstVendorDetails[0].Vendor_No;
                txtConsVendorNam.Text = lstVendorDetails[0].VendorName;
                //if (ucCountry.innerControlddlCountry.Items.FindByValue(Convert.ToString(lstVendorDetails[0].CountryID)) != null)
                //ucCountry.innerControlddlCountry.SelectedValue = Convert.ToString( lstVendorDetails[0].CountryID);
                
            }


            oUP_VendorBE.Action = "GetConsolidateVendor";
            oUP_VendorBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorId"));
            lstVendorDetails = new List<UP_VendorBE>();
            lstVendorDetails = oUP_VendorBAL.GetVendorConsolidationBAL(oUP_VendorBE);
            oUP_VendorBAL = null;
            ListBox lstSelectedVendor = (ListBox)ucSeacrhVendor1.FindControl("lstSelectedVendor");
            //ucVendorForCountry UserControlVendor = (ucVendorForCountry)ucSeacrhVendor1.FindControl("ucVendorForCountry");
            //ListBox lstVendor = (ListBox)UserControlVendor.FindControl("lstLeft");

            ListBox lstVendor = (ListBox)ucSeacrhVendor1.FindControl("ucVendorForCountry").FindControl("lstLeft");

            if (lstVendorDetails != null && lstVendorDetails.Count> 0) {
                for (int iCount = 0; iCount < lstVendorDetails.Count; iCount++) {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorDetails[iCount].Vendor_No + " - " + lstVendorDetails[iCount].VendorName, lstVendorDetails[iCount].VendorID.ToString()));
                    lstVendor.Items.Remove(new ListItem(lstVendorDetails[iCount].Vendor_No + " - " + lstVendorDetails[iCount].VendorName, lstVendorDetails[iCount].VendorID.ToString()));
                    ucSeacrhVendor1.SelectedVendorIDs = lstVendorDetails[iCount].VendorID.ToString();
                }
            }
        }
    }

    public override void CountrySelectedIndexChanged() {
        base.CountrySelectedIndexChanged();
        //ucSeacrhVendor1.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
        //ucSeacrhVendor1.ClearSearch();
    }
    #endregion

}
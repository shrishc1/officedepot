﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="InActiveUserDeletion.aspx.cs"
    Inherits="ModuleUI_GlobalSettings_InActiveUserDeletion" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="cc2" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script type="text/javascript" language="javascript">
        function ConfirmRejectedUserDeletion() {
            return confirm('<%=AllRejectedUserdeleteMessage%>');
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });

    </script>
    <script type="text/javascript">

        var gridCheckedCount = 0;

        $(document).ready(function () {
            $('[id$=chkSelectAllText]').click(function () {
                $("[id$='chkSelect']").attr('checked', this.checked);
                gridCheckedCount = $('[id$=chkSelect]:checked').length;
            });
        });

        function SelectAllGrid(id) {
            if ($(id).attr('checked', this.checked)) {
                $("[id$='chkSelect']").attr('checked', true);
                gridCheckedCount = $('[id$=chkSelect]:checked').length;
            }
            else {
                $("[id$='chkSelect']").attr('checked', false);
                gridCheckedCount = $('[id$=chkSelect]:checked').length;
            }
        }

        function CheckUncheckAllCheckBoxAsNeeded(input) {
            if ($('[id$=chkSelect]:checked').length == $('[id$=chkSelect]').length) {
                $("[id$=chkSelectAllText]").attr('checked', true);
                gridCheckedCount = $('[id$=chkSelect]:checked').length;
            }
            if (!$(input).is(":checked")) {
                $("[id$=chkSelectAllText]").attr('checked', false);
                gridCheckedCount = $('[id$=chkSelect]:checked').length;
            }

            if ($(input).is(":checked")) {
                gridCheckedCount = $('[id$=chkSelect]:checked').length;
            }
        }

        function CheckAtleastOneCheckBox() {
            var selectCheckboxToAccept = '<%=selectCheckboxToAccept%>';
            if (gridCheckedCount == 0) {
                alert(selectCheckboxToAccept);
                return false;
            }
            else {
                if (confirm('Are you sure you want to delete selected user(s)?')) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
    </script>
    <h2>
        <cc1:ucLabel ID="lblUsersDeletion" runat="server" Text="Users Deletion"></cc1:ucLabel>
    </h2>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td width="19%" style="font-weight: bold;">
                        <cc1:ucLabel ID="lblCountry" runat="server" />
                    </td>
                    <td style="font-weight: bold; width: 1%">:
                    </td>
                    <td width="30%">
                        <cc2:ucCountry runat="server" ID="ddlCountry" />
                    </td>

                    <td width="19%" style="font-weight: bold;">
                        <cc1:ucLabel ID="lblUserNameEmailId" Text="User Name" runat="server" />
                    </td>
                    <td style="font-weight: bold; width: 1%">:
                    </td>
                    <td width="30%">
                        <cc1:ucTextbox runat="server" ID="txtUserNameValue" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel1" runat="server" Text="Role"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">:
                    </td>
                    <td>
                        <cc1:ucDropdownList ID="ddlRole" runat="server" Width="150px">
                        </cc1:ucDropdownList>
                    </td>
                    <td width="15%" style="font-weight: bold;">
                        <cc1:ucLabel ID="lblLastLoginDays" runat="server" Text="Last Login Days" />
                    </td>
                    <td style="font-weight: bold;">:
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox runat="server" placeholder="365" ID="txtLastLoginDays" />
                    </td>

                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="float: right">
                        <cc1:ucButton runat="server" ID="btnSearch" CssClass="button" OnClick="btnSearch_Click" />
                    </td>
                </tr>

                <tr id="status" runat="server" visible="false">
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblStatus" runat="server" />
                    </td>
                    <td style="font-weight: bold;">:
                    </td>
                    <td>
                        <cc1:ucDropdownList runat="server" ID="ddlStatus" Width="150px">
                            <asp:ListItem Selected="True" Text="--Select--" Value="" />
                            <asp:ListItem Text="Active" Value="Active" />
                            <asp:ListItem Text="Registered Awaiting Approval" Value="Registered Awaiting Approval" />
                            <asp:ListItem Text="Edited Awaiting Approval" Value="Edited Awaiting Approval" />
                            <asp:ListItem Text="Awaiting Activation" Value="Awaiting Activation" />
                            <asp:ListItem Text="Blocked" Value="Blocked" />
                        </cc1:ucDropdownList>
                    </td>
                    <td colspan="3"></td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings"
        runat="server" visible="false">
        <tr>
            <td width="5%" style="font-weight: bold;">
                <cc1:ucLabel ID="lblReportType" runat="server" />
            </td>
            <td style="font-weight: bold; width: 1%">:
            </td>
            <td style="font-weight: bold;" class="checkbox-list">
                <cc1:ucRadioButton ID="rdoCarrierOverview" runat="server" Text="Carrier Overview"
                    GroupName="ReportVersion" />
            </td>
            <td style="font-weight: bold;" class="checkbox-list">
                <cc1:ucRadioButton ID="rdoVendorOverview" runat="server" Text="User Overview" GroupName="ReportVersion" />
            </td>
            <td style="font-weight: bold;" class="checkbox-list">
                <cc1:ucRadioButton ID="rdoVendorPOCOverview" runat="server" Text="Vendor POC Overview"
                    GroupName="ReportVersion" />
            </td>
            <td style="font-weight: bold;" class="checkbox-list">
                <cc1:ucRadioButton ID="rdoSchedulingOverview" runat="server" Text="Scheduling Overview"
                    GroupName="ReportVersion" />
            </td>
            <td style="font-weight: bold;" class="checkbox-list">
                <cc1:ucRadioButton ID="rdoDiscrepancyOverview" runat="server" Text="Discrepancy  Overview"
                    GroupName="ReportVersion" />
            </td>
            <td style="font-weight: bold;" class="checkbox-list">
                <cc1:ucRadioButton ID="rdoOfficeDepotUserOverview" runat="server" Text="Office Depot User Overview"
                    GroupName="ReportVersion" />
            </td>
        </tr>
    </table>

    <br />
    <br />

    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
        <ContentTemplate>
            <div style="width: 990px; overflow-x: scroll;">
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:GridView runat="server" AutoGenerateColumns="false" ID="gvUser" CssClass="grid"
                                OnSorting="gridView_Sorting" AllowSorting="true" OnRowCommand="gvUser_RowCommand" >
                                <RowStyle CssClass="row1" />
                                <AlternatingRowStyle CssClass="row2" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                        <ItemStyle Width="20px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:CheckBox runat="server" ID="chkSelectAllText"
                                                onclick="SelectAllGrid(this);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect"
                                                onclick="CheckUncheckAllCheckBoxAsNeeded(this);" />
                                            <cc1:ucLiteral ID="ltUserID" Visible="false" runat="server"
                                                Text='<%# Eval("UserID") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField HeaderText="User Name" SortExpression="FullName" DataField="FullName">
                                        <HeaderStyle Width="160" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="User ID" SortExpression="LoginID">
                                        <HeaderStyle Width="150" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlLoginID" runat="server" Text='<%#Eval("LoginID") %>' NavigateUrl='<%# (Eval("UserRoleID").ToString() == "2" || Eval("UserRoleID").ToString() == "3") ? (Eval("AccountStatus").ToString().ToLower()== "registered awaiting approval" || Eval("AccountStatus").ToString().ToLower()== "edited awaiting approval") ? (EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?UserID="+ Eval("UserID")+"&UStatus=RAA")) :(EncryptQuery("~/ModuleUI/Security/SCT_RegisterExternalUserEdit.aspx?UserID="+ Eval("UserID"))) : (EncryptQuery("~/ModuleUI/Security/SCT_UserEdit.aspx?UserID="+ Eval("UserID"))) %>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField HeaderText="Role Name" SortExpression="RoleName" DataField="RoleName">
                                        <HeaderStyle Width="160" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>

                                    <asp:BoundField HeaderText="Status" SortExpression="AccountStatus" DataField="AccountStatus">
                                        <HeaderStyle Width="150" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>

                                    <asp:BoundField HeaderText="Number of Elapsed Days" SortExpression="Elapseddays"
                                        DataField="Elapseddays">
                                        <HeaderStyle Width="130" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="Delete">
                                        <HeaderStyle Width="100" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lnkDelete" ImageUrl="~/Images/delete.jpg" runat="server"
                                                CommandArgument='<%#Eval("UserID") %>'
                                                CommandName="DeleteUser"
                                                OnClientClick="confirm('Are you sure you want to delete ?')" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:PagerV2_8 ID="Pager1" runat="server" OnCommand="pager_Command" 
                                GenerateGoToSection="false"
                                GenerateFirstLastSection="False" enerateGoToSection="False" 
                                MaxSmartShortCutCount="5"
                                GenerateHiddenHyperlinks="False" GeneratePagerInfoSection="False" 
                                GenerateSmartShortCuts="True"
                                GenerateToolTips="True"
                                SmartShortCutThreshold="10" PageSize="20"></cc1:PagerV2_8>

                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div style="float: left">
                <cc1:ucButton runat="server" ID="BtnDeleteUser" CssClass="button" Text="Delete checked users"
                    OnClientClick="return CheckAtleastOneCheckBox();"
                    OnClick="BtnDeleteUser_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>



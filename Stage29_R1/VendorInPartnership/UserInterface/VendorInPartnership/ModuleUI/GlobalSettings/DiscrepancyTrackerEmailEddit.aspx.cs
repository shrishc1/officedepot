﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using System;
using WebUtilities;

public partial class ModuleUI_GlobalSettings_DiscrepancyTrackerEmailEddit : CommonPage
{

    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("ID") != null)
            {
                // discrepancyEmailTracker.Action = GetQueryStringValue("ID") != null ? "Update" : "Add";
                txtEmail.Text = GetQueryStringValue("EmailID");
                ddlType.SelectedItem.Text = GetQueryStringValue("EmailType");
                btnSubmit.Text = "Update";
            }
            else
            {
                btnSubmit.Text = "Save";
            }
        }
    }

    #region Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DiscrepancyEmailTracker discrepancyEmailTracker = new DiscrepancyEmailTracker();
        CommunicationLibraryBAL discrepancyEmailTrackerBAL = new CommunicationLibraryBAL();

        if (GetQueryStringValue("ID") != null)
        {
            discrepancyEmailTracker.ID = Convert.ToInt32(GetQueryStringValue("ID").ToString());
        }

        discrepancyEmailTracker.Action = GetQueryStringValue("ID") != null ? "Update" : "Add";
        discrepancyEmailTracker.EmailID = txtEmail.Text;
        discrepancyEmailTracker.EmailType = ddlType.SelectedItem.Text;
        discrepancyEmailTracker.UserID = Convert.ToInt32(Session["UserID"].ToString());

        
        string result = discrepancyEmailTrackerBAL.AddDiscrepancyTrackerEmailBAL(discrepancyEmailTracker);

        if (result.Contains("Successfully"))
        {
            EncryptQueryString("DiscrepancyTrackerEmail.aspx");
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        DiscrepancyEmailTracker discrepancyEmailTracker = new DiscrepancyEmailTracker();
        CommunicationLibraryBAL discrepancyEmailTrackerBAL = new CommunicationLibraryBAL();
        discrepancyEmailTracker.EmailID = txtEmail.Text;
        discrepancyEmailTracker.EmailType = ddlType.SelectedItem.Text;
        if (GetQueryStringValue("ID") != null)
            discrepancyEmailTracker.Action = "Delete";
        discrepancyEmailTracker.UserID = Convert.ToInt32(Session["UserID"].ToString());

        discrepancyEmailTracker.ID = Convert.ToInt32(GetQueryStringValue("ID").ToString());

        string result = discrepancyEmailTrackerBAL.AddDiscrepancyTrackerEmailBAL(discrepancyEmailTracker);
        if (result.Contains("Successfully"))
        {
            EncryptQueryString("DiscrepancyTrackerEmail.aspx");
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("DiscrepancyTrackerEmail.aspx");
    }
    #endregion
}
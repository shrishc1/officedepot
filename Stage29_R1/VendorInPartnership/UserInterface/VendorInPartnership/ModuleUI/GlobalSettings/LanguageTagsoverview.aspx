﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="LanguageTagsoverview.aspx.cs" Inherits="LanguageTagsoverview"
    ValidateRequest="false"   %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<script language="javascript" type="text/javascript">
    function ConfirmOnDelete(item) {
        if (confirm("Are you sure to delete") == true)
            return true;
        else
            return false;
    }

    $(document).ready(function () {
        $("body").keypress(function (e) {
            if (e.which == 13 || e.keyCode == 13) {
                $('#<%=btnSearch.ClientID %>').click();
                return false;
            }
        });
    });
    </script>

    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblLanguageTags" runat="server" Text="LanguageTags"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc1:ucButton ID="btnApplyChanges" runat="server" CssClass="button" OnClick="btnApplyChanges_Click" />
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
            Width="109px" Height="20px" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td>
                        <cc1:ucLabel ID="lblLanguage" runat="server"></cc1:ucLabel>
                    </td>
                    <td>
                        :
                    </td>
                    <td colspan="4">
                        <cc1:ucDropdownList ID="ddlLanguage" runat="server" Width="150px" ValidationGroup="NotRequired">
                        </cc1:ucDropdownList>
                    </td>
                </tr>
                <tr>
                    <td style="width: 9%">
                        <cc1:ucLabel ID="lblSearchEnglish" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="width: 1%">
                        :
                    </td>
                    <td style="width: 35%">
                        <asp:TextBox TextMode="MultiLine" ID="txtSearchEnglish" runat="server" Width="300px"
                            Height="50px"></asp:TextBox>
                    </td>
                    <td style="width: 12%">
                        <cc1:ucLabel ID="lblSearchTranslation" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="width: 1%">
                        :
                    </td>
                    <td style="width: 42%">
                        <asp:TextBox TextMode="MultiLine" ID="txtSearchTranslation" runat="server" Width="300px"
                            Height="50px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="right">
                        <cc1:ucButton runat="server" ID="btnClear" Text="Search" CssClass="button" OnClick="btnClear_Click" />
                        <cc1:ucButton runat="server" ID="btnSearch" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <cc1:ucGridView ID="gvResourceEditor" runat="server" AutoGenerateColumns="False"
                                    ShowFooter="True" OnRowCommand="gvResourceEditor_RowCommand" OnRowEditing="gvResourceEditor_RowEditing"
                                    OnRowUpdating="gvResourceEditor_RowUpdating" OnRowCancelingEdit="gvResourceEditor_RowCancelingEdit"
                                    CssClass="grid" CellPadding="0" Width="100%" GridLines="None" AllowPaging="true"
                                    PageSize="50" OnPageIndexChanging="gvResourceEditor_PageIndexChanging" EmptyDataText="No Records Found"
                                    EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="ResourceKey" meta:resourcekey="TemplateFieldResource1">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblKey1" runat="server" Text='<%# Eval("Key") %>' meta:resourcekey="lblKeyResource1"></asp:Label>
                                                <asp:HiddenField ID="hdnLangTagId" runat="server" Value='<%# Eval("LanguageTagsId") %>' />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblKey2" runat="server" Text='<%# Eval("Key") %>' meta:resourcekey="lblKeyResource2"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                            <ItemStyle Width="20%" HorizontalAlign="Left" Wrap="true" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="English" meta:resourcekey="TemplateFieldResource2">
                                            <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                            <EditItemTemplate>
                                                <asp:Label ID="lblValue1" runat="server" Text='<%# Eval("Value") %>' meta:resourcekey="lblValueResource1"></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblValue2" runat="server" Text='<%# Bind("Value") %>' meta:resourcekey="lblValueResource2"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="30%" HorizontalAlign="Left" />
                                            <ItemStyle Wrap="True" Width="30%" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Translation" meta:resourcekey="TemplateFieldResource3">
                                            <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTranslation1" TextMode="MultiLine" CssClass="textBoxEnabled"
                                                    runat="server" Text='<%# Bind("Translation") %>' Width="100%" Height="150px"
                                                    meta:resourcekey="txtDescriptionResource1"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTranslation2" runat="server" Text='<%# Bind("Translation") %>'
                                                    meta:resourcekey="lblDescriptionResource1"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="30%" HorizontalAlign="Left" />
                                            <ItemStyle Wrap="True" Width="30%" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:CommandField ShowEditButton="True" HeaderText="Action" >
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" HorizontalAlign="Left" />
                                        </asp:CommandField>
                                    </Columns>
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
                                </cc1:ucGridView>
                                <cc1:ucGridView ID="grdResourceEditorExcel" Width="100%" runat="server" CssClass="grid"
                                    Visible="false">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="ResourceKey" meta:resourcekey="TemplateFieldResource1">
                                            <EditItemTemplate>
                                                <asp:Label ID="lblKey1" runat="server" Text='<%# Eval("Key") %>' meta:resourcekey="lblKeyResource1"></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblKey2" runat="server" Text='<%# Eval("Key") %>' meta:resourcekey="lblKeyResource2"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                            <ItemStyle Width="20%" HorizontalAlign="Left" Wrap="true" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="English" meta:resourcekey="TemplateFieldResource2">
                                            <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                            <EditItemTemplate>
                                                <asp:Label ID="lblValue1" runat="server" Text='<%# Eval("Value") %>' meta:resourcekey="lblValueResource1"></asp:Label>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblValue2" runat="server" Text='<%# Bind("Value") %>' meta:resourcekey="lblValueResource2"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="30%" HorizontalAlign="Left" />
                                            <ItemStyle Wrap="True" Width="30%" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Translation" meta:resourcekey="TemplateFieldResource3">
                                            <HeaderStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTranslation1" TextMode="MultiLine" CssClass="textBoxEnabled"
                                                    runat="server" Text='<%# Bind("Translation") %>' Width="100%" Height="150px"
                                                    meta:resourcekey="txtDescriptionResource1"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTranslation2" runat="server" Text='<%# Bind("Translation") %>'
                                                    meta:resourcekey="lblDescriptionResource1"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="30%" HorizontalAlign="Left" />
                                            <ItemStyle Wrap="True" Width="30%" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                </cc1:ucGridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
    </div>
  
</asp:Content>

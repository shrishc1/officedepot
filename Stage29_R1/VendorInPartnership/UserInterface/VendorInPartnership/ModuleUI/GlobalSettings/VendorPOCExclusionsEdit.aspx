﻿<%@ Page Language="C#" AutoEventWireup="true"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    MaintainScrollPositionOnPostback="true"
    CodeFile="VendorPOCExclusionsEdit.aspx.cs" Inherits="ModuleUI_GlobalSettings_VendorPOCExclusionsEdit" %>


<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">
        function checkVendorSelected(source, args) {
            if (document.getElementById('<%=lstLeft.ClientID%>') != null) {
                var obj = document.getElementById('<%=lstLeft.ClientID%>');
                if (obj.value == '') {
                    args.IsValid = false;
                }
            }
        }
        function ShowSelectedVendorName(type) {
            if (type == 'Local') {
                var txtObj = $('#<%=txtSelectedVendorValue.ClientID%>');
                var lstObj = $('#<%=lstLeft.ClientID%> option:selected'); //.text();
                var hdnID = $('#<%=hdnLocalvendorID.ClientID%>');
            }
            if (lstObj.text() != '') {
                $(txtObj).val(lstObj.text());
                $(hdnID).val(lstObj.val());
            }
        }

        function RemoveSelectedVendorName(type) {
            if (type == 'Local') {
                var txtObj = $('#<%=txtSelectedVendorValue.ClientID%>');
                $('#<%=lstLeft.ClientID%>').find("option").attr("selected", false);
                var hdnID = $('#<%=hdnLocalvendorID.ClientID%>');
                $(txtObj).val('');
                $(hdnID).val('0');
            }
        }

    </script>
   
    <asp:ScriptManager runat="server" />
    <h2>
        <cc1:ucLabel ID="VendorPOCExclusions" runat="server"></cc1:ucLabel>
    </h2>

  
    <asp:HiddenField ID="hdnLocalvendorID" runat="server" Value="0" />

    <table width="95%" cellspacing="5" cellpadding="2" border="0" align="center" class="top-settings" id="tblsearch" runat="server">
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor" isRequired="true"></cc1:ucLabel>
            </td>
            <td>:
            </td>
            <td style="width: 50%">
                <table border="0" style="width: 85%;">
                    <tr>
                        <td style="width: 40%">
                            <table>
                                <tr>
                                    <td>
                                        <cc1:ucTextbox ID="txtVendorNo" runat="server" MaxLength="15" Width="77px"></cc1:ucTextbox>
                                    </td>
                                    <td>
                                        <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendor" OnClick="btnSearch_Click" />&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendorNumber" OnClick="btnSearchByVendorNo_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 20%;" align="center"></td>
                        <td style='width: 20%'></td>
                        <td style="width: 30%"></td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="350px">
                            </cc1:ucListBox>
                        </td>
                        <td style='width: 20%'></td>
                        <td align="center" style="padding-left: 27px;">
                            <div>
                                <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                                    onclick="Javascript: ShowSelectedVendorName('Local');" />
                            </div>
                            &nbsp;
                           <div>
                               <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                                   onclick="Javascript: RemoveSelectedVendorName('Local');" />
                           </div>
                        </td>
                        <td>&nbsp;
                        </td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cusvNoSelectedVendor" runat="server" ClientValidationFunction="checkVendorSelected"
                    ValidationGroup="CheckVendor" Display="None"></asp:CustomValidator>
            </td>
            <td>
                <div style="margin-top: 26px;">
                    <cc1:ucTextbox ID="txtSelectedVendorValue" runat="server" Font-Bold="true" ReadOnly="true" Width="63%" Height="25px"></cc1:ucTextbox>
                </div>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblReason" runat="server" Text="Reason For Exclusion" isRequired="true"></cc1:ucLabel>
            </td>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
            </td>
            <td colspan="2">
                <cc1:ucTextarea ID="txtReason" runat="server" Style="width: 84%;"></cc1:ucTextarea>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;"></td>
            <td style="font-weight: bold;"></td>
            <td style="text-align: right" colspan="2">
                <cc1:ucButton ID="btnBack" Text="Back" runat="server" OnClick="btnBack_Click" />
                <cc1:ucButton ID="btnSubmit" Text="Submit" runat="server" OnClick="btnSubmit_Click" />
                <cc1:ucButton ID="btnDelete" Text="Delete" runat="server" OnClick="btnDelete_Click" />
            </td>
        </tr>
    </table>
</asp:Content>

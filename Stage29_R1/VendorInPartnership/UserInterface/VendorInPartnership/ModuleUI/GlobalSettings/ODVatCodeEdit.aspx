﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ODVatCodeEdit.aspx.cs" Inherits="ODVatCodeEdit" %>

<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <script language="javascript" type="text/javascript">
         function confirmDelete() {
             return confirm('<%=deleteMessage%>');
         }
        
    </script>
   
    <h2>
        <cc1:ucLabel ID="lblVATcodemaintananceOD" runat="server"></cc1:ucLabel>
    </h2>
    <div>
       
         <asp:RequiredFieldValidator ID="rfvSiteRequired" runat="server" ControlToValidate="ucSite$ddlsite" InitialValue="0"
            Display="None" ValidationGroup="a"> </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="rfvVatCodeRequired" runat="server" ControlToValidate="txtVatCode"
            Display="None" ValidationGroup="a"> </asp:RequiredFieldValidator>
        <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
            Style="color: Red" ValidationGroup="a" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <table width="40%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 5%">
                    </td>
                    <td style="font-weight: bold; width: 19%">
                        <cc1:ucLabel ID="lblSite" runat="server" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%" align="center">
                        :
                    </td>
                    <td style="width: 75%">
                        <cc2:ucSite ID="ucSite" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%">
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblVatCode" runat="server" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold" align="center">
                        :
                    </td>
                    <td>
                        <cc1:ucTextbox runat="server" ID="txtVatCode" MaxLength="20" Width="150px"></cc1:ucTextbox>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
            ValidationGroup="a" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click" OnClientClick="return confirmDelete();" Visible="false" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ReportLibraryOverview.aspx.cs" Inherits="ReportLibraryOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblReportLibrary" Text="Report Library" runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row" style="float: right;">
        <cc2:ucExportToExcel ID="btnExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="gvReportLibrary" Width="100%" runat="server" CssClass="grid"
                    AllowSorting="True" OnSorting="SortGrid" OnRowCommand="gvReportLibrary_RowCommand">
                    <Columns>
                        <asp:TemplateField HeaderText="ReportName" SortExpression="ReportName" ItemStyle-Width="20%">
                            <HeaderStyle Width="20%" HorizontalAlign="Left" VerticalAlign="Top" />
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnScreenIdText" Value='<%#Eval("ScreenId") %>' runat="server" />
                                <asp:HiddenField ID="hdnScreenUrlText" Value='<%#Eval("ScreenUrl") %>' runat="server" />
                                <asp:LinkButton ID="lnkReportNameText" CommandName="CHECKREPORT" runat="server"
                                    Text='<%#Eval("ReportName") %>'></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Module" SortExpression="ModuleName" ItemStyle-Width="10%">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" VerticalAlign="Top" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblModuleNameText" Text='<%#Eval("ModuleName")%>' runat="server"></cc1:ucLabel>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description1" SortExpression="Description" ItemStyle-Width="40%">
                            <HeaderStyle Width="40%" HorizontalAlign="Left" VerticalAlign="Top" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblDescriptionText" Text='<%#Eval("Description")%>' runat="server"></cc1:ucLabel>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="FurtherInfo" SortExpression="FurtherInfo" ItemStyle-Width="30%">
                            <HeaderStyle Width="30%" HorizontalAlign="Left" VerticalAlign="Top" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblFurtherInfoText" Text='<%#Eval("FurtherInfo")%>' runat="server"></cc1:ucLabel>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>
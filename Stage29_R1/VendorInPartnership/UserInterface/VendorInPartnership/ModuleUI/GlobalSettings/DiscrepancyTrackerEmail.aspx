﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DiscrepancyTrackerEmail.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    Inherits="ModuleUI_Discrepancy_DiscrepancyTrackerEmail" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
    </script>
    <h2>
        <cc1:ucLabel ID="lblDiscreapncyEscalationEmailRecipients" Text="Discreapncy Escalation Email Recipients" runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="UcAddButton1" runat="server" NavigateUrl="DiscrepancyTrackerEmailEddit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <div class="right-shadow">
        <asp:UpdatePanel ID="updCommLib" runat="server">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td align="center" colspan="9">
                            <cc1:ucGridView ID="gvDiscrepancyTrackerEmail" Width="100%" runat="server" CssClass="grid"
                                AllowPaging="true" PageSize="50" OnRowCommand="gvDiscrepancyTrackerEmail_RowCommand"
                                EmptyDataText="No record Found.">
                                <Columns>
                                    <asp:TemplateField HeaderText="Email ID" SortExpression="EmailID">
                                        <HeaderStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblSentOnBehalfOfValue" Text='<%#Eval("EmailID")%>' runat="server"></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type" SortExpression="Type">
                                        <HeaderStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblSentByText" Text='<%#Eval("EmailType")%>' runat="server"></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" SortExpression="ID">
                                        <HeaderStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnCommunicationIDText" Value='<%#Eval("ID") %>' runat="server" />
                                            <a id="lnkDateSentText" href='<%# EncryptQuery("DiscrepancyTrackerEmailEddit.aspx?ID=" + Eval("ID") +"&EmailType=" + Eval("EmailType") +"&EmailID=" + Eval("EmailID"))%>'
                                                runat="server" target='_blank'>Edit</a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete"
                                                OnClientClick="return confirmDelete();" 
                                                CommandName="dede" CommandArgument='<%#Eval("ID") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <cc1:ucGridView ID="gvDiscrepancyTrackerEmailExcel" Width="100%" runat="server" CssClass="grid"
                                Visible="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="EmailID" SortExpression="CreatedByEmail">
                                        <HeaderStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblSentOnBehalfOfValue" Text='<%#Eval("EmailID")%>' runat="server"></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type" SortExpression="SentBy">
                                        <HeaderStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblSentByText" Text='<%#Eval("EmailType")%>' runat="server"></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

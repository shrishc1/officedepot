﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="MaintainVendorPointsofContactEdit.aspx.cs" Inherits="MaintainVendorPointsofContactEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry" TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function DeleteConfirmation() {
            if (confirm('<%=DeletePOCConfirmation%>')) {
                return true;
            }
            else {
                return false;
            }
        }
        function SaveConfirmation() {
            if (confirm('<%=SavePOCConfirmation%>')) {
                return true;
            }
            else {
                return false;
            }
        }

        $(function () {
            var imgVendor = $("#imgVendor");
            var vendorPointId = '<%=vendorPointId%>'            
            if (imgVendor != null && vendorPointId>0) {
                imgVendor.hide();
            }
        });
       
    </script>
    <h2>
        <cc1:ucLabel ID="lblMaintainVendorPointsOfContact" runat="server" Text="Maintain Vendor Points of Contact"></cc1:ucLabel>
    </h2>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table class="top-settings" width="75%" id="tblHeader" runat="server">
                <tr>
                    <td style="font-weight: bold;" align="center">
                        <table width="100%" cellspacing="5" cellpadding="0" border="0">
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" align="left">
                                    <uc1:ucCountry ID="ucCountry" runat="server" />
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendorNumberSDR" runat="server" Text="Vendor Number" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <span id="spVender" runat="server">
                                        <cc3:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                                    </span>
                                    <cc1:ucLiteral ID="ltSearchVendorName" runat="server" Visible="false"></cc1:ucLiteral>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <div class="right-shadow">
                <%--<div class="formbox">--%>
                <div class="top-settings">
                    <table width="100%" cellspacing="5" cellpadding="0" border="0">
                        <tr>
                            <td style="font-weight: bold; width: 100%" align="left" colspan="3">
                                <cc1:ucPanel ID="pnlPleaseEnterDetailsBelow" runat="server" GroupingText="Please enter details below"
                                    CssClass="fieldset-form">
                                    <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                                        <tr>
                                            <td style="font-weight: bold; width: 10%;">
                                                <cc1:ucLabel ID="lblFirstName" runat="server" Text="First Name" isRequired="true"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 1%;">
                                                <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 39%;" align="left">
                                                <cc1:ucTextbox ID="txtFirstNameT" runat="server" MaxLength="50"></cc1:ucTextbox>
                                            </td>
                                            <td style="font-weight: bold; width: 10%;">
                                                <cc1:ucLabel ID="lblSecondName" runat="server" Text="Second Name" isRequired="true"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 1%;">
                                                <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 39%;" align="left">
                                                <cc1:ucTextbox ID="txtSecondNameT" runat="server" MaxLength="50"></cc1:ucTextbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; width: 10%;">
                                                <cc1:ucLabel ID="lblRoleTitle" runat="server" Text="Role / Title"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 1%;">
                                                <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 39%;">
                                                <cc1:ucTextbox ID="txtRoleTitleT" runat="server" MaxLength="50"></cc1:ucTextbox>
                                            </td>
                                            <td style="font-weight: bold; width: 10%;">
                                                <cc1:ucLabel ID="lblEmailAddress" runat="server" Text="Email Address"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 1%;">
                                                <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 39%;" align="left">
                                                <cc1:ucTextbox ID="txtEmailAddressT" Width="300px" runat="server" MaxLength="100"></cc1:ucTextbox>
                                                <asp:RegularExpressionValidator ID="revEmailRequired" ControlToValidate="txtEmailAddressT"
                                                runat="server" ValidationGroup="a" ErrorMessage="Please enter Correct Email Id"
                                                Display="None" ValidationExpression="^[\w-\.\']{1,}\@([\da-zA-Z-]{1,}\.){1,3}[\da-zA-Z-]{2,6}$" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; width: 10%;">
                                                <cc1:ucLabel ID="lblPhoneNumber" runat="server" Text="Phone Number"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 1%;">
                                                <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 89%;" align="left" colspan="4">
                                                <cc1:ucTextbox ID="txtPhoneNumberT" runat="server" MaxLength="20"></cc1:ucTextbox>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%" align="left" colspan="3">
                                <cc1:ucPanel ID="pnlAssignedPointofContact" runat="server" GroupingText="Assigned Point of Contact"
                                    CssClass="fieldset-form">
                                    <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                                        <tr>
                                            <td class="checkbox-input">
                                                <cc1:ucCheckboxList ID="cblPointOfContact" Width="100%" runat="server" RepeatColumns="4"
                                                    RepeatDirection="Horizontal">
                                                </cc1:ucCheckboxList>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                    </table>
                </div>
                <%--</div>--%>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
                    ValidationGroup="a" />
                    <cc1:ucButton ID="btnProceed" runat="server" Text="Proceed" CssClass="button" 
                    Visible="false" onclick="btnProceed_Click" />
                <%--OnClientClick="return SaveConfirmation();" --%>
                <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" OnClientClick="return DeleteConfirmation();"
                    CssClass="button" OnClick="btnDelete_Click" Visible="false" />
                <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                    Style="color: Red" ValidationGroup="a" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

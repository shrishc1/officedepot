﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using System.Collections;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using BaseControlLibrary;


public partial class MaintainVendorPointsofContactEdit : CommonPage 
{
    #region Local Declarations ...

    protected int vendorPointId = 0;
    protected string DeletePOCConfirmation = WebCommon.getGlobalResourceValue("DeletePointOfContactConfirmation");
    protected string SavePOCConfirmation = WebCommon.getGlobalResourceValue("SavePointOfContactConfirmation");
    string SavedPOContactConfirmation = WebCommon.getGlobalResourceValue("SavedPointOfContactConfirmation");
    string DeletedPointOfContactConfirmation = WebCommon.getGlobalResourceValue("DeletedPointOfContactConfirmation");    
    string PleaseEnterTheVendorNumber = WebCommon.getGlobalResourceValue("PleaseEnterTheVendorNumber");
    string PleaseEnterTheFirstName = WebCommon.getGlobalResourceValue("PleaseEnterTheFirstName");
    string PleaseEnterTheSecondName = WebCommon.getGlobalResourceValue("PleaseEnterTheSecondName");
    string POCAlreadyExistForSelectedVendor = WebCommon.getGlobalResourceValue("POCAlreadyExistForSelectedVendor");
    string PleaseSelectAtleastOneAssignedPointOfContact = WebCommon.getGlobalResourceValue("PleaseSelectAtleastOneAssignedPointOfContact");
    
    #endregion

    #region Events ...

    protected void Page_Init(object sender, EventArgs e)
    {
        vendorPointId = Convert.ToInt32(GetQueryStringValue("venPointId"));
        ucCountry.CurrentPage = this;
        this.LoadVendor();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ucCountry.innerControlddlCountry.AutoPostBack = true;
            this.BindAssignedpointOfContact();
            if (GetQueryStringValue("ReadOnly") == "1")
            {
                pnlAssignedPointofContact.Enabled = false;
                pnlPleaseEnterDetailsBelow.Enabled = false;
                btnSave.Visible = false;
                btnDelete.Visible = false;
                btnProceed.Visible = true;
            }
            else
            {
                pnlAssignedPointofContact.Enabled = true;
                pnlPleaseEnterDetailsBelow.Enabled = true;
                btnSave.Visible = true;
                btnDelete.Visible = true;
                btnProceed.Visible = false;
            }
        }
    }

    protected void Page_Prerender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ucSeacrhVendor1.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
            ucSeacrhVendor1.IsStandAloneRequired = true;

            if (GetQueryStringValue("VendorID") != null && GetQueryStringValue("Mode") != null)
            {
                if (GetQueryStringValue("Mode") == "AddInventoryPOC")
                {
                    // add code for Inventory POC;
                    var coiuntryIndex = ucCountry.innerControlddlCountry.Items.IndexOf(ucCountry.innerControlddlCountry.Items.FindByText(Convert.ToString(GetQueryStringValue("CountryName"))));
                    ucCountry.innerControlddlCountry.SelectedIndex = coiuntryIndex;

                    var txtVendorNo = (ucTextbox)ucSeacrhVendor1.FindControl("txtVendorNo");
                    var lblSelectedVendorName = (ucLabel)ucSeacrhVendor1.FindControl("SelectedVendorName");
                    if (txtVendorNo != null && lblSelectedVendorName != null)
                    {
                        txtVendorNo.Text = GetQueryStringValue("Vendor_No");
                        lblSelectedVendorName.Text = GetQueryStringValue("VendorName");
                    }

                }

            }



        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        var txtVendorNo = (ucTextbox)ucSeacrhVendor1.FindControl("txtVendorNo");
        if ((string.IsNullOrEmpty(txtVendorNo.Text.Trim()) || string.IsNullOrWhiteSpace(txtVendorNo.Text.Trim())) && GetQueryStringValue("venPointId") == null)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + PleaseEnterTheVendorNumber + "')", true);
            txtVendorNo.Focus();
            return;
        }
        else if (string.IsNullOrEmpty(txtFirstNameT.Text.Trim()) || string.IsNullOrWhiteSpace(txtFirstNameT.Text.Trim()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + PleaseEnterTheFirstName + "')", true);
            txtFirstNameT.Focus();
            return;
        }
        else if (string.IsNullOrEmpty(txtSecondNameT.Text.Trim()) || string.IsNullOrWhiteSpace(txtSecondNameT.Text.Trim()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + PleaseEnterTheSecondName + "')", true);
            txtSecondNameT.Focus();
            return;
        }
        else if (!IsAnyAssignedPOCChecked())
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + PleaseSelectAtleastOneAssignedPointOfContact + "')", true);
            txtSecondNameT.Focus();
            return;
        }
        else
        {
            var vendorPointsContactBE = new MAS_MaintainVendorPointsContactBE();
            #region Logic to set the properties of Vendor Point of Contact class.
            if (GetQueryStringValue("venPointId") == null) // In case of Save records.
            {
                vendorPointsContactBE.Action = "InsertMaintainVendorPointsContact";
            }
            else // In case of Update records.
            {
                vendorPointsContactBE.Action = "UpdateMaintainVendorPointsContact";
                vendorPointsContactBE.VendorPointID = GetQueryStringValue("venPointId") != null ? Convert.ToInt32(GetQueryStringValue("venPointId")) : (int?)null;
            }

            if (GetQueryStringValue("VendorID") != null && GetQueryStringValue("Mode") != null) // In case of Save records while adding Inventory POC
            {
                if (GetQueryStringValue("Mode") == "AddInventoryPOC")
                {
                    vendorPointsContactBE.Action = "InsertMaintainVendorPointsContact";
                    vendorPointsContactBE.Vendor = new MAS_VendorBE();
                    vendorPointsContactBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
                }
            }

            vendorPointsContactBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            vendorPointsContactBE.User.UserID = Convert.ToInt32(Session["UserID"]);

            if (!string.IsNullOrEmpty(ucCountry.innerControlddlCountry.SelectedValue) && GetQueryStringValue("venPointId") == null)
            {
                vendorPointsContactBE.Country = new MAS_CountryBE();
                vendorPointsContactBE.Country.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedValue);
            }

            if (!string.IsNullOrEmpty(ucSeacrhVendor1.VendorNo) && GetQueryStringValue("venPointId") == null)
            {
                vendorPointsContactBE.Vendor = new MAS_VendorBE();
                vendorPointsContactBE.Vendor.VendorID = Convert.ToInt32(ucSeacrhVendor1.VendorNo);
            }

            vendorPointsContactBE.FirstName = txtFirstNameT.Text;
            vendorPointsContactBE.SecondName = txtSecondNameT.Text;
            vendorPointsContactBE.RoleTitle = txtRoleTitleT.Text;
            vendorPointsContactBE.EmailAddress = txtEmailAddressT.Text;
            vendorPointsContactBE.PhoneNumber = txtPhoneNumberT.Text;

            for (var index = 0; index < cblPointOfContact.Items.Count; index++)
            {
                switch (index)
                {
                    case 0:
                        vendorPointsContactBE.InventoryPOC = cblPointOfContact.Items[index].Selected == true ? "Y" : "N";
                        break;
                    case 1:
                        vendorPointsContactBE.ProcurementPOC = cblPointOfContact.Items[index].Selected == true ? "Y" : "N";
                        break;
                    case 2:
                        vendorPointsContactBE.MerchandisingPOC = cblPointOfContact.Items[index].Selected == true ? "Y" : "N";
                        break;
                    case 3:
                        vendorPointsContactBE.ExecutivePOC = cblPointOfContact.Items[index].Selected == true ? "Y" : "N";
                        break;
                }
            }
            #endregion

            var vendorPointsContactBAL = new MAS_MaintainVendorPointsContactBAL();
            int ? result=vendorPointsContactBAL.AddVendorPointsContactBAL(vendorPointsContactBE);
            if (result != -1)
            {
                var varVendorID = GetQueryStringValue("VendorID") != null ? Convert.ToInt32(GetQueryStringValue("VendorID")) : 0;
                if (varVendorID > 0)
                {
                    if (GetQueryStringValue("PreviousPageWas") != null)
                    {
                        if (GetQueryStringValue("PreviousPageWas").Equals("UserList"))
                        {
                            EncryptQueryString("VendorEdit.aspx?VendorID=" + varVendorID + "&IsPageFrom=MaintainVendorPointsofContactEdit" + "&PreviousPageWas=UserList");
                        }
                        else if (GetQueryStringValue("PreviousPageWas").Equals("VendorOverview"))
                        {
                            EncryptQueryString("VendorEdit.aspx?VendorID=" + varVendorID + "&IsPageFrom=MaintainVendorPointsofContactEdit" + "&PreviousPageWas=VendorOverview");
                        }
                    }
                    else
                    {
                        EncryptQueryString("VendorEdit.aspx?VendorID=" + varVendorID + "&IsPageFrom=MaintainVendorPointsofContactEdit" + "&PreviousPageWas=UserList");
                    }
                }
                else
                {
                    this.RefreshPage();
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + SavedPOContactConfirmation + "')", true);
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert( '" + POCAlreadyExistForSelectedVendor + "')", true);
                return;
            }
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        //var txtVendorNo = (ucTextbox)ucSeacrhVendor1.FindControl("txtVendorNo");
        //if ((string.IsNullOrEmpty(txtVendorNo.Text.Trim()) || string.IsNullOrWhiteSpace(txtVendorNo.Text.Trim())) && GetQueryStringValue("venPointId") == null)
        //{
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + PleaseEnterTheVendorNumber + "')", true);
        //    txtVendorNo.Focus();
        //    return;
        //}
        //else
        //{
        var vendorPointsContactBE = new MAS_MaintainVendorPointsContactBE();
        vendorPointsContactBE.Action = "DeleteMaintainVendorPointsContact";
        vendorPointsContactBE.IsActive = 0;
        vendorPointsContactBE.VendorPointID = GetQueryStringValue("venPointId") != null ? Convert.ToInt32(GetQueryStringValue("venPointId")) : (int?)null;
        var vendorPointsContactBAL = new MAS_MaintainVendorPointsContactBAL();
        vendorPointsContactBAL.DeleteVendorPointsContactBAL(vendorPointsContactBE);
        //this.RefreshPage();
        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + DeletedPointOfContactConfirmation + "')", true);
        var varVendorID = GetQueryStringValue("VendorID") != null ? Convert.ToInt32(GetQueryStringValue("VendorID")) : 0;

        //EncryptQueryString("VendorEdit.aspx?VendorID=" + varVendorID);

        if (varVendorID > 0)
        {
            if (GetQueryStringValue("PreviousPageWas") != null)
            {
                if (GetQueryStringValue("PreviousPageWas").Equals("UserList"))
                {
                    EncryptQueryString("VendorEdit.aspx?VendorID=" + varVendorID + "&IsPageFrom=MaintainVendorPointsofContactEdit" + "&PreviousPageWas=UserList");
                }
                else if (GetQueryStringValue("PreviousPageWas").Equals("VendorOverview"))
                {
                    EncryptQueryString("VendorEdit.aspx?VendorID=" + varVendorID + "&IsPageFrom=MaintainVendorPointsofContactEdit" + "&PreviousPageWas=VendorOverview");
                }
            }
            else
            {
                EncryptQueryString("VendorEdit.aspx?VendorID=" + varVendorID + "&IsPageFrom=MaintainVendorPointsofContactEdit" + "&PreviousPageWas=UserList");
            }
        }
        //}
    }

    public override void CountrySelectedIndexChanged()
    {
        this.LoadVendor();
    }

    public override void CountryPost_Load() 
    {
        if (GetQueryStringValue("venPointId") != null)
        {
            this.SetVendorPointsOfContact(Convert.ToInt32(GetQueryStringValue("venPointId")));
            ucCountry.innerControlddlCountry.Enabled = false;
            var txtVendorNo = (ucTextbox)ucSeacrhVendor1.FindControl("txtVendorNo");
            if (txtVendorNo != null) { txtVendorNo.Enabled = false; }
        }
    }

    #endregion

    #region Methods ...

    private void LoadVendor()
    {
        var txtVendorNo = (ucTextbox)ucSeacrhVendor1.FindControl("txtVendorNo");
        var lblSelectedVendorName = (ucLabel)ucSeacrhVendor1.FindControl("SelectedVendorName");
        if (txtVendorNo != null && lblSelectedVendorName != null)
        {
            txtVendorNo.Text = string.Empty;
            lblSelectedVendorName.Text = string.Empty;
            txtVendorNo.Focus();
        }
        ucSeacrhVendor1.CurrentPage = this;
        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = true;
        if (!string.IsNullOrEmpty(ucCountry.innerControlddlCountry.SelectedValue))
            ucSeacrhVendor1.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedValue);
    }

    private void BindAssignedpointOfContact()
    {
        var lstItem = new ListItem(WebCommon.getGlobalResourceValue("InventoryPointOfContact"), "IPC");
        cblPointOfContact.Items.Add(lstItem);
        lstItem = new ListItem(WebCommon.getGlobalResourceValue("ProcurementPointOfContact"), "PPC");
        cblPointOfContact.Items.Add(lstItem);
        //lstItem = new ListItem(WebCommon.getGlobalResourceValue("MerchandisingPointOfContact"), "MPC");
        lstItem = new ListItem(WebCommon.getGlobalResourceValue("AccountsPayablePointofContact"), "MPC");
        cblPointOfContact.Items.Add(lstItem);
        lstItem = new ListItem(WebCommon.getGlobalResourceValue("ExecutivePointOfContact"), "EPC");
        cblPointOfContact.Items.Add(lstItem);

        if (GetQueryStringValue("Mode") != null)
        {
            if (GetQueryStringValue("Mode") == "AddInventoryPOC")
            {
                cblPointOfContact.Items.FindByText("Procurement Point of Contact").Enabled = false;
                cblPointOfContact.Items.FindByText("Accounts Payable Point of Contact").Enabled = false;
                cblPointOfContact.Items.FindByText("Executive Point of Contact").Enabled = false;
            }
        }

    }

    private void RefreshPage()
    {
        txtFirstNameT.Text = string.Empty;
        txtSecondNameT.Text = string.Empty;
        txtRoleTitleT.Text = string.Empty;
        txtEmailAddressT.Text = string.Empty;
        txtPhoneNumberT.Text = string.Empty;
        for (var index = 0; index < cblPointOfContact.Items.Count; index++)
        {
            cblPointOfContact.Items[index].Selected = false;
        }
        btnDelete.Visible = false;
        txtFirstNameT.Focus();

        //var txtVendorNo = (ucTextbox)ucSeacrhVendor1.FindControl("txtVendorNo");
        //var lblSelectedVendorName = (ucLabel)ucSeacrhVendor1.FindControl("SelectedVendorName");
        //if (txtVendorNo != null && lblSelectedVendorName != null)
        //{
        //    txtVendorNo.Text = string.Empty;
        //    lblSelectedVendorName.Text = string.Empty;
        //    txtVendorNo.Focus();
        //}
    }

    private bool IsAnyAssignedPOCChecked()
    {
        var check = false;
        for (var index = 0; index < cblPointOfContact.Items.Count; index++)
        {
            if (cblPointOfContact.Items[index].Selected)
            {
                check = true;
                break;
            }
        }
        return check;
    }

    private List<MAS_MaintainVendorPointsContactBE> GetVendorPointsOfContacts(int venPointId=0)
    {
        var lstVendorPointsContact = new List<MAS_MaintainVendorPointsContactBE>();
        if (!venPointId.Equals(0))
        {
            var vendorPointsContactBAL = new MAS_MaintainVendorPointsContactBAL();
            var VendorPointsContact = new MAS_MaintainVendorPointsContactBE();
            VendorPointsContact.Action = "GetMaintainVendorPointsContact";
            VendorPointsContact.VendorPointID = venPointId;
            lstVendorPointsContact = vendorPointsContactBAL.GetVendorPointsContactsBAL(VendorPointsContact);
            vendorPointsContactBAL = null;
        }
        return lstVendorPointsContact;
    }

    private void SetVendorPointsOfContact(int venPointId)
    {
        var vendorPOC = this.GetVendorPointsOfContacts(venPointId).Find(vc => vc.VendorPointID.Equals(venPointId));
        if (vendorPOC != null)
        {
            if (GetQueryStringValue("ReadOnly") == "1")
            {
                btnDelete.Visible = false;
            }
            else
            {
                btnDelete.Visible = true;
            }
           
            var coiuntryIndex = ucCountry.innerControlddlCountry.Items.IndexOf(ucCountry.innerControlddlCountry.Items.FindByValue(Convert.ToString(vendorPOC.Country.CountryID)));
            ucCountry.innerControlddlCountry.SelectedIndex = coiuntryIndex;

            var txtVendorNo = (ucTextbox)ucSeacrhVendor1.FindControl("txtVendorNo");
            var lblSelectedVendorName = (ucLabel)ucSeacrhVendor1.FindControl("SelectedVendorName");
            if (txtVendorNo != null && lblSelectedVendorName != null)
            {
                txtVendorNo.Text = vendorPOC.Vendor.Vendor_No;
                lblSelectedVendorName.Text = vendorPOC.Vendor.Vendor_Name;                
            }

            txtFirstNameT.Text = vendorPOC.FirstName;
            txtSecondNameT.Text = vendorPOC.SecondName;
            txtRoleTitleT.Text = vendorPOC.RoleTitle;
            txtEmailAddressT.Text = vendorPOC.EmailAddress;
            txtPhoneNumberT.Text = vendorPOC.PhoneNumber;

            for (var index = 0; index < cblPointOfContact.Items.Count; index++)
            {
                switch (index)
                {
                    case 0:
                        if (!string.IsNullOrEmpty(vendorPOC.InventoryPOC))
                            cblPointOfContact.Items[index].Selected = vendorPOC.InventoryPOC.Equals("Y") ? true : false;
                        break;
                    case 1:
                        if (!string.IsNullOrEmpty(vendorPOC.ProcurementPOC))
                            cblPointOfContact.Items[index].Selected = vendorPOC.ProcurementPOC.Equals("Y") ? true : false;
                        break;
                    case 2:
                        if (!string.IsNullOrEmpty(vendorPOC.MerchandisingPOC))
                            cblPointOfContact.Items[index].Selected = vendorPOC.MerchandisingPOC.Equals("Y") ? true : false;
                        break;
                    case 3:
                        if (!string.IsNullOrEmpty(vendorPOC.ExecutivePOC))
                            cblPointOfContact.Items[index].Selected = vendorPOC.ExecutivePOC.Equals("Y") ? true : false;
                        break;
                }
            }
        }
    }

    #endregion
    protected void btnProceed_Click(object sender, EventArgs e)
    {
        var varVendorID = GetQueryStringValue("VendorID") != null ? Convert.ToInt32(GetQueryStringValue("VendorID")) : 0;
        EncryptQueryString("VendorEdit.aspx?ReadOnly=1&VendorID=" + varVendorID);
    }
}
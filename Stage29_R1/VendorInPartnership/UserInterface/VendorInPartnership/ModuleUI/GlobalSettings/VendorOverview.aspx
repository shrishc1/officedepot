﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="VendorOverview.aspx.cs" Inherits="VendorOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendor.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function myFunction() {
            confirm('Are you sure with YES/NO option?');
        }

        function continuePurzing(ddlValue) {
            var res = confirm('Contact is defined for this vendor.Are you sure to change status?');
            if (res) {
                if (ddlValue == "N")
                    return false;
            } else return false;
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnShow.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:HiddenField ID="hfIsActiveVendor" runat="server" />
    <h2>
        <cc1:ucLabel ID="lblVendorSetUp" runat="server"></cc1:ucLabel></h2>
    <div>
        <br />
        <asp:ValidationSummary ID="vs" runat="server" ValidationGroup="Show" ShowMessageBox="true"
            ShowSummary="true" />
        <table width="85%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
            <tr>
                <td width="15%" style="font-weight: bold;">
                    <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td width="34%">
                    <uc1:ucCountry ID="ucCountry" runat="server" Width="150px" />
                </td>
                <%--<td width="15%" style="font-weight: bold;">
                    <cc1:ucLabel ID="lblSelectSite" runat="server" Text="Select Site"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td width="34%">
                    <cc2:ucSite ID="ucSite" runat="server" Width="150px" />
                </td>--%>
            </tr>
            <%--<tr>
                <td width="15%" style="font-weight: bold;">
                    <cc1:ucLabel ID="lblRegistrationStatus" runat="server" Text="Registration Status"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td width="34%">
                    <asp:DropDownList ID="ddlRegistrationStatus" runat="server" ValidationGroup="Show"
                        Width="150px">
                        <asp:ListItem Text="--Select--" Selected="True" />
                        <asp:ListItem Text="Complete" Value="Complete" />
                        <asp:ListItem Text="Incomplete" Value="incomplete" />
                        <asp:ListItem Text="Pending" Value="Pending" />
                    </asp:DropDownList>
                </td>
                <td width="15%" style="font-weight: bold;">
                    <cc1:ucLabel ID="lblActiveVendor" runat="server"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td width="34%">
                    <asp:DropDownList ID="ddlActiveVendor" runat="server" ValidationGroup="Show" Width="150px">
                        <asp:ListItem Text="--Select--" Selected="True" />
                        <asp:ListItem Text="Yes" Value="Y" />
                        <asp:ListItem Text="No" Value="N" />
                    </asp:DropDownList>
                </td>
            </tr>--%>
            <tr>
                <td width="15%" style="font-weight: bold;">
                    <cc1:ucLabel ID="lblVendor" runat="server" />
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td width="84%" colspan="4">
                    <cc2:ucSeacrhVendor runat="server" ID="ddlSeacrhVendor" Visible="false" />
                    <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                </td>
            </tr>
           <%-- <tr>
                <td width="15%" style="font-weight: bold;">
                    <cc1:ucLabel ID="lblEuropeanorLocal" runat="server" Text="European or Local"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 1%">
                    :
                </td>
                <td width="34%">
                    <asp:DropDownList ID="ddlEuroOrLoc" runat="server" ValidationGroup="Show" Width="150px">
                        <asp:ListItem Text="--Select--" Selected="True" />
                        <asp:ListItem Text="European" Value="1" />
                        <asp:ListItem Text="Local" Value="2" />
                    </asp:DropDownList>
                </td>
                <td width="15%" style="font-weight: bold;">
                    &nbsp;
                </td>
                <td style="font-weight: bold; width: 1%">
                    &nbsp;
                </td>
                <td width="34%">
                    &nbsp;
                </td>
            </tr>--%>
            <tr align="right">
                <td colspan="6">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <cc1:ucButton ID="btnShow" ValidationGroup="Show" runat="server" Text="Show" CssClass="button"
                        OnClick="UcButtonShow_Click" />
                </td>
            </tr>

        </table>
    </div>
     <br />
      <br />
       <br />
    <div class="button-row" style="float: right;">
        <%--<cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="SitePrefixSetupEdit.aspx" />  --%>
        <cc2:ucExportToExcel ID="btnExportToExcel1" runat="server" />
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <div style="overflow-x:scroll;">
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="gvVendorOverview" Width="100%" runat="server" CssClass="grid"
                    AllowSorting="True" OnSorting="SortGrid" OnRowCommand="gvVendorOverview_RowCommand"
                    OnRowDataBound="gvVendorOverview_RowDataBound" AllowPaging="True" OnPageIndexChanging="gvVendorOverview_PageIndexChanging"
                    PageSize="30">
                    <Columns>
                        <asp:BoundField HeaderText="Vendor No" DataField="Vendor_No" SortExpression="Vendor_No">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Name" SortExpression="Vendor_Name" ItemStyle-Width="25%">
                            <HeaderStyle Width="25%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="VendorLink" runat="server" NavigateUrl='<%# EncryptQuery("VendorEdit.aspx?VendorID="+Eval("VendorID")+"&IsVendorSubscribeToPenalty="+Eval("IsVendorSubscribeToPenalty")+"&PageFrom="+"VendorOverview") %>'
                                    Text='<%#Eval("Vendor_Name") %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Country" DataField="Vendor_Country" SortExpression="Vendor_Country">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                         <asp:TemplateField HeaderText="Cons Code" SortExpression="ConsVendorVendor_No" ItemStyle-Width="25%">
                            <HeaderStyle Width="25%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="ConsCodeLink" runat="server" NavigateUrl='<%# EncryptQuery("VendorConsolidationOverview.aspx?VendorID="+Eval("VendorID")+"&PageFrom="+"VendorOverview" ) %>'
                                    Text='<%#Eval("ConsVendorVendor_No") %>' Target="_blank"></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="No of SKUs" DataField="NoOfSKU" SortExpression="NoOfSKU">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Scheduling" DataField="SchedulingContact" SortExpression="SchedulingContact">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Discreps" DataField="DiscrepancyContact" SortExpression="DiscrepancyContact">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="OTIF" DataField="OTIFContact" SortExpression="OTIFContact">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Scorecard" DataField="ScorecardContact" SortExpression="ScorecardContact">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Inventory" DataField="InventoryPOC" SortExpression="InventoryPOC">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Procurement" DataField="ProcurementPOC" SortExpression="ProcurementPOC">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Accounts Payable" DataField="MerchandisingPOC" SortExpression="MerchandisingPOC">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Executive" DataField="ExecutivePOC" SortExpression="ExecutivePOC">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Stock Planner" DataField="StockPlannerName" SortExpression="StockPlannerName">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Accounts Payable" DataField="AccounPlannerName" SortExpression="AccounPlannerName">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                         <asp:TemplateField HeaderText="Penalty Agreement" SortExpression="IsVendorSubscribeToPenalty" ItemStyle-Width="25%">
                            <HeaderStyle Width="25%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="IsVendorSubscribeToPenaltyLink" runat="server"
                                 NavigateUrl='<%# EncryptQuery("../StockOverview/BackOrder/VendorPenaltyMaintenanceEdit.aspx?VendorID="+Eval("VendorID")+"&IsVendorSubscribeToPenalty="+Eval("IsVendorSubscribeToPenalty")+"&PageFrom="+"VendorOverview") %>'
                                    Text='<%#Eval("IsVendorSubscribeToPenalty") %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Returns Agreement" DataField="IsVendorReturnsAgreement" SortExpression="IsVendorReturnsAgreement">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Returns Agreement Details" DataField="VendorReturnsAgreementDetails" SortExpression="VendorReturnsAgreementDetails">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Overstock Agreement" DataField="IsVendorOverstockAgreement" SortExpression="IsVendorOverstockAgreement">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Overstock Agreement Details" DataField="VendorOverstockAgreementDetails" SortExpression="VendorOverstockAgreementDetails">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>



                        <%--<asp:BoundField HeaderText="Registration Status" DataField="RegistrationStatus" SortExpression="RegistrationStatus">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>--%>
                        <%--<asp:BoundField HeaderText="Contact Defined" DataField="ContactDefined" SortExpression="ContactDefined">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>--%>
                       <%-- <asp:TemplateField HeaderText="Contact Defined" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblContactDefinedVO" Text='<%#Eval("ContactDefined")%>' runat="server"></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Scheduled Enabled" ItemStyle-Width="30%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <div id="div1" runat="server">
                                    <%#Eval("VendorSite") %></div>
                            </ItemTemplate>
                        </asp:TemplateField>

                       
                         <asp:TemplateField HeaderText="EuropeanorLocal" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <table width="100%;" style="text-align: left;">
                                    <tr>
                                        <td style="width: 50%;" align="left">
                                             <asp:HiddenField ID="hdnEuropeanorLocal" Value='<%#Eval("EuropeanorLocal")%>' runat="server" />
                                            <cc1:ucDropdownList runat="server" ID="ddlEuropeanorLocal">
                                                <asp:ListItem Text="European" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Local" Value="2"></asp:ListItem>
                                            </cc1:ucDropdownList>
                                        </td>                                       
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ActiveVendor" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left" >
                            <ItemTemplate>
                                <table width="100%;" style="text-align: left;">
                                    <tr>
                                        <td style="width: 50%;" align="left">
                                            <cc1:ucDropdownList runat="server" ID="ddlActiveVendor1">
                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                            </cc1:ucDropdownList>
                                        </td>
                                        <td style="width: 50%;" align="left">
                                            <asp:HiddenField ID="hdnIsActiveVendor" Value='<%#Eval("IsActiveVendor")%>' runat="server" />
                                            <cc1:ucButton runat="server" ID="btnSaveVendor" Text="Save" CommandArgument='<%#Eval("VendorID")%>'
                                                CommandName="SAVE" Tag='<%#Eval("VendorID")%>' />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>--%>


                    </Columns>
                </cc1:ucGridView>
                <cc1:ucGridView ID="gvVendorExportExcel" Width="100%" runat="server" CssClass="grid"
                    Visible="false"  OnRowDataBound="gvVendorExportExcel_RowDataBound" >
                    <Columns>
                        <asp:BoundField HeaderText="Vendor No" DataField="Vendor_No" SortExpression="Vendor_No">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Name" SortExpression="Vendor_Name" ItemStyle-Width="25%">
                            <HeaderStyle Width="25%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="VendorLink" runat="server" Text='<%#Eval("Vendor_Name") %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Country" DataField="Vendor_Country" SortExpression="Vendor_Country">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        
                        <asp:BoundField HeaderText="Cons Code" DataField="ConsVendorVendor_No" SortExpression="ConsVendorVendor_No">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>

                        <asp:BoundField HeaderText="No of SKUs" DataField="NoOfSKU" SortExpression="NoOfSKU">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                                                
                        <asp:BoundField HeaderText="Scheduling" DataField="SchedulingContact" SortExpression="SchedulingContact">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Discreps" DataField="DiscrepancyContact" SortExpression="DiscrepancyContact">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="OTIF" DataField="OTIFContact" SortExpression="OTIFContact">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Scorecard" DataField="ScorecardContact" SortExpression="ScorecardContact">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Inventory" DataField="InventoryPOC" SortExpression="InventoryPOC">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Procurement" DataField="ProcurementPOC" SortExpression="ProcurementPOC">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Accounts Payable" DataField="MerchandisingPOC" SortExpression="MerchandisingPOC">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Executive" DataField="ExecutivePOC" SortExpression="ExecutivePOC">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Stock Planner" DataField="StockPlannerName" SortExpression="StockPlannerName">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                         <asp:BoundField HeaderText="Accounts Payable" DataField="AccounPlannerName" SortExpression="AccounPlannerName">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Penalty Agreement" DataField="IsVendorSubscribeToPenalty" SortExpression="IsVendorSubscribeToPenalty">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Returns Agreement" DataField="IsVendorReturnsAgreement" SortExpression="IsVendorReturnsAgreement">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Returns Agreement Details" DataField="VendorReturnsAgreementDetails" SortExpression="VendorReturnsAgreementDetails">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Overstock Agreement" DataField="IsVendorOverstockAgreement" SortExpression="IsVendorOverstockAgreement">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Overstock Agreement Details" DataField="VendorOverstockAgreementDetails" SortExpression="VendorOverstockAgreementDetails">
                            <HeaderStyle HorizontalAlign="Left" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>




                        <%--<asp:BoundField HeaderText="Registration Status" DataField="RegistrationStatus" SortExpression="RegistrationStatus">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Contact Defined" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblContactDefinedVO" Text='<%#Eval("ContactDefined")%>' runat="server"></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Scheduled Enabled" ItemStyle-Width="30%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <div id="div1" runat="server">
                                    <%#Eval("VendorSite") %></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EuropeanorLocal" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <cc1:ucLabel ID="ltEuropeanorLocal"  Text='<%#Eval("EuropeanorLocal")%>' runat="server"></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="ActiveVendor" DataField="IsActiveVendor" SortExpression="IsActiveVendor">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>--%>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>

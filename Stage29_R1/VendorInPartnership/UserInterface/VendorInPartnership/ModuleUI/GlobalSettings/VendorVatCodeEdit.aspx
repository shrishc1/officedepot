﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
 AutoEventWireup="true" CodeFile="VendorVatCodeEdit.aspx.cs" Inherits="VendorVatCodeEdit" %>

<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <script language="javascript" type="text/javascript">
         function confirmDelete() {
             return confirm('<%=deleteMessage%>');
         }
        
    </script>
   
    <h2>
        <cc1:ucLabel ID="lblVATcodemaintananceVendor" runat="server"></cc1:ucLabel>
    </h2>
    <div>
       <asp:RequiredFieldValidator ID="rfvVatCodeRequired" runat="server" ControlToValidate="txtVatCode"
            Display="None" ValidationGroup="a"> </asp:RequiredFieldValidator>
        <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
            Style="color: Red" ValidationGroup="a" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <table width="45%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 2%">
                    </td>
                    <td style="font-weight: bold; width: 19%">
                        <cc1:ucLabel ID="lblCountry" runat="server" ></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%" align="center">
                        :
                    </td>
                    <td style="width: 77%">
                        <cc1:ucTextbox ID="txtCountry" runat="server"  Width="150px" ReadOnly="true"></cc1:ucTextbox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 2%">
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblVendorNo" runat="server" ></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold" align="center">
                        :
                    </td>
                    <td>
                        <cc1:ucTextbox runat="server" ID="txtVendorNo"  Width="150px" ReadOnly="true"></cc1:ucTextbox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 2%">
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblVendorName" runat="server" ></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold" align="center">
                        :
                    </td>
                    <td>
                        <cc1:ucTextbox runat="server" ID="txtVendorName"  Width="300px" ReadOnly="true"></cc1:ucTextbox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 2%">
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblVatCode" runat="server" ></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold" align="center">
                        :
                    </td>
                    <td>
                        <cc1:ucTextbox runat="server" ID="txtVatCode"  Width="150px" MaxLength="20"></cc1:ucTextbox>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
            ValidationGroup="a" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click" OnClientClick="return confirmDelete();" Visible="false" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

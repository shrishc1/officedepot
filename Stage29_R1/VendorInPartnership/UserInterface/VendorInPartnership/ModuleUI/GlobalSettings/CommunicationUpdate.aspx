﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="CommunicationUpdate.aspx.cs" Inherits="CommunicationUpdate" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="../../CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="uc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucMultiSelectVendor.ascx" TagName="ucMultiSelectVendor"
    TagPrefix="uc1" %>
<%@ Register Src="../../CommonUI/UserControls/MultiSelectVendorRole.ascx" TagName="MultiSelectVendorRole"
    TagPrefix="uc1" %>
<%@ Register Src="../../CommonUI/UserControls/MultiSelectOfficeDepoteRole.ascx" TagName="MultiSelectOfficeDepoteRole"
    TagPrefix="uc1" %>
<%@ Register Src="../../CommonUI/UserControls/MultiSelectCarriers.ascx" TagName="MultiSelectCarriers"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagPrefix="cc2" TagName="MultiSelectStockPlanner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    if ($('#<%=rblCommunicationType.ClientID %> input:checked').val() == "1") {
                        $('#<%=btnSearch.ClientID %>').click();
                    }
                    else if ($('#<%=rblCommunicationType.ClientID %> input:checked').val() == "2") {
                        $('#<%=btnSearch_1.ClientID %>').click();
                    }
                    else if ($('#<%=rblCommunicationType.ClientID %> input:checked').val() == "3") {
                        $('#<%=btnSearch_2.ClientID %>').click();
                    }
                    else {
                        $('#<%=btnSearch.ClientID %>').click();
                    }
                    return false;
                }
            });
        });
        function PrintScreen() {
            document.getElementById('rwButtons').style.visibility = "hidden";
            window.print();
            document.getElementById('rwButtons').style.visibility = "visible";
        }

        var gridCheckedCount = 0;

        $(document).ready(function () {
            SelectAllGrid($("#chkSelectAllText")[0]);
        });


        function SelectAllGrid(chkBoxAllObj) {
            if (chkBoxAllObj != null) {
                if (chkBoxAllObj.checked) {

                    $("input[type='checkbox'][name$='chkSelect']").attr('checked', true);
                    gridCheckedCount = $("input[type='checkbox'][name$='chkSelect']:checked").size();
                }
                else {
                    $("input[type='checkbox'][name$='chkSelect']").attr('checked', false);
                    gridCheckedCount = 0;
                }
            }
        }


        function CheckUncheckVendor(obj) {
            var rouNum = $(obj).parents().find('input[type="hidden"]').val();
            if (rouNum != '') {
                if (!obj.checked) {
                    var ids = $('#<%= hdnFldSelectedValues.ClientID %>').val();
                    ids = ids.replace(rouNum + ',', "");
                    ids = ids.replace(rouNum, "");
                    $('#<%= hdnFldSelectedValues.ClientID %>').val(ids);
                }
            }

            if ($("input[type='checkbox'][name$='chkSelect']:checked").size() == $("input[type='checkbox'][name$='chkSelect']").length) {
                $("#chkSelectAllText").attr('checked', true);
                gridCheckedCount = $("#<%=gvVendors.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }

            else {
                $("#chkSelectAllText").attr('checked', false);
                gridCheckedCount = $("#<%=gvVendors.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
        }

        function CheckUncheckCarrier(obj) {
            var rouNum = $(obj).parents().find('input[type="hidden"]').val();
            if (rouNum != '') {
                if (!obj.checked) {
                    var ids = $('#<%= hdnFldSelectedValues.ClientID %>').val();
                    ids = ids.replace(rouNum + ',', "");
                    ids = ids.replace(rouNum, "");
                    $('#<%= hdnFldSelectedValues.ClientID %>').val(ids);
                }
            }
            if ($("input[type='checkbox'][name$='chkSelect']:checked").size() == $("input[type='checkbox'][name$='chkSelect']").length) {
                $("#chkSelectAllText").attr('checked', true);
                gridCheckedCount = $("#<%=gvCarriers.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
            else {
                $("#chkSelectAllText").attr('checked', false);
                gridCheckedCount = $("#<%=gvCarriers.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
        }

        function CheckUncheckOfficeDepot(obj) {
            var rouNum = $(obj).parents().find('input[type="hidden"]').val();
            if (rouNum != '') {
                if (!obj.checked) {
                    var ids = $('#<%= hdnFldSelectedValues.ClientID %>').val();
                    ids = ids.replace(rouNum + ',', "");
                    ids = ids.replace(rouNum, "");
                    $('#<%= hdnFldSelectedValues.ClientID %>').val(ids);
                }
            }
            if ($("input[type='checkbox'][name$='chkSelect']:checked").size() == $("input[type='checkbox'][name$='chkSelect']").length) {
                $("#chkSelectAllText").attr('checked', true);
                gridCheckedCount = $("#<%=gvOfficeDepots.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
            else {
                $("#chkSelectAllText").attr('checked', false);
                gridCheckedCount = $("#<%=gvOfficeDepots.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
        }

        function CommLibraryProceedMessage() {
            var hdnFldSelectedValues = $('#<%= hdnFldSelectedValues.ClientID %>').val()
            var lastChar = hdnFldSelectedValues.slice(-1);
            if (lastChar == ',') {
                hdnFldSelectedValues = hdnFldSelectedValues.slice(0, -1);
            }
            hdnFldSelectedValues = hdnFldSelectedValues.split(",");
            gridCheckedCount = hdnFldSelectedValues;
            if (gridCheckedCount == 0) {
                if ($('#<%= rblCommunicationType.ClientID %> input:radio')[0].checked) {
                    $("#<%=gvVendors.ClientID%> input[id*='chkSelect']:checkbox").each(function (index) {
                        if ($(this).is(':checked'))
                            gridCheckedCount++;
                    });
                }
                if ($('#<%= rblCommunicationType.ClientID %> input:radio')[1].checked) {
                    $("#<%=gvCarriers.ClientID%> input[id*='chkSelect']:checkbox").each(function (index) {
                        if ($(this).is(':checked'))
                            gridCheckedCount++;
                    });
                }
                if ($('#<%= rblCommunicationType.ClientID %> input:radio')[2].checked) {
                    $("#<%=gvOfficeDepots.ClientID%> input[id*='chkSelect']:checkbox").each(function (index) {
                        if ($(this).is(':checked'))
                            gridCheckedCount++;
                    });
                }
            }


            var commLibProceedMessage = '<%=commLibProceedMessage%>';
            if (gridCheckedCount > 50) {
                gridCheckedCount = 50;
            }

            if (gridCheckedCount < 1) {
                alert(commLibProceedMessage);
                return false;
            }
        }

        function EmailSendConfirmation() {
            if (confirm('<%=doYouWantToSubmit%>')) {
                return true;
            }
            else {
                return false;
            }
        }

        function CommLibDataLostConfirmation() {
            if (confirm('<%=commLibBackDataLostConfirmation%>')) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>
    <h2>
        <cc1:ucLabel ID="lblCommunicationUpdate" Text="CommunicationUpdate" runat="server"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnFldSelectedValues" runat="server" />
    <asp:HiddenField ID="hdnAllValues" runat="server" />

    <div class="button-row" style="float: right;">
        <cc2:ucExportToExcel ID="btnExportVendors" runat="server" Visible="false" />
        <cc2:ucExportToExcel ID="btnExportCarriers" runat="server" Visible="false" />
        <cc2:ucExportToExcel ID="btnExportOfficeDepots" runat="server" Visible="false" />
        <cc2:ucExportToExcel ID="btnExportOfficeDepot_Sps" runat="server" Visible="false" />
    </div>

    <div>
        <table class="top-settings" width="52%" id="tblHeader" runat="server">
            <tr>
                <td style="font-weight: bold;" align="center">
                    <table width="100%" cellspacing="5" cellpadding="0" border="0">
                        <tr>
                            <td style="font-weight: bold;" class="nobold radiobuttonlist">
                                <asp:RadioButtonList ID="rblCommunicationType" runat="server" RepeatColumns="4" RepeatDirection="Horizontal"
                                    CssClass="radio-fix" OnSelectedIndexChanged="rblCommunicationType_SelectedIndexChanged"
                                    AutoPostBack="true">
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <br />

    <asp:Panel ID="pnlCommonCommunication" runat="server">
        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
            <tr>
                <td style="font-weight: bold; width: 10%;">
                    <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 2%;">
                    <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                </td>
                <td style="width: 88%;">
                    <uc1:MultiSelectCountry ID="msCountry" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel ID="pnlVendorsCommunication" runat="server">
        <asp:Panel ID="pnlSubVendComm1" runat="server" Visible="true">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 10%;">
                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 2%;">
                        <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="width: 88%;">
                        <uc1:ucMultiSelectVendor ID="msVendor" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblRole" runat="server" Text="Role"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                    </td>
                    <td>
                        <uc1:MultiSelectVendorRole ID="msVendorRole" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblEuropeanLocal" runat="server" Text="European Local"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                    </td>
                    <td>
                        <cc1:ucDropdownList ID="ddlEuropeanLocal" runat="server">
                            <asp:ListItem Text="European Vendor Only" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Local Vendor Only" Value="2"></asp:ListItem>
                            <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
                        </cc1:ucDropdownList>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="3">
                        <div class="button-row">
                            <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlSubVendComm2" runat="server" Visible="false">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td colspan="3">
                        <cc1:ucGridView ID="gvVendors" Width="100%" runat="server" CssClass="grid" OnRowDataBound="gvVendors_RowDataBound" OnPageIndexChanging="gvVendors_PageIndexChanging"
                            AllowPaging="true" PageSize="50">
                            <Columns>
                                <asp:TemplateField HeaderText="SelectAllCheckBox">
                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                    <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderTemplate>
                                        <cc1:ucCheckbox runat="server" ID="chkSelectAllText" CssClass="checkbox-input" AutoPostBack="true" OnCheckedChanged="chkSelectAllText_CheckedChanged" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" onclick="CheckUncheckVendor(this);" />
                                        <asp:HiddenField ID="hdnRowNum" runat="server" Value='<%# Eval("RowNum") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Country" SortExpression="Country">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblCountryValue" runat="server" Text='<%# Eval("Country") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="VendorNo" SortExpression="VendorNo">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblVendorNoValue" runat="server" Text='<%# Eval("VendorNo") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="VendorName" SortExpression="VendorName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblVendorNameValue" runat="server" Text='<%# Eval("VendorName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UserName" SortExpression="UserName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblUserNameValue" runat="server" Text='<%# Eval("UserName") %>'></cc1:ucLabel>
                                        <cc1:ucLiteral ID="ltUserEmailValue" Text='<%# Eval("EmailId") %>' Visible="false"
                                            runat="server" />
                                        <cc1:ucLiteral ID="ltUserIDValue" Text='<%# Eval("UserID") %>' Visible="false" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                        <cc1:ucGridView ID="gvVendorsExcel" Width="100%" runat="server" CssClass="grid" Visible="false">
                            <Columns>

                                <asp:TemplateField HeaderText="Country" SortExpression="Country">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblCountryValue" runat="server" Text='<%# Eval("Country") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="VendorNo" SortExpression="VendorNo">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblVendorNoValue" runat="server" Text='<%# Eval("VendorNo") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="VendorName" SortExpression="VendorName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblVendorNameValue" runat="server" Text='<%# Eval("VendorName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UserName" SortExpression="UserName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblUserNameValue" runat="server" Text='<%# Eval("UserName") %>'></cc1:ucLabel>
                                        <cc1:ucLiteral ID="ltUserEmailValue" Text='<%# Eval("EmailId") %>' Visible="false"
                                            runat="server" />
                                        <cc1:ucLiteral ID="ltUserIDValue" Text='<%# Eval("UserID") %>' Visible="false" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="3">
                        <div class="button-row">
                            <cc1:ucButton ID="btnProceed" runat="server" Text="Proceed" CssClass="button" OnClick="btnProceed_Click"
                                Visible="false" OnClientClick="return CommLibraryProceedMessage();" />
                            <cc1:ucButton ID="btnBack_1" runat="server" Text="Back" CssClass="button" OnClick="btnBack_1_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>

    <asp:Panel ID="pnlCarriersCommunication" runat="server">
        <asp:Panel ID="pnlSubCarrComm1" runat="server" Visible="true">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 10%;">
                        <cc1:ucLabel ID="lblCarrier" Text="Carrier" runat="server" />
                    </td>
                    <td style="font-weight: bold; width: 2%">:
                    </td>
                    <td style="font-weight: bold; width: 88%;">
                        <uc1:MultiSelectCarriers runat="server" ID="msCarrier" />
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="3">
                        <div class="button-row">
                            <cc1:ucButton ID="btnSearch_1" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_1_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlSubCarrComm2" runat="server" Visible="false">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td colspan="3">
                        <cc1:ucGridView ID="gvCarriers" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid"
                            OnPageIndexChanging="gvCarriers_PageIndexChanging" OnRowDataBound="gvCarriers_RowDataBound" AllowPaging="true" PageSize="50">
                            <Columns>
                                <asp:TemplateField HeaderText="SelectAllCheckBox">
                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                    <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderTemplate>
                                        <cc1:ucCheckbox runat="server" ID="chkSelectAllText" CssClass="checkbox-input" AutoPostBack="true" OnCheckedChanged="UcCheckbox1_CheckedChanged" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" onclick="CheckUncheckCarrier(this);" />
                                        <asp:HiddenField ID="hdnRowNum" runat="server" Value='<%# Eval("RowNum") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Country" SortExpression="Country">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblCountryValue" runat="server" Text='<%# Eval("Country") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CarrierName" SortExpression="CarrierName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblCarrierNameValue" runat="server" Text='<%# Eval("CarrierName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UserName" SortExpression="UserName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblUserNameValue" runat="server" Text='<%# Eval("UserName") %>'></cc1:ucLabel>
                                        <cc1:ucLiteral ID="ltUserEmailValue" Text='<%# Eval("EmailId") %>' Visible="false"
                                            runat="server" />
                                        <cc1:ucLiteral ID="ltUserIDValue" Text='<%# Eval("UserID") %>' Visible="false" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                        <cc1:ucGridView ID="gvCarriersExcel" Width="100%" runat="server" CssClass="grid"
                            OnSorting="SortGrid" Visible="false">
                            <Columns>
                                <asp:TemplateField HeaderText="Country" SortExpression="Country">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblCountryValue" runat="server" Text='<%# Eval("Country") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CarrierName" SortExpression="CarrierName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblCarrierNameValue" runat="server" Text='<%# Eval("CarrierName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UserName" SortExpression="UserName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblUserNameValue" runat="server" Text='<%# Eval("UserName") %>'></cc1:ucLabel>
                                        <cc1:ucLiteral ID="ltUserEmailValue" Text='<%# Eval("EmailId") %>' Visible="false"
                                            runat="server" />
                                        <cc1:ucLiteral ID="ltUserIDValue" Text='<%# Eval("UserID") %>' Visible="false" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="3">
                        <div class="button-row">
                            <cc1:ucButton ID="btnProceed_1" runat="server" Text="Proceed" CssClass="button" OnClick="btnProceed_1_Click"
                                Visible="false" OnClientClick="return CommLibraryProceedMessage();" />
                            <cc1:ucButton ID="btnBack_2" runat="server" Text="Back" CssClass="button" OnClick="btnBack_2_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>

    <asp:Panel ID="pnlOfficeDepotCommunication" runat="server">

        <asp:Panel ID="pnlSubODComm1" runat="server" Visible="true">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 10%;">
                        <cc1:ucLabel ID="lblOfficeDepotRole" runat="server" Text="Office Depot Role"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 2%;">
                        <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 88%;">
                        <uc1:MultiSelectOfficeDepoteRole ID="msOfficeDepoteRole" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="3">
                        <div class="button-row">
                            <cc1:ucButton ID="btnSearch_2" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_2_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <asp:Panel ID="pnlSubODComm_StockPlanner" runat="server">
            <div class="button-row">
                <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
            </div>

            <table width="100%">
                <tr>
                    <td align="center" colspan="9">
                        <cc1:ucGridView ID="GvStockPlanarDiscrepancyTrackerReport" Width="100%" runat="server" CssClass="grid"
                            OnSorting="SortGrid" AllowSorting="true" OnRowCommand="GvStockPlanarDiscrepancyTrackerReport_RowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText="Week" SortExpression="Week">
                                    <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hpWeek" runat="server" Text='<%# Eval("Week") %>'
                                            NavigateUrl='<%# EncryptQuery("StockPlanarDiscrepancyTrackerReportEdit.aspx?DiscrepancyTrackerId="+ Eval("DiscrepancyTrackerId") + "&MailSent=" + Eval("MailSent") + "&File=" + Eval("ExcelPath"))%>'></asp:HyperLink>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Year" ItemStyle-Wrap="false" DataField="Year"
                                    SortExpression="Year">
                                    <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="EmailSubject" ItemStyle-Wrap="false" DataField="EmailSubject"
                                    SortExpression="EmailSubject">
                                    <HeaderStyle Width="35%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="MailSent" SortExpression="MailSent">
                                    <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblWeek" runat="server" Text='<%#Convert.ToBoolean( Eval("MailSent")) ==true ?"Yes" :"No" %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>

                                <asp:BoundField HeaderText="Date Created" DataFormatString="{0:dd-MM-yyyy}" ItemStyle-Wrap="false"
                                    DataField="CreatedDate" SortExpression="CreatedDate">
                                    <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>

                                <asp:ButtonField ButtonType="Link" HeaderStyle-Width="25%" Text="Click to Download" CommandName="Dwn" HeaderText="Files" />

                                <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnWeekFile" runat="server" Value='<%# Eval("ExcelPath") %>'></asp:HiddenField>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                </tr>
            </table>

        </asp:Panel>

        <asp:Panel ID="pnlSubODComm2" runat="server" Visible="false">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td colspan="3">
                        <cc1:ucGridView ID="gvOfficeDepots" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid"
                            OnPageIndexChanging="gvOfficeDepots_PageIndexChanging" OnRowDataBound="gvOfficeDepots_RowDataBound" AllowPaging="true" PageSize="50">
                            <Columns>
                                <asp:TemplateField HeaderText="SelectAllCheckBox">
                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                    <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderTemplate>
                                        <cc1:ucCheckbox runat="server" ID="chkSelectAllText" AutoPostBack="true" OnCheckedChanged="chkSelectAllText_CheckedChanged1" CssClass="checkbox-input" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" onclick="CheckUncheckOfficeDepot(this);" />
                                        <asp:HiddenField ID="hdnRowNum" runat="server" Value='<%# Eval("RowNum") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UserName" SortExpression="UserName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblUserNameValue" runat="server" Text='<%# Eval("UserName") %>'></cc1:ucLabel>
                                        <cc1:ucLiteral ID="ltUserEmailValue" Text='<%# Eval("EmailId") %>' Visible="false"
                                            runat="server" />
                                        <cc1:ucLiteral ID="ltUserIDValue" Text='<%# Eval("UserID") %>' Visible="false" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Role" SortExpression="RoleName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblRoleNameValue" runat="server" Text='<%# Eval("RoleName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                        <cc1:ucGridView ID="gvOfficeDepotsExcel" Width="100%" runat="server" CssClass="grid"
                            OnSorting="SortGrid" Visible="false">
                            <Columns>

                                <asp:TemplateField HeaderText="UserName" SortExpression="UserName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblUserNameValue" runat="server" Text='<%# Eval("UserName") %>'></cc1:ucLabel>
                                        <cc1:ucLiteral ID="ltUserEmailValue" Text='<%# Eval("EmailId") %>' Visible="false"
                                            runat="server" />
                                        <cc1:ucLiteral ID="ltUserIDValue" Text='<%# Eval("UserID") %>' Visible="false" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Role" SortExpression="RoleName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <cc1:ucLabel ID="lblRoleNameValue" runat="server" Text='<%# Eval("RoleName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="3">
                        <div class="button-row">
                            <cc1:ucButton ID="btnProceed_2" runat="server" Text="Proceed" CssClass="button" OnClick="btnProceed_2_Click"
                                Visible="false" OnClientClick="return CommLibraryProceedMessage();" />
                            <cc1:ucButton ID="btnBack_3" runat="server" Text="Back" CssClass="button" OnClick="btnBack_3_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </asp:Panel>

    <asp:Panel ID="pnlCommunication" runat="server" Visible="false">

        <asp:Panel ID="pnlManageLetter" runat="server" Visible="true">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="font-weight: bold; width: 15%;">
                        <cc1:ucLabel ID="lblPleaseenterSubject" runat="server" Text="Please enter Subject"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 2%;">
                        <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 83%;">
                        <cc1:ucTextbox ID="txtSubjectText" runat="server" Width="400px"></cc1:ucTextbox>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 100%;" colspan="3">
                        <cc1:ucLabel ID="lblPleaseenteremailcommunicationbelow" runat="server" Text="Please enter email communication below"></cc1:ucLabel>&nbsp;:
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 100%;" colspan="3">
                        <CKEditor:CKEditorControl ID="txtEmailCommunication" Height="400px" runat="server"></CKEditor:CKEditorControl>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 15%;">
                        <cc1:ucLabel ID="lblSentOnBehalfOf" runat="server" Text="Sent on Behalf of"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 2%;">
                        <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 83%;">
                        <cc1:ucTextbox ID="txtSentonBehalfofText" runat="server" Width="260px"></cc1:ucTextbox>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 15%;">
                        <cc1:ucLabel ID="lblTitle" runat="server" Text="Title" Visible="false"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 2%;">
                        <cc1:ucLabel ID="UcLabel8" runat="server" Visible="false">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 83%;">
                        <cc1:ucTextbox ID="txtTitleText" runat="server" Width="400px" Visible="false"></cc1:ucTextbox>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="3">
                        <div class="button-row">
                            <cc1:ucButton ID="btnSubmit" runat="server" Text="Submit" CssClass="button" OnClick="btnSubmit_Click" />
                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click"
                                OnClientClick="return CommLibDataLostConfirmation();" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <asp:Panel ID="pnlDisplayLetter" runat="server" Visible="false">
            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td align="left" style="width: 100%">
                        <div id="dvDisplayLetter" runat="server">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <div class="button-row" style="text-align: center !important;">
                            <cc1:ucButton ID="btnYes" runat="server" Text="Yes" CssClass="button" OnClick="btnYes_Click"
                                OnClientClick="return EmailSendConfirmation();" />
                            <cc1:ucButton ID="btnNo" runat="server" Text="No" CssClass="button" OnClick="btnNo_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </asp:Panel>


</asp:Content>

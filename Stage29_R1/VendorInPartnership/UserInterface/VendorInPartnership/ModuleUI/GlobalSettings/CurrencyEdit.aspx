﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" 
AutoEventWireup="true" CodeFile="CurrencyEdit.aspx.cs" Inherits="CurrencyEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblCreateEditCurrency" runat="server" Text="Create / Edit Currency Type"></cc1:ucLabel>
    </h2>
    <div>
        <asp:RequiredFieldValidator ID="rfvCurrencyType" ErrorMessage="Please enter Currency Type."
            runat="server" ControlToValidate="txtCurrencyType" Display="None" ValidationGroup="a"> </asp:RequiredFieldValidator>
        <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
            Style="color: Red" ValidationGroup="a" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 5%">
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblCurrency" runat="server"  isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold" align="center">
                        :
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtCurrencyType" runat="server" Width="90%" MaxLength="20"></cc1:ucTextbox>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" ValidationGroup="a"  />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Collections.Generic;


public partial class STK_CurrencyConvertorOverview : CommonPage {
    protected void Page_Load(object sender, EventArgs e) {
      

        if (!IsPostBack) {
            BindGrid();
        }
        ucExportToExcel1.GridViewControl = grdConversion;
        ucExportToExcel1.FileName = "CurrencyConvertorOverview";
    }

    #region Methods

    protected void BindGrid()
    {

        MAS_CurrencyConversionBE oMAS_RegionBE = new MAS_CurrencyConversionBE();
        MAS_CurrencyConversionBAL oMAS_RegionBAL = new MAS_CurrencyConversionBAL();
        List<MAS_CurrencyConversionBE> lstCurrencyConversion = new List<MAS_CurrencyConversionBE>();
        oMAS_RegionBE.Action = "ShowAll";
        lstCurrencyConversion = oMAS_RegionBAL.GetCurrenyConversionBAL(oMAS_RegionBE);
        oMAS_RegionBAL = null;
        grdConversion.DataSource = lstCurrencyConversion;
        grdConversion.DataBind();
        ViewState["lstSites"] = lstCurrencyConversion;
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MAS_CurrencyConversionBE>.SortList((List<MAS_CurrencyConversionBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }

    #endregion
}
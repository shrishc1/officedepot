﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;

using Utilities; using WebUtilities;


public partial class STK_CurrencyConvertorEdit : CommonPage 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        

       //this.rfvDateValidate.ControlToValidate = txtDate.innerControltxtDate.Value;

        if (!IsPostBack)
        {
            getCurrencyConvertordetail();
        }
    }

    private void getCurrencyConvertordetail()
    {
        MAS_CurrencyConversionBE oSYS_CurrencyConversionBE = new MAS_CurrencyConversionBE();
        MAS_CurrencyConversionBAL oSYS_CurrencyConversionBAL = new MAS_CurrencyConversionBAL();

        if (GetQueryStringValue("CurrencyConversionID") != null)
        {
            oSYS_CurrencyConversionBE.Action = "ShowAll";
            oSYS_CurrencyConversionBE.CurrencyConversionID = Convert.ToInt32(GetQueryStringValue("CurrencyConversionID"));

            List<MAS_CurrencyConversionBE> lstCCDetails = new List<MAS_CurrencyConversionBE>();
            lstCCDetails = oSYS_CurrencyConversionBAL.GetCurrenyConversionBAL(oSYS_CurrencyConversionBE);
            oSYS_CurrencyConversionBAL = null;

            if (lstCCDetails != null)
            {

                txtRateAppDate.innerControltxtDate.Value = Convert.ToDateTime(lstCCDetails[0].RateApplicableDate).ToString("dd/MM/yyyy");
                txtGBP.Text = Convert.ToString(lstCCDetails[0].ConversionRate);
            }


        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        MAS_CurrencyConversionBE oMAS_CurrencyConversionBE = new MAS_CurrencyConversionBE();
        MAS_CurrencyConversionBAL oMAS_CurrencyConversionBAL = new MAS_CurrencyConversionBAL();


        oMAS_CurrencyConversionBE.RateApplicableDate = Common.GetMM_DD_YYYY(txtRateAppDate.innerControltxtDate.Value);
        oMAS_CurrencyConversionBE.ConversionRate = Convert.ToDecimal(txtGBP.Text);
        oMAS_CurrencyConversionBE.Action = "CheckExistance";
        if (GetQueryStringValue("CurrencyConversionID") != null)
        {
            oMAS_CurrencyConversionBE.CurrencyConversionID = Convert.ToInt32(GetQueryStringValue("CurrencyConversionID"));
        }
        List<MAS_CurrencyConversionBE> lstCCDetails = oMAS_CurrencyConversionBAL.GetCurrenyConversionBAL(oMAS_CurrencyConversionBE);

        if (lstCCDetails.Count > 0)
        {
            string errorMessage = WebCommon.getGlobalResourceValue("RateApplicableDateExist");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
        }
        else
        {
            if (GetQueryStringValue("CurrencyConversionID") != null)
            {
                oMAS_CurrencyConversionBE.Action = "UpdateCurrencyConversion";
                oMAS_CurrencyConversionBE.CurrencyConversionID = Convert.ToInt32(GetQueryStringValue("CurrencyConversionID"));
            }
            else
            {
                oMAS_CurrencyConversionBE.Action = "AddCurrencyConversion";

            }
            int? iResult = oMAS_CurrencyConversionBAL.addEditCurrencyConversionBAL(oMAS_CurrencyConversionBE);           
            if (iResult == 0)
            {
                string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
                EncryptQueryString("STK_CurrencyConvertorOverview.aspx");
            }
        }
        oMAS_CurrencyConversionBAL = null;


       

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("STK_CurrencyConvertorOverview.aspx");
    }
    
}
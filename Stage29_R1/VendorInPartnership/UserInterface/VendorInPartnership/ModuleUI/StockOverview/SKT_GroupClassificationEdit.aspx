﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="SKT_GroupClassificationEdit.aspx.cs" Inherits="SKT_GroupClassificationEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $('#<%=btnDiscontinued.ClientID%>').click(function () {
                if ($('#<%=txtDescriptionEnding.ClientID%>').val()=='') {
                    alert('<%=GroupClassificationErrorMsg%>');
                    return false;
                }
            });

            $('#<%=btnPendingDiscontinued.ClientID%>').click(function () {
                if ($('#<%=txtDescriptionEnding.ClientID%>').val() == '') {
                    alert('<%=GroupClassificationErrorMsg%>');
                    return false;
                }
            });

            $('#<%=btnUnderReview.ClientID%>').click(function () {
                if ($('#<%=txtDescriptionEnding.ClientID%>').val() == '') {
                    alert('<%=GroupClassificationErrorMsg%>');
                    return false;
                }
            });

        });
        function CheckUniqueDesc() {

            var Desc = document.getElementById('<%=txtDescriptionEnding.ClientID %>').value;
            var lstDiscontinued = document.getElementById('<%=UclstDiscontinued.ClientID %>');
            var lstPenDiscontinued = document.getElementById('<%=UclstPenDiscontinued.ClientID %>');
            var lstUnderReview = document.getElementById('<%=UclstUnderReview.ClientID %>');

            if (lstDiscontinued.options.length > 0) {
                for (var x = 0; x < lstDiscontinued.options.length; x++) {
                    if (lstDiscontinued.options[x].value == Desc) {
                        alert("'" + Desc + "' " + "<%=DiscontinuedMessage%>");
                        return false;
                    }
                }
            }
            if (lstPenDiscontinued.options.length > 0) {
                for (var x = 0; x < lstPenDiscontinued.options.length; x++) {
                    if (lstPenDiscontinued.options[x].value == Desc) {
                        alert("'" + Desc + "' " + "<%=PendingDiscontinuedMessage%>");
                        return false;
                    }
                }
            }
            if (lstUnderReview.options.length > 0) {
                for (var x = 0; x < lstUnderReview.options.length; x++) {
                    if (lstUnderReview.options[x].value == Desc) {
                        alert("'" + Desc + "' " + "<%=UnderReviewMessage%>");
                        return false;
                    }
                }
            }
        }   
    
    </script>
    <h2>
        <cc1:ucLabel ID="lblGroupClassification" runat="server" Text="Group Classification"></cc1:ucLabel>
    </h2>
    <script language="javascript" type="text/javascript">
   
    </script>
    <div class="right-shadow">
        <div class="formbox">
            <table width="30%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 10%">
                    </td>
                    <td style="font-weight: bold; width: 30%">
                        <cc1:ucLabel ID="lblSitePrefixName" runat="server" Text="Site" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5%">
                        :
                    </td>
                    <td style="width: 45%">
                        <cc2:ucSite ID="ucSite" runat="server" />
                    </td>
                    <td style="width: 10%">
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" cellspacing="7" cellpadding="0" border="0" class="top-settings">
            <tr>
            <td valign="middle" align="center">
             <cc1:ucRadioButton ID="rdoEndsWIth" runat="server" GroupName="abc"
                                    Text="Ends With"  />
            </td>
            <td valign="middle" align="center"> 
             <cc1:ucRadioButton ID="rdoBeginsWith" runat="server" GroupName="abc"
                                    Text="Begins With"  />
            </td>
            <td valign="middle" align="center">
             <cc1:ucRadioButton ID="rdoCOntains" runat="server" GroupName="abc"
                                    Text="Contains" Checked="true" />
            </td>
            </tr>
            <tr>
                    <td colspan="3">
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;" width="33%">
                    </td>
                    <td style="font-weight: bold;" width="33%" align="center">
                        <cc1:ucLabel ID="lblText" runat="server" Text="Text"></cc1:ucLabel>
                        &nbsp;
                        <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        &nbsp;
                        <cc1:ucTextbox ID="txtDescriptionEnding" runat="server" Width="60px" MaxLength="7"
                            ></cc1:ucTextbox>
                    </td>
                    <td style="font-weight: bold;" width="33%">
                    </td>
                </tr>
                
                <tr>
                    <td colspan="3">
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="center">
                        <div>
                            <cc1:ucButton ID="btnDiscontinued" runat="server" Text="Discontinued" CssClass="button"
                                OnClientClick="return CheckUniqueDesc()" Width="100px" OnClick="btnDiscontinued_Click" /></div>
                        &nbsp;
                        <div>
                            <cc1:ucButton ID="btnRemove" runat="server" Text="Remove" CssClass="button" Width="70px"
                                OnClick="btnRemove_Click" /></div>
                    </td>
                    <td valign="middle" align="center">
                        <div>
                            <cc1:ucButton ID="btnPendingDiscontinued" runat="server" Text="Pending Discontinued"
                                OnClientClick="return CheckUniqueDesc()" CssClass="button" Width="140px" OnClick="btnPendingDiscontinued_Click" /></div>
                        &nbsp;
                        <div>
                            <cc1:ucButton ID="btnRemove_1" runat="server" Text="Remove" CssClass="button" Width="70px"
                                OnClick="btnRemove_1_Click" /></div>
                    </td>
                    <td valign="middle" align="center">
                        <div>
                            <cc1:ucButton ID="btnUnderReview" runat="server" Text="Under Review" CssClass="button"
                                OnClientClick="return CheckUniqueDesc()" Width="100px" OnClick="btnUnderReview_Click" /></div>
                        &nbsp;
                        <div>
                            <cc1:ucButton ID="btnRemove_2" runat="server" Text="Remove" CssClass="button" Width="70px"
                                OnClick="btnRemove_2_Click" /></div>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="center">
                        <cc1:ucListBox ID="UclstDiscontinued" runat="server" Height="200px" Width="160px">
                        </cc1:ucListBox>
                    </td>
                    <td valign="middle" align="center">
                        <cc1:ucListBox ID="UclstPenDiscontinued" runat="server" Height="200px" Width="160px">
                        </cc1:ucListBox>
                    </td>
                    <td valign="middle" align="center">
                        <cc1:ucListBox ID="UclstUnderReview" runat="server" Height="200px" Width="160px">
                        </cc1:ucListBox>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click" />
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccessLayer.ModuleDAL.StockOverview.Report;

public partial class VendorOverviewSummaryPaging : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            lblTotalCount.Text = Convert.ToString(DataAccessLayer.ModuleDAL.StockOverview.Report.StockOverviewReportDAL.getStockOverviewCount(txtFromDate.Text, txtToDate.Text));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using WebUtilities;
using Utilities;

public partial class ModuleUI_StockOverview_Report_ItemSummaryReport : CommonPage
{
    int? ReportRequest = 0;
    ReportRequestBE oReportRequestBE = new ReportRequestBE();
    ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();

    protected string Pleaseaddsearchcriteria = WebCommon.getGlobalResourceValue("Pleaseaddsearchcriteria");
    protected string Maximumonesiteisallowed = WebCommon.getGlobalResourceValue("Maximumonesiteisallowed");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e) {
        try {

            //UcDataPanel.Visible = false;
            //UcReportPanel.Visible = true;

            if (string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs) || string.IsNullOrWhiteSpace(msStockPlanner.SelectedStockPlannerIDs))
            {
                if (string.IsNullOrEmpty(msVendor.SelectedVendorIDs) || string.IsNullOrWhiteSpace(msVendor.SelectedVendorIDs))
                {
                    if (string.IsNullOrEmpty(UcVikingSku.SelectedSKUName))
                    {
                        if (string.IsNullOrEmpty(msSKU.SelectedSKUName))
                        {
                            if (string.IsNullOrEmpty(msSite.SelectedSiteIDs) || string.IsNullOrWhiteSpace(msSite.SelectedSiteIDs))
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Pleaseaddsearchcriteria + "')", true);
                                return;
                            }
                            else
                            {
                                string[] SelectedSite = msSite.SelectedSiteIDs.Split(',');
                                if (SelectedSite.Length > 1)
                                {
                                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Maximumonesiteisallowed + "')", true);
                                    return;
                                }

                            }

                        }
                    }
                }
            }

            oReportRequestBE.Action = "InsertReportRequest";
            oReportRequestBE.ModuleName = "StockOverview_ItemSummaryReport";
            oReportRequestBE.ReportName = "Item Summary";
            
            oReportRequestBE.ReportAction = "ItemSummaryReport";
            oReportRequestBE.ReportNamePath = "ItemSummaryReport.rdlc";
            oReportRequestBE.ReportDatatableName = "dtItemSummaryReport";
            oReportRequestBE.ReportType = "Report";



            oReportRequestBE.SiteIDs = msSite.SelectedSiteIDs;
            oReportRequestBE.SiteName = msSite.SelectedSiteName;
            oReportRequestBE.VendorIDs = msVendor.SelectedVendorIDs;
            oReportRequestBE.VendorName = msVendor.SelectedVendorName;           
            oReportRequestBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);
            oReportRequestBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Utilities.Common.TextToDateFormat(hdnJSToDt.Value);
            oReportRequestBE.OfficeDepotSKU = msSKU.SelectedSKUName;
            oReportRequestBE.ODCatCode = UcVikingSku.SelectedSKUName;
            oReportRequestBE.StockPlannerName = msStockPlanner.SelectedSPName;
            oReportRequestBE.StockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;

            oReportRequestBE.RequestStatus = "Pending";
            oReportRequestBE.UserID = Convert.ToInt32(Session["UserID"]);
            oReportRequestBE.RequestTime = DateTime.Now;

            ReportRequest = oReportRequestBAL.addReportRequestBAL(oReportRequestBE);
            if (ReportRequest > 0) {
                string ReportRequestSubmitted = WebCommon.getGlobalResourceValue("ReportRequestSubmitted");
                ReportRequestSubmitted = ReportRequestSubmitted + " : " + Convert.ToString(ReportRequest);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReportRequestSubmitted + "')", true);
                return;
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("ItemSummaryReport.aspx");
    }
}
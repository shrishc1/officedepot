﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VendorOverviewSummaryPaging.aspx.cs"
    Inherits="VendorOverviewSummaryPaging" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblVendorOverview" runat="server" Text="Vendor Overview"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%">
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                    </td>
                    <td align="left">
                        <table width="30%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-weight: bold; width: 6em;">
                                    <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                        onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                                </td>
                                <td style="font-weight: bold; width: 4em; text-align: center">
                                    <cc1:ucLabel ID="UcLabel1" runat="server" Text="To"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 6em;">
                                    <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                        onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="right" >
                        <cc1:ucLabel ID="UcLabel2" runat="server" Text="Total Records:  "></cc1:ucLabel><cc1:ucLabel
                            ID="lblTotalCount" runat="server"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <cc1:ucGridView runat="server" AutoGenerateColumns="false" ID="gvVendorOverviewSummary"
                            CssClass="grid" AllowPaging="true" DataSourceID="ODSVendorOverviewSummary" PageSize="25"
                            EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-BorderStyle="None" EmptyDataRowStyle-CssClass="text_normal"
                            EmptyDataText="No Records Found" EnableViewState="false">
                            <Columns>
                                <asp:BoundField HeaderText="Vendor Number" DataField="Vendor_no">
                                    <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Vendor Name" DataField="VENDORNAME">
                                    <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Number of Active SKU's" DataField="NumberOfActiveSKU">
                                    <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Total Stock Value Total" DataField="TotalStockValue">
                                    <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Value of Receipted in Reporting Period" DataField="ValueReceiptedInReportingPeriod">
                                    <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Value of stock Sold in Reporting Period" DataField="ValueOfStockSoldInReportingPeriod">
                                    <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Instances of SKUs on Back Order" DataField="InstancesOnBackOrder">
                                    <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Collective Number of Days on Back Order" DataField="CollectiveNumberofDaysonBackOrder">
                                    <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                        </cc1:ucGridView>
                        <asp:ObjectDataSource ID="ODSVendorOverviewSummary" runat="server" TypeName="DataAccessLayer.ModuleDAL.StockOverview.Report.StockOverviewReportDAL"
                            SelectMethod="getStockOverviewReportDAL2" EnablePaging="true" SelectCountMethod="getStockOverviewCount"
                            StartRowIndexParameterName="StartRow" MaximumRowsParameterName="PageSize">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="txtFromDate" DefaultValue=" " Type="string" Name="sFromDate" />
                                <asp:ControlParameter ControlID="txtToDate" DefaultValue=" " Type="string" Name="sToDate" />
                                
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="right">
                        <i>You are viewing page
                            <%=gvVendorOverviewSummary.PageIndex + 1%>
                            of
                            <%=gvVendorOverviewSummary.PageCount%>
                        </i>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

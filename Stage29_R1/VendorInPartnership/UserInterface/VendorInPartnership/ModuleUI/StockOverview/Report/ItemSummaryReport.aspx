﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ItemSummaryReport.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
 Inherits="ModuleUI_StockOverview_Report_ItemSummaryReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVikingSku.ascx" TagName="ucVikingSku"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <script type="text/javascript" language="javascript">
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

        function ValidatePage() {

            //debugger;
            var SiteCount = $("#ctl00_ContentPlaceHolder1_msSite_lstRight option").length;
            var StockPlannerCount = $("#ctl00_ContentPlaceHolder1_msStockPlanner_ucLBRight option").length;
            var VendorCount = $("#ctl00_ContentPlaceHolder1_msVendor_lstRight option").length;
            var VikingSkuCount = $("#ctl00_ContentPlaceHolder1_UcVikingSku_lstRight option").length;
            var ODSkuCount = $("#ctl00_ContentPlaceHolder1_msSKU_lstRight option").length;


            if (StockPlannerCount == 0) {
                if (VendorCount == 0) {
                    if (VikingSkuCount == 0) {
                        if (ODSkuCount == 0) {
                            if (SiteCount == 0) {
                                alert('<%=Pleaseaddsearchcriteria%>');
                                return false;
                            }
                            else if (SiteCount > 1) {
                                alert('<%=Maximumonesiteisallowed%>');
                                return false;
                            }
                        }
                    }
                }
            }
        }




        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>

     <h2>
        <cc1:ucLabel ID="lblItemSummaryReport" runat="server" Text="Vendor Overview"></cc1:ucLabel>
    </h2>

    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcDataPanel" runat="server">            
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                  
                      <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendorCollon" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr> 
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                                  
                    <tr>
                        <td align="left" style="font-weight: bold;">
                            <cc1:ucLabel ID="lblODSKUCode" runat="server" Text="OD SKU Code"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" align="center">
                            <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSKU runat="server" ID="msSKU" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblODCatCode" runat="server" >
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel12" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucVikingSku ID="UcVikingSku" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                        </td>
                        <td align="left">
                            <table width="30%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="font-weight: bold; width: 6em;">
                                        <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                            onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                                    </td>
                                    <td style="font-weight: bold; width: 4em; text-align: center">
                                        <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 6em;">
                                        <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                            onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>                   
                    
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnGenerateReport" OnClientClick="return ValidatePage();" runat="server" Text="Generate Report" CssClass="button"
                                    OnClick="btnGenerateReport_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcReportPanel" runat="server" Visible="false">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="800px" Width="950px"
                                DocumentMapCollapsed="True" Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)"
                                WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
                            </rsweb:ReportViewer>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                                OnClick="btnBack_Click" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>


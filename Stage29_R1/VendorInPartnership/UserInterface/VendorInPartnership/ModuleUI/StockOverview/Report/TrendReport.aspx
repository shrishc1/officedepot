﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="TrendReport.aspx.cs" Inherits="ModuleUI_StockOverview_Report_VendorOverviewReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectItemClassification.ascx" TagName="MultiSelectItemClassification"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAP.ascx" TagName="MultiSelectAP"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }
        function CheckSkuMarkedAs() {
            var Discontinued = document.getElementById('<%=chkDiscontinued.ClientID %>');
            var Pending = document.getElementById('<%=chkPendingDiscontinued.ClientID %>');
            var chkUnderReview = document.getElementById('<%=chkUnderReview.ClientID %>');
            var Active = document.getElementById('<%=chkActive.ClientID %>');
            if (Discontinued.checked || Pending.checked || Active.checked || chkUnderReview.checked) {
                return true;
            }
            else {
                alert("Please select SKU Marked As");
                return false;
            }
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <asp:HiddenField ID="hdnSKUMarkedAs" runat="server" />
    <h2>
        <cc1:ucLabel ID="lblTrendReport" runat="server" Text="Trend Report"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcDataPanel" runat="server">
             <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="90%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectSite runat="server" ID="msSite" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="lblVendorCollon" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblItemClassification" runat="server" Text="Item Classification"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectItemClassification ID="msItemClassification" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblODSKUCode" runat="server" Text="OD SKU Code"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%" align="center">
                                        <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:MultiSelectSKU runat="server" ID="msSKU" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%">
                                        <cc1:ucLabel ID="lblSKUMarkedAs" runat="server" Text="SKU Marked As"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 89%">
                                        <table width="60%" cellspacing="3" cellpadding="0" align="left">
                                            <tr>
                                                <td style="width: 30%;" class="nobold">
                                                    <cc1:ucCheckbox ID="chkDiscontinued" runat="server" Text="Discontinued" Checked="true" />
                                                </td>
                                                <td style="width: 30%;" class="nobold">
                                                    <cc1:ucCheckbox ID="chkPendingDiscontinued" runat="server" Text="Pending Discontinued"
                                                        Checked="true" />
                                                </td>
                                                <td style="width: 25%;" class="nobold">
                                                    <cc1:ucCheckbox ID="chkUnderReview" runat="server" Text="Under Review"
                                                        Checked="true" />
                                                </td>
                                                <td style="width: 30%;" class="nobold">
                                                    <cc1:ucCheckbox ID="chkActive" runat="server" Text="Active" Checked="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table width="100%">
                                            <tr>
                                                <td style="font-weight: bold; width: 10%">
                                                    <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                                                </td>
                                                <td style="font-weight: bold; width: 1%">
                                                    <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td align="left">
                                                    <table width="25%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="font-weight: bold; width: 6em;">
                                                                <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                                    onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                                                            </td>
                                                            <td style="font-weight: bold; width: 4em; text-align: center">
                                                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                                            </td>
                                                            <td style="font-weight: bold; width: 6em;">
                                                                <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                                                    onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <%--<cc1:ucLabel runat="server" ID="UcLabel3" Text="Please select if you want to view report by Carrier or By Vendor" />--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <div class="button-row">
                                <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button" OnClientClick="return CheckSkuMarkedAs();"
                                    OnClick="btnGenerateReport_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcReportPanel" runat="server" Visible="false">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="800px" Width="950px"
                                DocumentMapCollapsed="True">
                            </rsweb:ReportViewer>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                                OnClick="btnBack_Click" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using BusinessEntities.ModuleBE.StockOverview.Report;
using BusinessLogicLayer.ModuleBAL.StockOverview.Report;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using Utilities; using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;

public partial class ModuleUI_StockOverview_Report_VendorOverviewReport : CommonPage {
    //DataSet StockOverview = new DataSet();
    //StockOverviewReportBAL oStockOverviewReportBAL = new StockOverviewReportBAL();
    //StockOverviewReportBE oStockOverviewReportBE = new StockOverviewReportBE();
    int? ReportRequest = 0;
    ReportRequestBE oReportRequestBE = new ReportRequestBE();
    ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();

    #region ReportVariables

    Warning[] warnings;
    string[] streamIds;
    string mimeType = string.Empty;
    string encoding = string.Empty;
    string extension = string.Empty;
    string reportFileName = string.Empty;

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
        }
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e) {
        try {

            //UcDataPanel.Visible = false;
            //UcReportPanel.Visible = true;

            oReportRequestBE.Action = "InsertReportRequest";
            oReportRequestBE.ModuleName = "StockOverview";
            oReportRequestBE.ReportName = "Vendor Overview";
            if (rdoSummary.Checked == true)  // For Summary
            {
                oReportRequestBE.ReportAction = "VendorOverviewReportSummary_1";
                oReportRequestBE.ReportNamePath = "VendorOverviewReportSummary.rdlc";
                oReportRequestBE.ReportDatatableName = "dtVendorOverviewReportSummary_1";
                oReportRequestBE.ReportType = "Summary";
            }
            if (rdoDetail.Checked == true) // For Detail
            {
                oReportRequestBE.ReportAction = "VendorOverviewReportDetailed_1";
                oReportRequestBE.ReportNamePath = "VendorOverviewReportDetailed.rdlc";
                oReportRequestBE.ReportDatatableName = "dtVendorOverviewRptDetailed";
                oReportRequestBE.ReportType = "Detail";
            }
            oReportRequestBE.CountryIDs = msCountry.SelectedCountryIDs;
            oReportRequestBE.CountryName = msCountry.SelectedCountryName;
            oReportRequestBE.SiteIDs = msSite.SelectedSiteIDs;
            oReportRequestBE.SiteName = msSite.SelectedSiteName;
            oReportRequestBE.VendorIDs = msVendor.SelectedVendorIDs;
            oReportRequestBE.VendorName = msVendor.SelectedVendorName;
            oReportRequestBE.ItemClassification = msItemClassification.selectedItemClassification;
            oReportRequestBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);
            oReportRequestBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Utilities.Common.TextToDateFormat(hdnJSToDt.Value);
            oReportRequestBE.OfficeDepotSKU = msSKU.SelectedSKUName;
            oReportRequestBE.ActiveSKUs =1;

            oReportRequestBE.RequestStatus = "Pending";
            oReportRequestBE.UserID = Convert.ToInt32(Session["UserID"]);
            oReportRequestBE.RequestTime = DateTime.Now;

            ReportRequest = oReportRequestBAL.addReportRequestBAL(oReportRequestBE);
            if (ReportRequest > 0) {
                string ReportRequestSubmitted = WebCommon.getGlobalResourceValue("ReportRequestSubmitted");
                ReportRequestSubmitted = ReportRequestSubmitted + " : " + Convert.ToString(ReportRequest);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReportRequestSubmitted + "')", true);
                return;
            }




           // oStockOverviewReportBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
            //oStockOverviewReportBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
           // oStockOverviewReportBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
           // oStockOverviewReportBE.ItemClassification = msItemClassification.selectedItemClassification;
           // oStockOverviewReportBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);
           // oStockOverviewReportBE.DateTo = string.IsNullOrEmpty(txtToDate.Text) ? (DateTime?)null : Utilities.Common.TextToDateFormat(hdnJSToDt.Value);
          //  oStockOverviewReportBE.OfficeDepotSKU = msSKU.SelectedSKUName;
          
            //if (rdoSummary.Checked == true) {
            //    oStockOverviewReportBE.Action = "VendorOverviewReportSummary_1";
            //   Session["ReportPath"] = "\\ModuleUI\\StockOverview\\Report\\RDLC\\VendorOverviewReportSummary.rdlc";
            //   Session["DateTableName"] ="dtVendorOverviewReportSummary_1";
            //   reportFileName = "VendorOverviewReportSummary";
            //}

            //// For Detail
            //if (rdoDetail.Checked == true) {
            //    oStockOverviewReportBE.Action = "VendorOverviewReportDetailed_1";              
            //    Session["ReportPath"] ="\\ModuleUI\\StockOverview\\Report\\RDLC\\VendorOverviewReportDetailed.rdlc";
            //    Session["DateTableName"]="dtVendorOverviewRptDetailed";
            //    reportFileName = "VendorOverviewReportDetailed";               
            //}
             
           // StockOverview = oStockOverviewReportBAL.getDiscrepancyReportBAL(oStockOverviewReportBE);
           // oStockOverviewReportBAL = null;

            //ReportParameter[] reportParameter = new ReportParameter[8];
            //reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
            //reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
            //reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
            //reportParameter[3] = new ReportParameter("DateFrom", hdnJSFromDt.Value);
            //reportParameter[4] = new ReportParameter("DateTo", hdnJSToDt.Value);
            //reportParameter[5] = new ReportParameter("ItemClassification", msItemClassification.selectedItemClassification);
            //reportParameter[6] = new ReportParameter("OfficeDepotSKU", msSKU.SelectedSKUName);
            //reportParameter[7] = new ReportParameter("SKUMarkedAs", "Active");

            // ReportDataSource rdsOTIFReport = new ReportDataSource(Session["DateTableName"].ToString(), StockOverview.Tables[0]);
            // ReportViewer1.LocalReport.ReportPath = Server.MapPath("~") + Session["ReportPath"].ToString();
        
            // ReportViewer1.LocalReport.DataSources.Clear();
            // ReportViewer1.LocalReport.DataSources.Add(rdsOTIFReport);

            // ReportViewer1.LocalReport.SetParameters(reportParameter);
            // ReportViewer1.Visible = false;

            //byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            //Response.Buffer = true;
            //Response.Clear();
            //Response.ContentType = mimeType;
            //Response.AddHeader("content-disposition", "attachment; filename = " + reportFileName + "." + extension);
            //Response.BinaryWrite(bytes);
            //Response.Flush(); 
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("VendorOverview.aspx");
    }

}
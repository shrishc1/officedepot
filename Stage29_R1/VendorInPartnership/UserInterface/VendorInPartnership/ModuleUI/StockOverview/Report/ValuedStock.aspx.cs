﻿using System;
using System.Data;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using Microsoft.Reporting.WebForms;
using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;

public partial class ModuleUI_StockOverview_Report_VendorOverviewReport : CommonPage
{
    int? ReportRequest = 0;
    ReportRequestBE oReportRequestBE = new ReportRequestBE();
    ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();

    #region ReportVariables

    Warning[] warnings;
    string[] streamIds;
    string mimeType = string.Empty;
    string encoding = string.Empty;
    string extension = string.Empty;
    string reportFileName = string.Empty;

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DayOfWeek day = DateTime.Now.DayOfWeek;
            int days = day - DayOfWeek.Monday;

            txtFromDate.Text = DateTime.Now.AddDays(-days).ToString("dd/MM/yyyy");           
            hdnJSFromDt.Value = txtFromDate.Text;
           
        }
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        try
        {
            // To check whether curreny conversion is defined or not
            MAS_CurrencyConversionBE oMAS_RegionBE = new MAS_CurrencyConversionBE();
            MAS_CurrencyConversionBAL oMAS_RegionBAL = new MAS_CurrencyConversionBAL();
            List<MAS_CurrencyConversionBE> lstCurrencyConversion = new List<MAS_CurrencyConversionBE>();
            oMAS_RegionBE.Action = "CheckRateApplicableDate";
            oMAS_RegionBE.FromDate = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);
            lstCurrencyConversion = oMAS_RegionBAL.GetCurrenyConversionBAL(oMAS_RegionBE);
            oMAS_RegionBAL = null;
            if (lstCurrencyConversion.Count == 0) {
                string CurrencyConversionNotExist = WebCommon.getGlobalResourceValue("CurrencyConversionNotExist");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CurrencyConversionNotExist + "')", true);
                return;
            }


            oReportRequestBE.Action = "InsertReportRequest";
            oReportRequestBE.ModuleName = "StockOverview";
            oReportRequestBE.ReportName = "Valued Stock";


            oReportRequestBE.ReportAction = "ValuatedStockWeeklyReport";
            oReportRequestBE.ReportNamePath = "ValuatedStockWeekly.rdlc";
            oReportRequestBE.ReportDatatableName = "dtValuatedStockWeeklyReport";
            oReportRequestBE.ReportType = "Valued Stock";
           
            oReportRequestBE.CountryIDs = msCountry.SelectedCountryIDs;
            oReportRequestBE.CountryName = msCountry.SelectedCountryName;
            oReportRequestBE.SiteIDs = msSite.SelectedSiteIDs;
            oReportRequestBE.SiteName = msSite.SelectedSiteName;
            oReportRequestBE.VendorIDs = msVendor.SelectedVendorIDs;
            oReportRequestBE.VendorName = msVendor.SelectedVendorName;
            oReportRequestBE.ItemClassification = msItemClassification.selectedItemClassification;
            oReportRequestBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);
            oReportRequestBE.DateTo = (DateTime?)null;
            oReportRequestBE.OfficeDepotSKU = msSKU.SelectedSKUName;
            oReportRequestBE.PendingSKUs = Convert.ToInt32(chkPendingDiscontinued.Checked);
            oReportRequestBE.DiscSKUs = Convert.ToInt32(chkDiscontinued.Checked);
            oReportRequestBE.ActiveSKUs = Convert.ToInt32(chkActive.Checked);
            oReportRequestBE.UnderReviewSKUs = Convert.ToInt32(chkUnderReview.Checked);

            oReportRequestBE.RequestStatus = "Pending";
            oReportRequestBE.UserID = Convert.ToInt32(Session["UserID"]);
            oReportRequestBE.RequestTime = DateTime.Now;

            ReportRequest = oReportRequestBAL.addReportRequestBAL(oReportRequestBE);
            if (ReportRequest > 0) {
                string ReportRequestSubmitted = WebCommon.getGlobalResourceValue("ReportRequestSubmitted");
                ReportRequestSubmitted = ReportRequestSubmitted + " : " + Convert.ToString(ReportRequest);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReportRequestSubmitted + "')", true);
                return;
            }

            //UcDataPanel.Visible = false;
            //UcReportPanel.Visible = true;

            //oStockOverviewReportBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
            //oStockOverviewReportBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
            //oStockOverviewReportBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
            //oStockOverviewReportBE.ItemClassification = msItemClassification.selectedItemClassification;
            //oStockOverviewReportBE.DateFrom = string.IsNullOrEmpty(txtFromDate.Text) ? (DateTime?)null : Utilities.Common.TextToDateFormat(hdnJSFromDt.Value);
            //oStockOverviewReportBE.DateTo =  (DateTime?)null ;
            //oStockOverviewReportBE.OfficeDepotSKU = msSKU.SelectedSKUName;
            //oStockOverviewReportBE.PendingSKUs = Convert.ToInt16(chkPendingDiscontinued.Checked);
            //oStockOverviewReportBE.DiscSKUs = Convert.ToInt16(chkDiscontinued.Checked);
            //oStockOverviewReportBE.ActiveSKUs = Convert.ToInt16(chkActive.Checked);

            
          

            //oStockOverviewReportBE.Action = "ValuatedStockWeeklyReport";
           
            //StockOverview = oStockOverviewReportBAL.getDiscrepancyReportBAL(oStockOverviewReportBE);
            //oStockOverviewReportBAL = null;

            //ReportDataSource rdsStockOutOverview = new ReportDataSource("dtValuatedStockWeeklyReport", StockOverview.Tables[0]);
            //ReportViewer1.LocalReport.ReportPath = Server.MapPath("~") + "\\ModuleUI\\StockOverview\\Report\\RDLC\\ValuatedStockWeekly.rdlc";
            //ReportViewer1.LocalReport.DataSources.Clear();
            //ReportViewer1.LocalReport.DataSources.Add(rdsStockOutOverview);
            //reportFileName = "ValuatedStockWeeklyReport";
            
            //ReportParameter[] reportParameter = new ReportParameter[8];
            //reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
            //reportParameter[1] = new ReportParameter("Site", msSite.SelectedSiteName);
            //reportParameter[2] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
            //reportParameter[3] = new ReportParameter("DateFrom", hdnJSFromDt.Value);
            //reportParameter[4] = new ReportParameter("DateTo", string.Empty);
            //reportParameter[5] = new ReportParameter("ItemClassification", msItemClassification.selectedItemClassification);
            //reportParameter[6] = new ReportParameter("OfficeDepotSKU", msSKU.SelectedSKUName);
            //String SKUMarkedAs = string.Empty;
            //if (chkDiscontinued.Checked)
            //    SKUMarkedAs = " Discontinued";
            //if (chkPendingDiscontinued.Checked)
            //    SKUMarkedAs = SKUMarkedAs + " Pending Discontinued ";
            //if (chkActive.Checked)
            //    SKUMarkedAs = SKUMarkedAs + " Active";

            //reportParameter[7] = new ReportParameter("SKUMarkedAs", SKUMarkedAs);
            //ReportViewer1.LocalReport.SetParameters(reportParameter);
            //ReportViewer1.Visible = true;


            //byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            //Response.Buffer = true;
            //Response.Clear();
            //Response.ContentType = mimeType;
            //Response.AddHeader("content-disposition", "attachment; filename = " + reportFileName + "." + extension);
            //Response.BinaryWrite(bytes);
            //Response.Flush(); 
        

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("ValuedStock.aspx");
    }
   
}
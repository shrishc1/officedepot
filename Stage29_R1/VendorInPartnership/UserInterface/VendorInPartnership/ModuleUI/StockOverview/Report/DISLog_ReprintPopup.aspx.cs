﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System.Data;
using Utilities; using WebUtilities;


public partial class DISLog_ReprintPopup : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetQueryStringValue("DiscrepancyLogID") != null && GetQueryStringValue("DiscrepancyLogID").IsNumeric() && GetQueryStringValue("option") != null && !string.IsNullOrEmpty(GetQueryStringValue("option")))
        {
            rePrint(GetQueryStringValue("DiscrepancyLogID"), GetQueryStringValue("option"));
        }  
        else
        {
            showErrorMessage("InvalidDiscrepancyLogID");
        }        
    }    
    public void showErrorMessage(string message)
    {
        string errorMessage = WebCommon.getGlobalResourceValue(message);
        if (string.IsNullOrEmpty(errorMessage))
            errorMessage = message;
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + errorMessage + "')", true);
    }

    private void rePrint(string DiscrepancyLogID,string sOption)
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        oDiscrepancyBE.Action = "GetDiscrepancyReprint";
        oDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(DiscrepancyLogID);
        DataSet ds = oDiscrepancyBAL.GetDiscrepancyLogRePrintBAL(oDiscrepancyBE);
        oDiscrepancyBAL = null;
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            divDiscrepancyReprint.InnerHtml = Convert.ToString(ds.Tables[0].Rows[0]["letterbody"]);
        else
            showErrorMessage("NoDataFound");
    }
}
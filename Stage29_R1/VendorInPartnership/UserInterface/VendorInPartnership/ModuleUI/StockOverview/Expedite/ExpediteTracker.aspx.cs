﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using System.Drawing;
using Utilities;
using WebUtilities;
using System.Data;
using BaseControlLibrary;

public partial class ModuleUI_StockOverview_Expedite_Expedite_Tracker_ExpediteTracker : CommonPage
{
    bool IsExportClicked = false;
    protected string Month1 = string.Empty;
    protected string Month2 = string.Empty;
    protected string Month3 = string.Empty;
    protected string Month4 = string.Empty;
    protected string Month5 = string.Empty;
    protected string Month6 = string.Empty;
    protected string Month7 = string.Empty;
    protected string Month8 = string.Empty;
    protected string Month9 = string.Empty;
    protected string Month10 = string.Empty;
    protected string Month11 = string.Empty;
    protected string Month12 = string.Empty;
    protected string Day1 = string.Empty;
    protected string Day2 = string.Empty;
    protected string Day3 = string.Empty;
    protected string Day4 = string.Empty;
    protected string Day5 = string.Empty;
    protected string Day6 = string.Empty;
    protected string Day7 = string.Empty;
    protected string DayToOnload = string.Empty;
    protected string DayFromOnLoad = string.Empty;
    protected string WeekendCheck = WebCommon.getGlobalResourceValue("WeekendCheck");  
    DataTable dtExport = new DataTable();
    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        lblRecordNotFound.Visible = false;
        DayFromOnLoad=DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
        DayToOnload = DateTime.Now.ToString("dd/MM/yyyy");
        int YearFrom = DateTime.Now.Year;
        int YearTo = DateTime.Now.Year - 10;
        while (YearFrom > YearTo)
        {
            if (YearFrom >= 2013)
                ddlYear.Items.Add(YearFrom.ToString());
            YearFrom--;
        }
        if (!IsPostBack)
        {
            //if (GetQueryStringValue("SiteID") != "0" || GetQueryStringValue("CountryID") != "0")
            //{
            //    BindGridview();
            //}
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            //int YearFrom = DateTime.Now.Year;
            //int YearTo = DateTime.Now.Year - 10;
            //DayToOnload = txtToDate.Text;
            //DayFromOnLoad = txtFromDate.Text;
            //while (YearFrom > YearTo)
            //{
            //    if (YearFrom >= 2013)
            //        ddlYear.Items.Add(YearFrom.ToString());
            //    YearFrom--;
            //}
            if (GetQueryStringValue("SiteID") == null && GetQueryStringValue("CountryID") == null && GetQueryStringValue("StockPlannerID") == null)
            {
                pager1.PageSize = 200;
                pager1.GenerateGoToSection = false;
                pager1.GeneratePagerInfoSection = false;
                pager1.Visible = false;
                pager2.PageSize = 200;
                pager2.GenerateGoToSection = false;
                pager2.GeneratePagerInfoSection = false;
                pager2.Visible = false;
             
            }
           
            if (GetQueryStringValue("SiteID")!=null && GetQueryStringValue("CountryID")!=null && GetQueryStringValue("StockPlannerID")!=null)
            {
                pager1.PageSize = 200;
                pager1.GenerateGoToSection = false;
                pager1.GeneratePagerInfoSection = false;
                pager1.Visible = false;

                pager2.PageSize = 200;
                pager2.GenerateGoToSection = false;
                pager2.GeneratePagerInfoSection = false;
                pager2.Visible = false;
                BindGridviewSession();              
            }
        }
        else
        {           
            if (rdoDailyTracker.Checked)
            {
                txtFromDate.Text = hdnJSFromDt.Value;
                txtToDate.Text = hdnJSToDt.Value;
            }
            //else
            //{
            //    txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            //    txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //    hdnJSFromDt.Value = txtFromDate.Text;
            //    hdnJSToDt.Value = txtToDate.Text;                
            //}
        }
        //btnExportToExcel_1.GridViewControl = UcGridViewExport;
        //btnExportToExcel_1.FileName = "Expedite Tracker"; 
    }
    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
      BindGridview();
    }  
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }
    protected void hpStockPlanner_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btn = (LinkButton)sender;       
            pager1.ItemCount =Convert.ToDouble(ViewState["RecordCount"]);
            pager1.Visible = true;
            var script = "window.open('" + EncryptQuery("ExpediteTracker.aspx?DateTo=" + Common.GetYYYY_MM_DD(hdnJSToDt.Value) + "&DateFrom=" + Common.GetYYYY_MM_DD(hdnJSFromDt.Value) + "&SiteID=" + "0" + "&CountryID=" + "0" + "&StockPlannerID=" + btn.CommandArgument + "") + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", script, true);

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex); ;
        }
    }
    protected void hpSite_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btn = (LinkButton)sender;
            //oSessionStructs = new SessionStructs();
            string[] CommandArguments = btn.CommandArgument.Split(',');
            //oSessionStructs.StockPlannerName = null;
            //oSessionStructs.SiteID = Convert.ToInt32(CommandArguments[0]);
            //oSessionStructs.CountryID = Convert.ToInt32(CommandArguments[1]);
            //Cache["ExpediteTracker"] = oSessionStructs;
            pager1.ItemCount = Convert.ToDouble(ViewState["RecordCount"]);
            pager1.Visible = true;
            var script = "window.open('" + EncryptQuery("ExpediteTracker.aspx?DateTo=" + Common.GetYYYY_MM_DD(hdnJSToDt.Value) + "&DateFrom=" + Common.GetYYYY_MM_DD(hdnJSFromDt.Value) + "&SiteID=" + Convert.ToInt32(CommandArguments[0]) + "&CountryID=" + Convert.ToInt32(CommandArguments[1]) + "&StockPlannerID=" + "0" + "") + "');";            
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", script, true);

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex); ;
        }
    }
    public void BindGridviewSession(int Page = 1)
    {
        StockPlannerGroupingsBE oStockPlannerGroupingsBE = new StockPlannerGroupingsBE();
        StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();
       // oSessionStructs = (SessionStructs)Cache["ExpediteTracker"];
         if (GetQueryStringValue("SiteID")!=null && GetQueryStringValue("CountryID")!=null && GetQueryStringValue("StockPlannerID")!=null)
         {
            if (GetQueryStringValue("StockPlannerID") != "0" && GetQueryStringValue("SiteID") == "0" || GetQueryStringValue("CountryID") == "0")
            {
                oStockPlannerGroupingsBE.Action = "PlannerBySite";
                oStockPlannerGroupingsBE.StockPlanner = GetQueryStringValue("StockPlannerID");
                oStockPlannerGroupingsBE.DateTo = GetQueryStringValue("DateTo");
                oStockPlannerGroupingsBE.Datefrom = GetQueryStringValue("DateFrom");
            }
            else if (GetQueryStringValue("StockPlannerID") == "0"  || GetQueryStringValue("CountryID") != "0")
            {
                oStockPlannerGroupingsBE.Action = "VendorWiseByCountry";
                oStockPlannerGroupingsBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
                oStockPlannerGroupingsBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
                oStockPlannerGroupingsBE.DateTo = GetQueryStringValue("DateTo");
                oStockPlannerGroupingsBE.Datefrom = GetQueryStringValue("DateFrom");
            }
        }
       
        oStockPlannerGroupingsBE.VendorIDs = msVendor.SelectedVendorIDs;
        oStockPlannerGroupingsBE.StockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oStockPlannerGroupingsBE.ItemClassification = msItemClassification.selectedItemClassification;
        oStockPlannerGroupingsBE.StockPlannerGroupingIDs = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
        oStockPlannerGroupingsBE.PageCount = Page;
        List<StockPlannerGroupingsBE> lstoStockPlannerGroupingsBE = new List<StockPlannerGroupingsBE>();
        int RecordCount = 0;
        oStockPlannerGroupingsBAL.ExpediteTrackerReportDetailsVendorBAL(oStockPlannerGroupingsBE, out lstoStockPlannerGroupingsBE, out RecordCount);
        if (lstoStockPlannerGroupingsBE.Count > 0)
        {
            pager2.ItemCount = RecordCount;
            pager2.Visible = true;
            UcGridView1.DataSource = lstoStockPlannerGroupingsBE;
            UcGridView1.DataBind();
            UcGridView1.Visible = true;
           // ViewState["lstSites"] = lstoStockPlannerGroupingsBE;
            UcDataPanel.Visible = false;
            UcGridView1.Visible = true;
            divUcGridView1.Visible = true;
            btnBack.Visible = true;
            btnExportToExcel_1.Visible = true;
            btnExportToExcel.Visible = false;
        }
        else
        {
            UcGridView1.DataSource = null;
            UcGridView1.DataBind();
           // ViewState["lstSites"] = null;

        }
    }
    public void BindGridview(int Page = 1)
    {
        StockPlannerGroupingsBE oStockPlannerGroupingsBE = new StockPlannerGroupingsBE();
        StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();
       
          if (rdoPlannerReport.Checked == true)
            {
                oStockPlannerGroupingsBE.Action = "Planner";
                oStockPlannerGroupingsBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
                oStockPlannerGroupingsBE.Datefrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
            }
            else if (rdoPlannerCountry.Checked == true)
            {
                oStockPlannerGroupingsBE.Action = "PlannerBySite";
                oStockPlannerGroupingsBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
                oStockPlannerGroupingsBE.Datefrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
            }
            else if (rdoDetailed.Checked == true)
            { Response.Redirect("ExpediteTrackingReport.aspx"); }
            else if (rdoRegionReport.Checked == true)
            {
                oStockPlannerGroupingsBE.Action = "RegionWise";
                oStockPlannerGroupingsBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
                oStockPlannerGroupingsBE.Datefrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);

            }
            else if (rdoVendorReport.Checked == true)
            {
                oStockPlannerGroupingsBE.Action = "VendorWiseByCountry";
                oStockPlannerGroupingsBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
                oStockPlannerGroupingsBE.Datefrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
            }
            else if (rdoDailyTracker.Checked == true)
            { oStockPlannerGroupingsBE.Action = "DayWise";
            oStockPlannerGroupingsBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
            oStockPlannerGroupingsBE.Datefrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
            }
            else if (rdoMonthlyTracker.Checked == true)
            { oStockPlannerGroupingsBE.Action = "MonthWise";
            oStockPlannerGroupingsBE.Datefrom = ddlYear.SelectedValue+"-"+ddlMonth.SelectedValue+"-1";
            }
       
        oStockPlannerGroupingsBE.SiteIDs = msSite.SelectedSiteIDs;
        oStockPlannerGroupingsBE.VendorIDs = msVendor.SelectedVendorIDs;
        oStockPlannerGroupingsBE.StockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oStockPlannerGroupingsBE.ItemClassification = msItemClassification.selectedItemClassification;
        oStockPlannerGroupingsBE.StockPlannerGroupingIDs = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
        oStockPlannerGroupingsBE.PageCount = Page;
        List<StockPlannerGroupingsBE> lstoStockPlannerGroupingsBE = new List<StockPlannerGroupingsBE>();
        int RecordCount = 0;
        if (rdoPlannerReport.Checked == true || rdoPlannerCountry.Checked == true || rdoRegionReport.Checked == true || rdoVendorReport.Checked == true || rdoDetailed.Checked == true)
        {
            oStockPlannerGroupingsBAL.ExpediteTrackerReportDetailsVendorBAL(oStockPlannerGroupingsBE, out lstoStockPlannerGroupingsBE, out RecordCount);
        }
        else
        {
            if (rdoMonthlyTracker.Checked == true)
            {
                oStockPlannerGroupingsBAL.ExpediteTrackerMonthlyReportDetailsBAL(oStockPlannerGroupingsBE, out lstoStockPlannerGroupingsBE, out RecordCount,out dtExport);
            }
            else if (rdoDailyTracker.Checked == true)
            {
                oStockPlannerGroupingsBAL.ExpediteTrackerDailyReportDetailsBAL(oStockPlannerGroupingsBE, out lstoStockPlannerGroupingsBE, out RecordCount, out dtExport);
            }
        }
        if (lstoStockPlannerGroupingsBE.Count > 0)
        {
            ViewState["RecordCount"] = RecordCount;
            pager1.ItemCount = RecordCount;
            pager1.Visible = true;
            if (rdoPlannerReport.Checked == true || rdoPlannerCountry.Checked == true || rdoRegionReport.Checked == true || rdoVendorReport.Checked == true || rdoDetailed.Checked == true)
            {
                UcGridView1.DataSource = lstoStockPlannerGroupingsBE;
                UcGridView1.DataBind();
                UcGridViewExport.DataSource = lstoStockPlannerGroupingsBE;
                UcGridViewExport.DataBind();
                UcGridView1.Visible = true;                
                UcDataPanel.Visible = false;
                divUcGridView1.Visible = true;
                btnBack.Visible = true;
                btnExportToExcel_1.Visible = false;
                btnExportToExcel.Visible = true;
                gdMonthlyTracker.Visible = false;
                divMonthlyTracker.Visible = false;
                divDailyTracker.Visible = false;
                grdDailyTracker.Visible = false;
                //if(rdoRegionReport.Checked)
                //{
                //    UcGridViewExport.DataSource = lstoStockPlannerGroupingsBE;
                //    UcGridViewExport.DataBind();
                //}
            }

            else
            {
                if (rdoMonthlyTracker.Checked == true)
                {
                    ViewState["lstoStockPlannerGroupingsBE"] = lstoStockPlannerGroupingsBE;
                    AssignMonth();
                    gdMonthlyTracker.DataSource = lstoStockPlannerGroupingsBE;            
                    gdMonthlyTracker.DataBind();
                    UcGridView1.Visible = false;                    
                    UcDataPanel.Visible = false;
                    divUcGridView1.Visible = false;
                    btnBack.Visible = true;
                    btnExportToExcel_1.Visible = false;
                    btnExportToExcel.Visible = true;
                    gdMonthlyTracker.Visible = true;
                    divMonthlyTracker.Visible = true;
                    grdDailyTracker.Visible = false;
                    divDailyTracker.Visible = false;
                }              
                else if (rdoDailyTracker.Checked == true)
                {
                    ViewState["lstoStockPlannerGroupingsBE"] = lstoStockPlannerGroupingsBE;
                    AssignDay();
                    grdDailyTracker.DataSource = lstoStockPlannerGroupingsBE;
                    grdDailyTracker.DataBind();
                    UcGridView1.Visible = false;
                    UcDataPanel.Visible = false;
                    divUcGridView1.Visible = false;
                    btnBack.Visible = true;
                    btnExportToExcel_1.Visible = false;
                    btnExportToExcel.Visible = true;
                    gdMonthlyTracker.Visible = false;
                    divMonthlyTracker.Visible = false;
                    grdDailyTracker.Visible = true;
                    divDailyTracker.Visible = true;
                }
            }  
        }
        else
        {
            lblRecordNotFound.Visible = true;
            UcGridView1.DataSource = null;
            UcGridView1.DataBind();
            grdDailyTracker.DataSource = null;
            UcGridView1.DataBind();
            gdMonthlyTracker.DataSource = null;
            UcGridView1.DataBind();
            btnBack.Visible = true;
            UcDataPanel.Visible = false;
            BindSession();
        }
    }

    public void BindSession()
    {
        multiSelectStockPlannerGrouping.setStockPlannerGroupingsOnPostBack();
        msStockPlanner.setStockPlannerOnPostBack();
        msSite.setSitesOnPostBack();
        msVendor.setVendorsOnPostBack();
        msItemClassification.setItemClassificationOnPostBack();
        TextBox txtSearchedStockPlanner = msStockPlanner.FindControl("txtUserName") as TextBox;
        string StockPlannerText = txtSearchedStockPlanner.Text;
        string StockPlannerID = msStockPlanner.SelectedStockPlannerIDs;
        ucListBox lstRight = msStockPlanner.FindControl("ucLBRight") as ucListBox;
        ucListBox lstLeft = msStockPlanner.FindControl("ucLBLeft") as ucListBox;
        string strDtockPlonnerId = string.Empty;
        if (lstLeft != null && lstRight != null && !string.IsNullOrEmpty(StockPlannerID))
        {
            lstLeft.Items.Clear();
            msStockPlanner.SearchStockPlannerClick(StockPlannerText);
            string[] strStockPlonnerIDs = StockPlannerID.Split(',');
            for (int index = 0; index < strStockPlonnerIDs.Length; index++)
            {
                ListItem listItem = lstLeft.Items.FindByValue(strStockPlonnerIDs[index]);
                if (listItem != null)
                {
                    strDtockPlonnerId += strStockPlonnerIDs[index] + ",";
                    //lstRight.Items.Add(listItem);
                    lstLeft.Items.Remove(listItem);
                }

            }
            HiddenField hdnSelectedStockPlonner = msStockPlanner.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedStockPlonner.Value = strDtockPlonnerId;
        }

        ////************************* StockPlannerGrouping ***************************
        string StockPlannerGroupingID = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
        ucListBox lstRightGrouping = multiSelectStockPlannerGrouping.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftGrouping = multiSelectStockPlannerGrouping.FindControl("lstLeft") as ucListBox;
        // lstRightVendor.Items.Clear();
        if (lstLeftGrouping != null && lstRightGrouping != null)
        {
            lstLeftGrouping.Items.Clear();
            multiSelectStockPlannerGrouping.BindStockPlannerGrouping();
            if (!string.IsNullOrEmpty(StockPlannerGroupingID))
            {
                string[] strStockPlannerGroupingIDs = StockPlannerGroupingID.Split(',');
                for (int index = 0; index < strStockPlannerGroupingIDs.Length; index++)
                {
                    ListItem listItem = lstLeftGrouping.Items.FindByValue(strStockPlannerGroupingIDs[index]);
                    if (listItem != null)
                    {
                        lstLeftGrouping.Items.Remove(listItem);
                    }
                }
            }
        }
        //*********** Site ***************
        string SiteId = msSite.SelectedSiteIDs;
        ucListBox lstRightSite = msSite.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftSite = msSite.FindControl("lstLeft") as ucListBox;
        string msSiteid = string.Empty;
        lstRightSite.Items.Clear();
        lstRightSite.Items.Clear();
        msSite.SelectedSiteIDs = "";
        if (lstLeftSite != null && lstRightSite != null && !string.IsNullOrEmpty(SiteId))
        {
            //lstLeftSite.Items.Clear();
            lstRightSite.Items.Clear();
            msSite.BindSite();

            string[] strSiteIDs = SiteId.Split(',');
            for (int index = 0; index < strSiteIDs.Length; index++)
            {
                ListItem listItem = lstLeftSite.Items.FindByValue(strSiteIDs[index]);
                if (listItem != null)
                {
                    msSiteid = msSiteid + strSiteIDs[index].ToString() + ",";
                    lstRightSite.Items.Add(listItem);
                    lstLeftSite.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msSite.SelectedSiteIDs))
                msSite.SelectedSiteIDs = msSiteid.Trim(',');
        }

        ////************************* Vendor ***************************
        string IncludeVendorId = msVendor.SelectedVendorIDs;
        TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;
        string txtVendorIdText = txtVendorId.Text;
        ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
        // lstRightVendor.Items.Clear();
        if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(IncludeVendorId) || !string.IsNullOrEmpty(txtVendorIdText)))
        {
            lstLeftVendor.Items.Clear();
            //lstRightVendor.Items.Clear();
            msVendor.SearchVendorClick(txtVendorIdText);
            if (!string.IsNullOrEmpty(IncludeVendorId))
            {
                string[] strIncludeVendorIDs = IncludeVendorId.Split(',');
                for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                    if (listItem != null)
                    {
                        // lstRightVendor.Items.Add(listItem);
                        lstLeftVendor.Items.Remove(listItem);
                    }
                }
            }
        }

        //************************* ItemClassification ***************************
        string classificationId = msItemClassification.selectedItemClassification;
        ucListBox lstRightClassi = msItemClassification.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftClassi = msItemClassification.FindControl("lstLeft") as ucListBox;
        lstRightClassi.Items.Clear();
        msItemClassification.BindClassification();
        if (lstLeftClassi != null && lstRightClassi != null && !string.IsNullOrEmpty(classificationId))
        {
            // lstLeftClassi.Items.Clear();
            lstRightClassi.Items.Clear();
            // msItemClassification.BindClassification();
            string[] strClassificationIDs = classificationId.Split(',');
            for (int index = 0; index < strClassificationIDs.Length; index++)
            {
                string Item = strClassificationIDs[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    lstRightClassi.Items.Add(listItem);
                    lstLeftClassi.Items.Remove(listItem);
                }
            }
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        BindSession();
        txtFromDate.Text = hdnJSFromDt.Value;
        txtToDate.Text = hdnJSToDt.Value;      
        hdnJSFromDt.Value = txtFromDate.Text;
        hdnJSToDt.Value = txtToDate.Text;
        UcDataPanel.Visible = true;
        UcGridView1.Visible = false;
        divDailyTracker.Visible = false;
        divMonthlyTracker.Visible = false;
        divUcGridView1.Visible = false;
        gdMonthlyTracker.Visible = false;
        grdDailyTracker.Visible = false;
        btnExportToExcel.Visible = false;
        btnExportToExcel_1.Visible = false;
        pager1.Visible = false;
        btnBack.Visible = false;        
        if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("CountryID") != null && GetQueryStringValue("StockPlannerID") != null)
        {           
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
        }
    }
    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGridview(currnetPageIndx);
    }
    public void pager_Commandsession(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGridviewSession(currnetPageIndx);
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<StockPlannerGroupingsBE>.SortList((List<StockPlannerGroupingsBE>)ViewState["lstSites"], e.SortExpression, e.SortDirection).ToArray();
    }
    public void BindGridviewExportExcelSession()
    {
        StockPlannerGroupingsBE oStockPlannerGroupingsBE = new StockPlannerGroupingsBE();
        StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();

        if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("CountryID") != null && GetQueryStringValue("StockPlannerID") != null)
        {
            if (GetQueryStringValue("StockPlannerID") != "0" && GetQueryStringValue("SiteID") == "0" || GetQueryStringValue("CountryID") == "0")
            {
                oStockPlannerGroupingsBE.Action = "PlannerBySite";
                oStockPlannerGroupingsBE.StockPlanner = GetQueryStringValue("StockPlannerID");
                oStockPlannerGroupingsBE.PageCount = 0;
                oStockPlannerGroupingsBE.DateTo = GetQueryStringValue("DateTo");
                oStockPlannerGroupingsBE.Datefrom = GetQueryStringValue("DateFrom");
            }
            else if (GetQueryStringValue("StockPlannerID") == "0" || GetQueryStringValue("CountryID") != "0")
            {
                oStockPlannerGroupingsBE.Action = "VendorWiseByCountry";
                oStockPlannerGroupingsBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
                oStockPlannerGroupingsBE.CountryID = Convert.ToInt32(GetQueryStringValue("CountryID"));
                oStockPlannerGroupingsBE.PageCount = 0;
                oStockPlannerGroupingsBE.DateTo = GetQueryStringValue("DateTo");
                oStockPlannerGroupingsBE.Datefrom = GetQueryStringValue("DateFrom");
            }

            //oStockPlannerGroupingsBE.SiteIDs = msSite.SelectedSiteIDs;
            //oStockPlannerGroupingsBE.VendorIDs = msVendor.SelectedVendorIDs;
            oStockPlannerGroupingsBE.StockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
            oStockPlannerGroupingsBE.ItemClassification = msItemClassification.selectedItemClassification;
            oStockPlannerGroupingsBE.StockPlannerGroupingIDs = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
            oStockPlannerGroupingsBE.PageCount = 0;
            List<StockPlannerGroupingsBE> lstoStockPlannerGroupingsBE = new List<StockPlannerGroupingsBE>();
            int RecordCount = 0;
            oStockPlannerGroupingsBAL.ExpediteTrackerReportDetailsVendorBAL(oStockPlannerGroupingsBE, out lstoStockPlannerGroupingsBE, out RecordCount);
            if (lstoStockPlannerGroupingsBE.Count > 0)
            {
                UcGridViewExport.DataSource = lstoStockPlannerGroupingsBE;
                UcGridViewExport.DataBind();
            }
            else
            {
                UcGridView1.DataSource = null;
                UcGridView1.DataBind();
            }
    
        }
    }
    public void BindGridviewExcel()
    {
        StockPlannerGroupingsBE oStockPlannerGroupingsBE = new StockPlannerGroupingsBE();
        StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();
      
            if (rdoPlannerReport.Checked == true)
            {
                oStockPlannerGroupingsBE.Action = "Planner";
                oStockPlannerGroupingsBE.PageCount = 0;
                oStockPlannerGroupingsBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
                oStockPlannerGroupingsBE.Datefrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
            }
            else if (rdoPlannerCountry.Checked == true)
            {
                oStockPlannerGroupingsBE.Action = "PlannerBySite";
                oStockPlannerGroupingsBE.PageCount = 0;
                oStockPlannerGroupingsBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
                oStockPlannerGroupingsBE.Datefrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
            }
            else if (rdoDetailed.Checked == true)
            { Response.Redirect("ExpediteTrackingReport.aspx"); }
            else if (rdoRegionReport.Checked == true)
            {
                oStockPlannerGroupingsBE.Action = "RegionWise";
                oStockPlannerGroupingsBE.PageCount = 0;
                oStockPlannerGroupingsBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
                oStockPlannerGroupingsBE.Datefrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);

            }
            else if (rdoVendorReport.Checked == true)
            {
                oStockPlannerGroupingsBE.Action = "VendorWiseByCountry";
                oStockPlannerGroupingsBE.PageCount = 0;
                oStockPlannerGroupingsBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
                oStockPlannerGroupingsBE.Datefrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
            }
            else if (rdoDailyTracker.Checked == true)
            { oStockPlannerGroupingsBE.Action = "DayWise";
            oStockPlannerGroupingsBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
            oStockPlannerGroupingsBE.Datefrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
            oStockPlannerGroupingsBE.PageCount = 0;
            }
            else if (rdoMonthlyTracker.Checked == true)
            { oStockPlannerGroupingsBE.Action = "MonthWise";
            oStockPlannerGroupingsBE.Datefrom = ddlYear.SelectedValue+"-"+ddlMonth.SelectedValue+"-1";
            oStockPlannerGroupingsBE.PageCount = 0;
            oStockPlannerGroupingsBE.DateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);
            oStockPlannerGroupingsBE.Datefrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
            }
             
        oStockPlannerGroupingsBE.SiteIDs = msSite.SelectedSiteIDs;
        oStockPlannerGroupingsBE.VendorIDs = msVendor.SelectedVendorIDs;
        oStockPlannerGroupingsBE.StockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oStockPlannerGroupingsBE.ItemClassification = msItemClassification.selectedItemClassification;
        oStockPlannerGroupingsBE.StockPlannerGroupingIDs = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
        oStockPlannerGroupingsBE.PageCount = 0;
        List<StockPlannerGroupingsBE> lstoStockPlannerGroupingsBE = new List<StockPlannerGroupingsBE>();
        int RecordCount = 0;
        if (rdoPlannerReport.Checked == true || rdoPlannerCountry.Checked == true || rdoRegionReport.Checked == true || rdoVendorReport.Checked == true || rdoDetailed.Checked == true)
        {
            oStockPlannerGroupingsBAL.ExpediteTrackerReportDetailsVendorBAL(oStockPlannerGroupingsBE, out lstoStockPlannerGroupingsBE, out RecordCount);
        }
        else
        {
            if (rdoMonthlyTracker.Checked == true)
            {
                oStockPlannerGroupingsBAL.ExpediteTrackerMonthlyReportDetailsBAL(oStockPlannerGroupingsBE, out lstoStockPlannerGroupingsBE, out RecordCount, out dtExport);
            }
            else if (rdoDailyTracker.Checked == true)
            {
                oStockPlannerGroupingsBAL.ExpediteTrackerDailyReportDetailsBAL(oStockPlannerGroupingsBE, out lstoStockPlannerGroupingsBE, out RecordCount,out dtExport);
            }
        }
        if (lstoStockPlannerGroupingsBE.Count > 0)
        {
            if (rdoPlannerReport.Checked == true || rdoPlannerCountry.Checked == true || rdoRegionReport.Checked == true || rdoVendorReport.Checked == true || rdoDetailed.Checked == true)
            {
                UcGridViewExport.DataSource = lstoStockPlannerGroupingsBE;
                UcGridViewExport.DataBind();
            }

            else
            {
                if (rdoMonthlyTracker.Checked == true)
                {
                    AssignMonth();
                    ExportToExcelMonthlyTracker(dtExport, "Monthly Expedite Tracker");
                }
                else if (rdoDailyTracker.Checked == true)
                {
                    AssignDay();
                    ExportToExcelDailyTracker(dtExport, "Daily Expedite Tracker");
                }            
            }
        }
        else
        {
            if (rdoPlannerReport.Checked == true || rdoPlannerCountry.Checked == true || rdoRegionReport.Checked == true || rdoVendorReport.Checked == true || rdoDetailed.Checked == true)
            {
                UcGridView1.DataSource = null;
                UcGridView1.DataBind();
            }
        }
    
    }
    protected void btnExportSession_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        BindGridviewExportExcelSession();
        LocalizeGridHeader(UcGridViewExport);
        if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("CountryID") != null && GetQueryStringValue("StockPlannerID") != null)
        {
          
            if (UcGridViewExport.Rows.Count > 0)
            {
                if (GetQueryStringValue("SiteID") == "0" && GetQueryStringValue("CountryID") == "0" && GetQueryStringValue("StockPlannerID") != "0")
                {
                    WebCommon.Export("Expedite Tracker by User/Region", UcGridViewExport);
                }
                else if ( GetQueryStringValue("CountryID") != "0" && GetQueryStringValue("StockPlannerID") == "0")
                {
                    WebCommon.Export("Expedite Tracker by Vendor", UcGridViewExport);
                }
            }
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        BindGridviewExcel();
        if (rdoPlannerReport.Checked == true || rdoPlannerCountry.Checked == true || rdoRegionReport.Checked == true || rdoVendorReport.Checked == true || rdoDetailed.Checked == true)
        {           
            LocalizeGridHeader(UcGridViewExport);
        }
        //if (rdoMonthlyTracker.Checked == true)
        //{
        //    LocalizeGridHeader(UcMonthlyTrackerExport);
        //}
        //if (rdoMonthlyTracker.Checked == true)
        //{
        //    LocalizeGridHeader(UcDailyTrackerReportExport);
        //}
         if (rdoPlannerReport.Checked == true || rdoPlannerCountry.Checked == true || rdoRegionReport.Checked == true || rdoVendorReport.Checked == true || rdoDetailed.Checked == true)
            {
                if (UcGridViewExport.Rows.Count > 0)
                {
                    if (rdoPlannerReport.Checked == true)
                    {
                        WebCommon.Export("Expedite Tracker by User", UcGridViewExport);
                    }
                    else if (rdoPlannerCountry.Checked == true)
                    {
                        WebCommon.Export("Expedite Tracker by User/Region", UcGridViewExport);
                    }
                    else if (rdoRegionReport.Checked == true)
                    {
                        WebCommon.Export("Expedite Tracker by Region/Site", UcGridViewExport);
                    }
                    else if (rdoVendorReport.Checked == true)
                    {
                        WebCommon.Export("Expedite Tracker by Vendor", UcGridViewExport);
                    }
                    else if (rdoVendorReport.Checked == true)
                    {
                        WebCommon.Export("Expedite Tracker by Vendor", UcGridViewExport);
                    }
                 }            
              }
         //else
         //{
         //    if (rdoMonthlyTracker.Checked == true)
         //    {
         //        if (UcMonthlyTrackerExport.Rows.Count > 0)
         //        {
         //            WebCommon.Export("Monthly Expedite Tracker", UcMonthlyTrackerExport);
         //        }
         //    }
         //    if (rdoDailyTracker.Checked == true)
         //    {
         //        if (UcDailyTrackerReportExport.Rows.Count > 0)
         //        {
         //            WebCommon.Export("Daily Expedite Tracker", UcDailyTrackerReportExport);
         //        }
         //    }
         //}
    }
    protected void UcGridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {     
       
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnCountryID = (HiddenField)e.Row.FindControl("hdnCountryID");
            HiddenField hdnSiteID = (HiddenField)e.Row.FindControl("hdnSiteID");
            if (hdnSiteID.Value == "0" && hdnCountryID.Value != "0")
            {
                e.Row.Cells[0].ForeColor = Color.Black;
                e.Row.Cells[1].ForeColor = Color.Black;
                e.Row.Cells[2].ForeColor = Color.Black;
                e.Row.Cells[3].ForeColor = Color.Black;
                e.Row.Cells[4].ForeColor = Color.Black;
                e.Row.Cells[5].ForeColor = Color.Black;
                e.Row.Cells[6].ForeColor = Color.Black;
                e.Row.Cells[7].ForeColor = Color.Black;
                e.Row.Cells[8].ForeColor = Color.Black;
                e.Row.Cells[9].ForeColor = Color.Black;
                e.Row.Cells[10].ForeColor = Color.Black;
                e.Row.Cells[11].ForeColor = Color.Black;
                e.Row.Cells[12].ForeColor = Color.Black;
                e.Row.Cells[13].ForeColor = Color.Black;
                e.Row.Cells[14].ForeColor = Color.Black;
                e.Row.Cells[15].ForeColor = Color.Black;
                e.Row.Cells[16].ForeColor = Color.Black;
                e.Row.Cells[17].ForeColor = Color.Black;
                e.Row.Cells[18].ForeColor = Color.Black;
                e.Row.Cells[19].ForeColor = Color.Black;
                e.Row.Cells[20].ForeColor = Color.Black;
                e.Row.Cells[21].ForeColor = Color.Black;
                e.Row.Cells[22].ForeColor = Color.Black;
                e.Row.Cells[0].Font.Bold = true;
                e.Row.Cells[1].Font.Bold = true;
                e.Row.Cells[2].Font.Bold = true;
                e.Row.Cells[3].Font.Bold = true;
                e.Row.Cells[4].Font.Bold = true;
                e.Row.Cells[5].Font.Bold = true;
                e.Row.Cells[6].Font.Bold = true;
                e.Row.Cells[7].Font.Bold = true;
                e.Row.Cells[8].Font.Bold = true;
                e.Row.Cells[9].Font.Bold = true;
                e.Row.Cells[10].Font.Bold = true;
                e.Row.Cells[11].Font.Bold = true;
                e.Row.Cells[12].Font.Bold = true;
                e.Row.Cells[13].Font.Bold = true;
                e.Row.Cells[14].Font.Bold = true;
                e.Row.Cells[15].Font.Bold = true;
                e.Row.Cells[16].Font.Bold = true;
                e.Row.Cells[17].Font.Bold = true;
                e.Row.Cells[18].Font.Bold = true;
                e.Row.Cells[19].Font.Bold = true;
                e.Row.Cells[20].Font.Bold = true;
                e.Row.Cells[21].Font.Bold = true;
                e.Row.Cells[22].Font.Bold = true;
            }

            if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("CountryID") != null && GetQueryStringValue("StockPlannerID") != null)
            {
                if (GetQueryStringValue("SiteID") == "0" && GetQueryStringValue("CountryID") == "0" && GetQueryStringValue("StockPlannerID") != "0")
                {
                    LinkButton hpStockPlanner = (LinkButton)e.Row.FindControl("hpStockPlanner");
                    Label lblGrouping = (Label)e.Row.FindControl("lblGrouping");
                    Label lblSite = (Label)e.Row.FindControl("lblSite");
                    Label lblStockPlanner = (Label)e.Row.FindControl("lblStockPlanner");
                    LinkButton hpSite = (LinkButton)e.Row.FindControl("hpSite");
                    hpSite.Text = "";
                    hpStockPlanner.Text = "";
                    lblGrouping.Text = "";
                    hpStockPlanner.Visible = false;
                    lblSite.Visible = true;
                    lblGrouping.Visible = false;
                    lblStockPlanner.Visible = true;
                    if (lblStockPlanner.Text == "")
                    {
                        e.Row.Cells[0].Text = "";
                        //e.Row.Cells[1].Text = "Total";
                        e.Row.Cells[0].ForeColor = Color.Black;
                        e.Row.Cells[1].ForeColor = Color.Black;
                        e.Row.Cells[2].ForeColor = Color.Black;
                        e.Row.Cells[3].ForeColor = Color.Black;
                        e.Row.Cells[4].ForeColor = Color.Black;
                        e.Row.Cells[5].ForeColor = Color.Black;
                        e.Row.Cells[6].ForeColor = Color.Black;
                        e.Row.Cells[7].ForeColor = Color.Black;
                        e.Row.Cells[8].ForeColor = Color.Black;
                        e.Row.Cells[9].ForeColor = Color.Black;
                        e.Row.Cells[10].ForeColor = Color.Black;
                        e.Row.Cells[11].ForeColor = Color.Black;
                        e.Row.Cells[12].ForeColor = Color.Black;
                        e.Row.Cells[13].ForeColor = Color.Black;
                        e.Row.Cells[14].ForeColor = Color.Black;
                        e.Row.Cells[15].ForeColor = Color.Black;
                        e.Row.Cells[16].ForeColor = Color.Black;
                        e.Row.Cells[17].ForeColor = Color.Black;
                        e.Row.Cells[18].ForeColor = Color.Black;
                        e.Row.Cells[19].ForeColor = Color.Black;
                        e.Row.Cells[20].ForeColor = Color.Black;
                        e.Row.Cells[21].ForeColor = Color.Black;
                        e.Row.Cells[22].ForeColor = Color.Black;
                        e.Row.Cells[0].Font.Bold = true;
                        e.Row.Cells[1].Font.Bold = true;
                        e.Row.Cells[2].Font.Bold = true;
                        e.Row.Cells[3].Font.Bold = true;
                        e.Row.Cells[4].Font.Bold = true;
                        e.Row.Cells[5].Font.Bold = true;
                        e.Row.Cells[6].Font.Bold = true;
                        e.Row.Cells[7].Font.Bold = true;
                        e.Row.Cells[8].Font.Bold = true;
                        e.Row.Cells[9].Font.Bold = true;
                        e.Row.Cells[10].Font.Bold = true;
                        e.Row.Cells[11].Font.Bold = true;
                        e.Row.Cells[12].Font.Bold = true;
                        e.Row.Cells[13].Font.Bold = true;
                        e.Row.Cells[14].Font.Bold = true;
                        e.Row.Cells[15].Font.Bold = true;
                        e.Row.Cells[16].Font.Bold = true;
                        e.Row.Cells[17].Font.Bold = true;
                        e.Row.Cells[18].Font.Bold = true;
                        e.Row.Cells[19].Font.Bold = true;
                        e.Row.Cells[20].Font.Bold = true;
                        e.Row.Cells[21].Font.Bold = true;
                        e.Row.Cells[22].Font.Bold = true;

                    }
                    hpStockPlanner.Visible = false;
                    lblGrouping.Visible = false;
                    lblSite.Visible = true;
                    UcGridView1.Columns[0].HeaderText = "Stock Planner";
                    UcGridView1.Columns[1].HeaderText = "Region/Site";
                    UcGridView1.Columns[0].Visible = true;
                    UcGridViewExport.Columns[0].HeaderText = "Stock Planner";
                    UcGridViewExport.Columns[1].HeaderText = "Region/Site";
                    UcGridViewExport.Columns[0].Visible = true;

                }
                else if ( GetQueryStringValue("CountryID") != "0" && GetQueryStringValue("StockPlannerID") == "0")
                {
                    LinkButton hpStockPlanner = (LinkButton)e.Row.FindControl("hpStockPlanner");
                    Label lblGrouping = (Label)e.Row.FindControl("lblGrouping");
                    Label lblVendor = (Label)e.Row.FindControl("lblVendor");
                    Label lblSite = (Label)e.Row.FindControl("lblSite");
                    LinkButton hpSite = (LinkButton)e.Row.FindControl("hpSite");
                    hpSite.Text = "";
                    hpStockPlanner.Text = "";
                    lblGrouping.Text = "";
                    hpStockPlanner.Visible = false;
                    lblGrouping.Visible = false;
                    lblVendor.Visible = true;
                    lblSite.Visible = true;
                    UcGridView1.Columns[0].Visible = true;
                    UcGridView1.Columns[0].HeaderText = "Vendor";
                    UcGridView1.Columns[1].HeaderText = "Region/Site";
                    UcGridViewExport.Columns[0].Visible = true;
                    UcGridViewExport.Columns[0].HeaderText = "Vendor";
                    UcGridViewExport.Columns[1].HeaderText = "Region/Site";
                }
            }
            else
            {
                if (rdoPlannerReport.Checked == true)
                {
                    UcGridView1.Columns[0].HeaderText = "Stock Planner";
                    UcGridView1.Columns[1].HeaderText = "Grouping";
                    UcGridViewExport.Columns[0].HeaderText = "Stock Planner";
                    UcGridViewExport.Columns[1].HeaderText = "Grouping";
                    LinkButton hpStockPlanner = (LinkButton)e.Row.FindControl("hpStockPlanner");
                    Label lblGrouping = (Label)e.Row.FindControl("lblGrouping");
                    Label lblSite = (Label)e.Row.FindControl("lblSite");
                    Label lblStockPlanner = (Label)e.Row.FindControl("lblStockPlanner");
                    //Label lblGrouping = (Label)e.Row.FindControl("lblGrouping");
                    lblStockPlanner.Text = "";
                    hpStockPlanner.Visible = true;
                    lblSite.Visible = false;
                    lblGrouping.Visible = true;
                    lblStockPlanner.Visible = false;

                    if (lblGrouping.Text == "Total")
                    {
                        //e.Row.Cells[1].Text = "Total";
                        e.Row.Cells[0].ForeColor = Color.Black;
                        e.Row.Cells[1].ForeColor = Color.Black;
                        e.Row.Cells[2].ForeColor = Color.Black;
                        e.Row.Cells[3].ForeColor = Color.Black;
                        e.Row.Cells[4].ForeColor = Color.Black;
                        e.Row.Cells[5].ForeColor = Color.Black;
                        e.Row.Cells[6].ForeColor = Color.Black;
                        e.Row.Cells[7].ForeColor = Color.Black;
                        e.Row.Cells[8].ForeColor = Color.Black;
                        e.Row.Cells[9].ForeColor = Color.Black;
                        e.Row.Cells[10].ForeColor = Color.Black;
                        e.Row.Cells[11].ForeColor = Color.Black;
                        e.Row.Cells[12].ForeColor = Color.Black;
                        e.Row.Cells[13].ForeColor = Color.Black;
                        e.Row.Cells[14].ForeColor = Color.Black;
                        e.Row.Cells[15].ForeColor = Color.Black;
                        e.Row.Cells[16].ForeColor = Color.Black;
                        e.Row.Cells[17].ForeColor = Color.Black;
                        e.Row.Cells[18].ForeColor = Color.Black;
                        e.Row.Cells[19].ForeColor = Color.Black;
                        e.Row.Cells[20].ForeColor = Color.Black;
                        e.Row.Cells[21].ForeColor = Color.Black;
                        e.Row.Cells[22].ForeColor = Color.Black;
                        e.Row.Cells[0].Font.Bold = true;
                        e.Row.Cells[1].Font.Bold = true;
                        e.Row.Cells[2].Font.Bold = true;
                        e.Row.Cells[3].Font.Bold = true;
                        e.Row.Cells[4].Font.Bold = true;
                        e.Row.Cells[5].Font.Bold = true;
                        e.Row.Cells[6].Font.Bold = true;
                        e.Row.Cells[7].Font.Bold = true;
                        e.Row.Cells[8].Font.Bold = true;
                        e.Row.Cells[9].Font.Bold = true;
                        e.Row.Cells[10].Font.Bold = true;
                        e.Row.Cells[11].Font.Bold = true;
                        e.Row.Cells[12].Font.Bold = true;
                        e.Row.Cells[13].Font.Bold = true;
                        e.Row.Cells[14].Font.Bold = true;
                        e.Row.Cells[15].Font.Bold = true;
                        e.Row.Cells[16].Font.Bold = true;
                        e.Row.Cells[17].Font.Bold = true;
                        e.Row.Cells[18].Font.Bold = true;
                        e.Row.Cells[19].Font.Bold = true;
                        e.Row.Cells[20].Font.Bold = true;
                        e.Row.Cells[21].Font.Bold = true;
                        e.Row.Cells[22].Font.Bold = true;
                    }

                    UcGridView1.Columns[0].Visible = true;
                    UcGridView1.Columns[0].HeaderText = "Stock Planner";
                    UcGridViewExport.Columns[0].Visible = true;
                    UcGridViewExport.Columns[0].HeaderText = "Stock Planner";
                }

                else if (rdoPlannerCountry.Checked == true)
                {
                    LinkButton hpStockPlanner = (LinkButton)e.Row.FindControl("hpStockPlanner");
                    Label lblGrouping = (Label)e.Row.FindControl("lblGrouping");
                    Label lblSite = (Label)e.Row.FindControl("lblSite");
                    Label lblStockPlanner = (Label)e.Row.FindControl("lblStockPlanner");
                    LinkButton hpSite = (LinkButton)e.Row.FindControl("hpSite");                   
                    hpStockPlanner.Text = "";
                    hpSite.Text = "";
                    lblGrouping.Text = "";
                    hpStockPlanner.Visible = false;
                    lblSite.Visible = true;
                    lblGrouping.Visible = false;
                    lblStockPlanner.Visible = true;
                    if (lblSite.Text == "Total")
                    {
                        //e.Row.Cells[1].Text = "Total";
                        e.Row.Cells[0].ForeColor = Color.Black;
                        e.Row.Cells[1].ForeColor = Color.Black;
                        e.Row.Cells[2].ForeColor = Color.Black;
                        e.Row.Cells[3].ForeColor = Color.Black;
                        e.Row.Cells[4].ForeColor = Color.Black;
                        e.Row.Cells[5].ForeColor = Color.Black;
                        e.Row.Cells[6].ForeColor = Color.Black;
                        e.Row.Cells[7].ForeColor = Color.Black;
                        e.Row.Cells[8].ForeColor = Color.Black;
                        e.Row.Cells[9].ForeColor = Color.Black;
                        e.Row.Cells[10].ForeColor = Color.Black;
                        e.Row.Cells[11].ForeColor = Color.Black;
                        e.Row.Cells[12].ForeColor = Color.Black;
                        e.Row.Cells[13].ForeColor = Color.Black;
                        e.Row.Cells[14].ForeColor = Color.Black;
                        e.Row.Cells[15].ForeColor = Color.Black;
                        e.Row.Cells[16].ForeColor = Color.Black;
                        e.Row.Cells[17].ForeColor = Color.Black;
                        e.Row.Cells[18].ForeColor = Color.Black;
                        e.Row.Cells[19].ForeColor = Color.Black;
                        e.Row.Cells[20].ForeColor = Color.Black;
                        e.Row.Cells[21].ForeColor = Color.Black;
                        e.Row.Cells[22].ForeColor = Color.Black;
                        e.Row.Cells[0].Font.Bold = true;
                        e.Row.Cells[1].Font.Bold = true;
                        e.Row.Cells[2].Font.Bold = true;
                        e.Row.Cells[3].Font.Bold = true;
                        e.Row.Cells[4].Font.Bold = true;
                        e.Row.Cells[5].Font.Bold = true;
                        e.Row.Cells[6].Font.Bold = true;
                        e.Row.Cells[7].Font.Bold = true;
                        e.Row.Cells[8].Font.Bold = true;
                        e.Row.Cells[9].Font.Bold = true;
                        e.Row.Cells[10].Font.Bold = true;
                        e.Row.Cells[11].Font.Bold = true;
                        e.Row.Cells[12].Font.Bold = true;
                        e.Row.Cells[13].Font.Bold = true;
                        e.Row.Cells[14].Font.Bold = true;
                        e.Row.Cells[15].Font.Bold = true;
                        e.Row.Cells[16].Font.Bold = true;
                        e.Row.Cells[17].Font.Bold = true;
                        e.Row.Cells[18].Font.Bold = true;
                        e.Row.Cells[19].Font.Bold = true;
                        e.Row.Cells[20].Font.Bold = true;
                        e.Row.Cells[21].Font.Bold = true;
                        e.Row.Cells[22].Font.Bold = true;
                    }


                    hpStockPlanner.Visible = false;
                    lblGrouping.Visible = false;
                    lblSite.Visible = true;
                    UcGridView1.Columns[0].HeaderText = "Stock Planner";
                    UcGridView1.Columns[1].HeaderText = "Region/Site";
                    UcGridView1.Columns[0].Visible = true;
                    UcGridViewExport.Columns[0].HeaderText = "Stock Planner";
                    UcGridViewExport.Columns[1].HeaderText = "Region/Site";
                    UcGridViewExport.Columns[0].Visible = true;

                }
                if (rdoRegionReport.Checked == true)
                {

                    UcGridView1.Columns[0].HeaderText = "";
                    UcGridView1.Columns[0].Visible = false;
                    UcGridView1.Columns[1].HeaderText = "Region/Site";

                    UcGridViewExport.Columns[0].HeaderText = "";
                   // UcGridViewExport.Columns[0].Visible = false;
                    UcGridViewExport.Columns[1].HeaderText = "Region/Site";
                    LinkButton hpStockPlanner = (LinkButton)e.Row.FindControl("hpStockPlanner");
                    Label lblGrouping = (Label)e.Row.FindControl("lblGrouping");
                    LinkButton hpSite = (LinkButton)e.Row.FindControl("hpSite");
                    Label lblSite = (Label)e.Row.FindControl("lblSite");
                    lblGrouping.Text = "";
                    lblSite.Text = "";
                    hpStockPlanner.Visible = false;
                    lblGrouping.Visible = false;
                    hpSite.Visible = true;
                    //if (e.Row.RowIndex == (UcGridViewExport.Rows.Count - 1))
                    //{
                    //    UcGridViewExport.Columns.RemoveAt(1);
                    //}
                    //e.Row.Cells[0].Visible = false;
                   // if (UcGridViewExport.Rows.Count > 0)
                    if (UcGridViewExport.HeaderRow != null)
                    {
                        UcGridViewExport.HeaderRow.Cells[0].Visible = false;
                        e.Row.Cells[0].Visible = false;
                    }
                   
                   
                }
                else if (rdoVendorReport.Checked == true)
                {
                    LinkButton hpStockPlanner = (LinkButton)e.Row.FindControl("hpStockPlanner");
                    Label lblGrouping = (Label)e.Row.FindControl("lblGrouping");
                    Label lblVendor = (Label)e.Row.FindControl("lblVendor");
                    Label lblSite = (Label)e.Row.FindControl("lblSite");
                    LinkButton hpSite = (LinkButton)e.Row.FindControl("hpSite");
                    hpSite.Visible = false;
                    hpSite.Text = "";
                    // HiddenField hdnCountryID = (HiddenField)e.Row.FindControl("hdnCountryID");
                    hpStockPlanner.Text = "";
                    lblGrouping.Text = "";
                    hpStockPlanner.Visible = false;
                    lblGrouping.Visible = false;
                    lblVendor.Visible = true;
                    lblSite.Visible = true;
                    UcGridView1.Columns[0].Visible = true;
                    UcGridView1.Columns[0].HeaderText = "Vendor";
                    UcGridView1.Columns[1].HeaderText = "Region/Site";

                    UcGridViewExport.Columns[0].Visible = true;
                    UcGridViewExport.Columns[0].HeaderText = "Vendor";
                    UcGridViewExport.Columns[1].HeaderText = "Region/Site";

                }               
            }
        }
    }
    protected void gdMonthlyTracker_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    public void AssignMonth()
    {
        if (ViewState["lstoStockPlannerGroupingsBE"] != null)
        {
            List<StockPlannerGroupingsBE> lstoStockPlannerGroupingsBE = (List<StockPlannerGroupingsBE>)ViewState["lstoStockPlannerGroupingsBE"];
            Month1 = lstoStockPlannerGroupingsBE[0].Month1_Date;
            Month2 = lstoStockPlannerGroupingsBE[0].Month2_Date;
            Month3 = lstoStockPlannerGroupingsBE[0].Month3_Date;
            Month4 = lstoStockPlannerGroupingsBE[0].Month4_Date;
            Month5 = lstoStockPlannerGroupingsBE[0].Month5_Date;
            Month6 = lstoStockPlannerGroupingsBE[0].Month6_Date;
            Month7 = lstoStockPlannerGroupingsBE[0].Month7_Date;
            Month8 = lstoStockPlannerGroupingsBE[0].Month8_Date;
            Month9 = lstoStockPlannerGroupingsBE[0].Month9_Date;
            Month10 = lstoStockPlannerGroupingsBE[0].Month10_Date;
            Month11 = lstoStockPlannerGroupingsBE[0].Month11_Date;
            Month12 = lstoStockPlannerGroupingsBE[0].Month12_Date;           
        }
    }
    public void AssignDay()
    {
        if (ViewState["lstoStockPlannerGroupingsBE"] != null)
        {
            List<StockPlannerGroupingsBE> lstoStockPlannerGroupingsBE = (List<StockPlannerGroupingsBE>)ViewState["lstoStockPlannerGroupingsBE"];
            Day1 = Common.ToDateTimeInMMDDYYYY(lstoStockPlannerGroupingsBE[0].Day1_Date);
            Day2 = Common.ToDateTimeInMMDDYYYY(lstoStockPlannerGroupingsBE[0].Day2_Date);
            Day3 = Common.ToDateTimeInMMDDYYYY(lstoStockPlannerGroupingsBE[0].Day3_Date);
            Day4 = Common.ToDateTimeInMMDDYYYY(lstoStockPlannerGroupingsBE[0].Day4_Date);
            Day5 = Common.ToDateTimeInMMDDYYYY(lstoStockPlannerGroupingsBE[0].Day5_Date);
            Day6 = Common.ToDateTimeInMMDDYYYY(lstoStockPlannerGroupingsBE[0].Day6_Date);
            Day7 = Common.ToDateTimeInMMDDYYYY(lstoStockPlannerGroupingsBE[0].Day7_Date);           
        }
    }
    protected void grdDailyTracker_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //ViewState["check"] = null;
        //if (e.Row.RowType == DataControlRowType.Header)
        //{
        //    Label lbltest = (Label)e.Row.FindControl("lbltest");
        //}
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{

        //    //if (grdDailyTracker.HeaderRow!=null)
        //    //{
        //    //    Label Day1 = (Label)grdDailyTracker.HeaderRow.FindControl("day1");
        //    //    Day1.Text = "";
        //    //}
          
              
        //   if( grdDailyTracker.Rows.Count > 0)
        //   {
        //       if (ViewState["check"] != null)
        //       {
        //          // this.grdDailyTracker.Columns[2].Visible = false;
        //       }
        //       else
        //       {
        //           ViewState["check"] = 1;
        //       }
        //       //e.Row.Cells[12].Visible = false;
        //       //e.Row.Cells[13].Visible = false;
        //       //e.Row.Cells[14].Visible = false;
        //       //e.Row.Cells[15].Visible = false;
        //       //e.Row.Cells[16].Visible = false;
        //       //e.Row.Cells[17].Visible = false;
        //       //e.Row.Cells[18].Visible = false;
        //      // Day1 = "";
        //        //grdDailyTracker.HeaderRow.Cells[0].Visible = false;
        //       // e.Row.Cells[0].Visible = false;
        //    }
        //}
    }
    public void ExportToExcelDailyTracker(DataTable dtExport, string fileName)
    {
        HttpContext context = HttpContext.Current;
        string date = DateTime.Now.ToString("ddMMyyyy");
        string time = DateTime.Now.ToString("HH:mm");
        context.Response.ContentType = "text/vnd.ms-excel";
        context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}_{1}_{2}", fileName, date, time + ".xls"));               
        if (dtExport.Rows.Count > 0)
        {   
       
            for (int i = 0; i < dtExport.Columns.Count; i++)
            {

                if (i > 6)
                {
                    switch (dtExport.Columns[i].ColumnName.ToString())
                    {
                        case "StockPlannerName":
                            context.Response.Write("|Stock Planner|");
                            context.Response.Write("\t");
                            break;

                        case "Day1_Items":
                            context.Response.Write("|" + Day1 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Day1_AtRisk":
                            context.Response.Write("|" + Day1 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Day1_Expdtd":
                            context.Response.Write("|" + Day1 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Day1_PercentExpdtd":
                            context.Response.Write("|" + Day1 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Day2_Items":
                            context.Response.Write("|" + Day2 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Day2_AtRisk":
                            context.Response.Write("|" + Day2 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Day2_Expdtd":
                            context.Response.Write("|" + Day2 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Day2_PercentExpdtd":
                            context.Response.Write("|" + Day2 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Day3_Items":
                            context.Response.Write("|" + Day3 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Day3_AtRisk":
                            context.Response.Write("|" + Day3 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Day3_Expdtd":
                            context.Response.Write("|" + Day3 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Day3_PercentExpdtd":
                            context.Response.Write("|" + Day3 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Day4_Items":
                            context.Response.Write("|" + Day4 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Day4_AtRisk":
                            context.Response.Write("|" + Day4 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Day4_Expdtd":
                            context.Response.Write("|" + Day4 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Day4_PercentExpdtd":
                            context.Response.Write("|" + Day4 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Day5_Items":
                            context.Response.Write("|" + Day5 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Day5_AtRisk":
                            context.Response.Write("|" + Day5 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Day5_Expdtd":
                            context.Response.Write("|" + Day5 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Day5_PercentExpdtd":
                            context.Response.Write("|" + Day5 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Day6_Items":
                            context.Response.Write("|" + Day6 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Day6_AtRisk":
                            context.Response.Write("|" + Day6 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Day6_Expdtd":
                            context.Response.Write("|" + Day6 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Day6_PercentExpdtd":
                            context.Response.Write("|" + Day6 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Day7_Items":
                            context.Response.Write("|" + Day7 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Day7_AtRisk":
                            context.Response.Write("|" + Day7 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Day7_Expdtd":
                            context.Response.Write("|" + Day7 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Day7_PercentExpdtd":
                            context.Response.Write("|" + Day7 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;
                    }
                }

            }
        }

        context.Response.Write(Environment.NewLine);

        //Write data
        foreach (DataRow row in dtExport.Rows)
        {

            for (int i = 0; i < dtExport.Columns.Count; i++)
            {
                if (i > 6)
                {
                    string rowdata = string.Empty;
                    rowdata = Convert.ToString(row.ItemArray[i]) == "" ? "0" : Convert.ToString(row.ItemArray[i]);
                    context.Response.Write(rowdata);
                    context.Response.Write("\t");
                }
            }
            context.Response.Write(Environment.NewLine);
        }
        context.Response.End();
    }
    public void ExportToExcelMonthlyTracker(DataTable dtExport, string fileName)
    {
        HttpContext context = HttpContext.Current;
        string date = DateTime.Now.ToString("ddMMyyyy");
        string time = DateTime.Now.ToString("HH:mm");
        context.Response.ContentType = "text/vnd.ms-excel";
        context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}_{1}_{2}", fileName, date, time + ".xls"));
        if (dtExport.Rows.Count > 0)
        {

            for (int i = 0; i < dtExport.Columns.Count; i++)
            {

                if (i > 11)
                {
                    switch (dtExport.Columns[i].ColumnName.ToString())
                    {
                        case "StockPlannerName":
                            context.Response.Write("|Stock Planner|");
                            context.Response.Write("\t");
                            break;

                        case "Month1_Items":
                            context.Response.Write("|" + Month1 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Month1_AtRisk":
                            context.Response.Write("|" + Month1 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Month1_Expdtd":
                            context.Response.Write("|" + Month1 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Month1_PercentExpdtd":
                            context.Response.Write("|" + Month1 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Month2_Items":
                            context.Response.Write("|" + Month2 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Month2_AtRisk":
                            context.Response.Write("|" + Month2 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Month2_Expdtd":
                            context.Response.Write("|" + Month2 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Month2_PercentExpdtd":
                            context.Response.Write("|" + Month2 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Month3_Items":
                            context.Response.Write("|" + Month3 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Month3_AtRisk":
                            context.Response.Write("|" + Month3 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Month3_Expdtd":
                            context.Response.Write("|" + Month3 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Month3_PercentExpdtd":
                            context.Response.Write("|" + Month3 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Month4_Items":
                            context.Response.Write("|" + Month4 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Month4_AtRisk":
                            context.Response.Write("|" + Month4 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Month4_Expdtd":
                            context.Response.Write("|" + Month4 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Month4_PercentExpdtd":
                            context.Response.Write("|" + Month4 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Month5_Items":
                            context.Response.Write("|" + Month5 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Month5_AtRisk":
                            context.Response.Write("|" + Month5 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Month5_Expdtd":
                            context.Response.Write("|" + Month5 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Month5_PercentExpdtd":
                            context.Response.Write("|" + Month5 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Month6_Items":
                            context.Response.Write("|" + Month6 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Month6_AtRisk":
                            context.Response.Write("|" + Month6 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Month6_Expdtd":
                            context.Response.Write("|" + Month6 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Month6_PercentExpdtd":
                            context.Response.Write("|" + Month6 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Month7_Items":
                            context.Response.Write("|" + Month7 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Month7_AtRisk":
                            context.Response.Write("|" + Month7 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Month7_Expdtd":
                            context.Response.Write("|" + Month7 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Month7_PercentExpdtd":
                            context.Response.Write("|" + Month7 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Month8_Items":
                            context.Response.Write("|" + Month8 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Month8_AtRisk":
                            context.Response.Write("|" + Month8 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Month8_Expdtd":
                            context.Response.Write("|" + Month8 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Month8_PercentExpdtd":
                            context.Response.Write("|" + Month8 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Month9_Items":
                            context.Response.Write("|" + Month9 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Month9_AtRisk":
                            context.Response.Write("|" + Month9 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Month9_Expdtd":
                            context.Response.Write("|" + Month9 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Month9_PercentExpdtd":
                            context.Response.Write("|" + Month9 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Month10_Items":
                            context.Response.Write("|" + Month10 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Month10_AtRisk":
                            context.Response.Write("|" + Month10 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Month10_Expdtd":
                            context.Response.Write("|" + Month10 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Month10_PercentExpdtd":
                            context.Response.Write("|" + Month10 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                        case "Month11_Items":
                            context.Response.Write("|" + Month11 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Month11_AtRisk":
                            context.Response.Write("|" + Month11 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Month11_Expdtd":
                            context.Response.Write("|" + Month11 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Month11_PercentExpdtd":
                            context.Response.Write("|" + Month11 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;


                        case "Month12_Items":
                            context.Response.Write("|" + Month12 + "_Items|");
                            context.Response.Write("\t");
                            break;
                        case "Month12_AtRisk":
                            context.Response.Write("|" + Month12 + "_AtRisk|");
                            context.Response.Write("\t");
                            break;
                        case "Month12_Expdtd":
                            context.Response.Write("|" + Month12 + "_Expdtd|");
                            context.Response.Write("\t");
                            break;
                        case "Month12_PercentExpdtd":
                            context.Response.Write("|" + Month12 + "_PercentExpdtd|");
                            context.Response.Write("\t");
                            break;

                    }
                }

            }
        }

        context.Response.Write(Environment.NewLine);

        //Write data
        foreach (DataRow row in dtExport.Rows)
        {

            for (int i = 0; i < dtExport.Columns.Count; i++)
            {
                if (i > 11)
                {
                    string rowdata = string.Empty;
                    rowdata = Convert.ToString(row.ItemArray[i]) == "" ? "0" : Convert.ToString(row.ItemArray[i]);
                    context.Response.Write(rowdata);
                    context.Response.Write("\t");
                }
            }
            context.Response.Write(Environment.NewLine);
        }
        context.Response.End();
    }
}
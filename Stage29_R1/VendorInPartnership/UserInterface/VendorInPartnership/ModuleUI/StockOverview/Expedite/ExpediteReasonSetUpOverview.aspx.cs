﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using Utilities;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;

public partial class ExpediteReasonSetUpOverview : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
     if(!IsPostBack)
     {
         
         BindExpediteReasonSetUpGrid();
     }
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvExpediteReasonSetUp;
        ucExportToExcel1.FileName = "ExpediteReasonSetUpOverview";
    }

    private void BindExpediteReasonSetUpGrid()
    {

        var lstExpediteStock = new List<ExpediteStockBE>();
        var expediteStock = new ExpediteStockBE();   
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        try
        {
            expediteStock.Action = "ShowAll";
            lstExpediteStock = oMAS_ExpStockBAL.GetAllReasonTypesBAL(expediteStock);
            if (lstExpediteStock != null && lstExpediteStock.Count > 0)
            {
                gvExpediteReasonSetUp.DataSource = lstExpediteStock;
                gvExpediteReasonSetUp.DataBind();
                ViewState["lstSites"] = lstExpediteStock;
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }  
    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<ExpediteStockBE>.SortList((List<ExpediteStockBE>)ViewState["lstSites"], e.SortExpression,e.SortDirection).ToArray();
    }

 
}
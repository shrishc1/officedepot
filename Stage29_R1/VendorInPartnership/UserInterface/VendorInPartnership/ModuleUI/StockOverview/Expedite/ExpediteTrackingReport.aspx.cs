﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using BaseControlLibrary;
using System.Collections;

public partial class ExpediteTrackingReport : CommonPage
{
    #region Declarations ...

    string Pleaseselectatleastonesite = WebCommon.getGlobalResourceValue("Pleaseselectatleastonesite");
    string Maximumfoursitesareallowed = WebCommon.getGlobalResourceValue("Maximumfoursitesareallowed");

    #endregion

    #region Events ...

    protected void Page_PreRender(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "PO")
            {
                if (Session["ExpediteTrackingReport"] != null)
                {
                    GetSession();
                }
            }
        }
    }

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        ucExportToExcel1.CurrentPage = this;
        //ucExportToExcel1.GridViewControl = gvTrackingReportExportExcel;
        ucExportToExcel1.FileName = "ExpediteTrackingReport";

        if (!IsPostBack)
        {
            pnlSearchScreen.Visible = true;
            pnlPotentialOutput.Visible = false;
            ucExportToExcel1.Visible = false;
            pager1.PageSize = 200;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;           
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(msSite.SelectedSiteIDs) || string.IsNullOrWhiteSpace(msSite.SelectedSiteIDs))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Pleaseselectatleastonesite + "')", true);
            return;
        }

        string[] SelectedSite = msSite.SelectedSiteIDs.Split(',');
        if (SelectedSite.Length > 4)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Maximumfoursitesareallowed + "')", true);
            return;
        }

        BindExpedite();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        this.ClearCriteria();
        pnlSearchScreen.Visible = true;
        pnlPotentialOutput.Visible = false;
        ucExportToExcel1.Visible = false;
    }

    protected void gvTrackingReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["TrackingReportData"] != null)
        {
            gvTrackingReport.DataSource = (List<ExpediteStockBE>)ViewState["TrackingReportData"];
            gvTrackingReport.PageIndex = e.NewPageIndex;
            if (Session["ExpediteTrackingReport"] != null)
            {
                Hashtable htExpediteStockOutput = (Hashtable)Session["ExpediteTrackingReport"];
                htExpediteStockOutput.Remove("PageIndex");
                htExpediteStockOutput.Add("PageIndex", e.NewPageIndex);
                Session["ExpediteTrackingReport"] = htExpediteStockOutput;
            }
            gvTrackingReport.DataBind();
        }
    }

    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)ucIncludeVendor.FindControl("lstSelectedVendor");
        ListBox lstVendor = (ListBox)ucIncludeVendor.FindControl("lstVendor");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);
            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                //ucIncludeVendor.innerControlHiddenField.Value = string.Empty;
                //ucIncludeVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    //ucIncludeVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    //ucIncludeVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            //ucIncludeVendor.innerControlHiddenField.Value = string.Empty;
            //ucIncludeVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindExpedite(currnetPageIndx);
    }

    #endregion

    #region Methods ...

    private void BindExpedite(int Page = 1)
    {
        var oExpediteStockBE = new ExpediteStockBE();
        var oExpediteStockBAL = new ExpediteStockBAL();
        oExpediteStockBE.Action = "GetTrackingReportData";
        oExpediteStockBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oExpediteStockBE.SelectedIncludedVendorIDs = this.GetSelectedVendorIds(ucIncludeVendor);
        oExpediteStockBE.SelectedExcludedVendorIDs = this.GetSelectedVendorIds(ucExcludeVendor);
        oExpediteStockBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        oExpediteStockBE.SelectedItemClassification = msItemClassification.selectedItemClassification;
        oExpediteStockBE.SelectedSkuType = Convert.ToInt32(rblSkuType.SelectedItem.Value);
        oExpediteStockBE.SelectedDisplay = Convert.ToInt32(rblDisplay.SelectedItem.Value);
        oExpediteStockBE.SelectedUpdate = Convert.ToInt32(rblUpdate.SelectedItem.Value);
        oExpediteStockBE.Page = Page;
        this.SetSession(oExpediteStockBE);
        var lstExpediteStock = oExpediteStockBAL.GetTrackingReportBAL(oExpediteStockBE);

        /* Filtering the Update logic on list collection.*/
        //var lastFirstWorkingDate = GetPastWorkingDate(DateTime.Now, 1);
        //var lastSecondWorkingDate = GetPastWorkingDate(DateTime.Now, 2);
        var lastThirdWorkingDate = GetPastWorkingDate(DateTime.Now, 3);
        var dateBeforThreeDays = DateTime.Now.AddDays(-3);
        switch (Convert.ToInt32(rblUpdate.SelectedValue))
        {
            case 1:
                break;
            case 2:
                lstExpediteStock = lstExpediteStock.FindAll(es => es.CommentDateLastUpdated < lastThirdWorkingDate || es.CommentDateLastUpdated == null);
                break;
            case 3:
                lstExpediteStock = lstExpediteStock.FindAll(es => es.CommentUpdateValidTo > DateTime.Now);
                break;
        }
       
        if (lstExpediteStock != null && lstExpediteStock.Count > 0)
        {
            gvTrackingReport.DataSource = lstExpediteStock;
            //gvTrackingReportExportExcel.DataSource = lstExpediteStock;
            pager1.ItemCount = lstExpediteStock[0].TotalRecords;
        }
        else
        {
            gvTrackingReport.DataSource = null;
            //gvTrackingReportExportExcel.DataSource = null;
        }

        gvTrackingReport.PageIndex = Page;
        pager1.CurrentIndex = Page;

        gvTrackingReport.DataBind();
        //gvTrackingReportExportExcel.DataBind();

        if (gvTrackingReport.Rows.Count > 0)
            ucExportToExcel1.Visible = true;

        pnlSearchScreen.Visible = false;
        pnlPotentialOutput.Visible = true;
    }

    private void ClearCriteria()
    {
        //msStockPlanner.SelectedStockPlannerIDs = string.Empty;
        //ucIncludeVendor.SelectedVendorIDs = string.Empty;
        //ucExcludeVendor.SelectedVendorIDs = string.Empty;
        msSite.SelectedSiteIDs = string.Empty;
        //msItemClassification.selectedItemClassification = string.Empty;
        //ucMRPType.SelectedMRPTypes = string.Empty;
        //UcPurcGrp.SelectedPurGrp = string.Empty;
        //UcMatGroup.SelectedMatGroup = string.Empty;
    }

    private string GetSelectedVendorIds(ucMultiSelectVendor multiSelectVendor)
    {
        ucListBox lstSelectedVendor = (ucListBox)multiSelectVendor.FindControl("lstSelectedVendor");
        var vendorIds = string.Empty;
        for (int index = 0; index < lstSelectedVendor.Items.Count; index++)
        {
            if (index.Equals(0))
                vendorIds = lstSelectedVendor.Items[index].Value;
            else
                vendorIds = string.Format("{0},{1}", vendorIds, lstSelectedVendor.Items[index].Value);
        }
        return vendorIds;
    }

    private DateTime GetPastWorkingDate(DateTime datetime, int pastday)
    {
        var dt = datetime.AddDays(-pastday);
        if (Convert.ToString(dt.DayOfWeek).Equals("Saturday"))
            dt = dt.AddDays(-1);
        if (Convert.ToString(dt.DayOfWeek).Equals("Sunday"))
            dt = dt.AddDays(-2);

        return dt;
    }

    private DateTime GetFutureWorkingDate(DateTime datetime, int pastday)
    {
        var dt = datetime.AddDays(pastday);
        if (Convert.ToString(dt.DayOfWeek).Equals("Saturday"))
            dt = dt.AddDays(1);
        if (Convert.ToString(dt.DayOfWeek).Equals("Sunday"))
            dt = dt.AddDays(2);

        return dt;
    }

    private void SetSession(ExpediteStockBE oExpediteStockBE)
    {
        Session["ExpediteTrackingReport"] = null;
        Session.Remove("ExpediteTrackingReport");

        Hashtable htExpediteTrackingReport = new Hashtable();
        htExpediteTrackingReport.Add("SelectedStockPlannerIDs", oExpediteStockBE.SelectedStockPlannerIDs);
        htExpediteTrackingReport.Add("SelectedIncludedVendorIDs", oExpediteStockBE.SelectedIncludedVendorIDs);
        htExpediteTrackingReport.Add("SelectedExcludedVendorIDs", oExpediteStockBE.SelectedExcludedVendorIDs);
        htExpediteTrackingReport.Add("SelectedSiteIDs", oExpediteStockBE.SelectedSiteIDs);
        htExpediteTrackingReport.Add("SelectedItemClassification", oExpediteStockBE.SelectedItemClassification);
        htExpediteTrackingReport.Add("SelectedSkuType", oExpediteStockBE.SelectedSkuType);
        htExpediteTrackingReport.Add("SelectedDisplay", oExpediteStockBE.SelectedDisplay);
        htExpediteTrackingReport.Add("SelectedUpdate", oExpediteStockBE.SelectedUpdate);
        htExpediteTrackingReport.Add("PageIndex", 0);

        Session["ExpediteTrackingReport"] = htExpediteTrackingReport;


    }

    private void GetSession()
    {

        ExpediteStockBE oExpediteStockBE = new ExpediteStockBE();
        ExpediteStockBAL oExpediteStockBAL = new ExpediteStockBAL();
        if (Session["ExpediteTrackingReport"] != null)
        {
            Hashtable htExpediteTrackingReport = (Hashtable)Session["ExpediteTrackingReport"];

            oExpediteStockBE.SelectedStockPlannerIDs = (htExpediteTrackingReport.ContainsKey("SelectedStockPlannerIDs") && htExpediteTrackingReport["SelectedStockPlannerIDs"] != null) ? htExpediteTrackingReport["SelectedStockPlannerIDs"].ToString() : "";
            oExpediteStockBE.SelectedIncludedVendorIDs = (htExpediteTrackingReport.ContainsKey("SelectedIncludedVendorIDs") && htExpediteTrackingReport["SelectedIncludedVendorIDs"] != null) ? htExpediteTrackingReport["SelectedIncludedVendorIDs"].ToString() : "";
            oExpediteStockBE.SelectedExcludedVendorIDs = (htExpediteTrackingReport.ContainsKey("SelectedExcludedVendorIDs") && htExpediteTrackingReport["SelectedExcludedVendorIDs"] != null) ? htExpediteTrackingReport["SelectedExcludedVendorIDs"].ToString() : "";
            oExpediteStockBE.SelectedSiteIDs = (htExpediteTrackingReport.ContainsKey("SelectedSiteIDs") && htExpediteTrackingReport["SelectedSiteIDs"] != null) ? htExpediteTrackingReport["SelectedSiteIDs"].ToString() : "";
            oExpediteStockBE.SelectedItemClassification = (htExpediteTrackingReport.ContainsKey("SelectedItemClassification") && htExpediteTrackingReport["SelectedItemClassification"] != null) ? htExpediteTrackingReport["SelectedItemClassification"].ToString() : "";
            oExpediteStockBE.SelectedSkuType = (htExpediteTrackingReport.ContainsKey("SelectedSkuType") && htExpediteTrackingReport["SelectedSkuType"] != null) ? Convert.ToInt32(htExpediteTrackingReport["SelectedSkuType"].ToString()) : 0;
            oExpediteStockBE.SelectedDisplay = (htExpediteTrackingReport.ContainsKey("SelectedDisplay") && htExpediteTrackingReport["SelectedDisplay"] != null) ? Convert.ToInt32(htExpediteTrackingReport["SelectedDisplay"].ToString()) : 0;
            oExpediteStockBE.SelectedUpdate = (htExpediteTrackingReport.ContainsKey("SelectedUpdate") && htExpediteTrackingReport["SelectedUpdate"] != null) ? Convert.ToInt32(htExpediteTrackingReport["SelectedUpdate"].ToString()) : 0;
            gvTrackingReport.PageIndex = Convert.ToInt32(htExpediteTrackingReport["PageIndex"].ToString());            
            oExpediteStockBE.Action = "GetTrackingReportData";
            var lstExpediteStock = oExpediteStockBAL.GetTrackingReportBAL(oExpediteStockBE);
          
            var lastThirdWorkingDate = GetPastWorkingDate(DateTime.Now, 3);
            var dateBeforThreeDays = DateTime.Now.AddDays(-3);
            switch (Convert.ToInt32(rblUpdate.SelectedValue))
            {
                case 1:
                    break;
                case 2:
                    lstExpediteStock = lstExpediteStock.FindAll(es => es.CommentDateLastUpdated < lastThirdWorkingDate);
                    break;
                case 3:
                    lstExpediteStock = lstExpediteStock.FindAll(es => es.CommentUpdateValidTo > DateTime.Now);
                    break;
            }


            ViewState["TrackingReportData"] = lstExpediteStock;
            if (lstExpediteStock != null && lstExpediteStock.Count > 0)
            {
                gvTrackingReport.DataSource = lstExpediteStock;
                //gvTrackingReportExportExcel.DataSource = lstExpediteStock;
                pager1.ItemCount = lstExpediteStock[0].TotalRecords;
            }
            else
            {
                gvTrackingReport.DataSource = null;
                //gvTrackingReportExportExcel.DataSource = null;
            }

            
            gvTrackingReport.DataBind();
            //gvTrackingReportExportExcel.DataBind();

            pager1.CurrentIndex = Convert.ToInt32(htExpediteTrackingReport["PageIndex"].ToString()); 


            if (gvTrackingReport.Rows.Count > 0)
                ucExportToExcel1.Visible = true;

            pnlSearchScreen.Visible = false;
            pnlPotentialOutput.Visible = true;

        }
    }

    #endregion
}
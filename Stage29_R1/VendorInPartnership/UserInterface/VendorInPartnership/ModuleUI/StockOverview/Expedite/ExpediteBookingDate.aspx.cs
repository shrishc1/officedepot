﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using System.Data;
using BusinessLogicLayer.ModuleBAL.Upload;

public partial class ExpediteBookingDate : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        if (!Page.IsPostBack)
        {
            BindBookings();
        }
    }

    protected void BindBookings()
    {
        if (GetQueryStringValue("SiteID") != null && GetQueryStringValue("SKUID") != null && GetQueryStringValue("VendorID") != null)
        {
            var oAPPBOK_BookingBE = new APPBOK_BookingBE();
            var oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
            oAPPBOK_BookingBE = new APPBOK_BookingBE();
            oAPPBOK_BookingBE.Action = "GetExpeditePOEnquiryBookingDetails";
            oAPPBOK_BookingBE.SkuId = Convert.ToInt32(GetQueryStringValue("SKUID"));
            oAPPBOK_BookingBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
            oAPPBOK_BookingBE.SiteId = Convert.ToInt32(GetQueryStringValue("SiteID"));
            var lstPOInquiryBookingDetails = oUP_PurchaseOrderDetailBAL.GetExpeditePOEnquiryBookingDetailsBAL(oAPPBOK_BookingBE);
            if (lstPOInquiryBookingDetails != null && lstPOInquiryBookingDetails.Count > 0)
            {
                DateTime BookingDate = !string.IsNullOrEmpty(GetQueryStringValue("BookingDate").ToString()) ? Convert.ToDateTime(GetQueryStringValue("BookingDate")) : DateTime.Now;


                var lstFinalBooking = from date in lstPOInquiryBookingDetails
                                      where date.ScheduleDate.Value >= (BookingDate)
                                      select date;

                gvBookingDetails.DataSource = lstFinalBooking;
            }
            else
                gvBookingDetails.DataSource = null;

            gvBookingDetails.DataBind();
        }
    }

    protected void UcbtnBacktoOutput_Click(object sender, EventArgs e)
    {
        Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");
        //EncryptQueryString("ExpediteStockSearch.aspx?PrevoiusPage=Comments");
    }
}
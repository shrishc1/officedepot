﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System.Web.UI;
using WebUtilities;
using System.Linq;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Collections;
using Utilities;

public partial class OpenPurchaseListing : CommonPage
{
    Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = null;
    UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = null;
    APPBOK_BookingBE oAPPBOK_BookingBE = null;
    DiscrepancyBE oDiscrepancyBE = null;

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("action") == null && GetQueryStringValue("page") != "SPOR") //Calling only first time not when comes from previous page
            {
                pnlPurchaseOrderInqSearch.Visible = true;
                pnlPurchaseOrderInqDetail.Visible = false;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        if (!Page.IsPostBack)
        {
            if (GetQueryStringValue("page") != null && GetQueryStringValue("page") == "SPOR")
            {
                if (!string.IsNullOrWhiteSpace(GetQueryStringValue("po")) && !string.IsNullOrWhiteSpace(GetQueryStringValue("status"))
                    && !string.IsNullOrWhiteSpace(GetQueryStringValue("vendorId")) && !string.IsNullOrWhiteSpace(GetQueryStringValue("siteId"))
                    && !string.IsNullOrWhiteSpace(GetQueryStringValue("orderRaised")))
                {
                    var po = GetQueryStringValue("po");
                    var status = GetQueryStringValue("status");
                    var vendorId = GetQueryStringValue("vendorId");
                    var siteId = GetQueryStringValue("siteId");
                    var orderRaised = GetQueryStringValue("orderRaised");
                    ShowPOInquiryDetailSections(po, status, Convert.ToInt32(vendorId), Convert.ToInt32(siteId), 0, orderRaised);
                }
            }
            else
            {
                if (GetQueryStringValue("action") != null && GetQueryStringValue("action").ToString() == "showlisting")
                {
                    if (Session["POOutput"] != null)
                    {
                        GetSession();
                    }
                }
                else if (GetQueryStringValue("action") == null) //Calling only first time not when comes from previous page
                {
                    GetOpenPurchaseListing();
                }
            }
        }
    }

    private void GetOpenPurchaseListing()
    {
        oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        oUp_PurchaseOrderDetailBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        oUp_PurchaseOrderDetailBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oUp_PurchaseOrderDetailBE.SKU = new UP_SKUBE();

        oUp_PurchaseOrderDetailBE.Action = "GetExpediteOpenPoListing";
        if (GetQueryStringValue("SiteID") != null)
        {
            oUp_PurchaseOrderDetailBE.Site.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        }
        if (GetQueryStringValue("OD_Code") != null)
        {
           // oUp_PurchaseOrderDetailBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
            oUp_PurchaseOrderDetailBE.OD_Code = GetQueryStringValue("OD_Code").Trim();
        }

        if (GetQueryStringValue("SKUID") != null)
        {
            oUp_PurchaseOrderDetailBE.SKU.Direct_SKU = GetQueryStringValue("SKUID");
        }

        List<Up_PurchaseOrderDetailBE> lstUp_PurchaseOrderDetailBE = oUP_PurchaseOrderDetailBAL.GetExpediteOpenPOListingBAL(oUp_PurchaseOrderDetailBE);
        if (lstUp_PurchaseOrderDetailBE.Count > 0)
        {
            ViewState["POGridDate"] = lstUp_PurchaseOrderDetailBE;
            gvPurchaseOrderInquiry.DataSource = lstUp_PurchaseOrderDetailBE;
            gvPurchaseOrderInquiry.DataBind();
            lblNoRecordsFound.Visible = false;
        }
        else
        {
            gvPurchaseOrderInquiry.DataSource = null;
            gvPurchaseOrderInquiry.DataBind();
            ViewState["POGridDate"] = null;
            lblNoRecordsFound.Visible = true;
        }
    }

    protected void gvPurchaseOrderInquiry_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPurchaseOrderInquiry.PageIndex = e.NewPageIndex;
        if (ViewState["POGridDate"] != null)
        {
            gvPurchaseOrderInquiry.DataSource = ViewState["POGridDate"];
            gvPurchaseOrderInquiry.DataBind();
            lblNoRecordsFound.Visible = false;
        }
    }

    protected void gvPurchaseOrderInquiry_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "SendOnPOHistory")
        {
            GridViewRow oGridViewRow = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            int intRowIndex = oGridViewRow.RowIndex;
            string strPONo = Convert.ToString(e.CommandArgument);
            HiddenField hdnVendorId = (HiddenField)gvPurchaseOrderInquiry.Rows[intRowIndex].FindControl("hdnVendorId");
            HiddenField hdnSiteId = (HiddenField)gvPurchaseOrderInquiry.Rows[intRowIndex].FindControl("hdnSiteId");
            ucLabel lblStatusPOInq = (ucLabel)gvPurchaseOrderInquiry.Rows[intRowIndex].FindControl("lblStatusPOInq");
            ucLabel lblOrderRaisedDatePOInq = (ucLabel)gvPurchaseOrderInquiry.Rows[intRowIndex].FindControl("lblOrderRaisedDatePOInq");

            if (!string.IsNullOrWhiteSpace(strPONo) && !string.IsNullOrWhiteSpace(hdnVendorId.Value) && !string.IsNullOrWhiteSpace(hdnSiteId.Value))
                ShowPOInquiryDetailSections(strPONo, lblStatusPOInq.Text, Convert.ToInt32(hdnVendorId.Value),
                    Convert.ToInt32(hdnSiteId.Value), Convert.ToInt32(GetQueryStringValue("SKUID")), lblOrderRaisedDatePOInq.Text);
        }
    }

    private void ShowPOInquiryDetailSections(string poNo, string status, int vendorId, int siteId, int skuid, string OrderRaisedDateDDMMYYYY)
    {
        SetSession(poNo, status, vendorId, siteId, skuid, OrderRaisedDateDDMMYYYY);

        #region Vendor POC
        var lstVendorPOC = new List<Up_PurchaseOrderDetailBE>();
        oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();
        Up_PurchaseOrderDetailBE oMAS_MaintainVendorPointsContactBE = new Up_PurchaseOrderDetailBE();
        oMAS_MaintainVendorPointsContactBE.Vendor = new UP_VendorBE();
        oMAS_MaintainVendorPointsContactBE.Vendor.VendorID = vendorId;
        oMAS_MaintainVendorPointsContactBE.Action = "GetVendorPOCDetails";
        lstVendorPOC = oUP_PurchaseOrderDetailBAL.GetPurchaseOrderVendorBAL(oMAS_MaintainVendorPointsContactBE);
        if (lstVendorPOC.Count > 0 && lstVendorPOC != null)
        {
            gvVendorPOC.DataSource = lstVendorPOC;
            gvVendorPOC.DataBind();
        }
        else
        {
            gvVendorPOC.DataSource = null;
            gvVendorPOC.DataBind();

        }

        #endregion

        #region PO Inquiry Main Action

        oUp_PurchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
        oUp_PurchaseOrderDetailBE.Action = "GetPOEnquiryMain";

        if (!string.IsNullOrWhiteSpace(poNo))
            oUp_PurchaseOrderDetailBE.Purchase_order = poNo;

        oUp_PurchaseOrderDetailBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        if (vendorId != 0)
            oUp_PurchaseOrderDetailBE.Vendor.VendorID = vendorId;

        oUp_PurchaseOrderDetailBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        if (siteId != 0)
            oUp_PurchaseOrderDetailBE.Site.SiteID = siteId;

        if (!string.IsNullOrWhiteSpace(status))
            oUp_PurchaseOrderDetailBE.Status = status;

        if (!string.IsNullOrWhiteSpace(OrderRaisedDateDDMMYYYY))
            oUp_PurchaseOrderDetailBE.Order_raised = Common.GetMM_DD_YYYY(OrderRaisedDateDDMMYYYY.Replace('-', '/'));
        #endregion

        var lstPOInquiryMain = oUP_PurchaseOrderDetailBAL.GetPurchaseOrderInquiryMainBAL(oUp_PurchaseOrderDetailBE);
        ViewState["POInquiryMain"] = lstPOInquiryMain;
        ViewState["SiteId"] = siteId;
        ViewState["VendorId"] = vendorId;
        ViewState["OrderRaisedDateDDMMYYYY"] = OrderRaisedDateDDMMYYYY.Replace('-', '/');
        if (lstPOInquiryMain.Count > 0)
        {
            lblPurchaseOrderV.Text = lstPOInquiryMain[0].Purchase_order;
            lblStatusV.Text = lstPOInquiryMain[0].Status;
            lblSiteV.Text = lstPOInquiryMain[0].Site.SiteName;

            if (lstPOInquiryMain[0].Original_due_date != null)
            {
                var originalDueDate = Convert.ToDateTime(lstPOInquiryMain[0].Original_due_date);

            }
            lblStockPlannerNoV.Text = lstPOInquiryMain[0].Buyer_no;
            lblBuyerV.Text = lstPOInquiryMain[0].StockPlannerName;

            var orderRaised = DateTime.Now.Date;
            if (lstPOInquiryMain[0].Order_raised != null)
            {
                orderRaised = Convert.ToDateTime(lstPOInquiryMain[0].Order_raised);
                lblOrderCreatedV.Text = orderRaised.ToString("dd/MM/yyyy");
            }

            #region PO Inquiry Details Action
            oUp_PurchaseOrderDetailBE.Action = "GetPOEnquiryDetails";
            oUp_PurchaseOrderDetailBE.Status = lstPOInquiryMain[0].Status;

            var lstPOInquiryDetails = oUP_PurchaseOrderDetailBAL.GetPurchaseOrderInquiryDetailsBAL(oUp_PurchaseOrderDetailBE);
            gvPODetails.DataSource = lstPOInquiryDetails;
            gvPODetails.DataBind();
            #endregion

            #region PO Inquiry Booking Details Action
            oAPPBOK_BookingBE = new APPBOK_BookingBE();
            oAPPBOK_BookingBE.Action = "GetPOEnquiryBookingDetails";
            if (!string.IsNullOrWhiteSpace(poNo))
                oAPPBOK_BookingBE.PurchaseOrders = poNo;

            if (vendorId != 0)
                oAPPBOK_BookingBE.VendorID = vendorId;

            if (siteId != 0)
                oAPPBOK_BookingBE.SiteId = siteId;

            oAPPBOK_BookingBE.PurchaseOrder = new Up_PurchaseOrderDetailBE();
            oAPPBOK_BookingBE.PurchaseOrder.Order_raised = Common.GetMM_DD_YYYY(OrderRaisedDateDDMMYYYY.Replace('-', '/'));

            var lstPOInquiryBookingDetails = oUP_PurchaseOrderDetailBAL.GetPurchaseOrderInquiryBookingDetailsBAL(oAPPBOK_BookingBE).OrderByDescending(book => book.ScheduleDate);
            gvBookingDetails.DataSource = lstPOInquiryBookingDetails;
            gvBookingDetails.DataBind();
            #endregion

            #region PO Inquiry Discrepancies Action
            oDiscrepancyBE = new DiscrepancyBE();
            oDiscrepancyBE.Action = "GetPOEnquiryDiscrepancies";
            oDiscrepancyBE.PurchaseOrder = new Up_PurchaseOrderDetailBE();
            if (!string.IsNullOrWhiteSpace(poNo))
                oDiscrepancyBE.PurchaseOrder.Purchase_order = poNo;

            if (vendorId != 0)
                oDiscrepancyBE.VendorID = vendorId;

            if (siteId != 0)
                oDiscrepancyBE.POSiteID = siteId;

            oDiscrepancyBE.PurchaseOrder.Order_raised = Common.GetMM_DD_YYYY(OrderRaisedDateDDMMYYYY.Replace('-', '/'));

            var lstPOInquiryDiscrepancies = oUP_PurchaseOrderDetailBAL.GetPurchaseOrderInquiryDiscrepanciesBAL(oDiscrepancyBE);
            gvDiscrepancies.DataSource = lstPOInquiryDiscrepancies;
            gvDiscrepancies.DataBind();

            #endregion

            #region Expedite History
            //Remaining Part
            ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
            ExpediteStockBE oNewExpediteStock = new ExpediteStockBE();
            oNewExpediteStock.Action = "ViewCommunication";
            oNewExpediteStock.SkuID = skuid;
            List<ExpediteStockBE> lstExpediteComments = oMAS_ExpStockBAL.GetExpediteCommentsHistoryBAL(oNewExpediteStock);

            gvExpediteHistory.DataSource = lstExpediteComments;
            gvExpediteHistory.DataBind();
            #endregion

            pnlPurchaseOrderInqSearch.Visible = false;
            pnlPurchaseOrderInqDetail.Visible = true;
            lblPurchaseOrder_1.Visible = true;
            lblOpenPurchaseListing.Visible = false;

            if (GetQueryStringValue("page") != null && GetQueryStringValue("page") == "SPOR")
            {
                pnlExpediteHistory.Visible = false;
                btnBack.Visible = false;
            }
            else
            {
                pnlExpediteHistory.Visible = true;
                btnBack.Visible = true;
            }
        }
    }

    private void SetSession(string poNo, string status, int vendorId, int siteId, int skuid, string raisedDate)
    {
        Session["POOutput"] = null;
        Hashtable htPOOutput = new Hashtable();
        htPOOutput.Add("PONO", poNo);
        htPOOutput.Add("Status", status);
        htPOOutput.Add("VendorID", vendorId);
        htPOOutput.Add("SiteID", siteId);
        htPOOutput.Add("Skuid", skuid);
        htPOOutput.Add("RaisedDate", raisedDate);
        Session["POOutput"] = htPOOutput;
    }

    private void GetSession()
    {
        if (Session["POOutput"] != null)
        {
            Hashtable htPOOutput = (Hashtable)Session["POOutput"];
            string PONO = (htPOOutput.ContainsKey("PONO") && htPOOutput["PONO"] != null) ? htPOOutput["PONO"].ToString() : string.Empty;
            string Status = (htPOOutput.ContainsKey("Status") && htPOOutput["Status"] != null) ? htPOOutput["Status"].ToString() : string.Empty;
            int VendorID = (htPOOutput.ContainsKey("VendorID") && htPOOutput["VendorID"] != null) ? Convert.ToInt32(htPOOutput["VendorID"].ToString()) : 0;
            int SiteID = (htPOOutput.ContainsKey("SiteID") && htPOOutput["SiteID"] != null) ? Convert.ToInt32(htPOOutput["SiteID"].ToString()) : 0;
            int Skuid = (htPOOutput.ContainsKey("Skuid") && htPOOutput["Skuid"] != null) ? Convert.ToInt32(htPOOutput["Skuid"].ToString()) : 0;
            string RaisedDate = (htPOOutput.ContainsKey("RaisedDate") && htPOOutput["RaisedDate"] != null) ? htPOOutput["RaisedDate"].ToString() : string.Empty;
            ShowPOInquiryDetailSections(PONO, Status, VendorID, SiteID, Skuid, RaisedDate);
        }
    }

    protected void gvDiscrepancies_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            var hpBookingRefPOInq = (HyperLink)e.Row.FindControl("hpBookingRefPOInq");
            var hdnDiscrepancyTypeID = (HiddenField)e.Row.FindControl("hdnDiscrepancyTypeID");
            var hdnDisLogID = (HiddenField)e.Row.FindControl("hdnDiscrepancyLogID");
            var hdnUserID = (HiddenField)e.Row.FindControl("hdnUserID");
            string VDRNo = hpBookingRefPOInq.Text;

            if (!string.IsNullOrEmpty(hdnDiscrepancyTypeID.Value))
            {
                #region Switch Case detail ...
                switch (Convert.ToInt32(hdnDiscrepancyTypeID.Value.Trim()))
                {
                    case 1:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Overs.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 2:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Shortage.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 3:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_GoodsReceivedDamaged.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 4:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_NoPurchaseOrder.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 5:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_NoPaperwork.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 6:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_IncorrectProduct.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 7:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PresentationIssue.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 8:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_IncorrectAddress.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 9:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PaperworkAmended.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 10:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_WrongPackSize.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 11:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_FailPalletSpecification.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 12:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_QualityIssue.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 13:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PrematureInvoiceReceipt.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 14:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_GenericDescrepancy.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 15:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Shuttle.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 16:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_Reservation.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 17:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_InvoiceDiscrepancy.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;

                    case 18:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_StockPlannerInvoiceDiscrepancy.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                    case 19:
                        hpBookingRefPOInq.NavigateUrl = EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_InvoiceQueryDiscrepancy.aspx?disLogID=" + hdnDisLogID.Value + "&VDRNo=" + VDRNo + "&UserID=" + hdnUserID.Value + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value);
                        break;
                }
                #endregion
            }
        }
    }

    private void POInquiryPanelVisibility(int inqDetail = 0)
    {
        switch (inqDetail)
        {
            case 0:
                pnlPODetails.Visible = true;
                pnlBookingDetails.Visible = true;
                pnlDiscrepancies.Visible = true;

                break;
            case 1:
                pnlPODetails.Visible = false;
                pnlBookingDetails.Visible = false;
                pnlDiscrepancies.Visible = false;

                break;
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        pnlPurchaseOrderInqSearch.Visible = true;
        pnlPurchaseOrderInqDetail.Visible = false;
        lblPurchaseOrder_1.Visible = false;
        lblOpenPurchaseListing.Visible = true;
    }

    protected void UcbtnBacktoOutput_Click(object sender, EventArgs e)
    {
        Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");
        //if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "TrackingReport")
        //{
        //    EncryptQueryString("ExpediteTrackingReport.aspx?PreviousPage=PO");
        //}
        //else
        //{
        //    EncryptQueryString("ExpediteStockSearch.aspx?PreviousPage=Comments");
        //}
    }

    protected void lnkClickToView_Click(object sender, EventArgs e)
    {
    }

    protected void gvExpediteHistory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Web.UI.HtmlControls.HtmlAnchor lnk = (System.Web.UI.HtmlControls.HtmlAnchor)e.Row.FindControl("lnk");
            lnk.Attributes.Add("href", EncryptQuery("~/ModuleUI/StockOverview/Expedite/Communication.aspx?CommunicationID=" + ((ExpediteStockBE)(e.Row.DataItem)).CommunicationID.ToString()));
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using WebUtilities;

public partial class ModuleUI_StockOverview_Expedite_PriorityItemsEdit : CommonPage
{
    protected string PriorityItemErrorMessage = WebCommon.getGlobalResourceValue("PriorityItemErrorMessage");
   protected string PriorityErrorMesg = WebCommon.getGlobalResourceValue("PriorityErrorMesg");
   protected string PriorityErrorMesg1 = WebCommon.getGlobalResourceValue("PriorityErrorMesg1");
 

    protected void Page_Init(object sender, EventArgs e)
    {
        ddlCountry.CurrentPage = this;
        ddlCountry.IsAllRequired = true;
        ddlCountry.IsAllDefault = true;
        ddlCountry.innerControlddlCountry.AutoPostBack = true;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
       
       // string value = GetQueryStringValue("value");


        tboxCatCode.Enabled = false;
        tboxSKU.Enabled = false;
        tboxReason.Enabled = false;
       if (!IsPostBack)
        {
                      
            if (GetQueryStringValue("CountryCarrierID") != null)
            {
                tboxCatCode.Enabled = false;
                tboxSKU.Enabled = false;
                tboxDescription.Enabled = false;
                ddlCountry.innerControlddlCountry.Enabled = false;
                tboxReason.Enabled = true;
                btnSave.Enabled = true;
                btnDelete.Visible = true;
                string CountryID = GetQueryStringValue("CountryCarrierID");
                ddlCountry.innerControlddlCountry.SelectedValue = CountryID;
                tboxSKU.Text = GetQueryStringValue("OD_Code");
                tboxCatCode.Text = GetQueryStringValue("Viking_Code");
                tboxDescription.Text = GetQueryStringValue("Product_description");
                tboxReason.Text = GetQueryStringValue("Reason");
                btn_search.Enabled = false;
                btn_search.Visible = false;
            }
        }
    }
    protected void btnDescriptionSearch_Click(object sender, EventArgs e)
    {
        BindDescription();
    }

    public void BindDescription()
    {
        ExpediteStockBE oMAS_ExpediteStockBE = new ExpediteStockBE();
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        oMAS_ExpediteStockBE.Action = "GetDescription";
        oMAS_ExpediteStockBE.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
        oMAS_ExpediteStockBE.OD_Code = tboxSKU.Text;
        oMAS_ExpediteStockBE.Viking_Code = tboxCatCode.Text;

        List<ExpediteStockBE> lst_ExpediteStockBE = oMAS_ExpStockBAL.GetDescriptionBAL(oMAS_ExpediteStockBE);

        if (lst_ExpediteStockBE.Count > 0)
        {
            if (string.IsNullOrEmpty(tboxSKU.Text))
            {
                tboxSKU.Text = lst_ExpediteStockBE[0].OD_Code;
            }
            if (string.IsNullOrEmpty(tboxCatCode.Text))
            {
                tboxCatCode.Text = lst_ExpediteStockBE[0].Viking_Code;
            }
            if (lst_ExpediteStockBE[0].Product_description == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PriorityErrorMesg1 + "')", true);
                tboxCatCode.Text = string.Empty;
                tboxSKU.Text = string.Empty;
           
            }
            else
            {
                tboxDescription.Text = lst_ExpediteStockBE[0].Product_description;
            }
            btnSave.Enabled = true;
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PriorityItemErrorMessage + "')", true);
            tboxCatCode.Text = string.Empty;
            tboxSKU.Text = string.Empty;
            tboxDescription.Text = string.Empty;
            btnSave.Enabled = false;
            
        }
        tboxCatCode.Enabled = true;
        tboxSKU.Enabled = true;
        tboxReason.Enabled = true;

    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        ExpediteStockBE oMAS_ExpediteStockBE = new ExpediteStockBE();
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();

        //if (GetQueryStringValue("CountryCarrierID") != null)
        //{
        string username = Convert.ToString(Session["UserName"]);

        oMAS_ExpediteStockBE.Action = "DeletePriorityReason";
        oMAS_ExpediteStockBE.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
        oMAS_ExpediteStockBE.OD_Code = tboxSKU.Text;
        oMAS_ExpediteStockBE.Viking_Code = tboxCatCode.Text;
        oMAS_ExpediteStockBE.Reason = tboxReason.Text;
        oMAS_ExpediteStockBE.IsPriorityAddedBy = username;

        oMAS_ExpStockBAL.SavePriorityReasonBAL(oMAS_ExpediteStockBE);
        if (GetQueryStringValue("CountryCarrierID") != null)
        {
            EncryptQueryString("PriorityItems.aspx?BacktoBind=1");
        }
        else
        {
            EncryptQueryString("PriorityItems.aspx");
        }        
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        
        ExpediteStockBE oMAS_ExpediteStockBE = new ExpediteStockBE();
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        
        

        //if (GetQueryStringValue("CountryCarrierID") != null)
        //{
            string username = Convert.ToString(Session["UserName"]);

            oMAS_ExpediteStockBE.Action = "SavePriorityReason";
            int CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
           
            oMAS_ExpediteStockBE.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
            oMAS_ExpediteStockBE.OD_Code = tboxSKU.Text;
            oMAS_ExpediteStockBE.Viking_Code = tboxCatCode.Text;
            oMAS_ExpediteStockBE.Reason = tboxReason.Text;
            oMAS_ExpediteStockBE.IsPriorityAddedBy = username;

            oMAS_ExpStockBAL.SavePriorityReasonBAL(oMAS_ExpediteStockBE);
            EncryptQueryString("PriorityItems.aspx?BacktoBind=1&CountryId="+CountryID);
            
        //}
        //else
        //{
 
        
        //}
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("CountryCarrierID") != null)
        {
            EncryptQueryString("PriorityItems.aspx?BacktoBind=1");
        }
        else
        {
            EncryptQueryString("PriorityItems.aspx");
        }        
    }

    public override void CountrySelectedIndexChanged()
    
    {
        if (ddlCountry.innerControlddlCountry.SelectedIndex > 0)
        {
            if (!(string.IsNullOrEmpty(tboxCatCode.Text) || string.IsNullOrEmpty(tboxSKU.Text)))
            {
                BindDescription();
            }
            
            tboxCatCode.Enabled = true;
            tboxSKU.Enabled = true;
            tboxReason.Enabled = true;
        }
        else
        {
            tboxCatCode.Enabled = false;
            tboxSKU.Enabled = false;
            tboxReason.Enabled = false;

        }
    }
   
}
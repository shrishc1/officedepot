﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ExpediteStock.aspx.cs" Inherits="ExpediteStock" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSelectStockPlanner.ascx" TagName="ucSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<script language="javascript" type="text/javascript">
function EmailSendConfirmation() {
            if (confirm('<%=doYouWantToSubmit%>')) {
                return true;
            }
            else {
                return false;
            }
        }
</script>

    <style>
        .txtAreaWrap table img
        {
            height: 110px;
            max-width: 500px;
        }
        .txtAreaWrap > table
        {
            width: 100% !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <script src="~/Scripts/JScript.js" type="text/javascript"></script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblExpediteStock" runat="server" Text="Expedite Stock"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSearchScreen" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                         
                             <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="ucIncludeVendor" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlPotentialOutput" runat="server">

                <div style="width: 100%; overflow: auto" id="divPrint" class="fixedTable">
                    <asp:UpdatePanel ID="updatepanel" runat="server">
                                <ContentTemplate>
                    <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table">
                        <tr>
                                <td  align="center">
                                    <cc1:ucRadioButton ID="rdoShowAll"  runat="server" AutoPostBack="true" OnCheckedChanged="rdoShowAll_CheckedChanged" Text="Show All Vendors" GroupName="ReportType"
                                        />
                                &nbsp; &nbsp; &nbsp; &nbsp;
                                    <cc1:ucRadioButton ID="rdoShowOnlyOpenVendorsPO" Checked="true" AutoPostBack="true" OnCheckedChanged="rdoShowOnlyOpen_CheckedChanged" runat="server" Text="Show only Vendor with Open" GroupName="ReportType"
                                        />
                                </td>
                               
                         </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td style="font-weight: bold; width: 100%">
                                <asp:Repeater ID="rptExpedite" runat="server" OnItemDataBound="rptExpedite_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnVendorID" runat="server" Value='<%#Eval("VendorID") %>' />
                                        <cc1:ucGridView ID="gvPotentialOutput" ClientIDMode="Static" Width="100%" Height="80%"
                                            runat="server" CssClass="grid" GridLines="Both" ShowFooter="True" ShowHeader="true"
                                            OnRowDataBound="gvPotentialOutput_OnRowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="SelectAllCheckBox">
                                                    <HeaderStyle HorizontalAlign="Left" Width="6%" />
                                                    <ItemStyle Width="6%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <FooterStyle Width="100px" HorizontalAlign="Left" BorderStyle="None" BorderColor="White"
                                                        ForeColor="White" />
                                                    <HeaderTemplate>
                                                        <cc1:ucCheckbox runat="server" ID="chkSelectAllText" CssClass="checkbox-input" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" CssClass="innerCheckBox" ViewStateMode="Enabled" ID="chkSelect" />
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <cc1:ucButton ID="btnExpedite" runat="server" Text="Expedite" CssClass="button" OnClick="btnExpedite_Click" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vendor #" SortExpression="Vendor_No">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblVendorNoValue" Text='<%#Eval("Vendor_No") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vendor Name" SortExpression="VendorName">
                                                    <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblVendorNameValue" Text='<%#Eval("VendorName") %>'></cc1:ucLabel>
                                                        <asp:HiddenField ID="hdnVendor" runat="server" Value='<%#Eval("VendorID") %>'></asp:HiddenField>
                                                        <asp:HiddenField ID="hdnSkuId" runat="server" Value='<%#Eval("SkuID") %>'></asp:HiddenField>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Site" SortExpression="SiteName">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnSiteId" Value='<%#Eval("SiteID") %>' runat="server" />
                                                        <asp:HiddenField ID="hdnPurchase_order" Value='<%#Eval("PurchaseOrder.Purchase_order") %>'
                                                            runat="server" />
                                                        <asp:HiddenField ID="hdnCommentID" Value='<%#Eval("CommentID") %>' runat="server" />
                                                        <asp:HiddenField ID="hdnOriginal_due_date" Value='<%#Eval("PurchaseOrder.Original_due_date","{0:dd/MM/yyyy}") %>'
                                                            runat="server" />
                                                        <cc1:ucLabel runat="server" ID="lblSiteNameValue" Text='<%#Eval("SiteName") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Office Depot Code" SortExpression="OD_Code">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblOD_CodeValue" Text='<%#Eval("OD_Code") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Viking Code" SortExpression="Direct_code">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblVikingCodeValue" Text='<%#Eval("Direct_code") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vendor Code" SortExpression="Vendor_Code">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblVendorCodeValue" Text='<%#Eval("Vendor_Code") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" SortExpression="Product_description">
                                                    <HeaderStyle Width="400px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblProductDesValue" Text='<%#Eval("Product_description") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="300px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stock Planner #" SortExpression="StockPlannerNo">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblStockPlannerNoValue" Text='<%#Eval("StockPlannerNo") %>'></cc1:ucLabel>
                                                        <cc1:ucLabel Visible="false" runat="server" ID="lblStockPlannerCountry" Text='<%#Eval("CountryName") %>'></cc1:ucLabel>
                                                        <asp:HiddenField ID="hdnStockPlannerID" runat="server" Value='<%#Eval("SelectedStockPlannerIDs") %>'>
                                                        </asp:HiddenField>                                                        
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stock Planner Name" SortExpression="StockPlannerName">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblStockPlannerNameValue" Text='<%#Eval("StockPlannerName") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Comment Date" SortExpression="CommentDate">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblCommentDateDate" Text='<%#Eval("CommentsAdded_Date", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Latest Comment" SortExpression="Comment">
                                                    <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblMostCurrentCommentValue" Text='<%#Eval("COMMENTS") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="150px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblStatusValue" Text='<%#Eval("ExpStatus") %>'></cc1:ucLabel>
                                                        <a id="lnk" runat="server" target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=50px,top=20px,Width=1200px,height=640px,toolbar=0,scrollbars=0,status=0"); return false;'>
                                                        </a>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </cc1:ucGridView>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <asp:HiddenField ID="hdnVendorID" runat="server" Value='<%#Eval("VendorID") %>' />
                                        <cc1:ucGridView ID="gvPotentialOutput" ClientIDMode="Static" Width="100%" Height="80%"
                                            runat="server" CssClass="grid" GridLines="Both" ShowFooter="True" ShowHeader="true"
                                            OnRowDataBound="gvPotentialOutput_OnRowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="SelectAllCheckBox">
                                                    <HeaderStyle HorizontalAlign="Left" Width="6%" />
                                                    <ItemStyle Width="6%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <FooterStyle Width="100px" HorizontalAlign="Left" BorderStyle="None" BorderColor="White"
                                                        ForeColor="White" />
                                                    <HeaderTemplate>
                                                        <cc1:ucCheckbox runat="server" ID="chkSelectAllText" CssClass="checkbox-input" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" CssClass="innerCheckBox" ViewStateMode="Enabled" ID="chkSelect" />
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <cc1:ucButton ID="btnExpedite" runat="server" Text="Expedite" CssClass="button" OnClick="btnExpedite_Click" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vendor #" SortExpression="Vendor_No">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblVendorNoValue" Text='<%#Eval("Vendor_No") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vendor Name" SortExpression="VendorName">
                                                    <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblVendorNameValue" Text='<%#Eval("VendorName") %>'></cc1:ucLabel>
                                                        <asp:HiddenField ID="hdnVendor" runat="server" Value='<%#Eval("VendorID") %>'></asp:HiddenField>
                                                        <asp:HiddenField ID="hdnSkuId" runat="server" Value='<%#Eval("SkuID") %>'></asp:HiddenField>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Site" SortExpression="SiteName">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnSiteId" Value='<%#Eval("SiteID") %>' runat="server" />
                                                        <asp:HiddenField ID="hdnPurchase_order" Value='<%#Eval("PurchaseOrder.Purchase_order") %>'
                                                            runat="server" />
                                                        <asp:HiddenField ID="hdnCommentID" Value='<%#Eval("CommentID") %>' runat="server" />
                                                        <asp:HiddenField ID="hdnOriginal_due_date" Value='<%#Eval("PurchaseOrder.Original_due_date","{0:dd/MM/yyyy}") %>'
                                                            runat="server" />
                                                        <cc1:ucLabel runat="server" ID="lblSiteNameValue" Text='<%#Eval("SiteName") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Office Depot Code" SortExpression="OD_Code">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblOD_CodeValue" Text='<%#Eval("OD_Code") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Viking Code" SortExpression="Direct_code">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblVikingCodeValue" Text='<%#Eval("Direct_code") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vendor Code" SortExpression="Vendor_Code">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblVendorCodeValue" Text='<%#Eval("Vendor_Code") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" SortExpression="Product_description">
                                                    <HeaderStyle Width="400px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblProductDesValue" Text='<%#Eval("Product_description") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="300px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stock Planner #" SortExpression="StockPlannerNo">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblStockPlannerNoValue" Text='<%#Eval("StockPlannerNo") %>'></cc1:ucLabel>
                                                        <cc1:ucLabel Visible="false" runat="server" ID="lblStockPlannerCountry" Text='<%#Eval("CountryName") %>'></cc1:ucLabel>
                                                        <asp:HiddenField ID="hdnStockPlannerID" runat="server" Value='<%#Eval("SelectedStockPlannerIDs") %>'>
                                                        </asp:HiddenField>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="50px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stock Planner Name" SortExpression="StockPlannerName">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblStockPlannerNameValue" Text='<%#Eval("StockPlannerName") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Comment Date" SortExpression="CommentDate">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblCommentDateDate" Text='<%#Eval("CommentsAdded_Date", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Latest Comment" SortExpression="Comment">
                                                    <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblMostCurrentCommentValue" Text='<%#Eval("COMMENTS") %>'></cc1:ucLabel>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="150px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                                    <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <cc1:ucLabel runat="server" ID="lblStatusValue" Text='<%#Eval("ExpStatus") %>'></cc1:ucLabel>
                                                        <a id="lnk" runat="server" target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=50px,top=20px,Width=1200px,height=640px,toolbar=0,scrollbars=0,status=0"); return false;'>
                                                        </a>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </cc1:ucGridView>
                                    </AlternatingItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <cc1:ucGridView ID="gvExcel" ClientIDMode="Static" Width="100%" Height="80%" runat="server"
                                    CssClass="grid" GridLines="Both" Style="display: none;">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Vendor #" SortExpression="Vendor_No">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorNoValue" Text='<%#Eval("Vendor_No") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vendor Name" SortExpression="VendorName">
                                            <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorNameValue" Text='<%#Eval("VendorName") %>'></cc1:ucLabel>
                                                <asp:HiddenField ID="hdnVendor" runat="server" Value='<%#Eval("VendorID") %>'></asp:HiddenField>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="200px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Site" SortExpression="SiteName">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSiteId" Value='<%#Eval("SiteID") %>' runat="server" />
                                                <cc1:ucLabel runat="server" ID="lblSiteNameValue" Text='<%#Eval("SiteName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="100px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Office Depot Code" SortExpression="OD_Code">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblOD_CodeValue" Text='<%#Eval("OD_Code") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="100px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Viking Code" SortExpression="Direct_code">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVikingCodeValue" Text='<%#Eval("Direct_code") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="100px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vendor Code" SortExpression="Vendor_Code">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorCodeValue" Text='<%#Eval("Vendor_Code") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="100px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description" SortExpression="Product_description">
                                            <HeaderStyle Width="400px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblProductDesValue" Text='<%#Eval("Product_description") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="300px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Stock Planner #" SortExpression="StockPlannerNo">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblStockPlannerNoValue" Text='<%#Eval("StockPlannerNo") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Stock Planner Name" SortExpression="StockPlannerName">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblStockPlannerNameValue" Text='<%#Eval("StockPlannerName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comment Date" SortExpression="CommentDate">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblCommentDateDate" Text='<%#Eval("CommentsAdded_Date", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="100px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Latest Comment" SortExpression="Comment">
                                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblMostCurrentCommentValue" Text='<%#Eval("COMMENTS") %>'></cc1:ucLabel>
                                                <asp:HiddenField runat="server" ID="hdnCommentID" Value='<%#Eval("CommentID") %>'>
                                                </asp:HiddenField>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="20%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblStatusValue" Text='<%#Eval("ExpStatus") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="100px" />
                                        </asp:TemplateField>
                                    </Columns>
                                </cc1:ucGridView>
                            </td>
                        </tr>
                        <%--<tr>
                            <td align="right">
                                <div class="button-row">
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                </div>
                            </td>
                        </tr>--%>
                    </table>
                     </ContentTemplate>
                            </asp:UpdatePanel>
                    <div style="float:right">  <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" /></div>
                </div>
            </cc1:ucPanel>
        </div>
    </div>
    <asp:UpdatePanel ID="updNoShow" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnNoShows" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdNoShow" runat="server" TargetControlID="btnNoShows"
                PopupControlID="pnlbtnNoShow" BackgroundCssClass="modalBackground" BehaviorID="MsgNoShow"
                DropShadow="false" />
            <asp:Panel ID="pnlbtnNoShow" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLiteral ID="ltNoPo" runat="server" Text=""></cc1:ucLiteral>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnProceed" CommandName="NoShows" runat="server" Text="Proceed"
                                    OnCommand="btnProceed_Click" CssClass="button" />
                                <cc1:ucButton ID="btnBack_1" CommandName="NoShows" runat="server" Text="Back" OnCommand="btnBack_1_Click"
                                    CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnProceed" />
        </Triggers>
    </asp:UpdatePanel>
    <%-- for Expedite popup --%>
    <asp:UpdatePanel ID="updExpeditePopup" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnExpeditePopup" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlExpeditePopup" runat="server" TargetControlID="btnExpeditePopup"
                PopupControlID="pnlExpeditePopup" BackgroundCssClass="modalBackground" BehaviorID="MsgExpeditePopup"
                DropShadow="false" />
            <asp:Panel ID="pnlExpeditePopup" runat="server" Style="display: none; padding-left: 100px;">
                <div style="width: 85%; height: 585px; overflow: scroll;">
                    <asp:Panel ID="pnlManageLetter" runat="server">
                        <table width="99%" cellspacing="5" cellpadding="0" border="0" align="left" class="top-settings">
                            <tr>
                                <td align="left" style="font-weight: bold; width: 33%;">
                                    <cc1:ucLabel ID="lblSubject" runat="server" Text="Subject"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 2%;">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 20%;">
                                    <cc1:ucTextbox ID="txtSubjectText" runat="server" Width="200px"></cc1:ucTextbox>
                                </td>
                                <td align="right" style="font-weight: bold; width: 10%;">
                                    <cc1:ucLabel ID="lblSentTo" runat="server" Text="Sent To"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 2%;">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 33%;">
                                    <cc1:ucTextbox ID="txtSentTo" runat="server" Width="300px"></cc1:ucTextbox>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;" colspan="6">
                                    <cc1:ucLabel ID="lblPleaseenteremailcommunicationbelow" runat="server" Text="Please enter email communication below"></cc1:ucLabel>&nbsp;:
                                </td>
                            </tr>
                        </table>
                        <table width="99%" cellspacing="5" cellpadding="0" border="0" align="left" class="top-settings">
                            <tr>
                                <td style="font-weight: bold;" colspan="6">
                                    <CKEditor:CKEditorControl ID="txtEmailCommunication" Width="900px" Height="220px"
                                        runat="server"></CKEditor:CKEditorControl>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblSentBy" runat="server" Text="Sent By"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td colspan="3" style="font-weight: bold;">
                                    <cc1:ucSelectStockPlanner runat="server" ID="ucSP" />
                                </td>
                                <td align="center" colspan="1" valign="bottom">
                                    <div class="button-row">
                                        <cc1:ucButton ID="btnSubmit" runat="server" Text="Submit" CssClass="button" OnClick="btnSubmit_Click" />
                                        <cc1:ucButton ID="btnBack_2" runat="server" Text="Back" CssClass="button" OnClick="btnBack_2_Click"
                                            OnClientClick="return CommLibDataLostConfirmation();" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="pnlDisplayLetter" runat="server" Visible="false">
                        <table width="96%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td align="left" style="width: 100%">
                                    <div id="dvDisplayLetter" class="txtAreaWrap" runat="server">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <div class="button-row" style="text-align: center !important;">
                                        <cc1:ucButton ID="btnYes" runat="server" Text="Yes" CssClass="button" OnClick="btnYes_Click"
                                            OnClientClick="return EmailSendConfirmation();" />
                                        <cc1:ucButton ID="btnNo" runat="server" Text="No" CssClass="button" OnClick="btnNo_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnSubmit" />
            <%--<asp:PostBackTrigger ControlID="btnYes" />--%>
            <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnNo" />
             <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnYes" />
        </Triggers>
    </asp:UpdatePanel>
    <%-- for Expedite popup --%>
    <script type="text/javascript" language="javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
        function EndRequest(sender, args) {
            if (args.get_error() == undefined) {
                $("input[type='checkbox'][name$='chkSelectAllText']").click(function () {
                   // debugger;
                    var table = $(this).parent().parent().parent().parent().parent();
                    if ($(this).is(":checked")) {
                        $.each(table.find("input[type='checkbox']:gt(0)"), function (index, element) {
                            if ($(this).attr("disabled") == false) {
                                $(this).attr("checked", "checked");
                            }
                        });
                    }
                    else {
                        table.find("input[type='checkbox']:gt(0)").removeAttr('checked');
                    }
                });
//                $('#<%= btnBack_2.ClientID%>').click(function () {
//                    CommLibDataLostConfirmation();
//                });

            }
        }

        function CommLibDataLostConfirmation() {
            if (confirm('<%=commLibBackDataLostConfirmation%>')) {
                return true;
            }
            else {
                return false;
            }
        }

        $(document).ready(function () {

            $("input[type='checkbox'][name$='chkSelectAllText']").click(function () {

                var table = $(this).parent().parent().parent().parent().parent();
                if ($(this).is(":checked")) {
                    $.each(table.find("input[type='checkbox']:gt(0)"), function (index, element) {
                        if ($(this).attr("disabled") == false) {
                            $(this).attr("checked", "checked");
                        }
                    });
                }
                else {
                    table.find("input[type='checkbox']:gt(0)").removeAttr('checked');
                }
            });

        });       
    </script>
</asp:Content>

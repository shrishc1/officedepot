﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="ExpediteTracker.aspx.cs" Inherits="ModuleUI_StockOverview_Expedite_Expedite_Tracker_ExpediteTracker" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlannerGrouping.ascx" TagName="MultiSelectStockPlannerGrouping"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectItemClassification.ascx" TagName="MultiSelectItemClassification"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%-- <%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblExpediteTrackerReports" runat="server"></cc1:ucLabel>
    </h2>
    <style type="text/css">
        .wmd-view-topscroll, .wmd-view
        {
            overflow-x: auto;
            overflow-y: hidden;
            width: 960px;
        }
        
        .wmd-view-topscroll
        {
            height: 16px;
        }
        
        .dynamic-div
        {
            display: inline-block;
        }
        th.stock-color{background: url(../Images/table-headbg.jpg) repeat-x top left #faf5ef !important;}
        th.stock-color1{background: url(../Images/table-headbg.jpg) repeat-x top left #E0E0D1 !important;}
        #content .grid th {background:none;}
    </style>
   
    <script type="text/javascript">
//        function calcWorkingDays(fromDate, days) {
//            var count = 0;
//            while (count < days) {
//                fromDate.setDate(fromDate.getDate() + 1);
//                if (fromDate.getDay() != 0 && fromDate.getDay() != 6) // Skip weekends
//                    count++;
//            }
//            return fromDate;
//        }

        function WeekendCheck() {
            var rdoDailyTracker = document.getElementById('<%=rdoDailyTracker.ClientID %>');
            if (rdoDailyTracker != null) {
                if (rdoDailyTracker.checked) {
                    var datefrom = document.getElementById('<%=txtFromDate.ClientID %>').value;
                    var InputDate = datefrom;
                    var x = InputDate.split("/");
                    var date1 = new Date(x[2], (x[1] - 1), x[0]);
                    if (!(date1.getDay() % 6)) {
                        alert('<%=WeekendCheck%>');
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                else {
                    return true;
                }
            }
            else {
                return true;
            }
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
        $(document).ready(function () {
            //alert(calcWorkingDays(new Date("04/nov/2015"), 6));
            CheckDateForDailyTracker();
            $(".wmd-view-topscroll").scroll(function () {
                $(".wmd-view")
            .scrollLeft($(".wmd-view-topscroll").scrollLeft());
            });

            $(".wmd-view").scroll(function () {
                $(".wmd-view-topscroll")
            .scrollLeft($(".wmd-view").scrollLeft());
            });
            topscroll();
//                        $('#<%=btnBack.ClientID%>').click(function () {
//                          alert(getPathFromUrl(window.location.href));
//                        });
        });

        function getPathFromUrl(url) {
            return url.split("?")[0];
        }
        function topscroll() {
            $(".dynamic-div div").css("float", "left");
            var scrollwidth = $(".dynamic-div > div").width();
            $('.scroll-div').css('width', scrollwidth + "px");
        }
        function Reload() {
            window.open("ExpediteTracker.aspx");
        }
        // add n number of days
        function loadDate() {            
            $('#<%=txtToDate.ClientID %>').val('<%=DayToOnload%>');
            $('#<%=txtFromDate.ClientID %>').val('<%=DayFromOnLoad%>');
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = $('#<%=txtFromDate.ClientID %>').val();
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = $('#<%=txtToDate.ClientID %>').val();
        }
        function CheckDateForDailyTracker() {
            var rdoDailyTracker = document.getElementById('<%=rdoDailyTracker.ClientID %>');
            if (rdoDailyTracker != null) {
                if (rdoDailyTracker.checked) {
                    var datefrom = document.getElementById('<%=txtFromDate.ClientID %>').value;
                    var dateto = document.getElementById('<%=txtToDate.ClientID %>').value;
                    var InputDate = datefrom;
                    var x = InputDate.split("/");
                    var date1 = new Date(x[2], (x[1] - 1), x[0]);
                    var count = 0;
                    while (count < 6) {
                        date1.setDate(date1.getDate() + 1);
                        if (date1.getDay() != 0 && date1.getDay() != 6) // Skip weekends
                            count++;
                    }

                    //  date1.setDate(date1.getDate() + 6)
                    var day;

                    if (parseInt(date1.getDate()) > 9) {
                        day = date1.getDate();
                    }
                    else {
                        day = "0" + date1.getDate();
                    }
                    //debugger;
//                    if (new Date(x[2], (x[1] - 1), x[0]).getDay() == "5") {
//                        if (parseInt(date1.getDate()) + 1 > 9) {
//                            day = (parseInt(date1.getDate()) + 1);
//                        }
//                        else {
//                            day = "0" + (parseInt(date1.getDate()) + 1);
//                        }
//                    }
                           
                    var dateto = day + "/" + (date1.getMonth() + 1) + "/" + date1.getFullYear();
                   
                    $('#<%=txtToDate.ClientID %>').val(dateto);
                    document.getElementById('<%=hdnJSToDt.ClientID %>').value = $('#<%=txtToDate.ClientID %>').val();
                    document.getElementById('<%=hdnJSFromDt.ClientID %>').value = $('#<%=txtFromDate.ClientID %>').val();
                }
            } 
        }
    </script>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            // HideShowReportView();
            var fullDate = new Date();
            var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
            var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
            $('#txtToDate,#txtFromDate').datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy', maxDate: currentDate
            });


        });

        function setValue1(target) {
                document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
                var rdoDailyTracker = document.getElementById('<%=rdoDailyTracker.ClientID %>');
                if (rdoDailyTracker.checked) {                   
                    CheckDateForDailyTracker();
                }                
            }

            function setValue2(target) {
                document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
            }

            function checkFlag() {
               // debugger;
                var datePanel = document.getElementById('UcPanelDate');
                var yearPanel = document.getElementById('UcPanelYear');
                var rdoMonthlyTracker = document.getElementById('<%=rdoMonthlyTracker.ClientID %>');
                var rdoPlannerReport = document.getElementById('<%=rdoPlannerReport.ClientID %>');

                var rdoPlannerReport = document.getElementById('<%=rdoPlannerReport.ClientID %>');
                var rdoPlannerCountry = document.getElementById('<%=rdoPlannerCountry.ClientID %>');
                var rdoDetailed = document.getElementById('<%=rdoDetailed.ClientID %>');
                var rdoRegionReport = document.getElementById('<%=rdoRegionReport.ClientID %>');
                var rdoVendorReport = document.getElementById('<%=rdoVendorReport.ClientID %>');

                var rdoDailyTracker = document.getElementById('<%=rdoDailyTracker.ClientID %>');
                var UcLabel1 = document.getElementById('<%=UcLabel1.ClientID %>');
                var lblDateRange = document.getElementById('<%=lblDateFrom.ClientID %>');

                var txtToDate = document.getElementById('<%=txtToDate.ClientID %>');
                
                if (rdoPlannerReport.checked) {
                    datePanel.style.display = "block";
                    yearPanel.style.display = "none";
                    lblDateRange.innerText = "Date From";
                    UcLabel1.innerHTML = ":";
                   // lblDateTo.style.display = "none";
                    txtToDate.disabled = false;
                    loadDate(); 
                }
                else if (rdoPlannerCountry.checked) {
                    datePanel.style.display = "block";
                    yearPanel.style.display = "none";
                    lblDateRange.innerText = "Date From";
                    UcLabel1.innerHTML = ":";
                    //lblDateTo.style.display = "none";
                    txtToDate.disabled = false;
                    loadDate(); 
                }
                else if (rdoDetailed.checked) {
                    datePanel.style.display = "none";
                    yearPanel.style.display = "none";
                    UcLabel1.innerHTML = "";
                    lblDateRange.innerText = "";
                   // lblDateTo.style.display = "none";
                    txtToDate.disabled = false;
                          
                }
                else if (rdoRegionReport.checked) {
                    datePanel.style.display = "block";
                    yearPanel.style.display = "none";
                    lblDateRange.innerText = "Date From";
                    UcLabel1.innerHTML = ":";
                   // lblDateTo.style.display = "none";
                    txtToDate.disabled = false;
                    loadDate();       
                }
                else if (rdoVendorReport.checked) {
                    datePanel.style.display = "block";
                    yearPanel.style.display = "none";
                    lblDateRange.innerText = "Date From";
                    UcLabel1.innerHTML = ":";
                   // lblDateTo.style.display = "none";
                    txtToDate.disabled = false;
                    loadDate();       
                }
               
               else if (rdoMonthlyTracker.checked) {
                    yearPanel.style.display = "block";
                    datePanel.style.display = "none";
                    lblDateRange.innerText = "Select Year";
                    UcLabel1.innerHTML = ":";
                   // lblDateTo.style.display = "block";
                    txtToDate.disabled = false;
                }
              else  if (rdoDailyTracker.checked) {
                    yearPanel.style.display = "none";
                    datePanel.style.display = "block";
                    lblDateRange.innerText = "Date From";
                    UcLabel1.innerHTML = ":";
                    // lblDateTo.style.display = "none";
                    txtToDate.disabled = true;
                    CheckDateForDailyTracker();
                    
                }
            }
      
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="right-shadow">
        <%--   <asp:UpdatePanel ID="pnlUP1" runat="server">
    <ContentTemplate>--%>
        <div class="formbox">
            <cc1:ucPanel ID="UcDataPanel" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td colspan="4">
                            <cc1:ucRadioButton ID="rdoPlannerReport" runat="server" Checked="true" GroupName="ReportType"
                                onClick="checkFlag()" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <cc1:ucRadioButton ID="rdoPlannerCountry" runat="server" GroupName="ReportType" onClick="checkFlag()" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <cc1:ucRadioButton ID="rdoDetailed" runat="server" GroupName="ReportType" onClick="checkFlag()" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <cc1:ucRadioButton ID="rdoRegionReport" runat="server" GroupName="ReportType" onClick="checkFlag()" />
                            &nbsp;&nbsp;&nbsp;
                            <cc1:ucRadioButton ID="rdoVendorReport" runat="server" GroupName="ReportType" onClick="checkFlag()" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <cc1:ucRadioButton ID="rdoDailyTracker" runat="server" GroupName="ReportType" onClick="checkFlag()" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <cc1:ucRadioButton ID="rdoMonthlyTracker" runat="server" GroupName="ReportType" onClick="checkFlag()" />
                        </td>
                    </tr>
                    <tr style="height: 3em;">
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <div id="UcPanelDate">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="font-weight: bold; align: left; width: 6em;">
                                            <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                                                ReadOnly="True" Width="70px" />
                                        </td>
                                        <td style="font-weight: bold; width: 4em; text-align: center">
                                            <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                        </td>
                                        <td  style="font-weight: bold; width: 6em;">
                                            <cc1:ucTextbox ID="txtToDate" Enabled="false" ClientIDMode="Static" runat="server" onchange="setValue2(this)"
                                                ReadOnly="True" Width="70px" /> 
                                                                                  
                                        </td>                                        
                                    </tr>
                                </table>
                            </div>
                            <div id="UcPanelYear" style="display: none">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td colspan="2" style="font-weight: bold; width: 6em;">
                                          <asp:DropdownList ID="ddlMonth" runat="server" Width="60px">
                                           <asp:ListItem Text="Jan" Value="1"></asp:ListItem>
                                             <asp:ListItem Text="Feb" Value="2"></asp:ListItem>
                                             <asp:ListItem Text="Mar" Value="3"></asp:ListItem>
                                             <asp:ListItem Text="April" Value="4"></asp:ListItem>
                                             <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                             <asp:ListItem Text="June" Value="6"></asp:ListItem>
                                             <asp:ListItem Text="July" Value="7"></asp:ListItem>
                                             <asp:ListItem Text="Aug" Value="8"></asp:ListItem>
                                             <asp:ListItem Text="Sep" Value="9"></asp:ListItem>
                                             <asp:ListItem Text="Oct" Value="10"></asp:ListItem>
                                             <asp:ListItem Text="Nov" Value="11"></asp:ListItem>
                                             <asp:ListItem Text="Dec" Value="12"></asp:ListItem>
                                            </asp:DropdownList>
                                            </td>
                                            <td colspan="2" style="font-weight: bold; width: 6em;">
                                            <cc1:ucDropdownList ID="ddlYear" runat="server" Width="60px">
                                            </cc1:ucDropdownList>
                                        </td>
                                    </tr>
                                </table>
                                 <script>
                                    checkFlag();
                                </script>
                            </div>
                        </td>                         
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
                                    OnClick="btnGenerateReport_Click" OnClientClick="return WeekendCheck();"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlannerGroupings" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlannerGrouping runat="server" ID="multiSelectStockPlannerGrouping" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblItemClassification" runat="server" Text="Item Classification"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel10" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectItemClassification runat="server" ID="msItemClassification" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnGenerateReport2" runat="server" Text="Generate Report" CssClass="button"
                                    OnClick="btnGenerateReport_Click" OnClientClick="return WeekendCheck();"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <%--  <cc1:ucPanel ID="UcReportPanel" runat="server" >--%>
       
              <div class="button-row" style="width: 98%">
              
               <cc1:ucButton ID="btnExportToExcel" runat="server" Visible="false" CssClass="exporttoexcel" OnClick="btnExport_Click"
         Width="109px" Height="20px" />
           <cc1:ucButton ID="btnExportToExcel_1" runat="server" Visible="false" CssClass="exporttoexcel" OnClick="btnExportSession_Click"
         Width="109px" Height="20px" />
    </div>             
            <%--   <div class="formbox">--%>
            <div id="divUcGridView1" runat="server">
            <div class="wmd-view-topscroll">
                <div class="scroll-div">
                    &nbsp;
                </div>
            </div>
            <div class="wmd-view">
                <div class="dynamic-div">
                    <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" OnSorting="SortGrid" 
                        AllowSorting="true" OnRowDataBound="UcGridView1_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Stock Planner" >
                                <HeaderStyle Width="100%" CssClass="stock-color1"  HorizontalAlign="Center"  />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                <%--NavigateUrl='<%# EncryptQuery("ExpediteTracker.aspx?StockPlannerID="+ Eval("StockPlanner")) %>'--%>
                                    <asp:LinkButton ID="hpStockPlanner" runat="server" Text='<%#Eval("StockPlanner") %>' CommandArgument='<%#Eval("StockPlanner") %>'  OnClick="hpStockPlanner_Click" ></asp:LinkButton>
                                    <asp:Label ID="lblStockPlanner" Visible="false" runat="server" Text='<%#Eval("StockPlanner") %>'></asp:Label>
                                    <asp:Label ID="lblVendor" Visible="false" runat="server" Text='<%#Eval("VendorName") %>'></asp:Label>
                                     <asp:HiddenField ID="hdnCountryID"  runat="server" value='<%#Eval("CountryID") %>'></asp:HiddenField>
                                     <asp:HiddenField ID="hdnSiteID" runat="server" value='<%#Eval("SiteID") %>'></asp:HiddenField>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField>
                                <ItemTemplate>                                
                                        <asp:GridView ID="gvSite" runat="server" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="VendorName" />
                                                <asp:BoundField ItemStyle-Width="10%" DataField="SiteName"  />
                                                <asp:BoundField ItemStyle-Width="10%" DataField="TotalLines" />
                                                <asp:BoundField ItemStyle-Width="10%" DataField="AtRisk"  />
                                                <asp:BoundField ItemStyle-Width="10%" DataField="Comment" />
                                                <asp:BoundField ItemStyle-Width="10%" DataField="Expedited"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="Per_Expedited"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="Unavoidable"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="UnavoidableComment"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="UnavoidableExpedited"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="UnvoidablePer_Expedited"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="Avoidable"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="AvoidableComment"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="AvoidableExpedited"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="AvoidablePer_Expedited"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="BackOrder"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="BackOrderComment"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="BackOrderExpedited"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="BackOrderExpeditedPer"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="SOH"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="SOHComment"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="SOHExpedited"/>
                                                <asp:BoundField ItemStyle-Width="10%" DataField="SOHPer_Expedited"/>
                                            </Columns>
                                        </asp:GridView>                                 
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Grouping">
                                <HeaderStyle Width="30%"  CssClass="stock-color1"  HorizontalAlign="Center" />                              
                                <ItemTemplate>                                
                                    <asp:Label ID="lblGrouping" runat="server" Text='<%#Eval("StockPlannerGrouping") %>'></asp:Label>
                                    <asp:Label ID="lblSite" runat="server" Visible="false" Text='<%#Eval("SiteName") %>'></asp:Label>
                                      <asp:LinkButton ID="hpSite" Visible="false" runat="server"  Text='<%#Eval("SiteName") %>' CommandArgument='<%#Eval("SiteID") + "," + Eval("CountryID")%>'
                                                 OnClick="hpSite_Click" ></asp:LinkButton>
                                   <%-- <asp:HyperLink ID="hpSite" Visible="false" runat="server" Text='<%#Eval("SiteName") %>'
                                        NavigateUrl='<%# EncryptQuery("ExpediteTracker.aspx?SiteID="+ Eval("SiteID") +"&CountryID="+Eval("CountryID")) %>'></asp:HyperLink>--%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Line">
                                <HeaderStyle Width="10%" CssClass="stock-color1"   HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalLine" runat="server" Text=' <%# Eval("TotalLines") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="At Risk">
                                <HeaderStyle Width="5%" CssClass="stock-color" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAtRisk" runat="server" Text='<%#Eval("AtRisk") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comment Added">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Center" />
                                 <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblCommentAdded" runat="server" Text='<%#Eval("Comment") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblExpedited" runat="server" Text='<%#Eval("Expedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="%Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblExpeditedper" runat="server" Text='<%#Eval("Per_Expedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unavoidable Stock Out">
                                <HeaderStyle Width="10%" CssClass="stock-color1"   HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblUnavoidableStockOut" runat="server" Text='<%#Eval("Unavoidable") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comment Added">
                                <HeaderStyle Width="10%" CssClass="stock-color1"  HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblUnavoidableCommentAdded" runat="server" Text='<%#Eval("UnavoidableComment") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color1"  HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblUnavoidableExpedited" runat="server" Text='<%#Eval("UnavoidableExpedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="%Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color1"  HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblUnavoidableExpeditedper" runat="server" Text='<%#Eval("UnvoidablePer_Expedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Avoidable Stock Out">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAvoidableStockOut" runat="server" Text='<%#Eval("Avoidable") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comment Added">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAvoidableCommentAdded" runat="server" Text='<%#Eval("AvoidableComment") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAvoidableExpedited" runat="server" Text='<%#Eval("AvoidableExpedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="%Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAvoidableExpeditedper" runat="server" Text='<%#Eval("AvoidablePer_Expedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="On Backorder">
                                <HeaderStyle Width="10%" CssClass="stock-color1"   HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblBackorderStockOut" runat="server" Text='<%#Eval("BackOrder") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comment Added">
                                <HeaderStyle Width="10%" CssClass="stock-color1"  HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblBackorderCommentAdded" runat="server" Text='<%#Eval("BackOrderComment") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color1"  HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblBackorderExpedited" runat="server" Text='<%#Eval("BackOrderExpedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="%Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color1"  HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblBackorderExpeditedper" runat="server" Text='<%#Eval("BackOrderExpeditedPer") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--  <asp:TemplateField HeaderText="On Backorder">
                                    <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAvoidableBackorder" runat="server" Text='<%#Eval("BackOrder_Avoidable") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CommentAdded">
                                    <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAvoidableBackorderComment" runat="server" Text='<%#Eval("BackOrderComment_Avoidable") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expedited">
                                    <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAvoidableBackorderExpedited" runat="server" Text='<%#Eval("BackOrderExpedited_Avoidable") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="% Chased">
                                    <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAvoidableBackorderExpeditedper" runat="server" Text='<%#Eval("BackOrderPer_Avoidable") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="<=0 SOH">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSOHStockOut" runat="server" Text='<%#Eval("SOH") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comment Added">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSOHCommentAdded" runat="server" Text='<%#Eval("SOHComment") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSOHExpedited" runat="server" Text='<%#Eval("SOHExpedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="%Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSOHExpeditedper" runat="server" Text='<%#Eval("SOHPer_Expedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </cc1:ucGridView>
             </div>
                 </div>
            </div>
                                

            <cc1:ucGridView ID="UcGridViewExport" Width="100%" runat="server" CssClass="grid" Visible="false"
                        AllowSorting="true" OnRowDataBound="UcGridView1_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Stock Planner" >
                                <HeaderStyle Width="100%" CssClass="stock-color"  HorizontalAlign="Left"  />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>                               
                                    <asp:LinkButton ID="hpStockPlanner" runat="server" Text='<%#Eval("StockPlanner") %>' ></asp:LinkButton>
                                    <asp:Label ID="lblStockPlanner" Visible="false" runat="server" Text='<%#Eval("StockPlanner") %>'></asp:Label>
                                    <asp:Label ID="lblVendor" Visible="false" runat="server" Text='<%#Eval("VendorName") %>'></asp:Label>
                                     <asp:HiddenField ID="hdnCountryID"  runat="server" value='<%#Eval("CountryID") %>'></asp:HiddenField>
                                     <asp:HiddenField ID="hdnSiteID" runat="server" value='<%#Eval("SiteID") %>'></asp:HiddenField>
                                </ItemTemplate>
                            </asp:TemplateField>                
                            <asp:TemplateField HeaderText="Grouping">
                                <HeaderStyle Width="30%" CssClass="stock-color" HorizontalAlign="Left" />                              
                                <ItemTemplate>                                
                                    <asp:Label ID="lblGrouping" runat="server" Text='<%#Eval("StockPlannerGrouping") %>'></asp:Label>
                                    <asp:Label ID="lblSite" runat="server" Visible="false" Text='<%#Eval("SiteName") %>'></asp:Label>
                                    <asp:LinkButton ID="hpSite" Visible="false" runat="server"  Text='<%#Eval("SiteName") %>'></asp:LinkButton>                                  
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Line">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalLine" runat="server" Text=' <%# Eval("TotalLines") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="At Risk">
                                <HeaderStyle Width="5%" CssClass="stock-color" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAtRisk" runat="server" Text='<%#Eval("AtRisk") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comment Added">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Left" />
                                 <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblCommentAdded" runat="server" Text='<%#Eval("Comment") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblExpedited" runat="server" Text='<%#Eval("Expedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="%Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblExpeditedper" runat="server" Text='<%#Eval("Per_Expedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unavoidable Stock Out">
                                <HeaderStyle Width="10%"  HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblUnavoidableStockOut" runat="server" Text='<%#Eval("Unavoidable") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comment Added">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblUnavoidableCommentAdded" runat="server" Text='<%#Eval("UnavoidableComment") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Expedited">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblUnavoidableExpedited" runat="server" Text='<%#Eval("UnavoidableExpedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="%Expedited">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblUnavoidableExpeditedper" runat="server" Text='<%#Eval("UnvoidablePer_Expedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Avoidable Stock Out">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAvoidableStockOut" runat="server" Text='<%#Eval("Avoidable") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comment Added">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAvoidableCommentAdded" runat="server" Text='<%#Eval("AvoidableComment") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAvoidableExpedited" runat="server" Text='<%#Eval("AvoidableExpedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="%Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAvoidableExpeditedper" runat="server" Text='<%#Eval("AvoidablePer_Expedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="On Backorder">
                                <HeaderStyle Width="10%"  HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblBackorderStockOut" runat="server" Text='<%#Eval("BackOrder") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comment Added">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblBackorderCommentAdded" runat="server" Text='<%#Eval("BackOrderComment") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Expedited">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblBackorderExpedited" runat="server" Text='<%#Eval("BackOrderExpedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="%Expedited">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblBackorderExpeditedper" runat="server" Text='<%#Eval("BackOrderExpeditedPer") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                        
                            <asp:TemplateField HeaderText="<=0 SOH">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSOHStockOut" runat="server" Text='<%#Eval("SOH") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comment Added">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSOHCommentAdded" runat="server" Text='<%#Eval("SOHComment") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSOHExpedited" runat="server" Text='<%#Eval("SOHExpedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="%Expedited">
                                <HeaderStyle Width="10%" CssClass="stock-color" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSOHExpeditedper" runat="server" Text='<%#Eval("SOHPer_Expedited") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </cc1:ucGridView>

            <div id="divMonthlyTracker" runat="server" visible="false">
            <div class="wmd-view-topscroll">
                <div class="scroll-div">
                    &nbsp;
                </div>
            </div>            
            <div class="wmd-view">
                <div class="dynamic-div">
                      <cc1:ucGridView ID="gdMonthlyTracker" Width="100%" runat="server" CssClass="grid" Visible="false">
                     <Columns>
                               <asp:TemplateField>
                                  <HeaderTemplate >                                                     
                                    <th colspan="4"><%=Month1%></th>
                                    <th colspan="4"><%=Month2%></th>
                                    <th colspan="4"><%=Month3%></th>
                                    <th colspan="4"><%=Month4%></th>
                                    <th colspan="4"><%=Month5%></th>
                                    <th colspan="4"><%=Month6%></th>
                                    <th colspan="4"><%=Month7%></th>
                                    <th colspan="4"><%=Month8%></th>
                                    <th colspan="4"><%=Month9%></th>
                                    <th colspan="4"><%=Month10%></th>
                                    <th colspan="4"><%=Month11%></th>
                                    <th colspan="4"><%=Month12%></th>
                                    <tr>
                                     <%-- <th>&nbsp;</th>--%>
                                      <th class="stock-color" align="Center">Stock Planner</th>

                                      <th class="stock-color1" align="Center">Items</th>
                                      <th class="stock-color1" align="Center">At Risk</th>
                                      <th class="stock-color1" align="Center">Expdtd</th>
                                      <th class="stock-color1" align="Center">%Exp</th>

                                      <th class="stock-color" align="Center">Items</th>
                                      <th class="stock-color" align="Center">At Risk</th>
                                      <th class="stock-color" align="Center">Expdtd</th>
                                      <th class="stock-color" align="Center">%Exp</th>

                                      <th class="stock-color1" align="Center">Items</th>
                                      <th class="stock-color1" align="Center">At Risk</th>
                                      <th class="stock-color1" align="Center">Expdtd</th>
                                      <th class="stock-color1" align="Center">%Exp</th>

                                      <th class="stock-color" align="Center">Items</th>
                                      <th class="stock-color" align="Center">At Risk</th>
                                      <th class="stock-color" align="Center">Expdtd</th>
                                      <th class="stock-color" align="Center">%Exp</th>

                                      <th class="stock-color1" align="Center">Items</th>
                                      <th class="stock-color1" align="Center">At Risk</th>
                                      <th class="stock-color1" align="Center">Expdtd</th>
                                      <th class="stock-color1" align="Center">%Exp</th>

                                      <th class="stock-color" align="Center">Items</th>
                                      <th class="stock-color" align="Center">At Risk</th>
                                      <th class="stock-color" align="Center">Expdtd</th>
                                      <th class="stock-color" align="Center">%Exp</th>

                                      <th class="stock-color1" align="Center">Items</th>
                                      <th class="stock-color1" align="Center">At Risk</th>
                                      <th class="stock-color1" align="Center">Expdtd</th>
                                      <th class="stock-color1" align="Center">% Exp</th>

                                      <th class="stock-color" align="Center">Items</th>
                                      <th class="stock-color" align="Center">At Risk</th>
                                      <th class="stock-color" align="Center">Expdtd</th>
                                      <th class="stock-color" align="Center">%Exp</th>

                                      <th class="stock-color1" align="Center">Items</th>
                                      <th class="stock-color1" align="Center">At Risk</th>
                                      <th class="stock-color1" align="Center">Expdtd</th>
                                      <th class="stock-color1" align="Center">%Exp</th>

                                      <th class="stock-color" align="Center">Items</th>
                                      <th class="stock-color" align="Center">At Risk</th>
                                      <th class="stock-color" align="Center">Expdtd</th>
                                      <th class="stock-color" align="Center">%Exp</th>

                                      <th class="stock-color1" align="Center">Items</th>
                                      <th class="stock-color1" align="Center">At Risk</th>
                                      <th class="stock-color1" align="Center">Expdtd</th>
                                      <th class="stock-color1" align="Center">%Exp</th>

                                      <th class="stock-color" align="Center">Items</th>
                                      <th class="stock-color" align="Center">At Risk</th>
                                      <th class="stock-color" align="Center">Expdtd</th>
                                      <th class="stock-color" align="Center">%Exp</th>
                                    </tr>
                                  </HeaderTemplate> 
                                  <ItemTemplate>
                                    <%# Eval("StockPlanner")%>
                                    <td align="center"><%# Eval("Month1_Items")%></td>
                                    <td align="center"><%# Eval("Month1_AtRisk")%></td>
                                    <td align="center"><%# Eval("Month1_Expdtd")%></td>
                                    <td align="center"><%# Eval("Month1_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Month2_Items")%></td>
                                    <td align="center"><%# Eval("Month2_AtRisk")%></td>
                                    <td align="center"><%# Eval("Month2_Expdtd")%></td>
                                    <td align="center"><%# Eval("Month2_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Month3_Items")%></td>
                                    <td align="center"><%# Eval("Month3_AtRisk")%></td>
                                    <td align="center"><%# Eval("Month3_Expdtd")%></td>
                                    <td align="center"><%# Eval("Month3_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Month4_Items")%></td>
                                    <td align="center"><%# Eval("Month4_AtRisk")%></td>
                                    <td align="center"><%# Eval("Month4_Expdtd")%></td>
                                    <td align="center"><%# Eval("Month4_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Month5_Items")%></td>
                                    <td align="center"><%# Eval("Month5_AtRisk")%></td>
                                    <td align="center"><%# Eval("Month5_Expdtd")%></td>
                                    <td align="center"><%# Eval("Month5_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Month6_Items")%></td>
                                    <td align="center"><%# Eval("Month6_AtRisk")%></td>
                                    <td align="center"><%# Eval("Month6_Expdtd")%></td>
                                    <td align="center"><%# Eval("Month6_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Month7_Items")%></td>
                                    <td align="center"><%# Eval("Month7_AtRisk")%></td>
                                    <td align="center"><%# Eval("Month7_Expdtd")%></td>
                                    <td align="center"><%# Eval("Month7_PercentExpdtd")%></td>
                                      
                                    <td align="center"><%# Eval("Month8_Items")%></td>
                                    <td align="center"><%# Eval("Month8_AtRisk")%></td>
                                    <td align="center"><%# Eval("Month8_Expdtd")%></td>
                                    <td align="center"><%# Eval("Month8_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Month9_Items")%></td>
                                    <td align="center"><%# Eval("Month9_AtRisk")%></td>
                                    <td align="center"><%# Eval("Month9_Expdtd")%></td>
                                    <td align="center"><%# Eval("Month9_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Month10_Items")%></td>
                                    <td align="center"><%# Eval("Month10_AtRisk")%></td>
                                    <td align="center"><%# Eval("Month10_Expdtd")%></td>
                                    <td align="center"><%# Eval("Month10_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Month11_Items")%></td>
                                    <td align="center"><%# Eval("Month11_AtRisk")%></td>
                                    <td align="center"><%# Eval("Month11_Expdtd")%></td>
                                    <td align="center"><%# Eval("Month11_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Month12_Items")%></td>
                                    <td align="center"><%# Eval("Month12_AtRisk")%></td>
                                    <td align="center"><%# Eval("Month12_Expdtd")%></td>
                                    <td align="center"><%# Eval("Month12_PercentExpdtd")%></td>                                      
                                  </ItemTemplate>                                 
                                </asp:TemplateField>
                            </Columns>                     
                    </cc1:ucGridView>
                  </div>
                  </div>  
                        </div>
                     <%--<cc1:ucGridView ID="UcMonthlyTrackerExport" Width="100%" runat="server" CssClass="grid" Visible="false">
                     <Columns>
                               <asp:TemplateField>
                                  <HeaderTemplate >                                                     
                                    <th colspan="4"><%=Month1%></th>
                                    <th colspan="4"><%=Month2%></th>
                                    <th colspan="4"><%=Month3%></th>
                                    <th colspan="4"><%=Month4%></th>
                                    <th colspan="4"><%=Month5%></th>
                                    <th colspan="4"><%=Month6%></th>
                                    <th colspan="4"><%=Month7%></th>
                                    <th colspan="4"><%=Month8%></th>
                                    <th colspan="4"><%=Month9%></th>
                                    <th colspan="4"><%=Month10%></th>
                                    <th colspan="4"><%=Month11%></th>
                                    <th colspan="4"><%=Month12%></th>
                                    <tr>
                                     <%-- <th>&nbsp;</th>
                                      <th>UserName</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>
                                    </tr>
                                  </HeaderTemplate> 
                                  <ItemTemplate>
                                    <%# Eval("StockPlanner")%>
                                    <td><%# Eval("Month1_Items")%></td>
                                    <td><%# Eval("Month1_AtRisk")%></td>
                                    <td><%# Eval("Month1_Expdtd")%></td>
                                    <td><%# Eval("Month1_PercentExpdtd")%></td>

                                    <td><%# Eval("Month2_Items")%></td>
                                    <td><%# Eval("Month2_AtRisk")%></td>
                                    <td><%# Eval("Month2_Expdtd")%></td>
                                    <td><%# Eval("Month2_PercentExpdtd")%></td>

                                    <td><%# Eval("Month3_Items")%></td>
                                    <td><%# Eval("Month3_AtRisk")%></td>
                                    <td><%# Eval("Month3_Expdtd")%></td>
                                    <td><%# Eval("Month3_PercentExpdtd")%></td>

                                    <td><%# Eval("Month4_Items")%></td>
                                    <td><%# Eval("Month4_AtRisk")%></td>
                                    <td><%# Eval("Month4_Expdtd")%></td>
                                    <td><%# Eval("Month4_PercentExpdtd")%></td>

                                    <td><%# Eval("Month5_Items")%></td>
                                    <td><%# Eval("Month5_AtRisk")%></td>
                                    <td><%# Eval("Month5_Expdtd")%></td>
                                    <td><%# Eval("Month5_PercentExpdtd")%></td>

                                    <td><%# Eval("Month6_Items")%></td>
                                    <td><%# Eval("Month6_AtRisk")%></td>
                                    <td><%# Eval("Month6_Expdtd")%></td>
                                    <td><%# Eval("Month6_PercentExpdtd")%></td>

                                    <td><%# Eval("Month7_Items")%></td>
                                    <td><%# Eval("Month7_AtRisk")%></td>
                                    <td><%# Eval("Month7_Expdtd")%></td>
                                    <td><%# Eval("Month7_PercentExpdtd")%></td>
                                      
                                    <td><%# Eval("Month8_Items")%></td>
                                    <td><%# Eval("Month8_AtRisk")%></td>
                                    <td><%# Eval("Month8_Expdtd")%></td>
                                    <td><%# Eval("Month8_PercentExpdtd")%></td>

                                    <td><%# Eval("Month9_Items")%></td>
                                    <td><%# Eval("Month9_AtRisk")%></td>
                                    <td><%# Eval("Month9_Expdtd")%></td>
                                    <td><%# Eval("Month9_PercentExpdtd")%></td>

                                    <td><%# Eval("Month10_Items")%></td>
                                    <td><%# Eval("Month10_AtRisk")%></td>
                                    <td><%# Eval("Month10_Expdtd")%></td>
                                    <td><%# Eval("Month10_PercentExpdtd")%></td>

                                    <td><%# Eval("Month11_Items")%></td>
                                    <td><%# Eval("Month11_AtRisk")%></td>
                                    <td><%# Eval("Month11_Expdtd")%></td>
                                    <td><%# Eval("Month11_PercentExpdtd")%></td>

                                    <td><%# Eval("Month12_Items")%></td>
                                    <td><%# Eval("Month12_AtRisk")%></td>
                                    <td><%# Eval("Month12_Expdtd")%></td>
                                    <td><%# Eval("Month12_PercentExpdtd")%></td>                                      
                                  </ItemTemplate>                                 
                                </asp:TemplateField>
                            </Columns>                     
                    </cc1:ucGridView>--%>
                 
            <div id="divDailyTracker" runat="server" visible="false">
            <div class="wmd-view-topscroll">
                <div class="scroll-div">
                    &nbsp;
                </div>
            </div>
            <div class="wmd-view">
                <div class="dynamic-div">               
                      <cc1:ucGridView ID="grdDailyTracker" Width="100%" runat="server" 
                          CssClass="grid" Visible="false"
                        AllowSorting="true" onrowdatabound="grdDailyTracker_RowDataBound" >
                     <Columns>
                               <asp:TemplateField>
                                  <HeaderTemplate >
                                   <%-- <th colspan="4"><asp:Label ID="day1" runat="server"   Text='<%# Eval("Day1_Date")%>'></asp:Label></th>
                                    <th colspan="4"><asp:Label ID="Label1" runat="server" Text='<%# Eval("Day2_Date")%>'></asp:Label></th>
                                    <th colspan="4"><asp:Label ID="Label2" runat="server" Text='<%# Eval("Day3_Date")%>'></asp:Label></th>
                                    <th colspan="4"><asp:Label ID="Label3" runat="server" Text='<%# Eval("Day4_Date")%>'></asp:Label></th>
                                    <th colspan="4"><asp:Label ID="Label4" runat="server" Text='<%# Eval("Day5_Date")%>'></asp:Label></th>
                                    <th colspan="4"><asp:Label ID="Label5" runat="server" Text='<%# Eval("Day6_Date")%>'></asp:Label></th>
                                    <th colspan="4"><asp:Label ID="Label6" runat="server" Text='<%# Eval("Day7_Date")%>'></asp:Label></th>       --%>     
                                    <th colspan="4"><%=Day1%></th>
                                    <th colspan="4"><%=Day2%></th>
                                    <th colspan="4"><%=Day3%></th>
                                    <th colspan="4"><%=Day4%></th>
                                    <th colspan="4"><%=Day5%></th>
                                    <th colspan="4"><%=Day6%></th>
                                    <th colspan="4"><%=Day7%></th>                          
                                    <tr>
                                     <%-- <th>&nbsp;</th>--%>
                                      <th class="stock-color" align="left">Stock Planner</th>

                                      <th class="stock-color1" align="left">Items</th>
                                      <th class="stock-color1" align="left">At Risk</th>
                                      <th class="stock-color1" align="left">Expdtd</th>
                                      <th class="stock-color1" align="left">%Exp</th>

                                      <th class="stock-color" align="left">Items</th>
                                      <th class="stock-color" align="left">At Risk</th>
                                      <th class="stock-color" align="left">Expdtd</th>
                                      <th class="stock-color" align="left">%Exp</th>

                                      <th class="stock-color1" align="left">Items</th>
                                      <th class="stock-color1" align="left">At Risk</th>
                                      <th class="stock-color1" align="left">Expdtd</th>
                                      <th class="stock-color1" align="left">%Exp</th>

                                      <th class="stock-color" align="left">Items</th>
                                      <th class="stock-color" align="left">At Risk</th>
                                      <th class="stock-color" align="left">Expdtd</th>
                                      <th class="stock-color" align="left">%Exp</th>

                                      <th class="stock-color1" align="left">Items</th>
                                      <th class="stock-color1" align="left">At Risk</th>
                                      <th class="stock-color1" align="left">Expdtd</th>
                                      <th class="stock-color1" align="left">%Exp</th>

                                      <th class="stock-color" align="left">Items</th>
                                      <th class="stock-color" align="left">At Risk</th>
                                      <th class="stock-color" align="left">Expdtd</th>
                                      <th class="stock-color" align="left">%Exp</th>

                                      <th class="stock-color1" align="left">Items</th>
                                      <th class="stock-color1" align="left">At Risk</th>
                                      <th class="stock-color1" align="left">Expdtd</th>
                                      <th class="stock-color1" align="left">%Exp</th>                                   
                                    </tr>
                                  </HeaderTemplate> 
                                  <ItemTemplate>
                                    <%# Eval("StockPlanner")%>
                                    <td align="center"><%# Eval("Day1_Items")%></td>
                                    <td align="center"><%# Eval("Day1_AtRisk")%></td>
                                    <td align="center"><%# Eval("Day1_Expdtd")%></td>
                                    <td align="center"><%# Eval("Day1_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Day2_Items")%></td>
                                    <td align="center"><%# Eval("Day2_AtRisk")%></td>
                                    <td align="center"><%# Eval("Day2_Expdtd")%></td>
                                    <td align="center"><%# Eval("Day2_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Day3_Items")%></td>
                                    <td align="center"><%# Eval("Day3_AtRisk")%></td>
                                    <td align="center"><%# Eval("Day3_Expdtd")%></td>
                                    <td align="center"><%# Eval("Day3_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Day4_Items")%></td>
                                    <td align="center"><%# Eval("Day4_AtRisk")%></td>
                                    <td align="center"><%# Eval("Day4_Expdtd")%></td>
                                    <td align="center"><%# Eval("Day4_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Day5_Items")%></td>
                                    <td align="center"><%# Eval("Day5_AtRisk")%></td>
                                    <td align="center"><%# Eval("Day5_Expdtd")%></td>
                                    <td align="center"><%# Eval("Day5_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Day6_Items")%></td>
                                    <td align="center"><%# Eval("Day6_AtRisk")%></td>
                                    <td align="center"><%# Eval("Day6_Expdtd")%></td>
                                    <td align="center"><%# Eval("Day6_PercentExpdtd")%></td>

                                    <td align="center"><%# Eval("Day7_Items")%></td>
                                    <td align="center"><%# Eval("Day7_AtRisk")%></td>
                                    <td align="center"><%# Eval("Day7_Expdtd")%></td>
                                    <td align="center"><%# Eval("Day7_PercentExpdtd")%></td>                                     
                                                                         
                                  </ItemTemplate>                                 
                                </asp:TemplateField>
                            </Columns>                     
                   </cc1:ucGridView>
                   </div>
                  </div> 
                    </div>
                        <%--<cc1:ucGridView ID="UcDailyTrackerReportExport" Width="100%" runat="server" CssClass="grid" Visible="false"
                        AllowSorting="true" >
                     <Columns>
                               <asp:TemplateField>
                                  <HeaderTemplate >                            
                                    <th colspan="4"><%=Day1%></th>
                                    <th colspan="4"><%=Day2%></th>
                                    <th colspan="4"><%=Day3%></th>
                                    <th colspan="4"><%=Day4%></th>
                                    <th colspan="4"><%=Day5%></th>
                                    <th colspan="4"><%=Day6%></th>
                                    <th colspan="4"><%=Day7%></th>    
                                                      
                                    <tr>
                                     <%-- <th>&nbsp;</th>
                                      <th>UserName</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>

                                      <th>Items</th>
                                      <th>At Risk</th>
                                      <th>Expdtd</th>
                                      <th>% Exp</th>                                   
                                    </tr>
                                  </HeaderTemplate> 
                                  <ItemTemplate>
                                    <%# Eval("StockPlanner")%>
                                    <td><%# Eval("Day1_Items")%></td>
                                    <td><%# Eval("Day1_AtRisk")%></td>
                                    <td><%# Eval("Day1_Expdtd")%></td>
                                    <td><%# Eval("Day1_PercentExpdtd")%></td>

                                    <td><%# Eval("Day2_Items")%></td>
                                    <td><%# Eval("Day2_AtRisk")%></td>
                                    <td><%# Eval("Day2_Expdtd")%></td>
                                    <td><%# Eval("Day2_PercentExpdtd")%></td>

                                    <td><%# Eval("Day3_Items")%></td>
                                    <td><%# Eval("Day3_AtRisk")%></td>
                                    <td><%# Eval("Day3_Expdtd")%></td>
                                    <td><%# Eval("Day3_PercentExpdtd")%></td>

                                    <td><%# Eval("Day4_Items")%></td>
                                    <td><%# Eval("Day4_AtRisk")%></td>
                                    <td><%# Eval("Day4_Expdtd")%></td>
                                    <td><%# Eval("Day4_PercentExpdtd")%></td>

                                    <td><%# Eval("Day5_Items")%></td>
                                    <td><%# Eval("Day5_AtRisk")%></td>
                                    <td><%# Eval("Day5_Expdtd")%></td>
                                    <td><%# Eval("Day5_PercentExpdtd")%></td>

                                    <td><%# Eval("Day6_Items")%></td>
                                    <td><%# Eval("Day6_AtRisk")%></td>
                                    <td><%# Eval("Day6_Expdtd")%></td>
                                    <td><%# Eval("Day6_PercentExpdtd")%></td>

                                    <td><%# Eval("Day7_Items")%></td>
                                    <td><%# Eval("Day7_AtRisk")%></td>
                                    <td><%# Eval("Day7_Expdtd")%></td>
                                    <td><%# Eval("Day7_PercentExpdtd")%></td>                                     
                                                                         
                                  </ItemTemplate>                                 
                                </asp:TemplateField>
                            </Columns>                     
                   </cc1:ucGridView>--%>
                        
              <cc1:ucLabel ID="lblRecordNotFound"  runat="server" Visible="false"></cc1:ucLabel>        
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     
             <div class="button-row">   
            <cc1:ucButton ID="btnBack" runat="server" Text="Back" align="right" Visible="false" CssClass="button"
                OnClick="btnBack_Click"/>         
             </div>  
            <div class="button-row">
                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false">
                    </cc1:PagerV2_8>
                </div>
            <div class="button-row">
                    <cc1:PagerV2_8 ID="pager2" runat="server" OnCommand="pager_Commandsession" GenerateGoToSection="false">
                    </cc1:PagerV2_8>
                </div>
            
            <%-- </div>--%>
        </div>
        <%--  </cc1:ucPanel>--%>

    <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
    </div>
</asp:Content>

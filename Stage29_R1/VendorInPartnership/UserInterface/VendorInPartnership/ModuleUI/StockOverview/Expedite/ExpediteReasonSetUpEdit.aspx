﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ExpediteReasonSetUpEdit.aspx.cs" Inherits="ExpediteReasonSetUpEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
    <h2>
        <cc1:ucLabel ID="lblCreateNewReasonCode" runat="server" Text="Create New Reason Code"></cc1:ucLabel>
    </h2>
    <div>
            <asp:RequiredFieldValidator ID="rfvExpediteReasonSet" ErrorMessage="Please enter Reason." runat="server" ControlToValidate="txtReason"
                Display="None" ValidationGroup="a"> </asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                Style="color: Red" ValidationGroup="a" />
        </div>
    <div class="right-shadow">        
        <div class="formbox">
            <table width="60%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 5%">
                    </td>
                    <td style="font-weight: bold; width: 35%">
                        <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 5%" align="center">
                        :
                    </td>
                    <td style="font-weight: bold; width: 60%">
                        <cc2:ucCountry ID="ucCountry" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblReason" runat="server" Text="Reason" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold" align="center">
                        :
                    </td>
                    <td>
                        <cc1:ucTextbox ID="txtReason" runat="server" Width="220px" MaxLength="100"></cc1:ucTextbox>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblIsCommentRequired" runat="server" Text="Is a comment required" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold" align="center">
                        :
                    </td>
                    <td class="radiobuttonlist">
                        <cc1:ucRadioButtonList ID="rblUpdate" runat="server" RepeatColumns="4" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True">Yes</asp:ListItem>
                            <asp:ListItem>No</asp:ListItem>
                        </cc1:ucRadioButtonList>
                    </td>
                </tr>
                 <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblIsActive" runat="server" Text="Is Active" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold" align="center">
                        :
                    </td>
                    <td >
                       <cc1:ucCheckbox ID="chkIsactive" runat="server" Checked="true" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" ValidationGroup="a" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
            />        
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ExpediteComments.aspx.cs" Inherits="ExpediteComments" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">

        $('<%=btnErrorMsgOK.ClientID%>').click(function () {
            $find("ErrorMsg").hide();
        });
        function setValue1(target) {

            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
            var InputDate = target.value;
            var fullDate = new Date();
            var twoDigitMonth = fullDate.getMonth() + 1 + "";
            if (twoDigitMonth.length == 1)
                twoDigitMonth = "0" + twoDigitMonth;
            var twoDigitDate = fullDate.getDate() + "";
            if (twoDigitDate.length == 1)
                twoDigitDate = "0" + twoDigitDate;
            var currentDate = twoDigitDate + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

            var one_day = 1000 * 60 * 60 * 24;
            var x = InputDate.split("/");
            var y = currentDate.split("/");
            var date1 = new Date(x[2], (x[1] - 1), x[0]);
            var date2 = new Date(y[2], (y[1] - 1), y[0]);
            var _Diff = Math.ceil((date1.getTime() - date2.getTime()) / (one_day));
            if (_Diff > 21) {
                $find("ErrorMsg").show();
                document.getElementById('<%=txtFromDate.ClientID %>').value = '';
            }
            else {
                $find("ErrorMsg").hide();
            }

        }

        function UpdateExpedite(vikingcode, siteidcode) {
            //debugger;
            var vikingcode = vikingcode.trim();
            var vCode = vikingcode.split(",");
            var siteid = siteidcode.split(",");
            var grid = window.opener.document.getElementById("ctl00_ContentPlaceHolder1_gvPotentialOutput");
            if (grid != null && grid != undefined) {
                oRows = grid.rows;
                for (var j = 0; j < vCode.length; j++) {
                    for (var i = 1; i < oRows.length; i++) {
                        var abc = grid.rows[i].cells[5].innerHTML;
                        var iRowIndex = i;
                        if (parseInt(iRowIndex) < 10) {
                            iRowIndex = parseInt(iRowIndex) + 1;
                            iRowIndex = "0" + iRowIndex;
                        }
                        if (window.opener.document.getElementById("ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_hdnSiteId") != null) {
                            var hdnSiteID = window.opener.document.getElementById("ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_hdnSiteId").value;
                        }
                        //var hdnSiteID = window.opener.document.getElementById("ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_hdnSiteId").value;
                        if (abc.indexOf(vCode[j]) > 0 && siteid[j] == hdnSiteID) {
                            if (grid.rows[i].cells[1].style.backgroundColor != "LightGreen") {
                                grid.rows[i].cells[1].style.backgroundColor = "yellow";
                            }
                        }
                        // browser compatibility code                  
                        //  grid.rows[i].cells[0].childNodes[1].checked = false;
                        //                    debugger;
                        //                    var test = $(grid.rows[i].cells[0]).children('td').eq(1);
                        //                    alert(test.id);
                        //                    test.removeAttr('checked');
                        // grid.rows[i].cells[0].firstChild.checked = false;
                        //
                        window.opener.hdnSKUIDs.length = 0;
                        window.opener.hdnVendorIds.length = 0;
                        window.opener.hdnDirectCodes.length = 0;
                        window.opener.hdnOdCodes.length = 0;
                        window.opener.hdnStatuss.length = 0;
                        window.opener.hdnDiscs.length = 0;
                        window.opener.hdnSiteIds.length = 0;
                        window.opener.hdnCountryIDs.length = 0;
                        window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnSKUID").value = "";
                        window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnVendorId").value = "";
                        window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnDirectCode").value = "";
                        window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnOdCode").value = "";
                        window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnStatus").value = "";
                        window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnDisc").value = "";
                        window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnSiteId").value = "";
                        window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnCountryID").value = "";
                    }
                }
                //  debugger;
                var id = window.opener.document.getElementById("ctl00_ContentPlaceHolder1_gvPotentialOutput");
                $(id).find('input:checkbox[id$="chkSelect"]').each(function () {
                    if ($(this).is(':checked')) {
                        this.checked = false;
                    }
                });
            }
            else {
                var grid = window.opener.document.getElementById("ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise");
                if (grid != null && grid != undefined) {
                    oRows = grid.rows;
                    for (var j = 0; j < vCode.length; j++) {
                        for (var i = 1; i < oRows.length; i++) {
                            var abc = grid.rows[i].cells[5].innerHTML;
                            var iRowIndex = i;
                            if (parseInt(iRowIndex) < 10) {
                                iRowIndex = parseInt(iRowIndex) + 1;
                                iRowIndex = "0" + iRowIndex;
                            }
                            if (window.opener.document.getElementById("ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise_ctl" + iRowIndex + "_hdnSiteId") != null) {
                                var hdnSiteID = window.opener.document.getElementById("ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise_ctl" + iRowIndex + "_hdnSiteId").value;
                            }
                            //var hdnSiteID = window.opener.document.getElementById("ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_hdnSiteId").value;
                            if (abc.indexOf(vCode[j]) > 0 && siteid[j] == hdnSiteID) {
                                if (grid.rows[i].cells[1].style.backgroundColor != "LightGreen") {
                                    grid.rows[i].cells[1].style.backgroundColor = "yellow";
                                }
                            }
                            // browser compatibility code                  
                            //  grid.rows[i].cells[0].childNodes[1].checked = false;
                            //                    debugger;
                            //                    var test = $(grid.rows[i].cells[0]).children('td').eq(1);
                            //                    alert(test.id);
                            //                    test.removeAttr('checked');
                            // grid.rows[i].cells[0].firstChild.checked = false;
                            //
                            window.opener.hdnSKUIDs.length = 0;
                            window.opener.hdnVendorIds.length = 0;
                            window.opener.hdnDirectCodes.length = 0;
                            window.opener.hdnOdCodes.length = 0;
                            window.opener.hdnStatuss.length = 0;
                            window.opener.hdnDiscs.length = 0;
                            window.opener.hdnSiteIds.length = 0;
                            window.opener.hdnCountryIDs.length = 0;
                            window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnSKUID").value = "";
                            window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnVendorId").value = "";
                            window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnDirectCode").value = "";
                            window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnOdCode").value = "";
                            window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnStatus").value = "";
                            window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnDisc").value = "";
                            window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnSiteId").value = "";
                            window.opener.document.getElementById("ctl00_ContentPlaceHolder1_hdnCountryID").value = "";
                        }
                    }
                    //  debugger;
                    // var id = window.opener.document.getElementById("ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise");
                    //$(id).find('input:checkbox[id$="chkSelect"]').each(function () {
                    //    if ($(this).is(':checked')) {
                    //        this.checked = false;
                    //    }
                    //});
                }
            }
            window.open('', '_self', ''); window.close();
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblExpediteComment" Text="Expedite Comment" runat="server"></cc1:ucLabel>
    </h2>
    <%--  <div class="right-shadow">--%>
    <%--<div class="formbox">--%>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnCommentID" runat="server" />
    <cc1:ucPanel ID="pnlLatestUpdate" runat="server" CssClass="fieldset-form">
        <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
            <tr>
                <td colspan="3">
                    <table>
                        <tr>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblVikingCode" Text="Viking Code" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 14%">
                                <cc1:ucLabel ID="lblODCode" Text="OD Code" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 20%">
                                <cc1:ucLabel ID="lblDescription1" Text="Description" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel ID="lblVikingCod1" Text="" runat="server"></cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblOdcode1" Text="" runat="server"></cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblDescription2" Text="" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="right" colspan="3">
                    <div class="button-row">
                        <cc1:ucButton ID="btnAddComment" runat="server" Text="Add Comment" CssClass="button"
                            OnClick="btnAddComment_Click" />
                        <cc1:ucButton ID="btnViewHistoricComment" runat="server" Text="View Historic Updates"
                            CssClass="button" OnClick="btnViewHistoricComment_Click" />
                        <cc1:ucButton ID="btnBacktoOutput" runat="server" Text="Back to Output" CssClass="button"
                            OnClick="UcbtnBacktoOutput_Click" />
                    </div>
                </td>
            </tr>
            <%--  <tr style="vertical-align:text-top">
                <td>
                    <cc1:ucLabel ID="lblVikingCod1" Text="" runat="server"></cc1:ucLabel>
                </td>
                <td>
                    <cc1:ucLabel ID="lblOdcode1" Text="" runat="server"></cc1:ucLabel>
                </td>
                 <td>
                    <cc1:ucLabel ID="lblDescription2" Text="" runat="server"></cc1:ucLabel>
                </td>
                 <td colspan="3">&nbsp;</td>
             </tr>
            <tr><td colspan="6"></td></tr>--%>
        </table>
        <cc1:ucPanel ID="pnlComments" runat="server" CssClass="fieldset-form">
            <table width="100%" id="tblComment" runat="server" cellspacing="5" cellpadding="0"
                border="0" class="top-settingsNoBorder">
                <tr>
                    <td style="font-weight: bold; width: 23%;">
                        <cc1:ucLabel ID="lblCommentCreated" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%;">:
                    </td>
                    <td style="width: 25%;">
                        <cc1:ucLabel ID="lblDateCommentwasAddedValue" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" width="15%">
                        <cc1:ucLabel ID="lblCreateBy" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">:
                    </td>
                    <td style="width: 54%">
                        <cc1:ucLabel ID="lblCommentAddedByValue" runat="server"></cc1:ucLabel>
                    </td>
                </tr>

                <tr>
                    <td style="font-weight: bold; width: 23%;">
                        <cc1:ucLabel ID="lblLastEdited" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%;">:
                    </td>
                    <td style="width: 25%;">
                        <cc1:ucLabel ID="lblLastEditedValue" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;" width="15%">
                        <cc1:ucLabel ID="lblReason_1" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">:
                    </td>
                    <td style="width: 54%">
                        <cc1:ucLabel ID="lblReasonValue" runat="server"></cc1:ucLabel>
                        <cc1:ucDropdownList ID="ddlReason_1" runat="server" Width="200px" Visible="false">
                        </cc1:ucDropdownList>
                    </td>
                </tr>

                <tr>
                    <td colspan="6" style="font-weight: bold;">
                        <cc1:ucLabel ID="lblComments" Text="Comments" runat="server"></cc1:ucLabel>
                        :
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <cc1:ucLabel ID="lblCommentValue" runat="server"></cc1:ucLabel>
                        <cc1:ucTextarea ID="txtComments" MaxLength="900" Width="90%" Height="110px" runat="server" Visible="false"></cc1:ucTextarea>
                    </td>
                </tr>

                <tr>
                    <td align="right" colspan="6" id="tdSaveChanges" runat="server" visible="false">
                        <div class="button-row">
                            <cc1:ucButton ID="btnSaveChanges" runat="server" CssClass="button" Visible="false"
                                OnClick="btnSaveChanges_Click" />

                        </div>
                    </td>
                </tr>
            </table>
            <cc1:ucLabel ID="lblNoCommentsForSKU" Visible="false" runat="server" Font-Bold="true"></cc1:ucLabel>
        </cc1:ucPanel>
        <cc1:ucPanel ID="pnlExpedite" runat="server" CssClass="fieldset-form">
            <table width="100%" id="tbllatestExpedite" runat="server" cellspacing="5" cellpadding="0"
                border="0" class="top-settingsNoBorder">
                <tr>
                    <td style="font-weight: bold; width: 15%;">
                        <cc1:ucLabel ID="lblDateTimeWithoutHifan" Text="Date Time" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%;">
                        <cc1:ucLabel ID="lblDateTimeWithoutHifanColon" Text=":" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="width: 24%;">
                        <cc1:ucLabel ID="ltDatePost" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 15%;">
                        <cc1:ucLabel ID="lblExpeditedBy" Text="Expedited By" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%;">
                        <cc1:ucLabel ID="lblExpeditedByColon" Text=":" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="width: 25%">
                        <cc1:ucLabel ID="lblCommentExpeditedBy" runat="server"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 15%;">
                        <cc1:ucLabel ID="lblSentTo" Text="Sent To" runat="server"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%;">
                        <cc1:ucLabel ID="lblSentToColon" Text=":" runat="server"></cc1:ucLabel>
                    </td>
                    <td colspan="4">
                        <cc1:ucLabel ID="ltMailSentExpAdmin" runat="server"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <b><a id="lnkExpeditePopup" runat="server" target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1050px,height=700px,toolbar=0,scrollbars=0,status=0"); return false;'>Link</a></b>
                    </td>
                </tr>
            </table>
            <cc1:ucLabel ID="lblNoExpedite" Visible="false" runat="server" Font-Bold="true"> </cc1:ucLabel>
        </cc1:ucPanel>
    </cc1:ucPanel>
    <cc1:ucPanel ID="pnlAddComment" runat="server" CssClass="fieldset-form">
        <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
        <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
            <tr>
                <td style="font-weight: bold; width: 15%;">
                    <cc1:ucLabel ID="lblCommentAddedBy_1" Text="Comment Added By" runat="server"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 1%;">:
                </td>
                <td style="font-weight: bold; width: 34%;">
                    <cc1:ucLabel ID="lblCommentAddedBy_1Value" runat="server"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold;" width="10%" align="right">
                    <cc1:ucLabel ID="lblDate" Text="Date" runat="server"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 1%">:
                </td>
                <td style="font-weight: bold; width: 39%">
                    <cc1:ucLabel ID="lblDateValue" runat="server"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold; width: 15%;">
                    <cc1:ucLabel ID="lblReason" Text="Reason" runat="server" isRequired="true"></cc1:ucLabel>
                </td>
                <td style="font-weight: bold; width: 1%;">:
                </td>
                <td style="font-weight: bold; width: 84%;" colspan="4">
                    <cc1:ucDropdownList ID="ddlReason" runat="server" Width="200px">
                    </cc1:ucDropdownList>
                    <asp:RequiredFieldValidator ID="rfvPleaseSelectReason" runat="server" ControlToValidate="ddlReason"
                        Display="None" ValidationGroup="Save" InitialValue="0"> </asp:RequiredFieldValidator>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                        ShowSummary="false" Style="color: Red" ValidationGroup="Save" />
                </td>
            </tr>
            <tr>
                <td colspan="6" style="font-weight: bold;">
                    <cc1:ucLabel ID="lblComments_1" Text="Comments" runat="server"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold; width: 100%;" colspan="6">
                    <cc1:ucTextarea ID="txtComment" MaxLength="900" Width="90%" Height="110px" runat="server"></cc1:ucTextarea>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="font-weight: bold;">
                    <cc1:ucLabel ID="lblDateCommentUpdateisValidTill" Text="Date Comment Update is Valid Till"
                        runat="server"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold; width: 100%;" colspan="6">
                    <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                        onchange="setValue1(this)" Width="70px" ReadOnly="true" />
                    <%-- <asp:RequiredFieldValidator ID="rfvDateRequired" runat="server" ControlToValidate="txtFromDate"
                        Display="None" ValidationGroup="Save"> </asp:RequiredFieldValidator>      --%>
                    <%--<span id="spSCDate" runat="server">
                                <cc2:ucDate ID="txtDateCommentUpdateisValidTill" runat="server" AutoPostBack="true" />
                            </span>
                            <cc1:ucLiteral ID="ltSCdate" runat="server" Visible="false"></cc1:ucLiteral>--%>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="font-weight: bold;">
                    <cc1:ucLabel ID="lblItemToTheExpediteReview" Text="Do you want to add this item to the expedite review"
                        runat="server"></cc1:ucLabel>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold; width: 100%;" colspan="6" class="radiobuttonlist">
                    <cc1:ucRadioButtonList ID="rblItemToTheExpediteReview" runat="server" RepeatColumns="2"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                        <asp:ListItem Value="0">No</asp:ListItem>
                    </cc1:ucRadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="6">
                    <div class="button-row">
                        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
                            ValidationGroup="Save" />
                        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                    </div>
                </td>
            </tr>
        </table>
        <%--        </ContentTemplate>
        <Triggers>
         <asp:PostBackTrigger ControlID="btnSave"/>
         <asp:PostBackTrigger ControlID="btnBack"/>
         <asp:PostBackTrigger ControlID="btnHidden"/>
        </Triggers>
        </asp:UpdatePanel>--%>
    </cc1:ucPanel>
    <cc1:ucPanel ID="pnlHistoricComments" runat="server" CssClass="fieldset-form">
        <table id="tdpnl1" runat="server" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td align="right">
                    <cc1:ucButton ID="UcbtnBacktoCmmnt" runat="server" Text="Back to Comment" CssClass="button"
                        OnClick="UcbtnBacktoCmmnt_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <cc1:ucButton ID="UcbtnBacktoOutput" runat="server" Text="Back to Output" CssClass="button"
                        OnClick="UcbtnBacktoOutput_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="border-bottom: 1px solid #c2c2c2;">
                        &nbsp;
                    </div>
                    <asp:Repeater ID="rptReasonComments" runat="server">
                        <ItemTemplate>
                            <table cellspacing="5" cellpadding="0" align="center" style="border: 1px solid #c2c2c2; border-top: none; width: 85%; background-color: #F2FEFE">
                                <tr>
                                    <td style="font-weight: bold; width: 20%;">
                                        <cc1:ucLabel ID="lblDate_1" Text="Date" runat="server"></cc1:ucLabel>/<cc1:ucLabel
                                            ID="lblActualTime" Text=" Time" runat="server"></cc1:ucLabel>
                                        <cc1:ucLabel ID="Uctext" Text="Comment was Added" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">:
                                    </td>
                                    <td style="width: 31%;">
                                        <cc1:ucLabel ID="lblDateT_1" Text='<%#Eval("CommentsAdded_Date", "{0:dd/MM/yyyy}") + "  " + Eval("CommentsAdded_Date",@"{0:HH:mm}")%>'
                                            runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 12%;">
                                        <cc1:ucLabel ID="lblUser1" Text="Comment Added By" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">:
                                    </td>
                                    <td style="width: 35%;">
                                        <cc1:ucLabel ID="lblUserT_1" Text='<%#DataBinder.Eval(Container.DataItem, "CommentBy")%>'
                                            runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; width: 10%;" colspan="3">
                                        <cc1:ucLabel ID="lblCommentLabel_1" Text="Comment" runat="server"></cc1:ucLabel>
                                        :
                                    </td>
                                    <td style="font-weight: bold; width: 12%;">
                                        <cc1:ucLabel ID="lblReason" runat="server" Text="Reason"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%;">:
                                    </td>
                                    <td style="width: 35%;">
                                        <cc1:ucLabel ID="lblReasonValue" Text='<%#DataBinder.Eval(Container.DataItem, "Reason")%>'
                                            runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100%;" colspan="6">
                                        <cc1:ucLabel Style="width: 970px; word-wrap: break-word; display: block;" ID="lblCommentLabelT"
                                            Text='<%#DataBinder.Eval(Container.DataItem, "Comments")%>' runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:ucPanel ID="pnlExpedite_1" runat="server" CssClass="fieldset-form">
                        <asp:Repeater ID="rptCommunication" runat="server" OnItemDataBound="rptCommunication_ItemDataBound">
                            <ItemTemplate>
                                <table width="100%" id="tblhistory" runat="server" cellspacing="5" cellpadding="0"
                                    border="0" class="top-settingsNoBorder">
                                    <tr>
                                        <td style="font-weight: bold; width: 25%;">
                                            <cc1:ucLabel ID="lblDateTimeCommentwasAdded3" Text="Date Time" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%;">:
                                        </td>
                                        <td style="width: 24%;">
                                            <cc1:ucLabel ID="ltDateT_1" Text='<%#Eval("Comm_On", "{0:dd/MM/yyyy}") + "  " + Eval("Comm_On",@"{0:HH:mm}")%>'
                                                runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" width="15%">
                                            <cc1:ucLabel ID="lblExpeditedBy2" Text="Expedited By" runat="server"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold; width: 1%">:
                                        </td>
                                        <td style="width: 34%">
                                            <cc1:ucLabel ID="UcLabel1" Text='<%#DataBinder.Eval(Container.DataItem, "Comm_By")%>'
                                                runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblSentTo2" Text="Sent To" runat="server"></cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <asp:Literal ID="ltMailSentTo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SentTo")%>'></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <b><a id="lnk" runat="server" target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1050px,height=700px,toolbar=0,scrollbars=0,status=0"); return false;'>Link</a></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="6">
                                            <div class="button-row">
                                                <cc1:ucButton ID="btnBack_1" runat="server" Text="Back" CssClass="button" OnClick="btnBack_1_Click" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </cc1:ucPanel>
                </td>
            </tr>
        </table>
    </cc1:ucPanel>
    <%--  </div>--%>
    <%--</div>--%>
    <asp:UpdatePanel ID="updpnlError" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnErrorMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlErrorMsg" runat="server" TargetControlID="btnErrorMsg"
                PopupControlID="pnlErrorMsg" BackgroundCssClass="modalBackground" BehaviorID="ErrorMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlErrorMsg" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3>
                        <cc1:ucLabel ID="lblERROR" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <cc1:ucLiteral ID="ltErrorMsg" runat="server"></cc1:ucLiteral>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <cc1:ucButton ID="btnErrorMsgOK" runat="server" Text="OK" CssClass="button" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnErrorMsgOK" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

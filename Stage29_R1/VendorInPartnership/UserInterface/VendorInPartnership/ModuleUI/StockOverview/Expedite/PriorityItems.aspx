﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PriorityItems.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
Inherits="ModuleUI_StockOverview_Expedite_PriorityItems" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
    <%@ Register Src="../../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="cc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

  <script type="text/javascript">
 <%--//        $(document).ready(function () {
//            $('#<%=imgbtnADD.ClientID %>').click(function () {
//                $('#<%=pnlPriorityItemsSearch.ClientID%>').hide();
//                $('#<%=gvPriorityItemsExpedite.ClientID%>').hide();
//                $('#<%=pnlPriorityItemsRecord.ClientID%>').show();
//                $('#<%=btnDelete.ClientID%>').hide();
//                Disabletextbox();
//                return false;
//            });

//            $('#<%=btnBack.ClientID %>').click(function () {
//                $('#<%=pnlPriorityItemsSearch.ClientID%>').show();
//                $('#<%=gvPriorityItemsExpedite.ClientID%>').show();
//                $('#<%=pnlPriorityItemsRecord.ClientID%>').hide();
//                return false;
//            });

           // $("#ctl00_ContentPlaceHolder1_gvPriorityItemsExpedite_ctl").click(function () {
//                alert(1);

//                $('#ctl00_ContentPlaceHolder1_pnlPriorityItemsSearch%>').hide();
//                $('#<%=gvPriorityItemsExpedite.ClientID%>').hide();
//                $('#<%=pnlPriorityItemsRecord.ClientID%>').show();
//                return false;
//            });

//                        function Disabletextbox() {
//                            alert(1);
//                            $('#<%=tboxSKU.ClientID%>').attr('disabled',true);
//                            $('#<%=tboxCatCode.ClientID%>').attr('disabled',true);
//                            $('#<%=tboxReason.ClientID%>').attr('disabled',true);
//                         }

            //             $('#<%=gvPriorityItemsExpedite.ClientID %> td:nth-child(2)').each(function () {
            //                $('#<%=pnlPriorityItemsSearch.ClientID%>').hide();
            //                $('#<%=gvPriorityItemsExpedite.ClientID%>').hide();
            //                $('#<%=pnlPriorityItemsRecord.ClientID%>').show();
            //                $('#<%=btnDelete.ClientID%>').hide();
            //             }

--%>//        });
    </script>


    <h2>
        <cc1:ucLabel ID="lblPriorityItemsExpedite" runat="server"></cc1:ucLabel>
    </h2>
     <div class="right-shadow">
        <div class="formbox">
            <asp:Panel ID="pnlPriorityItemsSearch" runat="server">
                   
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                            <tr>
                                <td style="font-weight: bold; width: 8%;">
                                    <cc1:ucLabel ID="lblCountry" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%;">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 20%;">
                                   <cc3:ucCountry runat="server" ID="ddlCountry" />
                                </td>
                                <td style="font-weight: bold;" width="8%">
                                    <cc1:ucLabel ID="lblODSKU" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 62%">
                                     <cc1:ucTextbox ID="txtODSKU" Width="150px" runat="server"></cc1:ucTextbox>
                                </td>
                                
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 8%;">
                                    <cc1:ucLabel ID="lblCATCode" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%;">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 20%;">
                                    <cc1:ucTextbox ID="txtCATCode" Width="150px" runat="server"></cc1:ucTextbox>
                                </td>
                                <td style="font-weight: bold; width: 8%;">
                                   
                                </td>
                                <td style="font-weight: bold; width: 1%;">
                                    
                                </td>
                                <td style="font-weight: bold; width: 62%;">
                                    <div class="button-row" style="text-align: left">
                                        <cc1:ucButton ID="btnSearch" runat="server" CssClass="button" OnClick="btnSearch_Click" />
                                    </div>
                                </td>
                            </tr>
                     </table>
                   
                    
                        <div class="button-row"  >
                        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="PriorityItemsEdit.aspx?value=1"  />  
                        <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
         Width="109px" Height="20px" />
                        
                        </div>
                    
                  <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                            <tr>
                                <td style="font-weight: bold; width: 100%;" colspan="6" align="center">
                                    <cc1:ucGridView ID="gvPriorityItemsExpedite" runat="server" AutoGenerateColumns="false"
                                        CssClass="grid"  Width="100%" >
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField HeaderText="Country" SortExpression="Country.CountryName">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                   <cc1:ucLabel ID="lblCountryPriority" runat="server" Text='<%# Eval("Country.CountryName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SKU" SortExpression="OD_Code">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                     <asp:HyperLink ID="hplinkSKU" runat="server" 
                                                     NavigateUrl='<%# EncryptQuery("PriorityItemsEdit.aspx?CountryCarrierID="
                                                     + Eval("Country.CountryId") +"&OD_Code=" + Eval("OD_Code") + "&Viking_Code=" + Eval("Viking_Code")
                                                     + "&Product_description=" + Eval("Product_description") +  "&Reason=" + Eval("Reason") ) %>' 
                                                     Text='<%# Eval("OD_Code") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CatCode" SortExpression="Viking_Code">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblCatCodePriority" runat="server" Text='<%# Eval("Viking_Code") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="DescriptionNew" SortExpression="Product_description">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblDescriptionPriority" runat="server" Text='<%# Eval("Product_description") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Reason" SortExpression="Reason">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblReasonPriority" runat="server" Text='<%# Eval("Reason") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Added By" SortExpression="CommentsAddedBy">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                     <cc1:ucLabel ID="lblAddedByPriority" runat="server" Text='<%# Eval("CommentsAddedBy") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Date" SortExpression="Date">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                     <cc1:ucLabel ID="lblDatePriority" runat="server" Text='<%# Eval("Date", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                    </cc1:ucGridView>
                                <div class="button-row">
                                <cc1:PagerV2_8 ID="pager1" runat="server"  OnCommand="pager_Command" GenerateGoToSection="false">
                                </cc1:PagerV2_8>
                                </div>  
                                <cc1:ucLabel ID="lblNoRecordsFound" Font-Bold="true" ForeColor="Red" Visible="false"
                                        runat="server"></cc1:ucLabel>

                                            <cc1:ucGridView ID="UcGridView1Export" runat="server" AutoGenerateColumns="false"
                                        CssClass="grid"   Width="100%" >
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField HeaderText="Country" SortExpression="Country.CountryName">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                   <cc1:ucLabel ID="lblCountry" runat="server" Text='<%# Eval("Country.CountryName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SKU" SortExpression="OD_Code">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                     <asp:HyperLink ID="hplinkSKU" runat="server" 
                                                     NavigateUrl='<%# EncryptQuery("PriorityItemsEdit.aspx?CountryCarrierID="
                                                     + Eval("Country.CountryId") +"&OD_Code=" + Eval("OD_Code") + "&Viking_Code=" + Eval("Viking_Code")
                                                     + "&Product_description=" + Eval("Product_description") +  "&Reason=" + Eval("Reason") ) %>' 
                                                     Text='<%# Eval("OD_Code") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CatCode" SortExpression="Viking_Code">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblCatCodePriority" runat="server" Text='<%# Eval("Viking_Code") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="DescriptionNew" SortExpression="Product_description">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblDescriptionPriority" runat="server" Text='<%# Eval("Product_description") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Reason" SortExpression="Reason">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel ID="lblReasonPriority" runat="server" Text='<%# Eval("Reason") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Added By" SortExpression="CommentsAddedBy">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                     <cc1:ucLabel ID="lblAddedByPriority" runat="server" Text='<%# Eval("CommentsAddedBy") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Date" SortExpression="Date">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                     <cc1:ucLabel ID="lblDatePriority" runat="server" Text='<%# Eval("Date", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                    </cc1:ucGridView>
                                </td>
                            </tr>
                 </table>
                 </asp:Panel>
               


             </div>
       </div>
                   
   
                    
</asp:Content>
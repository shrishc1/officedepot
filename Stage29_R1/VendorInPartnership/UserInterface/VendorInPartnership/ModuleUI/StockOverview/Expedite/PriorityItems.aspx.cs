﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.StockOverview;
using System.Data;
using WebUtilities;

public partial class ModuleUI_StockOverview_Expedite_PriorityItems : CommonPage
{
    string PriorityItemErrorMessage = WebCommon.getGlobalResourceValue("PriorityItemErrorMessage");
    int RecordCount = 0;
    bool IsExportClicked = false;
    List<ExpediteStockBE> lst_priorityItems;
    protected void Page_Init(object sender, EventArgs e)
    {
        ddlCountry.CurrentPage = this;
        ddlCountry.IsAllDefault = true;
        ddlCountry.IsAllRequired = true;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
       // string Value = "true";
        //ucExportToExcel1.CurrentPage = this;
        //ucExportToExcel1.GridViewControl = UcGridView1Export;
        //ucExportToExcel1.IsAutoGenratedGridview = true;
        //ucExportToExcel1.FileName = "PriorityItemsExpedite";
        if (!IsPostBack)
        {
            pager1.PageSize = 200;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
            btnExportToExcel.Visible = false;
            btnExportToExcel.Enabled = false;

            if (GetQueryStringValue("BacktoBind") != null)
            {
                ExpediteStockBE oMAS_ExpediteStockBE = new ExpediteStockBE();
                ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
                oMAS_ExpediteStockBE = (ExpediteStockBE)Session["oMAS_ExpediteStockBE"];
                if (GetQueryStringValue("CountryID") != null) 
                {
                    oMAS_ExpediteStockBE.CountryID =Convert.ToInt32(GetQueryStringValue("CountryID"));
                    ddlCountry.innerControlddlCountry.SelectedValue = Convert.ToString(GetQueryStringValue("CountryID"));
                }
                //oMAS_ExpediteStockBE.PageCount = 1;
                oMAS_ExpStockBAL.GetPriorityItemsListBAL(oMAS_ExpediteStockBE, out lst_priorityItems, out RecordCount);
               
               
                if (lst_priorityItems.Count > 0)
                {
                    pager1.ItemCount = RecordCount;
                    pager1.Visible = true;
                    ViewState["PriorityItemsList"] = lst_priorityItems;
                    gvPriorityItemsExpedite.DataSource = lst_priorityItems;
                    gvPriorityItemsExpedite.DataBind();

                    UcGridView1Export.DataSource = lst_priorityItems;
                    UcGridView1Export.DataBind();
                    UcGridView1Export.Visible = false;
                    lblNoRecordsFound.Visible = false;
                    btnExportToExcel.Visible = true;
                    btnExportToExcel.Enabled = true;

                    //ucExportToExcel1.Visible = true;

                }
                else
                {
                    gvPriorityItemsExpedite.DataSource = null;
                    gvPriorityItemsExpedite.DataBind();

                    UcGridView1Export.DataSource = null;
                    UcGridView1Export.DataBind();
                    UcGridView1Export.Visible = false;
                    ViewState["PriorityItemsList"] = null;
                    lblNoRecordsFound.Visible = true;
                    btnExportToExcel.Visible = false;
                    btnExportToExcel.Enabled = false;

                    // ucExportToExcel1.Visible = false;
                }
            }
        }
     }

   
    protected void btnSearch_Click(object sender, EventArgs e)
    {
      BindPriorityItemsExpediteGrid();
     // ClearControls();
    }

    public void BindPriorityItemsExpediteGrid(int page=1)
    {
        ExpediteStockBE oMAS_ExpediteStockBE = new ExpediteStockBE();
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        oMAS_ExpediteStockBE.Action = "GetPriorityItemsExpedite";
        oMAS_ExpediteStockBE.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
        oMAS_ExpediteStockBE.OD_Code = txtODSKU.Text;
        oMAS_ExpediteStockBE.Viking_Code = txtCATCode.Text;
        oMAS_ExpediteStockBE.PageCount = page;
        Session["oMAS_ExpediteStockBE"] = oMAS_ExpediteStockBE;

        oMAS_ExpStockBAL.GetPriorityItemsListBAL(oMAS_ExpediteStockBE, out lst_priorityItems, out RecordCount);

        //List<ExpediteStockBE> lst_priorityItems = new List<ExpediteStockBE>();
        //ExpediteStockBE oNewMASCNT_ExpediteBE = new ExpediteStockBE();
        //oNewMASCNT_ExpediteBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
        //oNewMASCNT_ExpediteBE.Country.CountryName = "UK&IRE";
        //oNewMASCNT_ExpediteBE.OD_Code = "0129381";
        //oNewMASCNT_ExpediteBE.Viking_Code = "EAG210";
        //oNewMASCNT_ExpediteBE.Product_description = "ECONOMY PAPER";
        //oNewMASCNT_ExpediteBE.Reason = "Promo Item";
        //oNewMASCNT_ExpediteBE.CommentsAddedBy = "Julie Holt";
        //oNewMASCNT_ExpediteBE.Date = DateTime.Now;
        //lst_priorityItems.Add(oNewMASCNT_ExpediteBE);


        if (lst_priorityItems.Count > 0)
        {
            pager1.ItemCount = RecordCount;
            pager1.Visible = true;
            ViewState["PriorityItemsList"] = lst_priorityItems;
            gvPriorityItemsExpedite.DataSource = lst_priorityItems;
            gvPriorityItemsExpedite.DataBind();

            UcGridView1Export.DataSource = lst_priorityItems;
            UcGridView1Export.DataBind();
            UcGridView1Export.Visible = false;
            lblNoRecordsFound.Visible = false;
            btnExportToExcel.Visible = true;
            btnExportToExcel.Enabled = true;


            //ucExportToExcel1.Visible = true;
           
        }
        else
        {
            gvPriorityItemsExpedite.DataSource = null;
            gvPriorityItemsExpedite.DataBind();

            UcGridView1Export.DataSource = null;
            UcGridView1Export.DataBind();
            ViewState["PriorityItemsList"] = null;
            lblNoRecordsFound.Visible = true;
            btnExportToExcel.Visible = false;
            btnExportToExcel.Enabled = false;

           // ucExportToExcel1.Visible = false;
        }
    
    }
    //}
    //public void ClearControls()
    //{
    //    txtCATCode.Text = "";
    //    txtODSKU.Text = "";
    //}
    public void BindGridviewExcel()
    {
        ExpediteStockBE oMAS_ExpediteStockBE = new ExpediteStockBE();
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        oMAS_ExpediteStockBE.Action = "GetPriorityItemsExpedite";
        oMAS_ExpediteStockBE.CountryID = Convert.ToInt32(ddlCountry.innerControlddlCountry.SelectedItem.Value);
        oMAS_ExpediteStockBE.OD_Code = txtODSKU.Text;
        oMAS_ExpediteStockBE.Viking_Code = txtCATCode.Text;
        Session["oMAS_ExpediteStockBE"] = oMAS_ExpediteStockBE;

        oMAS_ExpStockBAL.GetPriorityItemsListBAL(oMAS_ExpediteStockBE, out lst_priorityItems, out RecordCount);

        //List<ExpediteStockBE> lst_priorityItems = new List<ExpediteStockBE>();
        //ExpediteStockBE oNewMASCNT_ExpediteBE = new ExpediteStockBE();
        //oNewMASCNT_ExpediteBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
        //oNewMASCNT_ExpediteBE.Country.CountryName = "UK&IRE";
        //oNewMASCNT_ExpediteBE.OD_Code = "0129381";
        //oNewMASCNT_ExpediteBE.Viking_Code = "EAG210";
        //oNewMASCNT_ExpediteBE.Product_description = "ECONOMY PAPER";
        //oNewMASCNT_ExpediteBE.Reason = "Promo Item";
        //oNewMASCNT_ExpediteBE.CommentsAddedBy = "Julie Holt";
        //oNewMASCNT_ExpediteBE.Date = DateTime.Now;
        //lst_priorityItems.Add(oNewMASCNT_ExpediteBE);


        if (lst_priorityItems.Count > 0)
        {
            pager1.ItemCount = RecordCount;
            pager1.Visible = true;
            ViewState["PriorityItemsList"] = lst_priorityItems;
            //gvPriorityItemsExpedite.DataSource = lst_priorityItems;
            //gvPriorityItemsExpedite.DataBind();

            UcGridView1Export.DataSource = lst_priorityItems;
            UcGridView1Export.DataBind();
            lblNoRecordsFound.Visible = false;
            //ucExportToExcel1.Visible = true;

        }
        else
        {          
            UcGridView1Export.DataSource = null;
            UcGridView1Export.DataBind();        
        }
    
    }
    public void pager_Command(object sender, CommandEventArgs e)
    {

        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindPriorityItemsExpediteGrid(currnetPageIndx);
    }
    //private int[] textColNumber;
    //public int[] TextColNumber
    //{
    //    get { return textColNumber; }
    //    set { textColNumber = value; }
    //}
    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
       // BindGridviewExcel();
        LocalizeGridHeader(UcGridView1Export);
        if (UcGridView1Export.Rows.Count > 0)
            WebCommon.ExportHideHidden("PriorityItemsExpedite", UcGridView1Export,null, IsHideHidden: false,IsAutogeneratedGridView: false);
         
    }
   

    

}
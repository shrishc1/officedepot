﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="OpenPurchaseListing.aspx.cs" Inherits="OpenPurchaseListing" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblOpenPurchaseListing" Text="Open Purchase Listing" runat="server"></cc1:ucLabel>
        <cc1:ucLabel ID="lblPurchaseOrder_1" Visible="false" runat="server"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnBaseSiteId" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <div class="button-row">
                <cc1:ucButton ID="UcbtnBacktoOutput" runat="server" Text="Back to Output" CssClass="button"
                    OnClick="UcbtnBacktoOutput_Click" />
            </div>
            <asp:Panel ID="pnlPurchaseOrderInqSearch" runat="server">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                    <tr>
                        <td style="font-weight: bold; width: 100%;" colspan="6" align="center">
                            <cc1:ucGridView ID="gvPurchaseOrderInquiry" runat="server" AutoGenerateColumns="false"
                                CssClass="grid" AllowPaging="true" PageSize="20" Width="100%" OnPageIndexChanging="gvPurchaseOrderInquiry_PageIndexChanging"
                                OnRowCommand="gvPurchaseOrderInquiry_RowCommand">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="PO" SortExpression="Purchase_order">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPO" Text='<%# Eval("Purchase_order") %>' CommandName="SendOnPOHistory"
                                                runat="server" CommandArgument='<%# Eval("Purchase_order") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor" SortExpression="Vendor_No">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnVendorId" runat="server" Value='<%# Eval("Vendor.VendorID") %>' />
                                            <cc1:ucLabel ID="lblOrderRaisedDatePOInq" Visible="false" runat="server" Text='<%# Eval("Order_raised","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            <cc1:ucLabel ID="lblVendorPOInq" runat="server" Text='<%# Eval("Vendor_No") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblStatusPOInq" runat="server" Text='<%# Eval("Status") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OpenLinesWithPreHash" SortExpression="Line_No">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblLineNoPOInq" runat="server" Text='<%# Eval("Line_No") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ExpectDuedate" SortExpression="Expected_date">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <cc1:ucLabel ID="lblExpecteddatePOInq" runat="server" Text='<%# Eval("Expected_date","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnSiteId" runat="server" Value='<%# Eval("Site.SiteID") %>' />
                                            <cc1:ucLabel ID="lblSiteNamePOInq" runat="server" Text='<%# Eval("Site.SiteName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            </cc1:ucGridView>
                            <cc1:ucLabel ID="lblNoRecordsFound" Font-Bold="true" ForeColor="Red" Visible="false"
                                runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlPurchaseOrderInqDetail" runat="server" Visible="false">
                <cc1:ucPanel ID="pnlVendorPointsOfContact" runat="server" CssClass="fieldset-form">
                    <table width="100%" cellspacing="5" cellpadding="0" align="center" class="top-settingsNoBorder">
                        <tr>
                            <td>
                                <cc1:ucGridView ID="gvVendorPOC" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                    Width="100%">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Vendor#" HeaderStyle-HorizontalAlign="Left">
                                            <ItemStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="ltVendor" runat="server" Text='<%# Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vendor_Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="ltVendor_Name" runat="server" Text='<%# Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Stock Planning Point of Contact" HeaderStyle-HorizontalAlign="Left">
                                            <ItemStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVendorPOC" runat="server" Text='<%# Eval("VendorPOC.FullName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:BoundField HeaderText="Stock Planning Point of Contact" DataField="InventoryPOC">
                                            <HeaderStyle Width="20%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>--%>
                                        <asp:TemplateField HeaderText="Vendor POC Email Address" HeaderStyle-HorizontalAlign="Left">
                                            <ItemStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVendorPOCEmail" runat="server" Text='<%# Eval("VendorPOC.EmailAddress") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:BoundField HeaderText="Vendor POC Email Address" DataField="EmailAddress">
                                            <HeaderStyle Width="20%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>--%>
                                        <asp:TemplateField HeaderText="Vendor POC  Number" HeaderStyle-HorizontalAlign="Left">
                                            <ItemStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVendorPOCPhone" runat="server" Text='<%# Eval("VendorPOC.PhoneNumber") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--
                                        <asp:BoundField HeaderText="Vendor POC  Number" DataField="PhoneNumber">
                                            <HeaderStyle Width="20%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>--%>
                                    </Columns>
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
                <cc1:ucPanel ID="pnlPurchaseOrderInformation" GroupingText="Purchase Order Information"
                    runat="server" CssClass="fieldset-form">
                    <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                        <tr>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblPurchaseOrder" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="font-weight: bold; width: 17%">
                                <cc1:ucLabel ID="lblPurchaseOrderV" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblStockPlannerNo" Text="Stock Planner #" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 17%">
                                <cc1:ucLabel ID="lblStockPlannerNoV" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblStockPlannerName" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 17%">
                                <cc1:ucLabel ID="lblBuyerV" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblSiteDiscShortage" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="font-weight: bold; width: 17%">
                                <cc1:ucLabel ID="lblSiteV" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblOrderCreated" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="font-weight: bold; width: 17%">
                                <cc1:ucLabel ID="lblOrderCreatedV" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 15%">
                                <cc1:ucLabel ID="lblStatus_1" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 17%">
                                <cc1:ucLabel ID="lblStatusV" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
                <cc1:ucPanel ID="pnlPODetails" runat="server" CssClass="fieldset-form" Style="display: block">
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="tabblocks">
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6" align="center">
                                <cc1:ucGridView ID="gvPODetails" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                    Width="100%">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="LineNbr" SortExpression="Line_No" HeaderStyle-HorizontalAlign="Left">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblLineNbrPOInq" runat="server" Text='<%# Eval("Line_No") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ODSKUWithHash" SortExpression="OD_Code">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblODSKUPOInq" runat="server" Text='<%# Eval("OD_Code") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VikingSKUWithHash" SortExpression="Direct_code">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVikingSKUPOInq" runat="server" Text='<%# Eval("Direct_code") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VendorCode" SortExpression="Vendor_Code">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVendorCodePOInq" runat="server" Text='<%# Eval("Vendor_Code") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description1" SortExpression="Product_description">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblDescriptionPOInq" runat="server" Text='<%# Eval("Product_description") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OriginalQty" SortExpression="Original_quantity">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblOriginalQtyPOInq" runat="server" Text='<%# Eval("Original_quantity") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OutstandingQty" SortExpression="Outstanding_Qty">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblOutstandingQtyPOInq" runat="server" Text='<%# Eval("Outstanding_Qty") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ExpectedDate" SortExpression="Expected_date">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblExpectedDatePOInq" runat="server" Text='<%# Eval("Expected_date","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LastReceipt" SortExpression="LastReceiptDate">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblLastReceiptPOInq" runat="server" Text='<%# Eval("LastReceiptDate","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="RevisedDueDate" SortExpression="Revised Due Date">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblLastReceiptPOInq" runat="server" Text='<%# Eval("Expected_date","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments" SortExpression="Comments">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="ltComments" runat="server" Text='<%# Eval("Comments") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
                <cc1:ucPanel ID="pnlBookingDetails" runat="server" CssClass="fieldset-form" Style="display: block">
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="Table1">
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6" align="center">
                                <cc1:ucGridView ID="gvBookingDetails" runat="server" AutoGenerateColumns="false"
                                    CssClass="grid" Width="100%">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date" SortExpression="ScheduleDate">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblDatePOInq" runat="server" Text='<%# Eval("ScheduleDate","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BookingTime" SortExpression="SlotTime.SlotTime">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblTimePOInq" runat="server" Text='<%# Eval("SlotTime.SlotTime") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" SortExpression="BookingStatus">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblStatusPOInq" runat="server" Text='<%# Eval("BookingStatus") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BookingRefNo" SortExpression="BookingRef">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnBookingID" Value='<%# Eval("BookingID") %>' runat="server" />
                                                <asp:HyperLink ID="hpBookingRefPOInq" runat="server" Text='<%# Eval("BookingRef") %>'
                                                    NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?Scheduledate=" + Eval("ScheduleDate") + "&ID=BK-" + Eval("SupplierType") + "-" + Eval("BookingID") + "-" + Eval("SiteId") + "&PN=OpenPL&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text)  %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ActualPallets" SortExpression="NumberOfPallet">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblPalletsPOInq" runat="server" Text='<%# Eval("NumberOfPallet") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ActualCartons" SortExpression="NumberOfCartons">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblCartonsPOInq" runat="server" Text='<%# Eval("NumberOfCartons") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ActualLines" SortExpression="NumberOfLines">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblLinesPOInq" runat="server" Text='<%# Eval("NumberOfLines") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CarrierActual" SortExpression="Carrier.CarrierName">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblCarrierPOInq" runat="server" Text='<%# Eval("Carrier.CarrierName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
                <cc1:ucPanel ID="pnlDiscrepancies" runat="server" CssClass="fieldset-form" Style="display: block">
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="Table2">
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6" align="center">
                                <cc1:ucGridView ID="gvDiscrepancies" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                    Width="100%" OnRowDataBound="gvDiscrepancies_RowDataBound">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Status" SortExpression="DiscrepancyStatus">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnDiscrepancyTypeID" Value='<%# Eval("DiscrepancyTypeID") %>'
                                                    runat="server" />
                                                <asp:HiddenField ID="hdnDiscrepancyLogID" Value='<%# Eval("DiscrepancyLogID") %>'
                                                    runat="server" />
                                                <asp:HiddenField ID="hdnUserID" Value='<%# Eval("UserID") %>' runat="server" />
                                                <cc1:ucLabel ID="lblStatusPOInq" runat="server" Text='<%# Eval("DiscrepancyStatus") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VDRNumber" SortExpression="VDRNo">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hpBookingRefPOInq" runat="server" Text='<%# Eval("VDRNo") %>'></asp:HyperLink>
                                                <%--NavigateUrl='<%# EncryptQuery("~/ModuleUI/Discrepancy/LogDiscrepancy/DISLog_PresentationIssue.aspx?disLogID=" + Eval("DiscrepancyLogID") + "&VDRNo=" + Eval("VDRNo") + "&UserID=" + Eval("UserID")+ "&PN=POINQ&Status=" + lblStatusV.Text + "&PO=" + lblPurchaseOrderV.Text + "&SId=" + hdnBaseSiteId.Value)  %>'></asp:HyperLink>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VDRDiscription" SortExpression="DiscrepancyType">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVDRDescription" runat="server" Text='<%# Eval("DiscrepancyType") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CreatedBy" SortExpression="Username">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblCreatedByPOInq" runat="server" Text='<%# Eval("Username") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VDRDate" SortExpression="DiscrepancyLogDate">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVDRDatePOInq" runat="server" Text='<%# Eval("DiscrepancyLogDate","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
                <cc1:ucPanel ID="pnlExpediteHistory" GroupingText="Expedite History" runat="server"
                    CssClass="fieldset-form">
                    <table width="100%" cellspacing="5" cellpadding="0" align="center" class="top-settingsNoBorder">
                        <tr>
                            <td>
                                <cc1:ucGridView ID="gvExpediteHistory" runat="server" AutoGenerateColumns="false"
                                    CssClass="grid" Width="100%" OnRowDataBound="gvExpediteHistory_RowDataBound">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                                            <ItemStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="ltDate" runat="server" Text='<%#Eval("Comm_On", "{0:dd/MM/yyyy}") + "  " + Eval("Comm_On",@"{0:HH:mm}")%>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Option" DataField="Option">
                                            <HeaderStyle Width="20%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Who was Contacted" DataField="SentTo" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-Width="20%" />
                                        <%-- <asp:TemplateField HeaderText="Who was Contact" HeaderStyle-HorizontalAlign="Left">
                                            <HeaderStyle Width="20%" />
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlWhoWasContact" runat="server" Font-Bold="true" Text='<%# Eval("SentTo") %>' target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=800px,height=600px,toolbar=0,scrollbars=0,status=0"); return false;'>></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:BoundField HeaderText="Who Expedite" DataField="Comm_By">
                                            <HeaderStyle Width="20%" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <b><a id="lnk" runat="server" target='_blank' onclick='window.open(this.href, this.target,"scrollbars=1,left=100px,top=20px,width=1050px,height=600px,toolbar=0,scrollbars=0,status=0"); return false;'>
                                                    Link</a></b>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
                <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="Table3">
                    <tr>
                        <td style="font-weight: bold; width: 100%;">
                            <%-- <asp:UpdatePanel ID="updPannelBack" runat="server">
                                <ContentTemplate>--%>
                            <div class="button-row" style="text-align: right">
                                <cc1:ucButton ID="btnBack" runat="server" CausesValidation="false" CssClass="button"
                                    OnClick="btnBack_Click" />
                            </div>
                            <%-- </ContentTemplate>
                                <%--<Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnBack" />
                                </Triggers>
                            </asp:UpdatePanel>--%>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ExpediteBookingDate.aspx.cs" Inherits="ExpediteBookingDate" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc1:ucPanel ID="pnlBookingDetails" runat="server" CssClass="fieldset-form" Style="display: block">
       <div class="button-row">
                <cc1:ucButton ID="UcbtnBacktoOutput" runat="server" Text="Back to Output" CssClass="button"
                    OnClick="UcbtnBacktoOutput_Click" />
            </div>
                    <table width="100%" cellspacing="5" cellpadding="0" class="form-table" id="Table1">
                        <tr>
                            <td style="font-weight: bold; width: 100%;" colspan="6" align="center">
                                <cc1:ucGridView ID="gvBookingDetails" runat="server" AutoGenerateColumns="false"
                                    CssClass="grid" Width="100%">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date" SortExpression="ScheduleDate">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblDatePOInq" runat="server" Text='<%# Eval("ScheduleDate","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BookingTime" SortExpression="SlotTime.SlotTime">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblTimePOInq" runat="server" Text='<%# Eval("SlotTime.SlotTime") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" SortExpression="BookingStatus">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblStatusPOInq" runat="server" Text='<%# Eval("BookingStatus") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BookingRefNo" SortExpression="BookingRef">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnBookingID" Value='<%# Eval("BookingID") %>' runat="server" />
                                                <asp:HyperLink ID="hpBookingRefPOInq" runat="server" Text='<%# Eval("BookingRef") %>'
                                                    NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/Receiving/APPRcv_BookingHistory.aspx?Scheduledate=" + Eval("ScheduleDate") + "&ID=BK-" + Eval("SupplierType") + "-" + Eval("BookingID") + "-" + Eval("SiteId") + "&PN=PotOutBoDate") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ActualPallets" SortExpression="NumberOfPallet">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblPalletsPOInq" runat="server" Text='<%# Eval("NumberOfPallet") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ActualCartons" SortExpression="NumberOfCartons">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblCartonsPOInq" runat="server" Text='<%# Eval("NumberOfCartons") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ActualLines" SortExpression="NumberOfLines">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblLinesPOInq" runat="server" Text='<%# Eval("NumberOfLines") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CarrierActual" SortExpression="Carrier.CarrierName">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblCarrierPOInq" runat="server" Text='<%# Eval("Carrier.CarrierName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
</asp:Content>

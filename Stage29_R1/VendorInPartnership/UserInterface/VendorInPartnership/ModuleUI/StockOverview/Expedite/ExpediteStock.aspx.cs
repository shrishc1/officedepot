﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Data;
using System.Reflection;
using System.Configuration;
using System.IO;
using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using Utilities;
using System.Text;
using BaseControlLibrary;

public partial class ExpediteStock : CommonPage
{
    Hashtable htVendor = new Hashtable();
    List<string> idList = new List<string>();
    string ODSKU = WebCommon.getGlobalResourceValue("ODSKU");
    string VikingSKU = WebCommon.getGlobalResourceValue("VikingSKU");
    string VendorItemCode = WebCommon.getGlobalResourceValue("VendorCode");
    string Description = WebCommon.getGlobalResourceValue("Description1");
    string EarliestOpenPONumber = WebCommon.getGlobalResourceValue("EarliestOpenPONumber");
    string OrignalDuedate = WebCommon.getGlobalResourceValue("OrignalDuedate");
    protected string commLibProceedMessage = WebCommon.getGlobalResourceValue("CommLibProceedMessage");
    protected string ValidEmail = WebCommon.getGlobalResourceValue("ValidEmail");

    protected string commLibBackDataLostConfirmation = WebCommon.getGlobalResourceValue("CommLibBackDataLostConfirmation");
    string emailSubjectMandatory = WebCommon.getGlobalResourceValue("EmailSubjectMandatory");
    string emailCommunicationMandatory = WebCommon.getGlobalResourceValue("EmailBodyMandatory");
    string emailSentByMandatory = WebCommon.getGlobalResourceValue("EmailSentByMandatory");
    string sentonBehalfofMandatory = WebCommon.getGlobalResourceValue("SentonBehalfofMandatory");
    string emailTitleMandatory = WebCommon.getGlobalResourceValue("EmailTitleMandatory");
    protected string doYouWantToSubmit = WebCommon.getGlobalResourceValue("DoYouWantToSubmit");

    string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
    string templatePathName = @"/EmailTemplates/Expedite/";
    string strVendorIds = string.Empty;
    DataTable dtExpediteStock = null;
    DataSet dsExpediteStock = null;

    //------------phase 11 point 7-----------------------//
    public object gridsender
    {
        get
        {
            return ViewState["gridsender"] != null ? (object)ViewState["gridsender"] : null;
        }
        set
        {
            ViewState["gridsender"] = value;
        }
    }

    public string VendorEmailID
    {
        get
        {
            return ViewState["VendorEmailID"] != null ? (string)ViewState["VendorEmailID"] : null;
        }
        set
        {
            ViewState["VendorEmailID"] = value;
        }
    }


    public string VendorEmailSPID
    {
        get
        {
            return ViewState["VendorEmailSPID"] != null ? (string)ViewState["VendorEmailSPID"] : null;
        }
        set
        {
            ViewState["VendorEmailSPID"] = value;
        }
    }

    public bool isExpedite
    {
        get
        {
            return ViewState["isExpedite"] != null ? (bool)ViewState["isExpedite"] : false;
        }
        set
        {
            ViewState["isExpedite"] = value;
        }
    }
    //---------------------------------------------------//

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
        //ucIncludeVendor.IsParentRequired = true;
        //ucIncludeVendor.IsStandAloneRequired = true;
        //ucIncludeVendor.IsChildRequired = true;
        //ucIncludeVendor.IsGrandParentRequired = true;
        ucExportToExcel1.GridViewControl = gvExcel;
        ucExportToExcel1.FileName = "ExpediteStock";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (isExpedite)
        {
            mdlExpeditePopup.Show();
        }
        UIUtility.PageValidateForODUser();
        dsExpediteStock = new DataSet();
        dtExpediteStock = new DataTable();
        if (!IsPostBack)
        {
            pnlSearchScreen.Visible = true;
            pnlPotentialOutput.Visible = false;
            ucExportToExcel1.Visible = false;
        }

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ViewState["ExpediteStocklstForOpen"] = null;
        ViewState["ExpediteStocklst"] = null;
        this.BindPotentialOutputGrid();
        pnlSearchScreen.Visible = false;
        pnlPotentialOutput.Visible = true;
        //if (gvPotentialOutput.Rows.Count > 0)
        ViewState["sentMailVendorId"] = null;
        rdoShowAll.Checked = false;
        rdoShowOnlyOpenVendorsPO.Checked = true;

    }

    private void BindPotentialOutputGrid()
    {
        var lstExpediteStock = new List<ExpediteStockBE>();
        var oNewExpediteStocklst = new List<ExpediteStockBE>();
        var oNewExpediateStockBE = new ExpediteStockBE();
        var oExpStockBAL = new ExpediteStockBAL();
        oNewExpediateStockBE.Action = "GetExpediateStockForOpenPO";
        oNewExpediateStockBE.SelectedIncludedVendorIDs = ucIncludeVendor.SelectedVendorIDs;
        oNewExpediateStockBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oNewExpediateStockBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        oNewExpediateStockBE.VendorName = strVendorIds;
        if (ViewState["ExpediteStocklstForOpen"] == null)
        {
            oNewExpediteStocklst = oExpStockBAL.GetExpediateStockBAL(oNewExpediateStockBE);
            ViewState["ExpediteStocklstForOpen"] = oNewExpediteStocklst;
            ViewState["ExpediteStocklst1"] = oNewExpediteStocklst;
        }
        else
        {
            oNewExpediteStocklst = (List<ExpediteStockBE>)ViewState["ExpediteStocklstForOpen"];
            ViewState["ExpediteStocklst1"] = ViewState["ExpediteStocklstForOpen"];
        }

        ucExportToExcel1.Visible = false;
        if (oNewExpediteStocklst.Count > 0 && oNewExpediteStocklst != null)
        {
            gvExcel.DataSource = oNewExpediteStocklst;
            gvExcel.DataBind();

            ucExportToExcel1.Visible = true;

            var lstVendorIds = oNewExpediteStocklst.Select
                (o => new ExpediteStockBE
                {
                    VendorID = o.VendorID
                }
                ).Distinct().ToList();
            rptExpedite.DataSource = lstVendorIds;
            rptExpedite.DataBind();

        }
        else
        {
            rptExpedite.DataSource = null;
            rptExpedite.DataBind();
        }
        ViewState["RadioButton"] = "rdoShowOnlyOpen";
    }

    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e) { }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        pnlSearchScreen.Visible = true;
        pnlPotentialOutput.Visible = false;
        ucExportToExcel1.Visible = false;
        HiddenField hdfVendor = ucIncludeVendor.FindControl("hiddenSelectedIDs") as HiddenField;
        hdfVendor.Value = "";
    }
    private string CheckPO(object sender)
    {
        Button btn = (Button)sender;
        GridView grdPotential = (GridView)btn.Parent.Parent.Parent.Parent;
        string strSKU = string.Empty;

        foreach (GridViewRow gvrow in grdPotential.Rows)
        {
            CheckBox chkSelect = (CheckBox)gvrow.FindControl("chkSelect");
            Label lblODSKU = (Label)gvrow.FindControl("lblOD_CodeValue");
            HiddenField hdnPurchaseOrder = (HiddenField)gvrow.FindControl("hdnPurchase_order");

            if (chkSelect != null)
            {
                if (chkSelect.Checked)
                {
                    if (hdnPurchaseOrder.Value == string.Empty)
                    {
                        if (strSKU == string.Empty)
                            strSKU = lblODSKU.Text;
                        else
                            strSKU = strSKU + "," + lblODSKU.Text;
                    }
                }
            }
        }
        return strSKU;
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        //Button btn = (Button)this.FindControl(Convert.ToString(ViewState["btnExpediteID"]));
        //Expedite(btn);
        SetExpediteDetails();
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        mdNoShow.Hide();
        mdlExpeditePopup.Hide();
        isExpedite = false;
    }

    protected void btnExpedite_Click(object sender, EventArgs e)
    {

        Button btn = (Button)sender;
        GridView grdPotential = (GridView)btn.Parent.Parent.Parent.Parent;

        bool isSkuSelected = false;

        foreach (GridViewRow gvrow in grdPotential.Rows)
        {
            CheckBox chkSelect = (CheckBox)gvrow.FindControl("chkSelect");

            if (chkSelect != null)
            {
                if (chkSelect.Checked)
                {
                    isSkuSelected = true;
                }
            }
        }

        if (!isSkuSelected)
            return;

        ViewState["btnExpediteID"] = btn.UniqueID;

        string strSKus = CheckPO(sender);

        if (strSKus != string.Empty)
        {
            ltNoPo.Text = strSKus + " SKUs has no purchase order, do you want to proceed?";
            mdNoShow.Show();
        }
        else
        {

            SetExpediteDetails();

        }
    }

    private void SetExpediteDetails()
    {
        isExpedite = true;
        string[] SPdetails = GetStockPlanner();
        ucSP.ClearAndBindStockPlanner(SPdetails[2], !string.IsNullOrEmpty(SPdetails[1]) ? SPdetails[1].Split(' ')[0] : "", SPdetails[1]);
        pnlManageLetter.Visible = true;
        pnlDisplayLetter.Visible = false;
        dvDisplayLetter.InnerHtml = txtEmailCommunication.Text = ((BaseControlLibrary.ucTextbox)ucSP.FindControl("txtStockPlanner")).Text = txtSentTo.Text = txtSubjectText.Text = string.Empty;
        txtSubjectText.Text = WebCommon.getGlobalResourceValue("ExpediteSubject") + ' ' + '-' + ' ' + Convert.ToString(ViewState["VendorDetails"]);
        txtSentTo.Text = GetVendorEmails();
        //txtSentBy.Text = GetStockPlanner();
        ((BaseControlLibrary.ucTextbox)ucSP.FindControl("txtStockPlanner")).Text = SPdetails[0];
        mdlExpeditePopup.Show();
    }

    private string GetVendorEmails()
    {
        string Emails = string.Empty;

        Button btn = (Button)this.FindControl(Convert.ToString(ViewState["btnExpediteID"]));

        GridView grdPotential = (GridView)btn.Parent.Parent.Parent.Parent;
        foreach (GridViewRow gvrow in grdPotential.Rows)
        {

            CheckBox chkSelect = (CheckBox)gvrow.FindControl("chkSelect");
            if (chkSelect != null)
            {
                if (chkSelect.Checked)
                {
                    HiddenField hdnVendorID = (HiddenField)gvrow.FindControl("hdnVendor");
                    Emails = SendExpediteMail(Convert.ToInt32(hdnVendorID.Value), "ToAddress");
                    break;
                }
            }
        }

        return Emails;
    }

    private string[] GetStockPlanner()
    {
        string[] StockPlanner = new string[3];



        Button btn = (Button)this.FindControl(Convert.ToString(ViewState["btnExpediteID"]));

        GridView grdPotential = (GridView)btn.Parent.Parent.Parent.Parent;
        foreach (GridViewRow gvrow in grdPotential.Rows)
        {

            CheckBox chkSelect = (CheckBox)gvrow.FindControl("chkSelect");
            if (chkSelect != null)
            {
                if (chkSelect.Checked)
                {
                    Label lblStockPlannerNoValue = (Label)gvrow.FindControl("lblStockPlannerNoValue");
                    Label lblStockPlannerNameValue = (Label)gvrow.FindControl("lblStockPlannerNameValue");
                    Label lblStockPlannerCountry = (Label)gvrow.FindControl("lblStockPlannerCountry");
                    HiddenField hdnStockPlannerID = (HiddenField)gvrow.FindControl("hdnStockPlannerID");
                    Label lblVendorNoValue = (Label)gvrow.FindControl("lblVendorNoValue");
                    Label lblVendorNameValue = (Label)gvrow.FindControl("lblVendorNameValue");

                    StockPlanner[0] = "(" + lblStockPlannerCountry.Text + ") " + lblStockPlannerNoValue.Text + " - " + lblStockPlannerNameValue.Text;
                    StockPlanner[1] = lblStockPlannerNameValue.Text;
                    StockPlanner[2] = hdnStockPlannerID.Value;
                    ViewState["VendorDetails"] = lblVendorNoValue.Text + ' ' + '-' + ' ' + lblVendorNameValue.Text;
                    break;
                }
            }
        }


        return StockPlanner;
    }

    private void Expedite(object sender)
    {
        ExpediteStockBE objExpediteStock = new ExpediteStockBE();
        ExpediteStockBE oNewbjExpediteStock = new ExpediteStockBE();
        Utilities.clsEmail oSendComm = new Utilities.clsEmail();
        ExpediteStockBAL objExpditeBAL = new ExpediteStockBAL();
        int? result = null;
        int count = 0;
        Button btn = (Button)sender;
        GridView grdPotential = (GridView)btn.Parent.Parent.Parent.Parent;

        //Creating Datatable to seleced records
        dtExpediteStock.Columns.Add("ChkSelect");
        dtExpediteStock.Columns.Add("ODSkU");
        dtExpediteStock.Columns.Add("VikingSKU");
        dtExpediteStock.Columns.Add("VendorCode");
        dtExpediteStock.Columns.Add("Description");
        dtExpediteStock.Columns.Add("PurchaseOrder");
        dtExpediteStock.Columns.Add("OriginalDueDate");
        dtExpediteStock.Columns.Add("SiteName");
        dtExpediteStock.Columns.Add("CommentId");

        foreach (GridViewRow gvrow in grdPotential.Rows)
        {
            CheckBox chkSelect = (CheckBox)gvrow.FindControl("chkSelect");
            Label lblODSKU = (Label)gvrow.FindControl("lblOD_CodeValue");
            Label lblVikingCode = (Label)gvrow.FindControl("lblVikingCodeValue");
            Label lblVendorCode = (Label)gvrow.FindControl("lblVendorCodeValue");
            Label lblProductDesc = (Label)gvrow.FindControl("lblProductDesValue");
            HiddenField hdnPurchaseOrder = (HiddenField)gvrow.FindControl("hdnPurchase_order");
            HiddenField hdnOriginal_due_date = (HiddenField)gvrow.FindControl("hdnOriginal_due_date");
            Label lblSiteName = (Label)gvrow.FindControl("lblSiteNameValue");
            HiddenField hdnCommentID = (HiddenField)gvrow.FindControl("hdnCommentID");

            if (chkSelect != null)
            {
                if (chkSelect.Checked)
                {
                    dtExpediteStock.Rows.Add(chkSelect,
                                            lblODSKU.Text,
                                            lblVikingCode.Text,
                                            lblVendorCode.Text,
                                            lblProductDesc.Text,
                                            !string.IsNullOrEmpty(hdnPurchaseOrder.Value) ? hdnPurchaseOrder.Value : string.Empty,
                                            !string.IsNullOrEmpty(hdnOriginal_due_date.Value) ? hdnOriginal_due_date.Value : string.Empty,
                                            !string.IsNullOrEmpty(lblSiteName.Text) ? lblSiteName.Text : string.Empty,
                                            !string.IsNullOrEmpty(hdnCommentID.Value) ? hdnCommentID.Value : string.Empty
                                            );
                }
            }
        }
        dsExpediteStock.Tables.Add(dtExpediteStock);

        foreach (GridViewRow gvrow in grdPotential.Rows)
        {

            CheckBox chkSelect = (CheckBox)gvrow.FindControl("chkSelect");
            int RowIndex = gvrow.RowIndex;
            HiddenField hdnVendorID = (HiddenField)gvrow.FindControl("hdnVendor");
            HiddenField hdnSiteId = (HiddenField)gvrow.FindControl("hdnSiteId");
            HiddenField hdnSkuID = (HiddenField)gvrow.FindControl("hdnSkuId");
            ucLabel lblOD_CodeValue = (ucLabel)gvrow.FindControl("lblOD_CodeValue");
            ucLabel lblVikingCodeValue = (ucLabel)gvrow.FindControl("lblVikingCodeValue");


            if (ViewState["sentMailVendorId"] != null)
            {
                if (ViewState["sentMailVendorId"].ToString() == hdnVendorID.Value)
                {
                    ViewState["sentMailVendorId"] = hdnVendorID.Value;

                }
                else if (ViewState["sentMailVendorId"].ToString() != hdnVendorID.Value)
                {
                    ViewState["sentMailVendorId"] = null;
                }
            }

            if (chkSelect != null)
            {
                if (chkSelect.Checked)
                {
                    HiddenField hdnCommentID = (HiddenField)gvrow.FindControl("hdnCommentID");
                    string cmmntID = hdnCommentID.Value;
                    objExpediteStock.CommentID = Convert.ToInt32(cmmntID);
                    if (Session["UserID"] != null)
                        objExpediteStock.UserID = Convert.ToInt32(Session["UserID"]);
                    objExpediteStock.Comm_Status = true;
                    objExpediteStock.Action = "SaveExpediteSKU";

                    if (!string.IsNullOrEmpty(hdnVendorID.Value))
                    {
                        objExpediteStock.VendorID = Convert.ToInt32(hdnVendorID.Value);
                        strVendorIds += hdnVendorID.Value + ",";
                    }

                    if (!string.IsNullOrEmpty(hdnSiteId.Value))
                        objExpediteStock.SiteID = Convert.ToInt32(hdnSiteId.Value);

                    if (!string.IsNullOrEmpty(hdnSkuID.Value))
                        objExpediteStock.SkuID = Convert.ToInt32(hdnSkuID.Value);

                    if (!string.IsNullOrEmpty(lblVikingCodeValue.Text))
                        objExpediteStock.Viking_Code = Convert.ToString(lblVikingCodeValue.Text);

                    if (!string.IsNullOrEmpty(lblOD_CodeValue.Text))
                        objExpediteStock.OD_Code = Convert.ToString(lblOD_CodeValue.Text);

                    //Save Expedite SKU.
                    result = objExpditeBAL.SaveExpediteSKUBAL(objExpediteStock);

                    if (result > 0 && result != null)
                    {
                        //Save and Send Communication.
                        if (ViewState["sentMailVendorId"] == null)
                        {
                            if (string.IsNullOrEmpty(VendorEmailID) || VendorEmailID != hdnVendorID.Value)
                            {
                                SendExpediteMail(Convert.ToInt32(hdnVendorID.Value), Convert.ToInt32(hdnSiteId.Value), dsExpediteStock);
                                ViewState["sentMailVendorId"] = hdnVendorID.Value;
                                VendorEmailID = hdnVendorID.Value;
                            }
                        }
                        //Send Mail to stock planner
                        if (!(string.IsNullOrEmpty(ucSP.SelectedSPEmail)))
                        {
                            if (string.IsNullOrEmpty(VendorEmailSPID) || VendorEmailSPID != ucSP.SelectedSPEmail.Trim())
                            {
                                SendMailToVendor(ucSP.SelectedSPEmail.Trim(), "English", dsExpediteStock, Convert.ToInt32(hdnVendorID.Value));
                                VendorEmailSPID = ucSP.SelectedSPEmail.Trim();
                            }
                        }
                    }
                }
                else
                {
                    count = count + 1;
                }
            }
        }

        if (grdPotential.Rows.Count == count)
        {

            //Validation to choose atleast one check box.
        }
        // BindPotentialOutputGrid();
    }

    private void SendExpediteMail(int VendorId, int SiteId, DataSet dsExpediteStock)
    {
        try
        {

            SendMailToVendor(txtSentTo.Text.Trim(), "English", dsExpediteStock, VendorId);


            //string EmailAddressList = string.Empty;
            //string Language = string.Empty;
            //ExpediteStockBE objExpediteStockBE = new ExpediteStockBE();
            //ExpediteStockBAL objExpediteStockBAL = new ExpediteStockBAL();

            //objExpediteStockBE.Action = "GetVendorLanguage";
            //objExpediteStockBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            //objExpediteStockBE.Vendor.VendorID = VendorId;

            //DataTable dtVendorLang = objExpediteStockBAL.GetVendorLanguageBAL(objExpediteStockBE);


            //if (dtVendorLang != null && dtVendorLang.Rows.Count > 0) {
            //    for (int i = 0; i < dtVendorLang.Rows.Count; i++) {
            //        if (!string.IsNullOrEmpty(dtVendorLang.Rows[i]["EmailAddress"].ToString())) {
            //            EmailAddressList += dtVendorLang.Rows[i]["EmailAddress"].ToString() + ",";
            //            Language = dtVendorLang.Rows[i]["Language"].ToString();
            //        }
            //        else {
            //            SendMailToVendor(txtSentTo.Text.Trim(), "English", dsExpediteStock);
            //        }
            //    }
            //    SendMailToVendor(txtSentTo.Text.Trim(), Language, dsExpediteStock);
            //}
            //else
            //    SendMailToVendor(txtSentTo.Text.Trim(), "English", dsExpediteStock);

        }
        catch { }
    }

    private void SendMailToVendor(string toAddress, string language, DataSet dsExpediteStockMain, int VendorId)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {

            string htmlBody = string.Empty;

            string CommentsIds = string.Empty;

            //string templatePath = null;
            //templatePath = path + templatePathName;
            //string LanguageFile = string.Empty;

            //LanguageFile = "Expedite.english.htm";

            string[] VendorHTML = GetVendorMailHTML(toAddress, language, dsExpediteStockMain, VendorId);
            htmlBody = VendorHTML[0].ToString();
            CommentsIds = VendorHTML[1].ToString();

            string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
            clsEmail oclsEmail = new clsEmail();
            oclsEmail.sendMail(toAddress, htmlBody, txtSubjectText.Text.Trim(), sFromAddress, true);
            saveMail(txtSubjectText.Text.Trim(), toAddress, htmlBody, language, CommentsIds);
            Page.UICulture = Convert.ToString(Session["CultureInfo"]);
        }
    }

    public int? saveMail(string mailSubject, string sentTo, string mailBody, string Language, string CommentIds)
    {
        ExpediteStockBE objExpediteStockBE = new ExpediteStockBE();
        ExpediteStockBAL objExpediteStockBAL = new ExpediteStockBAL();

        objExpediteStockBE.Action = "saveExpediteMail";

        objExpediteStockBE.Subject = mailSubject;
        objExpediteStockBE.SentTo = sentTo;
        objExpediteStockBE.Body = mailBody;
        objExpediteStockBE.Comm_By = Session["Username"].ToString();
        objExpediteStockBE.Comm_On = System.DateTime.Now;
        objExpediteStockBE.language = Language;
        objExpediteStockBE.CommentIDs = CommentIds;

        return objExpediteStockBAL.SaveExpCommunicationBAL(objExpediteStockBE);
    }

    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)ucIncludeVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)ucIncludeVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                ucIncludeVendor.innerControlHiddenField.Value = string.Empty;
                ucIncludeVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    ucIncludeVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    ucIncludeVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            ucIncludeVendor.innerControlHiddenField.Value = string.Empty;
            ucIncludeVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void rptExpedite_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            GridView gvPotential = (GridView)e.Item.FindControl("gvPotentialOutput");
            HiddenField hdnVendorId = (HiddenField)e.Item.FindControl("hdnVendorID");
            List<ExpediteStockBE> lstobjExpStock = new List<ExpediteStockBE>();

            if (ViewState["ExpediteStocklst1"] != null)
            {
                lstobjExpStock = ViewState["ExpediteStocklst1"] as List<ExpediteStockBE>;
                int VendorID = Convert.ToInt32(hdnVendorId.Value);

                var groupedCustomerList = lstobjExpStock.Where(id => id.VendorID == VendorID)
                   .GroupBy(u => u.VendorID)
                   .Select(grp => grp.ToList())
                   .ToList();

                if (!idList.Contains(Convert.ToString(VendorID)))
                {
                    gvPotential.DataSource = groupedCustomerList[0];
                    gvPotential.DataBind();
                }
                //Check For Duplicate Vendor ID : -  
                idList.Add(hdnVendorId.Value);
            }
        }
    }

    protected void gvPotentialOutput_OnRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == System.Web.UI.WebControls.DataControlRowType.DataRow)
        {
            CheckBox chkb = (CheckBox)e.Row.FindControl("chkSelect");
            BaseControlLibrary.ucLabel lblStatusValue = (BaseControlLibrary.ucLabel)e.Row.FindControl("lblStatusValue");


            //Button btn = (Button)this.FindControl(Convert.ToString(ViewState["btnExpediteID"]));
            //GridView grdPotential = (GridView)btn.Parent.Parent.Parent.Parent;
            //HiddenField hdnVendorID = (HiddenField)gvrow.FindControl("hdnVendor");
            //HiddenField hdnSiteId = (HiddenField)gvrow.FindControl("hdnSiteId");
            System.Web.UI.HtmlControls.HtmlAnchor lnk = (System.Web.UI.HtmlControls.HtmlAnchor)e.Row.FindControl("lnk");
            HiddenField hdfVendorId = (HiddenField)e.Row.FindControl("hdnVendor");
            if (lblStatusValue.Text.ToLower() == "sent to vendor")
            {
                chkb.Enabled = false;
                lnk.Attributes.Add("href", EncryptQuery("ShowCommunication.aspx?CommunicationID=" + ((ExpediteStockBE)(e.Row.DataItem)).CommunicationID.ToString()));
                lnk.InnerText = "sent to vendor";
                lblStatusValue.Visible = false;
            }
            //else {
            //    lnk.Attributes.Add("onclick", "javascript:;");
            //}
        }
        //Get data row view
        //DataRowView drview = e.Row.DataItem as DataRowView;

        //if (e.Row.RowType == DataControlRowType.Header)
        //{
        //    //Find checkbox and checked/Unchecked based on values
        //    CheckBox chkb = (CheckBox)e.Row.FindControl("chkSelectAll");
        //    //if (chkb.Checked)
        //    //{

        //    //}
        //}
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{         
        //    //Find checkbox and checked/Unchecked based on values
        //    CheckBox chkb = (CheckBox)e.Row.FindControl("chkSelect");

        //}

        //int index = e.Row.RowIndex;

        //CheckBox chk = (CheckBox)e.Row.FindControl("chkSelect"); 

    }

    //--------------Start Phase 11 Point 7-------------------------//
    protected void btnSubmit_Click(object sender, EventArgs e)
    {


        if (string.IsNullOrEmpty(txtSubjectText.Text) || string.IsNullOrWhiteSpace(txtSubjectText.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + emailSubjectMandatory + "')", true);
            txtSubjectText.Focus();
            return;
        }

        if (string.IsNullOrEmpty(txtEmailCommunication.Text) || string.IsNullOrWhiteSpace(txtEmailCommunication.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + emailCommunicationMandatory + "')", true);
            txtEmailCommunication.Focus();
            return;
        }

        //if (string.IsNullOrEmpty(txtSentBy.Text) || string.IsNullOrWhiteSpace(txtSentBy.Text)) {
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + sentonBehalfofMandatory + "')", true);
        //    txtSentBy.Focus();
        //    return;
        //}



        if (string.IsNullOrEmpty(((BaseControlLibrary.ucTextbox)ucSP.FindControl("txtStockPlanner")).Text) || string.IsNullOrWhiteSpace(((BaseControlLibrary.ucTextbox)ucSP.FindControl("txtStockPlanner")).Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + sentonBehalfofMandatory + "')", true);
            ((BaseControlLibrary.ucTextbox)ucSP.FindControl("txtStockPlanner")).Focus();
            return;
        }

        string MailHTML = GetExpedite();
        dvDisplayLetter.InnerHtml = MailHTML;

        pnlManageLetter.Visible = false;
        pnlDisplayLetter.Visible = true;
        mdlExpeditePopup.Show();
    }

    protected void btnBack_2_Click(object sender, EventArgs e)
    {
        isExpedite = false;
        mdlExpeditePopup.Hide();
    }

    protected void btnYes_Click(object sender, EventArgs e)
    {
        Button btn = (Button)this.FindControl(Convert.ToString(ViewState["btnExpediteID"]));
        Expedite(btn);
        isExpedite = false;
        mdlExpeditePopup.Hide();
        ViewState["sentMailVendorId"] = false;
        if (ViewState["RadioButton"] != null)
        {
            if (Convert.ToString(ViewState["RadioButton"]) == "rdoShowAll")
            {
                rdoShowAll_CheckedChanged(sender, e);
            }
            if (Convert.ToString(ViewState["RadioButton"]) == "rdoShowOnlyOpen")
            {
                ViewState["ExpediteStocklstForOpen"] = null;
                BindPotentialOutputGrid();
            }
        }

    }

    protected void btnNo_Click(object sender, EventArgs e)
    {
        pnlManageLetter.Visible = true;
        pnlDisplayLetter.Visible = false;
    }

    private string GetExpedite()
    {
        string HTML = string.Empty;

        Button btn = (Button)this.FindControl(Convert.ToString(ViewState["btnExpediteID"]));


        ExpediteStockBE objExpediteStock = new ExpediteStockBE();
        ExpediteStockBE oNewbjExpediteStock = new ExpediteStockBE();
        Utilities.clsEmail oSendComm = new Utilities.clsEmail();
        ExpediteStockBAL objExpditeBAL = new ExpediteStockBAL();
        int? result = null;
        int count = 0;
        //Button btn = (Button)gridsender;
        GridView grdPotential = (GridView)btn.Parent.Parent.Parent.Parent;

        //Creating Datatable to seleced records
        dtExpediteStock.Columns.Add("ChkSelect");
        dtExpediteStock.Columns.Add("ODSkU");
        dtExpediteStock.Columns.Add("VikingSKU");
        dtExpediteStock.Columns.Add("VendorCode");
        dtExpediteStock.Columns.Add("Description");
        dtExpediteStock.Columns.Add("PurchaseOrder");
        dtExpediteStock.Columns.Add("OriginalDueDate");
        dtExpediteStock.Columns.Add("SiteName");
        dtExpediteStock.Columns.Add("CommentId");
        //dtExpediteStock.Columns.Add("StockPlannerID");

        foreach (GridViewRow gvrow in grdPotential.Rows)
        {
            CheckBox chkSelect = (CheckBox)gvrow.FindControl("chkSelect");
            Label lblODSKU = (Label)gvrow.FindControl("lblOD_CodeValue");
            Label lblVikingCode = (Label)gvrow.FindControl("lblVikingCodeValue");
            Label lblVendorCode = (Label)gvrow.FindControl("lblVendorCodeValue");
            Label lblProductDesc = (Label)gvrow.FindControl("lblProductDesValue");
            HiddenField hdnPurchaseOrder = (HiddenField)gvrow.FindControl("hdnPurchase_order");
            HiddenField hdnOriginal_due_date = (HiddenField)gvrow.FindControl("hdnOriginal_due_date");
            Label lblSiteName = (Label)gvrow.FindControl("lblSiteNameValue");
            HiddenField hdnCommentID = (HiddenField)gvrow.FindControl("hdnCommentID");
            //HiddenField hdnStockPlannerID = (HiddenField)gvrow.FindControl("hdnStockPlannerID");

            if (chkSelect != null)
            {
                if (chkSelect.Checked)
                {
                    dtExpediteStock.Rows.Add(chkSelect,
                                            lblODSKU.Text,
                                            lblVikingCode.Text,
                                            lblVendorCode.Text,
                                            lblProductDesc.Text,
                                            !string.IsNullOrEmpty(hdnPurchaseOrder.Value) ? hdnPurchaseOrder.Value : string.Empty,
                                            !string.IsNullOrEmpty(hdnOriginal_due_date.Value) ? hdnOriginal_due_date.Value : string.Empty,
                                            !string.IsNullOrEmpty(lblSiteName.Text) ? lblSiteName.Text : string.Empty,
                                            !string.IsNullOrEmpty(hdnCommentID.Value) ? hdnCommentID.Value : string.Empty
                                            //,!string.IsNullOrEmpty(hdnStockPlannerID.Value) ? hdnStockPlannerID.Value : string.Empty
                                            );
                }
            }
        }
        dsExpediteStock.Tables.Add(dtExpediteStock);

        foreach (GridViewRow gvrow in grdPotential.Rows)
        {

            CheckBox chkSelect = (CheckBox)gvrow.FindControl("chkSelect");
            int RowIndex = gvrow.RowIndex;
            HiddenField hdnVendorID = (HiddenField)gvrow.FindControl("hdnVendor");
            HiddenField hdnSiteId = (HiddenField)gvrow.FindControl("hdnSiteId");

            if (chkSelect != null)
            {
                if (chkSelect.Checked)
                {
                    HTML = SendExpediteMail(Convert.ToInt32(hdnVendorID.Value));
                }
            }
        }
        return HTML;
    }

    private string SendExpediteMail(int VendorId, string returnContent = "EmailBody")
    {
        string HTML = string.Empty;
        try
        {
            string EmailAddressList = string.Empty;
            string Language = string.Empty;
            ExpediteStockBE objExpediteStockBE = new ExpediteStockBE();
            ExpediteStockBAL objExpediteStockBAL = new ExpediteStockBAL();

            objExpediteStockBE.Action = "GetVendorLanguage";
            objExpediteStockBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            objExpediteStockBE.Vendor.VendorID = VendorId;

            DataTable dtVendorLang = objExpediteStockBAL.GetVendorLanguageBAL(objExpediteStockBE);

            if (dtVendorLang != null && dtVendorLang.Rows.Count > 0)
            {
                for (int i = 0; i < dtVendorLang.Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(dtVendorLang.Rows[i]["EmailAddress"].ToString()))
                    {
                        EmailAddressList += dtVendorLang.Rows[i]["EmailAddress"].ToString() + ",";
                        Language = dtVendorLang.Rows[i]["Language"].ToString();
                    }
                }

                if (string.IsNullOrEmpty(EmailAddressList.Trim()))
                {
                    //SendMailToVendor(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", dsExpediteStock);
                    EmailAddressList = ConfigurationManager.AppSettings["DefaultEmailAddress"];
                    Language = "English";
                }

                if (returnContent == "EmailBody")
                    HTML = GetVendorMailHTML(EmailAddressList.TrimEnd(','), Language, dsExpediteStock, VendorId)[0].ToString();
                if (returnContent == "ToAddress")
                    HTML = EmailAddressList.TrimEnd(',');
            }
            else
            {
                if (returnContent == "EmailBody")
                    HTML = GetVendorMailHTML(ConfigurationManager.AppSettings["DefaultEmailAddress"], "English", dsExpediteStock, VendorId)[0].ToString();
                if (returnContent == "ToAddress")
                    HTML = ConfigurationManager.AppSettings["DefaultEmailAddress"];
            }
        }
        catch { }

        return HTML;
    }

    private string[] GetVendorMailHTML(string toAddress, string language, DataSet dsExpediteStockMain, int VendorId)
    {


        string[] VendorHTML = new string[2];
        VendorHTML[0] = string.Empty;
        VendorHTML[1] = string.Empty;
        var sendComm = new sendCommunicationCommon();

        //************************************
        ExpediteStockBE objExpediteStockBE = new ExpediteStockBE();
        ExpediteStockBAL objExpediteStockBAL = new ExpediteStockBAL();
        objExpediteStockBE.Action = "GetVendorLanguage";
        objExpediteStockBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        objExpediteStockBE.Vendor.VendorID = VendorId;
        objExpediteStockBE.Comments = "Vendor";
        DataTable dtVendorLang = objExpediteStockBAL.GetVendorLanguageBAL(objExpediteStockBE);
        string VendorCode1 = string.Empty;// dtVendorLang.Rows[0]["VendorCode"].ToString();
        string VendorName1 = string.Empty; //dtVendorLang.Rows[0]["VendorName"].ToString();
        if (dtVendorLang.Rows.Count > 0)
        {
            VendorCode1 = dtVendorLang.Rows[0]["VendorCode"].ToString();
            VendorName1 = dtVendorLang.Rows[0]["VendorName"].ToString();
        }
        //**************************************
        if (!string.IsNullOrEmpty(toAddress))
        {
            string htmlBody = string.Empty;

            sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
            string CommentsIds = string.Empty;

            string templatePath = null;
            templatePath = path + templatePathName;
            string LanguageFile = string.Empty;

            LanguageFile = "Expedite.english.htm";

            #region Setting reason as per the language ...
            switch (language)
            {
                case clsConstants.English:
                    Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                    break;
                case clsConstants.French:
                    Page.UICulture = clsConstants.FranceISO; // // France
                    break;
                case clsConstants.German:
                    Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                    break;
                case clsConstants.Dutch:
                    Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                    break;
                case clsConstants.Spanish:
                    Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                    break;
                case clsConstants.Italian:
                    Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                    break;
                case clsConstants.Czech:
                    Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                    break;
                default:
                    Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                    break;
            }

            #endregion

            using (StreamReader sReader = new StreamReader(templatePath + LanguageFile.ToLower()))
            {
                #region mailBody
                htmlBody = sReader.ReadToEnd();
                //*******************
                htmlBody = htmlBody.Replace("{Vendor}", WebCommon.getGlobalResourceValue("Vendor"));
                htmlBody = htmlBody.Replace("{VendorCode}", VendorCode1);
                htmlBody = htmlBody.Replace("{VendorName}", WebCommon.getGlobalResourceValue("VendorName"));
                htmlBody = htmlBody.Replace("{VendorName1}", VendorName1);
                htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));
                htmlBody = htmlBody.Replace("{Date1}", DateTime.Now.ToString("dd/MM/yy"));
                //*********************
                htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                htmlBody = htmlBody.Replace("{ExpediteText1}", txtEmailCommunication.Text.Trim());
                htmlBody = htmlBody.Replace("{ExpediteText4}", WebCommon.getGlobalResourceValue("ExpediteText4"));
                htmlBody = htmlBody.Replace("{YoursSincerely}", WebCommon.getGlobalResourceValue("YoursSincerely"));
                //htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots"));
                if (((BaseControlLibrary.ucTextbox)ucSP.FindControl("txtStockPlanner")).Text.Split('-').Length > 1)
                {
                    htmlBody = htmlBody.Replace("{StockPlannerName}", ((BaseControlLibrary.ucTextbox)ucSP.FindControl("txtStockPlanner")).Text.Split('-')[1].Trim());
                }
                else
                {
                    htmlBody = htmlBody.Replace("{StockPlannerName}", ((BaseControlLibrary.ucTextbox)ucSP.FindControl("txtStockPlanner")).Text.Trim());
                }
                htmlBody = htmlBody.Replace("{StockPlannerNumber}", ucSP.SelectedSPPhone);
                // htmlBody = htmlBody.Replace("{StockPlannerNumber}", string.Empty);
                string Sitenamevalue = string.Empty;
                StringBuilder sProductDetail = new StringBuilder();

                // Selecting Distinct Sites
                var distinctSites = dsExpediteStockMain.Tables[0].AsEnumerable()
                    .Select(s => new
                    {
                        SiteName = s.Field<string>("SiteName"),
                    })
                    .Distinct().ToList();

                if (distinctSites != null && distinctSites.Count > 0)
                {
                    for (int j = 0; j < distinctSites.Count; j++)
                    {
                        //selecting records for site
                        var dtExpediteSiteTemp = dsExpediteStockMain.Tables[0].AsEnumerable().
                            Where(r => r.Field<string>("SiteName") == Convert.ToString(distinctSites[j].SiteName)).CopyToDataTable();

                        if (dtExpediteSiteTemp != null && dtExpediteSiteTemp.Rows.Count > 0)
                        {
                            string Site = dtExpediteSiteTemp.Rows[0]["SiteName"].ToString();
                            sProductDetail.Append("<table style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=90%'>");
                            sProductDetail.Append("<tr><td colspan='6' style='font-size:12;font-family:Arial;font-weight: bold ;'>" + Site + "</td></tr>");
                            sProductDetail.Append("</table>");
                            sProductDetail.Append("<table style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=90%'>");
                            sProductDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='15%'>" + ODSKU + "</cc1:ucLabel></td><td width='15%'>" + VikingSKU + "</td><td width='15%'>" + VendorItemCode + "</td><td  width='20%'>" + Description + "</td><td  width='15%'>" + EarliestOpenPONumber + "</td><td  width='20%'>" + OrignalDuedate + "</td></tr>");

                            for (int i = 0; i < dtExpediteSiteTemp.Rows.Count; i++)
                            {
                                string ODSkU = dtExpediteSiteTemp.Rows[i]["ODSkU"].ToString();
                                string VikingSKU2 = dtExpediteSiteTemp.Rows[i]["VikingSKU"].ToString();
                                string VendorCode = dtExpediteSiteTemp.Rows[i]["VendorCode"].ToString();
                                string ProdDesc = dtExpediteSiteTemp.Rows[i]["Description"].ToString();
                                string PurchaseOrder = dtExpediteSiteTemp.Rows[i]["PurchaseOrder"].ToString();
                                string OriginalDueDate = dtExpediteSiteTemp.Rows[i]["OriginalDueDate"].ToString();
                                CommentsIds += dtExpediteSiteTemp.Rows[i]["CommentId"].ToString() + ",";
                                sProductDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + ODSkU + "</td><td>" + VikingSKU2 + "</td><td>" + VendorCode);
                                sProductDetail.Append("</td><td>" + ProdDesc + "</td><td>" + PurchaseOrder + "</td><td>" + OriginalDueDate + "</td></tr>");
                            }

                            sProductDetail.Append("</table>");
                        }
                        dtExpediteSiteTemp = null;
                    }
                }

                if (!string.IsNullOrEmpty(sProductDetail.ToString()))
                    htmlBody = htmlBody.Replace("{ProductGridValue}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br />" + sProductDetail + "</td></tr>");
                else
                    htmlBody = htmlBody.Replace("{ProductGridValue}", "");
                #endregion

            }

            htmlBody = htmlBody.Replace("{logoInnerPath}", sendComm.getAbsolutePath());
            VendorHTML[0] = htmlBody;
            VendorHTML[1] = CommentsIds;
        }
        //return htmlBody;
        return VendorHTML;
        //-----------------------------------------------------//
    }
    protected void rdoShowOnlyOpen_CheckedChanged(object sender, EventArgs e)
    {
        var lstExpediteStock = new List<ExpediteStockBE>();
        var oNewExpediteStocklst = new List<ExpediteStockBE>();
        var oNewExpediateStockBE = new ExpediteStockBE();
        var oExpStockBAL = new ExpediteStockBAL();
        oNewExpediateStockBE.Action = "GetExpediateStockForOpenPO";
        oNewExpediateStockBE.SelectedIncludedVendorIDs = ucIncludeVendor.SelectedVendorIDs;
        oNewExpediateStockBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oNewExpediateStockBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        oNewExpediateStockBE.VendorName = strVendorIds;
        if (ViewState["ExpediteStocklstForOpen"] == null)
        {
            oNewExpediteStocklst = oExpStockBAL.GetExpediateStockBAL(oNewExpediateStockBE);
            ViewState["ExpediteStocklstForOpen"] = oNewExpediteStocklst;
            ViewState["ExpediteStocklst1"] = oNewExpediteStocklst;
        }
        else
        {
            oNewExpediteStocklst = (List<ExpediteStockBE>)ViewState["ExpediteStocklstForOpen"];
            ViewState["ExpediteStocklst1"] = ViewState["ExpediteStocklstForOpen"];
        }

        ucExportToExcel1.Visible = false;
        if (oNewExpediteStocklst.Count > 0 && oNewExpediteStocklst != null)
        {
            gvExcel.DataSource = oNewExpediteStocklst;
            gvExcel.DataBind();

            ucExportToExcel1.Visible = true;

            var lstVendorIds = oNewExpediteStocklst.Select
                (o => new ExpediteStockBE
                {
                    VendorID = o.VendorID
                }
                ).Distinct().ToList();

            rptExpedite.DataSource = lstVendorIds;
            rptExpedite.DataBind();

        }
        else
        {
            rptExpedite.DataSource = null;
            rptExpedite.DataBind();
        }
        ViewState["RadioButton"] = "rdoShowOnlyOpen";

    }
    protected void rdoShowAll_CheckedChanged(object sender, EventArgs e)
    {
        //var oNewExpediteStocklst = new List<ExpediteStockBE>();
        //oNewExpediteStocklst = (List<ExpediteStockBE>)ViewState["ExpediteStocklst"];
        //ucExportToExcel1.Visible = false;
        //ViewState["ExpediteStocklst1"] = ViewState["ExpediteStocklst"];
        var lstExpediteStock = new List<ExpediteStockBE>();
        var oNewExpediteStocklst = new List<ExpediteStockBE>();
        var oNewExpediateStockBE = new ExpediteStockBE();
        var oExpStockBAL = new ExpediteStockBAL();
        oNewExpediateStockBE.Action = "GetExpediateStock";
        oNewExpediateStockBE.SelectedIncludedVendorIDs = ucIncludeVendor.SelectedVendorIDs;
        oNewExpediateStockBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oNewExpediateStockBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        oNewExpediateStockBE.VendorName = strVendorIds;
        oNewExpediteStocklst = oExpStockBAL.GetExpediateStockBAL(oNewExpediateStockBE);
        ViewState["ExpediteStocklst"] = oNewExpediteStocklst;
        ViewState["ExpediteStocklst1"] = oNewExpediteStocklst;
        ucExportToExcel1.Visible = false;
        if (oNewExpediteStocklst.Count > 0 && oNewExpediteStocklst != null)
        {
            gvExcel.DataSource = oNewExpediteStocklst;
            gvExcel.DataBind();

            ucExportToExcel1.Visible = true;

            var lstVendorIds = oNewExpediteStocklst.Select
                (o => new ExpediteStockBE
                {
                    VendorID = o.VendorID
                }
                ).Distinct().ToList();
            rptExpedite.DataSource = lstVendorIds;
            rptExpedite.DataBind();

        }
        else
        {
            rptExpedite.DataSource = null;
            rptExpedite.DataBind();
        }
        ViewState["RadioButton"] = "rdoShowAll";

    }
}

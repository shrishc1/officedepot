﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.Upload;
using Utilities;
using System.Data;
using WebUtilities;
using System.Globalization;
using System.Text;

public partial class ExpediteComments : CommonPage
{
    int SKUID = 0;
    protected string NoCommentsForSKU = WebCommon.getGlobalResourceValue("NoCommentsExits");
    protected string NoExpedite = WebCommon.getGlobalResourceValue("NoExpedite");
    protected string CheckDateForExpediteComment = WebCommon.getGlobalResourceValue("CheckDateForExpediteComments");

    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        if (!IsPostBack)
        {

            pnlLatestUpdate.Visible = true;
            pnlAddComment.Visible = false;
            pnlHistoricComments.Visible = false;
            string[] ArrSkuId = null;
            if (GetQueryStringValue("IsSingleComment") != null)
            {
                lblVikingCod1.Text = GetQueryStringValue("Viking");
                lblOdcode1.Text = GetQueryStringValue("ODCode");
                lblDescription2.Text = GetQueryStringValue("Desc");
                ArrSkuId = GetQueryStringValue("SKUID").TrimEnd(',').Split(',');
                BindLatestComments();
            }
            else
            {
                ExpediteStockBE oSessionQueryString = new ExpediteStockBE();
                oSessionQueryString = (ExpediteStockBE)Session["QueryString"];
                if (Session["QueryString"] != null)
                {
                    ArrSkuId = oSessionQueryString.skuids.TrimEnd(',').Split(',');
                    AddComment();
                }
            }

        }
        ltErrorMsg.Text = CheckDateForExpediteComment;
    }
    private void BindLatestComments()
    {
        var expediteStock = new ExpediteStockBE();
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        lblNoCommentsForSKU.Visible = false;
        BindLatestUpdatePanelCommentsRepeater();
        try
        {
            tblComment.Visible = true;
            expediteStock.Action = "GetLatestComment";
            expediteStock.PurchaseOrder = new Up_PurchaseOrderDetailBE();

            if (GetQueryStringValue("SKUID") != null)
                expediteStock.PurchaseOrder.SKUID = Convert.ToInt32(GetQueryStringValue("SKUID").TrimEnd(','));

            if (GetQueryStringValue("VendorID") != null)
                expediteStock.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").TrimEnd(','));

            var lstExpediteStock = oMAS_ExpStockBAL.GetExpediteCommentsBAL(expediteStock);

            if (lstExpediteStock != null && lstExpediteStock.Count > 0)
            {
                ExpediteStockBE oNewExpediteStock = lstExpediteStock[0];
                DateTime date = Convert.ToDateTime(oNewExpediteStock.CommentsAdded_Date.ToString());
                lblDateCommentwasAddedValue.Text = date.ToString("dd/MM/yyyy HH:mm");
                lblCommentAddedByValue.Text = oNewExpediteStock.CommentsAddedBy;
                lblCommentValue.Text = oNewExpediteStock.Comments;
                lblReasonValue.Text = oNewExpediteStock.Reason;

                string[] ArrSiteId = null;

                if (GetQueryStringValue("IsSingleComment") != null)
                {
                    ArrSiteId = GetQueryStringValue("SiteID").TrimEnd(',').Split(',');
                }
                else
                {
                    if (Session["QueryString"] != null)
                    {
                        ExpediteStockBE oSessionQueryString = (ExpediteStockBE)Session["QueryString"];
                        ArrSiteId = oSessionQueryString.SiteIds.TrimEnd(',').Split(',');
                    }
                }

                //------------Start Phase 15 R1 point 4-------------------------------//
                if (oNewExpediteStock.CommentsValid_Date == null || oNewExpediteStock.CommentsValid_Date.Value.Date >= DateTime.Now.Date)
                {

                    lblCommentValue.Visible = lblReasonValue.Visible = false;
                    txtComments.Visible = ddlReason_1.Visible = btnSaveChanges.Visible = tdSaveChanges.Visible = true;

                    txtComments.Text = oNewExpediteStock.Comments;
                    hdnCommentID.Value = oNewExpediteStock.CommentID.ToString();
                    expediteStock.Action = "GetReason";
                    expediteStock.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    expediteStock.SiteID = ArrSiteId != null && ArrSiteId.Length == 1 && GetQueryStringValue("IsSingleComment") != null
                        ? Convert.ToInt32(GetQueryStringValue("SiteID").TrimEnd(','))
                        : Convert.ToInt32(ArrSiteId[0].ToString());
                    DataTable dtAllType = oMAS_ExpStockBAL.GetReasonBAL(expediteStock);

                    if (dtAllType.Rows.Count > 0)
                    {

                        FillControls.FillDropDown(ref ddlReason_1, dtAllType, "Reason", "ReasonTypeID");
                        ViewState["dtReason"] = dtAllType;
                    }

                    ddlReason_1.SelectedIndex = ddlReason_1.Items.IndexOf(ddlReason_1.Items.FindByValue(oNewExpediteStock.ReasonTypeID.ToString()));

                }
                date = Convert.ToDateTime(oNewExpediteStock.LastEdited.ToString());
                lblLastEditedValue.Text = date.ToString("dd/MM/yyyy HH:mm");
                //------------End Phase 15 R1 point 4-------------------------------//

                ((List<ExpediteStockBE>)Session["ExpediteStockData"]).Where(x => x.SkuID == Convert.ToInt32(GetQueryStringValue("SKUID"))).ToList().ForEach(c =>
                {
                    c.CommentsAddedBy = lblCommentAddedByValue.Text;
                    c.Reason = ddlReason_1.SelectedItem.Text;
                    c.Comments = txtComments.Text;
                });
            }
            else
            {
                tblComment.Visible = false;
                lblNoCommentsForSKU.Visible = true;
                lblNoCommentsForSKU.Text = NoCommentsForSKU;
                lblNoExpedite.Visible = true;
                lblNoExpedite.Text = NoExpedite;
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    private void ExpediteControlVisibility(bool status)
    {
        lblDateTimeWithoutHifan.Visible = status;
        lblDateTimeWithoutHifanColon.Visible = status;
        ltDatePost.Visible = status;
        lblExpeditedBy.Visible = status;
        lblExpeditedByColon.Visible = status;
        lblCommentExpeditedBy.Visible = status;
        lblSentTo.Visible = status;
        lblSentToColon.Visible = status;
        ltMailSentExpAdmin.Visible = status;
        lnkExpeditePopup.Visible = status;
    }

    protected void btnSaveChanges_Click(object sender, EventArgs e)
    {
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        //Save comment.
        ExpediteStockBE oNewExpediteStock = new ExpediteStockBE();
        oNewExpediteStock.PurchaseOrder = new Up_PurchaseOrderDetailBE();
        oNewExpediteStock.Action = "UpdateCommentsById";

        if (ddlReason_1.SelectedValue.ToString() != "")
        {
            oNewExpediteStock.ReasonTypeID = Convert.ToInt32(ddlReason_1.SelectedValue);
        }

        oNewExpediteStock.Comments = txtComments.Text;


        if (Session["UserID"] != null)
        {
            oNewExpediteStock.PurchaseOrder.UserID = Convert.ToInt32(Session["UserID"]);
        }

        //Get the value that comment is required to enter or not.
        if (ViewState["dtReason"] != null)
        {
            DataTable dtIsCmmntNeed = ViewState["dtReason"] as DataTable;
            DataRow result = (from row in dtIsCmmntNeed.AsEnumerable()
                              where row.Field<int>("ReasonTypeID") == Convert.ToInt32(ddlReason_1.SelectedValue)
                              select row).FirstOrDefault();

            bool CmmntReq = Convert.ToBoolean(result.ItemArray[1]);
            if (CmmntReq && string.IsNullOrEmpty(txtComments.Text))
            {
                string warningMessage = WebCommon.getGlobalResourceValue("CommentIsRequired");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + warningMessage + "')", true);

                txtComments.Focus();
                return;
            }

        }
        oNewExpediteStock.CommentID = Convert.ToInt32(hdnCommentID.Value);
        oNewExpediteStock.CommentsAdded_Date = DateTime.Now;
        oMAS_ExpStockBAL.SaveExpediteCommentsBAL(oNewExpediteStock);


        BindLatestComments();
        Page.ClientScript.RegisterStartupScript(GetType(), "script1", "window.close();", true);
    }

    protected void btnAddComment_Click(object sender, EventArgs e)
    {
        AddComment();
    }

    protected void AddComment()
    {
        pnlLatestUpdate.Visible = false;
        pnlAddComment.Visible = true;
        pnlHistoricComments.Visible = false;
        txtComment.Text = string.Empty;
        txtFromDate.Text = string.Empty;
        if (Session["UserName"] != null)
        {
            lblCommentAddedBy_1Value.Text = Session["UserName"].ToString();
            string format = "dd/MM/yyyy HH:mm";
            lblDateValue.Text = DateTime.Now.ToString(format);

        }
        var expediteStock = new ExpediteStockBE();
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        string[] ArrSiteId = null;
        if (GetQueryStringValue("IsSingleComment") != null)
        {
            ArrSiteId = GetQueryStringValue("SiteID").TrimEnd(',').Split(',');
        }
        else
        {
            if (Session["QueryString"] != null)
            {
                ExpediteStockBE oSessionQueryString = new ExpediteStockBE();
                oSessionQueryString = (ExpediteStockBE)Session["QueryString"];
                ArrSiteId = oSessionQueryString.SiteIds.TrimEnd(',').Split(',');
            }
        }

        if (ViewState["dtReason"] == null)
        {
            expediteStock.Action = "GetReason";
            expediteStock.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
            if (ArrSiteId.Length == 1 && GetQueryStringValue("IsSingleComment") != null)
            {
                expediteStock.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").TrimEnd(','));
            }
            else
            {
                expediteStock.SiteID = Convert.ToInt32(ArrSiteId[0].ToString());
            }

            DataTable dtAllType = oMAS_ExpStockBAL.GetReasonBAL(expediteStock);

            if (dtAllType.Rows.Count > 0)
            {
                FillControls.FillDropDown(ref ddlReason, dtAllType, "Reason", "ReasonTypeID", "Select");
                ViewState["dtReason"] = dtAllType;
            }
        }
        else
        {
            DataTable dtAllType = (DataTable)ViewState["dtReason"];
            if (dtAllType.Rows.Count > 0)
            {
                FillControls.FillDropDown(ref ddlReason, dtAllType, "Reason", "ReasonTypeID", "Select");
            }
        }
    }
    protected void btnViewHistoricComment_Click(object sender, EventArgs e)
    {
        pnlLatestUpdate.Visible = false;
        pnlAddComment.Visible = false;
        pnlHistoricComments.Visible = true;
        BindHistoricCommentsRepeater();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        int? intResult = null;
        string[] arrSKUId = null;
        string[] arrSiteId = null;
        string[] arrVikingCode = null;
        string[] arrODCode = null;
        string[] arrVendorId = null;
        ExpediteStockBE oSessionQueryString = new ExpediteStockBE();
        oSessionQueryString = (ExpediteStockBE)Session["QueryString"];
        if (GetQueryStringValue("IsSingleComment") != null)
        {
            arrSKUId = GetQueryStringValue("SKUID").TrimEnd(',').Split(',');
            arrSiteId = GetQueryStringValue("SiteID").TrimEnd(',').Split(',');
            arrVikingCode = GetQueryStringValue("Viking").TrimEnd(',').Split(',');
            arrODCode = GetQueryStringValue("ODCode").TrimEnd(',').Split(',');
            arrVendorId = GetQueryStringValue("VendorID").TrimEnd(',').Split(',');
        }
        else
        {
            if (Session["QueryString"] != null)
            {
                arrSKUId = oSessionQueryString.skuids.TrimEnd(',').Split(',');
                arrSiteId = oSessionQueryString.SiteIds.TrimEnd(',').Split(',');
                arrVikingCode = oSessionQueryString.DirectCodes.TrimEnd(',').Split(',');
                arrODCode = oSessionQueryString.ODCodes.TrimEnd(',').Split(',');
                arrVendorId = oSessionQueryString.Vendors.TrimEnd(',').Split(',');
            }
        }
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        //Save comment.
        ExpediteStockBE oNewExpediteStock = new ExpediteStockBE();
        oNewExpediteStock.PurchaseOrder = new Up_PurchaseOrderDetailBE();
        oNewExpediteStock.Action = "SaveComments";

        oNewExpediteStock.CommentsAdded_Date = DateTime.Now;
        if (ddlReason.SelectedValue.ToString() != "")
        {
            oNewExpediteStock.ReasonTypeID = Convert.ToInt32(ddlReason.SelectedValue);
        }
        if (Session["UserID"] != null)
        {
            oNewExpediteStock.PurchaseOrder.UserID = Convert.ToInt32(Session["UserID"]);
        }
        if (arrSKUId.Length > 0)
        {
            for (int i = 0; i < arrSKUId.Length; i++)
            {
                if (GetQueryStringValue("SKUID") != null)
                {
                    oNewExpediteStock.PurchaseOrder.SKUID = Convert.ToInt32(arrSKUId[i].ToString());
                }
                else
                {
                    oNewExpediteStock.PurchaseOrder.SKUID = Convert.ToInt32(arrSKUId[i].ToString());
                }

                if (GetQueryStringValue("VendorID") != null)
                {
                    oNewExpediteStock.VendorID = Convert.ToInt32(arrVendorId[i].ToString());
                }
                else
                {
                    oNewExpediteStock.VendorID = Convert.ToInt32(arrVendorId[i].ToString());
                }

                if (GetQueryStringValue("SiteID") != null)
                {
                    oNewExpediteStock.SiteID = Convert.ToInt32(arrSiteId[i].ToString());
                }
                else
                {
                    oNewExpediteStock.SiteID = Convert.ToInt32(arrSiteId[i].ToString());
                }

                if (GetQueryStringValue("Viking") != null)
                {
                    oNewExpediteStock.Viking_Code = Convert.ToString(arrVikingCode[i].ToString()).Trim();
                }
                else
                {
                    oNewExpediteStock.Viking_Code = Convert.ToString(arrVikingCode[i].ToString()).Trim();
                }

                if (GetQueryStringValue("ODCode") != null)
                {
                    oNewExpediteStock.OD_Code = Convert.ToString(arrODCode[i].ToString()).Trim();
                }
                else
                {
                    oNewExpediteStock.OD_Code = Convert.ToString(arrODCode[i].ToString()).Trim();
                }

                if (string.Equals(rblItemToTheExpediteReview.SelectedValue.ToString(), "1"))
                    oNewExpediteStock.IsExpediteReview = true;
                else
                    oNewExpediteStock.IsExpediteReview = false;

                oNewExpediteStock.Comments = txtComment.Text;
                txtFromDate.Text = hdnJSFromDt.Value;


                oNewExpediteStock.CommentsValid_Date = !string.IsNullOrEmpty(txtFromDate.Text) ? Common.GetMM_DD_YYYY(txtFromDate.Text) : (DateTime?)null;



                //Get the value that comment is required to enter or not.
                if (ViewState["dtReason"] != null)
                {
                    DataTable dtIsCmmntNeed = ViewState["dtReason"] as DataTable;
                    DataRow result = (from row in dtIsCmmntNeed.AsEnumerable()
                                      where row.Field<int>("ReasonTypeID") == Convert.ToInt32(ddlReason.SelectedValue)
                                      select row).FirstOrDefault();

                    bool CmmntReq = Convert.ToBoolean(result.ItemArray[1]);
                    if (CmmntReq && string.IsNullOrEmpty(txtComment.Text))
                    {
                        string warningMessage = WebCommon.getGlobalResourceValue("CommentIsRequired");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + warningMessage + "')", true);
                        pnlLatestUpdate.Visible = false;
                        pnlAddComment.Visible = true;
                        pnlHistoricComments.Visible = false;
                        txtComment.Focus();
                        return;
                    }

                }
                intResult = oMAS_ExpStockBAL.SaveExpediteCommentsBAL(oNewExpediteStock);
                // intResult = 1;
            }
        }
        //pnlLatestUpdate.Visible = true;
        //pnlAddComment.Visible = false;
        //pnlHistoricComments.Visible = false;


        if (intResult > 0 && intResult.ToString() != null)
        {
            //BindLatestComments();
            //EncryptQueryString("ExpediteStockSearch.aspx?PreviousPage=Comments");
            if (GetQueryStringValue("IsSingleComment") == "1")
            {
                ((List<ExpediteStockBE>)Session["ExpediteStockData"]).Where(x => x.SkuID == Convert.ToInt32(GetQueryStringValue("SKUID"))).ToList().ForEach(c =>
                 {
                     c.CommentsAddedBy = lblCommentAddedBy_1Value.Text;
                     c.CommentsAdded_Date = DateTime.Now;
                     c.CommentsValid_Date = (hdnJSFromDt.Value == null || hdnJSFromDt.Value == "") ? (DateTime?)null : Convert.ToDateTime(Common.GetMM_DD_YYYY1(hdnJSFromDt.Value));
                     c.Reason = ddlReason.SelectedItem.Text;
                     c.Comments = txtComment.Text;
                 });

                ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateExpedite", "UpdateExpedite('" + Convert.ToString(GetQueryStringValue("Viking")).TrimEnd(',') + ",','" + Convert.ToString(GetQueryStringValue("SiteID")).TrimEnd(',') + "," + "');", true);
            }
            else
            {
                if (arrSKUId.Length == 1)
                {
                    ((List<ExpediteStockBE>)Session["ExpediteStockData"]).Where(x => x.SkuID == Convert.ToInt32(arrSKUId[0])).ToList().ForEach(c =>
                {
                    c.CommentsAddedBy = lblCommentAddedBy_1Value.Text;
                    c.CommentsAdded_Date = DateTime.Now;
                    c.CommentsValid_Date = (hdnJSFromDt.Value == null || hdnJSFromDt.Value == "") ? (DateTime?)null : Convert.ToDateTime(Common.GetMM_DD_YYYY1(hdnJSFromDt.Value));
                    c.Reason = ddlReason.SelectedItem.Text;
                    c.Comments = txtComment.Text;
                });
                }
                else
                {
                    for (int i = 0; i < arrSKUId.Length; i++)
                    {
                        ((List<ExpediteStockBE>)Session["ExpediteStockData"]).Where(x => x.SkuID == Convert.ToInt32(arrSKUId[i])).ToList().ForEach(c =>
                        {
                            c.CommentsAddedBy = lblCommentAddedBy_1Value.Text;
                            c.CommentsAdded_Date = DateTime.Now;
                            c.CommentsValid_Date = (hdnJSFromDt.Value == null || hdnJSFromDt.Value == "") ? (DateTime?)null : Convert.ToDateTime(Common.GetMM_DD_YYYY1(hdnJSFromDt.Value));
                            c.Reason = ddlReason.SelectedItem.Text;
                            c.Comments = txtComment.Text;
                        });
                    }

                }

                Session["QueryString"] = null;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateExpedite", "UpdateExpedite('" + Convert.ToString(oSessionQueryString.DirectCodes).TrimEnd(',') + ",','" + Convert.ToString(oSessionQueryString.SiteIds).TrimEnd(',') + "," + "');", true);


            }
            //Response.Write("<script language='javascript'>UpdateExpedite(" + Convert.ToInt32(GetQueryStringValue("SKUID")) + "," + Convert.ToInt32(GetQueryStringValue("VendorID")) + ");</script>");
        }

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        string[] ArrSkuId = null;
        ExpediteStockBE oSessionQueryString = new ExpediteStockBE();
        oSessionQueryString = (ExpediteStockBE)Session["QueryString"];
        if (GetQueryStringValue("IsSingleComment") != null)
        {
            ArrSkuId = GetQueryStringValue("SKUID").TrimEnd(',').Split(',');
        }
        else
        {
            if (Session["QueryString"] != null)
            {
                ArrSkuId = oSessionQueryString.skuids.TrimEnd(',').Split(',');
            }
        }

        if (ArrSkuId.Length > 1)
        {
            Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");
        }
        else
        {
            pnlLatestUpdate.Visible = true;
            pnlAddComment.Visible = false;
            pnlHistoricComments.Visible = false;
        }
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        pnlLatestUpdate.Visible = true;
        pnlAddComment.Visible = false;
        pnlHistoricComments.Visible = false;
    }

    private void BindLatestUpdatePanelCommentsRepeater()
    {
        var oNewExpediteStock = new ExpediteStockBE();
        var oMAS_ExpStockBAL = new ExpediteStockBAL();
        oNewExpediteStock.Action = "ViewCommunication";
        if (!string.IsNullOrEmpty(GetQueryStringValue("SKUID")))
            oNewExpediteStock.SkuID = Convert.ToInt32(GetQueryStringValue("SKUID").TrimEnd(','));

        var expediteStockBE = oMAS_ExpStockBAL.GetExpediteCommentsHistoryBAL(oNewExpediteStock).FirstOrDefault();
        if (expediteStockBE != null)
        {
            ltDatePost.Text = String.Format("{0:dd/MM/yyyy HH:mm}", expediteStockBE.Comm_On);
            lblCommentExpeditedBy.Text = expediteStockBE.Comm_By.ToString();
            ltMailSentExpAdmin.Text = expediteStockBE.SentTo;
            lnkExpeditePopup.Attributes.Add("href", EncryptQuery("~/ModuleUI/StockOverview/Expedite/Communication.aspx?CommunicationID=" + expediteStockBE.CommunicationID.ToString() + "&VendorId=" + expediteStockBE.VendorID));
        }
        else
        {
            // lblNoCommentsForSKU.Visible = true;
            tbllatestExpedite.Visible = false;
            lblNoExpedite.Visible = true;
        }
    }

    private void BindHistoricCommentsRepeater()
    {
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        ExpediteStockBE oNewExpediteStock = new ExpediteStockBE();
        oNewExpediteStock.PurchaseOrder = new Up_PurchaseOrderDetailBE();
        oNewExpediteStock.Action = "ViewHistoricComments";
        //if (Session["UserID"] != null)
        //{
        //    oNewExpediteStock.PurchaseOrder.UserID = Convert.ToInt32(Session["UserID"]);
        //}
        if (!string.IsNullOrEmpty(GetQueryStringValue("SKUID")))
        {
            oNewExpediteStock.PurchaseOrder.SKUID = Convert.ToInt32(GetQueryStringValue("SKUID").TrimEnd(','));
        }

        if (!string.IsNullOrEmpty(GetQueryStringValue("VendorID")))
        {
            oNewExpediteStock.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
        }


        oNewExpediteStock.Action = "ViewHistoricComments";
        DataTable dthistComments = oMAS_ExpStockBAL.ViewHistoricCommntsBAL(oNewExpediteStock);
        StringBuilder sb = null;
        if (dthistComments.Rows.Count > 0)
        {
            rptReasonComments.DataSource = dthistComments;
            rptReasonComments.DataBind();
        }

        oNewExpediteStock.Action = "ViewCommunication";
        if (!string.IsNullOrEmpty(GetQueryStringValue("SKUID")))
        {
            oNewExpediteStock.SkuID = Convert.ToInt32(GetQueryStringValue("SKUID").TrimEnd(','));
        }
        //  oNewExpediteStock.SkuID = 71222;
        List<ExpediteStockBE> lstExpediteComments = oMAS_ExpStockBAL.GetExpediteCommentsHistoryBAL(oNewExpediteStock);
        if (lstExpediteComments != null && lstExpediteComments.Count > 0)
        {
            pnlExpedite.Visible = true;
            rptCommunication.DataSource = lstExpediteComments;
            rptCommunication.DataBind();
        }
        else
            pnlExpedite.Visible = false;

    }
    protected void UcbtnBacktoCmmnt_Click(object sender, EventArgs e)
    {
        pnlLatestUpdate.Visible = true;
        pnlAddComment.Visible = false;
        pnlHistoricComments.Visible = false;
        txtComment.Text = string.Empty;
        txtFromDate.Text = string.Empty;
    }
    protected void UcbtnBacktoOutput_Click(object sender, EventArgs e)
    {
        Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");

        //EncryptQueryString("ExpediteStockSearch.aspx?PreviousPage=Comments");
    }

    protected void rptCommunication_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            System.Web.UI.HtmlControls.HtmlAnchor lnk = (System.Web.UI.HtmlControls.HtmlAnchor)e.Item.FindControl("lnk");
            lnk.Attributes.Add("href", EncryptQuery("~/ModuleUI/StockOverview/Expedite/Communication.aspx?CommunicationID=" + ((ExpediteStockBE)(e.Item.DataItem)).CommunicationID.ToString()));
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using Utilities;
using BaseControlLibrary;
using System.Collections.Specialized;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data;


public partial class ExpediteStockSearch : CommonPage
{
    #region Declarations ...

    protected string Pleaseselectatleastonesite = WebCommon.getGlobalResourceValue("Pleaseselectatleastonesite");
    protected string Maximumfoursitesareallowed = WebCommon.getGlobalResourceValue("Maximumfoursitesareallowed");
    protected string OverduePurchasesOrdersonly = WebCommon.getGlobalResourceValue("OverduePurchasesOrdersonly");
    protected string OverduesomeNotOverdue = WebCommon.getGlobalResourceValue("OverduesomeNotOverdue");
    protected string NoOverdue = WebCommon.getGlobalResourceValue("NoOverdue");
    protected string NoPORaised = WebCommon.getGlobalResourceValue("NoPORaised");
    protected string DaysStockErrorMsg = WebCommon.getGlobalResourceValue("DaysStockErrorMsg");
    protected string CommentCount = WebCommon.getGlobalResourceValue("CommentCount");
    protected string siteRequired = WebCommon.getGlobalResourceValue("SiteRequired");
    protected string VendorReq = WebCommon.getGlobalResourceValue("VendorReq");
    protected string WeekendCheck = WebCommon.getGlobalResourceValue("WeekendCheckSort");  
    protected int checkStatus = 0;
    bool isExpeditesubfilter = false;

    protected string Pleaseaddsearchcriteria = WebCommon.getGlobalResourceValue("Pleaseaddsearchcriteria");
    protected string NewMaximumfoursitesareallowed = WebCommon.getGlobalResourceValue("NewMaximumfoursitesareallowed");

    #endregion

    #region Events ...

    protected void Page_PreRender(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            hdnIsAdditionSearch.Value = "false";
            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "Comments")
            {
                if (Session["ExpediteStockOutput"] != null)
                {
                    GetSession();
                    RetainExpediteData();
                }
            }
            if (Session["ExpediteStockOutput"] != null)
            {
                RetainExpediteData();
            }
        }

    }

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSeacrhVendor1.CurrentPage = this;
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;
        ddlSite.SchedulingContact = true;
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;
        ucSeacrhVendor1.IsParentRequired = true;
        ucSeacrhVendor1.IsStandAloneRequired = true;
        ucSeacrhVendor1.IsChildRequired = true;
        ucSeacrhVendor1.IsGrandParentRequired = true;

        ucVendorTemplateSelect.CurrentPage = this;
        msSite.isMoveAllRequired = true;
        msSite.IsHubRequired = false;
    }

    public override void SiteSelectedIndexChanged()
    {
        ucSeacrhVendor1.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
        ddlSite.IsExpediteSites = true;
        ddlSite.siteIDs = msSite.SelectedSiteIDs;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
        HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
        HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.Cache.SetNoStore();

        UIUtility.PageValidateForODUser();

        ucExportToExcel1.CurrentPage = this;
        //ucExportToExcel1.GridViewControl = gvPotentialOutputExcelExp;
        if (hdnrblSearchType.Value == "0")
        {
            ucExportToExcel1.FileName = "ExpediteStock";
        }
        else
        {
            ucExportToExcel1.FileName = "ExpediteStock_WeekWise";
        }
            ddlSite.innerControlddlSite.AutoPostBack = true; 
        if (!IsPostBack)
        {
            //this.BindDropDowns();
            pnlSearchScreen.Visible = true;
            pnlPotentialOutput.Visible = false;
            ucExportToExcel1.Visible = false;
            pager1.PageSize = 200;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager2.PageSize = 200;
            pager2.GenerateGoToSection = false;
            pager2.GeneratePagerInfoSection = false;
            ddlSite.innerControlddlSite.SelectedIndex = 0;
            txtVikingSku.Text = string.Empty;
            txtODSKUCode.Text = string.Empty;
            ucSeacrhVendor1.VendorNo = string.Empty;
            ucSeacrhVendor1.innerControlVendorName.Text = string.Empty;
        }
        //this.BindDropDowns();
        ddlSite.IsExpediteSites = true;
        ddlSite.siteIDs = msSite.SelectedSiteIDs;
    }

    private void RetainExpediteData()
    {
        //multiSelectSKUGrouping.setSkuGroupingsOnPostBack();
        //multiSelectStockPlannerGrouping.setStockPlannerGroupingsOnPostBack();

        Hashtable htExpediteStockOutput = (Hashtable)Session["ExpediteStockOutput"];

        rblDisplay.SelectedValue = (htExpediteStockOutput.ContainsKey("SelectedDisplay") && htExpediteStockOutput["SelectedDisplay"] != null) ? htExpediteStockOutput["SelectedDisplay"].ToString() : "";
        rblSortBy.SelectedValue = (htExpediteStockOutput.ContainsKey("SortBy") && htExpediteStockOutput["SortBy"] != null) ? htExpediteStockOutput["SortBy"].ToString() : "";
        rblPOStatus.SelectedValue = (htExpediteStockOutput.ContainsKey("POStatus") && htExpediteStockOutput["POStatus"] != null) ? htExpediteStockOutput["POStatus"].ToString() : "";
        rblSkuType.SelectedValue = (htExpediteStockOutput.ContainsKey("SelectedSkuType") && htExpediteStockOutput["SelectedSkuType"] != null) ? htExpediteStockOutput["SelectedSkuType"].ToString() : "";
        rblCommentStatus.SelectedValue = (htExpediteStockOutput.ContainsKey("CommentStatus") && htExpediteStockOutput["CommentStatus"] != null) ? htExpediteStockOutput["CommentStatus"].ToString() : "";

        string StockPlannerText = (htExpediteStockOutput.ContainsKey("SelectedStockPlannerName") && htExpediteStockOutput["SelectedStockPlannerName"] != null) ? htExpediteStockOutput["SelectedStockPlannerName"].ToString() : "";
        string StockPlannerID = (htExpediteStockOutput.ContainsKey("SelectedStockPlannerUserIDs") && htExpediteStockOutput["SelectedStockPlannerUserIDs"] != null) ? htExpediteStockOutput["SelectedStockPlannerUserIDs"].ToString() : "";
        ucListBox lstRight = msStockPlanner.FindControl("ucLBRight") as ucListBox;
        ucListBox lstLeft = msStockPlanner.FindControl("ucLBLeft") as ucListBox;
        string strDtockPlonnerId = string.Empty;
        if (lstLeft != null && lstRight != null)
        {
            lstLeft.Items.Clear();
            lstRight.Items.Clear();
            msStockPlanner.SearchStockPlannerClick(StockPlannerText);
            string[] strStockPlonnerIDs = StockPlannerID.Split(',');
            for (int index = 0; index < strStockPlonnerIDs.Length; index++)
            {
                ListItem listItem = lstLeft.Items.FindByValue(strStockPlonnerIDs[index]);
                if (listItem != null)
                {
                    strDtockPlonnerId += strStockPlonnerIDs[index] + ",";
                    lstRight.Items.Add(listItem);
                    lstLeft.Items.Remove(listItem);
                }
            }
            HiddenField hdnSelectedStockPlonner = msStockPlanner.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedStockPlonner.Value = strDtockPlonnerId;
        }
        //*********** Site ***************
        string SiteId = (htExpediteStockOutput.ContainsKey("SelectedSiteIDs") && htExpediteStockOutput["SelectedSiteIDs"] != null) ? htExpediteStockOutput["SelectedSiteIDs"].ToString() : "";
        ucListBox lstRightSite = msSite.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftSite = msSite.FindControl("lstLeft") as ucListBox;
        string msSiteid = string.Empty;
        lstRightSite.Items.Clear();
        lstRightSite.Items.Clear();
        msSite.SelectedSiteIDs = "";
        if (lstLeftSite != null && lstRightSite != null && !string.IsNullOrEmpty(SiteId))
        {
            //lstLeftSite.Items.Clear();
            lstRightSite.Items.Clear();
            msSite.BindSite();

            string[] strSiteIDs = SiteId.Split(',');
            for (int index = 0; index < strSiteIDs.Length; index++)
            {
                ListItem listItem = lstLeftSite.Items.FindByValue(strSiteIDs[index]);
                if (listItem != null)
                {
                    msSiteid = msSiteid + strSiteIDs[index].ToString() + ",";
                    lstRightSite.Items.Add(listItem);
                    lstLeftSite.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msSite.SelectedSiteIDs))
                msSite.SelectedSiteIDs = msSiteid.Trim(',');
        }
        //**********  Include Vendor
        string IncludeVendorId = (htExpediteStockOutput.ContainsKey("SelectedIncludedVendorIDs") && htExpediteStockOutput["SelectedIncludedVendorIDs"] != null) ? htExpediteStockOutput["SelectedIncludedVendorIDs"].ToString() : "";
        string IncludeVendorText = (htExpediteStockOutput.ContainsKey("SelectedIncludedVendor") && htExpediteStockOutput["SelectedIncludedVendor"] != null) ? htExpediteStockOutput["SelectedIncludedVendor"].ToString() : "";
        ucListBox lstRightIncludeVendor = ucIncludeVendor.FindControl("lstSelectedVendor") as ucListBox;
        ucListBox lstLeftIncludeVendor = ucIncludeVendor.FindControl("lstVendor") as ucListBox;
        lstLeftIncludeVendor.Items.Clear();
        if (lstRightIncludeVendor != null && lstLeftIncludeVendor != null && (!string.IsNullOrEmpty(IncludeVendorId) || !string.IsNullOrEmpty(IncludeVendorText)))
        {
            lstRightIncludeVendor.Items.Clear();
            lstLeftIncludeVendor.Items.Clear();
            ucIncludeVendor.SearchVendorClick(IncludeVendorText);
            string[] strIncludeVendorIDs = IncludeVendorId.Split(',');
            for (int index = 0; index < strIncludeVendorIDs.Length; index++)
            {
                ListItem listItem = lstLeftIncludeVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                if (listItem != null)
                {
                    lstRightIncludeVendor.Items.Add(listItem);
                    lstLeftIncludeVendor.Items.Remove(listItem);
                }
            }
        }

        ////************************* StockPlannerGrouping ***************************
        string StockPlannerGroupingID = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
        ucListBox lstRightGrouping = multiSelectStockPlannerGrouping.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftGrouping = multiSelectStockPlannerGrouping.FindControl("lstLeft") as ucListBox;
        // lstRightVendor.Items.Clear();
        if (lstLeftGrouping != null && lstRightGrouping != null)
        {
            lstLeftGrouping.Items.Clear();
            multiSelectStockPlannerGrouping.BindStockPlannerGrouping();
            if (!string.IsNullOrEmpty(StockPlannerGroupingID))
            {
                string[] strStockPlannerGroupingIDs = StockPlannerGroupingID.Split(',');
                for (int index = 0; index < strStockPlannerGroupingIDs.Length; index++)
                {
                    ListItem listItem = lstLeftGrouping.Items.FindByValue(strStockPlannerGroupingIDs[index]);
                    if (listItem != null)
                    {
                        lstLeftGrouping.Items.Remove(listItem);
                    }
                }
            }
        }
        //********************* Exclude Vendor 
        string ExcludeVendorId = (htExpediteStockOutput.ContainsKey("SelectedExcludedVendorIDs") && htExpediteStockOutput["SelectedExcludedVendorIDs"] != null) ? htExpediteStockOutput["SelectedExcludedVendorIDs"].ToString() : "";
        string ExcludeVendorText = (htExpediteStockOutput.ContainsKey("SelectedExcludeVendor") && htExpediteStockOutput["SelectedExcludeVendor"] != null) ? htExpediteStockOutput["SelectedExcludeVendor"].ToString() : "";
        ucListBox lstRightExcludeVendor = ucExcludeVendor.FindControl("lstSelectedVendor") as ucListBox;
        ucListBox lstLeftExcludeVendor = ucExcludeVendor.FindControl("lstVendor") as ucListBox;
        lstLeftExcludeVendor.Items.Clear();
        if (lstRightExcludeVendor != null && lstLeftExcludeVendor != null && (!string.IsNullOrEmpty(ExcludeVendorId) || !string.IsNullOrEmpty(ExcludeVendorText)))
        {
            lstRightExcludeVendor.Items.Clear();
            lstLeftExcludeVendor.Items.Clear();
            ucExcludeVendor.SearchVendorClick(ExcludeVendorText);
            string[] strExcludeVendorIDs = ExcludeVendorId.Split(',');
            for (int index = 0; index < strExcludeVendorIDs.Length; index++)
            {
                ListItem listItem = lstLeftExcludeVendor.Items.FindByValue(strExcludeVendorIDs[index]);
                if (listItem != null)
                {
                    lstRightExcludeVendor.Items.Add(listItem);
                    lstLeftExcludeVendor.Items.Remove(listItem);
                }
            }
        }
        //************************* ItemClassification ***************************
        string classificationId = (htExpediteStockOutput.ContainsKey("SelectedItemClassification") && htExpediteStockOutput["SelectedItemClassification"] != null) ? htExpediteStockOutput["SelectedItemClassification"].ToString() : "";
        ucListBox lstRightClassi = msItemClassification.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftClassi = msItemClassification.FindControl("lstLeft") as ucListBox;
        lstRightClassi.Items.Clear();
        msItemClassification.BindClassification();
        if (lstLeft != null && lstRight != null && !string.IsNullOrEmpty(classificationId))
        {
            // lstLeftClassi.Items.Clear();
            lstRightClassi.Items.Clear();
            // msItemClassification.BindClassification();
            string[] strClassificationIDs = classificationId.Split(',');
            for (int index = 0; index < strClassificationIDs.Length; index++)
            {
                string Item = strClassificationIDs[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    lstRightClassi.Items.Add(listItem);
                    lstLeftClassi.Items.Remove(listItem);
                }
            }
        }
        //********************   MR Type
        string MRTypeId = (htExpediteStockOutput.ContainsKey("SelectedMRPType") && htExpediteStockOutput["SelectedMRPType"] != null) ? htExpediteStockOutput["SelectedMRPType"].ToString() : "";
        ucListBox lstRightMR = ucMRPType.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftMR = ucMRPType.FindControl("lstLeft") as ucListBox;
        if (lstRightMR != null && lstLeftMR != null && !string.IsNullOrEmpty(MRTypeId))
        {
            //lstLeftSite.Items.Clear();
            lstRightMR.Items.Clear();
            ucMRPType.BindAllMRPType();
            string[] strMRIDs = MRTypeId.Split(',');
            for (int index = 0; index < strMRIDs.Length; index++)
            {
                ListItem listItem = lstLeftSite.Items.FindByValue(strMRIDs[index]);
                if (listItem != null)
                {
                    lstRightMR.Items.Add(listItem);
                }
            }
        }
        //***********************Purc Grp
        string PurGroupId = (htExpediteStockOutput.ContainsKey("SelectedPurcGroup") && htExpediteStockOutput["SelectedPurcGroup"] != null) ? htExpediteStockOutput["SelectedPurcGroup"].ToString() : "";
        ucListBox lstRightPur = UcPurcGrp.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftPur = UcPurcGrp.FindControl("lstLeft") as ucListBox;
        if (lstRightPur != null && lstLeftPur != null && !string.IsNullOrEmpty(PurGroupId))
        {
            //lstLeftSite.Items.Clear();
            lstRightPur.Items.Clear();
            UcPurcGrp.BindAllMRPType();
            string[] strPurIDs = PurGroupId.Split(',');
            for (int index = 0; index < strPurIDs.Length; index++)
            {
                ListItem listItem = lstLeftSite.Items.FindByValue(strPurIDs[index]);
                if (listItem != null)
                {
                    lstRightPur.Items.Add(listItem);
                }
            }
        }
        //**************** Mat Group
        string MatGroupId = (htExpediteStockOutput.ContainsKey("SelectedMatGrp") && htExpediteStockOutput["SelectedMatGrp"] != null) ? htExpediteStockOutput["SelectedMatGrp"].ToString() : "";
        ucListBox lstRightMat = UcMatGroup.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftMat = UcMatGroup.FindControl("lstLeft") as ucListBox;
        if (lstRightMR != null && lstLeftMR != null && !string.IsNullOrEmpty(MatGroupId))
        {
            //lstLeftSite.Items.Clear();
            lstRightPur.Items.Clear();
            UcPurcGrp.BindAllMRPType();
            string[] strMatIDs = MatGroupId.Split(',');
            for (int index = 0; index < strMatIDs.Length; index++)
            {
                ListItem listItem = lstLeftSite.Items.FindByValue(strMatIDs[index]);
                if (listItem != null)
                {
                    lstRightMat.Items.Add(listItem);
                }
            }
        }
        //*********************** Viking Sku
        string vikingSkuId = (htExpediteStockOutput.ContainsKey("SelectedVIKINGSKU") && htExpediteStockOutput["SelectedVIKINGSKU"] != null) ? htExpediteStockOutput["SelectedVIKINGSKU"].ToString() : "";
        ucListBox lstRightViking = UcVikingSku.FindControl("lstRight") as ucListBox;
        string hdnSkuid = string.Empty;
        lstRightViking.Items.Clear();
        if (lstRightViking != null && !string.IsNullOrEmpty(vikingSkuId))
        {
            HiddenField hdfSelectedId = UcVikingSku.FindControl("hiddenSelectedId") as HiddenField;
            lstRightViking.Items.Clear();
            string[] strVikingIDs = vikingSkuId.Split(',');
            for (int index = 0; index < strVikingIDs.Length; index++)
            {
                string Item = strVikingIDs[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    hdnSkuid += Item + ",";
                    lstRightViking.Items.Add(listItem);
                }
            }
            hdfSelectedId.Value = hdnSkuid;
        }
        //************** Od sku
        string OdSkuId = (htExpediteStockOutput.ContainsKey("SelectedODSKU") && htExpediteStockOutput["SelectedODSKU"] != null) ? htExpediteStockOutput["SelectedODSKU"].ToString() : "";
        ucListBox lstRightOD = msSKU.FindControl("lstRight") as ucListBox;
        string hdnValue = string.Empty;
        lstRightOD.Items.Clear();
        if (lstRightOD != null && !string.IsNullOrEmpty(OdSkuId))
        {
            HiddenField hdfSelectedId = msSKU.FindControl("hiddenSelectedId") as HiddenField;
            lstRightOD.Items.Clear();
            string[] strODIDs = OdSkuId.Split(',');
            for (int index = 0; index < strODIDs.Length; index++)
            {
                string Item = strODIDs[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    lstRightOD.Items.Add(listItem);
                    hdnValue += Item + ",";
                }
            }
            hdfSelectedId.Value = hdnValue;
        }

        //**************** SKUGrouping Group
        string SKUGroupId = (htExpediteStockOutput.ContainsKey("SelectedSkuGroupingIDs") && htExpediteStockOutput["SelectedSkuGroupingIDs"] != null) ? htExpediteStockOutput["SelectedSkuGroupingIDs"].ToString() : "";
        ucListBox lstRightSKU = multiSelectSKUGrouping.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftSKU = multiSelectSKUGrouping.FindControl("lstLeft") as ucListBox;
        string msSKUgrouping = string.Empty;
        lstRightSKU.Items.Clear();        
        multiSelectSKUGrouping.SelectedSkuGroupingIDs = "";
        if (lstLeftSKU != null && lstRightSKU != null && !string.IsNullOrEmpty(SKUGroupId))
        {
            //lstLeftSite.Items.Clear();
            lstRightSKU.Items.Clear();
            multiSelectSKUGrouping.BindSkuGrouping();

            string[] strSKUs = SKUGroupId.Split(',');
            for (int index = 0; index < strSKUs.Length; index++)
            {
                ListItem listItem = lstLeftSKU.Items.FindByValue(strSKUs[index]);
                if (listItem != null)
                {
                    msSKUgrouping = msSKUgrouping + strSKUs[index].ToString() + ",";
                    lstRightSKU.Items.Add(listItem);
                    lstLeftSKU.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(multiSelectSKUGrouping.SelectedSkuGroupingIDs))
                multiSelectSKUGrouping.SelectedSkuGroupingIDs = msSKUgrouping.Trim(',');
        }


        //**************** StockPlannerGrouping 
        string SPGroupId = (htExpediteStockOutput.ContainsKey("SelectedStockPlannerGroupingIDs") && htExpediteStockOutput["SelectedStockPlannerGroupingIDs"] != null) ? htExpediteStockOutput["SelectedStockPlannerGroupingIDs"].ToString() : "";
        ucListBox lstRightSP = multiSelectStockPlannerGrouping.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftSP = multiSelectStockPlannerGrouping.FindControl("lstLeft") as ucListBox;
        string msSPgrouping = string.Empty;
        lstRightSP.Items.Clear();
        multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs = "";
        if (lstLeftSP != null && lstRightSP != null && !string.IsNullOrEmpty(SPGroupId))
        {
            //lstLeftSite.Items.Clear();
            lstRightSP.Items.Clear();
            multiSelectStockPlannerGrouping.BindStockPlannerGrouping();

            string[] strSPs = SPGroupId.Split(',');
            for (int index = 0; index < strSPs.Length; index++)
            {
                ListItem listItem = lstLeftSP.Items.FindByValue(strSPs[index]);
                if (listItem != null)
                {
                    msSPgrouping = msSPgrouping + strSPs[index].ToString() + ",";
                    lstRightSP.Items.Add(listItem);
                    lstLeftSP.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(multiSelectSKUGrouping.SelectedSkuGroupingIDs))
                multiSelectSKUGrouping.SelectedSkuGroupingIDs = msSPgrouping.Trim(',');
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        
        string SelectedIncludedVendorIDs = string.Empty;
        string SelectedExcludedVendorIDs = string.Empty;

        if (string.IsNullOrEmpty(ucSeacrhVendor1.VendorNo))
        {
            SelectedIncludedVendorIDs = this.GetSelectedVendorIds(ucIncludeVendor);
        }
        else
        {
            SelectedIncludedVendorIDs = ucSeacrhVendor1.VendorNo;
        }

        SelectedExcludedVendorIDs = this.GetSelectedVendorIds(ucExcludeVendor);

        if (string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs) || string.IsNullOrWhiteSpace(msStockPlanner.SelectedStockPlannerIDs))
        {
            if (string.IsNullOrEmpty(multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs) ||
               string.IsNullOrWhiteSpace(multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs))
            {
                if (string.IsNullOrEmpty(SelectedIncludedVendorIDs) || string.IsNullOrWhiteSpace(SelectedIncludedVendorIDs))
                {
                    if (string.IsNullOrEmpty(SelectedExcludedVendorIDs) || string.IsNullOrWhiteSpace(SelectedExcludedVendorIDs))
                    {
                        if (string.IsNullOrEmpty(msItemClassification.selectedItemClassification) || string.IsNullOrWhiteSpace(msItemClassification.selectedItemClassification))
                        {
                            if (string.IsNullOrEmpty(ucMRPType.SelectedMRPTypes))
                            {
                                if (string.IsNullOrEmpty(UcPurcGrp.SelectedPurGrp))
                                {
                                    if (string.IsNullOrEmpty(UcMatGroup.SelectedMatGroup))
                                    {
                                        if (string.IsNullOrEmpty(UcVikingSku.SelectedSKUName))
                                        {
                                            if (string.IsNullOrEmpty(msSKU.SelectedSKUName))
                                            {
                                                if (string.IsNullOrEmpty(multiSelectSKUGrouping.SelectedSkuGroupingIDs) ||
                                                    string.IsNullOrWhiteSpace(multiSelectSKUGrouping.SelectedSkuGroupingIDs))
                                                {
                                                    if (string.IsNullOrEmpty(msSite.SelectedSiteIDs) || string.IsNullOrWhiteSpace(msSite.SelectedSiteIDs))
                                                    {
                                                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Pleaseaddsearchcriteria + "')", true);
                                                        return;
                                                    }
                                                    else
                                                    {
                                                        string[] SelectedSite = msSite.SelectedSiteIDs.Replace(",,,,", ",").Replace(",,,", ",").Replace(",,", ",").Split(',');

                                                        if (SelectedSite.Length > 4)
                                                        {
                                                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + NewMaximumfoursitesareallowed + "')", true);
                                                            return;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
     
        
        //if (string.IsNullOrEmpty(msSite.SelectedSiteIDs) || string.IsNullOrWhiteSpace(msSite.SelectedSiteIDs))
        //{
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Pleaseselectatleastonesite + "')", true);
        //    return;
        //}

        
        isExpeditesubfilter = false;
        hdnIsAdditionSearch.Value = "true";

        if (rblSearchType.SelectedItem.Text == "Day Wise")
        {


            BindExpedite();

            #region Logic to set the 20 days header text ...
            var lstExpediteDayDays = this.Get20DaysDate(Convert.ToDateTime(ViewState["UpdatedDateValue"]));
            if (lstExpediteDayDays.Count > 0)
            {
                #region Changing header of Display Grid ...
                for (int index = 0; index < gvPotentialOutput.Columns.Count; index++)
                {
                    switch (gvPotentialOutput.Columns[index].HeaderText)
                    {
                        case "FirstWeekMon":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[0].UpdateDate;
                            break;
                        case "FirstWeekTue":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[1].UpdateDate;
                            break;
                        case "FirstWeekWed":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[2].UpdateDate;
                            break;
                        case "FirstWeekThu":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[3].UpdateDate;
                            break;
                        case "FirstWeekFri":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[4].UpdateDate;
                            break;
                        case "SecondWeekMon":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[5].UpdateDate;
                            break;
                        case "SecondWeekTue":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[6].UpdateDate;
                            break;
                        case "SecondWeekWed":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[7].UpdateDate;
                            break;
                        case "SecondWeekThu":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[8].UpdateDate;
                            break;
                        case "SecondWeekFri":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[9].UpdateDate;
                            break;
                        case "ThirdWeekMon":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[10].UpdateDate;
                            break;
                        case "ThirdWeekTue":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[11].UpdateDate;
                            break;
                        case "ThirdWeekWed":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[12].UpdateDate;
                            break;
                        case "ThirdWeekThu":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[13].UpdateDate;
                            break;
                        case "ThirdWeekFri":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[14].UpdateDate;
                            break;
                        case "FourthWeekMon":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[15].UpdateDate;
                            break;
                        case "FourthWeekTue":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[16].UpdateDate;
                            break;
                        case "FourthWeekWed":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[17].UpdateDate;
                            break;
                        case "FourthWeekThu":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[18].UpdateDate;
                            break;
                        case "FourthWeekFri":
                            gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[19].UpdateDate;
                            break;
                    }
                }
                #endregion
            }
            #endregion
        }
        else if (rblSearchType.SelectedItem.Text == "Week Wise")
        {
            BindExpedite();           
        }


    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        hdnIsAdditionSearch.Value = "false";
        ddlSite.innerControlddlSite.SelectedIndex = 0;
        txtVikingSku.Text = string.Empty;
        txtODSKUCode.Text = string.Empty;
        ucSeacrhVendor1.VendorNo = string.Empty;
        ucSeacrhVendor1.innerControlVendorName.Text = string.Empty;
        this.ClearCriteria();
        pnlSearchScreen.Visible = true;
        pnlPotentialOutput.Visible = false;
        ucExportToExcel1.Visible = false;
        RetainExpediteData();
        //EncryptQueryString("ExpediteStockSearch.aspx");
       
    }

    protected void btnAdditionalSearch_Click(object sender, EventArgs e)
    {
        isExpeditesubfilter = true;
        BindExpedite();
    }

    protected void gvPotentialOutput_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            #region Logic to set the color of cell based on their Color status ...

            var lblPriorityItemColor = e.Row.FindControl("lblPriorityColor") as Literal;
            if (lblPriorityItemColor != null)
                e.Row.Cells[5].BackColor = GetColor(lblPriorityItemColor.Text.Trim());

            var lblCommentColor = e.Row.FindControl("lblCommentColor") as Literal;
            if (lblCommentColor != null)
                e.Row.Cells[1].BackColor = GetColor(lblCommentColor.Text);

            var StatusValue = e.Row.FindControl("lblStatusValue") as Label;
            if (StatusValue != null)
                e.Row.Cells[2].BackColor = GetColor(StatusValue.Text);

            var lblFirstWeekMONColor = e.Row.FindControl("lblFirstWeekMONColor") as Literal;
            if (lblFirstWeekMONColor != null)
                e.Row.Cells[31].BackColor = GetColor(lblFirstWeekMONColor.Text);

            var lblFirstWeekTUEColor = e.Row.FindControl("lblFirstWeekTUEColor") as Literal;
            if (lblFirstWeekTUEColor != null)
                e.Row.Cells[32].BackColor = GetColor(lblFirstWeekTUEColor.Text);

            var lblFirstWeekWEDColor = e.Row.FindControl("lblFirstWeekWEDColor") as Literal;
            if (lblFirstWeekWEDColor != null)
                e.Row.Cells[33].BackColor = GetColor(lblFirstWeekWEDColor.Text);

            var lblFirstWeekTHUColor = e.Row.FindControl("lblFirstWeekTHUColor") as Literal;
            if (lblFirstWeekTHUColor != null)
                e.Row.Cells[34].BackColor = GetColor(lblFirstWeekTHUColor.Text);

            var lblFirstWeekFRIColor = e.Row.FindControl("lblFirstWeekFRIColor") as Literal;
            if (lblFirstWeekFRIColor != null)
                e.Row.Cells[35].BackColor = GetColor(lblFirstWeekFRIColor.Text);

            var lblSecondWeekMONColor = e.Row.FindControl("lblSecondWeekMONColor") as Literal;
            if (lblSecondWeekMONColor != null)
                e.Row.Cells[36].BackColor = GetColor(lblSecondWeekMONColor.Text);

            var lblSecondWeekTUEColor = e.Row.FindControl("lblSecondWeekTUEColor") as Literal;
            if (lblSecondWeekTUEColor != null)
                e.Row.Cells[37].BackColor = GetColor(lblSecondWeekTUEColor.Text);

            var lblSecondWeekWEDColor = e.Row.FindControl("lblSecondWeekWEDColor") as Literal;
            if (lblSecondWeekWEDColor != null)
                e.Row.Cells[38].BackColor = GetColor(lblSecondWeekWEDColor.Text);

            var lblSecondWeekTHUColor = e.Row.FindControl("lblSecondWeekTHUColor") as Literal;
            if (lblSecondWeekTHUColor != null)
                e.Row.Cells[39].BackColor = GetColor(lblSecondWeekTHUColor.Text);

            var lblSecondWeekFRIColor = e.Row.FindControl("lblSecondWeekFRIColor") as Literal;
            if (lblSecondWeekFRIColor != null)
                e.Row.Cells[40].BackColor = GetColor(lblSecondWeekFRIColor.Text);

            var lblThirdWeekMONColor = e.Row.FindControl("lblThirdWeekMONColor") as Literal;
            if (lblThirdWeekMONColor != null)
                e.Row.Cells[41].BackColor = GetColor(lblThirdWeekMONColor.Text);

            var lblThirdWeekTUEColor = e.Row.FindControl("lblThirdWeekTUEColor") as Literal;
            if (lblThirdWeekTUEColor != null)
                e.Row.Cells[42].BackColor = GetColor(lblThirdWeekTUEColor.Text);

            var lblThirdWeekWEDColor = e.Row.FindControl("lblThirdWeekWEDColor") as Literal;
            if (lblThirdWeekWEDColor != null)
                e.Row.Cells[43].BackColor = GetColor(lblThirdWeekWEDColor.Text);

            var lblThirdWeekTHUColor = e.Row.FindControl("lblThirdWeekTHUColor") as Literal;
            if (lblThirdWeekTHUColor != null)
                e.Row.Cells[44].BackColor = GetColor(lblThirdWeekTHUColor.Text);

            var lblThirdWeekFRIColor = e.Row.FindControl("lblThirdWeekFRIColor") as Literal;
            if (lblThirdWeekFRIColor != null)
                e.Row.Cells[45].BackColor = GetColor(lblThirdWeekFRIColor.Text);

            var lblFourthWeekMONColor = e.Row.FindControl("lblFourthWeekMONColor") as Literal;
            if (lblFourthWeekMONColor != null)
                e.Row.Cells[46].BackColor = GetColor(lblFourthWeekMONColor.Text);

            var lblFourthWeekTUEColor = e.Row.FindControl("lblFourthWeekTUEColor") as Literal;
            if (lblFourthWeekTUEColor != null)
                e.Row.Cells[47].BackColor = GetColor(lblFourthWeekTUEColor.Text);

            var lblFourthWeekWEDColor = e.Row.FindControl("lblFourthWeekWEDColor") as Literal;
            if (lblFourthWeekWEDColor != null)
                e.Row.Cells[48].BackColor = GetColor(lblFourthWeekWEDColor.Text);

            var lblFourthWeekTHUColor = e.Row.FindControl("lblFourthWeekTHUColor") as Literal;
            if (lblFourthWeekTHUColor != null)
                e.Row.Cells[49].BackColor = GetColor(lblFourthWeekTHUColor.Text);

            var lblFourthWeekFRIColor = e.Row.FindControl("lblFourthWeekFRIColor") as Literal;
            if (lblFourthWeekFRIColor != null)
                e.Row.Cells[50].BackColor = GetColor(lblFourthWeekFRIColor.Text);

            #endregion

            var UpdatedDateValue = e.Row.FindControl("lblUpdatedDateValue") as Literal;
            if (UpdatedDateValue != null && ViewState["UpdatedDateValue"] == null)
            {
                ViewState["UpdatedDateValue"] = UpdatedDateValue.Text;
            }

            var lnkEarliestAdviseDateValue = e.Row.FindControl("lnkEarliestAdviseDateValue") as HyperLink;
            if (lnkEarliestAdviseDateValue != null)
                if (string.IsNullOrEmpty(lnkEarliestAdviseDateValue.Text) || string.IsNullOrWhiteSpace(lnkEarliestAdviseDateValue.Text))
                    lnkEarliestAdviseDateValue.Text = "NA";

            //if (string.IsNullOrEmpty((e.Row.FindControl("hdncommentAddedDate") as HiddenField).Value))
            //{
             //   (e.Row.FindControl("pnlToolComment") as Panel).CssClass = "tooltipComment hidediv";
                //(e.Row.FindControl("lnkCommentValue") as HyperLink).CssClass = "";
            //}
            //GetLatestComment(Convert.ToInt32(((System.Web.UI.WebControls.HiddenField)e.Row.FindControl("hdnSKUID")).Value.ToString()),e.Row);

        }
    }

    protected void gvPotentialOutputWeekWise_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            #region Logic to set the color of cell based on their Color status ...

            var lblPriorityItemColor = e.Row.FindControl("lblPriorityColor") as Literal;
            if (lblPriorityItemColor != null)
                e.Row.Cells[5].BackColor = GetColor(lblPriorityItemColor.Text.Trim());

            var lblCommentColor = e.Row.FindControl("lblCommentColor") as Literal;
            if (lblCommentColor != null)
                e.Row.Cells[1].BackColor = GetColor(lblCommentColor.Text);

            var StatusValue = e.Row.FindControl("lblStatusValue") as Label;
            if (StatusValue != null)
                e.Row.Cells[2].BackColor = GetColor(StatusValue.Text);

            var lblFirstWeekMONColor = e.Row.FindControl("lblFirstWeekMONColor") as Literal;
            if (lblFirstWeekMONColor != null)
                e.Row.Cells[31].BackColor = GetColor(lblFirstWeekMONColor.Text);

            var lblFirstWeekTUEColor = e.Row.FindControl("lblFirstWeekTUEColor") as Literal;
            if (lblFirstWeekTUEColor != null)
                e.Row.Cells[32].BackColor = GetColor(lblFirstWeekTUEColor.Text);

            var lblFirstWeekWEDColor = e.Row.FindControl("lblFirstWeekWEDColor") as Literal;
            if (lblFirstWeekWEDColor != null)
                e.Row.Cells[33].BackColor = GetColor(lblFirstWeekWEDColor.Text);

            var lblFirstWeekTHUColor = e.Row.FindControl("lblFirstWeekTHUColor") as Literal;
            if (lblFirstWeekTHUColor != null)
                e.Row.Cells[34].BackColor = GetColor(lblFirstWeekTHUColor.Text);

            var lblFirstWeekFRIColor = e.Row.FindControl("lblFirstWeekFRIColor") as Literal;
            if (lblFirstWeekFRIColor != null)
                e.Row.Cells[35].BackColor = GetColor(lblFirstWeekFRIColor.Text);

            var lblSecondWeekMONColor = e.Row.FindControl("lblSecondWeekMONColor") as Literal;
            if (lblSecondWeekMONColor != null)
                e.Row.Cells[36].BackColor = GetColor(lblSecondWeekMONColor.Text);

            var lblSecondWeekTUEColor = e.Row.FindControl("lblSecondWeekTUEColor") as Literal;
            if (lblSecondWeekTUEColor != null)
                e.Row.Cells[37].BackColor = GetColor(lblSecondWeekTUEColor.Text);

            var lblSecondWeekWEDColor = e.Row.FindControl("lblSecondWeekWEDColor") as Literal;
            if (lblSecondWeekWEDColor != null)
                e.Row.Cells[38].BackColor = GetColor(lblSecondWeekWEDColor.Text);

            var lblSecondWeekTHUColor = e.Row.FindControl("lblSecondWeekTHUColor") as Literal;
            if (lblSecondWeekTHUColor != null)
                e.Row.Cells[39].BackColor = GetColor(lblSecondWeekTHUColor.Text);

            var lblSecondWeekFRIColor = e.Row.FindControl("lblSecondWeekFRIColor") as Literal;
            if (lblSecondWeekFRIColor != null)
                e.Row.Cells[40].BackColor = GetColor(lblSecondWeekFRIColor.Text);

            var lblThirdWeekMONColor = e.Row.FindControl("lblThirdWeekMONColor") as Literal;
            if (lblThirdWeekMONColor != null)
                e.Row.Cells[41].BackColor = GetColor(lblThirdWeekMONColor.Text);

            var lblThirdWeekTUEColor = e.Row.FindControl("lblThirdWeekTUEColor") as Literal;
            if (lblThirdWeekTUEColor != null)
                e.Row.Cells[42].BackColor = GetColor(lblThirdWeekTUEColor.Text);

            var lblThirdWeekWEDColor = e.Row.FindControl("lblThirdWeekWEDColor") as Literal;
            if (lblThirdWeekWEDColor != null)
                e.Row.Cells[43].BackColor = GetColor(lblThirdWeekWEDColor.Text);

            var lblThirdWeekTHUColor = e.Row.FindControl("lblThirdWeekTHUColor") as Literal;
            if (lblThirdWeekTHUColor != null)
                e.Row.Cells[44].BackColor = GetColor(lblThirdWeekTHUColor.Text);

            var lblThirdWeekFRIColor = e.Row.FindControl("lblThirdWeekFRIColor") as Literal;
            if (lblThirdWeekFRIColor != null)
                e.Row.Cells[45].BackColor = GetColor(lblThirdWeekFRIColor.Text);

            var lblFourthWeekMONColor = e.Row.FindControl("lblFourthWeekMONColor") as Literal;
            if (lblFourthWeekMONColor != null)
                e.Row.Cells[46].BackColor = GetColor(lblFourthWeekMONColor.Text);

            var lblFourthWeekTUEColor = e.Row.FindControl("lblFourthWeekTUEColor") as Literal;
            if (lblFourthWeekTUEColor != null)
                e.Row.Cells[47].BackColor = GetColor(lblFourthWeekTUEColor.Text);

            var lblFourthWeekWEDColor = e.Row.FindControl("lblFourthWeekWEDColor") as Literal;
            if (lblFourthWeekWEDColor != null)
                e.Row.Cells[48].BackColor = GetColor(lblFourthWeekWEDColor.Text);

            var lblFourthWeekTHUColor = e.Row.FindControl("lblFourthWeekTHUColor") as Literal;
            if (lblFourthWeekTHUColor != null)
                e.Row.Cells[49].BackColor = GetColor(lblFourthWeekTHUColor.Text);

            var lblFourthWeekFRIColor = e.Row.FindControl("lblFourthWeekFRIColor") as Literal;
            if (lblFourthWeekFRIColor != null)
                e.Row.Cells[50].BackColor = GetColor(lblFourthWeekFRIColor.Text);

            #endregion

            var UpdatedDateValue = e.Row.FindControl("lblUpdatedDateValue") as Literal;
            if (UpdatedDateValue != null && ViewState["UpdatedDateValue"] == null)
            {
                ViewState["UpdatedDateValue"] = UpdatedDateValue.Text;
            }

            var lnkEarliestAdviseDateValue = e.Row.FindControl("lnkEarliestAdviseDateValue") as HyperLink;
            if (lnkEarliestAdviseDateValue != null)
                if (string.IsNullOrEmpty(lnkEarliestAdviseDateValue.Text) || string.IsNullOrWhiteSpace(lnkEarliestAdviseDateValue.Text))
                    lnkEarliestAdviseDateValue.Text = "NA";

            //if (string.IsNullOrEmpty((e.Row.FindControl("hdncommentAddedDate") as HiddenField).Value))
            //{
            //   (e.Row.FindControl("pnlToolComment") as Panel).CssClass = "tooltipComment hidediv";
            //(e.Row.FindControl("lnkCommentValue") as HyperLink).CssClass = "";
            //}
            //GetLatestComment(Convert.ToInt32(((System.Web.UI.WebControls.HiddenField)e.Row.FindControl("hdnSKUID")).Value.ToString()),e.Row);

        }
    }

    private System.Drawing.Color GetColor(string colorText)
    {
        var color = new System.Drawing.Color();
        switch (colorText)
        {
            case "R":
                color = System.Drawing.Color.Red;
                break;
            case "O":
                color = System.Drawing.Color.Orange;
                break;
            case "B":
                color = System.Drawing.Color.SkyBlue;
                break;
            case "G":
                color = System.Drawing.Color.LightGreen;
                break;
            case "Y":
                color = System.Drawing.Color.Yellow;
                break;
            default:
                break;
        }

        return color;
    }

    protected void gvPotentialOutput_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["ExpediteStockData"] != null)
        {
            gvPotentialOutput.DataSource = (List<ExpediteStockBE>)ViewState["ExpediteStockData"];
            gvPotentialOutput.PageIndex = e.NewPageIndex;
            if (Session["ExpediteStockOutput"] != null)
            {
                Hashtable htExpediteStockOutput = (Hashtable)Session["ExpediteStockOutput"];

                htExpediteStockOutput.Remove("PageIndex");
                htExpediteStockOutput.Add("PageIndex", e.NewPageIndex);

                Session["ExpediteStockOutput"] = htExpediteStockOutput;
            }
            gvPotentialOutput.DataBind();
        }
    }

    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)ucIncludeVendor.FindControl("lstSelectedVendor");
        ListBox lstVendor = (ListBox)ucIncludeVendor.FindControl("lstVendor");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);
            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                //ucIncludeVendor.innerControlHiddenField.Value = string.Empty;
                //ucIncludeVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    //ucIncludeVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    //ucIncludeVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            //ucIncludeVendor.innerControlHiddenField.Value = string.Empty;
            //ucIncludeVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindExpedite(currnetPageIndx);
    }
    public void pager2_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager2.CurrentIndex = currnetPageIndx;
        BindExpedite(currnetPageIndx);
    }
    

    #endregion

    #region Methods ...

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static object GetSOH(string OD_SKU_NO,string CountryID)
    {

        var lstExpediteStock = new List<ExpediteStockBE>();
        var expediteStock = new ExpediteStockBE();
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
       

        // lstExpediteStock = oMAS_ExpStockBAL.GetExpediteCommentsBAL(expediteStock);
        string strJson = "";
         object result=null;
        // lstExpediteStock=((List<ExpediteStockBE>)HttpContext.Current.Session["ExpediteStockData"]);
        if (HttpContext.Current.Session["ExpediteStockData"] != null)
        {
            result = ((List<ExpediteStockBE>)HttpContext.Current.Session["ExpediteStockData"]).Where(p => p.PurchaseOrder.OD_Code.Trim() == OD_SKU_NO.Trim() && p.CountryID == Convert.ToInt32(CountryID.Trim())).Select(x => new { x.SiteNames_SOHPOP, x.SOH_SOHPOP, x.AVGForcast_SOHPOP }
                ).Distinct().ToList();
            //DataTable dt = new DataTable();
            //dt.Columns.Add("Site", typeof(string));
            //dt.Columns.Add("SOH", typeof(string));
            //dt.Columns.Add("DaysStock", typeof(string));
            //string[] sitename;
            //string[] SOH;
            //string[] DaysStock;
            //foreach (var item in result)
            //{
            //    sitename =item.SiteNames_SOHPOP.Split(',');
            //    SOH = item.SOH_SOHPOP.Split(',');
            //    DaysStock = item.AVGForcast_SOHPOP.Split(',');
            //}
            //for (int i = 0; i < sitename.Length; i++)
            //{
            //    DataRow dr = dt.NewRow(); //Create New Row
            //    dr["Call"] = "Legs";              // Set Column Value
            //    dt.Rows.InsertAt(dr, 11); // InsertAt specified position
            //    dt.Rows[i]["Site"] = sitename[i];            
            //}
            //for (int i = 0; i < SOH.Length; i++)
            //{
            //    dt.Rows[i]["SOH"] = SOH[i];
            //}
            //for (int i = 0; i < DaysStock.Length; i++)
            //{
            //    dt.Rows.[i]["DaysStock"] = DaysStock[i];
            //}
          
        }
        return result;
       
    }




    [System.Web.Services.WebMethod(EnableSession=true)]    
    public static string GetLatestComment(int skuId)
    {
      
        var lstExpediteStock = new List<ExpediteStockBE>();
        var expediteStock = new ExpediteStockBE();
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        expediteStock.Action = "GetLatestComment";
        //expediteStock.SkuID = skuId;
        expediteStock.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
        expediteStock.PurchaseOrder.SKUID = skuId;
      
       // lstExpediteStock = oMAS_ExpStockBAL.GetExpediteCommentsBAL(expediteStock);
        string strJson = "";
       // lstExpediteStock=((List<ExpediteStockBE>)HttpContext.Current.Session["ExpediteStockData"]);
        if (HttpContext.Current.Session["ExpediteStockData"] != null)
        {
            var a = ((List<ExpediteStockBE>)HttpContext.Current.Session["ExpediteStockData"]).Where(p => p.SkuID == skuId).Select(x => new { x.CommentsAdded_Date, x.CommentsAddedBy, x.CommentsValid_Date, x.Reason, x.Comments }
                ).Distinct().ToList();
            if (a.Count > 0)
            {
                expediteStock.CommentsAdded_Date = a[0].CommentsAdded_Date;
                expediteStock.CommentsAddedBy = a[0].CommentsAddedBy;
                expediteStock.CommentsValid_Date = a[0].CommentsValid_Date;
                expediteStock.Reason = a[0].Reason;
                expediteStock.Comments = a[0].Comments;
            }
            lstExpediteStock.Add(expediteStock);  
            List<string> values = new List<string>();           
            if (lstExpediteStock != null && lstExpediteStock.Count > 0)
            {
               // ExpediteStockBE oNewExpediteStock = new ExpediteStockBE();
                //oNewExpediteStock = lstExpediteStock[0];
                if (!string.IsNullOrEmpty(Convert.ToString(expediteStock.CommentsAdded_Date)))
                {
                    strJson = (Convert.ToDateTime(expediteStock.CommentsAdded_Date.ToString()).ToString("dd/MM/yyyy"));
                }
                strJson = strJson + "|" + (expediteStock.CommentsAddedBy);
                strJson = strJson + "|" + (expediteStock.CommentsValid_Date == null ? string.Empty : Convert.ToDateTime(expediteStock.CommentsValid_Date.ToString()).ToString("dd/MM/yyyy"));
                strJson = strJson + "|" + (expediteStock.Reason);
                strJson = strJson + "|" + (expediteStock.Comments);

                //DateTime date = Convert.ToDateTime(oNewExpediteStock.CommentsAdded_Date.ToString());
                //Label lblTemp = new Label();
                //lblTemp = row.FindControl("CommentDate") as Label;
                //lblTemp.Text = Convert.ToDateTime(oNewExpediteStock.CommentsAdded_Date.ToString()).ToString("dd/MM/yyyy HH:mm");

                //lblTemp = row.FindControl("CommentAddedBy") as Label;
                //lblTemp.Text = oNewExpediteStock.CommentsAddedBy;

                ////lblTemp = row.FindControl("CommentValid") as Label;
                ////lblTemp.Text = Convert.ToDateTime(oNewExpediteStock.CommentsValid_Date.ToString()).ToString("dd/MM/yyyy HH:mm");

                //lblTemp = row.FindControl("CommentReason") as Label;
                //lblTemp.Text = oNewExpediteStock.Reason;

                //lblTemp = row.FindControl("CommentText") as Label;
                //lblTemp.Text = oNewExpediteStock.Comments;


            }
        }
        return strJson;
    }
    private void BindExpedite(int Page = 1)
    {
        var oExpediteStockBE = new ExpediteStockBE();
        var oExpediteStockBAL = new ExpediteStockBAL();

        if (rblSearchType.SelectedItem.Text == "Day Wise")
        {
            oExpediteStockBE.Action = "GetPotentialOutput";
            hdnrblSearchType.Value = "0";
        }
        else
        {
            oExpediteStockBE.Action = "GetPotentialOutputForWeekly";
            hdnrblSearchType.Value = "1";
        }

        oExpediteStockBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oExpediteStockBE.SelectedStockPlannerName= msStockPlanner.SelectedSPName;
        oExpediteStockBE.SelectedStockPlannerUserIDs = msStockPlanner.SelectedStockPlannerUserIDs;

        if (string.IsNullOrEmpty(ucSeacrhVendor1.VendorNo))
        {
            oExpediteStockBE.SelectedIncludedVendorIDs = this.GetSelectedVendorIds(ucIncludeVendor);
        }
        else
        {
            oExpediteStockBE.SelectedIncludedVendorIDs = ucSeacrhVendor1.VendorNo;
        }
        oExpediteStockBE.SelectedExcludedVendorIDs = this.GetSelectedVendorIds(ucExcludeVendor);
        if (ddlSite.innerControlddlSite.SelectedIndex==0)
        {
            oExpediteStockBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        }
        else
        {
            oExpediteStockBE.SelectedSiteIDs = ddlSite.innerControlddlSite.SelectedValue;   
        }

        oExpediteStockBE.SelectedItemClassification = msItemClassification.selectedItemClassification;

        if (!string.IsNullOrEmpty(ucMRPType.SelectedMRPTypes))
            oExpediteStockBE.SelectedMRPType = ucMRPType.SelectedMRPTypes.Replace(", ", ",");

        if (!string.IsNullOrEmpty(UcPurcGrp.SelectedPurGrp))
            oExpediteStockBE.SelectedPurcGroup = UcPurcGrp.SelectedPurGrp.Replace(", ", ",");

        if (!string.IsNullOrEmpty(UcMatGroup.SelectedMatGroup))
            oExpediteStockBE.SelectedMatGrp = UcMatGroup.SelectedMatGroup.Replace(", ", ",");

        if (!string.IsNullOrEmpty(txtVikingSku.Text))
        {
            oExpediteStockBE.SelectedVIKINGSKU = txtVikingSku.Text.Trim();
        }
        else
        {
            oExpediteStockBE.SelectedVIKINGSKU = UcVikingSku.SelectedSKUName;
        }       

        if (isExpeditesubfilter == true && !string.IsNullOrEmpty(hdnEndDate.Value))
        {
            if (rblSearchType.SelectedItem.Text == "Day Wise")
            {
                oExpediteStockBE.SortingDay = GetDaySorting();
            }
        }

        if (!string.IsNullOrEmpty(txtODSKUCode.Text))
        {
            oExpediteStockBE.SelectedODSKU = txtODSKUCode.Text.Trim();
        }
        else
        {
            oExpediteStockBE.SelectedODSKU = msSKU.SelectedSKUName;       
        }
       
        oExpediteStockBE.SelectedSkuType = Convert.ToInt32(rblSkuType.SelectedItem.Value);
        oExpediteStockBE.SelectedDisplay = Convert.ToInt32(rblDisplay.SelectedItem.Value);

        oExpediteStockBE.SortBy = rblSortBy.SelectedItem.Value;
        oExpediteStockBE.Page = Page;
        if (!string.IsNullOrEmpty(txtDayStock.Text))
        {
            oExpediteStockBE.DaysStock = "D" + txtDayStock.Text.Trim() + "";
        }
       
        oExpediteStockBE.MinForecastedSales = txtMinForecastedSales.Text;
        oExpediteStockBE.StockPlannerGroupingID = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
        oExpediteStockBE.SkugroupingID = multiSelectSKUGrouping.SelectedSkuGroupingIDs;
        if (string.IsNullOrEmpty(txtMinForecastedSales.Text))
        {
            oExpediteStockBE.MinForecastedSales = "0";
        }

        if (!rblPOStatus.SelectedValue.Equals("0"))
            oExpediteStockBE.POStatusSearchCriteria = Convert.ToInt32(rblPOStatus.SelectedValue);
        else
            oExpediteStockBE.POStatusSearchCriteria = 0;

        if (!rblCommentStatus.SelectedValue.Equals("0"))
            oExpediteStockBE.CommentStatus = Convert.ToInt32(rblCommentStatus.SelectedValue);
        else
            oExpediteStockBE.CommentStatus = 0;

        if (rdoPriorityShowall.Checked)
        {
            oExpediteStockBE.IsPriority = false;
        }
        else
        {
            oExpediteStockBE.IsPriority = true;
        }

        var lstExpediteStock = oExpediteStockBAL.GetPotentialOutputBAL(oExpediteStockBE);
        //ViewState["ExpediteStockData"] = lstExpediteStock;
        // Session["UsersOverSearch"] = oExpediteStockBE;
        if (lstExpediteStock != null && lstExpediteStock.Count > 0)
        {
            if (rblSearchType.SelectedItem.Text == "Day Wise")
            {
                tblPotentialOutputDayWise.Visible = true;
                tblPotentialOutputWeekWise.Visible = false;
                gvPotentialOutput.DataSource = lstExpediteStock;
                gvPotentialOutput.DataBind();
                gvPotentialOutput.PageIndex = Page;
                pager1.CurrentIndex = Page;
                pager1.ItemCount = lstExpediteStock[0].TotalRecords;
                pager1.Visible = true;
                btnBack.Visible = true;
                btnBackWeekWise.Visible = false;
                pager2.Visible = false;            
                btnExpediteSelection.Visible = true;               
            }
            else
            {
                tblPotentialOutputDayWise.Visible = false;
                tblPotentialOutputWeekWise.Visible = true;
                gvPotentialOutputWeekWise.DataSource = lstExpediteStock;
                gvPotentialOutputWeekWise.DataBind();
                gvPotentialOutputWeekWise.PageIndex = Page;
                pager2.CurrentIndex = Page;
                pager2.ItemCount = lstExpediteStock[0].TotalRecords;
                pager2.Visible = true;
                btnBack.Visible = false;
                btnBackWeekWise.Visible = true;
                pager1.Visible = false;
                btnExpediteSelection.Visible = false;
            }

            
            //gvPotentialOutputExcelExp.DataSource = lstExpediteStock;
          
            btnAdditionalSearch.Enabled = true;
        }
        else
        {
            if (rblSearchType.SelectedItem.Text == "Day Wise")
            {
                gvPotentialOutput.DataSource = null;
                gvPotentialOutput.DataBind();
                //gvPotentialOutputExcelExp.DataSource = null;
                pager1.ItemCount = 0;
                btnBack.Visible = true;
                btnBackWeekWise.Visible = false;
            }
            else
            {
                gvPotentialOutputWeekWise.DataSource = null;
                gvPotentialOutputWeekWise.DataBind();
                //gvPotentialOutputExcelExp.DataSource = null;
                pager2.ItemCount = 0;
                btnBack.Visible = false;
                btnBackWeekWise.Visible = true;
            }

            pager1.Visible = false;
            pager2.Visible = false;

            btnAdditionalSearch.Enabled = false;
            //gvPotentialOutput.Width = System.Web.UI.WebControls.Unit.Percentage(80);

        }

       
        //gvPotentialOutputExcelExp.DataBind();
        Session["ExpediteStockData"] = lstExpediteStock;
        pnlSearchScreen.Visible = false;
        pnlPotentialOutput.Visible = true;

        if (gvPotentialOutput.Rows.Count > 0 || gvPotentialOutputWeekWise.Rows.Count > 0)
            ucExportToExcel1.Visible = true;

        msStockPlanner.SetSPOnPostBack();
        msSite.setSitesOnPostBack();
        ucIncludeVendor.SetVendorOnPostBack();
        ucExcludeVendor.SetVendorOnPostBack();
        ucMRPType.setMRTypeOnPostBack();
        UcPurcGrp.setPurGroupOnPostBack();
        UcMatGroup.setMatGroupOnPostBack();
        UcVikingSku.setVikingSkuOnPostBack();
        msSKU.setODSkuOnPostBack();
        SetSession(oExpediteStockBE);
        if (isExpeditesubfilter == false)
        {
            if (lstExpediteStock != null && lstExpediteStock.Count > 0)
            {
                txtFromDate.Text = Common.ToDateTimeInMMDDYYYY(lstExpediteStock[0].UpdatedDate);
                txtToDate.Text = Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(lstExpediteStock[0].UpdatedDate), 19));
                hdnStartDate.Value = Convert.ToString(lstExpediteStock[0].UpdatedDate);
                hdnEndDate.Value = Common.ToDateTimeInMMDDYYYY(lstExpediteStock[0].UpdatedDate);
            }
            ddlSite.IsExpediteSites = true;
            ddlSite.siteIDs = msSite.SelectedSiteIDs;
            ddlSite.BindSites();
            ddlSite.innerControlddlSite.SelectedIndex = 0;
        }
        //ScriptManager.RegisterStartupScript(SM1, SM1.GetType(), "script", "getdate()", true);   

    }

    private List<ExpediteStockBE> Get20DaysDate(DateTime updateDate)
    {
        var loopMaxCount = 35;
        var dayDateMaxCount = 20;
        var updateDateValue = updateDate;
        var dayUpdateDateValue = updateDateValue.ToString("ddd");
        var lstExpediteStock = new List<ExpediteStockBE>();
        for (int index = 0; index < loopMaxCount; index++)
        {
            if (lstExpediteStock.Count < dayDateMaxCount)
            {
                var nextDate = new DateTime();
                var nextDay = string.Empty;
                if (index.Equals(0))
                {
                    nextDate = updateDateValue;
                    nextDay = nextDate.ToString("ddd");
                }
                else
                {
                    nextDate = updateDateValue.AddDays(index);
                    nextDay = nextDate.ToString("ddd");
                }

                if (nextDay.Equals("Mon") || nextDay.Equals("Tue") || nextDay.Equals("Wed") || nextDay.Equals("Thu") || nextDay.Equals("Fri"))
                {
                    var expediteStock = new ExpediteStockBE();
                    expediteStock.UpdateDay = string.Format("{0}{1}", nextDay, index);
                    var day = Convert.ToString(nextDate.Date.Day);
                    var month = Convert.ToString(nextDate.Date.Month);

                    if (day.Length.Equals(1))
                        day = string.Format("0{0}", day);

                    if (month.Length.Equals(1))
                        month = string.Format("0{0}", month);

                    expediteStock.UpdateDate = string.Format("{0}/{1}", day, month);
                    lstExpediteStock.Add(expediteStock);
                }
            }
            else
                break;
        }

        return lstExpediteStock;
    }

    private void ClearCriteria()
    {
        //msStockPlanner.SelectedStockPlannerIDs = string.Empty;
        //ucIncludeVendor.SelectedVendorIDs = string.Empty;
        //ucExcludeVendor.SelectedVendorIDs = string.Empty;
        msSite.SelectedSiteIDs = string.Empty;
        //msItemClassification.selectedItemClassification = string.Empty;
        //ucMRPType.SelectedMRPTypes = string.Empty;
        //UcPurcGrp.SelectedPurGrp = string.Empty;
        //UcMatGroup.SelectedMatGroup = string.Empty;
        UcVikingSku.SelectedSKUName = string.Empty;
        msSKU.SelectedSKUName = string.Empty;
    }

    private string GetSelectedVendorIds(ucMultiSelectVendor multiSelectVendor)
    {
        ucListBox lstSelectedVendor = (ucListBox)multiSelectVendor.FindControl("lstSelectedVendor");
        var vendorIds = string.Empty;
        for (int index = 0; index < lstSelectedVendor.Items.Count; index++)
        {
            if (index.Equals(0))
                vendorIds = lstSelectedVendor.Items[index].Value;
            else
                vendorIds = string.Format("{0},{1}", vendorIds, lstSelectedVendor.Items[index].Value);
        }
        return vendorIds;
    }

    private void SetSession(ExpediteStockBE oExpediteStockBE)
    {
        Session["ExpediteStockOutput"] = null;
        Session.Remove("ExpediteStockOutput");
        oExpediteStockBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
        oExpediteStockBE.SelectedIncludedVendorIDs = this.GetSelectedVendorIds(ucIncludeVendor);
        oExpediteStockBE.SelectedVIKINGSKU = UcVikingSku.SelectedSKUName;
        oExpediteStockBE.SelectedODSKU = msSKU.SelectedSKUName;

        Hashtable htExpediteStockOutput = new Hashtable();
        htExpediteStockOutput.Add("AdditionalSelectedSiteIDs", ddlSite.innerControlddlSite.SelectedValue);
        htExpediteStockOutput.Add("AdditionalSelectedIncludedVendorIDs", ucSeacrhVendor1.VendorNo);
        htExpediteStockOutput.Add("AdditionalSelectedVIKINGSKU", txtVikingSku.Text);
        htExpediteStockOutput.Add("AdditionalSelectedODSKU", txtODSKUCode.Text);
        htExpediteStockOutput.Add("isExpeditesubfilter", isExpeditesubfilter);
        htExpediteStockOutput.Add("SelectedSkuGroupingIDs", multiSelectSKUGrouping.SelectedSkuGroupingIDs);
        htExpediteStockOutput.Add("SelectedStockPlannerGroupingIDs", multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs);
        htExpediteStockOutput.Add("SelectedStockPlannerIDs", oExpediteStockBE.SelectedStockPlannerIDs);        
        htExpediteStockOutput.Add("SelectedIncludedVendorIDs", oExpediteStockBE.SelectedIncludedVendorIDs);
        htExpediteStockOutput.Add("SelectedExcludedVendorIDs", oExpediteStockBE.SelectedExcludedVendorIDs);
        htExpediteStockOutput.Add("SelectedSiteIDs", oExpediteStockBE.SelectedSiteIDs);
        htExpediteStockOutput.Add("SelectedItemClassification", oExpediteStockBE.SelectedItemClassification);
        htExpediteStockOutput.Add("SelectedMRPType", oExpediteStockBE.SelectedMRPType);
        htExpediteStockOutput.Add("SelectedPurcGroup", oExpediteStockBE.SelectedPurcGroup);
        htExpediteStockOutput.Add("SelectedMatGrp", oExpediteStockBE.SelectedMatGrp);
        htExpediteStockOutput.Add("SelectedVIKINGSKU", oExpediteStockBE.SelectedVIKINGSKU);
        htExpediteStockOutput.Add("SelectedODSKU", oExpediteStockBE.SelectedODSKU);
        htExpediteStockOutput.Add("SelectedSkuType", oExpediteStockBE.SelectedSkuType);
        htExpediteStockOutput.Add("SelectedDisplay", oExpediteStockBE.SelectedDisplay);
        htExpediteStockOutput.Add("PageIndex", 0);
        htExpediteStockOutput.Add("SortBy", oExpediteStockBE.SortBy);
        htExpediteStockOutput.Add("POStatus", oExpediteStockBE.POStatusSearchCriteria);
        htExpediteStockOutput.Add("CommentStatus", oExpediteStockBE.CommentStatus);
        htExpediteStockOutput.Add("DaysStock", oExpediteStockBE.DaysStock);
        htExpediteStockOutput.Add("MinForecastedSales", oExpediteStockBE.MinForecastedSales);
        htExpediteStockOutput.Add("SortingDay", oExpediteStockBE.SortingDay);
        htExpediteStockOutput.Add("SelectedStockPlannerUserIDs", oExpediteStockBE.SelectedStockPlannerUserIDs);
        TextBox txtSearchedStockPlanner= msStockPlanner.FindControl("txtUserName") as TextBox;
        htExpediteStockOutput.Add("SelectedStockPlannerName", txtSearchedStockPlanner.Text);

        TextBox txtSearchedVendorInclude = ucIncludeVendor.FindControl("txtVendorNo") as TextBox;
        htExpediteStockOutput.Add("SelectedIncludedVendor", txtSearchedVendorInclude.Text);

        TextBox txtSearchedVendorExclude = ucExcludeVendor.FindControl("txtVendorNo") as TextBox;
        htExpediteStockOutput.Add("SelectedExcludeVendor", txtSearchedVendorExclude.Text);
        if (rdoPriorityShowall.Checked)
        {
            htExpediteStockOutput.Add("PriorityItems", false);
        }
        else
        {
            htExpediteStockOutput.Add("PriorityItems", true);
        }

        if (rblSearchType.SelectedItem.Text == "Day Wise")
        {
            htExpediteStockOutput.Add("hdnrblSearchType", "0");
        }
        else
        {
            htExpediteStockOutput.Add("hdnrblSearchType", "1");
        }

        //TextBox txtSearchedVikingCode = UcVikingSku.FindControl("txtOfficeDepotSKU") as TextBox;
        // htExpediteStockOutput.Add("SelectedVikingSku", txtSearchedVikingCode.Text);
        Session["ExpediteStockOutput"] = htExpediteStockOutput;
    }

    private void GetSession()
    {

        ExpediteStockBE oExpediteStockBE = new ExpediteStockBE();
        ExpediteStockBAL oExpediteStockBAL = new ExpediteStockBAL();
        if (Session["ExpediteStockOutput"] != null)
        {
            Hashtable htExpediteStockOutput = (Hashtable)Session["ExpediteStockOutput"];

            oExpediteStockBE.SelectedStockPlannerIDs = (htExpediteStockOutput.ContainsKey("SelectedStockPlannerIDs") && htExpediteStockOutput["SelectedStockPlannerIDs"] != null) ? htExpediteStockOutput["SelectedStockPlannerIDs"].ToString() : "";

            oExpediteStockBE.SelectedIncludedVendorIDs = (htExpediteStockOutput.ContainsKey("SelectedIncludedVendorIDs") && htExpediteStockOutput["SelectedIncludedVendorIDs"] != null) ? htExpediteStockOutput["SelectedIncludedVendorIDs"].ToString() : "";
            oExpediteStockBE.SelectedExcludedVendorIDs = (htExpediteStockOutput.ContainsKey("SelectedExcludedVendorIDs") && htExpediteStockOutput["SelectedExcludedVendorIDs"] != null) ? htExpediteStockOutput["SelectedExcludedVendorIDs"].ToString() : "";
            oExpediteStockBE.SelectedSiteIDs = (htExpediteStockOutput.ContainsKey("SelectedSiteIDs") && htExpediteStockOutput["SelectedSiteIDs"] != null) ? htExpediteStockOutput["SelectedSiteIDs"].ToString() : "";
            oExpediteStockBE.SelectedItemClassification = (htExpediteStockOutput.ContainsKey("SelectedItemClassification") && htExpediteStockOutput["SelectedItemClassification"] != null) ? htExpediteStockOutput["SelectedItemClassification"].ToString() : "";
            oExpediteStockBE.SelectedMRPType = (htExpediteStockOutput.ContainsKey("SelectedMRPType") && htExpediteStockOutput["SelectedMRPType"] != null) ? htExpediteStockOutput["SelectedMRPType"].ToString() : "";
            oExpediteStockBE.SelectedPurcGroup = (htExpediteStockOutput.ContainsKey("SelectedPurcGroup") && htExpediteStockOutput["SelectedPurcGroup"] != null) ? htExpediteStockOutput["SelectedPurcGroup"].ToString() : "";
            oExpediteStockBE.SelectedMatGrp = (htExpediteStockOutput.ContainsKey("SelectedMatGrp") && htExpediteStockOutput["SelectedMatGrp"] != null) ? htExpediteStockOutput["SelectedMatGrp"].ToString() : "";
            oExpediteStockBE.SelectedVIKINGSKU = (htExpediteStockOutput.ContainsKey("SelectedVIKINGSKU") && htExpediteStockOutput["SelectedVIKINGSKU"] != null) ? htExpediteStockOutput["SelectedVIKINGSKU"].ToString() : "";
            oExpediteStockBE.SelectedODSKU = (htExpediteStockOutput.ContainsKey("SelectedODSKU") && htExpediteStockOutput["SelectedODSKU"] != null) ? htExpediteStockOutput["SelectedODSKU"].ToString() : "";
            oExpediteStockBE.SelectedSkuType = (htExpediteStockOutput.ContainsKey("SelectedSkuType") && htExpediteStockOutput["SelectedSkuType"] != null) ? Convert.ToInt32(htExpediteStockOutput["SelectedSkuType"].ToString()) : 0;
            oExpediteStockBE.SelectedDisplay = (htExpediteStockOutput.ContainsKey("SelectedDisplay") && htExpediteStockOutput["SelectedDisplay"] != null) ? Convert.ToInt32(htExpediteStockOutput["SelectedDisplay"].ToString()) : 0;
            gvPotentialOutput.PageIndex = Convert.ToInt32(htExpediteStockOutput["PageIndex"].ToString());
            oExpediteStockBE.SortBy = (htExpediteStockOutput.ContainsKey("SortBy") && htExpediteStockOutput["SortBy"] != null) ? htExpediteStockOutput["SortBy"].ToString() : string.Empty;

            oExpediteStockBE.SkugroupingID = (htExpediteStockOutput.ContainsKey("SelectedSkuGroupingIDs") && htExpediteStockOutput["SelectedSkuGroupingIDs"] != null) ? htExpediteStockOutput["SelectedSkuGroupingIDs"].ToString() : string.Empty;
            string hdnrblSearchType = (htExpediteStockOutput.ContainsKey("hdnrblSearchType") && htExpediteStockOutput["hdnrblSearchType"] != null) ? htExpediteStockOutput["hdnrblSearchType"].ToString() : string.Empty;
            if (!string.IsNullOrEmpty(hdnrblSearchType.ToString()))
            {
                if (hdnrblSearchType.ToString() == "0")
                {
                    oExpediteStockBE.Action = "GetPotentialOutput";
                }
                else
                {
                    oExpediteStockBE.Action = "GetPotentialOutputForWeekly";
                }
            }
            

            var lstExpediteStock = oExpediteStockBAL.GetPotentialOutputBAL(oExpediteStockBE);
            //************************************************
            //rblDisplay.SelectedValue = (htExpediteStockOutput.ContainsKey("SelectedDisplay") && htExpediteStockOutput["SelectedDisplay"] != null) ? Convert.ToInt32(htExpediteStockOutput["SelectedDisplay"].ToString()) : 0;
            //************************************************
            ViewState["ExpediteStockData"] = lstExpediteStock;
            Session["ExpediteStockData"] = lstExpediteStock;
            if (lstExpediteStock != null && lstExpediteStock.Count > 0)
            {
                if (!string.IsNullOrEmpty(hdnrblSearchType.ToString()))
                {
                    if (hdnrblSearchType.ToString() == "0")
                    {
                        gvPotentialOutput.DataSource = lstExpediteStock;
                        //gvPotentialOutputExcelExp.DataSource = lstExpediteStock;
                        pager1.ItemCount = lstExpediteStock[0].TotalRecords;
                        gvPotentialOutput.DataBind();
                        //gvPotentialOutputExcelExp.DataBind();

                        pager1.CurrentIndex = Convert.ToInt32(htExpediteStockOutput["PageIndex"].ToString());
                    }
                    else
                    {
                        gvPotentialOutputWeekWise.DataSource = lstExpediteStock;
                        pager2.ItemCount = lstExpediteStock[0].TotalRecords;
                        gvPotentialOutputWeekWise.DataBind();


                        pager2.CurrentIndex = Convert.ToInt32(htExpediteStockOutput["PageIndex"].ToString());
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(hdnrblSearchType.ToString()))
                    {
                        if (hdnrblSearchType.ToString() == "0")
                        {
                            gvPotentialOutput.DataSource = null;
                            //gvPotentialOutputExcelExp.DataSource = null;
                        }
                        else
                        {
                            gvPotentialOutputWeekWise.DataSource = null;
                        }
                    }
                }



                pnlSearchScreen.Visible = false;
                pnlPotentialOutput.Visible = true;

                if (gvPotentialOutput.Rows.Count > 0 || gvPotentialOutputWeekWise.Rows.Count > 0)
                    ucExportToExcel1.Visible = true;
                if (!string.IsNullOrEmpty(hdnrblSearchType.ToString()))
                {
                    if (hdnrblSearchType.ToString() == "0")
                    {
                        #region Logic to set the 20 days header text ...
                        var lstExpediteDayDays = this.Get20DaysDate(Convert.ToDateTime(ViewState["UpdatedDateValue"]));
                        if (lstExpediteDayDays.Count > 0)
                        {
                            #region Changing header of Display Grid ...
                            for (int index = 0; index < gvPotentialOutput.Columns.Count; index++)
                            {
                                switch (gvPotentialOutput.Columns[index].HeaderText)
                                {
                                    case "FirstWeekMon":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[0].UpdateDate;
                                        break;
                                    case "FirstWeekTue":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[1].UpdateDate;
                                        break;
                                    case "FirstWeekWed":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[2].UpdateDate;
                                        break;
                                    case "FirstWeekThu":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[3].UpdateDate;
                                        break;
                                    case "FirstWeekFri":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[4].UpdateDate;
                                        break;
                                    case "SecondWeekMon":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[5].UpdateDate;
                                        break;
                                    case "SecondWeekTue":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[6].UpdateDate;
                                        break;
                                    case "SecondWeekWed":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[7].UpdateDate;
                                        break;
                                    case "SecondWeekThu":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[8].UpdateDate;
                                        break;
                                    case "SecondWeekFri":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[9].UpdateDate;
                                        break;
                                    case "ThirdWeekMon":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[10].UpdateDate;
                                        break;
                                    case "ThirdWeekTue":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[11].UpdateDate;
                                        break;
                                    case "ThirdWeekWed":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[12].UpdateDate;
                                        break;
                                    case "ThirdWeekThu":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[13].UpdateDate;
                                        break;
                                    case "ThirdWeekFri":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[14].UpdateDate;
                                        break;
                                    case "FourthWeekMon":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[15].UpdateDate;
                                        break;
                                    case "FourthWeekTue":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[16].UpdateDate;
                                        break;
                                    case "FourthWeekWed":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[17].UpdateDate;
                                        break;
                                    case "FourthWeekThu":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[18].UpdateDate;
                                        break;
                                    case "FourthWeekFri":
                                        gvPotentialOutput.Columns[index].HeaderText = lstExpediteDayDays[19].UpdateDate;
                                        break;
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                }
            }

        }
    }

    private void BindDropDowns()
    {
        /* SKU Type Data binding */
        var listItem = new ListItem(string.Format("{0} ", WebCommon.getGlobalResourceValue("ExpditeSKUTypeItem1")), "1");
        listItem.Selected = true;
        rblSkuType.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditeSKUTypeItem2"), "2");
        rblSkuType.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditeSKUTypeItem3"), "3");
        rblSkuType.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditeSKUTypeItem4"), "4");
        rblSkuType.Items.Add(listItem);

        /* Display Data binding */
        listItem = new ListItem(string.Format("{0} ", WebCommon.getGlobalResourceValue("ExpditeDisplayItem1")), "1");
        rblDisplay.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditeDisplayItem2"), "2");
        rblDisplay.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditeDisplayItem3"), "3");
        rblDisplay.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditeDisplayItem4"), "4");
        listItem.Selected = true;
        rblDisplay.Items.Add(listItem);

        /* Sort By Data binding */
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditeSortByItem1"), "V");
        rblSortBy.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditeSortByItem2"), "FB");
        listItem.Selected = true;
        rblSortBy.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditeSortByItem3"), "CBC");
        rblSortBy.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditeSortByItem4"), "CBQ");
        rblSortBy.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("StockOnHand"), "SOH");
        rblSortBy.Items.Add(listItem);

        /* PO Status Data binding */
        listItem = new ListItem(string.Format("{0} ", WebCommon.getGlobalResourceValue("ExpditePOStatusItem1")), "0");
        listItem.Selected = true;
        rblPOStatus.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditePOStatusItem2"), "1");
        rblPOStatus.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditePOStatusItem3"), "2");
        rblPOStatus.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditePOStatusItem4"), "3");
        rblPOStatus.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("ExpditePOStatusItem5"), "4");
        rblPOStatus.Items.Add(listItem);

        /* Comment Status Data binding */
        listItem = new ListItem(string.Format("{0} ", WebCommon.getGlobalResourceValue("ExpditePOStatusItem1")), "0"); // First one for All text
        listItem.Selected = true;
        rblCommentStatus.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("NoCommentUpdate"), "1");
        rblCommentStatus.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("CommentApplied"), "2");
        rblCommentStatus.Items.Add(listItem);
        listItem = new ListItem(WebCommon.getGlobalResourceValue("Expedited"), "3");
        rblCommentStatus.Items.Add(listItem);

    }

    #endregion
    protected void btnExpediteSelection_Click(object sender, EventArgs e)
    {
        ExpediteStockBE oSessionQueryString = new ExpediteStockBE();
        string YouSelectSKUsOneCountry = WebCommon.getGlobalResourceValue("YouSelectSKUsOneCountry");
        string PlzSelectOneSKU = WebCommon.getGlobalResourceValue("PlzSelectOneSKU");
        //string SKUID=string.Empty;
        //string VendorID =string.Empty;
        //string DirectCode =string.Empty;
        //string ODCode =string.Empty;
        //string Status =string.Empty;
        //string Disc =string.Empty;
        //string SiteId = string.Empty;
        //string CountryId = string.Empty;
        int countryStatus = 0;
        //foreach (GridViewRow gdv in gvPotentialOutput.Rows)
        //{
        //    CheckBox chk = (CheckBox)gdv.FindControl("chkSelect");
        //    HiddenField hdnSKUid = (HiddenField)gdv.FindControl("hdnSKUID");
        //    HiddenField hdnVendorId = (HiddenField)gdv.FindControl("hdnVendorId");
        //    HiddenField hdnDirectCode = (HiddenField)gdv.FindControl("hdnDirectCode");
        //    HiddenField hdnOdCode = (HiddenField)gdv.FindControl("hdnOdCode");
        //    HiddenField hdnStatus = (HiddenField)gdv.FindControl("hdnStatus");
        //    HiddenField hdnDisc = (HiddenField)gdv.FindControl("hdnDisc");
        //    HiddenField hdnSiteId = (HiddenField)gdv.FindControl("hdnSiteId");
        //    HiddenField hdnCountryId = (HiddenField)gdv.FindControl("hdnCountryID");
        //    if (chk.Checked)
        //    {
        //        checkStatus++;
        oSessionQueryString.skuids = hdnSKUID.Value + ",";
        oSessionQueryString.Vendors = hdnVendorId.Value + ",";
        oSessionQueryString.DirectCodes = hdnDirectCode.Value + ",";
        oSessionQueryString.Statuss = hdnStatus.Value + ",";
        oSessionQueryString.Discs = hdnDisc.Value + ",";
        oSessionQueryString.SiteIds = hdnSiteId.Value + ",";
        oSessionQueryString.ODCodes = hdnOdCode.Value + ",";
        oSessionQueryString.CountryIds = hdnCountryID.Value + ",";
        //if (CountryId.LastIndexOf(','))
        //{ 

        //}

        //}

        //}
        if (string.IsNullOrEmpty(oSessionQueryString.CountryIds))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + YouSelectSKUsOneCountry + "')", true);
        }
        else
        {
            if (Session["QueryString"] != null)
            {
                Session["QueryString"] = null;
            }
            Session["QueryString"] = oSessionQueryString;


            //    if (checkStatus > 0)
            //    {
            //string url = EncryptQuery("ExpediteComments.aspx?SiteID=" + SiteId + "&SKUID=" + SKUID + "&VendorID=" + VendorID + "&Viking=" + DirectCode + "&ODCode=" + ODCode + "&Status=" + Status + "&Desc=" + Disc + "");
            string url = EncryptQuery("ExpediteComments.aspx");
            ShowWarningMessages();
            ViewState["url"] = url;
            //if (checkStatus > 1)
            //{
            //    ShowWarningMessages();
            //    ViewState["url"] = url;
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "AddComment", "AddComment('" + url + "');", true);
            //}
            // }
            //else
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('"+PlzSelectOneSKU+"')", true);
            //}
        }
    }
    private void ShowWarningMessages()
    {
        if (hdnSKUID.Value == "" || hdnSKUID.Value == ",")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "AddCommentCheck", "checkCommentCount();", true);
        }
        else
        {
            string MultiExpediteWorning = WebCommon.getGlobalResourceValue("MultiExpediteWorning").Replace("#", Convert.ToString(hdnSKUID.Value.Split(',').Count()));
            ltConfirmMsg.Text = MultiExpediteWorning;
            btnErrContinue.CommandName = "Continue";
            btnErrBack.CommandName = "Back";
            mdlConfirmMsg.Show();
        }


    }
    protected void btnErrContinue_Command(object sender, CommandEventArgs e)
    {
        //Response.Write("<script language='javascript'>window.open('" + url + "', '_blank', '');</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "AddComment", "AddComment('" + ViewState["url"].ToString() + "');", true);
    }
    protected void btnErrBack_Command(object sender, CommandEventArgs e)
    {

    }
    public DateTime AddBusinessDays(DateTime current, int days)
    {
        var sign = Math.Sign(days);
        var unsignedDays = Math.Abs(days);
        for (var i = 0; i < unsignedDays; i++)
        {
            do
            {
                current = current.AddDays(sign);
            }
            while (current.DayOfWeek == DayOfWeek.Saturday ||
                current.DayOfWeek == DayOfWeek.Sunday);
        }
        return current;
    }
    public string GetDaySorting()
    {
        string sortingDay = string.Empty;
        if (!string.IsNullOrEmpty(hdnStartDate.Value))
        {
            if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 0)) == hdnEndDate.Value)
            {
                sortingDay = "D1";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 1)) == hdnEndDate.Value)
            {
                sortingDay = "D2";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 2)) == hdnEndDate.Value)
            {
                sortingDay = "D3";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 3)) == hdnEndDate.Value)
            {
                sortingDay = "D4";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 4)) == hdnEndDate.Value)
            {
                sortingDay = "D5";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 5)) == hdnEndDate.Value)
            {
                sortingDay = "D6";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 6)) == hdnEndDate.Value)
            {
                sortingDay = "D7";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 7)) == hdnEndDate.Value)
            {
                sortingDay = "D8";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 8)) == hdnEndDate.Value)
            {
                sortingDay = "D9";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 9)) == hdnEndDate.Value)
            {
                sortingDay = "D10";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 10)) == hdnEndDate.Value)
            {
                sortingDay = "D11";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 11)) == hdnEndDate.Value)
            {
                sortingDay = "D12";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 12)) == hdnEndDate.Value)
            {
                sortingDay = "D13";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 13)) == hdnEndDate.Value)
            {
                sortingDay = "D14";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 14)) == hdnEndDate.Value)
            {
                sortingDay = "D15";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 15)) == hdnEndDate.Value)
            {
                sortingDay = "D16";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 16)) == hdnEndDate.Value)
            {
                sortingDay = "D17";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 17)) == hdnEndDate.Value)
            {
                sortingDay = "D18";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 18)) == hdnEndDate.Value)
            {
                sortingDay = "D19";
            }
            else if (Common.ToDateTimeInMMDDYYYY(AddBusinessDays(Convert.ToDateTime(hdnStartDate.Value), 19)) == hdnEndDate.Value)
            {
                sortingDay = "D20";
            }
        }

        return sortingDay;
    }
}
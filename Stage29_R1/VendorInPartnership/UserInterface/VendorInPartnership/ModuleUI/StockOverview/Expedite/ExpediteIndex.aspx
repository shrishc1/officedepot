﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ExpediteIndex.aspx.cs" Inherits="ModuleUI_StockOverview_Expedite_ExpediteIndex" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:uclabel id="lblExpediteIndex" runat="server" text="Expedite Index"></cc1:uclabel>
    </h2>
    <br />
    <br />
    <ul>
        <li>
            <asp:HyperLink ID="hlSearchScreen" ForeColor="Blue" runat="server" Text="Search Screen"
                NavigateUrl="ExpediteStockSearch.aspx"></asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="hlReasonTypeSetup" ForeColor="Blue" runat="server" Text="Reason Type Set up"
                NavigateUrl="ExpediteReasonSetUpOverview.aspx"></asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="hlComments" ForeColor="Blue" runat="server" Text="Comments" NavigateUrl="ExpediteComments.aspx"></asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="hlSOH" ForeColor="Blue" runat="server" Text="SOH" NavigateUrl="StockonHandOverview.aspx"></asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="hlBookingDate" ForeColor="Blue" runat="server" Text="Booking Date"
                NavigateUrl="ExpediteBookingDate.aspx"></asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="hlPOStatus" ForeColor="Blue" runat="server" Text="PO Status" NavigateUrl="OpenPurchaseListing.aspx"></asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="hlExpediteStock" ForeColor="Blue" runat="server" Text="Expedite Stock"
                NavigateUrl="ExpediteStock.aspx"></asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="hlTrackingReportSelection" ForeColor="Blue" runat="server" Text="Tracking Report Selection"
                NavigateUrl="ExpediteTrackingReport.aspx"></asp:HyperLink></li>
    </ul>
</asp:Content>

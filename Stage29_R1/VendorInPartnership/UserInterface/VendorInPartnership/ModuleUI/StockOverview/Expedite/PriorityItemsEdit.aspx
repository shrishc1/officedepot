﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PriorityItemsEdit.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" 
 Inherits="ModuleUI_StockOverview_Expedite_PriorityItemsEdit" %>

 <%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
 <%@ Register Src="../../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<script type="text/javascript">
    $(document).ready(function () {
        $('#<%= btn_search.ClientID%>').click(function () {
            var tboxSKU = $('#<%=tboxSKU.ClientID%>').val();
            var tboxCatCode = $('#<%=tboxCatCode.ClientID%>').val();
            if (tboxSKU == '' && tboxCatCode == '') {
                alert('<%=PriorityErrorMesg %>');
                return false;
            }
        });
    });
//        $(document).ready(function () {
//              Disabletextbox();
//                return false;
//            });
//            function Disabletextbox() {
//                $('#<%=tboxSKU.ClientID%>').attr('disabled', true);
//                $('#<%=tboxCatCode.ClientID%>').attr('disabled',true);
//                $('#<%=tboxReason.ClientID%>').attr('disabled', true);
//                $('#<%=tboxDescription.ClientID%>').attr('disabled', true);
//            }
</script>

 <h2>
        <cc1:ucLabel ID="lblPriorityItemsExpedite" runat="server"></cc1:ucLabel>
    </h2>
     <div class="right-shadow">
        <div class="formbox">
           <cc1:ucPanel ID="pnlPriorityItemsRecord"  runat="server" CssClass="fieldset-form" Style="display: block">
                     <table width="65%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold; width: 20%;">
                                    <cc1:ucLabel ID="lableCountry" Text="Country" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%;">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 80%;" colspan="2" >
                                    <cc2:ucCountry runat="server" ID="ddlCountry" />
                                </td>
                              </tr>
                              <tr>
                                <td style="font-weight: bold; width: 20%;">
                                    <cc1:ucLabel ID="lblSKU" Text="SKU" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%;">
                                    :
                                </td>
                                <td colspan="2" style="font-weight: bold; width: 80%;" >
                                   <cc1:ucTextbox ID="tboxSKU" Width="150px" runat="server"></cc1:ucTextbox>
                                </td>
                              </tr>
                              <tr>
                                <td style="font-weight: bold; ">
                                    <cc1:ucLabel ID="lableCatCode" Text="Cat Code" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td colspan="2" style="font-weight: bold;">
                                   <cc1:ucTextbox ID="tboxCatCode" Width="150px" runat="server" 
                                        ></cc1:ucTextbox> &nbsp;&nbsp;&nbsp;
                                    <cc1:ucButton ID="btn_search" Text="Search" runat="server" CssClass="button" OnClick="btnDescriptionSearch_Click" />
                                </td>
                                

                              </tr>
                              <tr>
                                <td style="font-weight: bold; ">
                                    <cc1:ucLabel ID="lableDescription" Text="Description" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; ">
                                    :
                                </td>
                                <td style="font-weight: bold; " colspan="2" >
                                   <cc1:ucTextbox ID="tboxDescription"  Width="350px" runat="server" Enabled="false"></cc1:ucTextbox>
                                </td>
                              </tr>
                              <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lableReason" Text="Reason" runat="server" Enabled="false"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td style="font-weight: bold; " colspan="2" >
                                   <cc1:ucTextbox ID="tboxReason" Width="350px" MaxLength="500" runat="server"></cc1:ucTextbox>
                                </td>
                              </tr>

                      </table>
                       <div class="button-row">
                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" 
                                onclick="btnBack_Click" />
                            <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" Visible="false" CssClass="button" OnClick="btnDelete_Click" OnClientClick="return confirm('Are you sure you want to delete!');"  />
                            <cc1:ucButton ID="btnSave" runat="server" Text="Save" Enabled="false" CssClass="button" OnClick="btnSave_Click" />
                        </div>
                 </cc1:ucPanel>


             </div>
       </div>

</asp:Content>

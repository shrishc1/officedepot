﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowCommunication.aspx.cs"
    Inherits="ShowCommunication" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" type="text/css" href="../../Css/style.css" />
    <script language="javascript" type="text/javascript">
        function PrintScreen() {
            document.getElementById('rwPrintRow').style.visibility = "hidden";
            window.print();
            document.getElementById('rwPrintRow').style.visibility = "visible";
        }

        function CloseWindow() {
            window.open('', '_self', ''); window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="height: 600px; overflow: scroll; ">
        <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder-popup">
            <tr id="rwPrintRow">
                <td align="left">
                    <cc1:ucButton ID="btnPrintLetter" runat="server" CssClass="button" Text="Print Letter"
                        OnClientClick="PrintScreen();" />
                    <cc1:ucButton ID="btnClose" runat="server" Text="Close" CssClass="button" OnClientClick="CloseWindow();" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%;">
                    <div id="dvDisplayLetter" style="font-family: Arial; font-size: 12;!important" runat="server">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

﻿using System;
using System.Web.UI;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.StockOverview;


public partial class ShowCommunication : CommonPage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
            if (GetQueryStringValue("CommunicationID") != null)
                this.SetEmailInfo(Convert.ToInt32(GetQueryStringValue("CommunicationID")));
        }
    }

    private void SetEmailInfo(int communicationId) {
        ExpediteStockBAL oExpediteStockBAL = new ExpediteStockBAL();
        ExpediteStockBE oMAS_ExpStockBE = new ExpediteStockBE();

        oMAS_ExpStockBE.Action = "GetCommunicationDetails";
        oMAS_ExpStockBE.CommunicationID = communicationId;
        var emilDetails = oExpediteStockBAL.GetEmailCommunicationBAL(oMAS_ExpStockBE);
        if (emilDetails != null && emilDetails.Count > 0) {
            dvDisplayLetter.InnerHtml = emilDetails[0].Body;
        }
    }
}
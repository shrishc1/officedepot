﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ExpediteTrackingReport.aspx.cs" Inherits="ExpediteTrackingReport" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%--<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>--%>
<%@ Register Src="~/CommonUI/UserControls/ucMultiSelectVendor.ascx" TagName="ucMultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectItemClassification.ascx" TagName="MultiSelectItemClassification"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportExpediteTrackingReport.ascx" TagName="ucExportExpediteTrackingReport"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .radio-fix tr td:nth-child(2)
        {
            width: 245px;
        }
        .radio-fix tr td:nth-child(3)
        {
            width: 170px;
        }
        .wordbreak
        {
            word-break: break-all;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblExpediteTrackingReport" runat="server" Text="Expedite Tracking Report"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucExportExpediteTrackingReport ID="ucExportToExcel1" runat="server" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSearchScreen" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblIncludeVendor" runat="server" Text="Include Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <%--<cc1:MultiSelectVendor runat="server" ID="ucIncludeVendor" />  --%>
                            <cc1:ucMultiSelectVendor ID="ucIncludeVendor" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblExcludeVendor" runat="server" Text="Exclude Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <%--<cc1:MultiSelectVendor runat="server" ID="ucExcludeVendor" />   --%>
                            <cc1:ucMultiSelectVendor ID="ucExcludeVendor" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblItemClassification" runat="server" Text="Item Classification"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel10" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectItemClassification runat="server" ID="msItemClassification" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSKUType" runat="server" Text="SKU Type"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td align="left" class="nobold radiobuttonlist">
                            <cc1:ucRadioButtonList ID="rblSkuType" CssClass="radio-fix" runat="server" RepeatColumns="4"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text="All" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Only Active(Exclude XXDC and DISC)" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Exclude XXDC" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Exclude DISC" Value="4"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDisplay" runat="server" Text="Display"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                        </td>
                        <td align="left" class="nobold radiobuttonlist">
                            <cc1:ucRadioButtonList ID="rblDisplay" CssClass="radio-fix" runat="server" RepeatColumns="4"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text="All" Value="1"></asp:ListItem>
                                <asp:ListItem Text="At Risk(Avoidable Stock Outs)" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Unavoidable Stock Outs" Value="3"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblUpdate" runat="server" Text="Update"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                        </td>
                        <td align="left" class="nobold radiobuttonlist">
                            <cc1:ucRadioButtonList ID="rblUpdate" CssClass="radio-fix" runat="server" RepeatColumns="4"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text="All" Value="1"></asp:ListItem>
                                <asp:ListItem Text="No update in last 3 days" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Pending Date" Value="3"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlPotentialOutput" runat="server">
                <div style="width: 100%; overflow: auto" id="divPrint" class="fixedTable">
                    <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table">
                        <tr>
                            <td style="font-weight: bold;">
                                <div style="width: 950px; overflow-x: auto;">
                                    <cc1:ucGridView ID="gvTrackingReport" runat="server" CssClass="grid gvclass" GridLines="Both"
                                        AllowPaging="true" PageSize="200" OnPageIndexChanging="gvTrackingReport_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                                <HeaderStyle Width="10px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <%--<cc1:ucLinkButton runat="server" ID="lnkStatusValue" CommandName="Status" Text='<%#Eval("Status") %>'></cc1:ucLinkButton>--%>
                                                    <cc1:ucLabel runat="server" ID="lblStatusValue" Text='<%#Eval("Status") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="10px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                                                <HeaderStyle Width="30px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="30px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblVendorNameValue" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="80px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                                                <HeaderStyle Width="30px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnSiteId" Value='<%#Eval("Site.SiteID") %>' runat="server" />
                                                    <cc1:ucLabel runat="server" ID="lblSiteNameValue" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="30px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OfficeDepotCode" SortExpression="PurchaseOrder.OD_Code">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblOD_CodeValue" Text='<%#Eval("PurchaseOrder.OD_Code") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VikingCode" SortExpression="PurchaseOrder.Direct_code">
                                                <HeaderStyle Width="30px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblVikingCodeValue" Text='<%#Eval("PurchaseOrder.Direct_code") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="30px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="POStatus" SortExpression="POStatus">
                                                <HeaderStyle Width="30px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <%--<cc1:ucLinkButton runat="server" ID="lblPOStatusValue" CommandName="POStatus" Text='<%#Eval("POStatus") %>'></cc1:ucLinkButton>--%>
                                                    <asp:HyperLink ID="hlPOStatusValue" Target="_blank" runat="server" Text='<%# Eval("POStatus") %>'
                                                        NavigateUrl='<%# EncryptQuery("OpenPurchaseListing.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&VendorID=" + Eval("Vendor.VendorID")+ "&Status=" + Eval("Status")+ "&PreviousPage=TrackingReport")%>'></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="30px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description1" SortExpression="PurchaseOrder.Product_description">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblProductDesValue" Text='<%#Eval("PurchaseOrder.Product_description") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100px" CssClass="wordbreak" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="PurchaseOrder.StockPlannerNo">
                                                <HeaderStyle Width="30px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblStockPlannerNoValue" Text='<%#Eval("PurchaseOrder.StockPlannerNo") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="30px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockPlannerName" SortExpression="PurchaseOrder.StockPlannerName">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblStockPlannerNameValue" Text='<%#Eval("PurchaseOrder.StockPlannerName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorCode" SortExpression="PurchaseOrder.Vendor_Code">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblVendorCodeValue" Text='<%#Eval("PurchaseOrder.Vendor_Code") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ItemClassification" SortExpression="PurchaseOrder.Item_classification">
                                                <HeaderStyle Width="20px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblItemClassificationValue" Text='<%#Eval("PurchaseOrder.Item_classification") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="20px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DateLastUpdated" SortExpression="CommentDateLastUpdated">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblCommentDateLastUpdatedValue" Text='<%#Eval("CommentDateLastUpdated", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NextUpdateDue" SortExpression="CommentUpdateValidTo">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblCommentUpdateValidToValue" Text='<%#Eval("CommentUpdateValidTo", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="MostCurrentComment" SortExpression="MostCurrentComment">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblMostCurrentCommentValue" Text='<%#Eval("MostCurrentComment") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100px" CssClass="wordbreak" Font-Size="10px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </cc1:ucGridView>
                                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                                    </cc1:PagerV2_8>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <div class="button-row">
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

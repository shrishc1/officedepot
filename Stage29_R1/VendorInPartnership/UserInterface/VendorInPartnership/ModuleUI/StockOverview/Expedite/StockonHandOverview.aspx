﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="StockonHandOverview.aspx.cs" Inherits="StockonHandOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblStockonHandOverview" runat="server" Text="Stock on Hand Overview"></cc1:ucLabel>
    </h2>
     <div class="button-row">
        <cc1:ucButton ID="UcbtnBacktoOutput" runat="server" Text="Back to Output" CssClass="button"
            OnClick="UcbtnBacktoOutput_Click" />
    </div>
    <div class="button-row">
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
   
    <div class="right-shadow">
        <div class="formbox">
            <asp:Panel ID="pnlSearch" runat="server">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
                    <tr>
                        <td style="font-weight: bold; width: 15%;">
                            <cc1:ucLabel ID="lblODSKU" Text="OD SKU" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%;">
                            :
                        </td>
                        <td style="font-weight: bold; width: 34%;">
                            <cc1:ucLabel ID="lblODSKUValue" Width="145px" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 15%;">
                            <cc1:ucLabel ID="lblQtyonHand" Text="Qty on Hand" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%;">
                            :
                        </td>
                        <td style="font-weight: bold; width: 34%;">
                            <cc1:ucLabel ID="lblQtyonHandValue" Width="145px" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; width: 15%;">
                            <cc1:ucLabel ID="lblMoveQty" Text="Move Qty" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%;">
                            :
                        </td>
                        <td style="font-weight: bold; width: 34%;" colspan="4">
                            <cc1:ucTextbox ID="txtMoveQtyValue" Width="145px" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; width: 15%;">
                            <cc1:ucLabel ID="lblMoveFrom" Text="Move From" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%;">
                            :
                        </td>
                        <td style="font-weight: bold; width: 34%;">
                            <uc2:ucSite ID="ddlSiteMoveFrom" runat="server" />
                        </td>
                        <td style="font-weight: bold; width: 15%;">
                            <cc1:ucLabel ID="lblMoveto" Text="Move to" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%;">
                            :
                        </td>
                        <td style="font-weight: bold; width: 34%;">
                            <uc2:ucSite ID="ddlSiteMoveto" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="6">
                            <div class="button-row">
                                <cc1:ucButton ID="btnRun" runat="server" Text="Run" CssClass="button" Width="100px"
                                    OnClick="btnRun_Click" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <cc1:ucGridView ID="gvStockonHandOverview" Width="100%" runat="server" CssClass="grid"
                                OnRowDataBound="gvStockonHandOverview_OnRowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Day" SortExpression="Day">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblDayUploadedValue" runat="server" Text='<%#Eval("DayUploaded") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date" SortExpression="Date">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblDateUploadedValue" runat="server" Text='<%#Eval("DateUploaded", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            <asp:HiddenField ID="hdnActualDateUploadedValue" runat="server" Value='<%#Eval("DateUploaded") %>'>
                                            </asp:HiddenField>
                                            <asp:HiddenField ID="hdnCurrentDateValue" runat="server" Value='<%#Eval("CurrentDate", "{0:dd/MM/yyyy}") %>'>
                                            </asp:HiddenField>
                                            <asp:HiddenField ID="hdnActualCurrentDateValue" runat="server" Value='<%#Eval("CurrentDate") %>'>
                                            </asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OHQty" SortExpression="FromOHQty">
                                        <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblOHQtyMoveValue" runat="server" Text='<%#Eval("OHQtyMove") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ForecastedSoldAmount" SortExpression="FromForecastedSoldAmount">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblForecastSoldAmtMoveValue" runat="server" Text='<%#Eval("ForecastSoldAmtMove") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SoldAmount" SortExpression="FromActualSoldAmount">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblActualSoldAmtMoveValue" runat="server" Text='<%#Eval("ActualSoldAmtMove") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OnHandQty" SortExpression="ToOnHandQty">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblOHQtyMoveToValue" runat="server" Text='<%#Eval("OHQtyMoveTo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ForecastedSoldAmount" SortExpression="ToForecastedSoldAmount">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblForecastSoldAmtMoveToValue" runat="server" Text='<%#Eval("ForecastSoldAmtMoveTo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SoldAmount" SortExpression="ToActualSoldAmount">
                                        <HeaderStyle Width="15%" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblActualSoldAmtMoveToValue" runat="server" Text='<%#Eval("ActualSoldAmtMoveTo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <%-- <asp:Panel ID="pnlSearchResult" runat="server">
            </asp:Panel>--%>
        </div>
    </div>
</asp:Content>

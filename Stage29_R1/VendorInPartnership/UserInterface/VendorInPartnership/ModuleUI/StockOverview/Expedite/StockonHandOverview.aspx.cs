﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using WebUtilities;

public partial class StockonHandOverview : CommonPage
{
    #region Declarations ...

    string PleaseselecttheODSKU = WebCommon.getGlobalResourceValue("PleaseselecttheODSKU");
    string Pleaseenterthemovequantity = WebCommon.getGlobalResourceValue("Pleaseenterthemovequantity");
    string MovefromandMovetocannotbesamesite = WebCommon.getGlobalResourceValue("MovefromandMovetocannotbesamesite");
    string MoveQtycannotbelessthanorequaltozero = WebCommon.getGlobalResourceValue("MoveQtycannotbelessthanorequaltozero");
    string QtyonHandcannotbelessthanorequaltozero = WebCommon.getGlobalResourceValue("QtyonHandcannotbelessthanorequaltozero");

    #endregion

    #region Events ...

    protected void Page_InIt(object sender, EventArgs e)
    {
        ddlSiteMoveFrom.CurrentPage = this;
        ddlSiteMoveFrom.IsAllRequired = false;
        ddlSiteMoveto.CurrentPage = this;
        ddlSiteMoveto.IsAllRequired = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        if (!IsPostBack)
        {
            this.ClearViewSates();
            //ddlSiteMoveFrom.innerControlddlSite.Enabled = false;
            ddlSiteMoveto.innerControlddlSite.Enabled = false;

            if (GetQueryStringValue("ODSku") != null)
                lblODSKUValue.Text = Convert.ToString(GetQueryStringValue("ODSku"));

            //this.SetOnHandQty();
            pnlSearch.Visible = true;
            //pnlSearchResult.Visible = true;
        }

        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = gvStockonHandOverview;
        ucExportToExcel1.FileName = "StockonHandOverview";
    }

    protected void Page_PreRender(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
            ddlSiteMoveFrom.innerControlddlSite.AutoPostBack = true;
            //SiteSelectedIndexChanged();
            if (GetQueryStringValue("SKUID") != null && GetQueryStringValue("SiteID") != null) {
                SetOnHandQty(Convert.ToInt32(GetQueryStringValue("SKUID")), Convert.ToInt32(ddlSiteMoveFrom.innerControlddlSite.SelectedItem.Value));
            }
        }
    }

    public override void SiteSelectedIndexChanged() {
        base.SiteSelectedIndexChanged();
        if (ViewState["SKUID"] != null) {
            SetOnHandQty(Convert.ToInt32(ViewState["SKUID"]), Convert.ToInt32(ddlSiteMoveFrom.innerControlddlSite.SelectedItem.Value));
        }
        //if (GetQueryStringValue("SKUID") != null) {
        //    SetOnHandQty(Convert.ToInt32(GetQueryStringValue("SKUID")), Convert.ToInt32(ddlSiteMoveFrom.innerControlddlSite.SelectedItem.Value));
        //}
    }

    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (GetQueryStringValue("SiteID") != null)
        {
            var siteId = Convert.ToString(GetQueryStringValue("SiteID"));
            //ddlSiteMoveFrom.innerControlddlSite.SelectedIndex = ddlSiteMoveFrom.innerControlddlSite.Items.IndexOf(ddlSiteMoveFrom.innerControlddlSite.Items.FindByValue(siteId));
            ddlSiteMoveto.innerControlddlSite.SelectedIndex = ddlSiteMoveto.innerControlddlSite.Items.IndexOf(ddlSiteMoveto.innerControlddlSite.Items.FindByValue(siteId));
        }
    }

    protected void btnRun_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(lblQtyonHandValue.Text) || string.IsNullOrWhiteSpace(lblQtyonHandValue.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + QtyonHandcannotbelessthanorequaltozero + "')", true);
            return;
        }

        if (Convert.ToDecimal(lblQtyonHandValue.Text) <= 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + QtyonHandcannotbelessthanorequaltozero + "')", true);
            return;
        }

        if (string.IsNullOrEmpty(lblODSKUValue.Text) || string.IsNullOrWhiteSpace(lblODSKUValue.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselecttheODSKU + "')", true);
            return;
        }

        if (string.IsNullOrEmpty(txtMoveQtyValue.Text) || string.IsNullOrWhiteSpace(txtMoveQtyValue.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Pleaseenterthemovequantity + "')", true);
            return;
        }

        if (!validationFunctions.IsNumeric(txtMoveQtyValue.Text.Trim()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + MoveQtycannotbelessthanorequaltozero + "')", true);
            return;
        }

        if (Convert.ToDecimal(txtMoveQtyValue.Text) <= 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + MoveQtycannotbelessthanorequaltozero + "')", true);
            return;
        }

        if (ddlSiteMoveFrom.innerControlddlSite.SelectedValue.Equals(ddlSiteMoveto.innerControlddlSite.SelectedValue))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + MovefromandMovetocannotbesamesite + "')", true);
            return;
        }

        this.ClearViewSates();
        var expediteStock = new ExpediteStockBE();
        expediteStock.Action = "GetAllSOHData";

        //if (GetQueryStringValue("SKUID") != null)
        //    expediteStock.SkuID = Convert.ToInt32(GetQueryStringValue("SKUID"));

        if (ViewState["SKUID"] != null)
            expediteStock.SkuID = Convert.ToInt32(ViewState["SKUID"]);

        if (!string.IsNullOrEmpty(txtMoveQtyValue.Text) && !string.IsNullOrWhiteSpace(txtMoveQtyValue.Text))
            expediteStock.ActualSoldAmt = Convert.ToDecimal(txtMoveQtyValue.Text);
        expediteStock.MoveSiteId = Convert.ToInt32(ddlSiteMoveFrom.innerControlddlSite.SelectedValue);
        expediteStock.MoveToSiteId = Convert.ToInt32(ddlSiteMoveto.innerControlddlSite.SelectedValue);
        var expediteStockBAL = new ExpediteStockBAL();
        var lstStockOnHandOverview = expediteStockBAL.GetAllSOHBAL(expediteStock);
        ViewState["lstStockOnHandOverview"] = lstStockOnHandOverview;
        if (lstStockOnHandOverview != null && lstStockOnHandOverview.Count > 0)
            gvStockonHandOverview.DataSource = lstStockOnHandOverview;
        else
            gvStockonHandOverview.DataSource = null;

        gvStockonHandOverview.DataBind();

        if (gvStockonHandOverview.Rows.Count > 0)
            ucExportToExcel1.Visible = true;
        else
            ucExportToExcel1.Visible = false;

    }

    protected void gvStockonHandOverview_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {
            var DateUploadedValue = e.Row.FindControl("lblDateUploadedValue") as Label;
            var ActualDateUploadedValue = e.Row.FindControl("hdnActualDateUploadedValue") as HiddenField;
            var CurrentDateValue = e.Row.FindControl("hdnCurrentDateValue") as HiddenField;
            var ActualCurrentDateValue = e.Row.FindControl("hdnActualCurrentDateValue") as HiddenField;
            if (DateUploadedValue != null && CurrentDateValue != null && ActualDateUploadedValue != null)
            {
                if (!string.IsNullOrEmpty(ActualDateUploadedValue.Value))
                {
                    if (DateUploadedValue.Text.Equals(CurrentDateValue.Value))
                    {
                        e.Row.BackColor = System.Drawing.Color.LightGreen;

                        // Here setting the On Hand Qty for current day.
                        var OHQtyMoveValue = e.Row.FindControl("lblOHQtyMoveValue") as Label;
                        var ActualSoldAmtMoveValue = e.Row.FindControl("lblActualSoldAmtMoveValue") as Label;
                        var OHQtyMoveToValue = e.Row.FindControl("lblOHQtyMoveToValue") as Label;
                        if (OHQtyMoveValue != null && ActualSoldAmtMoveValue != null)
                        {
                            var lstStockOnHandOverview = (List<ExpediteStockBE>)ViewState["lstStockOnHandOverview"];
                            var expediteStock = lstStockOnHandOverview.Find(es => es.DateUploaded.Equals(Convert.ToDateTime(ActualDateUploadedValue.Value)));

                            if (string.IsNullOrEmpty(OHQtyMoveValue.Text))
                                OHQtyMoveValue.Text = "0";

                            if (string.IsNullOrEmpty(ActualSoldAmtMoveValue.Text))
                                ActualSoldAmtMoveValue.Text = "0";

                            //OHQtyMoveToValue.Text = Convert.ToString(Convert.ToDecimal(OHQtyMoveValue.Text));
                            ViewState["OHQtyMoveToValue"] = Convert.ToDecimal(OHQtyMoveToValue.Text) + expediteStock.ActualSoldAmtMove;
                            //OHQtyMoveToValue.Text = Convert.ToString(expediteStock.ActualSoldAmtMove + Convert.ToDecimal(OHQtyMoveValue.Text));
                        }

                        // Here getting the current day DateUploaded date.
                        ViewState["PrevRowDateUploaded"] = Convert.ToDateTime(ActualDateUploadedValue.Value);
                    }
                    else if (Convert.ToDateTime(ActualDateUploadedValue.Value) > Convert.ToDateTime(ActualCurrentDateValue.Value))
                    {
                        var lstStockOnHandOverview = (List<ExpediteStockBE>)ViewState["lstStockOnHandOverview"];
                        var expediteStock = lstStockOnHandOverview.Find(es => es.DateUploaded.Equals(Convert.ToDateTime(ViewState["PrevRowDateUploaded"])));

                        // Here setting the From OH Qty Logic : OHQty = Prev Row FromOHQty + Prev Row FromForecastedSoldAmount + Prev Row FromActualSoldAmount
                        var OHQtyMoveValue = e.Row.FindControl("lblOHQtyMoveValue") as Label;
                        var ActualSoldAmtMoveValue = e.Row.FindControl("lblActualSoldAmtMoveValue") as Label;
                        if (OHQtyMoveValue != null && ViewState["OHQtyMoveValue"] == null)
                        {
                            ActualSoldAmtMoveValue.Text = "0.000";
                            OHQtyMoveValue.Text = Convert.ToString(expediteStock.OHQtyMove - (expediteStock.ForecastSoldAmtMove + expediteStock.ActualSoldAmtMove));
                            ViewState["OHQtyMoveValue"] = OHQtyMoveValue.Text;
                        }
                        else if (ViewState["OHQtyMoveValue"] != null)
                        {
                            ActualSoldAmtMoveValue.Text = "0.000";
                            OHQtyMoveValue.Text = Convert.ToString(Convert.ToDecimal(ViewState["OHQtyMoveValue"]) - expediteStock.ForecastSoldAmtMove);
                            ViewState["OHQtyMoveValue"] = OHQtyMoveValue.Text;
                        }

                        // Here setting the To On Hand Qty Logic : OnHandQty = Prev Row ToOnHandQty + Prev Row ToForecastedSoldAmount
                        var OHQtyMoveToValue = e.Row.FindControl("lblOHQtyMoveToValue") as Label;
                        if (OHQtyMoveToValue != null && ViewState["OHQtyMoveToValue"] == null)
                        {
                            OHQtyMoveToValue.Text = Convert.ToString(expediteStock.OHQtyMoveTo - expediteStock.ForecastSoldAmtMoveTo);
                            ViewState["OHQtyMoveToValue"] = OHQtyMoveToValue.Text;
                        }
                        else if (ViewState["OHQtyMoveToValue"] != null)
                        {
                            OHQtyMoveToValue.Text = Convert.ToString(Convert.ToDecimal(ViewState["OHQtyMoveToValue"]) - expediteStock.ForecastSoldAmtMoveTo);
                            ViewState["OHQtyMoveToValue"] = OHQtyMoveToValue.Text;
                        }

                        // Here getting the current row DateUploaded date for next row OH qty.
                        ViewState["PrevRowDateUploaded"] = Convert.ToDateTime(ActualDateUploadedValue.Value);
                    }
                }
            }
        }
    }

    protected void UcbtnBacktoOutput_Click(object sender, EventArgs e)
    {
        Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");
        //EncryptQueryString("ExpediteStockSearch.aspx?PreviousPage=Comments");
    }

    #endregion

    #region Methods ...

    private void ClearViewSates()
    {
        ViewState["lstStockOnHandOverview"] = null;
        ViewState["PrevRowDateUploaded"] = null;
        ViewState["OHQtyMoveValue"] = null;
        ViewState["OHQtyMoveToValue"] = null;
    }

    private void SetOnHandQty(int skuid , int siteid) {
       
            var expediteStock = new ExpediteStockBE();
            expediteStock.Action = "GetSKUOnHandQty";
            expediteStock.SkuID = skuid;
            expediteStock.SiteID = siteid;
            if (GetQueryStringValue("ODSku") != null)
                expediteStock.OD_Code = Convert.ToString(GetQueryStringValue("ODSku")).Trim();

            var expediteStockBAL = new ExpediteStockBAL();
            List<ExpediteStockBE> lstExpediteStock = expediteStockBAL.GetSKUOnHandQtyBAL(expediteStock);
            if (lstExpediteStock  != null && lstExpediteStock.Count > 0)
            {
                lblQtyonHandValue.Text = Convert.ToString(lstExpediteStock[0].QtyOnHand);
                ViewState["SKUID"] = lstExpediteStock[0].SkuID;
            }
    }


    private void SetOnHandQty()
    {
        if (GetQueryStringValue("SKUID") != null && GetQueryStringValue("SiteID") != null)
        {
            var expediteStock = new ExpediteStockBE();
            expediteStock.Action = "GetSKUOnHandQty";
            expediteStock.SkuID = Convert.ToInt32(GetQueryStringValue("SKUID"));
            expediteStock.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
            var expediteStockBAL = new ExpediteStockBAL();
            lblQtyonHandValue.Text = Convert.ToString(expediteStockBAL.GetSKUOnHandQtyBAL(expediteStock));
        }
    }

    #endregion
}
﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using Utilities;
using WebUtilities;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using BusinessEntities.ModuleBE.AdminFunctions;
using System.Configuration;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.Upload;
using Utilities;
using System.Data;
using WebUtilities;
using System.Globalization;
using System.Text;
using Utilities;

public partial class Communication : CommonPage
{
    string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
    sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            UIUtility.PageValidateForODUser();
            if (!string.IsNullOrEmpty(GetQueryStringValue("CommunicationID"))) {
                ExpediteStockBE oNewExpediteStock = getCommunication();
                var path = oSendCommunicationCommon.getAbsolutePath();
                divReSendCommunication.InnerHtml = oNewExpediteStock.Body.Replace("{logoInnerPath}", path);
                txtEmail.Text = oNewExpediteStock.SentTo;
            }
        }
    }

    private ExpediteStockBE getCommunication() {
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        ExpediteStockBE oNewExpediteStock = new ExpediteStockBE();
        oNewExpediteStock.Action = "ViewCommunication";
        oNewExpediteStock.CommunicationID = Convert.ToInt32(GetQueryStringValue("CommunicationID"));
        oNewExpediteStock.VendorID = Convert.ToInt32(GetQueryStringValue("VendorId"));
        List<ExpediteStockBE> lstExpediteComments = oMAS_ExpStockBAL.GetExpediteCommentsHistoryBAL(oNewExpediteStock);
        if (lstExpediteComments != null && lstExpediteComments.Count > 0) {
            oNewExpediteStock = lstExpediteComments[0];
        }
        return oNewExpediteStock;
    }

    protected void btnReSendCommunication_Click(object sender, EventArgs e) {
        ExpediteStockBE oNewExpediteStock = getCommunication();
        var oclsEmail = new clsEmail();
        oNewExpediteStock.SentTo = txtEmail.Text.Trim();
        oclsEmail.sendMail(oNewExpediteStock.SentTo, oNewExpediteStock.Body, oNewExpediteStock.Subject, sFromAddress, true);
       
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "refreshParent", "window.opener.location.href = window.opener.location.href;", true);
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "close", "this.close();", true);
    }
}
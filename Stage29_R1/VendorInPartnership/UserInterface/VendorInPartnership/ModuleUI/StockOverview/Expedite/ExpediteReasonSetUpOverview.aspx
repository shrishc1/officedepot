﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" AutoEventWireup="true" CodeFile="ExpediteReasonSetUpOverview.aspx.cs" Inherits="ExpediteReasonSetUpOverview" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblExpediteReasonSetUp" runat="server" Text="Expedite Reason Set Up"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="ExpediteReasonSetUpEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="gvExpediteReasonSetUp" Width="100%" runat="server" CssClass="grid"  onsorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Country" SortExpression="Country.CountryName">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblCountryValue" runat="server" Text='<%#Eval("Country.CountryName") %>'></asp:Label>
                               <%-- <asp:Label ID="lblCountryID" runat="server" Visible="false" Text='<%#Eval("CountryID") %>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reason " SortExpression="Reason">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpReasonValue" runat="server" Text='<%# Eval("Reason") %>'  NavigateUrl='<%# EncryptQuery("ExpediteReasonSetUpEdit.aspx?ReasonID="+ Eval("ReasonTypeID")) %>'></asp:HyperLink>
                            </ItemTemplate>                                                                                  
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField> 
                       <asp:TemplateField HeaderText="Update Comment Required" SortExpression="UpdateCommentRequired">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblUpdateCommentRequiredValue" runat="server" Text='<%#Eval("UpdateCommentRequired") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="IsActive" SortExpression="IsActive">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Literal ID="ltisActive" runat="server" Text='<%#Eval("IsActiveType") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
               <%-- <asp:HyperLink ID="lnkERSUP" ForeColor="Blue" runat="server" Text="Expedite Stock"
                    NavigateUrl="ExpediteStockSearch.aspx"></asp:HyperLink>--%>
            </td>
        </tr>      
    </table>   
</asp:Content>


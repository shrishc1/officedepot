﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using BaseControlLibrary;
using Utilities;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.StockOverview;
using DataAccessLayer.ModuleDAL.StockOverview;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;

public partial class ExpediteReasonSetUpEdit : CommonPage
{
    int ReasonTypeID = 0;
    protected void Page_Init(object sender, EventArgs e)
    {
        ucCountry.CurrentPage = this;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        if (!IsPostBack)
        {
        }
            
    }
    private void GetReasonType()
    {
       
         ExpediteStockBE oMAS_ExpStockBE = new ExpediteStockBE();
         ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();


        if (GetQueryStringValue("ReasonID")!= null) {
          
            oMAS_ExpStockBE.Action = "ShowByID";
            oMAS_ExpStockBE.ReasonTypeID = Convert.ToInt32(GetQueryStringValue("ReasonID"));

            ExpediteStockBE lstCarrier = oMAS_ExpStockBAL.GetExpediteDetailsByIdBAL(oMAS_ExpStockBE);

            if (lstCarrier != null) {
                ucCountry.innerControlddlCountry.SelectedIndex = ucCountry.innerControlddlCountry.Items.IndexOf(ucCountry.innerControlddlCountry.Items.FindByValue(lstCarrier.CountryID.ToString()));

                txtReason.Text = lstCarrier.Reason;
                if (lstCarrier.UpdateCommentRequired.Equals("Yes"))
                    rblUpdate.SelectedIndex = 0;
                else
                    rblUpdate.SelectedIndex = 1;
                if (lstCarrier.IsActive == 1)
                    chkIsactive.Checked = true;
                else
                    chkIsactive.Checked = false;
            }

           ucCountry.innerControlddlCountry.Enabled = false;
           txtReason.Enabled = false;
        }
      

        }
    
    public override void CountryPost_Load()
    {
        if (!IsPostBack)
        {
            GetReasonType();          
        }
    }

    protected void btnSave_Click(object sender, EventArgs e) 
    {
        ExpediteStockBE oMAS_ExpStockBE = new ExpediteStockBE();
        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        int ?resultType =null;
        oMAS_ExpStockBE.Reason = txtReason.Text.Trim();

        oMAS_ExpStockBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedValue);
        if (rblUpdate.SelectedItem.Text == "Yes")
        {
            oMAS_ExpStockBE.IsCommentRequired = true;
        }
        else 
        {
            oMAS_ExpStockBE.IsCommentRequired = false;
        }
        if (chkIsactive.Checked)
        {
            oMAS_ExpStockBE.IsActive = 1;
        }
        else
        {
            oMAS_ExpStockBE.IsActive = 0;
        }
         //Update Reason type.
        if (GetQueryStringValue("ReasonID") != null && (!string.IsNullOrEmpty(GetQueryStringValue("ReasonID"))))
        {
            oMAS_ExpStockBE.Action = "Edit";
            oMAS_ExpStockBE.ReasonTypeID = Convert.ToInt32(GetQueryStringValue("ReasonID"));
            resultType = oMAS_ExpStockBAL.addEditReasonTypeSetupBAL(oMAS_ExpStockBE);
            if (resultType > 0 && resultType.ToString()!= null)
              EncryptQueryString("ExpediteReasonSetUpOverview.aspx");
        }
        else
        {
           
            //Check the duplicacy of reason.
            oMAS_ExpStockBE.Action = "CheckExistance";
            DataTable dtReasonType = oMAS_ExpStockBAL.GetAllReasonTypeDetailsBAL(oMAS_ExpStockBE);
            if (dtReasonType.Rows.Count > 0)
            {
                string errorMessage = WebCommon.getGlobalResourceValue("ReasonExist");
                 Page.ClientScript.RegisterStartupScript(this.GetType(), "Message", "alert('" + errorMessage + "')", true);
            }

            //Add new Reason type.
            oMAS_ExpStockBE.Action = "Add";
            resultType = oMAS_ExpStockBAL.addEditReasonTypeSetupBAL(oMAS_ExpStockBE);
            if (resultType > 0 && resultType.ToString()!= null)
            EncryptQueryString("ExpediteReasonSetUpOverview.aspx");      
        }

         
    }
   
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("ExpediteReasonSetUpOverview.aspx");
    }
}
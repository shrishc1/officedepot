﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ExpediteStockSearch.aspx.cs" Inherits="ExpediteStockSearch"
    EnableEventValidation="false" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucMatGroup.ascx" TagName="ucMatGroup" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucMultiSelectVendor.ascx" TagName="ucMultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectItemClassification.ascx" TagName="MultiSelectItemClassification"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportExpeditePotenOutput.ascx" TagName="ucExportExpeditePotenOutput"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucMRType.ascx" TagName="ucMRType" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucPurchGroup.ascx" TagName="ucPurchGroup"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVikingSku.ascx" TagName="ucVikingSku"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSeacrhVendorWithoutSM.ascx" TagName="ucSeacrhVendor"
    TagPrefix="cc3" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlannerGrouping.ascx" TagName="MultiSelectStockPlannerGrouping"
    TagPrefix="cc1" %>
    <%@ Register Src="~/CommonUI/UserControls/MultiSelectSKUGrouping.ascx" TagName="MultiSelectSKUGrouping"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucDate.ascx" TagName="ucDate" TagPrefix="cc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../../Scripts/jquery.tooltip.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="../../../Scripts/jquery.min.1.8.2.js"></script>     
    <script type="text/javascript" src="../../../Scripts/jquery-ui.min.1.9.1.js"></script> 
    <script src="../../../Scripts/gridviewScroll.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var jq = $.noConflict(true);
        jq(function () {
            gridviewScroll(950, 500);
        })
        function gridviewScroll(widthLength, heightLength) {           
            var val = $('[id*=hdnrblSearchType]').val();
            if (val == 0) {
                jq('#<%=gvPotentialOutput.ClientID%>').gridviewScroll({
                    width: widthLength,
                    height: heightLength
                });
            }
            else {
                jq('#<%=gvPotentialOutputWeekWise.ClientID%>').gridviewScroll({
                    width: widthLength,
                    height: heightLength
                });
            }
        }
    </script>
    <style type="text/css">
        .wmd-view-topscroll, .wmd-view
        {
            overflow-x: auto;
            overflow-y: hidden;
            width: 960px;
        }
        
        .wmd-view-topscroll
        {
            height: 16px;
        }
        
        .dynamic-div
        {
            display: inline-block;
        }
        
        /*  .radio-fix tr td:nth-child(1)
        {
            width: 120px;
            padding:3px 0;
        }
        .radio-fix tr td:nth-child(2)
        {
            width: 213px;
        }
        .radio-fix tr td:nth-child(3)
        {
            width: 235px;
        }
        .radio-fix tr td:nth-child(1) label{margin-right:0px;width:100px;}
        */
        .radio-fix label
        {
            margin-right: 10px !important;
            display: inline-block;
        }
        
        .radiobuttonlist label
        {
            margin-right: 0px;
        }
        .radio-fix-last label
        {
            margin-right: 10px !important;
        }
        
          .GridviewScrollHeader TH, .GridviewScrollHeader TD
        {
          
            font-weight: bold;           
            border-right: 1px solid #AAAAAA;
            border-bottom: 1px solid #AAAAAA;
            background-color: #EFEFEF;
            text-align: left;
            vertical-align: bottom;
        }
        .GridviewScrollItem TD
        {            
           font-weight: bold;           
            border-right: 1px solid #AAAAAA;
            border-bottom: 1px solid #AAAAAA;
            background-color: #EFEFEF;
            text-align: left;
            vertical-align: bottom;
        }
        .GridviewScrollPager
        {
           font-weight: bold;           
            border-right: 1px solid #AAAAAA;
            border-bottom: 1px solid #AAAAAA;
            background-color: #EFEFEF;
            text-align: left;
            vertical-align: bottom;
        }
        .GridviewScrollPager TD
        {
            
            font-size: 14px;
            
        }
        .GridviewScrollPager A
        {
            color: #666666;
        }
       
    </style>
    <script type="text/javascript">
        function setValue1(target) {
            document.getElementById('<%=hdnEndDate.ClientID %>').value = target.value;
        }
        function WeekendCheck() {
            var datefrom = document.getElementById('<%=txtFromDate.ClientID %>').value;
            if (datefrom.length > 0) {
                var InputDate = datefrom;
                var x = InputDate.split("/");
                var date1 = new Date(x[2], (x[1] - 1), x[0]);
                if (!(date1.getDay() % 6)) {
                    alert('<%=WeekendCheck%>');
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return true;
            }
        }

        function getdate() {
            $('#txtToDate').show();
            $('#txtToDate,#txtFromDate').datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: false, changeYear: false,
                minDate: $('#txtFromDate').val(),
                dateFormat: 'dd/mm/yy', maxDate: $('#txtToDate').val(),
                showButtonPanel: true,

                beforeShow: function (input) {
                    setTimeout(function () {

                        var buttonPane = $(input)
                .datepicker("widget")
                .find(".ui-datepicker-buttonpane");

                        var btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">Clear</button>');
                        btn.unbind("click")
                .bind("click", function () {
                    $.datepicker._clearDate(input);
                    document.getElementById('<%=hdnEndDate.ClientID %>').value = $('#txtFromDate').val();
                });

                        btn.appendTo(buttonPane);

                    }, 1);
                }
            });
            $('#txtToDate').hide();
        }
        $(document).ready(function () {
            getdate();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {           
            PageLoadsettings();
            $(".wmd-view-topscroll").scroll(function () {
                $(".wmd-view")
            .scrollLeft($(".wmd-view-topscroll").scrollLeft());
            });

            $(".wmd-view").scroll(function () {
                $(".wmd-view-topscroll")
            .scrollLeft($(".wmd-view").scrollLeft());
            });
            topscroll();
            $('#<%= txtDayStock.ClientID%>').blur(function () {
                if ($('#<%= txtDayStock.ClientID%>').val() < 0 || $('#<%= txtDayStock.ClientID%>').val() > 20) {
                    $('#<%= txtDayStock.ClientID%>').val('');
                    alert('<%= DaysStockErrorMsg%>');
                }
            });
        });

        //        if (typeof hdnSKUIDs === 'undefined') {

        var hdnSKUIDs = [];
        var hdnVendorIds = [];
        var hdnDirectCodes = [];
        var hdnOdCodes = [];
        var hdnStatuss = [];
        var hdnDiscs = [];
        var hdnSiteIds = [];
        var hdnCountryIDs = [];
        // }
        var checkcount = 0;
        function checkCommentCount() {
            alert('<%= CommentCount%>');
            return false;
        }
        $(document).ready(function () {
            // document.getElementById('<%=btnExpediteSelection.ClientID %>').disabled = true;

            $('#<%=btnErrContinue.ClientID%>').click(function () {
                hdnSKUIDs.length = 0;
                hdnVendorIds.length = 0;
                hdnDirectCodes.length = 0;
                hdnOdCodes.length = 0;
                hdnStatuss.length = 0;
                hdnDiscs.length = 0;
                hdnSiteIds.length = 0;
                hdnCountryIDs.length = 0;
            });
            //            $('#<%=btnErrBack.ClientID%>').click(function () {
            //                alert("manish");
            //                hdnSKUIDs.length = 0;
            //                hdnVendorIds.length = 0;
            //                hdnDirectCodes.length = 0;
            //                hdnOdCodes.length = 0;
            //                hdnStatuss.length = 0;
            //                hdnDiscs.length = 0;
            //                hdnSiteIds.length = 0;
            //                hdnCountryIDs.length = 0;

            //                $('#<%=gvPotentialOutput.ClientID %>').find('input:checkbox[id$="chkSelect"]').each(function () {
            //                    debugger;
            //                    if ($(this).is(':checked')) {
            //                        this.checked = false;
            //                    }
            //                });
            //            });


            //                $('#<%=btnExpediteSelection.ClientID%>').click(function () {
            //                    debugger;
            //                    if (checkcount == "0") {
            //                       
            //                        return false;
            //                    }
            //                });

            $('#<%=gvPotentialOutput.ClientID%>').find('input:checkbox[id$="chkSelect"]').bind('change', function (e) {

                //  $('#<%=btnExpediteSelection.ClientID%>').addClass("button");                  
                var iRowIndex = $(this).closest("tr").prevAll("tr").length;
                iRowIndex = parseInt(iRowIndex) + 1;
                if (parseInt(iRowIndex) < 10) {
                    iRowIndex = "0" + iRowIndex;
                }
                var hdnSKUID = $("#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_hdnSKUID").val();
                var hdnVendorId = $("#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_hdnVendorId").val();
                var hdnDirectCode = $("#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_hdnDirectCode").val();
                var hdnOdCode = $("#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_hdnOdCode").val();
                var hdnStatus = $("#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_hdnStatus").val();
                var hdnDisc = $("#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_hdnDisc").val();
                var hdnSiteId = $("#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_hdnSiteId").val();
                var hdnCountryID = $("#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_hdnCountryID").val();
                if ($(this).is(':checked')) {
                    checkcount = parseInt(checkcount) + 1;
                    //                        if (checkcount == "1") {
                    //                            document.getElementById('<%=btnExpediteSelection.ClientID %>').disabled = false;
                    //                            $('#<%=btnExpediteSelection.ClientID%>').removeClass("button-readonly");
                    //                            $('#<%=btnExpediteSelection.ClientID%>').addClass("button");
                    //                        }

                    hdnSKUIDs.push(hdnSKUID);
                    hdnVendorIds.push(hdnVendorId.trim());
                    hdnDirectCodes.push(hdnDirectCode.trim());
                    hdnOdCodes.push(hdnOdCode.trim());
                    hdnStatuss.push(hdnStatus.trim());
                    hdnDiscs.push(hdnDisc.trim());
                    hdnSiteIds.push(hdnSiteId.trim());
                    hdnCountryIDs.push(hdnCountryID.trim());
                    $("#<%=hdnSKUID.ClientID %>").val(hdnSKUIDs.join(','));
                    $("#<%=hdnVendorId.ClientID %>").val(hdnVendorIds.join(','));
                    $("#<%=hdnDirectCode.ClientID %>").val(hdnDirectCodes.join(','));
                    $("#<%=hdnOdCode.ClientID %>").val(hdnOdCodes.join(','));
                    $("#<%=hdnStatus.ClientID %>").val(hdnStatuss.join(','));
                    $("#<%=hdnDisc.ClientID %>").val(hdnDiscs.join(','));
                    $("#<%=hdnSiteId.ClientID %>").val(hdnSiteIds.join(','));
                    $("#<%=hdnCountryID.ClientID %>").val(hdnCountryIDs.join(','));
                }
                else {

                    checkcount = parseInt(checkcount) - 1;

                    hdnSKUIDs.splice(hdnSKUIDs.indexOf(hdnSKUID.trim()), 1);
                    hdnVendorIds.splice(hdnVendorIds.indexOf(hdnVendorId.trim()), 1);
                    hdnDirectCodes.splice(hdnDirectCodes.indexOf(hdnDirectCode.trim()), 1);
                    hdnOdCodes.splice(hdnOdCodes.indexOf(hdnOdCode.trim()), 1);
                    hdnStatuss.splice(hdnStatuss.indexOf(hdnStatus.trim()), 1);
                    hdnDiscs.splice(hdnDiscs.indexOf(hdnDisc.trim()), 1);
                    hdnSiteIds.splice(hdnSiteIds.indexOf(hdnSiteId.trim()), 1);
                    hdnCountryIDs.splice(hdnCountryIDs.indexOf(hdnCountryID.trim()), 1);

                    $("#<%= hdnSKUID.ClientID %>").val("");
                    $("#<%= hdnVendorId.ClientID %>").val("");
                    $("#<%= hdnDirectCode.ClientID %>").val("");
                    $("#<%= hdnOdCode.ClientID %>").val("");
                    $("#<%= hdnStatus.ClientID %>").val("");
                    $("#<%= hdnDisc.ClientID %>").val("");
                    $("#<%= hdnSiteId.ClientID %>").val("");
                    $("#<%= hdnCountryID.ClientID %>").val("");

                    $("#<%= hdnSKUID.ClientID %>").val(hdnSKUIDs.join(','));
                    $("#<%=hdnVendorId.ClientID %>").val(hdnVendorIds.join(','));
                    $("#<%=hdnDirectCode.ClientID %>").val(hdnDirectCodes.join(','));
                    $("#<%=hdnOdCode.ClientID %>").val(hdnOdCodes.join(','));
                    $("#<%=hdnStatus.ClientID %>").val(hdnStatuss.join(','));
                    $("#<%=hdnDisc.ClientID %>").val(hdnDiscs.join(','));
                    $("#<%=hdnSiteId.ClientID %>").val(hdnSiteIds.join(','));
                    $("#<%=hdnCountryID.ClientID %>").val(hdnCountryIDs.join(','));

                    //                        if (checkcount == "0") {
                    //                            document.getElementById('<%=btnExpediteSelection.ClientID %>').disabled = true;
                    //                            $('#<%=btnExpediteSelection.ClientID%>').removeClass("button");
                    //                            $('#<%=btnExpediteSelection.ClientID%>').addClass("button-readonly");
                    //                        }
                }
            });
        });
        function eliminateDuplicates(arr) {
            var i,
      len = arr.length,
      out = [],
      obj = {};
            for (i = 0; i < len; i++) {
                obj[arr[i]] = 0;
            }
            for (i in obj) {
                out.push(i);
            }
            return out;
        }

        function topscroll() {
            //            $(".dynamic-div div").css("float", "left");
            //            var scrollwidth = $(".dynamic-div > div").width();
            //            $('.scroll-div').css('width', scrollwidth + "px");
        }

        function PageLoadsettings() {          
            var pageSize = 202;
                var val = $('[id*=hdnrblSearchType]').val();
                if (val == 0) {
                $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+11):nth-child(-n+23), #ctl00_ContentPlaceHolder1_gvPotentialOutput tr th:nth-child(n+11):nth-child(-n+23)").css('display', 'none');
                $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+24):nth-child(-n+27), #ctl00_ContentPlaceHolder1_gvPotentialOutput tr th:nth-child(n+24):nth-child(-n+27)").css('display', 'none');
                $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+9):nth-child(-n+9), #ctl00_ContentPlaceHolder1_gvPotentialOutput tr th:nth-child(n+9):nth-child(-n+9)").css('display', 'none');
                var totaltr = $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr").length;
                if (totaltr >= pageSize) {
                    $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr:last > td").css('display', 'inline-block');
                }
            }
            else {
                $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+11):nth-child(-n+23), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr th:nth-child(n+11):nth-child(-n+23)").css('display', 'none');
                $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+24):nth-child(-n+27), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr th:nth-child(n+24):nth-child(-n+27)").css('display', 'none');
                $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+9):nth-child(-n+9), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr th:nth-child(n+9):nth-child(-n+9)").css('display', 'none');
                var totaltr = $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr").length;
                if (totaltr >= pageSize) {
                    $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr:last > td").css('display', 'inline-block');
                }
            }
            HideShowDiv();
        }

        function HideShowDiv() {           
            $("#showhide").click(function () {
                $(this).toggleClass('minus-icon');
                var val = $('[id*=hdnrblSearchType]').val();
                if (val == 0) {                   
                    if ($("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr > td:nth-child(n+11):nth-child(-n+23), #ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+11):nth-child(-n+23)").css('display') == 'none') {

                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputHeaderCopy tr th:nth-child(n+10):nth-child(-n+23), #ctl00_ContentPlaceHolder1_gvPotentialOutputHeaderCopy tr th:nth-child(n+10):nth-child(-n+23)").css("display", "table-cell");
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputHeaderCopy tr th:nth-child(n+24):nth-child(-n+26), #ctl00_ContentPlaceHolder1_gvPotentialOutputHeaderCopy tr th:nth-child(n+24):nth-child(-n+26)").css("display", "table-cell");
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputHeaderCopy tr th:nth-child(n+9):nth-child(-n+9), #ctl00_ContentPlaceHolder1_gvPotentialOutputHeaderCopy tr th:nth-child(n+9):nth-child(-n+9)").css("display", "table-cell");

                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+10):nth-child(-n+23), #ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+10):nth-child(-n+23)").css("display", "table-cell");
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+24):nth-child(-n+26), #ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+24):nth-child(-n+26)").css("display", "table-cell");
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+9):nth-child(-n+9), #ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+9):nth-child(-n+9)").css("display", "table-cell");

                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+10):nth-child(-n+12), #ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+10):nth-child(-n+12)").css("min-width", "30px");

                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+16):nth-child(-n+17), #ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+16):nth-child(-n+17)").css("min-width", "60px");

                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+19):nth-child(-n+23), #ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+19):nth-child(-n+23)").css("width", "auto");
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+25):nth-child(-n+26) ,#ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+25):nth-child(-n+26)").css("width", "auto");

                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+18):nth-child(-n+18), #ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+18):nth-child(-n+18)").css("min-width", "40px");
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+24):nth-child(-n+25), #ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+24):nth-child(-n+25)").css("min-width", "40px");
                        for (var icount = 32; icount <= 52; icount++) {
                            $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+" + icount + "):nth-child(-n+" + icount + ") ,#ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+" + icount + "):nth-child(-n+" + icount + ")").css("min-width", "25px");
                        }

                        gridviewScroll(950, 500);
                    } else {                       
                        //                    gridviewScroll(950, 500);
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+10):nth-child(-n+23), #ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+10):nth-child(-n+23)").hide();
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+24):nth-child(-n+26), #ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+24):nth-child(-n+26)").hide();
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr td:nth-child(n+9):nth-child(-n+9), #ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+9):nth-child(-n+9)").hide();
                        if (totaltr >= pageSize) {
                            $("#ctl00_ContentPlaceHolder1_gvPotentialOutput tr:last > td").css('display', 'inline-block');
                        }
                    }
                }
                else {                    
                    if ($("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr > td:nth-child(n+11):nth-child(-n+23), #ctl00_ContentPlaceHolder1_gvPotentialOutputCopy tr th:nth-child(n+11):nth-child(-n+23)").css('display') == 'none') {

                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputHeaderCopy tr th:nth-child(n+10):nth-child(-n+23), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseHeaderCopy tr th:nth-child(n+10):nth-child(-n+23)").css("display", "table-cell");
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputHeaderCopy tr th:nth-child(n+24):nth-child(-n+26), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseHeaderCopy tr th:nth-child(n+24):nth-child(-n+26)").css("display", "table-cell");
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputHeaderCopy tr th:nth-child(n+9):nth-child(-n+9), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseHeaderCopy tr th:nth-child(n+9):nth-child(-n+9)").css("display", "table-cell");

                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+10):nth-child(-n+23), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseCopy tr th:nth-child(n+10):nth-child(-n+23)").css("display", "table-cell");
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+24):nth-child(-n+26), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseCopy tr th:nth-child(n+24):nth-child(-n+26)").css("display", "table-cell");
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+9):nth-child(-n+9), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseCopy tr th:nth-child(n+9):nth-child(-n+9)").css("display", "table-cell");

                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+10):nth-child(-n+12), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseCopy tr th:nth-child(n+10):nth-child(-n+12)").css("min-width", "30px");

                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+16):nth-child(-n+17), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseCopy tr th:nth-child(n+16):nth-child(-n+17)").css("min-width", "60px");

                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+19):nth-child(-n+23), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseCopy tr th:nth-child(n+19):nth-child(-n+23)").css("width", "auto");
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+25):nth-child(-n+26) ,#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseCopy tr th:nth-child(n+25):nth-child(-n+26)").css("width", "auto");

                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+18):nth-child(-n+18), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseCopy tr th:nth-child(n+18):nth-child(-n+18)").css("min-width", "40px");
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+24):nth-child(-n+25), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseCopy tr th:nth-child(n+24):nth-child(-n+25)").css("min-width", "40px");
                        for (var icount = 32; icount <= 52; icount++) {
                            $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+" + icount + "):nth-child(-n+" + icount + ") ,#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseCopy tr th:nth-child(n+" + icount + "):nth-child(-n+" + icount + ")").css("min-width", "25px");
                        }

                        gridviewScroll(950, 500);
                    } else {
                        //debugger;
                        //                    gridviewScroll(950, 500);
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+10):nth-child(-n+23), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseCopy tr th:nth-child(n+10):nth-child(-n+23)").hide();
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+24):nth-child(-n+26), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseCopy tr th:nth-child(n+24):nth-child(-n+26)").hide();
                        $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr td:nth-child(n+9):nth-child(-n+9), #ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWiseCopy tr th:nth-child(n+9):nth-child(-n+9)").hide();
                        if (totaltr >= pageSize) {
                            $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise tr:last > td").css('display', 'inline-block');
                        }
                    }
                }
            });
        }


        function SiteCheck() {
            var StockPlannerCount = $("#ctl00_ContentPlaceHolder1_msStockPlanner_ucLBRight option").length;
            var InventoryGroupCount = $("#ctl00_ContentPlaceHolder1_multiSelectStockPlannerGrouping_lstRight option").length;
            var SiteCount = $("#ctl00_ContentPlaceHolder1_msSite_lstRight option").length;
            var IncludeVendorCount = $("#ctl00_ContentPlaceHolder1_ucIncludeVendor_lstSelectedVendor option").length;
            var ExcludeVendorCount = $("#ctl00_ContentPlaceHolder1_ucExcludeVendor_lstSelectedVendor option").length;
            var ItemClassificationCount = $("#ctl00_ContentPlaceHolder1_msItemClassification_lstRight option").length;
            var MRPTypeCount = $("#ctl00_ContentPlaceHolder1_ucMRPType_lstRight option").length;
            var PurcGrpCount = $("#ctl00_ContentPlaceHolder1_UcPurcGrp_lstRight option").length;
            var MatGroupCount = $("#ctl00_ContentPlaceHolder1_UcMatGroup_lstRight option").length;
            var VikingSkuCount = $("#ctl00_ContentPlaceHolder1_UcVikingSku_lstRight option").length;
            var ODSKUCount = $("#ctl00_ContentPlaceHolder1_msSKU_lstRight option").length;
            var SKUGroupingCount = $("#ctl00_ContentPlaceHolder1_multiSelectSKUGrouping_lstRight option").length;

            if (StockPlannerCount == 0) {
                if (InventoryGroupCount == 0) {
                    if (IncludeVendorCount == 0) {
                        if (ExcludeVendorCount == 0) {
                            if (ItemClassificationCount == 0) {
                                if (MRPTypeCount == 0) {
                                    if (PurcGrpCount == 0) {
                                        if (MatGroupCount == 0) {
                                            if (VikingSkuCount == 0) {
                                                if (ODSKUCount == 0) {
                                                    if (SKUGroupingCount == 0) {
                                                        if (SiteCount == 0) {
                                                            alert('<%=Pleaseaddsearchcriteria%>');
                                                            return false;
                                                        }
                                                        else if (SiteCount > 4) {
                                                            alert('<%=NewMaximumfoursitesareallowed%>');
                                                            return false;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //            if (InventoryGroupCount < 0) {
            //                alert('<%=Pleaseselectatleastonesite%>');
            //                return false;
            //            }
            //            if (SiteCount > 4) {
            //                alert('<%=Maximumfoursitesareallowed%>');
            //                return false;
            //            }
            //            else if (SiteCount <= 0) {
            //                alert('<%=Pleaseselectatleastonesite%>');
            //                return false;
            //            }
            //            else
            //                return true;
        }
        $(document).ready(function () {
            var counttd = $(".grid td").size();
            if (counttd == 1) {
                $(".grid").css('width', '100%');
            }
        });

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    if ($('#<%= hdnIsAdditionSearch.ClientID %>').val() == "true") {
                        $('#<%=btnAdditionalSearch.ClientID %>').click();
                    }
                    else {
                        if (SiteCheck() == true) {
                            $('#<%=btnSearch_1.ClientID %>').click();
                        }
                    }
                    return false;
                }
            });
        });

        
    </script>
    <script type="text/javascript">

        function InitializeToolTipSOH() {

            $(".gridViewToolTipNew_SOH").tooltip({
                track: true,
                delay: 0,
                showURL: false,
                fade: 100,
                bodyHandler: function () {

                    var obj = $(this).parent().find(".tooltipCommentSOH");
                    var SKUSOH = $(this).parent().find(".SKUSOH");
                    var VikingCodeSOH = $(this).parent().find(".VikingCodeSOH");
                    var DescriptionSOH = $(this).parent().find(".DescriptionSOH");
                    var tblDetails = $(this).parent().find(".tblDetails");

                    var length = "ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl".length;
                    var res = this.id.substring(length, length + 5);
                    iRowIndex = res.split("_")[0];

                    var lblOdCode = $("#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_lblOdCode").text();
                    var lblViking = $("#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_lblViking").text();
                    var lbldescription = $("#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_lbldescription").text();
                    var hdnCountryID = $("#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl" + iRowIndex + "_hdnCountryID").val();

                    //lblOdCode = $('#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl02_lblOdCode').text().trim();
                    var site, soh, avg;
                    $.ajax({

                        type: "POST",

                        url: "ExpediteStockSearch.aspx/GetSOH",

                        data: JSON.stringify({ OD_SKU_NO: lblOdCode, CountryID: hdnCountryID }),

                        contentType: "application/json; charset=utf-8",

                        dataType: "json",
                        async: false,
                        success: function (data) {
                            //                            if (data.d[0].SiteNames_SOHPOP.split(',').length > 1) {
                            $(".tblDetails").empty();
                            site = data.d[0].SiteNames_SOHPOP;
                            soh = data.d[0].SOH_SOHPOP;
                            avg = data.d[0].AVGForcast_SOHPOP;
                            var SOHValues;
                            $(".tblDetails").append("<tr><td style=width: 33%><b><u> Site</u></b></td><td style=width: 33% align=center><b> <u>SOH</u></b></td><td align=center style=width: 34%><b><u> # Days Stock</u></b></td></tr>");
                            for (var i = 0; i < site.split(',').length; i++) {
                                if (parseFloat(soh.split(',')[i]) < 0) {
                                    SOHValues = 0;
                                }
                                else {
                                    SOHValues = soh.split(',')[i]
                                }

                                if (soh.split(',')[i] == '' || soh.split(',')[i] == undefined || soh.split(',')[i] == NaN) {
                                    $(".tblDetails").append("<tr><td>" + site.split(',')[i] + "</td> <td align=middle></td> <td align=center></td>  </tr>");
                                }
                                else {
                                    $(".tblDetails").append("<tr><td>" + site.split(',')[i] + "</td> <td align=middle>" + soh.split(',')[i] + "</td> <td align=center>" + Math.floor(parseFloat(SOHValues) / parseFloat(avg.split(',')[i])) + "</td>  </tr>");
                                }
                                // console.log($("#tblDetails").html());
                            }

                            SKUSOH.text(lblOdCode);
                            VikingCodeSOH.text(lblViking);
                            DescriptionSOH.text(lbldescription);
                            //                            }
                            //                            else {
                            //                                $(obj).hide();
                            //                                obj = null;
                            //                            }
                            //                            }
                            //                            else {
                            //                                $("#tblDetails").hide();
                            //                                $("#tblDetailsMain").hide();
                            //                            }
                        },
                        error: function error(xhr, status, error) { alert('error'); }


                    });
                    //                    if (site.split(',').length > 1) {
                    //                        debugger;
                    //                        obj = null;
                    //                    }                  

                    if (obj != null) {
                        return $(obj).html();
                    }
                    $(obj).hide();

                }
            });

            $(".gridViewWeekWiseToolTipNew_SOH").tooltip({
                track: true,
                delay: 0,
                showURL: false,
                fade: 100,
                bodyHandler: function () {

                    var obj = $(this).parent().find(".tooltipWeekWiseCommentSOH");
                    var SKUSOH = $(this).parent().find(".SKUSOH");
                    var VikingCodeSOH = $(this).parent().find(".VikingCodeSOH");
                    var DescriptionSOH = $(this).parent().find(".DescriptionSOH");
                    var tblDetailsWeekWise = $(this).parent().find(".tblDetailsWeekWise");

                    var length = "ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise_ctl".length;
                    var res = this.id.substring(length, length + 5);
                    iRowIndex = res.split("_")[0];

                    var lblOdCode = $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise_ctl" + iRowIndex + "_lblOdCode").text();
                    var lblViking = $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise_ctl" + iRowIndex + "_lblViking").text();
                    var lbldescription = $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise_ctl" + iRowIndex + "_lbldescription").text();
                    var hdnCountryID = $("#ctl00_ContentPlaceHolder1_gvPotentialOutputWeekWise_ctl" + iRowIndex + "_hdnCountryID").val();

                    //lblOdCode = $('#ctl00_ContentPlaceHolder1_gvPotentialOutput_ctl02_lblOdCode').text().trim();
                    var site, soh, avg;
                    $.ajax({

                        type: "POST",

                        url: "ExpediteStockSearch.aspx/GetSOH",

                        data: JSON.stringify({ OD_SKU_NO: lblOdCode, CountryID: hdnCountryID }),

                        contentType: "application/json; charset=utf-8",

                        dataType: "json",
                        async: false,
                        success: function (data) {
                            //                            if (data.d[0].SiteNames_SOHPOP.split(',').length > 1) {
                            $(".tblDetailsWeekWise").empty();
                            site = data.d[0].SiteNames_SOHPOP;
                            soh = data.d[0].SOH_SOHPOP;
                            avg = data.d[0].AVGForcast_SOHPOP;
                            var SOHValues;
                            $(".tblDetailsWeekWise").append("<tr><td style=width: 33%><b><u> Site</u></b></td><td style=width: 33% align=center><b> <u>SOH</u></b></td><td align=center style=width: 34%><b><u> # Days Stock</u></b></td></tr>");
                            for (var i = 0; i < site.split(',').length; i++) {
                                if (parseFloat(soh.split(',')[i]) < 0) {
                                    SOHValues = 0;
                                }
                                else {
                                    SOHValues = soh.split(',')[i]
                                }

                                if (soh.split(',')[i] == '' || soh.split(',')[i] == undefined || soh.split(',')[i] == NaN) {
                                    $(".tblDetailsWeekWise").append("<tr><td>" + site.split(',')[i] + "</td> <td align=middle></td> <td align=center></td>  </tr>");
                                }
                                else {
                                    $(".tblDetailsWeekWise").append("<tr><td>" + site.split(',')[i] + "</td> <td align=middle>" + soh.split(',')[i] + "</td> <td align=center>" + Math.floor(parseFloat(SOHValues) / parseFloat(avg.split(',')[i])) + "</td>  </tr>");
                                }
                                // console.log($("#tblDetails").html());
                            }

                            SKUSOH.text(lblOdCode);
                            VikingCodeSOH.text(lblViking);
                            DescriptionSOH.text(lbldescription);
                            //                            }
                            //                            else {
                            //                                $(obj).hide();
                            //                                obj = null;
                            //                            }
                            //                            }
                            //                            else {
                            //                                $("#tblDetails").hide();
                            //                                $("#tblDetailsMain").hide();
                            //                            }
                        },
                        error: function error(xhr, status, error) { alert('error'); }


                    });
                    //                    if (site.split(',').length > 1) {
                    //                        debugger;
                    //                        obj = null;
                    //                    }                  

                    if (obj != null) {
                        return $(obj).html();
                    }
                    $(obj).hide();

                }
            });
        }



        function InitializeToolTipComment() {

            $(".gridViewToolTipNew").tooltip({
                track: true,
                delay: 0,
                showURL: false,
                fade: 100,
                bodyHandler: function () {

                    var obj = $(this).parent().find(".tooltipComment");
                    var commentDate = $(this).parent().find(".commentDateTooltip");
                    var commentText = $(this).parent().find(".commentTextTooltip");
                    var commentReason = $(this).parent().find(".commentReasonTooltip");
                    var commentValid = $(this).parent().find(".commentValidtooltip");
                    var commentAddedBy = $(this).parent().find(".commentAddedByTooltip");
                    //if (commentDate.text().length > 0) {

                    var hdnSKUID = $(this).parent().find(".lblComntSKU").text();
                    $.ajax({

                        type: "POST",

                        url: "ExpediteStockSearch.aspx/GetLatestComment",

                        data: JSON.stringify({ skuId: hdnSKUID }),

                        contentType: "application/json; charset=utf-8",

                        dataType: "json",
                        async: false,
                        success: function (r) {
                            var arr = [];
                            for (elem in r) {
                                arr.push(r[elem]);
                            }
                            //alert(arr[0].split('|')[0]);
                            commentDate.text(arr[0].split('|')[0]);
                            commentAddedBy.text(arr[0].split('|')[1]);
                            commentValid.text(arr[0].split('|')[2]);
                            commentReason.text(arr[0].split('|')[3]);
                            commentText.text(arr[0].split('|')[4]);
                        },
                        error: function error(xhr, status, error) { alert('error'); }
                    });

                    if (commentDate.text().length <= 0) {
                        obj = null;
                    }
                    return $(obj).html();
                }
            });

            $(".gridViewWeekWiseToolTipNew").tooltip({
                track: true,
                delay: 0,
                showURL: false,
                fade: 100,
                bodyHandler: function () {

                    var obj = $(this).parent().find(".tooltipWeekWiseComment");
                    var commentDate = $(this).parent().find(".commentDateWeekWiseTooltip");
                    var commentText = $(this).parent().find(".commentTextWeekWiseTooltip");
                    var commentReason = $(this).parent().find(".commentReasonWeekWiseTooltip");
                    var commentValid = $(this).parent().find(".commentValidWeekWisetooltip");
                    var commentAddedBy = $(this).parent().find(".commentAddedByWeekWiseTooltip");
                    //if (commentDate.text().length > 0) {

                    var hdnSKUID = $(this).parent().find(".lblComntSKU").text();
                    $.ajax({

                        type: "POST",

                        url: "ExpediteStockSearch.aspx/GetLatestComment",

                        data: JSON.stringify({ skuId: hdnSKUID }),

                        contentType: "application/json; charset=utf-8",

                        dataType: "json",
                        async: false,
                        success: function (r) {
                            var arr = [];
                            for (elem in r) {
                                arr.push(r[elem]);
                            }
                            //alert(arr[0].split('|')[0]);
                            commentDate.text(arr[0].split('|')[0]);
                            commentAddedBy.text(arr[0].split('|')[1]);
                            commentValid.text(arr[0].split('|')[2]);
                            commentReason.text(arr[0].split('|')[3]);
                            commentText.text(arr[0].split('|')[4]);
                        },
                        error: function error(xhr, status, error) { alert('error'); }
                    });

                    if (commentDate.text().length <= 0) {
                        obj = null;
                    }
                    return $(obj).html();
                }
            });
        }

        function InitializeToolTip() {
            $(".gridViewToolTip").tooltip({
                track: true,
                delay: 0,
                showURL: false,
                fade: 100,
                bodyHandler: function () {

                    return $("#tooltip").html();
                },
                showURL: false
            });

            $(".gridViewWeekWiseToolTip").tooltip({
                track: true,
                delay: 0,
                showURL: false,
                fade: 100,
                bodyHandler: function () {

                    return $("#tooltip").html();
                },
                showURL: false
            });
        }
        function CallExpediteComment(siteid, Skuid, VendorId, DirectCode, OdCode, Status) {
            window.open("ExpediteComments.aspx?SiteID=" + siteid + "&&SKUID=" + Skuid + "&VendorID=" + VendorId + "&Viking=" + DirectCode + "&ODCode=" + OdCode + "&Status=" + Status, 'ExpediteComment', 'height=600,width=600');
        }
    </script>
    <script type="text/javascript">
        $(function () {
            InitializeToolTip();
        })
        $(function () {
            InitializeToolTipSOH();
            InitializeToolTipComment();

        })      
      
    </script>
    <style type="text/css">
        #tooltip
        {
            position: absolute;
            z-index: 3000;
            border: 1px solid #111;
            background-color: #FEE18D;
            padding: 5px;
            opacity: 1.00;
        }
        #tooltip h3, #tooltip div
        {
            margin: 0;
        }
    </style>
    <script type="text/javascript">
        function AddComment(url) {
            window.open(url, '_blank');
        }
        
    </script>
  
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblExpediteStock" runat="server" Text="Expedite Stock"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnSKUID" runat="server" />
    <asp:HiddenField ID="hdnVendorId" runat="server" />
    <asp:HiddenField ID="hdnDirectCode" runat="server" />
    <asp:HiddenField ID="hdnOdCode" runat="server" />
    <asp:HiddenField ID="hdnStatus" runat="server" />
    <asp:HiddenField ID="hdnDisc" runat="server" />
    <asp:HiddenField ID="hdnSiteId" runat="server" />
    <asp:HiddenField ID="hdnCountryID" runat="server" />
    <asp:HiddenField ID="hdnStartDate" runat="server" />
    <asp:HiddenField ID="hdnEndDate" runat="server" />
    <asp:HiddenField ID="hdnIsAdditionSearch" runat="server" />
    <asp:HiddenField ID="hdnrblSearchType" runat="server" />
    <div id="tooltip" style="display: none;">
        <table>
            <tr>
                <td style="white-space: nowrap;">
                    <b>1 =
                        <%=OverduePurchasesOrdersonly%></b>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;">
                    <b>2 =
                        <%=OverduesomeNotOverdue%></b>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;">
                    <b>3 =
                        <%=NoOverdue%></b>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;">
                    <b>4 =
                        <%=NoPORaised%></b>
                </td>
            </tr>
        </table>
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSearchScreen" runat="server">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDisplay" runat="server" Text="Display">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="width: 800px;" class="nobold radiobuttonlist">
                            <cc1:ucRadioButtonList ID="rblDisplay" CssClass="radio-fix" runat="server" RepeatColumns="4"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="lstExpditeDisplayItem1" Value="1">
                                </asp:ListItem>
                                <asp:ListItem Text="lstExpditeDisplayItem2" Value="2">
                                </asp:ListItem>
                                <asp:ListItem Text="lstExpditeDisplayItem3" Value="3">
                                </asp:ListItem>
                                <asp:ListItem Text="lstExpditeDisplayItem4" Value="4" Selected="True">
                                </asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSortBy" runat="server" Text="Sort By">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel13" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="width: 800px;" class="nobold radiobuttonlist">
                            <cc1:ucRadioButtonList ID="rblSortBy" CssClass="radio-fix" runat="server" RepeatColumns="5"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="lstExpditeSortByItem1" Value="V">
                                </asp:ListItem>
                                <asp:ListItem Text="lstExpditeSortByItem2" Value="FB" Selected="True">
                                </asp:ListItem>
                                <asp:ListItem Text="lstExpditeSortByItem3" Value="CBC">
                                </asp:ListItem>
                                <asp:ListItem Text="lstExpditeSortByItem4" Value="CBQ">
                                </asp:ListItem>
                                <asp:ListItem Text="lstStockOnHand" Value="SOH">
                                </asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;" valign="top">
                            <cc1:ucLabel ID="lblPOStatus" runat="server" Text="PO Status">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" valign="top">
                            <cc1:ucLabel ID="UcLabel14" runat="server">:</cc1:ucLabel>
                        </td>
                        <td class="nobold radiobuttonlist">
                            <cc1:ucRadioButtonList ID="rblPOStatus" CssClass="radio-fix-last" runat="server"
                                RepeatColumns="5" RepeatDirection="Horizontal">
                                <asp:ListItem Text="lstExpditePOStatusItem1" Value="0" Selected="True">
                                </asp:ListItem>
                                <asp:ListItem Text="lstExpditePOStatusItem2" Value="1">
                                </asp:ListItem>
                                <asp:ListItem Text="lstExpditePOStatusItem3" Value="2">
                                </asp:ListItem>
                                <asp:ListItem Text="lstExpditePOStatusItem4" Value="3">
                                </asp:ListItem>
                                <asp:ListItem Text="lstExpditePOStatusItem5" Value="4">
                                </asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;" valign="top">
                            <cc1:ucLabel ID="lblDaysStock" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" valign="top">
                            <cc1:ucLabel ID="UcLabel16" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtDayStock" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);"
                                runat="server" Width="40px" MaxLength="2" ClientIDMode="Static">
                            </cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;" valign="top">
                            <cc1:ucLabel ID="lblMinForecastedSalesNew" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" valign="top">
                            <cc1:ucLabel ID="UcLabel17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucTextbox ID="txtMinForecastedSales" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);"
                                runat="server" Width="40px" ClientIDMode="Static">
                            </cc1:ucTextbox>
                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                            <cc1:ucLabel ID="lblPriorityItems" runat="server" Text="Priority Items"></cc1:ucLabel>&nbsp;&nbsp;:
                            <asp:RadioButton ID="rdoPriorityShowall" Text="Show All" runat="server" Checked="true" GroupName="abc"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:RadioButton ID="rdoPriorityShowPriority" Text="Show only Priority Items" runat="server" GroupName="abc"/>

                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch_1" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click"
                                    OnClientClick="return SiteCheck();" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblInventoryGroup" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlannerGrouping runat="server" ID="multiSelectStockPlannerGrouping" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblIncludeVendor" runat="server" Text="Include Vendor">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <%--<cc1:MultiSelectVendor runat="server" ID="ucIncludeVendor" />--%>
                            <cc1:ucMultiSelectVendor ID="ucIncludeVendor" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblExcludeVendor" runat="server" Text="Exclude Vendor">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <%--<cc2:MultiSelectVendor2 runat="server" ID="ucExcludeVendor" />--%>
                            <cc1:ucMultiSelectVendor ID="ucExcludeVendor" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblItemClassification" runat="server" Text="Item Classfication">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel10" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectItemClassification runat="server" ID="msItemClassification" />
                        </td>
                    </tr>
                    <%--------------Start Added New UserControls---------%>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UclblMRPType" runat="server" Text="MRP Type">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucMRType runat="server" ID="ucMRPType" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblPurcGrp" runat="server" Text="Purc Group">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel9" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucPurchGroup ID="UcPurcGrp" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblMatGrp" runat="server" Text="Mat Grp">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel11" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucMatGroup ID="UcMatGroup" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UclblVikingSku" runat="server" Text="Viking Sku">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel12" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucVikingSku ID="UcVikingSku" runat="server" />
                        </td>
                    </tr>
                    <%-----------------End-------------------------------%>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblODSKUCode" runat="server" Text="OD SKU Code">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSKU runat="server" ID="msSKU" />
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSKUgrouping" runat="server" Text="Sku Grouping">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel81" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSKUGrouping runat="server" ID="multiSelectSKUGrouping" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSKUType" runat="server" Text="SKU Type">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel55" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="width: 800px;" class="nobold radiobuttonlist" valign="middle">
                            <cc1:ucRadioButtonList CssClass="radio-fix" ID="rblSkuType" runat="server" RepeatColumns="4"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="lstExpditeSKUTypeItem1" Value="1" Selected="True">
                                </asp:ListItem>
                                <asp:ListItem Text="lstExpditeSKUTypeItem2" Value="2">
                                </asp:ListItem>
                                <asp:ListItem Text="lstExpditeSKUTypeItem3" Value="3">
                                </asp:ListItem>
                                <asp:ListItem Text="lstExpditeSKUTypeItem4" Value="4">
                                </asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblCommentStatus" runat="server" Text="Comment Status">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel15" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="width: 800px;" class="nobold radiobuttonlist">
                            <cc1:ucRadioButtonList ID="rblCommentStatus" CssClass="radio-fix" runat="server"
                                RepeatColumns="4" RepeatDirection="Horizontal">
                                <asp:ListItem Text="ExpditePOStatusItem1" Value="0" Selected="True">
                                </asp:ListItem>
                                <asp:ListItem Text="NoCommentUpdate" Value="1">
                                </asp:ListItem>
                                <asp:ListItem Text="CommentApplied" Value="2">
                                </asp:ListItem>
                                <asp:ListItem Text="Expedited" Value="3">
                                </asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>

                     <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSearchType" runat="server" Text="Search Type">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel18" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="width: 800px;" class="nobold radiobuttonlist">
                            <cc1:ucRadioButtonList ID="rblSearchType" CssClass="radio-fix" runat="server"
                                RepeatColumns="2" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Day Wise" Value="0" Selected="True">
                                </asp:ListItem>
                                <asp:ListItem Text="Week Wise" Value="1">
                                </asp:ListItem>                              
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click"
                                    OnClientClick="return SiteCheck();" />
                            </div>
                        </td>
                    </tr>
                    <%-- <tr>
                        <td align="left" colspan="3">
                            <cc1:ucPanel ID="pnlMenuLink" runat="server">
                                <asp:HyperLink ID="lnkERSUP" ForeColor="Blue" runat="server" Text="Expedite Reason Set Up"
                                    NavigateUrl="ExpediteReasonSetUpOverview.aspx"></asp:HyperLink>
                                <br />
                            </cc1:ucPanel>
                        </td>
                    </tr>--%>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlPotentialOutput" runat="server">
                <table width="99%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="width: 15%; font-weight: bold;">
                            <cc1:ucLabel ID="lblSiteName_1" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="width: 1%; font-weight: bold;">
                            :
                        </td>
                        <td style="width: 27%;">
                            <cc2:ucSite ID="ddlSite" runat="server" />
                        </td>
                        <td style="width: 15%; font-weight: bold;">
                            <cc1:ucLabel ID="lblStockByDate" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td style="width: 27%;">
                            <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                                ReadOnly="True" Width="70px" />
                            <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" Style="display: none;" runat="server"
                                ReadOnly="True" Width="70px" />
                        </td>
                        <td style="width: 10%; font-weight: bold;">
                            <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td style="width: 32%; font-weight: bold;">
                            <span id="spVender" runat="server">
                                <cc3:ucSeacrhVendor ID="ucSeacrhVendor1" runat="server" />
                            </span>
                            <cc1:ucLiteral ID="ltSearchVendorName" runat="server" Visible="false">
                             </cc1:ucLiteral>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVikingSku_1" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txtVikingSku" Width="100" runat="server" />
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblODSKUCode_1" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txtODSKUCode" Width="100" runat="server" />
                        </td>
                        <td colspan="3" align="right">
                            <cc1:ucButton ID="btnAdditionalSearch" runat="server" Text="Search" CssClass="button"
                                OnClientClick="return WeekendCheck();" OnClick="btnAdditionalSearch_Click" />
                        </td>
                    </tr>
                </table>
                <div id="divPrint" class="fixedTable">
                    <table cellspacing="1" cellpadding="0" border="0" class="form-table" width="99%">
                        <tr>
                            <td style="width: 10%; text-align: left;">
                                <div>
                                    <a href="javascript:void(0);" id="showhide">hide</a></div>
                            </td>
                            <td style="width: 50%; text-align: left;">
                                <asp:UpdatePanel ID="updpnlWarning" runat="server">
                                    <ContentTemplate>
                                        <cc1:ucButton ID="btnExpediteSelection" runat="server" Text="Expedite Selection"
                                            OnClick="btnExpediteSelection_Click" CssClass="button" />
                                        <asp:Button ID="btexpedite" runat="server" Style="display: none" />
                                        <ajaxToolkit:ModalPopupExtender ID="mdlConfirmMsg" runat="server" TargetControlID="btexpedite"
                                            PopupControlID="pnlConfirmMsg" BackgroundCssClass="modalBackground" BehaviorID="ConfirmMsg"
                                            DropShadow="false" />
                                        <asp:Panel ID="pnlConfirmMsg" runat="server" Style="display: none;">
                                            <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                                                <h3>
                                                    <cc1:ucLabel ID="lblWarningInfo_3" runat="server">
                                                     </cc1:ucLabel></h3>
                                                <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                                                    <tr>
                                                        <td>
                                                            <div class="popup-innercontainer top-setting-Popup">
                                                                <div class="row1">
                                                                    <cc1:ucLiteral ID="ltConfirmMsg" runat="server" Text=""></cc1:ucLiteral></div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <div class="row">
                                                                <cc1:ucButton ID="btnErrContinue" runat="server" Text="CONTINUE" OnCommand="btnErrContinue_Command"
                                                                    CssClass="button" />
                                                                &nbsp;
                                                                <cc1:ucButton ID="btnErrBack" runat="server" OnCommand="btnErrBack_Command" Text="BACK"
                                                                    CssClass="button" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnErrContinue" />
                                        <asp:AsyncPostBackTrigger ControlID="btnErrBack" />
                                        <asp:AsyncPostBackTrigger ControlID="btnExpediteSelection" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <cc2:ucExportExpeditePotenOutput ID="ucExportToExcel1" runat="server" />
                            </td>
                        </tr>
                    </table>
                  <%--  <div class="wmd-view-topscroll">
                        <div class="scroll-div">
                            &nbsp;
                        </div>
                    </div>--%>

                    <table id="tblPotentialOutputDayWise" cellspacing="1" cellpadding="0" border="0" class="form-table" width="99%" runat="server">
                    <tr> <td>
                    <div>
                        <div>
                            <cc1:ucGridView ID="gvPotentialOutput" OnRowDataBound="gvPotentialOutput_OnRowDataBound"
                                runat="server" CssClass="grid gvclass searchgrid-1" GridLines="Both" AllowPaging="false"
                                PageSize="200">
                                <emptydatatemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblRecordNotFound" runat="server" Text="No record found"></cc1:ucLabel>
                                    </div>
                                </emptydatatemplate>
                                <columns>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="C" SortExpression="Comment">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Literal runat="server" ID="lblUpdatedDateValue" Text='<%# Eval("UpdatedDate") %>'
                                                Visible="false"></asp:Literal>
                                            <asp:Literal runat="server" ID="lblCommentColor" Visible="false" Text='<%#Eval("CommentColor") %>'></asp:Literal>
                                            <asp:Literal runat="server" ID="lblPriorityColor" Visible="false" Text='<%#Eval("PriorityItemColor") %>'></asp:Literal>
                                            <%-- <asp:HyperLink ID="lnkCommentValue"  runat="server"   Target="_blank" Text='<%# Eval("Comment") %>' ></asp:HyperLink>--%>
                                            <asp:HiddenField ID="hdnSKUID" runat="server" Value='<%# Eval("SKUID") %>' />
                                            <asp:HiddenField ID="hdnVendorId" runat="server" Value='<%# Eval("Vendor.VendorID") %>' />
                                            <asp:HiddenField ID="hdnDirectCode" runat="server" Value='<%# Eval("PurchaseOrder.Direct_code") %>' />
                                            <asp:HiddenField ID="hdnOdCode" runat="server" Value='<%# Eval("PurchaseOrder.OD_Code") %>' />
                                            <asp:HiddenField ID="hdnStatus" runat="server" Value='<%# Eval("Status") %>' />
                                            <asp:HiddenField ID="hdnDisc" runat="server" Value='<%# Eval("PurchaseOrder.Product_description") %>' />
                                            <asp:HiddenField ID="hdnSiteId" runat="server" Value='<%# Eval("Site.SiteID") %>' />
                                            <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                            <cc1:ucLabel ID="lblComntSKU" class="lblComntSKU" Text='<%# Eval("SKUID") %>' Style="display: none;"
                                                runat="server" />
                                                <cc1:ucLabel ID="lblOdCode" class="lblOdCode" Text='<%# Eval("PurchaseOrder.OD_Code") %>' Style="display: none;"
                                                runat="server" />
                                                <cc1:ucLabel ID="lblViking" class="lblViking" Text='<%# Eval("PurchaseOrder.Direct_code") %>' Style="display: none;"
                                                runat="server" />
                                                <cc1:ucLabel ID="lbldescription" class="lbldescription" Text='<%# Eval("PurchaseOrder.Product_description") %>' Style="display: none;"
                                                runat="server" />
                                            <asp:HyperLink ID="lnkCommentValue" CssClass="gridViewToolTipNew" runat="server"
                                                Target="_blank" NavigateUrl='<%# EncryptQuery("ExpediteComments.aspx?SiteID=" + Eval("Site.SiteID") 
                                                        + "&SKUID=" + Eval("SKUID") 
                                                        + "&VendorID=" + Eval("Vendor.VendorID")
                                                        + "&Viking=" + Eval("PurchaseOrder.Direct_code")
                                                        + "&ODCode=" + Eval("PurchaseOrder.OD_Code")
                                                        + "&Status=" + Eval("Status")
                                                        + "&Desc=" + Eval("PurchaseOrder.Product_description")
                                                        + "&IsSingleComment=1"
                                                        )%>'>
                                                        <img  src="../../../Images/info_button1.gif" />
                                                        </asp:HyperLink>
                                            <%-- //style="display:none;"--%>
                                            <asp:Panel ID="pnlToolComment" runat="server" CssClass="tooltipComment" Style="display: none;">
                                                <asp:HiddenField ID="hdncommentAddedDate" runat="server" Value='<%# Eval("CommentsAdded_Date") %>'>
                                                </asp:HiddenField>                                               
                                                <table cellpadding="15" cellspacing="5" style="width:300px; background-color:#F4B084;" >
                                                <tr>
                                                    <td style="width: 30%;">
                                                        <b>Date</b>
                                                    </td>
                                                    <td style="width: 70%;">
                                                        <cc1:uclabel id="CommentDate" class="commentDateTooltip" text='<%# Eval("CommentsAdded_Date", "{0:dd/MM/yyyy}") %>'
                                                            runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Added By</b>
                                                    </td>
                                                    <td>
                                                        <cc1:uclabel id="CommentAddedBy" class="commentAddedByTooltip" text='<%#Eval("CommentsAddedBy") %>'
                                                            runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Valid Till</b>
                                                    </td>
                                                    <td>
                                                        <cc1:uclabel id="CommentValid" class="commentValidtooltip" text='<%# Eval("CommentsValid_Date", "{0:dd/MM/yyyy}")%>'
                                                            runat="server" />
                                                    </td>
                                                    <tr>
                                                        <td>
                                                            <b>Reason Code</b>
                                                        </td>
                                                        <td>
                                                            <cc1:uclabel id="CommentReason" class="commentReasonTooltip" text='<%#Eval("Reason") %>'
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <b><u>Comment</u> </b>
                                                        </td>
                                                        </tr>
                                                        <tr>
                                                        <td colspan="2">
                                                            <cc1:uclabel id="CommentText" class="commentTextTooltip" text='<%#Eval("Comments") %>'
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                            </table>                                               
                                            </asp:Panel>
                                     
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="S" SortExpression="Status">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <%--<cc1:ucLinkButton runat="server" ID="lnkStatusValue" CommandName="Status" Text='<%#Eval("Status") %>'></cc1:ucLinkButton>--%>
                                            <cc1:ucLabel runat="server" ID="lblStatusValue" Font-Bold="true" Text='<%#Eval("Status") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <%-- <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName" >
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblVendorNameValue" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSiteNameValue" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <%--   <asp:TemplateField HeaderText="OfficeDepotCode" SortExpression="PurchaseOrder.OD_Code">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblOD_CodeValue" Text='<%#Eval("PurchaseOrder.OD_Code") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" />
                                            </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="VikingCode" SortExpression="PurchaseOrder.Direct_code">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVikingCodeValue" Text='<%#Eval("PurchaseOrder.Direct_code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO" SortExpression="PurchaseOrder.Purchase_order">
                                       <%-- <HeaderStyle Width="30px" HorizontalAlign="Left" />--%>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lblPOValue" class="gridViewToolTip" runat="server" Target="_blank"
                                                Text='<%# Eval("POStatus") %>' NavigateUrl='<%# EncryptQuery("OpenPurchaseListing.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&OD_Code=" + Eval("PurchaseOrder.OD_Code")+ "&Status=" + Eval("Status"))%>'></asp:HyperLink>
                                        </ItemTemplate>
                                       <%-- <ItemStyle HorizontalAlign="Left" Width="30px" />--%>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description1" SortExpression="PurchaseOrder.Product_description">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblProductDesValue" Text='<%#Eval("PurchaseOrder.Product_description") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="SUBVNDR" SortExpression="subvndr">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblsubvndr" Text='<%#Eval("subvndr") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EarliestAdviseDate" SortExpression="EarliestAdviseDate">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkEarliestAdviseDateValue" runat="server" Target="_blank" Text='<%#Eval("EarliestAdviseDate", "{0:dd/MM/yyyy}") %>'
                                                NavigateUrl='<%# EncryptQuery("ExpediteBookingDate.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&VendorID=" + Eval("Vendor.VendorID") +"&BookingDate=" + Eval("EarliestAdviseDate", "{0:MM/dd/yyyy}"))%>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Earliest PO">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblEarliestPO" Text='<%#Eval("EarliestPO") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Due Date">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblDueDate" Text='<%#Eval("DueDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVendorNameValue" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OfficeDepotCode" SortExpression="PurchaseOrder.OD_Code">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblOD_CodeValue" Text='<%#Eval("PurchaseOrder.OD_Code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="PurchaseOrder.StockPlannerNo">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblStockPlannerNoValue" Text='<%#Eval("PurchaseOrder.StockPlannerNo") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StockPlannerName" SortExpression="PurchaseOrder.StockPlannerName">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblStockPlannerNameValue" Text='<%#Eval("PurchaseOrder.StockPlannerName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stock Planner Grouping" SortExpression="StockPlannerGrouping">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblStockPlannerGrouping" Text='<%#Eval("StockPlannerGrouping") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Sku Grouping" SortExpression="SkuGrouping">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSkuGrouping" Text='<%#Eval("SkuGrouping") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VendorCode" SortExpression="PurchaseOrder.Vendor_Code">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVendorCodeValue" Text='<%#Eval("PurchaseOrder.Vendor_Code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ItemClass" SortExpression="PurchaseOrder.Item_classification">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblItemClassificationValue" Text='<%#Eval("PurchaseOrder.Item_classification") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MRP">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblMRPValue" Text='<%#Eval("MRP") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PurGrp">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblPurGrpValue" Text='<%#Eval("PurGrp") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"/>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MatGrp">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblMatGrpValue" Text='<%#Eval("MatGrp") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DateLastUpdated" SortExpression="CommentDateLastUpdated">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblCommentDateLastUpdatedValue" Text='<%#Eval("CommentDateLastUpdated", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="UpdateValidTo" SortExpression="CommentUpdateValidTo">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblCommentUpdateValidToValue" Text='<%#Eval("CommentUpdateValidTo", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Days Stock" SortExpression="DaysStock">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblDaysStock" Text='<%# Eval("DaysStock") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lead Time" SortExpression="LeadTimeVariance">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblLeadTime" Text='<%# Eval("LeadTimeVariance") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="EarliestAdviseDate" SortExpression="EarliestAdviseDate">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lnkEarliestAdviseDateValue" runat="server" Target="_blank" Text='<%#Eval("EarliestAdviseDate", "{0:dd/MM/yyyy}") %>'
                                                        NavigateUrl='<%# EncryptQuery("ExpediteBookingDate.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&VendorID=" + Eval("Vendor.VendorID") +"&BookingDate=" + Eval("EarliestAdviseDate", "{0:MM/dd/yyyy}"))%>'></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" />
                                            </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="CBC" SortExpression="CB">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkCBValue" runat="server" Target="_blank" Text='<%# Eval("CB") %>'
                                                NavigateUrl='<%# EncryptQuery("~/ModuleUI/StockOverview/BackOrder/BackOrderHistory.aspx?SKUID=" + Eval("SKUID"))%>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="CBQ" DataField="CBQ" />
                                    <asp:TemplateField HeaderText="FB" SortExpression="FB">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFBValue" Text='<%#Eval("FB") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SOH" SortExpression="SOH">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkSOHValue" runat="server" CssClass="gridViewToolTipNew_SOH" Target="_blank" Text='<%# Eval("SOH") %>'
                                                NavigateUrl='<%# EncryptQuery("StockonHandOverview.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&VendorID=" + Eval("Vendor.VendorID") + "&ODSku=" + Eval("PurchaseOrder.OD_Code"))%>'></asp:HyperLink>
                                         <asp:Panel ID="pnlSOH" runat="server" CssClass="tooltipCommentSOH" style="display:none;">
                                              
                                                 <table id="tblDetailsMain" class="tblDetailsMain" cellpadding="15" cellspacing="5" style="width:300px; background-color:#F4B084;" >
                                                <tr>
                                                    <td style="width: 30%;">
                                                        <b>SKU</b>
                                                    </td>
                                                    <td style="width: 70%;">
                                                        <cc1:ucLabel ID="SKU" class="SKUSOH" runat="server" />
                                                    </td>
                                                </tr>
                                                    <tr>                                                        
                                                        <td>
                                                            <b>Viking Code</b>                                                           
                                                        </td>
                                                        <td>
                                                             <cc1:ucLabel ID="VikingCode" class="VikingCodeSOH" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td >
                                                            <b>Description</b>
                                                            
                                                        </td>
                                                        <td>
                                                            <cc1:ucLabel ID="Description" class="DescriptionSOH"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                              
                                                <table id="tblDetails" width="100%" border="0" cellpadding="15" cellspacing="5" style="width:300px; background-color:#F4B084;" class="tblDetails" >
                                                    
                                                </table>
                                            </asp:Panel>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"/>
                                    </asp:TemplateField>
                                    <%--Week Columns started from here...--%>
                                    <asp:TemplateField HeaderText="FirstWeekMon" SortExpression="FirstWeekMON">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFirstWeekMon" Text='<%# Eval("FirstWeekMON") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFirstWeekMONColor" Text='<%# Eval("FirstWeekMONColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FirstWeekTue" SortExpression="FirstWeekTUE">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFirstWeekTue" Text='<%# Eval("FirstWeekTUE") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFirstWeekTUEColor" Text='<%# Eval("FirstWeekTUEColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FirstWeekWed" SortExpression="FirstWeekWED">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFirstWeekWed" Text='<%# Eval("FirstWeekWED") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFirstWeekWEDColor" Text='<%# Eval("FirstWeekWEDColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"/>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FirstWeekThu" SortExpression="FirstWeekTHU">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFirstWeekThu" Text='<%# Eval("FirstWeekTHU") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFirstWeekTHUColor" Text='<%# Eval("FirstWeekTHUColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FirstWeekFri" SortExpression="Line_no">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFirstWeekFri" Text='<%# Eval("FirstWeekFri") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFirstWeekFRIColor" Text='<%# Eval("FirstWeekFRIColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SecondWeekMon" SortExpression="SecondWeekMON">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSecondWeekMon" Text='<%# Eval("SecondWeekMON") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblSecondWeekMONColor" Text='<%# Eval("SecondWeekMONColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"/>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SecondWeekTue" SortExpression="SecondWeekTUE">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSecondWeekTue" Text='<%# Eval("SecondWeekTUE") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblSecondWeekTUEColor" Text='<%# Eval("SecondWeekTUEColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SecondWeekWed" SortExpression="SecondWeekWED">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSecondWeekWed" Text='<%# Eval("SecondWeekWED") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblSecondWeekWEDColor" Text='<%# Eval("SecondWeekWEDColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SecondWeekThu" SortExpression="SecondWeekTHU">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSecondWeekThu" Text='<%# Eval("SecondWeekTHU") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblSecondWeekTHUColor" Text='<%# Eval("SecondWeekTHUColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SecondWeekFri" SortExpression="SecondWeekFRI">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSecondWeekFri" Text='<%# Eval("SecondWeekFRI") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblSecondWeekFRIColor" Text='<%# Eval("SecondWeekFRIColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ThirdWeekMon" SortExpression="ThirdWeekMON">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblThirdWeekMon" Text='<%# Eval("ThirdWeekMON") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblThirdWeekMONColor" Text='<%# Eval("ThirdWeekMONColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ThirdWeekTue" SortExpression="ThirdWeekTUE">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblThirdWeekTue" Text='<%# Eval("ThirdWeekTUE") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblThirdWeekTUEColor" Text='<%# Eval("ThirdWeekTUEColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ThirdWeekWed" SortExpression="ThirdWeekWED">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblThirdWeekWed" Text='<%# Eval("ThirdWeekWED") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblThirdWeekWEDColor" Text='<%# Eval("ThirdWeekWEDColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ThirdWeekThu" SortExpression="ThirdWeekTHU">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblThirdWeekThu" Text='<%# Eval("ThirdWeekTHU") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblThirdWeekTHUColor" Text='<%# Eval("ThirdWeekTHUColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ThirdWeekFri" SortExpression="ThirdWeekFRI">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblThirdWeekFri" Text='<%# Eval("ThirdWeekFRI") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblThirdWeekFRIColor" Text='<%# Eval("ThirdWeekFRIColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FourthWeekMon" SortExpression="FourthWeekMON">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFourthWeekMon" Text='<%# Eval("FourthWeekMON") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFourthWeekMONColor" Text='<%# Eval("FourthWeekMONColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FourthWeekTue" SortExpression="FourthWeekTUE">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFourthWeekTue" Text='<%# Eval("FourthWeekTUE") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFourthWeekTUEColor" Text='<%# Eval("FourthWeekTUEColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FourthWeekWed" SortExpression="FourthWeekWED">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFourthWeekWed" Text='<%# Eval("FourthWeekWED") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFourthWeekWEDColor" Text='<%# Eval("FourthWeekWEDColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FourthWeekThu" SortExpression="FourthWeekTHU">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFourthWeekThu" Text='<%# Eval("FourthWeekTHU") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFourthWeekTHUColor" Text='<%# Eval("FourthWeekTHUColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FourthWeekFri" SortExpression="FourthWeekFRI">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFourthWeekFri" Text='<%# Eval("FourthWeekFRI") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFourthWeekFRIColor" Text='<%# Eval("FourthWeekFRIColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </columns>
                                
                                
                            </cc1:ucGridView>
                      </div>
                        <br />
                    </div>  
                    </td>                     
                    </tr>
                    <tr>
                        <td>
                            <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false">
                            </cc1:PagerV2_8>
                            <div class="button-row">
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                            </div>
                        </td>
                    </tr>
                    </table>

                    <table id="tblPotentialOutputWeekWise" cellspacing="1" cellpadding="0" border="0" class="form-table" width="99%" runat="server">
                    <tr> <td>
                    <div>
                        <div>
                            <cc1:ucGridView ID="gvPotentialOutputWeekWise" OnRowDataBound="gvPotentialOutputWeekWise_OnRowDataBound"
                                runat="server" CssClass="grid gvclass searchgrid-1" GridLines="Both" AllowPaging="false"
                                PageSize="200">
                                <emptydatatemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblRecordNotFound" runat="server" Text="No record found"></cc1:ucLabel>
                                    </div>
                                </emptydatatemplate>
                                <columns>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" Visible="false"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="C" SortExpression="Comment">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Literal runat="server" ID="lblUpdatedDateValue" Text='<%# Eval("UpdatedDate") %>'
                                                Visible="false"></asp:Literal>
                                            <asp:Literal runat="server" ID="lblCommentColor" Visible="false" Text='<%#Eval("CommentColor") %>'></asp:Literal>
                                            <asp:Literal runat="server" ID="lblPriorityColor" Visible="false" Text='<%#Eval("PriorityItemColor") %>'></asp:Literal>
                                            <%-- <asp:HyperLink ID="lnkCommentValue"  runat="server"   Target="_blank" Text='<%# Eval("Comment") %>' ></asp:HyperLink>--%>
                                            <asp:HiddenField ID="hdnSKUID" runat="server" Value='<%# Eval("SKUID") %>' />
                                            <asp:HiddenField ID="hdnVendorId" runat="server" Value='<%# Eval("Vendor.VendorID") %>' />
                                            <asp:HiddenField ID="hdnDirectCode" runat="server" Value='<%# Eval("PurchaseOrder.Direct_code") %>' />
                                            <asp:HiddenField ID="hdnOdCode" runat="server" Value='<%# Eval("PurchaseOrder.OD_Code") %>' />
                                            <asp:HiddenField ID="hdnStatus" runat="server" Value='<%# Eval("Status") %>' />
                                            <asp:HiddenField ID="hdnDisc" runat="server" Value='<%# Eval("PurchaseOrder.Product_description") %>' />
                                            <asp:HiddenField ID="hdnSiteId" runat="server" Value='<%# Eval("Site.SiteID") %>' />
                                            <asp:HiddenField ID="hdnCountryID" runat="server" Value='<%# Eval("CountryID") %>' />
                                            <cc1:ucLabel ID="lblComntSKU" class="lblComntSKU" Text='<%# Eval("SKUID") %>' Style="display: none;"
                                                runat="server" />
                                                <cc1:ucLabel ID="lblOdCode" class="lblOdCode" Text='<%# Eval("PurchaseOrder.OD_Code") %>' Style="display: none;"
                                                runat="server" />
                                                <cc1:ucLabel ID="lblViking" class="lblViking" Text='<%# Eval("PurchaseOrder.Direct_code") %>' Style="display: none;"
                                                runat="server" />
                                                <cc1:ucLabel ID="lbldescription" class="lbldescription" Text='<%# Eval("PurchaseOrder.Product_description") %>' Style="display: none;"
                                                runat="server" />
                                            <asp:HyperLink ID="lnkCommentValue" CssClass="gridViewWeekWiseToolTipNew" runat="server"
                                                Target="_blank" NavigateUrl='<%# EncryptQuery("ExpediteComments.aspx?SiteID=" + Eval("Site.SiteID") 
                                                        + "&SKUID=" + Eval("SKUID") 
                                                        + "&VendorID=" + Eval("Vendor.VendorID")
                                                        + "&Viking=" + Eval("PurchaseOrder.Direct_code")
                                                        + "&ODCode=" + Eval("PurchaseOrder.OD_Code")
                                                        + "&Status=" + Eval("Status")
                                                        + "&Desc=" + Eval("PurchaseOrder.Product_description")
                                                        + "&IsSingleComment=1"
                                                        )%>'>
                                                        <img  src="../../../Images/info_button1.gif" />
                                                        </asp:HyperLink>
                                            <%-- //style="display:none;"--%>
                                            <asp:Panel ID="pnlToolComment" runat="server" CssClass="tooltipWeekWiseComment" Style="display: none;">
                                                <asp:HiddenField ID="hdncommentAddedDate" runat="server" Value='<%# Eval("CommentsAdded_Date") %>'>
                                                </asp:HiddenField>                                               
                                                <table cellpadding="15" cellspacing="5" style="width:300px; background-color:#F4B084;" >
                                                <tr>
                                                    <td style="width: 30%;">
                                                        <b>Date</b>
                                                    </td>
                                                    <td style="width: 70%;">
                                                        <cc1:uclabel id="CommentDate" class="commentDateWeekWiseTooltip" text='<%# Eval("CommentsAdded_Date", "{0:dd/MM/yyyy}") %>'
                                                            runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Added By</b>
                                                    </td>
                                                    <td>
                                                        <cc1:uclabel id="CommentAddedBy" class="commentAddedByWeekWiseTooltip" text='<%#Eval("CommentsAddedBy") %>'
                                                            runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Valid Till</b>
                                                    </td>
                                                    <td>
                                                        <cc1:uclabel id="CommentValid" class="commentValidWeekWisetooltip" text='<%# Eval("CommentsValid_Date", "{0:dd/MM/yyyy}")%>'
                                                            runat="server" />
                                                    </td>
                                                    <tr>
                                                        <td>
                                                            <b>Reason Code</b>
                                                        </td>
                                                        <td>
                                                            <cc1:uclabel id="CommentReason" class="commentReasonWeekWiseTooltip" text='<%#Eval("Reason") %>'
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <b><u>Comment</u> </b>
                                                        </td>
                                                        </tr>
                                                        <tr>
                                                        <td colspan="2">
                                                            <cc1:uclabel id="CommentText" class="commentTextWeekWiseTooltip" text='<%#Eval("Comments") %>'
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                            </table>                                               
                                            </asp:Panel>
                                     
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="S" SortExpression="Status">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <%--<cc1:ucLinkButton runat="server" ID="lnkStatusValue" CommandName="Status" Text='<%#Eval("Status") %>'></cc1:ucLinkButton>--%>
                                            <cc1:ucLabel runat="server" ID="lblStatusValue" Font-Bold="true" Text='<%#Eval("Status") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <%-- <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName" >
                                                <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblVendorNameValue" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Site" SortExpression="Site.SiteName">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSiteNameValue" Text='<%#Eval("Site.SiteName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <%--   <asp:TemplateField HeaderText="OfficeDepotCode" SortExpression="PurchaseOrder.OD_Code">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblOD_CodeValue" Text='<%#Eval("PurchaseOrder.OD_Code") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" />
                                            </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="VikingCode" SortExpression="PurchaseOrder.Direct_code">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVikingCodeValue" Text='<%#Eval("PurchaseOrder.Direct_code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PO" SortExpression="PurchaseOrder.Purchase_order">
                                       <%-- <HeaderStyle Width="30px" HorizontalAlign="Left" />--%>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lblPOValue" class="gridViewWeekWiseToolTip" runat="server" Target="_blank"
                                                Text='<%# Eval("POStatus") %>' NavigateUrl='<%# EncryptQuery("OpenPurchaseListing.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&OD_Code=" + Eval("PurchaseOrder.OD_Code")+ "&Status=" + Eval("Status"))%>'></asp:HyperLink>
                                        </ItemTemplate>
                                       <%-- <ItemStyle HorizontalAlign="Left" Width="30px" />--%>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description1" SortExpression="PurchaseOrder.Product_description">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblProductDesValue" Text='<%#Eval("PurchaseOrder.Product_description") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="SUBVNDR" SortExpression="subvndr">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblsubvndr" Text='<%#Eval("subvndr") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EarliestAdviseDate" SortExpression="EarliestAdviseDate">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkEarliestAdviseDateValue" runat="server" Target="_blank" Text='<%#Eval("EarliestAdviseDate", "{0:dd/MM/yyyy}") %>'
                                                NavigateUrl='<%# EncryptQuery("ExpediteBookingDate.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&VendorID=" + Eval("Vendor.VendorID") +"&BookingDate=" + Eval("EarliestAdviseDate", "{0:MM/dd/yyyy}"))%>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Earliest PO">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblEarliestPO" Text='<%#Eval("EarliestPO") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Due Date">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblDueDate" Text='<%#Eval("DueDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVendorNameValue" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OfficeDepotCode" SortExpression="PurchaseOrder.OD_Code">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblOD_CodeValue" Text='<%#Eval("PurchaseOrder.OD_Code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="PurchaseOrder.StockPlannerNo">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblStockPlannerNoValue" Text='<%#Eval("PurchaseOrder.StockPlannerNo") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StockPlannerName" SortExpression="PurchaseOrder.StockPlannerName">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblStockPlannerNameValue" Text='<%#Eval("PurchaseOrder.StockPlannerName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stock Planner Grouping" SortExpression="StockPlannerGrouping">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblStockPlannerGrouping" Text='<%#Eval("StockPlannerGrouping") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Sku Grouping" SortExpression="SkuGrouping">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSkuGrouping" Text='<%#Eval("SkuGrouping") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VendorCode" SortExpression="PurchaseOrder.Vendor_Code">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVendorCodeValue" Text='<%#Eval("PurchaseOrder.Vendor_Code") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ItemClass" SortExpression="PurchaseOrder.Item_classification">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblItemClassificationValue" Text='<%#Eval("PurchaseOrder.Item_classification") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MRP">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblMRPValue" Text='<%#Eval("MRP") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PurGrp">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblPurGrpValue" Text='<%#Eval("PurGrp") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"/>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MatGrp">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblMatGrpValue" Text='<%#Eval("MatGrp") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DateLastUpdated" SortExpression="CommentDateLastUpdated">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblCommentDateLastUpdatedValue" Text='<%#Eval("CommentDateLastUpdated", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="UpdateValidTo" SortExpression="CommentUpdateValidTo">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblCommentUpdateValidToValue" Text='<%#Eval("CommentUpdateValidTo", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Days Stock" SortExpression="DaysStock">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblDaysStock" Text='<%# Eval("DaysStock") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lead Time" SortExpression="LeadTimeVariance">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblLeadTime" Text='<%# Eval("LeadTimeVariance") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="EarliestAdviseDate" SortExpression="EarliestAdviseDate">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lnkEarliestAdviseDateValue" runat="server" Target="_blank" Text='<%#Eval("EarliestAdviseDate", "{0:dd/MM/yyyy}") %>'
                                                        NavigateUrl='<%# EncryptQuery("ExpediteBookingDate.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&VendorID=" + Eval("Vendor.VendorID") +"&BookingDate=" + Eval("EarliestAdviseDate", "{0:MM/dd/yyyy}"))%>'></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" />
                                            </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="CBC" SortExpression="CB">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkCBValue" runat="server" Target="_blank" Text='<%# Eval("CB") %>'
                                                NavigateUrl='<%# EncryptQuery("~/ModuleUI/StockOverview/BackOrder/BackOrderHistory.aspx?SKUID=" + Eval("SKUID"))%>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="CBQ" DataField="CBQ" />
                                    <asp:TemplateField HeaderText="FB" SortExpression="FB">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFBValue" Text='<%#Eval("FB") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SOH" SortExpression="SOH">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkSOHValue" runat="server" CssClass="gridViewWeekWiseToolTipNew_SOH" Target="_blank" Text='<%# Eval("SOH") %>'
                                                NavigateUrl='<%# EncryptQuery("StockonHandOverview.aspx?SiteID=" + Eval("Site.SiteID") + "&SKUID=" + Eval("SKUID") + "&VendorID=" + Eval("Vendor.VendorID") + "&ODSku=" + Eval("PurchaseOrder.OD_Code"))%>'></asp:HyperLink>
                                         <asp:Panel ID="pnlSOH" runat="server" CssClass="tooltipWeekWiseCommentSOH" style="display:none;">
                                              
                                                 <table id="tblDetailsMain" class="tblDetailsMain" cellpadding="15" cellspacing="5" style="width:300px; background-color:#F4B084;" >
                                                <tr>
                                                    <td style="width: 30%;">
                                                        <b>SKU</b>
                                                    </td>
                                                    <td style="width: 70%;">
                                                        <cc1:ucLabel ID="SKU" class="SKUSOH" runat="server" />
                                                    </td>
                                                </tr>
                                                    <tr>                                                        
                                                        <td>
                                                            <b>Viking Code</b>                                                           
                                                        </td>
                                                        <td>
                                                             <cc1:ucLabel ID="VikingCode" class="VikingCodeSOH" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td >
                                                            <b>Description</b>
                                                            
                                                        </td>
                                                        <td>
                                                            <cc1:ucLabel ID="Description" class="DescriptionSOH"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                              
                                                <table id="tblDetailsWeekWise" width="100%" border="0" cellpadding="15" cellspacing="5" style="width:300px; background-color:#F4B084;" class="tblDetailsWeekWise" >
                                                    
                                                </table>
                                            </asp:Panel>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"/>
                                    </asp:TemplateField>
                                    <%--Week Columns started from here...--%>
                                    <asp:TemplateField HeaderText="Week1" SortExpression="Week1">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFirstWeekMon" Text='<%# Eval("FirstWeekMON") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFirstWeekMONColor" Text='<%# Eval("FirstWeekMONColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week2" SortExpression="Week2">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFirstWeekTue" Text='<%# Eval("FirstWeekTUE") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFirstWeekTUEColor" Text='<%# Eval("FirstWeekTUEColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week3" SortExpression="Week431">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFirstWeekWed" Text='<%# Eval("FirstWeekWED") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFirstWeekWEDColor" Text='<%# Eval("FirstWeekWEDColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"/>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week4" SortExpression="Week4">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFirstWeekThu" Text='<%# Eval("FirstWeekTHU") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFirstWeekTHUColor" Text='<%# Eval("FirstWeekTHUColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week5" SortExpression="Week5">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFirstWeekFri" Text='<%# Eval("FirstWeekFri") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFirstWeekFRIColor" Text='<%# Eval("FirstWeekFRIColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week6" SortExpression="Week6">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSecondWeekMon" Text='<%# Eval("SecondWeekMON") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblSecondWeekMONColor" Text='<%# Eval("SecondWeekMONColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"/>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week7" SortExpression="Week7">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSecondWeekTue" Text='<%# Eval("SecondWeekTUE") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblSecondWeekTUEColor" Text='<%# Eval("SecondWeekTUEColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week8" SortExpression="Week8">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSecondWeekWed" Text='<%# Eval("SecondWeekWED") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblSecondWeekWEDColor" Text='<%# Eval("SecondWeekWEDColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week9" SortExpression="Week9">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSecondWeekThu" Text='<%# Eval("SecondWeekTHU") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblSecondWeekTHUColor" Text='<%# Eval("SecondWeekTHUColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week10" SortExpression="Week10">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSecondWeekFri" Text='<%# Eval("SecondWeekFRI") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblSecondWeekFRIColor" Text='<%# Eval("SecondWeekFRIColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week11" SortExpression="Week11">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblThirdWeekMon" Text='<%# Eval("ThirdWeekMON") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblThirdWeekMONColor" Text='<%# Eval("ThirdWeekMONColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week12" SortExpression="Week12">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblThirdWeekTue" Text='<%# Eval("ThirdWeekTUE") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblThirdWeekTUEColor" Text='<%# Eval("ThirdWeekTUEColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week13" SortExpression="Week13">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblThirdWeekWed" Text='<%# Eval("ThirdWeekWED") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblThirdWeekWEDColor" Text='<%# Eval("ThirdWeekWEDColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week14" SortExpression="Week14">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblThirdWeekThu" Text='<%# Eval("ThirdWeekTHU") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblThirdWeekTHUColor" Text='<%# Eval("ThirdWeekTHUColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week15" SortExpression="Week15">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblThirdWeekFri" Text='<%# Eval("ThirdWeekFRI") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblThirdWeekFRIColor" Text='<%# Eval("ThirdWeekFRIColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week16" SortExpression="Week16">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFourthWeekMon" Text='<%# Eval("FourthWeekMON") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFourthWeekMONColor" Text='<%# Eval("FourthWeekMONColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week17" SortExpression="Week17">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFourthWeekTue" Text='<%# Eval("FourthWeekTUE") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFourthWeekTUEColor" Text='<%# Eval("FourthWeekTUEColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week18" SortExpression="Week18">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFourthWeekWed" Text='<%# Eval("FourthWeekWED") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFourthWeekWEDColor" Text='<%# Eval("FourthWeekWEDColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week19" SortExpression="Week19">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFourthWeekThu" Text='<%# Eval("FourthWeekTHU") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFourthWeekTHUColor" Text='<%# Eval("FourthWeekTHUColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week20" SortExpression="Week20">
                                        <HeaderStyle  HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblFourthWeekFri" Text='<%# Eval("FourthWeekFRI") %>'></cc1:ucLabel>
                                            <asp:Literal runat="server" ID="lblFourthWeekFRIColor" Text='<%# Eval("FourthWeekFRIColor") %>'
                                                Visible="false"></asp:Literal>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </columns>
                                
                                
                            </cc1:ucGridView>
                      </div>
                        <br />
                    </div>  
                    </td>                     
                    </tr>
                    <tr>
                        <td>
                            <cc1:PagerV2_8 ID="pager2" runat="server" OnCommand="pager2_Command" GenerateGoToSection="false">
                            </cc1:PagerV2_8>
                            <div class="button-row" >
                                <cc1:ucButton ID="btnBackWeekWise" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                            </div>
                        </td>
                    </tr>
                    </table>
                </div>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

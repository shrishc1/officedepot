﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Data;
using System.Drawing;
using System.Web.Services;
using System.IO;
using System.Web.Script.Serialization;  

public partial class ModuleUI_StockOverview_SKUGroupingAddGroup : CommonPage
{
    protected string ErrorMaxlength500 = WebCommon.getGlobalResourceValue("ErrorMaxlength500");
    protected string ErrorGroupName = WebCommon.getGlobalResourceValue("ErrorGroupName");
    protected string SavedMesg = WebCommon.getGlobalResourceValue("SuccessMesg");
    protected string ErrorGroupNameUnique = WebCommon.getGlobalResourceValue("ErrorGroupNameUnique");
    protected string ErrorItemSearch = WebCommon.getGlobalResourceValue("ErrorItemSearch");
    protected string SKUgroupingADD = WebCommon.getGlobalResourceValue("SKUgroupingADD");
    protected string SkuGroupingsAdd = WebCommon.getGlobalResourceValue("SkuGroupingsAdd");   
    protected string SkuGroupingRemove = WebCommon.getGlobalResourceValue("SkuGroupingRemove");
    protected string CheckboxSelect = WebCommon.getGlobalResourceValue("CheckboxSelect");
    protected string SkuDetails = WebCommon.getGlobalResourceValue("SkuDetails");
    protected string SkuGroupingRemoveData = WebCommon.getGlobalResourceValue("SkuGroupingRemoveData");
    protected string SKUAlreadyError = WebCommon.getGlobalResourceValue("SKUAlreadyError");    
    
    int RecordCount = 0;

    
    protected void Page_Init(object sender, EventArgs e)
    {
        ucCountry.CurrentPage = this;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMesg.Text = string.Empty;
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = grdExport;
        ucExportToExcel1.FileName = "SkuGrouping";

        if (!IsPostBack)
        {
            BindGridviewSkuGrouping();
            hdnStatus.Value = string.Empty;            
            pager1.PageSize = 200;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;    
        }

        pager1.Visible = false;  
    }
    //private void BindDUmmyData()
    //{
    //    DataTable dummy = new DataTable();
    //    dummy.Columns.Add("SKUID");
    //    dummy.Columns.Add("SKU");
    //    dummy.Columns.Add("CatCode");
    //    dummy.Columns.Add("VendorCode");
    //    dummy.Columns.Add("DESCRIPTION");
    //    dummy.Rows.Add();
    //    UcGridView_AddRemove.DataSource = dummy;
    //    UcGridView_AddRemove.DataBind();
    //}
    private void BindGridviewSkuGrouping()
    {        
        ucExportToExcel1.Visible = true;
        btnAdd.Visible = true;
        lblSKUGrouping.Text = WebCommon.getGlobalResourceValue("SKUGrouping");
        SKUGroupingBE objSKUGroupingBE = new SKUGroupingBE();
        SKUGroupingBAL objSKUGroupingBAL = new SKUGroupingBAL();
        objSKUGroupingBE.Action = "BindGrouping";
        DataTable dt = objSKUGroupingBAL.SaveSkuGroupingBAL(objSKUGroupingBE);
        if (dt.Rows.Count > 0)
        {
            UcGridView1.DataSource = dt;
            UcGridView1.DataBind();
            grdExport.DataSource = dt;
            grdExport.DataBind();
            lblMesg.Text = string.Empty;
            UcGridView1.Width = new Unit("100%");
        }
        else
        {
            grdExport.DataSource = null;
            grdExport.DataBind();
            UcGridView1.DataSource = null;
            UcGridView1.DataBind();
            //lblMesg.Text = WebCommon.getGlobalResourceValue("RecordNotFound");
        }
    
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        lblMesg.Text = string.Empty;       
        SKUGroupingBE objSKUGroupingBE = new SKUGroupingBE();
        SKUGroupingBAL objSKUGroupingBAL = new SKUGroupingBAL();
        objSKUGroupingBE.Action = "ADDGrouping";
        objSKUGroupingBE.GroupName = txtGroupName.Text;
        objSKUGroupingBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedValue);
        if (hdnIsGroupUpdated.Value == "true")
        {
            objSKUGroupingBE.Isupdate = true;
            objSKUGroupingBE.SKugroupingID = Convert.ToInt32(hdnSKugroupingID.Value);
        }
        else
        {
            objSKUGroupingBE.Isupdate = false;
        }
       
        DataTable dt= objSKUGroupingBAL.SaveSkuGroupingBAL(objSKUGroupingBE);
        if (Convert.ToInt32(dt.Rows[0][0]) == -1)
        {
         //   lblMesg.Text = ErrorGroupNameUnique;
          //  lblMesg.ForeColor = Color.Red;
            tblGrid.Style.Add("display", "none");
            tblAddGroup.Style.Add("display", "block");
            tblAddRemove.Style.Add("display", "none");
            divSkuDetailsButton.Style.Add("display", "none");
            UcGridView1.Width = new Unit("100%");
            hdnStatus.Value = "true";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "myScript", "SKUGroupError();", true);
        }
        else
        {
           // lblMesg.ForeColor = Color.Red;
          //  lblMesg.Text = SavedMesg;
            tblGrid.Style.Add("display", "inline-table");
            tblAddGroup.Style.Add("display", "none");
            tblAddRemove.Style.Add("display", "none");
            divSkuDetailsButton.Style.Add("display", "block");
            UcGridView1.Width = new Unit("100%");
            hdnStatus.Value = "true";
            BindGridviewSkuGrouping();
        }

       
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        hdnStatus.Value = "";
        hdnAddRemoveStatus.Value = "";
        UcGridView_AddRemove.DataSource = null;
        UcGridView_AddRemove.DataBind();
        //BindDUmmyData();
        txtSKU.Text = string.Empty;
        txtCatCodeContains.Text = string.Empty;
        txtDesc.Text = string.Empty;
        BindGridviewSkuGrouping();
        tblGrid.Style.Add("display", "block");
        tblAddGroup.Style.Add("display", "none");
        tblAddRemove.Style.Add("display", "none");        
        divSkuDetailsButton.Style.Add("display", "block");
  
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        tblGrid.Style.Add("display","none");
        tblAddGroup.Style.Add("display","none");
        tblAddRemove.Style.Add("display", "inline-table");
        divSkuDetailsButton.Style.Add("display", "none");
        BindAddremoveOnSearchClick();
    }
    private void BindAddremoveOnSearchClick(int page=1)
    {
        UcGridView_AddRemove.Visible = true;
        lblMesg.Text = string.Empty;
        SKUGroupingBE objSKUGroupingBE = new SKUGroupingBE();
        SKUGroupingBAL objSKUGroupingBAL = new SKUGroupingBAL();
        if (hdnAddRemoveStatus.Value == "Add")
        {
            objSKUGroupingBE.Action = "SearchGrouping";
        }
        else
        {
            objSKUGroupingBE.Action = "GetRemoveSKU";
        }
        objSKUGroupingBE.ODSKU = txtSKU.Text;
        objSKUGroupingBE.VikingSKU = txtCatCodeContains.Text;
        objSKUGroupingBE.Description = txtDesc.Text;
        objSKUGroupingBE.SKugroupingID = Convert.ToInt32(hdnSKugroupingID.Value);
        txtGroupnameAddRemove.Text = hdnGroupNmae.Value;
        txtCountry.Text = hdnCountry.Value;
        bool check=true;
        if (hdnAddRemoveStatus.Value == "Add")
        {
            check=CheckSku();
        }
        if (check == true)
        {
            objSKUGroupingBE.PageCount = page;
            DataTable dt = objSKUGroupingBAL.SearchSkuGroupingBAL(objSKUGroupingBE, out RecordCount);
            if (dt.Rows.Count > 0)
            {
                pager1.ItemCount = RecordCount;
                pager1.Visible = true;
                UcGridView_AddRemove.Visible = true;
                UcGridView_AddRemove.DataSource = dt;
                UcGridView_AddRemove.DataBind();
                lblMesg.Text = string.Empty;
                if (hdnAddRemoveStatus.Value == "Add")
                {
                    btnADD_1.Style.Add("display", "block");
                    btnRemove.Style.Add("display", "none");
                }
                else
                {
                    btnADD_1.Style.Add("display", "none");
                    btnRemove.Style.Add("display", "block");
                }
            }
            else
            {
                UcGridView_AddRemove.DataSource = null;
                UcGridView_AddRemove.DataBind();
                if (hdnAddRemoveStatus.Value == "Add")
                {
                    btnADD_1.Style.Add("display", "none");
                    btnRemove.Style.Add("display", "none");
                }
                else
                {
                    btnADD_1.Style.Add("display", "none");
                    btnRemove.Style.Add("display", "none");
                }
            }
            divSkuDetailsButton.Style.Add("display", "none");
        }
        else
        {
            UcGridView_AddRemove.DataSource = null;
            UcGridView_AddRemove.DataBind();
            if (hdnAddRemoveStatus.Value == "Add")
            {
                btnADD_1.Style.Add("display", "none");
                btnRemove.Style.Add("display", "none");
            }
            else
            {
                btnADD_1.Style.Add("display", "none");
                btnRemove.Style.Add("display", "none");
            }
        }
    }
    protected void btnADD_1_Click(object sender, EventArgs e)
    {
        try
        {
            int count = 0;
            lblMesg.Text = string.Empty;
            DataTable dt = new DataTable();
            dt.Columns.Add("groupID", typeof(int));
            dt.Columns.Add("SKU", typeof(string));
            foreach (GridViewRow gvrow in UcGridView_AddRemove.Rows)
            {
                CheckBox chk = (CheckBox)gvrow.FindControl("chkSelect");
                Label lblSKU = (Label)gvrow.FindControl("lblSKU");                
                if (chk != null & chk.Checked)
                {
                    count = count+1;
                    dt.Rows.Add(Convert.ToInt32(hdnSKugroupingID.Value), lblSKU.Text);
                }
            }
            if (count == 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myScript", "SelectError();", true);
                return;
            }
            SKUGroupingBE objSKUGroupingBE = new SKUGroupingBE();
            SKUGroupingBAL objSKUGroupingBAL = new SKUGroupingBAL();
            objSKUGroupingBE.Action = "AddGroupingSKU";
            objSKUGroupingBE.Data = dt;
            objSKUGroupingBAL.ADDRemoveSkuGroupingBAL(objSKUGroupingBE, out RecordCount);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "myScript", "SaveMessage();", true);
            //lblMesg.ForeColor = Color.Red;
            //lblMesg.Text = SavedMesg;            
            btnADD_1.Style.Add("display", "none");
            UcGridView_AddRemove.Visible = false;
        }
        catch (Exception)
        {
            throw;
        }       
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        lblMesg.Text = string.Empty;
        DataTable dt = new DataTable();
        dt.Columns.Add("groupID", typeof(int));
        dt.Columns.Add("SKU", typeof(string));
        int count = 0;
        foreach (GridViewRow gvrow in UcGridView_AddRemove.Rows)
        {
            CheckBox chk = (CheckBox)gvrow.FindControl("chkSelect");
            Label lblSKU = (Label)gvrow.FindControl("lblSKU");
            if (chk != null & chk.Checked)
            {
                count = count + 1;
                dt.Rows.Add(Convert.ToInt32(hdnSKugroupingID.Value), lblSKU.Text);
            }
        }
        if (count == 0)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "myScript", "SelectError();", true);           
            return;
        }

        SKUGroupingBE objSKUGroupingBE = new SKUGroupingBE();
        SKUGroupingBAL objSKUGroupingBAL = new SKUGroupingBAL();
        objSKUGroupingBE.Action = "RemoveGroupingSKU";
        objSKUGroupingBE.Data = dt;
        objSKUGroupingBAL.ADDRemoveSkuGroupingBAL(objSKUGroupingBE, out RecordCount);
        //lblMesg.ForeColor = Color.Red;
        //lblMesg.Text = WebCommon.getGlobalResourceValue("SkuGroupingRemoveData");
        Page.ClientScript.RegisterStartupScript(this.GetType(), "myScript", "RemoveMessage();", true);
        if (RecordCount > 0)
        {
            BindAddremoveOnSearchClick();           
        }
        else
        {
            BindGridviewSkuGrouping();
            hdnAddRemoveStatus.Value = string.Empty;
            UcGridView_AddRemove.DataSource = null;
            UcGridView_AddRemove.DataBind();
            tblGrid.Style.Add("display", "inline-table");
            tblAddGroup.Style.Add("display", "none");
            tblAddRemove.Style.Add("display", "none");
            divSkuDetailsButton.Style.Add("display", "block");
            btnRemove.Style.Add("display", "none");
            btnADD_1.Style.Add("display", "none");           
        }
       
       
    }
   // [WebMethod]
    //public  void GetRemoveskuByGroup(int skugroupid)
    //{
    //    UcGridView_AddRemove.Visible = true;
    //    SKUGroupingBE objSKUGroupingBE = new SKUGroupingBE();
    //    SKUGroupingBAL objSKUGroupingBAL = new SKUGroupingBAL();
    //    objSKUGroupingBE.Action = "GetRemoveSKU";        
    //    objSKUGroupingBE.SKugroupingID = skugroupid;
    //    objSKUGroupingBE.ODSKU = string.Empty;
    //    objSKUGroupingBE.VikingSKU = string.Empty;
    //    objSKUGroupingBE.Description = string.Empty;
    //    objSKUGroupingBE.PageCount = Page;
    //    DataTable dt = objSKUGroupingBAL.SearchSkuGroupingBAL(objSKUGroupingBE, out RecordCount);
    //    if (dt.Rows.Count > 0)
    //    {
    //        pager1.ItemCount = RecordCount;
    //        pager1.Visible = true;
    //        UcGridView_AddRemove.DataSource = dt;
    //        UcGridView_AddRemove.DataBind();
    //    }
    //    else
    //    {
    //        UcGridView_AddRemove.DataSource = null;
    //        UcGridView_AddRemove.DataBind();
    //    }
    //    //MemoryStream str = new MemoryStream();
    //    //dt.WriteXml(str, true);
    //    //str.Seek(0, SeekOrigin.Begin);
    //    //StreamReader sr = new StreamReader(str);
    //    //string xmlstr;
    //    //xmlstr = sr.ReadToEnd();
    //    //return (xmlstr);
    //   // return DataTableToJSONWithJavaScriptSerializer(dt);       
    //}  
    protected void UcGridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkItems = (LinkButton)e.Row.FindControl("lnkItems");
            Label lablItem = (Label)e.Row.FindControl("lablItem");
            if (lablItem.Text == "0")
            {
                lnkItems.Enabled = false;              
            }
        }
    }
    public static string DataTableToJSONWithJavaScriptSerializer(DataTable table)
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
        Dictionary<string, object> childRow;
        foreach (DataRow row in table.Rows)
        {
            childRow = new Dictionary<string, object>();
            foreach (DataColumn col in table.Columns)
            {
                childRow.Add(col.ColumnName, row[col]);
            }
            parentRow.Add(childRow);
        }
        return jsSerializer.Serialize(parentRow);
    }
    //protected void UcGridView_AddRemove_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    UcGridView_AddRemove.PageIndex = e.NewPageIndex;
    //    UcGridView_AddRemove.DataBind();
    //}
    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindAddremoveOnSearchClick(currnetPageIndx);
    }    
    protected void UcGridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "items")
        {
            btnRemove.Style.Add("display", "block");
            btnADD_1.Style.Add("display", "none");
            tblGrid.Style.Add("display", "none");
            tblAddGroup.Style.Add("display", "none");
            tblAddRemove.Style.Add("display", "block");
            divSkuDetailsButton.Style.Add("display", "none");    
            lblSKUGrouping.Text=SkuGroupingRemove;       
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            txtGroupnameAddRemove.Text = commandArgs[1];
            txtCountry.Text = commandArgs[2];
            hdnGroupNmae.Value = commandArgs[1];
            hdnCountry.Value = commandArgs[2];
            hdnAddRemoveStatus.Value = "Remove";
            hdnSKugroupingID.Value = commandArgs[0];
            BindAddremoveOnSearchClick();
        }
        if (e.CommandName == "Group")
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            txtGroupName.Text = commandArgs[1];           
            hdnGroupNmae.Value = commandArgs[1];
            hdnCountry.Value = commandArgs[2];            
            hdnSKugroupingID.Value = commandArgs[0];
            hdnAddRemoveStatus.Value = "AddRemoveGroup";
            tblGrid.Style.Add("display", "none");
            tblAddGroup.Style.Add("display", "block");
            tblAddRemove.Style.Add("display", "none");
            btnRemove_1.Style.Add("display", "block");
            divSkuDetailsButton.Style.Add("display", "none");
            lblSKUGrouping.Text = SKUgroupingADD;
            hdnIsGroupUpdated.Value = "true";
            ucCountry.innerControlddlCountry.SelectedIndex = ucCountry.innerControlddlCountry.Items.IndexOf(ucCountry.innerControlddlCountry.Items.FindByText(commandArgs[2]));
        }
    }
    public bool CheckSku()
    {
        bool returnvalue=true;
        SKUGroupingBE objSKUGroupingBE = new SKUGroupingBE();
        SKUGroupingBAL objSKUGroupingBAL = new SKUGroupingBAL();
        objSKUGroupingBE.Action = "SKUGroupingCheck";  
        objSKUGroupingBE.ODSKU = txtSKU.Text;
        objSKUGroupingBE.VikingSKU = txtCatCodeContains.Text;
        objSKUGroupingBE.Description = txtDesc.Text;
        objSKUGroupingBE.SKugroupingID = Convert.ToInt32(hdnSKugroupingID.Value);
        txtGroupnameAddRemove.Text = hdnGroupNmae.Value;
        txtCountry.Text = hdnCountry.Value;
        DataTable dt = objSKUGroupingBAL.SearchSkuGroupingBAL(objSKUGroupingBE, out RecordCount);
        if (Convert.ToInt32(dt.Rows[0][0]) < 1)
        {
            returnvalue = false;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "myScript", "SKUError();", true);
            UcGridView_AddRemove.Visible = false;
            pager1.Visible = false;            
        }
        else
        {
            returnvalue=checkForexistingSKU();
        }
        return returnvalue;
    }
    protected void btnRemoveGroup_Click(object sender, EventArgs e)
    {
        SKUGroupingBE objSKUGroupingBE = new SKUGroupingBE();
        SKUGroupingBAL objSKUGroupingBAL = new SKUGroupingBAL();
        objSKUGroupingBE.Action = "RemoveGrouping";
        objSKUGroupingBE.SKugroupingID = Convert.ToInt32(hdnSKugroupingID.Value);       
        DataTable dt = objSKUGroupingBAL.SaveSkuGroupingBAL(objSKUGroupingBE);
        tblGrid.Style.Add("display", "block");
        tblAddGroup.Style.Add("display", "none");
        tblAddRemove.Style.Add("display", "none");
        divSkuDetailsButton.Style.Add("display", "block");
        UcGridView1.Width = new Unit("100%");
        hdnStatus.Value = "true";
        BindGridviewSkuGrouping();
    }
    public bool  checkForexistingSKU(){
        bool returnvalue = true;
        SKUGroupingBE objSKUGroupingBE = new SKUGroupingBE();
        SKUGroupingBAL objSKUGroupingBAL = new SKUGroupingBAL();
        objSKUGroupingBE.Action = "SkuAlreadyExist";
        objSKUGroupingBE.ODSKU = txtSKU.Text;
        objSKUGroupingBE.VikingSKU = txtCatCodeContains.Text;
        objSKUGroupingBE.Description = txtDesc.Text;
        objSKUGroupingBE.SKugroupingID = Convert.ToInt32(hdnSKugroupingID.Value);
        txtGroupnameAddRemove.Text = hdnGroupNmae.Value;
        txtCountry.Text = hdnCountry.Value;
        DataTable dt = objSKUGroupingBAL.SearchSkuGroupingBAL(objSKUGroupingBE, out RecordCount);
        if (Convert.ToInt32(dt.Rows[0][0]) > 0)
        {
            returnvalue = false;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "myScript", "SKUAlreadyError();", true);
            UcGridView_AddRemove.Visible = false;
            pager1.Visible = false;
        }
        return returnvalue;
    }

   

}
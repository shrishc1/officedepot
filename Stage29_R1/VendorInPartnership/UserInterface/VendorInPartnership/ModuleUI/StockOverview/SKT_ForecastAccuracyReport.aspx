﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="SKT_ForecastAccuracyReport.aspx.cs" Inherits="SKT_ForecastAccuracyReport" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectItemClassification.ascx" TagName="MultiSelectItemClassification"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectRMSCategory.ascx" TagName="MultiSelectRMSCategory"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script src="../../Scripts/jquery.tooltip.min.js" type="text/javascript"></script>
    <style type="text/css">
  
    .grid1{border:0px;background:#fff;}
    .grid1 th{background:url(../../Images/table-headbg.jpg) repeat-x top left #faf5ef;border-bottom:1px #e8e8e8;height:15px;font-weight:bold;font-size:9px;color:#934500;padding:3px 2px;vertical-align:top;}
    .grid1 th td{color:#934500!important;}
    .grid1 tr.altcolor{background:#F7F6F3 !important;}
    .grid1 tr.altcolor td{color:#333333;}
    .grid1 td{padding:3px 3px; font-family:arial;font-size:9px}
    .grid1 th:first-child{width:5px;} 
    .grid1 td:first-child{width:5px;}
    /*.grid td:first-child{display:none;}*/

    /*****************/
    </style>
    <style type="text/css">
       .wmd-view-topscroll, .wmd-view
        {
            overflow-x: auto;
            overflow-y: hidden;
            width: 960px;
        }
        
        .wmd-view-topscroll
        {
            height: 16px;
        }
        
        .dynamic-div
        {
            display: inline-block;
        }
     
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".wmd-view-topscroll").scroll(function () {
                $(".wmd-view")
            .scrollLeft($(".wmd-view-topscroll").scrollLeft());
            });

            $(".wmd-view").scroll(function () {
                $(".wmd-view-topscroll")
            .scrollLeft($(".wmd-view").scrollLeft());
            });
            topscroll();
        });

        function topscroll() {
           
            $(".dynamic-div div").css("float", "left");
            var scrollwidth = $(".dynamic-div > div").width();
            $('.scroll-div').css('width', scrollwidth + "px");
        }

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblForecastAccuracyReport1" runat="server" Text=""></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcDataPanel" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                           <%-- <asp:UpdatePanel ID="updVendor" runat="server">
                                <ContentTemplate>--%>
                                    <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                                <%--</ContentTemplate>
                            </asp:UpdatePanel>--%>
                            
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 89%">
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>
                   
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                     <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblCategory" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectRMSCategory runat="server" ID="MultiSelectRMSCategory1" />
                        </td>
                    </tr>
                  
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblItemClassification" runat="server" Text="Item Classification"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel10" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectItemClassification runat="server" ID="msItemClassification" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr style="height: 3em;">
                                    <td width="10%" style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblReportType" runat="server" Text="Report Type"></cc1:ucLabel>
                                    </td>
                                    <td width="1%" style="font-weight: bold;">
                                        <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td width="89%">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td  class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoShortTerm1" Checked="true" runat="server" Text="Short Term Forecast Accuracy " GroupName="ReportType"
                                                        onClick="checkFlag()" />
                                                </td>
                                                <td class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoLongTerm1" runat="server" Text="Long Term (6 Month Demand Detail/Forecast Accuracy)" GroupName="ReportType"
                                                        onClick="checkFlag()" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                </table>
                            </td>
                        </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnGenerateReport" runat="server"  Text="Generate Report" OnClick="btnGenerateReport_Click" CssClass="button"
                                   />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="Ucpnlgrid" runat="server">
                <div>&nbsp;</div>
                <div id="dvExport" runat="server" visible="false" style="float:right">
                   <table width="100%">
                       <tr>
                           <td>
                             <asp:Button ID="btexport" runat="server" CssClass="exporttoexcel" ToolTip="Export to Excel"  OnClick="btexport_Click" />
                           </td>
                       </tr>
                       <tr><td>&nbsp;</td></tr>
                   </table>
                </div>
                <div class="wmd-view-topscroll">
                                    <div class="scroll-div">
                                        &nbsp;
                                    </div>
                                </div>
               <div class="wmd-view">
                                    <div class="dynamic-div">
                  <asp:GridView ID="gdvAccuracyData"  Width="100%" runat="server"   CssClass="grid1"  OnRowDataBound="gdvAccuracyData_RowDataBound"
                          EmptyDataText="No Record Found"    ShowFooter="false" EmptyDataRowStyle-HorizontalAlign="Center" AutoGenerateColumns="false" >

                        <Columns>
                               <asp:TemplateField>
                                  <HeaderTemplate >
                                    <th colspan="9" >&nbsp;</th>
                                    <th colspan="4">YTD Values</th>
                                    <th colspan="4"><%=Month1%></th>
                                    <th colspan="4"><%=Month2%></th>
                                    <th colspan="4"><%=Month3%></th>
                                    <th colspan="4"><%=Month4%></th>
                                    <th colspan="4"><%=Month5%></th>
                                    <th colspan="4"><%=Month6%></th>
                                    <th colspan="4"><%=Month7%></th>
                                    <th colspan="4"><%=Month8%></th>
                                    <th colspan="4"><%=Month9%></th>
                                    <th colspan="4"><%=Month10%></th>
                                    <th colspan="4"><%=Month11%></th>
                                   <th colspan="4"><%=Month12%></th>
                                    <tr>
                                     <%-- <th>&nbsp;</th>--%>
                                      <th>Vendor</th>
                                      <th>Vendor Name</th>
                                      <th>Country</th>
                                      <th>OD SKU</th>
                                      <th>Viking SKU</th>
                                      <th>Description</th>
                                      <th>Item Classification</th>
                                      <th>Category</th>
                                      <th>Site</th>
                                     <th>Short Term (Next 20 days) forecast value</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                      <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                    <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                    <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                    <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                    <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                    </tr>
                                  </HeaderTemplate> 
                                  <ItemTemplate>
                                     <%# Eval("VendorNo") %>
                                    <td><%# Eval("VendorName")%></td>
                                    <td><%# Eval("Country")%></td>
                                    <td><%# Eval("OD_Sku")%></td>
                                    <td><%# Eval("Viking_Sku")%></td>
                                    <td><%# Eval("Description")%></td>
                                     <td><%# Eval("ItemClassification")%></td>
                                     <td><%# Eval("ItemCategory")%></td>
                                     <td><%# Eval("SiteName")%></td>
                                     <td><%# Eval("Next20DaysForecastValue")%></td> 
                                     <td><%# Eval("YTDForecastDemand")%></td>
                                     <td><%# Eval("YTDActualSale")%></td>
                                     <td><%# Eval("YTDAbsoluteDiff")%></td>
                                     <td><%# Eval("YTDForecastedAcuracyRate")%></td>
                                     <td><%# Eval("JanForecastDemand")%></td>
                                     <td><%# Eval("JanActualSale")%></td>
                                     <td><%# Eval("JanAbsoluteDiff")%></td>
                                     <td><%# Eval("JanForecastedAcuracyRate")%></td>
                                     <td><%# Eval("FebForecastDemand")%></td>
                                     <td><%# Eval("FebActualSale")%></td>
                                     <td><%# Eval("FebAbsoluteDiff")%></td>
                                     <td><%# Eval("FebForecastedAcuracyRate")%></td>
                                     <td><%# Eval("MarForecastDemand")%></td>
                                     <td><%# Eval("MarActualSale")%></td>
                                     <td><%# Eval("MarAbsoluteDiff")%></td>
                                     <td><%# Eval("MarForecastedAcuracyRate")%></td>
                                     <td><%# Eval("AprForecastDemand")%></td>
                                     <td><%# Eval("AprActualSale")%></td>
                                     <td><%# Eval("AprAbsoluteDiff")%></td>
                                     <td><%# Eval("AprForecastedAcuracyRate")%></td>
                                     <td><%# Eval("MayForecastDemand")%></td>
                                     <td><%# Eval("MayActualSale")%></td>
                                     <td><%# Eval("MayAbsoluteDiff")%></td>
                                     <td><%# Eval("MayForecastedAcuracyRate")%></td>
                                     <td><%# Eval("JunForecastDemand")%></td>
                                     <td><%# Eval("JunActualSale")%></td>
                                     <td><%# Eval("JunAbsoluteDiff")%></td>
                                     <td><%# Eval("JunForecastedAcuracyRate")%></td>
                                     <td><%# Eval("JulForecastDemand")%></td>
                                     <td><%# Eval("JulActualSale")%></td>
                                     <td><%# Eval("JulAbsoluteDiff")%></td>
                                     <td><%# Eval("JulForecastedAcuracyRate")%></td>
                                     <td><%# Eval("AugForecastDemand")%></td>
                                     <td><%# Eval("AugActualSale")%></td>
                                     <td><%# Eval("AugAbsoluteDiff")%></td>
                                     <td><%# Eval("AugForecastedAcuracyRate")%></td>

                                     <td><%# Eval("SepForecastDemand")%></td>
                                     <td><%# Eval("SepActualSale")%></td>
                                     <td><%# Eval("SepAbsoluteDiff")%></td>
                                     <td><%# Eval("SepForecastedAcuracyRate")%></td>

                                     <td><%# Eval("OctForecastDemand")%></td>
                                     <td><%# Eval("OctActualSale")%></td>
                                     <td><%# Eval("OctAbsoluteDiff")%></td>
                                     <td><%# Eval("OctForecastedAcuracyRate")%></td>

                                     <td><%# Eval("NovForecastDemand")%></td>
                                     <td><%# Eval("NovActualSale")%></td>
                                     <td><%# Eval("NovAbsoluteDiff")%></td>
                                     <td><%# Eval("NovForecastedAcuracyRate")%></td>

                                     <td><%# Eval("DecForecastDemand")%></td>
                                     <td><%# Eval("DecActualSale")%></td>
                                     <td><%# Eval("DecAbsoluteDiff")%></td>
                                     <td><%# Eval("DecForecastedAcuracyRate")%></td>
                                      
                                  </ItemTemplate>
                                   <FooterTemplate>
                                    <td colspan="9" align="right" ><strong>Average Accuracy Rate</strong></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblYtd" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblJan" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblFeb" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblMar" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblApr" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblMay" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblJun" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblJul" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblAug" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblSep" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblOct" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblNov" runat="server" ></asp:Label></td>
                                   <td colspan="4" align="right"><asp:Label ID="lblDec" runat="server" ></asp:Label></td>
                                   </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                     
                    </asp:GridView>
                     

                    <div>
                         <cc1:PagerV2_8 ID="pager1" runat="server" Visible="false"  OnCommand="pager1_Command" GenerateGoToSection="false">
                                </cc1:PagerV2_8>
                    </div>

                    <%-- <asp:GridView ID="gdvExportShortTerm" style="display:none"  OnRowDataBound="gdvExportShortTerm_RowDataBound"  Width="100%" runat="server"  CssClass="grid1" 
                          EmptyDataText="No Record Found"  ShowFooter="true"  EmptyDataRowStyle-HorizontalAlign="Center" AutoGenerateColumns="false" >
                          
                        <Columns>
                               <asp:TemplateField>
                                  <HeaderTemplate >
                                    <th colspan="9" >&nbsp;</th>
                                    <th colspan="4">YTD Values</th>
                                    <th colspan="4"><%=Month1%></th>
                                    <th colspan="4"><%=Month2%></th>
                                    <th colspan="4"><%=Month3%></th>
                                    <th colspan="4"><%=Month4%></th>
                                    <th colspan="4"><%=Month5%></th>
                                    <th colspan="4"><%=Month6%></th>
                                    <th colspan="4"><%=Month7%></th>
                                    <th colspan="4"><%=Month8%></th>
                                    <th colspan="4"><%=Month9%></th>
                                    <th colspan="4"><%=Month10%></th>
                                    <th colspan="4"><%=Month11%></th>
                                   <th colspan="4"><%=Month12%></th>
                                    <tr>                                    
                                      <th>Vendor</th>
                                      <th>Vendor Name</th>
                                      <th>Country</th>
                                      <th>OD SKU</th>
                                      <th>Viking SKU</th>
                                      <th>Description</th>
                                      <th>Item Classification</th>
                                      <th>Category</th>
                                      <th>Site</th>
                                     <th>Short Term (Next 20 days) forecast value</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                      <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                    <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                     <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                    <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                    <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                    <th>Forecasted Demand</th>
                                     <th>Actual Sales</th>
                                     <th>Absolute Difference</th>
                                     <th>Forecast Accuracy Rate</th>
                                    </tr>
                                  </HeaderTemplate> 
                                  <ItemTemplate>
                                     <%# Eval("VendorNo") %>
                                    <td><%# Eval("VendorName")%></td>
                                    <td><%# Eval("Country")%></td>
                                    <td><%# Eval("OD_Sku")%></td>
                                    <td><%# Eval("Viking_Sku")%></td>
                                    <td><%# Eval("Description")%></td>
                                     <td><%# Eval("ItemClassification")%></td>
                                     <td><%# Eval("ItemCategory")%></td>
                                     <td><%# Eval("SiteName")%></td>
                                     <td><%# Eval("Next20DaysForecastValue")%></td> 
                                     <td><%# Eval("YTDForecastDemand")%></td>
                                     <td><%# Eval("YTDActualSale")%></td>
                                     <td><%# Eval("YTDAbsoluteDiff")%></td>
                                     <td><%# Eval("YTDForecastedAcuracyRate")%></td>
                                     <td><%# Eval("JanForecastDemand")%></td>
                                     <td><%# Eval("JanActualSale")%></td>
                                     <td><%# Eval("JanAbsoluteDiff")%></td>
                                     <td><%# Eval("JanForecastedAcuracyRate")%></td>
                                     <td><%# Eval("FebForecastDemand")%></td>
                                     <td><%# Eval("FebActualSale")%></td>
                                     <td><%# Eval("FebAbsoluteDiff")%></td>
                                     <td><%# Eval("FebForecastedAcuracyRate")%></td>
                                     <td><%# Eval("MarForecastDemand")%></td>
                                     <td><%# Eval("MarActualSale")%></td>
                                     <td><%# Eval("MarAbsoluteDiff")%></td>
                                     <td><%# Eval("MarForecastedAcuracyRate")%></td>
                                     <td><%# Eval("AprForecastDemand")%></td>
                                     <td><%# Eval("AprActualSale")%></td>
                                     <td><%# Eval("AprAbsoluteDiff")%></td>
                                     <td><%# Eval("AprForecastedAcuracyRate")%></td>
                                     <td><%# Eval("MayForecastDemand")%></td>
                                     <td><%# Eval("MayActualSale")%></td>
                                     <td><%# Eval("MayAbsoluteDiff")%></td>
                                     <td><%# Eval("MayForecastedAcuracyRate")%></td>
                                     <td><%# Eval("JunForecastDemand")%></td>
                                     <td><%# Eval("JunActualSale")%></td>
                                     <td><%# Eval("JunAbsoluteDiff")%></td>
                                     <td><%# Eval("JunForecastedAcuracyRate")%></td>
                                     <td><%# Eval("JulForecastDemand")%></td>
                                     <td><%# Eval("JulActualSale")%></td>
                                     <td><%# Eval("JulAbsoluteDiff")%></td>
                                     <td><%# Eval("JulForecastedAcuracyRate")%></td>
                                     <td><%# Eval("AugForecastDemand")%></td>
                                     <td><%# Eval("AugActualSale")%></td>
                                     <td><%# Eval("AugAbsoluteDiff")%></td>
                                     <td><%# Eval("AugForecastedAcuracyRate")%></td>

                                     <td><%# Eval("SepForecastDemand")%></td>
                                     <td><%# Eval("SepActualSale")%></td>
                                     <td><%# Eval("SepAbsoluteDiff")%></td>
                                     <td><%# Eval("SepForecastedAcuracyRate")%></td>

                                     <td><%# Eval("OctForecastDemand")%></td>
                                     <td><%# Eval("OctActualSale")%></td>
                                     <td><%# Eval("OctAbsoluteDiff")%></td>
                                     <td><%# Eval("OctForecastedAcuracyRate")%></td>

                                     <td><%# Eval("NovForecastDemand")%></td>
                                     <td><%# Eval("NovActualSale")%></td>
                                     <td><%# Eval("NovAbsoluteDiff")%></td>
                                     <td><%# Eval("NovForecastedAcuracyRate")%></td>

                                     <td><%# Eval("DecForecastDemand")%></td>
                                     <td><%# Eval("DecActualSale")%></td>
                                     <td><%# Eval("DecAbsoluteDiff")%></td>
                                     <td><%# Eval("DecForecastedAcuracyRate")%></td>
                                      
                                  </ItemTemplate>
                                 <FooterTemplate>
                                    <td colspan="9" align="right" ><strong>Average Accuracy Rate</strong></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblYtd" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblJan" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblFeb" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblMar" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblApr" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblMay" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblJun" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblJul" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblAug" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblSep" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblOct" runat="server" ></asp:Label></td>
                                    <td colspan="4" align="right"><asp:Label ID="lblNov" runat="server" ></asp:Label></td>
                                   <td colspan="4" align="right"><asp:Label ID="lblDec" runat="server" ></asp:Label></td>
                                   </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                      
                    </asp:GridView>
--%>

                    <asp:GridView ID="gdvLongTerm" runat="server" Visible="false"  Width="100%" CssClass="grid1" 
                          EmptyDataText="No Record Found"   OnRowDataBound="gdvLongTerm_RowDataBound"  ShowFooter="false"  PageSize="50"  EmptyDataRowStyle-HorizontalAlign="Center" AutoGenerateColumns="false" >
                         <Columns>
                             <asp:TemplateField>
                                  <HeaderTemplate >
                                    <th colspan="8" >&nbsp;</th>
                                    <th colspan="4">Historic Values</th>
                                    <th colspan="13">Forecasted Demand</th>
                                    <tr>
                                      <th>Vendor</th>
                                      <th>Vendor Name</th>
                                      <th>Country</th>
                                      <th>OD SKU</th>
                                      <th>Viking SKU</th>
                                      <th>Description</th>
                                      <th>Item Classification</th>
                                      <th>Category</th>
                                      <th>Site</th>
                                     <th>Forecasted Demand(6 months)</th>
                                     <th>Actual Sales(6 months)</th>
                                     <th>Absolute Difference(6 months)</th>
                                     <th>Forecast Accuracy Rate(6 months)</th>
                                     <th>Next 12 Months Forecasted Demand</th>
                                     <th><%=Month1%></th>
                                     <th><%=Month2%></th>
                                     <th><%=Month3%></th>
                                     <th><%=Month4%></th>
                                     <th><%=Month5%></th>
                                     <th><%=Month6%></th>
                                     <th><%=Month7%></th>
                                     <th><%=Month8%></th>
                                     <th><%=Month9%></th>
                                     <th><%=Month10%></th>
                                     <th><%=Month11%></th>
                                     <th><%=Month12%></th>
                                    </tr>
                                  </HeaderTemplate> 
                                  <ItemTemplate>
                                     <%# Eval("VendorNo") %>
                                    <td><%# Eval("VendorName")%></td>
                                    <td><%# Eval("Country")%></td>
                                    <td><%# Eval("OD_Sku")%></td>
                                    <td><%# Eval("Viking_Sku")%></td>
                                    <td><%# Eval("Description")%></td>
                                     <td><%# Eval("ItemClassification")%></td>
                                     <td><%# Eval("ItemCategory")%></td>
                                     <td><%# Eval("SiteName")%></td>
                                     <td><%# Eval("HForecastDemand")%></td>
                                     <td><%# Eval("HActualSale")%></td>
                                     <td><%# Eval("HAbsoluteDiff")%></td>
                                     <td><%# Eval("HForecastedAcuracyRate")%></td>
                                     <td><%# Eval("Next12MonthForecastDemand")%></td>
                                     <td><%# Eval("Month1")%></td>
                                     <td><%# Eval("Month2")%></td>
                                     <td><%# Eval("Month3")%></td>
                                     <td><%# Eval("Month4")%></td>
                                     <td><%# Eval("Month5")%></td>
                                     <td><%# Eval("Month6")%></td>
                                     <td><%# Eval("Month7")%></td>
                                     <td><%# Eval("Month8")%></td>
                                     <td><%# Eval("Month9")%></td>
                                     <td><%# Eval("Month10")%></td>
                                     <td><%# Eval("Month11")%></td>
                                     <td><%# Eval("Month12")%></td> 
                                  </ItemTemplate>
                                 <FooterTemplate>
                                    <td colspan="8" align="right" ><strong>Average Accuracy Rate</strong></td>
                                    <td colspan="4"  align="right"><asp:Label ID="lblAvg" runat="server" ></asp:Label> </td>
                                    <td colspan="13">&nbsp;</td>
                                 </FooterTemplate>
                                </asp:TemplateField>
                         </Columns>
                    </asp:GridView>
                    <div>
                         <cc1:PagerV2_8 ID="pager2" OnCommand="pager2_Command" runat="server" Visible="false"   GenerateGoToSection="false">
                         </cc1:PagerV2_8>
                    </div>
                                         </div>
                </div>
                    <asp:GridView ID="gdvExportLongTerm" runat="server" style="display:none" Width="100%" CssClass="grid1" 
                          EmptyDataText="No Record Found" OnRowDataBound="gdvExportLongTerm_RowDataBound" ShowFooter="true" EmptyDataRowStyle-HorizontalAlign="Center" AutoGenerateColumns="false" >
                         <Columns>
                             <asp:TemplateField>
                                  <HeaderTemplate >
                                    <th colspan="8" >&nbsp;</th>
                                    <th colspan="4">Historic Values</th>
                                    <th colspan="13">Forecasted Demand</th>
                                    <tr>
                                      <th>Vendor</th>
                                      <th>Vendor Name</th>
                                      <th>Country</th>
                                      <th>OD SKU</th>
                                      <th>Viking SKU</th>
                                      <th>Description</th>
                                      <th>Item Classification</th>
                                      <th>Category</th>
                                      <th>Site</th>
                                     <th>Forecasted Demand(6 months)</th>
                                     <th>Actual Sales(6 months)</th>
                                     <th>Absolute Difference(6 months)</th>
                                     <th>Forecast Accuracy Rate(6 months)</th>
                                     <th>Next 12 Months Forecasted Demand</th>
                                     <th><%=Month1%></th>
                                     <th><%=Month2%></th>
                                     <th><%=Month3%></th>
                                     <th><%=Month4%></th>
                                     <th><%=Month5%></th>
                                     <th><%=Month6%></th>
                                     <th><%=Month7%></th>
                                     <th><%=Month8%></th>
                                     <th><%=Month9%></th>
                                     <th><%=Month10%></th>
                                     <th><%=Month11%></th>
                                     <th><%=Month12%></th>
                                    </tr>
                                  </HeaderTemplate> 
                                  <ItemTemplate>
                                     <%# Eval("VendorNo") %>
                                    <td><%# Eval("VendorName")%></td>
                                    <td><%# Eval("Country")%></td>
                                    <td><%# Eval("OD_Sku")%></td>
                                    <td><%# Eval("Viking_Sku")%></td>
                                    <td><%# Eval("Description")%></td>
                                     <td><%# Eval("ItemClassification")%></td>
                                     <td><%# Eval("ItemCategory")%></td>
                                     <td><%# Eval("SiteName")%></td>
                                     <td><%# Eval("HForecastDemand")%></td>
                                     <td><%# Eval("HActualSale")%></td>
                                     <td><%# Eval("HAbsoluteDiff")%></td>
                                     <td><%# Eval("HForecastedAcuracyRate")%></td>
                                     <td><%# Eval("Next12MonthForecastDemand")%></td>
                                     <td><%# Eval("Month1")%></td>
                                     <td><%# Eval("Month2")%></td>
                                     <td><%# Eval("Month3")%></td>
                                     <td><%# Eval("Month4")%></td>
                                     <td><%# Eval("Month5")%></td>
                                     <td><%# Eval("Month6")%></td>
                                     <td><%# Eval("Month7")%></td>
                                     <td><%# Eval("Month8")%></td>
                                     <td><%# Eval("Month9")%></td>
                                     <td><%# Eval("Month10")%></td>
                                     <td><%# Eval("Month11")%></td>
                                     <td><%# Eval("Month12")%></td> 
                                  </ItemTemplate>
                                  <FooterTemplate>
                                    <td colspan="8" align="right" ><strong>Average Accuracy Rate</strong></td>
                                    <td colspan="4"  align="right"><asp:Label ID="lblAvg" runat="server" ></asp:Label> </td>
                                    <td colspan="13">&nbsp;</td>
                                 </FooterTemplate>
                                </asp:TemplateField>
                         </Columns>
                    </asp:GridView>
                
                <div>&nbsp;</div>
                <div id="dvback" runat="server" style="float:right">
                   
                   <cc1:ucButton ID="btBack"  Visible="false" CssClass="button" OnClick="btBack_Click"  runat="server" Text="Back"/>
                      
                    </div>
                 <div>&nbsp;</div>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>


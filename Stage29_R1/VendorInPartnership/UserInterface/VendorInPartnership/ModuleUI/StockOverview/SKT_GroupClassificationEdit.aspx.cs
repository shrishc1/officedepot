﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Collections.Generic;
using System.Linq;
using Utilities;
using WebUtilities;


public partial class SKT_GroupClassificationEdit : CommonPage
{
    int? iDisResult;
    int? iPenDisResult;
    int? iReviewResult;

    protected string DiscontinuedMessage = WebCommon.getGlobalResourceValue("DiscontinuedMessage");
    protected string PendingDiscontinuedMessage = WebCommon.getGlobalResourceValue("PendingDiscontinuedMessage");
    protected string UnderReviewMessage = WebCommon.getGlobalResourceValue("UnderReviewMessage");
    protected string GroupClassificationErrorMsg = WebCommon.getGlobalResourceValue("GroupClassificationErrorMsg");    

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

        }
    }

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucSite.CurrentPage = this;
    }
    #region Methods


    public override void SitePost_Load()
    {
        base.SitePost_Load();
        if (!IsPostBack)
        {
            ucSite.innerControlddlSite.SelectedIndex = 0;
            ucSite.innerControlddlSite.AutoPostBack = true;
            getGroupClassification();
        }
    }
    public override void SiteSelectedIndexChanged()
    {
        getGroupClassification();
        txtDescriptionEnding.Text = string.Empty;

    }


    #endregion

    #region Events
    protected void btnSave_Click(object sender, EventArgs e)
    {

        MASSIT_GroupClassificationBAL oMASSIT_GroupClassificationBAL = new MASSIT_GroupClassificationBAL();
        MASSIT_GroupClassificationBE oMASSIT_GroupClassificationBE = new MASSIT_GroupClassificationBE();



        oMASSIT_GroupClassificationBE.Action = "Add";
        oMASSIT_GroupClassificationBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);

        //For adding Discontinued values
        if (UclstDiscontinued.Items.Count > 0)
        {
            if (UclstDiscontinued.Items.Count > 1)
            {
                //oMASSIT_GroupClassificationBE.SKUEndingWith = UclstDiscontinued.Items[0].Text + ",";
                //oMASSIT_GroupClassificationBE.SKUStartWith = UclstDiscontinued.Items[0].Text + ",";
                //oMASSIT_GroupClassificationBE.SKUContain = UclstDiscontinued.Items[0].Text + ",";
                for (int i = 0; i < UclstDiscontinued.Items.Count; i++)
                {
                    string[] arrayList = UclstDiscontinued.Items[i].Text.Split(' ');

                    if (arrayList.Length == 1)
                    {
                        oMASSIT_GroupClassificationBE.SKUEndingWith += UclstDiscontinued.Items[i].Text.Split(' ').ElementAtOrDefault(0) + ",";
                    }
                    else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(begins)")
                    {
                        oMASSIT_GroupClassificationBE.SKUStartWith += UclstDiscontinued.Items[i].Text.Split(' ').ElementAtOrDefault(0) + ",";
                    }
                    else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(ends)")
                    {
                        oMASSIT_GroupClassificationBE.SKUEndingWith += UclstDiscontinued.Items[i].Text.Split(' ').ElementAtOrDefault(0) + ",";
                    }
                    else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(contains)")
                    {
                        oMASSIT_GroupClassificationBE.SKUContain += UclstDiscontinued.Items[i].Text.Split(' ').ElementAtOrDefault(0) + ",";
                    }

                }


                if (!string.IsNullOrEmpty(oMASSIT_GroupClassificationBE.SKUEndingWith))
                {
                    oMASSIT_GroupClassificationBE.SKUEndingWith = oMASSIT_GroupClassificationBE.SKUEndingWith.Substring(0, oMASSIT_GroupClassificationBE.SKUEndingWith.Length - 1);
                }

                if (!string.IsNullOrEmpty(oMASSIT_GroupClassificationBE.SKUStartWith))
                {
                    oMASSIT_GroupClassificationBE.SKUStartWith = oMASSIT_GroupClassificationBE.SKUStartWith.Substring(0, oMASSIT_GroupClassificationBE.SKUStartWith.Length - 1);
                }

                if (!string.IsNullOrEmpty(oMASSIT_GroupClassificationBE.SKUContain))
                {
                    oMASSIT_GroupClassificationBE.SKUContain = oMASSIT_GroupClassificationBE.SKUContain.Substring(0, oMASSIT_GroupClassificationBE.SKUContain.Length - 1);
                }

            }
            else
            {
                string[] arrayList = UclstDiscontinued.Items[0].Text.Split(' ');
                if (arrayList.Length == 1)
                {
                    oMASSIT_GroupClassificationBE.SKUEndingWith += UclstDiscontinued.Items[0].Text.Split(' ').ElementAtOrDefault(0);
                }
                else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(begins)")
                {
                    oMASSIT_GroupClassificationBE.SKUStartWith += UclstDiscontinued.Items[0].Text.Split(' ').ElementAtOrDefault(0);
                }
                else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(ends)")
                {
                    oMASSIT_GroupClassificationBE.SKUEndingWith += UclstDiscontinued.Items[0].Text.Split(' ').ElementAtOrDefault(0);
                }
                else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(contains)")
                {
                    oMASSIT_GroupClassificationBE.SKUContain += UclstDiscontinued.Items[0].Text.Split(' ').ElementAtOrDefault(0);
                }
                //oMASSIT_GroupClassificationBE.SKUEndingWith = UclstDiscontinued.Items[0].Text.Split(' ').ElementAtOrDefault(0);
                //oMASSIT_GroupClassificationBE.SKUStartWith = UclstDiscontinued.Items[0].Text.Split(' ').ElementAtOrDefault(0);
                //oMASSIT_GroupClassificationBE.SKUContain = UclstDiscontinued.Items[0].Text.Split(' ').ElementAtOrDefault(0);
            }

            oMASSIT_GroupClassificationBE.ReviewStatus = 'D';
            iDisResult = oMASSIT_GroupClassificationBAL.addEditGroupClassificationBAL(oMASSIT_GroupClassificationBE);

        }

        //For adding PendingDiscontinued values
        oMASSIT_GroupClassificationBAL = new MASSIT_GroupClassificationBAL();
        oMASSIT_GroupClassificationBE = new MASSIT_GroupClassificationBE();
        if (UclstPenDiscontinued.Items.Count > 0)
        {
            if (UclstPenDiscontinued.Items.Count > 1)
            {
                //oMASSIT_GroupClassificationBE.SKUEndingWith = UclstPenDiscontinued.Items[0].Text + ",";
                //oMASSIT_GroupClassificationBE.SKUStartWith = UclstPenDiscontinued.Items[0].Text + ",";
                //oMASSIT_GroupClassificationBE.SKUEndingWith = UclstPenDiscontinued.Items[0].Text + ",";

                for (int i = 0; i < UclstPenDiscontinued.Items.Count; i++)
                {

                    string[] arrayList = UclstPenDiscontinued.Items[i].Text.Split(' ');

                    if (arrayList.Length == 1)
                    {
                        oMASSIT_GroupClassificationBE.SKUEndingWith += UclstPenDiscontinued.Items[i].Text.Split(' ').ElementAtOrDefault(0) + ",";
                    }
                    else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(begins)")
                    {
                        oMASSIT_GroupClassificationBE.SKUStartWith += UclstPenDiscontinued.Items[i].Text.Split(' ').ElementAtOrDefault(0) + ",";
                    }
                    else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(ends)")
                    {
                        oMASSIT_GroupClassificationBE.SKUEndingWith += UclstPenDiscontinued.Items[i].Text.Split(' ').ElementAtOrDefault(0) + ",";
                    }
                    else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(contains)")
                    {
                        oMASSIT_GroupClassificationBE.SKUContain += UclstPenDiscontinued.Items[i].Text.Split(' ').ElementAtOrDefault(0) + ",";
                    }
                }

                if (!string.IsNullOrEmpty(oMASSIT_GroupClassificationBE.SKUEndingWith))
                {
                    oMASSIT_GroupClassificationBE.SKUEndingWith = oMASSIT_GroupClassificationBE.SKUEndingWith.Substring(0, oMASSIT_GroupClassificationBE.SKUEndingWith.Length - 1);
                }

                if (!string.IsNullOrEmpty(oMASSIT_GroupClassificationBE.SKUStartWith))
                {
                    oMASSIT_GroupClassificationBE.SKUStartWith = oMASSIT_GroupClassificationBE.SKUStartWith.Substring(0, oMASSIT_GroupClassificationBE.SKUStartWith.Length - 1);
                }

                if (!string.IsNullOrEmpty(oMASSIT_GroupClassificationBE.SKUContain))
                {
                    oMASSIT_GroupClassificationBE.SKUContain = oMASSIT_GroupClassificationBE.SKUContain.Substring(0, oMASSIT_GroupClassificationBE.SKUContain.Length - 1);
                }
                // oMASSIT_GroupClassificationBE.SKUEndingWith += UclstPenDiscontinued.Items[UclstPenDiscontinued.Items.Count - 1].Text;
            }
            else
            {
                string[] arrayList = UclstPenDiscontinued.Items[0].Text.Split(' ');

                if (arrayList.Length == 1)
                {
                    oMASSIT_GroupClassificationBE.SKUEndingWith += UclstPenDiscontinued.Items[0].Text.Split(' ').ElementAtOrDefault(0) + ",";
                }
                else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(begins)")
                {
                    oMASSIT_GroupClassificationBE.SKUStartWith += UclstPenDiscontinued.Items[0].Text.Split(' ').ElementAtOrDefault(0) + ",";
                }
                else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(ends)")
                {
                    oMASSIT_GroupClassificationBE.SKUEndingWith += UclstPenDiscontinued.Items[0].Text.Split(' ').ElementAtOrDefault(0) + ",";
                }
                else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(contains)")
                {
                    oMASSIT_GroupClassificationBE.SKUContain += UclstPenDiscontinued.Items[0].Text.Split(' ').ElementAtOrDefault(0) + ",";
                }

                //oMASSIT_GroupClassificationBE.SKUEndingWith = UclstPenDiscontinued.Items[0].Text;
                //oMASSIT_GroupClassificationBE.SKUStartWith = UclstPenDiscontinued.Items[0].Text;
                //oMASSIT_GroupClassificationBE.SKUContain = UclstPenDiscontinued.Items[0].Text;
            }

            oMASSIT_GroupClassificationBE.ReviewStatus = 'P';
            oMASSIT_GroupClassificationBE.Action = "Add";
            oMASSIT_GroupClassificationBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
            iPenDisResult = oMASSIT_GroupClassificationBAL.addEditGroupClassificationBAL(oMASSIT_GroupClassificationBE);
        }

        //For adding Under Review values
        oMASSIT_GroupClassificationBAL = new MASSIT_GroupClassificationBAL();
        oMASSIT_GroupClassificationBE = new MASSIT_GroupClassificationBE();
        if (UclstUnderReview.Items.Count > 0)
        {
            if (UclstUnderReview.Items.Count > 1)
            {

                for (int i = 0; i < UclstUnderReview.Items.Count; i++)
                {
                    string[] arrayList = UclstUnderReview.Items[i].Text.Split(' ');

                    if (arrayList.Length == 1)
                    {
                        oMASSIT_GroupClassificationBE.SKUEndingWith += UclstUnderReview.Items[i].Text.Split(' ').ElementAtOrDefault(0) + ",";
                    }
                    else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(begins)")
                    {
                        oMASSIT_GroupClassificationBE.SKUStartWith += UclstUnderReview.Items[i].Text.Split(' ').ElementAtOrDefault(0) + ",";
                    }
                    else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(ends)")
                    {
                        oMASSIT_GroupClassificationBE.SKUEndingWith += UclstUnderReview.Items[i].Text.Split(' ').ElementAtOrDefault(0) + ",";
                    }
                    else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(contains)")
                    {
                        oMASSIT_GroupClassificationBE.SKUContain += UclstUnderReview.Items[i].Text.Split(' ').ElementAtOrDefault(0) + ",";
                    }

                    // oMASSIT_GroupClassificationBE.SKUEndingWith += UclstUnderReview.Items[i].Text + ",";
                }
                if (!string.IsNullOrEmpty(oMASSIT_GroupClassificationBE.SKUEndingWith))
                {
                    oMASSIT_GroupClassificationBE.SKUEndingWith = oMASSIT_GroupClassificationBE.SKUEndingWith.Substring(0, oMASSIT_GroupClassificationBE.SKUEndingWith.Length - 1);
                }

                if (!string.IsNullOrEmpty(oMASSIT_GroupClassificationBE.SKUStartWith))
                {
                    oMASSIT_GroupClassificationBE.SKUStartWith = oMASSIT_GroupClassificationBE.SKUStartWith.Substring(0, oMASSIT_GroupClassificationBE.SKUStartWith.Length - 1);
                }

                if (!string.IsNullOrEmpty(oMASSIT_GroupClassificationBE.SKUContain))
                {
                    oMASSIT_GroupClassificationBE.SKUContain = oMASSIT_GroupClassificationBE.SKUContain.Substring(0, oMASSIT_GroupClassificationBE.SKUContain.Length - 1);
                }
                // oMASSIT_GroupClassificationBE.SKUEndingWith += UclstUnderReview.Items[UclstUnderReview.Items.Count - 1].Text;
            }
            else
            {

                string[] arrayList = UclstUnderReview.Items[0].Text.Split(' ');

                if (arrayList.Length == 1)
                {
                    oMASSIT_GroupClassificationBE.SKUEndingWith += UclstUnderReview.Items[0].Text.Split(' ').ElementAtOrDefault(0) + ",";
                }
                else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(begins)")
                {
                    oMASSIT_GroupClassificationBE.SKUStartWith += UclstUnderReview.Items[0].Text.Split(' ').ElementAtOrDefault(0) + ",";
                }
                else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(ends)")
                {
                    oMASSIT_GroupClassificationBE.SKUEndingWith += UclstUnderReview.Items[0].Text.Split(' ').ElementAtOrDefault(0) + ",";
                }
                else if (arrayList.Length == 2 && Convert.ToString(arrayList[1]) == "(contains)")
                {
                    oMASSIT_GroupClassificationBE.SKUContain += UclstUnderReview.Items[0].Text.Split(' ').ElementAtOrDefault(0) + ",";
                }

                //oMASSIT_GroupClassificationBE.SKUEndingWith = UclstUnderReview.Items[0].Text;
                //oMASSIT_GroupClassificationBE.SKUStartWith = UclstUnderReview.Items[0].Text;
                //oMASSIT_GroupClassificationBE.SKUEndingWith = UclstUnderReview.Items[0].Text;
            }

            oMASSIT_GroupClassificationBE.ReviewStatus = 'R';
            oMASSIT_GroupClassificationBE.Action = "Add";
            oMASSIT_GroupClassificationBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
            iReviewResult = oMASSIT_GroupClassificationBAL.addEditGroupClassificationBAL(oMASSIT_GroupClassificationBE);
        }

        if (UclstDiscontinued.Items.Count == 0 && UclstPenDiscontinued.Items.Count == 0 && UclstUnderReview.Items.Count == 0)
        {
            iReviewResult = 1;
            oMASSIT_GroupClassificationBAL = new MASSIT_GroupClassificationBAL();
            oMASSIT_GroupClassificationBE = new MASSIT_GroupClassificationBE();
            oMASSIT_GroupClassificationBE.Action = "Add";
            oMASSIT_GroupClassificationBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedItem.Value);
            iReviewResult = oMASSIT_GroupClassificationBAL.addEditGroupClassificationBAL(oMASSIT_GroupClassificationBE);
        }
        oMASSIT_GroupClassificationBAL = null;
        if (iDisResult == 0 || iPenDisResult == 0 || iReviewResult == 0)
        {
            string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            //clearcontrols();
        }



    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        MASSIT_GroupClassificationBAL oMASSIT_GroupClassificationBAL = new MASSIT_GroupClassificationBAL();
        MASSIT_GroupClassificationBE oMASSIT_GroupClassificationBE = new MASSIT_GroupClassificationBE();
        oMASSIT_GroupClassificationBE.Action = "Delete";
        oMASSIT_GroupClassificationBE.SiteID = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);
        int? iResult = oMASSIT_GroupClassificationBAL.deleteGroupClassificationBAL(oMASSIT_GroupClassificationBE);
        oMASSIT_GroupClassificationBAL = null;
        if (iResult == 0)
        {
            string saveMessage = WebCommon.getGlobalResourceValue("DeleteConfirmed");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            clearcontrols();
        }




    }
    protected void btnDiscontinued_Click(object sender, EventArgs e)
    {
        if (txtDescriptionEnding.Text.Trim() != null)
        {
            if (rdoBeginsWith.Checked)
            {
                UclstDiscontinued.Items.Add(txtDescriptionEnding.Text.Trim() + " (begins)");
            }
            else if (rdoEndsWIth.Checked)
            {
                UclstDiscontinued.Items.Add(txtDescriptionEnding.Text.Trim() + " (ends)");
            }
            else if (rdoCOntains.Checked)
            {
                UclstDiscontinued.Items.Add(txtDescriptionEnding.Text.Trim() + " (contains)");
            }

            txtDescriptionEnding.Text = string.Empty;
        }
    }
    protected void btnPendingDiscontinued_Click(object sender, EventArgs e)
    {
        if (txtDescriptionEnding.Text.Trim() != null)
        {
            if (rdoBeginsWith.Checked)
            {
                UclstPenDiscontinued.Items.Add(txtDescriptionEnding.Text.Trim() + " (begins)");
            }
            else if (rdoEndsWIth.Checked)
            {
                UclstPenDiscontinued.Items.Add(txtDescriptionEnding.Text.Trim() + " (ends)");
            }
            else if (rdoCOntains.Checked)
            {
                UclstPenDiscontinued.Items.Add(txtDescriptionEnding.Text.Trim() + " (contains)");
            }



            txtDescriptionEnding.Text = string.Empty;
        }
    }
    protected void btnUnderReview_Click(object sender, EventArgs e)
    {
        if (txtDescriptionEnding.Text.Trim() != null)
        {

            if (rdoBeginsWith.Checked)
            {
                UclstUnderReview.Items.Add(txtDescriptionEnding.Text.Trim() + " (begins)");
            }
            else if (rdoEndsWIth.Checked)
            {
                UclstUnderReview.Items.Add(txtDescriptionEnding.Text.Trim() + " (ends)");
            }
            else if (rdoCOntains.Checked)
            {
                UclstUnderReview.Items.Add(txtDescriptionEnding.Text.Trim() + " (contains)");
            }

            txtDescriptionEnding.Text = string.Empty;
        }
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        if (UclstDiscontinued.SelectedItem != null)
        {
            FillControls.RemoveSelectedItem(UclstDiscontinued);
        }
    }
    protected void btnRemove_1_Click(object sender, EventArgs e)
    {
        if (UclstPenDiscontinued.SelectedItem != null)
        {
            FillControls.RemoveSelectedItem(UclstPenDiscontinued);
        }
    }
    protected void btnRemove_2_Click(object sender, EventArgs e)
    {
        if (UclstUnderReview.SelectedItem != null)
        {
            FillControls.RemoveSelectedItem(UclstUnderReview);
        }
    }
    #endregion

    private void clearcontrols()
    {
        txtDescriptionEnding.Text = string.Empty;
        UclstDiscontinued.Items.Clear();
        UclstPenDiscontinued.Items.Clear();
        UclstUnderReview.Items.Clear();
        ucSite.innerControlddlSite.SelectedIndex = 0;
    }

    private void getGroupClassification()
    {
        UclstDiscontinued.Items.Clear();
        UclstPenDiscontinued.Items.Clear();
        UclstUnderReview.Items.Clear();
        MASSIT_GroupClassificationBAL oMASSIT_GroupClassificationBAL = new MASSIT_GroupClassificationBAL();
        MASSIT_GroupClassificationBE oMASSIT_GroupClassificationBE = new MASSIT_GroupClassificationBE();

        oMASSIT_GroupClassificationBE.Action = "GetGroupClassification";
        oMASSIT_GroupClassificationBE.SiteID = !string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue) ? Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue) : (int?)null;
        List<MASSIT_GroupClassificationBE> lstGCDetails = oMASSIT_GroupClassificationBAL.GetGroupClassificationBAL(oMASSIT_GroupClassificationBE);
        oMASSIT_GroupClassificationBAL = null;
        if (lstGCDetails != null && lstGCDetails.Count > 0)
        {
            List<MASSIT_GroupClassificationBE> lstGCDetails1 = lstGCDetails.Where(a => a.ReviewStatus == 'D').ToList<MASSIT_GroupClassificationBE>();
            List<MASSIT_GroupClassificationBE> lstGCDetails2 = lstGCDetails.Where(a => a.ReviewStatus == 'P').ToList<MASSIT_GroupClassificationBE>();
            List<MASSIT_GroupClassificationBE> lstGCDetails3 = lstGCDetails.Where(a => a.ReviewStatus == 'R').ToList<MASSIT_GroupClassificationBE>();
            if (lstGCDetails1 != null && lstGCDetails1.Count > 0)
            {
                for (int i = 0; i < lstGCDetails1.Count; i++)
                {
                    if (string.IsNullOrEmpty(lstGCDetails1[i].SKUfilterWith))
                    {
                        lstGCDetails1[i].SKUEndingWith = lstGCDetails1[i].SKUEndingWith + " (ends)";
                    }
                    if (!string.IsNullOrEmpty(lstGCDetails1[i].SKUfilterWith) && Convert.ToString(lstGCDetails1[i].SKUfilterWith) == "ends")
                    {
                        lstGCDetails1[i].SKUEndingWith = lstGCDetails1[i].SKUEndingWith + " (ends)";
                    }

                    if (!string.IsNullOrEmpty(lstGCDetails1[i].SKUfilterWith) && Convert.ToString(lstGCDetails1[i].SKUfilterWith) == "begins")
                    {
                        lstGCDetails1[i].SKUEndingWith = lstGCDetails1[i].SKUEndingWith + " (begins)";
                    }

                    if (!string.IsNullOrEmpty(lstGCDetails1[i].SKUfilterWith) && Convert.ToString(lstGCDetails1[i].SKUfilterWith) == "contains")
                    {
                        lstGCDetails1[i].SKUEndingWith = lstGCDetails1[i].SKUEndingWith + " (contains)";
                    }
                }
                //if (UclstDiscontinued.Items.Count == 0)
                //{
                FillControls.FillListBox(ref UclstDiscontinued, lstGCDetails1, "SKUEndingWith", "SKUEndingWith");
                // }

                //if (UclstDiscontinued.Items.Count == 0)
                //{
                //    FillControls.FillListBox(ref UclstDiscontinued, lstGCDetails1, "SKUStartWith", "SKUStartWith");
                //}
                //else
                //{
                //    for (int i = 0; i < lstGCDetails1.Count; i++)
                //    {
                //        if (!string.IsNullOrEmpty(lstGCDetails1[i].SKUStartWith))
                //        {
                //            UclstDiscontinued.Items.Add(new ListItem(Convert.ToString(lstGCDetails1[i].SKUStartWith), Convert.ToString(lstGCDetails1[i].SKUStartWith)));
                //        }
                //    }

                //}


                //if (UclstDiscontinued.Items.Count == 0)
                //{
                //    FillControls.FillListBox(ref UclstDiscontinued, lstGCDetails1, "SKUContain", "SKUContain");
                //}
                //else
                //{
                //     for (int i = 0; i < lstGCDetails1.Count; i++)
                //    {
                //        if (!string.IsNullOrEmpty(lstGCDetails1[i].SKUContain))
                //        {
                //            UclstDiscontinued.Items.Add(new ListItem(Convert.ToString(lstGCDetails1[i].SKUContain), Convert.ToString(lstGCDetails1[i].SKUContain)));
                //        }
                //    }
                //}

            }
            if (lstGCDetails2 != null && lstGCDetails2.Count > 0)
            {
                for (int i = 0; i < lstGCDetails2.Count; i++)
                {
                    if (string.IsNullOrEmpty(lstGCDetails2[i].SKUfilterWith))
                    {
                        lstGCDetails2[i].SKUEndingWith = lstGCDetails2[i].SKUEndingWith + " (ends)";
                    }
                    if (!string.IsNullOrEmpty(lstGCDetails2[i].SKUfilterWith) && Convert.ToString(lstGCDetails2[i].SKUfilterWith) == "ends")
                    {
                        lstGCDetails2[i].SKUEndingWith = lstGCDetails2[i].SKUEndingWith + " (ends)";
                    }

                    if (!string.IsNullOrEmpty(lstGCDetails2[i].SKUfilterWith) && Convert.ToString(lstGCDetails2[i].SKUfilterWith) == "begins")
                    {
                        lstGCDetails2[i].SKUEndingWith = lstGCDetails2[i].SKUEndingWith + " (begins)";
                    }

                    if (!string.IsNullOrEmpty(lstGCDetails2[i].SKUfilterWith) && Convert.ToString(lstGCDetails2[i].SKUfilterWith) == "contains")
                    {
                        lstGCDetails2[i].SKUEndingWith = lstGCDetails2[i].SKUEndingWith + " (contains)";
                    }
                }

                FillControls.FillListBox(ref UclstPenDiscontinued, lstGCDetails2, "SKUEndingWith", "SKUEndingWith");
                //if (UclstPenDiscontinued.Items.Count == 0)
                //{
                //    FillControls.FillListBox(ref UclstPenDiscontinued, lstGCDetails2, "SKUEndingWith", "SKUEndingWith");
                //}
                //if (UclstPenDiscontinued.Items.Count == 0)
                //{
                //    FillControls.FillListBox(ref UclstPenDiscontinued, lstGCDetails2, "SKUStartWith", "SKUStartWith");
                //}
                //else
                //{
                //    for (int i = 0; i < lstGCDetails2.Count; i++)
                //    {
                //        if (!string.IsNullOrEmpty(lstGCDetails2[i].SKUStartWith))
                //        {
                //            UclstPenDiscontinued.Items.Add(new ListItem(Convert.ToString(lstGCDetails2[i].SKUStartWith), Convert.ToString(lstGCDetails2[i].SKUStartWith)));
                //        }
                //    }
                //}
                //if (UclstPenDiscontinued.Items.Count == 0)
                //{
                //    FillControls.FillListBox(ref UclstPenDiscontinued, lstGCDetails2, "SKUContain", "SKUContain");
                //}
                //else
                //{
                //    for (int i = 0; i < lstGCDetails2.Count; i++)
                //    {
                //        if (!string.IsNullOrEmpty(lstGCDetails2[i].SKUContain))
                //        {
                //            UclstPenDiscontinued.Items.Add(new ListItem(Convert.ToString(lstGCDetails2[i].SKUContain), Convert.ToString(lstGCDetails2[i].SKUContain)));
                //        }
                //    }
                //}

            }
            if (lstGCDetails3 != null && lstGCDetails3.Count > 0)
            {
                for (int i = 0; i < lstGCDetails3.Count; i++)
                {
                    if (string.IsNullOrEmpty(lstGCDetails3[i].SKUfilterWith))
                    {
                        lstGCDetails3[i].SKUEndingWith = lstGCDetails3[i].SKUEndingWith + " (ends)";
                    }
                    if (!string.IsNullOrEmpty(lstGCDetails3[i].SKUfilterWith) && Convert.ToString(lstGCDetails3[i].SKUfilterWith) == "ends")
                    {
                        lstGCDetails3[i].SKUEndingWith = lstGCDetails3[i].SKUEndingWith + " (ends)";
                    }

                    if (!string.IsNullOrEmpty(lstGCDetails3[i].SKUfilterWith) && Convert.ToString(lstGCDetails3[i].SKUfilterWith) == "begins")
                    {
                        lstGCDetails3[i].SKUEndingWith = lstGCDetails3[i].SKUEndingWith + " (begins)";
                    }

                    if (!string.IsNullOrEmpty(lstGCDetails3[i].SKUfilterWith) && Convert.ToString(lstGCDetails3[i].SKUfilterWith) == "contains")
                    {
                        lstGCDetails3[i].SKUEndingWith = lstGCDetails3[i].SKUEndingWith + " (contains)";
                    }
                }

                FillControls.FillListBox(ref UclstUnderReview, lstGCDetails3, "SKUEndingWith", "SKUEndingWith");
                //if (UclstUnderReview.Items.Count == 0)
                //{
                //    FillControls.FillListBox(ref UclstUnderReview, lstGCDetails3, "SKUEndingWith", "SKUEndingWith");
                //}
                //if (UclstUnderReview.Items.Count == 0)
                //{
                //    FillControls.FillListBox(ref UclstUnderReview, lstGCDetails3, "SKUStartWith", "SKUStartWith");
                //}
                //else
                //{
                //    for (int i = 0; i < lstGCDetails3.Count; i++)
                //    {
                //        if (!string.IsNullOrEmpty(lstGCDetails3[i].SKUStartWith))
                //        {
                //            UclstUnderReview.Items.Add(new ListItem(Convert.ToString(lstGCDetails3[i].SKUStartWith), Convert.ToString(lstGCDetails3[i].SKUStartWith)));
                //        }
                //    }
                //}
                //if (UclstUnderReview.Items.Count == 0)
                //{
                //    FillControls.FillListBox(ref UclstUnderReview, lstGCDetails3, "SKUContain", "SKUContain");
                //}
                //else
                //{
                //    for (int i = 0; i < lstGCDetails3.Count; i++)
                //    {
                //        if (!string.IsNullOrEmpty(lstGCDetails3[i].SKUContain))
                //        {
                //            UclstUnderReview.Items.Add(new ListItem(Convert.ToString(lstGCDetails3[i].SKUContain), Convert.ToString(lstGCDetails3[i].SKUContain)));
                //        }
                //    }
                //}

            }
        }



    }





}
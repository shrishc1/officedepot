﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="STK_CurrencyConvertorEdit.aspx.cs" Inherits="STK_CurrencyConvertorEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Src="~/CommonUI/UserControls/ucDate.ascx" TagName="ucDate" TagPrefix="cc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
      
        
    </script>
    <h2>
        <cc1:ucLabel ID="lblCurrencyConvertor" runat="server" Text="Currency Convertor"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="40%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td style="width: 10%">
                    </td>
                    <td style="font-weight: bold; width: 39%">
                        <cc1:ucLabel ID="lblRateAppDate" runat="server" Text="Rate Applicable Date" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 40%">
                        <cc2:ucDate ID="txtRateAppDate" runat="server" AutoPostBack="false" />
                        <asp:RequiredFieldValidator ID="drvRateAppDateValidation" runat="server" Display="None"
                            ControlToValidate="txtRateAppDate$txtUCDate" ValidateEmptyText="true" ValidationGroup="Save"
                            SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 10%">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblGBP" runat="server" Text="GBP Conversion Rate" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucNumericTextbox ID="txtGBP" runat="server" Width="65px" MaxLength="7" onkeyup="AllowDecimalOnly(this);"></cc1:ucNumericTextbox>
                        <cc1:ucLabel ID="lblEuro" runat="server" Text="Euro"></cc1:ucLabel>
                        <asp:RequiredFieldValidator ID="rfvConvertionRateRequired" runat="server" ControlToValidate="txtGBP"
                            Display="None" ValidationGroup="Save">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revConvertionRateExpression" ValidationExpression="^\d+(\.\d{0,3})?$"
                            runat="server" ControlToValidate="txtGBP" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                            Style="color: Red" ValidationGroup="Save" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
            ValidationGroup="Save" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>

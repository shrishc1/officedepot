﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using BaseControlLibrary;
using System.Collections;
using System.Data;


public partial class InventoryReviewOTIF : CommonPage
{
    protected void Page_InIt(object sender, EventArgs e) {
        ucVendorTemplateSelect.CurrentPage = this;
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.FileName = "InventoryReviewOTIF";
        ucExportToExcel1.GridViewControl = gvInventoryReviewOTIF;
    }

    protected void Page_Load(object sender, EventArgs e) {
      

        if (!IsPostBack) {
            pnlSearchScreen.Visible = true;
            pnlPotentialOutput.Visible = false;
            ucExportToExcel1.Visible = false;
            pager1.PageSize = 200;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            string PreviousMonth = DateTime.Now.AddMonths(-1).ToString("MM");
            drpMonthFrom.SelectedValue = PreviousMonth;

            string CurrentMonth = DateTime.Now.ToString("MM");
            drpMonthTo.SelectedValue = CurrentMonth;

            FillControls.FillDropDown(ref drpYearTo, GetLastYears(), "Year", "Year");
            FillControls.FillDropDown(ref drpYearFrom, GetLastYears(), "Year", "Year");

            if (CurrentMonth.Equals("01"))
            {
                int currentYear = DateTime.Now.Year;
                drpYearFrom.SelectedValue = (currentYear - 1).ToString();
            }
        }
    }
    private DataTable GetLastYears()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Year");
        int iStartYear = DateTime.Now.Year;
        for (int i = iStartYear; i >= iStartYear - 2; i--)
        {
            if (i >= 2014)
            {
                DataRow dr = dt.NewRow();
                dr["Year"] = i;
                dt.Rows.Add(dr);
            }
        }
        return dt;
    }

    protected void btnSearch_Click(object sender, EventArgs e) {
        BindVendorPanalty();
    }

    private void BindVendorPanalty(int Page = 1) {

        BackOrderBE oBackOrderBE = new BackOrderBE();

        List<BackOrderBE> lstoBackOrderBE = new List<BackOrderBE>();

        oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
        oBackOrderBE.PurchaseOrder.Purchase_order = "12345";
        oBackOrderBE.PurchaseOrder.Original_due_date = DateTime.Now.Date;
        oBackOrderBE.PurchaseOrder.StockPlannerNo = "25";
        oBackOrderBE.PurchaseOrder.StockPlannerName = "Neil McLachlan";
        oBackOrderBE.OTIFPenaltyRate = 98.78;
        oBackOrderBE.OTIFPenaltyCharges = 5.25;
        oBackOrderBE.PurchaseOrderValue = "399";
        oBackOrderBE.PotentialPenaltyCharge = Convert.ToDecimal( 19.29);
        oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oBackOrderBE.Vendor.Vendor_No = "025";
        oBackOrderBE.Vendor.VendorName = "ACCO";
        oBackOrderBE.Status = "Pending Review";
        lstoBackOrderBE.Add(oBackOrderBE);

        oBackOrderBE = new BackOrderBE();
        oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
        oBackOrderBE.PurchaseOrder.Purchase_order = "56789";
        oBackOrderBE.PurchaseOrder.Original_due_date = DateTime.Now.Date;
        oBackOrderBE.PurchaseOrder.StockPlannerNo = "60";
        oBackOrderBE.PurchaseOrder.StockPlannerName = "Hilroy Benjamin";
        oBackOrderBE.OTIFPenaltyRate = 98.78;
        oBackOrderBE.OTIFPenaltyCharges = 6.75;
        oBackOrderBE.PurchaseOrderValue = "345";
        oBackOrderBE.PotentialPenaltyCharge = Convert.ToDecimal(22.02);
        oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oBackOrderBE.Vendor.Vendor_No = "012";
        oBackOrderBE.Vendor.VendorName = "CPD";
        oBackOrderBE.Status = "Pending Review";
        lstoBackOrderBE.Add(oBackOrderBE);

        oBackOrderBE = new BackOrderBE();
        oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
        oBackOrderBE.PurchaseOrder.Purchase_order = "68340";
        oBackOrderBE.PurchaseOrder.Original_due_date = DateTime.Now.Date;
        oBackOrderBE.PurchaseOrder.StockPlannerNo = "65";
        oBackOrderBE.PurchaseOrder.StockPlannerName = "Natasha Todd";
        oBackOrderBE.OTIFPenaltyRate = 98.78;
        oBackOrderBE.OTIFPenaltyCharges = 5.25;
        oBackOrderBE.PurchaseOrderValue = "145";
        oBackOrderBE.PotentialPenaltyCharge = Convert.ToDecimal(20.04);
        oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oBackOrderBE.Vendor.Vendor_No = "011";
        oBackOrderBE.Vendor.VendorName = "HELIX";
        oBackOrderBE.Status = "Pending Review";
        lstoBackOrderBE.Add(oBackOrderBE);

        if (lstoBackOrderBE != null && lstoBackOrderBE.Count > 0) {
            ucExportToExcel1.Visible = true;
            gvInventoryReviewOTIF.DataSource = lstoBackOrderBE;
            gvInventoryReviewOTIF.DataBind();
        }

        pnlSearchScreen.Visible = false;
        pnlPotentialOutput.Visible = true;
    }

    protected void gvInventoryReviewOTIF_PageIndexChanging(object sender, GridViewPageEventArgs e) { }

    protected void btnBack_Click(object sender, EventArgs e) {
        //this.ClearCriteria();
        pnlSearchScreen.Visible = true;
        pnlPotentialOutput.Visible = false;
        ucExportToExcel1.Visible = false;

    }

    public override void TemplateSelectedIndexChanged() {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)ucIncludeVendor.FindControl("lstSelectedVendor");
        ListBox lstVendor = (ListBox)ucIncludeVendor.FindControl("lstVendor");

        if (VendorTemplateId > 0) {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);
            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0) {
                lstSelectedVendor.Items.Clear();
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++) {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                }
            }
        }
        else {
            lstSelectedVendor.Items.Clear();
        }
    }

    public void pager_Command(object sender, CommandEventArgs e) {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindVendorPanalty(currnetPageIndx);
    }
}
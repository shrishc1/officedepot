﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="SKUDetailedReview.aspx.cs" Inherits="SKUDetailedReview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>
    <%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        var gridCheckedCount = 0;

        $(document).ready(function () {
            SelectAllGrid($("#chkSelectAllText")[0]);
        });

        function SelectAllGrid(chkBoxAllObj) {                     
            if (chkBoxAllObj != undefined) {
                if (chkBoxAllObj.checked) {
                    $("input[type='checkbox'][name$='chkSelect']").attr('checked', true);
                    gridCheckedCount = $("input[type='checkbox'][name$='chkSelect']:checked").size();
                }
                else {
                    $("input[type='checkbox'][name$='chkSelect']").attr('checked', false);
                    gridCheckedCount = 0;
                }
            }
        }

        function CheckUncheckAllCheckBoxAsNeeded() {
           
            if ($("input[type='checkbox'][name$='chkSelect']:checked").size() == $("input[type='checkbox'][name$='chkSelect']").length) {
                $("[id$=chkSelectAllText]").attr('checked', true);
                gridCheckedCount = $("#<%=gvSKUDetailedReview.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
            else {
                $("[id$=chkSelectAllText]").attr('checked', false);
                gridCheckedCount = $("#<%=gvSKUDetailedReview.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
        }

        function CheckAtleastOneCheckBox() {

            CheckUncheckAllCheckBoxAsNeeded();

            var selectCheckboxToAccept = '<%=selectCheckboxToAccept%>';

            if (gridCheckedCount == 0) {
                alert(selectCheckboxToAccept);
                return false
            }

            var isAnyApprovedOrDeclined = false;
            $('#<%=gvSKUDetailedReview.ClientID%>  tr').each(function () {
                if ($(this).find('input:checkbox').is(':checked')) {
                    var lblInventoryStatus = $(this).find('[id$= lblInventoryStatus]').text();
                    if (lblInventoryStatus == 'Approved' || lblInventoryStatus == 'Declined') {
                        isAnyApprovedOrDeclined = true;
                    }
                }
            });
            if (isAnyApprovedOrDeclined == true) {
                var result = confirm('<%=MsgChangestatus%>');
                if (result) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
    </script>

    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblSKUDetail" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="100%" cellspacing="5" cellpadding="0" border="0">
                <tr>
                    <td style="width: 50%">
                        <cc1:ucPanel ID="pnlSkuInformation" runat="server" CssClass="fieldset-form">
                            <table width="95%" cellspacing="5" cellpadding="0" border="0" align="left">
                                <tr>
                                    <td style="font-weight: bold; width: 19%">
                                        <cc1:ucLabel ID="lblVikingCode" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        :
                                    </td>
                                    <td style="width: 30%">
                                        <asp:Literal ID="ltVikingCode" runat="server"></asp:Literal>
                                    </td>
                                    <td style="font-weight: bold; text-align: right; width: 19%">
                                        <cc1:ucLabel ID="lblODCode" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        :
                                    </td>
                                    <td style="width: 30%">
                                        <asp:Literal ID="ltODCode" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblDescription" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        :
                                    </td>
                                    <td colspan="4">
                                        <asp:Literal ID="ltDescription" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                    <td style="width: 50%;">
                        <cc1:ucPanel ID="pnlVendorInformation" runat="server" CssClass="fieldset-form">
                            <table width="95%" cellspacing="5" cellpadding="0" border="0" align="left">
                                <tr>
                                    <td style="font-weight: bold; width: 34%">
                                        <cc1:ucLabel ID="lblFixedVendorNo" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 1%">
                                        :
                                    </td>
                                    <td style="width: 65%">
                                        <asp:Literal ID="ltFixedVendorNo" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblFixedVendorName" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        :
                                    </td>
                                    <td>
                                        <asp:Literal ID="ltFixedVendorName" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                </tr>
            </table>
            <div class="button-row">
                <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="BtnExport_Click"
                    Width="109px" Height="20px" />
            </div>
            <table cellspacing="1" width="100%" cellpadding="0" border="0" align="center" class="form-table">
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucGridView ID="gvSKUDetailedReview" runat="server" CssClass="grid gvclass searchgrid-1"
                            Width="950px" GridLines="Both" OnPageIndexChanging="GvSKUDetailedReview_PageIndexChanging"
                            OnDataBound="GvSKUDetailedReview_OnDataBound">
                            <EmptyDataTemplate>
                                <div style="text-align: center">
                                    <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="SelectAllCheckBox">
                                    <HeaderStyle HorizontalAlign="Left" Width="30px" />
                                    <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderTemplate>
                                        <cc1:ucCheckbox runat="server" ID="chkSelectAllText" CssClass="checkbox-input" onclick="SelectAllGrid(this);" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnBOReportingID" Value='<%# Eval("BOReportingID") %>' runat="server" />
                                        <asp:HiddenField ID="hdnBOIncurredDate" Value='<%# Eval("BOIncurredDate") %>' runat="server" />
                                        <asp:HiddenField ID="hdnODSKUNO" Value='<%# Eval("SKU.OD_SKU_NO") %>' runat="server" />
                                        <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" onclick="CheckUncheckAllCheckBoxAsNeeded();"
                                            Visible='<%# (Convert.ToString( Eval("IsVendorCommuncationSent")) == "No") ? true: false %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DateBOIncurred" SortExpression="BOIncurredDate">
                                    <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="ltDateBOIncurred" Text='<%#Eval("BOIncurredDate", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Site" SortExpression="SKU.SiteName">
                                    <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="ltsite" Text='<%#Eval("SKU.SiteName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CountofBO(s)Incurred" SortExpression="NoofBO">
                                    <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="lt3" Text='<%#Eval("NoofBO") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right" Width="60px" Font-Size="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="QtyofBO(s)Incurred" SortExpression="QtyonBackOrder">
                                    <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="lt4" Text='<%#Convert.ToInt64(Math.Round(Convert.ToDouble(Eval("QtyonBackOrder")))) %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right" Width="60px" Font-Size="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CountofBOPenalties" SortExpression="NoofBOWithPenalities">
                                    <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("NoofBOWithPenalities") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right" Width="50px" Font-Size="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="OverduePO#" SortExpression="PurchaseOrder.Purchase_order">
                                    <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("PurchaseOrder.Purchase_order") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right" Width="70px" Font-Size="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="OutstandingQtyonPO" SortExpression="PurchaseOrder.Outstanding_Qty">
                                    <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("PurchaseOrder.Outstanding_Qty") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right" Width="70px" Font-Size="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="OrderRaisedDate" SortExpression="PurchaseOrder.Order_raised">
                                    <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="lt5" Text='<%#Eval("PurchaseOrder.Order_raised", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="OriginalDueDate" SortExpression="PurchaseOrder.Original_due_date">
                                    <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="lt7" Text='<%#Eval("PurchaseOrder.Original_due_date", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="RevisedDueDate" SortExpression="PurchaseOrder.RevisedDueDate">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="ltOutstandingQty" Text='<%#Eval("PurchaseOrder.RevisedDueDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Qty Received" SortExpression="TotalQtyReceived">
                                    <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="ltTotalQtyReceived" Text='<%#Eval("TotalQtyReceived") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="50px" Font-Size="10px" />
                                </asp:TemplateField>

                                 <asp:TemplateField HeaderText="Last Receipt Date" SortExpression="LastReceiptDate">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="ltLastReceiptDate" Text='<%#Eval("LastReceiptDate", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="40px" Font-Size="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="VendorNoassociatedtoPO" SortExpression="PurchaseOrder.Vendor.Vendor_No">
                                    <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="ltPotentialPenaltyCharge" Text='<%#Eval("PurchaseOrder.Vendor.Vendor_No") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="VendorName" SortExpression="PurchaseOrder.Vendor.VendorName">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="ltVendorVendorName" Text='<%#Eval("PurchaseOrder.Vendor.VendorName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" SortExpression="InventoryReviewStatus">
                                    <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="lblInventoryStatus" Text='<%#Eval("InventoryReviewStatus") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                        <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="Pager_Command" GenerateGoToSection="true">
                        </cc1:PagerV2_8>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settingsnoborder">
                <tr>
                    <td align="right">
                        <div class="button-row">
                            <cc1:ucButton ID="btnConfirm" runat="server" Text="Confirm" CssClass="button" OnClick="BtnConfirm_Click"
                                OnClientClick="return CheckAtleastOneCheckBox();" />
                            <cc1:ucButton ID="btnDecline" runat="server" Text="Decline" CssClass="button" OnClick="BtnDecline_Click"
                                OnClientClick="return CheckAtleastOneCheckBox();" />
                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="BtnBack_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
     <%---WARNING popup Decline start--%>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnRejectProvisional" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlRejectProvisional" runat="server" TargetControlID="btnRejectProvisional"
                PopupControlID="pnlRejectProvisional" BackgroundCssClass="modalBackground" BehaviorID="RejectProvisional"
                DropShadow="false" />
            <asp:Panel ID="pnlRejectProvisional" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3>
                        <cc1:ucLabel ID="lblWarningInfo" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <cc1:ucLabel ID="lblDeclineReasonMsg" runat="server"></cc1:ucLabel></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblReason" runat="server" isRequired="true" style="display: inline-block;width: 58px;"></cc1:ucLabel>&nbsp;
                                <cc1:ucDropdownList ID="ddlDeclineCode" runat="server" Width="300px">
                                </cc1:ucDropdownList>
                            </td>
                        </tr>
                          <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblComments" runat="server" isRequired="true" Text="Comments" style="display:none;float:left;"></cc1:ucLabel>&nbsp;
                                <cc1:ucTextbox  ID="tboxComment" runat="server" isRequired="true" style="display:none" TextMode="MultiLine" Height="48px" Width="300px"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <cc1:ucButton ID="btnConfirm_1" runat="server" CssClass="button" OnCommand="BtnConfirm_1_Click" OnClientClick="return IsCommentEntered()" />
                                    &nbsp;
                                    <cc1:ucButton ID="btnCancel" runat="server" CssClass="button" OnCommand="BtnCancel_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnConfirm" />
            <asp:AsyncPostBackTrigger ControlID = "ddlDeclineCode" EventName = "SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript">

        $(document).ready(function () {
            
            var declineCodevalue = $('#<%=ddlDeclineCode.ClientID%>').val();
            if (declineCodevalue != "") {
                
                var ddlDeclineCodeValue = declineCodevalue;
                var arr = ddlDeclineCodeValue.split('-');
                if (arr[1] == "1") {
                    document.getElementById('<%= tboxComment.ClientID %>').value = '';
                    $('#<%=lblComments.ClientID%>').show();
                    $('#<%=tboxComment.ClientID%>').show();
                }
                else if (arr[1] == "0") {
                    $('#<%=lblComments.ClientID%>').hide();
                    $('#<%=tboxComment.ClientID%>').hide();
                    document.getElementById('<%= tboxComment.ClientID %>').value = '';
                }
            }

            $('#<%=ddlDeclineCode.ClientID%>').change(function () {
                
                var selectedVal = $('option:selected', this).val();
                if (selectedVal != "") {
                    var ddlDeclineCodeValue = selectedVal;
                    var arr = ddlDeclineCodeValue.split('-');
                    if (arr[1] == "1") {
                        document.getElementById('<%= tboxComment.ClientID %>').value = '';
                        $('#<%=lblComments.ClientID%>').show();
                        $('#<%=tboxComment.ClientID%>').show();
                    }
                    else if (arr[1] == "0") {
                        $('#<%=lblComments.ClientID%>').hide();
                        $('#<%=tboxComment.ClientID%>').hide();
                        document.getElementById('<%= tboxComment.ClientID %>').value = '';
                    }
                }
            });
        });

        function IsCommentEntered() {

            var declineCodevalue = $('#<%=ddlDeclineCode.ClientID%>').val();
            if (declineCodevalue != "") {
                
                var ddlDeclineCodeValue = declineCodevalue;
                var arr = ddlDeclineCodeValue.split('-');
                if (arr[1] == "1") {
                    var DeclineComment = document.getElementById('<%= tboxComment.ClientID %>').value;
                    if (DeclineComment == "") {
                        alert('<%=PleaseEnterComments%>');
                        return false;
                    }
                }
                else if (arr[1] == "0") {

                    document.getElementById('<%= tboxComment.ClientID %>').value = '';
                }
            }
        }
    </script>
</asp:Content>

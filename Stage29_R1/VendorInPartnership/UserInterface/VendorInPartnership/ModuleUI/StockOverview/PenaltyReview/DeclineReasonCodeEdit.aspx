﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DeclineReasonCodeEdit.aspx.cs" Inherits="DeclineReasonCodeEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
        
    </script>
    <h2>
        <cc1:ucLabel ID="lblPenaltyManagementDeclineReasonCode" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div>
           
           <asp:RequiredFieldValidator ID="rfvCodeRequired" runat="server" ControlToValidate="txtCode"
                Display="None" ValidationGroup="a" SetFocusOnError="true">
            </asp:RequiredFieldValidator>

            <asp:RequiredFieldValidator ID="rfvReasonRequired" runat="server" ControlToValidate="txtReason"
                Display="None" ValidationGroup="a" SetFocusOnError="true">
            </asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                Style="color: Red" ValidationGroup="a" />
        </div>
        <div class="formbox">
            <table width="40%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
              
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblCode" runat="server" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td style="font-weight: bold;width:120px;">
                    
                        <cc1:ucTextbox ID="txtCode" ValidationGroup="a" runat="server" Width="100px" MaxLength="10"></cc1:ucTextbox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblReason" runat="server" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucTextbox ID="txtReason" ValidationGroup="a" runat="server" Width="200px" MaxLength="200"></cc1:ucTextbox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="font-weight: bold">
                        <cc1:ucLabel ID="lblMandatoryCommentRequired" style="width:100px" runat="server" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold">
                        :
                    </td>
                    <td style="font-weight: bold">
                       <cc1:ucCheckbox  id="chkCommentsRequired" runat="server" Checked="false" />       
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
            ValidationGroup="a" />
        <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click"
            Visible="false" OnClientClick="return confirmDelete();" />
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using BaseControlLibrary;
using System.Collections;
using Utilities;
public partial class SKUDetailedReview : CommonPage
{

    #region Declarations ...
    private const int gridPageSize = 50;
    bool IsExportClicked = false;
    protected string selectCheckboxToAccept = WebCommon.getGlobalResourceValue("SelectCheckboxToAction");
    protected string MsgChangestatus = WebCommon.getGlobalResourceValue("MsgChangestatus");
    protected string PleaseEnterComments = WebCommon.getGlobalResourceValue("PleaseEnterComments");
    #endregion

    #region Events...

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (!Page.IsPostBack) {
            if (GetQueryStringValue("ODSKUNO") != null && !string.IsNullOrWhiteSpace(GetQueryStringValue("ODSKUNO").ToString())) {
                lblSKUDetail.Text = lblSKUDetail.Text + " - " + GetQueryStringValue("ODSKUNO").ToString();
            }
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;

            if (!string.IsNullOrEmpty(Convert.ToString(GetQueryStringValue("ODSKUNO"))))
            {
                BindOpenBackOrderListing();
            }
        }
    }

    protected void BtnConfirm_Click(object sender, EventArgs e)
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            foreach (GridViewRow row in gvSKUDetailedReview.Rows)
            {
                CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;
                HiddenField hdnBOReportingID = row.FindControl("hdnBOReportingID") as HiddenField;


                if (chkSelect != null && hdnBOReportingID != null)
                {
                    if (chkSelect.Checked)
                    {
                        oBackOrderBE.SelectedBOReportingID += string.Format(",{0}", hdnBOReportingID.Value);                        
                    }
                }
            }

            oBackOrderBE.Action = "UpdateInventoryReviewStatus";
            oBackOrderBE.InventoryReviewStatus = "Approved";
            int? iResult = oBackOrderBAL.UpdateODSKUStatusBAL(oBackOrderBE);
            BindOpenBackOrderListing();
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void BtnDecline_Click(object sender, EventArgs e)
    {
        BindReasonCode();
        mdlRejectProvisional.Show();
    }

    protected void BtnExport_Click(object sender, EventArgs e)
    {
        try
        {
            IsExportClicked = true;
            BindOpenBackOrderListing();
            LocalizeGridHeader(gvSKUDetailedReview);
            gvSKUDetailedReview.Columns[0].Visible = false;
            WebCommon.ExportHideHidden("BackOrderPendingApproval", gvSKUDetailedReview);
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void BtnBack_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("BP") !=null && Convert.ToString(GetQueryStringValue("BP")) == "PAO")         
        {
            EncryptQueryString("InventoryReview1.aspx?PreviousPage=SkuDetiledReview&BP=PAQ");
        }
        else
            if (!string.IsNullOrEmpty(GetQueryStringValue("Purchase_order")))
            {
                EncryptQueryString("InventoryReview1.aspx?PreviousPage=SkuDetiledReview&Purchase_order=" + GetQueryStringValue("Purchase_order") + "");
            }
            else if (GetQueryStringValue("Page") == "Top")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close();", true);               
            }
            else 
            {
                EncryptQueryString("InventoryReview1.aspx?PreviousPage=SkuDetiledReview");
            }

    }

    protected void GvSKUDetailedReview_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        if (ViewState["lstBackOrderBE"] != null)
        {
            gvSKUDetailedReview.DataSource = (List<BackOrderBE>)ViewState["lstBackOrderBE"];
            gvSKUDetailedReview.PageIndex = e.NewPageIndex;
            if (Session["PendingPenalty"] != null)
            {
                Hashtable htPendingPenalty = (Hashtable)Session["PendingPenalty"];
                htPendingPenalty.Remove("PageIndex");
                htPendingPenalty.Add("PageIndex", e.NewPageIndex);
                Session["PendingPenalty"] = htPendingPenalty;
            }
            gvSKUDetailedReview.DataBind();
        }
    }

    protected void GvSKUDetailedReview_OnDataBound(object sender, EventArgs e)
    {

        for (int i = gvSKUDetailedReview.Rows.Count - 1; i > 0; i--)
        {
            GridViewRow row = gvSKUDetailedReview.Rows[i];
            GridViewRow previousRow = gvSKUDetailedReview.Rows[i - 1];
            var ltDateBOIncurred = (Label)row.FindControl("ltDateBOIncurred");
            var ltDateBOIncurredP = (Label)previousRow.FindControl("ltDateBOIncurred");
            var ltSite = (Label)row.FindControl("ltsite");
            var ltSiteP = (Label)previousRow.FindControl("ltsite");

            if (ltDateBOIncurred.Text == ltDateBOIncurredP.Text)
            {
                if (ltSite.Text == ltSiteP.Text)
                {
                    if (previousRow.Cells[1].RowSpan == 0)
                    {
                        if (row.Cells[1].RowSpan == 0)
                        {
                            previousRow.Cells[0].RowSpan += 2;
                            previousRow.Cells[1].RowSpan += 2;
                            previousRow.Cells[2].RowSpan += 2;
                            previousRow.Cells[3].RowSpan += 2;
                            previousRow.Cells[4].RowSpan += 2;
                            previousRow.Cells[5].RowSpan += 2;
                            previousRow.Cells[13].RowSpan += 2;

                        }
                        else
                        {
                            previousRow.Cells[0].RowSpan = row.Cells[0].RowSpan + 1;
                            previousRow.Cells[1].RowSpan = row.Cells[1].RowSpan + 1;
                            previousRow.Cells[2].RowSpan = row.Cells[2].RowSpan + 1;
                            previousRow.Cells[3].RowSpan = row.Cells[3].RowSpan + 1;
                            previousRow.Cells[4].RowSpan = row.Cells[4].RowSpan + 1;
                            previousRow.Cells[5].RowSpan = row.Cells[5].RowSpan + 1;
                            previousRow.Cells[13].RowSpan = row.Cells[13].RowSpan + 1;
                        }
                        row.Cells[0].Visible = false;
                        row.Cells[1].Visible = false;
                        row.Cells[2].Visible = false;
                        row.Cells[3].Visible = false;
                        row.Cells[4].Visible = false;
                        row.Cells[5].Visible = false;
                        row.Cells[13].Visible = false;
                    }
                }
            }
        }
    }

    protected void BtnConfirm_1_Click(object sender, EventArgs e) {
        BindPenalty();
    }

    protected void BtnCancel_Click(object sender, EventArgs e) {
        mdlRejectProvisional.Hide();
    }

    #endregion

    #region Methods...

    private void BindReasonCode() {
        MAS_DeclineReasonCodeBE oMAS_DeclineReasonCodeBE = new MAS_DeclineReasonCodeBE();
        MAS_DeclineReasonCodeBAL oMAS_DeclineReasonCodeBAL = new MAS_DeclineReasonCodeBAL();

        oMAS_DeclineReasonCodeBE.Action = sqlAction.ShowAll;

        List<MAS_DeclineReasonCodeBE> lstMAS_DeclineReasonCodeBE = oMAS_DeclineReasonCodeBAL.GetDeclineReasonCodeBAL(oMAS_DeclineReasonCodeBE);

        if (lstMAS_DeclineReasonCodeBE != null && lstMAS_DeclineReasonCodeBE.Count > 0) {
            FillControls.FillDropDown(ref ddlDeclineCode, lstMAS_DeclineReasonCodeBE, "ReasonCode", "IsDeclineReasonCommentMandatory");
        }
    }
    private void BindPenalty() {
        try {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            foreach (GridViewRow row in gvSKUDetailedReview.Rows) {
                CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;
                HiddenField hdnBOReportingID = row.FindControl("hdnBOReportingID") as HiddenField;

                ucLabel lblInventoryStatus = row.FindControl("ltStatus") as ucLabel;

                if (chkSelect != null && hdnBOReportingID != null) {
                    if (chkSelect.Checked) {

                        oBackOrderBE.SelectedBOReportingID += string.Format(",{0}", hdnBOReportingID.Value);
                    }
                }
            }
            string ddlDeclineCodeValue = ddlDeclineCode.SelectedItem.Value;
            string[] strDeclineReasonCode = ddlDeclineCodeValue.Split('-');

            oBackOrderBE.Action = "UpdateInventoryReviewStatus";
            oBackOrderBE.InventoryReviewStatus = "Declined";
            oBackOrderBE.DeclineCode = new MAS_DeclineReasonCodeBE();
            oBackOrderBE.DeclineCode.DeclineReasonCodeID = Convert.ToInt32(strDeclineReasonCode[0]);
            if (!string.IsNullOrEmpty(tboxComment.Text))
            {
                oBackOrderBE.Comments = tboxComment.Text;
            }

            int? iResult = oBackOrderBAL.UpdateODSKUStatusBAL(oBackOrderBE);
            BindOpenBackOrderListing();
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
    public void Pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindOpenBackOrderListing(currnetPageIndx);
    }
    private void BindOpenBackOrderListing(int Page = 1)
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();

            oBackOrderBE.Action = "GetOpenBackOrdersListing";
            oBackOrderBE.SKU.OD_SKU_NO = Convert.ToString(GetQueryStringValue("ODSKUNO"));
            oBackOrderBE.SelectedDateFrom =Convert.ToDateTime(GetQueryStringValue("DateFrom"));
            oBackOrderBE.SelectedDateTo = Convert.ToDateTime(GetQueryStringValue("Dateto"));
            if (!string.IsNullOrEmpty(Convert.ToString(GetQueryStringValue("Status"))))
            {
                oBackOrderBE.InventoryReviewStatus = Convert.ToString(GetQueryStringValue("Status"));
            }
            if (!string.IsNullOrEmpty(Convert.ToString(GetQueryStringValue("VikingCode"))))
            {
                oBackOrderBE.SKU.Direct_SKU = Convert.ToString(GetQueryStringValue("VikingCode"));
            }

            if (!string.IsNullOrEmpty(Convert.ToString(GetQueryStringValue("IsMailSend"))))
            {
                oBackOrderBE.IsVendorCommuncationSent = Convert.ToString(GetQueryStringValue("IsMailSend"));
            }
            if (IsExportClicked)
            {
                oBackOrderBE.GridCurrentPageNo = 0;
                oBackOrderBE.GridPageSize = 0;
            }
            else
            {
                oBackOrderBE.GridCurrentPageNo = Page;
                oBackOrderBE.GridPageSize = gridPageSize;
            }

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetOpenBackOrderListingBAL(oBackOrderBE);
            string sitenumber=Convert.ToString(GetQueryStringValue("Site")).Split('-').ElementAtOrDefault(0).Trim();
           
            if (!string.IsNullOrEmpty(Convert.ToString(GetQueryStringValue("Site")))) {
                lstBackOrderBE.RemoveAll(s => s.SKU.SiteName.Split('-').ElementAtOrDefault(0).Trim() != sitenumber);
            
            }
           

            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
            

                ltVikingCode.Text = lstBackOrderBE[0].SKU.Direct_SKU;
                ltODCode.Text = lstBackOrderBE[0].SKU.OD_SKU_NO;
                ltDescription.Text = lstBackOrderBE[0].SKU.DESCRIPTION;
                ltFixedVendorNo.Text = lstBackOrderBE[0].Vendor.Vendor_No;
                ltFixedVendorName.Text = lstBackOrderBE[0].Vendor.VendorName;
                string InventoryStatus = lstBackOrderBE[0].InventoryReviewStatus;
               

                //Bind SKU Listing
                btnExportToExcel.Visible = true;
                gvSKUDetailedReview.DataSource = null;
                gvSKUDetailedReview.DataSource = lstBackOrderBE;
                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                ViewState["lstBackOrderBE"] = lstBackOrderBE;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else
            {
                gvSKUDetailedReview.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;


            }

            gvSKUDetailedReview.PageIndex = Page;
            pager1.CurrentIndex = Page;
            gvSKUDetailedReview.DataBind();
            oBackOrderBAL = null;


            if (gvSKUDetailedReview.Rows.Count == 0)
            {
                btnExportToExcel.Visible = false;
                btnConfirm.Visible = false;
                btnDecline.Visible = false;
            }

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
    #endregion
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="DeclineReasonCodeOverview.aspx.cs" Inherits="DeclineReasonCodeOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblPenaltyManagementDeclineReasonCode" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            
            <div class="button-row" id="divbtnOpr" runat="server">
                <cc2:ucAddButton ID="btnAdd" runat="server" />
                <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
            </div>
            <table width="100%">
                <tr>
                    <td align="center" colspan="9">
                        <cc1:ucGridView ID="grdReasonCode" Width="100%" runat="server" CssClass="grid" OnRowDataBound="grdReasonCode_RowDataBound"
                            AlternatingRowStyle-CssClass="altcolor" OnSorting="SortGrid" AllowSorting="true">
                            <Columns>
                               
                                <asp:TemplateField HeaderText="Code" SortExpression="Code">
                                    <HeaderStyle Width="25%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hpLink" runat="server" Text='<%# Eval("Code") %>'
                                            NavigateUrl='<%# EncryptQuery("DeclineReasonCodeEdit.aspx?DeclineReasonCodeID=" + Eval("DeclineReasonCodeID"))%>'></asp:HyperLink>                                        
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Reason" DataField="Reason" SortExpression="Reason">
                                    <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                 <asp:TemplateField HeaderText="Mandatory Comment">
                                    <HeaderStyle Width="28%"  HorizontalAlign="Left"  />
                                   
                                    <ItemTemplate >
                                        <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" Checked='<%# Eval("IsMandatoryComment") %>' Enabled="false" />
                                        
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

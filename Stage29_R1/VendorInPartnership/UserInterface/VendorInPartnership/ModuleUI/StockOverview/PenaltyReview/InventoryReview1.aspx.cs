﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using BaseControlLibrary;
using System.Collections;
using Utilities;
using System.Data;


public partial class InventoryReview1 : CommonPage
{
    #region Declarations ...
    private const int gridPageSize = 50;
    bool IsExportClicked = false;
    protected string selectCheckboxToAccept = WebCommon.getGlobalResourceValue("SelectCheckboxToAction");
    protected string MsgChangestatus = WebCommon.getGlobalResourceValue("MsgChangestatus");
    protected string PleaseEnterComments = WebCommon.getGlobalResourceValue("PleaseEnterComments"); 
    #endregion

    #region Events...
    protected void Page_InIt(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }
    
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "SkuDetiledReview")
            {
                if (Session["InventoryReview1"] != null)
                {
                    GetSession();
                }
            }
            else if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "POInquiry") {  //----------phase 18 R2 point 10 -------------------------------//
                BtnSearch_Click(null,null);
            }
            if (GetQueryStringValue("BacktoBind") != null)
            {
                RetainSearchData();
            }
        }
        else if (ViewState["pagerItemCount"] != null)
        {
            pager1.ItemCount = Convert.ToInt32(ViewState["pagerItemCount"]);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (GetQueryStringValue("BP") != null && Convert.ToString(GetQueryStringValue("BP")) == "PAO")
        {
            Session["OpenFrom"] = "PAQ";
        }
       
        if (!IsPostBack)
        {            
            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
            string PreviousMonth = DateTime.Now.AddMonths(-1).ToString("MM");
            string CurrentMonth = DateTime.Now.ToString("MM");
            drpMonthFrom.SelectedValue = PreviousMonth;


            drpMonthTo.SelectedValue = CurrentMonth;

            FillControls.FillDropDown(ref drpYearTo, GetLastYears(), "Year", "Year");
            FillControls.FillDropDown(ref drpYearFrom, GetLastYears(), "Year", "Year");
           
            if (CurrentMonth.Equals("01"))
            { 
                int currentYear=DateTime.Now.Year;
                drpYearFrom.SelectedValue = (currentYear - 1).ToString();
            }

            if (GetQueryStringValue("VendorID") != null || GetQueryStringValue("FromDashboard") != null)
            {
                BindOpenBackOrder();
            } 
        }
    }

  
    private DataTable GetLastYears()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Year");
        int iStartYear = DateTime.Now.Year;
        for (int i = iStartYear; i >= iStartYear - 2; i--)
        {
            if (i >= 2014)
            {
                DataRow dr = dt.NewRow();
                dr["Year"] = i;
                dt.Rows.Add(dr);
            }
        }
        return dt;
    }

    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        Session["OpenFrom"] = null;
        Session.Remove("OpenFrom");
        BindOpenBackOrder();
    }

    protected void BtnExport_Click(object sender, EventArgs e)
    {

        IsExportClicked = true;
        BindOpenBackOrder();
        LocalizeGridHeader(gvInventoryReview);
        gvInventoryReview.Columns[0].Visible = false;
        WebCommon.ExportHideHidden("BackOrderPendingApproval", gvInventoryReview);
        
    }

    protected void BtnAccept_Click(object sender, EventArgs e)
    {
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        BackOrderBE oBackOrderBE = new BackOrderBE();

   
        foreach (GridViewRow row in gvInventoryReview.Rows)
        {
            CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;
            HiddenField hdnODSKUNO = row.FindControl("hdnODSKUNO") as HiddenField;
            HiddenField hdnSiteID = row.FindControl("hdnSiteID") as HiddenField;
            Literal lblInventoryStatus = row.FindControl("lblInventoryStatus") as Literal;
            HiddenField hdnPenaltyChargeID = row.FindControl("hdnPenaltyChargeID") as HiddenField;
            Label lblPlannerAmount = row.FindControl("lblPlannerAmount") as Label;
            Label lblVendorAmount = row.FindControl("lblVendorAmount") as Label;
            Label lblInventoryInitialActionDate = row.FindControl("lblInventoryInitialActionDate") as Label;
            Label lblInventoryInitialReview = row.FindControl("lblInventoryInitialReview") as Label;
            Label lblLastBackOrderDate = row.FindControl("lblLastBackOrderDate") as Label;
            Label lblCurrentStatus = row.FindControl("lblCurrentStatus") as Label;
            int numberOfDays = DateTime.DaysInMonth(Convert.ToInt16(drpYearTo.SelectedValue), Convert.ToInt16(drpMonthTo.SelectedValue));
            HiddenField hdnVendorID = row.FindControl("hdnVendorID") as HiddenField;
            HiddenField hdnSKUID = row.FindControl("hdnSKUID") as HiddenField;
            if (chkSelect != null && hdnODSKUNO != null)
            {
                if (chkSelect.Checked)
                {
                    if (lblCurrentStatus.Text == "Pending with Supply Planner")
                    {
                        oBackOrderBE.SelectedODSkUCode = hdnODSKUNO.Value;
                        oBackOrderBE.Action = "UpdateInventoryReviewStatus";
                        oBackOrderBE.InventoryReviewStatus = "Approved";
                        oBackOrderBE.SiteID = Convert.ToInt32(hdnSiteID.Value);
                        oBackOrderBE.CurrentBoStatus = lblInventoryStatus.Text;
                        if (GetQueryStringValue("FromDashboard") == null)
                        {
                            oBackOrderBE.SelectedDateFrom = Convert.ToDateTime(drpYearFrom.SelectedValue + "-" + drpMonthFrom.SelectedValue + "-" + "01");
                            oBackOrderBE.SelectedDateTo = Convert.ToDateTime(drpYearTo.SelectedValue + "-" + drpMonthTo.SelectedValue + "-" + numberOfDays.ToString());
                        }

                        oBackOrderBE.VendorID = Convert.ToInt32(hdnVendorID.Value);
                        int? iResult = oBackOrderBAL.UpdateODSKUStatusBAL(oBackOrderBE);
                    }
                    else if (lblCurrentStatus.Text == "Vendor Not Replied Mediator to Check" || lblCurrentStatus.Text == "In Dispute with Mediator")
                    {
                        oBackOrderBE.SelectedODSkUCode = hdnODSKUNO.Value;
                        oBackOrderBE.Action = "UpdateMediatorAction";
                        oBackOrderBE.MediatorAction = "Accept Penalty";
                        oBackOrderBE.MediatorStatus = "PenaltyApplied";
                        if (lblCurrentStatus.Text == "Vendor Not Replied Mediator to Check")
                        {
                            oBackOrderBE.AgreedPenaltyCharge = !string.IsNullOrEmpty(lblVendorAmount.Text) ? Convert.ToDecimal(lblVendorAmount.Text) : (decimal?)null;
                        }
                        if (lblCurrentStatus.Text == "In Dispute with Mediator")
                        {
                            oBackOrderBE.AgreedPenaltyCharge = !string.IsNullOrEmpty(lblPlannerAmount.Text) ? Convert.ToDecimal(lblPlannerAmount.Text) : (decimal?)null;
                        }
                        oBackOrderBE.SKUID = Convert.ToInt32(hdnSKUID.Value);
                        oBackOrderBE.PenaltyChargeAgreedWith = null;
                        oBackOrderBE.PenaltyChargeAgreedDatetime = DateTime.Now;
                        oBackOrderBE.IsMedaitionRequiredDailyEmail = false;
                        oBackOrderBE.InventoryActionDate = Common.GetMM_DD_YYYY(lblInventoryInitialActionDate.Text);
                        oBackOrderBE.BOIncurredDate = Common.GetMM_DD_YYYY(lblLastBackOrderDate.Text); // BAck Order Date
                        oBackOrderBE.SiteID = Convert.ToInt32(hdnSiteID.Value);
                        oBackOrderBE.PenaltyChargeID = Convert.ToInt32(hdnPenaltyChargeID.Value);
                        oBackOrderBE.VendorID = Convert.ToInt32(hdnVendorID.Value);
                        oBackOrderBE.IsMedActionFromBOPenaltyOverview = true;
                        int? iResult = oBackOrderBAL.UpdateMediatorReviewStatusBAL(oBackOrderBE);
                    } 
                }
            }
        } 
      
        
        BindOpenBackOrder();
    }

    protected void BtnDecline_Click(object sender, EventArgs e)
    {
        bool IsStatusPendingWithSP = false;
   
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            foreach (GridViewRow row in gvInventoryReview.Rows)
            {
                CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;
                HiddenField hdnODSKUNO = row.FindControl("hdnODSKUNO") as HiddenField;
                HiddenField hdnSiteID = row.FindControl("hdnSiteID") as HiddenField;
                HiddenField hdnPenaltyChargeID = row.FindControl("hdnPenaltyChargeID") as HiddenField;
                Label lblPlannerAmount = row.FindControl("lblPlannerAmount") as Label;
                Label lblVendorAmount = row.FindControl("lblVendorAmount") as Label;
                Label lblInventoryInitialActionDate = row.FindControl("lblInventoryInitialActionDate") as Label;
                Label lblInventoryInitialReview = row.FindControl("lblInventoryInitialReview") as Label; 
                Label lblLastBackOrderDate = row.FindControl("lblLastBackOrderDate") as Label;
                Label lblCurrentStatus = row.FindControl("lblCurrentStatus") as Label;
                HiddenField hdnVendorID = row.FindControl("hdnVendorID") as HiddenField;
                HiddenField hdnSKUID = row.FindControl("hdnSKUID") as HiddenField;


                if (chkSelect != null && hdnODSKUNO != null)
                {
                    if (chkSelect.Checked)
                    {
                        if (lblCurrentStatus.Text == "Vendor Not Replied Mediator to Check" || lblCurrentStatus.Text == "In Dispute with Mediator")
                        {
                            oBackOrderBE.SelectedODSkUCode = hdnODSKUNO.Value;
                            oBackOrderBE.Action = "UpdateMediatorAction";
                            oBackOrderBE.MediatorAction = "Decline Penalty";
                            oBackOrderBE.MediatorStatus = "NoPenaltyApplied";
                            if (lblCurrentStatus.Text == "Vendor Not Replied Mediator to Check")
                            {
                                oBackOrderBE.AgreedPenaltyCharge = !string.IsNullOrEmpty(lblVendorAmount.Text) ? Convert.ToDecimal(lblVendorAmount.Text) : (decimal?)null;
                            }
                            if (lblCurrentStatus.Text == "In Dispute with Mediator")
                            {
                                oBackOrderBE.AgreedPenaltyCharge = !string.IsNullOrEmpty(lblPlannerAmount.Text) ? Convert.ToDecimal(lblPlannerAmount.Text) : (decimal?)null;
                            }
                            oBackOrderBE.SKUID = Convert.ToInt32(hdnSKUID.Value);
                            oBackOrderBE.PenaltyChargeAgreedWith = null;
                            oBackOrderBE.PenaltyChargeAgreedDatetime = DateTime.Now;
                            oBackOrderBE.IsMedaitionRequiredDailyEmail = false;
                            oBackOrderBE.InventoryActionDate = Common.GetMM_DD_YYYY(lblInventoryInitialActionDate.Text);
                            oBackOrderBE.BOIncurredDate = Common.GetMM_DD_YYYY(lblLastBackOrderDate.Text); // BAck Order Date
                            oBackOrderBE.SiteID = Convert.ToInt32(hdnSiteID.Value);
                            oBackOrderBE.PenaltyChargeID = Convert.ToInt32(hdnPenaltyChargeID.Value);
                            oBackOrderBE.VendorID = Convert.ToInt32(hdnVendorID.Value);
                            oBackOrderBE.IsMedActionFromBOPenaltyOverview = true; // status check for updating Med Action from Bo Overview Screen
                            int? iResult = oBackOrderBAL.UpdateMediatorReviewStatusBAL(oBackOrderBE);
                        
                        }
                        else if (lblCurrentStatus.Text == "Pending with Supply Planner")
                        {
                            BindReasonCode();
                            mdlRejectProvisional.Show();
                            IsStatusPendingWithSP = true; // checking when to bind Bo Penalty 
                        }
                        
                    }
                }                
            }
        

        if (IsStatusPendingWithSP == false)
        {
            BindOpenBackOrder();
        }
    }
    
    protected void BtnBack_Click(object sender, EventArgs e)
    {
        string PreviousPage = string.Empty;
        if (Session["InventoryReview1"] != null)
        {
            BackOrderBE oBackOrderBE = new BackOrderBE();

            Hashtable htInventoryReview1 = (Hashtable)Session["InventoryReview1"];

            PreviousPage= (htInventoryReview1.ContainsKey("PreviousPage") && htInventoryReview1["PreviousPage"] != null) ? htInventoryReview1["PreviousPage"].ToString() : null;
           
        }
   
      
        if (Session["OpenFrom"] != null && Session["OpenFrom"].ToString() == "PAQ")
        {
            EncryptQueryString("../BackOrder/PenaltyPendingApprovalOverview.aspx?PreviousPage=PendingPenalty");
      
        }
        else if (PreviousPage == "PTR")
        {
            EncryptQueryString("../BackOrder/PenaltyTrackerReport.aspx?&PreviousPage=IRP");
            Session["InventoryReview1"] = null;
            Session.Remove("InventoryReview1");
        }
        else
        {
            EncryptQueryString("InventoryReview1.aspx?BackToBind=1");
        }
        msSKU.SelectedSKUName = null;
       
    }

    public void RetainSearchData()
    {
        Hashtable htInventoryReview1 = (Hashtable)Session["InventoryReview1"];
 
        //*********** StockPlanner ***************
        string StockPlannerText = (htInventoryReview1.ContainsKey("SelectedSPName") && htInventoryReview1["SelectedSPName"] != null) ? htInventoryReview1["SelectedSPName"].ToString() : "";
        string StockPlannerID = (htInventoryReview1.ContainsKey("SelectedSPUserIDs") && htInventoryReview1["SelectedSPUserIDs"] != null) ? htInventoryReview1["SelectedSPUserIDs"].ToString() : "";
        ucListBox lstRight = msStockPlanner.FindControl("ucLBRight") as ucListBox;
        ucListBox lstLeft = msStockPlanner.FindControl("ucLBLeft") as ucListBox;
        string strDtockPlonnerId = string.Empty;
        if (lstLeft != null && lstRight != null && !string.IsNullOrEmpty(StockPlannerID))
        {
            lstRight.Items.Clear();
            msStockPlanner.SearchStockPlannerClick(StockPlannerText);
            string[] strStockPlonnerIDs = StockPlannerID.Split(',');
            for (int index = 0; index < strStockPlonnerIDs.Length; index++)
            {
                ListItem listItem = lstLeft.Items.FindByValue(strStockPlonnerIDs[index]);
                if (listItem != null)
                {
                    strDtockPlonnerId += strStockPlonnerIDs[index] + ",";
                    lstRight.Items.Add(listItem);
                    lstLeft.Items.Remove(listItem);
                }

            }
            HiddenField hdnSelectedStockPlonner = msStockPlanner.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedStockPlonner.Value = strDtockPlonnerId;
        }

        //********** Vendor ***************
        string txtVendor = (htInventoryReview1.ContainsKey("txtVendor") && htInventoryReview1["txtVendor"] != null) ? htInventoryReview1["txtVendor"].ToString() : "";
        string VendorId = (htInventoryReview1.ContainsKey("SelectedVendorIDs") && htInventoryReview1["SelectedVendorIDs"] != null) ? htInventoryReview1["SelectedVendorIDs"].ToString() : "";
        TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;

        bool IsSearchedByVendorNo = (htInventoryReview1.ContainsKey("IsSearchedByVendorNo") && htInventoryReview1["IsSearchedByVendorNo"] != null) ?Convert.ToBoolean(htInventoryReview1["IsSearchedByVendorNo"]) : false;
        string txtVendorIdText = txtVendorId.Text;
        ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
        string strVendorId = string.Empty;
        //int value;
        lstLeftVendor.Items.Clear();
        if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(VendorId) || !string.IsNullOrEmpty(VendorId)))
        {
            lstLeftVendor.Items.Clear();
     

            if(IsSearchedByVendorNo==true)
            {
                msVendor.SearchVendorNumberClick(txtVendor);
                //parsing successful 
            }
            else
            {
                msVendor.SearchVendorClick(txtVendor);
                //parsing failed. 
            }
           
            if (!string.IsNullOrEmpty(VendorId))
            {
                string[] strIncludeVendorIDs = VendorId.Split(',');
                for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                    if (listItem != null)
                    {

                        strVendorId += strIncludeVendorIDs[index] + ",";
                        lstRightVendor.Items.Add(listItem);
                        lstLeftVendor.Items.Remove(listItem);
                    }
                }
            }
           
            HiddenField hdnSelectedVendor = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedVendor.Value = strVendorId;
        }


        //*********** Site ***************
        string SiteId = (htInventoryReview1.ContainsKey("SelectedSiteId") && htInventoryReview1["SelectedSiteId"] != null) ? htInventoryReview1["SelectedSiteId"].ToString() : "";
        ucListBox lstRightSite = msSite.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftSite = msSite.FindControl("lstLeft") as ucListBox;
        string msSiteid = string.Empty;
        lstRightSite.Items.Clear();
        lstRightSite.Items.Clear();
        msSite.SelectedSiteIDs = "";
        if (lstLeftSite != null && lstRightSite != null && !string.IsNullOrEmpty(SiteId))
        {
     
            lstRightSite.Items.Clear();
            msSite.BindSite();

            string[] strSiteIDs = SiteId.Split(',');
            for (int index = 0; index < strSiteIDs.Length; index++)
            {
                ListItem listItem = lstLeftSite.Items.FindByValue(strSiteIDs[index]);
                if (listItem != null)
                {
                    msSiteid = msSiteid + strSiteIDs[index].ToString() + ",";
                    lstRightSite.Items.Add(listItem);
                    lstLeftSite.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msSite.SelectedSiteIDs))
                msSite.SelectedSiteIDs = msSiteid.Trim(',');
        }

        //************** Od sku ***************
        string OdSkuId = (htInventoryReview1.ContainsKey("SelectedODSKUCodes") && htInventoryReview1["SelectedODSKUCodes"] != null) ? htInventoryReview1["SelectedODSKUCodes"].ToString() : "";
        ucListBox lstRightOD = msSKU.FindControl("lstRight") as ucListBox;
        string hdnValue = string.Empty;
        lstRightOD.Items.Clear();
        if (lstRightOD != null && !string.IsNullOrEmpty(OdSkuId))
        {
            HiddenField hdfSelectedId = msSKU.FindControl("hiddenSelectedId") as HiddenField;
            lstRightOD.Items.Clear();
            string[] strODIDs = OdSkuId.Split(',');
            for (int index = 0; index < strODIDs.Length; index++)
            {
                string Item = strODIDs[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    lstRightOD.Items.Add(listItem);
                    hdnValue += Item + ",";
                }
            }
            hdfSelectedId.Value = hdnValue;
        }

        ////************************* StockPlannerGrouping ***************************
        string StockPlannerGroupingID = (htInventoryReview1.ContainsKey("StockPlannerGroupings") && htInventoryReview1["StockPlannerGroupings"] != null) ? htInventoryReview1["StockPlannerGroupings"].ToString() : null;
        ucListBox lstRightGrouping = multiSelectStockPlannerGrouping.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftGrouping = multiSelectStockPlannerGrouping.FindControl("lstLeft") as ucListBox;
        string msSPgrouping = string.Empty;
     
        if (lstLeftGrouping != null && lstRightGrouping != null && !string.IsNullOrEmpty(StockPlannerGroupingID))
        {
            lstLeftGrouping.Items.Clear();
            multiSelectStockPlannerGrouping.BindStockPlannerGrouping();


            string[] strSPs = StockPlannerGroupingID.Split(',');

            if (!string.IsNullOrEmpty(StockPlannerGroupingID))
            {
                string[] strStockPlannerGroupingIDs = StockPlannerGroupingID.Split(',');
                for (int index = 0; index < strStockPlannerGroupingIDs.Length; index++)
                {
                    ListItem listItem = lstLeftGrouping.Items.FindByValue(strStockPlannerGroupingIDs[index]);
                    if (listItem != null)
                    {
                        msSPgrouping = msSPgrouping + strSPs[index].ToString() + ",";
                        lstRightGrouping.Items.Add(listItem);
                        lstLeftGrouping.Items.Remove(listItem);
                    }
                }

                if (string.IsNullOrEmpty(multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs))
                    multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs = msSPgrouping.Trim(',');
            }
        }

    }

    protected void GvInventoryReview_PageIndexChanging(object sender, GridViewPageEventArgs e) 
    {
        if (ViewState["lstBackOrderBE"] != null)
        {
            gvInventoryReview.DataSource = (List<BackOrderBE>)ViewState["lstBackOrderBE"];
            gvInventoryReview.PageIndex = e.NewPageIndex;
            if (Session["InventoryReview1"] != null)
            {
                Hashtable htInventoryReview1 = (Hashtable)Session["InventoryReview1"];
                htInventoryReview1.Remove("PageIndex");
                htInventoryReview1.Add("PageIndex", e.NewPageIndex);
                Session["InventoryReview1"] = htInventoryReview1;
            }
            gvInventoryReview.DataBind();
        }    
    }

    protected void GvInventoryReview_OnDataBound(object sender, EventArgs e)
    {
        try
        {
           
            for (int i = gvInventoryReview.Rows.Count - 1; i > 0; i--)
            {
                GridViewRow row = gvInventoryReview.Rows[i];
                GridViewRow previousRow = gvInventoryReview.Rows[i - 1];
                var ltODSKU = (HyperLink)row.FindControl("hlODSku");
                var ltODSKUP = (HyperLink)previousRow.FindControl("hlODSku");
                var ltVikingSku = (Label)row.FindControl("ltVikingSku");
                var ltVikingSkuP = (Label)previousRow.FindControl("ltVikingSku");
                var ltStatus = (Label)row.FindControl("lblInventoryStatus");
                var ltStatusP = (Label)previousRow.FindControl("lblInventoryStatus");
                var ltSite = (Label)row.FindControl("ltSite");
                var ltSiteP = (Label)previousRow.FindControl("ltSite");
                var hdnIsVendorCommuncationSent = (HiddenField)row.FindControl("hdnIsVendorCommuncationSent");
                var hdnIsVendorCommuncationSentP = (HiddenField)previousRow.FindControl("hdnIsVendorCommuncationSent");

                if (GetQueryStringValue("VendorID") != null || GetQueryStringValue("FromDashboard") != null)
                {
                }
                else
                {
                    if (ltODSKU.Text == ltODSKUP.Text && ltSite.Text == ltSiteP.Text && hdnIsVendorCommuncationSent.Value == hdnIsVendorCommuncationSentP.Value)
                    {
                        if (ltVikingSku.Text == ltVikingSkuP.Text)
                        {
                            if (ltStatus.Text == ltStatusP.Text)
                            {
                                if (previousRow.Cells[1].RowSpan == 0)
                                {
                                    if (row.Cells[1].RowSpan == 0)
                                    {
                                        previousRow.Cells[0].RowSpan += 2;
                                        previousRow.Cells[1].RowSpan += 2;
                                        previousRow.Cells[2].RowSpan += 2;
                                        previousRow.Cells[3].RowSpan += 2;
                                        previousRow.Cells[4].RowSpan += 2;
                                        previousRow.Cells[5].RowSpan += 2;
                                        previousRow.Cells[6].RowSpan += 2;
                                        previousRow.Cells[7].RowSpan += 2;
                                        previousRow.Cells[8].RowSpan += 2;
                                        previousRow.Cells[9].RowSpan += 2;
                                        previousRow.Cells[10].RowSpan += 2;
                                        previousRow.Cells[11].RowSpan += 2;
                                        previousRow.Cells[12].RowSpan += 2;
                                        previousRow.Cells[13].RowSpan += 2;

                                    }
                                    else
                                    {
                                        previousRow.Cells[0].RowSpan = row.Cells[0].RowSpan + 1;
                                        previousRow.Cells[1].RowSpan = row.Cells[1].RowSpan + 1;
                                        previousRow.Cells[2].RowSpan = row.Cells[2].RowSpan + 1;
                                        previousRow.Cells[3].RowSpan = row.Cells[3].RowSpan + 1;
                                        previousRow.Cells[4].RowSpan = row.Cells[4].RowSpan + 1;
                                        previousRow.Cells[5].RowSpan = row.Cells[5].RowSpan + 1;
                                        previousRow.Cells[6].RowSpan = row.Cells[6].RowSpan + 1;
                                        previousRow.Cells[7].RowSpan = row.Cells[7].RowSpan + 1;
                                        previousRow.Cells[8].RowSpan = row.Cells[8].RowSpan + 1;
                                        previousRow.Cells[9].RowSpan = row.Cells[9].RowSpan + 1;
                                        previousRow.Cells[10].RowSpan = row.Cells[10].RowSpan + 1;
                                        previousRow.Cells[11].RowSpan = row.Cells[11].RowSpan + 1;
                                        previousRow.Cells[12].RowSpan = row.Cells[12].RowSpan + 1;
                                        previousRow.Cells[13].RowSpan = row.Cells[13].RowSpan + 1;

                                    }
                                    row.Cells[0].Visible = false;
                                    row.Cells[1].Visible = false;
                                    row.Cells[2].Visible = false;
                                    row.Cells[3].Visible = false;
                                    row.Cells[4].Visible = false;
                                    row.Cells[5].Visible = false;
                                    row.Cells[6].Visible = false;
                                    row.Cells[7].Visible = false;
                                    row.Cells[8].Visible = false;
                                    row.Cells[9].Visible = false;
                                    row.Cells[10].Visible = false;
                                    row.Cells[11].Visible = false;
                                    row.Cells[12].Visible = false;
                                    row.Cells[13].Visible = false;

                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void BtnConfirm_Click(object sender, EventArgs e) {
        BindPenalty();
    }

    protected void BtnCancel_Click(object sender, EventArgs e) {
        mdlRejectProvisional.Hide();       
    }
    #endregion


    #region Methods...

    private void BindReasonCode() {
        MAS_DeclineReasonCodeBE oMAS_DeclineReasonCodeBE = new MAS_DeclineReasonCodeBE();
        MAS_DeclineReasonCodeBAL oMAS_DeclineReasonCodeBAL = new MAS_DeclineReasonCodeBAL();

        oMAS_DeclineReasonCodeBE.Action = sqlAction.ShowAll;

        List<MAS_DeclineReasonCodeBE> lstMAS_DeclineReasonCodeBE = oMAS_DeclineReasonCodeBAL.GetDeclineReasonCodeBAL(oMAS_DeclineReasonCodeBE);

        if (lstMAS_DeclineReasonCodeBE != null && lstMAS_DeclineReasonCodeBE.Count > 0) {
            FillControls.FillDropDown(ref ddlDeclineCode, lstMAS_DeclineReasonCodeBE, "ReasonCode", "IsDeclineReasonCommentMandatory");
        }
    }

    private void BindPenalty() {
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        BackOrderBE oBackOrderBE = new BackOrderBE();
        string ddlDeclineCodeValue = ddlDeclineCode.SelectedItem.Value;
        string[] strDeclineReasonCode = ddlDeclineCodeValue.Split('-');

        foreach (GridViewRow row in gvInventoryReview.Rows) {
            CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;

            HiddenField hdnODSKUNO = row.FindControl("hdnODSKUNO") as HiddenField;
            HiddenField hdnSiteID = row.FindControl("hdnSiteID") as HiddenField;
            Literal lblInventoryStatus = row.FindControl("lblInventoryStatus") as Literal;
            HiddenField hdnVendorID = row.FindControl("hdnVendorID") as HiddenField;

            if (chkSelect != null && hdnODSKUNO != null) {
                if (chkSelect.Checked)
                {
                    oBackOrderBE.SelectedODSkUCode = hdnODSKUNO.Value;
                    oBackOrderBE.Action = "UpdateInventoryReviewStatus";
                    oBackOrderBE.InventoryReviewStatus = "Declined";
                    oBackOrderBE.SiteID = Convert.ToInt32(hdnSiteID.Value);
                    oBackOrderBE.CurrentBoStatus = lblInventoryStatus.Text;
                    oBackOrderBE.DeclineCode = new MAS_DeclineReasonCodeBE();
                    oBackOrderBE.DeclineCode.DeclineReasonCodeID = Convert.ToInt32(strDeclineReasonCode[0]);
                    if (!string.IsNullOrEmpty(tboxComment.Text))
                    {
                        oBackOrderBE.Comments = tboxComment.Text;
                    }

                    if (GetQueryStringValue("FromDashboard") == null)
                    {
                        int numberOfDays = DateTime.DaysInMonth(Convert.ToInt16(drpYearTo.SelectedValue), Convert.ToInt16(drpMonthTo.SelectedValue));

                        oBackOrderBE.SelectedDateFrom = Convert.ToDateTime(drpYearFrom.SelectedValue + "-" + drpMonthFrom.SelectedValue + "-" + "01");
                        oBackOrderBE.SelectedDateTo = Convert.ToDateTime(drpYearTo.SelectedValue + "-" + drpMonthTo.SelectedValue + "-" + numberOfDays.ToString());
                    }
                    oBackOrderBE.VendorID = Convert.ToInt32(hdnVendorID.Value);

                    int? iResult = oBackOrderBAL.UpdateODSKUStatusBAL(oBackOrderBE);
                }
            }
        }
   
        BindOpenBackOrder();
    }

    public void Pager_Command(object sender, CommandEventArgs e)
    {
        int currentPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currentPageIndx;
        BindOpenBackOrder(currentPageIndx);
    }
    private void BindOpenBackOrder(int Page = 1)
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();
            oBackOrderBE.User = new SCT_UserBE();

            oBackOrderBE.Action = "GetOpenBackOrders";
            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage") == "PTR") //PTR is Penalty Tracker Report
            {                
                if (GetQueryStringValue("CountryID") != "")
                {
                    oBackOrderBE.SelectedCountryIDs = GetQueryStringValue("CountryID");
                }
                if (GetQueryStringValue("SiteID") != "")
                {
                    oBackOrderBE.SelectedSiteIDs = GetQueryStringValue("SiteID");
                }
                if (GetQueryStringValue("VendorID") != "")
                {
                    oBackOrderBE.SelectedVendorIDs = GetQueryStringValue("VendorID");
                }
                if (GetQueryStringValue("StockPlannerID") != "")
                {
                    oBackOrderBE.SelectedStockPlannerIDs = GetQueryStringValue("StockPlannerID");
                }
                if (GetQueryStringValue("StockPlannerGID") != "")
                {
                    oBackOrderBE.StockPlannerGroupings = GetQueryStringValue("StockPlannerGID");
                }
                if (GetQueryStringValue("InventoryReviewStatus") != null && GetQueryStringValue("InventoryReviewStatus") != "")
                {
                    oBackOrderBE.InventoryReviewStatus = GetQueryStringValue("InventoryReviewStatus");
                }
                if (GetQueryStringValue("VendorBOReviewStatus") != null && GetQueryStringValue("VendorBOReviewStatus") != "")
                {
                    oBackOrderBE.VendorBOReviewStatus = GetQueryStringValue("VendorBOReviewStatus");
                    oBackOrderBE.InventoryReviewStatus = "";
                }
                if (GetQueryStringValue("MediatorBOReviewStatus") != null && GetQueryStringValue("MediatorBOReviewStatus") != "")
                {
                    oBackOrderBE.MediatorBOReviewStatus = GetQueryStringValue("MediatorBOReviewStatus");
                    oBackOrderBE.InventoryReviewStatus = "";
                    oBackOrderBE.VendorBOReviewStatus = "";
                }

                if (GetQueryStringValue("ViewCharged") != null && GetQueryStringValue("ViewCharged") != "")
                {
                    oBackOrderBE.ViewByTotalsPenaltiesCharged = "PenaltiesCharged";
                    oBackOrderBE.InventoryReviewStatus = "";
                    oBackOrderBE.VendorBOReviewStatus = "";
                    oBackOrderBE.MediatorBOReviewStatus = "";

                }
                if (GetQueryStringValue("ViewDeclined") != null && GetQueryStringValue("ViewDeclined") != "")
                {
                    oBackOrderBE.ViewByTotalsPenaltiesDeclined = "PenaltiesDeclined";
                    oBackOrderBE.InventoryReviewStatus = "";
                    oBackOrderBE.VendorBOReviewStatus = "";
                    oBackOrderBE.MediatorBOReviewStatus = "";
                }
                if (GetQueryStringValue("ViewPending") != null && GetQueryStringValue("ViewPending") != "")
                {
                    oBackOrderBE.ViewByTotalsPenaltiesPending = "PenaltiesPending";
                    oBackOrderBE.InventoryReviewStatus = "";
                    oBackOrderBE.VendorBOReviewStatus = "";
                    oBackOrderBE.MediatorBOReviewStatus = "";
                }
                oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                oBackOrderBE.User.UserID = Convert.ToInt32(Session["UserID"]);             
                oBackOrderBE.SelectedDateFrom =  Convert.ToDateTime(GetQueryStringValue("DateFrom"));
                oBackOrderBE.SelectedDateTo =  Convert.ToDateTime(GetQueryStringValue("DateTo"));
              
            }
            else
            {
                oBackOrderBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
                oBackOrderBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
                oBackOrderBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
                if (!(string.IsNullOrEmpty(msSKU.SelectedSKUName)))
                {
                    oBackOrderBE.SelectedODSkUCode = msSKU.SelectedSKUName.Trim();
                }
                if (!(string.IsNullOrEmpty(multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs)))
                {
                oBackOrderBE.StockPlannerGroupings = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
                }
                 
                if (GetQueryStringValue("VendorID") != null || GetQueryStringValue("FromDashboard") != null)
                {
                    oBackOrderBE.InventoryReviewStatus = "Pending";
                }
                else if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "POInquiry")
                {
                    oBackOrderBE.InventoryReviewStatus = "";
                    rblApproveType.SelectedValue = "4";
                }
                else
                    oBackOrderBE.InventoryReviewStatus = rblApproveType.SelectedValue == "1" ? "Pending" : (rblApproveType.SelectedValue == "2" ? "Approved" : (rblApproveType.SelectedValue == "3" ? "Declined" : (rblApproveType.SelectedValue == "4" ? "All" : "")));

                oBackOrderBE.VendorBOReviewStatus = rblApproveType.SelectedValue == "8" ? "DisputeDeclined" : (rblApproveType.SelectedValue == "6" ? "Accepted" : (rblApproveType.SelectedValue == "5" ? "Query" : (rblApproveType.SelectedValue == "7" ? "DisputeAccepted" : (rblApproveType.SelectedValue == "11" ? "VendorNotRepliedMediatorToCheck" : ""))));

                oBackOrderBE.MediatorBOReviewStatus = rblApproveType.SelectedValue == "10" ? "MediatorDeclined" : (rblApproveType.SelectedValue == "9" ? "MediatorAccepted" : (rblApproveType.SelectedValue == "12" ? "MediatorAlternateAgreement" : ""));


                if (rblApproveType.SelectedValue == "8")
                {
                    hdnIsInDisputeWithMediator.Value = "Yes";
                }
                else
                {
                    hdnIsInDisputeWithMediator.Value = "No";
                }

                if (rblApproveType.SelectedValue == "11")
                {
                    hdnIsVenNotRepliedMedToCheck.Value = "Yes";
                }
                else
                {
                    hdnIsVenNotRepliedMedToCheck.Value = "No";
                }

                if (rblViewByTotals.SelectedValue == "1")
                {
                    oBackOrderBE.ViewByTotalsPenaltiesCharged = "PenaltiesCharged";
                    oBackOrderBE.InventoryReviewStatus = "";
                    oBackOrderBE.VendorBOReviewStatus = "";
                    oBackOrderBE.MediatorBOReviewStatus = "";

                }
                if (rblViewByTotals.SelectedValue == "2")
                {
                    oBackOrderBE.ViewByTotalsPenaltiesDeclined = "PenaltiesDeclined";
                    oBackOrderBE.InventoryReviewStatus = "";
                    oBackOrderBE.VendorBOReviewStatus = "";
                    oBackOrderBE.MediatorBOReviewStatus = "";
                }
                if (rblViewByTotals.SelectedValue == "3")
                {
                    oBackOrderBE.ViewByTotalsPenaltiesPending = "PenaltiesPending";
                    oBackOrderBE.InventoryReviewStatus = "";
                    oBackOrderBE.VendorBOReviewStatus = "";
                    oBackOrderBE.MediatorBOReviewStatus = "";
                }

                oBackOrderBE.User.UserID = Convert.ToInt32(Session["UserID"]);
                if (GetQueryStringValue("FromDashboard") != null)
                {
                    if (Convert.ToString(Session["Role"]) == "OD - Stock Planner")
                    {
                        oBackOrderBE.User.RoleName = Session["Role"] as string;
                    }
                }

                oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();

                if (GetQueryStringValue("VendorID") != null && GetQueryStringValue("PreviousPage") == null)
                {
                    oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
                }
                else if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "POInquiry")
                {
                    string[] strparams = new string[3];
                    strparams = GetQueryStringValue("Params").Split('~');
                    oBackOrderBE.SelectedVendorIDs = strparams[0];
                    oBackOrderBE.SelectedSiteIDs = strparams[1];
                    oBackOrderBE.PurchaseOrder.Purchase_order = strparams[2];
                }
                else
                {
                    if (GetQueryStringValue("FromDashboard") == null)
                    {
                        int numberOfDays = DateTime.DaysInMonth(Convert.ToInt16(drpYearTo.SelectedValue), Convert.ToInt16(drpMonthTo.SelectedValue)); // for numberofdays in month

                        oBackOrderBE.SelectedDateFrom = Convert.ToDateTime(drpYearFrom.SelectedValue + "-" + drpMonthFrom.SelectedValue + "-" + "01");
                        oBackOrderBE.SelectedDateTo = Convert.ToDateTime(drpYearTo.SelectedValue + "-" + drpMonthTo.SelectedValue + "-" + numberOfDays.ToString());
                    }
                }
            }

                if (IsExportClicked)
                {
                    oBackOrderBE.GridCurrentPageNo = 0;
                    oBackOrderBE.GridPageSize = 0;
                }
                else
                {
                    oBackOrderBE.GridCurrentPageNo = Page;
                    oBackOrderBE.GridPageSize = gridPageSize;
                }

           
             this.SetSession(oBackOrderBE);

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetOpenBackOrderBAL(oBackOrderBE);
           if (GetQueryStringValue("VendorID") == null && GetQueryStringValue("BP") != "PAO")
            {               
                lstBackOrderBE.ToList().ForEach(x => x.SelectedDateFrom = Convert.ToDateTime(drpYearFrom.SelectedValue + "-" + drpMonthFrom.SelectedValue + "-" + "01"));
                lstBackOrderBE.ToList().ForEach(x => x.SelectedDateTo = Convert.ToDateTime(drpYearTo.SelectedValue + "-" + drpMonthTo.SelectedValue + "-" + DateTime.DaysInMonth(Convert.ToInt16(drpYearTo.SelectedValue), Convert.ToInt16(drpMonthTo.SelectedValue)).ToString()));

            }
            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
              
                btnExportToExcel.Visible = true;
                gvInventoryReview.DataSource = null;
                gvInventoryReview.DataSource = lstBackOrderBE;
                pager1.ItemCount = Convert.ToInt32(lstBackOrderBE[0].TotalRecords.ToString());
                ViewState["pagerItemCount"] =   Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                ViewState["lstBackOrderBE"] = lstBackOrderBE;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else
            {
                gvInventoryReview.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
                btnAccept.Visible = false;
                btnDecline.Visible = false;
            }

            gvInventoryReview.PageIndex = Page;
            pager1.CurrentIndex = Page;
            gvInventoryReview.DataBind();
            oBackOrderBAL = null;

            pnlGrid.Visible = true;
            pnlSelection.Visible = false;

            if (oBackOrderBE.InventoryReviewStatus == "Declined")
            {
                btnAccept.Visible = true;
            }

            if(oBackOrderBE.InventoryReviewStatus == "Approved")
            {
                btnAccept.Visible = false;
                btnDecline.Visible = false;
            }

            if (GetQueryStringValue("PreviousPage") == null) 
            {
                int CountPendingWithSupplyPlanner = 0;
                int CountInDisputeWithMedaitor = 0;
                int CountVendorNotRepliedMediatorToCheck = 0;

                if (rblApproveType.SelectedValue == "1" || rblApproveType.SelectedValue == "8" || rblApproveType.SelectedValue == "11" || rblViewByTotals.SelectedValue == "3")
                {
                   
                    if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
                    {
                         
                            btnAccept.Visible = true;
                            btnDecline.Visible = true;
                      
                    }
                }
                else
                {
                    btnAccept.Visible = false;
                    btnDecline.Visible = false;
                }

                if (rblApproveType.SelectedValue == "4")
                {
                    if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
                    {
                        CountPendingWithSupplyPlanner = lstBackOrderBE.Where(x => x.CurrentStatus.Equals("Pending with Supply Planner")).Count();
                        CountInDisputeWithMedaitor = lstBackOrderBE.Where(x => x.CurrentStatus.Equals("In Dispute with Mediator")).Count();
                        CountVendorNotRepliedMediatorToCheck = lstBackOrderBE.Where(x => x.CurrentStatus.Equals("Vendor Not Replied Mediator to Check")).Count();

                        if (CountPendingWithSupplyPlanner != 0 || CountInDisputeWithMedaitor != 0 || CountVendorNotRepliedMediatorToCheck != 0)
                        {
                            btnAccept.Visible = true;
                            btnDecline.Visible = true;
                        }
                        else
                        {
                            btnAccept.Visible = false;
                            btnDecline.Visible = false;
                        }
                    }
                }               

            }

            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage") == "PTR") //PTR is Penalty Tracker Report
            {
                if ((GetQueryStringValue("InventoryReviewStatus") != null && (GetQueryStringValue("InventoryReviewStatus") == "Pending" || GetQueryStringValue("InventoryReviewStatus") == "AllDrill")) ||
                    (GetQueryStringValue("VendorBOReviewStatus") != null && (GetQueryStringValue("VendorBOReviewStatus") == "DisputeDeclined" || GetQueryStringValue("VendorBOReviewStatus") == "AllDisputeDeclined")) ||
                    (GetQueryStringValue("ViewPending") != null && GetQueryStringValue("ViewPending") == "Y"))
                {
                    if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
                    {
                        btnAccept.Visible = true;
                        btnDecline.Visible = true;
                    }
                    else
                    {
                        btnAccept.Visible = false;
                        btnDecline.Visible = false;
                    }
                }
                else
                {
                    btnAccept.Visible = false;
                    btnDecline.Visible = false;
                }
            }                
                
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }
    private void SetSession(BackOrderBE oBackOrderBE)
    {
        Session["InventoryReview1"] = null;
        Session.Remove("InventoryReview1");

        TextBox txt=(TextBox)msStockPlanner.FindControl("txtUserName");
        TextBox txtVendor = (TextBox)msVendor.FindControl("ucVendor").FindControl("txtVendorNo");
        

        Hashtable htInventoryReview1 = new Hashtable();
        htInventoryReview1.Add("SelectedSiteId", oBackOrderBE.SelectedSiteIDs);
        htInventoryReview1.Add("SelectedVendorIDs", oBackOrderBE.SelectedVendorIDs);
        htInventoryReview1.Add("SelectedSPIDs", oBackOrderBE.SelectedStockPlannerIDs);
        htInventoryReview1.Add("SelectedSPUserIDs", msStockPlanner.SelectedStockPlannerUserIDs);
        htInventoryReview1.Add("SelectedSPName", txt.Text);
        htInventoryReview1.Add("txtVendor", txtVendor.Text);
        htInventoryReview1.Add("SelectedODSKUCodes", oBackOrderBE.SelectedODSkUCode);
        htInventoryReview1.Add("SelectedStatus", oBackOrderBE.InventoryReviewStatus);
        htInventoryReview1.Add("VendorId",oBackOrderBE.Vendor.VendorID);
        htInventoryReview1.Add("UserId", oBackOrderBE.User.UserID);
        htInventoryReview1.Add("RoleName", oBackOrderBE.User.RoleName);
        htInventoryReview1.Add("PageIndex", oBackOrderBE.GridCurrentPageNo);
        htInventoryReview1.Add("DateTo", oBackOrderBE.SelectedDateTo);
        htInventoryReview1.Add("DateFrom", oBackOrderBE.SelectedDateFrom);
        htInventoryReview1.Add("Purchase_order", oBackOrderBE.PurchaseOrder.Purchase_order);
        htInventoryReview1.Add("IsSearchedByVendorNo", msVendor.IsSearchedByVendorNo);
        htInventoryReview1.Add("PreviousPage", GetQueryStringValue("PreviousPage"));
        htInventoryReview1.Add("StockPlannerGroupings", multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs); 
        Session["InventoryReview1"] = htInventoryReview1;
    }

    private void GetSession()
    {

        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        BackOrderBE oBackOrderBE = new BackOrderBE();
        oBackOrderBE.User = new SCT_UserBE();

        if (Session["InventoryReview1"] != null)
        {
            Hashtable htInventoryReview1 = (Hashtable)Session["InventoryReview1"];

            oBackOrderBE.SelectedSiteIDs = (htInventoryReview1.ContainsKey("SelectedSiteId") && htInventoryReview1["SelectedSiteId"] != null) ? htInventoryReview1["SelectedSiteId"].ToString() : null;
            oBackOrderBE.SelectedVendorIDs = (htInventoryReview1.ContainsKey("SelectedVendorIDs") && htInventoryReview1["SelectedVendorIDs"] != null) ? htInventoryReview1["SelectedVendorIDs"].ToString() : null;
            oBackOrderBE.SelectedStockPlannerIDs = (htInventoryReview1.ContainsKey("SelectedSPIDs") && htInventoryReview1["SelectedSPIDs"] != null) ? htInventoryReview1["SelectedSPIDs"].ToString() : null;
            oBackOrderBE.SelectedODSkUCode = (htInventoryReview1.ContainsKey("SelectedODSKUCodes") && htInventoryReview1["SelectedODSKUCodes"] != null) ? htInventoryReview1["SelectedODSKUCodes"].ToString() : null;
            oBackOrderBE.InventoryReviewStatus = htInventoryReview1["SelectedStatus"].ToString();
            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.Vendor.VendorID = Convert.ToInt32(htInventoryReview1["VendorId"].ToString());
            oBackOrderBE.User.UserID = Convert.ToInt32(htInventoryReview1["UserId"].ToString());
            oBackOrderBE.User.RoleName = (htInventoryReview1.ContainsKey("RoleName") && htInventoryReview1["RoleName"] != null) ? htInventoryReview1["RoleName"].ToString() : null;

            oBackOrderBE.StockPlannerGroupings = (htInventoryReview1.ContainsKey("StockPlannerGroupings") && htInventoryReview1["StockPlannerGroupings"] != null) ? htInventoryReview1["StockPlannerGroupings"].ToString() : null;

            oBackOrderBE.Action = "GetOpenBackOrders";

            oBackOrderBE.GridCurrentPageNo = Convert.ToInt32(htInventoryReview1["PageIndex"].ToString());         
            oBackOrderBE.GridPageSize = gridPageSize;

            oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
            oBackOrderBE.PurchaseOrder.Purchase_order = (htInventoryReview1.ContainsKey("Purchase_order") && htInventoryReview1["Purchase_order"] != null) ? htInventoryReview1["Purchase_order"].ToString() : null;

            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "POInquiry")  
            {}
            else if ((GetQueryStringValue("VendorID") != null && GetQueryStringValue("PreviousPage") == null))
            {}
            else
            {
                int numberOfDays = DateTime.DaysInMonth(Convert.ToInt16(drpYearTo.SelectedValue), Convert.ToInt16(drpMonthTo.SelectedValue)); // for numberofdays in month

                oBackOrderBE.SelectedDateFrom = Convert.ToDateTime(htInventoryReview1["DateFrom"].ToString());
                oBackOrderBE.SelectedDateTo = Convert.ToDateTime(htInventoryReview1["DateTo"].ToString());
            }        
             

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetOpenBackOrderBAL(oBackOrderBE);

            if (GetQueryStringValue("VendorID") == null && GetQueryStringValue("BP") != "PAO")
            {               
                lstBackOrderBE.ToList().ForEach(x => x.SelectedDateFrom = Convert.ToDateTime(htInventoryReview1["DateFrom"].ToString()));
                lstBackOrderBE.ToList().ForEach(x => x.SelectedDateTo = Convert.ToDateTime(htInventoryReview1["DateTo"].ToString()));
            }
            ViewState["lstBackOrderBE"] = lstBackOrderBE;
           
            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                btnExportToExcel.Visible = true;
                gvInventoryReview.DataSource = null;
                gvInventoryReview.DataSource = lstBackOrderBE;
                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else
            {
                gvInventoryReview.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
                btnAccept.Visible = false;
                btnDecline.Visible = false;
            }
            pager1.CurrentIndex = Convert.ToInt32(htInventoryReview1["PageIndex"].ToString()); ;
            gvInventoryReview.DataBind();
            oBackOrderBAL = null;

            pnlGrid.Visible = true;
            pnlSelection.Visible = false;

            if (oBackOrderBE.InventoryReviewStatus == "Declined")
            {
                btnAccept.Visible = true;
            }

            if (oBackOrderBE.InventoryReviewStatus == "Approved")
            {
                btnAccept.Visible = false;
                btnDecline.Visible = false;
            }
        }
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }
    #endregion  
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;

public partial class DeclineReasonCodeOverview : CommonPage
{
    protected void Page_InIt(object sender, EventArgs e) {
        btnAdd.NavigateUrl = "DeclineReasonCodeEdit.aspx";
        ucExportToExcel1.GridViewControl = grdReasonCode;
        ucExportToExcel1.FileName = "Decline Reason Code";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            BindReasonCode();
        }

    }

    private void BindReasonCode() {
        MAS_DeclineReasonCodeBE oMAS_DeclineReasonCodeBE = new MAS_DeclineReasonCodeBE();
        MAS_DeclineReasonCodeBAL oMAS_DeclineReasonCodeBAL = new MAS_DeclineReasonCodeBAL();

        oMAS_DeclineReasonCodeBE.Action = sqlAction.ShowAll;
       
        List<MAS_DeclineReasonCodeBE> lstMAS_DeclineReasonCodeBE = oMAS_DeclineReasonCodeBAL.GetDeclineReasonCodeBAL(oMAS_DeclineReasonCodeBE);

        if (lstMAS_DeclineReasonCodeBE != null && lstMAS_DeclineReasonCodeBE.Count > 0) {
            grdReasonCode.DataSource = lstMAS_DeclineReasonCodeBE;
            grdReasonCode.DataBind();
            ViewState["lstMAS_DeclineReasonCodeBE"] = lstMAS_DeclineReasonCodeBE;
        }

        
    }

    protected void grdReasonCode_RowDataBound(object sender, GridViewRowEventArgs e) {
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {
        return Utilities.GenericListHelper<MAS_DeclineReasonCodeBE>.SortList((List<MAS_DeclineReasonCodeBE>)ViewState["lstMAS_DeclineReasonCodeBE"], e.SortExpression, e.SortDirection).ToArray();
    }
    
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using BaseControlLibrary;
using System.Collections;
using Utilities;

public partial class DeclineReport : CommonPage {

    bool IsExportClicked = false;
    private const int gridPageSize = 50;

    protected void Page_InIt(object sender, EventArgs e) {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            pnlSearchScreen.Visible = true;
            pnlPotentialOutput.Visible = false;
            //btnExportToExcel.Visible = false;
            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = true;

            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
           
        }

        msCountry.SetCountryOnPostBack();       
        msStockPlanner.SetSPOnPostBack();
        msDeclineReasonCode.SetCodeOnPostBack();
        //btnExportToExcel.CurrentPage = this;
        //btnExportToExcel.FileName = "DeclineReport";        
    }

    public void pager_Command(object sender, CommandEventArgs e) {
        int currentPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currentPageIndx;
        BindBackOrder(currentPageIndx);
    }

    protected void btnSearch_Click(object sender, EventArgs e) {
        this.BindBackOrder();
    }   

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {
        return Utilities.GenericListHelper<BackOrderBE>.SortList((List<BackOrderBE>)ViewState["lstBOPenaltyTracker"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void btnExport_Click(object sender, EventArgs e) {
        try {
            IsExportClicked = true;
            BindBackOrder();
            LocalizeGridHeader(gvBOReport);

            WebCommon.Export("DeclineReport", gvBOReport);
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e) {
        //this.ClearCriteria();
        pnlSearchScreen.Visible = true;
        pnlPotentialOutput.Visible = false;
        btnExportToExcel.Visible = false;
        ViewState["lstBOPenaltyTracker"] = null;
    }

    public override void TemplateSelectedIndexChanged() {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstSelectedVendor");
        ListBox lstVendor = (ListBox)msVendor.FindControl("lstVendor");

        if (VendorTemplateId > 0) {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);
            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0) {
                lstSelectedVendor.Items.Clear();
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++) {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                }
            }
        }
        else {
            lstSelectedVendor.Items.Clear();
        }
    }

    //----------------------------------


    private void BindBackOrder(int Page = 1) {
        try {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();
            oBackOrderBE.User = new SCT_UserBE();

            oBackOrderBE.Action = "GetDeclineReport";
            if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs) && !string.IsNullOrWhiteSpace(msCountry.SelectedCountryIDs))
                oBackOrderBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;

            string SelectedVendorIDs = this.GetSelectedVendorIds(msVendor);

            if (!string.IsNullOrEmpty(SelectedVendorIDs) && !string.IsNullOrWhiteSpace(SelectedVendorIDs))
                oBackOrderBE.SelectedVendorIDs = SelectedVendorIDs;

            if (!string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs) && !string.IsNullOrWhiteSpace(msStockPlanner.SelectedStockPlannerIDs))
                oBackOrderBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;

            if (!string.IsNullOrEmpty(msDeclineReasonCode.SelectedDeclineReasonCodeIDs) && !string.IsNullOrWhiteSpace(msDeclineReasonCode.SelectedDeclineReasonCodeIDs))
                oBackOrderBE.SelectedDeclineReasonCodeIDs = msDeclineReasonCode.SelectedDeclineReasonCodeIDs;

           
            if (IsExportClicked) {
                oBackOrderBE.GridCurrentPageNo = 0;
                oBackOrderBE.GridPageSize = 0;
            }
            else {
                oBackOrderBE.GridCurrentPageNo = Page;
                oBackOrderBE.GridPageSize = gridPageSize;
            }

           // this.SetSession(oBackOrderBE);

            oBackOrderBE.DeclineDateFrom = Common.GetYYYY_MM_DD(hdnJSFromDt.Value);
            oBackOrderBE.DeclineDateTo = Common.GetYYYY_MM_DD(hdnJSToDt.Value);

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetDeclineReportBAL(oBackOrderBE);
            //lstBackOrderBE.ToList().ForEach(i => i.PotentialPenaltyCharge = (i.PotentialPenaltyCharge * i.NoofBOWithPenalities));


            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0) {
                // Session["lstBackOrderBE"] = null;
                btnExportToExcel.Visible = true;
                gvBOReport.DataSource = null;
                gvBOReport.DataSource = lstBackOrderBE;
                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                ViewState["lstBackOrderBE"] = lstBackOrderBE;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else {
                gvBOReport.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;                
            }

            gvBOReport.PageIndex = Page;
            pager1.CurrentIndex = Page;
            gvBOReport.DataBind();
            oBackOrderBAL = null;

            txtFromDate.Text = hdnJSFromDt.Value;
            txtToDate.Text = hdnJSToDt.Value;

            pnlPotentialOutput.Visible = true;
            pnlSearchScreen.Visible = false;

            LocalizeGridHeader(gvBOReport);
        }
        catch (Exception ex) {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }   

    private string GetSelectedVendorIds(ucMultiSelectVendor multiSelectVendor) {
        ucListBox lstSelectedVendor = (ucListBox)multiSelectVendor.FindControl("lstSelectedVendor");
        var vendorIds = string.Empty;
        for (int index = 0; index < lstSelectedVendor.Items.Count; index++) {
            if (index.Equals(0))
                vendorIds = lstSelectedVendor.Items[index].Value;
            else
                vendorIds = string.Format("{0},{1}", vendorIds, lstSelectedVendor.Items[index].Value);
        }
        return vendorIds;
    }
}
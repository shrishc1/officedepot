﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="InventoryReview1.aspx.cs" Inherits="InventoryReview1"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlannerGrouping.ascx" TagName="MultiSelectStockPlannerGrouping"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .radio-fix tr td:nth-child(2) {
            width: 245px;
        }

        .radio-fix tr td:nth-child(3) {
            width: 170px;
        }

        .wordbreak {
            word-break: break-all;
        }

        .PopupClass {
            width: 40%;
        }

        .PopupClass1 {
            width: 65%;
            position: absolute;
            z-index: 3000;
            border: 0px solid #111;
            background-color: #FFFFFF;
            padding: 5px;
            opacity: 1.00;
            top: 150px !important;
            left: 380px !important;
        }

        .HideShow {
            display: none;
        }
    </style>
    <script src="../../../Scripts/jquery.tooltip.min.js" type="text/javascript"></script>
    <style type="text/css">
        #tooltip {
            position: absolute;
            z-index: 2000;
            border: 1px solid #111;
            background-color: #ffcccc;
            padding: 5px;
            opacity: 1.00;
            color: #4C4A4A;
        }

            #tooltip h3, #tooltip div {
                margin: 0;
            }
    </style>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });

            $(function () {
                InitializeToolTip();
                function InitializeToolTip() {
                    $(".ImgInfo").tooltip({
                        track: true,
                        delay: 0,
                        showURL: false,
                        fade: 100,
                        extraClass: "PopupClass",
                        bodyHandler: function () {
                            return $("#tooltip1").html();
                        },
                        showURL: false
                    });


                    $(".ImgInfo1").tooltip({
                        track: true,
                        delay: 0,
                        showURL: false,
                        fade: 100,
                        extraClass: "PopupClass1",
                        bodyHandler: function () {
                            return $("#tooltip2").html();
                        },
                        showURL: false
                    });
                }
            });

        });

        var gridCheckedCount = 0;



        $(document).ready(function () {
            $('[id$=chkSelectAllText]').click(function () {
                $("[id$='chkSelect']").attr('checked', this.checked);
                gridCheckedCount = $('[id$=chkSelect]:checked').length;
            });


        });

        function CheckUncheckAllCheckBoxAsNeeded(input) {
            if ($('[id$=chkSelect]:checked').length == $('[id$=chkSelect]').length) {
                $("[id$=chkSelectAllText]").attr('checked', true);
                gridCheckedCount = $('[id$=chkSelect]:checked').length;
            }
            if (!$(input).is(":checked")) {
                $("[id$=chkSelectAllText]").attr('checked', false);
                gridCheckedCount = $('[id$=chkSelect]:checked').length;
            }

            if ($(input).is(":checked")) {
                gridCheckedCount = $('[id$=chkSelect]:checked').length;
            }

        }

        function CheckAtleastOneCheckBox() {

            var selectCheckboxToAccept = '<%=selectCheckboxToAccept%>';
            if (gridCheckedCount == 0) {
                alert(selectCheckboxToAccept);
                return false
            }

            var isAnyApprovedOrDeclined = false;
            $('#<%=gvInventoryReview.ClientID%>  tr').each(function () {
                if ($(this).find('input:checkbox').is(':checked')) {
                    var lblInventoryStatus = $(this).find('[id$= lblInventoryStatus]').text();
                    if (lblInventoryStatus == 'Approved' || lblInventoryStatus == 'Declined') {
                        isAnyApprovedOrDeclined = true;
                    }
                }
            });
            if (isAnyApprovedOrDeclined == true) {
                var result = confirm('<%=MsgChangestatus%>');
                if (result) {
                    
                    return true;
                }
                else {
                    
                    return false;
                }
            }
        }

    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnIsVenNotRepliedMedToCheck" runat="server" />
    <asp:HiddenField ID="hdnIsInDisputeWithMediator" runat="server" />
    <h2>
        <cc1:ucLabel ID="lblBackOrderPenaltiesPendingApproval" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSelection" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">:
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">:
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">:
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblInventoryGroup" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlannerGrouping runat="server" ID="multiSelectStockPlannerGrouping" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblODSKUCode" runat="server" Text="OD SKU Code">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">:
                        </td>
                        <td>
                            <cc1:MultiSelectSKU runat="server" ID="msSKU" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblPeriod" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">:
                        </td>
                        <td>
                            <table width="40%" border="0" cellspacing="5" cellpadding="0" id="tblDate">
                                <tr>
                                    <td>
                                        <cc1:ucDropdownList ID="drpMonthFrom" runat="server" Width="80px">
                                          
                                            <asp:ListItem Text="January" Value="01">
                                            </asp:ListItem>
                                            <asp:ListItem Text="February" Value="02">
                                            </asp:ListItem>
                                            <asp:ListItem Text="March" Value="03">
                                            </asp:ListItem>
                                            <asp:ListItem Text="April" Value="04">
                                            </asp:ListItem>
                                            <asp:ListItem Text="May" Value="05">
                                            </asp:ListItem>
                                            <asp:ListItem Text="June" Value="06">
                                            </asp:ListItem>
                                            <asp:ListItem Text="July" Value="07">
                                            </asp:ListItem>
                                            <asp:ListItem Text="August" Value="08">
                                            </asp:ListItem>
                                            <asp:ListItem Text="September" Value="09">
                                            </asp:ListItem>
                                            <asp:ListItem Text="October" Value="10">
                                            </asp:ListItem>
                                            <asp:ListItem Text="November" Value="11">
                                            </asp:ListItem>
                                            <asp:ListItem Text="December" Value="12">
                                            </asp:ListItem>
                                        </cc1:ucDropdownList>
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="drpYearFrom" runat="server" Width="60px">
                                        </cc1:ucDropdownList>
                                    </td>
                                    <td style="text-align: center">
                                        <cc1:ucLabel ID="lblTo" runat="server" Text="To">
                                        </cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="drpMonthTo" runat="server" Width="80px">
                                           
                                            <asp:ListItem Text="January" Value="01">
                                            </asp:ListItem>
                                            <asp:ListItem Text="February" Value="02">
                                            </asp:ListItem>
                                            <asp:ListItem Text="March" Value="03">
                                            </asp:ListItem>
                                            <asp:ListItem Text="April" Value="04">
                                            </asp:ListItem>
                                            <asp:ListItem Text="May" Value="05">
                                            </asp:ListItem>
                                            <asp:ListItem Text="June" Value="06">
                                            </asp:ListItem>
                                            <asp:ListItem Text="July" Value="07">
                                            </asp:ListItem>
                                            <asp:ListItem Text="August" Value="08">
                                            </asp:ListItem>
                                            <asp:ListItem Text="September" Value="09">
                                            </asp:ListItem>
                                            <asp:ListItem Text="October" Value="10">
                                            </asp:ListItem>
                                            <asp:ListItem Text="November" Value="11">
                                            </asp:ListItem>
                                            <asp:ListItem Text="December" Value="12">
                                            </asp:ListItem>
                                        </cc1:ucDropdownList>
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="drpYearTo" runat="server" Width="60px">
                                        </cc1:ucDropdownList>
                                    </td>
                                    <td>
                                        <div>
                                            <img src="../../../Images/info_button1.gif" runat="server" id="imgInfo" class="ImgInfo"
                                                align="absMiddle" border="0" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStatus" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">:
                        </td>
                        <td colspan="2" class="nobold radiobuttonlist" valign="middle">
                            <cc1:ucRadioButtonList ID="rblApproveType" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                <asp:ListItem Text="lstCheckAll" Value="4">
                                </asp:ListItem>
                                <asp:ListItem Text="Pending (with Supply Planner)" Value="1" Selected="True">
                                </asp:ListItem>
                                <asp:ListItem Text="Declined by Supply Planner" Value="3">
                                </asp:ListItem>
                                <asp:ListItem Text="Pending (with Vendor)" Value="2">
                                </asp:ListItem>
                                <asp:ListItem Text="Penalty Accepted by Vendor" Value="6">
                                </asp:ListItem>
                                <asp:ListItem Text="Vendor Not Replied, Mediator to Check" Value="11">
                                </asp:ListItem>
                                <asp:ListItem Text="In Dispute with Supply Planner" Value="5">
                                </asp:ListItem>
                                <asp:ListItem Text="Declined by Supply Planner after Dispute" Value="7">
                                </asp:ListItem>
                                <asp:ListItem Text="In dispute with Mediator" Value="8">
                                </asp:ListItem>
                                <asp:ListItem Text="Declined by Mediator" Value="10">
                                </asp:ListItem>
                                <asp:ListItem Text="Accepted by Mediator" Value="9">
                                </asp:ListItem>
                                <asp:ListItem Text="Alternate Agreement by Mediator" Value="12">
                                </asp:ListItem>
                            </cc1:ucRadioButtonList>
                            <cc1:ucRadioButtonList ID="Test" runat="server" Visible="false">
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblViewByTotals" runat="server" Text="View By Totals">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">:
                        </td>
                        <td colspan="2" class="nobold radiobuttonlist" valign="middle">
                            <cc1:ucRadioButtonList ID="rblViewByTotals" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Penalties Charged" Value="1">
                                </asp:ListItem>
                                <asp:ListItem Text="Penalties Declined" Value="2">
                                </asp:ListItem>
                                <asp:ListItem Text="Penalties Pending" Value="3">
                                </asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="BtnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <div id="tooltip1" style="display: none;">
                <table width="100%" cellspacing="10">
                    <tr>
                        <td style="width: 40%;">
                            <cc1:ucLabel runat="server" ID="lblNewStatus"></cc1:ucLabel>
                        </td>
                        <td style="width: 1%;"></td>
                        <td style="width: 58%;">
                            <cc1:ucLabel runat="server" ID="lblNewDescription"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblPendingWithSupplyPlanner"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblPenaltyNotActionedBySP"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblDeclinedBySP"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblDeclinedBySPInitialReview"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblPendingWithVendor"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblPenaltyAcceptedByPlanner"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblPenaltyAcceptedByVendor"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblAcceptedByVendorInitialReview"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblInDisputeWithSP"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblPenaltyNotAcceptedByVendorInitialReview"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblDeclinedBySPafterDispute"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblAcceptedBySPfromVendor"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblInDisputeWithMediator"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblNotAcceptedBySPfromVendor"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblDeclinedByMediator"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblChargeDeclinedByMediator"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblAcceptedByMediator"></cc1:ucLabel>
                        </td>
                        <td>:
                        </td>
                        <td>
                            <cc1:ucLabel runat="server" ID="lblChargeAcceptedByMediator"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </div>
            <cc1:ucPanel ID="pnlGrid" runat="server" Visible="false">
                <table border="0" style="width: 140%">
                    <tr>
                        <td style="font-weight: bold; width: 40px" valign="top">
                            <cc1:ucLabel ID="lblView" Text="View" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 10px" valign="top">:
                        </td>
                        <td style="width: 180px">
                            <cc1:ucRadioButtonList ID="rblViewByStatus" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                                <asp:ListItem Text="View by User" Value="0" Selected="True">
                                </asp:ListItem>
                                <asp:ListItem Text="View By Days" Value="1">
                                </asp:ListItem>
                            </cc1:ucRadioButtonList>
                        </td>
                        <td valign="top" style="width: 600px">
                            <img src="../../../Images/info_button1.gif" runat="server" id="Img1" class="ImgInfo1"
                                align="absMiddle" border="0" />
                        </td>
                        <td>
                            <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="BtnExport_Click"
                                Width="109px" Height="20px" />
                        </td>
                    </tr>
                </table>
                <div id="tooltip2" style="display: none;">
                    <table width="100%" cellspacing="4">
                        <tr>
                            <td style="width: 30%;">
                                <cc1:ucLabel runat="server" ID="lblCurrentStatusHeading">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblPendingWithSupplyPlanner_1">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblCurrentPendingWithSupplyPlannerText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblDeclinedBySP_1">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblCurrentDeclinedBySPInitialReviewText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblPendingWithVendor_1">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblCurrentPendingWithVendorText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblPenaltyAcceptedByVendor_1">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblCurrentPenaltyAcceptedByVendorText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblInDisputeWithSP_1">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblCurrentInDisputeWithSPText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblDeclinedBySPafterDispute_1">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblCurrentDeclinedBySPafterDisputeText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblInDisputeWithMediator_1">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblCurrentInDisputeWithMediatorText1">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" />
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblCurrentInDisputeWithMediatorText2">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblDeclinedByMediator_1">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblCurrentDeclinedByMediatorText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblAcceptedByMediator_1">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblCurrentAcceptedByMediatorText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">
                                <br />
                                <cc1:ucLabel runat="server" ID="lblIRStatusHeading">
                                </cc1:ucLabel>
                            </td>
                            <td style="width: 70%;"></td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblPending">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblIRPendingText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblApproved">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblIRApprovedText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblDeclined_1">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblIRDeclinedText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">
                                <br />
                                <cc1:ucLabel runat="server" ID="lblVendorStatusHeading">
                                </cc1:ucLabel>
                            </td>
                            <td style="width: 70%;"></td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblAccepted">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblVendorAcceptedText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblDisputed">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblVendorDisputedText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">
                                <br />
                                <cc1:ucLabel runat="server" ID="lblIRSecondaryStatusHeading">
                                </cc1:ucLabel>
                            </td>
                            <td style="width: 70%;"></td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblDisputeAccepted">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblDisputeAcceptedText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblDisputeDeclined">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblDisputeDeclinedText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">
                                <br />
                                <cc1:ucLabel runat="server" ID="lblMediatorStatusHeading">
                                </cc1:ucLabel>
                            </td>
                            <td style="width: 70%;"></td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblAccepted_1">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblMediatorAcceptedText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblDeclined_2">
                                </cc1:ucLabel>
                            </td>
                            <td>
                                <cc1:ucLabel runat="server" ID="lblMediatorDeclinedText">
                                </cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <div style="overflow-x: scroll;">
                    <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table"
                        style="width: 200%;">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucGridView ID="gvInventoryReview" runat="server" CssClass="grid gvclass searchgrid-1"
                                    GridLines="Both" OnPageIndexChanging="GvInventoryReview_PageIndexChanging">
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">
                                            <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                        </div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="SelectAllCheckBox">
                                            <HeaderStyle HorizontalAlign="Left" Width="20px" />
                                            <ItemStyle Width="20px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <cc1:ucCheckbox runat="server" ID="chkSelectAllText" CssClass="checkbox-input" onclick="SelectAllGrid(this);" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnODSKUNO" Value='<%# Eval("SKU.OD_SKU_NO") %>' runat="server" />
                                                <asp:HiddenField ID="hdnIsVendorCommuncationSent" Value='<%# Eval("IsVendorCommuncationSent") %>' runat="server" />
                                                <asp:HiddenField ID="hdnSiteid" Value='<%# Eval("SiteID") %>' runat="server" />
                                                <asp:HiddenField ID="hdnVendorID" Value='<%# Eval("Vendor.VendorID") %>' runat="server" />
                                                <asp:HiddenField ID="hdnPenaltyChargeID" Value='<%# Eval("PenaltyChargeID") %>' runat="server" />
                                                <asp:HiddenField ID="hdnSKUID" Value='<%# Eval("SKUID") %>' runat="server" />
                                                <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" onclick="CheckUncheckAllCheckBoxAsNeeded(this);"
                                                    Visible='<%# ((Convert.ToString( Eval("IsVendorCommuncationSent")) == "No")
                                                            || (Convert.ToString( Eval("CurrentStatus")) == "In Dispute with Mediator")
                                                            || (Convert.ToString( Eval("CurrentStatus")) == "Vendor Not Replied Mediator to Check")
                                                        )? true: false %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Site" SortExpression="SKU.SiteName">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltSite" Text='<%#Eval("SKU.SiteName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ODSKU" SortExpression="SKU.OD_SKU_NO">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlODSku" runat="server" Text='<%# Eval("SKU.OD_SKU_NO") %>'
                                                    NavigateUrl='<%# EncryptQuery("SKUDetailedReview.aspx?&ODSKUNO="+Eval("SKU.OD_SKU_NO")+"&Status="+Eval("InventoryReviewStatus")+"&VikingCode="+Eval("SKU.Direct_SKU")+"&BP="+GetQueryStringValue("BP")+"&Site="+Eval("SKU.SiteName")+"&Purchase_order=" + GetQueryStringValue("Purchase_order")+"&Dateto="+ Eval("SelectedDateTo") +"&DateFrom="+Eval("SelectedDateFrom")+"&IsMailSend="+Eval("IsVendorCommuncationSent"))%>'></asp:HyperLink>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="VikingSku" SortExpression="SKU.Direct_SKU">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltVikingSku" Text='<%#Eval("SKU.Direct_SKU") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>



                                        <asp:TemplateField HeaderText="VendorCode" SortExpression="SKU.Vendor_Code">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltCountry" Text='<%#Eval("SKU.Vendor_Code") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description" SortExpression="SKU.DESCRIPTION">
                                            <HeaderStyle Width="160px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lt2" Text='<%#Eval("SKU.DESCRIPTION") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="160px" Font-Size="10px" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="BOCount" SortExpression="NoofBO">
                                            <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lt3" Text='<%#Eval("NoofBO") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="QtyonBO" SortExpression="QtyonBackOrder">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lt4" Text='<%#Convert.ToInt64(Math.Round(Convert.ToDouble(Eval("QtyonBackOrder")))) %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="#BOwithpenalties" SortExpression="NoofBOWithPenalities">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lt5" Text='<%#Eval("NoofBOWithPenalities") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Overdue PO#" SortExpression="Purchase_Order">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltPurchase_Order" Text='<%#Eval("PurchaseOrder.Purchase_order") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="Vendor.StockPlannerNumber">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="StockPlannerName" SortExpression="Vendor.StockPlannerName">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Stock Planner Group" SortExpression="StockPlannerGroupings">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltStockPlannerGroupings" Text='<%#Eval("StockPlannerGroupings") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="FixedVendorNumber" SortExpression="Vendor.Vendor_No">
                                            <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="FixedVendorName" SortExpression="Vendor.VendorName">
                                            <HeaderStyle Width="160px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="160px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Current Status" SortExpression="InventoryReviewStatus">
                                            <HeaderStyle HorizontalAlign="Left" Width="120px" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblCurrentStatus" Visible="true" Text='<%#Eval("CurrentStatus") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="120px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PotentialPenaltyCharge" SortExpression="PotentialPenaltyCharge">
                                            <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lt7" Text='<%#Eval("PotentialPenaltyCharge") %>'></cc1:ucLabel>
                                                <asp:Literal runat="server" ID="lblInventoryStatus" Visible="false" Text='<%#Eval("InventoryReviewStatus") %>'></asp:Literal>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Planner Initial Review" SortExpression="InventoryInitialReview">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblInventoryInitialReview" Text='<%#Eval("InventoryInitialReview") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Decline Reason" SortExpression="DeclineCode.ReasonCode">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblDeclineReason" Text='<%#Eval("DeclineCode.ReasonCode") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User" SortExpression="PlannerInitialUser">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblPlannerInitialUser" Text='<%#Eval("PlannerInitialUser") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" SortExpression="InventoryInitialActionDate">
                                            <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblInventoryInitialActionDate" Text='<%# Convert.ToString(Eval("InventoryInitialActionDate")).Equals("01/01/1900") ? "" : Eval("InventoryInitialActionDate")  %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vendor Review" SortExpression="VendorReview">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorReview" Text='<%#Eval("VendorReview") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount" SortExpression="VendorAmount">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorAmount" Text='<%#Eval("VendorAmount") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User" SortExpression="VendorUser">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorUser" Text='<%#Eval("VendorUser") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" SortExpression="VendorActionTakenDate">
                                            <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorActionTakenDate" Text='<%# Convert.ToString(Eval("VendorActionTakenDate")).Equals("01/01/1900") ? "" : Eval("VendorActionTakenDate") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Planner Secondary Review" SortExpression="PlannerSecondaryReview">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblPlannerSecondaryReview" Text='<%#Eval("PlannerSecondaryReview") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount" SortExpression="PlannerAmount">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblPlannerAmount" Text='<%#Eval("PlannerAmount") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User" SortExpression="PlannerSecondaryUser">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblPlannerSecondaryUser" Text='<%#Eval("PlannerSecondaryUser") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" SortExpression="PlannerSecondaryActionTakenDate">
                                            <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblPlannerSecondaryActionTakenDate" Text='<%# Convert.ToString(Eval("PlannerSecondaryActionTakenDate")).Equals("01/01/1900") ? "" : Eval("PlannerSecondaryActionTakenDate")%>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Font-Size="60px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mediator Review" SortExpression="MediatorReview">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblMediatorReview" Text='<%#Eval("MediatorReview") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount" SortExpression="MediatorAmount">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblMediatorAmount" Text='<%#Eval("MediatorAmount") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User" SortExpression="MediatorUser">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblMediatorUser" Text='<%#Eval("MediatorUser") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" SortExpression="MediatorActionTakenDate">
                                            <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblMediatorActionTakenDate" Text='<%# Convert.ToString(Eval("MediatorActionTakenDate")).Equals("01/01/1900") ? "" : Eval("MediatorActionTakenDate") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vendor Comments" SortExpression="VendorComment">
                                            <HeaderStyle Width="120px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorComment" Text='<%#Eval("VendorComment") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Planner Comments" SortExpression="SPComment">
                                            <HeaderStyle Width="120px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblSPComment" Text='<%#Eval("SPComment") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mediator Comments" SortExpression="MediatorComments">
                                            <HeaderStyle Width="120px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblMediatorComments" Text='<%#Eval("MediatorComments") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Back Order Date" SortExpression="LastBackOrderDate">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblLastBackOrderDate" Text='<%#Eval("LastBackOrderDate") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Planner Initial Review" SortExpression="InventoryInitialReview">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblPlannerInitialReviews" Text='<%#Eval("InventoryInitialReview") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Days Taken" SortExpression="PlannerInitialActionDays">
                                            <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblPlannerInitialActionDays" Text='<%#  Convert.ToInt32(Eval("PlannerInitialActionDays")) == 0 ? "" : Eval("PlannerInitialActionDays") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vendor Review" SortExpression="VendorReview">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorReviews" Text='<%#Eval("VendorReview") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Days Taken" SortExpression="VendorActionDays">
                                            <HeaderStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblVendorActionDays" Text='<%# Convert.ToInt32(Eval("PlannerInitialActionDays")) == 0 ? "" :Convert.ToInt32(Eval("VendorActionDays")) == 0 ? "" : Eval("VendorActionDays") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Planner Secondary Review" SortExpression="PlannerSecondaryReview">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblPlannerSecondaryReviews" Text='<%#Eval("PlannerSecondaryReview") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Days Taken" SortExpression="PlannerSecondaryActionDays">
                                            <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblPlannerSecondaryActionDays" Text='<%# Convert.ToInt32(Eval("PlannerSecondaryActionDays")) == 0 ? "" : Eval("PlannerSecondaryActionDays") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MediatorReview" SortExpression="MediatorReview">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblMediatorReviews" Text='<%#Eval("MediatorReview") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Days Taken" SortExpression="MediatorActionDays">
                                            <HeaderStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblMediatorActionDays" Text='<%# Convert.ToInt32(Eval("MediatorActionDays")) == 0 ? "" : Eval("MediatorActionDays")%>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Completed Date" SortExpression="CompletedDate">
                                            <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblCompletedDate" Text='<%# Convert.ToString(Eval("CompletedDate")).Equals("01/01/1900") ? "" : Eval("CompletedDate") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Days Taken" SortExpression="TotalDays">
                                            <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lblTotalDays" Text='<%# !Convert.ToString(Eval("CompletedDate")).Equals("01/01/1900") && Convert.ToString(Eval("CompletedDate")) != "" ? 
                                                                                              Convert.ToInt32(Eval("TotalDays")) >= 0 ? Eval("TotalDays") : "" : "" %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>


                                    </Columns>
                                </cc1:ucGridView>
                            </td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td>
                            <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="Pager_Command" GenerateGoToSection="true"></cc1:PagerV2_8>
                        </td>
                    </tr>
                </table>
                <div class="button-row">
                    <cc1:ucButton ID="btnAccept" runat="server" Text="Accept" CssClass="button" OnClick="BtnAccept_Click"
                        OnClientClick="return CheckAtleastOneCheckBox();" />
                    <cc1:ucButton ID="btnDecline" runat="server" Text="Decline" CssClass="button" OnClick="BtnDecline_Click"
                        OnClientClick="return CheckAtleastOneCheckBox();" />
                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="BtnBack_Click" />
                </div>
            </cc1:ucPanel>
        </div>
    </div>
    <%---WARNING popup Decline start--%>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnRejectProvisional" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlRejectProvisional" runat="server" TargetControlID="btnRejectProvisional"
                PopupControlID="pnlRejectProvisional" BackgroundCssClass="modalBackground" BehaviorID="RejectProvisional"
                DropShadow="false" />
            <asp:Panel ID="pnlRejectProvisional" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3>
                        <cc1:ucLabel ID="lblWarningInfo" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <cc1:ucLabel ID="lblDeclineReasonMsg" runat="server"></cc1:ucLabel>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblReason" runat="server" isRequired="true" Style="display: inline-block; width: 58px;"></cc1:ucLabel>&nbsp;
                                <cc1:ucDropdownList ID="ddlDeclineCode" runat="server" Width="300px">
                                </cc1:ucDropdownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblComments" runat="server" isRequired="true" Text="Comments" Style="display: none; float: left;"></cc1:ucLabel>&nbsp;
                                <cc1:ucTextbox ID="tboxComment" runat="server" isRequired="true" Style="display: none;"
                                    TextMode="MultiLine" Height="48px" Width="300px"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <cc1:ucButton ID="btnConfirm" runat="server" CssClass="button" OnCommand="BtnConfirm_Click"
                                        OnClientClick="return IsCommentEntered()" />
                                    &nbsp;
                                    <cc1:ucButton ID="btnCancel" runat="server" CssClass="button" OnCommand="BtnCancel_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnConfirm" />
            <asp:PostBackTrigger ControlID="btnCancel" />
            <asp:AsyncPostBackTrigger ControlID="ddlDeclineCode" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript" language="javascript"> 

        $(document).ready(function () {

            var declineCodevalue = $('#<%=ddlDeclineCode.ClientID%>').val();

            if (declineCodevalue != "") {

                var ddlDeclineCodeValue = declineCodevalue;
                var arr = ddlDeclineCodeValue.split('-');
                if (arr[1] == "1") {
                    document.getElementById('ctl00_ContentPlaceHolder1_tboxComment').value = '';
                    $('#<%=lblComments.ClientID%>').show();
                    $('#<%=tboxComment.ClientID%>').show();
                }
                else if (arr[1] == "0") {
                    $('#<%=lblComments.ClientID%>').hide();
                    $('#<%=tboxComment.ClientID%>').hide();
                    document.getElementById('<%= tboxComment.ClientID %>').value = '';
                }
            }


            $('#<%=ddlDeclineCode.ClientID%>').change(function () {

                var selectedVal = $('option:selected', this).val();
                if (selectedVal != "") {

                    var ddlDeclineCodeValue = selectedVal;
                    var arr = ddlDeclineCodeValue.split('-');
                    if (arr[1] == "1") {
                        document.getElementById('ctl00_ContentPlaceHolder1_tboxComment').value = '';
                        $('#<%=lblComments.ClientID%>').show();
                        $('#<%=tboxComment.ClientID%>').show();
                    }
                    else if (arr[1] == "0") {
                        $('#<%=lblComments.ClientID%>').hide();
                        $('#<%=tboxComment.ClientID%>').hide();
                        document.getElementById('<%= tboxComment.ClientID %>').value = '';
                    }
                }

            });

        });

        function IsCommentEntered() {
            var declineCodevalue = $('#<%=ddlDeclineCode.ClientID%>').val();
            if (declineCodevalue != "") {

                var ddlDeclineCodeValue = declineCodevalue;
                var arr = ddlDeclineCodeValue.split('-');
                if (arr[1] == "1") {
                    var DeclineComment = document.getElementById('<%= tboxComment.ClientID %>').value;
                    if (DeclineComment == "") {
                        alert('<%=PleaseEnterComments%>');
                        return false;
                    }
                }
                else if (arr[1] == "0") {

                    document.getElementById('<%= tboxComment.ClientID %>').value = '';
                }
            }
        }

        $("#<%=rblApproveType.ClientID %> input").change(function () {
            $("#<%= rblViewByTotals.ClientID %> input[type=radio]").attr('checked', false);
        });

        $("#<%=rblViewByTotals.ClientID %> input").change(function () {
            $("#<%= rblApproveType.ClientID %> input[type=radio]").attr('checked', false);
        });

        function HideShow() {
            val = $('[id*=rblViewByStatus]').find('input:checked').val();
            if (val == 1) {
                
                $("#ctl00_ContentPlaceHolder1_gvInventoryReview tr td:nth-child(n+17):nth-child(-n+35)").hide();
                $("#ctl00_ContentPlaceHolder1_gvInventoryReview tr th:nth-child(n+17):nth-child(-n+35)").hide();
                $("#ctl00_ContentPlaceHolder1_gvInventoryReview tr td:nth-child(n+36):nth-child(-n+46)").show();
                $("#ctl00_ContentPlaceHolder1_gvInventoryReview tr th:nth-child(n+36):nth-child(-n+46)").show();
            }
            else if (val == 0) {
                
                $("#ctl00_ContentPlaceHolder1_gvInventoryReview tr td:nth-child(n+17):nth-child(-n+35)").show();
                $("#ctl00_ContentPlaceHolder1_gvInventoryReview tr th:nth-child(n+17):nth-child(-n+35)").show();
                $("#ctl00_ContentPlaceHolder1_gvInventoryReview tr td:nth-child(n+36):nth-child(-n+46)").hide();
                $("#ctl00_ContentPlaceHolder1_gvInventoryReview tr th:nth-child(n+36):nth-child(-n+46)").hide();
            }
        }

        $(".PagerHyperlinkStyle").click(function () {
            HideShow();
        });

        $("#<%=rblViewByStatus.ClientID %> input").change(function () {
            HideShow();
        });
        HideShow();
    </script>
</asp:Content>

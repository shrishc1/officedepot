﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;

public partial class DeclineReasonCodeEdit : CommonPage {
    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");

    protected void Page_Load(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
            if (!string.IsNullOrEmpty(GetQueryStringValue("DeclineReasonCodeID"))) {
                BindReasonCode();
            }
        }
    }

    private void BindReasonCode() {
        MAS_DeclineReasonCodeBE oMAS_DeclineReasonCodeBE = new MAS_DeclineReasonCodeBE();
        MAS_DeclineReasonCodeBAL oMAS_DeclineReasonCodeBAL = new MAS_DeclineReasonCodeBAL();

        oMAS_DeclineReasonCodeBE.Action = sqlAction.ShowAll;
        oMAS_DeclineReasonCodeBE.DeclineReasonCodeID = Convert.ToInt32(GetQueryStringValue("DeclineReasonCodeID"));

        List<MAS_DeclineReasonCodeBE> lstMAS_DeclineReasonCodeBE = oMAS_DeclineReasonCodeBAL.GetDeclineReasonCodeBAL(oMAS_DeclineReasonCodeBE);

        if (lstMAS_DeclineReasonCodeBE != null && lstMAS_DeclineReasonCodeBE.Count > 0) {
            
            txtReason.Text = lstMAS_DeclineReasonCodeBE[0].Reason;
            txtCode.Text = lstMAS_DeclineReasonCodeBE[0].Code;
            chkCommentsRequired.Checked = lstMAS_DeclineReasonCodeBE[0].IsMandatoryComment;
        }

        btnSave.Text = WebCommon.getGlobalResourceValue("Update");
        btnDelete.Visible = true;
    }

    #region Events

    protected void btnSave_Click(object sender, EventArgs e) {
        MAS_DeclineReasonCodeBE oMAS_DeclineReasonCodeBE = new MAS_DeclineReasonCodeBE();
        MAS_DeclineReasonCodeBAL oMAS_DeclineReasonCodeBAL = new MAS_DeclineReasonCodeBAL();
        
        if (string.IsNullOrEmpty(GetQueryStringValue("DeclineReasonCodeID")))
            oMAS_DeclineReasonCodeBE.Action = sqlAction.Add;
        else {
            oMAS_DeclineReasonCodeBE.Action = sqlAction.Edit;
            oMAS_DeclineReasonCodeBE.DeclineReasonCodeID = Convert.ToInt32(GetQueryStringValue("DeclineReasonCodeID"));
        }
        oMAS_DeclineReasonCodeBE.Reason = txtReason.Text.Trim();
        oMAS_DeclineReasonCodeBE.Code = txtCode.Text.Trim();
        if(chkCommentsRequired.Checked)
        {
            oMAS_DeclineReasonCodeBE.IsMandatoryComment = true;
        }
        else if(!chkCommentsRequired.Checked)
        {
            oMAS_DeclineReasonCodeBE.IsMandatoryComment = false;
        }
        

        int? result = oMAS_DeclineReasonCodeBAL.addEditDeclineReasonCodeBAL(oMAS_DeclineReasonCodeBE);

        if (result == -1) {
            string errorMeesage = WebCommon.getGlobalResourceValue("ExistsDeclineReasonCode");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert1", "alert('" + errorMeesage + "')", true);
            return;
        }

        EncryptQueryString("DeclineReasonCodeOverview.aspx");
    }

    protected void btnDelete_Click(object sender, EventArgs e) {
        MAS_DeclineReasonCodeBE oMAS_DeclineReasonCodeBE = new MAS_DeclineReasonCodeBE();
        MAS_DeclineReasonCodeBAL oMAS_DeclineReasonCodeBAL = new MAS_DeclineReasonCodeBAL();

         if (!string.IsNullOrEmpty(GetQueryStringValue("DeclineReasonCodeID"))){
            oMAS_DeclineReasonCodeBE.DeclineReasonCodeID = Convert.ToInt32(GetQueryStringValue("DeclineReasonCodeID"));
            oMAS_DeclineReasonCodeBE.Action = sqlAction.Delete;
        }
        int? result = oMAS_DeclineReasonCodeBAL.addEditDeclineReasonCodeBAL(oMAS_DeclineReasonCodeBE);
        EncryptQueryString("DeclineReasonCodeOverview.aspx");
    }

    protected void btnBack_Click(object sender, EventArgs e) {
        EncryptQueryString("DeclineReasonCodeOverview.aspx");
    }

    #endregion
}
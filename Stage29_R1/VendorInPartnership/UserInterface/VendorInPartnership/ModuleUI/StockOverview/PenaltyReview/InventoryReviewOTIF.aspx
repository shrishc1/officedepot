﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="InventoryReviewOTIF.aspx.cs" Inherits="InventoryReviewOTIF"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucMultiSelectVendor.ascx" TagName="ucMultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectPO.ascx" TagName="MultiSelectPO"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .radio-fix tr td:nth-child(2)
        {
            width: 245px;
        }
        .radio-fix tr td:nth-child(3)
        {
            width: 170px;
        }
        .wordbreak
        {
            word-break: break-all;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblOTIFPenaltiesPendingApproval" runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row">      
        <cc1:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSearchScreen" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:ucMultiSelectVendor ID="ucIncludeVendor" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblPurchaseOrderNumber" runat="server" Text=""></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectPO runat="server" ID="msPO" />
                        </td>
                    </tr>
                     <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblPeriod" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <table width="40%" border="0" cellspacing="5" cellpadding="0" id="tblDate">
                                        <tr>
                                            <td>
                                                <cc1:ucDropdownList ID="drpMonthFrom" runat="server" Width="80px">
                                                    <%-- <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>--%>
                                                    <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                                    <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                                    <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                                    <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                                    <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                                    <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                                    <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                                    <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                                    <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpYearFrom" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td style="text-align: center">
                                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpMonthTo" runat="server" Width="80px">
                                                    <%--  <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>--%>
                                                    <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                                    <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                                    <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                                    <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                                    <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                                    <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                                    <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                                    <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                                    <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpYearTo" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStatus" runat="server" Text=""></cc1:ucLabel>
                        </td>
                         <td style="font-weight: bold;">
                            :
                        </td>
                        <td class="nobold radiobuttonlist" valign="middle">
                            <cc1:ucRadioButtonList ID="rblApproveType" runat="server" RepeatColumns="4" RepeatDirection="Horizontal">
                                <asp:ListItem Text="lstPendingReview" Value="1"></asp:ListItem>
                                <asp:ListItem Text="lstApproved" Value="2"></asp:ListItem>
                                <asp:ListItem Text="lstDeclined" Value="3"></asp:ListItem>
                                <asp:ListItem Text="lstAllOnly" Value="4"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                            <cc1:ucRadioButtonList ID="Test" runat="server" Visible="false">
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlPotentialOutput" runat="server">
                <div style="width: 100%; overflow: auto" id="divPrint" class="fixedTable">
                    <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table">
                        <tr>
                            <td style="font-weight: bold;">
                                <div style="width: 950px; overflow-x: auto;">
                                    <cc1:ucGridView ID="gvInventoryReviewOTIF" runat="server" CssClass="grid gvclass searchgrid-1"
                                        GridLines="Both" AllowPaging="true" PageSize="200" OnPageIndexChanging="gvInventoryReviewOTIF_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="SelectAllCheckBox">
                                                <HeaderStyle HorizontalAlign="Left" Width="30px" />
                                                <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                <HeaderTemplate>
                                             
                                                        <cc1:ucCheckbox runat="server" ID="chkSelectAllText" CssClass="checkbox-input"  />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" CssClass="innerCheckBox" ViewStateMode="Enabled" ID="chkSelect" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PO Number" SortExpression="PurchaseOrder.Purchase_order">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                  <%--  <asp:HyperLink ID="hlODSku" runat="server" Text='<%# Eval("PurchaseOrder.Purchase_order") %>' NavigateUrl='#'></asp:HyperLink>--%>
                                                     <asp:Label ID="hlODSku" runat="server" Text='<%# Eval("PurchaseOrder.Purchase_order") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Month Due" SortExpression="PurchaseOrder.Original_due_date">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt1" Text='<%#Eval("PurchaseOrder.Original_due_date", "{0:MMMM}") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OTIF Penalty Rate" SortExpression="OTIFPenaltyRate">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltCountry" Text='<%#Eval("OTIFPenaltyRate") %>'></cc1:ucLabel>%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Purchase Order Value" SortExpression="PurchaseOrderValue">
                                                <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt2" Text='<%#Eval("PurchaseOrderValue") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Penalty Charge %" SortExpression="OTIFPenaltyCharges">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt3" Text='<%#Eval("OTIFPenaltyCharges") %>'></cc1:ucLabel>%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                            </asp:TemplateField>                                           
                                            <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="PurchaseOrder.StockPlannerNo">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("PurchaseOrder.StockPlannerNo") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockPlannerName" SortExpression="PurchaseOrder.StockPlannerName">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("PurchaseOrder.StockPlannerName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fixed Vendor Number" SortExpression="Vendor.Vendor_No">
                                                <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fixed Vendor Name" SortExpression="Vendor.VendorName">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Potential Penalty Charge" SortExpression="PotentialPenaltyCharge">
                                                <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt7" Text='<%#Eval("PotentialPenaltyCharge") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                                <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt8" Text='<%#Eval("Status") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </cc1:ucGridView>
                                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                                    </cc1:PagerV2_8>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <div class="button-row">
                                    <cc1:ucButton ID="btnAccept" runat="server" Text="Accept" CssClass="button" OnClick="btnBack_Click" />
                                    <cc1:ucButton ID="btnDecline" runat="server" Text="Decline" CssClass="button" OnClick="btnBack_Click" />
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>


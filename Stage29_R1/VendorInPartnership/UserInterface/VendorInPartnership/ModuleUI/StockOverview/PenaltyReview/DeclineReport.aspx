﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="DeclineReport.aspx.cs" Inherits="DeclineReport"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucMultiSelectVendor.ascx" TagName="ucMultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectDeclineReasonCode.ascx" TagName="MultiSelectDeclineReasonCode"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .radio-fix tr td:nth-child(2)
        {
            width: 100px;
        }
        .radio-fix tr td:nth-child(3)
        {
            width: 200px;
        }
        .wordbreak
        {
            word-break: break-all;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });


        $(document).ready(function () {
            // HideShowReportView();
            var fullDate = new Date();
            var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
            var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
            $('#txtToDate,#txtFromDate').datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy'
            });

        });

        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
     <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <h2>
        <cc1:ucLabel ID="lblDeclineReport" runat="server"></cc1:ucLabel>
    </h2>
    <%--<asp:UpdatePanel ID="uplPenaltyTracker" runat="server">
        <ContentTemplate>--%>
    <div class="button-row">
        <cc1:ucExportToExcel ID="btnExportToExcel1" runat="server" Visible="false" />
        <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
            Width="109px" Height="20px" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSearchScreen" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblCountry" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:ucMultiSelectVendor ID="msVendor" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblReason" runat="server" Text="Reason">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectDeclineReasonCode runat="server" ID="msDeclineReasonCode" />
                        </td>
                    </tr>
                    <tr>
                      <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDate" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                             <cc1:ucLabel ID="lblFrom" runat="server" Text="From :"></cc1:ucLabel>&nbsp;&nbsp;&nbsp;&nbsp;
                       
                             <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                 onchange="setValue1(this)" ReadOnly="True" Width="70px" />&nbsp;&nbsp;&nbsp;&nbsp;
                            <cc1:ucLabel ID="lblTo" runat="server" Text="To :"></cc1:ucLabel>&nbsp;&nbsp;&nbsp;
                            <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                            onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                        </td>
                                    
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlPotentialOutput" runat="server">
                <div style="width: 100%; overflow: auto" id="divPrint" class="fixedTable">
                    <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table"
                        style="width: 100%;">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucGridView ID="gvBOReport" runat="server" CssClass="grid gvclass searchgrid-1"
                                    GridLines="Both" Width="950px">
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">
                                            <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                        </div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Country" SortExpression="Country">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltCountry" Text='<%#Eval("Country") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Site" SortExpression="SKU.SiteName">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltSite" Text='<%#Eval("SKU.SiteName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ODSKU" SortExpression="SKU.OD_SKU_NO">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltODSku" Text='<%#Eval("SKU.OD_SKU_NO") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VikingSku" SortExpression="SKU.Direct_SKU">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltVikingSku" Text='<%#Eval("SKU.Direct_SKU") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VendorCode" SortExpression="SKU.Vendor_Code">
                                            <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltCountry" Text='<%#Eval("SKU.Vendor_Code") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description" SortExpression="SKU.DESCRIPTION">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lt2" Text='<%#Eval("SKU.DESCRIPTION") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BOCount" SortExpression="NoofBO">
                                            <HeaderStyle Width="30px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lt3" Text='<%#Eval("NoofBO") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" Width="30px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="QtyonBO" SortExpression="QtyonBackOrder">
                                            <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lt4" Text='<%#Convert.ToInt64(Math.Round(Convert.ToDouble(Eval("QtyonBackOrder")))) %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vendor#" SortExpression="Vendor.Vendor_No">
                                            <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="Vendor.StockPlannerNumber">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="StockPlannerName" SortExpression="Vendor.StockPlannerName">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PotentialPenaltyCharge" SortExpression="PotentialPenaltyCharge">
                                            <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lt7" Text='<%#Eval("PotentialPenaltyCharge") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PO#" SortExpression="PurchaseOrderValue">
                                            <HeaderStyle Width="30px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lt5" Text='<%#Eval("PurchaseOrderValue") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="30px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ReasonForDecline" SortExpression="DeclineCode.ReasonCode">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltReasonCode" Text='<%#Eval("DeclineCode.ReasonCode") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Decline Comment" SortExpression="DeclineReasonComments">
                                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltDeclineReasonComments" Text='<%#Eval("DeclineReasonComments") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DeclinedBy" SortExpression="DeclineCode.DeclinedBy">
                                            <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltDeclinedBy" Text='<%#Eval("DeclineCode.DeclinedBy") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DeclineDate" SortExpression="DeclineCode.DeclinedDate">
                                            <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltDeclineDate" Text='<%#Convert.ToDateTime(Eval("DeclineCode.DeclinedDate").ToString()).ToString("dd/MM/yyyy HH:MM") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                        </asp:TemplateField>
                                    </Columns>
                                </cc1:ucGridView>
                                <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                                </cc1:PagerV2_8>
                            </td>
                            
                        </tr>
                        <tr>
                            <td align="right">
                                <div class="button-row">
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </cc1:ucPanel>
        </div>
    </div>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

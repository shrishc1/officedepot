﻿using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System;
using System.Collections.Generic;
using System.Web.UI;
using Utilities;
using WebUtilities;

public partial class VendorPenaltyMaintenanceEdit : CommonPage
{
    #region Local Variable...

    private string PenaltyTypeMandatry = WebCommon.getGlobalResourceValue("PenaltyTypeMandatry");
    private string BORateMandatry = WebCommon.getGlobalResourceValue("BORateMandatry");
    private string OtifRateMandatry = WebCommon.getGlobalResourceValue("OtifRateMandatry");
    private string OtifPenaltyValueMandatry = WebCommon.getGlobalResourceValue("OtifPenaltyValueMandatry");
    private string CurrencyMandatry = WebCommon.getGlobalResourceValue("CurrencyMandatry");

    #endregion Local Variable...

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        UIUtility.PageValidateForODUser();
        if (!IsPostBack)
        {
            this.BindCurrency();
            GetVendorPenaltyDetail();
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("VendorID") != null && GetQueryStringValue("PageFrom").Equals("VendorMaintainance"))
        {
            EncryptQueryString("VendorPenaltyMaintenance.aspx?PreviousPage=VendorPenalty");
        }

        if (GetQueryStringValue("PageFrom") != null)
        {
            if (GetQueryStringValue("PageFrom").Equals("VendorEdit"))
            {
                EncryptQueryString("../../GlobalSettings/VendorEdit.aspx?VendorID=" + GetQueryStringValue("VendorID") + "&IsVendorSubscribeToPenalty=" + GetQueryStringValue("IsVendorSubscribeToPenalty") + "&PageFrom=VendorMaintainance");
            }
            else if (GetQueryStringValue("PageFrom").Equals("VendorOverview"))
            {
                EncryptQueryString("../../GlobalSettings/VendorOverview.aspx");
            }
        }
        if (GetQueryStringValue("PageFrom") != null && GetQueryStringValue("PreviousPageFrom") != null)
        {
            if (GetQueryStringValue("PreviousPageFrom").Equals("UserList"))
            {
                EncryptQueryString("../../GlobalSettings/VendorEdit.aspx?VendorID=" + GetQueryStringValue("VendorID") + "&IsVendorSubscribeToPenalty=" + GetQueryStringValue("IsVendorSubscribeToPenalty") + "&PageFrom=VendorMaintainance" + "&PreviousPageFrom=UserList");
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (rblPenaltyChargeType.SelectedItem.Text == "Yes")
            {
                if (string.IsNullOrEmpty(rblPenaltyBasedon.SelectedValue))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PenaltyTypeMandatry + "')", true);
                    return;
                }
                if (rblPenaltyBasedon.SelectedItem.Value == "BO")
                {
                    if (string.IsNullOrEmpty(txtValueBO.Text) && string.IsNullOrWhiteSpace(txtValueBO.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + BORateMandatry + "')", true);
                        txtValueBO.Focus();
                        return;
                    }
                }
                if (rblPenaltyBasedon.SelectedItem.Value == "OTIF")
                {
                    if (string.IsNullOrEmpty(txtOTIFRate.Text) && string.IsNullOrWhiteSpace(txtOTIFRate.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + OtifRateMandatry + "')", true);
                        txtOTIFRate.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(txtOtifPenaltyValue.Text) && string.IsNullOrWhiteSpace(txtOtifPenaltyValue.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + OtifPenaltyValueMandatry + "')", true);
                        txtOtifPenaltyValue.Focus();
                        return;
                    }
                }

                if (ddlCurrency.SelectedIndex.Equals(0))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CurrencyMandatry + "')", true);
                    ddlCurrency.Focus();
                    return;
                }
            }
            else if (GetQueryStringValue("PageFrom").Equals("VendorOverview") || GetQueryStringValue("PageFrom").Equals("VendorEdit"))
            {
                SaveVendorSubscription();
                return;
            }
            else
            {
                if ((string.IsNullOrEmpty(GetQueryStringValue("PenaltyID"))))
                {
                    EncryptQueryString("VendorPenaltyMaintenance.aspx?PreviousPage=VendorPenalty");
                }
            }

            SaveVendorSubscription();
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void rblPenaltyChargeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        trPenaltyBasedon.Visible = rblPenaltyChargeType.SelectedItem.Value == "1" ? true : false;

        if (trPenaltyBasedon.Visible == true)
        {
            rblPenaltyBasedon.Visible = true;
            rblPenaltyBasedon.SelectedValue = "BO";
            trBORate.Visible = true;
            trValueBO.Visible = true;
            tblPenaltyBasedon.Visible = true;
            trOtifPenalty.Visible = false;
            trLinePenalty.Visible = false;
        }
        else if (trPenaltyBasedon.Visible == false)
        {
            tblPenaltyBasedon.Visible = false;
            rblPenaltyBasedon.Visible = false;
        }
    }

    protected void rblPenaltyBasedon_SelectedIndexChanged(object sender, EventArgs e)
    {
        tblPenaltyBasedon.Visible = true;
        if (rblPenaltyBasedon.SelectedItem.Value == "BO")
        {
            txtOTIFRate.Text = "";
            txtOtifPenaltyValue.Text = "";
            ddlCurrency.SelectedIndex = 0;
            trBORate.Visible = trValueBO.Visible = true;
            trOtifPenalty.Visible = trLinePenalty.Visible = false;
        }
        else if (rblPenaltyBasedon.SelectedItem.Value == "OTIF")
        {
            txtValueBO.Text = "";
            ddlCurrency.SelectedIndex = 0;
            trBORate.Visible = trValueBO.Visible = false;
            trOtifPenalty.Visible = trLinePenalty.Visible = true;
        }
    }

    #endregion Events

    #region Methods

    private void BindCurrency()
    {
        var currencyBAL = new MAS_CurrencyBAL();
        var currencyBE = new CurrencyBE();
        currencyBE.Action = "ShowAll";
        var lstCurrency = currencyBAL.GetCurrenyBAL(currencyBE);
        if (lstCurrency != null && lstCurrency.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCurrency, lstCurrency, "CurrencyName", "CurrencyID", "Select");
        }
    }

    protected void GetVendorPenaltyDetail()
    {
        try
        {
            if (GetQueryStringValue("VendorID") != null)
            {
                BackOrderBAL oBackOrderBAL = new BackOrderBAL();
                BackOrderBE oBackOrderBE = new BackOrderBE();

                oBackOrderBE.Action = "GetVendorPenalty";

                oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
                oBackOrderBE.IsVendorSubscribeToPenalty = "All";
                List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetVendorPenaltyBAL(oBackOrderBE);

                if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
                {
                    lblVendorNoValue.Text = lstBackOrderBE[0].Vendor.Vendor_No;
                    lblVendorNameValue.Text = lstBackOrderBE[0].Vendor.VendorName;

                    #region Checking if Vendor is already subscribe to Penalty

                    if (lstBackOrderBE[0].PenaltyID != null && lstBackOrderBE[0].PenaltyID > 0 && lstBackOrderBE[0].PenaltyType != null)
                    {
                        tblPenaltyBasedon.Visible = true;
                        if (lstBackOrderBE[0].IsVendorSubscribeToPenalty == "Yes")
                        {
                            rblPenaltyChargeType.SelectedIndex = 0;
                            trPenaltyBasedon.Visible = true;
                        }
                        else
                        {
                            rblPenaltyChargeType.SelectedIndex = 1;
                            trPenaltyBasedon.Visible = false;
                        }

                        if (lstBackOrderBE[0].PenaltyType == "BO")
                        {
                            rblPenaltyBasedon.SelectedIndex = 0;
                            trBORate.Visible = trValueBO.Visible = true;
                            trOtifPenalty.Visible = trLinePenalty.Visible = false;
                        }
                        else if (lstBackOrderBE[0].PenaltyType == "OTIF")
                        {
                            rblPenaltyBasedon.SelectedIndex = 1;
                            trBORate.Visible = trValueBO.Visible = false;
                            trOtifPenalty.Visible = trLinePenalty.Visible = true;
                        }

                        txtValueBO.Text = Convert.ToString(lstBackOrderBE[0].ValueOfBackOrder);
                        txtOTIFRate.Text = Convert.ToString(lstBackOrderBE[0].OTIFRate);
                        txtOtifPenaltyValue.Text = Convert.ToString(lstBackOrderBE[0].OTIFLineValuePenalty);
                        ddlCurrency.SelectedValue = Convert.ToString(lstBackOrderBE[0].Currency.CurrencyId);
                    }

                    #endregion Checking if Vendor is already subscribe to Penalty
                }
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void SaveVendorSubscription()
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
            oBackOrderBE.IsVendorSubscribeToPenalty = rblPenaltyChargeType.SelectedItem.Text;

            if (oBackOrderBE.IsVendorSubscribeToPenalty == "No")
            {
                oBackOrderBE.Currency = new CurrencyBE();
                oBackOrderBE.Action = "UpdateVendorPenalty";
                oBackOrderBE.Currency.CurrencyId = 0;
                oBackOrderBE.PenaltyModifiedId = Convert.ToInt32(Session["UserId"]);
            }
            else
            {
                oBackOrderBE.PenaltyType = rblPenaltyBasedon.SelectedItem.Value;
                if (rblPenaltyBasedon.SelectedItem.Value == "BO")
                {
                    oBackOrderBE.ValueOfBackOrder = (!string.IsNullOrEmpty(txtValueBO.Text)) ? Convert.ToDouble(txtValueBO.Text) : (double?)null;
                }
                else
                {
                    oBackOrderBE.OTIFRate = (!string.IsNullOrEmpty(txtOTIFRate.Text)) ? Convert.ToDouble(txtOTIFRate.Text) : (double?)null;
                    oBackOrderBE.OTIFLineValuePenalty = (!string.IsNullOrEmpty(txtOtifPenaltyValue.Text)) ? Convert.ToDecimal(txtOtifPenaltyValue.Text) : (decimal?)null;
                }

                oBackOrderBE.Currency = new CurrencyBE();
                oBackOrderBE.Currency.CurrencyId = Convert.ToInt32(ddlCurrency.SelectedValue);

                oBackOrderBE.PenaltyAppliedId = Convert.ToInt32(Session["UserId"]);

                if ((!string.IsNullOrEmpty(GetQueryStringValue("PenaltyID"))) && Convert.ToInt32(GetQueryStringValue("PenaltyID")) > 0)
                {
                    // Update Penalty for Vendor
                    oBackOrderBE.Action = "UpdateVendorPenalty";
                    oBackOrderBE.PenaltyModifiedId = Convert.ToInt32(Session["UserId"]);
                }
                else if ((!string.IsNullOrEmpty(GetQueryStringValue("PageFrom")) && GetQueryStringValue("IsVendorSubscribeToPenalty") != null))
                {
                    oBackOrderBE.Action = "UpdateVendorPenalty";
                    oBackOrderBE.PenaltyModifiedId = Convert.ToInt32(Session["UserId"]);
                }
                else
                {
                    // Add Penalty for Vendor
                    oBackOrderBE.Action = "AddVendorPenalty";
                }
            }

            int? iResult = oBackOrderBAL.AddEditVendorPenaltyBAL(oBackOrderBE);

            if ((!string.IsNullOrEmpty(GetQueryStringValue("PenaltyID"))) && Convert.ToInt32(GetQueryStringValue("PenaltyID")) > 0)
            {
                EncryptQueryString("VendorPenaltyMaintenance.aspx?PreviousPage=VendorPenalty");
            }
            else if (GetQueryStringValue("PageFrom").Equals("VendorMaintainance"))
            {
                EncryptQueryString("VendorPenaltyMaintenance.aspx?PreviousPage=VendorPenalty");
            }
            else if (GetQueryStringValue("PageFrom").Equals("VendorEdit") && GetQueryStringValue("IsVendorSubscribeToPenalty") != null)
            {
                EncryptQueryString("../../GlobalSettings/VendorEdit.aspx?VendorID=" + GetQueryStringValue("VendorID") + "&IsVendorSubscribeToPenalty=" + oBackOrderBE.IsVendorSubscribeToPenalty + "&PageFrom=VendorMaintainance");
            }
            else if (GetQueryStringValue("PageFrom").Equals("VendorOverview") && GetQueryStringValue("IsVendorSubscribeToPenalty") != null)
            {
                EncryptQueryString("../../GlobalSettings/VendorOverview.aspx");
            }

            if (GetQueryStringValue("PageFrom") != null && GetQueryStringValue("PreviousPageFrom") != null)
            {
                if (GetQueryStringValue("PreviousPageFrom").Equals("UserList"))
                {
                    EncryptQueryString("../../GlobalSettings/VendorEdit.aspx?VendorID=" + GetQueryStringValue("VendorID") + "&IsVendorSubscribeToPenalty=" + GetQueryStringValue("IsVendorSubscribeToPenalty") + "&PageFrom=VendorMaintainance" + "&PreviousPageFrom=UserList");
                }
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    #endregion Methods
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;

public partial class BackOrderHistory : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.BindBacktoOrderGrid();
        }
    }

    protected void gvBackorderHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvBackorderHistory.PageIndex = e.NewPageIndex;
        if (ViewState["BOHistory"] != null)
        {
            gvBackorderHistory.DataSource = (List<ExpediteStockBE>)ViewState["BOHistory"];
            gvBackorderHistory.DataBind();
        }
    }

    private void BindBacktoOrderGrid()
    {
        if (!string.IsNullOrEmpty(GetQueryStringValue("SKUID")))
        {
            var expediteStockBE = new ExpediteStockBE();
            expediteStockBE.Action = "GetBackOrderHistory";
            expediteStockBE.SkuID = Convert.ToInt32(GetQueryStringValue("SKUID"));
            var expediteStockBAL = new ExpediteStockBAL();
            var lstBackOrderHistory = expediteStockBAL.GetBackOrderHistoryBAL(expediteStockBE);
            expediteStockBAL = null;
            gvBackorderHistory.DataSource = null;
            if (lstBackOrderHistory != null && lstBackOrderHistory.Count > 0)
            {
                ViewState["BOHistory"] = lstBackOrderHistory;
                gvBackorderHistory.DataSource = lstBackOrderHistory;
            }            
        }
        else
        {
            gvBackorderHistory.DataSource = null;
        }
        gvBackorderHistory.DataBind();
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="VendorViewOTIF.aspx.cs" Inherits="VendorViewOTIF"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucMultiSelectVendor.ascx" TagName="ucMultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .radio-fix tr td:nth-child(2)
        {
            width: 245px;
        }
        .radio-fix tr td:nth-child(3)
        {
            width: 170px;
        }
        .wordbreak
        {
            word-break: break-all;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblVendorViewOtif" runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc1:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSearchScreen" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:ucMultiSelectVendor ID="ucIncludeVendor" runat="server" />
                        </td>
                    </tr>
                    <%--<tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblMonth" runat="server" Text="Month"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:ucNumericTextbox ID="txtMonth" runat="server" Columns="40"></cc1:ucNumericTextbox>
                        </td>

                    </tr>--%>
                     <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblPeriod" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <table width="40%" border="0" cellspacing="5" cellpadding="0" id="tblDate">
                                        <tr>
                                            <td>
                                                <cc1:ucDropdownList ID="drpMonthFrom" runat="server" Width="80px">
                                                    <%-- <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>--%>
                                                    <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                                    <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                                    <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                                    <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                                    <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                                    <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                                    <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                                    <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                                    <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpYearFrom" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td style="text-align: center">
                                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpMonthTo" runat="server" Width="80px">
                                                    <%--  <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>--%>
                                                    <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                                    <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                                    <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                                    <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                                    <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                                    <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                                    <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                                    <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                                    <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpYearTo" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlPotentialOutput" runat="server">
                <table width="900px" cellspacing="10" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                    <tr>
                        <td style="font-weight: bold;" width="70%" align="left">
                            <cc1:ucLabel ID="lblPenaltyDetails" runat="server" Text="Detailed below are the open penalty charge(s)"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="30%" align="left">
                            <cc1:ucLabel ID="lblPenaltyCharge" runat="server" Text="The penalty charge is {xxx}"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-weight: bold;" align="left">
                            <cc1:ucLabel ID="lblVendorViewDescription" runat="server" Text="It is our mutual benefit to honor Purchase Order due dates. Please review and if for any reason you query any of the values / charges cited, please advise promptly"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
                <div style="width: 100%; overflow: auto" id="divPrint" class="fixedTable">
                    <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table">
                        <tr>
                            <td style="font-weight: bold;">
                                <%-- <div style="width: 950px; overflow-x: auto;">--%>
                                <cc1:ucGridView ID="gvVendorView" runat="server" CssClass="grid gvclass searchgrid-1"
                                    Width="900px" GridLines="Both" AllowPaging="true" PageSize="200" OnPageIndexChanging="gvVendorView_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="SelectAllCheckBox">
                                            <HeaderStyle HorizontalAlign="Left" Width="30px" />
                                            <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <cc1:ucCheckbox runat="server" ID="chkSelectAllText" CssClass="checkbox-input" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" CssClass="innerCheckBox" ViewStateMode="Enabled" ID="chkSelect" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PO Number" SortExpression="PurchaseOrder.Purchase_order">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltPONo" Text='<%#Eval("PurchaseOrder.Purchase_order") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Month Due" SortExpression="DateBOIncurred">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltMonth" Text='<%#Eval("DateBOIncurred", "{0:MMMM}")%>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OTIF Rate %" SortExpression="OTIFPenaltyRate">
                                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltOtifRate" Text='<%#Eval("OTIFPenaltyRate") %>'></cc1:ucLabel>%
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Purchase Order Value" SortExpression="PurchaseOrderValue">
                                            <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltPOValue" Text='<%#Eval("PurchaseOrderValue") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Penalty Charge %" SortExpression="OTIFPenaltyCharges">
                                            <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="lt3" Text='<%#Eval("OTIFPenaltyCharges") %>'></cc1:ucLabel>%
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="60px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="StockPlannerName" SortExpression="PurchaseOrder.StockPlannerName">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("PurchaseOrder.StockPlannerName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vendor Number" SortExpression="Vendor.Vendor_No">
                                            <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vendor Name" SortExpression="Vendor.VendorName">
                                            <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Charge Incurred" SortExpression="PotentialPenaltyCharge">
                                            <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                            <ItemTemplate>
                                                <cc1:ucLabel runat="server" ID="ltPotentialPenaltyCharge" Text='<%#Eval("PotentialPenaltyCharge") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                        </asp:TemplateField>
                                    </Columns>
                                </cc1:ucGridView>
                                <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                                </cc1:PagerV2_8>
                                <%-- </div>--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <div class="button-row">
                                    <cc1:ucButton ID="btnAccept" runat="server" Text="Accept" CssClass="button" OnClick="btnBack_Click" />
                                    <cc1:ucButton ID="btnQuery" runat="server" Text="Query" CssClass="button" OnClick="btnQuery_Click" />
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </cc1:ucPanel>
        </div>
    </div>
    <asp:UpdatePanel ID="updComment" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnComments" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdComment" runat="server" TargetControlID="btnComments"
                PopupControlID="pnlbtnComment" BackgroundCssClass="modalBackground" BehaviorID="MsgComment"
                DropShadow="false" />
            <asp:Panel ID="pnlbtnComment" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblVendorViewQuery" runat="server" Text="Please state details of your query. - Please ensure that you supply a detailed response."></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucTextarea ID="txtComments" runat="server" Width="500px" Height="150px"></cc1:ucTextarea>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnOk" CommandName="Comments" runat="server" Text="OK" OnCommand="btnProceed_Click"
                                    CssClass="button" />
                                <cc1:ucButton ID="btnBack_1" CommandName="Comments" runat="server" Text="Back" OnCommand="btnBack_1_Click"
                                    CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOk" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

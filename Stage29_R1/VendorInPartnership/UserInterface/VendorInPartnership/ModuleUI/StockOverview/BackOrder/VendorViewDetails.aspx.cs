﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using BaseControlLibrary;
using System.Collections;
using Utilities;



public partial class VendorView : CommonPage
{
    #region Declarations ...
    private const int gridPageSize = 50;
    bool IsExportClicked = false;
    protected string selectCheckboxToAccept = WebCommon.getGlobalResourceValue("SelectCheckboxToAction");
    protected string selectonlyoneCheckboxToQuery = WebCommon.getGlobalResourceValue("selectonlyoneCheckboxToQuery");
    protected string QueryInvestigationMsg = WebCommon.getGlobalResourceValue("QueryInvestigationMsg");

    protected string Pleasestatethedisputedvalue = WebCommon.getGlobalResourceValue("Pleasestatethedisputedvalue");
    protected string PleaseEnterRevisedPenalty = WebCommon.getGlobalResourceValue("PleaseEnterRevisedPenalty");
    protected string PleaseEnterComments = WebCommon.getGlobalResourceValue("PleaseEnterComments");
    protected string DisputedValueCannotGreaterThanCurrentPenalty = WebCommon.getGlobalResourceValue("DisputedValueCannotGreaterThanCurrentPenalty");
    BackOrderBAL oBackOrderBAL = new BackOrderBAL();
    BackOrderBE oBackOrderBE = new BackOrderBE();
    #endregion

    #region Events...
    protected void Page_InIt(object sender, EventArgs e)
    {
     //  msSite.isMoveAllRequired = true;

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
            string dv = GetQueryStringValue("FromDashboard");
            if (dv!= null)
            {
                BindVendorView();
            }
         
            

        }
    }

  

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(GetQueryStringValue("FromDashboard")))
        {
            EncryptQueryString("OpenBOCharges.aspx");
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {

        IsExportClicked = true;
        BindVendorView();
        LocalizeGridHeader(gvVendorView);
        WebCommon.ExportHideHidden("VendorView", gvVendorView);      
    }

   

    protected void gvVendorView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstBackOrderBE"] != null)
        {
            gvVendorView.DataSource = (List<BackOrderBE>)ViewState["lstBackOrderBE"];
            gvVendorView.PageIndex = e.NewPageIndex;
            if (Session["VendorView"] != null)
            {
                Hashtable htVendorView = (Hashtable)Session["PendingPenalty"];
                htVendorView.Remove("PageIndex");
                htVendorView.Add("PageIndex", e.NewPageIndex);
                Session["VendorView"] = htVendorView;
            }
            gvVendorView.DataBind();
        }

    }

    

    protected void gvVendorView_OnDataBound(object sender, EventArgs e)
    {
       
    }

    #endregion

    #region Methods..

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currentPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currentPageIndx;
        BindVendorView(currentPageIndx);
    }

    private void BindVendorView(int Page = 1)
    {

        try
        {
           
            oBackOrderBE.User = new SCT_UserBE();
            oBackOrderBE.Action = "GetVendorViewDetail";          
            string Vid = GetQueryStringValue("VendorID");
            oBackOrderBE.SelectedVendorIDs = Vid;          
            if (GetQueryStringValue("FromDashboard") != null)
            {
                //oBackOrderBE.VendorBOReviewStatus = "Pending";
            }
            else
            {
                //oBackOrderBE.VendorBOReviewStatus = rblStatus.SelectedValue == "1" ? "All" : (rblStatus.SelectedValue == "2" ? "Pending" : (rblStatus.SelectedValue == "3" ? "Accepted" : (rblStatus.SelectedValue == "4" ? "Query" :(rblStatus.SelectedValue=="5" ? "DisputeAccepted" : "DisputeDeclined"))));
                oBackOrderBE.VendorBOReviewStatus = "1";
            }           

            oBackOrderBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            if (Convert.ToString(Session["Role"]) == "Vendor")
            {
                oBackOrderBE.User.RoleName = Session["Role"] as string;
            }
            else
            {
                oBackOrderBE.User.RoleName = null;
            }            

            if (IsExportClicked)
            {
                oBackOrderBE.GridCurrentPageNo = 0;
                oBackOrderBE.GridPageSize = 0;
            }
            else
            {
                oBackOrderBE.GridCurrentPageNo = Page;
                oBackOrderBE.GridPageSize = gridPageSize;
            }
            // this.SetSession(oBackOrderBE);
            if (GetQueryStringValue("IsReminderCommSent") != null)
            {
                oBackOrderBE.IsReminderCommSent = Convert.ToInt32(GetQueryStringValue("IsReminderCommSent"));
            }
            if (GetQueryStringValue("StockPlannerID") != null)
            {
                oBackOrderBE.StockPlannerID = Convert.ToInt32(GetQueryStringValue("StockPlannerID"));
            }
            

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetVendorViewBAL(oBackOrderBE);         

            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                btnExportToExcel.Visible = true;
                gvVendorView.DataSource = null;
                string Vendor_No = GetQueryStringValue("Vendor_No");
               
                if (GetQueryStringValue("IsReminderCommSent") == null)
                {
                    gvVendorView.DataSource = lstBackOrderBE.Where(id => id.VendorID == Convert.ToInt32(Vid) && (id.vendor_No == Vendor_No)

                                                                   ).ToList();

                }
                else
                {
                    gvVendorView.DataSource = lstBackOrderBE.Where(id => id.VendorID == Convert.ToInt32(Vid) 

                                                                   ).ToList();
                }
              //  gvVendorView.DataSource = filterVendorDetails;
                gvVendorView.DataBind();
                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                ViewState["lstBackOrderBE"] = lstBackOrderBE;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;

                gvVendorView.PageIndex = Page;
                pager1.CurrentIndex = Page;
            }
            else
            {
                gvVendorView.DataSource = null;
                gvVendorView.DataBind();
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
              
            }


            oBackOrderBAL = null;

            pnlGrid.Visible = true;
          


        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

   
    #endregion
   
     

    
}
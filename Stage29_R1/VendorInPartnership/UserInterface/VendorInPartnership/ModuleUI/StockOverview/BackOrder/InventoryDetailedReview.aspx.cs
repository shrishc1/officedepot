﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using BaseControlLibrary;
using System.Collections;
using Utilities;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Languages.Languages;
using System.IO;
using System.Text;
using System.Configuration;

public partial class InventoryDetailedReview : CommonPage
{

    #region Declarations ...
    private const int gridPageSize = 50;
    bool IsExportClicked = false;

   protected string PleaseEnterYourResponseToTheVendor = WebCommon.getGlobalResourceValue("PleaseEnterYourResponseToTheVendorHere");
   protected string PleaseEnterPenaltyCharge = WebCommon.getGlobalResourceValue("PleaseEnterPenaltyCharge");
   protected string PleaseEnterComments = WebCommon.getGlobalResourceValue("PleaseEnterComments");
   protected string PleaseEnterForwardDisputeOnto = WebCommon.getGlobalResourceValue("PleaseEnterForwardDisputeOnto");
   protected string ImposedValueShouldEqualToCurrentPenalty = WebCommon.getGlobalResourceValue("ImposedValueShouldEqualToCurrentPenalty");
   protected string ImposedValueCannotEqualGreaterToCurrentPenalty = WebCommon.getGlobalResourceValue("ImposedValueCannotEqualGreaterToCurrentPenalty");
    string OurCode = WebCommon.getGlobalResourceValue("OurCode");
    string VendorCode = WebCommon.getGlobalResourceValue("VendorCode");
    string Description = WebCommon.getGlobalResourceValue("Description");
    string DateBOIncurred = WebCommon.getGlobalResourceValue("DateBOIncurred");
    string Site2 = WebCommon.getGlobalResourceValue("Site");
    string CountOfBoIncurred = WebCommon.getGlobalResourceValue("CountofBO(s)Incurred");
    string OriginalPenaltyValue = WebCommon.getGlobalResourceValue("OriginalPenaltyValue");
    string DisputedValue = WebCommon.getGlobalResourceValue("DisputedValue");
    string RevisedPenaltyCharge = WebCommon.getGlobalResourceValue("RevisedPenaltyCharge");
    string VendorCommunication2EmailSubject = WebCommon.getGlobalResourceValue("VendorCommunication2EmailSubject");
    string InternalCommunicationEmailSubject = WebCommon.getGlobalResourceValue("InternalCommunicationEmailSubject");
    string MediationCommunicationEmailSubject = WebCommon.getGlobalResourceValue("MediationCommunicationEmailSubject");
    protected string SelectCheckboxToAction = WebCommon.getGlobalResourceValue("SelectCheckboxToAction");
    protected string IsRecordsSelectedtobeUpdated = WebCommon.getGlobalResourceValue("IsRecordsSelectedtobeUpdated");
    protected string PleaseSelectOption = WebCommon.getGlobalResourceValue("PleaseSelectOption");
    protected string SelectCheckbox = WebCommon.getGlobalResourceValue("SelectCheckbox");
    //int chkSelectValue = 0;
    //int chkSelectAllValue = 0;
    int? IResult = 0;
    #endregion


    #region Events

   

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("Month") != null)
            {

                lblDisputeDetails.Visible = true;
                lblDisputeReview.Visible = false;
                pnlStockPlannerResponse_TobeTaken.Visible = false;
                pnlStockPannerResponse.Visible = false;
            }
            else if (GetQueryStringValue("Status") != null)
            {
                lblDisputeDetails.Visible = false;
                lblDisputeReview.Visible = true;
                if ((Convert.ToString(GetQueryStringValue("Status")) == "Open"))
                {
                    pnlStockPlannerResponse_TobeTaken.Visible = true;
                    pnlStockPannerResponse.Visible = false;
                    btnBack_ToMainPage.Visible = false;
                }
                else if ((Convert.ToString(GetQueryStringValue("Status")) == "Closed"))
                {
                    pnlStockPlannerResponse_TobeTaken.Visible = false;
                    pnlStockPannerResponse.Visible = true;
                }
            }

            BindODManger();
            BindDisputeReviewDetail();

        }
    }
    
    protected void btnBack_Click(object sender, EventArgs e)
    {
        // Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");

        EncryptQueryString("InventoryReview2.aspx?PreviousPage=InventoryDetailedReview");

    }

    protected void btnSaveSPAction_Click(object sender, EventArgs e)
    {
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        BackOrderBE oBackOrderBE = new BackOrderBE();


        oBackOrderBE.Action = "UpdateSPAction";
        oBackOrderBE.SPAction = rdoAgreeWithVendor.Checked ? rdoAgreeWithVendor.Text : (rdoDisagreeWithVendor.Checked ? rdoDisagreeWithVendor.Text : rdoPartiallyAgreeWithVendor.Text);
        oBackOrderBE.DisputeReviewStatus = rdoAgreeWithVendor.Checked ? "Agree" : (rdoDisagreeWithVendor.Checked ? "Disagree" : "PartialAgree");
        oBackOrderBE.SPComment = txtSPComments.Text;
        if (rdoDisagreeWithVendor.Checked || rdoPartiallyAgreeWithVendor.Checked)
        {
            oBackOrderBE.ImposedPenaltyCharge = !string.IsNullOrEmpty(txtPenaltyChargeValue.Text) ? Convert.ToDecimal(txtPenaltyChargeValue.Text) : (decimal?)null;
            string[] ForwardDisputeTo = ddlODManager.SelectedItem.Value.Split('-');
            oBackOrderBE.DisputeForwaredTo = ForwardDisputeTo[0].ToString();
            ViewState["ODManagerID"] = ForwardDisputeTo[1].ToString();
            ViewState["ODLanguageID"] = ForwardDisputeTo[2].ToString();
            oBackOrderBE.SPComment = txtDisagreeComment.Text;
            oBackOrderBE.PotentialPenaltyChargeTotal = !string.IsNullOrEmpty(hdnPotentialPenaltyChargeValue.Value) ? Convert.ToDecimal(hdnPotentialPenaltyChargeValue.Value) : (decimal?)null; 
        }
        if (rdoDisagreeWithVendor.Checked)
        {
            oBackOrderBE.ImposedPenaltyCharge = !string.IsNullOrEmpty(hdnPotentialPenaltyChargeValue.Value) ? Convert.ToDecimal(hdnPotentialPenaltyChargeValue.Value) : (decimal?)null;
        }
        if (rdoAgreeWithVendor.Checked)
        {
            oBackOrderBE.ImposedPenaltyCharge = !string.IsNullOrEmpty(hdnDisputedChargeValue.Value) ? Convert.ToDecimal(hdnDisputedChargeValue.Value) : (decimal?)null;
            oBackOrderBE.PotentialPenaltyChargeTotal = !string.IsNullOrEmpty(hdnPotentialPenaltyChargeValue.Value) ? Convert.ToDecimal(hdnPotentialPenaltyChargeValue.Value) : (decimal?)null;
        }

        //if (!(string.IsNullOrEmpty(Convert.ToString(ViewState["PenaltyChargeID"]))))
        //    oBackOrderBE.PenaltyChargeID = Convert.ToInt32(ViewState["PenaltyChargeID"]);


        foreach (GridViewRow row in gvDisputeDetail.Rows)
        {
            CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;
            HiddenField hdnPenaltyChargeID = row.FindControl("hdnPenaltyChargeID") as HiddenField;


            if (chkSelect != null && hdnPenaltyChargeID != null)
            {
                if (chkSelect.Checked)
                {
                    oBackOrderBE.PenaltyChargeID = Convert.ToInt32(hdnPenaltyChargeID.Value);
                    IResult = oBackOrderBAL.UpdateDisputeReviewStatusBAL(oBackOrderBE);
                }
            }             
        }

        if (IResult == 0)
        {
            if (rdoAgreeWithVendor.Checked)
            {
                SendVendorCommunication2();
            }
            else if (rdoDisagreeWithVendor.Checked || rdoPartiallyAgreeWithVendor.Checked)
            {
              //  SendInternalCommunication();      // As per mail dated 08/02/2017. Only one mail will be triggered
                SendMediationEmailCommunication();
            }

            EncryptQueryString("InventoryReview2.aspx?PreviousPage=InventoryDetailedReview");
        }




    }

    protected void btnExport_Click(object sender, EventArgs e)
    {

        IsExportClicked = true;
        BindDisputeReviewDetail();
        LocalizeGridHeader(gvDisputeDetail);
        WebCommon.ExportHideHidden("BackOrderDisputeOverview", gvDisputeDetail,null,IsHideHidden:true,isHideSecondColumn:true);
    }


    #region gridviewEvents
    protected void gvDisputeDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
    }

    protected void gvDisputeDetail_OnDataBound(object sender, EventArgs e)
    {
        try
        {
            for (int i = gvDisputeDetail.Rows.Count - 1; i > 0; i--)
            {
                GridViewRow row = gvDisputeDetail.Rows[i];
                GridViewRow previousRow = gvDisputeDetail.Rows[i - 1];
                var ltDateBOIncurred = (Label)row.FindControl("ltDateBOIncurred");
                var ltDateBOIncurredP = (Label)previousRow.FindControl("ltDateBOIncurred");
                var ltODSKUNO = (Label)row.FindControl("ltodsku");
                var ltODSKUNOP = (Label)previousRow.FindControl("ltodsku");
                var ltSite = (Label)row.FindControl("ltsite");
                var ltSiteP = (Label)previousRow.FindControl("ltsite");

                if (ltODSKUNO.Text == ltODSKUNOP.Text)
                {
                    if (ltDateBOIncurred.Text == ltDateBOIncurredP.Text)
                    {
                        if (ltSite.Text == ltSiteP.Text)
                        {
                            if (previousRow.Cells[1].RowSpan == 0)
                            {
                                if (row.Cells[1].RowSpan == 0)
                                {
                                    previousRow.Cells[0].RowSpan += 2;
                                    previousRow.Cells[1].RowSpan += 2;
                                    previousRow.Cells[2].RowSpan += 2;
                                    previousRow.Cells[3].RowSpan += 2;
                                    previousRow.Cells[4].RowSpan += 2;
                                    previousRow.Cells[5].RowSpan += 2;
                                    previousRow.Cells[6].RowSpan += 2;
                                    previousRow.Cells[7].RowSpan += 2;
                                    previousRow.Cells[8].RowSpan += 2;
                                    previousRow.Cells[9].RowSpan += 2;
                                    previousRow.Cells[10].RowSpan += 2;
                                    previousRow.Cells[11].RowSpan += 2;
                                    previousRow.Cells[12].RowSpan += 2;
                                    previousRow.Cells[13].RowSpan += 2;
                                    previousRow.Cells[18].RowSpan += 2;
                                    
                                }
                                else
                                {
                                    previousRow.Cells[0].RowSpan = row.Cells[0].RowSpan + 1;
                                    previousRow.Cells[1].RowSpan = row.Cells[1].RowSpan + 1;
                                    previousRow.Cells[2].RowSpan = row.Cells[2].RowSpan + 1;
                                    previousRow.Cells[3].RowSpan = row.Cells[3].RowSpan + 1;
                                    previousRow.Cells[4].RowSpan = row.Cells[4].RowSpan + 1;
                                    previousRow.Cells[5].RowSpan = row.Cells[5].RowSpan + 1;
                                    previousRow.Cells[6].RowSpan = row.Cells[6].RowSpan + 1;
                                    previousRow.Cells[7].RowSpan = row.Cells[7].RowSpan + 1;
                                    previousRow.Cells[8].RowSpan = row.Cells[8].RowSpan + 1;
                                    previousRow.Cells[9].RowSpan = row.Cells[9].RowSpan + 1;
                                    previousRow.Cells[10].RowSpan = row.Cells[10].RowSpan + 1;
                                    previousRow.Cells[11].RowSpan = row.Cells[11].RowSpan + 1;
                                    previousRow.Cells[12].RowSpan = row.Cells[12].RowSpan + 1;
                                    previousRow.Cells[13].RowSpan = row.Cells[13].RowSpan + 1;
                                    previousRow.Cells[18].RowSpan = row.Cells[18].RowSpan + 1;
                                    
                                }
                                row.Cells[0].Visible = false;
                                row.Cells[1].Visible = false;
                                row.Cells[2].Visible = false;
                                row.Cells[3].Visible = false;
                                row.Cells[4].Visible = false;
                                row.Cells[5].Visible = false;
                                row.Cells[6].Visible = false;
                                row.Cells[7].Visible = false;
                                row.Cells[8].Visible = false;
                                row.Cells[9].Visible = false;
                                row.Cells[10].Visible = false;
                                row.Cells[11].Visible = false;
                                row.Cells[12].Visible = false;
                                row.Cells[13].Visible = false;
                                row.Cells[18].Visible = false;
                                
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }



    #endregion


    #endregion

    #region Methods
   

    private void BindODManger()
    {
        SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
        oNewSCT_UserBE.Action = "GetODManager";
        oNewSCT_UserBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
        lstUser = oSCT_UserBAL.GetODMangersBAL(oNewSCT_UserBE);
        oSCT_UserBAL = null;
        FillControls.FillDropDown(ref ddlODManager, lstUser, "FirstName", "LoginID", "--Select--");
        
    }

    private void BindDisputeReviewDetail(int Page = 1)
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.Action = "GetDisputeReviewDetail";
            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
            if (GetQueryStringValue("VendorID") != null)
            {
                oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
            }
            //Incase of Status Click
            if (GetQueryStringValue("Status") != null )
            {
                oBackOrderBE.DisputeReviewStatus = Convert.ToString(GetQueryStringValue("Status"));

                if (Convert.ToString(GetQueryStringValue("Status")) == "Closed") 
                {
                 oBackOrderBE.Resolution = Convert.ToString(GetQueryStringValue("Resolution")) == "Partial Agree" ? "PartialAgree" : Convert.ToString(GetQueryStringValue("Resolution"));
                }
            }
            //Incase of Month Click
            if (GetQueryStringValue("StatusValue") != null)
            {
                oBackOrderBE.DisputeReviewStatus = Convert.ToString(GetQueryStringValue("StatusValue"));

                if  (Convert.ToString(GetQueryStringValue("StatusValue")) == "Closed")
                {
                    oBackOrderBE.Resolution = Convert.ToString(GetQueryStringValue("Resolution")) == "Partial Agree" ? "PartialAgree" : Convert.ToString(GetQueryStringValue("Resolution"));
                }
            }
            if (GetQueryStringValue("MonthValue") != null)
            {
                oBackOrderBE.SelectedMonth = Convert.ToString(GetQueryStringValue("MonthValue"));
            }
            else if (GetQueryStringValue("Month") != null)
            {
                oBackOrderBE.SelectedMonth = Convert.ToString(GetQueryStringValue("Month"));
            }

            if (GetQueryStringValue("OD_SKU_NO") != null)
            {
                oBackOrderBE.SKU.OD_SKU_NO = Convert.ToString(GetQueryStringValue("OD_SKU_NO").ToString());
            }

            if (IsExportClicked)
            {
                oBackOrderBE.GridCurrentPageNo = 0;
                oBackOrderBE.GridPageSize = 0;
            }
            else
            {
                oBackOrderBE.GridCurrentPageNo = Page;
                oBackOrderBE.GridPageSize = gridPageSize;
            }

            // this.SetSession(oBackOrderBE);

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetDisputeDetailBAL(oBackOrderBE);
            


            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                int DistinctBOIds = lstBackOrderBE.Select(x => x.BOReportingID).Distinct().Count();
                hdnCountOfRowInGrid.Value = Convert.ToString(DistinctBOIds);
                btnExportToExcel.Visible = true;
                gvDisputeDetail.DataSource = null;
                gvDisputeDetail.DataSource = lstBackOrderBE;                  

                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                ViewState["lstBackOrderBE"] = lstBackOrderBE;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;

                int? id = lstBackOrderBE.First().PenaltyChargeID;
                ViewState["ImposedPenaltyChargeTotal"] = Convert.ToString(lstBackOrderBE[0].ImposedPenaltyChargeTotal);
                BindQueryDetail(id);
            }
            else
            {
                gvDisputeDetail.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
            }

            gvDisputeDetail.PageIndex = Page;
            pager1.CurrentIndex = Page;
            gvDisputeDetail.DataBind();



            if (gvDisputeDetail.Rows.Count > 0)
            {
                RadioButton rbtn = (RadioButton)(gvDisputeDetail.Rows[0].FindControl("rblSelect"));
                rbtn.Checked = true;

                if (Convert.ToString(GetQueryStringValue("Status")) == "Closed")
                {
                    foreach (GridViewRow row in gvDisputeDetail.Rows)
                    {
                        CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;
                        RadioButton rblSelect = row.FindControl("rblSelect") as RadioButton;
                        CheckBox chkSelectAll = gvDisputeDetail.HeaderRow.Cells[1].FindControl("chkSelectAllText") as CheckBox;

                        if (chkSelect.Checked ==false)                      
                        {                           
                            chkSelect.Checked = true;
                            chkSelectAll.Checked = true;
                            chkSelect.Enabled = false;
                            chkSelectAll.Enabled = false;
                        }
                    }

                    //if (lstBackOrderBE[0].SPAction == "Disagree with the Vendors Dispute")
                    //{
                    //    txtOriginalPenaltyValue.Text = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyChargeTotal);
                    //    txtValueInDispute.Text = Convert.ToString(lstBackOrderBE[0].ImposedPenaltyChargeTotal);
                    //}
                    //else
                    //{
                        txtOriginalPenaltyValue.Text = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyChargeTotal);
                        txtValueInDispute.Text = Convert.ToString(lstBackOrderBE[0].ImposedPenaltyChargeTotal);
                    //}
                }

            }


            oBackOrderBAL = null;



        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currentPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currentPageIndx;
        BindDisputeReviewDetail(currentPageIndx);
    }


    protected void rblSelect_CheckedChanged(object sender, EventArgs e)
    {
        var rbtnText = ((System.Web.UI.WebControls.CheckBox)(sender)).Text;
        int? PenaltyChargeID = Convert.ToInt32(rbtnText);
        BindQueryDetail(PenaltyChargeID);
        getSKUDetialOfSelectedRow();
        if (hdnPotentialPenaltyChargeValue != null)
        {
            txtOriginalPenaltyValue.Text = hdnPotentialPenaltyChargeValue.Value;
            txtDisputedValue.Text = hdnPotentialPenaltyChargeValue.Value;
        }
        if (hdnDisputedChargeValue != null)
        {
            txtValueInDispute.Text = hdnDisputedChargeValue.Value;
        }
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "myfunction()", true);
    }

    private void BindQueryDetail(int? PenaltyChargeID)
    {
        ViewState["PenaltyChargeID"] = PenaltyChargeID;


        #region Getting Vendor Query detail by Penalty charge Id
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        BackOrderBE oBackOrderBE = new BackOrderBE();

        oBackOrderBE.Action = "GetQueryDetail";
        oBackOrderBE.PenaltyChargeID = PenaltyChargeID;

        List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetQueryDetailBAL(oBackOrderBE);

        if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
        {
            pnlVendorsQuery.Visible = true;

           // txtOriginalPenaltyValue.Text = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyCharge);
           // ViewState["PotentialPenaltyCharge"] = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyCharge);
           // txtValueInDispute.Text = Convert.ToString(lstBackOrderBE[0].DisputedValue);
           // ViewState["DisputedValue"] = Convert.ToString(lstBackOrderBE[0].DisputedValue);
           // txtDisputedValue.Text = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyCharge);
            txtRevisedPenalty.Text = Convert.ToString(lstBackOrderBE[0].RevisedPenalty);
            //txtPenaltyChargeValue.Text = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyCharge);
            ltRaisedOn.Text = Common.GetDD_MM_YYYY(Convert.ToString(lstBackOrderBE[0].VendorActionDate));
            ltRaisedBy.Text = lstBackOrderBE[0].VendorActionTakenName;
            ltContactDetail.Text = lstBackOrderBE[0].Vendor.VendorContactEmail;
            ltPhoneNumber.Text = lstBackOrderBE[0].Vendor.VendorContactNumber;
            txtQuery.Text = lstBackOrderBE[0].VendorComment;
            hdnsiteid.Value = Convert.ToString(lstBackOrderBE[0].SiteID);

            //Getting Stock Planner response
            if (lstBackOrderBE[0].SPAction != null)
            {
                lblSPAction.Text = lstBackOrderBE[0].SPAction;
                if (lstBackOrderBE[0].SPAction == "Agree with the Vendors Dispute")
                {
                    trPenaltyImposed.Visible = false;
                    trEsclatedTo.Visible = false;
                }
                else
                {
                    trPenaltyImposed.Visible = true;
                    trEsclatedTo.Visible = true;
                }

                lblDateValue.Text = Common.GetDD_MM_YYYY(Convert.ToString(lstBackOrderBE[0].DisputeReviewActionDate));
                lblSPNoValue.Text = lstBackOrderBE[0].Vendor.StockPlannerNumber;
                lblSPNameValue.Text = lstBackOrderBE[0].Vendor.StockPlannerName;
                //lblPenaltyChargeVal.Text = Convert.ToString(lstBackOrderBE[0].ImposedPenaltyCharge);
                lblPenaltyChargeVal.Text = Convert.ToString(ViewState["ImposedPenaltyChargeTotal"]);
                txtCommentsValue.Text = lstBackOrderBE[0].SPComment;
                lblDisputeEscVal.Text = lstBackOrderBE[0].DisputeForwaredTo;
            }


        }

        #endregion
    }

    private void getSKUDetialOfSelectedRow()
    {
        foreach (GridViewRow row in gvDisputeDetail.Rows)
        {

            RadioButton rdoSelect = row.FindControl("rblSelect") as RadioButton;
            ucLabel ltodsku = row.FindControl("ltodsku") as ucLabel;
            ucLabel ltVendorCode = row.FindControl("ltVendorCode") as ucLabel;
            ucLabel ltDescription = row.FindControl("ltDescription") as ucLabel;
            ucLabel ltDateBOIncurred = row.FindControl("ltDateBOIncurred") as ucLabel;
            ucLabel ltsite = row.FindControl("ltsite") as ucLabel;
            ucLabel ltCountofBoIncurred = row.FindControl("ltCountofBoIncurred") as ucLabel;
            HiddenField hdnBOReportingId = row.FindControl("hdnBOReportingID") as HiddenField;

            if (rdoSelect.Checked && ltodsku.Text != null)
            {
                ViewState["OdSKUNo"] = ltodsku.Text;
                ViewState["VendorCode"] = ltVendorCode.Text;
                ViewState["Description"] = ltDescription.Text;
                ViewState["BOIncurredDate"] =ltDateBOIncurred.Text;
                ViewState["Site"] = ltsite.Text;
                ViewState["CountofBO"] = ltCountofBoIncurred.Text;
                //ViewState["OriginalPenaltyCharge"] = txtOriginalPenaltyValue.Text;
                ViewState["OriginalPenaltyCharge"] = hdnPotentialPenaltyChargeValue.Value; 
                //ViewState["DisputedValue"] = txtValueInDispute.Text;
                ViewState["DisputedValue"] = hdnDisputedChargeValue.Value;
                ViewState["RevisedPenalty"] = txtRevisedPenalty.Text;
                ViewState["BOReportingId"] = hdnBOReportingId.Value;
                ViewState["SiteId"] = hdnsiteid.Value;
                break;
            }

        }
    }

    #endregion

    #region Mail
    private void SendVendorCommunication2()
    {

        getSKUDetialOfSelectedRow();
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        BackOrderBE oBackOrderBE = new BackOrderBE();
        oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();       
        oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
        oBackOrderBE.SiteID = Convert.ToInt32(ViewState["SiteId"]);

        string[] sentTo ;
        string[] language;     

        string[] VendorData = new string[2];
        VendorData = CommonPage.GetVendorOTIFContactWithLanguage(oBackOrderBE.Vendor.VendorID);

        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
        {
            sentTo = VendorData[0].Split(',');
            language = VendorData[1].Split(',');

            for (int j = 0; j < sentTo.Length; j++)
            {
                if (sentTo[j] != " ")
                {
                    SendMailToVendors(sentTo[j], language[j]);
                }
            }
        }

        Page.UICulture = Convert.ToString(Session["CultureInfo"]);

    }


    private void SendMailToVendors(string toAddress, string language)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.User = new SCT_UserBE();
            oBackOrderBE.Action = "GetVendorSPAPDetail";
            oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
            oBackOrderBE.SiteID = Convert.ToInt32(ViewState["SiteId"]);
            var lstBackOrderBE = oBackOrderBAL.GetVendorDetailBAL(oBackOrderBE);

            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {

                #region Logic to Save & Send email ...
                var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                foreach (MAS_LanguageBE objLanguage in oLanguages)
                {
                    bool MailSentInLanguage = false;
                    if (objLanguage.LanguageID.Equals(Convert.ToInt32(language)))
                        MailSentInLanguage = true;

                    var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                    var templateFile = string.Format(@"{0}emailtemplates/BOPenalty/VendorCommunication2.english.htm", path);

                    #region Setting reason as per the language ...
                    switch (objLanguage.LanguageID)
                    {
                        case 1:
                            Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                            break;
                        case 2:
                            Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                            break;
                        case 3:
                            Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                            break;
                        case 4:
                            Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                            break;
                        case 5:
                            Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                            break;
                        case 6:
                            Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                            break;
                        case 7:
                            Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                            break;
                        default:
                            Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                            break;
                    }

                    #endregion

                    #region  Prepairing html body format ...
                    string htmlBody = null;
                    using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                    {
                        htmlBody = sReader.ReadToEnd();
                        //htmlBody = htmlBody.Replace("{logoInnerPath}", getAbsolutePath());

                        htmlBody = htmlBody.Replace("{VendorNumber}", lstBackOrderBE[0].Vendor.Vendor_No);
                        htmlBody = htmlBody.Replace("{VendorName}", lstBackOrderBE[0].Vendor.VendorName);
                        htmlBody = htmlBody.Replace("{VendorAddress1}", lstBackOrderBE[0].Vendor.address1);
                        htmlBody = htmlBody.Replace("{VendorAddress2}", lstBackOrderBE[0].Vendor.address2);
                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.VMPPIN) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.VMPPIN))
                            htmlBody = htmlBody.Replace("{VMPPIN}", lstBackOrderBE[0].Vendor.VMPPIN);
                        else
                            htmlBody = htmlBody.Replace("{VMPPIN}", string.Empty);

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.VMPPOU) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.VMPPOU))
                            htmlBody = htmlBody.Replace("{VMPPOU}", lstBackOrderBE[0].Vendor.VMPPOU);
                        else
                            htmlBody = htmlBody.Replace("{VMPPOU}", string.Empty);

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.city) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.city))
                            htmlBody = htmlBody.Replace("{VendorCity}", lstBackOrderBE[0].Vendor.city);
                        else
                            htmlBody = htmlBody.Replace("{VendorCity}", string.Empty);

                        htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                        htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));
                        htmlBody = htmlBody.Replace("{DateValue}", DateTime.Now.ToString("dd/MM/yyyy"));

                        htmlBody = htmlBody.Replace("{VendorCommunication2Message1}", WebCommon.getGlobalResourceValue("VendorCommunication2Message1"));
                        htmlBody = htmlBody.Replace("{VendorCommunication2Message2}", WebCommon.getGlobalResourceValue("VendorCommunication2Message2"));
                        //{gridValue}
                        #region Logic for creating SKU detail Grid
                        StringBuilder sSkuDetail = new StringBuilder();
                        sSkuDetail.Append("<table style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=100%'>");

                        sSkuDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='10%'>" + OurCode + "</cc1:ucLabel></td><td width='10%'>" + VendorCode + "</td><td width='20%'>" + Description + "</td><td  width='10%'>" + DateBOIncurred + "</td><td  width='10%'>" + Site2 + "</td><td  width='10%'>" + CountOfBoIncurred + "</td><td  width='10%'>" + OriginalPenaltyValue + "</td><td  width='10%'>" + DisputedValue + "</td><td  width='10%'>" + RevisedPenaltyCharge + "</td></tr>");
                        sSkuDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + ViewState["OdSKUNo"] + "</td><td>" + ViewState["VendorCode"] + "</td><td>" + ViewState["Description"]);
                        sSkuDetail.Append("</td><td>" + ViewState["BOIncurredDate"] + "</td><td>" + ViewState["Site"] + "</td><td>" + ViewState["CountofBO"] + "</td>");
                        sSkuDetail.Append("<td>" + ViewState["OriginalPenaltyCharge"] + "</td><td>" + ViewState["DisputedValue"] + "</td><td>" + ViewState["RevisedPenalty"] + "</td></tr>");
                        sSkuDetail.Append("</table>");
                        #endregion

                        htmlBody = htmlBody.Replace(" {GridValue}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br />" + sSkuDetail + "</td></tr>");

                        htmlBody = htmlBody.Replace("{VendorCommunication2Message3}", WebCommon.getGlobalResourceValue("VendorCommunication2Message3"));

                        //Penalty Mgmt change
                        htmlBody = htmlBody.Replace("{VendorCommunication2Message4}", WebCommon.getGlobalResourceValue("VendorCommunication2Message4"));

                        htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));


                        htmlBody = htmlBody.Replace("{StockPlannerName}", lstBackOrderBE[0].Vendor.StockPlannerName);

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.StockPlannerContact) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.StockPlannerContact))
                            htmlBody = htmlBody.Replace("{StockPlannerContactNo}", lstBackOrderBE[0].Vendor.StockPlannerContact);
                        else
                            htmlBody = htmlBody.Replace("{StockPlannerContactNo}", string.Empty);


                        var apAddress = string.Empty;
                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress5) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress6))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4, lstBackOrderBE[0].APAddress5, lstBackOrderBE[0].APAddress6);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress5))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4, lstBackOrderBE[0].APAddress5);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                           && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                           && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2))
                        {
                            apAddress = string.Format("{0},&nbsp;{1}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2);
                        }
                        else
                        {
                            apAddress = lstBackOrderBE[0].APAddress1;
                        }

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].APPincode))
                            apAddress = string.Format("{0},&nbsp;{1}", apAddress, lstBackOrderBE[0].APPincode);

                        htmlBody = htmlBody.Replace("{APClerkAddress}", apAddress);



                    }
                    #endregion

                    #region Sending and saving email details ...

                    //string[] sMailAddress = toAddress.Split(',');
                    var sentToWithLink = new System.Text.StringBuilder();
                    //for (int index = 0; index < sMailAddress.Length; index++)
                    sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("Penalty_ReSendCommunication.aspx?BOReportingId=" + ViewState["BOReportingId"] + "&CommTitle=QueryAgree") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + toAddress + "</a>" + System.Environment.NewLine);

                    var oBackOrderMailBE = new BackOrderMailBE();
                    oBackOrderMailBE.Action = "AddCommunication";
                    oBackOrderMailBE.BOReportingID = Convert.ToInt32(ViewState["BOReportingId"]);
                    oBackOrderMailBE.CommunicationType = "Vendor Communication 2";
                    oBackOrderMailBE.Subject = VendorCommunication2EmailSubject;
                    oBackOrderMailBE.SentTo = toAddress;
                    oBackOrderMailBE.SentDate = DateTime.Now;
                    oBackOrderMailBE.Body = htmlBody;
                    oBackOrderMailBE.SendByID = Convert.ToInt32(Session["UserID"]);
                    oBackOrderMailBE.LanguageId = objLanguage.LanguageID;
                    oBackOrderMailBE.MailSentInLanguage = MailSentInLanguage;
                    oBackOrderMailBE.CommunicationStatus = "Initial";
                    oBackOrderMailBE.SentToWithLink = sentToWithLink.ToString();

                    BackOrderBAL oBackOrderCommBAL = new BackOrderBAL();
                    var CommId = oBackOrderCommBAL.AddCommunicationDetailBAL(oBackOrderMailBE);
                    if (MailSentInLanguage)
                    {
                        var emailToAddress = toAddress;
                        var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                        var emailToSubject = VendorCommunication2EmailSubject;
                        var emailBody = htmlBody;
                        Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                        oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                    }
                    #endregion
                }


                #endregion
            }
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    { /* Do nothing */ }
    public override bool EnableEventValidation
    {
        get { return false; }
        set { /* Do nothing */}
    }
    public string getAbsolutePath()
    {
        string LogoPath = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]);
        string absolutePath = LogoPath + "/Images/OfficeDepotLogo.jpg";
        return absolutePath;
    }
    private void SendInternalCommunication()
    {
        getSKUDetialOfSelectedRow();
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        BackOrderBE oBackOrderBE = new BackOrderBE();
        oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oBackOrderBE.Action = "GetVendorSPAPDetail";
        oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
        oBackOrderBE.SiteID = 0;
        
        var lstBackOrderBE = oBackOrderBAL.GetVendorDetailBAL(oBackOrderBE);
        if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
        {
            string emailIds = Convert.ToString(ViewState["ODManagerID"]);
            #region Logic to Save & Send email ...
            var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

            foreach (MAS_LanguageBE objLanguage in oLanguages)
            {
                bool MailSentInLanguage = false;
                if (objLanguage.LanguageID.Equals(Convert.ToInt32(ViewState["ODLanguageID"])))
                    MailSentInLanguage = true;

                var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                var templateFile = string.Format(@"{0}emailtemplates/BOPenalty/InternalCommunication.english.htm", path);

                #region Setting reason as per the language ...
                switch (objLanguage.LanguageID)
                {
                    case 1:
                        Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                        break;
                    case 2:
                        Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                        break;
                    case 3:
                        Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                        break;
                    case 4:
                        Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                        break;
                    case 5:
                        Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                        break;
                    case 6:
                        Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                        break;
                    case 7:
                        Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                        break;
                    default:
                        Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                        break;
                }

                #endregion

                #region  Prepairing html body format ...
                string htmlBody = null;
                using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                {
                    htmlBody = sReader.ReadToEnd();
                    
                    htmlBody = htmlBody.Replace("{InternalCommunicationMessage1}", WebCommon.getGlobalResourceValue("InternalCommunicationMessage1"));
                    htmlBody = htmlBody.Replace("{VendorNo}", WebCommon.getGlobalResourceValue("VendorNo"));
                    htmlBody = htmlBody.Replace("{VendorNoValue}", lstBackOrderBE[0].Vendor.Vendor_No);
                    htmlBody = htmlBody.Replace("{VendorName}", WebCommon.getGlobalResourceValue("VendorName"));
                    htmlBody = htmlBody.Replace("{VendorNameValue}", lstBackOrderBE[0].Vendor.VendorName);
                    htmlBody = htmlBody.Replace("{OriginalPenaltyValue}", WebCommon.getGlobalResourceValue("OriginalPenaltyValue"));
                    htmlBody = htmlBody.Replace("{OriginalPenaltyValueValue}",Convert.ToString( ViewState["OriginalPenaltyCharge"]));
                    htmlBody = htmlBody.Replace("{ValueBeingDisputedByTheVendor}", WebCommon.getGlobalResourceValue("ValueBeingDisputedByTheVendor"));
                    htmlBody = htmlBody.Replace("{ValueBeingDisputedByTheVendorValue}", Convert.ToString(ViewState["DisputedValue"]));
                    htmlBody = htmlBody.Replace("{InternalCommunicationMessage2}", WebCommon.getGlobalResourceValue("InternalCommunicationMessage2"));
                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                    htmlBody = htmlBody.Replace("{StockPlannerName}", lstBackOrderBE[0].Vendor.StockPlannerName);

                    if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.StockPlannerContact) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.StockPlannerContact))
                        htmlBody = htmlBody.Replace("{StockPlannerContactNo}", lstBackOrderBE[0].Vendor.StockPlannerContact);
                    else
                        htmlBody = htmlBody.Replace("{StockPlannerContactNo}", string.Empty);


                }
                #endregion

                #region Sending and saving email details ...
                string[] sMailAddress = emailIds.Split(',');
                var sentToWithLink = new System.Text.StringBuilder();
                for (int index = 0; index < sMailAddress.Length; index++)
                    sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("Penalty_ReSendCommunication.aspx?BOReportingId=" + ViewState["BOReportingId"] + "&CommTitle=QueryDisagree") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[index] + "</a>" + System.Environment.NewLine);

                var oBackOrderMailBE = new BackOrderMailBE();
                oBackOrderMailBE.Action = "AddCommunication";
                oBackOrderMailBE.BOReportingID = Convert.ToInt32(ViewState["BOReportingId"]); 
                oBackOrderMailBE.CommunicationType = "Internal Communication";
                oBackOrderMailBE.Subject = InternalCommunicationEmailSubject;
                oBackOrderMailBE.SentTo = emailIds;
                oBackOrderMailBE.SentDate = DateTime.Now;
                oBackOrderMailBE.Body = htmlBody;
                oBackOrderMailBE.SendByID = Convert.ToInt32(Session["UserID"]);
                oBackOrderMailBE.LanguageId = objLanguage.LanguageID;
                oBackOrderMailBE.MailSentInLanguage = MailSentInLanguage;
                oBackOrderMailBE.CommunicationStatus = "Initial";
                oBackOrderMailBE.SentToWithLink = sentToWithLink.ToString();

                BackOrderBAL oBackOrderCommBAL = new BackOrderBAL();
                var CommId = oBackOrderCommBAL.AddCommunicationDetailBAL(oBackOrderMailBE);
                if (MailSentInLanguage)
                {
                    var emailToAddress = emailIds;
                    var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                    var emailToSubject = InternalCommunicationEmailSubject;
                    var emailBody = htmlBody;
                    Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                    oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                }
                #endregion
            }


            #endregion
        }

        Page.UICulture = Convert.ToString(Session["CultureInfo"]);

    }


    private void SendMediationEmailCommunication()
    {
        getSKUDetialOfSelectedRow();
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        BackOrderBE oBackOrderBE = new BackOrderBE();
        oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oBackOrderBE.Action = "GetVendorSPAPDetail";
        oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
        oBackOrderBE.SiteID = 0;

        var lstBackOrderBE = oBackOrderBAL.GetVendorDetailBAL(oBackOrderBE);
        if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
        {
            string emailIds = Convert.ToString(ViewState["ODManagerID"]);
            #region Logic to Save & Send email ...
            var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

            foreach (MAS_LanguageBE objLanguage in oLanguages)
            {
                bool MailSentInLanguage = false;
                if (objLanguage.LanguageID.Equals(Convert.ToInt32(ViewState["ODLanguageID"])))
                    MailSentInLanguage = true;

                var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                var templateFile = string.Format(@"{0}emailtemplates/BOPenalty/MediationEmail.english.htm", path);

                #region Setting reason as per the language ...
                switch (objLanguage.LanguageID)
                {
                    case 1:
                        Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                        break;
                    case 2:
                        Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                        break;
                    case 3:
                        Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                        break;
                    case 4:
                        Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                        break;
                    case 5:
                        Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                        break;
                    case 6:
                        Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                        break;
                    case 7:
                        Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                        break;
                    default:
                        Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                        break;
                }

                #endregion

                #region  Prepairing html body format ...
                string htmlBody = null;
                using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                {
                    htmlBody = sReader.ReadToEnd();
                    htmlBody = htmlBody.Replace("{logoInnerPath}", getAbsolutePath());
                    htmlBody = htmlBody.Replace("{Hi}", WebCommon.getGlobalResourceValue("Hi"));
                    htmlBody = htmlBody.Replace("{BOMessage1}", WebCommon.getGlobalResourceValue("BOMessage1"));
                    htmlBody = htmlBody.Replace("{Comments}", txtDisagreeComment.Text);
                    htmlBody = htmlBody.Replace("{BOMessage3}", WebCommon.getGlobalResourceValue("BOMessage3"));
                    htmlBody = htmlBody.Replace("{HREF}", EncryptQuery(HttpContext.Current.Request.Url.ToString().Split('?').ElementAtOrDefault(0).Replace("InventoryDetailedReview.aspx", "Esclationoverview.aspx") + "?NoLogin=1&VendorID=" + Convert.ToInt32(GetQueryStringValue("VendorID").ToString()) + "&PenaltyChargeId=" +
Convert.ToString(ViewState["PenaltyChargeID"])));
                    htmlBody = htmlBody.Replace("{LinkTitle}", WebCommon.getGlobalResourceValue("LinkTitle"));
                    #region Logic for creating SKU detail Grid
                    StringBuilder sSkuDetail = new StringBuilder();                    
                                
                    //gvDisputeDetail.HeaderRow.Cells[0].Visible = false;
                    //for (int i = 0; i <= gvDisputeDetail.Rows.Count - 1; i++)
                    //{
                    //    // Remove the rows.
                    //    RadioButton rbl = (RadioButton)gvDisputeDetail.Rows[i].FindControl("rblSelect");
                    //    CheckBox chk = (CheckBox)gvDisputeDetail.Rows[i].FindControl("chkSelect");
                    //    if (rbl.Checked == false && chk.Checked == false)
                    //    {
                    //        gvDisputeDetail.Rows[i].Visible = false;
                    //    }                      
                    //}

                    for (int i = 0; i <= gvDisputeDetail.Rows.Count - 1; i++)
                    {
                        RadioButton rbl = (RadioButton)gvDisputeDetail.Rows[i].FindControl("rblSelect");
                        rbl.Enabled = false;
                        CheckBox chk = (CheckBox)gvDisputeDetail.Rows[i].FindControl("chkSelect");
                        chk.Enabled = false;
                        CheckBox chkSelectAll = gvDisputeDetail.HeaderRow.Cells[1].FindControl("chkSelectAllText") as CheckBox;
                        chkSelectAll.Enabled = false;
                    }

                    StringWriter tw = new StringWriter(sSkuDetail);
                    HtmlTextWriter hw = new HtmlTextWriter(tw);
                    pnlQueryDetails.RenderControl(hw);

                  //  gvDisputeDetail.HeaderRow.Cells[0].Visible = false;
                    
                    for (int i = 1; i <= gvDisputeDetail.Rows.Count; i++)
                    {
                        int count = 0;
                        RadioButton hdn = (RadioButton)gvDisputeDetail.Rows[i - 1].FindControl("rblSelect");
                        if (i >= 9)
                        {
                            count = i + 1;
                            sSkuDetail.Replace("<label for=\"ctl00_ContentPlaceHolder1_gvDisputeDetail_ctl" + count + "_rblSelect\">" + hdn.Text + "</label>", "");
                        }
                        else
                        {
                            count = i + 1;
                            sSkuDetail.Replace("<label for=\"ctl00_ContentPlaceHolder1_gvDisputeDetail_ctl0" + count + "_rblSelect\">" + hdn.Text + "</label>", "");
                        }

                        string s = "<label for=\"ctl00_ContentPlaceHolder1_gvDisputeDetail_ctl0" + count + "_rblSelect\">" + hdn.Text + "</label>";
                    }
                                       
                    sSkuDetail.Replace("<input name=\"ctl00$ContentPlaceHolder1$txtOriginalPenaltyValue\" type=\"text\" readonly=\"readonly\" id=\"ctl00_ContentPlaceHolder1_txtOriginalPenaltyValue\" class=\"inputbox\" autocomplete=\"off\" style=\"width:100px;\" />",
                     "<input name=\"ctl00$ContentPlaceHolder1$txtOriginalPenaltyValue\" type=\"text\" readonly=\"readonly\" id=\"ctl00_ContentPlaceHolder1_txtValueInDispute\" class=\"inputbox\" autocomplete=\"off\" style=\"width:100px;\" value=" + Convert.ToString(ViewState["OriginalPenaltyCharge"]) + ">"
                     );
                    sSkuDetail.Replace("<input name=\"ctl00$ContentPlaceHolder1$txtValueInDispute\" type=\"text\" readonly=\"readonly\" id=\"ctl00_ContentPlaceHolder1_txtValueInDispute\" class=\"inputbox\" autocomplete=\"off\" style=\"width:100px;\" />",
                     "<input name=\"ctl00$ContentPlaceHolder1$txtValueInDispute\" type=\"text\" readonly=\"readonly\" id=\"ctl00_ContentPlaceHolder1_txtValueInDispute\" class=\"inputbox\" autocomplete=\"off\" style=\"width:100px;\" value=" + Convert.ToString(ViewState["DisputedValue"]) + ">"
                     );
                    //sSkuDetail.Replace("<label for=\"ctl00$ContentPlaceHolder1$txtOriginalPenaltyValue\"> </label>", Convert.ToString(ViewState["OriginalPenaltyCharge"]));
                    //sSkuDetail.Replace("<label for=\"ctl00$ContentPlaceHolder1$txtValueInDispute\"> </label>", Convert.ToString(ViewState["DisputedValue"]));
                    sSkuDetail.Replace("width:50px;", "width:120px");                   
                    sSkuDetail.Replace("<legend>","<legend style='color:#934500;'>");
                    sSkuDetail.Replace("Query Details", "<b> Query Details </b>");
                    
                    #endregion

                    htmlBody = htmlBody.Replace(" {GridValue}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br />" + sSkuDetail.Replace("<th", "<th style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'") + "</td></tr>");

                    //htmlBody = htmlBody.Replace("{VendorCommunication2Message3}", WebCommon.getGlobalResourceValue("VendorCommunication2Message3"));
                    htmlBody = htmlBody.Replace("{ManyThanks}", WebCommon.getGlobalResourceValue("ManyThanksBO"));


                    htmlBody = htmlBody.Replace("{StockPlannerName}", lstBackOrderBE[0].Vendor.StockPlannerName);

                    if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.StockPlannerContact) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.StockPlannerContact))
                        htmlBody = htmlBody.Replace("{StockPlannerContactNo}", lstBackOrderBE[0].Vendor.StockPlannerContact);
                    else
                        htmlBody = htmlBody.Replace("{StockPlannerContactNo}", string.Empty);             

                }
                #endregion

                #region Sending and saving email details ...
                string[] sMailAddress = emailIds.Split(',');
                var sentToWithLink = new System.Text.StringBuilder();
                for (int index = 0; index < sMailAddress.Length; index++)
                    sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("Penalty_ReSendCommunication.aspx?BOReportingId=" + ViewState["BOReportingId"] + "&CommTitle=QueryDisagree") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[index] + "</a>" + System.Environment.NewLine);

                var oBackOrderMailBE = new BackOrderMailBE();
                oBackOrderMailBE.Action = "AddCommunication";
                oBackOrderMailBE.BOReportingID = Convert.ToInt32(ViewState["BOReportingId"]);
                oBackOrderMailBE.CommunicationType = "Mediation Communication";
                oBackOrderMailBE.Subject = MediationCommunicationEmailSubject;
                oBackOrderMailBE.SentTo = emailIds;
                oBackOrderMailBE.SentDate = DateTime.Now;
                oBackOrderMailBE.Body = htmlBody;
                oBackOrderMailBE.SendByID = Convert.ToInt32(Session["UserID"]);
                oBackOrderMailBE.LanguageId = objLanguage.LanguageID;
                oBackOrderMailBE.MailSentInLanguage = MailSentInLanguage;
                oBackOrderMailBE.CommunicationStatus = "Initial";
                oBackOrderMailBE.SentToWithLink = sentToWithLink.ToString();

                BackOrderBAL oBackOrderCommBAL = new BackOrderBAL();
                var CommId = oBackOrderCommBAL.AddCommunicationDetailBAL(oBackOrderMailBE);
                if (MailSentInLanguage)
                {
                    var emailToAddress = emailIds;
                    var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                    var emailToSubject = MediationCommunicationEmailSubject;
                    var emailBody = htmlBody;
                    Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                    oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                }
                #endregion
            }


            #endregion
        }

        Page.UICulture = Convert.ToString(Session["CultureInfo"]);

    }
    #endregion
       

    //protected void chkSelect_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (hdnIsRadiobtnChecked.Value != "rbtnChecked")
    //    {
    //        int PenaltyChargeIDValue = 0;
    //        decimal? PotentialChargeValue = 0;
    //        decimal? DisputedValue = 0;
    //        List<BackOrderBE> lstBackOrderBE = (List<BackOrderBE>)ViewState["lstBackOrderBE"];

    //        foreach (GridViewRow row in gvDisputeDetail.Rows)
    //        {
    //            CheckBox chk = (CheckBox)row.FindControl("chkSelect");
    //            HiddenField hdnPenaltyChargeId = row.FindControl("hdnPenaltyChargeID") as HiddenField;

    //            if (chk.Checked)
    //            {
    //                chkSelectValue = chkSelectValue + 1;
    //                PenaltyChargeIDValue = Convert.ToInt32(hdnPenaltyChargeId.Value);
    //                var items = lstBackOrderBE.Where(x => x.PenaltyChargeID == PenaltyChargeIDValue);

    //                foreach (var item in items)
    //                {
    //                    PotentialChargeValue = PotentialChargeValue + item.PotentialPenaltyCharge;
    //                    DisputedValue = DisputedValue + item.DisputedValue;
    //                }
    //                txtOriginalPenaltyValue.Text = Convert.ToString(PotentialChargeValue);
    //                txtValueInDispute.Text = Convert.ToString(DisputedValue);
    //            }
    //        }

    //        if (chkSelectValue == 0)
    //        {
    //             txtOriginalPenaltyValue.Text = Convert.ToString(ViewState["PotentialPenaltyCharge"]);
    //             txtValueInDispute.Text = Convert.ToString(ViewState["DisputedValue"]);
    //        }

    //        if (chkSelectValue > 1)
    //        {
    //            rdoPartiallyAgreeWithVendor.Visible = false;
    //        }
    //        else
    //        {
    //            rdoPartiallyAgreeWithVendor.Visible = true;
    //        }
    //    }
    //    else
    //    {
    //        rdoPartiallyAgreeWithVendor.Visible = true;
    //    }
    //}


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Reporting.WebForms;
using WebUtilities;

public partial class ModuleUI_StockOverview_BackOrder_BackOrderSubReporting : CommonPage
{
    private const int gridPageSize = 50;
    bool IsExportClicked = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
          

            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;


            if (GetQueryStringValue("OpenSubReport") != null && GetQueryStringValue("OpenSubReport").ToString() == "Yes")
            {
                BindSubReportForVendor();
            }

        }
    }

    protected void btnBackSearch_Click(object sender, EventArgs e)
    {
        Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");
    }


    protected void btnExport_Click(object sender, EventArgs e)
    {

        IsExportClicked = true;
        BindSubReportForVendor();
        WebCommon.Export("DailyBackOrderListing", gvBOListing);
      
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindSubReportForVendor(currnetPageIndx);
    }

    protected void gvDailyBOListing_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        
    }

    private void BindSubReportForVendor(int Page = 1)
    {
        if (GetQueryStringValue("VendorId") != null)
        {
            hdnVendorIDs.Value = GetQueryStringValue("VendorId");

            int iLastdate = DateTime.DaysInMonth(Convert.ToInt32(GetQueryStringValue("DateFrom")), Convert.ToInt32(GetQueryStringValue("MonthValue")));

            hdnToDt.Value = hdnJSToDt.Value = Convert.ToInt32(GetQueryStringValue("DateFrom")) + "-" + Convert.ToInt32(GetQueryStringValue("MonthValue")) + "-" + iLastdate.ToString();
            hdnFromDt.Value = hdnJSFromDt.Value = Convert.ToInt32(GetQueryStringValue("DateFrom")) + "-" + Convert.ToInt32(GetQueryStringValue("MonthValue")) + "-01";

            hdnStockPlannerIDs.Value = (!string.IsNullOrEmpty(GetQueryStringValue("StockPlannerID"))) ? GetQueryStringValue("StockPlannerID") : "-2";

            hdnSiteIDs.Value = (!string.IsNullOrEmpty(GetQueryStringValue("SiteID"))) ? GetQueryStringValue("SiteID") : "-1";
            if (!string.IsNullOrEmpty(GetQueryStringValue("SPNAME_vendor")))
            {
                hdnStockPlannerGroupingID.Value = "-2";
            }
            else
            {
                hdnStockPlannerGroupingID.Value = (!string.IsNullOrEmpty(GetQueryStringValue("StockPlannerGroupingID"))) ? GetQueryStringValue("StockPlannerGroupingID") : "-1";
            }

            hdnODSkUCode.Value = (!string.IsNullOrEmpty(GetQueryStringValue("ODSkUCode"))) ? GetQueryStringValue("ODSkUCode") : "-1";

            hdnVikingSkUCode.Value = (!string.IsNullOrEmpty(GetQueryStringValue("VikingSkUCode"))) ? GetQueryStringValue("VikingSkUCode") : "-1";

            hdnReportType.Value = "DailyBackOrderDetail";
            ltBackOrderReporting.Text = "Daily Back Order Listing";

            hdnStockPlannerNo.Value = (!string.IsNullOrEmpty(GetQueryStringValue("BuyerNo"))) ? GetQueryStringValue("BuyerNo") : "-1"; 

            if (IsExportClicked)
            {
                hdnGridCurrentPageNo.Value = "0";
                hdnGridPageSize.Value = "0";
            }
            else
            {
                hdnGridCurrentPageNo.Value = Page.ToString();
                hdnGridPageSize.Value = gridPageSize.ToString();
            }


            DataView dvSql = (DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty);

          
            if (!IsExportClicked)
            {
                if (dvSql.Count > 0)
                {
                    pager1.ItemCount = Convert.ToDouble(dvSql[0]["TotalRecords"].ToString());
                    pager1.CurrentIndex = Page;
                    btnExportToExcel.Visible = true;
                    pager1.Visible = true;
                }
                else
                {
                    pager1.Visible = false;
                    btnExportToExcel.Visible = false;
                }
            }

            gvBOListing.Visible = true;
            gvBOListing.PageIndex = Page;
            gvBOListing.DataBind();
        }
    }

    protected void gvBOListing_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    Literal ltStockPlannerNo = (Literal)e.Row.FindControl("ltStockPlannerNo");
        //    Literal ltStockPlannerName = (Literal)e.Row.FindControl("ltStockPlannerName");
        //    if (!string.IsNullOrEmpty(GetQueryStringValue("SPNO_vendor")) || !string.IsNullOrEmpty(GetQueryStringValue("SPNAME_vendor")))
        //    {
        //        ltStockPlannerNo.Text = GetQueryStringValue("SPNO_vendor");
        //        ltStockPlannerName.Text = GetQueryStringValue("SPNAME_vendor");
        //    }
        //}
    }
}
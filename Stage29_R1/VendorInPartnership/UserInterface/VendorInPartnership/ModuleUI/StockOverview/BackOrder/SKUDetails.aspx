﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SKUDetails.aspx.cs" 
MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="ModuleUI_StockOverview_BackOrder_SKUDetails" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
 </script>

  <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblSKUDetailNew" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <table width="60%" cellspacing="5" cellpadding="0" border="0">
                <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblSite" Text="Site" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="width: 25%">
                            <asp:Literal ID="ltSite"  runat="server"></asp:Literal>
                        </td>
                        <td style="font-weight: bold;  width: 10%">
                            <cc1:ucLabel ID="lblDescription" Text="Decsription" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="width: 25%">
                            <asp:Literal ID="ltDescription" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <cc1:ucLabel ID="lblVikingCode" Text="Viking Code" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td >
                            <asp:Literal ID="ltVikingCode"  runat="server"></asp:Literal>
                        </td>
                         <td style="font-weight: bold">
                            <cc1:ucLabel ID="lblODCode"  Text="OD Code" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td >
                            <asp:Literal ID="ltODCode" runat="server"></asp:Literal>
                        </td>
                    </tr>
                </table>
             </div>
          </div>   
                   <div class="button-row">
                      <cc1:ucExportToExcel ID="ucExportToExcel1" runat="server" />
                  </div>
           
        <table cellspacing="1" cellpadding="0" border="0" align="center" width="95%">
            <tr>
                <td>
                    <cc1:ucGridView ID="grdSKU_PODetails" Width="95%" runat="server" CssClass="grid" 
                    OnSorting="SortGrid" AllowSorting="true"  >
                              <Columns>
                                       <asp:TemplateField HeaderText="PO #" SortExpression="PurchaseOrderValue">
                                            <HeaderStyle Width="80px" HorizontalAlign="left" />
                                            <ItemStyle HorizontalAlign="left" />
                                            <ItemTemplate>
                                             <asp:HyperLink ID="lblPONo" target="_blank" runat="server" Text='<%# Eval("PurchaseOrderValue") %>' 
                                                    NavigateUrl='<%# EncryptQuery("~/ModuleUI/Appointment/PurchaseOrderInquiry.aspx?&Purchase_order="+Eval("PurchaseOrderValue")+
                                                    "&Description="+Eval("Description")+"&Viking_Code="+Eval("Viking")+"&SiteName="+Eval("SiteName")+"&OD_Code="+Eval("SKU_No")+
                                                    "&VendorId="+Eval("VendorID")+"&SiteID="+Eval("SiteID")+"&Order_raised="+Eval("OrderRaised")+"&Status=Open"+"&Page=SKUDetails")%>'></asp:HyperLink>
                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vendor" SortExpression="Vendor.VendorName">
                                            <HeaderStyle Width="100px" HorizontalAlign="left" />
                                            <ItemStyle HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblVendorNew" runat="server" Text='<%# Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Order Raised" SortExpression="OrderRaised">
                                            <HeaderStyle Width="100px" HorizontalAlign="center" />
                                            <ItemStyle HorizontalAlign="center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblOrderRaisedNew" runat="server" Text='<%#Eval("OrderRaised","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Original Due Date" SortExpression="OriginalDuedate">
                                            <HeaderStyle Width="100px" HorizontalAlign="center" />
                                            <ItemStyle HorizontalAlign="center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblOriginalDueDateNew" runat="server" Text='<%#Eval("OriginalDuedate","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Revised Due Date" SortExpression="RevisedDueDate">
                                            <HeaderStyle Width="100px" HorizontalAlign="center" />
                                            <ItemStyle HorizontalAlign="center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblRevisedDueDateNew" runat="server" Text='<%#Eval("RevisedDueDate","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="BookingDatesearch" SortExpression="BookingDate">
                                            <HeaderStyle Width="100px" HorizontalAlign="center" />
                                            <ItemStyle HorizontalAlign="center" />
                                            <ItemTemplate>
                                                <cc1:ucLabel ID="lblBookedDate" runat="server" Text='<%#Eval("BookingDate","{0:dd-MM-yyyy}") %>'></cc1:ucLabel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                          
                      </td>
                  </tr>
                  <tr style="text-align:center;">
                   <td >
                      <cc1:ucLabel ID="lblNoOpenPOFound"  Font-Bold="true" ForeColor="Red" Visible="false"
                                        runat="server"></cc1:ucLabel>
                      </td>
                  </tr>
            </table>
          

</asp:content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using BaseControlLibrary;
using System.Collections;
using Utilities;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessEntities.ModuleBE.Languages.Languages;
using System.IO;
using System.Text;
using System.Configuration;
public partial class EsclationReview : CommonPage
{


    #region Declarations ...
    private const int gridPageSize = 50;
    bool IsExportClicked = false;
    string OurCode = WebCommon.getGlobalResourceValue("OurCode");
    string VendorCode = WebCommon.getGlobalResourceValue("VendorCode");
    string Description = WebCommon.getGlobalResourceValue("Description");
    string DateBOIncurred = WebCommon.getGlobalResourceValue("DateBOIncurred");
    string Site2 = WebCommon.getGlobalResourceValue("Site");
    string CountOfBoIncurred = WebCommon.getGlobalResourceValue("CountofBO(s)Incurred");
    string OriginalPenaltyValue = WebCommon.getGlobalResourceValue("OriginalPenaltyValue");
    string DisputedValue = WebCommon.getGlobalResourceValue("DisputedValue");
    string RevisedPenaltyCharge = WebCommon.getGlobalResourceValue("RevisedPenaltyCharge");
    string VendorCommunication2EmailSubject = WebCommon.getGlobalResourceValue("VendorCommunication2EmailSubject");
    string InternalCommunicationEmailSubject = WebCommon.getGlobalResourceValue("InternalCommunicationEmailSubject");
    protected string PleaseEnterPenaltyCharge = WebCommon.getGlobalResourceValue("PleaseEnterPenaltyCharge");
    protected string PleaseEnterComments = WebCommon.getGlobalResourceValue("PleaseEnterComments");
    protected string PleaseEnterTime = WebCommon.getGlobalResourceValue("ValidTime");
    protected string PleaseEnterPenaltyChargeAgreedWith = WebCommon.getGlobalResourceValue("PleaseEnterPenaltyChargeAgreedWith");
    protected string ImposedValueCannotGreaterThanCurrentPenalty = WebCommon.getGlobalResourceValue("ImposedValueCannotGreaterThanCurrentPenalty");
    protected string NewVendorCommunication2EmailSubject = WebCommon.getGlobalResourceValue("NewVendorCommunication2EmailSubject");
    protected string PenaltyChargeImposedCannotBeZero = WebCommon.getGlobalResourceValue("PenaltyChargeImposedCannotBeZero");

    #endregion


    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!(string.IsNullOrEmpty(GetQueryStringValue("VendorID"))))
            {
                BindEsclationDetails();
                DateTime dt = DateTime.Now;
                txtAgreedPenaltyDate.innerControltxtDate.Value = dt.ToString("dd/MM/yyyy");
                txtAgreedPenaltyTime.Text = dt.ToString("HH:mm");

            }
            if (!string.IsNullOrEmpty(GetQueryStringValue("NoLogin")) )
            {
                btnBack.Visible = false;              
            }

        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
          if (!string.IsNullOrEmpty(GetQueryStringValue("NoLogin")) )
        {
            MasterPageFile = "~/CommonUI/CMN_MasterPages/NoUser_MasterPage.Master";
        }
    }

    private DateTime ConvertInDateTime(string date, string time)
    {
        string timeType = string.Empty;

        return Convert.ToDateTime(string.Format("{0} {1}:00 {2}", date, time, timeType));
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        string redirectPage = "EsclationOverview.aspx";
        ClientScript.RegisterStartupScript(GetType(), "Load", "<script type='text/javascript'>window.parent.location.href = '" + EncryptQuery(redirectPage) + "'; </script>");
        //EncryptQueryString("EsclationOverview.aspx");
    }
    protected void btnSaveMediationAction_Click(object sender, EventArgs e)
    {

        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        BackOrderBE oBackOrderBE = new BackOrderBE();

        oBackOrderBE.Action = "UpdateMediatorAction";
        oBackOrderBE.MediatorAction = rdoPenaltytobeAppliedNew.Checked ? rdoPenaltytobeAppliedNew.Text : rdoNoPenaltyAppliedNew.Checked ? rdoNoPenaltyAppliedNew.Text : rdoAlternate.Checked ? "AlternateAgreement" : "ReviewedandPending";
        oBackOrderBE.MediatorStatus = rdoPenaltytobeAppliedNew.Checked ? "PenaltyApplied" : rdoNoPenaltyAppliedNew.Checked ?  "NoPenaltyApplied" : rdoAlternate.Checked ? "AlternateAgreement" : "ReviewedandPending" ;
        oBackOrderBE.MediatorComments = txtMediatorComment.Text.Trim();
        if (rdoPenaltytobeAppliedNew.Checked)
        {
            oBackOrderBE.AgreedPenaltyCharge = !string.IsNullOrEmpty(txtPenaltyChargeImposed.Text) ? Convert.ToDecimal(txtPenaltyChargeImposed.Text) : (decimal?)null;
            oBackOrderBE.PenaltyChargeAgreedWith = txtPenaltyChargeAgreedWith.Text;

            string Date = Common.GetMM_DD_YYYY1(txtAgreedPenaltyDate.innerControltxtDate.Value);

            DateTime? dtex = ConvertInDateTime(Date, txtAgreedPenaltyTime.Text);
            oBackOrderBE.PenaltyChargeAgreedDatetime = dtex;
        }
        else
        {
            oBackOrderBE.PenaltyChargeAgreedDatetime = (DateTime?)null;
        }

        if (rdoAlternate.Checked)
        {
            oBackOrderBE.PenaltyChargeAgreedWith = txtPenaltyChargeAgreedWith_1.Text;
        }

        if (rdoNoPenaltyAppliedNew.Checked)
        {
            oBackOrderBE.IsMedaitionRequiredDailyEmail = true;
        }
        else
        {
            oBackOrderBE.IsMedaitionRequiredDailyEmail = false;
        }

        if (GetQueryStringValue("VendorID") != null)
        {
            oBackOrderBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
        }

        if (GetQueryStringValue("InventoryActionDate") != null)
        {
            oBackOrderBE.InventoryActionDate = Common.GetMM_DD_YYYY(GetQueryStringValue("InventoryActionDate"));
        }

        if (GetQueryStringValue("BoDate") != null)
        {
            oBackOrderBE.BOIncurredDate = Common.GetMM_DD_YYYY(GetQueryStringValue("BoDate"));
        }
        
        if (GetQueryStringValue("SiteID") != null)
        {
            oBackOrderBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
        }
        if (GetQueryStringValue("SKUID") != null)
        {
            oBackOrderBE.SKUID = Convert.ToInt32(GetQueryStringValue("SKUID").ToString());
        }

        

        if (!(string.IsNullOrEmpty(Convert.ToString(ViewState["PenaltyChargeID"]))))
            oBackOrderBE.PenaltyChargeID = Convert.ToInt32(ViewState["PenaltyChargeID"]);
        else
        {
            if (GetQueryStringValue("PenaltyChargeId") != null && GetQueryStringValue("PenaltyChargeId") != "0")
            {
                oBackOrderBE.PenaltyChargeID = Convert.ToInt32(GetQueryStringValue("PenaltyChargeId").ToString());
            }
        }

        int? IResult = oBackOrderBAL.UpdateMediatorReviewStatusBAL(oBackOrderBE);

        if (IResult == 0)
        {
            //if (rdoNoPenaltyApplied.Checked)
            //{
            //   // SendVendorCommunication2();
            //}


              if (!string.IsNullOrEmpty(GetQueryStringValue("NoLogin")) )
            {
                mdlQueryMsg.Show();
            }
            else
            {
                string redirectPage = "EsclationOverview.aspx?PreviousPage=EsclationReview";
                ClientScript.RegisterStartupScript(GetType(), "Load", "<script type='text/javascript'>window.parent.location.href = '" + EncryptQuery(redirectPage) + "'; </script>");
                //EncryptQueryString("EsclationOverview.aspx?PreviousPage=EsclationReview");
                //EncryptQueryString("EsclationOverview.aspx");
            }
        }



    }
    protected void btnAcceptOk_Click(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.open('','_self').close();", true);        
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {

        IsExportClicked = true;
        BindEsclationDetails();
        LocalizeGridHeader(gvMediatorReview);
        //WebCommon.ExportHideHidden("BackOrderEsclationOverview", gvMediatorReview);
        WebCommon.ExportHideHidden("BackOrderEsclationOverview", gvMediatorReview, null, IsHideHidden: true, isHideSecondColumn: false);
    }

    protected void gvMediatorReview_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstBackOrderBE"] != null)
        {
            gvMediatorReview.DataSource = (List<BackOrderBE>)ViewState["lstBackOrderBE"];
            gvMediatorReview.PageIndex = e.NewPageIndex;
            gvMediatorReview.DataBind();
        }
    }

    protected void gvMediatorReview_OnDataBound(object sender, EventArgs e)
    {
        try
        {
            for (int i = gvMediatorReview.Rows.Count - 1; i > 0; i--)
            {
                GridViewRow row = gvMediatorReview.Rows[i];
                GridViewRow previousRow = gvMediatorReview.Rows[i - 1];
                var ltDateBOIncurred = (Label)row.FindControl("ltDateBOIncurred");
                var ltDateBOIncurredP = (Label)previousRow.FindControl("ltDateBOIncurred");
                var ltODSKUNO = (Label)row.FindControl("ltodsku");
                var ltODSKUNOP = (Label)previousRow.FindControl("ltodsku");
                var ltSite = (Label)row.FindControl("ltsite");
                var ltSiteP = (Label)previousRow.FindControl("ltsite");

                if (ltODSKUNO.Text == ltODSKUNOP.Text)
                {
                    if (ltDateBOIncurred.Text == ltDateBOIncurredP.Text)
                    {
                        if (ltSite.Text == ltSiteP.Text)
                        {
                            if (previousRow.Cells[1].RowSpan == 0)
                            {
                                if (row.Cells[1].RowSpan == 0)
                                {
                                    previousRow.Cells[0].RowSpan += 2;
                                    previousRow.Cells[1].RowSpan += 2;
                                    previousRow.Cells[2].RowSpan += 2;
                                    previousRow.Cells[3].RowSpan += 2;
                                    previousRow.Cells[4].RowSpan += 2;
                                    previousRow.Cells[5].RowSpan += 2;
                                    previousRow.Cells[6].RowSpan += 2;
                                    previousRow.Cells[7].RowSpan += 2;
                                    previousRow.Cells[8].RowSpan += 2;
                                    previousRow.Cells[9].RowSpan += 2;
                                    previousRow.Cells[10].RowSpan += 2;
                                    previousRow.Cells[11].RowSpan += 2;
                                    previousRow.Cells[12].RowSpan += 2;
                                    previousRow.Cells[17].RowSpan += 2;

                                }
                                else
                                {
                                    previousRow.Cells[0].RowSpan = row.Cells[0].RowSpan + 1;
                                    previousRow.Cells[1].RowSpan = row.Cells[1].RowSpan + 1;
                                    previousRow.Cells[2].RowSpan = row.Cells[2].RowSpan + 1;
                                    previousRow.Cells[3].RowSpan = row.Cells[3].RowSpan + 1;
                                    previousRow.Cells[4].RowSpan = row.Cells[4].RowSpan + 1;
                                    previousRow.Cells[5].RowSpan = row.Cells[5].RowSpan + 1;
                                    previousRow.Cells[6].RowSpan = row.Cells[6].RowSpan + 1;
                                    previousRow.Cells[7].RowSpan = row.Cells[7].RowSpan + 1;
                                    previousRow.Cells[8].RowSpan = row.Cells[8].RowSpan + 1;
                                    previousRow.Cells[9].RowSpan = row.Cells[9].RowSpan + 1;
                                    previousRow.Cells[10].RowSpan = row.Cells[10].RowSpan + 1;
                                    previousRow.Cells[11].RowSpan = row.Cells[11].RowSpan + 1;
                                    previousRow.Cells[12].RowSpan = row.Cells[12].RowSpan + 1;
                                    previousRow.Cells[17].RowSpan = row.Cells[17].RowSpan + 1;

                                }
                                row.Cells[0].Visible = false;
                                row.Cells[1].Visible = false;
                                row.Cells[2].Visible = false;
                                row.Cells[3].Visible = false;
                                row.Cells[4].Visible = false;
                                row.Cells[5].Visible = false;
                                row.Cells[6].Visible = false;
                                row.Cells[7].Visible = false;
                                row.Cells[8].Visible = false;
                                row.Cells[9].Visible = false;
                                row.Cells[10].Visible = false;
                                row.Cells[11].Visible = false;
                                row.Cells[12].Visible = false;
                                row.Cells[17].Visible = false;

                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }


    #endregion

    #region Methods
    private void BindEsclationDetails(int Page = 1)
    {
        try
        {

            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.Action = "GetEsclationReviewDetail";
            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            if (GetQueryStringValue("VendorID") != null)
            {
                oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
            }
            if (GetQueryStringValue("PenaltyChargeId") != null && GetQueryStringValue("PenaltyChargeId") != "0")
            {
                oBackOrderBE.PenaltyChargeID = Convert.ToInt32(GetQueryStringValue("PenaltyChargeId").ToString());
            }
            else
            {
                oBackOrderBE.PenaltyChargeID = null; // To handle case, when vednor did not reply
            }

            if (GetQueryStringValue("InventoryActionDate") != null)
            {
                oBackOrderBE.InventoryActionDate = Common.GetMM_DD_YYYY(GetQueryStringValue("InventoryActionDate"));
            }

            if (GetQueryStringValue("BoDate") != null)
            {
                oBackOrderBE.BOIncurredDate = Common.GetMM_DD_YYYY(GetQueryStringValue("BoDate"));
            }

            if (GetQueryStringValue("SiteID") != null)
            {
                oBackOrderBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID").ToString());
            }
            if (GetQueryStringValue("SKUID") != null)
            {
                oBackOrderBE.SKUID = Convert.ToInt32(GetQueryStringValue("SKUID").ToString());
            }

            if (IsExportClicked)
            {
                oBackOrderBE.GridCurrentPageNo = 0;
                oBackOrderBE.GridPageSize = 0;
            }
            else
            {
                oBackOrderBE.GridCurrentPageNo = Page;
                oBackOrderBE.GridPageSize = gridPageSize;
            }

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetDisputeDetailBAL(oBackOrderBE);

            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {

                btnExportToExcel.Visible = true;
                gvMediatorReview.DataSource = null;
                gvMediatorReview.DataSource = lstBackOrderBE;
                gvMediatorReview.DataBind();

                txtPenaltyValue.Text = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyCharge);
                txtPenaltyChargeImposed.Text = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyCharge);

                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                ViewState["lstBackOrderBE"] = lstBackOrderBE;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;


                if (lstBackOrderBE[0].VendorBOReviewStatus != null)
                {
                    int? id = lstBackOrderBE.First().PenaltyChargeID;
                    BindQueryDetail(id);
                    lblVendorNotRepliedMsg.Visible = false;
                    lblConfirmNextTextMsg.Visible = true;
                }
                else
                {
                    pnlVendorsQuery.Visible = false;
                    pnlStockPlannerResponse.Visible = false;
                    lblVendorNotRepliedMsg.Visible = true;
                    lblConfirmNextTextMsg.Visible = false;
                }


                string MediatorAction = lstBackOrderBE[0].MediatorAction;
                string MediatorComments = lstBackOrderBE[0].MediatorComments;
                if (MediatorAction != null && MediatorComments != null)
                {
                    if (MediatorAction.Equals("ReviewedandPending"))
                    {
                        rdoReviewedPending.Checked = true;
                        txtMediatorComment.Text = MediatorComments;
                        txtMediatorComment.Visible = true;
                    }
                }

                if (lstBackOrderBE[0].VendorBOReviewStatus == null && MediatorAction.Equals("ReviewedandPending")) // Case when Vendor did not reply and Mediator takes Reviewed Pending action
                {
                    if (GetQueryStringValue("PenaltyValue") != null)
                    {
                        txtPenaltyValue.Text = GetQueryStringValue("PenaltyValue");
                        txtPenaltyChargeImposed.Text = GetQueryStringValue("PenaltyValue");
                    }


                    // gvMediatorReview.DataSource = null;
                    // btnExportToExcel.Visible = false;
                    // pager1.Visible = false;
                    // pnlQueryDetails.Visible = false;
                    pnlVendorsQuery.Visible = false;
                    pnlStockPlannerResponse.Visible = false;
                    lblVendorNotRepliedMsg.Visible = true;
                    lblConfirmNextTextMsg.Visible = false;
                    // gvMediatorReview.DataSource = null;
                    // btnExportToExcel.Visible = false;
                    //pager1.Visible = false;
                }

            }

            //else
            //{
            //    if (GetQueryStringValue("PenaltyValue") != null)
            //    {
            //        txtPenaltyValue.Text = GetQueryStringValue("PenaltyValue");
            //        txtPenaltyChargeImposed.Text = GetQueryStringValue("PenaltyValue");
            //    }      


            //    gvMediatorReview.DataSource = null;
            //    btnExportToExcel.Visible = false;
            //    pager1.Visible = false;
            //    pnlQueryDetails.Visible = false;
            //    pnlVendorsQuery.Visible = false;
            //    pnlStockPlannerResponse.Visible = false;
            //    lblVendorNotRepliedMsg.Visible = true;
            //    lblConfirmNextTextMsg.Visible = false;
            //    gvMediatorReview.DataSource = null;
            //    btnExportToExcel.Visible = false;
            //    pager1.Visible = false;
            //}

            gvMediatorReview.PageIndex = Page;
            pager1.CurrentIndex = Page;

            if (gvMediatorReview.Rows.Count > 0)
            {
                RadioButton rbtn = (RadioButton)(gvMediatorReview.Rows[0].FindControl("rblSelect"));
                rbtn.Checked = true;
            }
            oBackOrderBAL = null;
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }
    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currentPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currentPageIndx;
        BindEsclationDetails(currentPageIndx);
    }

    protected void rblSelect_CheckedChanged(object sender, EventArgs e)
    {
        var rbtnText = ((System.Web.UI.WebControls.CheckBox)(sender)).Text;
        int? PenaltyChargeID = Convert.ToInt32(rbtnText);
        BindQueryDetail(PenaltyChargeID);
        getSKUDetialOfSelectedRow();
    }
    private void BindQueryDetail(int? PenaltyChargeID)
    {
        ViewState["PenaltyChargeID"] = PenaltyChargeID;


        #region Getting Vendor Query detail by Penalty charge Id
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        BackOrderBE oBackOrderBE = new BackOrderBE();

        oBackOrderBE.Action = "GetQueryDetail";
        oBackOrderBE.PenaltyChargeID = PenaltyChargeID;

        List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetQueryDetailBAL(oBackOrderBE);

        if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
        {
            pnlVendorsQuery.Visible = true;

            txtOriginalPenaltyValue.Text = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyCharge);
            txtValueInDispute.Text = Convert.ToString(lstBackOrderBE[0].DisputedValue);
            txtRevisedPenalty.Text = Convert.ToString(lstBackOrderBE[0].RevisedPenalty);

            ltRaisedOn.Text = Common.GetDD_MM_YYYY(Convert.ToString(lstBackOrderBE[0].VendorActionDate));
            ltRaisedBy.Text = lstBackOrderBE[0].VendorActionTakenName;
            ltContactDetail.Text = lstBackOrderBE[0].Vendor.VendorContactEmail;
            ltPhoneNumber.Text = lstBackOrderBE[0].Vendor.VendorContactNumber;
            txtQuery.Text = lstBackOrderBE[0].VendorComment;
            hdnsiteid.Value = Convert.ToString(lstBackOrderBE[0].SiteID);

            //Getting Stock Planner response
            if (lstBackOrderBE[0].SPAction != null)
            {
                if (lstBackOrderBE[0].SPAction == "Disagree with the Vendors Dispute")
                {
                    lblMsgFullDecline.Visible = true;
                    lblMsgPartailDecline.Visible = false;
                    txtSuggestedPenaltyCharge.Text = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyCharge);
                }
                else
                {
                    lblMsgFullDecline.Visible = false;
                    lblMsgPartailDecline.Visible = true;
                    txtSuggestedPenaltyCharge.Text = Convert.ToString(lstBackOrderBE[0].ImposedPenaltyCharge);
                }

                txtOriginalPenalty.Text = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyCharge);
               // txtSuggestedPenaltyCharge.Text = Convert.ToString(lstBackOrderBE[0].ImposedPenaltyCharge);
                txtStockPlannerComment.Text = lstBackOrderBE[0].SPComment;
                // txtPenaltyChargeImposed.Text = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyCharge);

                txtVendorDisputedValue.Text = Convert.ToString(lstBackOrderBE[0].DisputedValue);
                txtPenaltyValue.Text = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyCharge);
                txtPenaltyChargeImposed.Text = Convert.ToString(lstBackOrderBE[0].PotentialPenaltyCharge);
            }


        }

        #endregion
    }
    private void getSKUDetialOfSelectedRow()
    {
        foreach (GridViewRow row in gvMediatorReview.Rows)
        {

            RadioButton rdoSelect = row.FindControl("rblSelect") as RadioButton;
            ucLabel ltodsku = row.FindControl("ltodsku") as ucLabel;
            ucLabel ltVendorCode = row.FindControl("ltVendorCode") as ucLabel;
            ucLabel ltDescription = row.FindControl("ltDescription") as ucLabel;
            ucLabel ltDateBOIncurred = row.FindControl("ltDateBOIncurred") as ucLabel;
            ucLabel ltsite = row.FindControl("ltsite") as ucLabel;
            ucLabel ltCountofBoIncurred = row.FindControl("ltCountofBoIncurred") as ucLabel;
            HiddenField hdnBOReportingId = row.FindControl("hdnBOReportingID") as HiddenField;

            if (rdoSelect.Checked && ltodsku.Text != null)
            {
                ViewState["OdSKUNo"] = ltodsku.Text;
                ViewState["VendorCode"] = ltVendorCode.Text;
                ViewState["Description"] = ltDescription.Text;
                ViewState["BOIncurredDate"] = ltDateBOIncurred.Text;
                ViewState["Site"] = ltsite.Text;
                ViewState["CountofBO"] = ltCountofBoIncurred.Text;
                ViewState["OriginalPenaltyCharge"] = txtOriginalPenaltyValue.Text;
                ViewState["DisputedValue"] = txtValueInDispute.Text;
                ViewState["RevisedPenalty"] = txtRevisedPenalty.Text;
                ViewState["BOReportingId"] = hdnBOReportingId.Value;
                ViewState["SiteId"] = hdnsiteid.Value;
                break;
            }

        }
    }
    #endregion


    #region Mail
    private void SendVendorCommunication2()
    {
        try
        {
            getSKUDetialOfSelectedRow();
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();
            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
            oBackOrderBE.SiteID = Convert.ToInt32(ViewState["SiteId"]);

            string[] sentTo;
            string[] language;

            string[] VendorData = new string[2];
            VendorData = CommonPage.GetVendorOTIFContactWithLanguage(oBackOrderBE.Vendor.VendorID);

            if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
            {
                sentTo = VendorData[0].Split(',');
                language = VendorData[1].Split(',');

                for (int j = 0; j < sentTo.Length; j++)
                {
                    if (sentTo[j] != " ")
                    {
                        SendMailToVendors(sentTo[j], language[j]);
                    }
                }
            }

            Page.UICulture = Convert.ToString(Session["CultureInfo"]);
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }

    private void SendMailToVendors(string toAddress, string language)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.User = new SCT_UserBE();
            oBackOrderBE.Action = "GetVendorSPAPDetail";
            oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID").ToString());
            oBackOrderBE.SiteID = Convert.ToInt32(ViewState["SiteId"]);
            var lstBackOrderBE = oBackOrderBAL.GetVendorDetailBAL(oBackOrderBE);

            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {

                #region Logic to Save & Send email ...
                var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

                foreach (MAS_LanguageBE objLanguage in oLanguages)
                {
                    bool MailSentInLanguage = false;
                    if (objLanguage.LanguageID.Equals(Convert.ToInt32(language)))
                        MailSentInLanguage = true;

                    var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                    var templateFile = string.Format(@"{0}emailtemplates/BOPenalty/VendorCommunication2.english.htm", path);

                    #region Setting reason as per the language ...
                    switch (objLanguage.LanguageID)
                    {
                        case 1:
                            Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                            break;
                        case 2:
                            Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                            break;
                        case 3:
                            Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                            break;
                        case 4:
                            Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                            break;
                        case 5:
                            Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                            break;
                        case 6:
                            Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                            break;
                        case 7:
                            Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                            break;
                        default:
                            Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                            break;
                    }

                    #endregion

                    #region  Prepairing html body format ...
                    string htmlBody = null;
                    using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                    {
                        htmlBody = sReader.ReadToEnd();
                        //htmlBody = htmlBody.Replace("{logoInnerPath}", getAbsolutePath());

                        htmlBody = htmlBody.Replace("{VendorNumber}", lstBackOrderBE[0].Vendor.Vendor_No);
                        htmlBody = htmlBody.Replace("{VendorName}", lstBackOrderBE[0].Vendor.VendorName);
                        htmlBody = htmlBody.Replace("{VendorAddress1}", lstBackOrderBE[0].Vendor.address1);
                        htmlBody = htmlBody.Replace("{VendorAddress2}", lstBackOrderBE[0].Vendor.address2);
                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.VMPPIN) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.VMPPIN))
                            htmlBody = htmlBody.Replace("{VMPPIN}", lstBackOrderBE[0].Vendor.VMPPIN);
                        else
                            htmlBody = htmlBody.Replace("{VMPPIN}", string.Empty);

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.VMPPOU) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.VMPPOU))
                            htmlBody = htmlBody.Replace("{VMPPOU}", lstBackOrderBE[0].Vendor.VMPPOU);
                        else
                            htmlBody = htmlBody.Replace("{VMPPOU}", string.Empty);

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.city) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.city))
                            htmlBody = htmlBody.Replace("{VendorCity}", lstBackOrderBE[0].Vendor.city);
                        else
                            htmlBody = htmlBody.Replace("{VendorCity}", string.Empty);

                        htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                        htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));
                        htmlBody = htmlBody.Replace("{DateValue}", DateTime.Now.ToString("dd/MM/yyyy"));

                        htmlBody = htmlBody.Replace("{VendorCommunication2Message1}", WebCommon.getGlobalResourceValue("VendorCommunication2Message1"));
                        htmlBody = htmlBody.Replace("{VendorCommunication2Message2}", WebCommon.getGlobalResourceValue("VendorCommunication2Message2"));
                        //{gridValue}
                        #region Logic for creating SKU detail Grid
                        StringBuilder sSkuDetail = new StringBuilder();
                        sSkuDetail.Append("<table style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=100%'>");

                        sSkuDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='10%'>" + OurCode + "</cc1:ucLabel></td><td width='10%'>" + VendorCode + "</td><td width='20%'>" + Description + "</td><td  width='10%'>" + DateBOIncurred + "</td><td  width='10%'>" + Site2 + "</td><td  width='10%'>" + CountOfBoIncurred + "</td><td  width='10%'>" + OriginalPenaltyValue + "</td><td  width='10%'>" + DisputedValue + "</td><td  width='10%'>" + RevisedPenaltyCharge + "</td></tr>");
                        sSkuDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + ViewState["OdSKUNo"] + "</td><td>" + ViewState["VendorCode"] + "</td><td>" + ViewState["Description"]);
                        sSkuDetail.Append("</td><td>" + ViewState["BOIncurredDate"] + "</td><td>" + ViewState["Site"] + "</td><td>" + ViewState["CountofBO"] + "</td>");
                        sSkuDetail.Append("<td>" + ViewState["OriginalPenaltyCharge"] + "</td><td>" + ViewState["DisputedValue"] + "</td><td>" + ViewState["RevisedPenalty"] + "</td></tr>");
                        sSkuDetail.Append("</table>");
                        #endregion

                        htmlBody = htmlBody.Replace(" {GridValue}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br />" + sSkuDetail + "</td></tr>");

                        htmlBody = htmlBody.Replace("{VendorCommunication2Message3}", WebCommon.getGlobalResourceValue("VendorCommunication2Message3"));

                        //Penalty Mgmt change
                        htmlBody = htmlBody.Replace("{VendorCommunication2Message4}", WebCommon.getGlobalResourceValue("VendorCommunication2Message4"));

                        htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));


                        htmlBody = htmlBody.Replace("{StockPlannerName}", lstBackOrderBE[0].Vendor.StockPlannerName);

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.StockPlannerContact) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.StockPlannerContact))
                            htmlBody = htmlBody.Replace("{StockPlannerContactNo}", lstBackOrderBE[0].Vendor.StockPlannerContact);
                        else
                            htmlBody = htmlBody.Replace("{StockPlannerContactNo}", string.Empty);


                        var apAddress = string.Empty;
                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress5) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress6))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4, lstBackOrderBE[0].APAddress5, lstBackOrderBE[0].APAddress6);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4)
                            && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress5))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4, lstBackOrderBE[0].APAddress5);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                           && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                           && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3))
                        {
                            apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                lstBackOrderBE[0].APAddress3);
                        }
                        else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2))
                        {
                            apAddress = string.Format("{0},&nbsp;{1}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2);
                        }
                        else
                        {
                            apAddress = lstBackOrderBE[0].APAddress1;
                        }

                        if (!string.IsNullOrEmpty(lstBackOrderBE[0].APPincode))
                            apAddress = string.Format("{0},&nbsp;{1}", apAddress, lstBackOrderBE[0].APPincode);

                        htmlBody = htmlBody.Replace("{APClerkAddress}", apAddress);



                    }
                    #endregion

                    #region Sending and saving email details ...

                    //string[] sMailAddress = toAddress.Split(',');
                    var sentToWithLink = new System.Text.StringBuilder();
                    //for (int index = 0; index < sMailAddress.Length; index++)
                    sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("Penalty_ReSendCommunication.aspx?BOReportingId=" + ViewState["BOReportingId"] + "&CommTitle=QueryAgree") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + toAddress + "</a>" + System.Environment.NewLine);

                    var oBackOrderMailBE = new BackOrderMailBE();
                    oBackOrderMailBE.Action = "AddCommunication";
                    oBackOrderMailBE.BOReportingID = Convert.ToInt32(ViewState["BOReportingId"]);
                    oBackOrderMailBE.CommunicationType = "Vendor Communication 2";
                    oBackOrderMailBE.Subject = NewVendorCommunication2EmailSubject;
                    oBackOrderMailBE.SentTo = toAddress;
                    oBackOrderMailBE.SentDate = DateTime.Now;
                    oBackOrderMailBE.Body = htmlBody;
                    oBackOrderMailBE.SendByID = Convert.ToInt32(Session["UserID"]);
                    oBackOrderMailBE.LanguageId = objLanguage.LanguageID;
                    oBackOrderMailBE.MailSentInLanguage = MailSentInLanguage;
                    oBackOrderMailBE.CommunicationStatus = "Initial";
                    oBackOrderMailBE.SentToWithLink = sentToWithLink.ToString();

                    BackOrderBAL oBackOrderCommBAL = new BackOrderBAL();
                    var CommId = oBackOrderCommBAL.AddCommunicationDetailBAL(oBackOrderMailBE);
                    if (MailSentInLanguage)
                    {
                        var emailToAddress = toAddress;
                        var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                        var emailToSubject = NewVendorCommunication2EmailSubject;
                        var emailBody = htmlBody;
                        Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                        oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                    }
                    #endregion
                }


                #endregion
            }
        }
    }
    #endregion
}

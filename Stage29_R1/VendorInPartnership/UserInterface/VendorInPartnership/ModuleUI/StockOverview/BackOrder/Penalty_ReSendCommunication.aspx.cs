﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web.UI;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Languages.Languages;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.StockOverview;
using System.Web.UI.WebControls;
using System.Linq;
using WebUtilities;

public partial class Penalty_ReSendCommunication : CommonPage
{
    #region Declarations ...

    string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
    sendCommunicationCommon oSendCommunicationCommon = new sendCommunicationCommon();
    StringBuilder sSendMailLink = new StringBuilder();

    #endregion

    #region Events ...

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.BindLanguages();
            btnReSendCommunication.Visible = false;
            lblEmail.Visible = true;
            txtEmail.Visible = false;

            if (GetQueryStringValue("VendorID") != null && GetQueryStringValue("DateofEsc") != null)
            {
                var vendorId = Convert.ToInt32(GetQueryStringValue("VendorID"));
                var dateofEscalation = Convert.ToDateTime(GetQueryStringValue("DateofEsc")).ToString("dd/MM/yyyy");

                var backOrderBE = new BackOrderBE();
                backOrderBE.Action = "GetBOEmailEscalationData";
                backOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                backOrderBE.Vendor.VendorID = vendorId;
                backOrderBE.BackOrderMail = new BackOrderMailBE();
                backOrderBE.BackOrderMail.SentDate = Common.GetMM_DD_YYYY(dateofEscalation);
                var backOrderBAL = new BackOrderBAL();
                var lstEmailEscalationData = backOrderBAL.GetGetBOEmailEscalationDataBAL(backOrderBE);
                if (lstEmailEscalationData != null && lstEmailEscalationData.Count > 0)
                {
                    this.BindEmailIds(lstEmailEscalationData);
                    ViewState["lstEmailEscalationData"] = lstEmailEscalationData;

                    var filteredData = lstEmailEscalationData.Find(x => x.BackOrderMail.LanguageId == Convert.ToInt32(drpLanguageId.SelectedValue)
                        && x.BackOrderMail.SentTo == ddlSentEmailIds.SelectedItem.Text);

                    divReSendCommunication.InnerHtml = filteredData.BackOrderMail.Body.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
                    hdSubject.Value = filteredData.BackOrderMail.Subject;
                    btnReSendCommunication.Visible = true;
                    lblEmail.Visible = true;                    
                }
            }
            else 
            {
                this.ShowErrorMessage();
            }

            #region Commented code ...
            /*
            if (GetQueryStringValue("CommTitle") != null && GetQueryStringValue("DebitRaiseId") != null)
            {
                //DebitCancel, DebitRaised
                var CommTitle = GetQueryStringValue("CommTitle");
                ViewState["CommunicationLevel"] = GetQueryStringValue("CommTitle");
                var DebitRaiseId = GetQueryStringValue("DebitRaiseId");
                var discrepancyBAL = new DiscrepancyBAL();
                var discrepancyMailBE = new DiscrepancyMailBE();
                discrepancyMailBE.Action = "GetDebitCommunication";
                discrepancyMailBE.DebitRaiseId = Convert.ToInt32(DebitRaiseId);
                discrepancyMailBE.CommTitle = CommTitle;

                if (GetQueryStringValue("LanguageID") != null)
                    discrepancyMailBE.LanguageId = Convert.ToInt32(GetQueryStringValue("LanguageID"));

                var lstDebitCommunication = discrepancyBAL.GetDebitCommunicationBAL(discrepancyMailBE);
                if (lstDebitCommunication != null && lstDebitCommunication.Count > 0)
                {
                    if (drpLanguageId.Items.FindByValue(Convert.ToString(lstDebitCommunication[0].LanguageId)) != null)
                        drpLanguageId.Items.FindByValue(Convert.ToString(lstDebitCommunication[0].LanguageId)).Selected = true;

                    divReSendCommunication.InnerHtml = lstDebitCommunication[0].mailBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());

                    ViewState["EmailSubject"] = lstDebitCommunication[0].mailSubject;
                    hdSubject.Value = lstDebitCommunication[0].mailSubject;
                    txtEmail.Text = lstDebitCommunication[0].sentTo;
                    btnReSendCommunication.Visible = true;
                    lblEmail.Visible = true;
                    txtEmail.Visible = true;
                }
                else { this.ShowErrorMessage(); }
            }
            else
            {
                if (GetQueryStringValue("CommunicationLevel") != null)
                    ViewState["CommunicationLevel"] = GetQueryStringValue("CommunicationLevel");
                else if (Request.QueryString["CommunicationLevel"] != null)
                    ViewState["CommunicationLevel"] = GetQueryStringValue(Request.QueryString["CommunicationLevel"]);

                int iCommId = 0;
                if (GetQueryStringValue("communicationid") != null)
                {
                    iCommId = Convert.ToInt32(GetQueryStringValue("communicationid"));
                }
                else if (Request.QueryString["communicationid"] != null)
                {
                    iCommId = Convert.ToInt32(Request.QueryString["communicationid"]);
                }

                if (iCommId != 0)
                {
                    DataSet dsCommunication = getCommunicationDetails(iCommId);

                    if (dsCommunication != null && dsCommunication.Tables.Count > 0 && dsCommunication.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            if (drpLanguageId.Items.FindByValue(dsCommunication.Tables[0].Rows[0]["LanguageID"].ToString()) != null)
                                drpLanguageId.Items.FindByValue(dsCommunication.Tables[0].Rows[0]["LanguageID"].ToString()).Selected = true;
                        }
                        catch
                        {
                            //ShowErrorMessage();
                        }
                        divReSendCommunication.InnerHtml = Convert.ToString(dsCommunication.Tables[0].Rows[0]["body"]);
                        hdSubject.Value = Convert.ToString(dsCommunication.Tables[0].Rows[0]["Subject"]);
                        txtEmail.Text = distictEmails(Convert.ToString(dsCommunication.Tables[0].Rows[0]["SentTo"]));
                        btnReSendCommunication.Visible = true;
                        lblEmail.Visible = true;
                        txtEmail.Visible = true;
                        Session["dsCommunication"] = dsCommunication;
                    }
                    else { ShowErrorMessage(); }
                }
                else { ShowErrorMessage(); }
            }
             */
            #endregion
        }
    }

    protected void btnReSendCommunication_Click(object sender, EventArgs e)
    {
        // if (ViewState["lstEmailEscalationData"] != null)
        //{
            var lstEmailEscalationData = (List<BackOrderBE>)ViewState["lstEmailEscalationData"];
            var filteredData = lstEmailEscalationData.Find(x => x.BackOrderMail.LanguageId == Convert.ToInt32(drpLanguageId.SelectedValue)
                && x.BackOrderMail.SentTo == ddlSentEmailIds.SelectedItem.Text);

        //var VendorComm1Subject = Common.getGlobalResourceValue("VendorComm1Subject");
        var ReminderCommSubject = WebCommon.getGlobalResourceValue("ReminderCommSubject");

        var sentToWithLink = new System.Text.StringBuilder();

        sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("Penalty_ReSendCommunication.aspx?VendorId=" + Convert.ToInt32(GetQueryStringValue("VendorID")) + "&CommTitle=QueryAgree") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + ddlSentEmailIds.SelectedItem.Text + "</a>" + System.Environment.NewLine);

        var oBackOrderMailBE = new BackOrderMailBE();
        oBackOrderMailBE.Action = "AddCommunication";
        oBackOrderMailBE.BOReportingID = 0;
        oBackOrderMailBE.CommunicationType = "ResentVendorCommunication4a";
        oBackOrderMailBE.Subject = ReminderCommSubject;
        oBackOrderMailBE.SentTo = ddlSentEmailIds.SelectedItem.Text;
        oBackOrderMailBE.SentDate = DateTime.Now;
        oBackOrderMailBE.Body = filteredData.BackOrderMail.Body;
        oBackOrderMailBE.SendByID = 0;
        oBackOrderMailBE.LanguageId = filteredData.BackOrderMail.LanguageId;
        oBackOrderMailBE.MailSentInLanguage = true;
        oBackOrderMailBE.CommunicationStatus = "ResentFromWebApp";
        oBackOrderMailBE.SentToWithLink = sentToWithLink.ToString();
        oBackOrderMailBE.VendorId = Convert.ToInt32(GetQueryStringValue("VendorID"));
        oBackOrderMailBE.SelectedBOIds = filteredData.BackOrderMail.SelectedBOIds;
        oBackOrderMailBE.VendorEmailIds = filteredData.BackOrderMail.VendorEmailIds;
        BackOrderBAL oBackOrderCommBAL = new BackOrderBAL();
        var CommId = oBackOrderCommBAL.AddCommunicationDetailBAL(oBackOrderMailBE);

        var emailToAddress = ddlSentEmailIds.SelectedItem.Text;
        var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
        var emailToSubject = ReminderCommSubject;
        var emailBody = filteredData.BackOrderMail.Body;
        Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
        oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);

        Response.Write("<script language='javascript'>window.open('', '_self', '');window.close();</script>");
        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "MailToReSend", "window.opener.location.reload();window.close();", true);       
    }

    protected void drpLanguageId_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ViewState["lstEmailEscalationData"] != null)
        {
            var lstEmailEscalationData = (List<BackOrderBE>)ViewState["lstEmailEscalationData"];
            var filteredData = lstEmailEscalationData.Find(x => x.BackOrderMail.LanguageId == Convert.ToInt32(drpLanguageId.SelectedValue)
                && x.BackOrderMail.SentTo == ddlSentEmailIds.SelectedItem.Text);

            divReSendCommunication.InnerHtml = filteredData.BackOrderMail.Body.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
            hdSubject.Value = filteredData.BackOrderMail.Subject;
            btnReSendCommunication.Visible = true;
            lblEmail.Visible = true;
        }
        else
        {
            this.ShowErrorMessage();
        }

        #region Commented code ...
        /*
        if (GetQueryStringValue("CommTitle") != null && GetQueryStringValue("DebitRaiseId") != null)
        {
            var CommTitle = GetQueryStringValue("CommTitle");
            ViewState["CommunicationLevel"] = GetQueryStringValue("CommTitle");
            var DebitRaiseId = GetQueryStringValue("DebitRaiseId");
            var discrepancyBAL = new DiscrepancyBAL();
            var discrepancyMailBE = new DiscrepancyMailBE();
            discrepancyMailBE.Action = "GetDebitCommunicationWithLanguageId";
            discrepancyMailBE.DebitRaiseId = Convert.ToInt32(DebitRaiseId);

            if (CommTitle.Equals("DebitCancel"))
                discrepancyMailBE.CommTitle = "DebitCancel";
            else
                discrepancyMailBE.CommTitle = "DebitRaised";

            discrepancyMailBE.LanguageId = Convert.ToInt32(drpLanguageId.SelectedValue);
            var lstDebitCommunication = discrepancyBAL.GetDebitCommunicationBAL(discrepancyMailBE);
            if (lstDebitCommunication != null && lstDebitCommunication.Count > 0)
            {
                divReSendCommunication.InnerHtml = lstDebitCommunication[0].mailBody.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
                hdSubject.Value = Convert.ToString(ViewState["EmailSubject"]); //lstDebitCommunication[0].mailSubject;
                txtEmail.Text = lstDebitCommunication[0].sentTo;
            }
            else
            {
                this.ShowErrorMessage();
            }
        }
        else
        {
            int iCommId = 0;
            string CommunicationLevel = "communication1";//by defalut
            if (GetQueryStringValue("communicationid") != null)
            {
                iCommId = Convert.ToInt32(GetQueryStringValue("communicationid"));
            }
            else if (Request.QueryString["communicationid"] != null)
            {
                iCommId = Convert.ToInt32(Request.QueryString["communicationid"]);
            }

            if (ViewState["CommunicationLevel"] != null)
                CommunicationLevel = Convert.ToString(ViewState["CommunicationLevel"]).Replace("Resent", string.Empty);
            else if (GetQueryStringValue("CommunicationLevel") != null)
                CommunicationLevel = Convert.ToString(GetQueryStringValue("CommunicationLevel")).Replace("Resent", string.Empty);

            if (iCommId != 0)
            {
                try
                {
                    DataSet dsCommunication = getCommunicationDetails(iCommId, Convert.ToInt32(drpLanguageId.SelectedValue), CommunicationLevel, txtEmail.Text);
                    Session["dsCommunication"] = dsCommunication;
                    hdSubject.Value = Convert.ToString(dsCommunication.Tables[0].Rows[0]["Subject"]);
                    divReSendCommunication.InnerHtml = Convert.ToString(dsCommunication.Tables[0].Rows[0]["body"]);
                }
                catch
                {

                    ShowErrorMessage();
                }
            }
        }
        */
        #endregion
    }

    protected void ddlSentEmailIds_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ViewState["lstEmailEscalationData"] != null)
        {
            var lstEmailEscalationData = (List<BackOrderBE>)ViewState["lstEmailEscalationData"];
            var filteredData = lstEmailEscalationData.Find(x => x.BackOrderMail.LanguageId == Convert.ToInt32(drpLanguageId.SelectedValue)
                && x.BackOrderMail.SentTo == ddlSentEmailIds.SelectedItem.Text);

            divReSendCommunication.InnerHtml = filteredData.BackOrderMail.Body.Replace("{logoInnerPath}", oSendCommunicationCommon.getAbsolutePath());
            hdSubject.Value = filteredData.BackOrderMail.Subject;
            btnReSendCommunication.Visible = true;
            lblEmail.Visible = true;
        }
        else
        {
            this.ShowErrorMessage();
        }        
    }

    #endregion

    #region Methods ...

    private void BindEmailIds(List<BackOrderBE> lstBackOrderBE)
    {
        var filteredComm = (from x in lstBackOrderBE
                            where x.BackOrderMail.MailSentInLanguage
                            select new
                            {
                                PenaltyCommunicationId = x.BackOrderMail.PenaltyCommunicationId,
                                SentTo = x.BackOrderMail.SentTo,
                                LanguageId = x.BackOrderMail.LanguageId
                            }).ToList();

        if (filteredComm.Count > 0 && filteredComm != null)
        {
            ddlSentEmailIds.DataSource = filteredComm;
            ddlSentEmailIds.DataValueField = "PenaltyCommunicationId";
            ddlSentEmailIds.DataTextField = "SentTo";
            ddlSentEmailIds.DataBind();

            ddlSentEmailIds.SelectedIndex = 0;
            var languageId = from x in filteredComm
                             where x.PenaltyCommunicationId == Convert.ToInt32(ddlSentEmailIds.SelectedValue)
                             select x.LanguageId;

            drpLanguageId.SelectedIndex = drpLanguageId.Items.IndexOf(drpLanguageId.Items.FindByValue(languageId.ToString()));
        }
    }

    public string distictEmails(string Emails)
    {
        string[] arrEMails = Emails.Split(new char[] { ',' });

        List<string> lstEmails = new List<string>();
        foreach (string email in arrEMails)
        {
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrWhiteSpace(email) && !lstEmails.Contains(email.Trim()))
                lstEmails.Add(email.Trim());
        }
        return string.Join(",", lstEmails.ToArray());
    }

    private void ShowErrorMessage()
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "showNoMailToReSend", "alert('" + WebUtilities.WebCommon.getGlobalResourceValue("NoMailToResend") + "');", true);
    }

    public DataSet getCommunicationDetails(int iDiscrepancyCommunicaitonID)
    {
        DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
        oDiscrepancyMailBE.Action = "getDiscrepancyCommunciationDetail"; // returns one record by communicationid
        oDiscrepancyMailBE.discrepancyCommunicationID = iDiscrepancyCommunicaitonID;
        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        DataSet ds = oNewDiscrepancyBAL.getDiscrepancyCommunicationBAL(oDiscrepancyMailBE);
        oNewDiscrepancyBAL = null;
        return ds;
    }

    public DataSet getCommunicationDetails(int iDiscrepancyCommunicaitonID, int LanguageID, string CommunicationLevel, string SentTo)
    {
        DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
        oDiscrepancyMailBE.Action = "getDiscrepancyCommunciationDetailWithLanguageID";
        oDiscrepancyMailBE.discrepancyCommunicationID = iDiscrepancyCommunicaitonID;
        oDiscrepancyMailBE.languageID = LanguageID;
        oDiscrepancyMailBE.communicationLevel = CommunicationLevel;
        oDiscrepancyMailBE.sentTo = SentTo;

        DiscrepancyBAL oNewDiscrepancyBAL = new DiscrepancyBAL();
        DataSet ds = oNewDiscrepancyBAL.getDiscrepancyCommunicationItemBAL(oDiscrepancyMailBE);
        oNewDiscrepancyBAL = null;
        return ds;
    }

    public void insertHtml(int DiscrepancyLogID, string EmailLink)
    {
        DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
        DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

        oDiscrepancyBE.GINHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.GINActionRequired = false;

        oDiscrepancyBE.INVHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.INVActionRequired = false;

        oDiscrepancyBE.APHTML = oWorkflowHTML.function3("white", null, "", "");
        oDiscrepancyBE.APActionRequired = false;

        oDiscrepancyBE.VENHTML = oWorkflowHTML.function4("Blue", null, null, "", null, "Letter resent by " + Session["UserName"].ToString() + " to " + EmailLink + " on " + System.DateTime.Now.ToString("dd/MM/yyyy"), "", "", "", null, "");
        oDiscrepancyBE.VenActionRequired = false;

        oDiscrepancyBE.Action = "InsertHTML";
        oDiscrepancyBE.DiscrepancyLogID = DiscrepancyLogID;
        oDiscrepancyBE.LoggedDateTime = DateTime.Now;
        oDiscrepancyBE.LevelNumber = 1;
        int? iResult1 = oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
        oDiscrepancyBAL = null;
    }

    private void BindLanguages()
    {
        APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
        List<MAS_LanguageBE> oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
        drpLanguageId.DataSource = oLanguages;
        drpLanguageId.DataValueField = "LanguageID";
        drpLanguageId.DataTextField = "Language";
        drpLanguageId.DataBind();
    }

    #endregion
}
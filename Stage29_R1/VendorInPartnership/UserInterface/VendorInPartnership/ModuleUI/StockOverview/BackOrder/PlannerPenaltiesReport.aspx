﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PlannerPenaltiesReport.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    Inherits="ModuleUI_StockOverview_BackOrder_PlannerPenaltiesReport" %>

 <%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" language="javascript">
    

     function setValue1(target) {
         document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
     }

     function setValue2(target) {
         document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
     }


</script>
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
     <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
      <h2>
        <cc1:ucLabel ID="lblPlannerPenaltiesReport" runat="server"></cc1:ucLabel>
        <cc1:ucLabel ID="lblPenaltiesPerPlannerReport" runat="server" Visible="false"></cc1:ucLabel>  
    </h2>
    <div class="right-shadow">
  
        <div class="formbox">
    
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings" id="tblSearch" runat="server">
                     <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 89%">
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>                               
                    
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDate" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                       <%-- <td>
                          <table width="40%" border="0" cellspacing="5" cellpadding="0" id="tblDate">
                                <tr>
                            <td >
                                <cc1:ucLabel ID="lblFrom" runat="server" Text="From"></cc1:ucLabel>
                            </td>
                            <td style="width: 1%">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                    onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                            </td>
                            <td style="text-align: center">
                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                            </td>
                            <td style="width: 1%">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                    onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                            </td>
                            </tr>
                            </table>
                            </td>
                        --%>
                         <td>
                            <table width="40%" border="0" cellspacing="5" cellpadding="0" id="tblDate">
                                <tr>
                                    <td>
                                        <cc1:ucDropdownList ID="drpMonthFrom" runat="server" Width="80px">                                            
                                            <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                            <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                            <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                            <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                            <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                            <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                            <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                            <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                            <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                            <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                            <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                        </cc1:ucDropdownList>
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="drpYearFrom" runat="server" Width="60px">
                                        </cc1:ucDropdownList>
                                    </td>
                                    <td style="text-align: center">
                                        <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="drpMonthTo" runat="server" Width="80px">                                           
                                            <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                            <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                            <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                            <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                            <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                            <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                            <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                            <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                            <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                            <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                            <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                        </cc1:ucDropdownList>
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="drpYearTo" runat="server" Width="60px">
                                        </cc1:ucDropdownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                       </tr> 
                   

                     <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblReportType" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td align="left" class="nobold radiobuttonlist">
                            <cc1:ucRadioButton ID="rdoPlannerReport" runat="server" Text="Planner Report" Checked="true" GroupName="ReportType" />
                            <cc1:ucRadioButton ID="rdoInventoryGroupReport" runat="server" Text="Inventory Group Report"  GroupName="ReportType" />                           
                        </td>
                    </tr>

                       <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblReviewType" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td align="left" class="nobold radiobuttonlist">
                            <cc1:ucRadioButton ID="rdoInitialReview" runat="server" Text="Initial Review" Checked="true" GroupName="ReviewType" />
                            <cc1:ucRadioButton ID="rdoSecondaryReview" runat="server" Text="Secondary Review"  GroupName="ReviewType" />                           
                        </td>
                    </tr>

                    
                       
                        <tr>
                    <td align="right" colspan="5">
                        <div class="button-row">
                            <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                OnClick="btnSearch_Click" />
                        </div>
                    </td>
                </tr>        
              </table>


            <div id="divSummaryGrid" runat="server" visible="false">
                        <div class="button-row">
                        <br />
                                             
                            <cc1:ucButton ID="btnExportToExcel" runat="server" Visible="false" CssClass="exporttoexcel button" OnClick="btnExport_Click"
                             Width="109px" Height="20px" /> 
                        </div>
                          <div class="wmd-view">
                            <div class="dynamic-div">
                                <cc1:ucGridView ID="grdBind" ClientIDMode="Static" Width="100%" Height="80%" runat="server"
                                    AllowPaging="true" PageSize="50" CssClass="grid modify-grid"  AutoGenerateColumns="true" EnableViewState="false"
                                     OnPageIndexChanging="OnPageIndexChanging" OnRowDataBound="grdBind_RowDataBound" >
                                    <RowStyle HorizontalAlign="Center"></RowStyle>
                                   
                                </cc1:ucGridView>
                            </div>
                            <asp:Label ID="lblError" runat="server" Visible="false" />
                        </div>
                        <cc1:ucGridView ID="grdBindExport" Visible="false" ClientIDMode="Static" Width="100%"
                            Height="80%" runat="server" CssClass="grid" GridLines="Both" 
                            AutoGenerateColumns="true">                          
                            
                        </cc1:ucGridView>
               
                        <div class="button-row">
                            <br />
                            <br />
                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" align="right" Visible="false"
                                CssClass="button" OnClick="btnBack_Click" />
                        </div>
                 </div>
        
          </div>
        </div>

    </asp:Content>
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="VendorPenaltyMaintenanceEdit.aspx.cs" Inherits="VendorPenaltyMaintenanceEdit"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblVendorPenaltyMaintenance" runat="server"></cc1:ucLabel>
    </h2>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:ValidationSummary ID="vsAdd" runat="server" ValidationGroup="a" DisplayMode="BulletList"
                ShowMessageBox="true" ShowSummary="false" />
            <div class="right-shadow">
                <div class="formbox">
                    <table width="80%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold; width: 14%">
                                <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">:
                            </td>
                            <td style="width: 85%">
                                <cc1:ucLabel ID="lblVendorNoValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblVendorName" runat="server" Text="Vendor Name"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">:
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblVendorNameValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="nobold radiobuttonlist" colspan="3">
                                <cc1:ucLabel ID="lblVendorSuscribePenaltyCharge" runat="server" Text="Does the vendor suscribe to penalty charge"></cc1:ucLabel><br />
                                <cc1:ucRadioButtonList ID="rblPenaltyChargeType" CssClass="radio-fix" runat="server"
                                    RepeatColumns="2" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rblPenaltyChargeType_SelectedIndexChanged">
                                    <asp:ListItem Text="lstYes" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="lstNo" Value="0" Selected="True"></asp:ListItem>
                                </cc1:ucRadioButtonList>
                            </td>
                        </tr>
                        <tr id="trPenaltyBasedon" runat="server" visible="false">
                            <td class="nobold radiobuttonlist" colspan="3">
                                <cc1:ucRadioButtonList ID="rblPenaltyBasedon" CssClass="radio-fix" runat="server"
                                    RepeatColumns="2" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblPenaltyBasedon_SelectedIndexChanged"
                                    AutoPostBack="true">
                                    <asp:ListItem Text="lstPenaltyBasedOnBackOrders" Value="BO"></asp:ListItem>
                                    <asp:ListItem Text="lstPenaltyBasedOnOTIF" Value="OTIF"></asp:ListItem>
                                </cc1:ucRadioButtonList>
                            </td>
                        </tr>
                    </table>
                    <table width="80%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"
                        id="tblPenaltyBasedon" runat="server" visible="false">
                        <tr id="trBORate" runat="server" visible="false">
                            <td style="font-weight: bold;" colspan="3">
                                <cc1:ucLabel ID="lblMsgBORate" runat="server" Text="Please select the back order rate value and the associated currency"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr id="trValueBO" runat="server" visible="false">
                            <td style="font-weight: bold;" colspan="3">
                                <table>
                                    <tr>
                                        <td width="340px" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblValueBO" runat="server" Text="Value per Back Order"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">:
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtValueBO" runat="server" Width="40px" MaxLength="6" onkeyup="AllowDecimalOnly(this);"></cc1:ucTextbox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trOtifPenalty" runat="server" visible="false">
                            <td style="font-weight: bold;" colspan="3">
                                <table>
                                    <tr>
                                        <td width="340px" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblOTIFRate_1" runat="server" Text="lblOTIFRate_1"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">:
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtOTIFRate" runat="server" Width="40px" MaxLength="6" onkeyup="AllowDecimalOnly(this);"></cc1:ucTextbox>
                                            <asp:RegularExpressionValidator ID="revAbsolutePenaltyBelowPercentage" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                                runat="server" ControlToValidate="txtOTIFRate" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trLinePenalty" runat="server" visible="false">
                            <td style="font-weight: bold;" colspan="3">
                                <table>
                                    <tr>
                                        <td width="340px" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblPanaltyValue_1" runat="server" Text="lblPanaltyValue_1"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">:
                                        </td>
                                        <td>
                                            <cc1:ucTextbox ID="txtOtifPenaltyValue" runat="server" Width="40px" MaxLength="6"
                                                onkeyup="AllowDecimalOnly(this);"></cc1:ucTextbox>
                                            <asp:RegularExpressionValidator ID="revAbsoluteAppliedPenaltyPercentage" ValidationExpression="^\d{0,8}(\.\d{0,2})?$"
                                                runat="server" ControlToValidate="txtOtifPenaltyValue" Display="None" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trCurrency" runat="server">
                            <td style="font-weight: bold;" colspan="3">
                                <table>
                                    <tr>
                                        <td width="340px" style="font-weight: bold;">
                                            <cc1:ucLabel ID="lblCurrency" runat="server" Text="Currency"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;">:
                                        </td>
                                        <td>
                                            <cc1:ucDropdownList ID="ddlCurrency" runat="server" Width="150px">
                                            </cc1:ucDropdownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" OnClick="btnSave_Click"
                    ValidationGroup="a" />
                <cc1:ucButton ID="btnBack" runat="server" Text="Back" class="button" OnClick="btnBack_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using BaseControlLibrary;
using System.Collections;
using Utilities;
using System.Data;

public partial class InventoryReview2 : CommonPage
{

    #region Declarations ...
    private const int gridPageSize = 50;
    bool IsExportClicked = false;
    protected string selectCheckboxToAccept = WebCommon.getGlobalResourceValue("SelectCheckboxToAction");
    #endregion


    #region Events..

  

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "InventoryDetailedReview")
            {
                if (Session["DisputeReview"] != null)
                {
                    GetSession();
                }
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;


            string PreviousMonth = DateTime.Now.AddMonths(-1).ToString("MM");
            drpMonthFrom.SelectedValue = PreviousMonth;

            string CurrentMonth = DateTime.Now.ToString("MM");
            drpMonthTo.SelectedValue = CurrentMonth;

            FillControls.FillDropDown(ref drpYearTo, GetLastYears(), "Year", "Year");
            FillControls.FillDropDown(ref drpYearFrom, GetLastYears(), "Year", "Year");

            if (CurrentMonth.Equals("01"))
            {
                int currentYear = DateTime.Now.Year;
                drpYearFrom.SelectedValue = (currentYear - 1).ToString();
            }

            if (GetQueryStringValue("FromDashboard") != null)
            {
                BindDisputeReviewListing();
            }

        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindDisputeReviewListing();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {

        IsExportClicked = true;
        BindDisputeReviewListing();
        LocalizeGridHeader(gvDisputeReview);
        WebCommon.Export("BackOrderPendingApproval", gvDisputeReview);
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //pnlSelection.Visible = true;
        //pnlGrid.Visible = false;

        EncryptQueryString("InventoryReview2.aspx");
    }

    protected void gvInventoryReview_PageIndexChanging(object sender, GridViewPageEventArgs e) 
    {
        if (ViewState["lstBackOrderBE"] != null)
        {
            gvDisputeReview.DataSource = (List<BackOrderBE>)ViewState["lstBackOrderBE"];
            gvDisputeReview.PageIndex = e.NewPageIndex;
            if (Session["DisputeReview"] != null)
            {
                Hashtable htDisputeReview = (Hashtable)Session["DisputeReview"];
                htDisputeReview.Remove("PageIndex");
                htDisputeReview.Add("PageIndex", e.NewPageIndex);
                Session["DisputeReview"] = htDisputeReview;
            }
            gvDisputeReview.DataBind();
        }
    }


    #endregion


    #region Methods..

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currentPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currentPageIndx;
        BindDisputeReviewListing(currentPageIndx);
    }


    private void BindDisputeReviewListing(int Page = 1)
    {

        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();
            oBackOrderBE.User = new SCT_UserBE();

            oBackOrderBE.Action = "GetDisputeReviewListing";
            
            oBackOrderBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
            oBackOrderBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
            oBackOrderBE.SelectedODSkUCode = msSKU.SelectedSKUName;
            if (GetQueryStringValue("FromDashboard") != null)
            {
                oBackOrderBE.DisputeReviewStatus = "Open";
            }
            else
            {
                oBackOrderBE.DisputeReviewStatus = rblDisplayType.SelectedValue == "1" ? "Open" : (rblDisplayType.SelectedValue == "2" ? "Closed" : "All");
            }
            oBackOrderBE.PenaltyType = rblPenaltyType.SelectedValue == "2" ? "OTIF" : (rblPenaltyType.SelectedValue=="3" ? "BO": "All");

             int numberOfDays = DateTime.DaysInMonth(Convert.ToInt16(drpYearTo.SelectedValue), Convert.ToInt16(drpMonthTo.SelectedValue) ); // for numberofdays in month

             if (GetQueryStringValue("FromDashboard") == null)
             {

                 oBackOrderBE.SelectedDateFrom = Convert.ToDateTime(drpYearFrom.SelectedValue + "-" + drpMonthFrom.SelectedValue + "-" + "01");
                 oBackOrderBE.SelectedDateTo = Convert.ToDateTime(drpYearTo.SelectedValue + "-" + drpMonthTo.SelectedValue + "-" + numberOfDays.ToString());
             }


             oBackOrderBE.User.UserID = Convert.ToInt32(Session["UserID"]);
             if (Convert.ToString(Session["Role"]) == "OD - Stock Planner")
             {
                 if (GetQueryStringValue("FromDashboard") != null && GetQueryStringValue("FromDashboard") == "YES")
                 {
                     oBackOrderBE.User.RoleName = Session["Role"] as string;
                 }
             }
             else
             {
                 oBackOrderBE.User.RoleName = null;
             }

            if (IsExportClicked)
            {
                oBackOrderBE.GridCurrentPageNo = 0;
                oBackOrderBE.GridPageSize = 0;
            }
            else
            {
                oBackOrderBE.GridCurrentPageNo = Page;
                oBackOrderBE.GridPageSize = gridPageSize;
            }

            this.SetSession(oBackOrderBE);

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetGetDisputeReviewListingBAL(oBackOrderBE);

            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                btnExportToExcel.Visible = true;
                gvDisputeReview.DataSource = null;
                gvDisputeReview.DataSource = lstBackOrderBE;
                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                ViewState["lstBackOrderBE"] = lstBackOrderBE;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else
            {
                gvDisputeReview.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
            }

            gvDisputeReview.PageIndex = Page;
            pager1.CurrentIndex = Page;
            gvDisputeReview.DataBind();
            oBackOrderBAL = null;

            pnlGrid.Visible = true;
            pnlSelection.Visible = false;

           
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }



    private void SetSession(BackOrderBE oBackOrderBE)
    {
        try
        {
            Session["DisputeReview"] = null;
            Session.Remove("DisputeReview");

            Hashtable htDisputeReview = new Hashtable();

            htDisputeReview.Add("SelectedVendorIDs", oBackOrderBE.SelectedVendorIDs);
            htDisputeReview.Add("SelectedSPIDs", oBackOrderBE.SelectedStockPlannerIDs);
            htDisputeReview.Add("SelectedODSKUCodes", oBackOrderBE.SelectedODSkUCode);
            htDisputeReview.Add("SelectedStatus", oBackOrderBE.DisputeReviewStatus);
            htDisputeReview.Add("PenaltyType", oBackOrderBE.PenaltyType);
            htDisputeReview.Add("SelectedFrom", oBackOrderBE.SelectedDateFrom);
            htDisputeReview.Add("SelectesTo", oBackOrderBE.SelectedDateTo);
            htDisputeReview.Add("UserId", oBackOrderBE.User.UserID);
            htDisputeReview.Add("RoleName", oBackOrderBE.User.RoleName);
            htDisputeReview.Add("PageIndex", oBackOrderBE.GridCurrentPageNo);

            Session["DisputeReview"] = htDisputeReview;
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }


    private void GetSession()
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();
            oBackOrderBE.User = new SCT_UserBE();

            if (Session["DisputeReview"] != null)
            {
                Hashtable htDisputeReview = (Hashtable)Session["DisputeReview"];


                oBackOrderBE.SelectedVendorIDs = (htDisputeReview.ContainsKey("SelectedVendorIDs") && htDisputeReview["SelectedVendorIDs"] != null) ? htDisputeReview["SelectedVendorIDs"].ToString() : null;
                oBackOrderBE.SelectedStockPlannerIDs = (htDisputeReview.ContainsKey("SelectedSPIDs") && htDisputeReview["SelectedSPIDs"] != null) ? htDisputeReview["SelectedSPIDs"].ToString() : null;
                oBackOrderBE.SelectedODSkUCode = (htDisputeReview.ContainsKey("SelectedODSKUCodes") && htDisputeReview["SelectedODSKUCodes"] != null) ? htDisputeReview["SelectedODSKUCodes"].ToString() : null;
                oBackOrderBE.DisputeReviewStatus = htDisputeReview["SelectedStatus"].ToString();
                oBackOrderBE.PenaltyType = htDisputeReview["PenaltyType"].ToString();
                oBackOrderBE.SelectedDateFrom = Convert.ToDateTime(htDisputeReview["SelectedFrom"].ToString());
                oBackOrderBE.SelectedDateTo = Convert.ToDateTime(htDisputeReview["SelectesTo"].ToString());
                oBackOrderBE.User.UserID = Convert.ToInt32(htDisputeReview["UserId"].ToString());
                if (Convert.ToString(Session["Role"]) == "OD - Stock Planner")
                {
                    oBackOrderBE.User.RoleName = Session["Role"] as string;
                }
                else
                {
                    oBackOrderBE.User.RoleName = null;
                }

                oBackOrderBE.Action = "GetDisputeReviewListing";

                oBackOrderBE.GridCurrentPageNo = Convert.ToInt32(htDisputeReview["PageIndex"].ToString());
                oBackOrderBE.GridPageSize = gridPageSize;

                List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetGetDisputeReviewListingBAL(oBackOrderBE);

                ViewState["lstBackOrderBE"] = lstBackOrderBE;
                if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
                {
                    btnExportToExcel.Visible = true;
                    gvDisputeReview.DataSource = lstBackOrderBE;
                    pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                    if (pager1.ItemCount > gridPageSize)
                        pager1.Visible = true;
                    else
                        pager1.Visible = false;
                }
                else
                {
                    gvDisputeReview.DataSource = null;
                    btnExportToExcel.Visible = false;
                    pager1.Visible = false;
                }
                pager1.CurrentIndex = Convert.ToInt32(htDisputeReview["PageIndex"].ToString()); ;
                gvDisputeReview.DataBind();
                oBackOrderBAL = null;

                pnlGrid.Visible = true;
                pnlSelection.Visible = false;


            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }


    }


    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();      

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);
            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
        }
    }


    private DataTable GetLastYears()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Year");
        int iStartYear = DateTime.Now.Year;
        for (int i = iStartYear; i >= iStartYear - 2; i--)
        {
            if (i >= 2014)
            {
                DataRow dr = dt.NewRow();
                dr["Year"] = i;
                dt.Rows.Add(dr);
            }
        }
        return dt;
    }

    #endregion





   

   

 

   

  
}
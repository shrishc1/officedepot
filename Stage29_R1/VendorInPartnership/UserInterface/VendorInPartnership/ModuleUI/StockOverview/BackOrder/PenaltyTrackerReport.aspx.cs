﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Data;
using System.Drawing;
using WebUtilities;
using BaseControlLibrary;
using System.Collections;

public partial class ModuleUI_StockOverview_BackOrder_PenaltyTrackerReport : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        btnExportToExcel.CurrentPage = this;
        btnExportToExcel.GridViewControl = ucGridViewExport;
        btnExportToExcel.FileName = "Penalty Tracker Report";
        if (!IsPostBack)
        {           
            int YearFrom = DateTime.Now.Year;
            int YearTo = DateTime.Now.Year - 10;
            while (YearFrom > YearTo)
            {
                if (YearFrom >= 2015)
                    ddlYear.Items.Add(YearFrom.ToString());
                YearFrom--;
            }
            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage") == "IRP")
            {
                BindGridview();
            }
            
        }
    }
    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        try
        {
            BindGridview();
           
        }
        catch (Exception)
        {
        }
    }

    private void BindGridview()
    {
        BackOrderBE oBackOrderBE = new BackOrderBE();
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage") == "IRP")
        {
            if (Session["PenaltyTracker"] != null)
            {

                Hashtable htInventoryReview1 = (Hashtable)Session["PenaltyTracker"];
                ddlYear.SelectedValue = (htInventoryReview1.ContainsKey("Year") && htInventoryReview1["Year"] != null) ? htInventoryReview1["Year"].ToString() : null;
                oBackOrderBE.SelectedCountryIDs = (htInventoryReview1.ContainsKey("SelectedCountryIDs") && htInventoryReview1["SelectedCountryIDs"] != null) ? htInventoryReview1["SelectedCountryIDs"].ToString() : null;
                oBackOrderBE.SelectedCountryName = (htInventoryReview1.ContainsKey("SelectedCountryName") && htInventoryReview1["SelectedCountryName"] != null) ? htInventoryReview1["SelectedCountryName"].ToString() : null;
                oBackOrderBE.SelectedSiteIDs = (htInventoryReview1.ContainsKey("SelectedSiteId") && htInventoryReview1["SelectedSiteId"] != null) ? htInventoryReview1["SelectedSiteId"].ToString() : null;
                oBackOrderBE.SiteName = (htInventoryReview1.ContainsKey("SelectedSiteName") && htInventoryReview1["SelectedSiteName"] != null) ? htInventoryReview1["SelectedSiteName"].ToString() : null;
                oBackOrderBE.SelectedVendorIDs = (htInventoryReview1.ContainsKey("SelectedVendorIDs") && htInventoryReview1["SelectedVendorIDs"] != null) ? htInventoryReview1["SelectedVendorIDs"].ToString() : null;
                oBackOrderBE.SelectedVendorName = (htInventoryReview1.ContainsKey("SelectedVendorName") && htInventoryReview1["SelectedVendorName"] != null) ? htInventoryReview1["SelectedVendorName"].ToString() : null;
                oBackOrderBE.SelectedStockPlannerIDs = (htInventoryReview1.ContainsKey("SelectedSPIDs") && htInventoryReview1["SelectedSPIDs"] != null) ? htInventoryReview1["SelectedSPIDs"].ToString() : null;
                oBackOrderBE.SelectedStockPlannerName = (htInventoryReview1.ContainsKey("SelectedSPName") && htInventoryReview1["SelectedSPName"] != null) ? htInventoryReview1["SelectedSPName"].ToString() : null;
                oBackOrderBE.SelectedStockPlannerGroupID = (htInventoryReview1.ContainsKey("SelectedStockPlannerGroupID") && htInventoryReview1["SelectedStockPlannerGroupID"] != null) ? htInventoryReview1["SelectedStockPlannerGroupID"].ToString() : null;
                oBackOrderBE.SelectedStockPlannerGroupName = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingName;
                oBackOrderBE.Year = (htInventoryReview1.ContainsKey("Year") && htInventoryReview1["Year"] != null) ? htInventoryReview1["Year"].ToString() : null;
                this.SetSession(oBackOrderBE);

                msCountry.SelectedCountryIDs = oBackOrderBE.SelectedCountryIDs;
                msCountry.SelectedCountryName = oBackOrderBE.SelectedCountryName;
                msSite.SelectedSiteIDs = oBackOrderBE.SelectedSiteIDs;
                msSite.SelectedSiteName = oBackOrderBE.SiteName;
                msVendor.SelectedVendorIDs = oBackOrderBE.SelectedVendorIDs;
                msVendor.SelectedVendorName = oBackOrderBE.SelectedVendorName;
                msStockPlanner.SelectedStockPlannerIDs = oBackOrderBE.SelectedStockPlannerIDs;
                msStockPlanner.SelectedSPName = oBackOrderBE.SelectedStockPlannerName;
                oBackOrderBE.SelectedStockPlannerGroupID = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
                multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingName = oBackOrderBE.SelectedStockPlannerGroupName;
                ddlYear.SelectedValue = oBackOrderBE.Year;
            }
            else
            {
                oBackOrderBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
                oBackOrderBE.SelectedCountryName = msCountry.SelectedCountryName;
                oBackOrderBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
                oBackOrderBE.SiteName = msSite.SelectedSiteName;
                oBackOrderBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
                oBackOrderBE.SelectedVendorName = msVendor.SelectedVendorName;
                oBackOrderBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
                oBackOrderBE.SelectedStockPlannerName = msStockPlanner.SelectedSPName;
                oBackOrderBE.SelectedStockPlannerGroupID = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
                oBackOrderBE.SelectedStockPlannerGroupName = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingName;
                oBackOrderBE.Year = ddlYear.SelectedValue;
                this.SetSession(oBackOrderBE);
            }
        }
        else
        {
            oBackOrderBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
            oBackOrderBE.SelectedCountryName = msCountry.SelectedCountryName;
            oBackOrderBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
            oBackOrderBE.SiteName = msSite.SelectedSiteName;
            oBackOrderBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
            oBackOrderBE.SelectedVendorName = msVendor.SelectedVendorName;
            oBackOrderBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
            oBackOrderBE.SelectedStockPlannerName = msStockPlanner.SelectedSPName;
            oBackOrderBE.SelectedStockPlannerGroupID = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
            oBackOrderBE.SelectedStockPlannerGroupName = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingName;
            oBackOrderBE.Year = ddlYear.SelectedValue;
            this.SetSession(oBackOrderBE);
        }
        
        DataSet ds = oBackOrderBAL.GetPenaltyReviewBAL(oBackOrderBE);
        hdnGridcount.Value =Convert.ToString(ds.Tables[0].Rows.Count);
        ucGridView1.Visible = true;
        ucGridView1.DataSource = ds.Tables[0];
        ucGridView1.DataBind();
        ucGridViewExport.DataSource = ds.Tables[0];
        ucGridViewExport.DataBind();
        UcDataPanel.Visible = false;
        btnExportToExcel.Visible = true;
        btnBack.Visible = true;
    }


    protected void ucGridView1_DataBound(object sender, EventArgs e)
    {
        try
        {

            if (ucGridView1.Rows.Count > 0)
            {
                GridViewRow row = new GridViewRow(1, 1, DataControlRowType.Header, DataControlRowState.Normal);
                TableHeaderCell cell = new TableHeaderCell();
                cell.Text = "";
                cell.ColumnSpan = 1;                
                row.Controls.Add(cell);

                cell = new TableHeaderCell();
                cell.Text = " Stage 1 Charge Opportunity";
                cell.ColumnSpan = 1;
                cell.CssClass = "gridViewToolTip1";
                row.Controls.Add(cell);

                cell = new TableHeaderCell();
                cell.ColumnSpan = 3;
                cell.Text = "Stage 2 - Inventory Initial Review";
                cell.CssClass = "gridViewToolTip2";
                row.Controls.Add(cell);

                cell = new TableHeaderCell();
                cell.ColumnSpan = 3;
                cell.Text = "Stage 3 - Vendor Review";
                cell.CssClass = "gridViewToolTip3";
                row.Controls.Add(cell);

                cell = new TableHeaderCell();
                cell.ColumnSpan = 3;
                cell.Text = "Stage 4 - Inventory Secondary Review";
                cell.CssClass = "gridViewToolTip4";
                row.Controls.Add(cell);

                cell = new TableHeaderCell();
                cell.ColumnSpan = 3;
                cell.Text = " Stage 5 - Mediaton Review";
                cell.CssClass = "gridViewToolTip5";
                row.Controls.Add(cell);

                cell = new TableHeaderCell();
                cell.ColumnSpan = 3;
                cell.Text = "Results";
                cell.CssClass = "gridViewToolTip6";
                row.Controls.Add(cell);
                ucGridView1.HeaderRow.Parent.Controls.AddAt(0, row);
            }

        }
        catch (Exception)
        {
            
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        UcDataPanel.Visible = true;
        ucGridView1.Visible = false;
        ucGridViewExport.DataSource = null;
        ucGridViewExport.DataBind();
        btnExportToExcel.Visible = false;
        btnBack.Visible = false;       
        RetainSearchData();
        Session["PenaltyTracker"] = null;
    }

    public void RetainSearchData()
    {
        //msStockPlanner.setStockPlannerOnPostBack();
        //msVendor.setVendorsOnPostBack();
        multiSelectStockPlannerGrouping.setStockPlannerGroupingsOnPostBack();
        msCountry.SetCountryOnPostBack();
        Hashtable htInventoryReview1 = (Hashtable)Session["PenaltyTracker"];

        //*********** StockPlanner ***************
        string StockPlannerText = (htInventoryReview1.ContainsKey("SelectedSPName") && htInventoryReview1["SelectedSPName"] != null) ? htInventoryReview1["SelectedSPName"].ToString() : "";
        string StockPlannerID = (htInventoryReview1.ContainsKey("SelectedSPUserIDs") && htInventoryReview1["SelectedSPUserIDs"] != null) ? htInventoryReview1["SelectedSPUserIDs"].ToString() : "";
        ucListBox lstRight = msStockPlanner.FindControl("ucLBRight") as ucListBox;
        ucListBox lstLeft = msStockPlanner.FindControl("ucLBLeft") as ucListBox;
        string strDtockPlonnerId = string.Empty;
        if (lstLeft != null && lstRight != null && !string.IsNullOrEmpty(StockPlannerID))
        {
            lstRight.Items.Clear();
            msStockPlanner.SearchStockPlannerClick(StockPlannerText);
            string[] strStockPlonnerIDs = StockPlannerID.Split(',');
            for (int index = 0; index < strStockPlonnerIDs.Length; index++)
            {
                ListItem listItem = lstLeft.Items.FindByValue(strStockPlonnerIDs[index]);
                if (listItem != null)
                {
                    strDtockPlonnerId += strStockPlonnerIDs[index] + ",";
                    lstRight.Items.Add(listItem);
                    lstLeft.Items.Remove(listItem);
                }

            }
            HiddenField hdnSelectedStockPlonner = msStockPlanner.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedStockPlonner.Value = strDtockPlonnerId;
        }

        //********** Vendor ***************
        string txtVendor = (htInventoryReview1.ContainsKey("txtVendor") && htInventoryReview1["txtVendor"] != null) ? htInventoryReview1["txtVendor"].ToString() : "";
        string VendorId = (htInventoryReview1.ContainsKey("SelectedVendorIDs") && htInventoryReview1["SelectedVendorIDs"] != null) ? htInventoryReview1["SelectedVendorIDs"].ToString() : "";
        TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;

        bool IsSearchedByVendorNo = (htInventoryReview1.ContainsKey("IsSearchedByVendorNo") && htInventoryReview1["IsSearchedByVendorNo"] != null) ? Convert.ToBoolean(htInventoryReview1["IsSearchedByVendorNo"]) : false;
        string txtVendorIdText = txtVendorId.Text;
        ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
        string strVendorId = string.Empty;
        int value;
        lstLeftVendor.Items.Clear();
        if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(VendorId) || !string.IsNullOrEmpty(VendorId)))
        {
            lstLeftVendor.Items.Clear();
            lstRightVendor.Items.Clear();

            if (IsSearchedByVendorNo == true)
            {
                msVendor.SearchVendorNumberClick(txtVendor);
                //parsing successful 
            }
            else
            {
                msVendor.SearchVendorClick(txtVendor);
                //parsing failed. 
            }

            if (!string.IsNullOrEmpty(VendorId))
            {
                string[] strIncludeVendorIDs = VendorId.Split(',');
                for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                    if (listItem != null)
                    {

                        strVendorId += strIncludeVendorIDs[index] + ",";
                        lstRightVendor.Items.Add(listItem);
                        lstLeftVendor.Items.Remove(listItem);
                    }
                }
            }

            HiddenField hdnSelectedVendor = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedVendor.Value = strVendorId;
        }


        //*********** Site ***************
        string SiteId = (htInventoryReview1.ContainsKey("SelectedSiteId") && htInventoryReview1["SelectedSiteId"] != null) ? htInventoryReview1["SelectedSiteId"].ToString() : "";
        ucListBox lstRightSite = msSite.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftSite = msSite.FindControl("lstLeft") as ucListBox;
        string msSiteid = string.Empty;
        lstRightSite.Items.Clear();
        lstRightSite.Items.Clear();
        msSite.SelectedSiteIDs = "";
        if (lstLeftSite != null && lstRightSite != null && !string.IsNullOrEmpty(SiteId))
        {
            //lstLeftSite.Items.Clear();
            lstRightSite.Items.Clear();
            msSite.BindSite();

            string[] strSiteIDs = SiteId.Split(',');
            for (int index = 0; index < strSiteIDs.Length; index++)
            {
                ListItem listItem = lstLeftSite.Items.FindByValue(strSiteIDs[index]);
                if (listItem != null)
                {
                    msSiteid = msSiteid + strSiteIDs[index].ToString() + ",";
                    lstRightSite.Items.Add(listItem);
                    lstLeftSite.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msSite.SelectedSiteIDs))
                msSite.SelectedSiteIDs = msSiteid.Trim(',');
        }

    
       



    }

    private void SetSession(BackOrderBE oBackOrderBE)
    {
        Session["PenaltyTracker"] = null;
        Session.Remove("InventoryReview1");

        TextBox txt = (TextBox)msStockPlanner.FindControl("txtUserName");
        TextBox txtVendor = (TextBox)msVendor.FindControl("ucVendor").FindControl("txtVendorNo");


        Hashtable htInventoryReview1 = new Hashtable();
        htInventoryReview1.Add("SelectedSiteId", oBackOrderBE.SelectedSiteIDs);
        htInventoryReview1.Add("SelectedVendorIDs", oBackOrderBE.SelectedVendorIDs);
        htInventoryReview1.Add("SelectedSPIDs", oBackOrderBE.SelectedStockPlannerIDs);
        htInventoryReview1.Add("SelectedCountryIDs", oBackOrderBE.SelectedCountryIDs);
        htInventoryReview1.Add("SelectedStockPlannerGroupID", oBackOrderBE.SelectedStockPlannerGroupID);
        htInventoryReview1.Add("Year", ddlYear.SelectedValue);
        htInventoryReview1.Add("SelectedSPName", txt.Text);
        htInventoryReview1.Add("txtVendor", txtVendor.Text);
        htInventoryReview1.Add("IsSearchedByVendorNo", msVendor.IsSearchedByVendorNo);

        htInventoryReview1.Add("SelectedCountryName", msCountry.SelectedCountryName);
        htInventoryReview1.Add("SelectedSiteName", msSite.SelectedSiteName);
        htInventoryReview1.Add("SelectedVendorName", msVendor.SelectedVendorName);      
        Session["PenaltyTracker"] = htInventoryReview1;
    }
    protected void ucGridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblMonth = (Label)e.Row.FindControl("lblMonth");
            string DateToMonth = string.Empty;
            string DateFromMonth = string.Empty;
            if (string.IsNullOrEmpty(lblMonth.Text))
            {
                DateToMonth="12";
                DateFromMonth = "01";
            }
            else
            {
                DateToMonth = lblMonth.Text;
                DateFromMonth = lblMonth.Text;
            }
           
     
            HyperLink hpPotentialPenaltyCharge = (HyperLink)e.Row.FindControl("hpPotentialPenaltyCharge");
            hpPotentialPenaltyCharge.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
              //  + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&InventoryReviewStatus=AllDrill" 
                + "&PreviousPage=PTR");
           // hpPotentialPenaltyCharge.Target = "_blank";
            HyperLink hpIAccepted = (HyperLink)e.Row.FindControl("hpIAccepted");
            hpIAccepted.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
             //   + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                 + "&VendorBOReviewStatus=" + "AllApproved"
                + "&PreviousPage=PTR");
           // hpIAccepted.Target = "_blank";
            HyperLink hpIDeclined = (HyperLink)e.Row.FindControl("hpIDeclined");

            hpIDeclined.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                 + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
             //   + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                 + "&InventoryReviewStatus=" + "Declined"
                + "&PreviousPage=PTR");
           // hpIDeclined.Target = "_blank";
            HyperLink hpIPending = (HyperLink)e.Row.FindControl("hpIPending");


            hpIPending.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
               // + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&InventoryReviewStatus=" + "Pending"
                + "&PreviousPage=PTR");
           // hpIPending.Target = "_blank";
            HyperLink hpVAccepted = (HyperLink)e.Row.FindControl("hpVAccepted");

            hpVAccepted.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                 + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
             //   + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&VendorBOReviewStatus=" + "Accepted"
                + "&PreviousPage=PTR");
           // hpVAccepted.Target = "_blank";
            HyperLink hpVDeclined = (HyperLink)e.Row.FindControl("hpVDeclined");


            hpVDeclined.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                 + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
              //  + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&VendorBOReviewStatus=" + "AllQuery"
                + "&PreviousPage=PTR");
           // hpVDeclined.Target = "_blank";
            HyperLink hpVPending = (HyperLink)e.Row.FindControl("hpVPending");


            hpVPending.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                  + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
              //  + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&InventoryReviewStatus=" + "Approved"
                + "&PreviousPage=PTR");
           // hpVPending.Target = "_blank";
            HyperLink hpDDeclined = (HyperLink)e.Row.FindControl("hpDDeclined");

            hpDDeclined.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
               + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
             //  + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
               + "&VendorBOReviewStatus=" + "AllDisputeDeclined"
               + "&PreviousPage=PTR");
           // hpDDeclined.Target = "_blank";
            HyperLink hpDAccepted = (HyperLink)e.Row.FindControl("hpDAccepted");

            hpDAccepted.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
               + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
               + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
             //  + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
               + "&VendorBOReviewStatus=" + "DisputeAccepted"
               + "&PreviousPage=PTR");
            //hpDAccepted.Target = "_blank";
            HyperLink hpDPending = (HyperLink)e.Row.FindControl("hpDPending");

            hpDPending.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
               + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
               + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
             //  + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
               + "&VendorBOReviewStatus=" + "Query"
               + "&PreviousPage=PTR");
           // hpDPending.Target = "_blank";
            HyperLink hpMAccepted = (HyperLink)e.Row.FindControl("hpMAccepted");

            hpMAccepted.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
            + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
            + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
           // + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
            + "&MediatorBOReviewStatus=" + "MediatorDeclined"
            + "&PreviousPage=PTR");
          //  hpMAccepted.Target = "_blank";
            HyperLink hpMDeclined = (HyperLink)e.Row.FindControl("hpMDeclined");


            hpMDeclined.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
            + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
             + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
           // + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
            + "&MediatorBOReviewStatus=" + "MediatorAccepted"
            + "&PreviousPage=PTR");
           // hpMDeclined.Target = "_blank";

            HyperLink hpMPending = (HyperLink)e.Row.FindControl("hpMPending");


            hpMPending.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
            + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
            + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
           // + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
            + "&VendorBOReviewStatus=" + "DisputeDeclined"
            + "&PreviousPage=PTR");

          //  hpMPending.Target = "_blank";
            HyperLink hpRAccepted = (HyperLink)e.Row.FindControl("hpRAccepted");

            hpRAccepted.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
            + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
             + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
           // + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
            + "&ViewCharged=Y"
            + "&PreviousPage=PTR");
          //  hpRAccepted.Target = "_blank";
            HyperLink hpRDeclined = (HyperLink)e.Row.FindControl("hpRDeclined");

            hpRDeclined.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
        + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
         + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""
      //  + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
        + "&ViewDeclined=Y"
        + "&PreviousPage=PTR");
           // hpRDeclined.Target = "_blank";
            HyperLink hpRPending = (HyperLink)e.Row.FindControl("hpRPending");

          hpRPending.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
        + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
        + "&DateFrom=" + ddlYear.SelectedValue + "-" + DateFromMonth + "-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-" + DateToMonth + "-" + DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), Convert.ToInt32(DateToMonth)) + ""     
        + "&ViewPending=Y"
        + "&PreviousPage=PTR");
        //  hpRPending.Target = "_blank";
        }

        if (e.Row.RowType == DataControlRowType.Header)
        {
            HyperLink hpHPotentialPenaltyCharge = (HyperLink)e.Row.FindControl("hpHPotentialPenaltyCharge");
            hpHPotentialPenaltyCharge.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
             //   + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&InventoryReviewStatus=AllDrill"
                + "&PreviousPage=PTR");

            HyperLink hpHIAccepted = (HyperLink)e.Row.FindControl("hpHIAccepted");
            hpHIAccepted.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
           //     + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                 + "&VendorBOReviewStatus=" + "AllApproved"
                + "&PreviousPage=PTR");

            HyperLink hpHIDeclined = (HyperLink)e.Row.FindControl("hpHIDeclined");

            hpHIDeclined.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
           //     + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                 + "&InventoryReviewStatus=" + "Declined"
                + "&PreviousPage=PTR");

            HyperLink hpHIPending = (HyperLink)e.Row.FindControl("hpHIPending");


            hpHIPending.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
            //    + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&InventoryReviewStatus=" + "Pending"
                + "&PreviousPage=PTR");

            HyperLink hpHVAccepted = (HyperLink)e.Row.FindControl("hpHVAccepted");

            hpHVAccepted.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
            //    + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&VendorBOReviewStatus=" + "Accepted"
                + "&PreviousPage=PTR");

            HyperLink hpHVDeclined = (HyperLink)e.Row.FindControl("hpHVDeclined");


            hpHVDeclined.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
            //    + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&VendorBOReviewStatus=" + "AllQuery"
                + "&PreviousPage=PTR");

            HyperLink hpHVPending = (HyperLink)e.Row.FindControl("hpHVPending");


            hpHVPending.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
                + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
                + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
             //   + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
                + "&InventoryReviewStatus=" + "Approved"
                + "&PreviousPage=PTR");

            HyperLink hpHDDeclined = (HyperLink)e.Row.FindControl("hpHDDeclined");

            hpHDDeclined.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
               + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
               + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
               + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
            //   + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
               + "&VendorBOReviewStatus=" + "AllDisputeDeclined"
               + "&PreviousPage=PTR");

            HyperLink hpHDAccepted = (HyperLink)e.Row.FindControl("hpHDAccepted");

            hpHDAccepted.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
               + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
               + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
               + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
            //   + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
               + "&VendorBOReviewStatus=" + "DisputeAccepted"
               + "&PreviousPage=PTR");

            HyperLink hpHDPending = (HyperLink)e.Row.FindControl("hpHDPending");

            hpHDPending.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
               + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
               + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
               + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
            //   + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
               + "&VendorBOReviewStatus=" + "Query"
               + "&PreviousPage=PTR");

            HyperLink hpHMAccepted = (HyperLink)e.Row.FindControl("hpHMAccepted");

            hpHMAccepted.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
            + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
            + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
            + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
           // + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
            + "&MediatorBOReviewStatus=" + "MediatorDeclined"
            + "&PreviousPage=PTR");

            HyperLink hpHMDeclined = (HyperLink)e.Row.FindControl("hpHMDeclined");


            hpHMDeclined.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
            + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
            + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
            + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
          //  + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
            + "&MediatorBOReviewStatus=" + "MediatorAccepted"
            + "&PreviousPage=PTR");


            HyperLink hpHMPending = (HyperLink)e.Row.FindControl("hpHMPending");


            hpHMPending.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
            + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
            + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
            + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
          //  + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
            + "&VendorBOReviewStatus=" + "DisputeDeclined"
            + "&PreviousPage=PTR");


            HyperLink hpHRAccepted = (HyperLink)e.Row.FindControl("hpHRAccepted");

            hpHRAccepted.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
            + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
            + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
            + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
          //  + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
           + "&ViewCharged=Y"
            + "&PreviousPage=PTR");

            HyperLink hpHRDeclined = (HyperLink)e.Row.FindControl("hpHRDeclined");

            hpHRDeclined.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
        + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
        + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
        + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
       // + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
        + "&ViewDeclined=Y"
        + "&PreviousPage=PTR");

            HyperLink hpHRPending = (HyperLink)e.Row.FindControl("hpHRPending");

            hpHRPending.NavigateUrl = EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/InventoryReview1.aspx?CountryID=" + msCountry.SelectedCountryIDs + "&SiteID=" + msSite.SelectedSiteIDs
          + "&VendorID=" + msVendor.SelectedVendorIDs + "&StockPlannerID=" + msStockPlanner.SelectedStockPlannerIDs + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
          + "&DateFrom=" + ddlYear.SelectedValue + "-01-01"
          + "&DateTo=" + ddlYear.SelectedValue + "-12-31"
         // + "&StockPlannerGID=" + multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs
           + "&ViewPending=Y"
          + "&PreviousPage=PTR");
            
        }

    }



}
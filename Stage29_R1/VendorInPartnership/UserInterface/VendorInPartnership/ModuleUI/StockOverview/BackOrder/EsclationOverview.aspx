﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="EsclationOverview.aspx.cs" Inherits="EsclationOverview"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectMediator.ascx" TagName="MultiSelectMediator"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });


            $("input[type='radio'][id$='rblSelect']").click(function () {
                SetRadioButton(this);
            });

            function SetRadioButton(spanChk) {

                $("input[type='radio'][id$='rblSelect']").removeAttr("checked");
                spanChk.checked = true;
            }            

            $("#<%=ifUserControl.ClientID %>").load(function () {
                var iFrame = parent.document.getElementById("<%=ifUserControl.ClientID %>");
                newHeight = parseInt(iFrame.offsetHeight) + 840;
                $("#" + iFrame.id).height(newHeight);
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblPenaltyDisputesPendingMediation" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSelection" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblCountry" runat="server" ></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                           <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                             <br />
                        </td>
                       
                    </tr>

                     <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblMediator" runat="server" Text="Mediator"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                           <cc1:MultiSelectMediator runat="server" ID="msMediator" />
                        </td>
                    </tr>
                     <tr>
                        <td style="font-weight: bold; width: 40px" valign="top">
                            <cc1:ucLabel ID="lblView" Text="View" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 10px" valign="top">
                            :
                        </td>
                         <td align="left" class="nobold radiobuttonlist">
                         <cc1:ucRadioButton ID="rdoAllOnly" runat="server" Text="All"
                                Checked="true"  GroupName="ReportType" />
                            <cc1:ucRadioButton ID="rdoPending" runat="server" Text="Pending"
                                GroupName="ReportType" />
                            <cc1:ucRadioButton ID="rdoReviewedPending" runat="server" Text="Reviewed, pending" GroupName="ReportType" />                            
                        </td>
                    </tr>

                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGrid" runat="server" Visible="false">
               <table border="0" cellspacing="20"  style="width: 100%">
                    <tr>                        
                        <td style="width: 250px">                          
                            <cc1:ucRadioButton ID="rdoCheckAll" runat="server" Text="All" Checked="true" AutoPostBack="true"  GroupName="DisputeType" OnCheckedChanged="rdoDisputeType_CheckedChanged"   />
                            <cc1:ucRadioButton ID="rdoVendorDispute" runat="server" Text="Vendor Dispute"  AutoPostBack="true" GroupName="DisputeType" OnCheckedChanged="rdoDisputeType_CheckedChanged" />    
                            <cc1:ucRadioButton ID="rdoVendorDidNot" runat="server" Text="Vendor Did Not Reply" AutoPostBack="true" GroupName="DisputeType" OnCheckedChanged="rdoDisputeType_CheckedChanged" />    
                        </td>                        
                        <td align="right">
                            <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                        Width="109px" Height="20px" />
                        </td>
                    </tr>
                </table>
                <br />               

                
                <%-- <div class="button-row">
                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                        Width="109px" Height="20px" />
                </div>--%>
                <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table"
                    style="width: 100%;">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucGridView ID="gvMediatorReview" runat="server" CssClass="grid gvclass searchgrid-1"
                                GridLines="Both" OnPageIndexChanging="gvInventoryReview_PageIndexChanging" Width="960px">
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                <asp:TemplateField HeaderText="Select">
                                                <HeaderStyle HorizontalAlign="Left" Width="20px" />
                                                <ItemStyle Width="20px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>                                                   
                                                    <asp:RadioButton ID="rblSelect"  CssClass="innerCheckBox"  OnCheckedChanged="rblSelect_CheckedChanged" 
                                                        AutoPostBack="true" runat="server" GroupName="Select"/>
                                                   <%-- <cc1:ucLabel runat="server" ID="lblPenaltyChargeId" Text='<%#Eval("PenaltyChargeID") %>'
                                                        Visible="false"></cc1:ucLabel>--%>
                                                    <asp:HiddenField ID="hdnPenaltyChargeID" Value='<%# Eval("PenaltyChargeID") %>' runat="server" />
                                                    <asp:HiddenField ID="hdnVendorID" Value='<%# Eval("Vendor.VendorID") %>' runat="server" />
                                                    <asp:HiddenField ID="hdnSiteID" Value='<%# Eval("SiteID") %>' runat="server" />
                                                    <asp:HiddenField ID="hdnSKUID" Value='<%# Eval("SKUID") %>' runat="server" />
                                                    <asp:HiddenField ID="hdnBoDate" Value='<%#Eval("BOIncurredDate", "{0:dd/MM/yyyy}")%>' runat="server" />
                                                </ItemTemplate>
                                  </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Country" SortExpression="Vendor.VendorCountryName">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltVendorCountry" Text='<%#Eval("Vendor.VendorCountryName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Site" SortExpression="SKU.SiteName">
                                    <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                    <ItemTemplate>
                                        <cc1:ucLabel runat="server" ID="ltsite" Text='<%#Eval("SKU.SiteName") %>'></cc1:ucLabel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                </asp:TemplateField>

                                    <asp:TemplateField HeaderText="VendorNumberSDR" SortExpression="Vendor.Vendor_No">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                        <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DatePenaltyRaised" SortExpression="InventoryActionDate">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltDateBOIncurred" Text='<%#Eval("InventoryActionDate", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DateDisputeRaised" SortExpression="VendorActionDate">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltVendorActionDate" Text='<%#Eval("VendorActionDate", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DateofInitialReviewed" SortExpression="DisputeReviewActionDate">
                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltDisputeReviewActionDate" Text='<%#Eval("DisputeReviewActionDate", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="InitialDisputeReviewedBy" SortExpression="DisputeReviewActionTakenName">
                                        <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltInitialDisputeReviewedBy" Text='<%#Eval("DisputeReviewActionTakenName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DisputeEsclatedto" SortExpression="DisputeForwaredTo">
                                        <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltDisputeEsclatedto" Text='<%#Eval("DisputeForwaredTo") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dispute Type" SortExpression="DisputeType">
                                        <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltDisputeType" Text='<%#Eval("DisputeType") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="100px" Font-Size="10px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Penalty Reviewed" SortExpression="IsPenaltyReviewed">
                                        <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltIsPenaltyReviewed" Text='<%#Eval("IsPenaltyReviewed") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="100px" Font-Size="10px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PenaltyValue" SortExpression="PotentialPenaltyCharge">
                                        <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                             <cc1:ucLabel runat="server" ID="ltPenaltyValue" Text='<%#Eval("PotentialPenaltyCharge") %>'></cc1:ucLabel>
                                           <%-- <asp:HyperLink ID="hlPenaltyValue" runat="server" Text='<%# Eval("PotentialPenaltyCharge") %>'
                                                NavigateUrl='<%# EncryptQuery("EsclationReview.aspx?VendorID="+Eval("Vendor.VendorID")+"&PenaltyChargeId="+Eval("PenaltyChargeID")+"&NoLogin="+GetQueryStringValue("NoLogin")
                                                            +"&PenaltyValue="+Eval("PotentialPenaltyCharge")+"&SiteID="+Eval("SiteID")+"&InventoryActionDate="+Eval("InventoryActionDate")+"&SKUID="+Eval("SKUID"))%>'></asp:HyperLink>--%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="90px" Font-Size="10px" />
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                           <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                            </cc1:PagerV2_8>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <iframe id="ifUserControl" runat="server" frameborder="0" scrolling="no" height="400px"
                                width="954" style="margin: 0px 0px 0px 0px; background-image: url('../Images/conteint-mainbg.jpg') no-repeat scroll right bottom transparent;">
                            </iframe>                        
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <div class="button-row">
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

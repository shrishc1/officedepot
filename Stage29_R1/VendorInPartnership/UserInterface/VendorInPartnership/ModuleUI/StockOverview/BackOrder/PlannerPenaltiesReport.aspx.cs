﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Data;
using WebUtilities;
using Utilities;
using System.Collections;
using BaseControlLibrary;

public partial class ModuleUI_StockOverview_BackOrder_PlannerPenaltiesReport : CommonPage
{
    protected string RecordNotFound = WebCommon.getGlobalResourceValue("RecordNotFound");
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            //txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //hdnJSFromDt.Value = txtFromDate.Text;
            //hdnJSToDt.Value = txtToDate.Text;            

            string PreviousMonth = DateTime.Now.AddMonths(-1).ToString("MM");
            drpMonthFrom.SelectedValue = PreviousMonth;

            string CurrentMonth = DateTime.Now.ToString("MM");
            drpMonthTo.SelectedValue = CurrentMonth;

            FillControls.FillDropDown(ref drpYearTo, GetLastYears(), "Year", "Year");
            FillControls.FillDropDown(ref drpYearFrom, GetLastYears(), "Year", "Year");

            if (CurrentMonth.Equals("01"))
            {
                int currentYear = DateTime.Now.Year;
                drpYearFrom.SelectedValue = (currentYear - 1).ToString();
            }

            int numberOfDays = DateTime.DaysInMonth(Convert.ToInt16(drpYearTo.SelectedValue), Convert.ToInt16(drpMonthTo.SelectedValue)); // for numberofdays in month

            hdnJSFromDt.Value = Convert.ToString(drpYearFrom.SelectedValue + "-" + drpMonthFrom.SelectedValue + "-" + "01");
            hdnJSToDt.Value = Convert.ToString(drpYearTo.SelectedValue + "-" + drpMonthTo.SelectedValue + "-" + numberOfDays.ToString());

            if (GetQueryStringValue("InventoryGroup") != null && GetQueryStringValue("InventoryGroup") != "")
            {
                if (GetQueryStringValue("ReviewBy") != null)
                {
                    string strReviewBy = GetQueryStringValue("ReviewBy");
                    rdoInventoryGroupReport.Checked = false;                    
                    rdoPlannerReport.Checked = true;   
                    
                    if(strReviewBy.Equals("PlannerInitialReview"))
                    {
                        rdoInitialReview.Checked = true;
                        BindOnQueryString("ByPlanner", "PlannerInitialReview");
                    }
                    else if (strReviewBy.Equals("PlannerSecondaryReview"))
                    {
                        rdoSecondaryReview.Checked = true;
                        BindOnQueryString("ByPlanner", "PlannerSecondaryReview");
                    }                                     
                    
                    

                }
            }
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("PreviousPage") != null && (GetQueryStringValue("PreviousPage") == "PlannerPenaltiesReport"))
            {
                if (Session["PlannerPenaltiesReport"] != null)
                {
                    RetainSearchData();
                }
            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGridview();
    }

    protected void grdBind_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (divSummaryGrid.Visible == true)
            {
                var URL = "";
                dynamic firstCell = e.Row.Cells[0];
                firstCell.Controls.Clear();

                if (rdoInventoryGroupReport.Checked)
                {
                    if (rdoInitialReview.Checked)
                    {
                        string firstCellValue = firstCell.Text;
                        URL = EncryptQuery("PlannerPenaltiesReport.aspx?InventoryGroup=" + firstCellValue.Replace("&amp;", "()") + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "&ReviewBy=PlannerInitialReview");
                    }
                    else if (rdoSecondaryReview.Checked)
                    {
                        string firstCellValue = firstCell.Text;
                        URL = EncryptQuery("PlannerPenaltiesReport.aspx?InventoryGroup=" + firstCellValue.Replace("&amp;", "()") + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "&ReviewBy=PlannerSecondaryReview");
                    }

                    firstCell.Controls.Add(new HyperLink
                    {
                        NavigateUrl = URL,
                        Target = "_blank",
                        Text = firstCell.Text
                    });
                }
            }
        }
    }

    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        divSummaryGrid.Visible = true;
        btnBack.Visible = true;
        lblError.Text = "";
        lblError.Visible = false;
        btnExportToExcel.Visible = true;
        grdBind.PageIndex = e.NewPageIndex;
        grdBind.DataSource = (DataTable)ViewState["DataSource"];
        grdBind.DataBind();

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        divSummaryGrid.Visible = false;
        btnBack.Visible = false;
        tblSearch.Visible = true;

        if (GetQueryStringValue("ReviewBy") != null)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
        }
        else
        {
            EncryptQueryString("PlannerPenaltiesReport.aspx?PreviousPage=PlannerPenaltiesReport");
        }
        
        //if (Request.RawUrl.Split('?').Length > 1)
        //{  
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
        //}
        //else
        //{
        //    EncryptQueryString("PlannerPenaltiesReport.aspx?PreviousPage=PlannerPenaltiesReport");
        //}
    }


    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("ReviewBy") != null)
        {
            string strReviewBy = GetQueryStringValue("ReviewBy");

            if (strReviewBy.Equals("PlannerInitialReview"))
            {
                rdoInitialReview.Checked = true;
                rdoSecondaryReview.Checked = false;
            }
            else if (strReviewBy.Equals("PlannerSecondaryReview"))
            {
                rdoSecondaryReview.Checked = true;
                rdoInitialReview.Checked = false;
            }
        }

        if (rdoPlannerReport.Checked)
        {
            if (rdoInitialReview.Checked)
            {
                WebCommon.ExportHideHidden("Planner_InitialReview", grdBindExport, null, false, true);
            }
            else if (rdoSecondaryReview.Checked)
            {
                WebCommon.ExportHideHidden("Planner_SecondaryReview", grdBindExport, null, false, true);
            }
        }
        else if (rdoInventoryGroupReport.Checked)
        {
            if (rdoInitialReview.Checked)
            {
                WebCommon.ExportHideHidden("InventoryGroup_InitialReview", grdBindExport, null, false, true);
            }
            else if (rdoSecondaryReview.Checked)
            {
                WebCommon.ExportHideHidden("InventoryGroup_SecondaryReview", grdBindExport, null, false, true);
            }
        }
    }

    public void BindGridview(int page = 1)
    {
        divSummaryGrid.Visible = true;
        btnBack.Visible = true;
        BackOrderBE oBackOrderBE = new BackOrderBE();
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();

        oBackOrderBE.Action = "GetPenaltiesPerPlannerReport";

        //oBackOrderBE.SelectedDateFrom = Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
        //oBackOrderBE.SelectedDateTo = Common.GetMM_DD_YYYY(hdnJSToDt.Value);

        int numberOfDays = DateTime.DaysInMonth(Convert.ToInt16(drpYearTo.SelectedValue), Convert.ToInt16(drpMonthTo.SelectedValue)); // for numberofdays in month
        
        oBackOrderBE.SelectedDateFrom = Convert.ToDateTime(drpYearFrom.SelectedValue + "-" + drpMonthFrom.SelectedValue + "-" + "01");
        oBackOrderBE.SelectedDateTo = Convert.ToDateTime(drpYearTo.SelectedValue + "-" + drpMonthTo.SelectedValue + "-" + numberOfDays.ToString());

        hdnJSFromDt.Value = Convert.ToString(oBackOrderBE.SelectedDateFrom);
        hdnJSToDt.Value = Convert.ToString(oBackOrderBE.SelectedDateTo);

        if (!(string.IsNullOrEmpty(msVendor.SelectedVendorIDs)))
        {
            oBackOrderBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
        }

        if (!(string.IsNullOrEmpty(msCountry.SelectedCountryIDs)))
        {
            oBackOrderBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
        }
        if (!(string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs)))
        {
            oBackOrderBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        }

        if (rdoPlannerReport.Checked)
        {
            oBackOrderBE.SummaryBy = "ByPlanner";
        }
        else if (rdoInventoryGroupReport.Checked)
        {
            oBackOrderBE.SummaryBy = "ByInventory";
        }


        if (rdoInitialReview.Checked)
        {
            oBackOrderBE.ReviewBy = "PlannerInitialReview";
        }
        else if (rdoSecondaryReview.Checked)
        {
            oBackOrderBE.ReviewBy = "PlannerSecondaryReview";
        }

        DataSet ds = oBackOrderBAL.GetPlannerPenaltyReportBAL(oBackOrderBE);
        this.SetSession(oBackOrderBE);

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["DataSource"] = ds.Tables[0];
                Session["InputFields"] = oBackOrderBE;
                grdBind.DataSource = ds.Tables[0];
                grdBind.DataBind();
                grdBindExport.DataSource = ds.Tables[0];
                grdBindExport.DataBind();
                lblPenaltiesPerPlannerReport.Visible = true;
                lblPlannerPenaltiesReport.Visible = false;
                lblError.Text = "";
                lblError.Visible = false;
                tblSearch.Visible = false;
                btnExportToExcel.Visible = true;

            }
            else
            {
                grdBind.DataSource = null;
                grdBind.DataBind();
                lblError.Visible = true;
                lblError.Text = RecordNotFound;
                lblPenaltiesPerPlannerReport.Visible = true;
                lblPlannerPenaltiesReport.Visible = false;
                tblSearch.Visible = false;
                btnExportToExcel.Visible = false;
            }
        }
        else
        {
            grdBind.DataSource = null;
            grdBind.DataBind();
            lblError.Visible = true;
            lblError.Text = RecordNotFound;
            lblPenaltiesPerPlannerReport.Visible = true;
            lblPlannerPenaltiesReport.Visible = false;
        }

    }

    public void BindOnQueryString(string action, string ReviewBy)
    {
        divSummaryGrid.Visible = true;
        btnBack.Visible = true;
        lblPenaltiesPerPlannerReport.Visible = true;
        lblPlannerPenaltiesReport.Visible = false;

        if (Session["InputFields"] != null)
        {
            BackOrderBE oBackOrderBE = (BackOrderBE)Session["InputFields"];
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();

            //oBackOrderBE.SelectedDateFrom = Common.GetMM_DD_YYYY(GetQueryStringValue("DateFrom"));
            //oBackOrderBE.SelectedDateTo = Common.GetMM_DD_YYYY(GetQueryStringValue("DateTo"));

            oBackOrderBE.SelectedDateFrom = Convert.ToDateTime(GetQueryStringValue("DateFrom"));
            oBackOrderBE.SelectedDateTo = Convert.ToDateTime(GetQueryStringValue("DateTo"));

            hdnJSFromDt.Value = Convert.ToString(GetQueryStringValue("DateFrom"));
            hdnJSToDt.Value = Convert.ToString(GetQueryStringValue("DateTo"));

            if (ReviewBy.Equals("PlannerInitialReview"))
            {
                oBackOrderBE.ReviewBy = "PlannerInitialReview";                
            }
            else if (ReviewBy.Equals("PlannerSecondaryReview"))
            {
                oBackOrderBE.ReviewBy = "PlannerSecondaryReview";                
            }
            

            if (GetQueryStringValue("InventoryGroup") != null)
            {
                oBackOrderBE.StockPlannerGroupings = GetQueryStringValue("InventoryGroup").Replace("()", "&");
                oBackOrderBE.SummaryBy = action;                         
            }           

            DataSet dt = oBackOrderBAL.GetPlannerPenaltyReportBAL(oBackOrderBE);
            if (dt != null)
            {
                if (dt.Tables[0].Rows.Count > 0)
                {
                    grdBind.DataSource = dt.Tables[0];
                    grdBind.DataBind();
                    grdBindExport.DataSource = dt.Tables[0];
                    grdBindExport.DataBind();
                    lblError.Text = "";
                    lblError.Visible = false;
                    tblSearch.Visible = false;
                    btnExportToExcel.Visible = true;
                    oBackOrderBE = null;

                }
                else
                {
                    grdBind.DataSource = null;
                    grdBind.DataBind();
                    lblError.Visible = true;
                    lblError.Text = RecordNotFound;
                    tblSearch.Visible = false;
                    btnExportToExcel.Visible = false;

                }
            }
            else
            {
                grdBind.DataSource = null;
                grdBind.DataBind();
                lblError.Visible = true;
                lblError.Text = RecordNotFound;

            }
        }
    }


    private void SetSession(BackOrderBE oBackOrderBE)
    {
        Session["PlannerPenaltiesReport"] = null;

        TextBox txt = (TextBox)msStockPlanner.FindControl("txtUserName");
        HiddenField hdnSelectedStockPlonner = msStockPlanner.FindControl("hiddenSelectedIDs") as HiddenField;
        HiddenField hiddenSelectedName = msStockPlanner.FindControl("hiddenSelectedName") as HiddenField;
        TextBox txtVendor = (TextBox)msVendor.FindControl("ucVendor").FindControl("txtVendorNo");

        Hashtable htPlannerPenaltiesReport = new Hashtable();
        htPlannerPenaltiesReport.Add("txtVendor", txtVendor.Text);
        htPlannerPenaltiesReport.Add("SelectedVendorIDs", oBackOrderBE.SelectedVendorIDs);
        htPlannerPenaltiesReport.Add("IsSearchedByVendorNo", msVendor.IsSearchedByVendorNo);
        htPlannerPenaltiesReport.Add("SelectedSPIDs", hdnSelectedStockPlonner.Value);
        htPlannerPenaltiesReport.Add("SelectedSPNames", hiddenSelectedName.Value);
        htPlannerPenaltiesReport.Add("SelectedCountryIDs", oBackOrderBE.SelectedCountryIDs);
        htPlannerPenaltiesReport.Add("SelectedStockPlannerGroupID", oBackOrderBE.SelectedStockPlannerGroupID);
        htPlannerPenaltiesReport.Add("SelectedDateFrom", hdnJSFromDt.Value);
        htPlannerPenaltiesReport.Add("SelectedDateTo", hdnJSToDt.Value);
        htPlannerPenaltiesReport.Add("SelectedSPName", txt.Text);
        htPlannerPenaltiesReport.Add("SelectedCountryName", msCountry.SelectedCountryName);
        htPlannerPenaltiesReport.Add("ReviewBy", oBackOrderBE.ReviewBy);
        htPlannerPenaltiesReport.Add("SummaryBy", oBackOrderBE.SummaryBy);        
        Session["PlannerPenaltiesReport"] = htPlannerPenaltiesReport;
    }

    public void RetainSearchData()
    {

        Hashtable htPlannerPenaltiesReport = (Hashtable)Session["PlannerPenaltiesReport"];


        //********** Vendor ***************
        string txtVendor = (htPlannerPenaltiesReport.ContainsKey("txtVendor") && htPlannerPenaltiesReport["txtVendor"] != null) ? htPlannerPenaltiesReport["txtVendor"].ToString() : "";
        string VendorId = (htPlannerPenaltiesReport.ContainsKey("SelectedVendorIDs") && htPlannerPenaltiesReport["SelectedVendorIDs"] != null) ? htPlannerPenaltiesReport["SelectedVendorIDs"].ToString() : "";
        TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;

        bool IsSearchedByVendorNo = (htPlannerPenaltiesReport.ContainsKey("IsSearchedByVendorNo") && htPlannerPenaltiesReport["IsSearchedByVendorNo"] != null) ? Convert.ToBoolean(htPlannerPenaltiesReport["IsSearchedByVendorNo"]) : false;
        string txtVendorIdText = txtVendorId.Text;
        ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
        string strVendorId = string.Empty;
        int value;
        lstLeftVendor.Items.Clear();
        if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(VendorId) || !string.IsNullOrEmpty(VendorId)))
        {
            lstLeftVendor.Items.Clear();
            //lstRightVendor.Items.Clear();

            if (IsSearchedByVendorNo == true)
            {
                msVendor.SearchVendorNumberClick(txtVendor);
                //parsing successful 
            }
            else
            {
                msVendor.SearchVendorClick(txtVendor);
                //parsing failed. 
            }

            if (!string.IsNullOrEmpty(VendorId))
            {
                string[] strIncludeVendorIDs = VendorId.Split(',');
                for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                    if (listItem != null)
                    {

                        strVendorId += strIncludeVendorIDs[index] + ",";
                        lstRightVendor.Items.Add(listItem);
                        lstLeftVendor.Items.Remove(listItem);
                    }
                }
            }

            HiddenField hdnSelectedVendor = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedVendor.Value = strVendorId;
        }

        //*********** StockPlanner ***************
        string StockPlannerText = (htPlannerPenaltiesReport.ContainsKey("SelectedSPName") && htPlannerPenaltiesReport["SelectedSPName"] != null) ? htPlannerPenaltiesReport["SelectedSPName"].ToString() : "";
        string StockPlannerID = (htPlannerPenaltiesReport.ContainsKey("SelectedSPIDs") && htPlannerPenaltiesReport["SelectedSPIDs"] != null) ? htPlannerPenaltiesReport["SelectedSPIDs"].ToString() : "";
        string StockPlannerName = (htPlannerPenaltiesReport.ContainsKey("SelectedSPNames") && htPlannerPenaltiesReport["SelectedSPNames"] != null) ? htPlannerPenaltiesReport["SelectedSPNames"].ToString() : "";
        ListBox lstRight = msStockPlanner.FindControl("ucLBRight") as ListBox;
        ListBox lstLeft = msStockPlanner.FindControl("ucLBLeft") as ListBox;
        string strDtockPlonnerId = string.Empty;
        lstRight.Items.Clear();


        if (lstLeft != null && lstRight != null && !string.IsNullOrEmpty(StockPlannerID))
            lstRight.Items.Clear();
        msStockPlanner.SearchStockPlannerClick(StockPlannerText);
        {
            string[] strStockPlonnerIDs = StockPlannerID.Split(',');
            for (int index = 0; index < strStockPlonnerIDs.Length; index++)
            {
                ListItem listItem = lstLeft.Items.FindByValue(strStockPlonnerIDs[index]);
                if (listItem != null)
                {
                    strDtockPlonnerId += strStockPlonnerIDs[index] + ",";
                    lstRight.Items.Add(listItem);
                    lstLeft.Items.Remove(listItem);
                }

            }
            HiddenField hdnSelectedStockPlonner = msStockPlanner.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedStockPlonner.Value = strDtockPlonnerId;
        }

        //*********** Country ***************
        string CountryIDs = (htPlannerPenaltiesReport.ContainsKey("SelectedCountryIDs") && htPlannerPenaltiesReport["SelectedCountryIDs"] != null) ? htPlannerPenaltiesReport["SelectedCountryIDs"].ToString() : "";
        ListBox lstRightCountry = msCountry.FindControl("lstRight") as ListBox;
        ListBox lstLeftCountry = msCountry.FindControl("lstLeft") as ListBox;
        string msCountryid = string.Empty;
        lstRightCountry.Items.Clear();
        //lstRightSite.Items.Clear();
        msCountry.SelectedCountryIDs = "";
        msCountry.BindCountry();
        if (lstLeftCountry != null && lstRightCountry != null && !string.IsNullOrEmpty(CountryIDs))
        {
            //lstLeftSite.Items.Clear();
            lstRightCountry.Items.Clear();
            // msCountry.BindCountry();

            string[] strCountryIDs = CountryIDs.Split(',');
            for (int index = 0; index < strCountryIDs.Length; index++)
            {
                ListItem listItem = lstLeftCountry.Items.FindByValue(strCountryIDs[index]);
                if (listItem != null)
                {
                    msCountryid = msCountryid + strCountryIDs[index].ToString() + ",";
                    lstRightCountry.Items.Add(listItem);
                    lstLeftCountry.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
                msCountry.SelectedCountryIDs = msCountryid.Trim(',');
        }

        string[] arrMonthFrom = (htPlannerPenaltiesReport.ContainsKey("SelectedDateFrom") && htPlannerPenaltiesReport["SelectedDateFrom"] != null) ? htPlannerPenaltiesReport["SelectedDateFrom"].ToString().Split('/') : null;

        if (arrMonthFrom != null)
        {
            if (arrMonthFrom[0].Length != 1)
            {
                drpMonthFrom.SelectedValue = arrMonthFrom[0].ToString();
            }
            else
            {
                drpMonthFrom.SelectedValue = "0" + arrMonthFrom[0].ToString();
            }

            drpYearFrom.SelectedValue = arrMonthFrom[2].ToString().Split(' ').ElementAt(0);
        }


        string[] arrMonthTo = (htPlannerPenaltiesReport.ContainsKey("SelectedDateTo") && htPlannerPenaltiesReport["SelectedDateTo"] != null) ? htPlannerPenaltiesReport["SelectedDateTo"].ToString().Split('/') : null;

        if (arrMonthTo != null)
        {
            if (arrMonthTo[0].Length != 1)
            {
                drpMonthTo.SelectedValue = arrMonthTo[0].ToString();
            }
            else
            {
                drpMonthTo.SelectedValue = "0" + arrMonthTo[0].ToString();
            }
            drpYearTo.SelectedValue = arrMonthTo[2].ToString().Split(' ').ElementAt(0);
        }

        //drpMonthFrom.Text = (htPlannerPenaltiesReport.ContainsKey("SelectedDateFrom") && htPlannerPenaltiesReport["SelectedDateFrom"] != null) ? htPlannerPenaltiesReport["SelectedDateFrom"].ToString() : null;
        //drpMonthFrom.Text = (htPlannerPenaltiesReport.ContainsKey("SelectedFrom") && htPlannerPenaltiesReport["SelectedFrom"] != null) ? htPlannerPenaltiesReport["SelectedFrom"].ToString() : null;
        //txtToDate.Text = (htPlannerPenaltiesReport.ContainsKey("SelectedDateTo") && htPlannerPenaltiesReport["SelectedDateTo"] != null) ? htPlannerPenaltiesReport["SelectedDateTo"].ToString() : null;

        //hdnJSFromDt.Value = txtFromDate.Text;
        //hdnJSToDt.Value = txtToDate.Text;


        string strReviewBy =  (htPlannerPenaltiesReport.ContainsKey("ReviewBy") && htPlannerPenaltiesReport["ReviewBy"] != null) ? htPlannerPenaltiesReport["ReviewBy"].ToString() : null;

        if (strReviewBy.Equals("PlannerInitialReview"))
        {
            rdoInitialReview.Checked = true;           
        }
        else if (strReviewBy.Equals("PlannerSecondaryReview"))
        {
            rdoSecondaryReview.Checked = true;            
        }
        lblPenaltiesPerPlannerReport.Visible = false;
        lblPlannerPenaltiesReport.Visible = true;

        Session["PlannerPenaltiesReport"] = null;
    }

    private DataTable GetLastYears()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Year");
        int iStartYear = DateTime.Now.Year;
        for (int i = iStartYear; i >= iStartYear - 2; i--)
        {
            if (i >= 2014)
            {
                DataRow dr = dt.NewRow();
                dr["Year"] = i;
                dt.Rows.Add(dr);
            }
        }
        return dt;
    }

}
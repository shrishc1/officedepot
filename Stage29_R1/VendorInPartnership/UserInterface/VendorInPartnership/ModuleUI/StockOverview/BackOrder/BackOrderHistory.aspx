﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="BackOrderHistory.aspx.cs" Inherits="BackOrderHistory" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function CloseSelfWindow() {
            window.open('', '_self', ''); 
            window.close();
        }
    </script>
   <%-- <div class="button-row">
        <cc1:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>--%>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblBackOrderHistory" Text="Back Order History" runat="server"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div class="formbox">
                    <div class="button-row">
                        <cc1:ucButton ID="btnBacktoOutput" runat="server" Text="Back to Output" CssClass="button"
                            OnClientClick="CloseSelfWindow();" />
                    </div>
                    <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table">
                        <tr>
                            <td>
                                <%--<cc1:ucPanel ID="pnlBackorderHistory" runat="server" CssClass="fieldset-form">--%>
                                    <cc1:ucGridView ID="gvBackorderHistory" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass "
                                        CellPadding="0" GridLines="Both" Width="100%" AllowPaging="true" PageSize="50"
                                        OnPageIndexChanging="gvBackorderHistory_PageIndexChanging">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">
                                                <cc1:ucLabel ID="lblRecordNotFound" runat="server" Text="No record found"></cc1:ucLabel>
                                            </div>
                                        </EmptyDataTemplate>
                                        <Columns>                                          
                                            <asp:TemplateField HeaderText="Date" SortExpression="BackOrder.BOHDate">
                                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemTemplate>                                                
                                                    <cc1:ucLabel runat="server" ID="lblBOHDateValue" Text='<%#Eval("BackOrder.BOHDate", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CountofBackOrder" SortExpression="BackOrder.CountofBackOrder">
                                                <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblCountofBackOrderValue" Text='<%#Eval("BackOrder.CountofBackOrder") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="200px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="QtyonBackOrder" SortExpression="BackOrder.QtyonBackOrder">
                                                <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lblQtyonBackOrderValue" Text='<%#Eval("BackOrder.QtyonBackOrder") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="200px" />
                                            </asp:TemplateField>                                            
                                        </Columns>
                                    </cc1:ucGridView>                                    
                                <%--</cc1:ucPanel>--%>
                            </td>
                        </tr>                        
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

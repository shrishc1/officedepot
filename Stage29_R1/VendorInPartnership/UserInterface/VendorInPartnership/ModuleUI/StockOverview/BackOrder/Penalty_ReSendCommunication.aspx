﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Penalty_ReSendCommunication.aspx.cs"
    Inherits="Penalty_ReSendCommunication" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../../../Css/style.css" />
    <script type="text/javascript">
        function PrintScreen() {
            document.getElementById('rwButtons').style.visibility = "hidden";
            window.print();
            document.getElementById('rwButtons').style.visibility = "visible";
            //setTimeout('self.close()', 1000);
        } 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 100%; padding-left: 10px">
        <table>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr id="rwButtons">
                <td style="text-align: left; font-weight: bold;">
                    <cc1:ucLabel Text="To : " runat="server" ID="lblEmail"></cc1:ucLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                    <cc1:ucDropdownList ID="ddlSentEmailIds" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSentEmailIds_SelectedIndexChanged">
                    </cc1:ucDropdownList>
                    <cc1:ucTextbox ID="txtEmail" runat="server" Width="200px"></cc1:ucTextbox>&nbsp;&nbsp;&nbsp;
                    <asp:RegularExpressionValidator ID="revValidEmail" runat="server" Display="None"
                        ValidationGroup="ReSendCommunication" ControlToValidate="txtEmail" ErrorMessage="Please enter valid email address"
                        ValidationExpression="((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([,])*)*"></asp:RegularExpressionValidator>
                    <cc1:ucButton ID="btnReSendCommunication" runat="server" Text="ReSend" class="button"
                        OnClick="btnReSendCommunication_Click" ValidationGroup="ReSendCommunication" />
                    &nbsp;&nbsp;
                    <cc1:ucButton ID="btnPrintLetter" runat="server" CssClass="button" Text="Print Letter"
                        OnClientClick="PrintScreen();" />
                </td>
            </tr>
            <tr>
                <td style="text-align: left; font-weight: bold;">
                    <cc1:ucLabel Text="Language : " runat="server" ID="UcLabel1"></cc1:ucLabel>
                    <asp:DropDownList ID="drpLanguageId" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpLanguageId_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail"
                        Display="None" ValidationGroup="ReSendCommunication" SetFocusOnError="true" ErrorMessage="Please enter email address">
                    </asp:RequiredFieldValidator>
                    <asp:HiddenField ID="hdSubject" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                        Style="color: Red" ValidationGroup="ReSendCommunication" />
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divReSendCommunication" runat="server">
                    </div>
                </td>
            </tr>
        </table>
        <div id="innerData" style="display: none;">
            <cc1:ucLabel ID="ltlable" runat="server"></cc1:ucLabel>
        </div>
    </div>
    </form>
</body>
</html>

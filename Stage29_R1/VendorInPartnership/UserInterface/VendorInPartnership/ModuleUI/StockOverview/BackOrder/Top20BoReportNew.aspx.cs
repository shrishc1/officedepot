﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Drawing;
using Utilities;
using WebUtilities;


public partial class ModuleUI_StockOverview_BackOrder_Top20BoReportNew : CommonPage
{
    BackOrderBE oBackOrderBE = new BackOrderBE();
    BackOrderBAL oBackOrderBAL = new BackOrderBAL();
    protected string ErrorMessage = WebCommon.getGlobalResourceValue("ErrorMesg");
    protected string SuccessMesg = WebCommon.getGlobalResourceValue("SuccessMesg");
    protected string ErrorMessageSP = WebCommon.getGlobalResourceValue("ErrorMesgSP");
    protected string ErrorMesgTop20 = WebCommon.getGlobalResourceValue("ErrorMesgTop20");
    protected void Page_Init(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;
        ucCountry.CurrentPage = this;
        ucCountry.IsAllRequired = true;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        btnExportToExcel.CurrentPage = this;
        btnExportToExcel.GridViewControl = ucGridView1;
        btnExportToExcel.FileName = "Top 20 Backorder Report";
        if (!IsPostBack)
        {
            bindPlanner();
        }
    }
    protected void ucGridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TextBox txtDate = (TextBox)e.Row.FindControl("txtCommentdate");
            HiddenField hdnSkuid = (HiddenField)e.Row.FindControl("hdnSkuid");
            if (!string.IsNullOrEmpty(txtDate.Text))
            {
                if ((Convert.ToDateTime(Common.GetMM_DD_YYYY(txtDate.Text)) - DateTime.Now).TotalDays <= 0)
                {
                    txtDate.ForeColor = Color.Red;
                }
                else
                {
                    txtDate.ForeColor = Color.Black;
                }
            }
        }
    }

    private string GetViewValue()
    {
        string checkedValue = string.Empty;
        if (rdoallonly.Checked == true)
        {
            checkedValue = "A";
        }
        else if (rdoOpenOverdue.Checked == true)
        {
            checkedValue = "O";
        }
        return checkedValue;
    }

    private string GetOredrByValue()
    {
        string checkedValue = string.Empty;
        if (rdoValue.Checked == true)
        {
            checkedValue = "V";
        }
        else if (rdoCount.Checked == true)
        {
            checkedValue = "C";
        }
        return checkedValue;
    }


    private void bindPlanner()
    {
        SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
        SCT_UserBAL oSCT_UserBAL = new SCT_UserBAL();
        List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
        oNewSCT_UserBE.Action = "GetStockPalnnerByUserId";
        oNewSCT_UserBE.UserID = Convert.ToInt32(Session["UserID"]);
        lstUser = oSCT_UserBAL.GetPlannerBAL(oNewSCT_UserBE);
        if (lstUser.Count > 0 && lstUser != null)
        {
            FillControls.FillDropDown(ref ddlStockPalnner, lstUser, "FullName", "UserID", "All");
        }


    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BackOrderBE oBackOrderBE = new BackOrderBE();
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();

        oBackOrderBE.Action = "PlannerView";
        if (!string.IsNullOrEmpty(ddlStockPalnner.SelectedValue))
        {
            oBackOrderBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
            oBackOrderBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
            oBackOrderBE.StockPlannerID = Convert.ToInt32(ddlStockPalnner.SelectedValue);
            oBackOrderBE.OpenOverdue = GetViewValue();
            oBackOrderBE.OrderBy = GetOredrByValue();
            List<BackOrderBE> lstBackOrders = oBackOrderBAL.GetBackOrderTop20BAL(oBackOrderBE);

            lstBackOrders.ToList().ForEach(x => x.SelectedDateFrom = DateTime.Now.Date);
            lstBackOrders.ToList().ForEach(x => x.SelectedDateTo = DateTime.Now.Date);
            if (lstBackOrders.Count > 0)
            {
                ucGridView1.DataSource = lstBackOrders;
                ucGridView1.DataBind();

            }
            else
            {
                ucGridView1.DataSource = null;
                ucGridView1.DataBind();
            }

        }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static int? AddComment(string CommentDate, string Comments, string SKUID, string VendorID, string SiteID, string VikingSKU, string ODCode)
    {

        ExpediteStockBAL oMAS_ExpStockBAL = new ExpediteStockBAL();
        //Save comment.
        ExpediteStockBE oNewExpediteStock = new ExpediteStockBE();
        oNewExpediteStock.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
        oNewExpediteStock.Action = "SaveComments";

        oNewExpediteStock.CommentsAdded_Date = DateTime.Now;
        //if (ddlReason.SelectedValue.ToString() != "")
        //{
        //    oNewExpediteStock.ReasonTypeID = Convert.ToInt32(ddlReason.SelectedValue);
        //}
        if (HttpContext.Current.Session["UserID"] != null)
        {
            oNewExpediteStock.PurchaseOrder.UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
        }
        oNewExpediteStock.PurchaseOrder.SKUID = Convert.ToInt32(SKUID);

        oNewExpediteStock.VendorID = Convert.ToInt32(VendorID);

        oNewExpediteStock.SiteID = Convert.ToInt32(SiteID);

        oNewExpediteStock.Viking_Code = VikingSKU;

        oNewExpediteStock.OD_Code = ODCode;

        oNewExpediteStock.CommentsValid_Date = !string.IsNullOrEmpty(CommentDate) ? Common.GetMM_DD_YYYY(CommentDate) : (DateTime?)null;

        int? Result = oMAS_ExpStockBAL.SaveExpediteCommentsBAL(oNewExpediteStock);
        return Result;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using BaseControlLibrary;
using System.Collections;
using Utilities;

public partial class VendorPenaltyMaintenance : CommonPage
{
    #region Declarations ...
    private const int gridPageSize = 50;
    bool IsExportClicked = false;
    #endregion

    #region Events

   
    protected void Page_InIt(object sender, EventArgs e) {

        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "VendorPenalty")
            {
                if (Session["VendorPenalty"] != null)
                {
                    GetSession();
                }
            }

            if (GetQueryStringValue("VendorID") != null && GetQueryStringValue("IsVendorSubscribeToPenalty") != null)
           {
               BindVendorPenalty();
           } 


        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      

        if (!IsPostBack) {

            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e) {

        BindVendorPenalty();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {

        IsExportClicked = true;
        BindVendorPenalty();
        LocalizeGridHeader(gvVendorPenaltyMaintenance);
        WebCommon.Export("VendorPenaltyListing", gvVendorPenaltyMaintenance);
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("VendorPenaltyMaintenance.aspx");
    }


    protected void gvVendorPenaltyMaintenance_PageIndexChanging(object sender, GridViewPageEventArgs e) 
    {

        if (ViewState["lstBackOrderBE"] != null)
        {
            gvVendorPenaltyMaintenance.DataSource = (List<BackOrderBE>)ViewState["lstBackOrderBE"];
            gvVendorPenaltyMaintenance.PageIndex = e.NewPageIndex;
            if (Session["VendorPenalty"] != null)
            {
                Hashtable htVendorPenalty = (Hashtable)Session["VendorPenalty"];
                htVendorPenalty.Remove("PageIndex");
                htVendorPenalty.Add("PageIndex", e.NewPageIndex);
                Session["VendorPenalty"] = htVendorPenalty;
            }
            gvVendorPenaltyMaintenance.DataBind();
        }

    }

    protected void gvVendorPenaltyMaintenance_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Header && e.Row.RowType != DataControlRowType.Footer)
        {

            //var BackRate = e.Row.FindControl("ltBackRate") as ucLabel;
            //if (BackRate != null && BackRate.Text != "")
            //{
            //    BackRate.Text = BackRate.Text + "%";
            //}

            var OTIFPenaltyRate = e.Row.FindControl("ltOTIFPenaltyRate") as ucLabel;
            if (OTIFPenaltyRate != null && OTIFPenaltyRate.Text != "")
            {
                OTIFPenaltyRate.Text = OTIFPenaltyRate.Text + "%";
            }

        }
    }

    #endregion

    #region Methods

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindVendorPenalty(currnetPageIndx);
    }

    private void BindVendorPenalty(int Page = 1)
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.Action = "GetVendorPenalty";
            oBackOrderBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
            oBackOrderBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
            oBackOrderBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
            oBackOrderBE.IsVendorSubscribeToPenalty = rdoSubscribe.Checked ? "Yes" : (rdoDoNotSubscribe.Checked ? "No" : "All");

            if (IsExportClicked)
            {
                oBackOrderBE.GridCurrentPageNo = 0;
                oBackOrderBE.GridPageSize = 0;
            }
            else
            {
                oBackOrderBE.GridCurrentPageNo = Page;
                oBackOrderBE.GridPageSize = gridPageSize;
            }

            this.SetSession(oBackOrderBE);

            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.Vendor.VendorID = 0;


            if (GetQueryStringValue("VendorID") != null && GetQueryStringValue("IsVendorSubscribeToPenalty") != null)
            {
                oBackOrderBE.Vendor.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
                oBackOrderBE.IsVendorSubscribeToPenalty = GetQueryStringValue("IsVendorSubscribeToPenalty");
            } 


            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetVendorPenaltyBAL(oBackOrderBE);

            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                btnExportToExcel.Visible = true;
                gvVendorPenaltyMaintenance.DataSource = null;
                gvVendorPenaltyMaintenance.DataSource = lstBackOrderBE;
                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                ViewState["lstBackOrderBE"] = lstBackOrderBE;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else
            {
                gvVendorPenaltyMaintenance.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
            }

            gvVendorPenaltyMaintenance.PageIndex = Page;
            pager1.CurrentIndex = Page;
            gvVendorPenaltyMaintenance.DataBind();
            oBackOrderBAL = null;

            pnlGrid.Visible = true;
            pnlSelection.Visible = false;
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }


    private void SetSession(BackOrderBE oBackOrderBE)
    {
        Session["VendorPenalty"] = null;
        Session.Remove("VendorPenalty");

        Hashtable htVendorPenalty = new Hashtable();
        htVendorPenalty.Add("SelectedCountryId", oBackOrderBE.SelectedCountryIDs);
        htVendorPenalty.Add("SelectedVendorIDs", oBackOrderBE.SelectedVendorIDs);
        htVendorPenalty.Add("SelectedSPIDs", oBackOrderBE.SelectedStockPlannerIDs);
        htVendorPenalty.Add("SelectedViewMode", oBackOrderBE.IsVendorSubscribeToPenalty);
        htVendorPenalty.Add("PageIndex", oBackOrderBE.GridCurrentPageNo);

        Session["VendorPenalty"] = htVendorPenalty;

    }


    private void GetSession()
    {

        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        BackOrderBE oBackOrderBE = new BackOrderBE();

        if (Session["VendorPenalty"] != null)
        {
            Hashtable htVendorPenalty = (Hashtable)Session["VendorPenalty"];

            oBackOrderBE.SelectedCountryIDs = (htVendorPenalty.ContainsKey("SelectedCountryId") && htVendorPenalty["SelectedCountryId"] != null) ? htVendorPenalty["SelectedCountryId"].ToString() : null;
            oBackOrderBE.SelectedVendorIDs = (htVendorPenalty.ContainsKey("SelectedVendorIDs") && htVendorPenalty["SelectedVendorIDs"] != null) ? htVendorPenalty["SelectedVendorIDs"].ToString() : null;
            oBackOrderBE.SelectedStockPlannerIDs = (htVendorPenalty.ContainsKey("SelectedSPIDs") && htVendorPenalty["SelectedSPIDs"] != null) ? htVendorPenalty["SelectedSPIDs"].ToString() : null;
            oBackOrderBE.IsVendorSubscribeToPenalty = Convert.ToString(htVendorPenalty["SelectedViewMode"]);
            oBackOrderBE.Action = "GetVendorPenalty";

            oBackOrderBE.GridCurrentPageNo = Convert.ToInt32(htVendorPenalty["PageIndex"].ToString());
            oBackOrderBE.GridPageSize = gridPageSize;

            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.Vendor.VendorID = 0;

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetVendorPenaltyBAL(oBackOrderBE);

            ViewState["lstBackOrderBE"] = lstBackOrderBE;
            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                btnExportToExcel.Visible = true;
                gvVendorPenaltyMaintenance.DataSource = lstBackOrderBE;
                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else
            {
                gvVendorPenaltyMaintenance.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
            }
            pager1.CurrentIndex = Convert.ToInt32(htVendorPenalty["PageIndex"].ToString()); ;
            gvVendorPenaltyMaintenance.DataBind();
            oBackOrderBAL = null;

            pnlGrid.Visible = true;
            pnlSelection.Visible = false;


        }

    
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        int pageindex = pager1.CurrentIndex;
        BindVendorPenalty(pageindex);
        return Utilities.GenericListHelper<BackOrderBE>.SortList((List<BackOrderBE>)ViewState["lstBackOrderBE"], e.SortExpression, e.SortDirection).ToArray();
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    #endregion

 
    

  

  

    

    
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="BOPenaltyTracker.aspx.cs" Inherits="BOPenaltyTracker"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucMultiSelectVendor.ascx" TagName="ucMultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>

    <%@ Register Src="~/CommonUI/UserControls/MultiSelectDeclineReasonCode.ascx" TagName="MultiSelectDeclineReasonCode"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .radio-fix tr td:nth-child(2)
        {
            width: 100px;
        }
        .radio-fix tr td:nth-child(3)
        {
            width: 200px;
        }
        .wordbreak
        {
            word-break: break-all;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblPenaltyTracker" runat="server"></cc1:ucLabel>
    </h2>
    <%--<asp:UpdatePanel ID="uplPenaltyTracker" runat="server">
        <ContentTemplate>--%>
            <div class="button-row">
                <cc1:ucExportToExcel ID="btnExportToExcel" runat="server" Visible="false" />
            </div>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucPanel ID="pnlSearchScreen" runat="server">
                        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td align="right">
                                    <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                                </td>
                            </tr>
                        </table>
                        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblCountry" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucMultiSelectVendor ID="msVendor" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                                </td>
                            </tr>

                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblReason" runat="server" Text="Reason"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:MultiSelectDeclineReasonCode runat="server" ID="msDeclineReasonCode" />
                                </td>
                            </tr>
                            <tr>
                               <%-- <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblYear" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                             
                                                <cc1:ucDropdownList ID="drpYearTo" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                              
                                </td>--%>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblPeriod" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <table width="40%" border="0" cellspacing="5" cellpadding="0" id="tblDate">
                                        <tr>
                                            <td>
                                                <cc1:ucDropdownList ID="drpMonthFrom" runat="server" Width="80px">
                                                    <%-- <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>--%>
                                                    <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                                    <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                                    <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                                    <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                                    <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                                    <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                                    <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                                    <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                                    <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpYearFrom" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td style="text-align: center">
                                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpMonthTo" runat="server" Width="80px">
                                                    <%--  <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>--%>
                                                    <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                                    <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                                    <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                                    <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                                    <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                                    <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                                    <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                                    <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                                    <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpYearTo" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>                                
                            <tr>
                                <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblStatus" runat="server"></cc1:ucLabel>                                    
                                </td>
                                <td style="font-weight: bold;">
                                </td>
                                <td align="left" class="nobold radiobuttonlist" valign="top">
                                    <cc1:ucRadioButtonList ID="rblSubscribe" CssClass="radio-fix" runat="server" RepeatColumns="3"
                                        RepeatDirection="Horizontal">
                                    </cc1:ucRadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="3">
                                    <div class="button-row">
                                        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <cc1:ucPanel ID="pnlPotentialOutput" runat="server">
                        <div style="width: 100%; overflow: auto" id="divPrint" class="fixedTable">
                            <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table">
                                <tr>
                                    <td style="font-weight: bold;">
                                        <div style="width: 950px; overflow-x: auto;">
                                            <cc1:ucGridView ID="gvBOPenaltyTracker" runat="server" CssClass="grid gvclass" GridLines="Both"
                                                AllowPaging="true" PageSize="50" OnPageIndexChanging="gvBOPenaltyTracker_PageIndexChanging"
                                                OnSorting="SortGrid" AllowSorting="true" Width="100%">
                                                 <EmptyDataTemplate>
                                                    <div style="text-align: center">
                                                        <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>
                                                    </div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Country" SortExpression="Country">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltCountryValue" Text='<%#Eval("Country") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VendorNo" SortExpression="Vendor.Vendor_No">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltVendorValue" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Subscribe" SortExpression="Subscribe">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltSuscribeValue" Text='<%#Eval("Subscribe") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="StockPlNoSDR" SortExpression="Vendor.StockPlannerNumber">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="StockPlNameSDR" SortExpression="Vendor.StockPlannerName">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="YTDTotalNoofBackOrder" SortExpression="YTDTotalNoOfBO">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltTotalCountValue" Text='<%#Eval("YTDTotalNoOfBO") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="YTDTotalNumberofVendorBackOrders" SortExpression="YTDTotalNoOfVendorBO">
                                                        <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltChargesValue" Text='<%#Eval("YTDTotalNoOfVendorBO") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" Width="120px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="YTDPenaltyCharges" SortExpression="YTDPenaltyCharges">
                                                        <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltChargesValue" Text='<%#Eval("YTDPenaltyCharges") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" Width="120px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </cc1:ucGridView>
                                            <cc1:ucGridView ID="gvBOPenaltyTrackerExcel" runat="server" CssClass="grid gvclass"
                                                GridLines="Both" Visible="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Country" SortExpression="Country">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltCountry" Text='<%#Eval("Country") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VendorNo" SortExpression="Vendor.Vendor_No">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Subscribe" SortExpression="Subscribe">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltSubscribeValue" Text='<%#Eval("Subscribe") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="StockPlNoSDR" SortExpression="Vendor.StockPlannerNumber">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="StockPlNameSDR" SortExpression="Vendor.StockPlannerName">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="YTDTotalNoofBackOrder" SortExpression="YTDTotalNoOfBO">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltTotalCount" Text='<%#Eval("YTDTotalNoOfBO") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="YTDTotalNumberofVendorBackOrders" SortExpression="YTDTotalNoOfVendorBO">
                                                        <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltCharges" Text='<%#Eval("YTDTotalNoOfVendorBO") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="120px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="YTDPenaltyCharges" SortExpression="YTDPenaltyCharges">
                                                        <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltCharges" Text='<%#Eval("YTDPenaltyCharges") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="120px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </cc1:ucGridView>
                                            <%--<cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                                    </cc1:PagerV2_8>--%>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <div class="button-row">
                                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </cc1:ucPanel>
                </div>
            </div>
        <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

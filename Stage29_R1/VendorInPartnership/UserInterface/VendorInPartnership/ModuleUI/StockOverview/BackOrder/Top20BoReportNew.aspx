﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Top20BoReportNew.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="ModuleUI_StockOverview_BackOrder_Top20BoReportNew" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/ModuleUI/Discrepancy/UserControl/ucSDRCommunicationAPListing.ascx"
    TagName="ucSDRCommunication" TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAPSearch.ascx" TagName="MultiSelectAPSearch"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucMultiSelectVendor.ascx" TagName="ucMultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>

<%@ Register Src="../../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
        function EndRequest(sender, args) {
            if (args.get_error() == undefined) {
               
                var iRowIndex = 0;
                $('#<%= ucGridView1.ClientID%>').find('input:submit[id$="btnUpdate"]').bind('click', function (e) {
                   // debugger;
                    iRowIndex = parseInt($(this).closest("tr").prevAll("tr").length) + 2;
                    if (parseInt(iRowIndex) < 10) {
                        iRowIndex = "0" + iRowIndex;
                    }
                    var CommentDate = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_txtCommentdate").val();
                    var Comments = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_txtComment").val();
                    if ((CommentDate == null || CommentDate == undefined || CommentDate == "") || (Comments == null || Comments == undefined || Comments == "")) {
                        alert('<%= ErrorMesgTop20%>');
                        return false;
                    }

                    var SKUID = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_hdnSkuid").val();
                    var VendorID = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_hdnVendorid").val();
                    var SiteID = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_hdnSIteid").val();
                    var VikingSKU = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_lblVikingCode").text();
                    var ODCode = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_lblSKU").text();
                    // alert(Comments);
                    $.ajax({
                        type: "POST",
                        url: "Top20BOReport.aspx/AddComment",
                        data: '{CommentDate:"' + CommentDate + '",Comments:"' + Comments + '",SKUID:"' + SKUID + '",VendorID:"' + VendorID + '",SiteID:"' + SiteID + '",VikingSKU:"' + VikingSKU + '",ODCode:"' + ODCode + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: OnSuccess,
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                });

                function OnSuccess(response, userContext, methodName) {
                  //  debugger;
                    if (response.d == 1) {
                        alert('<%= SuccessMesg%>');
                    }
                    else {
                        alert('<%= ErrorMessage%>');
                        $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_txtCommentdate").val(" ");
                        $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_txtComment").val(" ");

                    }
                }
                //              $(window).unload(function () {
                //                  alert("manish");
                //                  $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_txtCommentdate").val(" ");
                //                  $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_txtComment").val(" ");
                //              });
                $('#<%= ucGridView1.ClientID%>').find('input:submit[id$="btnUpdate"]').addClass("button");
                $('input:submit[id$="btnSearch"]').addClass("button");

            }
        }
        $(function () {
            var fullDate = new Date();
            var numberOfDaysToAdd = 21;
            fullDate.setDate(fullDate.getDate() + numberOfDaysToAdd);
            var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);

            var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
            $('.Calender').datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: new Date(2013, 0, 1),
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy', maxDate: currentDate
            });


            var iRowIndex = 0;
            $('#<%= ucGridView1.ClientID%>').find('input:submit[id$="btnUpdate"]').bind('click', function (e) {
               // debugger;
              //  var id = this.id;
                var length = "ctl00_ContentPlaceHolder1_ucGridView1_ctl".length;

                var res = this.id.substring(length, length + 5);

                iRowIndex = res.split("_")[0];
               
              //  iRowIndex = parseInt($(this).closest("tr").prevAll("tr").length) + 2;
//                if (parseInt(iRowIndex) < 10) {
//                    iRowIndex = "0" + iRowIndex;
//                }
                var CommentDate = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_txtCommentdate").val();
                var Comments = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_txtComment").val();
                if ((CommentDate == null || CommentDate == undefined || CommentDate == "") || (Comments == null || Comments == undefined || Comments == "")) {
                    alert('<%= ErrorMesgTop20%>');
                    return false;
                }

                var SKUID = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_hdnSkuid").val();
                var VendorID = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_hdnVendorid").val();
                var SiteID = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_hdnSIteid").val();
                var VikingSKU = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_lblVikingCode").text();
                var ODCode = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_lblSKU").text();
                // alert(Comments);
                $.ajax({
                    type: "POST",
                    url: "Top20BOReport.aspx/AddComment",
                    data: '{CommentDate:"' + CommentDate + '",Comments:"' + Comments + '",SKUID:"' + SKUID + '",VendorID:"' + VendorID + '",SiteID:"' + SiteID + '",VikingSKU:"' + VikingSKU + '",ODCode:"' + ODCode + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnSuccess,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            });

            function OnSuccess(response, userContext, methodName) {
               // debugger;
                if (response.d == 1) {
                    alert('<%= SuccessMesg%>');
                }
                else {
                    alert('<%= ErrorMessage%>');
                    $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_txtCommentdate").val(" ");
                    $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_txtComment").val(" ");

                }
            }
            //              $(window).unload(function () {
            //                  alert("manish");
            //                  $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_txtCommentdate").val(" ");
            //                  $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_txtComment").val(" ");
            //              });
            $('#<%= ucGridView1.ClientID%>').find('input:submit[id$="btnUpdate"]').addClass("button");
            $('input:submit[id$="btnSearch"]').addClass("button");


            $('#<%= btnSearch.ClientID%>').click(function () {
                if ($('#<%= ddlStockPalnner.ClientID%>').val() == 0) {
                    alert('<%= ErrorMessageSP%>'); return false;
                }
                else {
                    return true;
                }
            });



        });
    </script>
 <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
   <%-- <asp:UpdatePanel ID="updAPView" runat="server">
        <ContentTemplate>--%>
            <h2>
                <cc1:ucLabel ID="lblStockPlannerView" runat="server"></cc1:ucLabel>
            </h2>

              <div class="right-shadow">
              
              <div class="formbox">
              <div id="divSearchButtons" runat="server">
                        <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold; width: 16%">
                                    <cc1:ucLabel ID="lblStockPlanner" ClientIDMode="Static" runat="server" ></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    :
                                </td>
                                <td style="font-weight: bold; " class="style2">
                                     <cc1:ucDropdownList ID="ddlStockPalnner" runat="server" Width="150px">
                                    </cc1:ucDropdownList>
                                </td>

                                <td style="font-weight: bold; width: 3%">
                                </td>

                                <td style="font-weight: bold; width: 10%">
                                    <cc1:ucLabel ID="lblCountry" ClientIDMode="Static" runat="server" ></cc1:ucLabel>
                                </td>

                                <td style="font-weight: bold;  width:1%">
                                    :
                                </td>
                                <td style="font-weight: bold; " class="style1">
                                   
                                    <label id="UserName" runat="server" />
                                    <uc1:ucCountry ID="ucCountry" runat="server" />
                                </td>
                                <td style="font-weight: bold; width: 3%">
                                </td>

                                 <td style="font-weight: bold; width: 10%">
                                    <cc1:ucLabel ID="lblSitePrefixName" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    :
                                </td>
                                <td style="width: 16%">
                                    <cc2:ucSite ID="ddlSite" runat="server" />
                                </td>
                            </tr>
                            
                            <tr >
                                <td colspan="11" style="height:20px">
                            
                                </td>
                            </tr>

                            <tr>
                            <td style="font-weight: bold; width: 16%">
                                    <cc1:ucLabel ID="lblView" ClientIDMode="Static" runat="server" ></cc1:ucLabel>
                            </td>
                               
                            <td style="font-weight: bold; width: 1%">
                                    :
                            </td>
                               
                            <td style="font-weight: bold; " class="style2">
                                 <cc1:ucRadioButton ID="rdoallonly" runat="server" Checked="true" GroupName="View"/>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 <cc1:ucRadioButton ID="rdoOpenOverdue" runat="server" GroupName="View"/>
                            </td>
                            
                            <td style="font-weight: bold; width: 2%">
                            </td>
                            
                            <td style="font-weight: bold; width: 10%">
                                    <cc1:ucLabel ID="lblorderby" ClientIDMode="Static" runat="server" ></cc1:ucLabel>
                            </td>
                            
                            <td style="font-weight: bold; width: 1%">
                                    :
                            </td>
                               
                            <td style="font-weight: bold; " class="style1">
                                 <cc1:ucRadioButton ID="rdoValue" runat="server" Checked="true" GroupName="Order"/>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 <cc1:ucRadioButton ID="rdoCount" runat="server" GroupName="Order"/>
                            </td>
                             
                             <td colspan="3">
                                    
                            </td>
                            
                             <td style="font-weight: bold; width: 10%">
                              <cc1:ucButton ID="btnSearch" runat="server" onclick="btnSearch_Click" />                                 
                             </td>
                               
                            </tr>
                           
                   </table>
                    </div>
                     <div class="button-row">
                         <cc1:ucExportToExcel ID="btnExportToExcel" runat="server" Visible="true" />
                     </div>
             
                        <table cellspacing="1" cellpadding="0" class="form-table">
                            <tr>
                                <td>
                                    <cc1:ucGridView ID="ucGridView1" runat="server" AutoGenerateColumns="false"  CssClass="grid gvclass"
                                        CellPadding="0" Width="100%"  onrowdatabound="ucGridView1_RowDataBound" ViewStateMode="Enabled">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                      
                                      <asp:TemplateField HeaderText="Site" >
                                           <HeaderStyle Width="120px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                <asp:HiddenField ID="hdnSkuid" runat="server" Value='<%# Eval("SKUID") %>'/>
                                                <asp:HiddenField ID="hdnVendorid" runat="server" Value='<%# Eval("VendorID") %>'/>
                                                <asp:HiddenField ID="hdnSIteid" runat="server" Value='<%# Eval("SiteID") %>'/>
                                                <cc1:ucLabel ID="lblSite" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLabel>                                          
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                          
                                       <asp:TemplateField HeaderText="Vendor" >
                                           <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="left" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblVendor" runat="server" Text='<%# Eval("VendorNo") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                                
                                       <asp:TemplateField HeaderText="SKU" >
                                           <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>         
                                                     <asp:HyperLink ID="lblSKU" runat="server" Text='<%# Eval("SKU_No") %>' 
                                            NavigateUrl='<%# EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/SKUDetailedReview.aspx?&ODSKUNO="+Eval("SKU_No")+"&Status="+
                                            "Pending"+"&VikingCode="+Eval("Viking")+"&Site="+Eval("SiteName")+"&Dateto="+ Eval("SelectedDateTo") +"&DateFrom="+Eval("SelectedDateFrom"))%>'></asp:HyperLink>                                        
                                                <%--    <cc1:ucLabel ID="lblSKU" runat="server" Text='<%# Eval("SKU_No") %>' ></cc1:ucLabel>--%>
                                                </ItemTemplate>
                                       </asp:TemplateField> 

                                       <asp:TemplateField HeaderText="Viking Code" >
                                           <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblVikingCode" runat="server" Text='<%# Eval("Viking") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                        

                                       <asp:TemplateField HeaderText="Description" >
                                           <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="left" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblDescription" runat="server" Text='<%# Eval("Description") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                            
                                       <asp:TemplateField HeaderText="Qty" >
                                           <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblQuantity" runat="server" Text='<%# Eval("Qty") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                      
                                       <asp:TemplateField HeaderText="Orders" >
                                           <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblOrders" runat="server" Text='<%# Eval("Orders") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                        

                                       <asp:TemplateField HeaderText="Value" >
                                           <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblValue" runat="server" Text='<%# Eval("Value") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                        

                                       <asp:TemplateField HeaderText="On BO Since" >
                                           <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblOnBOSince" runat="server" Text='<%# Eval("Bosince") != null ?  Convert.ToDateTime(Eval("Bosince").ToString()).ToString("dd/MM/yyyy") : ""%>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                       
                                           
                                           <asp:TemplateField HeaderText=" Date" SortExpression="Date">
                                                <HeaderStyle Width="80px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtCommentdate"  runat="server" Class="Calender" Width="70px"  Text='<%# Eval("CommentDate") != null ?  Convert.ToDateTime(Eval("CommentDate").ToString()).ToString("dd/MM/yyyy") : ""%>'/>
                                                 <%--<cc1:ucTextbox ID="txtFromDate"  runat="server" Class="Calender" Text='<%# Eval("CommentDate") != null ?  Convert.ToDateTime(Eval("CommentDate").ToString()).ToString("dd/MM/yyyy") : ""%>'
                                                 Width="70px" />--%>
                                                    <%--<cc1:ucLiteral ID="lblDate" runat="server" Text='<%# Eval("CommentDate") != null ?  Convert.ToDateTime(Eval("CommentDate").ToString()).ToString("dd/MM/yyyy") : ""%>'></cc1:ucLiteral>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Comments" >
                                                <HeaderStyle Width="350px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>    
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                            <asp:TextBox runat="server" ID="txtComment" EnableViewState="false" TextMode="MultiLine" Text='<%# Eval("Comments") %>'/>
                                                            </td>
                                                            <td><cc1:ucButton Text="Update" id="btnUpdate" runat="server" /></td>
                                                        </tr>
                                                    </table> 
                                                </ItemTemplate>
                                                </asp:TemplateField>  
                                           <%-- <asp:TemplateField HeaderText="" >
                                                <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            

                                         </Columns>
                                    </cc1:ucGridView>
                                </td>
                             </tr>
                        </table>
                   
              </div>
                  </label>
       <%--  </ContentTemplate>
       <Triggers>
        <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .style1
        {
            width: 19%;
        }
        .style2
        {
            width: 45%;
        }
    </style>
</asp:Content>


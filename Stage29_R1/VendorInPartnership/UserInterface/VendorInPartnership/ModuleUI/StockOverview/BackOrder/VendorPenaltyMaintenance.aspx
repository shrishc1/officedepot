﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="VendorPenaltyMaintenance.aspx.cs" Inherits="VendorPenaltyMaintenance"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucAddButton.ascx" TagName="ucAddButton"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .radio-fix tr td:nth-child(2)
        {
            width: 245px;
        }
        .radio-fix tr td:nth-child(3)
        {
            width: 170px;
        }
        .wordbreak
        {
            word-break: break-all;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblVendorPenaltyMaintenance" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSelection" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="width: 90%">
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendorPenalty" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" align="center">
                            :
                        </td>
                        <td>
                            <table width="60%" cellspacing="3" cellpadding="0" align="left">
                                <tr>
                                    <td style="width: 20%;" class="nobold">
                                        <cc1:ucRadioButton ID="rdoSubscribe" runat="server" GroupName="Penalty" />
                                    </td>
                                    <td style="font-weight: bold; width: 4em; text-align: center">
                                        &nbsp;&nbsp;
                                    </td>
                                    <td style="width: 30%;" class="nobold">
                                        <cc1:ucRadioButton ID="rdoDoNotSubscribe" runat="server" GroupName="Penalty"  Checked="true"/>
                                    </td>
                                    <td style="font-weight: bold; width: 4em; text-align: center">
                                        &nbsp;&nbsp;
                                    </td>
                                    <td style="width: 40%;" class="nobold">
                                        <cc1:ucRadioButton ID="rdoAllOnly" runat="server" GroupName="Penalty" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGrid" runat="server" Visible="false">
                <div class="button-row">
                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                        Width="109px" Height="20px" />
                </div>
               <%-- <div style="width: 100%; overflow: auto" id="divPrint" class="fixedTable">--%>
                    <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table" style="width: 100%;">
                     <%-- <table style="width: 100%;">--%>
                        <tr>
                            <td style="font-weight: bold;">
                               <%-- <div style="width: 950px; overflow-x: auto;">--%>
                                    <cc1:ucGridView ID="gvVendorPenaltyMaintenance" runat="server" CssClass="grid gvclass searchgrid-1" 
                                        GridLines="Both"  OnPageIndexChanging="gvVendorPenaltyMaintenance_PageIndexChanging" OnSorting="SortGrid"
                                        OnRowDataBound="gvVendorPenaltyMaintenance_RowDataBound" Width="960px">
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">
                                                <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                            </div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="Country" SortExpression="Vendor.VendorCountryName">
                                                <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltCountry" Text='<%#Eval("Vendor.VendorCountryName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="70px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                                                <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                                <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlVendorName" runat="server" Text='<%# Eval("Vendor.VendorName") %>'
                                                        NavigateUrl='<%# EncryptQuery("VendorPenaltyMaintenanceEdit.aspx?VendorID="+Eval("Vendor.VendorID")+"&PenaltyID="+Eval("PenaltyID")+"&PageFrom="+"VendorMaintainance" )%>'></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Suscribe Y or N " SortExpression="IsVendorSubscribeToPenalty">
                                                <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltSuscribe" Text='<%#Eval("IsVendorSubscribeToPenalty") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lines Rcvd" SortExpression="Lines">
                                                <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltLinesRcvd" Text='<%#Eval("Lines") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Weight" SortExpression="Weightage">
                                                <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltWeightage" Text='<%#Eval("Weightage") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Spend Amount" SortExpression="SpendAmount">
                                                <HeaderStyle Width="30px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltSpendAmount" Text='<%#Eval("SpendAmount") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="If Yes what penalty Type" SortExpression="PenaltyType">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltPenaltyType" Text='<%#Eval("PenaltyType") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="60px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="If OTIF then Penalty Rate" SortExpression="OTIFRate">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltOTIFPenaltyRate" Text='<%#Eval("OTIFRate") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Line Value Penalty" SortExpression="OTIFLineValuePenalty">
                                                <HeaderStyle Width="40px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltLineValuePenalty" Text='<%#Eval("OTIFLineValuePenalty") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="40px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="If BO then Penalty Rate" SortExpression="ValueOfBackOrder">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltBackRate" Text='<%#Eval("ValueOfBackOrder") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="60px" CssClass="wordbreak" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Currency" SortExpression="Currency.CurrencyName">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltCurrency" Text='<%#Eval("Currency.CurrencyName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="60px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Inclusion Applied By" SortExpression="PenaltyAppliedName">
                                                <HeaderStyle Width="90px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltInclusionUserName" Text='<%#Eval("PenaltyAppliedName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="90px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="Vendor.StockPlannerNumber">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="60px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockPlannerName" SortExpression="Vendor.StockPlannerName">
                                                <HeaderStyle Width="90px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="90px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Local Consolidation" SortExpression="LocalConslolidation">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltLocalConslolidation" Text='<%#Eval("LocalConslolidation") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="60px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="EU Consolidation" SortExpression="EUConsolidation">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltEUConsolidation" Text='<%#Eval("EUConsolidation") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="60px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </cc1:ucGridView>
                                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                                    </cc1:PagerV2_8>
                              <%--  </div>--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <div class="button-row">
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
               <%-- </div>--%>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

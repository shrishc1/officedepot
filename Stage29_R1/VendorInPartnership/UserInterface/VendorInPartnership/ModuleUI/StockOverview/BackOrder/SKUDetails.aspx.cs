﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;

public partial class ModuleUI_StockOverview_BackOrder_SKUDetails : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.GridViewControl = grdSKU_PODetails;
        ucExportToExcel1.FileName = "SKU_PO_Details";

        if (!IsPostBack)
        {
            if (GetQueryStringValue("Site") != null)
            {
                ltSite.Text = Convert.ToString(GetQueryStringValue("Site"));
            }
            if (GetQueryStringValue("VikingCode") != null)
            {
                ltVikingCode.Text = Convert.ToString(GetQueryStringValue("VikingCode"));

            }
            if (GetQueryStringValue("ODSKUNO") != null)
            {
                ltODCode.Text = Convert.ToString(GetQueryStringValue("ODSKUNO"));

            }
            if (GetQueryStringValue("Description") != null)
            {
                ltDescription.Text = Convert.ToString(GetQueryStringValue("Description"));
            }

            if (GetQueryStringValue("PreviousPage") != null)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
            }
            
           BindSKU_PODetails();
        }
    }

    public void BindSKU_PODetails()
    {
        BackOrderBE oBackOrderBE = new BackOrderBE();
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        oBackOrderBE.Action = "GetSKU_PODetails";
        if (GetQueryStringValue("VikingCode") != null)
        {
            oBackOrderBE.Viking = Convert.ToString(GetQueryStringValue("VikingCode"));
        }
        if (GetQueryStringValue("ODSKUNO") != null)
        {
            oBackOrderBE.SKU_No = Convert.ToString(GetQueryStringValue("ODSKUNO"));
        }
        if (GetQueryStringValue("SiteID") != null)
        {
            oBackOrderBE.SiteID = Convert.ToInt32(GetQueryStringValue("SiteID"));
        }

        List<BackOrderBE> lstBackOrders = oBackOrderBAL.GetSKU_PODetailsBAL(oBackOrderBE);

        if (lstBackOrders.Count > 0 && lstBackOrders != null)
        {
            grdSKU_PODetails.DataSource = lstBackOrders;
            grdSKU_PODetails.DataBind();
            ViewState["lstBackOrders"] = lstBackOrders;
            lblNoOpenPOFound.Visible = false;
            ucExportToExcel1.Visible = true;

        }
        else
        {
            grdSKU_PODetails.DataSource = null;
            grdSKU_PODetails.DataBind();
            lblNoOpenPOFound.Visible = true;
            ucExportToExcel1.Visible = false;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<BackOrderBE>.SortList((List<BackOrderBE>)ViewState["lstBackOrders"], e.SortExpression, e.SortDirection).ToArray();
    }
    
    
}
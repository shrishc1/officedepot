﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using BaseControlLibrary;
using System.Collections;
using System.Data;

public partial class BOPenaltyTracker : CommonPage
{
    string AllOnly = WebCommon.getGlobalResourceValue("AllOnly");
    string Subscribe = WebCommon.getGlobalResourceValue("Subscribe");
    string DoNotSubscribe = WebCommon.getGlobalResourceValue("DoNotSubscribe");

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pnlSearchScreen.Visible = true;
            pnlPotentialOutput.Visible = false;
            //btnExportToExcel.Visible = false;
            //pager1.PageSize = 200;
            //pager1.GenerateGoToSection = false;
            //pager1.GeneratePagerInfoSection = false;
            //FillControls.FillDropDown(ref drpYearTo, GetLastYears(), "Year", "Year");

            string PreviousMonth = DateTime.Now.AddMonths(-1).ToString("MM");
            drpMonthFrom.SelectedValue = PreviousMonth;

            string CurrentMonth = DateTime.Now.ToString("MM");
            drpMonthTo.SelectedValue = CurrentMonth;

            FillControls.FillDropDown(ref drpYearTo, GetLastYears(), "Year", "Year");
            FillControls.FillDropDown(ref drpYearFrom, GetLastYears(), "Year", "Year");

            this.BindSubscribeList();
        }

        msCountry.SetCountryOnPostBack();
        //msVendor.setVendorsOnPostBack();
        msStockPlanner.SetSPOnPostBack();
        msDeclineReasonCode.SetCodeOnPostBack();
        btnExportToExcel.CurrentPage = this;
        btnExportToExcel.FileName = "BOPenaltyTracker";
        btnExportToExcel.GridViewControl = gvBOPenaltyTrackerExcel;
    }

    private DataTable GetLastYears()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Year");
        int iStartYear = DateTime.Now.Year;
        for (int i = iStartYear; i >= iStartYear - 2; i--)
        {
            if (i >= 2014)
            {
                DataRow dr = dt.NewRow();
                dr["Year"] = i;
                dt.Rows.Add(dr);
            }
        }
        return dt;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.BindGrid();
    }    

    protected void gvBOPenaltyTracker_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstBOPenaltyTracker"] != null)
        {
            gvBOPenaltyTracker.PageIndex = e.NewPageIndex;
            gvBOPenaltyTracker.DataSource = (List<BackOrderBE>)ViewState["lstBOPenaltyTracker"];
            gvBOPenaltyTracker.DataBind();

            LocalizeGridHeader(gvBOPenaltyTracker);
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e) {       
     
       return Utilities.GenericListHelper<BackOrderBE>.SortList((List<BackOrderBE>)ViewState["lstBOPenaltyTracker"], e.SortExpression, e.SortDirection).ToArray();
      
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //this.ClearCriteria();
        pnlSearchScreen.Visible = true;
        pnlPotentialOutput.Visible = false;
        btnExportToExcel.Visible = false;
        ViewState["lstBOPenaltyTracker"] = null;
    }

    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstSelectedVendor");
        ListBox lstVendor = (ListBox)msVendor.FindControl("lstVendor");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);
            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
        }
    }

    //public void pager_Command(object sender, CommandEventArgs e)
    //{
    //    int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
    //    pager1.CurrentIndex = currnetPageIndx;
    //    BindVendorPanalty(currnetPageIndx);
    //}

    //----------------------------------

    private void BindSubscribeList()
    {
        ListItem listItem = new ListItem();
        listItem.Text = " "+AllOnly;
        listItem.Value = AllOnly;
        listItem.Selected = true;
        rblSubscribe.Items.Add(listItem);

        listItem = new ListItem();
        listItem.Text = Subscribe;
        listItem.Value = "Yes";
        rblSubscribe.Items.Add(listItem);

        listItem = new ListItem();
        listItem.Text = DoNotSubscribe;
        listItem.Value = "No";
        rblSubscribe.Items.Add(listItem);
    }

    private void BindGrid()
    {
        var backOrderBE = new BackOrderBE();
        backOrderBE.Action = "GetBOPenaltyTrackerData";
        if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs) && !string.IsNullOrWhiteSpace(msCountry.SelectedCountryIDs))
            backOrderBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;

        backOrderBE.SelectedVendorIDs = this.GetSelectedVendorIds(msVendor);

        if (!string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs) && !string.IsNullOrWhiteSpace(msStockPlanner.SelectedStockPlannerIDs))
            backOrderBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;

        if (!string.IsNullOrEmpty(msDeclineReasonCode.SelectedDeclineReasonCodeIDs) && !string.IsNullOrWhiteSpace(msDeclineReasonCode.SelectedDeclineReasonCodeIDs))
            backOrderBE.SelectedDeclineReasonCodeIDs = msDeclineReasonCode.SelectedDeclineReasonCodeIDs;


        if (rblSubscribe.SelectedValue.Equals("Yes") || rblSubscribe.SelectedValue.Equals("No"))
            backOrderBE.IsVendorSubscribeToPenalty = rblSubscribe.SelectedValue;

        //backOrderBE.SelectedDateTo = Convert.ToDateTime(drpYearTo.SelectedValue + "-" + "01" + "-" + "01");

        int numberOfDays = DateTime.DaysInMonth(Convert.ToInt16(drpYearTo.SelectedValue), Convert.ToInt16(drpMonthTo.SelectedValue)); // for numberofdays in month

        backOrderBE.SelectedDateFrom = Convert.ToDateTime(drpYearFrom.SelectedValue + "-" + drpMonthFrom.SelectedValue + "-" + "01");
        backOrderBE.SelectedDateTo = Convert.ToDateTime(drpYearTo.SelectedValue + "-" + drpMonthTo.SelectedValue + "-" + numberOfDays.ToString());

        var backOrderBAL = new BackOrderBAL();
        var lstBOPenaltyTracker = backOrderBAL.GetBOPenaltyTrackerDataBAL(backOrderBE);
        if (lstBOPenaltyTracker != null && lstBOPenaltyTracker.Count > 0)
        {
            gvBOPenaltyTracker.DataSource = lstBOPenaltyTracker;
            gvBOPenaltyTrackerExcel.DataSource = lstBOPenaltyTracker;
            ViewState["lstBOPenaltyTracker"] = lstBOPenaltyTracker;
            gvBOPenaltyTracker.PageIndex = 0;
            btnExportToExcel.Visible = true;            
        }
        else
        {
            gvBOPenaltyTrackerExcel.DataSource = null;
            gvBOPenaltyTracker.DataSource = null;
            btnExportToExcel.Visible = false;           
        }

        pnlSearchScreen.Visible = false;
        pnlPotentialOutput.Visible = true;
        gvBOPenaltyTracker.DataBind();
        gvBOPenaltyTrackerExcel.DataBind();

        LocalizeGridHeader(gvBOPenaltyTracker);
    }

    private string GetSelectedVendorIds(ucMultiSelectVendor multiSelectVendor)
    {
        ucListBox lstSelectedVendor = (ucListBox)multiSelectVendor.FindControl("lstSelectedVendor");
        var vendorIds = string.Empty;
        for (int index = 0; index < lstSelectedVendor.Items.Count; index++)
        {
            if (index.Equals(0))
                vendorIds = lstSelectedVendor.Items[index].Value;
            else
                vendorIds = string.Format("{0},{1}", vendorIds, lstSelectedVendor.Items[index].Value);
        }
        return vendorIds;
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="BackOrderReporting.aspx.cs" Inherits="BackOrderReporting"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectItemClassification.ascx" TagName="MultiSelectItemClassification"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlannerGrouping.ascx" TagName="MultiSelectStockPlannerGrouping"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
 <%@ Register Src="~/CommonUI/UserControls/ucVikingSku.ascx" TagName="ucVikingSku"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="~/Scripts/JScript.js" type="text/javascript"></script>
    <style type="text/css">
        .SubTotalRowStyle
        {
            background-color: #F7F6F3;
            font-weight: bolder;
            font-size: 10px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var rdoDailyBackOrder = "#" + '<%=rdoDailyBackOrder.ClientID %>';
            var rdoOpenBackOrder = "#" + '<%=rdoOpenBackOrder.ClientID %>';
            var rdoYTDBackOrder = "#" + '<%=rdoYTDBackOrder.ClientID %>';

            $("#divDailyBackOrder").show();
            $("#divOpenBackOrder").hide();
            $("#divYTDBackOrder").hide();
            $("#tblYear").hide();
            $("#tblDate").show();
            $("#tblCurrent").hide();

            $(rdoDailyBackOrder).change(function () {
                $("#divDailyBackOrder").show();
                $("#divOpenBackOrder").hide();
                $("#divYTDBackOrder").hide();
                $("#tblYear").hide();
                $("#tblDate").show();
                $("#tblCurrent").hide();
            });

            $(rdoOpenBackOrder).change(function () {
                $("#divDailyBackOrder").hide();
                $("#divOpenBackOrder").show();
                $("#divYTDBackOrder").hide();
                $("#tblYear").hide();
                $("#tblDate").hide();
                $("#tblCurrent").show();
            });

            $(rdoYTDBackOrder).change(function () {
                $("#divDailyBackOrder").hide();
                $("#divOpenBackOrder").hide();
                $("#divYTDBackOrder").show();
                $("#tblYear").show();
                $("#tblDate").hide();
                $("#tblCurrent").hide();
                $('#YTDListing').show();
            });

            $('#<%= rdoYTDMasterVendor.ClientID%>').click(function () {
                $('#YTDListing').hide();
            });
            $('#<%= rdoYTDListing.ClientID%>').click(function () {
                $('#YTDListing').show();
            });
        });


        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <asp:HiddenField ID="hdnFromDt" runat="server" />
    <asp:HiddenField ID="hdnToDt" runat="server" />
    <asp:HiddenField ID="hdnSiteIDs" runat="server" />
    <asp:HiddenField ID="hdnCountryIDs" runat="server" />
    <asp:HiddenField ID="hdnVendorIDs" runat="server" />
    <asp:HiddenField ID="hdnItemclassifications" runat="server" />
    <asp:HiddenField ID="hdnStockPlannerIDs" runat="server" />
    <asp:HiddenField ID="hdnReportType" runat="server" />
    <asp:HiddenField ID="hdnGridCurrentPageNo" runat="server" />
    <asp:HiddenField ID="hdnGridPageSize" runat="server" />
    <asp:HiddenField ID="hdnChildReportType" runat="server" />
    <asp:HiddenField ID="hdnChildReportTypeInner" runat="server" />
    <asp:HiddenField ID="hdnStockPlannerNo" runat="server" />
    <asp:HiddenField ID="hdnStockPlannerGroupingID" runat="server" />
     <asp:HiddenField ID="hdnSelectedODSkUCode" runat="server" />
     <asp:HiddenField ID="hdnSelectedVikingSkUCode" runat="server" />
    <h2>
        <cc1:ucLabel ID="ltBackOrderReporting" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSearchScreen" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="15" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblCountry" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblItemClassification" runat="server" Text="Item Classfication"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectItemClassification runat="server" ID="msItemClassification" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                     <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblInventoryGroup" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlannerGrouping runat="server" ID="multiSelectStockPlannerGrouping" />
                        </td>
                    </tr>
                     <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblODSKUCode" runat="server" Text="OD SKU Code">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectSKU runat="server" ID="msSKU" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblODCatCode" runat="server" >
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel12" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:ucVikingSku ID="UcVikingSku" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblReportType" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td align="left" class="nobold radiobuttonlist">
                            <cc1:ucRadioButton ID="rdoDailyBackOrder" runat="server" Text="Daily Back Order"
                                Checked="true" GroupName="ReportType" />
                            <cc1:ucRadioButton ID="rdoOpenBackOrder" runat="server" Text="Open Back Order" GroupName="ReportType" />
                            <cc1:ucRadioButton ID="rdoYTDBackOrder" runat="server" Text="YTD Back Order" GroupName="ReportType" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDisplayat" runat="server" Text="Display at"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td align="left" class="nobold radiobuttonlist">
                            <div id="divDailyBackOrder">
                                <cc1:ucRadioButton Checked="true" ID="rdoDetailedLevel" runat="server" Text="Detailed Level"
                                    GroupName="Displayat1" />
                                <cc1:ucRadioButton ID="rdoSummaryLevel" runat="server" Text="Summary Level" GroupName="Displayat1" />
                            </div>
                            <div id="divOpenBackOrder">
                                <cc1:ucRadioButton Checked="true" ID="rdoOpenDetailedLavel" runat="server" Text="Detailed Level"
                                    GroupName="Displayat2" />
                                <cc1:ucRadioButton ID="rdoSummaryVendor" runat="server" Text="Summary (Vendor)" GroupName="Displayat2" />
                                <cc1:ucRadioButton ID="rdoSummarySite" runat="server" Text="Summary (Site)" GroupName="Displayat2" />
                            </div>
                            <div id="divYTDBackOrder">
                                <cc1:ucRadioButton ID="rdoYTDListing" runat="server" Text="YTD Listing" Checked="true"
                                    GroupName="Displayat3" />
                                <cc1:ucRadioButton ID="rdoYTDMasterVendor" runat="server" Text="YTD Master Vendor"
                                    GroupName="Displayat3" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDate" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <table width="40%" border="0" cellspacing="5" cellpadding="0" id="tblDate">
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblFrom" runat="server" Text="From"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 1%">
                                        :
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                            onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                                    </td>
                                    <td style="text-align: center">
                                        <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 1%">
                                        :
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                            onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                                    </td>
                                </tr>
                            </table>
                            <table width="40%" border="0" cellspacing="5" cellpadding="0" id="tblCurrent">
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="ltCurrentDate" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                            </table>
                            <table width="40%" border="0" cellspacing="5" cellpadding="3" id="tblYear">
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblYear" runat="server" Text="Year"></cc1:ucLabel>
                                    </td>
                                    <td style="width: 1%">
                                        :
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucDropdownList ID="drpYear" runat="server" Width="60px">
                                        </cc1:ucDropdownList>
                                    </td>
                                </tr>
                                <tr><td></td></tr>
                                <tr id="YTDListing" style="display:none;">
                                     <td style="font-weight: bold;">
                                        <cc1:ucRadioButton ID="rblByVendor" Checked="true" runat="server" Text="By Vendor" GroupName="YTDListing"></cc1:ucRadioButton>
                                    </td>
                                    <td style="width: 1%">
                                       
                                    </td>
                                    <td style="font-weight: bold;">
                                         <cc1:ucRadioButton ID="rblByVendorPlanner" runat="server" Text="By Planner/Vendor" GroupName="YTDListing"></cc1:ucRadioButton>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGrids" runat="server" Visible="false">
                <div class="button-row">
                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                        Width="109px" Height="20px" Visible="false" />
                </div>
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <cc1:ucGridView ID="gvBOListing" Visible="false" runat="server" 
                                AutoGenerateColumns="false" AllowSorting="True"
                                CssClass="grid gvclass searchgrid-1 " CellPadding="0" Width="960px" 
                                OnPageIndexChanging="gvDailyBOListing_PageIndexChanging" 
                                onsorting="gvBOListing_Sorting" onrowdatabound="gvBOListing_RowDataBound"
                                >
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="VikingCode" SortExpression="VikingCode">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltVikingCode" runat="server" Text='<%#Eval("VikingCode") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OD Code" SortExpression="ODCode">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltODCode" runat="server" Text='<%#Eval("ODCode") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Number of BO's" SortExpression="NumberOfBOs">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltNumberofBO" runat="server" Text='<%#Eval("NumberOfBOs") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty of BO" SortExpression="QuanityofBackOrder">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltQtyofBO" runat="server" Text='<%#Eval("QuanityofBackOrder") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                  

                                      <asp:TemplateField HeaderText="Item Price" SortExpression="Item_Val">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltItemPrice" runat="server" Text='<%#Eval("Item_Val") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                      <asp:TemplateField HeaderText="COGS" SortExpression="COGS">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltCOGS" runat="server" Text='<%# String.Format("{0:0.00}",Eval("COGS"))%>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                     
                                                                 

                                      <asp:TemplateField HeaderText="Item Price" SortExpression="Item_Val">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltItemPrice_1" runat="server" Text='<%#Eval("Item_Val") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Daily backorder count" SortExpression="NumberOfBOs">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltNumberofBO_1" runat="server" Text='<%#Eval("NumberOfBOs") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                      <asp:TemplateField HeaderText="Daily backorder quantity" SortExpression="QuanityofBackOrder">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltQtyofBO_1" runat="server" Text='<%#Eval("QuanityofBackOrder") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>   

                                      <asp:TemplateField HeaderText="Cogs's of BO's" SortExpression="COGS">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltCOGS_1" runat="server" Text='<%# String.Format("{0:0.00}",Eval("COGS"))%>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                     <asp:TemplateField HeaderText="Total backorders" SortExpression="BOPreviousDay">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltNumberofBOPrevious_1" runat="server" Text='<%#Eval("BOPreviousDay") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Total qunatity on backorder" SortExpression="QuanityPreviousDay">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltNumberofUnitsPrevious_1" runat="server" Text='<%#Eval("QuanityPreviousDay") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="COGs of previous day's BO's" SortExpression="COGSPreviousDay">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltCOGSprevious_1" runat="server" Text='<%# String.Format("{0:0.00}",Eval("COGSPreviousDay"))%>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Item Classification" SortExpression="ItemClassification">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltItemClassification" runat="server" Text='<%#Eval("ItemClassification") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Number" SortExpression="VendorNumber">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltVendorNo" runat="server" Text='<%#Eval("VendorNumber") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Name" SortExpression="VendorName">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltVendorName" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" SortExpression="DESCRIPTION">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltDescription" runat="server" Text='<%#Eval("DESCRIPTION") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stock Planner Number" SortExpression="StockPlannerNumber">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltStockPlannerNo" runat="server" Text='<%#Eval("StockPlannerNumber") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stock Planner Name" SortExpression="StockPlannerName">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltStockPlannerName" runat="server" Text='<%#Eval("StockPlannerName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                   <%--  <asp:TemplateField HeaderText="Stock Planner Grouping" SortExpression="StockPlannerGroupings">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltStockPlannerGroupings" runat="server" Text='<%#Eval("StockPlannerGroupings") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>

                                     <asp:TemplateField HeaderText="On Back Order Since" SortExpression="BoSince" Visible="false">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left"/>
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltBOSince" runat="server" Text='<%#Eval("BoSince","{0:dd/MM/yyyy}") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Days On Back Order" SortExpression="DaysOnBackOrder" Visible="false">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Center" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltDaysOnBackOrder" runat="server" Text='<%#Eval("DaysOnBackOrder") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Date" SortExpression="DATE">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltDate" runat="server" Text='<%#Eval("DATE","{0:dd/MM/yyyy}") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Site" SortExpression="Site">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltSite" runat="server" Text='<%#Eval("Site") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Country" SortExpression="Country">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltCountry" runat="server" Text='<%#Eval("Country") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <cc1:ucGridView ID="gvBOSummary" Visible="false" runat="server" 
                                AutoGenerateColumns="false" AllowSorting="true"
                                CssClass="grid gvclass searchgrid-1 " CellPadding="0" Width="960px" 
                                OnPageIndexChanging="gvDailyBOListing_PageIndexChanging" onsorting="gvBOSummary_Sorting"
                                >
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="Vendor Number" SortExpression="VendorNumber">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltVendorNo" runat="server" Text='<%#Eval("VendorNumber") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Name" SortExpression="VendorName">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltVendorName" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stock Planner Number" SortExpression="StockPlannerNumber">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltStockPlannerNo" runat="server" Text='<%#Eval("StockPlannerNumber") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stock Planner Name" SortExpression="StockPlannerName">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltStockPlannerName" runat="server" Text='<%#Eval("StockPlannerName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Stock Planner Grouping" SortExpression="StockPlannerGroupings">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltStockPlannerGroupings" runat="server" Text='<%#Eval("StockPlannerGroupings") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Date" SortExpression="DATE">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltDate" runat="server" Text='<%#Eval("DATE","{0:dd/MM/yyyy}") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Site" SortExpression="Site">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltSite" runat="server" Text='<%#Eval("Site") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Country" SortExpression="Country">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltCountry" runat="server" Text='<%#Eval("Country") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Number of BO's" SortExpression="NumberOfBOs">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltNumberofBO" runat="server" Text='<%#Eval("NumberOfBOs") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="COGS" SortExpression="COGS">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltNumberofCOGS" runat="server" Text='<%#Eval("COGS") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <cc1:ucGridView ID="gvBOSummarySite" Visible="false" runat="server" 
                                AutoGenerateColumns="false" AllowSorting="true"
                                CssClass="grid gvclass searchgrid-1 " CellPadding="0" Width="950px" 
                                OnPageIndexChanging="gvDailyBOListing_PageIndexChanging" onsorting="gvBOSummarySite_Sorting"
                                >
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="Country" SortExpression="Country">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="250px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltCountry" runat="server" Text='<%#Eval("Country") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Site" SortExpression="Site">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="250px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltSite" runat="server" Text='<%#Eval("Site") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Number of BO's" SortExpression="NumberOfBOs">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="250px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltNumberofBO" runat="server" Text='<%#Eval("NumberOfBOs") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="COGS" SortExpression="COGS">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="250px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltNumberofCOGS" runat="server" Text='<%#Eval("COGS") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date" SortExpression="DATE">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltDate" runat="server" Text='<%#Eval("DATE","{0:dd/MM/yyyy}") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>

                            <table border="0" cellpadding="0" cellspacing="0" Width="960px" id="tblFilter" runat="server" visible="false">
                                <tr>
                                    <td align="center">
                                    <cc1:ucRadioButton ID="rblByQuantity" Checked="true" runat="server"   AutoPostBack="true" GroupName="child"
                                            Text="By Quantity" oncheckedchanged="rblByQuantity_CheckedChanged" />                                           
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                           
                                     <cc1:ucRadioButton ID="rblByValue" runat="server"  Text="By Value" AutoPostBack="true" GroupName="child"
                                            oncheckedchanged="rblByValue_CheckedChanged" />                                           
                            
                                    </td>
                                </tr>
                            </table>
                            
                  <br />
                  <br />
                            
                            <cc1:ucGridView ID="gvYTDBOListing" Visible="false" runat="server" AutoGenerateColumns="false"
                                CssClass="grid gvclass searchgrid-1 " CellPadding="0" Width="960px" 
                                OnPageIndexChanging="gvDailyBOListing_PageIndexChanging" onrowdatabound="gvYTDBOListing_RowDataBound"
                                >
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="Country">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltCountry" runat="server" Text='<%#Eval("Country") %>'></cc1:ucLiteral>
                                            <cc1:ucLiteral ID="ltIsvendorsearch" runat="server" Visible="false" Text='<%#Eval("filtercheck") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor No">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltVendorNo" runat="server" Text='<%#Eval("VendorNumber") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Name">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltVendorName" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="YTD Total No of BO">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltNumberofBO" runat="server" Text='<%#Eval("NumberOfBOs") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stock Planner Number">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltStockPlannerNo" runat="server" Text='<%#Eval("StockPlannerNumber") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stock Planner Name">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltStockPlannerName" runat="server" Text='<%#Eval("StockPlannerName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Stock Planner Grouping">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltStockPlannerGroupings" runat="server" Text='<%#Eval("StockPlannerGroupings") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Jan">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                          <asp:HyperLink ID="hlJan" runat="server" Text='<%# Eval("Jan") %>' Target="_blank"
                                                NavigateUrl='<%# EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + Eval("VendorID") + "&BuyerNo=" + Eval("StockPlannerNumber")  + "&SiteID=" + hdnSiteIDs.Value + "&DateFrom="+ hdnJSFromDt.Value + "&StockPlannerID="+ Eval("StockPlannerID") +"&StockPlannerGroupingID="+ Eval("StockPlannerGroupingsID") +"&ODSkUCode="+ Eval("ODCode") +"&VikingSkUCode="+ Eval("VikingCode") +"&MonthValue=01&OpenSubReport=Yes")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Feb">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlFeb" runat="server" Text='<%# Eval("Feb") %>' Target="_blank"
                                                NavigateUrl='<%# EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + Eval("VendorID") + "&BuyerNo=" + Eval("StockPlannerNumber")  + "&SiteID=" + hdnSiteIDs.Value  + "&DateFrom="+ hdnJSFromDt.Value + "&StockPlannerID="+ Eval("StockPlannerID") +"&StockPlannerGroupingID="+ Eval("StockPlannerGroupingsID") +"&ODSkUCode="+ Eval("ODCode") +"&VikingSkUCode="+ Eval("VikingCode") +"&MonthValue=02&OpenSubReport=Yes")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mar">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlMar" runat="server" Text='<%# Eval("Mar") %>' Target="_blank"
                                                NavigateUrl='<%# EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + Eval("VendorID") + "&BuyerNo=" + Eval("StockPlannerNumber")  + "&SiteID=" + hdnSiteIDs.Value  + "&DateFrom="+ hdnJSFromDt.Value +"&StockPlannerID="+ Eval("StockPlannerID") +"&StockPlannerGroupingID="+ Eval("StockPlannerGroupingsID") +"&ODSkUCode="+ Eval("ODCode") +"&VikingSkUCode="+ Eval("VikingCode") +"&MonthValue=03&OpenSubReport=Yes")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Apr">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlApr" runat="server" Text='<%# Eval("Apr") %>' Target="_blank"
                                                NavigateUrl='<%# EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + Eval("VendorID") + "&BuyerNo=" + Eval("StockPlannerNumber")  + "&SiteID=" + hdnSiteIDs.Value  + "&DateFrom="+ hdnJSFromDt.Value +"&StockPlannerID="+ Eval("StockPlannerID") +"&StockPlannerGroupingID="+ Eval("StockPlannerGroupingsID") +"&ODSkUCode="+ Eval("ODCode") +"&VikingSkUCode="+ Eval("VikingCode") +"&MonthValue=04&OpenSubReport=Yes")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="May">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlMay" runat="server" Text='<%# Eval("May") %>' Target="_blank"
                                                NavigateUrl='<%# EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + Eval("VendorID") + "&BuyerNo=" + Eval("StockPlannerNumber")  + "&SiteID=" + hdnSiteIDs.Value  + "&DateFrom="+ hdnJSFromDt.Value +"&StockPlannerID="+ Eval("StockPlannerID") +"&StockPlannerGroupingID="+ Eval("StockPlannerGroupingsID") +"&ODSkUCode="+ Eval("ODCode") +"&VikingSkUCode="+ Eval("VikingCode") +"&MonthValue=05&OpenSubReport=Yes")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Jun">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlJun" runat="server" Text='<%# Eval("Jun") %>' Target="_blank"
                                                NavigateUrl='<%# EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + Eval("VendorID") + "&BuyerNo=" + Eval("StockPlannerNumber")  + "&SiteID=" + hdnSiteIDs.Value  + "&DateFrom="+ hdnJSFromDt.Value +"&StockPlannerID="+ Eval("StockPlannerID") +"&StockPlannerGroupingID="+ Eval("StockPlannerGroupingsID") +"&ODSkUCode="+ Eval("ODCode") +"&VikingSkUCode="+ Eval("VikingCode") +"&MonthValue=06&OpenSubReport=Yes")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Jul">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlJul" runat="server" Text='<%# Eval("Jul") %>' Target="_blank"
                                                NavigateUrl='<%# EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + Eval("VendorID") + "&BuyerNo=" + Eval("StockPlannerNumber") + "&SiteID=" + hdnSiteIDs.Value  + "&DateFrom="+ hdnJSFromDt.Value +"&StockPlannerID="+ Eval("StockPlannerID") +"&StockPlannerGroupingID="+ Eval("StockPlannerGroupingsID") +"&ODSkUCode="+ Eval("ODCode") +"&VikingSkUCode="+ Eval("VikingCode") +"&MonthValue=07&OpenSubReport=Yes")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Aug">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlAug" runat="server" Text='<%# Eval("Aug") %>' Target="_blank"
                                                NavigateUrl='<%# EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + Eval("VendorID") + "&BuyerNo=" + Eval("StockPlannerNumber")  + "&SiteID=" + hdnSiteIDs.Value  + "&DateFrom="+ hdnJSFromDt.Value +"&StockPlannerID="+ Eval("StockPlannerID") +"&StockPlannerGroupingID="+ Eval("StockPlannerGroupingsID") +"&ODSkUCode="+ Eval("ODCode") +"&VikingSkUCode="+ Eval("VikingCode") +"&MonthValue=08&OpenSubReport=Yes")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sep">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlSep" runat="server" Text='<%# Eval("Sep") %>' Target="_blank"
                                                NavigateUrl='<%# EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + Eval("VendorID") + "&BuyerNo=" + Eval("StockPlannerNumber")  + "&SiteID=" + hdnSiteIDs.Value  + "&DateFrom="+ hdnJSFromDt.Value +"&StockPlannerID="+ Eval("StockPlannerID") +"&StockPlannerGroupingID="+ Eval("StockPlannerGroupingsID") +"&ODSkUCode="+ Eval("ODCode") +"&VikingSkUCode="+ Eval("VikingCode") +"&MonthValue=09&OpenSubReport=Yes")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Oct">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlOct" runat="server" Text='<%# Eval("Oct") %>' Target="_blank"
                                                NavigateUrl='<%# EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + Eval("VendorID") + "&BuyerNo=" + Eval("StockPlannerNumber")  + "&SiteID=" + hdnSiteIDs.Value  + "&DateFrom="+ hdnJSFromDt.Value +"&StockPlannerID="+ Eval("StockPlannerID") +"&StockPlannerGroupingID="+ Eval("StockPlannerGroupingsID") +"&ODSkUCode="+ Eval("ODCode") +"&VikingSkUCode="+ Eval("VikingCode") +"&MonthValue=10&OpenSubReport=Yes")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nov">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlNov" runat="server" Text='<%# Eval("Nov") %>' Target="_blank"
                                                NavigateUrl='<%# EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + Eval("VendorID") + "&BuyerNo=" + Eval("StockPlannerNumber")  + "&SiteID=" + hdnSiteIDs.Value  + "&DateFrom="+ hdnJSFromDt.Value +"&StockPlannerID="+ Eval("StockPlannerID") +"&StockPlannerGroupingID="+ Eval("StockPlannerGroupingsID") +"&ODSkUCode="+ Eval("ODCode") +"&VikingSkUCode="+ Eval("VikingCode") +"&MonthValue=11&OpenSubReport=Yes")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dec">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlDec" runat="server" Text='<%# Eval("Dec") %>' Target="_blank"
                                                NavigateUrl='<%# EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + Eval("VendorID") + "&BuyerNo=" + Eval("StockPlannerNumber")  + "&SiteID=" + hdnSiteIDs.Value  + "&DateFrom="+ hdnJSFromDt.Value +"&StockPlannerID="+ Eval("StockPlannerID") +"&StockPlannerGroupingID="+ Eval("StockPlannerGroupingsID") +"&ODSkUCode="+ Eval("ODCode") +"&VikingSkUCode="+ Eval("VikingCode") +"&MonthValue=12&OpenSubReport=Yes")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>


                                <cc1:ucGridView ID="grdYTDMaster" Visible="false" runat="server" AutoGenerateColumns="false"
                                CssClass="grid gvclass searchgrid-1 " CellPadding="0" Width="100%" 
                                OnPageIndexChanging="gvDailyBOListing_PageIndexChanging" onrowdatabound="grdYTDMaster_RowDataBound" 
                                >
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                 <asp:TemplateField HeaderText="Global Consolidation">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltGlobalConsolidation" runat="server" Text='<%#Eval("GlobalConsolidation") %>'></cc1:ucLiteral>        
                                            <cc1:ucLiteral ID="ltIsParent" runat="server" Visible="false" Text='<%#Eval("IsParent") %>'></cc1:ucLiteral>                                               
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Global Name">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltGlobalName" runat="server" Text='<%#Eval("GlobalName") %>'></cc1:ucLiteral>                                            
                                     </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Local Consolidation">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltLocalConsolidation" runat="server" Text='<%#Eval("LocalConsolidation") %>'></cc1:ucLiteral>                                            
                                     </ItemTemplate>
                                    </asp:TemplateField>

                                      <asp:TemplateField HeaderText="Local Name">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltLocalName" runat="server" Text='<%#Eval("LocalName") %>'></cc1:ucLiteral>                                            
                                     </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Vendor No">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltVendorNo" runat="server" Text='<%#Eval("VendorNumber") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Name">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltVendorName" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Country">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltCountry" runat="server" Text='<%#Eval("Country") %>'></cc1:ucLiteral>                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="YTD Back Orders">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltNumberofBO" runat="server" Text='<%#Eval("NumberOfBOs") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>  
                                    <asp:TemplateField HeaderText="YTD COGS">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltCOGS" runat="server" Text='<%#Eval("COGS") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                        
                                    <asp:TemplateField HeaderText="Jan">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        <ItemTemplate>
                                          <asp:Label ID="hlJan" runat="server" Text='<%# Eval("Jan") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Feb">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        <ItemTemplate>
                                            <asp:Label ID="hlFeb" runat="server" Text='<%# Eval("Feb") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mar">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        <ItemTemplate>
                                            <asp:Label ID="hlMar" runat="server" Text='<%# Eval("Mar") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Apr">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        <ItemTemplate>
                                            <asp:Label ID="hlApr" runat="server" Text='<%# Eval("Apr") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="May">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        <ItemTemplate>
                                            <asp:Label ID="hlMay" runat="server" Text='<%# Eval("May") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Jun">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        <ItemTemplate>
                                            <asp:Label ID="hlJun" runat="server" Text='<%# Eval("Jun") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Jul">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        <ItemTemplate>
                                            <asp:Label ID="hlJul" runat="server" Text='<%# Eval("Jul") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Aug">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        <ItemTemplate>
                                            <asp:Label ID="hlAug" runat="server" Text='<%# Eval("Aug") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sep">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        <ItemTemplate>
                                            <asp:Label ID="hlSep" runat="server" Text='<%# Eval("Sep") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Oct">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        <ItemTemplate>
                                            <asp:Label ID="hlOct" runat="server" Text='<%# Eval("Oct") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nov">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        <ItemTemplate>
                                            <asp:Label ID="hlNov" runat="server" Text='<%# Eval("Nov") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dec">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                        <ItemTemplate>
                                            <asp:Label ID="hlDec" runat="server" Text='<%# Eval("Dec") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>


                            <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                            </cc1:PagerV2_8>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sConn %>"
                                SelectCommand="spRPT_BackOrderReport" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hdnCountryIDs" DefaultValue="-1" Name="SelectedCountryIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnItemclassifications" DefaultValue="-1" Name="SelectedItemClassification"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnSiteIDs" Name="SelectedSiteIDs" PropertyName="Value"
                                        Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnVendorIDs" Name="SelectedVendorIDs" PropertyName="Value"
                                        Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnStockPlannerIDs" Name="SelectedStockPlannerIDs"
                                        PropertyName="Value" Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnToDt" Name="DateTo" PropertyName="Value" Type="DateTime"
                                        DefaultValue="" />
                                    <asp:ControlParameter ControlID="hdnFromDt" Name="DateFrom" PropertyName="Value"
                                        Type="DateTime" DefaultValue="" />
                                    <asp:ControlParameter ControlID="hdnReportType" Name="ReportType" PropertyName="Value"
                                        Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnGridCurrentPageNo" Name="GridCurrentPageNo" PropertyName="Value"
                                        Type="Int32" DefaultValue="1" />
                                    <asp:ControlParameter ControlID="hdnGridPageSize" Name="GridPageSize" PropertyName="Value"
                                        Type="Int32" DefaultValue="50" />
                                     <asp:ControlParameter ControlID="hdnChildReportType" Name="ReportChildType" PropertyName="Value"
                                     Type="String" DefaultValue="-1" />
                                      <asp:ControlParameter ControlID="hdnChildReportTypeInner" Name="ReportChildTypeInner" PropertyName="Value"
                                     Type="String" DefaultValue="-1" />
                                       <asp:ControlParameter ControlID="hdnStockPlannerNo" Name="SelectedStockPlannerNo" PropertyName="Value"
                                     Type="String" DefaultValue="-1" />
                                       <asp:ControlParameter ControlID="hdnStockPlannerGroupingID" Name="SelectedStockPlannerGroupingIDs" PropertyName="Value"
                                     Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnSelectedODSkUCode" Name="SelectedODSkUCode" PropertyName="Value"
                                     Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnSelectedVikingSkUCode" Name="SelectedVikingSkUCode" PropertyName="Value"
                                     Type="String" DefaultValue="-1" />
                                     
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlReportViewer" runat="server" Visible="false">
                <rsweb:ReportViewer ID="BackOrderReportViewer" runat="server" Width="950px" DocumentMapCollapsed="True"
                    Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                    WaitMessageFont-Size="14pt" Height="750px">
                    <LocalReport>
                        <DataSources>
                            <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="dtBackOrder" />
                        </DataSources>
                    </LocalReport>
                </rsweb:ReportViewer>
            </cc1:ucPanel>
            <div class="button-row">
                <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                    Visible="false" OnClick="btnBackSearch_Click" />
            </div>
        </div>
    </div>
</asp:Content>

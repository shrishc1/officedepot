﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BackOrderPenlatyOverview.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
 Inherits="ModuleUI_StockOverview_BackOrder_BackOrderPenlatyOverview" %>

 <%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
    <%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlannerGrouping.ascx" TagName="MultiSelectStockPlannerGrouping"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


 <script type="text/javascript" language="javascript">
     $(document).ready(function () {
         var rdoSummaryLevel = "#" + '<%=rdoSummaryLevel.ClientID %>';
         var rdoDetailedLevel = "#" + '<%=rdoDetailedLevel.ClientID %>';
         var rdoSiteView = "#" + '<%=rdoSiteView.ClientID %>';         

         if ($(rdoDetailedLevel).is(':checked')) {
             $("#divSummaryByRdoBtn").hide();
             $("#divSummaryBy").hide();
             $("#divColon").hide();
         }

         $(rdoSummaryLevel).change(function () {             
             $("#divSummaryByRdoBtn").show();
             $("#divSummaryBy").show();
             $("#divColon").show();
             $(rdoSiteView).attr('checked', true);
             //             $("#<%= rdoSiteView.ClientID %> input[type=radio]").attr('checked', true);

         });


         $(rdoDetailedLevel).change(function () {             
             $("#divSummaryByRdoBtn").hide();
             $("#divSummaryBy").hide();
             $("#divColon").hide();
             $(rdoSiteView).attr('checked', false);
             //             $("#<%= rdoSiteView.ClientID %> input[type=radio]").attr('checked', false);

         });
     });

     function setValue1(target) {
         document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
     }

     function setValue2(target) {
         document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
     }


</script>

 <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <asp:HiddenField ID="hdnUserID" runat="server" />
     <h2>
        <cc1:ucLabel ID="lblBackOrderPenaltyOverview" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
  
        <div class="formbox">
    
                <%--              <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>--%>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings" id="tblSearch" runat="server">
                    <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 89%">
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>                   
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblInventoryGroup" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Uclbl8" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlannerGrouping runat="server" ID="multiSelectStockPlannerGrouping" />
                             <br />
                        <br />
                        </td>
                       
                    </tr>                  
                    
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblReportType" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td align="left" class="nobold radiobuttonlist">
                            <cc1:ucRadioButton ID="rdoSummaryLevel" runat="server" Text="Summary Level" Checked="true" GroupName="ReportType" />
                            <cc1:ucRadioButton ID="rdoDetailedLevel" runat="server" Text="Detailed Level"  GroupName="ReportType" />                           
                        </td>
                    </tr>

                     <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStatus" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                         <td align="left" class="nobold radiobuttonlist" valign="middle">
                            <cc1:ucRadioButtonList ID="rblApproveType" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="lstCheckAll" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Accepted" Value="2" ></asp:ListItem>
                                <asp:ListItem Text="Declined" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Pending" Value="4"></asp:ListItem>                                
                            </cc1:ucRadioButtonList>                           
                        </td>                        
                    </tr>
                     <tr>                      
                        <td style="font-weight: bold;">
                        <div id="divSummaryBy">
                            <cc1:ucLabel ID="lblSummaryBy" runat="server" Text="Summary By"></cc1:ucLabel>
                            </div>
                        </td>
                        <td style="font-weight: bold;">
                        <div id="divColon">
                            : 
                             </div>
                        </td>
                        <td align="left" class="nobold radiobuttonlist">
                            <div id="divSummaryByRdoBtn">
                                <cc1:ucRadioButton  ID="rdoSiteView" runat="server" Text="Site" Checked="true" GroupName="SummaryBy" />
                                <cc1:ucRadioButton ID="rdoInventoryGroupView" runat="server" Text="Inventory Group" GroupName="SummaryBy" />                            
                                <cc1:ucRadioButton  ID="rdoPlannerView" runat="server" Text="Planner" GroupName="SummaryBy" />
                                <cc1:ucRadioButton ID="rdoVendorView" runat="server" Text="Vendor" GroupName="SummaryBy" />                               
                               </div>                   
                        </td>                          
                    </tr>  
                     
                     <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDate" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                          <table width="40%" border="0" cellspacing="5" cellpadding="0" id="tblDate">
                                <tr>
                            <td >
                                <cc1:ucLabel ID="lblFrom" runat="server" Text="From"></cc1:ucLabel>
                            </td>
                            <td style="width: 1%">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtFromDate" runat="server" ClientIDMode="Static" CssClass="date"
                                    onchange="setValue1(this)" ReadOnly="True" Width="70px" />
                            </td>
                            <td style="text-align: center">
                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                            </td>
                            <td style="width: 1%">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucTextbox ID="txtToDate" runat="server" ClientIDMode="Static" CssClass="date"
                                    onchange="setValue2(this)" ReadOnly="True" Width="70px" />
                            </td>
                            </tr>
                            </table>
                            </td>
                       </tr> 
                       
                        <tr>
                    <td align="right" colspan="5">
                        <div class="button-row">
                            <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
                                OnClick="btnGenerateReport_Click" />
                        </div>
                    </td>
                </tr>        
              </table>


                   <div id="divSummaryGrid" runat="server" visible="false">
                        <div class="button-row">
                        <br />
                             <%--<cc2:ucexporttoexcel ID="ucExportToExcel1" runat="server"  Visible="false" />     --%>                        
                            <cc1:ucButton ID="btnExportToExcel" runat="server" Visible="false" CssClass="exporttoexcel button" OnClick="btnExport_Click"
                             Width="109px" Height="20px" /> 
                        </div>
                         
                        <div class="wmd-view-topscroll">
                            <div class="scroll-div">
                                &nbsp;
                            </div>
                        </div>
                        <div class="wmd-view">
                            <div class="dynamic-div">
                                <cc1:ucGridView ID="grdBind" ClientIDMode="Static" Width="100%" Height="80%" runat="server"
                                    AllowPaging="true" PageSize="50" CssClass="grid modify-grid" GridLines="Both" AutoGenerateColumns="true" EnableViewState="false"
                                     OnPageIndexChanging="OnPageIndexChanging" OnRowDataBound="grdBind_RowDataBound" OnRowCreated="grdBind_RowCreated" >
                                    <RowStyle HorizontalAlign="Center"></RowStyle>

                                    <%--<Columns>
                                     <asp:BoundField DataField="UserID" HeaderText="UserID" ItemStyle-Width="150" Visible="false" />
                                     </Columns>
--%>
                                </cc1:ucGridView>
                            </div>
                            <asp:Label ID="lblError" runat="server" Visible="false" />
                        </div>
                        <cc1:ucGridView ID="grdBindExport" Visible="false" ClientIDMode="Static" Width="100%"
                            Height="80%" runat="server" CssClass="grid" GridLines="Both" 
                            AutoGenerateColumns="true" onrowcreated="grdBindExport_RowCreated">                          
                            
                        </cc1:ucGridView>
                <div class="button-row" >
                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false">
                    </cc1:PagerV2_8>
                </div>
                        <div class="button-row">
                            <br />
                            <br />
                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" align="right" Visible="false"
                                CssClass="button" OnClick="btnBack_Click" />
                        </div>
                 </div>
        
          </div>
        </div>
</asp:Content>
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/NoLogoMasterPage.master"
    AutoEventWireup="true" CodeFile="EsclationReview.aspx.cs" Inherits="EsclationReview" %>

<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">

        $(document).ready(function () {            
            $("#ctl00_ContentPlaceHolder1_gvMediatorReview").find("tr").each(function () {
                $(this).find("td:first label").hide();
            });

            $("#trPenalty").hide();
            $("#trComments").hide();
            $("#trAlternateDetails").hide();

            var rdoNoPenaltyAppliedNew = '<%=rdoNoPenaltyAppliedNew.ClientID %>';
            var rdoPenaltytobeAppliedNew = '<%=rdoPenaltytobeAppliedNew.ClientID %>';
            var rdoAlternate = '<%= rdoAlternate.ClientID %>';
            var rdoReviewedPending = '<%= rdoReviewedPending.ClientID %>';


            //if ($("[id$='rdoReviewedPending']").is(":checked"))


            if ($("#" + rdoReviewedPending).is(":checked")) {               
                $("#trPenalty").hide();
                $("#trAlternateDetails").hide();
                $("#trComments").show();
            }

            $("#" + rdoNoPenaltyAppliedNew).change(function () {
                $("#trPenalty").hide();
                $("#trComments").show();
                $("#trAlternateDetails").hide();

            });

            $("#" + rdoPenaltytobeAppliedNew).change(function () {
                $("#trPenalty").show();
                $("#trComments").show();
                $("#trAlternateDetails").hide();

            });

            $("#" + rdoAlternate).change(function () {
                $("#trPenalty").hide();
                $("#trAlternateDetails").show();
                $("#trComments").show();
                

            });

            $("#" + rdoReviewedPending).change(function () {
                $("#trPenalty").hide();
                $("#trAlternateDetails").hide();
                $("#trComments").show();
            });
        });


        $(document).ready(function () {
            $("[id$='btnSave']").click(function (e) {
                if ($("[id$='rdoNoPenaltyAppliedNew']").is(":checked")) {
                    var txtComments = $("[id$='txtMediatorComment']").val().trim();
                    if (txtComments == '') {
                        alert('<%=PleaseEnterComments %>');
                        return false;
                    }
                }
                else if ($("[id$='rdoPenaltytobeAppliedNew']").is(":checked")) {
                    var txtPenaltyChargeImposed = $("[id$='txtPenaltyChargeImposed']").val().trim();
                    var txtPenaltyChargeAgreed = $("[id$='txtPenaltyChargeAgreedWith']").val().trim();
                    var txtComments = $("[id$='txtMediatorComment']").val().trim();
                    var txtAgreedTime = $("[id$='txtAgreedPenaltyTime']").val().trim();

                    if (txtPenaltyChargeImposed == '') {
                        alert('<%=PleaseEnterPenaltyCharge %>');
                        return false;
                    }
                    if (txtPenaltyChargeAgreed == '') {
                        alert('<%=PleaseEnterPenaltyChargeAgreedWith %>');
                        return false;
                    }
                    if (txtAgreedTime == '') {
                        alert('<%=PleaseEnterTime%>');
                        return false;
                    }
                    if (txtComments == 0) {
                        alert('<%=PleaseEnterComments %>');
                        return false;
                    }
                }
                else if ($("[id$='rdoAlternate']").is(":checked")) {
                    var txtPenaltyChargeAgreedWith_1 = $("[id$='txtPenaltyChargeAgreedWith_1']").val().trim();
                    var txtComments = $("[id$='txtMediatorComment']").val().trim();

                    if(txtPenaltyChargeAgreedWith_1 == '')
                    {
                        alert('<%=PleaseEnterPenaltyChargeAgreedWith %>');
                        return false;
                    }
                    if (txtComments == '') {
                        alert('<%=PleaseEnterComments %>');
                        return false;
                    }
                }
                else if ($("[id$='rdoReviewedPending']").is(":checked")) {                    
                    var txtComments = $("[id$='txtMediatorComment']").val().trim();

                    if(txtPenaltyChargeAgreedWith_1 == '')
                    {
                        alert('<%=PleaseEnterPenaltyChargeAgreedWith %>');
                        return false;
                    }
                    if (txtComments == '') {
                        alert('<%=PleaseEnterComments %>');
                        return false;
                    }
                }
                return true;
            });
        });

        function SPActonReqired(sender, args) {

            var objrdo1 = document.getElementById('<%=rdoNoPenaltyAppliedNew.ClientID %>');
            var objrdo2 = document.getElementById('<%=rdoPenaltytobeAppliedNew.ClientID %>');
            var objrdo3 = document.getElementById('<%=rdoAlternate.ClientID %>');
            var objrdo4 = document.getElementById('<%=rdoReviewedPending.ClientID %>');
            if (objrdo1.checked == false && objrdo2.checked == false && objrdo3.checked == false && objrdo4.checked == false) {
                args.IsValid = false;
            }
        }

        function SetRadioButton(spanChk) {

            $("input[type='radio'][id$='rblSelect']").removeAttr("checked");
            spanChk.checked = true;
        }

        function CalculateRevisedPenalty() {
            var PenaltyChargeImposed = document.getElementById("<%=txtPenaltyChargeImposed.ClientID %>").value;
            var OriginalPenaltyCharge = document.getElementById("<%=txtOriginalPenaltyValue.ClientID %>").value;
            var dval = parseFloat(parseFloat(PenaltyChargeImposed).toFixed(2));

            if (!isNaN(dval)) {

                if (dval == 0) {
                    alert("<%= PenaltyChargeImposedCannotBeZero %>");
                    document.getElementById("<%=txtPenaltyChargeImposed.ClientID %>").value = '';
                    return false;
                }
                if (parseFloat(parseFloat(PenaltyChargeImposed).toFixed(2)) > parseFloat(parseFloat(OriginalPenaltyCharge).toFixed(2))) {
                    alert("<%=ImposedValueCannotGreaterThanCurrentPenalty %>");
                    document.getElementById("<%=txtPenaltyChargeImposed.ClientID %>").value = '';
                    $('#' + document.getElementById("<%=txtPenaltyChargeImposed.ClientID %>")).focus();
                    return false;
                }
                document.getElementById("<%=txtPenaltyChargeImposed.ClientID %>").value = parseFloat(PenaltyChargeImposed).toFixed(2);
            }
            else {
                document.getElementById("<%=txtPenaltyChargeImposed.ClientID %>").value = '';
            }
        }



    </script>
   <%-- <h2>
        <cc1:ucLabel ID="lblSKUDetail" runat="server"></cc1:ucLabel>
    </h2>--%>
    <div class="right-shadow" style="width:100%">
        <div class="formbox">
            <div id="divGridView" runat="server">
                <div class="button-row" style="text-align:right">
                    <br />
                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                        Width="109px" Height="20px" />
                </div>
                <cc1:ucPanel ID="pnlQueryDetails" runat="server" CssClass="fieldset-form">
                    <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table">
                        <tr>
                            <td style="font-weight: bold;" colspan="6">
                                <div style="width: 910px; " class="fixedTable">
                                    <cc1:ucGridView ID="gvMediatorReview" runat="server" CssClass="grid gvclass searchgrid-1"
                                        Width="910px" GridLines="Both" OnPageIndexChanging="gvMediatorReview_PageIndexChanging"
                                        OnDataBound="gvMediatorReview_OnDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Select">
                                                <HeaderStyle HorizontalAlign="Left" Width="20px" />
                                                <ItemStyle Width="20px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:RadioButton runat="server" CssClass="innerCheckBox" ID="rblSelect" OnCheckedChanged="rblSelect_CheckedChanged"
                                                        AutoPostBack="true" Text='<%#Eval("PenaltyChargeID") %>' onclick='<%# "javascript:SetRadioButton(this);" %>' />
                                                    <cc1:ucLabel runat="server" ID="lblPenaltyChargeId" Text='<%#Eval("PenaltyChargeID") %>'
                                                        Visible="false"></cc1:ucLabel>
                                                    <asp:HiddenField ID="hdnBOReportingID" Value='<%# Eval("BOReportingID") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ODSKU" SortExpression="SKU.OD_SKU_NO">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltodsku" Text='<%#Eval("SKU.OD_SKU_NO") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VikingSku" SortExpression="SKU.Direct_SKU">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVikingSku" Text='<%#Eval("SKU.Direct_SKU") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorCode" SortExpression="SKU.Vendor_Code">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendorCode" Text='<%#Eval("SKU.Vendor_Code") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description" SortExpression="SKU.DESCRIPTION">
                                                <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltDescription" Text='<%#Eval("SKU.DESCRIPTION") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DateBOIncurred" SortExpression="BOIncurredDate">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltDateBOIncurred" Text='<%#Eval("BOIncurredDate", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Site" SortExpression="SKU.SiteName">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltsite" Text='<%#Eval("SKU.SiteName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CountofBO(s)Incurred" SortExpression="NoofBO">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltCountofBoIncurred" Text='<%#Eval("NoofBO") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" Width="60px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="QtyofBO(s)Incurred" SortExpression="QtyonBackOrder">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt4" Text='<%#Convert.ToInt64(Math.Round(Convert.ToDouble(Eval("QtyonBackOrder")))) %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" Width="60px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="Vendor.StockPlannerName">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockPlannerName" SortExpression="Vendor.StockPlannerNumber">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                                                <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OpenPONumbers" SortExpression="PurchaseOrder.Purchase_order">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltOpenPONo" Text='<%#Eval("PurchaseOrder.Purchase_order") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OrderRaisedDate" SortExpression="PurchaseOrder.Order_raised">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt5" Text='<%#Eval("PurchaseOrder.Order_raised", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OriginalDueDate" SortExpression="PurchaseOrder.Original_due_date">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt7" Text='<%#Eval("PurchaseOrder.Original_due_date", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OutstandingQtyonPO" SortExpression="PurchaseOrder.Outstanding_Qty">
                                                <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltOutstandingQty" Text='<%#Eval("PurchaseOrder.Outstanding_Qty") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ChargeIncurred" SortExpression="PotentialPenaltyCharge">
                                                <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltPotentialPenaltyCharge" Text='<%#Eval("PotentialPenaltyCharge") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" Width="40px" Font-Size="10px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </cc1:ucGridView>
                                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                                    </cc1:PagerV2_8>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; width: 19%">
                                <cc1:ucLabel ID="lblOriginalPenaltyValue" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 27%">
                                <cc1:ucTextbox ID="txtOriginalPenaltyValue" runat="server" Width="100px" Enabled="false"></cc1:ucTextbox>
                            </td>
                            <td style="font-weight: bold; width: 19%">
                                <cc1:ucLabel ID="lblValueInDispute" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 33%">
                                <cc1:ucTextbox ID="txtValueInDispute" runat="server"  Width="100px" Enabled="false"></cc1:ucTextbox>
                                <cc1:ucTextbox ID="txtRevisedPenalty" runat="server" Width="100px" Visible="false" Enabled="false"></cc1:ucTextbox>
                                <asp:HiddenField runat="server" ID="hdnsiteid" />
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
            </div>
            <cc1:ucPanel ID="pnlVendorsQuery" runat="server" CssClass="fieldset-form" GroupingText="VendorsQuery">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                    <tr>
                        <td style="font-weight: bold; width: 19%">
                            <cc1:ucLabel ID="lblRaisedOn" runat="server" Text="Raised on"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="width: 30%">
                            <asp:Literal ID="ltRaisedOn" runat="server" ></asp:Literal>
                        </td>
                        <td style="font-weight: bold; width: 19%">
                            <cc1:ucLabel ID="lblRaisedBy" runat="server" Text="Raised by"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="width: 30%">
                            <asp:Literal ID="ltRaisedBy" runat="server" ></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <cc1:ucLabel ID="lblContactDetail" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td>
                            <asp:Literal ID="ltContactDetail" runat="server" ></asp:Literal>
                        </td>
                        <td style="font-weight: bold">
                            <cc1:ucLabel ID="lblPhoneNumber" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td>
                            <asp:Literal ID="ltPhoneNumber" runat="server" ></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="font-weight: bold">
                            <cc1:ucLabel ID="lblQuery" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="font-weight: bold">
                            <cc1:ucTextbox ID="txtQuery" CssClass="inputbox textarea" runat="server" Height="70px"
                                onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Width="98%"
                                ReadOnly="true"></cc1:ucTextbox>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlStockPlannerResponse" runat="server" CssClass="fieldset-form"
                GroupingText="StockPlannerResponse">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                    <tr>
                        <td colspan="3" style="font-weight: bold">
                            <cc1:ucLabel ID="lblMsgFullDecline" runat="server" CssClass="action-required-heading"
                                Style="text-align: center;"></cc1:ucLabel>
                            <cc1:ucLabel ID="lblMsgPartailDecline" runat="server" CssClass="action-required-heading"
                                Style="text-align: center;"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="left">
                                <tr>
                                    <td style="font-weight: bold" width="49%">
                                        <cc1:ucLabel ID="lblOrignalPenaltyValueMsg" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold" width="1%">
                                        :
                                    </td>
                                    <td style="font-weight: bold" width="50%">
                                        <cc1:ucTextbox runat="server" ID="txtOriginalPenalty" Width="100px" ReadOnly="true"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblVendorDisputedMsg" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        :
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucTextbox runat="server" ID="txtVendorDisputedValue" Width="100px" ReadOnly="true"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblSuggestedPenaltyValueMsg" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        :
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucTextbox runat="server" ID="txtSuggestedPenaltyCharge" Width="100px" ReadOnly="true"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="font-weight: bold">
                                        <cc1:ucLabel ID="lblStockPlannerComment" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <cc1:ucTextbox ID="txtStockPlannerComment" CssClass="inputbox textarea" runat="server"
                                            Height="70px" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine"
                                            Width="98%" ReadOnly="true"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlConfirmAction" runat="server" CssClass="fieldset-form" GroupingText="ConfirmAction">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                    <tr>
                        <td colspan="5" style="font-weight: bold">
                            <cc1:ucLabel ID="lblConfirmNextTextMsg" runat="server" CssClass="action-required-heading"
                                isRequired="true" Style="text-align: center;"></cc1:ucLabel>
                            <cc1:ucLabel ID="lblVendorNotRepliedMsg" runat="server" CssClass="action-required-heading"
                                isRequired="true" Style="text-align: center;"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr id="trPenaltyValue" >
                        <td colspan="5">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="left">
                                <tr>
                                    <td style="font-weight: bold" width="10%">
                                        <cc1:ucLabel ID="lblPenaltyValueNew" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold" width="1%">
                                        :
                                    </td>
                                    <td style="font-weight: bold" width="81%">
                                        <cc1:ucTextbox runat="server" ID="txtPenaltyValue" Width="100px" Enabled="false" ></cc1:ucTextbox>
                                    </td>
                                </tr>
                              </table>
                        </td>
                    </tr>
                    <tr style="width:100%">
                        <td style="width: 18%">
                            <cc1:ucRadioButton ID="rdoNoPenaltyAppliedNew" runat="server" GroupName="MediatorAction" />
                        </td>
                        <td style="width: 18%">
                            <cc1:ucRadioButton ID="rdoPenaltytobeAppliedNew" runat="server" GroupName="MediatorAction" />
                        </td>
                        <td style="width: 14%">
                            <cc1:ucRadioButton ID="rdoAlternate" runat="server" GroupName="MediatorAction" />
                        </td>
                        <td style="width: 18%">
                            <cc1:ucRadioButton ID="rdoReviewedPending" runat="server" GroupName="MediatorAction" />
                        </td>
                        <td style="width: 32%">
                          
                        </td>
                    </tr>
                    <tr id="trPenalty">
                        <td colspan="5">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="left">
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblpenaltychargetobeapplied" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        :
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucTextbox runat="server" ID="txtPenaltyChargeImposed" Width="100px" onkeyup="AllowDecimalOnly(this);"
                                            MaxLength="10" onblur="CalculateRevisedPenalty();"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold" width="29%">
                                        <cc1:ucLabel ID="lblPenaltyChargeAgreedwith" runat="server"></cc1:ucLabel><br />
                                        <cc1:ucLabel ID="lblConfirmVendorPoint" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold" width="1%">
                                        :
                                    </td>
                                    <td style="font-weight: bold" width="30%">
                                        <cc1:ucTextbox runat="server" ID="txtPenaltyChargeAgreedWith" Width="200px" MaxLength="50"></cc1:ucTextbox>
                                    </td>
                                    <td style="font-weight: bold" width="14%">
                                        <cc1:ucLabel ID="lblDateTime" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold" width="1%">
                                        :
                                    </td>
                                    <td style="font-weight: bold" width="25%">
                                        <cc2:ucDate ID="txtAgreedPenaltyDate" runat="server" AutoPostBack="false" />
                                        <cc1:ucTextbox ID="txtAgreedPenaltyTime" runat="server" Width="40px" MaxLength="5"></cc1:ucTextbox>
                                        <cc1:ucLabel ID="lblTimeFormat" runat="server"></cc1:ucLabel>
                                        <asp:RegularExpressionValidator ID="revValidTime" runat="server" ValidationGroup="ValidSPAction"
                                            ControlToValidate="txtAgreedPenaltyTime" ValidationExpression="^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$"
                                            Display="None"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                     <tr id="trAlternateDetails">
                        <td colspan="5">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="left">
                                <tr>
                                     <td style="font-weight: bold" width="29%">
                                        <cc1:ucLabel ID="lblPenaltyChargeAgreedwith_1" runat="server"></cc1:ucLabel><br />
                                        <cc1:ucLabel ID="lblConfirmVendorPoint_1" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold" width="1%">
                                        :
                                    </td>
                                    <td style="font-weight: bold" width="70%">
                                        <cc1:ucTextbox runat="server" ID="txtPenaltyChargeAgreedWith_1" Width="200px" MaxLength="50"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                            </td>
                     </tr>
                    <tr id="trComments">
                        <td colspan="5">
                            <table width="100%" cellspacing="5" cellpadding="0" border="0" align="left">
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblComment" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucTextbox ID="txtMediatorComment" CssClass="inputbox textarea" runat="server"
                                            Height="70px" onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine"
                                            Width="98%"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="5">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSaveMediationAction_Click"
                                    ValidationGroup="ValidSPAction" />
                                <asp:CustomValidator ID="cusvPleaseSelectOption" runat="server" Text="Please select Option."
                                    ClientValidationFunction="SPActonReqired" Display="None" ValidationGroup="ValidSPAction">
                                </asp:CustomValidator>
                                <asp:ValidationSummary ID="VSValidDispute" runat="server" ValidationGroup="ValidSPAction"
                                    ShowSummary="false" ShowMessageBox="true" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>


             <asp:Button ID="btnQueryMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlQueryMsg" runat="server" TargetControlID="btnQueryMsg"
                PopupControlID="pnlQueryMsg" BackgroundCssClass="modalBackground" BehaviorID="QueryMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlQueryMsg" runat="server" Style="display: none;">
              <cc1:ucPanel ID="pnlWarningInfo" runat="server" CssClass="fieldset-form">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblMediationEmail" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <cc1:ucButton ID="btnOk_2"  runat="server" Text="OK" CssClass="button"
                                    OnClick="btnAcceptOk_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
              </cc1:ucPanel>
            </asp:Panel>
        </div>
    </div>
</asp:Content> 

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using WebUtilities;
using BaseControlLibrary;
using System.Collections;
using System.Data;

public partial class VendorViewOTIF : CommonPage
{
    protected void Page_InIt(object sender, EventArgs e)
    {

        ucExportToExcel1.CurrentPage = this;
        ucExportToExcel1.FileName = "VendorViewOTIF";
        ucExportToExcel1.GridViewControl = gvVendorView;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            pnlSearchScreen.Visible = true;
            pnlPotentialOutput.Visible = false;
            ucExportToExcel1.Visible = false;
            pager1.PageSize = 200;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            string PreviousMonth = DateTime.Now.AddMonths(-1).ToString("MM");
            drpMonthFrom.SelectedValue = PreviousMonth;

            string CurrentMonth = DateTime.Now.ToString("MM");
            drpMonthTo.SelectedValue = CurrentMonth;

            FillControls.FillDropDown(ref drpYearTo, GetLastYears(), "Year", "Year");
            FillControls.FillDropDown(ref drpYearFrom, GetLastYears(), "Year", "Year");

            if (CurrentMonth.Equals("01"))
            {
                int currentYear = DateTime.Now.Year;
                drpYearFrom.SelectedValue = (currentYear - 1).ToString();
            }
        }
    }

    private DataTable GetLastYears()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Year");
        int iStartYear = DateTime.Now.Year;
        for (int i = iStartYear; i >= iStartYear - 2; i--)
        {
            if (i >= 2014)
            {
                DataRow dr = dt.NewRow();
                dr["Year"] = i;
                dt.Rows.Add(dr);
            }
        }
        return dt;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindVendorPanalty();
    }

    private void BindVendorPanalty(int Page = 1)
    {

        BackOrderBE oBackOrderBE = new BackOrderBE();

        List<BackOrderBE> lstoBackOrderBE = new List<BackOrderBE>();

        oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
        oBackOrderBE.PurchaseOrder.Purchase_order = "42116";
        oBackOrderBE.DateBOIncurred = DateTime.Now.Date;
        oBackOrderBE.OTIFPenaltyRate = 98.78;
        oBackOrderBE.PurchaseOrderValue = "432.00";
        oBackOrderBE.OTIFPenaltyCharges = 5.12;
        oBackOrderBE.PurchaseOrder.StockPlannerName = "Neil McLachlan";
        oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oBackOrderBE.Vendor.Vendor_No = "025";
        oBackOrderBE.Vendor.VendorName = "ACCO";
        oBackOrderBE.PotentialPenaltyCharge = Convert.ToDecimal(43.25);
        lstoBackOrderBE.Add(oBackOrderBE);

        oBackOrderBE = new BackOrderBE();
        oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
        oBackOrderBE.PurchaseOrder.Purchase_order = "56772";
        oBackOrderBE.DateBOIncurred = DateTime.Now.Date;
        oBackOrderBE.OTIFPenaltyRate = 18.78;
        oBackOrderBE.PurchaseOrderValue = "54.00";
        oBackOrderBE.OTIFPenaltyCharges = 6.34;
        oBackOrderBE.PurchaseOrder.StockPlannerName = "Hilroy Benjamin";
        oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oBackOrderBE.Vendor.Vendor_No = "025";
        oBackOrderBE.Vendor.VendorName = "ACCO";
        oBackOrderBE.PotentialPenaltyCharge = Convert.ToDecimal(493.09);
        lstoBackOrderBE.Add(oBackOrderBE);

        oBackOrderBE = new BackOrderBE();
        oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
        oBackOrderBE.PurchaseOrder.Purchase_order = "678554";
        oBackOrderBE.DateBOIncurred = DateTime.Now.Date;
        oBackOrderBE.OTIFPenaltyRate = 98.74;
        oBackOrderBE.PurchaseOrderValue = "44.00";
        oBackOrderBE.OTIFPenaltyCharges = 4.77;
        oBackOrderBE.PurchaseOrder.StockPlannerName = "Natasha Todd";
        oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        oBackOrderBE.Vendor.Vendor_No = "025";
        oBackOrderBE.Vendor.VendorName = "ACCO";
        oBackOrderBE.PotentialPenaltyCharge = Convert.ToDecimal(93.95);
        lstoBackOrderBE.Add(oBackOrderBE);

        if (lstoBackOrderBE != null && lstoBackOrderBE.Count > 0)
        {
            ucExportToExcel1.Visible = true;
            gvVendorView.DataSource = lstoBackOrderBE;
            gvVendorView.DataBind();
        }

        pnlSearchScreen.Visible = false;
        pnlPotentialOutput.Visible = true;
    }

    protected void gvVendorView_PageIndexChanging(object sender, GridViewPageEventArgs e) { }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //this.ClearCriteria();
        pnlSearchScreen.Visible = true;
        pnlPotentialOutput.Visible = false;
        ucExportToExcel1.Visible = false;
    }



    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindVendorPanalty(currnetPageIndx);
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        mdComment.Show();
    }

    protected void btnProceed_Click(object sender, EventArgs e) { }

    protected void btnBack_1_Click(object sender, EventArgs e) { }
}
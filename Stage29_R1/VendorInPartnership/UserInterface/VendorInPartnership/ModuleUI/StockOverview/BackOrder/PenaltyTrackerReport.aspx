﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="PenaltyTrackerReport.aspx.cs" Inherits="ModuleUI_StockOverview_BackOrder_PenaltyTrackerReport" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectPO.ascx" TagName="MultiSelectPO"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectItemClassification.ascx" TagName="MultiSelectItemClassification"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectRMSCategory.ascx" TagName="MultiSelectCategory"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlannerGrouping.ascx" TagName="MultiSelectStockPlannerGrouping"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../../Scripts/jquery.tooltip.min.js" type="text/javascript"></script>
    <style type="text/css">
        #tooltip
        {
            position: absolute;
            z-index: 3000;
            border: 1px solid #111;
            background-color: #FEE18D;
            padding: 5px;
            opacity: 1.00;
        }
        #tooltip h3, #tooltip div
        {
            margin: 0;
        }
           .PopupClass
        {
            width:20%;
        }
        
        
        
    </style>
    <script type="text/javascript">
        $(function () {
            InitializeToolTip();
            function InitializeToolTip() {
                $(".gridViewToolTip1").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    extraClass: "PopupClass",      
                    bodyHandler: function () {
                        return $("#tooltip1").html();
                    },
                    showURL: false
                });

                $(".gridViewToolTip2").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    extraClass: "PopupClass",      
                    bodyHandler: function () {
                        return $("#tooltip2").html();
                    },
                    showURL: false
                });

                $(".gridViewToolTip3").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    extraClass: "PopupClass",      
                    bodyHandler: function () {
                        return $("#tooltip3").html();
                    },
                    showURL: false
                });

                $(".gridViewToolTip4").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    extraClass: "PopupClass",      
                    bodyHandler: function () {
                        return $("#tooltip4").html();
                    },
                    showURL: false
                });

                $(".gridViewToolTip5").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    extraClass: "PopupClass",      
                    bodyHandler: function () {
                        return $("#tooltip5").html();
                    },
                    showURL: false
                });
                $(".gridViewToolTip6").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    extraClass: "PopupClass",      
                    bodyHandler: function () {
                        return $("#tooltip6").html();
                    },
                    showURL: false
                });

                var gridviewcontrolID;
                $("#<%=ucGridView1.ClientID %> tr td").mouseenter(function () {
                    var iRowIndex = $(this).closest("tr").prevAll("tr").length;
                    iRowIndex = parseInt(iRowIndex);
                    if (parseInt(iRowIndex) < 10) {
                        iRowIndex = "0" + iRowIndex;
                    }
                    gridviewcontrolID = "ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex;

                });

                $("#<%=ucGridView1.ClientID %> tr th").mouseenter(function () {
                    var iRowIndex = $(this).closest("tr").prevAll("tr").length;
                    if (parseInt(iRowIndex) == 1) {
                        iRowIndex = parseInt($('#<%=hdnGridcount.ClientID %>').val()) + 1;
                        if (parseInt(iRowIndex) < 10) {
                            iRowIndex = "0" + iRowIndex;
                        }
                        gridviewcontrolID = "ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex;
                    }
                });

                $(".hpPotentialPenaltyCharge_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                       // debugger;
                        var id = gridviewcontrolID + "_hpPotentialPenaltyCharge_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });


                $(".hpIAccepted_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpIAccepted_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpIDeclined_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpIDeclined_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpIDeclined_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpIDeclined_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpIPending_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpIPending_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpVAccepted_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpVAccepted_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpVDeclined_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpVDeclined_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpVPending_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpVPending_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpDDeclined_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpDDeclined_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpDAccepted_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpDAccepted_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpDPending_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpDPending_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpMAccepted_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpMAccepted_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });


                $(".hpMDeclined_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpMDeclined_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpMPending_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpMPending_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpRAccepted_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpRAccepted_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpRDeclined_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpRDeclined_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });

                $(".hpRPending_ToolTip").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    bodyHandler: function () {
                        var id = gridviewcontrolID + "_hpRPending_Popup";
                        return $('#' + id).html();
                    },
                    showURL: false
                });
            }


        });
    </script>
    <asp:ScriptManager runat="server" />
    <h2>
        <cc1:ucLabel ID="lblPenaltyTrackerReport" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <%--   <asp:UpdatePanel ID="pnlUP1" runat="server">
    <ContentTemplate>--%>
        <div class="formbox">
            <cc1:ucPanel ID="UcDataPanel" runat="server">
                <%--              <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>--%>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 89%">
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblInventoryGroup" runat="server">
                            </cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Uclbl8" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlannerGrouping runat="server" ID="multiSelectStockPlannerGrouping" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr style="height: 3em;">
                                    <td style="font-weight: bold; width: 12%">
                                        <cc1:ucLabel ID="lblYear" runat="server" Text="Year"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 6em;">
                                        <cc1:ucDropdownList ID="ddlYear" runat="server" Width="60px">
                                        </cc1:ucDropdownList>
                                    </td>
                        </td>
                    </tr>
                </table>
                </td> </tr>
                <tr>
                    <td align="right" colspan="3">
                        <div class="button-row">
                            <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
                                OnClick="btnGenerateReport_Click" />
                        </div>
                    </td>
                </tr>
                </table>
            </cc1:ucPanel>
           <%-- <div id="tooltip1" style="display: none;">
            <table width="100%" cellspacing="10">
                <tr>
                    <td colspan="4">
                          <cc1:ucLabel runat="server" ID="lblStage1Heading" ></cc1:ucLabel>      
                    </td>
                    
                  </tr>
                  <tr>                 
                    <td style="width:15%">
                        <cc1:ucLabel runat="server" ID="lblPotentialCharge" ></cc1:ucLabel>   
                    </td>
                    <td style="width:5%">  
                                      
                    </td>
                    <td style="width:80%">
                        <cc1:ucLabel runat="server" ID="lblPotentialChargeDescription" ></cc1:ucLabel>  
                    </td>
                </tr>
                
                </table>
               
            </div>
            <div id="tooltip2" style="display: none;">
            <table width="100%" cellspacing="10">
                <tr>
                    <td colspan="4">
                          <cc1:ucLabel runat="server" ID="lblStage2Heading" ></cc1:ucLabel>      
                    </td>
                  </tr>
                  <tr>                 
                    <td style="width:20%">
                        <cc1:ucLabel runat="server" ID="lblAccepted" ></cc1:ucLabel>   
                    </td>
                    <td style="width:5%">  
                                      
                    </td >
                    <td style="width:75%">
                        <cc1:ucLabel runat="server" ID="lblSumAcceptedBySP" ></cc1:ucLabel>  
                    </td>
                </tr>
                <tr>
                   <td>
                        <cc1:ucLabel runat="server" ID="lblDeclined" ></cc1:ucLabel>   
                    </td>
                    <td>  
                                      
                    </td>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblSumDeclinedBySP" ></cc1:ucLabel>  
                    </td>
                </tr>
                <tr>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblPendingReview" ></cc1:ucLabel>   
                    </td>
                    <td>  
                                      
                    </td>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblSumNotActionedBySP" ></cc1:ucLabel>  
                    </td>
                </tr>
                </table>
               
            </div>
            <div id="tooltip3" style="display: none;">
            <table width="100%" cellspacing="10">
                <tr>
                    <td  colspan="4">
                          <cc1:ucLabel runat="server" ID="lblStage3Heading" ></cc1:ucLabel>      
                    </td>
                  </tr>
                  <tr>                 
                    <td style="width:20%">
                        <cc1:ucLabel runat="server" ID="lblAccepted_1" ></cc1:ucLabel>   
                    </td>
                    <td style="width:5%">  
                                      
                    </td>
                    <td style="width:75%">
                        <cc1:ucLabel runat="server" ID="lblSumAcceptedByVendor" ></cc1:ucLabel>  
                    </td>
                </tr>
                <tr>
                   <td>
                        <cc1:ucLabel runat="server" ID="lblDisputed" ></cc1:ucLabel>   
                    </td>
                    <td>  
                                      
                    </td>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblSumDisputedByVendor" ></cc1:ucLabel>  
                    </td>
                </tr>
                <tr>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblPendingReview_1" ></cc1:ucLabel>   
                    </td>
                    <td>  
                                      
                    </td>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblSumNotActionedByVendor" ></cc1:ucLabel>  
                    </td>
                </tr>
                </table>
               
            </div>
            <div id="tooltip4" style="display: none;">
             <table width="100%" cellspacing="10">
                <tr>
                    <td  colspan="4">
                          <cc1:ucLabel runat="server" ID="lblStage4Heading" ></cc1:ucLabel>      
                    </td>
                  </tr>
                  <tr>                 
                    <td style="width:20%">
                        <cc1:ucLabel runat="server" ID="lblSentToMediator" ></cc1:ucLabel>   
                    </td>
                    <td style="width:5%">  
                                      
                    </td>
                    <td style="width:75%">
                        <cc1:ucLabel runat="server" ID="lblSumSenttoMediator" ></cc1:ucLabel>  
                    </td>
                </tr>
                <tr>
                   <td>
                        <cc1:ucLabel runat="server" ID="lblDeclined_1" ></cc1:ucLabel>   
                    </td>
                    <td>  
                                      
                    </td>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblSumDeclinedBySP_1" ></cc1:ucLabel>  
                    </td>
                </tr>
                <tr>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblPendingReview_2" ></cc1:ucLabel>   
                    </td>
                    <td>  
                                      
                    </td>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblSumNotActionedBySP_1" ></cc1:ucLabel>  
                    </td>
                </tr>
                </table>
               
            </div>
            <div id="tooltip5" style="display: none;">
             <table width="100%" cellspacing="10">
                <tr>
                    <td colspan="4">
                          <cc1:ucLabel runat="server" ID="lblStage5Heading" ></cc1:ucLabel>      
                    </td>
                  </tr>
                  <tr>                 
                    <td style="width:20%">
                        <cc1:ucLabel runat="server" ID="lblAccepted_2" ></cc1:ucLabel>   
                    </td>
                    <td style="width:5%">  
                                      
                    </td >
                    <td style="width:75%">
                        <cc1:ucLabel runat="server" ID="lblSumAcceptedByMediator" ></cc1:ucLabel>  
                    </td>
                </tr>
                <tr>
                   <td>
                        <cc1:ucLabel runat="server" ID="lblDeclined_2" ></cc1:ucLabel>   
                    </td>
                    <td>  
                                      
                    </td>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblSumDeclinedByMediator" ></cc1:ucLabel>  
                    </td>
                </tr>
                <tr>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblPendingReview_3" ></cc1:ucLabel>   
                    </td>
                    <td>  
                                      
                    </td>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblSumNotActionedByMediator" ></cc1:ucLabel>  
                    </td>
                </tr>
                </table>
                
            </div>
            <div id="tooltip6" style="display: none;">
             <table width="100%" cellspacing="10">
                <tr>
                    <td colspan="4">
                          <cc1:ucLabel runat="server" ID="lblStage6Heading" ></cc1:ucLabel>      
                    </td>
                  </tr>
                  <tr>                 
                    <td style="width:20%">
                        <cc1:ucLabel runat="server" ID="lblAccepted_3" ></cc1:ucLabel>   
                    </td>
                    <td style="width:5%">  
                                      
                    </td>
                    <td style="width:75%">
                        <cc1:ucLabel runat="server" ID="lblSumofAcceptedResult" ></cc1:ucLabel>  
                    </td>
                </tr>
                <tr>
                   <td>
                        <cc1:ucLabel runat="server" ID="lblDeclined_3" ></cc1:ucLabel>   
                    </td>
                    <td>  
                                      
                    </td>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblSumofDeclinedResult" ></cc1:ucLabel>  
                    </td>
                </tr>
                <tr>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblPendingReview_4" ></cc1:ucLabel>   
                    </td>
                    <td>  
                                      
                    </td>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblSumofPendingResult" ></cc1:ucLabel>  
                    </td>
                </tr>
                </table>
               
            </div>--%>

          <div id="tooltip1" style="display: none;">
            <table width="100%" cellspacing="10">                
                  <tr>                 
                    <td style="width:8%">
                         <cc1:ucLabel runat="server" ID="lblStage1" ></cc1:ucLabel>  
                    </td>
                    <td style="width:2%">  
                                      
                    </td>
                    <td style="width:90%">
                        <cc1:ucLabel runat="server" ID="lblStage1Description" ></cc1:ucLabel>  
                    </td>
                </tr>
                
                </table>
               
            </div>
            <div id="tooltip2" style="display: none;">
            <table width="100%" cellspacing="10">
                <tr>                 
                    <td style="width:8%">
                         <cc1:ucLabel runat="server" ID="lblStage2" ></cc1:ucLabel>  
                    </td>
                    <td style="width:2%">  
                                      
                    </td>
                    <td style="width:90%">
                        <cc1:ucLabel runat="server" ID="lblStage2Description" ></cc1:ucLabel>  
                    </td>
                </tr>
                </table>
               
            </div>
            <div id="tooltip3" style="display: none;">
            <table width="100%" cellspacing="10">
               <tr>                 
                    <td style="width:8%">
                         <cc1:ucLabel runat="server" ID="lblStage3" ></cc1:ucLabel>  
                    </td>
                    <td style="width:2%">  
                                      
                    </td>
                    <td style="width:90%">
                        <cc1:ucLabel runat="server" ID="lblStage3Description" ></cc1:ucLabel>  
                    </td>
                </tr>
                </table>
               
            </div>
            <div id="tooltip4" style="display: none;">
             <table width="100%" cellspacing="10">
                <tr>                 
                    <td style="width:8%">
                         <cc1:ucLabel runat="server" ID="lblStage4" ></cc1:ucLabel>  
                    </td>
                    <td style="width:2%">  
                                      
                    </td>
                    <td style="width:90%">
                        <cc1:ucLabel runat="server" ID="lblStage4Description" ></cc1:ucLabel>  
                    </td>
                </tr>
                </table>
               
            </div>
            <div id="tooltip5" style="display: none;">
             <table width="100%" cellspacing="10">
               <tr>                 
                    <td style="width:8%">
                         <cc1:ucLabel runat="server" ID="lblStage5" ></cc1:ucLabel>  
                    </td>
                    <td style="width:2%">  
                                      
                    </td>
                    <td style="width:90%">
                        <cc1:ucLabel runat="server" ID="lblStage5Description" ></cc1:ucLabel>  
                    </td>
                </tr>
                </table>
                
            </div>
            <div id="tooltip6" style="display: none;">
             <table width="100%" cellspacing="10">
                <tr>
                    <td colspan="4">
                          <cc1:ucLabel runat="server" ID="lblStage6Heading" ></cc1:ucLabel>      
                    </td>
                  </tr>
                  <tr>                 
                    <td style="width:20%">
                        <cc1:ucLabel runat="server" ID="lblAccepted_3" ></cc1:ucLabel>   
                    </td>
                    <td style="width:5%">  
                                      
                    </td>
                    <td style="width:75%">
                        <cc1:ucLabel runat="server" ID="lblSumofAcceptedResult" ></cc1:ucLabel>  
                    </td>
                </tr>
                <tr>
                   <td>
                        <cc1:ucLabel runat="server" ID="lblDeclined_3" ></cc1:ucLabel>   
                    </td>
                    <td>  
                                      
                    </td>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblSumofDeclinedResult" ></cc1:ucLabel>  
                    </td>
                </tr>
                <tr>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblPendingReview_4" ></cc1:ucLabel>   
                    </td>
                    <td>  
                                      
                    </td>
                    <td>
                        <cc1:ucLabel runat="server" ID="lblSumofPendingResult" ></cc1:ucLabel>  
                    </td>
                </tr>
                </table>               
            </div>



            <div class="button-row">
                <cc1:ucExportToExcel ID="btnExportToExcel" runat="server" Visible="false" />
            </div>
            <asp:HiddenField ID="hdnGridcount" runat="server"/>
            <asp:GridView ID="ucGridView1" runat="server" AutoGenerateColumns="false" CssClass="grid gvclass"
                CellPadding="4" Width="100%" ViewStateMode="Enabled" OnDataBound="ucGridView1_DataBound"
                CellSpacing="4" Font-Size="9px" OnRowDataBound="ucGridView1_RowDataBound" UseAccessibleHeader="true">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" Font-Size="9px"></AlternatingRowStyle>
                <EmptyDataTemplate>
                    No Record Found</EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                        <ItemTemplate>
                            <asp:Label ID="hpDate" runat="server" Text='<%#Eval("Date") %>'></asp:Label>
                            <asp:Label ID="lblMonth" runat="server" Visible="false" Text='<%#Eval("Month") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:HyperLink ID="hpHPotentialPenaltyCharge" runat="server" Text="Potential Penalty Charge"
                                > </asp:HyperLink>
                        </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px"  CssClass="hpPotentialPenaltyCharge_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpPotentialPenaltyCharge" runat="server" Text='<%#Eval("PotentialPenaltyCharge") %>'></asp:HyperLink>
                               <div id="hpPotentialPenaltyCharge_Popup" runat="server" style="display: none;">
                                <table id="tblhpPotentialPenaltyCharge" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                             <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpPotentialPenaltyCharge" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel> </b>
                                        </td>
                                        <td>
                                           <b>  - </b>
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpPotentialPenaltyCharge" runat="server" Text='<%#Eval("PotentialPenaltyCharge")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b>  <cc1:ucLabel ID="lblCountofBOTexthpPotentialPenaltyCharge" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel> </b>
                                        </td>
                                        <td>
                                            <b>  - </b>
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpPotentialPenaltyCharge" runat="server" Text='<%#Eval("PotentialBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpPotentialPenaltyCharge" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel> </b>
                                        </td>
                                        <td>
                                           <b>  - </b>
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpPotentialPenaltyCharge" runat="server" Text='<%#Eval("PotentialOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpPotentialPenaltyCharge" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel> </b>
                                        </td>
                                        <td>
                                           <b>  - </b>
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpPotentialPenaltyCharge" runat="server" Text='<%#Eval("PotentialVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpPotentialPenaltyCharge_ToolTip"/>
                    </asp:TemplateField>
                    <asp:TemplateField >
                        <HeaderTemplate>
                            <asp:HyperLink ID="hpHIAccepted" runat="server" Text="Accepted" > </asp:HyperLink>                       
                        </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpIAccepted_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpIAccepted" runat="server" Text='<%#Eval("IAccepted") %>'></asp:HyperLink>
                            <div id="hpIAccepted_Popup" runat="server" style="display: none;">
                                <table id="tblhpIAccepted" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                             <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpIAccepted" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel> </b>
                                        </td>
                                        <td>
                                           <b>  - </b>
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpIAccepted" runat="server" Text='<%#Eval("IAccepted")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b>  <cc1:ucLabel ID="lblCountofBOTexthpIAccepted" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel> </b>
                                        </td>
                                        <td>
                                            <b>  - </b>
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpIAccepted" runat="server" Text='<%#Eval("IAcceptedBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpIAccepted" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel> </b>
                                        </td>
                                        <td>
                                           <b>  - </b>
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpIAccepted" runat="server" Text='<%#Eval("IAcceptedOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsText" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel> </b>
                                        </td>
                                        <td>
                                           <b>  - </b>
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpIAccepted" runat="server" Text='<%#Eval("IAcceptedVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpIAccepted_ToolTip" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:HyperLink ID="hpHIDeclined" runat="server" Text="Declined" > </asp:HyperLink>                       
                        </HeaderTemplate>
 
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpIDeclined_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpIDeclined" runat="server" Text='<%#Eval("IDeclined") %>'></asp:HyperLink>
                            <div id="hpIDeclined_Popup" runat="server" style="display: none;">
                                <table id="tblIDeclined" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                         <b>  <cc1:ucLabel ID="lblYTDPotentialValueTextIDeclined" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel> </b> 
                                        </td>
                                        <td>
                                          <b>   - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValueIDeclined" runat="server" Text='<%#Eval("IDeclined")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b>  <cc1:ucLabel ID="lblCountofBOTextIDeclined" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel> </b> 
                                        </td>
                                        <td>
                                          <b>   - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOIDeclined" runat="server" Text='<%#Eval("IDeclinedBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTextIDeclined" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel> </b> 
                                        </td>
                                        <td>
                                          <b>   - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUIDeclined" runat="server" Text='<%#Eval("IDeclinedOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                         <b>    <cc1:ucLabel ID="lblCountofVendorsTextIDeclined" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>   - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorsIDeclined" runat="server" Text='<%#Eval("IDeclinedVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpIDeclined_ToolTip"  />
                    </asp:TemplateField>
                    <asp:TemplateField>
                     <HeaderTemplate>
                            <asp:HyperLink ID="hpHIPending" runat="server" Text="Pending Review" > </asp:HyperLink>                       
                        </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpIPending_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpIPending" runat="server" Text='<%#Eval("IPending") %>'></asp:HyperLink>
                              <div id="hpIPending_Popup" runat="server" style="display: none;">
                                <table id="tblhpIPending" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                           <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpIPending" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                          <b>  - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpIPending" runat="server" Text='<%#Eval("IPending")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b> <cc1:ucLabel ID="lblCountofBOTexthpIPending" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpIPending" runat="server" Text='<%#Eval("IPendingBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpIPending" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                            -
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpIPending" runat="server" Text='<%#Eval("IPendingOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpIPending" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpIPending" runat="server" Text='<%#Eval("IPendingVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpIPending_ToolTip"/>
                    </asp:TemplateField>
                    <asp:TemplateField>
                      <HeaderTemplate>
                            <asp:HyperLink ID="hpHVAccepted" runat="server" Text="Accepted"  CssClass="hpVAccepted_ToolTip"> </asp:HyperLink>                       
                      </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                        <ItemTemplate>
                            <asp:HyperLink ID="hpVAccepted" runat="server" Text='<%#Eval("VAccepted") %>'></asp:HyperLink>
                                <div id="hpVAccepted_Popup"  runat="server" style="display: none;">
                                <table id="tblhpVAccepted" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                           <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpVAccepted" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                          <b>  - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpVAccepted" runat="server" Text='<%#Eval("VAccepted")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b> <cc1:ucLabel ID="lblCountofBOTexthpVAccepted" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpVAccepted" runat="server" Text='<%#Eval("VAcceptedBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpVAccepted" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                            -
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpVAccepted" runat="server" Text='<%#Eval("VAcceptedOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpVAccepted" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpVAccepted" runat="server" Text='<%#Eval("VAcceptedVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpVAccepted_ToolTip"/>
                    </asp:TemplateField>
                    <asp:TemplateField>
                     <HeaderTemplate>
                            <asp:HyperLink ID="hpHVDeclined" runat="server" Text="Disputed" > </asp:HyperLink>                       
                      </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpVDeclined_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpVDeclined" runat="server" Text='<%#Eval("VDeclined") %>'></asp:HyperLink>
                            <div id="hpVDeclined_Popup"  runat="server" style="display: none;">
                                <table id="tblhpVDeclined" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                           <b><cc1:ucLabel ID="lblYTDPotentialValueTexthphpVDeclined" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                          <b>  - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpVDeclined" runat="server" Text='<%#Eval("VDeclined")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b> <cc1:ucLabel ID="lblCountofBOTexthpVDeclined" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpVDeclined" runat="server" Text='<%#Eval("VDeclinedBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpVDeclined" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                            -
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpVDeclined" runat="server" Text='<%#Eval("VDeclinedOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpVDeclined" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpVDeclined" runat="server" Text='<%#Eval("VDeclinedVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpVDeclined_ToolTip"/>
                    </asp:TemplateField>
                    <asp:TemplateField>
                      <HeaderTemplate>
                            <asp:HyperLink ID="hpHVPending" runat="server" Text="Pending Review" > </asp:HyperLink>                       
                      </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpVPending_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpVPending" runat="server" Text='<%#Eval("VPending") %>'></asp:HyperLink>
                               <div id="hpVPending_Popup"  runat="server" style="display: none;">
                                <table id="tblhpVPending" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                           <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpVPending" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                          <b>  - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpVPending" runat="server" Text='<%#Eval("VPending")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b> <cc1:ucLabel ID="lblCountofBOTexthpVPending" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpVPending" runat="server" Text='<%#Eval("VPendingBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpVPending" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                            -
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpVPending" runat="server" Text='<%#Eval("VPendingOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpVPending" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpVPending" runat="server" Text='<%#Eval("VPendingVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpVPending_ToolTip"/>
                    </asp:TemplateField>
                    <asp:TemplateField>
                    <HeaderTemplate>
                            <asp:HyperLink ID="hpHDDeclined" runat="server" Text="Sent to Mediator" > </asp:HyperLink>                       
                      </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpDDeclined_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpDDeclined" runat="server" Text='<%#Eval("DDeclined") %>'></asp:HyperLink>
                            <div id="hpDDeclined_Popup"  runat="server" style="display: none;">
                                <table id="tblhpDDeclined" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                           <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpDDeclined" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                          <b>  - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpDDeclined" runat="server" Text='<%#Eval("DDeclined")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b> <cc1:ucLabel ID="lblCountofBOTexthpDDeclined" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpDDeclined" runat="server" Text='<%#Eval("DDeclinedBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpDDeclined" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                            -
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpDDeclined" runat="server" Text='<%#Eval("DDeclinedOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpDDeclined" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpDDeclined" runat="server" Text='<%#Eval("DDeclinedVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpDDeclined_ToolTip"/>
                    </asp:TemplateField>
                    <asp:TemplateField>
                     <HeaderTemplate>
                            <asp:HyperLink ID="hpHDAccepted" runat="server" Text="Declined" > </asp:HyperLink>                       
                      </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpDAccepted_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpDAccepted" runat="server" Text='<%#Eval("DAccepted") %>'></asp:HyperLink>
                                   <div id="hpDAccepted_Popup"  runat="server" style="display: none;">
                                <table id="tblhpDAccepted" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                           <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpDAccepted" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                          <b>  - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpDAccepted" runat="server" Text='<%#Eval("DAccepted")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b> <cc1:ucLabel ID="lblCountofBOTexthpDAccepted" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpDAccepted" runat="server" Text='<%#Eval("DAcceptedBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpDAccepted" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                            -
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpDAccepted" runat="server" Text='<%#Eval("DAcceptedOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpDAccepted" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpDAccepted" runat="server" Text='<%#Eval("DAcceptedVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpDAccepted_ToolTip"/>
                    </asp:TemplateField>
                    <asp:TemplateField>
                     <HeaderTemplate>
                            <asp:HyperLink ID="hpHDPending" runat="server" Text="Pending Review" > </asp:HyperLink>                       
                      </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpDPending_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpDPending" runat="server" Text='<%#Eval("DPending") %>'></asp:HyperLink>
                              <div id="hpDPending_Popup"  runat="server" style="display: none;">
                                <table id="tblhpDPending" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                           <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpDPending" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                          <b>  - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpDPending" runat="server" Text='<%#Eval("DPending")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b> <cc1:ucLabel ID="lblCountofBOTexthpDPending" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpDPending" runat="server" Text='<%#Eval("DPendingBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpDPending" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                            -
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpDPending" runat="server" Text='<%#Eval("DPendingOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpDPending" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpDPending" runat="server" Text='<%#Eval("DPendingVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpDPending_ToolTip"/>
                    </asp:TemplateField>
                       <asp:TemplateField>
                      <HeaderTemplate>
                            <asp:HyperLink ID="hpHMDeclined" runat="server" Text="Accepted" > </asp:HyperLink>                       
                      </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpMDeclined_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpMDeclined" runat="server" Text='<%#Eval("MDeclined") %>'></asp:HyperLink>
                           <div id="hpMDeclined_Popup"  runat="server" style="display: none;">
                                <table id="tblhpMDeclined" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                           <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpMDeclined" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                          <b>  - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpMDeclined" runat="server" Text='<%#Eval("MDeclined")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b> <cc1:ucLabel ID="lblCountofBOTexthpMDeclined" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpMDeclined" runat="server" Text='<%#Eval("MDeclinedBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpMDeclined" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                            -
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpMDeclined" runat="server" Text='<%#Eval("MDeclinedOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpMDeclined" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpMDeclined" runat="server" Text='<%#Eval("MDeclinedVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpMDeclined_ToolTip"/>
                    </asp:TemplateField>
                    <asp:TemplateField>
                    <HeaderTemplate>
                            <asp:HyperLink ID="hpHMAccepted" runat="server" Text="Declined" > </asp:HyperLink>                       
                      </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpMAccepted_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpMAccepted" runat="server" Text='<%#Eval("MAccepted") %>'></asp:HyperLink>
                            <div id="hpMAccepted_Popup"  runat="server" style="display: none;">
                                <table id="tblhpMAccepted" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                           <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpMAccepted" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                          <b>  - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpMAccepted" runat="server" Text='<%#Eval("MAccepted")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b> <cc1:ucLabel ID="lblCountofBOTexthpMAccepted" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpMAccepted" runat="server" Text='<%#Eval("MAcceptedBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpMAccepted" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                            -
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpMAccepted" runat="server" Text='<%#Eval("MAcceptedOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpMAccepted" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpMAccepted" runat="server" Text='<%#Eval("MAcceptedVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpMAccepted_ToolTip"/>
                    </asp:TemplateField>
                 
                    <asp:TemplateField>
                    <HeaderTemplate>
                            <asp:HyperLink ID="hpHMPending" runat="server" Text="Pending Review" > </asp:HyperLink>                       
                      </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpMPending_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpMPending" runat="server" Text='<%#Eval("MPending") %>'></asp:HyperLink>
                                <div id="hpMPending_Popup"  runat="server" style="display: none;">
                                <table id="tblhpMPending" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                           <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpMPending" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                          <b>  - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpMPending" runat="server" Text='<%#Eval("MPending")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b> <cc1:ucLabel ID="lblCountofBOTexthpMPending" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpMPending" runat="server" Text='<%#Eval("MPendingBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpMPending" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                            -
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpMPending" runat="server" Text='<%#Eval("MPendingOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpMPending" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpMPending" runat="server" Text='<%#Eval("MPendingVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpMPending_ToolTip"/>
                    </asp:TemplateField>
                    <asp:TemplateField>
                    <HeaderTemplate>
                            <asp:HyperLink ID="hpHRAccepted" runat="server" Text="Accepted" > </asp:HyperLink>                       
                      </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpRAccepted_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpRAccepted" runat="server" Text='<%#Eval("RAccepted") %>'></asp:HyperLink>
                               <div id="hpRAccepted_Popup"  runat="server" style="display: none;">
                                <table id="tblhpRAccepted" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                           <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpRAccepted" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                          <b>  - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpRAccepted" runat="server" Text='<%#Eval("RAccepted")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b> <cc1:ucLabel ID="lblCountofBOTexthpRAccepted" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpRAccepted" runat="server" Text='<%#Eval("RAcceptedBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpRAccepted" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                            -
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpRAccepted" runat="server" Text='<%#Eval("RAcceptedOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpRAccepted" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpRAccepted" runat="server" Text='<%#Eval("RAcceptedVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpRAccepted_ToolTip"/>
                    </asp:TemplateField>
                    <asp:TemplateField>
                     <HeaderTemplate>
                            <asp:HyperLink ID="hpHRDeclined" runat="server" Text="Declined" > </asp:HyperLink>                       
                      </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpRDeclined_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpRDeclined" runat="server" Text='<%#Eval("RDeclined") %>'></asp:HyperLink>

                              <div id="hpRDeclined_Popup"  runat="server" style="display: none;">
                                <table id="tblhpRDeclined" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                           <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpRDeclined" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                          <b>  - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpRDeclined" runat="server" Text='<%#Eval("RDeclined")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b> <cc1:ucLabel ID="lblCountofBOTexthpRDeclined" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpRDeclined" runat="server" Text='<%#Eval("RDeclinedBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpRDeclined" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                            -
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpRDeclined" runat="server" Text='<%#Eval("RDeclinedOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpRDeclined" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpRDeclined" runat="server" Text='<%#Eval("RDeclinedVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpRDeclined_ToolTip"/>
                    </asp:TemplateField>
                    <asp:TemplateField>
                    <HeaderTemplate>
                            <asp:HyperLink ID="hpHRPending" runat="server" Text="Pending" > </asp:HyperLink>                       
                      </HeaderTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" CssClass="hpRPending_ToolTip"/>
                        <ItemTemplate>
                            <asp:HyperLink ID="hpRPending" runat="server" Text='<%#Eval("RPending") %>'></asp:HyperLink>

                             <div id="hpRPending_Popup"  runat="server" style="display: none;">
                                <table id="tblhpRPending" runat="server" cellspacing="2">
                                    <tr>
                                        <td>
                                           <b><cc1:ucLabel ID="lblYTDPotentialValueTexthpRPending" Text="YTD Potential Value" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                          <b>  - </b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblYTDPotentialValuehpRPending" runat="server" Text='<%#Eval("RPending")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <b> <cc1:ucLabel ID="lblCountofBOTexthpRPending" Text="Count of Back Orders" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofBOhpRPending" runat="server" Text='<%#Eval("RPendingBackOrder")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <b>  <cc1:ucLabel ID="lblCountofDistinctSKUTexthpRPending" Text="Count of distinct SKU's" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                            -
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofDistinctSKUhpRPending" runat="server" Text='<%#Eval("RPendingOD_SKU_NO")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b> <cc1:ucLabel ID="lblCountofVendorsTexthpRPending" Text="Count of vendors" runat="server">
                                            </cc1:ucLabel></b> 
                                        </td>
                                        <td>
                                           <b>  -</b> 
                                        </td>
                                        <td>
                                            <cc1:ucLabel ID="lblCountofVendorshpRPending" runat="server" Text='<%#Eval("RPendingVendorID")%>'>
                                            </cc1:ucLabel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" CssClass="hpRPending_ToolTip"/>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <cc1:ucGridView ID="ucGridViewExport" Visible="false" runat="server" AutoGenerateColumns="false"
                CssClass="grid gvclass" CellPadding="4" Width="80%" ViewStateMode="Enabled" CellSpacing="5"
                Font-Size="9px">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" Font-Size="9px"></AlternatingRowStyle>
                <Columns>
                    <asp:BoundField DataField="Date" HeaderText="" ItemStyle-Font-Bold="true" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="PotentialPenaltyCharge" HeaderText="Stage 1-Potential Charge"
                        ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="IAccepted" HeaderText="Stage 2-Accepted" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="IDeclined" HeaderText="Stage 2-Declined" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="IPending" HeaderText="Stage 2-Pending Review" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="VAccepted" HeaderText="Stage 3-Accepted" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="VDeclined" HeaderText="Stage 3-Disputed" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="VPending" HeaderText="Stage 3-Pending Review" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="DDeclined" HeaderText="Stage 4-Sent To Mediator" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="DAccepted" HeaderText="Stage 4-Declined" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="DPending" HeaderText="Stage 4-Pending Review" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="MAccepted" HeaderText="Stage 5-Accepted" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="MDeclined" HeaderText="Stage 5-Declined" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="MPending" HeaderText="Stage 5-Pending Review" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="RAccepted" HeaderText="Result-Accepted" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="RDeclined" HeaderText="Result-Declined" ItemStyle-Font-Size="9px" />
                    <asp:BoundField DataField="RPending" HeaderText="Result-Pending" ItemStyle-Font-Size="9px" />
                </Columns>
            </cc1:ucGridView>
        </div>
        <div class="button-row">
            <cc1:ucButton ID="btnBack" runat="server" Visible="false" Text="Back" OnClick="btnBack_Click" />
        </div>
        <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
    </div>
</asp:Content>

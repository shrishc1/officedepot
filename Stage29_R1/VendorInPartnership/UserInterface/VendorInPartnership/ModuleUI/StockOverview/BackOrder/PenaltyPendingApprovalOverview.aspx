﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="PenaltyPendingApprovalOverview.aspx.cs" Inherits="PenaltyPendingApprovalOverview"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .radio-fix tr td:nth-child(2) {
            width: 245px;
        }

        .radio-fix tr td:nth-child(3) {
            width: 170px;
        }

        .wordbreak {
            word-break: break-all;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblPenaltyPendingApprovalOverview" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <cc1:ucPanel ID="pnlSelection" runat="server">
                        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td align="right">
                                    <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                                </td>
                            </tr>
                        </table>
                        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold;" width="10%">
                                    <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" width="1%" align="center">:
                                </td>
                                <td style="font-weight: bold;" width="80%">&nbsp;
                                    <uc1:ucCountry ID="ucCountry" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">:
                                </td>
                                <td>
                                    <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">:
                                </td>
                                <td>
                                    <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="3">
                                    <div class="button-row">
                                        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <cc1:ucPanel ID="pnlGrid" runat="server" Visible="false">
                        <div class="button-row">
                            <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                                Width="109px" Height="20px" />
                        </div>
                        <%--  <div style="width: 100%; overflow: auto" id="divPrint" class="fixedTable">--%>
                        <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table" style="width: 100%;">
                            <tr>
                                <td style="font-weight: bold;">
                                    <%--  <div style="width: 950px; overflow-x: auto;">--%>
                                    <cc1:ucGridView ID="gvPenaltyPendingApprovalOverview" runat="server" CssClass="grid gvclass" Width="960px"
                                        GridLines="Both" OnPageIndexChanging="gvPenaltyPendingApprovalOverview_PageIndexChanging"
                                        OnSorting="SortGrid">
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">
                                                <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                            </div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="Country" SortExpression="Vendor.VendorCountryName">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltCountry" Text='<%#Eval("Vendor.VendorCountryName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                                <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlVendorName" runat="server" Text='<%# Eval("Vendor.VendorName") %>'
                                                        NavigateUrl='<%# EncryptQuery("../PenaltyReview/InventoryReview1.aspx?VendorID="+Eval("Vendor.VendorID")+"&BP=PAO")%>'></asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="200px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="Vendor.StockPlannerNumber">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockPlannerName" SortExpression="Vendor.StockPlannerName">
                                                <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="200px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CurrentMonth" SortExpression="CurrentMonthCount">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltCurrentMonthCount" Text='<%#Eval("CurrentMonthCount") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PreviousMonth" SortExpression="PreviousMonthCount">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltPreviousMonthCount" Text='<%#Eval("PreviousMonthCount") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" Width="100px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </cc1:ucGridView>
                                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true"></cc1:PagerV2_8>
                                    <%-- </div>--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <div class="button-row">
                                        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <%--</div>--%>
                    </cc1:ucPanel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSearch" />
                    <asp:PostBackTrigger ControlID="btnBack" />
                    <asp:PostBackTrigger ControlID="btnExportToExcel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
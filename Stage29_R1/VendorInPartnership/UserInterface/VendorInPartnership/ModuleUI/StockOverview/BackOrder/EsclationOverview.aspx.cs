﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using System.Collections;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using Utilities;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Configuration;

public partial class EsclationOverview : CommonPage
{
    #region Declarations ...
    private const int gridPageSize = 20;
    bool IsExportClicked = false;
    bool IsbtnSearchClicked = false;
    
    #endregion


    #region Events

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
          if (!string.IsNullOrEmpty(GetQueryStringValue("NoLogin")) )
        {
            MasterPageFile = "~/CommonUI/CMN_MasterPages/NoUser_MasterPage.Master";
        }       
    }

    //protected void Page_PreRender(object sender, EventArgs e)
    //{

    //    if (!Page.IsPostBack)
    //    {
    //        if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "EsclationReview")
    //        {
    //            if (Session["EsclationOverview"] != null)
    //            {
    //                GetSession();
    //            }
    //        }
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;

            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "EsclationReview")
            {
                if (Session["EsclationOverview"] != null)
                {
                    GetSession();
                }
            }
        }

        if (!string.IsNullOrEmpty(GetQueryStringValue("NoLogin")))
        {
            BindEsclationListingOverview();
            btnBack.Visible = false;
        }        
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        IsbtnSearchClicked = true;
        BindEsclationListingOverview();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {

        IsExportClicked = true;
        BindEsclationListingOverview();
        //rdoDisputeType_CheckedChanged(sender, e);
        LocalizeGridHeader(gvMediatorReview);
        //WebCommon.Export("PenaltyDisputesPendingMediation", gvMediatorReview);
        if (rdoCheckAll.Checked)
        {
            WebCommon.ExportHideHidden("PenaltyDisputesPendingMediation_All", gvMediatorReview, null, IsHideHidden: true, isHideSecondColumn: false);
        }
        else if(rdoVendorDispute.Checked)
        {
            WebCommon.ExportHideHidden("PenaltyDisputesPendingMediation_VendorDispute", gvMediatorReview, null, IsHideHidden: true, isHideSecondColumn: false);
        }
        else if(rdoVendorDidNot.Checked)
        {
            WebCommon.ExportHideHidden("PenaltyDisputesPendingMediation_VendorDidNotReply", gvMediatorReview, null, IsHideHidden: true, isHideSecondColumn: false);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("EsclationOverview.aspx");        
    }

    protected void gvInventoryReview_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstBackOrderBE"] != null)
        {
            gvMediatorReview.DataSource = (List<BackOrderBE>)ViewState["lstBackOrderBE"];
            gvMediatorReview.PageIndex = e.NewPageIndex;
            if (Session["EsclationOverview"] != null)
            {
                Hashtable htEsclationOverview = (Hashtable)Session["EsclationOverview"];
                htEsclationOverview.Remove("PageIndex");
                htEsclationOverview.Add("PageIndex", e.NewPageIndex);
                Session["EsclationOverview"] = htEsclationOverview;
            }
            gvMediatorReview.DataBind();
        }

    }

    #endregion


    #region Methods

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currentPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currentPageIndx;
        BindEsclationListingOverview(currentPageIndx);
    }


    private void BindEsclationListingOverview(int Page = 1)
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();
            oBackOrderBE.User = new SCT_UserBE();

            oBackOrderBE.Action = "GetEsclationOverview";
            oBackOrderBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
            oBackOrderBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
            oBackOrderBE.SelectedMediatorIDs = msMediator.SelectedMediatorUserID;

            if (rdoAllOnly.Checked)
            {
                oBackOrderBE.MedaitorActionSearchBy = "All";
            }

            else if (rdoPending.Checked)
            {
                oBackOrderBE.MedaitorActionSearchBy = "Pending";
            }
            else if (rdoReviewedPending.Checked)
            {
                oBackOrderBE.MedaitorActionSearchBy = "ReviewedPending";
            }

            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "EsclationReview")
            {
                if (Session["EsclationOverview"] != null)
                {
                    Hashtable htEsclationOverview = (Hashtable)Session["EsclationOverview"];
                    oBackOrderBE.MedaitorActionSearchBy = (htEsclationOverview.ContainsKey("MedaitorActionSearchBy") && htEsclationOverview["MedaitorActionSearchBy"] != null) ? htEsclationOverview["MedaitorActionSearchBy"].ToString() : null;
                    
                    if (oBackOrderBE.MedaitorActionSearchBy == "All")
                    {
                        rdoAllOnly.Checked = true;
                    }
                    else if (oBackOrderBE.MedaitorActionSearchBy == "ReviewedPending")
                    {
                        rdoReviewedPending.Checked = true;
                    }
                    else
                    {
                        rdoPending.Checked = true;
                    }

                }
            }

                    oBackOrderBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            //if (Convert.ToString(Session["Role"]) == "OD - Manager")
            //{
            //    oBackOrderBE.User.RoleName = Session["Role"] as string;
            //}
            //else
            //{
            //    oBackOrderBE.User.RoleName = null;
            //}


            if (IsExportClicked)
            {
                oBackOrderBE.GridCurrentPageNo = 0;
                oBackOrderBE.GridPageSize = 0;
            }
            else
            {
                oBackOrderBE.GridCurrentPageNo = Page;
                oBackOrderBE.GridPageSize = gridPageSize;
            }

            if (!string.IsNullOrEmpty(GetQueryStringValue("NoLogin")))
            {
                oBackOrderBE.PenaltyChargeID = Convert.ToInt32(GetQueryStringValue("PenaltyChargeId"));
                oBackOrderBE.VendorID = Convert.ToInt32(GetQueryStringValue("VendorID"));
            }
            else
            {
                oBackOrderBE.PenaltyChargeID = null;
                oBackOrderBE.VendorID = null;
            }


            if (rdoVendorDispute.Checked)
            {
                ifUserControl.Visible = false;
                btnBack.Visible = true;
                oBackOrderBE.DisputeType = "VendorDispute";

            }

            if (rdoVendorDidNot.Checked)
            {
                ifUserControl.Visible = false;
                btnBack.Visible = true;
                oBackOrderBE.DisputeType = "VendorDidNot";
            }

            if (rdoCheckAll.Checked)
            {
                ifUserControl.Visible = false;
                btnBack.Visible = true;
                oBackOrderBE.DisputeType = "All";
            }


            this.SetSession(oBackOrderBE);

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetEsclationOverviewBAL(oBackOrderBE);

            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                pager1.Visible = true;
                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                ViewState["ItemCount"] = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                btnExportToExcel.Visible = true;
                gvMediatorReview.DataSource = null;
                gvMediatorReview.DataSource = lstBackOrderBE;
                ViewState["lstBackOrderBE"] = lstBackOrderBE;
                lstBackOrderBE.Where(x => x.DisputeForwaredTo == "Default").ToList().ForEach(x => x.DisputeForwaredTo = ConfigurationManager.AppSettings["DisputeEsclateTo"]);

            }
            else
            {
                gvMediatorReview.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
            }

            gvMediatorReview.PageIndex = Page;
            pager1.CurrentIndex = Page;

            gvMediatorReview.DataBind();
            oBackOrderBAL = null;

            pnlGrid.Visible = true;
            pnlSelection.Visible = false;

            if (IsbtnSearchClicked == true)
            {
                ifUserControl.Visible = false;
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }

    }

    private void SetSession(BackOrderBE oBackOrderBE)
    {
        Session["EsclationOverview"] = null;
        Session.Remove("EsclationOverview");

        Hashtable htEsclationOverview = new Hashtable();
        htEsclationOverview.Add("SelectedCountryId", oBackOrderBE.SelectedCountryIDs);
        htEsclationOverview.Add("SelectedVendorIDs", oBackOrderBE.SelectedVendorIDs);
        htEsclationOverview.Add("SelectedMediatorIDs", oBackOrderBE.SelectedMediatorIDs);
        htEsclationOverview.Add("UserId", oBackOrderBE.User.UserID);
        htEsclationOverview.Add("RoleName", oBackOrderBE.User.RoleName);
        htEsclationOverview.Add("PageIndex", oBackOrderBE.GridCurrentPageNo);
        htEsclationOverview.Add("MedaitorActionSearchBy", oBackOrderBE.MedaitorActionSearchBy);
        htEsclationOverview.Add("DisputeType", oBackOrderBE.DisputeType); 

        Session["EsclationOverview"] = htEsclationOverview;
    }

    private void GetSession()
    {

        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        BackOrderBE oBackOrderBE = new BackOrderBE();
        oBackOrderBE.User = new SCT_UserBE();

        if (Session["EsclationOverview"] != null)
        {
            Hashtable htEsclationOverview = (Hashtable)Session["EsclationOverview"];

            oBackOrderBE.SelectedCountryIDs = (htEsclationOverview.ContainsKey("SelectedCountryIDs") && htEsclationOverview["SelectedCountryIDs"] != null) ? htEsclationOverview["SelectedCountryIDs"].ToString() : null;
            oBackOrderBE.SelectedVendorIDs = (htEsclationOverview.ContainsKey("SelectedVendorIDs") && htEsclationOverview["SelectedVendorIDs"] != null) ? htEsclationOverview["SelectedVendorIDs"].ToString() : null;
            oBackOrderBE.SelectedMediatorIDs = (htEsclationOverview.ContainsKey("SelectedMediatorIDs") && htEsclationOverview["SelectedMediatorIDs"] != null) ? htEsclationOverview["SelectedMediatorIDs"].ToString() : null;
            oBackOrderBE.User.UserID = Convert.ToInt32(htEsclationOverview["UserId"].ToString());
            oBackOrderBE.MedaitorActionSearchBy = (htEsclationOverview.ContainsKey("MedaitorActionSearchBy") && htEsclationOverview["MedaitorActionSearchBy"] != null) ? htEsclationOverview["MedaitorActionSearchBy"].ToString() : null;
            oBackOrderBE.DisputeType = (htEsclationOverview.ContainsKey("DisputeType") && htEsclationOverview["DisputeType"] != null) ? htEsclationOverview["DisputeType"].ToString() : null;
            //if (Convert.ToString(Session["Role"]) == "OD - Manager")
            //{
            //    oBackOrderBE.User.RoleName = Session["Role"] as string;
            //}
            //else
            //{
            //    oBackOrderBE.User.RoleName = null;
            //}

            if (oBackOrderBE.DisputeType != null)
            {
                if (oBackOrderBE.DisputeType == "VendorDispute")
                {
                    rdoVendorDispute.Checked = true;
                }
                else if(oBackOrderBE.DisputeType == "VendorDidNot")
                {
                    rdoVendorDidNot.Checked = true;
                }
                else
                {
                    rdoCheckAll.Checked = true;
                }
            }

            oBackOrderBE.Action = "GetEsclationOverview";
            oBackOrderBE.GridCurrentPageNo = 1;
            oBackOrderBE.GridPageSize = gridPageSize;
            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetEsclationOverviewBAL(oBackOrderBE);
            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {                
                pager1.Visible = true;
                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                ViewState["ItemCount"] = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                btnExportToExcel.Visible = true;
                gvMediatorReview.DataSource = null;
                gvMediatorReview.DataSource = lstBackOrderBE;
                lstBackOrderBE.Where(x => x.DisputeForwaredTo == "Default").ToList().ForEach(x => x.DisputeForwaredTo = ConfigurationManager.AppSettings["DisputeEsclateTo"]);
                gvMediatorReview.DataBind();


                //if (pager1.ItemCount > gridPageSize)
                //    pager1.Visible = true;
                //else
                //    pager1.Visible = false;
            }
            else
            {
                gvMediatorReview.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
            }
            pager1.CurrentIndex = 1;
           
            oBackOrderBAL = null;

            pnlGrid.Visible = true;
            pnlSelection.Visible = false;

            ifUserControl.Visible = false;


        }


    }

    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);
            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
        }
    }

    #endregion





    protected void rdoDisputeType_CheckedChanged(object sender, EventArgs e)
    {

        BindEsclationListingOverview();



        //if (rdoVendorDispute.Checked)
        //{
        //    ifUserControl.Visible = false;
        //    btnBack.Visible = true;
        //    lstBackOrderBE = (List<BackOrderBE>) lstBackOrderBE.Where(x => x.DisputeType == "Vendor dispute").ToList();
        //    if (lstBackOrderBE.Count > 0)
        //    {
        //        gvMediatorReview.DataSource = lstBackOrderBE;
        //        gvMediatorReview.DataBind();
        //        btnExportToExcel.Visible = true;
        //        if (lstBackOrderBE.Count > gridPageSize)
        //        {
        //           // pager1.Visible = true;
        //        }
        //        else
        //        {
        //           // pager1.Visible = false;
        //        }
        //    }
        //    else
        //    {
        //        gvMediatorReview.DataSource = null;
        //        gvMediatorReview.DataBind();
        //        btnExportToExcel.Visible = false;
        //    }
        //}

        //if (rdoVendorDidNot.Checked)
        //{
        //    ifUserControl.Visible = false;
        //    btnBack.Visible = true;
        //    lstBackOrderBE = (List<BackOrderBE>)lstBackOrderBE.Where(x => x.DisputeType == "Vendor did not reply").ToList();
        //    if (lstBackOrderBE.Count > 0)
        //    {
        //        gvMediatorReview.DataSource = lstBackOrderBE;
        //        gvMediatorReview.DataBind();
        //        btnExportToExcel.Visible = true;
        //        if(lstBackOrderBE.Count > gridPageSize)
        //        {
        //           // pager1.Visible = true;
        //        }
        //        else
        //        {
        //          //  pager1.Visible = false;
        //        }

        //    }
        //    else
        //    {
        //        gvMediatorReview.DataSource = null;
        //        gvMediatorReview.DataBind();
        //        btnExportToExcel.Visible = false;
        //    }
        //}

        //if (rdoCheckAll.Checked)
        //{
        //    ifUserControl.Visible = false;
        //    btnBack.Visible = true;
        //    lstBackOrderBE = (List<BackOrderBE>)lstBackOrderBE.Where(x => x.DisputeType == "Vendor did not reply" || x.DisputeType == "Vendor dispute").ToList();
        //    if (lstBackOrderBE.Count > 0)
        //    {
        //        gvMediatorReview.DataSource = lstBackOrderBE;
        //        gvMediatorReview.DataBind();
        //        btnExportToExcel.Visible = true;
        //        if (lstBackOrderBE.Count > gridPageSize)
        //        {
        //           // pager1.Visible = true;
        //        }
        //        else
        //        {
        //          //  pager1.Visible = false;
        //        }
        //    }
        //    else
        //    {
        //        gvMediatorReview.DataSource = null;
        //        gvMediatorReview.DataBind();
        //        btnExportToExcel.Visible = false;
        //    }
        //}
    }

    protected void rblSelect_CheckedChanged(object sender, EventArgs e)
    {       
        foreach (GridViewRow row in gvMediatorReview.Rows)
        {
            RadioButton rblSelect = (RadioButton)row.FindControl("rblSelect");
            if (rblSelect != null & rblSelect.Checked)
            {
                HiddenField hdnPenaltyChargeID = (HiddenField)row.FindControl("hdnPenaltyChargeID"); 
                HiddenField hdnVendorID = (HiddenField)row.FindControl("hdnVendorID");
                HiddenField hdnSiteID = (HiddenField)row.FindControl("hdnSiteID");
                HiddenField hdnSKUID = (HiddenField)row.FindControl("hdnSKUID");
                Label ltPenaltyValue = (Label)row.FindControl("ltPenaltyValue");
                Label ltDateBOIncurred = (Label)row.FindControl("ltDateBOIncurred");
                HiddenField hdnBoDate = (HiddenField)row.FindControl("hdnBoDate"); 

                string sUserControl = "EsclationReview.aspx?VendorID=" + hdnVendorID.Value + "&PenaltyChargeId=" + hdnPenaltyChargeID.Value
               + "&NoLogin=" + GetQueryStringValue("NoLogin") + "&PenaltyValue=" + ltPenaltyValue.Text.Trim() + "&SiteID=" + hdnSiteID.Value
               + "&InventoryActionDate=" + ltDateBOIncurred.Text.Trim() + "&SKUID=" + hdnSKUID.Value + "&BoDate=" + hdnBoDate.Value;
                ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
                ifUserControl.Visible = true;
                pager1.Visible = true;
                pager1.ItemCount = Convert.ToInt32(ViewState["ItemCount"]);
                btnBack.Visible = false;
            }
        }      
    }

    //protected void AddEsclationDetails()
    //{
    //    if (!(string.IsNullOrEmpty(GetQueryStringValue("VendorID"))))
    //    {
    //        string sUserControl = "EsclationReview.aspx?VendorID=" + Eval("Vendor.VendorID") + "&PenaltyChargeId=" + Eval("PenaltyChargeID")
    //        + "&NoLogin=" + GetQueryStringValue("NoLogin") + "&PenaltyValue=" + Eval("PotentialPenaltyCharge") + "&SiteID=" + Eval("SiteID")
    //        + "&InventoryActionDate=" + Eval("InventoryActionDate") + "&SKUID=" + Eval("SKUID");
    //        ifUserControl.Attributes.Add("src", EncryptURL(sUserControl));
    //    }
    //    else
    //    {
    //        ifUserControl.Style["display"] = "none";
    //    }
    //}


}
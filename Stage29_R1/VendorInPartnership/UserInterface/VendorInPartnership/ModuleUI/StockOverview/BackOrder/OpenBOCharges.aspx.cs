﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using BaseControlLibrary;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Discrepancy.APAction;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Languages.Languages;
using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using Utilities;
using WebUtilities;

public partial class OpenBOCharges : CommonPage
{
    #region Page level variables ...

    string PleaseselectoneitemtoRaiseaDebit = WebCommon.getGlobalResourceValue("PleaseselectoneitemtoRaiseaDebit");
    string PleaseselecttheDebitType = WebCommon.getGlobalResourceValue("PleaseselecttheDebitType");
    string PleaseentertheVendorVATReference = WebCommon.getGlobalResourceValue("PleaseentertheVendorVATReference");
    string PleaseselecttheOfficeDepotVATReference = WebCommon.getGlobalResourceValue("PleaseselecttheOfficeDepotVATReference");
    string CurrencyMandatry = WebCommon.getGlobalResourceValue("CurrencyMandatry");
    string AdditionEmailValidation = WebCommon.getGlobalResourceValue("AdditionEmailValidation");
    string DebitRaisedSuccessfully = WebCommon.getGlobalResourceValue("DebitRaisedSuccessfully");
    string BackOrderDebitNoteNumberEmailSubject = WebCommon.getGlobalResourceValue("BackOrderDebitNoteNumberEmailSubject");

    #endregion

    #region Events ...

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pnlSearchScreen.Visible = true;
            pnlPotentialOutput.Visible = false;
            this.BindCurrency();
            this.BindODVATReference();
            this.BindDebitTypeReason();
        }

        msCountry.SetCountryOnPostBack();
        //msVendor.setVendorsOnPostBack();
        msAPClerk.SetAPOnPostBack();
        btnExportToExcel.CurrentPage = this;
        btnExportToExcel.FileName = "OpenBOCharges";
        btnExportToExcel.GridViewControl = gvAccountsPayableViewExcel;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.BindGrid();
    }

    protected void gvAccountsPayableView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstAccountsPayableView"] != null)
        {
            gvAccountsPayableView.PageIndex = e.NewPageIndex;
            gvAccountsPayableView.DataSource = (List<BackOrderBE>)ViewState["lstAccountsPayableView"];
            gvAccountsPayableView.DataBind();
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        //this.ClearCriteria();
        pnlSearchScreen.Visible = true;
        pnlPotentialOutput.Visible = false;
        btnExportToExcel.Visible = false;
        ViewState["backOrderBE"] = null;
    }

    protected void btnRaisedDebit_Click(object sender, EventArgs e)
    {
        bool checkedStatus = false;
        for (int index = 0; index < gvAccountsPayableView.Rows.Count; index++)
        {
            var rblSelect = (RadioButton)gvAccountsPayableView.Rows[index].FindControl("rblSelect");
            var lblVendorIDValue = (ucLabel)gvAccountsPayableView.Rows[index].FindControl("lblVendorIDValue");
            var ltVendorNoValue = (ucLabel)gvAccountsPayableView.Rows[index].FindControl("ltVendorNoValue");
            var ltVendorNameValue = (HyperLink)gvAccountsPayableView.Rows[index].FindControl("ltVendorNameValue");
            var lblVendorVATRefValue = (ucLabel)gvAccountsPayableView.Rows[index].FindControl("lblVendorVATRefValue");
            var lblAmountDebitedValue = (ucLabel)gvAccountsPayableView.Rows[index].FindControl("ltAmountDebitedValue");
            var lblCountryIDValue = (ucLabel)gvAccountsPayableView.Rows[index].FindControl("lblCountryIDValue");
            var hlDateofEscalationValue = (HyperLink)gvAccountsPayableView.Rows[index].FindControl("hlDateofEscalationValue");
            if (rblSelect.Checked)
            {
                checkedStatus = true;
                hdnVendorIdVal.Value = lblVendorIDValue.Text;
                lblVendorNumberSDRValue.Text = ltVendorNoValue.Text;
                lblVendorNameSDRValue.Text = ltVendorNameValue.Text;
                txtVendorVATReferenceValue.Text = lblVendorVATRefValue.Text;
                hdnAmountDebitedValue.Value = lblAmountDebitedValue.Text;
                hdnCountryIDValue.Value = lblCountryIDValue.Text;
                hdnDateofEscalationValue.Value = hlDateofEscalationValue.Text;
                break;
            }
        }

        if (checkedStatus)
        {
            var backOrderBE = new BackOrderBE();
            backOrderBE.Action = "IsVendorPendingForApproveBO";
            backOrderBE.Vendor = new UP_VendorBE();
            backOrderBE.Vendor.VendorID = Convert.ToInt32(hdnVendorIdVal.Value);
            var backOrderBAL = new BackOrderBAL();
            if (backOrderBAL.IsVendorPendingForApproveBOBAL(backOrderBE) > 0)
            {
                mdlAPConfirmation.Show();
            }
            else
            {
                this.RaisedDebit();

                var txtCommEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtVendorEmailList");
                var txtAdditionalEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtAdditionalEmailList");

                if (txtCommEmailList != null)
                    txtCommEmailList.Text = string.Empty;

                if (txtAdditionalEmailList != null)
                    txtAdditionalEmailList.Text = string.Empty;

                ucSDRCommunication1.FillAPContacts(Convert.ToInt32(hdnVendorIdVal.Value));

                if (ddlDebitType.Items.Count > 0)
                    ddlDebitType.SelectedIndex = 0;

                if (ddlCurrency.Items.Count > 0)
                    ddlCurrency.SelectedIndex = 0;

                if (ddlOfficeDepotVATReference.Items.Count > 0)
                    ddlOfficeDepotVATReference.SelectedIndex = 0;

                ddlDebitType.Focus();
                mdlAPRaiseDebit.Show();
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselectoneitemtoRaiseaDebit + "')", true);
            return;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlDebitType.SelectedIndex.Equals(0))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselecttheDebitType + "')", true);
            ddlDebitType.Focus();
            mdlAPRaiseDebit.Show();
            return;
        }

        if (string.IsNullOrEmpty(txtVendorVATReferenceValue.Text) && string.IsNullOrWhiteSpace(txtVendorVATReferenceValue.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseentertheVendorVATReference + "')", true);
            txtVendorVATReferenceValue.Focus();
            mdlAPRaiseDebit.Show();
            return;
        }

        if (ddlOfficeDepotVATReference.SelectedIndex.Equals(0))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselecttheOfficeDepotVATReference + "')", true);
            ddlOfficeDepotVATReference.Focus();
            mdlAPRaiseDebit.Show();
            return;
        }

        if (ddlCurrency.SelectedIndex.Equals(0))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + CurrencyMandatry + "')", true);
            ddlCurrency.Focus();
            mdlAPRaiseDebit.Show();
            return;
        }

        var txtAdditionalEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtAdditionalEmailList");
        if (!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text))
        {
            string[] sMailAddress;
            if (txtAdditionalEmailList.Text.Contains(','))
            {
                sMailAddress = txtAdditionalEmailList.Text.Trim().Split(',');

                for (int index = 0; index < sMailAddress.Length; index++)
                {
                    if (!RegexUtilities.IsValidEmail(sMailAddress[index].Trim()))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AdditionEmailValidation + "')", true);
                        txtAdditionalEmailList.Focus();
                        mdlAPRaiseDebit.Show();
                        return;
                    }
                }
            }
            else
            {
                if (!RegexUtilities.IsValidEmail(txtAdditionalEmailList.Text.Trim()))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + AdditionEmailValidation + "')", true);
                    txtAdditionalEmailList.Focus();
                    mdlAPRaiseDebit.Show();
                    return;
                }
            }
        }

        this.SaveDabit();
    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        this.HideModelPopup();
    }

    protected void btnYes_Click(object sender, EventArgs e)
    {
        this.RaisedDebit();
    }

    protected void btnNo_Click(object sender, EventArgs e)
    {
        mdlAPConfirmation.Hide();
    }

    #endregion

    #region Methods ...

    private void BindGrid()
    {
        var backOrderBE = new BackOrderBE();
        if (ViewState["backOrderBE"] == null)
        {
            backOrderBE.Action = "GetOpenBOChargesDetails";
            if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs) && !string.IsNullOrWhiteSpace(msCountry.SelectedCountryIDs))
                backOrderBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;

            backOrderBE.SelectedVendorIDs = this.GetSelectedVendorIds(msVendor);

            if (!string.IsNullOrEmpty(msAPClerk.SelectedStockPlannerIDs) && !string.IsNullOrWhiteSpace(msAPClerk.SelectedStockPlannerIDs))
                backOrderBE.SelectedAPClerkIDs = msAPClerk.SelectedStockPlannerIDs;
        }
        else
        {
            backOrderBE = (BackOrderBE)ViewState["backOrderBE"];
        }

        var backOrderBAL = new BackOrderBAL();
        var lstAccountsPayableView = backOrderBAL.GetOpenBOChargesDetailsBAL(backOrderBE);
        ViewState["backOrderBE"] = backOrderBE;

        #region Commented dummy data code for testing ...
        /* Dummy records for testing*/
        //if (lstAccountsPayableView.Count == 0)
        //{
        //    var oBackOrderBE = new BackOrderBE();
        //    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
        //    oBackOrderBE.Vendor.VendorID = 1621;
        //    oBackOrderBE.Vendor.Vendor_No = "025";
        //    oBackOrderBE.Vendor.VendorName = "ACCO";
        //    oBackOrderBE.CountryID = 1;
        //    oBackOrderBE.Country = "UKandIRE";
        //    oBackOrderBE.PenaltyType = "BO";
        //    oBackOrderBE.Rate = 5;
        //    oBackOrderBE.NoOfBO = 10;
        //    oBackOrderBE.QtyOnBO = 10;
        //    oBackOrderBE.Month = "January";
        //    oBackOrderBE.AmountDebited = 500;
        //    oBackOrderBE.Currency = new CurrencyBE();
        //    oBackOrderBE.Currency.CurrencyName = "EURO";
        //    oBackOrderBE.APClerk = "Jack Smith";
        //    oBackOrderBE.Vendor.StockPlannerNumber = "25";
        //    oBackOrderBE.Vendor.StockPlannerName = "Neil McLachlan";
        //    oBackOrderBE.Vendor.VendorVATRef = "ACCO025";
        //    lstAccountsPayableView.Add(oBackOrderBE);
        //}
        #endregion

        if (lstAccountsPayableView != null && lstAccountsPayableView.Count > 0)
        {
            gvAccountsPayableView.DataSource = lstAccountsPayableView;
            gvAccountsPayableViewExcel.DataSource = lstAccountsPayableView;
            ViewState["lstAccountsPayableView"] = lstAccountsPayableView;
            gvAccountsPayableView.PageIndex = 0;
            btnExportToExcel.Visible = true;
            btnRaisedDebit.Visible = true;
        }
        else
        {
            gvAccountsPayableViewExcel.DataSource = null;
            gvAccountsPayableView.DataSource = null;
            btnExportToExcel.Visible = false;
            btnRaisedDebit.Visible = false;
        }

        pnlSearchScreen.Visible = false;
        pnlPotentialOutput.Visible = true;
        gvAccountsPayableView.DataBind();
        gvAccountsPayableViewExcel.DataBind();
    }

    private string GetSelectedVendorIds(ucMultiSelectVendor multiSelectVendor)
    {
        ucListBox lstSelectedVendor = (ucListBox)multiSelectVendor.FindControl("lstSelectedVendor");
        var vendorIds = string.Empty;
        for (int index = 0; index < lstSelectedVendor.Items.Count; index++)
        {
            if (index.Equals(0))
                vendorIds = lstSelectedVendor.Items[index].Value;
            else
                vendorIds = string.Format("{0},{1}", vendorIds, lstSelectedVendor.Items[index].Value);
        }
        return vendorIds;
    }

    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstSelectedVendor");
        ListBox lstVendor = (ListBox)msVendor.FindControl("lstVendor");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);
            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<BackOrderBE>.SortList((List<BackOrderBE>)ViewState["lstAccountsPayableView"], e.SortExpression, e.SortDirection).ToArray();
    }

    private void BindCurrency()
    {
        var currencyBAL = new MAS_CurrencyBAL();
        var currencyBE = new CurrencyBE();
        currencyBE.Action = "ShowAll";
        var lstCurrency = currencyBAL.GetCurrenyBAL(currencyBE);
        if (lstCurrency != null && lstCurrency.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCurrency, lstCurrency, "CurrencyName", "CurrencyID", "Select");
        }
    }

    private void BindODVATReference()
    {
        var masVatCodeBAL = new MAS_VatCodeBAL();
        var masVatCodeBE = new MAS_VatCodeBE();
        masVatCodeBE.Action = "ShowAllODVatCode";
        //if (!string.IsNullOrEmpty(ucSite.innerControlddlSite.SelectedValue))
        //    masVatCodeBE.SiteId = Convert.ToInt32(ucSite.innerControlddlSite.SelectedValue);

        var lstGetODVatCode = masVatCodeBAL.GetODVatCodeBAL(masVatCodeBE);
        if (lstGetODVatCode != null && lstGetODVatCode.Count > 0)
        {
            FillControls.FillDropDown(ref ddlOfficeDepotVATReference, lstGetODVatCode, "VatCode", "ODVatCodeID", "Select");
        }
    }

    private void BindDebitTypeReason()
    {
        var debitReasonTypeBAL = new DIS_DebitReasonTypeBAL();
        var apActionBE = new APActionBE();
        apActionBE.Action = "GetReasonTypeForBO";
        var lstAPActionBE = debitReasonTypeBAL.GetReasonTypeBAL(apActionBE);
        if (lstAPActionBE != null && lstAPActionBE.Count > 0)
        {
            FillControls.FillDropDown(ref ddlDebitType, lstAPActionBE, "Reason", "ReasonTypeID", "Select");
        }
    }

    private void SaveDabit()
    {
        if (!string.IsNullOrEmpty(hdnVendorIdVal.Value) && !string.IsNullOrWhiteSpace(hdnVendorIdVal.Value))
        {
            var vendorId = Convert.ToInt32(hdnVendorIdVal.Value);
            var discrepancyBE = new DiscrepancyBE();
            var discrepancyBAL = new DiscrepancyBAL();

            #region Inserting data Debit raise tables ...
            discrepancyBE.Action = "AddDebitRaise";
            discrepancyBE.DebitRaseType = "OBOC"; /* Open BO Charges  */
            discrepancyBE.PurchaseOrderNumber = string.Empty;
            //discrepancyBE.SiteID = 0; //Convert.ToInt32(ViewState["SiteID"]);
            discrepancyBE.VendorID = vendorId;
            discrepancyBE.InvoiceNo = string.Empty;
            discrepancyBE.ReferenceNo = string.Empty;
            discrepancyBE.VendorVatReference = txtVendorVATReferenceValue.Text;

            if (!string.IsNullOrEmpty(hdnAmountDebitedValue.Value) && !string.IsNullOrWhiteSpace(hdnAmountDebitedValue.Value))
                discrepancyBE.DebitTotalValue = Convert.ToDecimal(hdnAmountDebitedValue.Value);

            if (!string.IsNullOrEmpty(ddlOfficeDepotVATReference.SelectedValue) && !string.IsNullOrWhiteSpace(ddlOfficeDepotVATReference.SelectedValue))
                discrepancyBE.ODVatReferenceID = Convert.ToInt32(ddlOfficeDepotVATReference.SelectedValue);

            var txtCommEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtVendorEmailList");
            var txtAdditionalEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtAdditionalEmailList");
            var emailIds = string.Empty;
            if (txtCommEmailList != null && txtAdditionalEmailList != null)
            {
                if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)) &&
                    (!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
                {
                    emailIds = string.Format("{0},{1}", txtCommEmailList.Text, txtAdditionalEmailList.Text);
                }
                else if ((!string.IsNullOrEmpty(txtCommEmailList.Text) && !string.IsNullOrWhiteSpace(txtCommEmailList.Text)))
                {
                    emailIds = txtCommEmailList.Text;
                }
                else if ((!string.IsNullOrEmpty(txtAdditionalEmailList.Text) && !string.IsNullOrWhiteSpace(txtAdditionalEmailList.Text)))
                {
                    emailIds = txtAdditionalEmailList.Text;
                }
            }

            discrepancyBE.CommunicationEmails = emailIds;
            discrepancyBE.InternalComments = txtUserComments.Text;

            if (ddlCurrency.SelectedIndex > 0)
            {
                discrepancyBE.Currency = new CurrencyBE();
                discrepancyBE.Currency.CurrencyId = Convert.ToInt32(ddlCurrency.SelectedValue);
            }

            //discrepancyBE.DiscrepancyLogID = discripancyLogId;
            discrepancyBE.Status = "A"; // A is an Active status of Debit Raise.
            discrepancyBE.UserID = Convert.ToInt32(Session["UserID"]);
            discrepancyBE.Country = new MAS_CountryBE();

            if (!string.IsNullOrEmpty(hdnCountryIDValue.Value) && !string.IsNullOrWhiteSpace(hdnCountryIDValue.Value))
                discrepancyBE.Country.CountryID = Convert.ToInt32(hdnCountryIDValue.Value);

            var debitRaiseId = discrepancyBAL.addDebitRaiseBAL(discrepancyBE);
            if (debitRaiseId > 0)
            {
                var discrepancy = new DiscrepancyBE();
                discrepancy.Action = "AddDebitRaiseItem";
                discrepancy.DebitRaiseId = debitRaiseId;
                discrepancy.StateValueDebited = Convert.ToDecimal(hdnAmountDebitedValue.Value);

                if (ddlDebitType.SelectedIndex > 0)
                    discrepancy.DebitReasonId = Convert.ToInt32(ddlDebitType.SelectedValue);

                discrepancyBAL.addDebitRaiseItemBAL(discrepancy);
            }

            #endregion

            #region Get Debit Raise data And Logic to Save & Send email ...
            if (debitRaiseId > 0)
            {
                discrepancyBE = new DiscrepancyBE();
                discrepancyBE.Action = "GetAllDebitRaise";
                discrepancyBE.DebitRaiseId = debitRaiseId;
                var lstDiscrepancyBE = discrepancyBAL.GetAllDebitRaiseBAL(discrepancyBE);

                var backOrderBE = new BackOrderBE();
                backOrderBE.Action = "GetOpenBOChargesEmailDetails";
                backOrderBE.Vendor = new UP_VendorBE();
                backOrderBE.Vendor.VendorID = vendorId;
                var backOrderBAL = new BackOrderBAL();
                var lstBOChargesEmailDetails = backOrderBAL.GetOpenBOChargesEmailDetailsBAL(backOrderBE);

                if ((lstDiscrepancyBE != null && lstDiscrepancyBE.Count > 0) && lstBOChargesEmailDetails != null && lstBOChargesEmailDetails.Count > 0)
                {
                    ViewState["DebitNoteNo"] = lstDiscrepancyBE[0].DebitNoteNo;
                    //DebitRaisedSuccessfully = DebitRaisedSuccessfully.Replace("##RaisedDebitNumber##", lstDiscrepancyBE[0].DebitNoteNo);
                    BackOrderDebitNoteNumberEmailSubject = BackOrderDebitNoteNumberEmailSubject.Replace("##DebitNoteNumber##", lstDiscrepancyBE[0].DebitNoteNo);
                    #region Logic to Save & Send email ...
                    var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
                    var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();
                    foreach (MAS_LanguageBE objLanguage in oLanguages)
                    {
                        bool MailSentInLanguage = false;
                        if (objLanguage.LanguageID.Equals(lstDiscrepancyBE[0].Vendor.LanguageID))
                            MailSentInLanguage = true;

                        var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                        //var templatesPath = @"emailtemplates/communication1";
                        var templateFile = string.Format(@"{0}emailtemplates/communication1/OpenBOCharges.english.htm", path);

                        #region Setting reason as per the language ...
                        switch (objLanguage.LanguageID)
                        {
                            case 1:
                                Page.UICulture = clsConstants.EnglishISO; //"en-US"; // English
                                break;
                            case 2:
                                Page.UICulture = clsConstants.FranceISO; //"fr"; // France
                                break;
                            case 3:
                                Page.UICulture = clsConstants.GermanyISO; //"de"; // German
                                break;
                            case 4:
                                Page.UICulture = clsConstants.NederlandISO; //"nl"; // Dutch
                                break;
                            case 5:
                                Page.UICulture = clsConstants.SpainISO; //"es"; // Spanish
                                break;
                            case 6:
                                Page.UICulture = clsConstants.ItalyISO; //"it"; // Italy
                                break;
                            case 7:
                                Page.UICulture = clsConstants.CzechISO; //"cz"; // Czech
                                break;
                            default:
                                Page.UICulture = clsConstants.EnglishISO; // "en-US"; // English
                                break;
                        }

                        #endregion

                        #region  Prepairing html body format ...
                        string htmlBody = null;
                        using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                        {
                            htmlBody = sReader.ReadToEnd();
                            //htmlBody = htmlBody.Replace("{logoInnerPath}", getAbsolutePath());

                            htmlBody = htmlBody.Replace("{VendorNumber}", lstDiscrepancyBE[0].Vendor.Vendor_No);
                            htmlBody = htmlBody.Replace("{DebitNoteNumber}", WebCommon.getGlobalResourceValue("DebitNoteNumber"));
                            htmlBody = htmlBody.Replace("{DebitNoteNumberValue}", lstDiscrepancyBE[0].DebitNoteNo);
                            htmlBody = htmlBody.Replace("{VendorName}", lstDiscrepancyBE[0].Vendor.VendorName);
                            htmlBody = htmlBody.Replace("{OurVATReference}", WebCommon.getGlobalResourceValue("OurVATReference"));

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].OurVATReference) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].OurVATReference))
                                htmlBody = htmlBody.Replace("{OurVATReferenceValue}", lstDiscrepancyBE[0].OurVATReference);
                            else
                                htmlBody = htmlBody.Replace("{OurVATReferenceValue}", string.Empty);

                            htmlBody = htmlBody.Replace("{VendorAddress1}", lstDiscrepancyBE[0].Vendor.address1);
                            htmlBody = htmlBody.Replace("{YourVATReference}", WebCommon.getGlobalResourceValue("YourVATReference"));

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].YourVATReference) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].YourVATReference))
                                htmlBody = htmlBody.Replace("{YourVATReferenceValue}", lstDiscrepancyBE[0].YourVATReference);
                            else
                                htmlBody = htmlBody.Replace("{YourVATReferenceValue}", string.Empty);

                            htmlBody = htmlBody.Replace("{VendorAddress2}", lstDiscrepancyBE[0].Vendor.address2);
                            htmlBody = htmlBody.Replace("{DebitReason}", WebCommon.getGlobalResourceValue("DebitReason"));
                            htmlBody = htmlBody.Replace("{DebitReasonValue}", lstDiscrepancyBE[0].Reason);

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].Vendor.VMPPIN) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].Vendor.VMPPIN))
                                htmlBody = htmlBody.Replace("{VMPPIN}", lstDiscrepancyBE[0].Vendor.VMPPIN);
                            else
                                htmlBody = htmlBody.Replace("{VMPPIN}", string.Empty);

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].Vendor.VMPPOU) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].Vendor.VMPPOU))
                                htmlBody = htmlBody.Replace("{VMPPOU}", lstDiscrepancyBE[0].Vendor.VMPPOU);
                            else
                                htmlBody = htmlBody.Replace("{VMPPOU}", string.Empty);

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].Vendor.city) && !string.IsNullOrWhiteSpace(lstDiscrepancyBE[0].Vendor.city))
                                htmlBody = htmlBody.Replace("{VendorCity}", lstDiscrepancyBE[0].Vendor.city);
                            else
                                htmlBody = htmlBody.Replace("{VendorCity}", string.Empty);

                            htmlBody = htmlBody.Replace("{DearSirMadam}", WebCommon.getGlobalResourceValue("DearSirMadam"));
                            htmlBody = htmlBody.Replace("{Date}", WebCommon.getGlobalResourceValue("Date"));

                            var debitRaiseDate = Convert.ToDateTime(lstDiscrepancyBE[0].DebitRaiseDate);
                            htmlBody = htmlBody.Replace("{DateValue}", debitRaiseDate.ToString("dd/MM/yyyy"));

                            if (!string.IsNullOrEmpty(hdnDateofEscalationValue.Value))
                            {
                                htmlBody = htmlBody.Replace("{DebitLetterOpenBOAPViewMessage1}", WebCommon.getGlobalResourceValue("DebitLetterOpenBOAPViewMessage1"));
                                htmlBody = htmlBody.Replace("##xx/xx/xxxx##", hdnDateofEscalationValue.Value);
                            }
                            else { htmlBody = htmlBody.Replace("{DebitLetterOpenBOAPViewMessage1}:&nbsp;", string.Empty); }

                            htmlBody = htmlBody.Replace("{DebitLetterOpenBOAPViewMessage2}", WebCommon.getGlobalResourceValue("DebitLetterOpenBOAPViewMessage2"));
                            htmlBody = htmlBody.Replace("{DebitLetterOpenBOAPViewMessage3}", WebCommon.getGlobalResourceValue("DebitLetterOpenBOAPViewMessage3"));

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].Comment))
                                htmlBody = htmlBody.Replace("{InternalComments}", lstDiscrepancyBE[0].Comment);
                            else
                                htmlBody = htmlBody.Replace("{InternalComments}", string.Empty);

                            #region Prepairing grid data ....

                            var strOurCode = WebCommon.getGlobalResourceValue("OurCode");
                            var strVendorCode = WebCommon.getGlobalResourceValue("VendorCode");
                            var strDescription1 = WebCommon.getGlobalResourceValue("Description1");
                            var strDateBOIncurred = WebCommon.getGlobalResourceValue("DateBOIncurred");
                            var strSite = WebCommon.getGlobalResourceValue("Site");
                            var strCountofBackOrdersIncurred = WebCommon.getGlobalResourceValue("CountofBackOrdersIncurred");
                            var strAgreedPenaltyCharge = WebCommon.getGlobalResourceValue("AgreedPenaltyCharge");
                            var strAgreedApprovedByWithSlash = WebCommon.getGlobalResourceValue("AgreedApprovedByWithSlash");

                            var sbBOCharges = new StringBuilder();
                            sbBOCharges.Append("<table style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=100%'>");
                            sbBOCharges.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'>"
                                + "<td width='10%'>" + strOurCode + "</cc1:ucLabel></td>"
                                + "<td width='10%'>" + strVendorCode + "</td>"
                                + "<td  width='20%'>" + strDescription1 + "</td>"
                                + "<td  width='10%'>" + strDateBOIncurred + "</td>"
                                + "<td  width='15%'>" + strSite + "</td>"
                                + "<td  width='10%'>" + strCountofBackOrdersIncurred + "</td>"
                                + "<td  width='10%'>" + strAgreedPenaltyCharge + "</td></tr>");
                            //+ "<td  width='15%'>" + strAgreedApprovedByWithSlash + "</td></tr>");

                            for (int index = 0; index < lstBOChargesEmailDetails.Count; index++)
                            {
                                var dateBOIncurred = Convert.ToDateTime(lstBOChargesEmailDetails[index].DateBOIncurred);

                                sbBOCharges.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'>"
                                + "<td width='10%'>" + lstBOChargesEmailDetails[index].PurchaseOrder.OD_Code + "</cc1:ucLabel></td>"
                                + "<td width='10%'>" + lstBOChargesEmailDetails[index].PurchaseOrder.Vendor_Code + "</td>"
                                + "<td width='20%'>" + lstBOChargesEmailDetails[index].PurchaseOrder.ProductDescription + "</td>"
                                + "<td  width='10%'>" + dateBOIncurred.ToString("dd/MM/yyyy") + "</td>"
                                + "<td  width='15%'>" + lstBOChargesEmailDetails[index].PurchaseOrder.SiteName + "</td>"
                                + "<td  width='10%'>" + lstBOChargesEmailDetails[index].NoofBO + "</td>"
                                + "<td  width='10%'>" + lstBOChargesEmailDetails[index].AmountDebited + "</td></tr>");
                                //+ "<td  width='15%'>" + lstBOChargesEmailDetails[index].AgreedApprovedBy + "</td></tr>");
                            }
                            sbBOCharges.Append("</table>");
                            htmlBody = htmlBody.Replace("{DebitLetterOpenBOAPViewGridDetails}", sbBOCharges.ToString());

                            #endregion

                            if (!string.IsNullOrEmpty(lstBOChargesEmailDetails[0].AgreedApprovedBy))
                                htmlBody = htmlBody.Replace("{AgreedApprovedBY}", lstBOChargesEmailDetails[0].AgreedApprovedBy);
                            else
                                htmlBody = htmlBody.Replace("{AgreedApprovedBY}", string.Empty);

                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", WebCommon.getGlobalResourceValue("Yoursfaithfully"));
                            htmlBody = htmlBody.Replace("{OfficeDepots}", WebCommon.getGlobalResourceValue("OfficeDepots"));

                            var apAddress = string.Empty;
                            if (!string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress1) && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress2)
                                && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress3) && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress4)
                                && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress5) && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress6))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", lstBOChargesEmailDetails[0].APAddress1, lstBOChargesEmailDetails[0].APAddress2,
                                    lstBOChargesEmailDetails[0].APAddress3, lstBOChargesEmailDetails[0].APAddress4, lstBOChargesEmailDetails[0].APAddress5, lstBOChargesEmailDetails[0].APAddress6);
                            }
                            else if (!string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress1) && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress2)
                                && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress3) && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress4)
                                && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress5))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", lstBOChargesEmailDetails[0].APAddress1, lstBOChargesEmailDetails[0].APAddress2,
                                    lstBOChargesEmailDetails[0].APAddress3, lstBOChargesEmailDetails[0].APAddress4, lstBOChargesEmailDetails[0].APAddress5);
                            }
                            else if (!string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress1) && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress2)
                               && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress3) && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress4))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", lstBOChargesEmailDetails[0].APAddress1, lstBOChargesEmailDetails[0].APAddress2,
                                    lstBOChargesEmailDetails[0].APAddress3, lstBOChargesEmailDetails[0].APAddress4);
                            }
                            else if (!string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress1) && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress2)
                               && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress3))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", lstBOChargesEmailDetails[0].APAddress1, lstBOChargesEmailDetails[0].APAddress2,
                                    lstBOChargesEmailDetails[0].APAddress3);
                            }
                            else if (!string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress1) && !string.IsNullOrEmpty(lstBOChargesEmailDetails[0].APAddress2))
                            {
                                apAddress = string.Format("{0},&nbsp;{1}", lstBOChargesEmailDetails[0].APAddress1, lstBOChargesEmailDetails[0].APAddress2);
                            }
                            else
                            {
                                apAddress = lstBOChargesEmailDetails[0].APAddress1;
                            }

                            if (!string.IsNullOrEmpty(lstDiscrepancyBE[0].APPincode))
                                apAddress = string.Format("{0},&nbsp;{1}", apAddress, lstBOChargesEmailDetails[0].APPincode);

                            htmlBody = htmlBody.Replace("{APClerkAddress}", apAddress);

                        }
                        #endregion

                        #region Sending and saving email details ...
                        string[] sMailAddress = emailIds.Split(',');
                        var sentToWithLink = new System.Text.StringBuilder();
                        for (int index = 0; index < sMailAddress.Length; index++)
                            sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("../LogDiscrepancy/DISLog_ReSendCommunication.aspx?CommTitle=DebitRaised&DebitRaiseId=" + debitRaiseId + "&CommunicationType=DebitRaised") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + sMailAddress[index] + "</a>" + System.Environment.NewLine);

                        var discrepancyMailBE = new DiscrepancyMailBE();
                        discrepancyMailBE.Action = "AddDebitRaiseCommunication";
                        discrepancyMailBE.DebitRaiseId = debitRaiseId;
                        discrepancyMailBE.mailSubject = BackOrderDebitNoteNumberEmailSubject;
                        discrepancyMailBE.sentTo = emailIds;
                        discrepancyMailBE.mailBody = htmlBody;
                        discrepancyMailBE.SendByID = Convert.ToInt32(Session["UserID"]);
                        discrepancyMailBE.IsMailSent = MailSentInLanguage;
                        discrepancyMailBE.languageID = objLanguage.LanguageID;
                        discrepancyMailBE.MailSentInLanguage = MailSentInLanguage;
                        discrepancyMailBE.CommTitle = "DebitRaised";
                        discrepancyMailBE.SentToWithLink = sentToWithLink.ToString();
                        var debitRaiseCommId = discrepancyBAL.addDebitRaiseCommunicationBAL(discrepancyMailBE);
                        if (MailSentInLanguage)
                        {
                            var emailToAddress = emailIds;
                            var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                            var emailToSubject = BackOrderDebitNoteNumberEmailSubject;
                            var emailBody = htmlBody;
                            Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                            oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                        }
                        #endregion

                        Page.UICulture = Convert.ToString(Session["CultureInfo"]);
                    }
                    #endregion

                    #region Logic to Updating BOReporting Ids against the Debit Raised ...
                    string boReportingIDs = string.Empty;
                    foreach (BackOrderBE oBackOrderBE in lstBOChargesEmailDetails)
                    {
                        if (string.IsNullOrEmpty(boReportingIDs) && oBackOrderBE.BOReportingID != null)
                            boReportingIDs = Convert.ToString(oBackOrderBE.BOReportingID);
                        else if (oBackOrderBE.BOReportingID != null)
                            boReportingIDs = string.Format("{0},{1}", boReportingIDs, Convert.ToString(oBackOrderBE.BOReportingID));
                    }
                    backOrderBE = new BackOrderBE();
                    backOrderBE.Action = "UpdateBOReportingByAP";
                    backOrderBE.APActionTakenBy = Convert.ToInt32(Session["UserID"]);
                    backOrderBE.DebitRaiseId = debitRaiseId;
                    backOrderBE.BOReportingIDs = boReportingIDs;
                    backOrderBAL.UpdateBOReportingByAPBAL(backOrderBE);
                    #endregion
                }

                this.HideModelPopup();
                this.BindGrid();
            }
            #endregion
        }
    }

    private void HideModelPopup()
    {
        hdnVendorIdVal.Value = string.Empty;
        lblVendorNumberSDRValue.Text = string.Empty;
        lblVendorNameSDRValue.Text = string.Empty;
        txtVendorVATReferenceValue.Text = string.Empty;
        txtUserComments.Text = string.Empty;
        mdlAPRaiseDebit.Hide();
    }

    private void RaisedDebit()
    {
        bool checkedStatus = false;
        for (int index = 0; index < gvAccountsPayableView.Rows.Count; index++)
        {
            var rblSelect = (RadioButton)gvAccountsPayableView.Rows[index].FindControl("rblSelect");
            var lblVendorIDValue = (ucLabel)gvAccountsPayableView.Rows[index].FindControl("lblVendorIDValue");
            var ltVendorNoValue = (ucLabel)gvAccountsPayableView.Rows[index].FindControl("ltVendorNoValue");
            var ltVendorNameValue = (HyperLink)gvAccountsPayableView.Rows[index].FindControl("ltVendorNameValue");
            var lblVendorVATRefValue = (ucLabel)gvAccountsPayableView.Rows[index].FindControl("lblVendorVATRefValue");
            var lblAmountDebitedValue = (ucLabel)gvAccountsPayableView.Rows[index].FindControl("ltAmountDebitedValue");
            var lblCountryIDValue = (ucLabel)gvAccountsPayableView.Rows[index].FindControl("lblCountryIDValue");
            if (rblSelect.Checked)
            {
                checkedStatus = true;
                hdnVendorIdVal.Value = lblVendorIDValue.Text;
                lblVendorNumberSDRValue.Text = ltVendorNoValue.Text;
                lblVendorNameSDRValue.Text = ltVendorNameValue.Text;
                txtVendorVATReferenceValue.Text = lblVendorVATRefValue.Text;
                hdnAmountDebitedValue.Value = lblAmountDebitedValue.Text;
                hdnCountryIDValue.Value = lblCountryIDValue.Text;
                break;
            }
        }

        if (checkedStatus)
        {
            var txtCommEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtVendorEmailList");
            var txtAdditionalEmailList = (ucTextbox)ucSDRCommunication1.FindControl("txtAdditionalEmailList");

            if (txtCommEmailList != null)
                txtCommEmailList.Text = string.Empty;

            if (txtAdditionalEmailList != null)
                txtAdditionalEmailList.Text = string.Empty;

            ucSDRCommunication1.FillAPContacts(Convert.ToInt32(hdnVendorIdVal.Value));

            if (ddlDebitType.Items.Count > 0)
                ddlDebitType.SelectedIndex = 0;

            if (ddlCurrency.Items.Count > 0)
                ddlCurrency.SelectedIndex = 0;

            if (ddlOfficeDepotVATReference.Items.Count > 0)
                ddlOfficeDepotVATReference.SelectedIndex = 0;

            ddlDebitType.Focus();
            mdlAPRaiseDebit.Show();
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PleaseselectoneitemtoRaiseaDebit + "')", true);
            return;
        }
    }

    #endregion
    
    protected void gvAccountsPayableView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label ltValueApprovedValue = (Label)e.Row.FindControl("ltValueApprovedValue");
            ltValueApprovedValue.Text = string.Empty;
            Label ltValueDisputedValue = (Label)e.Row.FindControl("ltValueDisputedValue");
            ltValueDisputedValue.Text = string.Empty;
        }
    }
}
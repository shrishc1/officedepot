﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    EnableEventValidation="false" AutoEventWireup="true" CodeFile="VendorViewDetails.aspx.cs"
    Inherits="VendorView" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="ucMultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .radio-fix tr td:nth-child(2)
        {
            width: 245px;
        }
        .radio-fix tr td:nth-child(3)
        {
            width: 170px;
        }
        .wordbreak
        {
            word-break: break-all;
        }
    </style>
    
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblVendorViewBO" runat="server"></cc1:ucLabel>
    </h2>
    <%-- <asp:UpdatePanel ID="updGrid" runat="server">
        <ContentTemplate>--%>
    <div class="right-shadow">
        <div class="formbox">
         
            <cc1:ucPanel ID="pnlGrid" runat="server" Visible="false">
                <div class="button-row">
                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel button"
                        OnClick="btnExport_Click" Width="109px" Height="20px" />
                </div>
                <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                    <tr>
                        <td style="font-weight: bold;" width="70%" align="left">
                            <cc1:ucLabel ID="lblPenaltyDetails" runat="server" Text="Detailed below are the open penalty charge(s)"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="30%" align="left">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-weight: bold;" align="left">
                            <cc1:ucLabel ID="lblVendorViewDescription" runat="server" Text="It is our mutual benefit to honor Purchase Order due dates. Please review and if for any reason you query any of the values / charges cited, please advise promptly"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
                <div style="width: 100%; overflow: auto" id="divPrint" class="fixedTable">
                    <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table">
                        <tr>
                            <td style="font-weight: bold;">
                                <div style="width: 950px; overflow-x: auto;">
                                    <cc1:ucGridView ID="gvVendorView" runat="server" CssClass="grid gvclass searchgrid-1"
                                        Width="950px" GridLines="Both" OnPageIndexChanging="gvVendorView_PageIndexChanging">
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">
                                                <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                            </div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                             <%-- <asp:HiddenField ID="hdnBOReportingID" Value='<%# Eval("BOReportingID") %>' runat="server" />--%>
                                            <asp:TemplateField HeaderText="ODSKU" SortExpression="SKU.OD_SKU_NO">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltodsku" Text='<%#Eval("SKU.OD_SKU_NO") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VikingSku" SortExpression="SKU.Direct_SKU">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt1" Text='<%#Eval("SKU.Direct_SKU") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorCode" SortExpression="SKU.Vendor_Code">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendorCode" Text='<%#Eval("SKU.Vendor_Code") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description" SortExpression="SKU.DESCRIPTION">
                                                <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt2" Text='<%#Eval("SKU.DESCRIPTION") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PenaltyRelatingto" SortExpression="PenaltyRelatingTo">
                                                <HeaderStyle Width="30px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltPenaltyRelatingTo" Text='<%#Eval("PenaltyRelatingTo") %>'></cc1:ucLabel>
                                                    <asp:HiddenField ID="hdnYear" Value='<%# Eval("Year") %>' runat="server" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="30px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DateBOIncurred" SortExpression="BOIncurredDate">
                                                <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltDateBOIncurred" Text='<%#Eval("BOIncurredDate", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Site" SortExpression="SKU.SiteName">
                                                <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltsite" Text='<%#Eval("SKU.SiteName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CountofBO(s)Incurred" SortExpression="NoofBO">
                                                <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltCountBo" Text='<%#Eval("NoofBO") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" Width="40px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="QtyofBO(s)Incurred" SortExpression="QtyonBackOrder">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt4" Text='<%#Convert.ToInt64(Math.Round(Convert.ToDouble(Eval("QtyonBackOrder")))) %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" Width="60px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="Vendor.StockPlannerNumber">
                                                <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockPlannerName" SortExpression="Vendor.StockPlannerName">
                                                <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                                                <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                    <asp:HiddenField ID="hdnVendorID" Value='<%# Eval("Vendor.VendorID") %>' runat="server" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                                <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OpenPONumbers" SortExpression="PurchaseOrder.Purchase_order">
                                                <HeaderStyle Width="20px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltOpenPONo" Text='<%#Eval("PurchaseOrder.Purchase_order") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="20px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OrderRaisedDate" SortExpression="PurchaseOrder.Order_raised">
                                                <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt5" Text='<%#Eval("PurchaseOrder.Order_raised", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OriginalDueDate" SortExpression="PurchaseOrder.Original_due_date">
                                                <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt7" Text='<%#Eval("PurchaseOrder.Original_due_date", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OutstandingQtyonPO" SortExpression="PurchaseOrder.Outstanding_Qty">
                                                <HeaderStyle Width="20px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltOutstandingQty" Text='<%#Eval("PurchaseOrder.Outstanding_Qty") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" Width="20px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ChargeIncurred" SortExpression="PotentialPenaltyCharge">
                                                <HeaderStyle Width="20px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltPotentialPenaltyCharge" Text='<%#Eval("PotentialPenaltyCharge") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" Width="20px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status" SortExpression="VendorBOReviewStatus">
                                                <HeaderStyle Width="20px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <%--<cc1:ucLabel runat="server" ID="ltStatus" Text='<%#Eval("VendorBOReviewStatus") %>'></cc1:ucLabel>--%>
                                                    <asp:LinkButton runat="server" CommandName="Select" ID="ltStatus" CommandArgument='<%#Eval("BOReportingID") %>'
                                                        Text='<%#Eval("VendorBOReviewStatus") %>'></asp:LinkButton>
                                                    <%--<cc1:ucLinkButton runat="server" CommandName="Select" ID="lnkStatus" Text='<%#Eval("VendorBOReviewStatus") %>'></cc1:ucLinkButton>--%>
                                                    <%-- <asp:HyperLink runat="server" ID="hylnkkStatus"  Text='<%#Eval("VendorBOReviewStatus") %>'></asp:HyperLink>--%>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="20px" Font-Size="10px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </cc1:ucGridView>
                                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                                    </cc1:PagerV2_8>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <div class="button-row">
                                   
                                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </cc1:ucPanel>
        </div>
    </div>
   
</asp:Content>

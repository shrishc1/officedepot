﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="InventoryReview2.aspx.cs" Inherits="InventoryReview2"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="ucMultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .radio-fix tr td:nth-child(2)
        {
            width: 245px;
        }
        .radio-fix tr td:nth-child(3)
        {
            width: 170px;
        }
        .wordbreak
        {
            word-break: break-all;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblDisputeReview" runat="server"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="pnlSelection" runat="server">
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:ucMultiSelectVendor ID="msVendor" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblODSKUCode" runat="server" Text="OD SKU Code"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:MultiSelectSKU runat="server" ID="msSKU" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblDisplay" runat="server" Text="Display"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td class="nobold radiobuttonlist" valign="middle">
                            <cc1:ucRadioButtonList ID="rblDisplayType" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                <asp:ListItem Text="lstOpen" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="lstClosed" Value="2"></asp:ListItem>
                                <asp:ListItem Text="lstAllOnly" Value="3"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                            <cc1:ucRadioButtonList ID="Test" runat="server" Visible="false">
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblPenaltyType" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td class="nobold radiobuttonlist" valign="middle">
                            <cc1:ucRadioButtonList ID="rblPenaltyType" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                <asp:ListItem Text="lstAllOnly" Value="1"></asp:ListItem>
                                <asp:ListItem Text="lstOTIF" Value="2"></asp:ListItem>
                                <asp:ListItem Text="lstBackOrder" Value="3" Selected="True"></asp:ListItem>
                            </cc1:ucRadioButtonList>
                            <cc1:ucRadioButtonList ID="UcRadioButtonList2" runat="server" Visible="false">
                            </cc1:ucRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblPeriod" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <table width="40%" border="0" cellspacing="5" cellpadding="0" id="tblDate">
                                <tr>
                                    <td>
                                        <cc1:ucDropdownList ID="drpMonthFrom" runat="server" Width="80px">
                                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                            <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                            <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                            <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                            <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                            <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                            <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                            <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                            <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                            <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                            <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                        </cc1:ucDropdownList>
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="drpYearFrom" runat="server" Width="60px">
                                        </cc1:ucDropdownList>
                                    </td>
                                    <td style="text-align: center">
                                        <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="drpMonthTo" runat="server" Width="80px">
                                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                            <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                            <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                            <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                            <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                            <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                            <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                            <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                            <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                            <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                            <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                        </cc1:ucDropdownList>
                                    </td>
                                    <td>
                                        <cc1:ucDropdownList ID="drpYearTo" runat="server" Width="60px">
                                        </cc1:ucDropdownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlGrid" runat="server" Visible="false">
                <div class="button-row">
                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                        Width="109px" Height="20px" />
                </div>
                <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table"
                    style="width: 100%;">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucGridView ID="gvDisputeReview" runat="server" CssClass="grid gvclass" GridLines="Both"
                                OnPageIndexChanging="gvInventoryReview_PageIndexChanging" Width="960px">
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <%--<asp:TemplateField HeaderText="Site" SortExpression="SKU.SiteName">
                                        <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltsite" Text='<%#Eval("SKU.SiteName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Vendor Number" SortExpression="Vendor.Vendor_No">
                                        <HeaderStyle Width="60px" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="60px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Name" SortExpression="Vendor.VendorName">
                                        <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="Vendor.StockPlannerNumber">
                                        <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StockPlannerName" SortExpression="Vendor.StockPlannerName">
                                        <HeaderStyle Width="150px" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Value of Query" SortExpression="PotentialPenaltyCharge">
                                        <HeaderStyle Width="90px" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltPotentialPenaltyCharge" Text='<%#Eval("PotentialPenaltyCharge") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="90px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Penalty Type" SortExpression="PenaltyType">
                                        <HeaderStyle Width="60px" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltPenaltyType" Text='<%#Eval("PenaltyType") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="60px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Month Query is for" SortExpression="QueryMonth">
                                        <HeaderStyle Width="90px" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="hlQueryMonth" runat="server" Text='<%# Eval("SelectedMonth") %>'></asp:Label>
                                            <%-- NavigateUrl='<%# EncryptQuery("InventoryDetailedReview.aspx?Month="+Eval("SelectedMonth")+"&VendorId="+Eval("Vendor.VendorID")+"&StatusValue="+Eval("DisputeReviewStatus")+"&Resolution="+Eval("Resolution"))%>'></asp:HyperLink>--%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="90px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                        <HeaderStyle Width="90px" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlStatus" runat="server" Text='<%# Eval("DisputeReviewStatus") %>'
                                                NavigateUrl='<%# EncryptQuery("InventoryDetailedReview.aspx?Status="+ Eval("DisputeReviewStatus")+"&VendorId="+Eval("Vendor.VendorID")+"&MonthValue="+Eval("SelectedMonth")+"&Resolution="+Eval("Resolution")+"&OD_SKU_NO="+Eval("SKU.OD_SKU_NO"))%>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="90px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Resolution" SortExpression="Resolution">
                                        <HeaderStyle Width="90px" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="ltResolution" Text='<%#Eval("Resolution") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="90px" />
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                            </cc1:PagerV2_8>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <div class="button-row">
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    EnableEventValidation="false" AutoEventWireup="true" CodeFile="VendorView.aspx.cs"
    Inherits="VendorView" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="ucMultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>

<%@ Register Src="~/CommonUI/UserControls/ucVikingSku.ascx" TagName="ucVikingSku"
    TagPrefix="cc1" %>

<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>

<%@ Register Src="~/CommonUI/UserControls/ucVendorItemCode.ascx" TagName="MultiSelectVendorItem"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .radio-fix tr td:nth-child(2) {
            width: 245px;
        }

        .radio-fix tr td:nth-child(3) {
            width: 170px;
        }

        .wordbreak {
            word-break: break-all;
        }

        .PopupClass {
            width: 40%;
        }
    </style>

    <script src="../../../Scripts/jquery.tooltip.min.js" type="text/javascript"></script>

    <style type="text/css">
        #tooltip {
            position: absolute;
            z-index: 2000;
            border: 1px solid #111;
            background-color: #ffcccc;
            padding: 5px;
            opacity: 1.00;
            color: #4C4A4A;
        }

            #tooltip h3, #tooltip div {
                margin: 0;
            }
    </style>


    <script language="javascript" type="text/javascript">
        $(function () {
            InitializeToolTip();
            function InitializeToolTip() {
                $(".ImgInfo").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    extraClass: "PopupClass",
                    bodyHandler: function () {
                        return $("#tooltip1").html();
                    },
                    showURL: false
                });
            }
        });
        var gridCheckedCount = 0;

        $(document).ready(function () {
            SelectAllGrid($("#chkSelectAllText")[0]);
        });

        function SelectAllGrid(chkBoxAllObj) {
            if (chkBoxAllObj.checked) {
                $("input[type='checkbox'][name$='chkSelect']").attr('checked', true);
                gridCheckedCount = $("input[type='checkbox'][name$='chkSelect']:checked").size();
            }
            else {
                $("input[type='checkbox'][name$='chkSelect']").attr('checked', false);
                gridCheckedCount = 0;
            }
        }

        function CheckUncheckAllCheckBoxAsNeeded() {

            if ($("input[type='checkbox'][name$='chkSelect']:checked").size() == $("input[type='checkbox'][name$='chkSelect']").length) {
                $("#chkSelectAllText").attr('checked', true);
                gridCheckedCount = $("#<%=gvVendorView.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
            else {
                $("#chkSelectAllText").attr('checked', false);
                gridCheckedCount = $("#<%=gvVendorView.ClientID%> input[id*='chkSelect']:checkbox:checked").size();
            }
        }

        function CheckAtleastOneCheckBox() {
            var selectCheckboxToAccept = '<%=selectCheckboxToAccept%>';
            if (gridCheckedCount == 0) {
                alert(selectCheckboxToAccept);
                return false
            }
        }

        function CheckOnlyOneCheckBox() {
            var selectCheckboxToAccept = '<%=selectCheckboxToAccept%>';
            var selectonlyoneCheckboxToQuery = '<%=selectonlyoneCheckboxToQuery%>';

            if (gridCheckedCount == 0) {
                alert(selectCheckboxToAccept);
                return false
            }
            if (gridCheckedCount > 1) {
                alert(selectonlyoneCheckboxToQuery);
                return false
            }
        }

        function CalculateRevisedPenalty() {
            var CurrentPenaltyCharge = document.getElementById("<%=txtCurrentPenaltyCharge.ClientID %>").value;
            var DisputedValue = document.getElementById("<%=txtDisputedValue.ClientID %>").value;
            var dval = parseFloat(parseFloat(DisputedValue).toFixed(2));
            if (!isNaN(dval)) {
                if (dval == 0) {
                    alert("<%= DisputedValueCannotBeZero %>");
                    document.getElementById("<%=txtDisputedValue.ClientID %>").value = '';
                    document.getElementById("<%=txtRevisedPenalty.ClientID %>").value = '';
                    return false;
                }

                if (parseFloat(parseFloat(DisputedValue).toFixed(2)) > parseFloat(parseFloat(CurrentPenaltyCharge).toFixed(2))) {
                    alert("<%=DisputedValueCannotGreaterThanCurrentPenalty %>");
                    document.getElementById("<%=txtDisputedValue.ClientID %>").value = '';
                    document.getElementById("<%=txtRevisedPenalty.ClientID %>").value = '';
                    $('#' + document.getElementById("<%=txtDisputedValue.ClientID %>")).focus();
                    return false;
                }
                var RevisedPenalty = (parseFloat(CurrentPenaltyCharge) - parseFloat(DisputedValue)).toFixed(2);
                document.getElementById("<%=txtRevisedPenalty.ClientID %>").value = RevisedPenalty;
                document.getElementById("<%=txtDisputedValue.ClientID %>").value = parseFloat(DisputedValue).toFixed(2);
            }
            else {

                document.getElementById("<%=txtRevisedPenalty.ClientID %>").value = '';
                document.getElementById("<%=txtDisputedValue.ClientID %>").value = '';
            }
        }

        function ValidatePopUp() {
            var CurrentPenaltyCharge = document.getElementById("<%=txtCurrentPenaltyCharge.ClientID %>").value;
            var DisputedValue = document.getElementById("<%=txtDisputedValue.ClientID %>").value;
            var RevisedPenalty = document.getElementById("<%=txtRevisedPenalty.ClientID %>").value;
            var Comments = document.getElementById("<%=txtComments.ClientID %>").value;

            if (DisputedValue == '') {
                alert("<%=Pleasestatethedisputedvalue %>");
                return false;
            }
            if (RevisedPenalty == '') {
                alert("<%=PleaseEnterRevisedPenalty %>");
                return false;
            }
            if (Comments == '') {
                alert("<%=PleaseEnterComments %>");
                return false;
            }

            if (!isNaN(DisputedValue)) {
                if (parseFloat(parseFloat(DisputedValue).toFixed(2)) > parseFloat(parseFloat(CurrentPenaltyCharge).toFixed(2))) {
                    alert("<%=DisputedValueCannotGreaterThanCurrentPenalty %>");
                    $('#' + document.getElementById("<%=txtDisputedValue.ClientID %>")).focus();
                    return false;
                }
            }
            return true;
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>


    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblVendorViewBO" runat="server"></cc1:ucLabel>
    </h2>

    <asp:UpdateProgress runat="server" ID="PageUpdateProgress" AssociatedUpdatePanelID="updGrid" >
        <ProgressTemplate>
            <div align="center" style="background-color: #000; top: 0px; left: 0px; bottom: 0px; right: 0px; padding-top: 20%; margin: 0; width: 100%; height: 50%; overflow: hidden; position: absolute; z-index: 1000; filter: alpha(opacity=50); opacity: 0.5;">
                <asp:Image ID="imgWait" runat="server" ImageUrl="~/Images/Ajaxloading.gif" ImageAlign="Middle" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="updGrid" runat="server">
        <ContentTemplate>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucPanel ID="pnlSelection" runat="server">
                        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">:
                                </td>
                                <td>
                                    <cc1:MultiSelectSite runat="server" ID="msSite" />
                                </td>
                            </tr>
                            <tr id="trVendor" runat="server">
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">:
                                </td>
                                <td>
                                    <cc1:ucMultiSelectVendor ID="msVendor" runat="server" />
                                </td>
                            </tr>
                            <%-----------------End-------------------------------%>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblODSKUCode" runat="server" Text="OD SKU Code">
                                    </cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                                </td>
                                <td>
                                    <cc1:MultiSelectSKU runat="server" ID="msSKU" />
                                </td>
                            </tr>

                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblODCatCode" runat="server">
                                    </cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel12" runat="server">:</cc1:ucLabel>
                                </td>
                                <td>
                                    <cc1:ucVikingSku ID="UcVikingSku" runat="server" />
                                </td>
                            </tr>

                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendorItemCode" runat="server">
                                    </cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel123" runat="server">:</cc1:ucLabel>
                                </td>
                                <td>
                                    <cc1:MultiSelectVendorItem ID="msVendorItem" runat="server" />
                                </td>
                            </tr>
                            <%--     <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblMonth" runat="server" Text="Month"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            :
                        </td>
                        <td>
                            <cc1:ucDropdownList ID="ddlMonth" runat="server" Width="150px">
                                <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                <asp:ListItem Text="January" Value="January"></asp:ListItem>
                                <asp:ListItem Text="February" Value="February"></asp:ListItem>
                                <asp:ListItem Text="March" Value="March"></asp:ListItem>
                                <asp:ListItem Text="April" Value="April"></asp:ListItem>
                                <asp:ListItem Text="May" Value="May"></asp:ListItem>
                                <asp:ListItem Text="June" Value="June"></asp:ListItem>
                                <asp:ListItem Text="July" Value="July"></asp:ListItem>
                                <asp:ListItem Text="August" Value="August"></asp:ListItem>
                                <asp:ListItem Text="September" Value="September"></asp:ListItem>
                                <asp:ListItem Text="October" Value="October"></asp:ListItem>
                                <asp:ListItem Text="November" Value="November"></asp:ListItem>
                                <asp:ListItem Text="December" Value="December"></asp:ListItem>
                            </cc1:ucDropdownList>
                        </td>
                    </tr>--%>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblPeriod" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">:
                                </td>
                                <td>
                                    <table width="40%" border="0" cellspacing="5" cellpadding="0" id="tblDate">
                                        <tr>
                                            <td>
                                                <cc1:ucDropdownList ID="drpMonthFrom" runat="server" Width="80px">
                                                    <%-- <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>--%>
                                                    <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                                    <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                                    <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                                    <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                                    <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                                    <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                                    <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                                    <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                                    <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpYearFrom" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td style="text-align: center">
                                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpMonthTo" runat="server" Width="80px">
                                                    <%--  <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>--%>
                                                    <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                                    <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                                    <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                                    <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                                    <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                                    <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                                    <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                                    <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                                    <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpYearTo" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td>
                                                <div>
                                                    <img src="../../../Images/info_button1.gif" runat="server" id="imgInfo" class="ImgInfo" align="absMiddle" border="0" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblStatus" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">:
                                </td>
                                <td colspan="2" class="nobold radiobuttonlist" valign="middle">
                                    <cc1:ucRadioButtonList ID="rblStatus" runat="server" RepeatColumns="4" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="lstCheckAll" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Pending (with Vendor)" Value="2" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Penalty Accepted by Vendor" Value="3"></asp:ListItem>
                                        <%-- <asp:ListItem Text="lstInDisputeUnderInvestigation" Value="4"></asp:ListItem>--%>
                                        <asp:ListItem Text="In Dispute with Supply Planner" Value="4"></asp:ListItem>
                                        <%--<asp:ListItem Text="Charge Accepted" Value="3"></asp:ListItem>--%>
                                        <%--<asp:ListItem Text="Dispute Accepted" Value="5"></asp:ListItem>--%>
                                        <asp:ListItem Text="Declined by Supply Planner after Dispute" Value="5"></asp:ListItem>
                                        <%--<asp:ListItem Text="lstDisputeDeclined" Value="6"></asp:ListItem>--%>
                                        <asp:ListItem Text="In dispute with Mediator" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="Declined by Mediator" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="Accepted by Mediator" Value="7"></asp:ListItem>
                                    </cc1:ucRadioButtonList>
                                    <cc1:ucRadioButtonList ID="UcRadioButtonList2" runat="server" Visible="false">
                                    </cc1:ucRadioButtonList>
                                </td>
                            </tr>

                            <tr>
                                <td align="right" colspan="3">
                                    <div class="button-row">
                                        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="tooltip1" style="display: none;">
                                        <table width="100%" cellspacing="5">
                                            <tr>
                                                <td style="width: 40%;">
                                                    <cc1:ucLabel runat="server" ID="lblNewStatus"></cc1:ucLabel>
                                                </td>
                                                <td style="width: 1%;"></td>
                                                <td style="width: 58%;">
                                                    <cc1:ucLabel runat="server" ID="lblNewDescription"></cc1:ucLabel>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblPendingWithVendor"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblPenaltyAcceptedByPlanner"></cc1:ucLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblPenaltyAcceptedByVendor"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblAcceptedByVendorInitialReview"></cc1:ucLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblInDisputeWithSP"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblPenaltyNotAcceptedByVendorInitialReview"></cc1:ucLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblDeclinedBySPafterDispute"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblAcceptedBySPfromVendor"></cc1:ucLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblInDisputeWithMediator"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblNotAcceptedBySPfromVendor"></cc1:ucLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblDeclinedByMediator"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblChargeDeclinedByMediator"></cc1:ucLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblAcceptedByMediator"></cc1:ucLabel>
                                                </td>
                                                <td>:
                                                </td>
                                                <td>
                                                    <cc1:ucLabel runat="server" ID="lblChargeAcceptedByMediator"></cc1:ucLabel>
                                                </td>
                                            </tr>

                                        </table>

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>




                    <cc1:ucPanel ID="pnlGrid" runat="server" Visible="false">
                        <div class="button-row">
                            <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel button"
                                OnClick="btnExport_Click" Width="109px" Height="20px" />
                        </div>
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                            <tr>
                                <td style="font-weight: bold;" width="70%" align="left">
                                    <cc1:ucLabel ID="lblPenaltyDetails" runat="server" Text="Detailed below are the open penalty charge(s)"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;" width="30%" align="left"></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="font-weight: bold;" align="left">
                                    <cc1:ucLabel ID="lblVendorViewDescription" runat="server" Text="It is our mutual benefit to honor Purchase Order due dates. Please review and if for any reason you query any of the values / charges cited, please advise promptly"></cc1:ucLabel>
                                </td>
                            </tr>
                        </table>
                        <div style="width: 100%; overflow: auto" id="divPrint" class="fixedTable">
                            <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table">
                                <tr>
                                    <td style="font-weight: bold;">
                                        <div style="width: 1200px; overflow-x: auto;">
                                            <cc1:ucGridView ID="gvVendorView" runat="server" CssClass="grid gvclass searchgrid-1"
                                                Width="1200px" GridLines="Both" OnPageIndexChanging="gvVendorView_PageIndexChanging"
                                                OnDataBound="gvVendorView_OnDataBound" OnRowCommand="gvVendorView_RowCommand"
                                                OnRowDataBound="gvVendorView_RowDataBound">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">
                                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                                    </div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SelectAllCheckBox">
                                                        <HeaderStyle HorizontalAlign="Left" Width="30px" />
                                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                        <HeaderTemplate>
                                                            <%-- <asp:CheckBox runat="server" ID="chkSelectAll" Text="<%$Resources:OfficeDepot, SelectAllText%>"
                                                                CssClass="checkbox-input" onclick="SelectAllGrid(this);" />--%>
                                                            <cc1:ucCheckbox runat="server" ID="chkSelectAllText" CssClass="checkbox-input" onclick="SelectAllGrid(this);" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hdnBOReportingID" Value='<%# Eval("BOReportingID") %>' runat="server" />
                                                            <asp:HiddenField ID="hdnStockPlannerID" Value='<%# Eval("StockPlannerID") %>' runat="server" />
                                                            <asp:HiddenField ID="hdnCountryName" Value='<%# Eval("Country") %>' runat="server" />
                                                            <asp:CheckBox runat="server" ViewStateMode="Enabled" CssClass="chk" ID="chkSelect"
                                                                onclick="CheckUncheckAllCheckBoxAsNeeded();" Visible='<%# (Convert.ToString( Eval("VendorBOReviewStatus")) == "Pending") ? true: false %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ODSKU" SortExpression="SKU.OD_SKU_NO">
                                                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltodsku" Text='<%#Eval("SKU.OD_SKU_NO") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VikingSku" SortExpression="SKU.Direct_SKU">
                                                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="lt1" Text='<%#Eval("SKU.Direct_SKU") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VendorCode" SortExpression="SKU.Vendor_Code">
                                                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltVendorCode" Text='<%#Eval("SKU.Vendor_Code") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description" SortExpression="SKU.DESCRIPTION">
                                                        <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="lt2" Text='<%#Eval("SKU.DESCRIPTION") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PenaltyRelatingto" SortExpression="PenaltyRelatingTo">
                                                        <HeaderStyle Width="30px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltPenaltyRelatingTo" Text='<%#Eval("PenaltyRelatingTo") %>'></cc1:ucLabel>
                                                            <asp:HiddenField ID="hdnYear" Value='<%# Eval("Year") %>' runat="server" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="30px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="DateBOIncurred" SortExpression="BOIncurredDate">
                                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltDateBOIncurred" Text='<%#Eval("BOIncurredDate", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Site" SortExpression="SKU.SiteName">
                                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltsite" Text='<%#Eval("SKU.SiteName") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="CountofBO(s)Incurred" SortExpression="NoofBO">
                                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltCountBo" Text='<%#Eval("NoofBO") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" Width="40px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="QtyofBO(s)Incurred" SortExpression="QtyonBackOrder">
                                                        <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="lt4" Text='<%#Convert.ToInt64(Math.Round(Convert.ToDouble(Eval("QtyonBackOrder")))) %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" Width="60px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="Vendor.StockPlannerNumber">
                                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="StockPlannerName" SortExpression="Vendor.StockPlannerName">
                                                        <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                            <asp:HiddenField ID="hdnVendorID" Value='<%# Eval("Vendor.VendorID") %>' runat="server" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                                        <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OpenPONumbers" SortExpression="PurchaseOrder.Purchase_order">
                                                        <HeaderStyle Width="20px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltOpenPONo" Text='<%#Eval("PurchaseOrder.Purchase_order") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="20px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OrderRaisedDate" SortExpression="PurchaseOrder.Order_raised">
                                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="lt5" Text='<%#Eval("PurchaseOrder.Order_raised", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OriginalDueDate" SortExpression="PurchaseOrder.Original_due_date">
                                                        <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="lt7" Text='<%#Eval("PurchaseOrder.Original_due_date", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OutstandingQtyonPO" SortExpression="PurchaseOrder.Outstanding_Qty">
                                                        <HeaderStyle Width="20px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltOutstandingQty" Text='<%#Eval("PurchaseOrder.Outstanding_Qty") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" Width="20px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ChargeIncurred" SortExpression="PotentialPenaltyCharge">
                                                        <HeaderStyle Width="20px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltPotentialPenaltyCharge" Text='<%#Eval("PotentialPenaltyCharge") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" Width="20px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status" SortExpression="CurrentStatus">
                                                        <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                            <%--<cc1:ucLabel runat="server" ID="ltStatus" Text='<%#Eval("VendorBOReviewStatus") %>'></cc1:ucLabel>--%>
                                                            <cc1:ucLabel runat="server" ID="lblVendorStatus" Text='<%#Eval("CurrentStatus") %>'></cc1:ucLabel>
                                                            <asp:LinkButton runat="server" CommandName="Select" ID="ltStatus" CommandArgument='<%#Eval("BOReportingID")+","+ Eval("VendorBOReviewStatus")%>'
                                                                Text='<%#Eval("CurrentStatus") %>'></asp:LinkButton>
                                                            <%--<cc1:ucLinkButton runat="server" CommandName="Select" ID="lnkStatus" Text='<%#Eval("VendorBOReviewStatus") %>'></cc1:ucLinkButton>--%>
                                                            <%-- <asp:HyperLink runat="server" ID="hylnkkStatus"  Text='<%#Eval("VendorBOReviewStatus") %>'></asp:HyperLink>--%>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                                    </asp:TemplateField>
                                                    <%--<asp:TemplateField HeaderText="Status" SortExpression="CurrentStatus">
                                                        <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                                        <ItemTemplate>
                                                        
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                                    </asp:TemplateField>--%>
                                                </Columns>
                                            </cc1:ucGridView>
                                            <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true"></cc1:PagerV2_8>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <div class="button-row" style="text-align: left;">
                                            <cc1:ucButton ID="btnAccept" runat="server" Text="Accept" CssClass="button" OnClick="btnAccept_Click"
                                                OnClientClick="return CheckAtleastOneCheckBox();" />
                                            <cc1:ucButton ID="btnQuery" runat="server" Text="Query" CssClass="button" OnClick="btnQuery_Click"
                                                OnClientClick="return CheckOnlyOneCheckBox();" />
                                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </cc1:ucPanel>
                </div>
            </div>
            <%--<asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>--%>
            <asp:Button ID="btnPenalty" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlPenaltyPopup" runat="server" TargetControlID="btnPenalty"
                PopupControlID="pnlPenalty" BackgroundCssClass="modalBackground" BehaviorID="mdlPenaltyPopup"
                DropShadow="false" />
            <asp:Panel ID="pnlPenalty" runat="server" Style="display: none">
                <cc1:ucPanel ID="pnlPenaltyAccepted" runat="server" CssClass="fieldset-form">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup"
                        width="100%">
                        <tr>
                            <td align="left" width="40%">
                                <b>
                                    <cc1:ucLabel ID="lblDateaccp" Text="Date Accepted " runat="server"></cc1:ucLabel></b>
                            </td>
                            <td width="2%">:
                            </td>
                            <td align="left" width="48%">
                                <cc1:ucLabel ID="lblDateAccepted" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <cc1:ucLabel ID="lblAccep" Text="Accepted By " runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>:
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblAcceptedBy" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup"
                        width="100%">
                        <tr>
                            <td align="right">
                                <cc1:ucButton ID="UcButton1" CommandName="Comments" runat="server" Text="Back" CssClass="button"
                                    OnClick="btnAcceptOk_Click" />
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
            </asp:Panel>
            <!--Penalty Pending pop up -->
            <asp:Button ID="btnPenaltyPending" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlPending" runat="server" TargetControlID="btnPenaltyPending"
                PopupControlID="pnlPenaltyPending" BackgroundCssClass="modalBackground" BehaviorID="mdlPending"
                DropShadow="false" />
            <asp:Panel ID="pnlPenaltyPending" runat="server" Style="display: none;">
                <cc1:ucPanel ID="pnlpending" runat="server" CssClass="fieldset-form">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td>
                                <div>
                                    <cc1:ucLabel ID="lblPendingStatus" runat="server"></cc1:ucLabel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <br />
                                    <cc1:ucButton ID="UcButton5" CommandName="Comments" runat="server" Text="Back" CssClass="button"
                                        OnClick="btnAcceptOk_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
            </asp:Panel>
            <%-- POP FOR DISPUTE ACCEPTED PANEL--%>
            <asp:Button ID="btnDispute" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="ModalPopupDisputeAccepted" runat="server" TargetControlID="btnDispute"
                PopupControlID="pnlDisputedAccepted" BackgroundCssClass="modalBackground" BehaviorID="ModalPopupDisputeAccepted"
                DropShadow="false" />
            <cc1:ucPanel ID="pnlDisputedAccepted" runat="server" Width="30%" CssClass="fieldset-form"
                Style="display: none;">
                <cc1:ucPanel ID="pnlDisputedDetails" runat="server" CssClass="fieldset-form">
                    <table cellspacing="5" cellpadding="0" border="0" align="left" class="top-setting-Popup"
                        width="100%">
                        <tr>
                            <td align="left" width="40%">
                                <b>
                                    <cc1:ucLabel ID="lblprpchg" Text="Proposed Charge" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td width="2%">:
                            </td>
                            <td align="left" width="58%">
                                <cc1:ucLabel ID="lblProposeCharge" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <cc1:ucLabel ID="lbldisval" Text="Dispute Value" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>:
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblDisputeValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <cc1:ucLabel ID="lblDateDispute" Text="Date Disputed Raised" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>:
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblDateDisputedRaised" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <cc1:ucLabel ID="lblDispby" Text="Disputed By" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>:
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblDisputedBy" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b>
                                    <cc1:ucLabel ID="lblreason" Text="Reason for Disputed :" runat="server"></cc1:ucLabel></b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b>
                                    <asp:TextBox ID="lbldisputedReason" TextMode="MultiLine" Wrap="true" Width="300"
                                        Height="50" ReadOnly="true" runat="server"></asp:TextBox></b>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
                <%-- OFFICE DEPOT RESPONSE PANEL--%>
                <cc1:ucPanel ID="pnlOfficeDepotResponse" runat="server" CssClass="fieldset-form">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup"
                        width="100%">
                        <tr>
                            <td align="left" width="40%">
                                <b>
                                    <cc1:ucLabel ID="lblDatetext" Text="Date" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td width="2%">:
                            </td>
                            <td align="left" width="58%">
                                <cc1:ucLabel ID="lblDateResponse" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <cc1:ucLabel ID="lblresfrom" Text="Response From" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>:
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblResponseFrom" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <cc1:ucLabel ID="lblResponsereason" Text="Response Detailed" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td colspan="2">:
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:TextBox ID="lblResponseDetailed" TextMode="MultiLine" Wrap="true" Width="300"
                                    Height="50" ReadOnly="true" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup"
                        width="100%">
                        <tr>
                            <td align="right">
                                <cc1:ucButton ID="UcButton2" CommandName="Comments" runat="server" Text="Back" CssClass="button"
                                    OnClick="btnAcceptOk_Click" />
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
            </cc1:ucPanel>
            <%-- POPUP FOR DISPUTED DECLINED--%>
            <%-- POP FOR DISPUTE DECLINED PANEL--%>
            <asp:Button ID="btnDecline" runat="Server" CssClass="fieldset-form" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="ModalPopupDisputeDeclined" runat="server" TargetControlID="btnDecline"
                PopupControlID="pnlDisputedDeclined" BackgroundCssClass="modalBackground" BehaviorID="pnlDisputedDeclined"
                DropShadow="false" />
            <cc1:ucPanel ID="pnlDisputedDeclined" runat="server" Width="30%" CssClass="fieldset-form"
                Style="display: none;">
                <cc1:ucPanel ID="pnlDisputedDetails_1" runat="server" CssClass="fieldset-form">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup"
                        width="100%">
                        <tr>
                            <td align="left" width="40%">
                                <b>
                                    <cc1:ucLabel ID="lbldecprofchrg" Text="Proposed Charge" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>:
                            </td>
                            <td align="left" width="58%">
                                <cc1:ucLabel ID="lblDeclineProChrg" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <cc1:ucLabel ID="lbldispval" Text="Disputed Value " runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>:
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblDeclineDisputeval" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <cc1:ucLabel ID="lblDecDate" Text="Date Disputed Raised" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>:
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblDeclineDisputeraisedDate" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <cc1:ucLabel ID="lbldisputby" Text="Disputed By" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>:
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblDeclineDisputedBy" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <cc1:ucLabel ID="lblResponseDet" Text="Reason for Disputed " runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>:
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:TextBox ID="lblDeclineReason" TextMode="MultiLine" Wrap="true" Width="300" Height="50"
                                    ReadOnly="true" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
                <%-- OFFICE DEPOT RESPONSE PANEL--%>
                <cc1:ucPanel ID="pnlDeclineOfficeDepotResponse" runat="server" CssClass="fieldset-form">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup"
                        width="100%">
                        <tr>
                            <td align="left" width="40%">
                                <b>
                                    <cc1:ucLabel ID="lblDecoffdate" Text=" Date" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>:
                            </td>
                            <td align="left" width="58%">
                                <cc1:ucLabel ID="lblDeclineDate" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <cc1:ucLabel ID="lbldecresfrom" Text="Response From" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>:
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblDeclineResponseFrom" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <cc1:ucLabel ID="lbdecresdet" Text="Response Detailed" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>:
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:TextBox ID="lblDeclinedResponseDetailed" TextMode="MultiLine" Wrap="true" Width="300"
                                    Height="50" ReadOnly="true" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup"
                        width="100%">
                        <tr>
                            <td align="right">
                                <cc1:ucButton ID="UcButton3" CommandName="Comments" runat="server" Text="Back" CssClass="button"
                                    OnClick="btnAcceptOk_Click" />
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
            </cc1:ucPanel>
            <%--POPUP FOR DISPUTE UNDER INVESTIGATION--%>
            <asp:Button ID="btnUnderinvest" runat="Server" CssClass="fieldset-form" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="ModalPopupDisputeUnderInvestigation" runat="server"
                TargetControlID="btnUnderinvest" PopupControlID="pnlInDisputeUnderInvestigation"
                BackgroundCssClass="modalBackground" BehaviorID="pnlInDisputeUnderInvestigation"
                DropShadow="false" />
            <cc1:ucPanel ID="pnlInDisputeUnderInvestigation" Width="30%" runat="server" CssClass="fieldset-form"
                Style="display: none;">
                <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup"
                    width="100%">
                    <tr>
                        <td align="left" width="40%">
                            <b>
                                <cc1:ucLabel ID="lbldisunderdate" Text="Proposed Charge" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td width="2%">:
                        </td>
                        <td align="left" width="48%">
                            <cc1:ucLabel ID="lblDisputeUnderDate" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <cc1:ucLabel ID="lbldisunderdateval" Text="Dispute Value " runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>:
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblDisputeUnderDisputeValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <cc1:ucLabel ID="lbldispraised" Text="Date Disputed Raised " runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>:
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblDisputeUnderDisputeRaised" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <cc1:ucLabel ID="lbldisby" Text="Disputed By" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>:
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblDisputeUnderDisputeBy" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1">
                            <b>
                                <cc1:ucLabel ID="lblRsndispt" Text="Reason for disputed " runat="server"></cc1:ucLabel></b>
                        </td>
                        <td colspan="2">:
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:TextBox ID="lblDisputeUnderReasonForDispute" TextMode="MultiLine" Wrap="true"
                                Width="350" Height="80" ReadOnly="true" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup"
                    width="100%">
                    <tr>
                        <td align="right">
                            <cc1:ucButton ID="UcButton4" CommandName="Comments" runat="server" Text="Back" CssClass="button"
                                OnClick="btnAcceptOk_Click" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <%--</ContentTemplate>
        <Triggers>
          <%-- <asp:AsyncPostBackTrigger ControlID="gvVendorView"  EventName="RowCommand" />--%>
            <%-- <asp:AsyncPostBackTrigger ControlID="btnOK_1" />
        </Triggers>
    </asp:UpdatePanel>--%>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updComment" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnComments" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlQueryCharge" runat="server" TargetControlID="btnComments"
                PopupControlID="pnlbtnComment" BackgroundCssClass="modalBackground" BehaviorID="MsgComment"
                DropShadow="false" />
            <asp:Panel ID="pnlbtnComment" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px; border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold; width: 39%">
                                <cc1:ucLabel ID="lblCurrentPenaltyCharge" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="width: 1%">:
                            </td>
                            <td style="width: 60%">
                                <cc1:ucTextbox runat="server" ID="txtCurrentPenaltyCharge" ReadOnly="true"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblPleasestatethedisputedvalue" runat="server"></cc1:ucLabel>
                            </td>
                            <td>:
                            </td>
                            <td>
                                <cc1:ucTextbox runat="server" ID="txtDisputedValue" onkeyup="AllowDecimalOnly(this);"
                                    MaxLength="10" onblur="CalculateRevisedPenalty();"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblRevisedPenalty" runat="server"></cc1:ucLabel>
                            </td>
                            <td>:
                            </td>
                            <td>
                                <cc1:ucTextbox runat="server" ID="txtRevisedPenalty" onkeyup="AllowDecimalOnly(this);" ReadOnly="true"
                                    MaxLength="10"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;" colspan="3">
                                <cc1:ucLabel ID="lblVendorViewQuery" runat="server" Text="Please state details of your query. - Please ensure that you supply a detailed response."></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;" colspan="3">
                                <cc1:ucTextarea ID="txtComments" runat="server" Width="500px" Height="150px"></cc1:ucTextarea>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <cc1:ucButton ID="btnOK" CommandName="Comments" runat="server" Text="OK" OnCommand="btnProceed_Click"
                                    CssClass="button" OnClientClick="return ValidatePopUp();" />
                                <cc1:ucButton ID="btnBack_1" CommandName="Comments" runat="server" Text="Back" OnClientClick="CheckUncheckAllCheckBoxAsNeeded();"
                                    OnCommand="btnBack_1_Click" CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOK" />
            <asp:AsyncPostBackTrigger ControlID="btnBack_1" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnAcceptCharge" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlAcceptCharge" runat="server" TargetControlID="btnAcceptCharge"
                PopupControlID="pnlAcceptCharge" BackgroundCssClass="modalBackground" BehaviorID="AcceptMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlAcceptCharge" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px; border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblAcceptDescription" runat="server" Text="The selected  penalites have been accepted - a debit note will be raised in respect of the penalty values"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <cc1:ucButton ID="btnOK_1" CommandName="Comments" runat="server" Text="OK" CssClass="button"
                                    OnClick="btnAcceptOk_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOK_1" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnQueryMsg" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlQueryMsg" runat="server" TargetControlID="btnQueryMsg"
                PopupControlID="pnlQueryMsg" BackgroundCssClass="modalBackground" BehaviorID="QueryMsg"
                DropShadow="false" />
            <asp:Panel ID="pnlQueryMsg" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px; border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblQueryInvestigationMsg" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <cc1:ucButton ID="btnOk_2" CommandName="Comments" runat="server" Text="OK" CssClass="button"
                                    OnClick="btnAcceptOk_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOK_2" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

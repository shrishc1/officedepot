﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="BackOrderSubReporting.aspx.cs" Inherits="ModuleUI_StockOverview_BackOrder_BackOrderSubReporting" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <asp:HiddenField ID="hdnFromDt" runat="server" />
    <asp:HiddenField ID="hdnToDt" runat="server" />
    <asp:HiddenField ID="hdnSiteIDs" runat="server" />
    <asp:HiddenField ID="hdnCountryIDs" runat="server" />
    <asp:HiddenField ID="hdnVendorIDs" runat="server" />
    <asp:HiddenField ID="hdnItemclassifications" runat="server" />
    <asp:HiddenField ID="hdnStockPlannerIDs" runat="server" />
    <asp:HiddenField ID="hdnReportType" runat="server" />
    <asp:HiddenField ID="hdnVendorName" runat="server" />
    <asp:HiddenField ID="hdnGridCurrentPageNo" runat="server" />
    <asp:HiddenField ID="hdnGridPageSize" runat="server" />
    <asp:HiddenField ID="hdnStockPlannerNo" runat="server" />
    <asp:HiddenField ID="hdnStockPlannerGroupingID" runat="server" />
     <asp:HiddenField ID="hdnODSkUCode" runat="server" />
     <asp:HiddenField ID="hdnVikingSkUCode" runat="server" />
    
    <h2>
        <cc1:ucLabel ID="ltBackOrderReporting" runat="server" Text="Daily Back Order Listing"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcReportViewPanel" runat="server">
                <div class="button-row">
                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                        Width="109px" Height="20px" Visible="false" />
                </div>
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <cc1:ucGridView ID="gvBOListing" Visible="false" runat="server" AutoGenerateColumns="false"
                                CssClass="grid gvclass searchgrid-1 " CellPadding="0" Width="960px" OnPageIndexChanging="gvDailyBOListing_PageIndexChanging"
                                DataSourceID="SqlDataSource1" onrowdatabound="gvBOListing_RowDataBound">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="VikingCode">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltVikingCode" runat="server" Text='<%#Eval("VikingCode") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OD Code">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltODCode" runat="server" Text='<%#Eval("ODCode") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Number of BO's`">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltNumberofBO" runat="server" Text='<%#Eval("NumberOfBOs") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty of BO">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltQtyofBO" runat="server" Text='<%#Eval("QuanityofBackOrder") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Item Price" SortExpression="Item_Val">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltItemPrice" runat="server" Text='<%#Eval("Item_Val") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                      <asp:TemplateField HeaderText="COGS" SortExpression="COGS">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltCOGS" runat="server" Text='<%# String.Format("{0:0.00}",Eval("COGS"))%>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item Classification">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltItemClassification" runat="server" Text='<%#Eval("ItemClassification") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Number">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltVendorNo" runat="server" Text='<%#Eval("VendorNumber") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Name">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltVendorName" runat="server" Text='<%#Eval("VendorName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltDescription" runat="server" Text='<%#Eval("DESCRIPTION") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stock Planner Number">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltStockPlannerNo" runat="server" Text='<%#Eval("StockPlannerNumber") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stock Planner Name">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltStockPlannerName" runat="server" Text='<%#Eval("StockPlannerName") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltDate" runat="server" Text='<%#Eval("DATE","{0:dd/MM/yyyy}") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Site">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltSite" runat="server" Text='<%#Eval("Site") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Country">
                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        <ItemTemplate>
                                            <cc1:ucLiteral ID="ltCountry" runat="server" Text='<%#Eval("Country") %>'></cc1:ucLiteral>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                            </cc1:PagerV2_8>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sConn %>"
                                SelectCommand="spRPT_BackOrderReport" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hdnCountryIDs" DefaultValue="-1" Name="SelectedCountryIDs"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnItemclassifications" DefaultValue="-1" Name="SelectedItemClassification"
                                        PropertyName="Value" Type="String" />
                                    <asp:ControlParameter ControlID="hdnSiteIDs" Name="SelectedSiteIDs" PropertyName="Value"
                                        Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnVendorIDs" Name="SelectedVendorIDs" PropertyName="Value"
                                        Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnStockPlannerIDs" Name="SelectedStockPlannerIDs"
                                        PropertyName="Value" Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnToDt" Name="DateTo" PropertyName="Value" Type="DateTime"
                                        DefaultValue="" />
                                    <asp:ControlParameter ControlID="hdnFromDt" Name="DateFrom" PropertyName="Value"
                                        Type="DateTime" DefaultValue="" />
                                    <asp:ControlParameter ControlID="hdnReportType" Name="ReportType" PropertyName="Value"
                                        Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnGridCurrentPageNo" Name="GridCurrentPageNo" PropertyName="Value"
                                        Type="Int32" DefaultValue="1" />
                                    <asp:ControlParameter ControlID="hdnGridPageSize" Name="GridPageSize" PropertyName="Value"
                                        Type="Int32" DefaultValue="50" />
                                         <asp:ControlParameter ControlID="hdnStockPlannerNo" Name="SelectedStockPlannerNo"
                                        PropertyName="Value" Type="String" DefaultValue="-1" />
                                          <asp:ControlParameter ControlID="hdnStockPlannerGroupingID" Name="SelectedStockPlannerGroupingIDs" PropertyName="Value"
                                     Type="String" DefaultValue="-1" />
                                     <asp:ControlParameter ControlID="hdnODSkUCode" Name="SelectedODSkUCode" PropertyName="Value"
                                     Type="String" DefaultValue="-1" />
                                    <asp:ControlParameter ControlID="hdnVikingSkUCode" Name="SelectedVikingSkUCode" PropertyName="Value"
                                     Type="String" DefaultValue="-1" />
                                     
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClientClick="javascript:window.open('', '_self', '');window.close();" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

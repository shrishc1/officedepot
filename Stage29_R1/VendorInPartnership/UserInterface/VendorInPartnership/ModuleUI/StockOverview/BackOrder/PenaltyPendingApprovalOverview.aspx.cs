﻿using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;

public partial class PenaltyPendingApprovalOverview : CommonPage
{
    #region Declarations ...

    private const int gridPageSize = 50;
    private bool IsExportClicked = false;

    #endregion Declarations ...

    #region Events ...

    protected void Page_InIt(object sender, EventArgs e)
    {
        ucCountry.CurrentPage = this;
        ucCountry.IsAllRequired = true;
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Prerender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //msVendor.IsParentRequired = false;
            //msVendor.IsStandAloneRequired = true;
            //msVendor.IsGrandParentRequired = true;
            //msVendor.IsChildRequired = true;
            //msVendor.CountryID = -2;
            //msVendor.EuropeOrLocal = 0;
            ucCountry.IsAllRequired = true;

            if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage").ToString() == "PendingPenalty")
            {
                if (Session["PendingPenalty"] != null)
                {
                    GetSession();
                }
            }
        }
    }

    public override void CountryPost_Load()
    {
        if (!IsPostBack)
        {
            ucCountry.innerControlddlCountry.SelectedIndex = 0;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;

            //if (Convert.ToString(GetQueryStringValue("BP")) == "IR1")
            //{
            //    BindVendorPendingPenalty();
            //}
        }

        msVendor.setVendorsOnPostBack();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindVendorPendingPenalty();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        BindVendorPendingPenalty();
        LocalizeGridHeader(gvPenaltyPendingApprovalOverview);
        WebCommon.ExporttoExcel("PendingPenaltyListing", gvPenaltyPendingApprovalOverview, false);
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("PenaltyPendingApprovalOverview.aspx");
    }

    protected void gvPenaltyPendingApprovalOverview_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstBackOrderBE"] != null)
        {
            gvPenaltyPendingApprovalOverview.DataSource = (List<BackOrderBE>)ViewState["lstBackOrderBE"];
            gvPenaltyPendingApprovalOverview.PageIndex = e.NewPageIndex;
            if (Session["PendingPenalty"] != null)
            {
                Hashtable htPendingPenalty = (Hashtable)Session["PendingPenalty"];
                htPendingPenalty.Remove("PageIndex");
                htPendingPenalty.Add("PageIndex", e.NewPageIndex);
                Session["PendingPenalty"] = htPendingPenalty;
            }
            gvPenaltyPendingApprovalOverview.DataBind();
        }
    }

    #endregion Events ...

    #region Methods..

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currentPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currentPageIndx;
        BindVendorPendingPenalty(currentPageIndx);
    }

    private void BindVendorPendingPenalty(int Page = 1)
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();
            oBackOrderBE.User = new SCT_UserBE();

            oBackOrderBE.Action = "GetPendingPenalty";
            oBackOrderBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
            oBackOrderBE.SelectedCountryIDs = ucCountry.innerControlddlCountry.SelectedItem.Value;
            oBackOrderBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
            oBackOrderBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            if (Convert.ToString(Session["Role"]) == "OD - Stock Planner")
            {
                oBackOrderBE.User.RoleName = Session["Role"] as string;
            }
            else
            {
                oBackOrderBE.User.RoleName = null;
            }

            if (IsExportClicked)
            {
                oBackOrderBE.GridCurrentPageNo = 0;
                oBackOrderBE.GridPageSize = 0;
            }
            else
            {
                oBackOrderBE.GridCurrentPageNo = Page;
                oBackOrderBE.GridPageSize = gridPageSize;
            }

            this.SetSession(oBackOrderBE);

            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.Vendor.VendorID = 0;

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetPendingPenaltyApprovalBAL(oBackOrderBE);

            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                btnExportToExcel.Visible = true;
                gvPenaltyPendingApprovalOverview.DataSource = null;
                gvPenaltyPendingApprovalOverview.DataSource = lstBackOrderBE;
                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                ViewState["lstBackOrderBE"] = lstBackOrderBE;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else
            {
                gvPenaltyPendingApprovalOverview.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
            }

            gvPenaltyPendingApprovalOverview.PageIndex = Page;
            pager1.CurrentIndex = Page;
            gvPenaltyPendingApprovalOverview.DataBind();
            oBackOrderBAL = null;

            pnlGrid.Visible = true;
            pnlSelection.Visible = false;
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    private void SetSession(BackOrderBE oBackOrderBE)
    {
        Session["PendingPenalty"] = null;
        Session.Remove("PendingPenalty");

        Hashtable htPendingPenalty = new Hashtable();
        htPendingPenalty.Add("SelectedCountryId", oBackOrderBE.SelectedCountryIDs);
        htPendingPenalty.Add("SelectedVendorIDs", oBackOrderBE.SelectedVendorIDs);
        htPendingPenalty.Add("SelectedSPIDs", oBackOrderBE.SelectedStockPlannerIDs);
        htPendingPenalty.Add("UserId", oBackOrderBE.User.UserID);
        htPendingPenalty.Add("RoleName", oBackOrderBE.User.RoleName);
        htPendingPenalty.Add("PageIndex", oBackOrderBE.GridCurrentPageNo);

        Session["PendingPenalty"] = htPendingPenalty;
    }

    private void GetSession()
    {
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();
        BackOrderBE oBackOrderBE = new BackOrderBE();
        oBackOrderBE.User = new SCT_UserBE();

        if (Session["PendingPenalty"] != null)
        {
            Hashtable htPendingPenalty = (Hashtable)Session["PendingPenalty"];

            oBackOrderBE.SelectedCountryIDs = (htPendingPenalty.ContainsKey("SelectedCountryId") && htPendingPenalty["SelectedCountryId"] != null) ? htPendingPenalty["SelectedCountryId"].ToString() : null;
            oBackOrderBE.SelectedVendorIDs = (htPendingPenalty.ContainsKey("SelectedVendorIDs") && htPendingPenalty["SelectedVendorIDs"] != null) ? htPendingPenalty["SelectedVendorIDs"].ToString() : null;
            oBackOrderBE.SelectedStockPlannerIDs = (htPendingPenalty.ContainsKey("SelectedSPIDs") && htPendingPenalty["SelectedSPIDs"] != null) ? htPendingPenalty["SelectedSPIDs"].ToString() : null;
            oBackOrderBE.User.UserID = Convert.ToInt32(htPendingPenalty["UserId"].ToString());
            oBackOrderBE.User.RoleName = (htPendingPenalty.ContainsKey("RoleName") && htPendingPenalty["RoleName"] != null) ? htPendingPenalty["RoleName"].ToString() : null;
            oBackOrderBE.Action = "GetPendingPenalty";

            oBackOrderBE.GridCurrentPageNo = Convert.ToInt32(htPendingPenalty["PageIndex"].ToString());
            oBackOrderBE.GridPageSize = gridPageSize;

            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.Vendor.VendorID = 0;

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetPendingPenaltyApprovalBAL(oBackOrderBE);

            ViewState["lstBackOrderBE"] = lstBackOrderBE;
            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                btnExportToExcel.Visible = true;
                gvPenaltyPendingApprovalOverview.DataSource = lstBackOrderBE;
                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;
            }
            else
            {
                gvPenaltyPendingApprovalOverview.DataSource = null;
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
            }
            pager1.CurrentIndex = Convert.ToInt32(htPendingPenalty["PageIndex"].ToString()); ;
            gvPenaltyPendingApprovalOverview.DataBind();
            oBackOrderBAL = null;

            pnlGrid.Visible = true;
            pnlSelection.Visible = false;
        }
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        int pageindex = pager1.CurrentIndex;
        BindVendorPendingPenalty(pageindex);
        return Utilities.GenericListHelper<BackOrderBE>.SortList((List<BackOrderBE>)ViewState["lstBackOrderBE"], e.SortExpression, e.SortDirection).ToArray();
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {
            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    #endregion Methods..
}
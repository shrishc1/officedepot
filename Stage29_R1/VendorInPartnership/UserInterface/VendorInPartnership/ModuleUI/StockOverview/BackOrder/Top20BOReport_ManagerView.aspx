﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Top20BOReport_ManagerView.aspx.cs" Inherits="ModuleUI_StockOverview_BackOrder_Top20BOReport_ManagerView" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" %>


<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/ModuleUI/Discrepancy/UserControl/ucSDRCommunicationAPListing.ascx"
    TagName="ucSDRCommunication" TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAPSearch.ascx" TagName="MultiSelectAPSearch"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucMultiSelectVendor.ascx" TagName="ucMultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucSite.ascx" TagName="ucSite" TagPrefix="cc2" %>

<%@ Register Src="../../../CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <style type="text/css">
        #tooltip
        {
            position: absolute;
            z-index: 3000;
            border: 1px solid #111;
            background-color: #FEE18D;
            padding: 5px;
            opacity: 1.00;
        }
        #tooltip h3, #tooltip div
        {
            margin: 0;
        }
    </style>
    <script src="../../../Scripts/jquery.tooltip.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () { InitializeToolTip(); });
        function InitializeToolTip() {
            $(".gridViewToolTipNew").tooltip({
                track: true,
                delay: 0,
                showURL: false,
                fade: 100,
                bodyHandler: function () {
                    $(this).parent().find(".tooltipPanel").show();
                    var obj = $(this).parent().find(".tooltipPanel");
                    $(this).parent().find(".tooltipPanel").hide();
                    var SKUSOH = $(this).parent().find(".SKU");
                    var VikingCodeSOH = $(this).parent().find(".VikingCode");
                    var DescriptionSOH = $(this).parent().find(".Description");
                    var tblDetails = $(this).parent().find(".tblDetails");

                    var length = "ctl00_ContentPlaceHolder1_ucGridView1_ctl".length;
                    var res = this.id.substring(length, length + 2);
                    iRowIndex = res.split("_")[0];
                    var DailtyQTY = $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_hdnDailyQty").val();
                    if (DailtyQTY.length > 0)
                    { DailtyQTY = parseInt(DailtyQTY); }
                    $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_lblTotalOrders").text($("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_lblOrders").text());
                    $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_lblTotalQty").text($("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_lblQuantity").text());
                    $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_lblDailyOrders").text($("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_hdnDailyOrder").val());
                    $("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_lblDailyQty").text(DailtyQTY);
                    SKUSOH.text($("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_hdnSKUhidden").val());
                    VikingCodeSOH.text($("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_lblVikingCode").text());
                    DescriptionSOH.text($("#ctl00_ContentPlaceHolder1_ucGridView1_ctl" + iRowIndex + "_lblDescription").text());
                    if (obj != null) {
                        return $(obj).html();
                    }
                    $(obj).hide();
                }
            });
        }
    </script>
 <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updAPView" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblManagerView" runat="server"></cc1:ucLabel>
            </h2>

            
              <div class="right-shadow">
              
              <div class="formbox">
              <div id="divSearchButtons" runat="server">
                        <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold; width: 10%">
                                <cc1:ucLabel ID="lblCategory" ClientIDMode="Static" runat="server" ></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 2%">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 15%">
                                       <cc1:ucDropdownList ID="ddlCategory" runat="server" Width="150px">
                                    </cc1:ucDropdownList>
                                </td>

                                <td style="font-weight:bold;width:3%">
                                </td>

                                <td style="font-weight: bold; width: 10%">
                                    <cc1:ucLabel ID="lblCountry" ClientIDMode="Static" runat="server" ></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 2%">
                                    :
                                </td>
                                <td style="font-weight: bold; width: 15%">
                                    <uc1:ucCountry ID="ucCountry" runat="server" />
                                </td>

                                <td style="font-weight:bold;width:3%">
                                </td>
                     
                                 <td style="font-weight: bold; width: 10%">
                                    <cc1:ucLabel ID="lblSitePrefixName" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 2%">
                                    :
                                </td>
                                
                                <td style="width: 15%">
                                    <cc2:ucSite ID="ddlSite" runat="server" />
                                </td>
                           </tr>

                          <tr>
                                <td colspan="11" style="height:20px" >
                                </td>
                          </tr>
 
                            <tr>
                             <td style="font-weight: bold; width: 10%">
                                 <cc1:ucLabel ID="lblView" ClientIDMode="Static" runat="server" ></cc1:ucLabel>
                             </td>
                               
                            <td style="font-weight: bold; width: 2%">
                                    :
                            </td>
                               
                            <td style="font-weight: bold; width: 15%">
                                 <cc1:ucRadioButton ID="rdoallonly" runat="server"  Checked="true" GroupName="View"/>
                                
                                 <cc1:ucRadioButton ID="rdoOpenOverdue" runat="server"  GroupName="View"/>
                            </td>

                             <td style="font-weight:bold;width:3%">
                                </td>

                            <td style="font-weight: bold; width: 10%">
                                    <cc1:ucLabel ID="lblorderby" ClientIDMode="Static" runat="server" ></cc1:ucLabel>
                            </td>
                            
                            <td style="font-weight: bold; width: 2%">
                                    :
                            </td>
                               
                            <td style="font-weight: bold; width: 15%">
                                 <cc1:ucRadioButton ID="rdoValue" runat="server" Checked="true" GroupName="OrderBy"/>
                                 <cc1:ucRadioButton ID="rdoCount" runat="server" GroupName="OrderBy"/>
                                 <cc1:ucRadioButton ID="rdoPlanner" runat="server" GroupName="OrderBy"/>
                            </td>

                            <td colspan="3">
                                    
                            </td>

                             <td style="font-weight: bold; width: 15%">
                            <cc1:ucButton runat="server" id="btnsearch" onclick="btnsearch_Click"/>
                            </td>
                            </tr>

                         </table>

                         <div class="button-row">
                         <cc1:ucExportToExcel ID="btnExportToExcel" runat="server" Visible="true" />
                     </div>
             
                        <table cellspacing="1" cellpadding="0">
                            <tr>
                                <td>
                                    <cc1:ucGridView ID="ucGridView1" runat="server" AutoGenerateColumns="false"  CssClass="grid gvclass"
                                        CellPadding="0" Width="100%" onrowdatabound="ucGridView1_RowDataBound"  >
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                      
                                      <asp:TemplateField HeaderText="Site" >
                                           <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblSite" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLabel>
                                                     <asp:HiddenField ID="hdnDailyQty" runat="server" Value='<%# Eval("QtyOnBO") %>' />
                                                <asp:HiddenField ID="hdnDailyOrder" runat="server" Value='<%# Eval("BackOrder") %>' />
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                          
                                          
                                       <asp:TemplateField HeaderText="Planner" >
                                           <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblPlanner" runat="server" Text='<%# Eval("PlannerName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                          

                                       <asp:TemplateField HeaderText="Vendor" >
                                           <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblVendor" runat="server" Text='<%# Eval("VendorNo") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                                
                                       <asp:TemplateField HeaderText="SKU" >
                                           <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                         <asp:HyperLink ID="lblSKU" runat="server" Text='<%# Eval("SKU_No") %>' Target="_blank"
                                            NavigateUrl='<%# EncryptQuery("~/ModuleUI/StockOverview/PenaltyReview/SKUDetailedReview.aspx?&ODSKUNO="+Eval("SKU_No")+"&Status="+
                                            "Pending"+"&VikingCode="+Eval("Viking")+"&Site="+Eval("SiteName")+"&Dateto="+ Eval("SelectedDateTo") +"&DateFrom="+Eval("SelectedDateFrom")+"&Page=Top")%>'></asp:HyperLink>                                        
                                                </ItemTemplate>
                                       </asp:TemplateField> 

                                       <asp:TemplateField HeaderText="Viking Code" >
                                           <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblVikingCode" runat="server" Text='<%# Eval("Viking") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                        

                                       <asp:TemplateField HeaderText=" Description " >
                                           <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="left" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblDescription" runat="server" Text='<%# Eval("Description") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                                
                                       <asp:TemplateField HeaderText="Qty" >
                                           <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblQuantity" runat="server" Text='<%# Eval("Qty") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                      
                                       <asp:TemplateField HeaderText="Orders" >
                                           <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblOrders" runat="server" Text='<%# Eval("Orders") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                        

                                       <asp:TemplateField HeaderText="Value" >
                                           <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblValue" runat="server" Text='<%# Eval("Value") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                        

                                     <asp:TemplateField HeaderText="On BO Since" >
                                           <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <cc1:ucLabel ID="lblOnBOSince" runat="server" Text='<%# Eval("Bosince") != null ?  Convert.ToDateTime(Eval("Bosince").ToString()).ToString("dd/MM/yyyy") : ""%>'></cc1:ucLabel>
                                                </ItemTemplate>
                                       </asp:TemplateField> 
                                          <asp:TemplateField HeaderText="BO Info">
                                            <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <img id="tooltipID" runat="server" class="gridViewToolTipNew" src="../../../Images/info_button1.gif"
                                                    align="absMiddle" border="0" />
                                                <asp:Panel ID="pnlSOH" runat="server" CssClass="tooltipPanel" Style="display: none;">
                                                    <table id="tblDetailsMain" class="tblDetailsMain" cellpadding="2" cellspacing="5"
                                                        style="width: 300px; background-color: #F4B084;">
                                                        <tr>
                                                            <td>
                                                                <b>SKU:</b>
                                                            </td>
                                                            <td>
                                                                <cc1:ucLabel ID="SKUID" class="SKU" runat="server" />
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <b>Viking Code:</b>
                                                            </td>
                                                            <td>
                                                                <cc1:ucLabel ID="VikingCode" class="VikingCode" runat="server" />
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <b>Description:</b>
                                                            </td>
                                                            <td>
                                                                <cc1:ucLabel ID="Description" class="Description" runat="server" />
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr><td colspan="8">
                                                        <table border="0" class="tblDetailsMain" cellpadding="2" cellspacing="2" style="width: 250px;
                                                            background-color: #F4B084;">
                                                            <tr>
                                                                <td style="width: 91px;">
                                                                    <b>Total Orders:</b>
                                                                </td>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblTotalOrders" class="TotalOrders" runat="server" />
                                                                </td>
                                                                <td style="width: 111px;">
                                                                    <b>Daily Orders:</b>
                                                                </td>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblDailyOrders" class="DailyOrders" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 91px;">
                                                                    <b>Total Qty:</b>
                                                                </td>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblTotalQty" class="TotalQty" runat="server" />
                                                                </td>
                                                                <td style="width: 111px;">
                                                                    <b>Daily Qty:</b>
                                                                </td>
                                                                <td>
                                                                    <cc1:ucLabel ID="lblDailyQty" class="DailyQty" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        </td></tr>
                                                        </tbody>
                                                    </table>
                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                           
                                           <asp:TemplateField HeaderText=" Date" SortExpression="Date">
                                                <HeaderStyle Width="80px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:Label ID="txtCommentdate"  runat="server" Class="Calender" Width="70px"  Text='<%# Eval("CommentDate") != null ?  Convert.ToDateTime(Eval("CommentDate").ToString()).ToString("dd/MM/yyyy") : ""%>'/>
                                                 <%--<cc1:ucTextbox ID="txtFromDate"  runat="server" Class="Calender" Text='<%# Eval("CommentDate") != null ?  Convert.ToDateTime(Eval("CommentDate").ToString()).ToString("dd/MM/yyyy") : ""%>'
                                                 Width="70px" />--%>
                                                    <%--<cc1:ucLiteral ID="lblDate" runat="server" Text='<%# Eval("CommentDate") != null ?  Convert.ToDateTime(Eval("CommentDate").ToString()).ToString("dd/MM/yyyy") : ""%>'></cc1:ucLiteral>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Comments" >
                                                <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>                                                  
                                                    <asp:Label runat="server" ID="txtComment"  TextMode="MultiLine" Text='<%# Eval("Comments") %>'/>
                                                </ItemTemplate>
                                                </asp:TemplateField>  
                                           
                                       </Columns>
                                    </cc1:ucGridView>
                                </td>
                             </tr>
                        </table>
                   
              </div>
       </ContentTemplate>
       <Triggers>
       <asp:PostBackTrigger ControlID="btnExportToExcel" />
         <asp:PostBackTrigger ControlID="btnsearch" />
       </Triggers>
    </asp:UpdatePanel>
 </asp:Content>

                            
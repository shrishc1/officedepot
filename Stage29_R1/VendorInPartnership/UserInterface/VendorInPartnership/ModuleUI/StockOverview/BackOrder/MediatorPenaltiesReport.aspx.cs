﻿using BaseControlLibrary;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;

public partial class ModuleUI_StockOverview_BackOrder_MediatorPenaltiesReport : CommonPage
{
    protected string RecordNotFound = WebCommon.getGlobalResourceValue("RecordNotFound");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //    txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            //txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //hdnJSFromDt.Value = txtFromDate.Text;
            //hdnJSToDt.Value = txtToDate.Text;

            string PreviousMonth = DateTime.Now.AddMonths(-1).ToString("MM");
            drpMonthFrom.SelectedValue = PreviousMonth;

            string CurrentMonth = DateTime.Now.ToString("MM");
            drpMonthTo.SelectedValue = CurrentMonth;

            FillControls.FillDropDown(ref drpYearTo, GetLastYears(), "Year", "Year");
            FillControls.FillDropDown(ref drpYearFrom, GetLastYears(), "Year", "Year");

            if (CurrentMonth.Equals("01"))
            {
                int currentYear = DateTime.Now.Year;
                drpYearFrom.SelectedValue = (currentYear - 1).ToString();
            }

        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("PreviousPage") != null && (GetQueryStringValue("PreviousPage") == "MediatorPenaltiesReport"))
            {
                if (Session["MediatorPenaltiesReport"] != null)
                {
                    RetainSearchData();
                }
            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGridview();
    }

    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        divSummaryGrid.Visible = true;
        btnBack.Visible = true;
        lblError.Text = "";
        lblError.Visible = false;
        btnExportToExcel.Visible = true;
        grdBind.PageIndex = e.NewPageIndex;
        grdBind.DataSource = (DataTable)ViewState["DataSource"];
        grdBind.DataBind();

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        divSummaryGrid.Visible = false;
        btnBack.Visible = false;
        tblSearch.Visible = true;

        EncryptQueryString("MediatorPenaltiesReport.aspx?PreviousPage=MediatorPenaltiesReport");

        //if (Request.RawUrl.Split('?').Length > 1)
        //{
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
        //}
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (rdoAllPenalties.Checked)
        {
            WebCommon.ExportHideHidden("AllPenalties_MediatorPlannerReport", grdBindExport, null, false, true);
        }
        else if (rdoVendorDidNot.Checked)
        {
            WebCommon.ExportHideHidden("VendorDidNotReply_MediatorPlannerReport", grdBindExport, null, false, true);
        }
        else if (rdoVendorDispute.Checked)
        {
            WebCommon.ExportHideHidden("VendorDisputed_MediatorPlannerReport", grdBindExport, null, false, true);
        }        
    }


    public void BindGridview(int page = 1)
    {
        divSummaryGrid.Visible = true;
        btnBack.Visible = true;
        BackOrderBE oBackOrderBE = new BackOrderBE();
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();

        oBackOrderBE.Action = "GetPenaltiesPerMediatorReport";

        //oBackOrderBE.SelectedDateFrom = Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
        //oBackOrderBE.SelectedDateTo = Common.GetMM_DD_YYYY(hdnJSToDt.Value);

        int numberOfDays = DateTime.DaysInMonth(Convert.ToInt16(drpYearTo.SelectedValue), Convert.ToInt16(drpMonthTo.SelectedValue)); // for numberofdays in month

        oBackOrderBE.SelectedDateFrom = Convert.ToDateTime(drpYearFrom.SelectedValue + "-" + drpMonthFrom.SelectedValue + "-" + "01");
        oBackOrderBE.SelectedDateTo = Convert.ToDateTime(drpYearTo.SelectedValue + "-" + drpMonthTo.SelectedValue + "-" + numberOfDays.ToString());

        hdnJSFromDt.Value = Convert.ToString(oBackOrderBE.SelectedDateFrom);
        hdnJSToDt.Value = Convert.ToString(oBackOrderBE.SelectedDateTo);

        if (!(string.IsNullOrEmpty(msVendor.SelectedVendorIDs)))
        {
            oBackOrderBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
        }

        if (!(string.IsNullOrEmpty(msMediator.SelectedMediatorUserID)))
        {
            oBackOrderBE.SelectedMediatorIDs = msMediator.SelectedMediatorUserID;
        }       

        if (!(string.IsNullOrEmpty(msCountry.SelectedCountryIDs)))
        {
            oBackOrderBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
        }
        

        if (rdoAllPenalties.Checked)
        {
            oBackOrderBE.DisputeType = "All";
        }
        else if (rdoVendorDidNot.Checked)
        {
            oBackOrderBE.DisputeType = "VendorDidNot";
        }
        else if (rdoVendorDispute.Checked)
        {
            oBackOrderBE.DisputeType = "VendorDispute";
        }       

        DataSet ds = oBackOrderBAL.GetMediatorPenaltyReportBAL(oBackOrderBE);
        this.SetSession(oBackOrderBE);

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["DataSource"] = ds.Tables[0];
                Session["InputFields"] = oBackOrderBE;
                grdBind.DataSource = ds.Tables[0];
                grdBind.DataBind();
                grdBindExport.DataSource = ds.Tables[0];
                grdBindExport.DataBind();
                lblPenaltiesPerMediatorReport.Visible = true;
                lblMediatorPenaltiesReport.Visible = false;
                lblError.Text = "";
                lblError.Visible = false;
                tblSearch.Visible = false;
                btnExportToExcel.Visible = true;

            }
            else
            {
                grdBind.DataSource = null;
                grdBind.DataBind();
                lblError.Visible = true;
                lblError.Text = RecordNotFound;
                lblPenaltiesPerMediatorReport.Visible = true;
                lblMediatorPenaltiesReport.Visible = false;
                tblSearch.Visible = false;
                btnExportToExcel.Visible = false;
            }
        }
        else
        {
            grdBind.DataSource = null;
            grdBind.DataBind();
            lblError.Visible = true;
            lblError.Text = RecordNotFound;
            lblPenaltiesPerMediatorReport.Visible = true;
            lblMediatorPenaltiesReport.Visible = false;
        }

    }

    private void SetSession(BackOrderBE oBackOrderBE)
    {
        Session["MediatorPenaltiesReport"] = null;
           
        TextBox txtVendor = (TextBox)msVendor.FindControl("ucVendor").FindControl("txtVendorNo");
        TextBox txtMediator = (TextBox)msMediator.FindControl("txtMediator");

        Hashtable htMediatorPenaltiesReport = new Hashtable();
        htMediatorPenaltiesReport.Add("txtVendor", txtVendor.Text);
        htMediatorPenaltiesReport.Add("SelectedVendorIDs", oBackOrderBE.SelectedVendorIDs);
        htMediatorPenaltiesReport.Add("IsSearchedByVendorNo", msVendor.IsSearchedByVendorNo);
        htMediatorPenaltiesReport.Add("SelectedCountryIDs", oBackOrderBE.SelectedCountryIDs);
        htMediatorPenaltiesReport.Add("SelectedMediatorIDs", oBackOrderBE.SelectedMediatorIDs);
        htMediatorPenaltiesReport.Add("SelectedMediatorName", txtMediator.Text);
        htMediatorPenaltiesReport.Add("SelectedDateFrom", hdnJSFromDt.Value);
        htMediatorPenaltiesReport.Add("SelectedDateTo", hdnJSToDt.Value);
        htMediatorPenaltiesReport.Add("SelectedCountryName", msCountry.SelectedCountryName);
        htMediatorPenaltiesReport.Add("DisputeType", oBackOrderBE.DisputeType);        
        Session["MediatorPenaltiesReport"] = htMediatorPenaltiesReport;
    }

    public void RetainSearchData()
    {

        Hashtable htMediatorPenaltiesReport = (Hashtable)Session["MediatorPenaltiesReport"];


        //********** Vendor ***************
        string txtVendor = (htMediatorPenaltiesReport.ContainsKey("txtVendor") && htMediatorPenaltiesReport["txtVendor"] != null) ? htMediatorPenaltiesReport["txtVendor"].ToString() : "";
        string VendorId = (htMediatorPenaltiesReport.ContainsKey("SelectedVendorIDs") && htMediatorPenaltiesReport["SelectedVendorIDs"] != null) ? htMediatorPenaltiesReport["SelectedVendorIDs"].ToString() : "";
        TextBox txtVendorId = msVendor.FindControl("ucVendor").FindControl("txtVendorNo") as TextBox;

        bool IsSearchedByVendorNo = (htMediatorPenaltiesReport.ContainsKey("IsSearchedByVendorNo") && htMediatorPenaltiesReport["IsSearchedByVendorNo"] != null) ? Convert.ToBoolean(htMediatorPenaltiesReport["IsSearchedByVendorNo"]) : false;
        string txtVendorIdText = txtVendorId.Text;
        ucListBox lstRightVendor = msVendor.FindControl("lstRight") as ucListBox;
        ucListBox lstLeftVendor = msVendor.FindControl("ucVendor").FindControl("lstLeft") as ucListBox;
        string strVendorId = string.Empty;
        int value;
        lstLeftVendor.Items.Clear();
        if (lstLeftVendor != null && lstRightVendor != null && (!string.IsNullOrEmpty(VendorId) || !string.IsNullOrEmpty(VendorId)))
        {
            lstLeftVendor.Items.Clear();
            //lstRightVendor.Items.Clear();

            if (IsSearchedByVendorNo == true)
            {
                msVendor.SearchVendorNumberClick(txtVendor);
                //parsing successful 
            }
            else
            {
                msVendor.SearchVendorClick(txtVendor);
                //parsing failed. 
            }

            if (!string.IsNullOrEmpty(VendorId))
            {
                string[] strIncludeVendorIDs = VendorId.Split(',');
                for (int index = 0; index < strIncludeVendorIDs.Length; index++)
                {
                    ListItem listItem = lstLeftVendor.Items.FindByValue(strIncludeVendorIDs[index]);
                    if (listItem != null)
                    {

                        strVendorId += strIncludeVendorIDs[index] + ",";
                        lstRightVendor.Items.Add(listItem);
                        lstLeftVendor.Items.Remove(listItem);
                    }
                }
            }

            HiddenField hdnSelectedVendor = msVendor.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedVendor.Value = strVendorId;
        }

        //*********** Mediator ***************
        string SelectedMediatorName = (htMediatorPenaltiesReport.ContainsKey("SelectedMediatorName") && htMediatorPenaltiesReport["SelectedMediatorName"] != null) ? htMediatorPenaltiesReport["SelectedMediatorName"].ToString() : "";
        string SelectedMediatorIDs = (htMediatorPenaltiesReport.ContainsKey("SelectedMediatorIDs") && htMediatorPenaltiesReport["SelectedMediatorIDs"] != null) ? htMediatorPenaltiesReport["SelectedMediatorIDs"].ToString() : "";

        ListBox lstRight = msMediator.FindControl("lstRight") as ListBox;
        ListBox lstLeft = msMediator.FindControl("lstLeft") as ListBox;
        string strMediatorId = string.Empty;        
        lstRight.Items.Clear();


        if (lstLeft != null && lstRight != null && !string.IsNullOrEmpty(SelectedMediatorIDs))
            lstRight.Items.Clear();
        msMediator.SearchMediatorClick(SelectedMediatorName);
        {
            string[] strMediatorIDs = SelectedMediatorIDs.Split(',');
            for (int index = 0; index < strMediatorIDs.Length; index++)
            {
                ListItem listItem = lstLeft.Items.FindByValue(strMediatorIDs[index]);
                if (listItem != null)
                {
                    strMediatorId += strMediatorIDs[index] + ",";
                    lstRight.Items.Add(listItem);
                    lstLeft.Items.Remove(listItem);
                }

            }
            HiddenField hdnSelectedMediator = msMediator.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedMediator.Value = strMediatorId;
        }

        //*********** Country ***************
        string CountryIDs = (htMediatorPenaltiesReport.ContainsKey("SelectedCountryIDs") && htMediatorPenaltiesReport["SelectedCountryIDs"] != null) ? htMediatorPenaltiesReport["SelectedCountryIDs"].ToString() : "";
        ListBox lstRightCountry = msCountry.FindControl("lstRight") as ListBox;
        ListBox lstLeftCountry = msCountry.FindControl("lstLeft") as ListBox;
        string msCountryid = string.Empty;
        lstRightCountry.Items.Clear();
        //lstRightSite.Items.Clear();
        msCountry.SelectedCountryIDs = "";
        msCountry.BindCountry();
        if (lstLeftCountry != null && lstRightCountry != null && !string.IsNullOrEmpty(CountryIDs))
        {
            //lstLeftSite.Items.Clear();
            lstRightCountry.Items.Clear();
            // msCountry.BindCountry();

            string[] strCountryIDs = CountryIDs.Split(',');
            for (int index = 0; index < strCountryIDs.Length; index++)
            {
                ListItem listItem = lstLeftCountry.Items.FindByValue(strCountryIDs[index]);
                if (listItem != null)
                {
                    msCountryid = msCountryid + strCountryIDs[index].ToString() + ",";
                    lstRightCountry.Items.Add(listItem);
                    lstLeftCountry.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
                msCountry.SelectedCountryIDs = msCountryid.Trim(',');
        }

        //txtFromDate.Text = (htMediatorPenaltiesReport.ContainsKey("SelectedDateFrom") && htMediatorPenaltiesReport["SelectedDateFrom"] != null) ? htMediatorPenaltiesReport["SelectedDateFrom"].ToString() : null;
        //txtToDate.Text = (htMediatorPenaltiesReport.ContainsKey("SelectedDateTo") && htMediatorPenaltiesReport["SelectedDateTo"] != null) ? htMediatorPenaltiesReport["SelectedDateTo"].ToString() : null;

        //hdnJSFromDt.Value = txtFromDate.Text;
        //hdnJSToDt.Value = txtToDate.Text;

        string[] arrMonthFrom = (htMediatorPenaltiesReport.ContainsKey("SelectedDateFrom") && htMediatorPenaltiesReport["SelectedDateFrom"] != null) ? htMediatorPenaltiesReport["SelectedDateFrom"].ToString().Split('/') : null;

        if (arrMonthFrom != null)
        {
            if (arrMonthFrom[0].Length != 1)
            {
                drpMonthFrom.SelectedValue = arrMonthFrom[0].ToString();
            }
            else
            {
                drpMonthFrom.SelectedValue = "0" + arrMonthFrom[0].ToString();
            }

            drpYearFrom.SelectedValue = arrMonthFrom[2].ToString().Split(' ').ElementAt(0);
        }


        string[] arrMonthTo = (htMediatorPenaltiesReport.ContainsKey("SelectedDateTo") && htMediatorPenaltiesReport["SelectedDateTo"] != null) ? htMediatorPenaltiesReport["SelectedDateTo"].ToString().Split('/') : null;

        if (arrMonthTo != null)
        {
            if (arrMonthTo[0].Length != 1)
            {
                drpMonthTo.SelectedValue = arrMonthTo[0].ToString();
            }
            else
            {
                drpMonthTo.SelectedValue = "0" + arrMonthTo[0].ToString();
            }
            drpYearTo.SelectedValue = arrMonthTo[2].ToString().Split(' ').ElementAt(0);
        }



        string strDisputeType = (htMediatorPenaltiesReport.ContainsKey("DisputeType") && htMediatorPenaltiesReport["DisputeType"] != null) ? htMediatorPenaltiesReport["DisputeType"].ToString() : null;

        if (strDisputeType.Equals("All"))
        {
            rdoAllPenalties.Checked = true;
        }
        else if (strDisputeType.Equals("VendorDidNot"))
        {
            rdoVendorDidNot.Checked = true;
        }
        else if (strDisputeType.Equals("VendorDispute"))
        {
            rdoVendorDispute.Checked = true;
        }
        lblPenaltiesPerMediatorReport.Visible = false;
        lblMediatorPenaltiesReport.Visible = true;

        Session["PlannerPenaltiesReport"] = null;
    }

    private DataTable GetLastYears()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Year");
        int iStartYear = DateTime.Now.Year;
        for (int i = iStartYear; i >= iStartYear - 2; i--)
        {
            if (i >= 2014)
            {
                DataRow dr = dt.NewRow();
                dr["Year"] = i;
                dt.Rows.Add(dr);
            }
        }
        return dt;
    }
}
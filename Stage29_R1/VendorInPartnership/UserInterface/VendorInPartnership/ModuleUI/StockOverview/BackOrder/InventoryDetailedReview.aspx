﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="InventoryDetailedReview.aspx.cs" Inherits="InventoryDetailedReview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        var ValidationMesgArray = new Array();
        function ValidationMessage() {
           
            //            ValidationMesgArray["PleaseEnterYourResponseToTheVendor"] = '<%= PleaseEnterYourResponseToTheVendor %>';
            //            ValidationMesgArray["PleaseEnterPenaltyCharge"] = '<%= PleaseEnterPenaltyCharge %>';
            //            ValidationMesgArray["PleaseEnterComments"] = '<%= PleaseEnterComments %>';
            //            ValidationMesgArray["PleaseEnterForwardDisputeOnto"] = '<%= PleaseEnterForwardDisputeOnto %>';
            //            ValidationMesgArray["ImposedValueShouldEqualToCurrentPenalty"] = '<%= ImposedValueShouldEqualToCurrentPenalty %>';
            //            ValidationMesgArray["ImposedValueCannotEqualGreaterToCurrentPenalty"] = '<%= ImposedValueCannotEqualGreaterToCurrentPenalty %>';
            ValidationMesgArray["SelectCheckboxToAction"] = '<%= SelectCheckboxToAction %>';
            ValidationMesgArray["IsRecordsSelectedtobeUpdated"] = '<%= IsRecordsSelectedtobeUpdated %>';
            ValidationMesgArray["PleaseSelectOption"] = '<%= PleaseSelectOption %>';
            ValidationMesgArray["SelectCheckbox"] = '<%= SelectCheckbox %>';

        }
        function myfunction() {

            //            $("[id$='rblSelect']").click(function () {
            //                debugger;
            if ($('[id$=chkSelect]:checked').length > 1) {
                // debugger;
                $('#<%= rdoPartiallyAgreeWithVendor.ClientID %>').hide();
                $('label[for="ctl00_ContentPlaceHolder1_rdoPartiallyAgreeWithVendor"]').hide();
                return false;
            }
            else {
                $('#<%= rdoPartiallyAgreeWithVendor.ClientID %>').show();
                $('label[for="ctl00_ContentPlaceHolder1_rdoPartiallyAgreeWithVendor"]').show();
                return false;
            }
            //            });
        }
        $(document).ready(function () {

            $("#ctl00_ContentPlaceHolder1_gvDisputeDetail").find("tr").each(function () {
                $(this).find("td:first label").hide();
            });

            $("#trAgree").hide();
            $("#trDisagree").hide();
            $("#trDisagreeDispute").hide();

            var rdoAgreeWithVendor = '<%=rdoAgreeWithVendor.ClientID %>';
            var rdoDisagreeWithVendor = '<%=rdoDisagreeWithVendor.ClientID %>';
            var rdpPartiallyDisagreeWithVendor = '<%=rdoPartiallyAgreeWithVendor.ClientID %>';

            $("#" + rdoAgreeWithVendor).change(function () {
                if ($('[id$=chkSelect]:checked').length == 0) {
                    if ($('#<%= hdnCountOfRowInGrid.ClientID %>').val() == "1") {
                        alert(ValidationMesgArray["SelectCheckbox"]);
                    }
                    else {
                        alert(ValidationMesgArray["SelectCheckboxToAction"]);
                    }
                    $(this).attr("checked", false);
                    return false;
                }
                $("#trAgree").show();
                $("#trDisagree").hide();
            });

            $("#" + rdoDisagreeWithVendor).change(function () {                
                if ($('[id$=chkSelect]:checked').length == 0) {
                    if ($('#<%= hdnCountOfRowInGrid.ClientID %>').val() == "1") {
                        alert(ValidationMesgArray["SelectCheckbox"]);
                    }
                    else {
                        alert(ValidationMesgArray["SelectCheckboxToAction"]);
                    }                    
                    $(this).attr("checked", false);
                    return false;
                }
                $("#trAgree").hide();
                $("#trDisagree").show();                
                $('#trPenlatyChargetobeApplied').hide();
                $("#trDisagreeDisputeNote").show();
                $("#trPartialDisputeNote").hide();
               <%-- document.getElementById("<%=txtPenaltyChargeValue.ClientID %>").value = '';--%>
            });

            $("#" + rdpPartiallyDisagreeWithVendor).change(function () {
                if ($('[id$=chkSelect]:checked').length == 0) {
                    if ($('#<%= hdnCountOfRowInGrid.ClientID %>').val() == "1") {
                        alert(ValidationMesgArray["SelectCheckbox"]);
                    }
                    else {
                        alert(ValidationMesgArray["SelectCheckboxToAction"]);
                    }
                    $(this).attr("checked", false);
                    return false;
                }
                $("#trAgree").hide();
                $("#trDisagree").show();
                $('#trPenlatyChargetobeApplied').show();
                $("#trDisagreeDisputeNote").hide();
                $("#trPartialDisputeNote").show();
                document.getElementById("<%=txtPenaltyChargeValue.ClientID %>").value = '';
            });



        });

        $(document).ready(function () {

            ValidationMessage();

            $("[id$='btnSave']").click(function (e) {
                if ($('[id$=chkSelect]:checked').length == 0) {
                    if ($('#<%= hdnCountOfRowInGrid.ClientID %>').val() == "1") {
                        alert(ValidationMesgArray["SelectCheckbox"]);
                    }
                    else {
                        alert(ValidationMesgArray["SelectCheckboxToAction"]);
                    }
                    return false;
                }

                var objrdo1 = document.getElementById('<%=rdoAgreeWithVendor.ClientID %>');
                var objrdo2 = document.getElementById('<%=rdoDisagreeWithVendor.ClientID %>');
                var objrdo3 = document.getElementById('<%=rdoPartiallyAgreeWithVendor.ClientID %>');

                if (objrdo1.checked == false && objrdo2.checked == false && objrdo3.checked == false) {
                    alert(ValidationMesgArray["PleaseSelectOption"]);
                    return false;
                }

                if ($("[id$='rdoAgreeWithVendor']").is(":checked")) {
                    var txtSPComments = $("[id$='txtSPComments']").val().trim();
                    if (txtSPComments == '') {
                        alert('<%= PleaseEnterYourResponseToTheVendor %>');
                        return false;
                    }
                    if ($('#<%= hdnCountOfRowInGrid.ClientID %>').val() != "1") {
                        var res = confirm(ValidationMesgArray["IsRecordsSelectedtobeUpdated"]);
                        if (res == true) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                }
                else if ($("[id$='rdoPartiallyAgreeWithVendor']").is(":checked")) {
                    // debugger;
                    var txtPenaltyChargeValue = $("[id$='txtPenaltyChargeValue']").val().trim();
                    var txtDisagreeComment = $("[id$='txtDisagreeComment']").val().trim();
                    var ddlODManager = $("[id$='ddlODManager']").val().trim();

                    if (txtPenaltyChargeValue == '') {
                        alert('<%= PleaseEnterPenaltyCharge %>');
                        return false;
                    }
                    if (txtDisagreeComment == '') {
                        alert('<%= PleaseEnterComments %>');
                        return false;
                    }
                    if (ddlODManager == 0) {
                        alert('<%= PleaseEnterForwardDisputeOnto %>');
                        return false;
                    }
                    if ($("[id$='rblSelect']").is(":checked") && $('#<%= hdnCountOfRowInGrid.ClientID %>').val() != "1") {
                        var res = confirm(ValidationMesgArray["IsRecordsSelectedtobeUpdated"]);
                        if (res == true) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }

                }
                else if ($("[id$='rdoDisagreeWithVendor']").is(":checked")) {
                    var txtDisagreeComment = $("[id$='txtDisagreeComment']").val().trim();
                    var ddlODManager = $("[id$='ddlODManager']").val().trim();

                     if (txtDisagreeComment == '') {
                        alert('<%= PleaseEnterComments %>');
                        return false;
                    }
                    if (ddlODManager == 0) {
                        alert('<%= PleaseEnterForwardDisputeOnto %>');
                        return false;
                    }
                    if ($("[id$='rblSelect']").is(":checked") && $('#<%= hdnCountOfRowInGrid.ClientID %>').val() != "1") {
                        var res = confirm(ValidationMesgArray["IsRecordsSelectedtobeUpdated"]);
                        if (res == true) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                }

                return true;
            });
        });
        $(document).ready(function () {


            $("[id$='rblSelect']").click(function () {
                $("[id$='<%=rdoAgreeWithVendor.ClientID %>']").attr('checked', false);
                $("[id$='<%=rdoDisagreeWithVendor.ClientID %>']").attr('checked', false);
                $("[id$='<%=rdoPartiallyAgreeWithVendor.ClientID %>']").attr('checked', false);
            });
        });


        function SetRadioButton(spanChk) {

            $("input[type='radio'][id$='rblSelect']").removeAttr("checked");
            spanChk.checked = true;
        }

        function SPActonReqired(sender, args) {

            var objrdo1 = document.getElementById('<%=rdoAgreeWithVendor.ClientID %>');
            var objrdo2 = document.getElementById('<%=rdoDisagreeWithVendor.ClientID %>');
            var objrdo3 = document.getElementById('<%=rdoPartiallyAgreeWithVendor.ClientID %>');

            if (objrdo1.checked == false && objrdo2.checked == false && objrdo3.checked == false) {
                args.IsValid = false;
            }

        }
        function CalculatePenalty() {

            var PenaltyChargeImposed = document.getElementById("<%=txtPenaltyChargeValue.ClientID %>").value;
            var OriginalPenaltyCharge = document.getElementById("<%=txtDisputedValue.ClientID %>").value;

            var dval = parseFloat(parseFloat(PenaltyChargeImposed).toFixed(2));
            if (!isNaN(dval)) {

                <%--if ($("[id$='rdoDisagreeWithVendor']").is(":checked")) {
                    if (parseFloat(parseFloat(PenaltyChargeImposed).toFixed(2)) != parseFloat(parseFloat(OriginalPenaltyCharge).toFixed(2))) {
                        alert("<%= ImposedValueShouldEqualToCurrentPenalty %>");
                        document.getElementById("<%=txtPenaltyChargeValue.ClientID %>").value = '';
                        $('#' + document.getElementById("<%=txtPenaltyChargeValue.ClientID %>")).focus();
                        return false;
                    }
                }--%>
                if ($("[id$='rdoPartiallyAgreeWithVendor']").is(":checked")) {
                    if (parseFloat(parseFloat(PenaltyChargeImposed).toFixed(2)) >= parseFloat(parseFloat(OriginalPenaltyCharge).toFixed(2))) {
                        alert("<%= ImposedValueCannotEqualGreaterToCurrentPenalty %>");
                        document.getElementById("<%=txtPenaltyChargeValue.ClientID %>").value = '';
                        $('#' + document.getElementById("<%=txtPenaltyChargeValue.ClientID %>")).focus();
                        return false;
                    }
                }
                document.getElementById("<%=txtPenaltyChargeValue.ClientID %>").value = parseFloat(PenaltyChargeImposed).toFixed(2);
            }
            else {
                document.getElementById("<%=txtPenaltyChargeValue.ClientID %>").value = '';
            }
        }
       


    
    </script>
    <h2>
        <cc1:ucLabel ID="lblDisputeDetails" runat="server"></cc1:ucLabel>
        <cc1:ucLabel ID="lblDisputeReview" runat="server" Visible="false"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <div id="divGridView" runat="server">
                <div class="button-row">
                    <br />
                    <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                        Width="109px" Height="20px" />
                </div>
                <cc1:ucPanel ID="pnlQueryDetails" runat="server" CssClass="fieldset-form">
                    <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table">
                        <tr>
                            <td style="font-weight: bold;" colspan="6">
                                <div style="width: 950px; overflow-x: auto;" class="fixedTable">
                                    <cc1:ucGridView ID="gvDisputeDetail" runat="server" CssClass="grid gvclass searchgrid-1"
                                        Width="950px" GridLines="Both" OnPageIndexChanging="gvDisputeDetail_PageIndexChanging"
                                        OnDataBound="gvDisputeDetail_OnDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="View">
                                                <HeaderStyle HorizontalAlign="Left" Width="20px" />
                                                <ItemStyle Width="20px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:RadioButton runat="server" CssClass="innerCheckBox" ID="rblSelect" OnCheckedChanged="rblSelect_CheckedChanged"
                                                        AutoPostBack="true" Text='<%#Eval("PenaltyChargeID") %>' onclick='<%# "javascript:SetRadioButton(this);" %>' />
                                                    <cc1:ucLabel runat="server" ID="lblPenaltyChargeId" Text='<%#Eval("PenaltyChargeID") %>'
                                                        Visible="false"></cc1:ucLabel>
                                                    <asp:HiddenField ID="hdnBOReportingID" Value='<%# Eval("BOReportingID") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SelectAllCheckBox">
                                                <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                <ItemStyle Width="20px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <HeaderTemplate>
                                                    <cc1:ucCheckbox runat="server" ID="chkSelectAllText" Checked="false" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" />
                                                    <asp:HiddenField ID="hdnPenaltyChargeID" runat="server" Value='<%# Eval("PenaltyChargeID") %>' />
                                                    <asp:HiddenField ID="hdnPotentialPenaltyChargeTotal" runat="server" Value='<%# Eval("PotentialPenaltyChargeTotal") %>' />
                                                    <%-- <asp:HiddenField ID="hdnDisputeTotal" runat="server" Value='<%# Eval("DisputeTotal") %>' />--%>
                                                    <%-- <asp:HiddenField ID="hdnPotentialPenaltyChargeTotal" runat="server" Value='<%# Eval("PotentialPenaltyChargeTotal") %>' />--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ODSKU" SortExpression="SKU.OD_SKU_NO">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltodsku" Text='<%#Eval("SKU.OD_SKU_NO") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VikingSku" SortExpression="SKU.Direct_SKU">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVikingSku" Text='<%#Eval("SKU.Direct_SKU") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorCode" SortExpression="SKU.Vendor_Code">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendorCode" Text='<%#Eval("SKU.Vendor_Code") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description" SortExpression="SKU.DESCRIPTION">
                                                <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltDescription" Text='<%#Eval("SKU.DESCRIPTION") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DateBOIncurred" SortExpression="BOIncurredDate">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltDateBOIncurred" Text='<%#Eval("BOIncurredDate", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Site" SortExpression="SKU.SiteName">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltsite" Text='<%#Eval("SKU.SiteName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CountofBO(s)Incurred" SortExpression="NoofBO">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltCountofBoIncurred" Text='<%#Eval("NoofBO") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" Width="60px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="QtyofBO(s)Incurred" SortExpression="QtyonBackOrder">
                                                <HeaderStyle Width="60px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt4" Text='<%#Convert.ToInt64(Math.Round(Convert.ToDouble(Eval("QtyonBackOrder")))) %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" Width="60px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="Vendor.StockPlannerName">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockPlannerName" SortExpression="Vendor.StockPlannerNumber">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                                                <HeaderStyle Width="70px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="70px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OpenPONumbers" SortExpression="PurchaseOrder.Purchase_order">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltOpenPONo" Text='<%#Eval("PurchaseOrder.Purchase_order") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OrderRaisedDate" SortExpression="PurchaseOrder.Order_raised">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt5" Text='<%#Eval("PurchaseOrder.Order_raised", "{0:dd/MM/yyyy}") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OriginalDueDate" SortExpression="PurchaseOrder.Original_due_date">
                                                <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="lt7" Text='<%#Eval("PurchaseOrder.Original_due_date", "{0:dd/MM/yyyy}")%>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OutstandingQtyonPO" SortExpression="PurchaseOrder.Outstanding_Qty">
                                                <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltOutstandingQty" Text='<%#Eval("PurchaseOrder.Outstanding_Qty") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="40px" Font-Size="10px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ChargeIncurred" SortExpression="PotentialPenaltyCharge">
                                                <HeaderStyle Width="40px" HorizontalAlign="Left" Font-Size="10px" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltPotentialPenaltyCharge" Text='<%#Eval("PotentialPenaltyCharge") %>'></cc1:ucLabel>
                                                    <asp:HiddenField ID="hdnPotentialPenaltyCharge" runat="server" Value='<%# Eval("PotentialPenaltyCharge") %>' />
                                                    <asp:HiddenField ID="hdnDisputedValue" runat="server" Value='<%# Eval("DisputedValue") %>' />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" Width="40px" Font-Size="10px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </cc1:ucGridView>
                                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                                    </cc1:PagerV2_8>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 19%">
                                <cc1:ucLabel ID="lblOriginalPenaltyValue" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 27%">
                                <cc1:ucTextbox ID="txtOriginalPenaltyValue" runat="server" Width="100px" ReadOnly="true"></cc1:ucTextbox>
                            </td>
                            <td style="width: 19%">
                                <cc1:ucLabel ID="lblValueInDispute" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold; width: 1%">
                                :
                            </td>
                            <td style="width: 33%">
                                <cc1:ucTextbox ID="txtValueInDispute" runat="server" Width="100px" ReadOnly="true"></cc1:ucTextbox>
                                <cc1:ucTextbox ID="txtRevisedPenalty" runat="server" Width="100px" Visible="false"></cc1:ucTextbox>
                                <asp:HiddenField runat="server" ID="hdnsiteid" />
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
            </div>
            <cc1:ucPanel ID="pnlVendorsQuery" runat="server" CssClass="fieldset-form" GroupingText="VendorsQuery">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                    <tr>
                        <td style="font-weight: bold; width: 24%">
                            <cc1:ucLabel ID="lblRaisedOn" runat="server" Text="Raised on"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="width: 25%">
                            <asp:Literal ID="ltRaisedOn" runat="server"></asp:Literal>
                        </td>
                        <td style="font-weight: bold; width: 19%">
                            <cc1:ucLabel ID="lblRaisedBy" runat="server" Text="Raised by"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="width: 30%">
                            <asp:Literal ID="ltRaisedBy" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <cc1:ucLabel ID="lblContactDetail" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td>
                            <asp:Literal ID="ltContactDetail" runat="server"></asp:Literal>
                        </td>
                        <td style="font-weight: bold">
                            <cc1:ucLabel ID="lblPhoneNumber" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold">
                            :
                        </td>
                        <td>
                            <asp:Literal ID="ltPhoneNumber" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <cc1:ucLabel ID="lblQuery" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold" colspan="6">
                            :
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="font-weight: bold">
                            <cc1:ucTextarea ID="txtQuery" runat="server" Height="70px" Width="860px" ReadOnly="true"></cc1:ucTextarea>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlStockPlannerResponse_TobeTaken" runat="server" CssClass="fieldset-form"
                GroupingText="StockPlannerResponse">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                    <tr>
                        <td colspan="3" style="font-weight: bold">
                            <cc1:ucLabel ID="lblMsgQueryReview" runat="server" isRequired="true"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="font-weight: bold">
                            <cc1:ucRadioButton ID="rdoAgreeWithVendor" runat="server" GroupName="Agree" />&nbsp;&nbsp;
                            <cc1:ucRadioButton ID="rdoDisagreeWithVendor" runat="server" GroupName="Agree" />&nbsp;&nbsp;
                            <cc1:ucRadioButton ID="rdoPartiallyAgreeWithVendor" runat="server" GroupName="Agree" />
                        </td>
                    </tr>
                    <tr id="trAgree">
                        <td colspan="3">
                            <table width="95%" cellspacing="15" cellpadding="0" border="0" align="left" class="top-settings">
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblresponsetothevendor" runat="server"></cc1:ucLabel>
                                        :
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">
                                        <cc1:ucTextbox ID="txtSPComments" CssClass="inputbox textarea" runat="server" onkeyup="checkTextLengthOnKeyUp(this,1000);"
                                            TextMode="MultiLine" Height="70px" Width="850px"></cc1:ucTextbox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trDisagree">
                        <td>
                            <table width="95%" cellspacing="15" cellpadding="0" border="0" align="left" class="top-settings">
                                <tr>
                                    <td style="font-weight: bold" width="39%">
                                        <cc1:ucLabel ID="lblpenaltychargeappliedmonth" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold" width="1%">
                                        :
                                    </td>
                                    <td style="font-weight: bold" width="60%">
                                        <cc1:ucTextbox runat="server" ID="txtDisputedValue" ReadOnly="true" Width="100px"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr id="trDisagreeDisputeNote">
                                    <td colspan="3" style="font-weight: bold">
                                        <cc1:ucLabel ID="lblDisagreeDisputeNote" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr id="trPartialDisputeNote">
                                    <td colspan="3" style="font-weight: bold">
                                        <cc1:ucLabel ID="lblPartialAgreeDisputeNote" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr id="trPenlatyChargetobeApplied">
                                    <td style="font-weight: bold">
                                        <cc1:ucLabel ID="lblpenaltychargetobeapplied" runat="server"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold">
                                        :
                                    </td>
                                    <td style="font-weight: bold">
                                        <cc1:ucTextbox runat="server" ID="txtPenaltyChargeValue" Width="100px" onkeyup="AllowDecimalOnly(this);"
                                            MaxLength="10" onblur="CalculatePenalty();"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="font-weight: bold">
                                        <cc1:ucLabel ID="lblComment" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <cc1:ucTextbox ID="txtDisagreeComment" CssClass="inputbox textarea" runat="server"
                                            onkeyup="checkTextLengthOnKeyUp(this,1000);" TextMode="MultiLine" Height="70px"
                                            Width="850px"></cc1:ucTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="font-weight: bold">
                                        <cc1:ucLabel ID="lblForwardDisputeTo" runat="server"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <cc1:ucDropdownList ID="ddlODManager" runat="server" Width="200px">
                                        </cc1:ucDropdownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSaveSPAction_Click"
                                    ValidationGroup="ValidSPAction" />
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                               <%-- <asp:CustomValidator ID="cusvPleaseSelectOption" runat="server" Text="Please select Option."
                                    ClientValidationFunction="SPActonReqired" Display="None" ValidationGroup="ValidSPAction">
                                </asp:CustomValidator>--%>
                                <asp:ValidationSummary ID="VSValidDispute" runat="server" ValidationGroup="ValidSPAction"
                                    ShowSummary="false" ShowMessageBox="true" />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="pnlStockPannerResponse" runat="server" CssClass="fieldset-form"
                GroupingText="StockPannerResponse">
                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="form-table">
                    <tr>
                        <td colspan="6" style="font-weight: bold">
                            <cc1:ucLabel ID="lblSPAction" runat="server" isRequired="true"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <cc1:ucLabel ID="lblDate" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td colspan="3" style="font-weight: bold">
                            <cc1:ucLabel ID="lblDateValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr style="font-weight: bold">
                        <td style="width: 24%">
                            <cc1:ucLabel ID="lblStockPlannerNumber" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="width: 25%; font-weight: bold">
                            <cc1:ucLabel ID="lblSPNoValue" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width: 19%">
                            <cc1:ucLabel ID="lblStockPlannerName" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td style="width: 30%; font-weight: bold">
                            <cc1:ucLabel ID="lblSPNameValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr id="trPenaltyImposed" runat="server">
                        <td style="font-weight: bold">
                            <cc1:ucLabel ID="lblpenaltychargetobeapplied_1" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td colspan="4" style="font-weight: bold">
                            <cc1:ucLabel ID="lblPenaltyChargeVal" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            <cc1:ucLabel ID="lblComments_1" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold" colspan="6">
                            <cc1:ucTextarea ID="txtCommentsValue" runat="server" Height="70px" Width="860px"
                                ReadOnly="true"></cc1:ucTextarea>
                        </td>
                    </tr>
                    <tr id="trEsclatedTo" runat="server">
                        <td style="font-weight: bold">
                            <cc1:ucLabel ID="lblDisputeEsclatedto" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            :
                        </td>
                        <td colspan="4" style="font-weight: bold">
                            <cc1:ucLabel ID="lblDisputeEscVal" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                        </td>
                    </tr>
                    <%--<tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="UcButton1" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                <cc1:ucButton ID="UcButton2" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
                            </div>
                        </td>
                    </tr>--%>
                </table>
            </cc1:ucPanel>
            <div class="button-row">
                <cc1:ucButton ID="btnBack_ToMainPage" runat="server" CssClass="button" OnClick="btnBack_Click" />
            </div>
        </div>
        <asp:HiddenField ID="hdnPotentialPenaltyChargeValue" runat="server" />
        <asp:HiddenField ID="hdnDisputedChargeValue" runat="server" />
        <asp:HiddenField ID="hdnCountOfRowInGrid" runat="server" />
    </div>
    <script type="text/javascript">
        $(document).ready(function () {

            var PotentialPenaltyCharge = 0.00;
            var DisputedValue = 0.00;
            var isMasterchecked = false;

            $('[id$=chkSelectAllText]').click(function () {
                // debugger;
                if (this.checked == true) {
                    $("[id$='chkSelect']").attr('checked', this.checked);

                    isMasterchecked = true;
                    if ($('[id$=chkSelect]:checked').length > 1) {                        

                        $('#<%= rdoPartiallyAgreeWithVendor.ClientID %>').hide();
                        $('label[for="ctl00_ContentPlaceHolder1_rdoPartiallyAgreeWithVendor"]').hide()
                        $("#trAgree").hide();
                        $("#trDisagree").hide();
                        $("#trPartialDisputeNote").hide();
                        $('#<%= rdoAgreeWithVendor.ClientID %>').attr('checked', false);
                        $('#<%= rdoDisagreeWithVendor.ClientID %>').attr('checked', false);
                        $('#<%= rdoPartiallyAgreeWithVendor.ClientID %>').attr('checked', false);
                    }
                }
                else {                    
                    $("[id$='chkSelect']").attr('checked', false);
                    $('#<%= rdoPartiallyAgreeWithVendor.ClientID %>').show();
                    $('label[for="ctl00_ContentPlaceHolder1_rdoPartiallyAgreeWithVendor"]').show()
                    $("#trAgree").hide();
                    $("#trDisagree").hide();
                    $("#trPartialDisputeNote").hide();
                    $('#<%= rdoAgreeWithVendor.ClientID %>').attr('checked', false);
                    $('#<%= rdoDisagreeWithVendor.ClientID %>').attr('checked', false);
                    $('#<%= rdoPartiallyAgreeWithVendor.ClientID %>').attr('checked', false);
                }



                $("[id$='chkSelect']").trigger("change");

                //                $('#<%= rdoPartiallyAgreeWithVendor.ClientID %>').text("");

                //                var PotentialChargeValue = 0.0;
                //                $("input[name$=chkSelect]:checked").each(function () {
                //                    debugger;
                //                    PotentialChargeValue = PotentialChargeValue + $(this).next("input[name$=hdnDisputeValue]").val()
                //                });



            });
            $("[id$='chkSelect']").click(function () {
                isMasterchecked = false;
                if ($('[id$=chkSelect]:checked').length == $('[id$=chkSelect]').length) {
                    $("[id$=chkSelectAllText]").attr('checked', true);
                }
                if (!$(this).is(":checked")) {
                    $("[id$=chkSelectAllText]").attr('checked', false);
                }

                if ($('[id$=chkSelect]:checked').length > 1) {                    
                    $('#<%= rdoPartiallyAgreeWithVendor.ClientID %>').hide();
                    $('label[for="ctl00_ContentPlaceHolder1_rdoPartiallyAgreeWithVendor"]').hide()
                    $("#trAgree").hide();
                    $("#trDisagree").hide();
                    $("#trPartialDisputeNote").hide();
                    $('#<%= rdoAgreeWithVendor.ClientID %>').attr('checked', false);
                    $('#<%= rdoDisagreeWithVendor.ClientID %>').attr('checked', false);
                    $('#<%= rdoPartiallyAgreeWithVendor.ClientID %>').attr('checked', false);
                }
                else if ($('[id$=chkSelect]:checked').length == 0) {                    
                    $("#trAgree").hide();
                    $("#trDisagree").hide();
                    $("#trPartialDisputeNote").hide();
                    $('#<%= rdoAgreeWithVendor.ClientID %>').attr('checked', false);
                    $('#<%= rdoDisagreeWithVendor.ClientID %>').attr('checked', false);
                    $('#<%= rdoPartiallyAgreeWithVendor.ClientID %>').attr('checked', false);
                }
                else {                    
                    $('#<%= rdoPartiallyAgreeWithVendor.ClientID %>').show();
                    $('label[for="ctl00_ContentPlaceHolder1_rdoPartiallyAgreeWithVendor"]').show()
                }
            });

            //            $('[id$=rblSelect]').click(function () {
            //                $("[id$='chkSelectAllText']").attr('checked', false);
            //                $("[id$='chkSelect']").attr('checked', false);
            //            

            //            });

            $(document).ready(function () {
                $('#<%=gvDisputeDetail.ClientID%>').find('input:checkbox[id$="chkSelect"]').bind('change', function (e) {
                    // debugger;
                    var iRowIndex = $(this).closest("tr").prevAll("tr").length;
                    iRowIndex = parseInt(iRowIndex) + 1;
                    if (parseInt(iRowIndex) < 10) {
                        iRowIndex = "0" + iRowIndex;
                    }
                    var PotentialPenaltyChargeTotal = $("#ctl00_ContentPlaceHolder1_gvDisputeDetail_ctl" + iRowIndex + "_hdnPotentialPenaltyCharge").val();
                    var DisputedValueTotal = $("#ctl00_ContentPlaceHolder1_gvDisputeDetail_ctl" + iRowIndex + "_hdnDisputedValue").val();



                    if (PotentialPenaltyCharge == 0) {
                        //debugger;
                        if ($('#<%= hdnPotentialPenaltyChargeValue.ClientID %>').val() != "") /// this condition for the first time check
                        {
                            PotentialPenaltyCharge = $('#<%= hdnPotentialPenaltyChargeValue.ClientID %>').val();
                            //$('#<%= txtDisputedValue.ClientID %>').val($('#<%= hdnPotentialPenaltyChargeValue.ClientID %>').val());
                        }
                    }
                    if (DisputedValue == 0) {
                        if ($('#<%= hdnDisputedChargeValue.ClientID %>').val() != "") /// this condition for the first time check
                        {
                            DisputedValue = $('#<%= hdnDisputedChargeValue.ClientID %>').val();
                        }
                    }
                    if (isMasterchecked == true) {
                        isMasterchecked = false;
                        PotentialPenaltyCharge = 0;
                        DisputedValue = 0;
                    }
                    if ($(this).is(':checked')) {

                        PotentialPenaltyCharge = parseFloat(PotentialPenaltyCharge) + parseFloat(PotentialPenaltyChargeTotal);
                        DisputedValue = parseFloat(DisputedValue) + parseFloat(DisputedValueTotal);
                    }
                    else {
                        PotentialPenaltyCharge = parseFloat(PotentialPenaltyCharge) - parseFloat(PotentialPenaltyChargeTotal);
                        DisputedValue = parseFloat(DisputedValue) - parseFloat(DisputedValueTotal);
                    }
                    $('#<%= hdnPotentialPenaltyChargeValue.ClientID %>').val(PotentialPenaltyCharge.toFixed(2));
                    $('#<%= hdnDisputedChargeValue.ClientID %>').val(DisputedValue.toFixed(2))

                    $('#<%= txtOriginalPenaltyValue.ClientID %>').val($('#<%= hdnPotentialPenaltyChargeValue.ClientID %>').val());
                    $('#<%= txtDisputedValue.ClientID %>').val($('#<%= hdnPotentialPenaltyChargeValue.ClientID %>').val());
                    $('#<%= txtValueInDispute.ClientID %>').val($('#<%= hdnDisputedChargeValue.ClientID %>').val());
                    // PotentialPenaltyCharge = $('#<%= hdnPotentialPenaltyChargeValue.ClientID %>').val();
                    // DisputedValue = $('#<%= hdnDisputedChargeValue.ClientID %>').val();
                });

            });
            if ($('#<%= hdnCountOfRowInGrid.ClientID %>').val() == "1") /// this condition for the first time check
            {
                // debugger;                
                $("[id$='chkSelectAllText']").attr('checked', true);
                $("[id$='chkSelect']").attr('checked', true);

                $('#<%=gvDisputeDetail.ClientID%>').find('input:checkbox[id$="chkSelect"]').trigger("change");

                $('#<%= rdoPartiallyAgreeWithVendor.ClientID %>').show();
                $('label[for="ctl00_ContentPlaceHolder1_rdoPartiallyAgreeWithVendor"]').show()
            }

        });

    </script>
</asp:Content>

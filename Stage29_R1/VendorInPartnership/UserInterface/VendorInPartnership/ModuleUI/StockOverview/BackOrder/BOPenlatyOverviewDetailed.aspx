﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BOPenlatyOverviewDetailed.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
 Inherits="ModuleUI_StockOverview_BackOrder_BOPenlatyOverviewDetailed" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<%@ Register Src="~/CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h2>
        <cc1:ucLabel ID="lblBOPenaltyOverviewDetailed" runat="server"></cc1:ucLabel>
    </h2>
     <div class="button-row">       
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
     <br />
           <div style="overflow-x:scroll;">
    <table width="100%">
        <tr>
            <td align="center">
                <cc1:ucGridView ID="grdBOPenaltyDetailed" Width="100%" runat="server" CssClass="grid" AllowPaging="true" PageSize="50" 
                 OnPageIndexChanging="grdBOPenaltyDetailed_PageIndexChanging">
                 <EmptyDataTemplate>
                    <div style="text-align: center">
                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                    </div>
                   </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Site" >
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSiteName" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="OD SKU" >
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSKU_No" runat="server" Text='<%# Eval("SKU_No") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Viking SKU" >
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltViking" runat="server" Text='<%# Eval("Viking") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DescriptionNew">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltDescription" runat="server" Text='<%# Eval("Description") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Vendor">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltVendor" runat="server" Text='<%# Eval("Vendor.VendorName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="BO Count">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltBOCount" runat="server" Text='<%# Eval("NoofBO") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="BO Qty">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltBOQty" runat="server" Text='<%# Eval("QtyOnBO") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- <asp:TemplateField HeaderText="BO with Penalties">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltBOwithPenalties" runat="server" Text='<%# Eval("NoofBOWithPenalities") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                           <asp:TemplateField HeaderText="# Days">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltTotalDays" runat="server" Text='<%# Eval("TotalDays") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="First BO">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltFirstBO" runat="server" Text='<%# Eval("FirstBO") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Stock Planner">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltStockPlanner" runat="server" Text='<%# Eval("Vendor.StockPlannerName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SP Action">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSPAction" runat="server" Text='<%# Eval("InventoryInitialReview") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                         </asp:TemplateField>
                          <asp:TemplateField HeaderText="SP 1st Action">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSP1stAction" runat="server"  Text='<%# Convert.ToString(Eval("InventoryInitialActionDate")).Equals("01/01/1900") ? "" : Eval("InventoryInitialActionDate") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Charge">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltCharge" runat="server" Text='<%# Eval("PotentialPenaltyCharge") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Vendor Action">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltVendorAction" runat="server" Text='<%# Eval("VendorReview") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Mediator Status">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltMediatorStatus" runat="server" Text='<%# Eval("MediatorReview") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField> 
                         <asp:TemplateField HeaderText="Pending With">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltPendingWith" runat="server" Text='<%# Eval("PenaltyPendingWith") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Status">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltStatus" runat="server" Text='<%# Eval("PenaltyStatus") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField> 

                        
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
    </div>

     <table width="100%">
        <tr>
            <td align="center">
                <cc1:ucGridView ID="gvExport" Width="100%" runat="server" CssClass="grid" Style="display: none;">                 
                    <Columns>
                        <asp:TemplateField HeaderText="Site" >
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSiteName" runat="server" Text='<%# Eval("SiteName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="OD SKU" >
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSKU_No" runat="server" Text='<%# Eval("SKU_No") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Viking SKU" >
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltViking" runat="server" Text='<%# Eval("Viking") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DescriptionNew">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltDescription" runat="server" Text='<%# Eval("Description") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Vendor">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltVendor" runat="server" Text='<%# Eval("Vendor.VendorName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="BO Count">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltBOCount" runat="server" Text='<%# Eval("NoofBO") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="BO Qty">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltBOQty" runat="server" Text='<%# Eval("QtyOnBO") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="BO with Penalties">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltBOwithPenalties" runat="server" Text='<%# Eval("NoofBOWithPenalities") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                           <asp:TemplateField HeaderText="# Days">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltTotalDays" runat="server" Text='<%# Eval("TotalDays") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="First BO">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltFirstBO" runat="server" Text='<%# Eval("FirstBO") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Stock Planner">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltStockPlanner" runat="server" Text='<%# Eval("Vendor.StockPlannerName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SP Action">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSPAction" runat="server" Text='<%# Eval("InventoryInitialReview") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                         </asp:TemplateField>
                          <asp:TemplateField HeaderText="SP 1st Action">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltSP1stAction" runat="server"  Text='<%# Convert.ToString(Eval("InventoryInitialActionDate")).Equals("01/01/1900") ? "" : Eval("InventoryInitialActionDate") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Charge">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltCharge" runat="server" Text='<%# Eval("PotentialPenaltyCharge") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Vendor Action">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltVendorAction" runat="server" Text='<%# Eval("VendorReview") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Mediator Status">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltMediatorStatus" runat="server" Text='<%# Eval("MediatorReview") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField> 
                         <asp:TemplateField HeaderText="Pending With">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltPendingWith" runat="server" Text='<%# Eval("PenaltyPendingWith") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Status">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltStatus" runat="server" Text='<%# Eval("PenaltyStatus") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField> 

                        
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>


                    <div class="button-row"> 
                                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                  </div>
</asp:Content>
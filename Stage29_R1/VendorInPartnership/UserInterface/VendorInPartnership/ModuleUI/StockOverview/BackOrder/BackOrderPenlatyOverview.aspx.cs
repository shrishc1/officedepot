﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Data;
using WebUtilities;
using Utilities;
using System.Collections;

public partial class ModuleUI_StockOverview_BackOrder_BackOrderPenlatyOverview : CommonPage
{

    protected string RecordNotFound = WebCommon.getGlobalResourceValue("RecordNotFound");
    protected string BOPenaltyOverviewSummary = WebCommon.getGlobalResourceValue("BOPenaltyOverviewSummary");
  
    //protected string IsfromPage = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //ucExportToExcel1.CurrentPage = this;
        //ucExportToExcel1.GridViewControl = grdBindExport;
        //ucExportToExcel1.IsAutoGenratedGridview = true;
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            pager1.PageSize = 50;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;

            if (GetQueryStringValue("InventoryGroup") != null && GetQueryStringValue("InventoryGroup") !="")
            {               
                if (GetQueryStringValue("rblStatus") != null)
                {
                    int rblStatus = Convert.ToInt32(GetQueryStringValue("rblStatus"));
                    rdoInventoryGroupView.Checked = false;
                    rdoSiteView.Checked = false;
                    rdoPlannerView.Checked = true;
                    rdoVendorView.Checked = false;
                   // IsfromPage = "Inventory";
                    rblApproveType.SelectedValue = Convert.ToString(rblStatus);
                    BindOnQueryString("ByPlanner", rblStatus);
                    
                }               
            }

            if (GetQueryStringValue("PlannerName") != null)
            {
                if (GetQueryStringValue("rblStatus") != null)
                {
                    int rblStatus = Convert.ToInt32(GetQueryStringValue("rblStatus"));
                    rdoPlannerView.Checked = false;
                    rdoInventoryGroupView.Checked = false;
                    rdoSiteView.Checked = false;
                    rdoVendorView.Checked = true;
                    //IsfromPage = "Vendor";
                    rblApproveType.SelectedValue = Convert.ToString(rblStatus);
                    BindOnQueryString("ByVendor", rblStatus);
                    
                }
            }

            //if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage") == "BOPenaltyOverviewDetailed")
            //{                
            //    BindGridview();
            //}
        }

        //if (rdoSummaryLevel.Checked)
        //{
        //    if (rdoSiteView.Checked)
        //    {               
        //        ucExportToExcel1.FileName = "Site_SummaryView";
        //    }
        //    else if (rdoInventoryGroupView.Checked)
        //    {               
        //        ucExportToExcel1.FileName = "InventoryGroup_SummaryView";
        //    }
        //    else if (rdoPlannerView.Checked)
        //    {               
        //        ucExportToExcel1.FileName = "StockPlanner_SummaryView";
        //    }
        //    else if (rdoVendorView.Checked)
        //    {               
        //        ucExportToExcel1.FileName = "Vendor_SummaryView";
        //    }
        //}



    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("PreviousPage") != null && (GetQueryStringValue("PreviousPage") == "BOPenaltyOverviewDetailed" || GetQueryStringValue("PreviousPage") == "BOPenaltySummary"))
            {
                if (Session["BOPenaltyOverview"] != null)
                {
                    RetainSearchData();
                }
            }
        }
    }
    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        try
        {
            if (rdoDetailedLevel.Checked == false)
            {
                BindGridview();
            }
            else
            {
                BackOrderBE oBackOrderBE = new BackOrderBE();
                BackOrderBAL oBackOrderBAL = new BackOrderBAL();
                
                string SelectedSiteIDs = null; 
                string SelectedCountryIDs = null;
                string SelectedStockPlannerIDs = null;
                string SelectedStockPlannerGroupIDs = null;

                if (!(string.IsNullOrEmpty(msSite.SelectedSiteIDs)))
                {
                    oBackOrderBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
                    SelectedSiteIDs = msSite.SelectedSiteIDs; 
                }

                if (!(string.IsNullOrEmpty( msCountry.SelectedCountryIDs)))
                {
                    oBackOrderBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;
                    SelectedCountryIDs = msCountry.SelectedCountryIDs;
                }
                if (!(string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs)))
                {
                    oBackOrderBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
                    SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
                }
                if (!(string.IsNullOrEmpty(multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs)))
                {
                    oBackOrderBE.SelectedStockPlannerGroupID = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
                    SelectedStockPlannerGroupIDs = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
                }
                

                oBackOrderBE.InventoryReviewStatus = rblApproveType.SelectedValue == "1" ? "All" : (rblApproveType.SelectedValue == "2" ? "Approved" : (rblApproveType.SelectedValue == "3" ? "Declined" : (rblApproveType.SelectedValue == "4" ? "Pending" : "")));

                oBackOrderBE.SelectedDateFrom = Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
                oBackOrderBE.SelectedDateTo = Common.GetMM_DD_YYYY(hdnJSToDt.Value);
                

                this.SetSession(oBackOrderBE);

                EncryptQueryString("BOPenlatyOverviewDetailed.aspx?PageFrom=BOPenaltyOverview" + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "&rblStatus=" + rblApproveType.SelectedItem + "&SelectedSiteIDs=" + SelectedSiteIDs
                    + "&SelectedCountryIDs=" + SelectedCountryIDs + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs);
               
            
            }
        }
        catch (Exception)
        {
        }
    }

    public void BindGridview(int page = 1)
    {
        divSummaryGrid.Visible = true;
        btnBack.Visible = true;
        BackOrderBE oBackOrderBE = new BackOrderBE();
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();

        // if (GetQueryStringValue("PreviousPage") != null && GetQueryStringValue("PreviousPage") == "BOPenaltyOverviewDetailed")
        //{

        //    if (Session["BOPenaltyOverview"] != null)
        //    {

        //        Hashtable htBOPenaltyOverview = (Hashtable)Session["BOPenaltyOverview"];

        //        //oBackOrderBE.SelectedCountryIDs = (htBOPenaltyOverview.ContainsKey("SelectedCountryIDs") && htBOPenaltyOverview["SelectedCountryIDs"] != null) ? htBOPenaltyOverview["SelectedCountryIDs"].ToString() : null;
        //        //oBackOrderBE.SelectedCountryName = (htBOPenaltyOverview.ContainsKey("SelectedCountryName") && htBOPenaltyOverview["SelectedCountryName"] != null) ? htBOPenaltyOverview["SelectedCountryName"].ToString() : null;
        //        //oBackOrderBE.SelectedSiteIDs = (htBOPenaltyOverview.ContainsKey("SelectedSiteId") && htBOPenaltyOverview["SelectedSiteId"] != null) ? htBOPenaltyOverview["SelectedSiteId"].ToString() : null;
        //        //oBackOrderBE.SiteName = (htBOPenaltyOverview.ContainsKey("SelectedSiteName") && htBOPenaltyOverview["SelectedSiteName"] != null) ? htBOPenaltyOverview["SelectedSiteName"].ToString() : null;
        //        //oBackOrderBE.SelectedStockPlannerIDs = (htBOPenaltyOverview.ContainsKey("SelectedSPIDs") && htBOPenaltyOverview["SelectedSPIDs"] != null) ? htBOPenaltyOverview["SelectedSPIDs"].ToString() : null;
        //        //oBackOrderBE.SelectedStockPlannerName = (htBOPenaltyOverview.ContainsKey("SelectedSPName") && htBOPenaltyOverview["SelectedSPName"] != null) ? htBOPenaltyOverview["SelectedSPName"].ToString() : null;
        //        //oBackOrderBE.SelectedStockPlannerGroupID = (htBOPenaltyOverview.ContainsKey("SelectedStockPlannerGroupID") && htBOPenaltyOverview["SelectedStockPlannerGroupID"] != null) ? htBOPenaltyOverview["SelectedStockPlannerGroupID"].ToString() : null;
        //        //oBackOrderBE.SelectedStockPlannerGroupName = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingName;

        //        txtFromDate.Text = (htBOPenaltyOverview.ContainsKey("SelectedDateFrom") && htBOPenaltyOverview["SelectedDateFrom"] != null) ? htBOPenaltyOverview["SelectedDateFrom"].ToString() : null;
        //        txtToDate.Text = (htBOPenaltyOverview.ContainsKey("SelectedDateTo") && htBOPenaltyOverview["SelectedDateTo"] != null) ? htBOPenaltyOverview["SelectedDateTo"].ToString() : null;

        //        hdnJSFromDt.Value = txtFromDate.Text;
        //        hdnJSToDt.Value = txtToDate.Text;

        //        //msCountry.SelectedCountryIDs = oBackOrderBE.SelectedCountryIDs;
        //        //msCountry.SelectedCountryName = oBackOrderBE.SelectedCountryName;
        //        //msSite.SelectedSiteIDs = oBackOrderBE.SelectedSiteIDs;
        //        //msSite.SelectedSiteName = oBackOrderBE.SiteName;
        //        //msStockPlanner.SelectedStockPlannerIDs = oBackOrderBE.SelectedStockPlannerIDs;
        //        //msStockPlanner.SelectedSPName = oBackOrderBE.SelectedStockPlannerName;
        //        //multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs = oBackOrderBE.SelectedStockPlannerGroupID;
        //        //multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingName = oBackOrderBE.SelectedStockPlannerGroupName;
        //    }
        //}


        //else
        //{
        oBackOrderBE.Action = "GetBackOrderPenaltySummaryView";

        oBackOrderBE.SelectedDateFrom = Common.GetMM_DD_YYYY(hdnJSFromDt.Value);
        oBackOrderBE.SelectedDateTo = Common.GetMM_DD_YYYY(hdnJSToDt.Value);

        if (!(string.IsNullOrEmpty(msSite.SelectedSiteIDs)))
        {
            oBackOrderBE.SelectedSiteIDs = msSite.SelectedSiteIDs;            
        }

        if (!(string.IsNullOrEmpty(msCountry.SelectedCountryIDs)))
        {
            oBackOrderBE.SelectedCountryIDs = msCountry.SelectedCountryIDs;            
        }
        if (!(string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs)))
        {
            oBackOrderBE.SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;            
        }
        if (!(string.IsNullOrEmpty(multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs)))
        {
            oBackOrderBE.SelectedStockPlannerGroupID = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;            
        }       

        oBackOrderBE.InventoryReviewStatus = rblApproveType.SelectedValue == "1" ? "All" : (rblApproveType.SelectedValue == "2" ? "Approved" : (rblApproveType.SelectedValue == "3" ? "Declined" : (rblApproveType.SelectedValue == "4" ? "Pending" : "")));

        if (rdoSummaryLevel.Checked)
        {
            if (rdoSiteView.Checked)
            {
                oBackOrderBE.SummaryBy = "BySite";
                //ucExportToExcel1.FileName = "Site_SummaryView";               
            }
            else if (rdoInventoryGroupView.Checked)
            {
                oBackOrderBE.SummaryBy = "ByInventory";               
                // IsfromPage = "Inventory";
               // ucExportToExcel1.FileName = "InventoryGroup_SummaryView";
            }
            else if (rdoPlannerView.Checked)
            {
                oBackOrderBE.SummaryBy = "ByPlanner";               
               // ucExportToExcel1.FileName = "StockPlanner_SummaryView";
            }
            else if (rdoVendorView.Checked)
            {
                oBackOrderBE.SummaryBy = "ByVendor";             
               // ucExportToExcel1.FileName = "Vendor_SummaryView";
            }

            lblBackOrderPenaltyOverview.Text = BOPenaltyOverviewSummary;
        }
        DataSet ds = oBackOrderBAL.GetBackOrderPenaltyReportBAL(oBackOrderBE);
        this.SetSession(oBackOrderBE);
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["DataSource"] = ds.Tables[0];
                Session["InputFields"] = oBackOrderBE;
                grdBind.DataSource = ds.Tables[0];
                grdBind.DataBind();
                grdBindExport.DataSource = ds.Tables[0];
                grdBindExport.DataBind();
                lblError.Text = "";
                lblError.Visible = false;
                tblSearch.Visible = false;
                btnExportToExcel.Visible = true;

            }
            else
            {
                grdBind.DataSource = null;
                grdBind.DataBind();
                lblError.Visible = true;
                lblError.Text = RecordNotFound;
                tblSearch.Visible = false;
                btnExportToExcel.Visible = false;
            }
        }
        else
        {
            grdBind.DataSource = null;
            grdBind.DataBind();
            lblError.Visible = true;
            lblError.Text = RecordNotFound;
        }
    } 
    

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (rdoSummaryLevel.Checked)
        {
            if (rdoSiteView.Checked)
            {               
                WebCommon.ExportHideHidden("Site_SummaryView", grdBindExport,null, false,true);
               // ucExportToExcel1.FileName = "Site_SummaryView";
            }
            else if (rdoInventoryGroupView.Checked)
            {
                WebCommon.ExportHideHidden("InventoryGroup_SummaryView", grdBindExport, null, false, true);
               // ucExportToExcel1.FileName = "InventoryGroup_SummaryView";
            }
            else if (rdoPlannerView.Checked)
            {              
                WebCommon.ExportHideHidden("InventoryGroup_SummaryView", grdBindExport, null, false, true);
               // ucExportToExcel1.FileName = "StockPlanner_SummaryView";
            }
            else if (rdoVendorView.Checked)
            {              
                WebCommon.ExportHideHidden("InventoryGroup_SummaryView", grdBindExport, null, false, true);
               // ucExportToExcel1.FileName = "Vendor_SummaryView";
            }
        }
    }

    protected void grdBind_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (divSummaryGrid.Visible == true)
            {
                //string SelectedSiteIDs = msSite.SelectedSiteIDs;
                //string SelectedCountryIDs = msCountry.SelectedCountryIDs;
                //string SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
                //string SelectedStockPlannerGroupIDs = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;

                string SelectedSiteIDs = null;
                string SelectedCountryIDs = null;
                string SelectedStockPlannerIDs = null;
                string SelectedStockPlannerGroupIDs = null;

                if (!(string.IsNullOrEmpty(msSite.SelectedSiteIDs)))
                {                    
                    SelectedSiteIDs = msSite.SelectedSiteIDs;
                }

                if (!(string.IsNullOrEmpty(msCountry.SelectedCountryIDs)))
                {                    
                    SelectedCountryIDs = msCountry.SelectedCountryIDs;
                }
                if (!(string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs)))
                {                    
                    SelectedStockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
                }
                if (!(string.IsNullOrEmpty(multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs)))
                {                   
                    SelectedStockPlannerGroupIDs = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;
                }


                var URL = "";
                dynamic firstCell = e.Row.Cells[0];
                firstCell.Controls.Clear();

                if (rdoInventoryGroupView.Checked)
                {
                    dynamic BoWithPenaltiesCell = e.Row.Cells[3];
                    BoWithPenaltiesCell.Controls.Clear();

                    dynamic ApprovedCell = e.Row.Cells[4];
                    ApprovedCell.Controls.Clear();

                    dynamic RejectedCell = e.Row.Cells[6];
                    RejectedCell.Controls.Clear();

                    dynamic OutstandingCell = e.Row.Cells[8];
                    RejectedCell.Controls.Clear();                                       
                 
                    string firstCellValue = firstCell.Text;
                    URL = EncryptQuery("BackOrderPenlatyOverview.aspx?InventoryGroup=" + firstCellValue.Replace("&amp;", "()") + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "&rblStatus=" + rblApproveType.SelectedValue);

                    firstCell.Controls.Add(new HyperLink
                    {
                        NavigateUrl = URL,
                        Target = "_blank",
                        Text = firstCell.Text
                    });

                    if (BoWithPenaltiesCell.Text != "0")
                    {
                        string Status = rblApproveType.SelectedItem.Text;
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedInventoryGroup" + "&InventoryGroup=" + Convert.ToString(firstCell.Text)
                            + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "&Status=" + Status + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs);
                        BoWithPenaltiesCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = BoWithPenaltiesCell.Text
                        });
                    }

                    if (ApprovedCell.Text != "0")
                    {
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedInventoryGroup" + "&InventoryGroup=" + Convert.ToString(firstCell.Text)
                            + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs + "&rblStatus=Accepted");
                        ApprovedCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = ApprovedCell.Text
                        });
                    }

                    if (RejectedCell.Text != "0")
                    {
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedInventoryGroup" + "&InventoryGroup=" + Convert.ToString(firstCell.Text)
                            + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs + "&rblStatus=Declined");
                        RejectedCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = RejectedCell.Text
                        });
                    }

                    if (OutstandingCell.Text != "0")
                    {
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedInventoryGroup" + "&InventoryGroup=" + Convert.ToString(firstCell.Text)
                            + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs + "&rblStatus=Pending");
                        OutstandingCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = OutstandingCell.Text
                        });
                    }
                }

                if (rdoPlannerView.Checked)
                {

                    dynamic BoWithPenaltiesCell = e.Row.Cells[4];
                    BoWithPenaltiesCell.Controls.Clear();

                    dynamic ApprovedCell = e.Row.Cells[5];
                    ApprovedCell.Controls.Clear();

                    dynamic RejectedCell = e.Row.Cells[7];
                    RejectedCell.Controls.Clear();

                    dynamic OutstandingCell = e.Row.Cells[9];
                    RejectedCell.Controls.Clear();

                    dynamic SecondCell = e.Row.Cells[1];
                    URL = EncryptQuery("BackOrderPenlatyOverview.aspx?UserID=" + Convert.ToString(firstCell.Text) + "&PlannerName=" + Convert.ToString(SecondCell.Text) + "&InventoryGroup=" + null + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value
                        + "&rblStatus=" + rblApproveType.SelectedValue);
                    SecondCell.Controls.Add(new HyperLink
                    {
                        NavigateUrl = URL,
                        Target = "_blank",
                        Text = SecondCell.Text
                    });


                    if (BoWithPenaltiesCell.Text != "0")
                    {
                        string Status = rblApproveType.SelectedItem.Text;
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedPlanner" + "&UserID=" + Convert.ToString(firstCell.Text)
                            + "&Planner=" + Convert.ToString(SecondCell.Text) + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value
                            + "&Status=" + Status + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs);
                        BoWithPenaltiesCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = BoWithPenaltiesCell.Text
                        });
                    }

                    if (ApprovedCell.Text != "0")
                    {
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedPlanner" + "&UserID=" + Convert.ToString(firstCell.Text)
                            + "&Planner=" + Convert.ToString(SecondCell.Text) + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value
                            + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs
                            + "&rblStatus=Accepted");
                        ApprovedCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = ApprovedCell.Text
                        });
                    }

                    if (RejectedCell.Text != "0")
                    {
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedPlanner" + "&UserID=" + Convert.ToString(firstCell.Text)
                            + "&Planner=" + Convert.ToString(SecondCell.Text) + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value
                            + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs
                            + "&rblStatus=Declined");
                        RejectedCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = RejectedCell.Text
                        });
                    }

                    if (OutstandingCell.Text != "0")
                    {
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedPlanner" + "&UserID=" + Convert.ToString(firstCell.Text)
                            + "&Planner=" + Convert.ToString(SecondCell.Text) + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value
                            + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs
                            + "&rblStatus=Pending");
                        OutstandingCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = OutstandingCell.Text
                        });
                    }

                }

                if (rdoVendorView.Checked)
                {
                    dynamic BoWithPenaltiesCell = e.Row.Cells[5];
                    BoWithPenaltiesCell.Controls.Clear();

                    dynamic ApprovedCell = e.Row.Cells[6];
                    ApprovedCell.Controls.Clear();

                    dynamic RejectedCell = e.Row.Cells[8];
                    RejectedCell.Controls.Clear();

                    dynamic OutstandingCell = e.Row.Cells[10];
                    RejectedCell.Controls.Clear();


                    if (BoWithPenaltiesCell.Text != "0")
                    {
                        string Status = rblApproveType.SelectedItem.Text;
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedVendor" + "&VendorID=" + Convert.ToString(firstCell.Text)
                            + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value
                             + "&Status=" + Status + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs);
                        BoWithPenaltiesCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = BoWithPenaltiesCell.Text
                        });
                    }

                    if (ApprovedCell.Text != "0")
                    {
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedVendor" + "&VendorID=" + Convert.ToString(firstCell.Text)
                            + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs
                            + "&rblStatus=Accepted");
                        ApprovedCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = ApprovedCell.Text
                        });
                    }

                    if (RejectedCell.Text != "0")
                    {
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedVendor" + "&VendorID=" + Convert.ToString(firstCell.Text)
                             + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs
                            + "&rblStatus=Declined");
                        RejectedCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = RejectedCell.Text
                        });
                    }

                    if (OutstandingCell.Text != "0")
                    {
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedVendor" + "&VendorID=" + Convert.ToString(firstCell.Text)
                             + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs
                            + "&rblStatus=Pending");
                        OutstandingCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = OutstandingCell.Text
                        });
                    }
                }

                if (rdoSiteView.Checked)
                {
                    dynamic BoWithPenaltiesCell = e.Row.Cells[4];
                    BoWithPenaltiesCell.Controls.Clear();

                    dynamic ApprovedCell = e.Row.Cells[5];
                    ApprovedCell.Controls.Clear();

                    dynamic RejectedCell = e.Row.Cells[7];
                    RejectedCell.Controls.Clear();

                    dynamic OutstandingCell = e.Row.Cells[9];
                    RejectedCell.Controls.Clear();


                    if (BoWithPenaltiesCell.Text != "0")
                    {
                        string Status = rblApproveType.SelectedItem.Text;
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedSite" + "&SiteID=" + Convert.ToString(firstCell.Text)
                            + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value
                             + "&Status=" + Status + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs);
                        BoWithPenaltiesCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = BoWithPenaltiesCell.Text
                        });
                    }

                    if (ApprovedCell.Text != "0")
                    {
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedSite" + "&SiteID=" + Convert.ToString(firstCell.Text)
                            + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value 
                            + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs
                            + "&rblStatus=Accepted");
                        ApprovedCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = ApprovedCell.Text
                        });
                    }

                    if (RejectedCell.Text != "0")
                    {
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedSite" + "&SiteID=" + Convert.ToString(firstCell.Text)
                             + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value
                             + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs
                            + "&rblStatus=Declined");
                        RejectedCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = RejectedCell.Text
                        });
                    }

                    if (OutstandingCell.Text != "0")
                    {
                        URL = EncryptQuery("BOPenlatyOverviewDetailed.aspx?IsFrom=BODetailedSite" + "&SiteID=" + Convert.ToString(firstCell.Text)
                             + "&DateTo=" + hdnJSToDt.Value + "&DateFrom=" + hdnJSFromDt.Value
                             + "&SelectedSiteIDs=" + SelectedSiteIDs + "&SelectedCountryIDs=" + SelectedCountryIDs
                            + "&SelectedStockPlannerIDs=" + SelectedStockPlannerIDs + "&SelectedStockPlannerGroupIDs=" + SelectedStockPlannerGroupIDs
                            + "&rblStatus=Pending");
                        OutstandingCell.Controls.Add(new HyperLink
                        {
                            NavigateUrl = URL,
                            Target = "_blank",
                            Text = OutstandingCell.Text
                        });
                    }
                }
            }            

        }
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGridview(currnetPageIndx);
    }

    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        divSummaryGrid.Visible = true;
        btnBack.Visible = true;
        lblError.Text = "";
        lblError.Visible = false;
        btnExportToExcel.Visible = true;       
        grdBind.PageIndex = e.NewPageIndex;
        grdBind.DataSource = (DataTable)ViewState["DataSource"];
        grdBind.DataBind();

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        divSummaryGrid.Visible = false;
        btnBack.Visible = false;
        tblSearch.Visible = true;
        pager1.Visible = false;

        //if (Session["BOPenaltyOverview"] != null)
        //{
        //    RetainSearchData();
        //}
        
        //msStockPlanner.setStockPlannerOnPostBack();
        //msSite.setSitesOnPostBack();
      
        //msCountry.SetCountryOnPostBack();   

        EncryptQueryString("BackOrderPenlatyOverview.aspx?PreviousPage=BOPenaltySummary");
        if (Request.RawUrl.Split('?').Length > 1)
        {
            //if (GetQueryStringValue("IsFrom") == "Planner")
            //{
            //     //int rblStatus = Convert.ToInt32(GetQueryStringValue("rblStatus"));
            //     //   rdoInventoryGroupView.Checked = true;
            //     //   rdoSiteView.Checked = false;
            //     //   rdoPlannerView.Checked = false;
            //     //   rdoVendorView.Checked = false;
            //     //   IsfromPage = "Vendor";
            //     //   BindOnQueryString("ByPlanner", rblStatus);

            //    EncryptQuery("BackOrderPenlatyOverview.aspx");
            //}

            //if (GetQueryStringValue("IsFrom") == "Inventory")
            //{
            //    int rblStatus = Convert.ToInt32(GetQueryStringValue("rblStatus"));
            //    rdoInventoryGroupView.Checked = true;
            //    rdoSiteView.Checked = false;
            //    rdoPlannerView.Checked = false;
            //    rdoVendorView.Checked = false;
            //    //IsfromPage = "Planner";
            //    BindOnQueryString("ByInventory", rblStatus);
            //}

            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
        }      
        
    }



    public void BindOnQueryString(string action, int Status)
    {
        divSummaryGrid.Visible = true;       
        btnBack.Visible = true;
        
        if (Session["InputFields"] != null)
        {
            BackOrderBE oBackOrderBE = (BackOrderBE) Session["InputFields"];
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();

            oBackOrderBE.SelectedDateFrom = Common.GetMM_DD_YYYY(GetQueryStringValue("DateFrom"));
            oBackOrderBE.SelectedDateTo = Common.GetMM_DD_YYYY(GetQueryStringValue("DateTo"));
            hdnJSFromDt.Value = Convert.ToString(GetQueryStringValue("DateFrom"));
            hdnJSToDt.Value = Convert.ToString(GetQueryStringValue("DateTo"));            
            
            if (Status == 1)
            {
                oBackOrderBE.InventoryReviewStatus = "All";
            }
            else if (Status == 2)
            {
                oBackOrderBE.InventoryReviewStatus = "Approved";
            }
            else if (Status == 3)
            {
                oBackOrderBE.InventoryReviewStatus = "Declined";
            }
            else if (Status == 4)
            {
                oBackOrderBE.InventoryReviewStatus = "Pending";
            }

            if (GetQueryStringValue("InventoryGroup") != null)
            {
                oBackOrderBE.StockPlannerGroupings = GetQueryStringValue("InventoryGroup").Replace("()", "&");
                oBackOrderBE.SummaryBy = action;

               // ucExportToExcel1.FileName = "InventoryGroup_SummaryView";                
            }

            if (GetQueryStringValue("PlannerName") != null)
            {
                if (GetQueryStringValue("UserID") != null)
                {
                    oBackOrderBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
                }
               // oBackOrderBE.SelectedStockPlannerName = GetQueryStringValue("PlannerName");
                oBackOrderBE.SummaryBy = action;
              //  ucExportToExcel1.FileName = "StockPlanner_SummaryView";
            }

            DataSet dt = oBackOrderBAL.GetBackOrderPenaltyReportBAL(oBackOrderBE);
            if (dt != null)
            {
                if (dt.Tables[0].Rows.Count > 0)
                {
                    grdBind.DataSource = dt.Tables[0];
                    grdBind.DataBind();
                    grdBindExport.DataSource = dt.Tables[0];
                    grdBindExport.DataBind();
                    lblError.Text = "";
                    lblError.Visible = false;
                    tblSearch.Visible = false;
                    btnExportToExcel.Visible = true;
                    oBackOrderBE = null;              
                    
                }
                else
                {
                    grdBind.DataSource = null;
                    grdBind.DataBind();
                    lblError.Visible = true;
                    lblError.Text = RecordNotFound;
                    tblSearch.Visible = false;
                    btnExportToExcel.Visible = false;                                   
                    
                }
            }
            else
            {
                grdBind.DataSource = null;
                grdBind.DataBind();
                lblError.Visible = true;
                lblError.Text = RecordNotFound;               
               
            }
        }
    }

    protected void grdBind_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (GetQueryStringValue("InventoryGroup") != null)
            {
                e.Row.Cells[0].Visible = false; // hides the first column
            }
            else
            {
                e.Row.Cells[0].Visible = true;
            }

            if (rdoPlannerView.Checked || rdoVendorView.Checked || rdoSiteView.Checked)
            {
                e.Row.Cells[0].Visible = false; // hides the first column
            }
            else
            {
                e.Row.Cells[0].Visible = true;
            }

            if (rdoVendorView.Checked)
            {
                e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Left;
            }
            else
            {
                e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Center;
            }
        }
        if (e.Row.RowType == DataControlRowType.Header)
        {
            if (GetQueryStringValue("InventoryGroup") != null)
            {
                e.Row.Cells[0].Visible = false; // hides the first column
            }
            else
            {
                e.Row.Cells[0].Visible = true;
            }

            if (rdoPlannerView.Checked || rdoVendorView.Checked || rdoSiteView.Checked)
            {
                e.Row.Cells[0].Visible = false; // hides the first column
            }
            else
            {
                e.Row.Cells[0].Visible = true;
            }

            if (rdoVendorView.Checked)
            {
                e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Left;
            }
            else
            {
                e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Center;
            }
        }
    }

    private void SetSession(BackOrderBE oBackOrderBE)
    {
        Session["BOPenaltyOverview"] = null;       

        TextBox txt = (TextBox)msStockPlanner.FindControl("txtUserName");
        HiddenField hdnSelectedStockPlonner = msStockPlanner.FindControl("hiddenSelectedIDs") as HiddenField;
        HiddenField hiddenSelectedName = msStockPlanner.FindControl("hiddenSelectedName") as HiddenField;

        Hashtable htBOPenaltyOverview = new Hashtable();
        htBOPenaltyOverview.Add("SelectedSiteId", oBackOrderBE.SelectedSiteIDs);
        htBOPenaltyOverview.Add("SelectedSPIDs", hdnSelectedStockPlonner.Value);
        htBOPenaltyOverview.Add("SelectedSPNames", hiddenSelectedName.Value);
        htBOPenaltyOverview.Add("SelectedCountryIDs", oBackOrderBE.SelectedCountryIDs);
        htBOPenaltyOverview.Add("SelectedStockPlannerGroupID", oBackOrderBE.SelectedStockPlannerGroupID);
        htBOPenaltyOverview.Add("SelectedDateFrom",hdnJSFromDt.Value);
        htBOPenaltyOverview.Add("SelectedDateTo", hdnJSToDt.Value);
        htBOPenaltyOverview.Add("SelectedSPName", txt.Text);       
        htBOPenaltyOverview.Add("SelectedCountryName", msCountry.SelectedCountryName);
        htBOPenaltyOverview.Add("SelectedSiteName", msSite.SelectedSiteName);
        htBOPenaltyOverview.Add("InventoryReviewStatus", oBackOrderBE.InventoryReviewStatus);
        htBOPenaltyOverview.Add("SummaryBy", oBackOrderBE.SummaryBy);
        Session["BOPenaltyOverview"] = htBOPenaltyOverview;
    }


    public void RetainSearchData()
    {
      
        //msVendor.setVendorsOnPostBack();
       // multiSelectStockPlannerGrouping.setStockPlannerGroupingsOnPostBack();
       // msCountry.SetCountryOnPostBack();
        Hashtable htBOPenaltyOverview = (Hashtable)Session["BOPenaltyOverview"];

       


        //*********** StockPlanner ***************
        string StockPlannerText = (htBOPenaltyOverview.ContainsKey("SelectedSPName") && htBOPenaltyOverview["SelectedSPName"] != null) ? htBOPenaltyOverview["SelectedSPName"].ToString() : "";
        string StockPlannerID = (htBOPenaltyOverview.ContainsKey("SelectedSPIDs") && htBOPenaltyOverview["SelectedSPIDs"] != null) ? htBOPenaltyOverview["SelectedSPIDs"].ToString() : "";
        string StockPlannerName = (htBOPenaltyOverview.ContainsKey("SelectedSPNames") && htBOPenaltyOverview["SelectedSPNames"] != null) ? htBOPenaltyOverview["SelectedSPNames"].ToString() : "";
        ListBox lstRight = msStockPlanner.FindControl("ucLBRight") as ListBox;
        ListBox lstLeft = msStockPlanner.FindControl("ucLBLeft") as ListBox;
        string strDtockPlonnerId = string.Empty;
        lstRight.Items.Clear();
        //HiddenField hdnSelectedStockPlonner = msStockPlanner.FindControl("hiddenSelectedIDs") as HiddenField;
        //hdnSelectedStockPlonner.Value = StockPlannerID;

        //HiddenField hiddenSelectedName = msStockPlanner.FindControl("hiddenSelectedName") as HiddenField;
        //hiddenSelectedName.Value = StockPlannerName;

        //msStockPlanner.SelectedSPName = (htBOPenaltyOverview.ContainsKey("SelectedSPNames") && htBOPenaltyOverview["SelectedSPNames"] != null) ? htBOPenaltyOverview["SelectedSPNames"].ToString() : "";
        //msStockPlanner.SelectedStockPlannerIDs = (htBOPenaltyOverview.ContainsKey("SelectedSPIDs") && htBOPenaltyOverview["SelectedSPIDs"] != null) ? htBOPenaltyOverview["SelectedSPIDs"].ToString() : "";
        //msStockPlanner.setStockPlannerOnPostBack();



        if (lstLeft != null && lstRight != null && !string.IsNullOrEmpty(StockPlannerID))
            lstRight.Items.Clear();
            msStockPlanner.SearchStockPlannerClick(StockPlannerText);
        {
            string[] strStockPlonnerIDs = StockPlannerID.Split(',');
            for (int index = 0; index < strStockPlonnerIDs.Length; index++)
            {
                ListItem listItem = lstLeft.Items.FindByValue(strStockPlonnerIDs[index]);
                if (listItem != null)
                {
                    strDtockPlonnerId += strStockPlonnerIDs[index] + ",";
                    lstRight.Items.Add(listItem);
                    lstLeft.Items.Remove(listItem);
                }

            }
            HiddenField hdnSelectedStockPlonner = msStockPlanner.FindControl("hiddenSelectedIDs") as HiddenField;
            hdnSelectedStockPlonner.Value = strDtockPlonnerId;
        }

        ////************************* StockPlannerGrouping ***************************
        string StockPlannerGroupingID = (htBOPenaltyOverview.ContainsKey("SelectedStockPlannerGroupID") && htBOPenaltyOverview["SelectedStockPlannerGroupID"] != null) ? htBOPenaltyOverview["SelectedStockPlannerGroupID"].ToString() : null;
        ListBox lstRightGrouping = multiSelectStockPlannerGrouping.FindControl("lstRight") as ListBox;
        ListBox lstLeftGrouping = multiSelectStockPlannerGrouping.FindControl("lstLeft") as ListBox;
        string msSPgrouping = string.Empty;
        lstRightGrouping.Items.Clear();
        multiSelectStockPlannerGrouping.BindStockPlannerGrouping();

        if (lstLeftGrouping != null && lstRightGrouping != null && !string.IsNullOrEmpty(StockPlannerGroupingID))
        {
            lstLeftGrouping.Items.Clear();
            multiSelectStockPlannerGrouping.BindStockPlannerGrouping();


            string[] strSPs = StockPlannerGroupingID.Split(',');

            if (!string.IsNullOrEmpty(StockPlannerGroupingID))
            {
                string[] strStockPlannerGroupingIDs = StockPlannerGroupingID.Split(',');
                for (int index = 0; index < strStockPlannerGroupingIDs.Length; index++)
                {
                    ListItem listItem = lstLeftGrouping.Items.FindByValue(strStockPlannerGroupingIDs[index]);
                    if (listItem != null)
                    {
                        msSPgrouping = msSPgrouping + strSPs[index].ToString() + ",";
                        lstRightGrouping.Items.Add(listItem);
                        lstLeftGrouping.Items.Remove(listItem);
                    }
                }

                if (string.IsNullOrEmpty(multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs))
                    multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs = msSPgrouping.Trim(',');
            }
        }


        //*********** Site ***************
        string SiteId = (htBOPenaltyOverview.ContainsKey("SelectedSiteId") && htBOPenaltyOverview["SelectedSiteId"] != null) ? htBOPenaltyOverview["SelectedSiteId"].ToString() : "";
        ListBox lstRightSite = msSite.FindControl("lstRight") as ListBox;
        ListBox lstLeftSite = msSite.FindControl("lstLeft") as ListBox;
        string msSiteid = string.Empty;
        lstRightSite.Items.Clear();
        lstRightSite.Items.Clear();
        msSite.SelectedSiteIDs = "";
        msSite.BindSite();

        if (lstLeftSite != null && lstRightSite != null && !string.IsNullOrEmpty(SiteId))
        {
            //lstLeftSite.Items.Clear();
            lstRightSite.Items.Clear();
            msSite.BindSite();

            string[] strSiteIDs = SiteId.Split(',');
            for (int index = 0; index < strSiteIDs.Length; index++)
            {
                ListItem listItem = lstLeftSite.Items.FindByValue(strSiteIDs[index]);
                if (listItem != null)
                {
                    msSiteid = msSiteid + strSiteIDs[index].ToString() + ",";
                    lstRightSite.Items.Add(listItem);
                    lstLeftSite.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msSite.SelectedSiteIDs))
                msSite.SelectedSiteIDs = msSiteid.Trim(',');
        }


        //*********** Country ***************
        string CountryIDs = (htBOPenaltyOverview.ContainsKey("SelectedCountryIDs") && htBOPenaltyOverview["SelectedCountryIDs"] != null) ? htBOPenaltyOverview["SelectedCountryIDs"].ToString() : "";
        ListBox lstRightCountry = msCountry.FindControl("lstRight") as ListBox;
        ListBox lstLeftCountry = msCountry.FindControl("lstLeft") as ListBox;
        string msCountryid = string.Empty;
        lstRightCountry.Items.Clear();
        //lstRightSite.Items.Clear();
        msCountry.SelectedCountryIDs = "";
        msCountry.BindCountry();
        if (lstLeftCountry != null && lstRightCountry != null && !string.IsNullOrEmpty(CountryIDs))
        {
            //lstLeftSite.Items.Clear();
            lstRightCountry.Items.Clear();
           // msCountry.BindCountry();

            string[] strCountryIDs = CountryIDs.Split(',');
            for (int index = 0; index < strCountryIDs.Length; index++)
            {
                ListItem listItem = lstLeftCountry.Items.FindByValue(strCountryIDs[index]);
                if (listItem != null)
                {
                    msCountryid = msCountryid + strCountryIDs[index].ToString() + ",";
                    lstRightCountry.Items.Add(listItem);
                    lstLeftCountry.Items.Remove(listItem);
                }

            }
            if (string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
                msCountry.SelectedCountryIDs = msCountryid.Trim(',');
        }

        txtFromDate.Text = (htBOPenaltyOverview.ContainsKey("SelectedDateFrom") && htBOPenaltyOverview["SelectedDateFrom"] != null) ? htBOPenaltyOverview["SelectedDateFrom"].ToString() : null;
        txtToDate.Text = (htBOPenaltyOverview.ContainsKey("SelectedDateTo") && htBOPenaltyOverview["SelectedDateTo"] != null) ? htBOPenaltyOverview["SelectedDateTo"].ToString() : null;

        hdnJSFromDt.Value = txtFromDate.Text;
        hdnJSToDt.Value = txtToDate.Text;

        string Status = (htBOPenaltyOverview.ContainsKey("InventoryReviewStatus") && htBOPenaltyOverview["InventoryReviewStatus"] != null) ? htBOPenaltyOverview["InventoryReviewStatus"].ToString() : "";

        if (Status == "All")
        {
            rblApproveType.SelectedIndex = 0; 
        }
        else if (Status == "Approved")
        {
            rblApproveType.SelectedIndex = 1; 
        }
        else if (Status == "Declined")
        {
            rblApproveType.SelectedIndex = 2; 
        }
        else if (Status == "Pending")
        {
            rblApproveType.SelectedIndex = 3; 
        }

        string SummaryBy = (htBOPenaltyOverview.ContainsKey("SummaryBy") && htBOPenaltyOverview["SummaryBy"] != null) ? htBOPenaltyOverview["SummaryBy"].ToString() : "";

        if (!string.IsNullOrEmpty(SummaryBy))
        {
            rdoSummaryLevel.Checked = true;

            if (SummaryBy == "BySite")
            {
                rdoSiteView.Checked = true;
            }
            else if (SummaryBy == "ByInventory")
            {
                rdoInventoryGroupView.Checked = true;
            }
            else if (SummaryBy == "ByPlanner")
            {
                rdoPlannerView.Checked = true;
            }
            else if (SummaryBy == "ByVendor")
            {
                rdoVendorView.Checked = true;
            }
        }
        else
        {
            rdoDetailedLevel.Checked = true;
        }

        Session["BOPenaltyOverview"] = null;
    }

    protected void grdBindExport_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (rdoSummaryLevel.Checked)
            {
                if (rdoSiteView.Checked)
                {
                    e.Row.Cells[0].Visible = false;                    
                }                
                else if (rdoPlannerView.Checked)
                {
                    e.Row.Cells[0].Visible = false;                   
                }
                else if (rdoVendorView.Checked)
                {
                    e.Row.Cells[0].Visible = false;                  
                }
            }
        }
        if (e.Row.RowType == DataControlRowType.Header)
        {
            if (rdoSummaryLevel.Checked)
            {
                if (rdoSiteView.Checked)
                {
                    e.Row.Cells[0].Visible = false;
                }
                else if (rdoPlannerView.Checked)
                {
                    e.Row.Cells[0].Visible = false;
                }
                else if (rdoVendorView.Checked)
                {
                    e.Row.Cells[0].Visible = false;
                }
            }
        }
    }
    
}
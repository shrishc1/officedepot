﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Data;
using Utilities;

public partial class ModuleUI_StockOverview_BackOrder_BOPenlatyOverviewDetailed : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGridview();
        }
        ucExportToExcel1.GridViewControl = gvExport;
        ucExportToExcel1.FileName = "BOpenaltyOverviewDetailedReport";
    }


    public void BindGridview()
    {
        BackOrderBE oBackOrderBE = new BackOrderBE();
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();


        oBackOrderBE.Action = "GetBackOrderPenaltyDetailedView";

        if (GetQueryStringValue("PageFrom") != null)
        {
            if (GetQueryStringValue("PageFrom") == "BOPenaltyOverview")
            {
                oBackOrderBE.SelectedDateFrom = Common.GetMM_DD_YYYY(GetQueryStringValue("DateFrom"));
                oBackOrderBE.SelectedDateTo = Common.GetMM_DD_YYYY(GetQueryStringValue("DateTo"));

                if (!string.IsNullOrEmpty(GetQueryStringValue("SelectedSiteIDs")))
                {
                    oBackOrderBE.SelectedSiteIDs = GetQueryStringValue("SelectedSiteIDs");
                }

                if (!string.IsNullOrEmpty(GetQueryStringValue("SelectedCountryIDs")))
                {
                    oBackOrderBE.SelectedCountryIDs = GetQueryStringValue("SelectedCountryIDs");
                }
                if (!string.IsNullOrEmpty(GetQueryStringValue("SelectedStockPlannerIDs")))
                {
                    oBackOrderBE.SelectedStockPlannerIDs = GetQueryStringValue("SelectedStockPlannerIDs");
                }
                if (!string.IsNullOrEmpty(GetQueryStringValue("SelectedStockPlannerGroupIDs")))
                {
                    oBackOrderBE.SelectedStockPlannerGroupID = GetQueryStringValue("SelectedStockPlannerGroupIDs");
                }

                oBackOrderBE.InventoryReviewStatus = GetQueryStringValue("rblStatus");

                //if (rdoSummaryLevel.Checked)
                //{
                //    if (rdoSiteView.Checked)
                //    {
                //        oBackOrderBE.SummaryBy = "BySite";
                //        ucExportToExcel1.FileName = "Site_SummaryView";
                //    }
                //    else if (rdoInventoryGroupView.Checked)
                //    {
                //        oBackOrderBE.SummaryBy = "ByInventory";
                //        // IsfromPage = "Inventory";
                //        ucExportToExcel1.FileName = "InventoryGroup_SummaryView";
                //    }
                //    else if (rdoPlannerView.Checked)
                //    {
                //        oBackOrderBE.SummaryBy = "ByPlanner";
                //        ucExportToExcel1.FileName = "StockPlanner_SummaryView";
                //    }
                //    else if (rdoVendorView.Checked)
                //    {
                //        oBackOrderBE.SummaryBy = "ByVendor";
                //        ucExportToExcel1.FileName = "Vendor_SummaryView";
                //    }
                //}

            }
        }

        if (GetQueryStringValue("IsFrom") != null)
        {
            oBackOrderBE.SelectedDateFrom = Common.GetMM_DD_YYYY(GetQueryStringValue("DateFrom"));
            oBackOrderBE.SelectedDateTo = Common.GetMM_DD_YYYY(GetQueryStringValue("DateTo"));

            if (!string.IsNullOrEmpty(GetQueryStringValue("SelectedSiteIDs")))
            {
                oBackOrderBE.SelectedSiteIDs = GetQueryStringValue("SelectedSiteIDs");
            }

            if (!string.IsNullOrEmpty(GetQueryStringValue("SelectedCountryIDs")))
            {
                oBackOrderBE.SelectedCountryIDs = GetQueryStringValue("SelectedCountryIDs");
            }
            if (!string.IsNullOrEmpty(GetQueryStringValue("SelectedStockPlannerIDs")))
            {
                oBackOrderBE.SelectedStockPlannerIDs = GetQueryStringValue("SelectedStockPlannerIDs");
            }
            if (!string.IsNullOrEmpty(GetQueryStringValue("SelectedStockPlannerGroupIDs")))
            {
                oBackOrderBE.SelectedStockPlannerGroupID = GetQueryStringValue("SelectedStockPlannerGroupIDs");
            }

            if (GetQueryStringValue("IsFrom") == "BODetailedInventoryGroup")
            {
                if (GetQueryStringValue("Status") != null)
                {
                    oBackOrderBE.InventoryReviewStatus = GetQueryStringValue("Status");
                }
                else
                {
                    oBackOrderBE.InventoryReviewStatus = GetQueryStringValue("rblStatus");
                }
                
                oBackOrderBE.StockPlannerGroupings = GetQueryStringValue("InventoryGroup");
            }

            if (GetQueryStringValue("IsFrom") == "BODetailedPlanner")
            {
                if (GetQueryStringValue("Status") != null)
                {
                    oBackOrderBE.InventoryReviewStatus = GetQueryStringValue("Status");
                }
                else
                {
                    oBackOrderBE.InventoryReviewStatus = GetQueryStringValue("rblStatus");
                }

                oBackOrderBE.UserID = Convert.ToInt32(GetQueryStringValue("UserID"));
            }

            if (GetQueryStringValue("IsFrom") == "BODetailedVendor")
            {
                if (GetQueryStringValue("Status") != null)
                {
                    oBackOrderBE.InventoryReviewStatus = GetQueryStringValue("Status");
                }
                else
                {
                    oBackOrderBE.InventoryReviewStatus = GetQueryStringValue("rblStatus");
                }

                oBackOrderBE.SelectedVendorIDs = GetQueryStringValue("VendorID");
            }

            if (GetQueryStringValue("IsFrom") == "BODetailedSite")
            {
                if (GetQueryStringValue("Status") != null)
                {
                    oBackOrderBE.InventoryReviewStatus = GetQueryStringValue("Status");
                }
                else
                {
                    oBackOrderBE.InventoryReviewStatus = GetQueryStringValue("rblStatus");
                }

                oBackOrderBE.SelectedSiteIDs = GetQueryStringValue("SiteID");
            }

            

        }


        List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetBackOrderPenaltyDetailedReportBAL(oBackOrderBE);

        if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
        {
            ViewState["DataSource"] = lstBackOrderBE;

            grdBOPenaltyDetailed.DataSource = lstBackOrderBE;
            grdBOPenaltyDetailed.DataBind();
            gvExport.DataSource = lstBackOrderBE;
            gvExport.DataBind();
            ViewState["lstBackOrderBE"] = lstBackOrderBE;
            ucExportToExcel1.Visible = true;
            oBackOrderBE = null;
        }
        else
        {
            ucExportToExcel1.Visible = false;
            grdBOPenaltyDetailed.DataSource = null;
            grdBOPenaltyDetailed.DataBind();
            gvExport.DataSource = null;
            gvExport.DataBind();
        }
    }
 


    protected void grdBOPenaltyDetailed_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstBackOrderBE"] != null)
        {
            grdBOPenaltyDetailed.DataSource = (List<BackOrderBE>)ViewState["lstBackOrderBE"];
            grdBOPenaltyDetailed.PageIndex = e.NewPageIndex;
            grdBOPenaltyDetailed.DataBind();
        }

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (GetQueryStringValue("PageFrom") != null)
        {
            if (GetQueryStringValue("PageFrom") == "BOPenaltyOverview")
            {
                EncryptQueryString("BackOrderPenlatyOverview.aspx?PreviousPage=BOPenaltyOverviewDetailed");
            }
        }

        if (GetQueryStringValue("IsFrom") != null)
        {
            if (GetQueryStringValue("IsFrom") == "BODetailedInventoryGroup")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
            }
            if (GetQueryStringValue("IsFrom") == "BODetailedPlanner")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
            }

            if (GetQueryStringValue("IsFrom") == "BODetailedVendor")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
            }

            if (GetQueryStringValue("IsFrom") == "BODetailedSite")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "window.close()", true);
            }
            
        }

    }

   
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="AccountsPayableView.aspx.cs" Inherits="AccountsPayableView"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/ModuleUI/Discrepancy/UserControl/ucSDRCommunicationAPListing.ascx"
    TagName="ucSDRCommunication" TagPrefix="uc2" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectAPSearch.ascx" TagName="MultiSelectAPSearch"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucMultiSelectVendor.ascx" TagName="ucMultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="../../../CommonUI/UserControls/ucExportToExcel.ascx" TagName="ucExportToExcel"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function SetRadioButton(spanChk) {
            $("input[type='radio'][id$='rblSelect']").removeAttr("checked");
            spanChk.checked = true;
        }
    </script>
    <style type="text/css">
        .radio-fix tr td:nth-child(2)
        {
            width: 245px;
        }
        .radio-fix tr td:nth-child(3)
        {
            width: 170px;
        }
        .wordbreak
        {
            word-break: break-all;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updAPView" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblBOChargestobeApplied" runat="server"></cc1:ucLabel>
            </h2>
            <div class="button-row">
                <cc1:ucExportToExcel ID="btnExportToExcel" runat="server" Visible="false" />
            </div>
            <div class="right-shadow">
                <div class="formbox">
                    <cc1:ucPanel ID="pnlSearchScreen" runat="server">
                        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td align="right">
                                    <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                                </td>
                            </tr>
                        </table>
                        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblCountry" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucMultiSelectVendor ID="msVendor" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblAccountsPayable" runat="server" Text="Accounts Payable"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:MultiSelectAPSearch runat="server" ID="msAPClerk" />
                                </td>
                            </tr>
                               <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblPeriod" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <table width="40%" border="0" cellspacing="5" cellpadding="0" id="tblDate">
                                        <tr>
                                            <td>
                                                <cc1:ucDropdownList ID="drpMonthFrom" runat="server" Width="80px">
                                                    <%-- <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>--%>
                                                    <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                                    <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                                    <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                                    <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                                    <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                                    <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                                    <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                                    <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                                    <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpYearFrom" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td style="text-align: center">
                                                <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpMonthTo" runat="server" Width="80px">
                                                    <%--  <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>--%>
                                                    <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                                    <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                                    <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                                    <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                                    <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                                    <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                                    <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                                    <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                                    <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                                </cc1:ucDropdownList>
                                            </td>
                                            <td>
                                                <cc1:ucDropdownList ID="drpYearTo" runat="server" Width="60px">
                                                </cc1:ucDropdownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="3">
                                    <div class="button-row">
                                        <cc1:ucButton ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                    <cc1:ucPanel ID="pnlPotentialOutput" runat="server">
                        <div style="width: 100%; overflow: auto" id="divPrint" class="fixedTable">
                            <table cellspacing="1" cellpadding="0" border="0" align="center" class="form-table">
                                <tr>
                                    <td style="font-weight: bold;">
                                        <div style="width: 950px; overflow-x: auto;">
                                            <cc1:ucGridView ID="gvAccountsPayableView" runat="server" CssClass="grid gvclass" Width="100%"
                                                GridLines="Both" AllowPaging="true" PageSize="50" OnPageIndexChanging="gvAccountsPayableView_PageIndexChanging">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">
                                                        <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>
                                                    </div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Select">
                                                        <HeaderStyle HorizontalAlign="Left" Width="20px" />
                                                        <ItemStyle Width="20px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="lblVendorIDValue" Text='<%#Eval("Vendor.VendorID") %>'
                                                                Visible="false"></cc1:ucLabel>
                                                            <cc1:ucLabel runat="server" ID="lblVendorVATRefValue" Text='<%#Eval("Vendor.VendorVATRef") %>'
                                                                Visible="false"></cc1:ucLabel>
                                                            <cc1:ucLabel runat="server" ID="lblCountryIDValue" Text='<%#Eval("CountryID") %>'
                                                                Visible="false"></cc1:ucLabel>
                                                            <asp:RadioButton runat="server" CssClass="innerCheckBox" ID="rblSelect" onclick='<%# "javascript:SetRadioButton(this);" %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltVendorNameValue" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Country" SortExpression="Country">
                                                        <HeaderStyle Width="90px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltCountryValue" Text='<%#Eval("Country") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="90px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PenaltyType" SortExpression="PenaltyType">
                                                        <HeaderStyle Width="90px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltPenaltyTypeValue" Text='<%#Eval("PenaltyType") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="90px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Rate" SortExpression="Rate">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltRateValue" Text='<%#Eval("Rate") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="NumberofBOs" SortExpression="NoOfBO">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltNoOfBOsValue" Text='<%#Eval("NoOfBO") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="QtyonBO" SortExpression="QtyOnBO">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltQtyOnBOValue" Text='<%#Eval("QtyOnBO") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Month" SortExpression="Month">
                                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltMonth" Text='<%#Eval("Month")%>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="AmounttobeDebited" SortExpression="AmountDebited">
                                                        <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltAmountDebitedValue" Text='<%#Eval("AmountDebited") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="120px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Currency" SortExpression="Currency.CurrencyName">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltCurrencyNameValue" Text='<%#Eval("Currency.CurrencyName") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="APClerk" SortExpression="APClerk">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltAPClerkValue" Text='<%#Eval("APClerk") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="Vendor.StockPlannerNumber">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="StockPlannerName" SortExpression="Vendor.StockPlannerName">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Reason for Charge" SortExpression="ReasonForCharge">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltReasonForCharge" Text='<%#Eval("ReasonForCharge") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="200px" />
                                                    </asp:TemplateField>
                                                    <%-- <asp:TemplateField HeaderText="Was a Query Raised against this Charge" SortExpression="Status">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStatus" Text='<%#Eval("Status") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                                            </asp:TemplateField>--%>
                                                </Columns>
                                            </cc1:ucGridView>
                                            <cc1:ucGridView ID="gvAccountsPayableViewExcel" runat="server" CssClass="grid gvclass"
                                                GridLines="Both" Visible="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="VendorNumber" SortExpression="Vendor.Vendor_No">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltVendorNoValue" Text='<%#Eval("Vendor.Vendor_No") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VendorName" SortExpression="Vendor.VendorName">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltVendor" Text='<%#Eval("Vendor.VendorName") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Country" SortExpression="Country">
                                                        <HeaderStyle Width="90px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltCountryValue" Text='<%#Eval("Country") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="90px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PenaltyType" SortExpression="PenaltyType">
                                                        <HeaderStyle Width="90px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltPenaltyTypeValue" Text='<%#Eval("PenaltyType") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="90px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Rate" SortExpression="Rate">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltRateValue" Text='<%#Eval("Rate") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="NumberofBOs" SortExpression="NoOfBO">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltNoOfBOValue" Text='<%#Eval("NoOfBO") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="QtyonBO" SortExpression="QtyOnBO">
                                                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltQtyOnBOValue" Text='<%#Eval("QtyOnBO") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Month" SortExpression="Month">
                                                        <HeaderStyle Width="70px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltMonth" Text='<%#Eval("Month")%>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="AmounttobeDebited" SortExpression="AmountDebited">
                                                        <HeaderStyle Width="120px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltAmountDebitedValue" Text='<%#Eval("AmountDebited") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="120px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Currency" SortExpression="Currency.CurrencyName">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltCurrencyName" Text='<%#Eval("Currency.CurrencyName") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="APClerk" SortExpression="APClerk">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltAPClerkValue" Text='<%#Eval("APClerk") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="StockPlannerNumber" SortExpression="Vendor.StockPlannerNumber">
                                                        <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltStockPlannerNoValue" Text='<%#Eval("Vendor.StockPlannerNumber") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="StockPlannerName" SortExpression="Vendor.StockPlannerName">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltStockPlannerNameValue" Text='<%#Eval("Vendor.StockPlannerName") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Reason for Charge" SortExpression="ReasonForCharge">
                                                        <HeaderStyle Width="200px" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <cc1:ucLabel runat="server" ID="ltReasonForCharge" Text='<%#Eval("ReasonForCharge") %>'></cc1:ucLabel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="200px" />
                                                    </asp:TemplateField>
                                                    <%-- <asp:TemplateField HeaderText="Was a Query Raised against this Charge" SortExpression="Status">
                                                <HeaderStyle Width="100px" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <cc1:ucLabel runat="server" ID="ltStatus" Text='<%#Eval("Status") %>'></cc1:ucLabel>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                                            </asp:TemplateField>--%>
                                                </Columns>
                                            </cc1:ucGridView>
                                            <%--<cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="true">
                                    </cc1:PagerV2_8>--%>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <div class="button-row">
                                            <cc1:ucButton ID="btnRaisedDebit" runat="server" Text="Raised Debit" CssClass="button"
                                                OnClick="btnRaisedDebit_Click" />
                                            <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </cc1:ucPanel>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updAPRaiseDebit" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnAPRaiseDebitMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlAPRaiseDebit" runat="server" TargetControlID="btnAPRaiseDebitMPE"
                PopupControlID="pnlAPRaiseDebit" BackgroundCssClass="modalBackground" BehaviorID="ShowAPRaiseDebitPopup"
                DropShadow="false" />
            <asp:Panel ID="pnlAPRaiseDebit" runat="server" Style="display: none; width: 630px">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <cc1:ucPanel ID="pnlAPRaiseDebits" runat="server" Width="100%" CssClass="fieldset-form"
                        GroupingText="AP Action">
                        <table cellspacing="5" cellpadding="0" align="center" width="100%" class="top-settingsNoBorder-popup">
                            <tr>
                                <td style="width: 34%; font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendorNumberSDR" runat="server" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="width: 1%; font-weight: bold;">
                                    :
                                </td>
                                <td style="width: 65%; font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendorNumberSDRValue" runat="server"></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendorNameSDR" runat="server" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendorNameSDRValue" runat="server"></cc1:ucLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblDebitType" runat="server" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucDropdownList ID="ddlDebitType" runat="server" Width="200px">
                                    </cc1:ucDropdownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendorVATReference" runat="server" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucTextbox ID="txtVendorVATReferenceValue" runat="server" Width="196px"></cc1:ucTextbox>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblOfficeDepotVATReference" runat="server" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucDropdownList ID="ddlOfficeDepotVATReference" runat="server" Width="200px">
                                    </cc1:ucDropdownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblCurrency" runat="server" isRequired="true"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td>
                                    <cc1:ucDropdownList ID="ddlCurrency" runat="server" Width="200px">
                                    </cc1:ucDropdownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <cc1:ucPanel ID="pnlDebitwillbesentto" runat="server" GroupingText="Debit will be sent to"
                                        CssClass="fieldset-form">
                                        <table width="100%" cellspacing="0" cellpadding="0" class="form-table" height="100px">
                                            <tr>
                                                <td>
                                                    <uc2:ucSDRCommunication ID="ucSDRCommunication1" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </cc1:ucPanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="right" valign="bottom" colspan="3">
                                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" class="button" OnClick="btnSave_Click" />
                                    <cc1:ucButton ID="btnBack_1" runat="server" Text="Back" class="button" OnClick="btnBack_1_Click" />
                                    <asp:HiddenField ID="hdnVendorIdVal" runat="server" />
                                    <asp:HiddenField ID="hdnAmountDebitedValue" runat="server" />
                                    <asp:HiddenField ID="hdnCountryIDValue" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updAPConfirmation" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnAPConfirmationMPE" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlAPConfirmation" runat="server" TargetControlID="btnAPConfirmationMPE"
                PopupControlID="pnlConfirmation" BackgroundCssClass="modalBackground" BehaviorID="ShowAPConfirmationPopup"
                DropShadow="false" />
            <asp:Panel ID="pnlConfirmation" runat="server" Style="display: none; width: 630px">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <cc1:ucPanel ID="pnlAPConfirmations" runat="server" Width="100%" CssClass="fieldset-form">
                        <table cellspacing="5" cellpadding="0" align="center" width="100%" class="top-settingsNoBorder-popup">
                            <tr>
                                <td align="center" valign="bottom">
                                    <cc1:ucLabel ID="lblBOPendingVendorForReview" Text="The Vendor has some pending queries. Do you want to proceed ?"
                                        runat="server"></cc1:ucLabel>
                                    <br />
                                    <br />
                                    <cc1:ucButton ID="btnYes" runat="server" Text="Yes" class="button" OnClick="btnYes_Click" />&nbsp;
                                    <cc1:ucButton ID="btnNo" runat="server" Text="No" class="button" OnClick="btnNo_Click" />
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

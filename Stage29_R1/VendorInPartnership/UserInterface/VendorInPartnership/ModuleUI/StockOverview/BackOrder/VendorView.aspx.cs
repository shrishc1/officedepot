﻿using BaseControlLibrary;
using BusinessEntities.ModuleBE.Languages.Languages;
using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;

public partial class VendorView : CommonPage
{
    #region Declarations ...
    private const int gridPageSize = 50;
    bool IsExportClicked = false;
    protected string selectCheckboxToAccept = WebCommon.getGlobalResourceValue("SelectCheckboxToAction");
    protected string selectonlyoneCheckboxToQuery = WebCommon.getGlobalResourceValue("selectonlyoneCheckboxToQuery");
    protected string QueryInvestigationMsg = WebCommon.getGlobalResourceValue("QueryInvestigationMsg");

    protected string Pleasestatethedisputedvalue = WebCommon.getGlobalResourceValue("Pleasestatethedisputedvalue");
    protected string PleaseEnterRevisedPenalty = WebCommon.getGlobalResourceValue("PleaseEnterRevisedPenalty");
    protected string PleaseEnterComments = WebCommon.getGlobalResourceValue("PleaseEnterComments");
    protected string DisputedValueCannotGreaterThanCurrentPenalty = WebCommon.getGlobalResourceValue("DisputedValueCannotGreaterThanCurrentPenalty");
    protected string BOStockPlannerMailSubject = WebCommon.getGlobalResourceValue("BOStockPlannerMailSubject");
    protected string Hi = WebCommon.getGlobalResourceValue("Hi");
    protected string VendorQueryMessage1 = WebCommon.getGlobalResourceValue("VendorQueryMessage1");
    protected string VendorQueryMessage2 = WebCommon.getGlobalResourceValue("VendorQueryMessage2");
    protected string Thanks = WebCommon.getGlobalResourceValue("Thanks");
    protected string DisputedValueCannotBeZero = WebCommon.getGlobalResourceValue("DisputedValueCannotBeZero");


    BackOrderBAL oBackOrderBAL = new BackOrderBAL();
    BackOrderBE oBackOrderBE = new BackOrderBE();
    #endregion

    #region Events...
    protected void Page_InIt(object sender, EventArgs e)
    {
        msSite.isMoveAllRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;

            string PreviousMonth = DateTime.Now.AddMonths(-1).ToString("MM");
            drpMonthFrom.SelectedValue = PreviousMonth;

            string CurrentMonth = DateTime.Now.ToString("MM");
            drpMonthTo.SelectedValue = CurrentMonth;

            FillControls.FillDropDown(ref drpYearTo, GetLastYears(), "Year", "Year");
            FillControls.FillDropDown(ref drpYearFrom, GetLastYears(), "Year", "Year");

            if (CurrentMonth.Equals("01"))
            {
                int currentYear = DateTime.Now.Year;
                drpYearFrom.SelectedValue = (currentYear - 1).ToString();
            }

            if (GetQueryStringValue("FromDashboard") != null)
            {
                BindVendorView();
            }
            if (Session["Role"].ToString().ToLower() == "vendor")
            {
                trVendor.Visible = false;
            }
        }
    }
    private DataTable GetLastYears()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Year");
        int iStartYear = DateTime.Now.Year;
        for (int i = iStartYear; i >= iStartYear - 2; i--)
        {
            if (i >= 2018)
            {
                DataRow dr = dt.NewRow();
                dr["Year"] = i;
                dt.Rows.Add(dr);
            }
        }
        return dt;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindVendorView();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        msSite.setSitesOnPostBack();
        msVendor.setVendorsOnPostBack();

        string vikingSkuId = ViewState["SelectedVIKINGSKU"] != null ? ViewState["SelectedVIKINGSKU"].ToString() : "";
        ucListBox lstRightViking = UcVikingSku.FindControl("lstRight") as ucListBox;
        string hdnSkuid = string.Empty;
        lstRightViking.Items.Clear();
        if (lstRightViking != null && !string.IsNullOrEmpty(vikingSkuId))
        {
            HiddenField hdfSelectedId = UcVikingSku.FindControl("hiddenSelectedId") as HiddenField;
            lstRightViking.Items.Clear();
            string[] strVikingIDs = vikingSkuId.Split(',');
            for (int index = 0; index < strVikingIDs.Length; index++)
            {
                string Item = strVikingIDs[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    hdnSkuid += Item + ",";
                    lstRightViking.Items.Add(listItem);
                }
            }
            hdfSelectedId.Value = hdnSkuid;
        }

        string VendorCode = ViewState["SelectedmsVendorItem"] != null ? ViewState["SelectedmsVendorItem"].ToString() : "";
        ucListBox lstRightVendorCode = msVendorItem.FindControl("lstRight") as ucListBox;
        string hdnVendorCode = string.Empty;
        lstRightVendorCode.Items.Clear();
        if (lstRightVendorCode != null && !string.IsNullOrEmpty(VendorCode))
        {
            HiddenField hdfSelectedId = msVendorItem.FindControl("hiddenSelectedId") as HiddenField;
            lstRightVendorCode.Items.Clear();
            string[] strVendorCodes = VendorCode.Split(',');
            for (int index = 0; index < strVendorCodes.Length; index++)
            {
                string Item = strVendorCodes[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    hdnVendorCode += Item + ",";
                    lstRightVendorCode.Items.Add(listItem);
                }
            }
            hdfSelectedId.Value = hdnVendorCode;
        }






        string OdSkuId = ViewState["SelectedODSKU"] != null ? ViewState["SelectedODSKU"].ToString() : "";
        ucListBox lstRightOD = msSKU.FindControl("lstRight") as ucListBox;
        string hdnValue = string.Empty;
        lstRightOD.Items.Clear();
        if (lstRightOD != null && !string.IsNullOrEmpty(OdSkuId))
        {
            HiddenField hdfSelectedId = msSKU.FindControl("hiddenSelectedId") as HiddenField;
            lstRightOD.Items.Clear();
            string[] strODIDs = OdSkuId.Split(',');
            for (int index = 0; index < strODIDs.Length; index++)
            {
                string Item = strODIDs[index];
                if (!string.IsNullOrEmpty(Item))
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = Item;
                    lstRightOD.Items.Add(listItem);
                    hdnValue += Item + ",";
                }
            }
            hdfSelectedId.Value = hdnValue;
        }


        pnlGrid.Visible = false;
        pnlSelection.Visible = true;
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        BindVendorView();
        LocalizeGridHeader(gvVendorView);
        gvVendorView.Columns[0].Visible = false;
        WebCommon.ExportHideHidden("VendorView", gvVendorView);
    }

    protected void btnAccept_Click(object sender, EventArgs e)
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            foreach (GridViewRow row in gvVendorView.Rows)
            {
                CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;
                HiddenField hdnBOReportingID = row.FindControl("hdnBOReportingID") as HiddenField;
                ucLabel lblPotentialPenaltyCharge = row.FindControl("ltPotentialPenaltyCharge") as ucLabel;
                if (chkSelect != null && hdnBOReportingID != null)
                {
                    if (chkSelect.Checked)
                    {
                        oBackOrderBE.SelectedBOReportingID += string.Format(",{0}", hdnBOReportingID.Value);
                        oBackOrderBE.SelectedBOIDPC += string.Format(",{0}~{1}", hdnBOReportingID.Value, lblPotentialPenaltyCharge.Text);
                    }
                }
            }

            oBackOrderBE.SelectedBOIDPC = oBackOrderBE.SelectedBOIDPC.TrimStart(',');
            oBackOrderBE.Action = "UpdateVendorReviewStatus";
            oBackOrderBE.VendorBOReviewStatus = "Accepted";
            int? iResult = oBackOrderBAL.UpdateVendorReviewStatusBAL(oBackOrderBE);


            mdlAcceptCharge.Show();

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        try
        {
            txtDisputedValue.Text = string.Empty;
            txtRevisedPenalty.Text = string.Empty;
            txtComments.Text = string.Empty;

            decimal PenaltyCharge = 0;



            foreach (GridViewRow row in gvVendorView.Rows)
            {

                CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;
                HiddenField hdnBOReportingID = row.FindControl("hdnBOReportingID") as HiddenField;
                HiddenField hdnVendorId = row.FindControl("hdnVendorID") as HiddenField;
                ucLabel ltodsku = row.FindControl("ltodsku") as ucLabel;
                ucLabel ltDateBOIncurred = row.FindControl("ltDateBOIncurred") as ucLabel;
                ucLabel ltMonth = row.FindControl("ltPenaltyRelatingTo") as ucLabel;
                HiddenField hdnYear = row.FindControl("hdnYear") as HiddenField;
                ucLabel ltTotalCountBO = row.FindControl("ltCountBo") as ucLabel;
                ucLabel ltPotentialPenaltyCharge = row.FindControl("ltPotentialPenaltyCharge") as ucLabel;


                if (chkSelect.Checked && ltodsku.Text != null)
                {
                    PenaltyCharge = Convert.ToDecimal(ltPotentialPenaltyCharge.Text);

                    ViewState["VendorId"] = hdnVendorId.Value;
                    ViewState["OdSKUNo"] = ltodsku.Text;
                    ViewState["BOIncurredDate"] = Common.GetMM_DD_YYYY(ltDateBOIncurred.Text);
                    ViewState["Month"] = ltMonth.Text;
                    ViewState["Year"] = hdnYear.Value;
                    ViewState["BoReportingId"] = hdnBOReportingID.Value;
                    ViewState["CountofBO"] = Convert.ToInt32(ltTotalCountBO.Text);
                    break;
                }

            }

            txtCurrentPenaltyCharge.Text = Convert.ToString(PenaltyCharge);
            txtDisputedValue.Text = Convert.ToString(PenaltyCharge);
            txtRevisedPenalty.Text = "0.00";
            mdlQueryCharge.Show();

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
            oBackOrderBE.Action = "UpdateVendorReviewStatus";
            oBackOrderBE.SelectedODSkUCode = Convert.ToString(ViewState["OdSKUNo"]);
            oBackOrderBE.VendorBOReviewStatus = "Query";
            oBackOrderBE.SelectedBOReportingID = Convert.ToString(ViewState["BoReportingId"]);
            oBackOrderBE.Vendor.VendorID = Convert.ToInt32(ViewState["VendorId"]);
            oBackOrderBE.SKU.OD_SKU_NO = Convert.ToString(ViewState["OdSKUNo"]);
            oBackOrderBE.BOIncurredDate = Convert.ToDateTime(ViewState["BOIncurredDate"]);
            oBackOrderBE.SelectedMonth = Convert.ToString(ViewState["Month"]);
            oBackOrderBE.Year = Convert.ToString(ViewState["Year"]);
            oBackOrderBE.NoofBO = Convert.ToInt32(ViewState["CountofBO"]);
            oBackOrderBE.PotentialPenaltyCharge = Convert.ToDecimal(txtCurrentPenaltyCharge.Text);
            oBackOrderBE.DisputedValue = Convert.ToDecimal(txtDisputedValue.Text);
            oBackOrderBE.RevisedPenalty = Convert.ToDecimal(txtRevisedPenalty.Text);
            oBackOrderBE.VendorComment = Convert.ToString(txtComments.Text);
            oBackOrderBE.PenaltyType = "BO";

            int? iResult = oBackOrderBAL.UpdateVendorReviewStatusBAL(oBackOrderBE);

            if (iResult == 0)
            {
                SendSPCommunication();
            }

            mdlQueryMsg.Show();

        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }




    }

    protected void btnBack_1_Click(object sender, EventArgs e)
    {
        int pageindex = pager1.CurrentIndex;
        BindVendorView(pageindex);
        pager1.Visible = true;
    }

    protected void btnAcceptOk_Click(object sender, EventArgs e)
    {
        int pageindex = pager1.CurrentIndex;
        BindVendorView(pageindex);
        pager1.Visible = true;

    }

    protected void gvVendorView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["lstBackOrderBE"] != null)
        {
            gvVendorView.DataSource = (List<BackOrderBE>)ViewState["lstBackOrderBE"];
            gvVendorView.PageIndex = e.NewPageIndex;
            if (Session["VendorView"] != null)
            {
                Hashtable htVendorView = (Hashtable)Session["PendingPenalty"];
                htVendorView.Remove("PageIndex");
                htVendorView.Add("PageIndex", e.NewPageIndex);
                Session["VendorView"] = htVendorView;
            }
            gvVendorView.DataBind();
        }

    }



    protected void gvVendorView_OnDataBound(object sender, EventArgs e)
    {
        try
        {
            for (int i = gvVendorView.Rows.Count - 1; i > 0; i--)
            {
                GridViewRow row = gvVendorView.Rows[i];
                GridViewRow previousRow = gvVendorView.Rows[i - 1];
                var ltDateBOIncurred = (Label)row.FindControl("ltDateBOIncurred");
                var ltDateBOIncurredP = (Label)previousRow.FindControl("ltDateBOIncurred");
                var ltODSKUNO = (Label)row.FindControl("ltodsku");
                var ltODSKUNOP = (Label)previousRow.FindControl("ltodsku");
                var ltSite = (Label)row.FindControl("ltsite");
                var ltSiteP = (Label)previousRow.FindControl("ltsite");

                if (ltODSKUNO.Text == ltODSKUNOP.Text)
                {
                    if (ltDateBOIncurred.Text == ltDateBOIncurredP.Text)
                    {
                        if (ltSite.Text == ltSiteP.Text)
                        {
                            if (previousRow.Cells[1].RowSpan == 0)
                            {
                                if (row.Cells[1].RowSpan == 0)
                                {
                                    previousRow.Cells[0].RowSpan += 2;
                                    previousRow.Cells[1].RowSpan += 2;
                                    previousRow.Cells[2].RowSpan += 2;
                                    previousRow.Cells[3].RowSpan += 2;
                                    previousRow.Cells[4].RowSpan += 2;
                                    previousRow.Cells[5].RowSpan += 2;
                                    previousRow.Cells[6].RowSpan += 2;
                                    previousRow.Cells[7].RowSpan += 2;
                                    previousRow.Cells[8].RowSpan += 2;
                                    previousRow.Cells[9].RowSpan += 2;
                                    previousRow.Cells[10].RowSpan += 2;
                                    previousRow.Cells[11].RowSpan += 2;
                                    previousRow.Cells[12].RowSpan += 2;
                                    previousRow.Cells[13].RowSpan += 2;
                                    previousRow.Cells[18].RowSpan += 2;
                                    previousRow.Cells[19].RowSpan += 2;
                                }
                                else
                                {
                                    previousRow.Cells[0].RowSpan = row.Cells[0].RowSpan + 1;
                                    previousRow.Cells[1].RowSpan = row.Cells[1].RowSpan + 1;
                                    previousRow.Cells[2].RowSpan = row.Cells[2].RowSpan + 1;
                                    previousRow.Cells[3].RowSpan = row.Cells[3].RowSpan + 1;
                                    previousRow.Cells[4].RowSpan = row.Cells[4].RowSpan + 1;
                                    previousRow.Cells[5].RowSpan = row.Cells[5].RowSpan + 1;
                                    previousRow.Cells[6].RowSpan = row.Cells[6].RowSpan + 1;
                                    previousRow.Cells[7].RowSpan = row.Cells[7].RowSpan + 1;
                                    previousRow.Cells[8].RowSpan = row.Cells[8].RowSpan + 1;
                                    previousRow.Cells[9].RowSpan = row.Cells[9].RowSpan + 1;
                                    previousRow.Cells[10].RowSpan = row.Cells[10].RowSpan + 1;
                                    previousRow.Cells[11].RowSpan = row.Cells[11].RowSpan + 1;
                                    previousRow.Cells[12].RowSpan = row.Cells[12].RowSpan + 1;
                                    previousRow.Cells[13].RowSpan = row.Cells[13].RowSpan + 1;
                                    previousRow.Cells[18].RowSpan = row.Cells[18].RowSpan + 1;
                                    previousRow.Cells[19].RowSpan = row.Cells[19].RowSpan + 1;
                                }
                                row.Cells[0].Visible = false;
                                row.Cells[1].Visible = false;
                                row.Cells[2].Visible = false;
                                row.Cells[3].Visible = false;
                                row.Cells[4].Visible = false;
                                row.Cells[5].Visible = false;
                                row.Cells[6].Visible = false;
                                row.Cells[7].Visible = false;
                                row.Cells[8].Visible = false;
                                row.Cells[9].Visible = false;
                                row.Cells[10].Visible = false;
                                row.Cells[11].Visible = false;
                                row.Cells[12].Visible = false;
                                row.Cells[13].Visible = false;
                                row.Cells[18].Visible = false;
                                row.Cells[19].Visible = false;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    #endregion

    #region Methods..

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currentPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currentPageIndx;
        BindVendorView(currentPageIndx);
    }

    private void BindVendorView(int Page = 1)
    {

        try
        {

            oBackOrderBE.User = new SCT_UserBE();

            oBackOrderBE.Action = "GetVendorViewOnly";
            oBackOrderBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
            oBackOrderBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
            if (GetQueryStringValue("FromDashboard") != null)
            {
                oBackOrderBE.VendorBOReviewStatus = "Pending";
            }
            else
            {
                int numberOfDays = DateTime.DaysInMonth(Convert.ToInt16(drpYearTo.SelectedValue), Convert.ToInt16(drpMonthTo.SelectedValue));

                oBackOrderBE.SelectedDateFrom = Convert.ToDateTime(drpYearFrom.SelectedValue + "-" + drpMonthFrom.SelectedValue + "-" + "01");
                oBackOrderBE.SelectedDateTo = Convert.ToDateTime(drpYearTo.SelectedValue + "-" + drpMonthTo.SelectedValue + "-" + numberOfDays.ToString());
                oBackOrderBE.VendorBOReviewStatus = rblStatus.SelectedValue == "1" ? "All" : (rblStatus.SelectedValue == "2" ? "Pending" : (rblStatus.SelectedValue == "3" ? "Accepted" : (rblStatus.SelectedValue == "4" ? "Query" : (rblStatus.SelectedValue == "5" ? "DisputeAccepted" : (rblStatus.SelectedValue == "6" ? "DisputeDeclined" : "")))));
                oBackOrderBE.MediatorBOReviewStatus = rblStatus.SelectedValue == "8" ? "MediatorDeclined" : (rblStatus.SelectedValue == "7" ? "MediatorAccepted" : "");
            }

            if (rblStatus.SelectedValue == "1" || rblStatus.SelectedValue == "2")
            {
                btnAccept.Visible = true;
                btnQuery.Visible = true;
            }
            else
            {
                btnAccept.Visible = false;
                btnQuery.Visible = false;
            }


            oBackOrderBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            if (Convert.ToString(Session["Role"]) == "Vendor")
            {
                oBackOrderBE.User.RoleName = Session["Role"] as string;
            }
            else
            {
                oBackOrderBE.User.RoleName = null;
            }

            oBackOrderBE.SelectedODSkUCode = msSKU.SelectedSKUName;
            oBackOrderBE.SelectedVikingSkUCode = UcVikingSku.SelectedSKUName;
            oBackOrderBE.SelectedVendorCode = msVendorItem.SelectedVendorItemName;
            ViewState["SelectedVIKINGSKU"] = UcVikingSku.SelectedSKUName;
            ViewState["SelectedODSKU"] = msSKU.SelectedSKUName;
            ViewState["SelectedmsVendorItem"] = msVendorItem.SelectedVendorItemName;
            if (IsExportClicked)
            {
                oBackOrderBE.GridCurrentPageNo = 0;
                oBackOrderBE.GridPageSize = 0;
            }
            else
            {
                oBackOrderBE.GridCurrentPageNo = Page;
                oBackOrderBE.GridPageSize = gridPageSize;
            }

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetVendorViewBAL(oBackOrderBE);


            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                btnExportToExcel.Visible = true;
                gvVendorView.DataSource = null;
                gvVendorView.DataSource = lstBackOrderBE;
                gvVendorView.DataBind();
                pager1.ItemCount = Convert.ToDouble(lstBackOrderBE[0].TotalRecords.ToString());
                ViewState["lstBackOrderBE"] = lstBackOrderBE;
                if (pager1.ItemCount > gridPageSize)
                    pager1.Visible = true;
                else
                    pager1.Visible = false;

                gvVendorView.PageIndex = Page;
                pager1.CurrentIndex = Page;
            }
            else
            {
                gvVendorView.DataSource = null;
                gvVendorView.DataBind();
                btnExportToExcel.Visible = false;
                pager1.Visible = false;
                btnAccept.Visible = false;
                btnQuery.Visible = false;
            }


            oBackOrderBAL = null;

            pnlGrid.Visible = true;
            pnlSelection.Visible = false;


        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }


    #endregion
    protected void gvVendorView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Label lblVendorStatus = (Label)e.Row.FindControl("lblVendorStatus");
        LinkButton linkStatus = (LinkButton)e.Row.FindControl("ltStatus");

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (!IsExportClicked)
            {
                if (lblVendorStatus.Text.Equals("Pending with Vendor") || lblVendorStatus.Text.Equals("Vendor Not Replied Mediator to Check") || lblVendorStatus.Text.Equals("Alternate Agreement by Mediator"))
                {
                    lblVendorStatus.Visible = true;
                    linkStatus.Visible = false;
                }
                else
                {
                    lblVendorStatus.Visible = false;
                    linkStatus.Visible = true;
                }
            }
            else
            {
                lblVendorStatus.Text = null;
            }
        }

    }
    protected void gvVendorView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int borepindex = Convert.ToInt32(e.CommandArgument.ToString().Split(',').ElementAtOrDefault(0));
            string status = Convert.ToString(e.CommandArgument.ToString().Split(',').ElementAtOrDefault(1));

            oBackOrderBE.User = new SCT_UserBE();

            oBackOrderBE.Action = "GetVendorView";
            oBackOrderBE.BOReportingID = borepindex;
            oBackOrderBE.SelectedSiteIDs = msSite.SelectedSiteIDs;
            oBackOrderBE.SelectedVendorIDs = msVendor.SelectedVendorIDs;
            int numberOfDays = DateTime.DaysInMonth(Convert.ToInt16(drpYearTo.SelectedValue), Convert.ToInt16(drpMonthTo.SelectedValue));

            oBackOrderBE.SelectedDateFrom = Convert.ToDateTime(drpYearFrom.SelectedValue + "-" + drpMonthFrom.SelectedValue + "-" + "01");
            oBackOrderBE.SelectedDateTo = Convert.ToDateTime(drpYearTo.SelectedValue + "-" + drpMonthTo.SelectedValue + "-" + numberOfDays.ToString());
            oBackOrderBE.VendorBOReviewStatus = GetQueryStringValue("FromDashboard") != null
                ? "Pending"
                : rblStatus.SelectedValue == "1" ? "All" : (rblStatus.SelectedValue == "2" ? "Pending" : (rblStatus.SelectedValue == "3" ? "Accepted" : (rblStatus.SelectedValue == "4" ? "Query" : (rblStatus.SelectedValue == "5" ? "DisputeAccepted" : "DisputeDeclined"))));

            oBackOrderBE.User.UserID = Convert.ToInt32(Session["UserID"]);
            oBackOrderBE.User.RoleName = Convert.ToString(Session["Role"]) == "Vendor" ? Session["Role"] as string : null;
            BackOrderBE lstBackOrderBEobj = new BackOrderBE();

            var varBackOrderBEobj = oBackOrderBAL.GetVendorViewBALPopUp(oBackOrderBE);
            if (varBackOrderBEobj.Count > 0)
            {
                lstBackOrderBEobj = varBackOrderBEobj[0];
                if (status.Contains("Accepted"))
                {
                    string disputestatus = string.Empty;
                    string penaltystatus = string.Empty;
                    disputestatus = Convert.ToString(lstBackOrderBEobj.DisputeReviewStatus);
                    if (disputestatus != "" && disputestatus != null)
                    {
                        pnlDisputedAccepted.Visible = true;
                        lblProposeCharge.Text = Convert.ToString(lstBackOrderBEobj.PotentialPenaltyCharge);
                        lblDisputeValue.Text = Convert.ToString(lstBackOrderBEobj.DisputedValue);
                        DateTime date = Convert.ToDateTime(lstBackOrderBEobj.VendorActionDate);
                        lblDateDisputedRaised.Text = date.ToString("dd/MM/yyyy HH:mm");
                        lblDisputedBy.Text = Convert.ToString(lstBackOrderBEobj.VendorActionTakenName);
                        lbldisputedReason.Text = Convert.ToString(lstBackOrderBEobj.VendorComment);
                        DateTime datedispute = Convert.ToDateTime(lstBackOrderBEobj.DisputeReviewActionDate);
                        lblDateResponse.Text = datedispute.ToString("dd/MM/yyyy HH:mm") + "<br>";
                        lblResponseFrom.Text = Convert.ToString(lstBackOrderBEobj.DisputeReviewActionTakenName);
                        lblResponseDetailed.Text = Convert.ToString(lstBackOrderBEobj.SPComment);
                        ModalPopupDisputeAccepted.Show();

                    }
                    else
                    {
                        DateTime datepnlsts = Convert.ToDateTime(lstBackOrderBEobj.VendorActionDate);
                        lblDateAccepted.Text = datepnlsts.ToString("dd/MM/yyyy HH:mm");
                        lblAcceptedBy.Text = Convert.ToString(lstBackOrderBEobj.VendorActionTakenName);
                        lstBackOrderBEobj = varBackOrderBEobj[0];
                        mdlPenaltyPopup.Show();
                    }


                }
                else
                {
                    if (status == "Declined")
                    {
                        pnlDisputedDeclined.Visible = true;
                        lblDeclineProChrg.Text = Convert.ToString(lstBackOrderBEobj.PotentialPenaltyCharge);
                        lblDeclineDisputeval.Text = Convert.ToString(lstBackOrderBEobj.DisputedValue);
                        DateTime date = Convert.ToDateTime(lstBackOrderBEobj.VendorActionDate);
                        lblDeclineDisputeraisedDate.Text = date.ToString("dd/MM/yyyy HH:mm");
                        lblDeclineDisputedBy.Text = Convert.ToString(lstBackOrderBEobj.VendorActionTakenName);
                        lblDeclineReason.Text = Convert.ToString(lstBackOrderBEobj.VendorComment);
                        DateTime MediatorActiondate = Convert.ToDateTime(lstBackOrderBEobj.MediatorActionDate);
                        lblDeclineDate.Text = MediatorActiondate.ToString("dd/MM/yyyy HH:mm");
                        lblDeclineResponseFrom.Text = Convert.ToString(lstBackOrderBEobj.MediatorActionTakenName);
                        lblDeclinedResponseDetailed.Text = Convert.ToString(lstBackOrderBEobj.MediatorComments);
                        ModalPopupDisputeDeclined.Show();
                    }

                    if (status == "In Mediation" || status == "In Dispute - Under Investigation")
                    {
                        lblDisputeUnderDate.Text = Convert.ToString(lstBackOrderBEobj.PotentialPenaltyCharge);
                        lblDisputeUnderDisputeValue.Text = Convert.ToString(lstBackOrderBEobj.DisputedValue);
                        DateTime date = Convert.ToDateTime(lstBackOrderBEobj.VendorActionDate);
                        lblDisputeUnderDisputeRaised.Text = date.ToString("dd/MM/yyyy HH:mm");
                        lblDisputeUnderDisputeBy.Text = Convert.ToString(lstBackOrderBEobj.VendorActionTakenName);
                        lblDisputeUnderReasonForDispute.Text = Convert.ToString(lstBackOrderBEobj.VendorComment);
                        ModalPopupDisputeUnderInvestigation.Show();
                    }
                }
            }
        }
    }

    #region Mail
    private void SendSPCommunication()
    {
        try
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            oBackOrderBE.SiteID = Convert.ToInt32(ViewState["SiteId"]);

            oBackOrderBE.Action = "GetBOSPCommDetails";


            foreach (GridViewRow row in gvVendorView.Rows)
            {
                CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;
                HiddenField hdnStockPlannerID = row.FindControl("hdnStockPlannerID") as HiddenField;
                if (chkSelect != null && hdnStockPlannerID != null && chkSelect.Checked)
                {
                    oBackOrderBE.StockPlannerID = Convert.ToInt32(hdnStockPlannerID.Value);
                    break;
                }
            }

            string sentTo;
            string language;

            List<BackOrderBE> lstBackOrderBE = oBackOrderBAL.GetStockPlannerContactDetailsBAL(oBackOrderBE);
            if (lstBackOrderBE != null && lstBackOrderBE.Count > 0)
            {
                foreach (BackOrderBE item in lstBackOrderBE)
                {
                    if (item.Vendor.StockPlannerContact != null || item.Vendor.LanguageID != 0)
                    {
                        sentTo = item.Vendor.StockPlannerContact.ToString();
                        language = item.Vendor.LanguageID.ToString();

                        if (!string.IsNullOrEmpty(sentTo) && !string.IsNullOrEmpty(language))
                        {
                            SendMailToStockPlanner(sentTo, language);
                        }
                    }
                }
            }

            Page.UICulture = Convert.ToString(Session["CultureInfo"]);
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    private void SendMailToStockPlanner(string toAddress, string language)
    {
        if (!string.IsNullOrEmpty(toAddress))
        {
            BackOrderBAL oBackOrderBAL = new BackOrderBAL();
            BackOrderBE oBackOrderBE = new BackOrderBE();

            #region Logic to Save & Send email ...
            var oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            var oLanguages = oAPPBOK_CommunicationBAL.GetLanguages();

            foreach (MAS_LanguageBE objLanguage in oLanguages)
            {
                bool MailSentInLanguage = false;
                if (objLanguage.LanguageID.Equals(Convert.ToInt32(language)))
                    MailSentInLanguage = true;

                var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                var templateFile = string.Format(@"{0}emailtemplates/BOPenalty/StockPlannerEmail.english.htm", path);

                #region Setting reason as per the language ...
                switch (objLanguage.LanguageID)
                {
                    case 1:
                        Page.UICulture = clsConstants.EnglishISO;
                        break;
                    case 2:
                        Page.UICulture = clsConstants.FranceISO;
                        break;
                    case 3:
                        Page.UICulture = clsConstants.GermanyISO;
                        break;
                    case 4:
                        Page.UICulture = clsConstants.NederlandISO;
                        break;
                    case 5:
                        Page.UICulture = clsConstants.SpainISO;
                        break;
                    case 6:
                        Page.UICulture = clsConstants.ItalyISO;
                        break;
                    case 7:
                        Page.UICulture = clsConstants.CzechISO;
                        break;
                    default:
                        Page.UICulture = clsConstants.EnglishISO;
                        break;
                }

                #endregion

                #region  Prepairing html body format ...

                string Country = string.Empty;
                string VendorDescription = string.Empty;

                foreach (GridViewRow row in gvVendorView.Rows)
                {
                    CheckBox chkSelect = row.FindControl("chkSelect") as CheckBox;
                    Label ltVendorNoValue = row.FindControl("ltVendorNoValue") as Label;
                    Label ltVendor = row.FindControl("ltVendor") as Label;
                    HiddenField hdnCountryName = row.FindControl("hdnCountryName") as HiddenField;

                    if (chkSelect != null)
                    {
                        if (chkSelect.Checked)
                        {
                            VendorDescription = Convert.ToString(ltVendorNoValue.Text) + "-" + Convert.ToString(ltVendor.Text) + "(" + Convert.ToString(hdnCountryName.Value) + ")";
                            break;
                        }
                    }
                }

                string htmlBody = null;


                VendorQueryMessage1 = VendorQueryMessage1.Replace("{VendorDescription}", VendorDescription);
                BOStockPlannerMailSubject = BOStockPlannerMailSubject.Replace("{VendorDescription}", VendorDescription);



                using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                {
                    htmlBody = sReader.ReadToEnd();
                    htmlBody = htmlBody.Replace("{Hi}", WebCommon.getGlobalResourceValue("Hi"));
                    htmlBody = htmlBody.Replace("{VendorQueryMessage1}", VendorQueryMessage1);
                    htmlBody = htmlBody.Replace("{VendorQueryMessage2}", WebCommon.getGlobalResourceValue("VendorQueryMessage2"));
                    htmlBody = htmlBody.Replace("{Thanks}", WebCommon.getGlobalResourceValue("Thanks"));
                    htmlBody = htmlBody.Replace("{VIPAdmin}", WebCommon.getGlobalResourceValue("VIPAdmin"));
                }
                #endregion

                #region Sending and saving email details ...

                var oBackOrderMailBE = new BackOrderMailBE();
                oBackOrderMailBE.Action = "AddCommunication";
                oBackOrderMailBE.CommunicationType = "Vendor Query Comm";
                oBackOrderMailBE.Subject = BOStockPlannerMailSubject;
                oBackOrderMailBE.SentTo = toAddress;
                oBackOrderMailBE.SentDate = DateTime.Now;
                oBackOrderMailBE.Body = htmlBody;
                oBackOrderMailBE.SendByID = Convert.ToInt32(Session["UserID"]);
                oBackOrderMailBE.LanguageId = objLanguage.LanguageID;
                oBackOrderMailBE.MailSentInLanguage = MailSentInLanguage;
                oBackOrderMailBE.CommunicationStatus = "Initial";
                BackOrderBAL oBackOrderCommBAL = new BackOrderBAL();
                var CommId = oBackOrderCommBAL.AddCommunicationDetailBAL(oBackOrderMailBE);
                if (MailSentInLanguage)
                {
                    var emailToAddress = toAddress;
                    var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                    var emailToSubject = BOStockPlannerMailSubject;
                    var emailBody = htmlBody;
                    Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                    oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                }
                #endregion
            }


            #endregion
        }
    }

    #endregion

}
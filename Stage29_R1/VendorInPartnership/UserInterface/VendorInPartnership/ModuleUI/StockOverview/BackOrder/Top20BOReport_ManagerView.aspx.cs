﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.Security;
using System.Drawing;
using Utilities;

public partial class ModuleUI_StockOverview_BackOrder_Top20BOReport_ManagerView : CommonPage
{
    BackOrderBE oBackOrderBE = new BackOrderBE();
    BackOrderBAL oBackOrderBAL = new BackOrderBAL();

    protected void Page_Init(object sender, EventArgs e)
    {
        ddlSite.CurrentPage = this;
        ddlSite.IsAllRequired = true;
        ucCountry.CurrentPage = this;
        ucCountry.IsAllRequired = true;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        btnExportToExcel.CurrentPage = this;
        btnExportToExcel.GridViewControl = ucGridView1;
        btnExportToExcel.FileName = "Top 20 Backorder Report - Manager View";
        if (!IsPostBack)
        {
            BindCategory();
        }
    }

    protected void BindCategory()
    {
        StockPlannerGroupingsBE oStockPlannerGroupingsBE = new StockPlannerGroupingsBE();
        StockPlannerGroupingsBAL oStockPlannerGroupingsBAL = new StockPlannerGroupingsBAL();
        oStockPlannerGroupingsBE.Action = "ShowAll";
        List<StockPlannerGroupingsBE> lstStockPlannerGrouping = new List<StockPlannerGroupingsBE>();
        lstStockPlannerGrouping = oStockPlannerGroupingsBAL.GetStockPlannerGroupingsBAL(oStockPlannerGroupingsBE);
        if (lstStockPlannerGrouping.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCategory, lstStockPlannerGrouping, "StockPlannerGroupings", "StockPlannerGroupingsID", "--All--");
        }
       
    
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        BackOrderBE oBackOrderBE = new BackOrderBE();
        BackOrderBAL oBackOrderBAL = new BackOrderBAL();

         oBackOrderBE.Action = "ManagerView";
         if (!string.IsNullOrEmpty(ddlCategory.SelectedValue))
         {
             oBackOrderBE.StockPlannerGroupingID = Convert.ToInt32(ddlCategory.SelectedValue);
             oBackOrderBE.SiteID = Convert.ToInt32(ddlSite.innerControlddlSite.SelectedItem.Value);
             oBackOrderBE.CountryID = Convert.ToInt32(ucCountry.innerControlddlCountry.SelectedItem.Value);
             oBackOrderBE.OpenOverdue = GetViewValue();
             oBackOrderBE.OrderBy = GetOredrByValue();
             List<BackOrderBE> lstBackOrders = oBackOrderBAL.GetBackOrderTop20_ManagerViewBAL(oBackOrderBE);
             lstBackOrders.ToList().ForEach(x => x.SelectedDateFrom = DateTime.Now.Date);
             lstBackOrders.ToList().ForEach(x => x.SelectedDateTo = DateTime.Now.Date);
             if (lstBackOrders.Count > 0)
             {
                 ucGridView1.DataSource = lstBackOrders;
                 ucGridView1.DataBind();
             }
             else
             {
                 ucGridView1.DataSource = null;
                 ucGridView1.DataBind();
             }
         }
   }

    private string GetViewValue()
    {
        string checkedValue = string.Empty;
        if (rdoallonly.Checked == true)
        {
            checkedValue = "A";
        }
        else if (rdoOpenOverdue.Checked == true)
        {
            checkedValue = "O";
        }
        return checkedValue;
    }

    private string GetOredrByValue()
    {
        string checkedValue = string.Empty;
        if (rdoValue.Checked == true)
        {
            checkedValue = "V";
        }
        else if (rdoCount.Checked == true)
        {
            checkedValue = "C";
        }
        else if (rdoPlanner.Checked == true)
        {
            checkedValue = "P";
        }
        return checkedValue;
    }
    protected void ucGridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label txtDate = (Label)e.Row.FindControl("txtCommentdate");     
            if (!string.IsNullOrEmpty(txtDate.Text))
            {
                if ((Convert.ToDateTime(Common.GetMM_DD_YYYY(txtDate.Text)) - DateTime.Now).TotalDays <= 0)
                {
                    txtDate.ForeColor = Color.Red;
                }
                else
                {
                    txtDate.ForeColor = Color.Black;
                }
            }
        }
    }
}
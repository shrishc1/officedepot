﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Collections;
using System.Text;
using WebUtilities;
using BaseControlLibrary;
using System.IO;
using System.Linq;
using System.Drawing;

public partial class BackOrderReporting : CommonPage
{
    DataView dvSql = new DataView();


    #region Declarations ...
    private const int gridPageSize = 200;
    bool IsExportClicked = false;
    #endregion

    #region Events

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
        msSite.isMoveAllRequired = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            pager1.PageSize = gridPageSize;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;

            txtFromDate.Text = GetLastBusinessDay().ToString("dd/MM/yyyy");
            txtToDate.Text = txtFromDate.Text;
            hdnJSFromDt.Value = txtFromDate.Text;
            hdnJSToDt.Value = txtToDate.Text;
            ltBackOrderReporting.Text = "Back Order Reporting";
            ltCurrentDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            FillControls.FillDropDown(ref drpYear, GetLastYears(), "Year", "Year");

        }

        if (Page.IsPostBack)
            msStockPlanner.SetSPOnPostBack();
    }



    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindReport(currnetPageIndx);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        pager1.CurrentIndex = 1;
        BindReport(1);
        btnBackSearch.Visible = true;
    }



    /// <summary>
    /// Export To Excel Click Functionality
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExport_Click(object sender, EventArgs e)
    {

        IsExportClicked = true;
        BindReport();
        if (rdoDailyBackOrder.Checked)
        {
            if (rdoDetailedLevel.Checked)
            {
                gvBOListing.Columns[2].Visible = true;
                gvBOListing.Columns[3].Visible = true;
                gvBOListing.Columns[4].Visible = true;
                gvBOListing.Columns[5].Visible = true;
                gvBOListing.Columns[6].Visible = false;
                gvBOListing.Columns[7].Visible = false;
                gvBOListing.Columns[8].Visible = false;
                gvBOListing.Columns[9].Visible = false;
                gvBOListing.Columns[10].Visible = false;
                gvBOListing.Columns[11].Visible = false;
                gvBOListing.Columns[12].Visible = false;
                gvBOListing.Columns[19].Visible = false;
                gvBOListing.Columns[20].Visible = false;
                WebCommon.ExportHideHidden("DailyBackOrderListing", gvBOListing);
            }
            else if (rdoSummaryLevel.Checked)
            {

                WebCommon.ExportHideHidden("DailyBackOrderSummary", gvBOSummary);
            }
        }
        else if (rdoOpenBackOrder.Checked)
        {
            if (rdoOpenDetailedLavel.Checked)
            {
                gvBOListing.Columns[2].Visible = false;
                gvBOListing.Columns[3].Visible = false;
                gvBOListing.Columns[4].Visible = false;
                gvBOListing.Columns[5].Visible = false;
                gvBOListing.Columns[6].Visible = true;
                gvBOListing.Columns[7].Visible = true;
                gvBOListing.Columns[8].Visible = true;
                gvBOListing.Columns[9].Visible = true;
                gvBOListing.Columns[10].Visible = true;
                gvBOListing.Columns[11].Visible = true;
                gvBOListing.Columns[12].Visible = true;
                gvBOListing.Columns[19].Visible = true;
                gvBOListing.Columns[20].Visible = true;
                gvBOListing.Columns[17].Visible = false;
                WebCommon.ExportHideHidden("OpenBackOrderDetailListing", gvBOListing);
            }
            else if (rdoSummaryVendor.Checked)
            {
                WebCommon.ExportHideHidden("OpenBackOrderSummaryVendor", gvBOSummary);
            }
            else if (rdoSummarySite.Checked)
            {
                WebCommon.ExportHideHidden("OpenBackOrderSummarySite", gvBOSummarySite);
            }
        }
        else if (rdoYTDBackOrder.Checked)
        {
            if (rdoYTDMasterVendor.Checked)
            {

                WebCommon.Export("YTDBackOrderListingMaster", grdYTDMaster);
            }
            else if (rdoYTDListing.Checked)
            {
                WebCommon.Export("YTDBackOrderListing", gvYTDBOListing);
            }
        }
    }

    /// <summary>
    /// YTDMasterVendor Export to Click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }

    protected void btnBackSearch_Click(object sender, EventArgs e)
    {

        EncryptQueryString("BackOrderReporting.aspx");
        btnSearch.Visible = false;

    }



    protected void gvDailyBOListing_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //if (ViewState["lstBackOrder"] != null)
        //{
        //    //grdRevisedleadListing.DataSource = (List<Up_PurchaseOrderDetailBE>)ViewState["lstSites"];
        //   // gvDailyBOListing.PageIndex = e.NewPageIndex;
        //    if (Session["BackOrder"] != null)
        //    {
        //        Hashtable htBackOrder = (Hashtable)Session["BackOrder"];
        //        htBackOrder.Remove("PageIndex");
        //        htBackOrder.Add("PageIndex", e.NewPageIndex);
        //        Session["BackOrder"] = htBackOrder;
        //    }
        //   // gvDailyBOListing.DataBind();
        //}
    }


    #endregion

    #region Methods




    private void BindReport(int Page = 1)
    {


        if (!string.IsNullOrEmpty(msCountry.SelectedCountryIDs))
            hdnCountryIDs.Value = msCountry.SelectedCountryIDs;

        if (!string.IsNullOrEmpty(msItemClassification.selectedItemClassification))
            hdnItemclassifications.Value = msItemClassification.selectedItemClassification;

        if (!string.IsNullOrEmpty(msStockPlanner.SelectedStockPlannerIDs))
            hdnStockPlannerIDs.Value = msStockPlanner.SelectedStockPlannerIDs;

        if (!string.IsNullOrEmpty(msVendor.SelectedVendorIDs))
            hdnVendorIDs.Value = msVendor.SelectedVendorIDs;

        if (!string.IsNullOrEmpty(msSite.SelectedSiteIDs))
            hdnSiteIDs.Value = msSite.SelectedSiteIDs;
        if (!(string.IsNullOrEmpty(multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs)))
            hdnStockPlannerGroupingID.Value = multiSelectStockPlannerGrouping.SelectedStockPlannerGroupingIDs;

        if (!(string.IsNullOrEmpty(msSKU.SelectedSKUName)))
        {
            hdnSelectedODSkUCode.Value = msSKU.SelectedSKUName;
        }

        if (!(string.IsNullOrEmpty(UcVikingSku.SelectedSKUName)))
        {
            hdnSelectedVikingSkUCode.Value = UcVikingSku.SelectedSKUName;
        }   

        if (rdoDailyBackOrder.Checked)
        {

            if (!string.IsNullOrEmpty(txtFromDate.Text))
                hdnFromDt.Value = Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSFromDt.Value).Day;
            if (!string.IsNullOrEmpty(txtToDate.Text))
                hdnToDt.Value = Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Year + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Month + "-" + Utilities.Common.TextToDateFormat(hdnJSToDt.Value).Day;


            if (rdoDetailedLevel.Checked)
            {
                hdnReportType.Value = "DailyBackOrderDetail";
                ltBackOrderReporting.Text = "Daily Back Order Listing";

            }
            else if (rdoSummaryLevel.Checked)
            {

                ltBackOrderReporting.Text = "Daily Back Order Listing";
                hdnReportType.Value = "DailyBackOrderSummary";
                ltBackOrderReporting.Text = "Daily Back Order Summary";

            }
        }
        else if (rdoOpenBackOrder.Checked)
        {

            if (!string.IsNullOrEmpty(txtFromDate.Text))
                hdnFromDt.Value = DateTime.Now.ToString("yyyy-MM-dd");
            if (!string.IsNullOrEmpty(txtToDate.Text))
                hdnToDt.Value = DateTime.Now.ToString("yyyy-MM-dd");

            hdnJSFromDt.Value = DateTime.Now.ToString("yyyy-MM-dd");



            if (rdoOpenDetailedLavel.Checked)
            {

                hdnReportType.Value = "OpenBackOrderDetail";
                ltBackOrderReporting.Text = "Open Back Order Detailed Listing";

            }
            else if (rdoSummaryVendor.Checked)
            {
                hdnReportType.Value = "OpenBackOrderSummary";
                ltBackOrderReporting.Text = "Open Back Order Summary Vendor";

            }
            else if (rdoSummarySite.Checked)
            {
                hdnReportType.Value = "OpenBackOrderSummarySite";
                ltBackOrderReporting.Text = "Open Back Order Summary Site";

            }
        }
        else if (rdoYTDBackOrder.Checked)
        {

            if (!string.IsNullOrEmpty(txtFromDate.Text))
                hdnFromDt.Value = DateTime.Now.ToString("yyyy-MM-dd");
            if (!string.IsNullOrEmpty(txtToDate.Text))
                hdnToDt.Value = DateTime.Now.ToString("yyyy-MM-dd");

            hdnJSFromDt.Value = drpYear.SelectedItem.Value;

            if (rdoYTDMasterVendor.Checked)
            {
                hdnReportType.Value = "YTDBackOrderListingMaster";
                ltBackOrderReporting.Text = "YTD Master Vendor Back Order";
               // BindRDLCReport("YTDMasterVendorBackOrder.rdlc");
                btnExportToExcel.Visible = true;
                pager1.Visible = true;
            }
            else if (rdoYTDListing.Checked)
            {
                hdnReportType.Value = "YTDBackOrderListing";
                if (rblByVendor.Checked)
                {
                    hdnChildReportType.Value = "ByVendor";
                }
                else if (rblByQuantity.Checked)
                {
                    hdnChildReportType.Value = "ByVendorSP";
                }

                if (rblByValue.Checked)
                {
                    hdnChildReportTypeInner.Value = "ByValue";
                }
                else if (rblByQuantity.Checked)
                {
                    hdnChildReportTypeInner.Value = "ByValueQTY";
                }
                ltBackOrderReporting.Text = "YTD  Back Order Listing";

            }
        }

        if (IsExportClicked)
        {
            hdnGridCurrentPageNo.Value = "0";
            hdnGridPageSize.Value = "0";
        }
        else
        {
            hdnGridCurrentPageNo.Value = Page.ToString();
            hdnGridPageSize.Value = gridPageSize.ToString();
        }

        //if (hdnReportType.Value != "YTDBackOrderListingExport" && hdnReportType.Value != "YTDBackOrderListingMaster")
        //{
        dvSql = (DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty);

        int totalRecords = dvSql.Count;
        int pageSize = 200;
        int startRow = (Convert.ToInt32(hdnGridCurrentPageNo.Value) - 1) * pageSize;

        if (!IsExportClicked)
        {
            if (dvSql.Count > 0)
            {
                pager1.ItemCount = Convert.ToDouble(dvSql[0]["TotalRecords"].ToString());
                pager1.CurrentIndex = Page;
                btnExportToExcel.Visible = true;
                pager1.Visible = true;
            }
            else
            {
                pager1.Visible = false;
                btnExportToExcel.Visible = false;
            }
        }

        if (rdoDailyBackOrder.Checked)
        {
            if (rdoDetailedLevel.Checked)
            {
                gvBOListing.Visible = true;
                gvBOListing.PageIndex = Page;
                if (dvSql.Count > 0)
                {

                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        if (Convert.ToString(ViewState["SortDirection"]) == "Ascending")
                        {
                            dvSql.Sort = Convert.ToString(ViewState["SortExpression"]) + " ASC";
                        }
                        else
                        {
                            dvSql.Sort = Convert.ToString(ViewState["SortExpression"]) + " DESC";
                        }

                    }

                    if (!IsExportClicked)
                    {
                        DataTable dt = dvSql.ToTable().Rows.Cast<DataRow>().ToList().Skip(startRow).Take(pageSize).CopyToDataTable();
                        dvSql = new DataView(dt);
                    }

                    gvBOListing.DataSource = dvSql;
                    gvBOListing.DataBind();
                }
            }
            else if (rdoSummaryLevel.Checked)
            {
                gvBOSummary.Visible = true;
                gvBOSummary.PageIndex = Page;
                if (dvSql.Count > 0)
                {

                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        if (Convert.ToString(ViewState["SortDirection"]) == "Ascending")
                        {
                            dvSql.Sort = Convert.ToString(ViewState["SortExpression"]) + " ASC";
                        }
                        else
                        {
                            dvSql.Sort = Convert.ToString(ViewState["SortExpression"]) + " DESC";
                        }

                    }

                    if (!IsExportClicked)
                    {
                        DataTable dt = dvSql.ToTable().Rows.Cast<DataRow>().ToList().Skip(startRow).Take(pageSize).CopyToDataTable();
                        dvSql = new DataView(dt);
                    }
                    gvBOSummary.DataSource = dvSql;
                    gvBOSummary.DataBind();
                }
            }
        }
        else if (rdoOpenBackOrder.Checked)
        {
            if (rdoOpenDetailedLavel.Checked)
            {

                gvBOListing.Visible = true;
                gvBOListing.Columns[17].Visible = false;
                gvBOListing.PageIndex = Page;
                if (dvSql.Count > 0)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        if (Convert.ToString(ViewState["SortDirection"]) == "Ascending")
                        {
                            dvSql.Sort = Convert.ToString(ViewState["SortExpression"]) + " ASC";
                        }
                        else
                        {
                            dvSql.Sort = Convert.ToString(ViewState["SortExpression"]) + " DESC";
                        }

                    }
                    if (!IsExportClicked)
                    {
                        DataTable dt = dvSql.ToTable().Rows.Cast<DataRow>().ToList().Skip(startRow).Take(pageSize).CopyToDataTable();
                        dvSql = new DataView(dt);
                    }
                    gvBOListing.DataSource = dvSql;
                    gvBOListing.DataBind();
                }
            }
            else if (rdoSummaryVendor.Checked)
            {

                gvBOSummary.Visible = true;
                gvBOSummary.PageIndex = Page;
                if (dvSql.Count > 0)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        if (Convert.ToString(ViewState["SortDirection"]) == "Ascending")
                        {
                            dvSql.Sort = Convert.ToString(ViewState["SortExpression"]) + " ASC";
                        }
                        else
                        {
                            dvSql.Sort = Convert.ToString(ViewState["SortExpression"]) + " DESC";
                        }

                    }

                    if (!IsExportClicked)
                    {
                        DataTable dt = dvSql.ToTable().Rows.Cast<DataRow>().ToList().Skip(startRow).Take(pageSize).CopyToDataTable();
                        dvSql = new DataView(dt);
                    }
                    gvBOSummary.DataSource = dvSql;
                    gvBOSummary.DataBind();
                }
            }
            else if (rdoSummarySite.Checked)
            {

                gvBOSummarySite.Visible = true;
                gvBOSummarySite.PageIndex = Page;
                if (dvSql.Count > 0)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        if (Convert.ToString(ViewState["SortDirection"]) == "Ascending")
                        {
                            dvSql.Sort = Convert.ToString(ViewState["SortExpression"]) + " ASC";
                        }
                        else
                        {
                            dvSql.Sort = Convert.ToString(ViewState["SortExpression"]) + " DESC";
                        }

                    }
                    if (!IsExportClicked)
                    {
                        DataTable dt = dvSql.ToTable().Rows.Cast<DataRow>().ToList().Skip(startRow).Take(pageSize).CopyToDataTable();
                        dvSql = new DataView(dt);
                    }

                    gvBOSummarySite.DataSource = dvSql;
                    gvBOSummarySite.DataBind();
                }
            }
        }
        else if (rdoYTDBackOrder.Checked)
        {
            if (rdoYTDMasterVendor.Checked)
            {
                grdYTDMaster.Visible = true;
                grdYTDMaster.PageIndex = Page;
                if (dvSql.Count > 0)
                {
                    //if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    //{
                    //    if (Convert.ToString(ViewState["SortDirection"]) == "Ascending")
                    //    {
                    //        dvSql.Sort = Convert.ToString(ViewState["SortExpression"]) + " ASC";
                    //    }
                    //    else
                    //    {
                    //        dvSql.Sort = Convert.ToString(ViewState["SortExpression"]) + " DESC";
                    //    }

                    //}
                    if (!IsExportClicked)
                    {
                        DataTable dt = dvSql.ToTable().Rows.Cast<DataRow>().ToList().Skip(startRow).Take(pageSize).CopyToDataTable();
                        dvSql = new DataView(dt);
                    }
                    grdYTDMaster.DataSource = dvSql;
                    grdYTDMaster.DataBind();
                }
            }
            else if (rdoYTDListing.Checked)
            {
                tblFilter.Visible = true;
                gvYTDBOListing.Visible = true;
                gvYTDBOListing.PageIndex = Page;
                if (dvSql.Count > 0)
                {
                    gvYTDBOListing.DataSource = dvSql;
                    gvYTDBOListing.DataBind();
                }
            }
        }

        pnlSearchScreen.Visible = false;
        pnlReportViewer.Visible = false;
        pnlGrids.Visible = true;
    }
   // }


    private void BindRDLCReport(string reportName)
    {
        var reportPath = Server.MapPath("~") + "\\ModuleUI\\StockOverview\\BackOrder\\RDLC\\" + reportName;
        BackOrderReportViewer.LocalReport.ReportPath = reportPath;

        string baseUrl = Request.Url.Scheme + @"://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + '/';

        ReportParameter[] reportParameter = new ReportParameter[8];
        reportParameter[0] = new ReportParameter("Country", msCountry.SelectedCountryName);
        reportParameter[1] = new ReportParameter("ItemClassification", msItemClassification.selectedItemClassification);
        reportParameter[2] = new ReportParameter("SPName", msStockPlanner.SelectedSPName);
        reportParameter[3] = new ReportParameter("Vendor", msVendor.SelectedVendorName);
        reportParameter[4] = new ReportParameter("Site", msSite.SelectedSiteName);
        reportParameter[5] = new ReportParameter("DateFrom", hdnJSFromDt.Value);
        if (reportName != "rptYTDBackOrderListing.rdlc")
            reportParameter[6] = new ReportParameter("DateTo", hdnJSToDt.Value);
        else
            reportParameter[6] = new ReportParameter("DateTo", baseUrl);
        reportParameter[7] = new ReportParameter("ReportType", hdnReportType.Value);
        BackOrderReportViewer.LocalReport.EnableHyperlinks = true;
        BackOrderReportViewer.HyperlinkTarget = "_blank";
        BackOrderReportViewer.LocalReport.SetParameters(reportParameter);


        pnlSearchScreen.Visible = false;
        pnlGrids.Visible = false;
        pnlReportViewer.Visible = true;
    }

    public DateTime GetLastBusinessDay()
    {
        DateTime LastBusinessDay = DateTime.Now.AddDays(-1);


        if (LastBusinessDay.DayOfWeek == DayOfWeek.Sunday)
        {
            LastBusinessDay = LastBusinessDay.AddDays(-2);
        }
        else if (LastBusinessDay.DayOfWeek == DayOfWeek.Saturday)
        {
            LastBusinessDay = LastBusinessDay.AddDays(-1);
        }

        return LastBusinessDay;

    }

    private DataTable GetLastYears()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Year");
        int iStartYear = DateTime.Now.Year;
        for (int i = iStartYear; i >= iStartYear - 2; i--)
        {
            if (i >= 2014)
            {
                DataRow dr = dt.NewRow();
                dr["Year"] = i;
                dt.Rows.Add(dr);
            }
        }
        return dt;
    }

    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }



    public static void ExportHideHidden(string fileName, GridView gv)
    {
        //for left align
        string style = @"<style> .text { mso-number-format:\@;text-align:left;border:.5pt solid black;} </style> ";
        System.Web.HttpContext.Current.Response.Clear();
        System.Web.HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));

        //System.Web.HttpContext.Current.Response.ContentType = "application/ms-excel";
        System.Web.HttpContext.Current.Response.ContentType = "application/vnd.xls";

        System.Web.HttpContext.Current.Response.Charset = "utf-8";
        System.Web.HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");

        using (System.IO.StringWriter sw = new System.IO.StringWriter())
        {
            using (HtmlTextWriter htw = new HtmlTextWriter(sw))
            {
                // Create a form to contain the grid
                Table table = new Table();

                // add the header row to the table
                if (gv.HeaderRow != null)
                {
                    //for left align
                    for (int columnIndex = 0; columnIndex < gv.HeaderRow.Cells.Count; columnIndex++)
                    {
                        if (gv.Columns[columnIndex].Visible == true)
                        {
                            gv.HeaderRow.Cells[columnIndex].Attributes.Add("class", "text");
                            gv.HeaderRow.Cells[columnIndex].Style.Add("background-color", "#DBDBDB");
                        }
                        else
                        {
                            gv.HeaderRow.Cells[columnIndex].Style.Add("display", "none");
                            gv.HeaderRow.Cells[columnIndex].Text = string.Empty;
                        }
                    }
                    PrepareControlForExport(gv.HeaderRow);
                    table.Rows.Add(gv.HeaderRow);
                }


                // add each of the data rows to the table
                foreach (GridViewRow row in gv.Rows)
                {
                    PrepareControlForExport(row);
                    for (int columnIndex = 0; columnIndex < row.Cells.Count; columnIndex++)
                    {
                        if (gv.Columns[columnIndex].Visible == true)
                        {
                            row.Cells[columnIndex].Attributes.Add("class", "text");
                        }
                        else
                        {
                            row.Cells[columnIndex].Style.Add("display", "none");

                        }
                    }
                    table.Rows.Add(row);
                }

                // add the footer row to the table
                if (gv.FooterRow != null)
                {
                    PrepareControlForExport(gv.FooterRow);
                    table.Rows.Add(gv.FooterRow);
                }

                //  render the table into the htmlwriter
                for (int iRow = 0; iRow < table.Rows.Count; iRow++)
                {
                    table.Rows[iRow].Cells[10].Visible = false;
                }

                table.RenderControl(htw);
                System.Web.HttpContext.Current.Response.Write(style);
                System.Web.HttpContext.Current.Response.Write(sw.ToString());
                System.Web.HttpContext.Current.Response.End();
            }
        }
    }

    private static void PrepareControlForExport(Control control)
    {
        for (int i = 0; i < control.Controls.Count; i++)
        {
            Control current = control.Controls[i];
            if (current is Label)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as Label).Text));
            }
            if (current is LinkButton)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
            }
            else if (current is ImageButton)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
            }
            else if (current is HyperLink)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
            }
            else if (current is DropDownList)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
            }
            else if (current is CheckBox)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
            }

            else if (current is RadioButton)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as RadioButton).Checked ? "True" : "False"));
            }
            else if (current is HiddenField)
            {
                control.Controls.Remove(current);
                //control.Controls.AddAt(i, new LiteralControl((current as HiddenField).Value));
            }
            else if (current is Button)
            {
                control.Controls.Remove(current);
                // control.Controls.AddAt(i, new LiteralControl((current as Button).Text));
            }

            if (current.HasControls())
            {
                PrepareControlForExport(current);
            }
        }
    }

    private static void PrepareVisibleControlForExport(Control control)
    {
        for (int i = 0; i < control.Controls.Count; i++)
        {
            Control current = control.Controls[i];
            if (current is Label)
            {
                var v = (Label)current;
                if (!string.IsNullOrEmpty(v.Text))
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as Label).Text));
                }
            }
            if (current is LinkButton)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
            }
            else if (current is ImageButton)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
            }
            else if (current is HyperLink)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
            }
            else if (current is DropDownList)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
            }
            else if (current is CheckBox)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
            }

            else if (current is RadioButton)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as RadioButton).Checked ? "True" : "False"));
            }
            else if (current is HiddenField)
            {
                control.Controls.Remove(current);
                //control.Controls.AddAt(i, new LiteralControl((current as HiddenField).Value));
            }
            else if (current is Button)
            {
                control.Controls.Remove(current);
                // control.Controls.AddAt(i, new LiteralControl((current as Button).Text));
            }

            if (current.HasControls())
            {
                PrepareVisibleControlForExport(current);
            }
        }
    }

    #endregion


    protected void gvBOListing_Sorting(object sender, GridViewSortEventArgs e)
    {
        int totalRecords = dvSql.Count;
        int pageSize = 200;
        int startRow = (Convert.ToInt32(hdnGridCurrentPageNo.Value) - 1) * pageSize;     

        dvSql = (DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty);
        ViewState["SortExpression"] = e.SortExpression;
        if (Convert.ToString(ViewState["SortExpression"]) == e.SortExpression && Convert.ToString(ViewState["SortDirection"]) == Convert.ToString(e.SortDirection))
        {
            dvSql.Sort = e.SortExpression + " DESC";
            ViewState["SortDirection"] = "DESC";
        }
        else
        {
            dvSql.Sort = e.SortExpression + " ASC";
            ViewState["SortDirection"] = "Ascending";
        }

        DataTable dt = dvSql.ToTable().Rows.Cast<DataRow>().ToList().Skip(startRow).Take(pageSize).CopyToDataTable();
        dvSql = new DataView(dt);
        gvBOListing.DataSource = dvSql;
        gvBOListing.DataBind();

        pager1.ItemCount = Convert.ToDouble(dvSql[0]["TotalRecords"].ToString());
        pager1.CurrentIndex = Convert.ToInt32(hdnGridCurrentPageNo.Value);
        btnExportToExcel.Visible = true;
        pager1.Visible = true;
    }
    protected void gvBOSummary_Sorting(object sender, GridViewSortEventArgs e)
    {
        int totalRecords = dvSql.Count;
        int pageSize = 200;
        int startRow = (Convert.ToInt32(hdnGridCurrentPageNo.Value) - 1) * pageSize;     

        dvSql = (DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty);
        ViewState["SortExpression"] = e.SortExpression;
        if (Convert.ToString(ViewState["SortExpression"]) == e.SortExpression && Convert.ToString(ViewState["SortDirection"]) == Convert.ToString(e.SortDirection))
        {
            dvSql.Sort = e.SortExpression + " DESC";
            ViewState["SortDirection"] = "DESC";
        }
        else
        {
            dvSql.Sort = e.SortExpression + " ASC";
            ViewState["SortDirection"] = "Ascending";
        }

        DataTable dt = dvSql.ToTable().Rows.Cast<DataRow>().ToList().Skip(startRow).Take(pageSize).CopyToDataTable();
        dvSql = new DataView(dt);
        gvBOSummary.DataSource = dvSql;
        gvBOSummary.DataBind();

        pager1.ItemCount = Convert.ToDouble(dvSql[0]["TotalRecords"].ToString());
        pager1.CurrentIndex = Convert.ToInt32(hdnGridCurrentPageNo.Value);
        btnExportToExcel.Visible = true;
        pager1.Visible = true;
    }
    protected void gvBOSummarySite_Sorting(object sender, GridViewSortEventArgs e)
    {
        int totalRecords = dvSql.Count;
        int pageSize = 200;       
        int startRow = (Convert.ToInt32(hdnGridCurrentPageNo.Value) - 1) * pageSize;     

        dvSql = (DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty);
        ViewState["SortExpression"] = e.SortExpression;
        if (Convert.ToString(ViewState["SortExpression"]) == e.SortExpression && Convert.ToString(ViewState["SortDirection"]) == Convert.ToString(e.SortDirection))
        {
            dvSql.Sort = e.SortExpression + " DESC";
            ViewState["SortDirection"] = "DESC";
        }
        else
        {
            dvSql.Sort = e.SortExpression + " ASC";
            ViewState["SortDirection"] = "Ascending";
        }

        DataTable dt = dvSql.ToTable().Rows.Cast<DataRow>().ToList().Skip(startRow).Take(pageSize).CopyToDataTable();
        dvSql = new DataView(dt);
        gvBOSummarySite.DataSource = dvSql;
        gvBOSummarySite.DataBind();

        pager1.ItemCount = Convert.ToDouble(dvSql[0]["TotalRecords"].ToString());
        pager1.CurrentIndex = Convert.ToInt32(hdnGridCurrentPageNo.Value);
        btnExportToExcel.Visible = true;
        pager1.Visible = true;
    }
    protected void gvBOListing_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (rdoOpenBackOrder.Checked)
        {
            if (rdoOpenDetailedLavel.Checked)
            {
                gvBOListing.Columns[2].Visible = false;
                gvBOListing.Columns[3].Visible = false;
                gvBOListing.Columns[4].Visible = false;
                gvBOListing.Columns[5].Visible = false;               
                gvBOListing.Columns[6].Visible = true;
                gvBOListing.Columns[7].Visible = true;
                gvBOListing.Columns[8].Visible = true;
                gvBOListing.Columns[9].Visible = true;
                gvBOListing.Columns[10].Visible = true;
                gvBOListing.Columns[11].Visible = true;
                gvBOListing.Columns[12].Visible = true;              
                gvBOListing.Columns[19].Visible = true;
                gvBOListing.Columns[20].Visible = true;
            }
        }
        else
        {
            gvBOListing.Columns[2].Visible = true;
            gvBOListing.Columns[3].Visible = true;
            gvBOListing.Columns[4].Visible = true;
            gvBOListing.Columns[5].Visible = true;
            gvBOListing.Columns[6].Visible = false;
            gvBOListing.Columns[7].Visible = false;
            gvBOListing.Columns[8].Visible = false;
            gvBOListing.Columns[9].Visible = false;
            gvBOListing.Columns[10].Visible = false;
            gvBOListing.Columns[11].Visible = false;
            gvBOListing.Columns[12].Visible = false;
            gvBOListing.Columns[19].Visible = false;
            gvBOListing.Columns[20].Visible = false;
        }
    }   
    protected void rblByQuantity_CheckedChanged(object sender, EventArgs e)
    {
        BindReport();
    }
    protected void rblByValue_CheckedChanged(object sender, EventArgs e)
    {
        BindReport();
    }
    protected void gvYTDBOListing_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Literal ltIsvendorsearch = (Literal)e.Row.FindControl("ltIsvendorsearch");
            if (ltIsvendorsearch.Text == "isvendorsearch")
            {
                HyperLink hlJan = (HyperLink)e.Row.FindControl("hlJan");
                hlJan.NavigateUrl = EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + DataBinder.Eval(e.Row.DataItem, "VendorID") + "&SiteID=" + hdnSiteIDs.Value + "&DateFrom=" + hdnJSFromDt.Value + "&MonthValue=01&OpenSubReport=Yes&SPNO_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerNumber") + "&SPNAME_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerName") + "&StockPlannerGroupingID=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerGroupingsID") + "&ODSkUCode=" + DataBinder.Eval(e.Row.DataItem, "ODCode") + "&VikingSkUCode=" + DataBinder.Eval(e.Row.DataItem, "VikingCode") + "");

                HyperLink hlFeb = (HyperLink)e.Row.FindControl("hlFeb");
                hlFeb.NavigateUrl = EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + DataBinder.Eval(e.Row.DataItem, "VendorID") + "&SiteID=" + hdnSiteIDs.Value + "&DateFrom=" + hdnJSFromDt.Value + "&MonthValue=02&OpenSubReport=Yes&SPNO_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerNumber") + "&SPNAME_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerName") + "&StockPlannerGroupingID=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerGroupingsID") + "&ODSkUCode=" + DataBinder.Eval(e.Row.DataItem, "ODCode") + "&VikingSkUCode=" + DataBinder.Eval(e.Row.DataItem, "VikingCode") + "");
                HyperLink hlMar = (HyperLink)e.Row.FindControl("hlMar");
                hlMar.NavigateUrl = EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + DataBinder.Eval(e.Row.DataItem, "VendorID") + "&SiteID=" + hdnSiteIDs.Value + "&DateFrom=" + hdnJSFromDt.Value + "&MonthValue=03&OpenSubReport=Yes&SPNO_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerNumber") + "&SPNAME_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerName") + "&StockPlannerGroupingID=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerGroupingsID") + "&ODSkUCode=" + DataBinder.Eval(e.Row.DataItem, "ODCode") + "&VikingSkUCode=" + DataBinder.Eval(e.Row.DataItem, "VikingCode") + "");

                HyperLink hlApr = (HyperLink)e.Row.FindControl("hlApr");
                hlApr.NavigateUrl = EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + DataBinder.Eval(e.Row.DataItem, "VendorID") + "&SiteID=" + hdnSiteIDs.Value + "&DateFrom=" + hdnJSFromDt.Value + "&MonthValue=04&OpenSubReport=Yes&SPNO_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerNumber") + "&SPNAME_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerName") + "&StockPlannerGroupingID=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerGroupingsID") + "&ODSkUCode=" + DataBinder.Eval(e.Row.DataItem, "ODCode") + "&VikingSkUCode=" + DataBinder.Eval(e.Row.DataItem, "VikingCode") + "");

                HyperLink hlMay = (HyperLink)e.Row.FindControl("hlMay");
                hlMay.NavigateUrl = EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + DataBinder.Eval(e.Row.DataItem, "VendorID") + "&SiteID=" + hdnSiteIDs.Value + "&DateFrom=" + hdnJSFromDt.Value + "&MonthValue=05&OpenSubReport=Yes&SPNO_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerNumber") + "&SPNAME_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerName") + "&StockPlannerGroupingID=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerGroupingsID") + "&ODSkUCode=" + DataBinder.Eval(e.Row.DataItem, "ODCode") + "&VikingSkUCode=" + DataBinder.Eval(e.Row.DataItem, "VikingCode") + "");

                HyperLink hlJun = (HyperLink)e.Row.FindControl("hlJun");
                hlJun.NavigateUrl = EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + DataBinder.Eval(e.Row.DataItem, "VendorID") + "&SiteID=" + hdnSiteIDs.Value + "&DateFrom=" + hdnJSFromDt.Value + "&MonthValue=06&OpenSubReport=Yes&SPNO_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerNumber") + "&SPNAME_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerName") + "&StockPlannerGroupingID=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerGroupingsID") + "&ODSkUCode=" + DataBinder.Eval(e.Row.DataItem, "ODCode") + "&VikingSkUCode=" + DataBinder.Eval(e.Row.DataItem, "VikingCode") + "");


                HyperLink hlJul = (HyperLink)e.Row.FindControl("hlJul");
                hlJul.NavigateUrl = EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + DataBinder.Eval(e.Row.DataItem, "VendorID") + "&SiteID=" + hdnSiteIDs.Value + "&DateFrom=" + hdnJSFromDt.Value + "&MonthValue=07&OpenSubReport=Yes&SPNO_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerNumber") + "&SPNAME_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerName") + "&StockPlannerGroupingID=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerGroupingsID") + "&ODSkUCode=" + DataBinder.Eval(e.Row.DataItem, "ODCode") + "&VikingSkUCode=" + DataBinder.Eval(e.Row.DataItem, "VikingCode") + "");

                HyperLink hlAug = (HyperLink)e.Row.FindControl("hlAug");
                hlAug.NavigateUrl = EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + DataBinder.Eval(e.Row.DataItem, "VendorID") + "&SiteID=" + hdnSiteIDs.Value + "&DateFrom=" + hdnJSFromDt.Value + "&MonthValue=08&OpenSubReport=Yes&SPNO_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerNumber") + "&SPNAME_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerName") + "&StockPlannerGroupingID=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerGroupingsID") + "&ODSkUCode=" + DataBinder.Eval(e.Row.DataItem, "ODCode") + "&VikingSkUCode=" + DataBinder.Eval(e.Row.DataItem, "VikingCode") + "");

                HyperLink hlSep = (HyperLink)e.Row.FindControl("hlSep");
                hlSep.NavigateUrl = EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + DataBinder.Eval(e.Row.DataItem, "VendorID") + "&SiteID=" + hdnSiteIDs.Value + "&DateFrom=" + hdnJSFromDt.Value + "&MonthValue=09&OpenSubReport=Yes&SPNO_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerNumber") + "&SPNAME_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerName") + "&StockPlannerGroupingID=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerGroupingsID") + "&ODSkUCode=" + DataBinder.Eval(e.Row.DataItem, "ODCode") + "&VikingSkUCode=" + DataBinder.Eval(e.Row.DataItem, "VikingCode") + "");

                HyperLink hlOct = (HyperLink)e.Row.FindControl("hlOct");
                hlOct.NavigateUrl = EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + DataBinder.Eval(e.Row.DataItem, "VendorID") + "&SiteID=" + hdnSiteIDs.Value + "&DateFrom=" + hdnJSFromDt.Value + "&MonthValue=10&OpenSubReport=Yes&SPNO_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerNumber") + "&SPNAME_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerName") + "&StockPlannerGroupingID=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerGroupingsID") + "&ODSkUCode=" + DataBinder.Eval(e.Row.DataItem, "ODCode") + "&VikingSkUCode=" + DataBinder.Eval(e.Row.DataItem, "VikingCode") + "");

                HyperLink hlNov = (HyperLink)e.Row.FindControl("hlNov");
                hlNov.NavigateUrl = EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + DataBinder.Eval(e.Row.DataItem, "VendorID") + "&SiteID=" + hdnSiteIDs.Value + "&DateFrom=" + hdnJSFromDt.Value + "&MonthValue=11&OpenSubReport=Yes&SPNO_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerNumber") + "&SPNAME_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerName") + "&StockPlannerGroupingID=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerGroupingsID") + "&ODSkUCode=" + DataBinder.Eval(e.Row.DataItem, "ODCode") + "&VikingSkUCode=" + DataBinder.Eval(e.Row.DataItem, "VikingCode") + "");

                HyperLink hlDec = (HyperLink)e.Row.FindControl("hlDec");
                hlDec.NavigateUrl = EncryptQuery("BackOrderSubReporting.aspx?VendorId=" + DataBinder.Eval(e.Row.DataItem, "VendorID") + "&SiteID=" + hdnSiteIDs.Value + "&DateFrom=" + hdnJSFromDt.Value + "&MonthValue=12&OpenSubReport=Yes&SPNO_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerNumber") + "&SPNAME_vendor=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerName") + "&StockPlannerGroupingID=" + DataBinder.Eval(e.Row.DataItem, "StockPlannerGroupingsID") + "&ODSkUCode=" + DataBinder.Eval(e.Row.DataItem, "ODCode") + "&VikingSkUCode=" + DataBinder.Eval(e.Row.DataItem, "VikingCode") + "");
            }
        }
    }
    protected void grdYTDMaster_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Literal IsParent = (Literal)e.Row.FindControl("ltIsParent");
             if (IsParent.Text == "1")
             {
                 e.Row.Cells[0].ForeColor = Color.Black;
                 e.Row.Cells[1].ForeColor = Color.Black;
                 e.Row.Cells[2].ForeColor = Color.Black;
                 e.Row.Cells[3].ForeColor = Color.Black;
                 e.Row.Cells[4].ForeColor = Color.Black;
                 e.Row.Cells[5].ForeColor = Color.Black;
                 e.Row.Cells[6].ForeColor = Color.Black;
                 e.Row.Cells[7].ForeColor = Color.Black;
                 e.Row.Cells[8].ForeColor = Color.Black;
                 e.Row.Cells[9].ForeColor = Color.Black;
                 e.Row.Cells[10].ForeColor = Color.Black;
                 e.Row.Cells[11].ForeColor = Color.Black;
                 e.Row.Cells[12].ForeColor = Color.Black;
                 e.Row.Cells[13].ForeColor = Color.Black;
                 e.Row.Cells[14].ForeColor = Color.Black;
                 e.Row.Cells[15].ForeColor = Color.Black;
                 e.Row.Cells[16].ForeColor = Color.Black;
                 e.Row.Cells[17].ForeColor = Color.Black;
                 e.Row.Cells[18].ForeColor = Color.Black;
                 e.Row.Cells[19].ForeColor = Color.Black;
                 e.Row.Cells[20].ForeColor = Color.Black;
                
                 e.Row.Cells[0].Font.Bold = true;
                 e.Row.Cells[1].Font.Bold = true;
                 e.Row.Cells[2].Font.Bold = true;
                 e.Row.Cells[3].Font.Bold = true;
                 e.Row.Cells[4].Font.Bold = true;
                 e.Row.Cells[5].Font.Bold = true;
                 e.Row.Cells[6].Font.Bold = true;
                 e.Row.Cells[7].Font.Bold = true;
                 e.Row.Cells[8].Font.Bold = true;
                 e.Row.Cells[9].Font.Bold = true;
                 e.Row.Cells[10].Font.Bold = true;
                 e.Row.Cells[11].Font.Bold = true;
                 e.Row.Cells[12].Font.Bold = true;
                 e.Row.Cells[13].Font.Bold = true;
                 e.Row.Cells[14].Font.Bold = true;
                 e.Row.Cells[15].Font.Bold = true;
                 e.Row.Cells[16].Font.Bold = true;
                 e.Row.Cells[17].Font.Bold = true;
                 e.Row.Cells[18].Font.Bold = true;
                 e.Row.Cells[19].Font.Bold = true;
                 e.Row.Cells[20].Font.Bold = true;
                
                // e.Row.Cells[22].Font.Bold = true;
                 //Literal ltGlobalConsolidation = (Literal)e.Row.FindControl("ltGlobalConsolidation");
                 //ltGlobalConsolidation.ForeColor=Color.Black;
                 //Literal ltGlobalName = (Literal)e.Row.FindControl("ltGlobalName");
                 //Literal ltLocalConsolidation = (Literal)e.Row.FindControl("ltLocalConsolidation");
                 //Literal ltLocalName = (Literal)e.Row.FindControl("ltLocalName");
                 //Literal ltVendorNo = (Literal)e.Row.FindControl("ltVendorNo");
                 //Literal ltVendorName = (Literal)e.Row.FindControl("ltVendorName");
                 //Literal ltCountry = (Literal)e.Row.FindControl("ltCountry");
                 //Literal ltNumberofBO = (Literal)e.Row.FindControl("ltNumberofBO");
                 //Literal ltCOGS = (Literal)e.Row.FindControl("ltCOGS");
                 //Label hlJan = (Label)e.Row.FindControl("hlJan");
                 //hlJan.Font=font
                 //Label hlFeb = (Label)e.Row.FindControl("hlFeb");
                 //Label hlMar = (Label)e.Row.FindControl("hlMar");
                 //Label hlApr = (Label)e.Row.FindControl("hlApr");
                 //Label hlMay = (Label)e.Row.FindControl("hlMay");
                 //Label hlJun = (Label)e.Row.FindControl("hlJun");
                 //Label hlJul = (Label)e.Row.FindControl("hlJul");
                 //Label hlAug = (Label)e.Row.FindControl("hlAug");
                 //Label hlSep = (Label)e.Row.FindControl("hlSep");
                 //Label hlOct = (Label)e.Row.FindControl("hlOct");
                 //Label hlNov = (Label)e.Row.FindControl("hlNov");
                 //Label hlDec = (Label)e.Row.FindControl("hlDec");
             }
        }
    }
}
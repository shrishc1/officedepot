﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="STK_CurrencyConvertorOverview.aspx.cs" Inherits="STK_CurrencyConvertorOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register src="../../CommonUI/UserControls/ucAddButton.ascx" tagname="ucAddButton" tagprefix="cc2" %>
<%@ Register src="../../CommonUI/UserControls/ucExportToExcel.ascx" tagname="ucExportToExcel" tagprefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblCurrencyConverter" runat="server" Text="Currency Converter"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <cc2:ucAddButton ID="btnAdd" runat="server" NavigateUrl="STK_CurrencyConvertorEdit.aspx" />
        <cc2:ucExportToExcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center" colspan="9">
                <cc1:ucGridView ID="grdConversion" Width="100%" runat="server" CssClass="grid" AutoGenerateColumns="False"
                    CellPadding="0" GridLines="None" onsorting="SortGrid" AllowSorting="true">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                    <Columns>                                                                
                         <asp:TemplateField HeaderText="Rate Applicable Date"  SortExpression="RateApplicableDate" >
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />                            
                            <ItemTemplate >
                                <asp:HyperLink ID="hpRateApplicable" runat="server" Text='<%# Eval("RateApplicableDate","{0:dd/MM/yyyy}")  %>'  NavigateUrl='<%# EncryptQuery("STK_CurrencyConvertorEdit.aspx?CurrencyConversionID="+Eval("CurrencyConversionID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left"  />
                        </asp:TemplateField>                      
                                            

                        <asp:BoundField HeaderText="Conversion Rate" DataField="ConversionRate" SortExpression="ConversionRate">
                            <HeaderStyle HorizontalAlign="Center" Width="15%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>

                        <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="Center" Width="70%" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>


                      
                    </Columns>
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
                </cc1:ucGridView>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
       
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>

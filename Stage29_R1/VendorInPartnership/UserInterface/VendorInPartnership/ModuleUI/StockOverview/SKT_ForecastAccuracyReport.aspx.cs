﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessEntities.ModuleBE.Security;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.IO;
using System.Data;
using Utilities;
using WebUtilities;
using System.ComponentModel;
public partial class SKT_ForecastAccuracyReport : CommonPage
{
    protected string Month1 = string.Empty;
    protected string Month2 = string.Empty;
    protected string Month3 = string.Empty;
    protected string Month4 = string.Empty;
    protected string Month5 = string.Empty;
    protected string Month6 = string.Empty;
    protected string Month7 = string.Empty;
    protected string Month8 = string.Empty;
    protected string Month9 = string.Empty;
    protected string Month10 = string.Empty;
    protected string Month11 = string.Empty;
    protected string Month12 = string.Empty;

    protected decimal HValue = decimal.Zero;
    protected decimal HActualSale = decimal.Zero;
    protected string lblHeading = WebCommon.getGlobalResourceValue("ForecastAccuracyReport");
    protected string shortTerm = WebCommon.getGlobalResourceValue("ShortTerm");
    protected string longTerm = WebCommon.getGlobalResourceValue("LongTerm");
    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            pager1.PageSize = 200;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;

            pager2.PageSize = 200;
            pager2.GenerateGoToSection = false;
            pager2.GeneratePagerInfoSection = false;
            lblForecastAccuracyReport1.Text = lblHeading;
        }
        if (HttpContext.Current.Session["Role"].ToString().ToLower() == "vendor")
        {
            msVendor.FunctionCalled = "BindVendorByVendorId";
            ucVendorTemplateSelect.Visible = false;
        }
    }
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {

        AssignMonthColom();
        UcDataPanel.Visible = false;
        dvExport.Visible = true;
        btBack.Visible = true;
        Ucpnlgrid.Visible = true;

        if (rdoShortTerm1.Checked)
        {
            lblForecastAccuracyReport1.Text = lblHeading + " - " + shortTerm;
            BingShortTermData();
        }
        else
        {
            lblForecastAccuracyReport1.Text = lblHeading + " - " + longTerm;
            BingLongTermData();
        }

    }
    protected void BingShortTermData(int page = 1)
    {
        gdvLongTerm.Visible = false;
        pager2.Visible = false;
        gdvAccuracyData.Visible = true;
        pager1.Visible = true;
        int PageNumber;
        STK_ForecastAccuracyBE forecastBE = new STK_ForecastAccuracyBE();
        forecastBE.CountryIds = msCountry.SelectedCountryIDs;
        forecastBE.VendorIds = msVendor.SelectedVendorIDs;
        forecastBE.ItemCategoryIds = MultiSelectRMSCategory1.SelectedRMSCategoryIDs;
        forecastBE.ItemClassification = msItemClassification.selectedItemClassification;
        forecastBE.Action = "GetShortTermForecastAccuracyData";
        forecastBE.PageSize = page;
        SKT_ForecastAccuracyBAL oMAS_ForecastAccuracyBAL = new SKT_ForecastAccuracyBAL();
        List<STK_ForecastAccuracyBE> lstForecastData = new List<STK_ForecastAccuracyBE>();
        int RecordCount;
        oMAS_ForecastAccuracyBAL.GetForcastAccuracyDataBAL(forecastBE, out lstForecastData, out RecordCount);
        pager1.ItemCount = RecordCount;
        if (RecordCount % 200 == 0)
            PageNumber = RecordCount / 200;
        else
            PageNumber = (RecordCount / 200) + 1;

        if (PageNumber == page)
            gdvAccuracyData.ShowFooter = true;
        ViewState["lstForecastData"] = lstForecastData;
        gdvAccuracyData.DataSource = lstForecastData;
        gdvAccuracyData.DataBind();

    }
    protected void BindShortTermForExport()
    {
        STK_ForecastAccuracyBE forecastBE = new STK_ForecastAccuracyBE();
        forecastBE.CountryIds = msCountry.SelectedCountryIDs;
        forecastBE.VendorIds = msVendor.SelectedVendorIDs;
        forecastBE.ItemCategoryIds = MultiSelectRMSCategory1.SelectedRMSCategoryIDs;
        forecastBE.ItemClassification = msItemClassification.selectedItemClassification;
        forecastBE.Action = "GetShortTermForecastAccuracyData";
        forecastBE.PageSize = 0;
        SKT_ForecastAccuracyBAL oMAS_ForecastAccuracyBAL = new SKT_ForecastAccuracyBAL();
        List<STK_ForecastAccuracyBE> lstForecastData = new List<STK_ForecastAccuracyBE>();
        int RecordCount;
        DataTable dtExport = new DataTable();
        oMAS_ForecastAccuracyBAL.GetForcastAccuracyDataExportBAL(forecastBE, out dtExport, out RecordCount);
        ViewState["lstForecastDataExport"] = lstForecastData;
        ExportToExcel(dtExport, "ForecastAccuracyReport");
        //gdvExportShortTerm.DataSource = lstForecastData;
        //gdvExportShortTerm.DataBind();


    }


    public void ExportToExcel(DataTable dtExport, string fileName)
    {
        HttpContext context = HttpContext.Current;
        string date = DateTime.Now.ToString("ddMMyyyy");
        string time = DateTime.Now.ToString("HH:mm");
        context.Response.ContentType = "text/vnd.ms-excel";
        context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}_{1}_{2}", fileName, date, time + ".xls"));
        //context.Response.Write("<th scope='col'>  <th colspan='9' >&nbsp;</th> <th colspan='4'>YTD Values</th><th colspan='4'>Sep-2015</th><th colspan='4'>Aug-2015</th><th colspan='4'>Jul-2015</th> <th colspan='4'>Jun-2015</th> <th colspan='4'>May-2015</th><th colspan='4'>Apr-2015</th> <th colspan='4'>Mar-2015</th><th colspan='4'>Feb-2015</th><th colspan='4'>Jan-2015</th><th colspan='4'>Dec-2014</th><th colspan='4'>Nov-2014</th><th colspan='4'>Oct-2014</th> <tr>"); 
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("|YTD Values|");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("|" + Month1 + "|");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("|"+Month2+"|");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("|" + Month3 + "|");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("|" + Month4 + "|");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("|" + Month5 + "|");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("|" + Month6 + "|");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("|" + Month7 + "|");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("|" + Month8 + "|");
        //context.Response.Write("\t");       
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("|" + Month9 + "|");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("|" + Month10 + "|");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("|" + Month11+"|");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("\t");
        //context.Response.Write("|" + Month12 + "|");        
        // context.Response.Write(Environment.NewLine);
        if (dtExport.Rows.Count > 0)
        {
            // context.Response.Write("\t");
            for (int i = 0; i < dtExport.Columns.Count; i++)
            {
                switch (dtExport.Columns[i].ColumnName.ToString())
                {
                    case "Vendor_No":
                        context.Response.Write("Vendor");
                        context.Response.Write("\t");
                        break;
                    case "VendorName":
                        context.Response.Write("Vendor Name");
                        context.Response.Write("\t");
                        break;
                    case "Country":
                        context.Response.Write("Country");
                        context.Response.Write("\t");
                        break;
                    case "OD_SkU":
                        context.Response.Write("OD SKU");
                        context.Response.Write("\t");
                        break;
                    case "Viking_SkU":
                        context.Response.Write("Viking SKU");
                        context.Response.Write("\t");
                        break;
                    case "Description":
                        context.Response.Write("Description");
                        context.Response.Write("\t");
                        break;
                    case "Item_Classification":
                        context.Response.Write("Item Classification");
                        context.Response.Write("\t");
                        break;
                    case "Category":
                        context.Response.Write("Category");
                        context.Response.Write("\t");
                        break;
                    case "SiteName":
                        context.Response.Write("Site");
                        context.Response.Write("\t");
                        break;
                    case "Next20DaysForecastValue":
                        context.Response.Write("Short Term (Next 20 days) forecast value");
                        context.Response.Write("\t");
                        break;

                    case "YTDForecastDemand":
                        context.Response.Write("Forecasted Demand-YTD Values");
                        context.Response.Write("\t");
                        break;

                    case "YTDActualSale":
                        context.Response.Write("Actual Sales-YTD Values");
                        context.Response.Write("\t");
                        break;

                    case "YTDAbsoluteDiff":
                        context.Response.Write("Absolute Difference-YTD Values");
                        context.Response.Write("\t");
                        break;
                    case "YTDForecastedAcuracyRate":
                        context.Response.Write("Forecast Accuracy Rate-YTD Values");
                        context.Response.Write("\t");
                        break;

                    case "JanForecastDemand":
                        context.Response.Write("Forecasted Demand-" + Month1 + "");
                        context.Response.Write("\t");
                        break;


                    case "JanActualSale":
                        context.Response.Write("Actual Sales-" + Month1 + "");
                        context.Response.Write("\t");
                        break;

                    case "JanAbsoluteDiff":
                        context.Response.Write("Absolute Difference-" + Month1 + "");
                        context.Response.Write("\t");
                        break;
                    case "JanForecastedAcuracyRate":
                        context.Response.Write("Forecast Accuracy Rate-" + Month1 + "");
                        context.Response.Write("\t");
                        break;

                    case "FebForecastDemand":
                        context.Response.Write("Forecasted Demand-" + Month2 + "");
                        context.Response.Write("\t");
                        break;

                    case "FebActualSale":
                        context.Response.Write("Actual Sales-" + Month2 + "");
                        context.Response.Write("\t");
                        break;

                    case "FebAbsoluteDiff":
                        context.Response.Write("Absolute Difference-" + Month2 + "");
                        context.Response.Write("\t");
                        break;
                    case "FebForecastedAcuracyRate":
                        context.Response.Write("Forecast Accuracy Rate-" + Month2 + "");
                        context.Response.Write("\t");
                        break;

                    case "MarForecastDemand":
                        context.Response.Write("Forecasted Demand-" + Month3 + "");
                        context.Response.Write("\t");
                        break;

                    case "MarActualSale":
                        context.Response.Write("Actual Sales-" + Month3 + "");
                        context.Response.Write("\t");
                        break;

                    case "MarAbsoluteDiff":
                        context.Response.Write("Absolute Difference-" + Month3 + "");
                        context.Response.Write("\t");
                        break;
                    case "MarForecastedAcuracyRate":
                        context.Response.Write("Forecast Accuracy Rate-" + Month3 + "");
                        context.Response.Write("\t");
                        break;

                    case "AprForecastDemand":
                        context.Response.Write("Forecasted Demand-" + Month4 + "");
                        context.Response.Write("\t");
                        break;

                    case "AprActualSale":
                        context.Response.Write("Actual Sales-" + Month4 + "");
                        context.Response.Write("\t");
                        break;

                    case "AprAbsoluteDiff":
                        context.Response.Write("Absolute Difference-" + Month4 + "");
                        context.Response.Write("\t");
                        break;
                    case "AprForecastedAcuracyRate":
                        context.Response.Write("Forecast Accuracy Rate-" + Month4 + "");
                        context.Response.Write("\t");
                        break;

                    case "MayForecastDemand":
                        context.Response.Write("Forecasted Demand-" + Month5 + "");
                        context.Response.Write("\t");
                        break;

                    case "MayActualSale":
                        context.Response.Write("Actual Sales-" + Month5 + "");
                        context.Response.Write("\t");
                        break;

                    case "MayAbsoluteDiff":
                        context.Response.Write("Absolute Difference-" + Month5 + "");
                        context.Response.Write("\t");
                        break;
                    case "MayForecastedAcuracyRate":
                        context.Response.Write("Forecast Accuracy Rate-" + Month5 + "");
                        context.Response.Write("\t");
                        break;

                    case "JunForecastDemand":
                        context.Response.Write("Forecasted Demand-" + Month6 + "");
                        context.Response.Write("\t");
                        break;

                    case "JunActualSale":
                        context.Response.Write("Actual Sales-" + Month6 + "");
                        context.Response.Write("\t");
                        break;

                    case "JunAbsoluteDiff":
                        context.Response.Write("Absolute Difference-" + Month6 + "");
                        context.Response.Write("\t");
                        break;
                    case "JunForecastedAcuracyRate":
                        context.Response.Write("Forecast Accuracy Rate-" + Month6 + "");
                        context.Response.Write("\t");
                        break;

                    case "JulForecastDemand":
                        context.Response.Write("Forecasted Demand-" + Month7 + "");
                        context.Response.Write("\t");
                        break;

                    case "JulActualSale":
                        context.Response.Write("Actual Sales-" + Month7 + "");
                        context.Response.Write("\t");
                        break;

                    case "JulAbsoluteDiff":
                        context.Response.Write("Absolute Difference-" + Month7 + "");
                        context.Response.Write("\t");
                        break;
                    case "JulForecastedAcuracyRate":
                        context.Response.Write("Forecast Accuracy Rate-" + Month7 + "");
                        context.Response.Write("\t");
                        break;

                    case "AugForecastDemand":
                        context.Response.Write("Forecasted Demand-" + Month8 + "");
                        context.Response.Write("\t");
                        break;

                    case "AugActualSale":
                        context.Response.Write("Actual Sales-" + Month8 + "");
                        context.Response.Write("\t");
                        break;

                    case "AugAbsoluteDiff":
                        context.Response.Write("Absolute Difference-" + Month8 + "");
                        context.Response.Write("\t");
                        break;
                    case "AugForecastedAcuracyRate":
                        context.Response.Write("Forecast Accuracy Rate-" + Month8 + "");
                        context.Response.Write("\t");
                        break;

                    case "SepForecastDemand":
                        context.Response.Write("Forecasted Demand-" + Month9 + "");
                        context.Response.Write("\t");
                        break;

                    case "SepActualSale":
                        context.Response.Write("Actual Sales-" + Month9 + "");
                        context.Response.Write("\t");
                        break;

                    case "SepAbsoluteDiff":
                        context.Response.Write("Absolute Difference-" + Month9 + "");
                        context.Response.Write("\t");
                        break;
                    case "SepForecastedAcuracyRate":
                        context.Response.Write("Forecast Accuracy Rate-" + Month9 + "");
                        context.Response.Write("\t");
                        break;

                    case "OctForecastDemand":
                        context.Response.Write("Forecasted Demand-" + Month10 + "");
                        context.Response.Write("\t");
                        break;

                    case "OctActualSale":
                        context.Response.Write("Actual Sales-" + Month10 + "");
                        context.Response.Write("\t");
                        break;

                    case "OctAbsoluteDiff":
                        context.Response.Write("Absolute Difference-" + Month10 + "");
                        context.Response.Write("\t");
                        break;
                    case "OctForecastedAcuracyRate":
                        context.Response.Write("Forecast Accuracy Rate-" + Month10 + "");
                        context.Response.Write("\t");
                        break;

                    case "NovForecastDemand":
                        context.Response.Write("Forecasted Demand-" + Month11 + "");
                        context.Response.Write("\t");
                        break;

                    case "NovActualSale":
                        context.Response.Write("Actual Sales-" + Month11 + "");
                        context.Response.Write("\t");
                        break;

                    case "NovAbsoluteDiff":
                        context.Response.Write("Absolute Difference-" + Month11 + "");
                        context.Response.Write("\t");
                        break;
                    case "NovForecastedAcuracyRate":
                        context.Response.Write("Forecast Accuracy Rate-" + Month11 + "");
                        context.Response.Write("\t");
                        break;

                    case "DecForecastDemand":
                        context.Response.Write("Forecasted Demand-" + Month12 + "");
                        context.Response.Write("\t");
                        break;

                    case "DecActualSale":
                        context.Response.Write("Actual Sales-" + Month12 + "");
                        context.Response.Write("\t");
                        break;

                    case "DecAbsoluteDiff":
                        context.Response.Write("Absolute Difference-" + Month12 + "");
                        context.Response.Write("\t");
                        break;
                    case "DecForecastedAcuracyRate":
                        context.Response.Write("Forecast Accuracy Rate-" + Month12 + "");
                        context.Response.Write("\t");
                        break;

                    //default:
                    //    context.Response.Write(dtExport.Columns[i].ColumnName);
                    //    break;
                }

            }
        }

        context.Response.Write(Environment.NewLine);

        //Write data
        foreach (DataRow row in dtExport.Rows)
        {

            for (int i = 0; i < dtExport.Columns.Count; i++)
            {
                context.Response.Write(row.ItemArray[i].ToString());
                context.Response.Write("\t");
            }
            context.Response.Write(Environment.NewLine);
        }
        context.Response.End();
    }



    protected void BingLongTermData(int page = 1)
    {
        gdvAccuracyData.Visible = false;
        pager1.Visible = false;
        gdvLongTerm.Visible = true;
        pager2.Visible = true;
        int PageNumber;
        STK_ForecastAccuracyBE forecastBE = new STK_ForecastAccuracyBE();
        forecastBE.CountryIds = msCountry.SelectedCountryIDs;
        forecastBE.VendorIds = msVendor.SelectedVendorIDs;
        forecastBE.ItemCategoryIds = MultiSelectRMSCategory1.SelectedRMSCategoryIDs;
        forecastBE.ItemClassification = msItemClassification.selectedItemClassification;
        forecastBE.Action = "GetLongTermForecastAccuracyData";
        forecastBE.PageSize = page;
        SKT_ForecastAccuracyBAL oMAS_ForecastAccuracyBAL = new SKT_ForecastAccuracyBAL();
        List<STK_ForecastAccuracyBE> lstForecastData = new List<STK_ForecastAccuracyBE>();
        int RecordCount;
        oMAS_ForecastAccuracyBAL.GetLongTermForcastAccuracyDataBAL(forecastBE, out lstForecastData, out RecordCount);
        pager2.ItemCount = RecordCount;
        if (RecordCount % 200 == 0)
            PageNumber = RecordCount / 200;
        else
            PageNumber = (RecordCount / 200) + 1;

        if (PageNumber == page)
            gdvLongTerm.ShowFooter = true;
        ViewState["lstForecastLongTermData"] = lstForecastData;
        gdvLongTerm.DataSource = lstForecastData;
        gdvLongTerm.DataBind();

    }
    protected void BindLongTermForExport()
    {
        STK_ForecastAccuracyBE forecastBE = new STK_ForecastAccuracyBE();
        forecastBE.CountryIds = msCountry.SelectedCountryIDs;
        forecastBE.VendorIds = msVendor.SelectedVendorIDs;
        forecastBE.ItemCategoryIds = MultiSelectRMSCategory1.SelectedRMSCategoryIDs;
        forecastBE.ItemClassification = msItemClassification.selectedItemClassification;
        forecastBE.Action = "GetLongTermForecastAccuracyData";
        forecastBE.PageSize = 0;
        SKT_ForecastAccuracyBAL oMAS_ForecastAccuracyBAL = new SKT_ForecastAccuracyBAL();
        List<STK_ForecastAccuracyBE> lstForecastData = new List<STK_ForecastAccuracyBE>();
        int RecordCount;
        oMAS_ForecastAccuracyBAL.GetLongTermForcastAccuracyDataBAL(forecastBE, out lstForecastData, out RecordCount);
        ViewState["lstForecastLongTermExportData"] = lstForecastData;
        gdvExportLongTerm.DataSource = lstForecastData;
        gdvExportLongTerm.DataBind();

    }


    //public DataTable ToDataTable<T>( IList<T> data)
    //{
    //    PropertyDescriptorCollection properties =
    //        TypeDescriptor.GetProperties(typeof(T));
    //    DataTable table = new DataTable();
    //    foreach (PropertyDescriptor prop in properties)
    //        table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
    //    foreach (T item in data)
    //    {
    //        DataRow row = table.NewRow();
    //        foreach (PropertyDescriptor prop in properties)
    //            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
    //        table.Rows.Add(row);
    //    }
    //    return table;
    //}

    protected void btexport_Click(object sender, EventArgs e)
    {
        // ExportExcel();
        AssignMonthColom();
        GridView objGrid = new GridView();
        if (rdoShortTerm1.Checked)
        {
            BindShortTermForExport();
            //objGrid = gdvExportShortTerm;
        }
        else
        {
            BindLongTermForExport();
            objGrid = gdvExportLongTerm;
        }


        //Response.Clear();
        //Response.Buffer = true;
        //Response.AddHeader("content-disposition", "attachment;filename=ForecastAccuracyReport.xls");
        //Response.Charset = "";
        //Response.ContentType = "application/vnd.ms-excel";
        //using (StringWriter sw = new StringWriter())
        //{
        //    HtmlTextWriter hw = new HtmlTextWriter(sw);

        //    for (int i = 0; i < objGrid.Rows.Count; i++)
        //    {
        //        GridViewRow row = objGrid.Rows[i];
        //        row.Attributes.Add("class", "textmode");
        //    }
        //    objGrid.RenderControl(hw);

        //    //style to format numbers to string
        //    string style = @"<style> .textmode { mso-number-format:\@;} </style>";
        //    Response.Write(style);
        //    Response.Output.Write(sw.ToString());
        //    Response.Flush();
        //    Response.End();
        //}
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }

    protected void btBack_Click(object sender, EventArgs e)
    {
        // UcDataPanel.Visible = true;
        // Ucpnlgrid.Visible = false;
        Response.Redirect("SKT_ForecastAccuracyReport.aspx");
    }
    public void AssignMonthColom()
    {
        int countMonth = -1;
        int count = 1;
        while (count > 0 && count <= 12)
        {
            if (count == 1)
                Month1 = DateTime.Now.AddMonths(countMonth).ToString("MMM") + "-" + DateTime.Now.AddMonths(countMonth).ToString("yyyy");
            if (count == 2)
                Month2 = DateTime.Now.AddMonths(countMonth).ToString("MMM") + "-" + DateTime.Now.AddMonths(countMonth).ToString("yyyy");
            if (count == 3)
                Month3 = DateTime.Now.AddMonths(countMonth).ToString("MMM") + "-" + DateTime.Now.AddMonths(countMonth).ToString("yyyy");
            if (count == 4)
                Month4 = DateTime.Now.AddMonths(countMonth).ToString("MMM") + "-" + DateTime.Now.AddMonths(countMonth).ToString("yyyy");
            if (count == 5)
                Month5 = DateTime.Now.AddMonths(countMonth).ToString("MMM") + "-" + DateTime.Now.AddMonths(countMonth).ToString("yyyy");
            if (count == 6)
                Month6 = DateTime.Now.AddMonths(countMonth).ToString("MMM") + "-" + DateTime.Now.AddMonths(countMonth).ToString("yyyy");
            if (count == 7)
                Month7 = DateTime.Now.AddMonths(countMonth).ToString("MMM") + "-" + DateTime.Now.AddMonths(countMonth).ToString("yyyy");
            if (count == 8)
                Month8 = DateTime.Now.AddMonths(countMonth).ToString("MMM") + "-" + DateTime.Now.AddMonths(countMonth).ToString("yyyy");
            if (count == 9)
                Month9 = DateTime.Now.AddMonths(countMonth).ToString("MMM") + "-" + DateTime.Now.AddMonths(countMonth).ToString("yyyy");
            if (count == 10)
                Month10 = DateTime.Now.AddMonths(countMonth).ToString("MMM") + "-" + DateTime.Now.AddMonths(countMonth).ToString("yyyy");
            if (count == 11)
                Month11 = DateTime.Now.AddMonths(countMonth).ToString("MMM") + "-" + DateTime.Now.AddMonths(countMonth).ToString("yyyy");
            if (count == 12)
                Month12 = DateTime.Now.AddMonths(countMonth).ToString("MMM") + "-" + DateTime.Now.AddMonths(countMonth).ToString("yyyy");
            countMonth += -1;
            count++;

        }
    }
    protected void gdvAccuracyData_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (gdvAccuracyData.ShowFooter == true && e.Row.RowType == DataControlRowType.Footer)
        {
            List<STK_ForecastAccuracyBE> objBE = new List<STK_ForecastAccuracyBE>();
            objBE = (List<STK_ForecastAccuracyBE>)ViewState["lstForecastData"];
            foreach (var item in objBE)
            {
                Label lblYtd = (Label)e.Row.FindControl("lblYtd");
                lblYtd.Text = item.YTDAvg.ToString();
                Label lblJan = (Label)e.Row.FindControl("lblJan");
                lblJan.Text = item.JanAvg.ToString();
                Label lblFeb = (Label)e.Row.FindControl("lblFeb");
                lblFeb.Text = item.FebAvg.ToString();
                Label lblMar = (Label)e.Row.FindControl("lblMar");
                lblMar.Text = item.MayAvg.ToString();
                Label lblApr = (Label)e.Row.FindControl("lblApr");
                lblApr.Text = item.AprAvg.ToString();
                Label lblMay = (Label)e.Row.FindControl("lblMay");
                lblMay.Text = item.MayAvg.ToString();
                Label lblJun = (Label)e.Row.FindControl("lblJun");
                lblJun.Text = item.JunAvg.ToString();
                Label lblJul = (Label)e.Row.FindControl("lblJul");
                lblJul.Text = item.JulAvg.ToString();
                Label lblAug = (Label)e.Row.FindControl("lblAug");
                lblAug.Text = item.AugAvg.ToString();
                Label lblSep = (Label)e.Row.FindControl("lblSep");
                lblSep.Text = item.SepAvg.ToString();
                Label lblOct = (Label)e.Row.FindControl("lblOct");
                lblOct.Text = item.OctAvg.ToString();
                Label lblNov = (Label)e.Row.FindControl("lblNov");
                lblNov.Text = item.NovAvg.ToString();
                Label lblDec = (Label)e.Row.FindControl("lblDec");
                lblDec.Text = item.DecAvg.ToString();
                break;
            }


        }
    }
    protected void gdvExportShortTerm_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            List<STK_ForecastAccuracyBE> objBE = new List<STK_ForecastAccuracyBE>();
            objBE = (List<STK_ForecastAccuracyBE>)ViewState["lstForecastDataExport"];
            foreach (var item in objBE)
            {
                Label lblYtd = (Label)e.Row.FindControl("lblYtd");
                lblYtd.Text = item.YTDAvg.ToString();
                Label lblJan = (Label)e.Row.FindControl("lblJan");
                lblJan.Text = item.JanAvg.ToString();
                Label lblFeb = (Label)e.Row.FindControl("lblFeb");
                lblFeb.Text = item.FebAvg.ToString();
                Label lblMar = (Label)e.Row.FindControl("lblMar");
                lblMar.Text = item.MarAvg.ToString();
                Label lblApr = (Label)e.Row.FindControl("lblApr");
                lblApr.Text = item.AprAvg.ToString();
                Label lblMay = (Label)e.Row.FindControl("lblMay");
                lblMay.Text = item.MayAvg.ToString();
                Label lblJun = (Label)e.Row.FindControl("lblJun");
                lblJun.Text = item.JunAvg.ToString();
                Label lblJul = (Label)e.Row.FindControl("lblJul");
                lblJul.Text = item.JulAvg.ToString();
                Label lblAug = (Label)e.Row.FindControl("lblAug");
                lblAug.Text = item.AugAvg.ToString();
                Label lblSep = (Label)e.Row.FindControl("lblSep");
                lblSep.Text = item.SepAvg.ToString();
                Label lblOct = (Label)e.Row.FindControl("lblOct");
                lblOct.Text = item.OctAvg.ToString();
                Label lblNov = (Label)e.Row.FindControl("lblNov");
                lblNov.Text = item.NovAvg.ToString();
                Label lblDec = (Label)e.Row.FindControl("lblDec");
                lblDec.Text = item.DecAvg.ToString();
                break;
            }
        }
    }
    protected void gdvLongTerm_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (gdvLongTerm.ShowFooter == true && e.Row.RowType == DataControlRowType.Footer)
        {
            List<STK_ForecastAccuracyBE> objBE = new List<STK_ForecastAccuracyBE>();
            objBE = (List<STK_ForecastAccuracyBE>)ViewState["lstForecastLongTermData"];
            foreach (var item in objBE)
            {
                Label lblAvg = (Label)e.Row.FindControl("lblAvg");
                lblAvg.Text = item.HAvg.ToString();
                break;
            }

        }
    }
    protected void pager1_Command(object sender, CommandEventArgs e)
    {
        AssignMonthColom();
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BingShortTermData(currnetPageIndx);
    }
    protected void pager2_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BingLongTermData(currnetPageIndx);
    }
    protected void gdvExportLongTerm_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            List<STK_ForecastAccuracyBE> objBE = new List<STK_ForecastAccuracyBE>();
            objBE = (List<STK_ForecastAccuracyBE>)ViewState["lstForecastLongTermExportData"];
            foreach (var item in objBE)
            {
                Label lblAvg = (Label)e.Row.FindControl("lblAvg");
                lblAvg.Text = item.HAvg.ToString();
                break;
            }

        }
    }

}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.StockOverview.Stock_Turn;
using BusinessLogicLayer.ModuleBAL.StockOverview.StockTurn;

public partial class ModuleUI_StockOverview_Stock_Turn_StockTurnSettings :CommonPage
{
    private string IsValueMatchedStockTurnActuals = WebCommon.getGlobalResourceValue("IsValueMatchedStockTurnActuals");
    public string StockCompareCOGandInventory = WebCommon.getGlobalResourceValue("StockCompareCOGandInventory"); 
    public string StockODCOG= WebCommon.getGlobalResourceValue("StockODCOG");
    public string StockTurnCOG= WebCommon.getGlobalResourceValue("StockTurnCOG");
    public string StockAcutalsCOG= WebCommon.getGlobalResourceValue("StockAcutalsCOG");
    public string StockTurnInventory= WebCommon.getGlobalResourceValue("StockTurnInventory");
    public string StockActualsInventory= WebCommon.getGlobalResourceValue("StockActualsInventory");
    public string Weeks = WebCommon.getGlobalResourceValue("Weeks");
    StockTurnSettingBE oStockTurnSettingBE;
    StockTurnBAL oStockTurnBAL;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetSettings();
        }
    }

    protected void btnStockTurnSave_Click(object sender, EventArgs e)
    {
       // txtAverageInventory_3.Enabled = true;
        oStockTurnSettingBE = new StockTurnSettingBE();
        oStockTurnBAL = new StockTurnBAL();
        oStockTurnSettingBE.ODMeasure_COG =Convert.ToInt32(txtCogDuration_1.Text);
        oStockTurnSettingBE.StockTurnAverage_COG = Convert.ToInt32(txtCogDuration_2.Text);
        oStockTurnSettingBE.StockTurnAverage_INVENTORY = Convert.ToInt32(txtAverageInventory_2.Text);
        oStockTurnSettingBE.StockTurnActuals_COG = Convert.ToInt32(txtCogDuration_3.Text);
        oStockTurnSettingBE.StockTurnActuals_INVENTORY = Convert.ToInt32(txtCogDuration_3.Text);
        oStockTurnSettingBE.Action = "ADD";
        int? result =oStockTurnBAL.AddStockTurn(oStockTurnSettingBE);
        oStockTurnBAL = null;
        if (result > 0)
        {
            string saveMessage = WebCommon.getGlobalResourceValue("SavedMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
        GetSettings();

    }

    private void GetSettings()
    {
        oStockTurnSettingBE = new StockTurnSettingBE();
        oStockTurnBAL = new StockTurnBAL();
        oStockTurnSettingBE.Action = "VIEW";
        if (oStockTurnBAL.ViewStockTurn(oStockTurnSettingBE) != null)
        {
            txtCogDuration_1.Text = Convert.ToString(oStockTurnSettingBE.ODMeasure_COG);
            txtCogDuration_2.Text = Convert.ToString(oStockTurnSettingBE.StockTurnAverage_COG);
            txtAverageInventory_2.Text = Convert.ToString(oStockTurnSettingBE.StockTurnAverage_INVENTORY);
            txtCogDuration_3.Text = Convert.ToString(oStockTurnSettingBE.StockTurnActuals_COG);
            txtAverageInventory_3.Text = Convert.ToString(oStockTurnSettingBE.StockTurnActuals_INVENTORY);
            txtAverageInventory_3.Enabled = false;
        }
    }
}
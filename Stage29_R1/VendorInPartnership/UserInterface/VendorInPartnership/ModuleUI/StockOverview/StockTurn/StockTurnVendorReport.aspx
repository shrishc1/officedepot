﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StockTurnVendorReport.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="ModuleUI_StockOverview_StockTurn_StockTurnVendorReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .wmd-view-topscroll, .wmd-view {
            overflow-x: auto;
            overflow-y: hidden;
            width: 960px;
        }

        .wmd-view-topscroll {
            height: 16px;
        }

        .dynamic-div {
            display: inline-block;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".wmd-view-topscroll").scroll(function () {
                $(".wmd-view")
                    .scrollLeft($(".wmd-view-topscroll").scrollLeft());
            });

            $(".wmd-view").scroll(function () {
                $(".wmd-view-topscroll")
                    .scrollLeft($(".wmd-view").scrollLeft());
            });
            topscroll();
        });

        function topscroll() {
            $(".dynamic-div div").css("float", "left");
            var scrollwidth = $(".dynamic-div > div").width();
            $('.scroll-div').css('width', scrollwidth + "px");
        }
    </script>

    <h2>
        <cc1:ucLabel ID="lblStockTurnVendorReport" runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="formbox">
        <div id="divSearch" runat="server">
            <cc1:ucMultiView ID="mvSPStockTurnReport" runat="server" ActiveViewIndex="0">
                <cc1:ucView ID="vwVendorReport" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <cc1:ucPanel ID="UcDataPanel" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="5"
                            class="top-settings" width="95%">
                            <tr>
                                <td style="font-weight: bold; width: 10%">
                                    <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 1%">
                                    <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 89%">
                                    <cc1:MultiSelectCountry ID="msCountry" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                                </td>
                                <td>
                                    <cc1:MultiSelectVendor ID="msVendor" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </cc1:ucPanel>

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </cc1:ucView>
            </cc1:ucMultiView>
            <div class="button-row" style="width: 98%">
                <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
                    ValidationGroup="CheckVendor" OnClick="btnGenerateReport_Click" />
            </div>
        </div>

        <div id="divgrd" runat="server" visible="false">
            <div class="button-row">
                <cc1:ucButton ID="btnExportToExcel" runat="server" CssClass="exporttoexcel" OnClick="btnExport_Click"
                    Width="109px" Height="20px" />
            </div>
            <div>
                <cc1:ucGridView ID="UcGridViewDownload" Width="100%" runat="server" CssClass="grid"
                    OnRowDataBound="UcGridView2_RowDataBound" ShowFooter="false" Visible="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Vendor#">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblVendorNo" runat="server" Text='<%#Eval("VENDOR_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor Name">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblVendorName" runat="server" Text='<%#Eval("VENDOR_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor Country">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblVendorCountry" runat="server" Text='<%#Eval("VENDOR_COUNTRY") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="OD SKU">
                            <HeaderStyle Width="5%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblODsku" runat="server" Text='<%#Eval("OD_SKU_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Viking SKU">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblViking" runat="server" Text='<%#Eval("DIRECT_SKU") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <HeaderStyle Width="5%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("DESCRIPTION") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item Classification">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblItem" runat="server" Text='<%#Eval("ITEM_CLASSIFICATION") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("ITEM_CATEGORY") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Site">
                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblSite" runat="server" Text='<%#Eval("SITENAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="YTD">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblODMeasure" runat="server" Text='<%#Eval("OD_MEASURE") %>'></asp:Label>
                            </ItemTemplate>
                            <%-- <FooterTemplate>
                             <asp:Label ID="lblTotalOD" runat="server" Text="Total Amount"></asp:Label>
                            </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Jan">
                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblMonth1" runat="server" Text='<%#Eval("Month1_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Feb">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblMonth2" runat="server" Text='<%#Eval("Month2_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mar">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblMonth3" runat="server" Text='<%#Eval("Month3_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--   <asp:TemplateField HeaderText="Apr">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth4" runat="server" Text='<%#Eval("Month4_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="May">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth5" runat="server" Text='<%#Eval("Month5_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Jun">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth6" runat="server" Text='<%#Eval("Month6_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="Jul">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth7" runat="server" Text='<%#Eval("Month7_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="Aug">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth8" runat="server" Text='<%#Eval("Month8_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="Sep">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth9" runat="server" Text='<%#Eval("Month9_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="Oct">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth10" runat="server" Text='<%#Eval("Month10_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="Nov">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth11" runat="server" Text='<%#Eval("Month11_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                            <asp:TemplateField HeaderText="Dec">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth12" runat="server" Text='<%#Eval("Month12_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>       --%>
                    </Columns>
                </cc1:ucGridView>
            </div>
            <div class="wmd-view-topscroll">
                <div class="scroll-div">
                    &nbsp;
                </div>
            </div>
            <div class="wmd-view">
                <div class="dynamic-div">
                    <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid"
                        OnRowDataBound="UcGridView1_RowDataBound" ShowFooter="false">
                        <Columns>
                            <asp:TemplateField HeaderText="Vendor#">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblVendorNo" runat="server" Text='<%#Eval("VENDOR_NO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vendor Name">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblVendorName" runat="server" Text='<%#Eval("VENDOR_NAME") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vendor Country">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblVendorCountry" runat="server" Text='<%#Eval("VENDOR_COUNTRY") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="OD SKU">
                                <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblODsku" runat="server" Text='<%#Eval("OD_SKU_NO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Viking SKU">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblViking" runat="server" Text='<%#Eval("DIRECT_SKU") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description">
                                <HeaderStyle Width="5%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("DESCRIPTION") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Item Classification">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblItem" runat="server" Text='<%#Eval("ITEM_CLASSIFICATION") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category">
                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("ITEM_CATEGORY") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Site">
                                <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSite" runat="server" Text='<%#Eval("SITENAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="YTD">
                                <HeaderStyle Width="100%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblODMeasure" runat="server" Text='<%#Eval("OD_MEASURE") %>'></asp:Label>
                                </ItemTemplate>
                                <%-- <FooterTemplate>
                             <asp:Label ID="lblTotalOD" runat="server" Text="Total Amount"></asp:Label>
                            </FooterTemplate>--%>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblMonth1" runat="server" Text='<%#Eval("Month1_OD") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderStyle Width="100%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblMonth2" runat="server" Text='<%#Eval("Month2_OD") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderStyle Width="100%" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblMonth3" runat="server" Text='<%#Eval("Month3_OD") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <%--  <asp:TemplateField HeaderText="April">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth4" runat="server" Text='<%#Eval("Month4_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="May">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth5" runat="server" Text='<%#Eval("Month5_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="June">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth6" runat="server" Text='<%#Eval("Month6_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="July">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth7" runat="server" Text='<%#Eval("Month7_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="Aug">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth8" runat="server" Text='<%#Eval("Month8_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="Sep">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth9" runat="server" Text='<%#Eval("Month9_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="Oct">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth10" runat="server" Text='<%#Eval("Month10_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="Nov">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth11" runat="server" Text='<%#Eval("Month11_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                            <asp:TemplateField HeaderText="Dec">
                            <HeaderStyle Width="100%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblMonth12" runat="server" Text='<%#Eval("Month12_OD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>   --%>
                        </Columns>
                    </cc1:ucGridView>
                    <br />
                    <br />
                </div>
                <div class="button-row">
                    <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false"></cc1:PagerV2_8>
                </div>
                <div class="button-row">
                    <cc1:ucButton ID="btnBack" runat="server" CssClass="button" OnClick="btnBack_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
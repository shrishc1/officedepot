﻿using BusinessEntities.ModuleBE.StockOverview.Stock_Turn;
using BusinessLogicLayer.ModuleBAL.StockOverview.StockTurn;
using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;

public partial class ModuleUI_StockOverview_StockTurn_StockTurnVendorReport : CommonPage
{
    private bool IsExportClicked = false;
    public string Month1_Year = string.Empty;
    public string Month2_Year = string.Empty;
    public string Month3_Year = string.Empty;
    public string Month4_Year = string.Empty;
    public string Month5_Year = string.Empty;
    public string Month6_Year = string.Empty;
    public string Month7_Year = string.Empty;
    public string Month8_Year = string.Empty;
    public string Month9_Year = string.Empty;
    public string Month10_Year = string.Empty;
    public string Month11_Year = string.Empty;
    public string Month12_Year = string.Empty;
    private decimal? totalOD;
    private decimal? Month1_OD;
    private decimal? Month2_OD;
    private decimal? Month3_OD;
    private decimal? Month4_OD;
    private decimal? Month5_OD;
    private decimal? Month6_OD;
    private decimal? Month7_OD;
    private decimal? Month8_OD;
    private decimal? Month9_OD;
    private decimal? Month10_OD;
    private decimal? Month11_OD;
    private decimal? Month12_OD;
    private int RecordCount = 0;
    private DataSet dsVendorResult;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pager1.PageSize = 200;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
        }
        msVendor.FunctionCalled = "BindVendorByVendorId";
        msVendor.BindVendorByVendorIdOnPageLoad = true;

        //btnExportToExcel.GridViewControl = UcGridViewDownload;
        //btnExportToExcel.FileName = "Stock Turn Detailed Report (Vendor View)";
        //btnExportToExcel.Page = this;
    }

    public void BindGridview(int Page = 1)
    {
        try
        {
            dsVendorResult = new DataSet();
            string SelectedVendorIDs = string.Empty;
            string SelectedCountryIDs = string.Empty;
            StockTurnSettingBE oStockTurnSettingBE = new StockTurnSettingBE();
            StockTurnBAL oStockTurnBAL = new StockTurnBAL();
            oStockTurnSettingBE.Action = "VendorReportByPageWise";

            if (msVendor.SelectedVendorIDs != null)
            {
                oStockTurnSettingBE.vendorIDs = msVendor.SelectedVendorIDs;
            }
            else
            {
                ListBox lstLeft = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");
                foreach (ListItem item in lstLeft.Items)
                {
                    SelectedVendorIDs += item.Value + ",";
                }
                oStockTurnSettingBE.vendorIDs = SelectedVendorIDs.Trim(',');
            }

            if (msCountry.SelectedCountryIDs != null)
            {
                oStockTurnSettingBE.countryIDs = msCountry.SelectedCountryIDs;
            }
            else
            {
                ListBox lstLeft = (ListBox)msCountry.FindControl("lstLeft");
                foreach (ListItem item in lstLeft.Items)
                {
                    SelectedCountryIDs += item.Value + ",";
                }
                oStockTurnSettingBE.countryIDs = SelectedCountryIDs.Trim(',');
            }

            oStockTurnSettingBE.PageCount = Page;
            ViewState["countryIDs"] = oStockTurnSettingBE.countryIDs;
            ViewState["vendorIDs"] = oStockTurnSettingBE.vendorIDs;

            oStockTurnBAL.StockTurnVendorReport(oStockTurnSettingBE, out dsVendorResult, out RecordCount);

            if (dsVendorResult.Tables[0].Rows.Count > 0)
            {
                pager1.ItemCount = RecordCount;
                pager1.Visible = true;
                UcGridView1.DataSource = dsVendorResult.Tables[0];

                Month1_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month1_Year")).First();
                Month2_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month2_Year")).First();
                Month3_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month3_Year")).First();

                UcGridView1.DataBind();

                divgrd.Visible = true;
                divSearch.Visible = false;

                //Month4_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month4_Year")).First()!=null
                //              ?dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month4_Year")).First():"Apr";
                //Month5_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month5_Year")).First()!=null
                //              ?dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month5_Year")).First():"May";
                //Month6_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month6_Year")).First()!=null
                //              ?dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month6_Year")).First():"Jun";
                //Month7_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month7_Year")).First()!=null
                //              ?dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month7_Year")).First():"Jul";
                //Month8_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month8_Year")).First()!=null
                //              ?dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month8_Year")).First():"Aug";
                //Month9_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month9_Year")).First()!=null
                //              ?dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month9_Year")).First():"Sep";
                //Month10_Year =dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month10_Year")).First()!=null
                //              ?dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month10_Year")).First():"Oct";
                //Month11_Year =dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month11_Year")).First()!=null
                //              ?dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month11_Year")).First():"Nov";
                //Month12_Year =dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month12_Year")).First()!=null
                //              ?dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month12_Year")).First():"Dec";

                //if (Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(RecordCount) / 200)) == Page)
                //{
                //    totalOD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("OD_MEASURE_Total")).FirstOrDefault();
                //    Month1_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month1_OD_Total")).FirstOrDefault();
                //    Month2_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month2_OD_Total")).FirstOrDefault();
                //    Month3_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month3_OD_Total")).FirstOrDefault();
                //    Month4_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month4_OD_Total")).FirstOrDefault();
                //    Month5_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month5_OD_Total")).FirstOrDefault();
                //    Month6_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month6_OD_Total")).FirstOrDefault();
                //    Month7_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month7_OD_Total")).FirstOrDefault();
                //    Month8_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month8_OD_Total")).FirstOrDefault();
                //    Month9_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month9_OD_Total")).FirstOrDefault();
                //    Month10_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month10_OD_Total")).FirstOrDefault();
                //    Month11_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month11_OD_Total")).FirstOrDefault();
                //    Month12_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month12_OD_Total")).FirstOrDefault();

                //    UcGridView1.FooterRow.Cells[8].Text = "<b>Overall</b>";
                //    UcGridView1.FooterRow.Cells[9].Text = totalOD != null ? Convert.ToDecimal(totalOD).ToString("0.00") : Convert.ToString(totalOD);
                //    UcGridView1.FooterRow.Cells[10].Text = Month1_OD != null ? Convert.ToDecimal(Month1_OD).ToString("0.00") : Convert.ToString(Month1_OD);
                //    UcGridView1.FooterRow.Cells[11].Text = Month2_OD != null ? Convert.ToDecimal(Month2_OD).ToString("0.00") : Convert.ToString(Month2_OD);
                //    UcGridView1.FooterRow.Cells[12].Text = Month3_OD != null ? Convert.ToDecimal(Month3_OD).ToString("0.00") : Convert.ToString(Month3_OD);
                //    UcGridView1.FooterRow.Cells[13].Text = Month4_OD != null ? Convert.ToDecimal(Month4_OD).ToString("0.00") : Convert.ToString(Month4_OD);
                //    UcGridView1.FooterRow.Cells[14].Text = Month5_OD != null ? Convert.ToDecimal(Month5_OD).ToString("0.00") : Convert.ToString(Month5_OD);
                //    UcGridView1.FooterRow.Cells[15].Text = Month6_OD != null ? Convert.ToDecimal(Month6_OD).ToString("0.00") : Convert.ToString(Month6_OD);
                //    UcGridView1.FooterRow.Cells[16].Text = Month7_OD != null ? Convert.ToDecimal(Month7_OD).ToString("0.00") : Convert.ToString(Month7_OD);
                //    UcGridView1.FooterRow.Cells[17].Text = Month8_OD != null ? Convert.ToDecimal(Month8_OD).ToString("0.00") : Convert.ToString(Month8_OD);
                //    UcGridView1.FooterRow.Cells[18].Text = Month9_OD != null ? Convert.ToDecimal(Month9_OD).ToString("0.00") : Convert.ToString(Month9_OD);
                //    UcGridView1.FooterRow.Cells[19].Text = Month10_OD != null ? Convert.ToDecimal(Month10_OD).ToString("0.00") : Convert.ToString(Month10_OD);
                //    UcGridView1.FooterRow.Cells[20].Text = Month11_OD != null ? Convert.ToDecimal(Month11_OD).ToString("0.00") : Convert.ToString(Month11_OD);
                //    UcGridView1.FooterRow.Cells[21].Text = Month12_OD != null ? Convert.ToDecimal(Month12_OD).ToString("0.00") : Convert.ToString(Month12_OD);
                //}
            }
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    public void BindGridviewExcel()
    {
        try
        {
            if (IsExportClicked)
            {
                dsVendorResult = new DataSet();
                string SelectedVendorIDs = string.Empty;
                StockTurnSettingBE oStockTurnSettingBE = new StockTurnSettingBE();
                StockTurnBAL oStockTurnBAL = new StockTurnBAL();
                oStockTurnSettingBE.Action = "VendorReport";
                oStockTurnSettingBE.vendorIDs = Convert.ToString(ViewState["vendorIDs"]);
                oStockTurnSettingBE.countryIDs = Convert.ToString(ViewState["countryIDs"]);
                oStockTurnBAL.StockTurnVendorReport(oStockTurnSettingBE, out dsVendorResult, out RecordCount);
                UcGridViewDownload.DataSource = dsVendorResult.Tables[0];

                Month1_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month1_Year")).First();
                Month2_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month2_Year")).First();
                Month3_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month3_Year")).First();
                UcGridViewDownload.DataBind();

                //Month4_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month4_Year")).First() != null
                //              ? dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month4_Year")).First() : "Apr";
                //Month5_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month5_Year")).First() != null
                //              ? dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month5_Year")).First() : "May";
                //Month6_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month6_Year")).First() != null
                //              ? dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month6_Year")).First() : "Jun";
                //Month7_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month7_Year")).First() != null
                //              ? dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month7_Year")).First() : "Jul";
                //Month8_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month8_Year")).First() != null
                //              ? dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month8_Year")).First() : "Aug";
                //Month9_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month9_Year")).First() != null
                //              ? dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month9_Year")).First() : "Sep";
                //Month10_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month10_Year")).First() != null
                //              ? dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month10_Year")).First() : "Oct";
                //Month11_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month11_Year")).First() != null
                //              ? dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month11_Year")).First() : "Nov";
                //Month12_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month12_Year")).First() != null
                //              ? dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month12_Year")).First() : "Dec";
                //Month1_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month1_Year")).First();
                //Month2_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month2_Year")).First();
                //Month3_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month3_Year")).First();
                //Month4_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month4_Year")).First();
                //Month5_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month5_Year")).First();
                //Month6_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month6_Year")).First();
                //Month7_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month7_Year")).First();
                //Month8_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month8_Year")).First();
                //Month9_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month9_Year")).First();
                //Month10_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month10_Year")).First();
                //Month11_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month11_Year")).First();
                //Month12_Year = dsVendorResult.Tables[0].AsEnumerable().Select(x => x.Field<string>("Month12_Year")).First();

                //totalOD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("OD_MEASURE_Total")).FirstOrDefault();
                //Month1_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month1_OD_Total")).FirstOrDefault();
                //Month2_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month2_OD_Total")).FirstOrDefault();
                //Month3_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month3_OD_Total")).FirstOrDefault();
                //Month4_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month4_OD_Total")).FirstOrDefault();
                //Month5_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month5_OD_Total")).FirstOrDefault();
                //Month6_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month6_OD_Total")).FirstOrDefault();
                //Month7_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month7_OD_Total")).FirstOrDefault();
                //Month8_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month8_OD_Total")).FirstOrDefault();
                //Month9_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month9_OD_Total")).FirstOrDefault();
                //Month10_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month10_OD_Total")).FirstOrDefault();
                //Month11_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month11_OD_Total")).FirstOrDefault();
                //Month12_OD = dsVendorResult.Tables[0].AsEnumerable().Select(row => row.Field<decimal?>("Month12_OD_Total")).FirstOrDefault();

                //UcGridViewDownload.FooterRow.Cells[8].Text = "<b>Overall</b>";
                //UcGridViewDownload.FooterRow.Cells[9].Text = totalOD != null ? Convert.ToDecimal(totalOD).ToString("0.00") : Convert.ToString(totalOD);
                //UcGridViewDownload.FooterRow.Cells[10].Text = Month1_OD != null ? Convert.ToDecimal(Month1_OD).ToString("0.00") : Convert.ToString(Month1_OD);
                //UcGridViewDownload.FooterRow.Cells[11].Text = Month2_OD != null ? Convert.ToDecimal(Month2_OD).ToString("0.00") : Convert.ToString(Month2_OD);
                //UcGridViewDownload.FooterRow.Cells[12].Text = Month3_OD != null ? Convert.ToDecimal(Month3_OD).ToString("0.00") : Convert.ToString(Month3_OD);
                //UcGridViewDownload.FooterRow.Cells[13].Text = Month4_OD != null ? Convert.ToDecimal(Month4_OD).ToString("0.00") : Convert.ToString(Month4_OD);
                //UcGridViewDownload.FooterRow.Cells[14].Text = Month5_OD != null ? Convert.ToDecimal(Month5_OD).ToString("0.00") : Convert.ToString(Month5_OD);
                //UcGridViewDownload.FooterRow.Cells[15].Text = Month6_OD != null ? Convert.ToDecimal(Month6_OD).ToString("0.00") : Convert.ToString(Month6_OD);
                //UcGridViewDownload.FooterRow.Cells[16].Text = Month7_OD != null ? Convert.ToDecimal(Month7_OD).ToString("0.00") : Convert.ToString(Month7_OD);
                //UcGridViewDownload.FooterRow.Cells[17].Text = Month8_OD != null ? Convert.ToDecimal(Month8_OD).ToString("0.00") : Convert.ToString(Month8_OD);
                //UcGridViewDownload.FooterRow.Cells[18].Text = Month9_OD != null ? Convert.ToDecimal(Month9_OD).ToString("0.00") : Convert.ToString(Month9_OD);
                //UcGridViewDownload.FooterRow.Cells[19].Text = Month10_OD != null ? Convert.ToDecimal(Month10_OD).ToString("0.00") : Convert.ToString(Month10_OD);
                //UcGridViewDownload.FooterRow.Cells[20].Text = Month11_OD != null ? Convert.ToDecimal(Month11_OD).ToString("0.00") : Convert.ToString(Month11_OD);
                //UcGridViewDownload.FooterRow.Cells[21].Text = Month12_OD != null ? Convert.ToDecimal(Month12_OD).ToString("0.00") : Convert.ToString(Month12_OD);
            }

            btnExportToExcel.Visible = dsVendorResult.Tables[0].Rows.Count != 0;
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        string s = msVendor.SelectedVendorIDs;
        BindGridview();
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGridview(currnetPageIndx);
    }

    protected void UcGridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (1 == 1)
        {
            UcGridView1.Columns[10].HeaderText = Month1_Year;
            UcGridView1.Columns[11].HeaderText = Month2_Year;
            UcGridView1.Columns[12].HeaderText = Month3_Year;
            //UcGridView1.Columns[13].HeaderText = Month4_Year;
            //UcGridView1.Columns[14].HeaderText = Month5_Year;
            //UcGridView1.Columns[15].HeaderText = Month6_Year;
            //UcGridView1.Columns[16].HeaderText = Month7_Year;
            //UcGridView1.Columns[17].HeaderText = Month8_Year;
            //UcGridView1.Columns[18].HeaderText = Month9_Year;
            //UcGridView1.Columns[19].HeaderText = Month10_Year;
            //UcGridView1.Columns[20].HeaderText = Month11_Year;
            //UcGridView1.Columns[21].HeaderText = Month12_Year;
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        divgrd.Visible = false;
        divSearch.Visible = true;
    }

    protected void UcGridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (1 == 1)
        {
            UcGridViewDownload.Columns[10].HeaderText = Month1_Year;
            UcGridViewDownload.Columns[11].HeaderText = Month2_Year;
            UcGridViewDownload.Columns[12].HeaderText = Month3_Year;
            //UcGridViewDownload.Columns[13].HeaderText = Month4_Year;
            //UcGridViewDownload.Columns[14].HeaderText = Month5_Year;
            //UcGridViewDownload.Columns[15].HeaderText = Month6_Year;
            //UcGridViewDownload.Columns[16].HeaderText = Month7_Year;
            //UcGridViewDownload.Columns[17].HeaderText = Month8_Year;
            //UcGridViewDownload.Columns[18].HeaderText = Month9_Year;
            //UcGridViewDownload.Columns[19].HeaderText = Month10_Year;
            //UcGridViewDownload.Columns[20].HeaderText = Month11_Year;
            //UcGridViewDownload.Columns[21].HeaderText = Month12_Year;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        IsExportClicked = true;
        BindGridviewExcel();
        LocalizeGridHeader(UcGridViewDownload);

        if (UcGridViewDownload.Rows.Count > 0)
        {
            WebCommon.ExporttoExcel("Stock Turn Detailed Report (Vendor View)", UcGridViewDownload, false);
            // WebCommon.Export("Stock Turn Detailed Report (Vendor View)", UcGridViewDownload);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Security;
using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using Utilities;

public partial class ModuleUI_StockOverview_StockTurn_StockTurnReporting : CommonPage
{

    int? ReportRequest = 0;
    ReportRequestBE oReportRequestBE = new ReportRequestBE();
    ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();
    protected string Pleaseselectatleastonesite = WebCommon.getGlobalResourceValue("Pleaseselectatleastonesite");
    protected string Maximumfoursitesareallowed = WebCommon.getGlobalResourceValue("Maximumfoursitesareallowed");
    #region ReportVariables
    Warning[] warnings;
    string[] streamIds;
    string mimeType = string.Empty;
    string encoding = string.Empty;
    string extension = string.Empty;
    string reportFileName = string.Empty;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ucVendorTemplateSelect.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ////txtFromDate.Text = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("dd/MM/yyyy");
            ////txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //hdnJSFromDt.Value = txtFromDate.Text;
            //hdnJSToDt.Value = txtToDate.Text;
            int YearFrom = DateTime.Now.Year;
            int YearTo = DateTime.Now.Year - 10;
            while (YearFrom > YearTo)
            {
                if (YearFrom >= 2013)
                    ddlYear.Items.Add(YearFrom.ToString());
                YearFrom--;
            }
        }
    }

    /// <summary>
    /// Method to show Vendor on the basis of selected Template
    /// </summary>
    public override void TemplateSelectedIndexChanged()
    {
        base.TemplateSelectedIndexChanged();

        int VendorTemplateId = Convert.ToInt32(ucVendorTemplateSelect.innerControlddlTemplate.SelectedValue);

        ListBox lstSelectedVendor = (ListBox)msVendor.FindControl("lstRight");
        ListBox lstVendor = (ListBox)msVendor.FindControl("ucVendor").FindControl("lstLeft");

        if (VendorTemplateId > 0)
        {

            SCT_TemplateBAL oSCT_TemplateBAL = new SCT_TemplateBAL();
            SCT_TemplateBE oSCT_TemplateBE = new SCT_TemplateBE();

            oSCT_TemplateBE.VendorTemplateID = VendorTemplateId;
            oSCT_TemplateBE.Action = "GetVendorTemplateInfobyID";
            List<SCT_TemplateBE> lstVendorTemplate = oSCT_TemplateBAL.GetVendorTemplateById(oSCT_TemplateBE);

            if (lstVendorTemplate != null && lstVendorTemplate.Count > 0)
            {
                lstSelectedVendor.Items.Clear();
                msVendor.innerControlHiddenField.Value = string.Empty;
                msVendor.innerControlHiddenField2.Value = string.Empty;
                for (int iCount = 0; iCount < lstVendorTemplate.Count; iCount++)
                {
                    lstSelectedVendor.Items.Add(new ListItem(lstVendorTemplate[iCount].Vendor.Vendor_Name, lstVendorTemplate[iCount].Vendor.VendorID.ToString()));
                    msVendor.SelectedVendorIDs = lstVendorTemplate[iCount].Vendor.VendorID.ToString();
                    msVendor.SelectedVendorName = lstVendorTemplate[iCount].Vendor.Vendor_Name.ToString();
                }
            }
        }
        else
        {
            lstSelectedVendor.Items.Clear();
            msVendor.innerControlHiddenField.Value = string.Empty;
            msVendor.innerControlHiddenField2.Value = string.Empty;
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {

        oReportRequestBE.Action = "InsertReportRequest";
        oReportRequestBE.ModuleName = "Stock Turn";
       

        if (string.IsNullOrEmpty(msSite.SelectedSiteIDs) || string.IsNullOrWhiteSpace(msSite.SelectedSiteIDs))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Pleaseselectatleastonesite + "')", true);
            msVendor.setVendorsOnPostBack();
            return;
        }

        string[] SelectedSite = msSite.SelectedSiteIDs.Replace(",,,,", ",").Replace(",,,", ",").Replace(",,", ",").Split(',');

        if (SelectedSite.Length > 4)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + Maximumfoursitesareallowed + "')", true);
            msVendor.setVendorsOnPostBack();
            return;
        }

        if (rdoDetailViewOD.Checked == true)  // For Monthly
        {
            oReportRequestBE.ReportName = "Stock Turn Detailed Report (OD View)";
            oReportRequestBE.ReportType = "DetailViewOD";
            oReportRequestBE.ReportNamePath = "StockTurnODView.rdlc";
            oReportRequestBE.ReportDatatableName = "dtStockTurn";
           if (rdoAllStock.Checked)
            {
                oReportRequestBE.ReportAction = "ALL";
            }
            else if (rdoOnlyActive.Checked)
            {
                oReportRequestBE.ReportAction = "ACTIVE";
            }
            else if (rdoExcludeXXDC.Checked)
            {
                oReportRequestBE.ReportAction = "EXCLUDE XXDC";
            }
            else if (rdoExcludeDSIC.Checked)
            {
                oReportRequestBE.ReportAction = "EXCLUDE DISC";
            }
        }

        if (rdoSummaryView.Checked == true) // For Summary
        {
            oReportRequestBE.ReportName = "Stock Turn Summary Report";
            oReportRequestBE.ReportType = "Summary View";        
            oReportRequestBE.ReportNamePath = "StockTurnSummaryView.rdlc";
            oReportRequestBE.ReportDatatableName = "dtStockTurnSummary";
            if (rdoAllStock.Checked)
            {
                oReportRequestBE.ReportAction = "ALL";
            }
            else if (rdoOnlyActive.Checked)
            {
                oReportRequestBE.ReportAction = "ACTIVE";
            }
            else if (rdoExcludeXXDC.Checked)
            {
                oReportRequestBE.ReportAction = "EXCLUDE XXDC";
            }
            else if (rdoExcludeDSIC.Checked)
            {
                oReportRequestBE.ReportAction = "EXCLUDE DISC";
            }
        }
        if (rdoDetailedViewVendor.Checked == true) // For Detail
        {
            oReportRequestBE.ReportName = "Stock Turn Vendor Report";
            oReportRequestBE.ReportType = "Detailed_View_Vendor";
            oReportRequestBE.ReportNamePath = "StockTurnVendorView.rdlc";
            oReportRequestBE.ReportDatatableName = "dtStockTurnVendorView";
            if (rdoAllStock.Checked)
            {
                oReportRequestBE.ReportAction = "ALL";
            }
            else if (rdoOnlyActive.Checked)
            {
                oReportRequestBE.ReportAction = "ACTIVE";
            }
            else if (rdoExcludeXXDC.Checked)
            {
                oReportRequestBE.ReportAction = "EXCLUDE XXDC";
            }
            else if (rdoExcludeDSIC.Checked)
            {
                oReportRequestBE.ReportAction = "EXCLUDE DISC";
            }
        }

        oReportRequestBE.CountryIDs = msCountry.SelectedCountryIDs;
        oReportRequestBE.CountryName = msCountry.SelectedCountryName;
        oReportRequestBE.SiteIDs = msSite.SelectedSiteIDs;
        oReportRequestBE.SiteName = msSite.SelectedSiteName;
        oReportRequestBE.VendorIDs = msVendor.SelectedVendorIDs;
        oReportRequestBE.VendorName = msVendor.SelectedVendorName;   
        oReportRequestBE.StockPlannerIDs = msStockPlanner.SelectedStockPlannerIDs;
        oReportRequestBE.StockPlannerName = msStockPlanner.SelectedSPName;
        oReportRequestBE.SelectedRMSCategoryIDs = msCategory.SelectedRMSCategoryIDs;
        oReportRequestBE.SelectedRMSCategoryName = msCategory.SelectedRMSCategoryName;
        oReportRequestBE.ItemClassification = msItemClassification.selectedItemClassification;
        oReportRequestBE.OfficeDepotSKU = msSKU.SelectedSKUName;
        if (rdoDetailedViewVendor.Checked)
        {
            oReportRequestBE.DateFrom = string.IsNullOrEmpty(ddlYear.SelectedValue.ToString()) ? (DateTime?)null : Common.GetMM_DD_YYYY("01/01/" + ddlYear.SelectedValue.ToString());
            DateTime now = DateTime.Now;
            string sDateTo = DateTime.Now.AddDays(-now.Day).ToString("dd/MM/yyyy");
            oReportRequestBE.DateTo = Common.GetMM_DD_YYYY(sDateTo);
        }
        else
        {

            string sDateTo = DateTime.DaysInMonth(DateTime.Now.Year, Convert.ToInt32(ddlMonth.SelectedValue)).ToString() + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue;
            if (ddlMonth.SelectedValue == "01")
            {
                oReportRequestBE.DateFrom = string.IsNullOrEmpty(ddlYear.SelectedValue.ToString()) ? (DateTime?)null : Common.GetMM_DD_YYYY("01/" + "11" + "/" +Convert.ToString(Convert.ToInt32(ddlYear.SelectedValue)-1));
                oReportRequestBE.DateTo = Common.GetMM_DD_YYYY(sDateTo);
            }
            else if (ddlMonth.SelectedValue == "02")
            {
                oReportRequestBE.DateFrom = string.IsNullOrEmpty(ddlYear.SelectedValue.ToString()) ? (DateTime?)null : Common.GetMM_DD_YYYY("01/" + "12" + "/" + Convert.ToString(Convert.ToInt32(ddlYear.SelectedValue) - 1));
                oReportRequestBE.DateTo = Common.GetMM_DD_YYYY(sDateTo);
            }
            else
            {
                oReportRequestBE.DateFrom = string.IsNullOrEmpty(ddlYear.SelectedValue.ToString()) ? (DateTime?)null : Common.GetMM_DD_YYYY("01/" + Convert.ToString(Convert.ToInt32(ddlMonth.SelectedValue) - 2) + "/" + ddlYear.SelectedValue.ToString());
                oReportRequestBE.DateTo = Common.GetMM_DD_YYYY(sDateTo);
            }
        }

      

        //if (rdoAllStock.Checked)
        //{
        //    oReportRequestBE.Reportview = "ALL";
        //}
        //else if (rdoExcludeDSIC.Checked)
        //{
        //    oReportRequestBE.Reportview = "DSIC";
        //}
        //else if (rdoExcludeXXDC.Checked)
        //{
        //    oReportRequestBE.Reportview = "XXDC";
        //}
        //else if (rdoOnlyActive.Checked)
        //{
        //    oReportRequestBE.Reportview = "ACTIVE";
        //}

        oReportRequestBE.RequestStatus = "Pending";
        oReportRequestBE.UserID = Convert.ToInt32(Session["UserID"]);
        oReportRequestBE.RequestTime = DateTime.Now;
        msVendor.setVendorsOnPostBack();       
        ReportRequest = oReportRequestBAL.addReportRequestBAL(oReportRequestBE);
        if (ReportRequest > 0)
        {
            string ReportRequestSubmitted = WebCommon.getGlobalResourceValue("ReportRequestSubmitted");
            ReportRequestSubmitted = ReportRequestSubmitted + " : " + Convert.ToString(ReportRequest);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ReportRequestSubmitted + "')", true);
            return;
        }
       

    }
}


     
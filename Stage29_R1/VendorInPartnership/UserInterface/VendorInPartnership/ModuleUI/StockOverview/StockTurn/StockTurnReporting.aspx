﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StockTurnReporting.aspx.cs"  MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="ModuleUI_StockOverview_StockTurn_StockTurnReporting" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectCountry.ascx" TagName="MultiSelectCountry"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSite.ascx" TagName="MultiSelectSite"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectSKU.ascx" TagName="MultiSelectSKU"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectPO.ascx" TagName="MultiSelectPO"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectStockPlanner.ascx" TagName="MultiSelectStockPlanner"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectItemClassification.ascx" TagName="MultiSelectItemClassification"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucVendorSelectionTemplate.ascx" TagName="VendorSelectTemplate"
    TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectRMSCategory.ascx" TagName="MultiSelectCategory"
    TagPrefix="cc1" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblStockTurnReporting" runat="server"></cc1:ucLabel>
    </h2>
    <script type="text/javascript" language="javascript">
        function setValue1(target) {
            document.getElementById('<%=hdnJSFromDt.ClientID %>').value = target.value;
        }

        function setValue2(target) {
            document.getElementById('<%=hdnJSToDt.ClientID %>').value = target.value;
        }

        function checkFlag() {
            var datePanel = document.getElementById('UcPanelDate');
            var rdoDetailViewOD = document.getElementById('<%=rdoDetailViewOD.ClientID %>');
            var rdoSummaryView = document.getElementById('<%=rdoSummaryView.ClientID %>');
            var rdoDetailedViewVendor = document.getElementById('<%=rdoDetailedViewVendor.ClientID %>');
            //var rdoNewMonthlyOTIF = document.getElementById('=rdoNewMonthlyOTIF.ClientID ');
            var lblDateRange = document.getElementById('<%=lblDateFrom.ClientID %>');
            if (rdoDetailedViewVendor.checked) {
               // yearPanel.style.display = "block";
                datePanel.style.display = "none";
                lblDateRange.innerText = "Select Year";
            } else {
//                yearPanel.style.display = "none";
                datePanel.style.display = "block";
                lblDateRange.innerText = "Date From";
            }
        }
        $(document).ready(function () {
            HideShowReportView();            

            $("[id$='rdoDetail']").click(function () {
                HideShowReportView();
            });
            $("[id$='rdoSummary']").click(function () {
                HideShowReportView();
            });
            $("[id$='rdoMonthly']").click(function () {
                HideShowReportView();
            });
        });

        function HideShowReportView() {
            if ($("[id$='rdoDetail']").is(':checked')) {
                $('#tdReportView').hide();
            }
            if ($("[id$='rdoSummary']").is(':checked')) {
                $('#tdReportView').show();
            }
            if ($("[id$='rdoMonthly']").is(':checked')) {
                $('#tdReportView').show();
            }
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
    <asp:HiddenField ID="hdnJSToDt" runat="server" />
    <div class="right-shadow">
 <%--   <asp:UpdatePanel ID="pnlUP1" runat="server">
    <ContentTemplate>--%>
    <div class="formbox">
            <cc1:ucPanel ID="UcDataPanel" runat="server">            
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td align="right">
                            <cc1:VendorSelectTemplate runat="server" ID="ucVendorTemplateSelect" />
                        </td>
                    </tr>
                </table>
                <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                    <tr>
                        <td style="font-weight: bold; width: 10%">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%">
                            <cc1:ucLabel ID="UcLabel5" runat="server">:</cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 89%">
                            <cc1:MultiSelectCountry runat="server" ID="msCountry" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblSite" runat="server" Text="Site"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSite runat="server" ID="msSite" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblStockPlanner" runat="server" Text="Stock Planner"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectStockPlanner runat="server" ID="msStockPlanner" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectVendor runat="server" ID="msVendor" />
                        </td>
                    </tr>
                        <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblCategory" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel9" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectCategory runat="server" ID="msCategory" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblODSKUCode" runat="server" Text="OD SKU Code"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel8" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectSKU runat="server" ID="msSKU" />
                        </td>
                    </tr>
               
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblItemClassification" runat="server" Text="Item Classification"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="UcLabel10" runat="server">:</cc1:ucLabel>
                        </td>
                        <td>
                            <cc1:MultiSelectItemClassification runat="server" ID="msItemClassification" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr style="height: 3em;">
                                    <td width="10%" style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblReportType" runat="server" Text="Report Type"></cc1:ucLabel>
                                    </td>
                                    <td width="1%" style="font-weight: bold;">
                                        <cc1:ucLabel ID="UcLabel6" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td width="89%">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoDetailViewOD" runat="server"  GroupName="ReportType"
                                                        onClick="checkFlag()" />
                                                </td>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoSummaryView" runat="server"  GroupName="ReportType"
                                                        Checked="true" onClick="checkFlag()" />
                                                </td>
                                                <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoDetailedViewVendor" runat="server" GroupName="ReportType"
                                                        onClick="checkFlag()" />
                                                </td>
                                                <%--   <td style="font-weight: bold;" class="checkbox-list">
                                                    <cc1:ucRadioButton ID="rdoNewMonthlyOTIF" runat="server" Text="New Monthly OTIF"
                                                        GroupName="ReportType" onClick="checkFlag()" />
                                                </td>--%>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="tdReportView">
                                    <td colspan="3">
                                        <table width="100%">
                                            <tr style="height: 3em;">
                                                <td width="10%" style="font-weight: bold;">
                                                    <cc1:ucLabel ID="lblReportView" runat="server" Text="Report View"></cc1:ucLabel>
                                                </td>
                                                <td width="1%" style="font-weight: bold;">
                                                    <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                                                </td>
                                                <td width="89%">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="font-weight: bold;" class="checkbox-list">
                                                                <cc1:ucRadioButton ID="rdoAllStock" runat="server"  GroupName="ReportView" Checked="true" />
                                                            </td>
                                                            <td style="font-weight: bold;" class="checkbox-list">
                                                                <cc1:ucRadioButton ID="rdoOnlyActive" runat="server"  GroupName="ReportView" />
                                                            </td>
                                                            <td style="font-weight: bold;" class="checkbox-list">
                                                                <cc1:ucRadioButton ID="rdoExcludeXXDC" runat="server" GroupName="ReportView" />
                                                            </td>
                                                             <td style="font-weight: bold;" class="checkbox-list">
                                                                <cc1:ucRadioButton ID="rdoExcludeDSIC" runat="server" GroupName="ReportView" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="height: 3em;">
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblDateFrom" runat="server" Text="Date From"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                                    </td>
                                    <td>
                                <%--        <div id="UcPanelDate">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="font-weight: bold; width: 6em;">
                                                        <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                                                            ReadOnly="True" Width="70px" />
                                                    </td>
                                                    <td style="font-weight: bold; width: 4em; text-align: center">
                                                        <cc1:ucLabel ID="lblTo" runat="server" Text="To"></cc1:ucLabel>
                                                    </td>
                                                    <td style="font-weight: bold; width: 6em;">
                                                        <cc1:ucTextbox ID="txtToDate" ClientIDMode="Static" runat="server" onchange="setValue2(this)"
                                                            ReadOnly="True" Width="70px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>--%>
                                        <div>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                 <td id="UcPanelDate" colspan="2" style="font-weight: bold; width: 6em;">
                                                        <asp:DropDownList ID="ddlMonth" runat="server" Width="60px">
                                                        <asp:ListItem Text="Jan" Value="01"></asp:ListItem>
                                                          <asp:ListItem Text="Feb" Value="02"></asp:ListItem>
                                                          <asp:ListItem Text="Mar" Value="03"></asp:ListItem>
                                                          <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                                          <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                                          <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                                          <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                                          <asp:ListItem Text="Aug" Value="08"></asp:ListItem>
                                                          <asp:ListItem Text="Sep" Value="09"></asp:ListItem>
                                                          <asp:ListItem Text="Oct" Value="10"></asp:ListItem>
                                                          <asp:ListItem Text="Nov" Value="11"></asp:ListItem>
                                                          <asp:ListItem Text="Dec" Value="12"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td colspan="2" style="font-weight: bold; width: 6em;">
                                                        <cc1:ucDropdownList ID="ddlYear" runat="server" Width="60px">
                                                        </cc1:ucDropdownList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <script>
                                                checkFlag();
                                            </script>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
         
                    <tr>
                        <td align="right" colspan="3">
                            <div class="button-row">
                                <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button" OnClick="btnGenerateReport_Click"
                                 />
                            </div>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcReportPanel" runat="server" Visible="false">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="550px" Width="950px"
                                DocumentMapCollapsed="True" Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)"
                                WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
                            </rsweb:ReportViewer>
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
<%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StockTurnSettings.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" Inherits="ModuleUI_StockOverview_Stock_Turn_StockTurnSettings" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblStockTurnSettings" runat="server" Text="Stock Turn Settings"></cc1:ucLabel>
        <script type="text/javascript">
                
            function RequiredTextValidation() {
                //debugger;
                var txtCogDuration_1 = document.getElementById('txtCogDuration_1').value;
                var txtCogDuration_2 = document.getElementById('txtCogDuration_2').value;
                var txtCogDuration_3 = document.getElementById('txtCogDuration_3').value;
                var txtAverageInventory_2 = document.getElementById('txtAverageInventory_2').value;
                var txtAverageInventory_3 = document.getElementById('txtAverageInventory_3').value;

                var Error = false;
                var ErrorHTml = "";
                if (txtCogDuration_1 == "") { ErrorHTml += '<%= StockODCOG%>' + "\n"; Error = true; }
                if (txtCogDuration_2 == "") { ErrorHTml += '<%= StockTurnCOG%>' + "\n"; Error = true; }
                if (txtAverageInventory_2 == "") { ErrorHTml += '<%= StockTurnInventory%>' + "\n"; Error = true; }
                if (txtCogDuration_3 == "") { ErrorHTml += '<%= StockAcutalsCOG%>' + "\n"; Error = true; }
                if (txtAverageInventory_3 == "") { ErrorHTml += '<%= StockActualsInventory%>' + "\n"; Error = true; }

//                if (txtAverageInventory_3 != "") {
//                    if (txtCogDuration_3 != txtAverageInventory_3) {
//                        var result = confirm('<%= StockCompareCOGandInventory%>');
//                        if (result == false)
//                        {return false;}                    
//                    }
//                }

                if (Error == true) {
                    alert(ErrorHTml);
                    return false;                
                   
                }
                else { return true; }
            }
            
            function Copytext() {
                var txtCogDuration_3 = document.getElementById('txtCogDuration_3').value;
                var txtAverageInventory_3 = document.getElementById('txtAverageInventory_3').value;
                txtAverageInventory_3 = "";
                document.getElementById('txtAverageInventory_3').value = txtCogDuration_3;
            }
       
        </script>
    </h2>
    <asp:ScriptManager ID="sn" runat="server">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <asp:UpdatePanel ID="pnlUP1" runat="server">
                <ContentTemplate>
                    <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                        <tr>
                            <td>
                                <cc1:ucPanel ID="pnlODMeasure" runat="server" GroupingText="OD Measure" CssClass="fieldset-form">
                                    <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                                        <tr>
                                            <td style="font-weight: bold; width: 30%" colspan="3">
                                                <cc1:ucLabel ID="lblODMeasureHeading_1" runat="server"></cc1:ucLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; width:122px;" >
                                                <cc1:ucLabel ID="lblCogDuration_1" runat="server"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5px">
                                                <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucTextbox ID="txtCogDuration_1" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);"
                                                    runat="server" Width="40px" MaxLength="2" ClientIDMode="Static"></cc1:ucTextbox> &nbsp; <%=Weeks%>
                                            </td>
                                        </tr>
                                        <%--        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lbCurrentNetInventory_1" runat="server" Text="Current Net Inventory "></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5px">
                                                <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucTextbox ID="txtCurrentNetInventory_1" onkeyup="AllowNumbersOnly(this);"  onkeypress="return IsNumberKey(event, this);"
                                                    runat="server" Width="40px" MaxLength="2"></cc1:ucTextbox>
                                            </td>
                                        </tr>--%>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucPanel ID="UcPanel1" runat="server" GroupingText="Stock Turn Average" CssClass="fieldset-form">
                                    <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                                        <tr>
                                            <td style="font-weight: bold; width: 30%" colspan="3">
                                                <cc1:ucLabel ID="lblStockTurnAverageHeading_2" runat="server"></cc1:ucLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblCogDuration_2" runat="server"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5px">
                                                <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucTextbox ID="txtCogDuration_2" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this); "
                                                    runat="server" Width="40px" MaxLength="2" ClientIDMode="Static"></cc1:ucTextbox> &nbsp; <%=Weeks%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblAverageInventory_2" runat="server"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5px">
                                                <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucTextbox ID="txtAverageInventory_2" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);"
                                                    runat="server" Width="40px" MaxLength="2" ClientIDMode="Static"></cc1:ucTextbox> &nbsp; <%=Weeks%>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:ucPanel ID="UcPanel2" runat="server" GroupingText="Stock Turn Actuals" CssClass="fieldset-form">
                                    <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                                        <tr>
                                            <td style="font-weight: bold; width: 30%" colspan="3">
                                                <cc1:ucLabel ID="lblStockTurnActualsHeading_3" runat="server"></cc1:ucLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblCogDurationActuals_3" runat="server"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5px">
                                                <cc1:ucLabel ID="UcLabel7" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucTextbox ID="txtCogDuration_3" onkeyup="AllowNumbersOnly(this); Copytext();" onkeypress=" return IsNumberKey(event, this); "
                                                    runat="server" Width="40px" MaxLength="2" ClientIDMode="Static"></cc1:ucTextbox> &nbsp; <%=Weeks%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">
                                                <cc1:ucLabel ID="lblAverageInventory_3" runat="server"></cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold; width: 5px">
                                                <cc1:ucLabel ID="UcLabel9" runat="server">:</cc1:ucLabel>
                                            </td>
                                            <td style="font-weight: bold;">
                                                <cc1:ucTextbox ID="txtAverageInventory_3" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);"
                                                    runat="server" Width="40px" MaxLength="2"  ClientIDMode="Static"></cc1:ucTextbox> &nbsp;&nbsp; <%=Weeks%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="bottom-shadow">
</div>
<div class="button-row">
    <cc1:ucButton ID="btnStockTurnSave" runat="server" Text="Save" CssClass="button"
        OnClientClick="return RequiredTextValidation();" OnClick="btnStockTurnSave_Click" />
</div>
</asp:Content>


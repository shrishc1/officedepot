﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="SKUGrouping.aspx.cs" Inherits="ModuleUI_StockOverview_SKUGroupingAddGroup" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/ucCountry.ascx" TagName="ucCountry" TagPrefix="uc1" %>
<%@ Register TagName="ucAddButton" TagPrefix="uc" Src="~/CommonUI/UserControls/ucAddButton.ascx" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
         function SKUAlreadyError() {
             alert('<%=SKUAlreadyError %>');
         }
         function RemoveMessage() {
             alert('<%=SkuGroupingRemoveData %>');
         }
        
         function SaveMessage() {
             alert('<%=SavedMesg %>');
         }
         function SKUGroupError() {
             alert('<%=ErrorGroupNameUnique %>');
         }
         function SelectError() {
             alert('<%=CheckboxSelect %>');
         }
         function SKUError() {
             alert('<%=SkuDetails %>');
         }
         $(document).ready(function () {
             $("input[type='checkbox'][name$='chkSelectAllText']").click(function () {

                 var table = $(this).parent().parent().parent().parent().parent();
                 if ($(this).is(":checked")) {
                     $.each(table.find("input[type='checkbox']:gt(0)"), function (index, element) {
                         if ($(this).attr("disabled") == false) {
                             $(this).attr("checked", "checked");
                         }
                     });
                 }
                 else {
                     table.find("input[type='checkbox']:gt(0)").removeAttr('checked');
                 }
             });

             $("[id*=chkSelect]").live("click", function () {
                 var grid = $(this).closest("table");
                 var chkHeader = $("[id*=chkSelectAllText]", grid);
                 if (!$(this).is(":checked")) {
                     $("td", $(this).closest("tr")).removeClass("selected");
                     chkHeader.removeAttr("checked");
                 } else {
                     $("td", $(this).closest("tr")).addClass("selected");
                     if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                         chkHeader.attr("checked", "checked");
                     }
                 }
             });

             if ($('#<%=hdnStatus.ClientID %>').val().length <= 0) {
                 //debugger;
                 if ($('#<%=hdnAddRemoveStatus.ClientID %>').val().length <= 0) {
                     $('#<%= tblGrid.ClientID%>').show();
                     $('#<%= tblAddGroup.ClientID%>').hide();
                     $('#<%= tblAddRemove.ClientID%>').hide();
                 }
                 else if ($('#<%=hdnAddRemoveStatus.ClientID %>').val() == 'AddRemoveGroup') {
                     $('#<%= tblGrid.ClientID%>').hide();
                     $('#<%= tblAddGroup.ClientID%>').show();
                     $('#<%= tblAddRemove.ClientID%>').hide();
                 }
                 else {
                     $('#<%= tblGrid.ClientID%>').hide();
                     $('#<%= tblAddGroup.ClientID%>').hide();
                     $('#<%= tblAddRemove.ClientID%>').show();
                 }
             }

             $('#<%=btnBack.ClientID %>').click(function () {
                 //                 $('#<%= tblGrid.ClientID%>').show();
                 //                 $('#<%= tblAddGroup.ClientID%>').hide();
                 //                 $('#<%= tblAddRemove.ClientID%>').hide();
                 //                 $('#<%= UcGridView1.ClientID%>').css("width", "100%");
             });
             $('#<%=btnAdd.ClientID %>').click(function () {
                 $('#<%= tblGrid.ClientID%>').hide();
                 $('#<%= tblAddGroup.ClientID%>').show();
                 $('#<%= tblAddRemove.ClientID%>').hide();
                 $('#<%= divSkuDetailsButton.ClientID%>').hide();
                 $('#<%= lblSKUGrouping.ClientID%>').html('<%=SKUgroupingADD%>');
                 //                 $('#ctl00$ContentPlaceHolder1$ucCountry$ddlCountry').val("UKandIRE");
                 $('#<%= txtGroupName.ClientID%>').val("");
                 $('#<%= hdnIsGroupUpdated.ClientID%>').val("false");
                 $('#<%= btnRemove_1.ClientID%>').hide();
                 $('#<%= lblMesg.ClientID%>').html("");
                 return false;
             });
             $('#<%=btnSave.ClientID %>').click(function () {
                 if ($('#<%=txtGroupName.ClientID %>').length > 500) {
                     alert('<%= ErrorMaxlength500 %>');
                     return false;
                 }
                 else if ($('#<%=txtGroupName.ClientID %>').val() == '') {
                     alert('<%= ErrorGroupName %>');
                     return false;
                 }
                 else {
                     return true;
                 }
                 $('#<%= tblGrid.ClientID%>').hide();
                 $('#<%= tblAddGroup.ClientID%>').show();
                 $('#<%= tblAddRemove.ClientID%>').hide();
                 $('#<%= divSkuDetailsButton.ClientID%>').hide();
             });
             $('#<%= btnSearch.ClientID %>').click(function () {
                 if ($('#<%= hdnAddRemoveStatus.ClientID%>').val() == "Add") {
                     if ($('#<%=txtSKU.ClientID %>').val() == '' && $('#<%=txtCatCodeContains.ClientID %>').val() == '' && $('#<%=txtDesc.ClientID %>').val() == '') {
                         alert('<%= ErrorItemSearch %>');
                         return false;
                     }
                 }

                 $('#<%= tblGrid.ClientID%>').hide();
                 $('#<%= tblAddGroup.ClientID%>').hide();
                 $('#<%= tblAddRemove.ClientID%>').show();
             });

             //             $('#<%= UcGridView1.ClientID%>').find('a[id$="lkGroupName"]').bind('click', function (e) {
             //                 debugger;
             //                 var length = "ctl00_ContentPlaceHolder1_UcGridView1_ctl".length;
             //                 var res = this.id.substring(length, length + 2);
             //                 iRowIndex = res.split("_")[0];
             //                 var lkGroupName = document.getElementById("ctl00_ContentPlaceHolder1_UcGridView1_ctl" + iRowIndex + "_lkGroupName").innerText;
             //                 var lblCountrye = document.getElementById("ctl00_ContentPlaceHolder1_UcGridView1_ctl" + iRowIndex + "_lblCountrye").innerText;
             //                 var hdnGroupID = $("#ctl00_ContentPlaceHolder1_UcGridView1_ctl" + iRowIndex + "_hdnGroupID").val();
             //                 $('#<%= hdnSKugroupingID.ClientID%>').val(hdnGroupID);
             //                 $('#<%= txtGroupName.ClientID%>').val(lkGroupName);
             //                 $('#<%= tblGrid.ClientID%>').hide();
             //                 $('#<%= tblAddGroup.ClientID%>').show();
             //                 $('#<%= tblAddRemove.ClientID%>').hide();
             //                 $('#<%= divSkuDetailsButton.ClientID%>').hide();
             //                 $('#<%= lblSKUGrouping.ClientID%>').html('<%=SKUgroupingADD%>');
             //                 $('#<%= hdnIsGroupUpdated.ClientID%>').val("true");
             //                 $('#<%= btnRemove_1.ClientID%>').show();
             //                 $('#ctl00$ContentPlaceHolder1$ucCountry$ddlCountry').val(lblCountrye);
             //                 return false;
             //             });
             $('#<%=btnRemove.ClientID %>').click(function () {
                 if (confirm("Do you want to remove items") == true) {
                    return true;
                } else {
                    return false;
                 }
            });

            $('#<%=btnRemove_1.ClientID %>').click(function () {
                if (confirm("Do you want to remove group") == true) {
                    return true;
                } else {
                    return false;
                }
            });
             $('#<%= UcGridView1.ClientID%>').find('a[id$="lnkClick"]').bind('click', function (e) {
                 $('#<%= btnADD_1.ClientID%>').hide();
                 $('#<%= btnRemove.ClientID%>').hide();
                 $('#<%= tblGrid.ClientID%>').hide();
                 $('#<%= tblAddGroup.ClientID%>').hide();
                 $('#<%= tblAddRemove.ClientID%>').show();
                 $('#<%= lblSKUGrouping.ClientID%>').html('<%=SkuGroupingsAdd%>');
                 var length = "ctl00_ContentPlaceHolder1_UcGridView1_ctl".length;
                 var res = this.id.substring(length, length + 2);
                 iRowIndex = res.split("_")[0];
                 var lkGroupName = document.getElementById("ctl00_ContentPlaceHolder1_UcGridView1_ctl" + iRowIndex + "_lkGroupName").innerText;
                 var lblCountrye = document.getElementById("ctl00_ContentPlaceHolder1_UcGridView1_ctl" + iRowIndex + "_lblCountrye").innerText;
                 var hdnGroupID = $("#ctl00_ContentPlaceHolder1_UcGridView1_ctl" + iRowIndex + "_hdnGroupID").val();
                 $('#<%= txtGroupnameAddRemove.ClientID%>').val(lkGroupName);
                 $('#<%= txtCountry.ClientID%>').val(lblCountrye);
                 $('#<%= hdnSKugroupingID.ClientID%>').val(hdnGroupID);
                 $('#<%= hdnGroupNmae.ClientID%>').val(lkGroupName);
                 $('#<%= hdnCountry.ClientID%>').val(lblCountrye);
                 $('#<%= hdnAddRemoveStatus.ClientID%>').val("Add");
                 $('#<%= divSkuDetailsButton.ClientID%>').hide();
                 $("[id*=<%= UcGridView_AddRemove.ClientID%>] tr:not(:first-child)").html("");
                 $("[id*=<%= UcGridView_AddRemove.ClientID%>]").html("");
                 return false;
             });

             //             $('#<%= UcGridView1.ClientID%>').find('a[id$="lnkItems"]').bind('click', function (e) {                
             //                 $('#<%= lblSKUGrouping.ClientID%>').html('<%=SkuGroupingRemove%>');
             //                 var length = "ctl00_ContentPlaceHolder1_UcGridView1_ctl".length;
             //                 var res = this.id.substring(length, length + 2);
             //                 iRowIndex = res.split("_")[0];
             //                 var lkGroupName = document.getElementById("ctl00_ContentPlaceHolder1_UcGridView1_ctl" + iRowIndex + "_lkGroupName").innerText;
             //                 var lblCountrye = document.getElementById("ctl00_ContentPlaceHolder1_UcGridView1_ctl" + iRowIndex + "_lblCountrye").innerText;
             //                 var hdnGroupID = $("#ctl00_ContentPlaceHolder1_UcGridView1_ctl" + iRowIndex + "_hdnGroupID").val();

             //                 $('#<%= txtGroupnameAddRemove.ClientID%>').val(lkGroupName);
             //                 $('#<%= txtCountry.ClientID%>').val(lblCountrye);
             //                 $('#<%= hdnGroupNmae.ClientID%>').val(lkGroupName);
             //                 $('#<%= hdnCountry.ClientID%>').val(lblCountrye);
             //                 $('#<%= hdnAddRemoveStatus.ClientID%>').val("Remove");
             //                 $('#<%= divSkuDetailsButton.ClientID%>').hide();
             //                 $('#<%= hdnSKugroupingID.ClientID%>').val(hdnGroupID);

             ////                 $.ajax({
             ////                     type: "POST",
             ////                     url: "SKUGrouping.aspx/GetRemoveskuByGroup",
             ////                     data: '{skugroupid:"' + hdnGroupID + '"}',
             ////                     contentType: "application/json; charset=utf-8",
             ////                     dataType: "json",
             ////                     async:false,
             ////                     success: OnSuccess,
             ////                     failure: function (response) {
             ////                         alert(response.d);
             ////                     },
             ////                     error: function (response) {
             ////                         alert(response.d);
             ////                     }
             ////                 });
             ////                 return false;
             //             });

         })

         function OnSuccess(response) {           
             var json = $.parseJSON(response.d);
             var jsonData = $(json);


             var row = $("[id*=<%= UcGridView_AddRemove.ClientID%>] tr:last-child").clone(true);           
             $("[id*=<%= UcGridView_AddRemove.ClientID%>] tr").not($("[id*=<%= UcGridView_AddRemove.ClientID%>] tr:first-child")).remove();
//             for (var i = 0; i < jsonData.length; i++) {
//             $("td", row).eq(1).html(this['SKU']);
//                 $("td", i).eq(2).html(this['CatCode']);
//                 $("td", i).eq(3).html(this['VendorCode']);
//                 $("td", i).eq(4).html(this['DESCRIPTION']);
//                 $("[id*=<%= UcGridView_AddRemove.ClientID%>]").append();
             //             }

             $.each(jsonData, function () {                
                 $("td", row).eq(1).html(this['SKU']);
                 $("td", row).eq(2).html(this['CatCode']);
                 $("td", row).eq(3).html(this['VendorCode']);
                 $("td", row).eq(4).html(this['DESCRIPTION']);
                 $("[id*=<%= UcGridView_AddRemove.ClientID%>]").append(row);
                 row = $("[id*=<%= UcGridView_AddRemove.ClientID%>] tr:last-child").clone(true);
             });
         }
    </script>
    <asp:HiddenField ID="hdnStatus" runat="server" />
    <asp:HiddenField ID="hdnAddRemoveStatus" runat="server" />
    <asp:HiddenField ID="hdnGroupNmae" runat="server" />
    <asp:HiddenField ID="hdnCountry" runat="server" />
    <asp:HiddenField ID="hdnSKugroupingID" runat="server" />
    <asp:HiddenField ID="hdnIsGroupUpdated" runat="server" />
    <div class="right-shadow">
        <div class="formbox">
            <h2>
                <asp:Label ID="lblSKUGrouping" runat="server"></asp:Label>
            </h2>
            <div class="button-row" id="divSkuDetailsButton" runat="server">
                <cc1:ucButton ID="btnAdd" runat="server" Text="Add" CssClass="button add" />
                <uc1:ucExportButton ID="ucExportToExcel1" runat="server" />
            </div>
            <cc1:ucLabel ID="lblMesg" runat="server"></cc1:ucLabel>
            <br />
            <br />
            <div class="formbox">
                <table width="100%" id="tblGrid" runat="server">
                    <tr>
                        <td align="center" colspan="8">
                            <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" OnRowDataBound="UcGridView1_RowDataBound"
                                OnRowCommand="UcGridView1_RowCommand">
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="GroupName">
                                        <HeaderStyle Width="90px" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnGroupID" runat="server" Value='<%#Eval("SkuGroupingId") %>' />
                                            <cc1:ucLinkButton runat="server" ID="lkGroupName" Text='<%#Eval("GroupName") %>' CommandName="Group" CommandArgument='<%#Eval("SkuGroupingId") +","+ Eval("GroupName") +","+ Eval("Country")%>'></cc1:ucLinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Country">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblCountrye" Text='<%#Eval("Country") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="# Items">
                                        <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <%--<cc1:ucLinkButton runat="server" ID="lnkItems" CommandName="items" CommandArgument='<%#Eval("SkuGroupingId") +","+ Eval("GroupName") +","+ Eval("Country")%>'
                                                Text='<%#Eval("Items") %>'></cc1:ucLinkButton>--%>
                                            <cc1:ucLabel runat="server" ID="lablItem" Text='<%#Eval("Items") %>' ></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Add | Edit">
                                        <HeaderStyle Width="90px" HorizontalAlign="Center" />
                                        <ItemTemplate>                                        
                                            <cc1:ucLinkButton runat="server" ID="lnkClick" Text="Add"></cc1:ucLinkButton>&nbsp; | &nbsp;
                                            <cc1:ucLinkButton runat="server" ID="lnkItems" CommandName="items" CommandArgument='<%#Eval("SkuGroupingId") +","+ Eval("GroupName") +","+ Eval("Country")%>'
                                                Text="Edit"></cc1:ucLinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="90px" />
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                        </td>
                    </tr>
                    <%--    <tr>
            <td align="center">
              
                </td>
                </tr>--%>
                </table>
                <cc1:ucGridView ID="grdExport" Width="50%" runat="server" CssClass="grid" Visible="false">
                    <Columns>
                        <asp:TemplateField HeaderText="GroupName">
                            <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                            <ItemTemplate>
                                <cc1:ucLinkButton runat="server" ID="lblGroupName" Text='<%#Eval("GroupName") %>'></cc1:ucLinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Country">
                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                            <ItemTemplate>
                                <cc1:ucLabel runat="server" ID="lblCountry" Text='<%#Eval("Country") %>'></cc1:ucLabel>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="# Items">
                            <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                            <ItemTemplate>
                                <cc1:ucLinkButton runat="server" ID="lblItems" Text='<%#Eval("Items") %>'></cc1:ucLinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="left" Width="50px" Font-Size="10px" />
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
                <%--  <h2>
        <cc1:ucLabel ID="lblSKUgroupingADD" runat="server" ></cc1:ucLabel>
    </h2>--%>
                <table border="0" cellpadding="2" cellspacing="3" id="tblAddGroup" runat="server">
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblCountry" runat="server" isRequired="true"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="5%" align="center">
                            :
                        </td>
                        <td style="font-weight: bold;" width="57%">
                            <uc1:ucCountry ID="ucCountry" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <cc1:ucLabel ID="lblGroupName" runat="server" isRequired="true"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold;" width="5%" align="center">
                            :
                        </td>
                        <td style="font-weight: bold;" width="57%">
                            <cc1:ucTextbox ID="txtGroupName" runat="server" Height="24px" MaxLength="500"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td colspan="8">
                            <table border="0" cellpadding="2" cellspacing="3">
                                <tr>
                                    <td> <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
                                    </td>
                                    <td> <cc1:ucButton ID="btnRemove_1" runat="server" Style="display: none;" Text="Remove"
                                CssClass="button" OnClick="btnRemoveGroup_Click" />
                                    </td>
                                    <td>  <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                                    </td>
                                </tr>
                            </table>                           
                        </td>
                    </tr>
                </table>
                <%--     <h2>
        <cc1:ucLabel ID="lblAddGroup" runat="server" ></cc1:ucLabel>
        <cc1:ucLabel ID="lblRemoveGroup" runat="server" ></cc1:ucLabel>
    </h2>
                --%>
                <table border="0" width="100%" cellpadding="2" cellspacing="3" id="tblAddRemove"
                    runat="server">
                    <tr>
                        <td style="font-weight: bold; width: 15%;">
                            <cc1:ucLabel ID="lblGroup" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%;" align="center">
                            :
                        </td>
                        <td style="font-weight: bold; width: 34%;">
                            <cc1:ucTextbox ID="txtGroupnameAddRemove" Width="168px" runat="server" Enabled="false"></cc1:ucTextbox>
                        </td>
                        <td style="font-weight: bold; width: 15%;">
                            <cc1:ucLabel ID="lblCountry_1" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%;" align="center">
                            :
                        </td>
                        <td style="font-weight: bold; width: 34%;">
                            <cc1:ucTextbox ID="txtCountry" Width="168px" runat="server" Enabled="false"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; width: 15%;">
                            <cc1:ucLabel ID="lblSKU" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%;" align="center">
                            :
                        </td>
                        <td style="font-weight: bold; width: 34%;">
                            <cc1:ucTextbox ID="txtSKU" Width="165px" runat="server"></cc1:ucTextbox>
                        </td>
                        <td style="font-weight: bold; width: 15%;">
                            <cc1:ucLabel ID="lblCatCodeContains" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%;" align="center">
                            :
                        </td>
                        <td style="font-weight: bold; width: 34%;">
                            <cc1:ucTextbox ID="txtCatCodeContains" Style="width: 166px;" runat="server"></cc1:ucTextbox>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; width: 15%;">
                            <cc1:ucLabel ID="lblDESCRIPTION" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="font-weight: bold; width: 1%;" align="center">
                            :
                        </td>
                        <td style="font-weight: bold; width: 34%;">
                            <cc1:ucTextbox ID="txtDesc" Style="width: 166px;" runat="server"></cc1:ucTextbox>
                        </td>
                        <td colspan="3" >
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <cc1:ucButton ID="btnSearch" runat="server" Text="Search"  CssClass="button" OnClick="btnSearch_Click" />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <br />
                            <br />
                            <cc1:ucGridView ID="UcGridView_AddRemove" Width="100%" runat="server" CssClass="grid">
                                <EmptyDataTemplate>
                                    <div style="text-align: center">
                                        <cc1:ucLabel ID="lblNoData" runat="server" Text="No Records found."></cc1:ucLabel>
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="SelectAllCheckBox">
                                        <HeaderStyle HorizontalAlign="Left" Width="30px" />
                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAllText" runat="server" CssClass="checkbox-input" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ViewStateMode="Enabled" ID="chkSelect" />
                                            <%--   <cc1:ucLabel runat="server" ID="lblSKUID" Text='<%#Eval("SKUID") %>' Visible="false"></cc1:ucLabel>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--  <asp:TemplateField HeaderText="SiteName">
                                        <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                             <cc1:ucLabel runat="server" ID="lblSiteName" Text='<%#Eval("SiteName") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="SKU">
                                        <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblSKU" Text='<%#Eval("SKU") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Catcode">
                                        <HeaderStyle Width="50px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblCatcode" Text='<%#Eval("Catcode") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Code">
                                        <HeaderStyle Width="80px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblVendorCode" Text='<%#Eval("VendorCode") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="50px" Font-Size="10px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderStyle Width="90px" HorizontalAlign="Left" Font-Size="10px" />
                                        <ItemTemplate>
                                            <cc1:ucLabel runat="server" ID="lblDescription" Text='<%#Eval("DESCRIPTION") %>'></cc1:ucLabel>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="90px" Font-Size="10px" />
                                    </asp:TemplateField>
                                </Columns>
                            </cc1:ucGridView>
                            <div class="button-row">
                                <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false">
                                </cc1:PagerV2_8>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8" align="right">
                            <table cellpadding="2" cellspacing="3">
                                <tr>
                                    <td>
                                        <cc1:ucButton ID="btnBackAddRemove" runat="server" Text="Back" CssClass="button"
                                            OnClick="btnBack_Click" />
                                    </td>
                                    <td>
                                        <cc1:ucButton ID="btnADD_1" runat="server" Text="Add" CssClass="button" Style="display: none;"
                                            OnClick="btnADD_1_Click" />
                                    </td>
                                    <td>
                                        <cc1:ucButton ID="btnRemove" runat="server" Text="Remove" CssClass="button" Style="display: none;"
                                            OnClick="btnRemove_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(window).unload(function () {
            $('#<%= UcGridView1.ClientID%>').css("width", "100%");
            return false;
        });
    </script>
</asp:Content>

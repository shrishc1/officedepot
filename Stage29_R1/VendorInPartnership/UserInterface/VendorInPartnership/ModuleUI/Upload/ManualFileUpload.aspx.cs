﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;
using System.IO;
using System.Configuration;
using System.Data;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using Microsoft.Reporting.WebForms;


public partial class ManualFileUpload : CommonPage
{

    #region ReportVariables
    Warning[] warnings;
    string[] streamIds;
    string mimeType = string.Empty;
    string encoding = string.Empty;
    string extension = string.Empty;
    string reportFileName = string.Empty;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRunningStatus();
            BindmanualUploadGrid();
        }
        if (GetQueryStringValue("RecordInfo") != null && GetQueryStringValue("ManualFileUploadID") != null)
        {
            ShowErrorReport(GetQueryStringValue("RecordInfo").ToString(), GetQueryStringValue("ManualFileUploadID").ToString());
        }
    }

    private void CheckRunningStatus() {
        string DataImportRunningMessage = WebCommon.getGlobalResourceValue("DataImportRunningMessage");
        UP_DataImportSchedulerBE up_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        UP_DataImportSchedulerBAL oUP_DataImportSchedulerBAL = new UP_DataImportSchedulerBAL();

        // Check If DataImport is already running in Background using [UP_ImportProcessStatus] table.
        up_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        up_DataImportSchedulerBE.Action = "CheckStatusOnly";
        up_DataImportSchedulerBE.ProcessType = "M";
        string strStatus = oUP_DataImportSchedulerBAL.CheckUpdateImportStatusBAL(up_DataImportSchedulerBE);
        if (strStatus.Trim().ToUpper().Equals("AUT0")) {
            lblMessage.Text = "Auto Data Import Process is running in background, Please wait for few minutes....";
            UploadBtn.Visible = false;            
        }
        else if (strStatus.Trim().ToUpper().Equals("MAN")) {
            lblMessage.Text = "Manual Data Import Process is running in background, Please wait for few minutes....";
            UploadBtn.Visible = false;
        }
        else {
            lblMessage.Text = "No Import is running, you can import file.";
            UploadBtn.Visible = true;
        }
    }

    protected void btnCheck_Click(object sender, EventArgs e) {
        CheckRunningStatus();
    }

    private void ShowErrorReport(string RecordInfo, string ManualFileUploadID)
    {

        int ManualFileUploadId = Convert.ToInt32(GetQueryStringValue("ManualFileUploadID"));
        UP_DataImportSchedulerBE oUP_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        UP_DataImportSchedulerBAL oUP_PurchaseOrderDetailBAL = new UP_DataImportSchedulerBAL();
        DataSet dsErrorCount = new DataSet();
        string DataTableName = string.Empty;
        string ReportName = string.Empty;
        if (RecordInfo == "VEN")
        {

            oUP_DataImportSchedulerBE.Action = "GetVendorErrorDetail";
            oUP_DataImportSchedulerBE.ManualFileUploadID = ManualFileUploadId;
            dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
            DataTableName = "UP_ErrorVendor";
            ReportName = "\\ModuleUI\\Upload\\Report\\RptVendorErrorDetail.rdlc";
            reportFileName = "Vendor Error Detail";
        }
        else if (RecordInfo == "PO")
        {

            oUP_DataImportSchedulerBE.Action = "GetPOErrorDetail";
            oUP_DataImportSchedulerBE.ManualFileUploadID = ManualFileUploadId;
            dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
            DataTableName = "UP_ErrorPurchaseOrderDetails";
            ReportName = "\\ModuleUI\\Upload\\Report\\RptPOErrorDetail.rdlc";
            reportFileName = "Purchase Order Error Detail";
        }
        else if (RecordInfo == "RI")
        {

            oUP_DataImportSchedulerBE.Action = "GetReceiptErrorDetail";
            oUP_DataImportSchedulerBE.ManualFileUploadID = ManualFileUploadId;
            dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
            DataTableName = "UP_ErrorReceiptInformation";
            ReportName = "\\ModuleUI\\Upload\\Report\\RptRIErrorDetail.rdlc";
            reportFileName = "Receipt Information Error Detail";
        }
        else if (RecordInfo == "SKU")
        {

            oUP_DataImportSchedulerBE.Action = "GetSKUErrorDetail";
            oUP_DataImportSchedulerBE.ManualFileUploadID = ManualFileUploadId;
            dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
            DataTableName = "UP_ErrorSKU";
            ReportName = "\\ModuleUI\\Upload\\Report\\RptSKUErrorDetail.rdlc";
            reportFileName = "SKU Error Detail";
        }
        else if (RecordInfo == "EX") {

            oUP_DataImportSchedulerBE.Action = "GetExpediteErrorDetail";
            oUP_DataImportSchedulerBE.ManualFileUploadID = ManualFileUploadId;
            dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
            DataTableName = "DataSet1";
            ReportName = "\\ModuleUI\\Upload\\Report\\RptExpediteSKUErrorDetail.rdlc";
            reportFileName = "Expedite SKU Error Detail";
        }
        else if (RecordInfo == "BO")
        {

            oUP_DataImportSchedulerBE.Action = "GetBOErrorDetail";
            oUP_DataImportSchedulerBE.ManualFileUploadID = ManualFileUploadId;
            dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
            DataTableName = "DataSet1";
            ReportName = "\\ModuleUI\\Upload\\Report\\RptBOErrorDetail.rdlc";
            reportFileName = "BO Error Detail";
        }
        oUP_PurchaseOrderDetailBAL = null;
        ReportDataSource rdsErrorReport = new ReportDataSource(DataTableName, dsErrorCount.Tables[0]);
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~") + ReportName;

        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(rdsErrorReport);

        byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "attachment; filename = " + reportFileName + "." + extension);
        Response.BinaryWrite(bytes);
        Response.Flush();

    }

    private void BindmanualUploadGrid()
    {
        grdManualUpload.DataSource = null;
        UP_ManualFileUploadBE oUP_ManualFileUploadBE = new UP_ManualFileUploadBE();

        oUP_ManualFileUploadBE.Action = "GetManualUploadData";
        UP_ManualFileUploadBAL oUP_ManualFileUploadBAL = new UP_ManualFileUploadBAL();
        List<UP_ManualFileUploadBE> UP_ManualFileUploadBEList = new List<UP_ManualFileUploadBE>();
        UP_ManualFileUploadBEList = oUP_ManualFileUploadBAL.GetManualUploadBAL(oUP_ManualFileUploadBE);
        grdManualUpload.DataSource = UP_ManualFileUploadBEList;
        grdManualUpload.DataBind();
    }

   

    

    public delegate void MethodInvoker(string uploadedFilePath, string strUserID);
    
    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        string returnError = "";
        bool isSKUFile = false;
        UP_DataImportSchedulerBE up_DataImportSchedulerBE = null;
        string strStatus = string.Empty;
        try {
            string DataImportRunningMessage = WebCommon.getGlobalResourceValue("DataImportRunningMessage");
            UP_DataImportSchedulerBAL oUP_DataImportSchedulerBAL = new UP_DataImportSchedulerBAL();

            // Check If DataImport is already running in Background using [UP_ImportProcessStatus] table.
            up_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
            up_DataImportSchedulerBE.Action = "CheckImportStatus";
            up_DataImportSchedulerBE.ProcessType = "M";
            strStatus = oUP_DataImportSchedulerBAL.CheckUpdateImportStatusBAL(up_DataImportSchedulerBE);
            if (strStatus.Trim().ToUpper().Equals("TRUE")) {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DataImportRunningMessage + "')", true);
                return;
            }

            // Check If DataImport is already running in Background.            
            DataSet dsDataImportRunningStatus = oUP_DataImportSchedulerBAL.CheckIsDataImportRunningBAL();
            if (dsDataImportRunningStatus != null && dsDataImportRunningStatus.Tables[0].Rows.Count > 0) {
                if (dsDataImportRunningStatus.Tables[0].Rows[0]["IsDataImportRunning"].ToString() == "True")//data import is running in background 
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DataImportRunningMessage + "')", true);
                    return;
                }
            }
            //--------------------------

            
            string uploadedFilePath = ConfigurationManager.AppSettings["UploadFilePath"];

            if (!System.IO.Directory.Exists(uploadedFilePath))
                System.IO.Directory.CreateDirectory(uploadedFilePath);

            uploadedFilePath = uploadedFilePath + DateTime.Now.Year.ToString();
            if (DateTime.Now.Month.ToString().Length == 1)
                uploadedFilePath = uploadedFilePath + "0" + DateTime.Now.Month.ToString();
            else
                uploadedFilePath = uploadedFilePath + DateTime.Now.Month.ToString();

            if (DateTime.Now.Date.ToString().Length == 1)
                uploadedFilePath = uploadedFilePath + "0" + DateTime.Now.Day.ToString();
            else
                uploadedFilePath = uploadedFilePath + DateTime.Now.Day.ToString();

            uploadedFilePath = uploadedFilePath + "_" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            uploadedFilePath = uploadedFilePath + "_" + FileUpLoad1.FileName;


            string[] strArray = Path.GetFileNameWithoutExtension(FileUpLoad1.FileName).Split('_');
            if (strArray[strArray.Length - 2].Length < 8 || strArray[strArray.Length - 1].Length < 4) {
                
                string returnDateError = WebCommon.getGlobalResourceValue("InvalidDateFormat");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + returnDateError + "')", true);
                return;
            }

            if (!validationFunctions.IsValidDateFormat(strArray[strArray.Length - 2])) {
                
                string returnDateError = WebCommon.getGlobalResourceValue("InvalidDateFormat");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + returnDateError + "')", true);
                return;
            }

            if (ImportDB.CheckToBeUploadedFileDateIsGreater(Path.GetFileNameWithoutExtension(FileUpLoad1.FileName))) // Added By Hemant
            {
                lblMessage.Text = "Manual Data Import is running in background, Please wait for few minutes....";
                FileUpLoad1.SaveAs(uploadedFilePath);

                if (FileUpLoad1.HasFile) {
                    string ext = Path.GetExtension(FileUpLoad1.FileName);

                    if (ext != ".txt") {
                        string ValidFileExtension = WebCommon.getGlobalResourceValue("ValidFileExtension");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ValidFileExtension + "')", true);
                        return;
                    }
                    if (!((FileUpLoad1.FileName.ToLower().Contains("vendor")) || (FileUpLoad1.FileName.ToLower().Contains("item")) || (FileUpLoad1.FileName.ToLower().Contains("purchaseorder")) || (FileUpLoad1.FileName.ToLower().Contains("receipt")) || (FileUpLoad1.FileName.ToLower().Contains("forecastedsales")) || (FileUpLoad1.FileName.ToLower().Contains("filbov")))) {
                        string ValidFileName = WebCommon.getGlobalResourceValue("ValidFileName");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ValidFileName + "')", true);
                        return;
                    }

                    if (FileUpLoad1.FileName.ToLower().Contains("vendor")) {
                        clsVendorImport objclsVendorImport = new clsVendorImport();
                        returnError = objclsVendorImport.UploadVendor(uploadedFilePath, FileUpLoad1.FileName);
                    }
                    else if (FileUpLoad1.FileName.ToLower().Contains("item")) {
                        string strUserID = Convert.ToString(System.Web.HttpContext.Current.Session["UserId"]);
                        isSKUFile = true;
                        MethodInvoker simpleDelegate = new MethodInvoker(SKUImport);
                        // Calling SendNoShowMails Async
                        simpleDelegate.BeginInvoke(uploadedFilePath, strUserID,null, null);

                        //pnlModelBox.Visible = true;
                        //clsSKUImport objclsSKUImport = new clsSKUImport();
                        //returnError = objclsSKUImport.UploadItem(uploadedFilePath, FileUpLoad1.FileName, strUserID);
                    }
                    else if (FileUpLoad1.FileName.ToLower().Contains("purchaseorder")) {
                        ClsPurchaseOrderImport objClsPurchaseOrderImport = new ClsPurchaseOrderImport();
                        returnError = objClsPurchaseOrderImport.UploadPurchaseOrder(uploadedFilePath, FileUpLoad1.FileName);
                    }
                    else if (FileUpLoad1.FileName.ToLower().Contains("receipt")) {
                        clsReceiptInformation objclsReceiptInformation = new clsReceiptInformation();
                        returnError = objclsReceiptInformation.UploadReceiptInformation(uploadedFilePath, FileUpLoad1.FileName);
                    }
                    else if (FileUpLoad1.FileName.ToLower().Contains("forecastedsales")) {
                        clsExpediteImport objclsExpediteImport = new clsExpediteImport();
                        returnError = objclsExpediteImport.UploadExpedite(uploadedFilePath, FileUpLoad1.FileName);
                    }
                    else if (FileUpLoad1.FileName.ToLower().Contains("filbov"))
                    {
                        ClsBOImport objBOFile = new ClsBOImport();
                        objBOFile.UploadBO(uploadedFilePath, FileUpLoad1.FileName);
                    }

                    if (!string.IsNullOrEmpty(returnError)) {
                       
                        string InvalidFileFormat = WebCommon.getGlobalResourceValue("InvalidFileFormat");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidFileFormat + "')", true);
                        return;
                    }

                    if (!isSKUFile)
                    BindmanualUploadGrid();

                }
                else {
                    lblMessage.Text = "No File Uploaded.";
                }
            }
            else {
                string InvalidFileFormat = WebCommon.getGlobalResourceValue("UploadLatestFile");//"Cannot upload file.Please upload latest file ";//WebCommon.getGlobalResourceValue("InvalidFileFormat");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidFileFormat + "')", true);
                return;
            }
        }
        catch (Exception ex) {
            if (ex.Message.ToLower().Trim() == "index was outside the bounds of the array.") {
                
                string InvalidFileFormat = WebCommon.getGlobalResourceValue("UploadProperFile");//"Cannot upload file.Please upload file with proper format.";//WebCommon.getGlobalResourceValue("InvalidFileFormat");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidFileFormat + "')", true);
                return;
            }
        }
        finally {
            if(!isSKUFile)
                ResetStatus();
        }
    }

    private void SKUImport(string uploadedFilePath, string strUserID) {
        try {
            clsSKUImport objclsSKUImport = new clsSKUImport();
            string skuError = objclsSKUImport.UploadItem(uploadedFilePath, FileUpLoad1.FileName, strUserID);

            if (!string.IsNullOrEmpty(skuError)) {                
                mdNoShow.Show();
            }
            else {
                BindmanualUploadGrid();
            }
        }
        catch { }
        finally {            
            ResetStatus(); }
        //pnlModelBox.Visible = false;
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        mdNoShow.Hide();
     }

    
    private void ResetStatus() {
        // [UP_ImportProcessStatus] table is being reset here. 
        UP_DataImportSchedulerBE up_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        UP_DataImportSchedulerBAL oUP_DataImportSchedulerBAL = new UP_DataImportSchedulerBAL();

        up_DataImportSchedulerBE.Action = "UpdateImportStatus";
        up_DataImportSchedulerBE.ProcessType = "M";
        oUP_DataImportSchedulerBAL.CheckUpdateImportStatusBAL(up_DataImportSchedulerBE);   
    }

    protected string GetErrorCountLink(string ErrorCount, string ManualFileUploadID, string RecordInfo)
    {
        string labelText = string.Empty;
        if (!string.IsNullOrEmpty(ErrorCount))
        {
            if (Convert.ToInt32(ErrorCount) > 0)
            {
                labelText = "<a href=" + EncryptQuery("ManualFileUpload.aspx?RecordInfo=" + RecordInfo + "&ManualFileUploadID=" + ManualFileUploadID) + ">" + ErrorCount.ToString() + "</a>";
            }
            else
            {
                labelText = ErrorCount.ToString();
            }
        }
        return labelText;
    }


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilities;
using WebUtilities;
using System.IO;
using System.Configuration;
using System.Data;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using Microsoft.Reporting.WebForms;

public partial class RMSFileUpload : CommonPage
{
    #region ReportVariables
    Warning[] warnings;
    string[] streamIds;
    string mimeType = string.Empty;
    string encoding = string.Empty;
    string extension = string.Empty;
    string reportFileName = string.Empty;
    #endregion

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            CheckRunningStatus();
            BindmanualUploadGrid();
        }
        if (GetQueryStringValue("RecordInfo") != null && GetQueryStringValue("ManualFileUploadID") != null) {
            ShowErrorReport(GetQueryStringValue("RecordInfo").ToString(), GetQueryStringValue("ManualFileUploadID").ToString());
        }
    }

    private void CheckRunningStatus() {
        string DataImportRunningMessage = WebCommon.getGlobalResourceValue("DataImportRunningMessage");
        UP_DataImportSchedulerBE up_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        UP_DataImportSchedulerBAL oUP_DataImportSchedulerBAL = new UP_DataImportSchedulerBAL();

        // Check If DataImport is already running in Background using [UP_ImportProcessStatus] table.
        up_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        up_DataImportSchedulerBE.Action = "CheckRMSStatus";
        up_DataImportSchedulerBE.ProcessType = "M";
        string strStatus = oUP_DataImportSchedulerBAL.CheckUpdateImportStatusBAL(up_DataImportSchedulerBE);
        if (strStatus.Trim().ToUpper().Equals("MAN")) {
            lblMessage.Text = "Manual Data Import Process is running in background, Please wait for few minutes....";
            UploadBtn.Visible = false;
        }
        else {
            lblMessage.Text = "No Import is running, you can import file.";
            UploadBtn.Visible = true;
        }
    }

    protected void btnCheck_Click(object sender, EventArgs e) {
        CheckRunningStatus();

        if (lblMessage.Text == "No Import is running, you can import file.") {
            BindmanualUploadGrid();
        }
    }

    private void ShowErrorReport(string RecordInfo, string ManualFileUploadID) {

        int ManualFileUploadId = Convert.ToInt32(GetQueryStringValue("ManualFileUploadID"));
        UP_DataImportSchedulerBE oUP_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        UP_DataImportSchedulerBAL oUP_PurchaseOrderDetailBAL = new UP_DataImportSchedulerBAL();
        DataSet dsErrorCount = new DataSet();
        string DataTableName = string.Empty;
        string ReportName = string.Empty;
        if (RecordInfo == "RMS") {

            oUP_DataImportSchedulerBE.Action = "GetRMSErrorDetail";
            oUP_DataImportSchedulerBE.ManualFileUploadID = ManualFileUploadId;
            dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
            DataTableName = "UP_ErrorRMS";
            ReportName = "\\ModuleUI\\Upload\\Report\\RptRMSErrorDetails.rdlc";
            reportFileName = "RMS Error Detail";
        }
        
        oUP_PurchaseOrderDetailBAL = null;
        ReportDataSource rdsErrorReport = new ReportDataSource(DataTableName, dsErrorCount.Tables[0]);
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~") + ReportName;

        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(rdsErrorReport);

        byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "attachment; filename = " + reportFileName + "." + extension);
        Response.BinaryWrite(bytes);
        Response.Flush();

    }

    private void BindmanualUploadGrid() {
        grdManualUpload.DataSource = null;
        UP_ManualFileUploadBE oUP_ManualFileUploadBE = new UP_ManualFileUploadBE();

        oUP_ManualFileUploadBE.Action = "GetRMSUploadData";
        UP_ManualFileUploadBAL oUP_ManualFileUploadBAL = new UP_ManualFileUploadBAL();
        List<UP_ManualFileUploadBE> UP_ManualFileUploadBEList = new List<UP_ManualFileUploadBE>();
        UP_ManualFileUploadBEList = oUP_ManualFileUploadBAL.GetRMSUploadBAL(oUP_ManualFileUploadBE);
        grdManualUpload.DataSource = UP_ManualFileUploadBEList;
        grdManualUpload.DataBind();
    }

    public delegate void MethodInvoker(string uploadedFilePath, string strUserID);

    protected void UploadBtn_Click(object sender, EventArgs e) {
        string returnError = "";
       
        UP_DataImportSchedulerBE up_DataImportSchedulerBE = null;
        string strStatus = string.Empty;
        try {
            string DataImportRunningMessage = WebCommon.getGlobalResourceValue("RMSImportRunningMessage");
            UP_DataImportSchedulerBAL oUP_DataImportSchedulerBAL = new UP_DataImportSchedulerBAL();

            // Check If DataImport is already running in Background using [UP_ImportProcessStatus] table.
            up_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
            up_DataImportSchedulerBE.Action = "CheckRMSStatus";
            up_DataImportSchedulerBE.ProcessType = "M";
            strStatus = oUP_DataImportSchedulerBAL.CheckUpdateImportStatusBAL(up_DataImportSchedulerBE);
            if (strStatus.Trim().ToUpper().Equals("MAN")) {
                lblMessage.Text = DataImportRunningMessage;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DataImportRunningMessage + "')", true);
                return;
            }

            // Check If DataImport is already running in Background.            
            DataSet dsDataImportRunningStatus = oUP_DataImportSchedulerBAL.CheckIsDataImportRunningBAL("CheckByRMSStageTable");
            if (dsDataImportRunningStatus != null && dsDataImportRunningStatus.Tables[0].Rows.Count > 0) {
                if (dsDataImportRunningStatus.Tables[0].Rows[0]["IsDataImportRunning"].ToString() == "True")//data import is running in background 
                {
                    lblMessage.Text = DataImportRunningMessage;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DataImportRunningMessage + "')", true);
                    return;
                }
            }
            //--------------------------


            string uploadedFilePath = ConfigurationManager.AppSettings["UploadFilePath"];

            if (!System.IO.Directory.Exists(uploadedFilePath))
                System.IO.Directory.CreateDirectory(uploadedFilePath);

            uploadedFilePath = uploadedFilePath + DateTime.Now.Year.ToString();
            if (DateTime.Now.Month.ToString().Length == 1)
                uploadedFilePath = uploadedFilePath + "0" + DateTime.Now.Month.ToString();
            else
                uploadedFilePath = uploadedFilePath + DateTime.Now.Month.ToString();

            if (DateTime.Now.Date.ToString().Length == 1)
                uploadedFilePath = uploadedFilePath + "0" + DateTime.Now.Day.ToString();
            else
                uploadedFilePath = uploadedFilePath + DateTime.Now.Day.ToString();

            uploadedFilePath = uploadedFilePath + "_" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            uploadedFilePath = uploadedFilePath + "_" + FileUpLoad1.FileName;


            string[] strArray = Path.GetFileNameWithoutExtension(FileUpLoad1.FileName).Split('_');
            if (strArray[2].Length < 8) {

                string returnDateError = WebCommon.getGlobalResourceValue("RMSInvalidDateFormat");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + returnDateError + "')", true);
                return;
            }

            if (!validationFunctions.IsValidDateFormat(strArray[2])) {

                string returnDateError = WebCommon.getGlobalResourceValue("RMSInvalidDateFormat");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + returnDateError + "')", true);
                return;
            }

            if (ImportDB.CheckToBeRMSFileDateIsGreater(Path.GetFileNameWithoutExtension(FileUpLoad1.FileName))) 
            {
                lblMessage.Text = DataImportRunningMessage;
                FileUpLoad1.SaveAs(uploadedFilePath);

                if (FileUpLoad1.HasFile) {
                    string ext = Path.GetExtension(FileUpLoad1.FileName);

                    if (ext != ".txt") {
                        string ValidFileExtension = WebCommon.getGlobalResourceValue("ValidFileExtension");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ValidFileExtension + "')", true);
                        return;
                    }
                    if (!((FileUpLoad1.FileName.ToLower().Contains("rms")))) {
                        string ValidFileName = WebCommon.getGlobalResourceValue("ValidFileName");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ValidFileName + "')", true);
                        return;
                    }

                    if (FileUpLoad1.FileName.ToLower().Contains("rms")) {
                        string strUserID = Convert.ToString(System.Web.HttpContext.Current.Session["UserId"]);
                       
                        MethodInvoker simpleDelegate = new MethodInvoker(RMSImport);
                        // Calling SendNoShowMails Async
                        simpleDelegate.BeginInvoke(uploadedFilePath, strUserID, null, null);

                        //pnlModelBox.Visible = true;
                        //clsSKUImport objclsSKUImport = new clsSKUImport();
                        //returnError = objclsSKUImport.UploadItem(uploadedFilePath, FileUpLoad1.FileName, strUserID);
                    }
                   

                    if (!string.IsNullOrEmpty(returnError)) {

                        string InvalidFileFormat = WebCommon.getGlobalResourceValue("InvalidFileFormat");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidFileFormat + "')", true);
                        return;
                    }
                   
                    BindmanualUploadGrid();

                }
                else {
                    lblMessage.Text = "No File Uploaded.";
                }
            }
            else {
                string InvalidFileFormat = WebCommon.getGlobalResourceValue("UploadLatestFile");//"Cannot upload file.Please upload latest file ";//WebCommon.getGlobalResourceValue("InvalidFileFormat");
                lblMessage.Text = InvalidFileFormat;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidFileFormat + "')", true);
                return;
            }
        }
        catch (Exception ex) {
            if (ex.Message.ToLower().Trim() == "index was outside the bounds of the array.") {

                string InvalidFileFormat = WebCommon.getGlobalResourceValue("UploadProperFile");//"Cannot upload file.Please upload file with proper format.";//WebCommon.getGlobalResourceValue("InvalidFileFormat");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidFileFormat + "')", true);
                return;
            }
        }
        finally {           
                ResetStatus();
        }
    }

    private void RMSImport(string uploadedFilePath, string strUserID) {
        try {
            clsRMSImport objclsRMSImport = new clsRMSImport();
            string skuError = objclsRMSImport.UploadRMS(uploadedFilePath, FileUpLoad1.FileName, strUserID);

            if (!string.IsNullOrEmpty(skuError)) {
                mdNoShow.Show();
            }
            else {
                BindmanualUploadGrid();
            }
        }
        catch { }
        finally {
            ResetStatus();
        }
        //pnlModelBox.Visible = false;
    }

    protected void btnProceed_Click(object sender, EventArgs e) {
        mdNoShow.Hide();
    }


    private void ResetStatus() {
        // [UP_ImportProcessStatus] table is being reset here. 
        UP_DataImportSchedulerBE up_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        UP_DataImportSchedulerBAL oUP_DataImportSchedulerBAL = new UP_DataImportSchedulerBAL();

        up_DataImportSchedulerBE.Action = "UpdateRMSImportStatus";
        up_DataImportSchedulerBE.ProcessType = "M";
        oUP_DataImportSchedulerBAL.CheckUpdateImportStatusBAL(up_DataImportSchedulerBE);
    }

    protected string GetErrorCountLink(string ErrorCount, string ManualFileUploadID, string RecordInfo) {
        string labelText = string.Empty;
        if (!string.IsNullOrEmpty(ErrorCount)) {
            if (Convert.ToInt32(ErrorCount) > 0) {
                labelText = "<a href=" + EncryptQuery("RMSFileUpload.aspx?RecordInfo=" + RecordInfo + "&ManualFileUploadID=" + ManualFileUploadID) + ">" + ErrorCount.ToString() + "</a>";
            }
            else {
                labelText = ErrorCount.ToString();
            }
        }
        return labelText;
    }


}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="Scheduler.aspx.cs" Inherits="ModuleUI_Upload_Scheduler" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="~/Scripts/JScript.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
           
        function setValue(target) {
            document.getElementById('<%=hdnJSDt.ClientID %>').value = target.value;
        }
        $(document).ready(function () {
            $('#txtFromDate').datepicker({ showOtherMonths: true,
                selectOtherMonths: true, changeMonth: true, changeYear: true,
                minDate: '0',
                yearRange: '2013:+100',
                dateFormat: 'dd/mm/yy'
            });
        });



        function RadioCheck(rb) {
            var gv = document.getElementById("<%=gvUpcomingJob.ClientID%>");
            var rbs = gv.getElementsByTagName("input");

            var row = rb.parentNode.parentNode;
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i].type == "radio") {
                    if (rbs[i].checked && rbs[i] != rb) {
                        rbs[i].checked = false;
                        break;
                    }
                }
            }
        }

        $(document).ready(function () {
            var isChecked = false;
            $('#<%=btnDelete.ClientID%>').click(function () {
               
                $('#<%=gvUpcomingJob.ClientID%> tr').each(function () {
                   
                    var radio = $(this).find("input[type='radio']");
                    if ($(radio).is(':checked')) {
                        isChecked = true;
                    }

                });

                if (isChecked == true) {
                    return confirm('<%=DeleteMessage%>');
                }
                else {
                    alert('<%=PleaseSelectAtleastOneJob%>')
                    return false;
                }

            });

        });

    </script>
    <asp:HiddenField ID="hdnJSDt" runat="server" />
    <h2>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
        <cc1:ucLabel ID="lblOTIFScorecardRescheduler" Text="OTIF / Scorecard Rescheduler"
            runat="server"></cc1:ucLabel>
    </h2>
    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
        <tr>
            <td style="font-weight: bold; width: 15%;">
                <cc1:ucLabel ID="lblSelectWhichJobToRun" Text="Select which job to run" runat="server"></cc1:ucLabel>:
            </td>
            <td align="left" class="nobold radiobuttonlist" style="font-weight: bold; width: 60%;">
                <cc1:ucRadioButton ID="rbtnOTIF" runat="server" Text="OTIF" GroupName="job" Checked="true" />
                <cc1:ucRadioButton ID="rbtnVSC" runat="server" Text="Vendor Score Card" GroupName="job" />
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblSelectMonthYear" Text="Select month/year" runat="server"></cc1:ucLabel>:
            </td>
            <td style="font-weight: bold;">
                <cc1:ucDropdownList ID="ddlDate" runat="server" Width="100px">
                </cc1:ucDropdownList>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblSelectRunDate" runat="server" Text="Select run date"></cc1:ucLabel>:
            </td>
            <td style="font-weight: bold;">
                <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue(this)"
                    ReadOnly="True" Width="70px" />
                <%--<cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                    ReadOnly="True" Width="70px" />--%>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblSelectRunTime" runat="server" Text="Select run time"></cc1:ucLabel>:
            </td>
            <td style="font-weight: bold;">
                <cc1:ucDropdownList ID="ddlSlotTime" runat="server" Width="60px">
                </cc1:ucDropdownList>
                <cc1:ucLabel ID="lblHHMM1" runat="server"></cc1:ucLabel>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td style="font-weight: bold; width: 70%;">
                <cc1:ucLabel ID="lblSchedulerNoticeMessage" runat="server" Text="Time Slot" isRequired="true"></cc1:ucLabel>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <div class="button-row">
                    <cc1:ucButton ID="btnRun" runat="server" Text="Run" CssClass="button" OnClick="btnRun_Click" />
                </div>
            </td>
        </tr>
    </table>
    <h2>
        <cc1:ucLabel ID="lblUpcomingJobSchedule" Text="Upcoming Job Schedule" runat="server"></cc1:ucLabel>
    </h2>
    <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
        <tr>
            <td style="font-weight: bold; width: 100%;" colspan="6" align="center">
                <cc1:ucGridView ID="gvUpcomingJob" runat="server" AutoGenerateColumns="false" CssClass="grid"
                    OnSorting="SortGrid" AllowPaging="true" PageSize="20" Width="100%" AllowSorting="true">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                    <EmptyDataTemplate>
                        <div style="text-align: center">
                            <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>
                        </div>
                    </EmptyDataTemplate>
                    <Columns>
                    <asp:TemplateField >
                                    <HeaderStyle HorizontalAlign="Left" Width="100px" />
                                    <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                          <asp:RadioButton ID="rbCheckSelect" runat="server" onclick = "RadioCheck(this);"/>
                                       <asp:HiddenField ID="hdnSchedulerId" runat="server" Value='<%# Eval("SchedulerId") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                        <asp:TemplateField HeaderText="ScheduledFor" SortExpression="SchedulerDate">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblSchedulerDate" runat="server" Text='<%# Eval("SchedulerDate","{0:dd/MM/yyyy HH:mm}") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" SortExpression="JobName">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblJobName" runat="server" Text='<%# (Eval("JobName").ToString()=="OTIF")?"OTIF":"Scorecard" %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MonthYear" SortExpression="DisplayMonthYear">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblMonthYears" runat="server" Text='<%# Eval("DisplayMonthYear","{0:MMM - yy}") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User" SortExpression="UserName">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblUsrName" runat="server" Text='<%# Eval("UserName") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                </cc1:ucGridView>
                <cc1:ucLabel ID="lblNoRecordsFound" Font-Bold="true" ForeColor="Red" Visible="false"
                    runat="server"></cc1:ucLabel>
                <div class="button-row">
                <cc1:ucButton ID="btnEdit" runat="server" Text="Edit" CssClass="button" OnClick="btnEdit_Click"
                    ValidationGroup="Edit" />
                <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" 
                        onclick="btnDelete_Click" />
               
            </div>
            </td>
        </tr>
    </table>
</asp:Content>

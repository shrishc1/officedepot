﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ManualFileUpload.aspx.cs" Inherits="ManualFileUpload" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        //        function showModelBox(show) {
        //            if (show == 'Y')
        //                document.getElementById('<%=pnlModelBox.ClientID %>').style.display = 'block';
        //            else
        //                document.getElementById('<%=pnlModelBox.ClientID %>').style.display = 'none';
        //        }

    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblManualFileUpload" runat="server" Text="Manual File Upload"></cc1:ucLabel>
    </h2>
    <table width="100%" border="0">
        <tr>
            <td style="width:50%;text-align:left;">
                <asp:FileUpload ID="FileUpLoad1" runat="server" Width="380px" />
            </td>
            <td style="width:50%;text-align:left;">
                <cc1:ucButton ID="UploadBtn" Text="Upload File" OnClick="UploadBtn_Click" runat="server"
                    Width="105px" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                -----------------------------------------------------------------------------------------------------------------
            </td>
        </tr>
        <tr>
            <td style="text-align:left;">
                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
            </td>
            <td style="text-align:left;">
                <cc1:ucButton ID="bt123" Text="Click to Check import running status" OnClick="btnCheck_Click"
                    runat="server" Width="220px" />
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <table width="100%">
                <tr>
                    <td align="center" colspan="9">
                        <cc1:ucGridView ID="grdManualUpload" Width="100%" runat="server" CssClass="grid"
                            AutoGenerateColumns="False" CellPadding="0" GridLines="None">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                            <Columns>
                                <asp:BoundField HeaderText="Date Uploaded" DataField="TransactionDate" SortExpression="TransactionDate"
                                    DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="User Name" DataField="Username" SortExpression="Username">
                                    <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Downloaded Filename" DataField="DownloadedFilename" SortExpression="DownloadedFilename">
                                    <HeaderStyle HorizontalAlign="Left" Width="6%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="3%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblBank1" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Total Vendor Records" DataField="TotalVendorRecord" SortExpression="TotalVendorRecord">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Error Count in Vendor" SortExpression="ErrorVendorCount">
                                    <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblErrorVendorCount" runat="server" Text='<%# GetErrorCountLink(Convert.ToString(Eval("ErrorVendorCount")),Convert.ToString(Eval("ManualFileUploadID")),"VEN")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="3%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblBank2" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Total SKU Records" DataField="TotalSKURecord" SortExpression="TotalSKURecord">
                                    <HeaderStyle HorizontalAlign="Center" Width="7%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Error Count in SKU" SortExpression="ErrorSKUCount">
                                    <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblErrorSKUCount" runat="server" Text='<%# GetErrorCountLink(Convert.ToString(Eval("ErrorSKUCount")),Convert.ToString(Eval("ManualFileUploadID")),"SKU")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="3%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblBank3" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Total PO Records" DataField="TotalPORecord" SortExpression="TotalPORecord">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Error Count in PO" SortExpression="ErrorPOCount">
                                    <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblErrorPOCount" runat="server" Text='<%# GetErrorCountLink(Convert.ToString(Eval("ErrorPOCount")),Convert.ToString(Eval("ManualFileUploadID")),"PO")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="3%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblBank4" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Total Receipt Records" DataField="TotalReceiptInformationRecord"
                                    SortExpression="TotalReceiptInformationRecord">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Error Count in Receipt" SortExpression="ErrorReceiptCount">
                                    <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblErrorReceiptCount" runat="server" Text='<%# GetErrorCountLink(Convert.ToString(Eval("ErrorReceiptCount")),Convert.ToString(Eval("ManualFileUploadID")),"RI")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="3%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblBank5" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Total Expedite Records" DataField="TotalExpediteRecord"
                                    SortExpression="TotalExpediteRecord">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Error Count in Expedite" SortExpression="ErrorExpediteCount">
                                    <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblErrorExpediteCount" runat="server" Text='<%# GetErrorCountLink(Convert.ToString(Eval("ErrorExpediteCount")),Convert.ToString(Eval("ManualFileUploadID")),"EX")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                  <asp:BoundField HeaderText="Total BO Records" DataField="TotalBORecord"
                                    SortExpression="TotalBORecord">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Error Count in BO" SortExpression="ErrorBOCount">
                                    <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblErrorExpediteCount" runat="server" Text='<%# GetErrorCountLink(Convert.ToString(Eval("ErrorBOCount")),Convert.ToString(Eval("ManualFileUploadID")),"BO")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="3%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblBank7" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
                        </cc1:ucGridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="pnlModelBox" runat="server" visible="false">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="center">
                    <div align="center" style="background-color: #000; top: 0px; left: 0px; bottom: 0px;
                        right: 0px; padding-top: 20%; margin: 0; width: 100%; height: 100%; overflow: hidden;
                        position: absolute; z-index: 1000; filter: alpha(opacity=50); opacity: 0.5;">
                        <asp:Image ID="imgWait" runat="server" ImageUrl="~/Images/Ajaxloading.gif" ImageAlign="Middle" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Visible="false">
    </rsweb:ReportViewer>
    <asp:UpdatePanel ID="updNoShow" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnNoShows" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdNoShow" runat="server" TargetControlID="btnNoShows"
                PopupControlID="pnlbtnNoShow" BackgroundCssClass="modalBackground" BehaviorID="MsgNoShow"
                DropShadow="false" />
            <asp:Panel ID="pnlbtnNoShow" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" width="400px;" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold; text-align: center;">
                                <cc1:ucLabel ID="ltError" Text="Some error occurred in SKU import." runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnOK" runat="server" Text="OK" OnCommand="btnProceed_Click" CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOK" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="ReschedulerHistory.aspx.cs" Inherits="ModuleUI_Upload_ReschedulerHistory" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <cc1:ucLabel ID="lblOTIFScorecardReschedulerHistory" Text="OTIF / Scorecard Rescheduler History"
            runat="server"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <table width="100%" cellspacing="5" cellpadding="0" border="0" class="top-settingsNoBorder">
        <tr>
            <td style="font-weight: bold; width: 100%;" colspan="6" align="center">
                <div class="button-row">
                    <uc1:ucExportButton ID="btnExportToExcel" runat="server" />
                </div>
                <cc1:ucGridView ID="gvJobHistory" runat="server" AutoGenerateColumns="false" CssClass="grid"
                    OnSorting="SortGrid" AllowPaging="true" PageSize="50" Width="100%" AllowSorting="true"
                    OnPageIndexChanging="gvJobHistory_PageIndexChanging">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                    <EmptyDataTemplate>
                        <div style="text-align: center">
                            <cc1:ucLabel ID="lblRecordNotFound" isRequired="true" runat="server" Text="No record found"></cc1:ucLabel>
                        </div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="StartTime" SortExpression="StartTime">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnSchedularId" runat="server" Value='<%# Eval("SchedulerId") %>' />
                                <cc1:ucLabel ID="lblStartTimes" runat="server" Text='<%# Eval("StartTime","{0:d-MMM-yyyy HH:mm}") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EndTime" SortExpression="EndTime">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblEndTimes" runat="server" Text='<%# Eval("EndTime","{0:dd-MMM-yyyy HH:mm}") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" SortExpression="JobName">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblJobName" runat="server" Text='<%# (Eval("JobName").ToString()=="OTIF")?"OTIF":"Scorecard" %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MonthYear" SortExpression="DisplayMonthYear">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblMonthYears" runat="server" Text='<%# Eval("DisplayMonthYear","{0:MMM - yy}") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User" SortExpression="UserName">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblUsrName" runat="server" Text='<%# Eval("UserName") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" SortExpression="Status">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:LinkButton ID="linkStatuss" runat="server" OnClick="linkStatuss_Click"
                                    Visible='<%# (Eval("Status").ToString()=="Processing" && Eval("JobName").ToString()=="OTIF") ? true :false %>'>    
                                    <%# Eval("Status") %></asp:LinkButton>

                                <cc1:ucLabel ID="lblStatuss" runat="server" Text='<%# Eval("Status") %>'
                                    Visible='<%# (Eval("Status").ToString()=="Processing" && Eval("JobName").ToString()=="OTIF") ? false :true %>'>
                                </cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                </cc1:ucGridView>
                <cc1:ucGridView ID="tempGridView" Visible="false" Width="100%" runat="server" CssClass="grid"
                    OnSorting="SortGrid" AllowSorting="true" Style="overflow: auto;">
                    <Columns>
                        <asp:TemplateField HeaderText="StartTime" SortExpression="StartTime">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblStartTimes" runat="server" Text='<%# Eval("StartTime","{0:MM/dd/yyyy HH:mm}") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EndTime" SortExpression="EndTime">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblEndTimes" runat="server" Text='<%# Eval("EndTime","{0:MM/dd/yyyy HH:mm}") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" SortExpression="JobName">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblJobName" runat="server" Text='<%# (Eval("JobName").ToString()=="OTIF")?"OTIF":"Scorecard" %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MonthYear" SortExpression="DisplayMonthYear">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblMonthYears" runat="server" Text='<%# Eval("DisplayMonthYear","{0:MMM - yy}") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User" SortExpression="UserName">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblUsrName" runat="server" Text='<%# Eval("UserName") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" SortExpression="Status">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <cc1:ucLabel ID="lblStatuss" runat="server" Text='<%# Eval("Status") %>'></cc1:ucLabel>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
                <cc1:ucLabel ID="lblNoRecordsFound" Font-Bold="true" ForeColor="Red" Visible="false"
                    runat="server"></cc1:ucLabel>
            </td>
        </tr>
    </table>

    <asp:UpdatePanel ID="upShowLegend" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Button ID="btnShowLegend" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlShowLegend" runat="server" TargetControlID="btnShowLegend"
                PopupControlID="pnlShowLegend" BackgroundCssClass="modalBackground" BehaviorID="ShowLegend"
                DropShadow="false" />
            <asp:Panel ID="pnlShowLegend" runat="server" CssClass="fieldset-form" Style="display: none">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <table width="100%" cellspacing="2" cellpadding="0" border="0" class="popup-maincontainer">
                        <tr>
                            <td width="100%" valign="top">
                                <asp:Panel ID="pnlLegend_5" runat="server" GroupingText="Scheduling Progress Information" CssClass="fieldset-form">
                                    <table cellspacing="5" cellpadding="0" border="0" align="center">
                                        <tr>
                                            <td colspan="9">
                                                <b>
                                                    <cc1:ucLabel ID="lblOTIFVendor" runat="server" Text="OTIF Vendor"></cc1:ucLabel></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 50px;" class="spanLeft">
                                                <cc1:ucLabel ID="lblVendorTotal" runat="server" Text="Total"></cc1:ucLabel>
                                            </td>
                                            <td class="spanLeft">:                                         
                                            </td>
                                            <td style="width: 50px" class="spanLeft">
                                                <cc1:ucLabel ID="lblVendorTotal_1" runat="server"></cc1:ucLabel>
                                            </td>
                                           

                                            <td style="width: 50px" class="spanLeft">
                                                <cc1:ucLabel ID="lblVendorDone" runat="server" Text="Calculated"></cc1:ucLabel>
                                            </td>
                                            <td class="spanLeft">:                                         
                                            </td>
                                            <td style="width: 50px" class="spanLeft">
                                                <cc1:ucLabel ID="lblVendorDone_1" runat="server"></cc1:ucLabel>
                                            </td>

                                            <td style="width: 50px" class="spanLeft">
                                                <cc1:ucLabel ID="lblVendorRemaing" runat="server" Text="Remaining"></cc1:ucLabel>
                                            </td>
                                            <td class="spanLeft">:                                         
                                            </td>
                                            <td style="width: 50px;" class="spanLeft">
                                                <cc1:ucLabel ID="lblVendorRemaing_1" runat="server"></cc1:ucLabel>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="9"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="9">
                                                <b>
                                                    <cc1:ucLabel ID="UcLabel1" runat="server" Text="OTIF Master Vendor"></cc1:ucLabel></b>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td style="width: 50px;" class="spanLeft">
                                                <cc1:ucLabel ID="lblMasterVendorTotal" runat="server" Text="Total"></cc1:ucLabel>
                                            </td>
                                            <td class="spanLeft">:                                         
                                            </td>
                                            <td style="width: 50px" class="spanLeft">
                                                <cc1:ucLabel ID="lblMasterVendorTotal_1" runat="server"></cc1:ucLabel>
                                            </td>


                                            <td style="width: 50px" class="spanLeft">
                                                <cc1:ucLabel ID="lblMasterVendorDone" runat="server" Text="Calculated"></cc1:ucLabel>
                                            </td>
                                            <td class="spanLeft">:                                         
                                            </td>
                                            <td style="width: 50px" class="spanLeft">
                                                <cc1:ucLabel ID="lblMasterVendorDone_1" runat="server"></cc1:ucLabel>
                                            </td>


                                            <td style="width: 50px" class="spanLeft">
                                                <cc1:ucLabel ID="lblMasterVendorRemaing" runat="server" Text="Remaining"></cc1:ucLabel>
                                            </td>
                                            <td class="spanLeft">:                                         
                                            </td>
                                            <td style="width: 50px" class="spanLeft">
                                                <cc1:ucLabel ID="lblMasterVendorRemaing_1" runat="server"></cc1:ucLabel>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td align="center" colspan="9">
                                                <br />
                                                <cc1:ucButton ID="btnClose" runat="server" Text="CLOSE" CssClass="button"
                                                    Style="text-transform: uppercase;"
                                                    OnClientClick="return HideLegendModalPopup();" />
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>

                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        function ShowLegendPopup() {
            $find("ShowLegend").show();
            return false;
        }
        function HideLegendModalPopup() {
            $find("ShowLegend").hide();
            return false;
        }
    </script>


</asp:Content>

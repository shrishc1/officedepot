﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using BusinessLogicLayer.ModuleBAL.Upload;
using BusinessEntities.ModuleBE.Upload;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using WebUtilities;
using Utilities;
using System.Web.UI.WebControls;

public partial class ModuleUI_Upload_Scheduler : CommonPage
{

    #region Declarations ...

    string schedulerAlreadyScheduled = WebCommon.getGlobalResourceValue("SchedulerAlreadyScheduled");
    string pleaseSelectTime = WebCommon.getGlobalResourceValue("PleaseSelectTime");
    string savedMessage = WebCommon.getGlobalResourceValue("SavedMessage");
    string pleaseSelectFutureTime = WebCommon.getGlobalResourceValue("PleaseSelectFutureTime");
   protected string DeleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
   protected string PleaseSelectAtleastOneJob = WebCommon.getGlobalResourceValue("PleaseSelectAtleastOneJob"); 
       
    
    int SchedulerId;

    #endregion Declarations ...

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMoths();
            BindTimes();
            ddlSlotTime.SelectedValue = "288";
            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnJSDt.Value = txtFromDate.Text;
            BindUpcomingJobGrid();
        }
    }

    protected void btnRun_Click(object sender, EventArgs e)
    {
        TimeSpan scheduleTime = TimeSpan.Parse(ddlSlotTime.SelectedItem.ToString());
        DateTime SchedulerDate = string.IsNullOrEmpty(Convert.ToString(hdnJSDt.Value)) ? DateTime.Now : Common.GetMM_DD_YYYY(hdnJSDt.Value);
        SchedulerDate = SchedulerDate.Add(scheduleTime);

        if (SchedulerDate < DateTime.Now)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + pleaseSelectFutureTime + "')", true);
            return;
        }

        //var ApplicationName = ddlJob.SelectedItem.ToString();
        string ApplicationName = string.Empty;
        if (rbtnOTIF.Checked)
            ApplicationName = "OTIF";
        else
            ApplicationName = "VSC";

        int month = Convert.ToInt32(ddlDate.SelectedValue);
        int year = Convert.ToInt32(ddlDate.SelectedItem.ToString().Split('-')[1].Trim());

        ScheduleBAL scheduleBAL = new ScheduleBAL();
        ScheduleBE scheduleBE = new ScheduleBE();
        scheduleBE.Action = "isAlreadySchedule";
        scheduleBE.JobName = ApplicationName;
        scheduleBE.Month = month;
        scheduleBE.Year = year;
        if (ddlSlotTime.SelectedIndex == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + pleaseSelectTime + "')", true);
            return;
        }
        if (scheduleBAL.isAlreadySchedulingInfoBAL(scheduleBE) > 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + schedulerAlreadyScheduled + "')", true);
            return;
        }

        scheduleBE.Action = "addScheduling";

        scheduleBE.IsExecuted = false;
        scheduleBE.InputDate = DateTime.Now;
        scheduleBE.UserID = Convert.ToInt32(Session["UserID"]);
        scheduleBE.SchedulerTime = TimeSpan.Parse(ddlSlotTime.SelectedItem.ToString());
        scheduleBE.SchedulerDate = string.IsNullOrEmpty(Convert.ToString(hdnJSDt.Value)) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSDt.Value);
        scheduleBAL.addSchedulingInfoBAL(scheduleBE);

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + savedMessage + "')", true);

        ddlSlotTime.SelectedValue = "96";
        ddlDate.SelectedIndex = 0;
        rbtnOTIF.Checked = true;
        Response.Redirect("~/ModuleUI/Upload/scheduler.aspx");
        //BindUpcomingJobGrid();
    }

    private void BindMoths()
    {
        Dictionary<string, int> dicDate = new Dictionary<string, int>();
        for (int i = 1; i <= 12; i++)
        {
            DateTime startDate = DateTime.Now.AddMonths(-i);
            DateTime currDate = DateTime.Now;
            string dat = startDate.ToString("MMM") + " - " + startDate.ToString("yyyy");
            int monthDiff = ((currDate.Year - startDate.Year) * 12) + currDate.Month - startDate.Month;
            dicDate.Add(dat, monthDiff);
        }

        ddlDate.DataSource = dicDate;
        ddlDate.DataTextField = "Key";
        ddlDate.DataValueField = "Value";
        ddlDate.DataBind();
    }

    public void BindTimes()
    {
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstTimes = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);
        if (lstTimes != null && lstTimes.Count > 0)
        {
            FillControls.FillDropDown(ref ddlSlotTime, lstTimes, "SlotTime", "SlotTimeID");
        }
    }

    private void BindUpcomingJobGrid()
    {
        ScheduleBE scheduleBE = new ScheduleBE();
        scheduleBE.Action = "GetUpcomingJobs";
        List<ScheduleBE> lstScheduleBE = new List<ScheduleBE>();
        ScheduleBAL scheduleBAL = new ScheduleBAL();
        lstScheduleBE = scheduleBAL.GetUpcomingJobBAL(scheduleBE);

        if (lstScheduleBE.Count > 0)
        {
            gvUpcomingJob.DataSource = lstScheduleBE;
            gvUpcomingJob.DataBind();
            btnEdit.Visible = true;
            btnEdit.Enabled = true;
        }
        else
        {
            gvUpcomingJob.DataSource = null;
            gvUpcomingJob.DataBind();
            btnEdit.Visible = false;
            btnEdit.Enabled = false;
        }
        Cache["lstScheduleBE"] = lstScheduleBE;
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<ScheduleBE>.SortList((List<ScheduleBE>)Cache["lstScheduleBE"], e.SortExpression, e.SortDirection).ToArray();
    }

   
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        bool rbtncheck = false;
        RadioButton rbCheckSelect = sender as RadioButton;
        foreach (GridViewRow row in gvUpcomingJob.Rows)
        {
            RadioButton rbtn = row.Cells[0].FindControl("rbCheckSelect") as RadioButton;
            HiddenField hdnSchedulerId = (HiddenField)row.FindControl("hdnSchedulerId");
            if (rbtn.Checked)
            {
                SchedulerId = Convert.ToInt32((row.Cells[0].FindControl("hdnSchedulerId") as HiddenField).Value);
                rbtncheck = true;
                break;
            }
        }

        if (rbtncheck == false)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + PleaseSelectAtleastOneJob + "')", true);
            return;
        }
        else if (rbtncheck == true && SchedulerId != 0)
        {
            EncryptQueryString("~/ModuleUI/Upload/SchedulerEdit.aspx?&SchedulerId=" + SchedulerId);
        }

        
    }



    protected void btnDelete_Click(object sender, EventArgs e)
    {       
        ScheduleBAL scheduleBAL = new ScheduleBAL();
        ScheduleBE scheduleBE = new ScheduleBE();

        RadioButton rbCheckSelect = sender as RadioButton;
        foreach (GridViewRow row in gvUpcomingJob.Rows)
        {
            RadioButton rbtn = row.Cells[0].FindControl("rbCheckSelect") as RadioButton;
            HiddenField hdnSchedulerId = (HiddenField)row.FindControl("hdnSchedulerId");
            if (rbtn.Checked)
            {
                SchedulerId = Convert.ToInt32((row.Cells[0].FindControl("hdnSchedulerId") as HiddenField).Value);
            }
        }

       
        if (SchedulerId != 0)
        {
            scheduleBE.Action = "DeleteScheduling";
            scheduleBE.SchedulerId = SchedulerId;

            int? result = scheduleBAL.DeleteSchedulingInfoBAL(scheduleBE);

            if (result == 1)
            {
                BindUpcomingJobGrid();
            }
        }
    }
}
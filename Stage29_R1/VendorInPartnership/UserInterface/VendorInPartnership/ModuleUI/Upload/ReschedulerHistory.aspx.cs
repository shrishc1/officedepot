﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System.Data;

public partial class ModuleUI_Upload_ReschedulerHistory : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        btnExportToExcel.GridViewControl = tempGridView;
        btnExportToExcel.Page = this;
        btnExportToExcel.FileName = "ReschedulerHistory";
        if (!IsPostBack)
        {
            BindJobHistoryGrid();
        }
    }

    private void BindJobHistoryGrid()
    {
        ScheduleBE scheduleBE = new ScheduleBE();
        scheduleBE.Action = "GetSchedulerHistory";
        List<ScheduleBE> lstScheduleBE = new List<ScheduleBE>();
        ScheduleBAL scheduleBAL = new ScheduleBAL();
        lstScheduleBE = scheduleBAL.GetJobHistoryBAL(scheduleBE);

        if (lstScheduleBE.Count > 0)
        {
            gvJobHistory.DataSource = lstScheduleBE;
            gvJobHistory.DataBind();
            btnExportToExcel.Visible = true;
        }
        else
        {
            gvJobHistory.DataSource = null;
            gvJobHistory.DataBind();
            btnExportToExcel.Visible = false;
        }
        //for export to excel
        tempGridView.DataSource = lstScheduleBE;
        tempGridView.DataBind();

        Cache["lstScheduleBEHistory"] = lstScheduleBE;
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<ScheduleBE>.SortList((List<ScheduleBE>)Cache["lstScheduleBEHistory"], e.SortExpression, e.SortDirection).ToArray();
    }

    protected void gvJobHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (Cache["lstScheduleBEHistory"] != null)
        {
            List<ScheduleBE> lstScheduleBE = Cache["lstScheduleBEHistory"] as List<ScheduleBE>;
            gvJobHistory.PageIndex = e.NewPageIndex;
            gvJobHistory.DataSource = lstScheduleBE;
            gvJobHistory.DataBind();
        }
    }

    protected void linkStatuss_Click(object sender, EventArgs e)
    {
        ScheduleBAL scheduleBAL = new ScheduleBAL();
        DataSet ds = scheduleBAL.GetProcessesingDataOTIFBAL();
        if (ds != null && ds.Tables.Count > 0)
        {
            lblVendorDone_1.Text= ds.Tables[0].Rows[0]["Done"].ToString();
            lblVendorRemaing_1.Text = ds.Tables[0].Rows[0]["Remaining"].ToString();
            lblVendorTotal_1.Text = ds.Tables[0].Rows[0]["Total"].ToString();

            lblMasterVendorDone_1.Text = ds.Tables[1].Rows[0]["Done"].ToString();
            lblMasterVendorRemaing_1.Text = ds.Tables[1].Rows[0]["Remaining"].ToString();
            lblMasterVendorTotal_1.Text = ds.Tables[1].Rows[0]["Total"].ToString();

            mdlShowLegend.Show();
        }
    }
}
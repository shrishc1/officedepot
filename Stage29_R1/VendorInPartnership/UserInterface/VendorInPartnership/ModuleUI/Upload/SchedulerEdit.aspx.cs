﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using Utilities;
using WebUtilities;
using BusinessLogicLayer.ModuleBAL.Upload;
using BusinessEntities.ModuleBE.Upload;

public partial class ModuleUI_Upload_SchedulerEdit : CommonPage
{
    string pleaseSelectFutureTime = WebCommon.getGlobalResourceValue("PleaseSelectFutureTime");
    string pleaseSelectTime = WebCommon.getGlobalResourceValue("PleaseSelectTime");
    string schedulerAlreadyScheduled = WebCommon.getGlobalResourceValue("SchedulerAlreadyScheduled");
    protected string savedMessage = WebCommon.getGlobalResourceValue("SavedMessage");


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMoths();
            BindTimes();
            if (GetQueryStringValue("SchedulerId") != null)
            {
                BindUpcomingJobGrid();
            }
        }         
    }

    private void BindUpcomingJobGrid()
    {
        ScheduleBE scheduleBE = new ScheduleBE();
        scheduleBE.Action = "GetUpcomingJobs";
        scheduleBE.SchedulerId = Convert.ToInt32(GetQueryStringValue("SchedulerId"));
        List<ScheduleBE> lstScheduleBE = new List<ScheduleBE>();
        ScheduleBAL scheduleBAL = new ScheduleBAL();
        lstScheduleBE = scheduleBAL.GetUpcomingJobBAL(scheduleBE);

        if (lstScheduleBE.Count > 0)
        {
            string ApplicationName=lstScheduleBE[0].JobName;
            if(ApplicationName.Equals("OTIF"))
            {
                 rbtnOTIF.Checked = true;
            }
            else if (ApplicationName.Equals("VSC"))
            {
                rbtnVSC.Checked = true; 
            }
            
            txtFromDate.Text = Convert.ToString(Convert.ToDateTime(lstScheduleBE[0].SchedulerDate).ToString("dd/MM/yyyy"));
            hdnJSDt.Value = txtFromDate.Text;
            ddlDate.SelectedIndex = ddlDate.Items.IndexOf(ddlDate.Items.FindByValue(lstScheduleBE[0].Month.ToString()));
            
            ddlSlotTime.SelectedIndex = ddlSlotTime.Items.IndexOf(ddlSlotTime.Items.FindByText(lstScheduleBE[0].SchedulerTime.ToString().Substring(0,5)));

        }
        
        Cache["lstScheduleBE"] = lstScheduleBE;
    }

    private void BindMoths()
    {
        Dictionary<string, int> dicDate = new Dictionary<string, int>();
        for (int i = 1; i <= 12; i++)
        {
            DateTime startDate = DateTime.Now.AddMonths(-i);
            DateTime currDate = DateTime.Now;
            string dat = startDate.ToString("MMM") + " - " + startDate.ToString("yyyy");
            int monthDiff = ((currDate.Year - startDate.Year) * 12) + currDate.Month - startDate.Month;
            dicDate.Add(dat, monthDiff);
        }

        ddlDate.DataSource = dicDate;
        ddlDate.DataTextField = "Key";
        ddlDate.DataValueField = "Value";
        ddlDate.DataBind();
    }

    public void BindTimes()
    {
        SYS_SlotTimeBE oSYS_SlotTimeBE = new SYS_SlotTimeBE();
        SYS_SlotTimeBAL oSYS_SlotTimeBAL = new SYS_SlotTimeBAL();
        oSYS_SlotTimeBE.Action = "ShowAll";
        List<SYS_SlotTimeBE> lstTimes = oSYS_SlotTimeBAL.GetSlotTimeBAL(oSYS_SlotTimeBE);
        if (lstTimes != null && lstTimes.Count > 0)
        {
            FillControls.FillDropDown(ref ddlSlotTime, lstTimes, "SlotTime", "SlotTimeID");
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/ModuleUI/Upload/Scheduler.aspx");
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        int? result = 0;
        TimeSpan scheduleTime = TimeSpan.Parse(ddlSlotTime.SelectedItem.ToString());
        DateTime SchedulerDate = string.IsNullOrEmpty(Convert.ToString(hdnJSDt.Value)) ? DateTime.Now : Common.GetMM_DD_YYYY(hdnJSDt.Value);
        SchedulerDate = SchedulerDate.Add(scheduleTime);

        if (SchedulerDate < DateTime.Now)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + pleaseSelectFutureTime + "')", true);
            return;
        }

        //var ApplicationName = ddlJob.SelectedItem.ToString();
        string ApplicationName = string.Empty;
        if (rbtnOTIF.Checked)
            ApplicationName = "OTIF";
        else
            ApplicationName = "VSC";

        int month = Convert.ToInt32(ddlDate.SelectedValue);
        int year = Convert.ToInt32(ddlDate.SelectedItem.ToString().Split('-')[1].Trim());

        ScheduleBAL scheduleBAL = new ScheduleBAL();
        ScheduleBE scheduleBE = new ScheduleBE();
        scheduleBE.JobName = ApplicationName;
        scheduleBE.Month = month;
        scheduleBE.Year = year;
              
        scheduleBE.Action = "EditScheduling";

        scheduleBE.IsExecuted = false;
        scheduleBE.InputDate = DateTime.Now;
        scheduleBE.UserID = Convert.ToInt32(Session["UserID"]);
        scheduleBE.SchedulerTime = TimeSpan.Parse(ddlSlotTime.SelectedItem.ToString());
        scheduleBE.SchedulerDate = string.IsNullOrEmpty(Convert.ToString(hdnJSDt.Value)) ? (DateTime?)null : Common.GetMM_DD_YYYY(hdnJSDt.Value);
        scheduleBE.SchedulerId = Convert.ToInt32(GetQueryStringValue("SchedulerId"));
        result= scheduleBAL.EditSchedulingInfoBAL(scheduleBE);

        if (result == 1)
        {
            ScriptManager.RegisterClientScriptBlock((Page)HttpContext.Current.CurrentHandler, this.GetType(), "Message", "alert('" + savedMessage + "')", true);
        }
       Response.Redirect("~/ModuleUI/Upload/Scheduler.aspx");
    }
}
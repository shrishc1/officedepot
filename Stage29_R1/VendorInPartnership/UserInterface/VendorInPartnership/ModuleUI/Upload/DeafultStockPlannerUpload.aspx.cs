﻿using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI;
using WebUtilities;

public partial class DeafultStockPlannerUpload : CommonPage
{
    #region ReportVariables

    private Warning[] warnings;
    private string[] streamIds;
    private string mimeType = string.Empty;
    private string encoding = string.Empty;
    private string extension = string.Empty;
    private string reportFileName = string.Empty;

    #endregion ReportVariables

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindmanualUploadGrid();
        }


        if (GetQueryStringValue("ManualFileUploadID") != null)
        {
            ShowErrorReport(GetQueryStringValue("ManualFileUploadID").ToString());
        }
    }


    private void BindmanualUploadGrid()
    {
        grdManualUpload.DataSource = null;
        UP_ManualFileUploadBE oUP_ManualFileUploadBE = new UP_ManualFileUploadBE();
        oUP_ManualFileUploadBE.Action = "GetManualUploadDataForDefaultStockPlanner";
        UP_ManualFileUploadBAL oUP_ManualFileUploadBAL = new UP_ManualFileUploadBAL();
        grdManualUpload.DataSource = oUP_ManualFileUploadBAL.GetManualMassUploadSPBAL(oUP_ManualFileUploadBE); ;
        grdManualUpload.DataBind();
        grdManualUpload.Visible = true;
    }

    public delegate void MethodInvoker(string uploadedFilePath, string strUserID);

    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        string returnError = "";
        bool isSKUFile = false;
        UP_DataImportSchedulerBE up_DataImportSchedulerBE = null;
        string strStatus = string.Empty;
        try
        {
            string DataImportRunningMessage = WebCommon.getGlobalResourceValue("DataImportRunningMessage");
            UP_DataImportSchedulerBAL oUP_DataImportSchedulerBAL = new UP_DataImportSchedulerBAL();

            // Check If DataImport is already running in Background using [UP_ImportProcessStatus] table.
            up_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
            up_DataImportSchedulerBE.Action = "CheckImportStatus";
            up_DataImportSchedulerBE.ProcessType = "M";
            strStatus = oUP_DataImportSchedulerBAL.CheckUpdateImportStatusBAL(up_DataImportSchedulerBE);
            if (strStatus.Trim().ToUpper().Equals("TRUE"))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DataImportRunningMessage + "')", true);
                return;
            }

            // Check If DataImport is already running in Background.
            DataSet dsDataImportRunningStatus = oUP_DataImportSchedulerBAL.CheckIsDataImportRunningBAL();
            if (dsDataImportRunningStatus != null && dsDataImportRunningStatus.Tables[0].Rows.Count > 0)
            {
                if (dsDataImportRunningStatus.Tables[0].Rows[0]["IsDataImportRunning"].ToString() == "True")//data import is running in background
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + DataImportRunningMessage + "')", true);
                    return;
                }
            }
            //--------------------------

            string uploadedFilePath = ConfigurationManager.AppSettings["UploadFilePath"];

            if (!System.IO.Directory.Exists(uploadedFilePath))
                System.IO.Directory.CreateDirectory(uploadedFilePath);

            uploadedFilePath = uploadedFilePath + DateTime.Now.Year.ToString();
            if (DateTime.Now.Month.ToString().Length == 1)
                uploadedFilePath = uploadedFilePath + "0" + DateTime.Now.Month.ToString();
            else
                uploadedFilePath = uploadedFilePath + DateTime.Now.Month.ToString();

            if (DateTime.Now.Date.ToString().Length == 1)
                uploadedFilePath = uploadedFilePath + "0" + DateTime.Now.Day.ToString();
            else
                uploadedFilePath = uploadedFilePath + DateTime.Now.Day.ToString();

            uploadedFilePath = uploadedFilePath + "_" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            uploadedFilePath = uploadedFilePath + "_" + fileUploadStockPlanners.FileName;

            string[] strArray = Path.GetFileNameWithoutExtension(fileUploadStockPlanners.FileName).Split('_');

            if (strArray[strArray.Length - 2].Length < 8 || strArray[strArray.Length - 1].Length < 4)
            {
                string returnDateError = WebCommon.getGlobalResourceValue("InvalidDateFormat");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + returnDateError + "')", true);
                return;
            }

            if (!validationFunctions.IsValidDateFormat(strArray[strArray.Length - 2]))
            {
                string returnDateError = WebCommon.getGlobalResourceValue("InvalidDateFormat");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + returnDateError + "')", true);
                return;
            }

            if (ImportDB.CheckToBeUploadedFileDateIsGreater(Path.GetFileNameWithoutExtension(fileUploadStockPlanners.FileName))) // Added By Hemant
            {
                //lblMessage.Text = "Manual Data Import is running in background, Please wait for few minutes....";
                fileUploadStockPlanners.SaveAs(uploadedFilePath);

                if (fileUploadStockPlanners.HasFile)
                {
                    string ext = Path.GetExtension(fileUploadStockPlanners.FileName);

                    if (ext != ".txt")
                    {
                        string ValidFileExtension = WebCommon.getGlobalResourceValue("ValidFileExtension");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ValidFileExtension + "')", true);
                        return;
                    }

                    if (fileUploadStockPlanners.FileName.ToLower().Contains("defaultplanners"))
                    {
                        ClsDefaultSPImport objBOFile = new ClsDefaultSPImport();
                        objBOFile.UploadSPImport(uploadedFilePath, fileUploadStockPlanners.FileName);
                    }

                    if (!string.IsNullOrEmpty(returnError))
                    {
                        string InvalidFileFormat = WebCommon.getGlobalResourceValue("InvalidFileFormat");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidFileFormat + "')", true);
                        return;
                    }

                    if (!isSKUFile)
                        BindmanualUploadGrid();
                }
                else
                {
                    //lblMessage.Text = "No File Uploaded.";
                }
            }
            else
            {
                string InvalidFileFormat = WebCommon.getGlobalResourceValue("UploadLatestFile");//"Cannot upload file.Please upload latest file ";//WebCommon.getGlobalResourceValue("InvalidFileFormat");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidFileFormat + "')", true);
                return;
            }
        }
        catch (Exception ex)
        {
            if (ex.Message.ToLower().Trim() == "index was outside the bounds of the array.")
            {
                string InvalidFileFormat = WebCommon.getGlobalResourceValue("UploadProperFile");//"Cannot upload file.Please upload file with proper format.";//WebCommon.getGlobalResourceValue("InvalidFileFormat");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + InvalidFileFormat + "')", true);
                return;
            }
        }
        finally
        {
            if (!isSKUFile)
                ResetStatus();
        }
    }


    protected void btnProceed_Click(object sender, EventArgs e)
    {
        mdNoShow.Hide();
    }

    private void ResetStatus()
    {
        // [UP_ImportProcessStatus] table is being reset here.
        UP_DataImportSchedulerBE up_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        UP_DataImportSchedulerBAL oUP_DataImportSchedulerBAL = new UP_DataImportSchedulerBAL();

        up_DataImportSchedulerBE.Action = "UpdateImportStatus";
        up_DataImportSchedulerBE.ProcessType = "M";
        oUP_DataImportSchedulerBAL.CheckUpdateImportStatusBAL(up_DataImportSchedulerBE);
    }

    protected string GetErrorCountLink(string ErrorCount, string ManualFileUploadID)
    {
        string labelText = string.Empty;
        if (!string.IsNullOrEmpty(ErrorCount))
        {
            if (Convert.ToInt32(ErrorCount) > 0)
            {
                labelText = "<a href=" + EncryptQuery("DeafultStockPlannerUpload.aspx?ManualFileUploadID=" + ManualFileUploadID) + ">" + ErrorCount.ToString() + "</a>";
            }
            else
            {
                labelText = ErrorCount.ToString();
            }
        }
        return labelText;
    }

    private void ShowErrorReport(string ManualFileUploadID)
    {

        int ManualFileUploadId = Convert.ToInt32(GetQueryStringValue("ManualFileUploadID"));
        UP_DataImportSchedulerBE oUP_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        UP_DataImportSchedulerBAL oUP_PurchaseOrderDetailBAL = new UP_DataImportSchedulerBAL();
        DataSet dsErrorCount = new DataSet();
        string DataTableName = string.Empty;
        string ReportName = string.Empty;

        //Error Count Details
        oUP_DataImportSchedulerBE.Action = "GetDefaultSPErrorDetail";
        oUP_DataImportSchedulerBE.ManualFileUploadID = ManualFileUploadId;
        dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
        DataTableName = "Up_ErrorSKUDefaultPlanner";
        ReportName = "\\ModuleUI\\Upload\\Report\\DefaultSPErrorDetails.rdlc";
        reportFileName = "Default Stock Planner Error Detail";

        oUP_PurchaseOrderDetailBAL = null;
        ReportDataSource rdsErrorReport = new ReportDataSource(DataTableName, dsErrorCount.Tables[0]);
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~") + ReportName;

        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(rdsErrorReport);

        byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "attachment; filename = " + reportFileName + "." + extension);
        Response.BinaryWrite(bytes);
        Response.Flush();

    }
}
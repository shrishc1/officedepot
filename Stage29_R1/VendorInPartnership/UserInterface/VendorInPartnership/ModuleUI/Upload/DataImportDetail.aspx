﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DataImportDetail.aspx.cs"
    Inherits="ModuleUI_Upload_DataImportDetail" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <script type="text/javascript">
        function chkResetScheduler() {
            if (confirm('<%=isResetSchedulerMessage1%>')) {
               if (confirm('<%=isResetSchedulerMessage2%>')) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnResetScheduler.ClientID %>').click();
                   return false;
               }
           });
        });

        //function HideLegendModalPopup() {
        //    $find("ShowLegend").hide();
        //    return false;
        //}

    </script>

    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblSchedulerDetail" runat="server" Text="Scheduler Detail"></cc1:ucLabel>
    </h2>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <table width="100%">
                <tr>
                    <td align="center" colspan="9">
                        <div class="button-row">
                            <cc1:ucButton ID="btnResetScheduler" runat="server" Text="Proceed" CssClass="button"
                                OnClick="btnResetScheduler_Click" OnClientClick="return chkResetScheduler();" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="9">
                        <cc1:ucGridView ID="grdDataImport" Width="100%" runat="server" CssClass="grid" AutoGenerateColumns="False"
                            CellPadding="0" GridLines="None" OnRowCommand="grdDataImport_RowCommand"
                            OnRowDataBound="grdDataImport_RowDataBound">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                            <Columns>

                                <asp:TemplateField HeaderText="Data Import Id" SortExpression="DataImportSchedulerID">
                                    <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblDataImportSchedulerID" runat="server" Text='<%# Eval("DataImportSchedulerID") %>'
                                            CommandArgument='<%#Eval("DataImportSchedulerID")%>'
                                            CommandName="GET_Log" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Folder Date" SortExpression="FolderName">
                                    <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbFileInformation" runat="server" Text='<%# Eval("FolderName","{0:dd/MM/yyyy}")  %>'
                                            CommandArgument='<%#Eval("DataImportSchedulerID")%>' OnCommand="lbFileInformation_Command" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:BoundField HeaderText="Start Time" DataField="StartTime" SortExpression="StartTime"
                                    DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="End Time" DataField="EndTime" SortExpression="EndTime"
                                    DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="TimeTakenInMinutes" DataField="TimeTaken" SortExpression="TimeTaken">
                                    <HeaderStyle HorizontalAlign="Center" Width="6%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="3%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblBank1" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Total Vendor Records" DataField="TotalVendorRecord" SortExpression="TotalVendorRecord">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Error Count in Vendor" SortExpression="ErrorVendorCount">
                                    <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblErrorVendorCount" runat="server" Text='<%# GetErrorCountLink(Convert.ToString(Eval("ErrorVendorCount")),Convert.ToString(Eval("FolderDownloadID")),"VEN")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="3%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblBank2" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Total SKU Records" DataField="TotalSKURecord" SortExpression="TotalSKURecord">
                                    <HeaderStyle HorizontalAlign="Center" Width="7%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Error Count in SKU" SortExpression="ErrorSKUCount">
                                    <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblErrorSKUCount" runat="server" Text='<%# GetErrorCountLink(Convert.ToString(Eval("ErrorSKUCount")),Convert.ToString(Eval("FolderDownloadID")),"SKU")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="3%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblBank3" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Total PO Records" DataField="TotalPORecord" SortExpression="TotalPORecord">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Error Count in PO" SortExpression="ErrorPOCount">
                                    <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblErrorPOCount" runat="server" Text='<%# GetErrorCountLink(Convert.ToString(Eval("ErrorPOCount")),Convert.ToString(Eval("FolderDownloadID")),"PO")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="3%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblBank4" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Total Receipt Records" DataField="TotalReceiptInformationRecord"
                                    SortExpression="TotalReceiptInformationRecord">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Error Count in Receipt" SortExpression="ErrorReceiptCount">
                                    <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblErrorReceiptCount" runat="server" Text='<%# GetErrorCountLink(Convert.ToString(Eval("ErrorReceiptCount")),Convert.ToString(Eval("FolderDownloadID")),"RI")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="3%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblBank5" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:BoundField HeaderText="Total Expedite Records" DataField="TotalExpediteRecord"
                                    SortExpression="TotalExpediteRecord">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Error Count in Expedite" SortExpression="ErrorExpediteCount">
                                    <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblErrorExpediteCount" runat="server" Text='<%# GetErrorCountLink(Convert.ToString(Eval("ErrorExpediteCount")),Convert.ToString(Eval("FolderDownloadID")),"EX")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Error Count in BO" SortExpression="ErrorBOCount">
                                    <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblErrorBOCount" runat="server" Text='<%# GetErrorCountLink(Convert.ToString(Eval("ErrorBOCount")),Convert.ToString(Eval("FolderDownloadID")),"BO")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="3%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblBank6" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Error Message" SortExpression="ErrorMessage">
                                    <HeaderStyle Width="3%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblErrorMessage" runat="server" Text='Error Message'
                                            CommandArgument='<%#Eval("DataImportSchedulerID")%>'
                                            CommandName="GET_ERROR" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Invoked By" DataField="InvokedBy" SortExpression="InvokedBy">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Button OnCommand="btnReRun_Click" ID="btnReRun" runat="server" Text="Re-Run"
                                            CssClass="button" Visible="false" CommandArgument='<%# Eval("FolderName") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
                        </cc1:ucGridView>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%;">
                        <br />
                        <br />
                        <br />
                        <br />
                        <cc1:ucLabel runat="server" Text="" ID="lblFileProcessed" Font-Bold="true" />
                        <cc1:ucGridView ID="gvFileProcessedDetail" runat="server" CssClass="grid" AutoGenerateColumns="False"
                            CellPadding="0" GridLines="None" Visible="false">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="Folder Date" SortExpression="FolderName">
                                    <HeaderStyle Width="30%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hpFileName" runat="server" Text='<%# Eval("FileName")%>' NavigateUrl='<%# Eval("LogFilePath")%>'></asp:HyperLink>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Total Record" DataField="NoOfRecordProcessed" SortExpression="NoOfRecordProcessed">
                                    <HeaderStyle HorizontalAlign="right" Width="30%" />
                                    <ItemStyle HorizontalAlign="right" />
                                </asp:BoundField>
                            </Columns>
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
                        </cc1:ucGridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="updNoShow" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnNoShows" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdNoShow" runat="server" TargetControlID="btnNoShows"
                PopupControlID="pnlbtnNoShow" BackgroundCssClass="modalBackground" BehaviorID="MsgNoShow"
                DropShadow="false" />
            <asp:Panel ID="pnlbtnNoShow" runat="server" Style="display: none;">
                <div style="overflow-y: scroll; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: center;max-height:400px">
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold;text-align:left">
                                <cc1:ucLiteral ID="ltNoShow" runat="server" Text=""></cc1:ucLiteral>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnOk"   runat="server" Text="OK" OnClick="btnOk_Click"
                                    CssClass="button" />

                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Visible="false">
    </rsweb:ReportViewer>

</asp:Content>

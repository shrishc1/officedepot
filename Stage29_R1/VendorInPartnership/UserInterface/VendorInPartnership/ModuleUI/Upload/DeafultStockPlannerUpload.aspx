﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"    
    AutoEventWireup="true" CodeFile="DeafultStockPlannerUpload.aspx.cs" Inherits="DeafultStockPlannerUpload" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        //        function showModelBox(show) {
        //            if (show == 'Y')
        //                document.getElementById('<%=pnlModelBox.ClientID %>').style.display = 'block';
        //            else
        //                document.getElementById('<%=pnlModelBox.ClientID %>').style.display = 'none';
        //        }

    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblMassUploadDefaultStockPlanner" runat="server" Text="Mass Upload Default StockPlanner"></cc1:ucLabel>
    </h2>
    <table width="100%" border="0">
        <tr>
            <td style="width:50%;text-align:left;">
                <asp:FileUpload ID="fileUploadStockPlanners" runat="server" Width="380px" />
            </td>
            <td style="width:50%;text-align:left;">
                <cc1:ucButton ID="UploadBtn" Text="Upload File" OnClick="UploadBtn_Click" runat="server"
                    Width="105px" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                -----------------------------------------------------------------------------------------------------------------
            </td>
        </tr>       
    </table>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <table width="100%">
                <tr>
                    <td align="center" colspan="9">

                        <cc1:ucGridView ID="grdManualUpload" Width="100%" runat="server" CssClass="grid"
                            AutoGenerateColumns="False" CellPadding="0" GridLines="None">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                            <Columns>
                               <asp:BoundField HeaderText="Date Uploaded" DataField="UploadDate" SortExpression="UploadDate"
                                    DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>

                                <asp:BoundField HeaderText="User Name" DataField="UploadedBy" SortExpression="UploadedBy">
                                    <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>

                                <asp:BoundField HeaderText="Downloaded Filename" DataField="FileName" SortExpression="FileName">
                                    <HeaderStyle HorizontalAlign="Left" Width="6%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                               
                                <asp:BoundField HeaderText="Total Records" DataField="TotalRecord" SortExpression="TotalRecord">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>

                                <asp:TemplateField HeaderText="Error Count" SortExpression="ErrorRecord">
                                    <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblErrorVendorCount" runat="server" Text='<%# GetErrorCountLink(Convert.ToString(Eval("ErrorRecord")),Convert.ToString(Eval("MasDefaultUploadID")))%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
                        </cc1:ucGridView>

                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="pnlModelBox" runat="server" visible="false">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="center">
                    <div align="center" style="background-color: #000; top: 0px; left: 0px; bottom: 0px;
                        right: 0px; padding-top: 20%; margin: 0; width: 100%; height: 100%; overflow: hidden;
                        position: absolute; z-index: 1000; filter: alpha(opacity=50); opacity: 0.5;">
                        <asp:Image ID="imgWait" runat="server" ImageUrl="~/Images/Ajaxloading.gif" ImageAlign="Middle" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Visible="false">
    </rsweb:ReportViewer>

    <asp:UpdatePanel ID="updNoShow" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnNoShows" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdNoShow" runat="server" TargetControlID="btnNoShows"
                PopupControlID="pnlbtnNoShow" BackgroundCssClass="modalBackground" BehaviorID="MsgNoShow"
                DropShadow="false" />
            <asp:Panel ID="pnlbtnNoShow" runat="server" Style="display: none;">
                <div style="overflow-y: hidden; overflow-x: hidden; background-color: #fff; padding: 5px;
                    border: 2px solid #ccc; text-align: left;">
                    <table cellspacing="5" cellpadding="0" width="400px;" border="0" align="center" class="top-setting-Popup">
                        <tr>
                            <td style="font-weight: bold; text-align: center;">
                                <cc1:ucLabel ID="ltError" Text="Some error occurred in SKU import." runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <cc1:ucButton ID="btnOK" runat="server" Text="OK" OnCommand="btnProceed_Click" CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>       
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOK" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
  
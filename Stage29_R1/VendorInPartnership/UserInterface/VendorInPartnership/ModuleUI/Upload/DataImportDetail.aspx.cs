﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System.Web.Services;
using Utilities;
using WebUtilities;
using System.IO;
using System.Configuration;
using System.Data;
using Microsoft.Reporting.WebForms;
using BusinessEntities.ModuleBE.AdminFunctions;

public partial class ModuleUI_Upload_DataImportDetail : CommonPage
{
    protected string isResetSchedulerMessage1 = WebCommon.getGlobalResourceValue("IsResetSchedulerMessage1");
    protected string isResetSchedulerMessage2 = WebCommon.getGlobalResourceValue("IsResetSchedulerMessage2");
    protected string isSchedulerReset = WebCommon.getGlobalResourceValue("IsSchedulerReset");
    

    #region ReportVariables
    Warning[] warnings;
    string[] streamIds;
    string mimeType = string.Empty;
    string encoding = string.Empty;
    string extension = string.Empty;
    string reportFileName = string.Empty;
    #endregion

    string Datefrom, DateTo = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        string date = DateTime.Now.ToString("yyyyMMdd");

        if (!IsPostBack)
        {
            Datefrom = DateTime.Now.AddDays(Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DataImportBackDay"])).ToString("dd/MM/yyyy");
            DateTo = DateTime.Now.ToString("dd/MM/yyyy");
            BindGrid();
        }

        if (GetQueryStringValue("RecordInfo") != null && GetQueryStringValue("FolderDownloadID") != null)
        {
            ShowErrorReport(GetQueryStringValue("RecordInfo").ToString(), GetQueryStringValue("FolderDownloadID").ToString());
        }
    }

    private void ShowErrorReport(string RecordInfo, string FolderDownloadID)
    {

        int FolderDownloadId = Convert.ToInt32(GetQueryStringValue("FolderDownloadID"));
        UP_DataImportSchedulerBE oUP_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        UP_DataImportSchedulerBAL oUP_PurchaseOrderDetailBAL = new UP_DataImportSchedulerBAL();
        DataSet dsErrorCount = new DataSet();
        string DataTableName = string.Empty;
        string ReportName = string.Empty;
        if (RecordInfo == "VEN")
        {

            oUP_DataImportSchedulerBE.Action = "GetVendorErrorDetail";
            oUP_DataImportSchedulerBE.FolderDownloadID = FolderDownloadId;
            dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
            DataTableName = "UP_ErrorVendor";
            ReportName = "\\ModuleUI\\Upload\\Report\\RptVendorErrorDetail.rdlc";
            reportFileName = "Vendor Error Detail";
        }
        else if (RecordInfo == "PO")
        {

            oUP_DataImportSchedulerBE.Action = "GetPOErrorDetail";
            oUP_DataImportSchedulerBE.FolderDownloadID = FolderDownloadId;
            dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
            DataTableName = "UP_ErrorPurchaseOrderDetails";
            ReportName = "\\ModuleUI\\Upload\\Report\\RptPOErrorDetail.rdlc";
            reportFileName = "Purchase Order Error Detail";
        }
        else if (RecordInfo == "RI")
        {

            oUP_DataImportSchedulerBE.Action = "GetReceiptErrorDetail";
            oUP_DataImportSchedulerBE.FolderDownloadID = FolderDownloadId;
            dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
            DataTableName = "UP_ErrorReceiptInformation";
            ReportName = "\\ModuleUI\\Upload\\Report\\RptRIErrorDetail.rdlc";
            reportFileName = "Receipt Information Error Detail";
        }
        else if (RecordInfo == "SKU")
        {

            oUP_DataImportSchedulerBE.Action = "GetSKUErrorDetail";
            oUP_DataImportSchedulerBE.FolderDownloadID = FolderDownloadId;
            dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
            DataTableName = "UP_ErrorSKU";
            ReportName = "\\ModuleUI\\Upload\\Report\\RptSKUErrorDetail.rdlc";
            reportFileName = "SKU Error Detail";
        }
        else if (RecordInfo == "EX") {

            oUP_DataImportSchedulerBE.Action = "GetExpediteErrorDetail";
            oUP_DataImportSchedulerBE.FolderDownloadID = FolderDownloadId;
            dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
            DataTableName = "DataSet1";
            ReportName = "\\ModuleUI\\Upload\\Report\\RptExpediteSKUErrorDetail.rdlc";
            reportFileName = "Expedite SKU Error Detail";
        }
        else if (RecordInfo == "BO")
        {

            oUP_DataImportSchedulerBE.Action = "GetBOErrorDetail";
            oUP_DataImportSchedulerBE.FolderDownloadID = FolderDownloadId;
            dsErrorCount = oUP_PurchaseOrderDetailBAL.GetErrorDetailBAL(oUP_DataImportSchedulerBE);
            DataTableName = "DataSet1";
            ReportName = "\\ModuleUI\\Upload\\Report\\RptBOErrorDetail.rdlc";
            reportFileName = "BO Error Detail";
        }

        oUP_PurchaseOrderDetailBAL = null;
        ReportDataSource rdsErrorReport = new ReportDataSource(DataTableName, dsErrorCount.Tables[0]);
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~") + ReportName;

        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(rdsErrorReport);

        byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "attachment; filename = " + reportFileName + "." + extension);
        Response.BinaryWrite(bytes);
        Response.Flush();

    }

    protected void lbFileInformation_Command(object sender, CommandEventArgs e)
    {
        UP_DataImportSchedulerBE oUP_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        UP_DataImportSchedulerBAL oUP_PurchaseOrderDetailBAL = new UP_DataImportSchedulerBAL();

        oUP_DataImportSchedulerBE.DataImportSchedulerID = Convert.ToInt32(e.CommandArgument);
        oUP_DataImportSchedulerBE.Action = "GetFileProcessedData";
        List<UP_DataImportSchedulerBE> lstFileProcessed = oUP_PurchaseOrderDetailBAL.GetGetFileProcessedDataBAL(oUP_DataImportSchedulerBE);
        oUP_PurchaseOrderDetailBAL = null;
        gvFileProcessedDetail.Visible = true;
        gvFileProcessedDetail.DataSource = lstFileProcessed;
        gvFileProcessedDetail.DataBind();
        if (lstFileProcessed.Count > 0)
            lblFileProcessed.Text = string.Format("Folder Downloaded {0} ", lstFileProcessed[0].FolderName);

    }
 
    #region Methods

    protected void BindGrid()
    {
        UP_DataImportSchedulerBE oUP_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        UP_DataImportSchedulerBAL oUP_PurchaseOrderDetailBAL = new UP_DataImportSchedulerBAL();
        List<UP_DataImportSchedulerBE> lstUP_DataImportScheduler = new List<UP_DataImportSchedulerBE>();
        oUP_DataImportSchedulerBE.Action = "GetDataImportScheduler";
        oUP_DataImportSchedulerBE.DateFrom = string.IsNullOrEmpty(Datefrom) ? (DateTime?)null : Common.GetMM_DD_YYYY(Datefrom);
        oUP_DataImportSchedulerBE.DateTo = string.IsNullOrEmpty(DateTo) ? (DateTime?)null : Common.GetMM_DD_YYYY(DateTo);
        lstUP_DataImportScheduler = oUP_PurchaseOrderDetailBAL.GetDataImportDetailBAL(oUP_DataImportSchedulerBE);
        oUP_PurchaseOrderDetailBAL = null;
        grdDataImport.DataSource = lstUP_DataImportScheduler;
        grdDataImport.DataBind();
        ViewState["lstDataImport"] = lstUP_DataImportScheduler;
    }

    //public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    //{
    //    return Utilities.GenericListHelper<UP_DataImportSchedulerBE>.SortList((List<UP_DataImportSchedulerBE>)ViewState["lstDataImport"], e.SortExpression, e.SortDirection).ToArray();
    //}

    #endregion

    protected void btnReRun_Click(object sender, CommandEventArgs e)
    {
        string ProcessName = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["DataImportSchedulerExe"]);
        string arguments = e.CommandArgument.ToString();
        string folderDate = arguments;
        Startprocess(folderDate, ProcessName);

        Datefrom = DateTime.Now.AddDays(Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DataImportBackDay"])).ToString("dd/MM/yyyy");
        DateTo = DateTime.Now.ToString("dd/MM/yyyy");
        BindGrid();
    }

    public void Startprocess(string ProcessArgs, string ProcessName)
    {
        try
        {
            System.Diagnostics.Process Process = new System.Diagnostics.Process();
            Process.StartInfo.FileName = ProcessName;
            Process.StartInfo.Arguments = ProcessArgs;
            Process.Start();
            Process.WaitForExit();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    protected string GetErrorCountLink(string ErrorCount, string FolderDownloadID, string RecordInfo)
    {
        string labelText = string.Empty;
        if (Convert.ToInt32(ErrorCount) > 0)
        {
            labelText = "<a href=" + EncryptQuery("DataImportDetail.aspx?RecordInfo=" + RecordInfo + "&FolderDownloadID=" + FolderDownloadID) + ">" + ErrorCount.ToString() + "</a>";
        }
        else
        {
            labelText = ErrorCount.ToString();
        }
        return labelText;
    }

    protected void grdDataImport_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "GET_ERROR")
        {
            UP_DataImportSchedulerBE oUP_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
            UP_DataImportSchedulerBAL oUP_PurchaseOrderDetailBAL = new UP_DataImportSchedulerBAL();

            oUP_DataImportSchedulerBE.DataImportSchedulerID = Convert.ToInt32(e.CommandArgument);
            oUP_DataImportSchedulerBE.Action = "GetErrorMessage";
            List<UP_DataImportSchedulerBE> lstFileProcessed = oUP_PurchaseOrderDetailBAL.GetErrorMessage(oUP_DataImportSchedulerBE);
            var ErrorMessage = string.Empty;
            if (lstFileProcessed != null && lstFileProcessed.Count > 0)
            {
                ErrorMessage = lstFileProcessed[0].ErrorMessage;
                ltNoShow.Text = ErrorMessage;
                mdNoShow.Show();
            }
        }

        if (e.CommandName == "GET_Log")
        {
            DataImportLogBE oUP_DataImportSchedulerBE = new DataImportLogBE();
            UP_DataImportSchedulerBAL oUP_PurchaseOrderDetailBAL = new UP_DataImportSchedulerBAL();
            oUP_DataImportSchedulerBE.DataImportSchedulerID = Convert.ToInt32(e.CommandArgument);
            var lstFileProcessed = oUP_PurchaseOrderDetailBAL.GetImportDataLogBAL(oUP_DataImportSchedulerBE);      
            if (lstFileProcessed != null )
            {
                ltNoShow.Text =!string.IsNullOrEmpty(lstFileProcessed.TaskRunning)? lstFileProcessed.TaskRunning.ToString() :"No log available.";
                mdNoShow.Show();
            }
        }
    }

    protected void grdDataImport_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton link = ((LinkButton)e.Row.FindControl("lblErrorMessage"));

            UP_DataImportSchedulerBE oUP_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
            UP_DataImportSchedulerBAL oUP_PurchaseOrderDetailBAL = new UP_DataImportSchedulerBAL();

            LinkButton linkButton= ((LinkButton)e.Row.FindControl("lblDataImportSchedulerID"));

            oUP_DataImportSchedulerBE.DataImportSchedulerID = Convert.ToInt32(linkButton.Text);
            oUP_DataImportSchedulerBE.Action = "GetErrorMessage";
            List<UP_DataImportSchedulerBE> lstFileProcessed = oUP_PurchaseOrderDetailBAL.GetErrorMessage(oUP_DataImportSchedulerBE);

            var ErrorMessage = string.Empty;
            if (lstFileProcessed != null && lstFileProcessed.Count > 0)
            {
                ErrorMessage = lstFileProcessed[0].ErrorMessage;
            }

            if (string.IsNullOrEmpty(ErrorMessage))
                link.Text = string.Empty;
        }
    }

    protected void btnResetScheduler_Click(object sender, EventArgs e)
    {
        UP_DataImportSchedulerBE oUP_DataImportSchedulerBE = new UP_DataImportSchedulerBE();
        UP_DataImportSchedulerBAL oUP_PurchaseOrderDetailBAL = new UP_DataImportSchedulerBAL();
        oUP_DataImportSchedulerBE.Action = "DeleteImportStatus";
        oUP_PurchaseOrderDetailBAL.DeleteImportStatusBAL(oUP_DataImportSchedulerBE);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "<script>alert('" + isSchedulerReset + "');</script>", false);
        
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        mdNoShow.Hide();
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SchedulerEdit.aspx.cs" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
Inherits="ModuleUI_Upload_SchedulerEdit" %>


<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <script type="text/javascript" language="javascript">
 
      function setValue(target) {
         document.getElementById('<%=hdnJSDt.ClientID %>').value = target.value;
     }

     $(document).ready(function () {
         $('#txtFromDate').datepicker({ showOtherMonths: true,
             selectOtherMonths: true, changeMonth: true, changeYear: true,
             minDate: '0',
             yearRange: '2013:+100',
             dateFormat: 'dd/mm/yy'
         });
     });
   
 </script>

  <asp:HiddenField ID="hdnJSDt" runat="server" />
 <h2>
    <asp:HiddenField ID="hdnJSFromDt" runat="server" />
        <cc1:ucLabel ID="lblEditOTIFScorecardRescheduler" Text="Edit OTIF / Scorecard Rescheduler"
            runat="server"></cc1:ucLabel>
    </h2>
    <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
        <tr>
            <td style="font-weight: bold; width: 15%;">
                <cc1:ucLabel ID="lblSelectWhichJobToRun" Text="Select which job to run" runat="server"></cc1:ucLabel>:
            </td>
            <td align="left" class="nobold radiobuttonlist" style="font-weight: bold; width: 60%;">
                <cc1:ucRadioButton ID="rbtnOTIF" runat="server" Text="OTIF" GroupName="job" Enabled="false" />
                <cc1:ucRadioButton ID="rbtnVSC" runat="server" Text="Vendor Score Card" GroupName="job" Enabled="false" />
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblSelectMonthYear" Text="Select month/year" runat="server"></cc1:ucLabel>:
            </td>
            <td style="font-weight: bold;">
                <cc1:ucDropdownList ID="ddlDate" runat="server" Width="100px">
                </cc1:ucDropdownList>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblSelectRunDate" runat="server" Text="Select run date"></cc1:ucLabel>:
            </td>
            <td style="font-weight: bold;">
                <cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue(this)"
                    ReadOnly="True" Width="70px" />
                <%--<cc1:ucTextbox ID="txtFromDate" ClientIDMode="Static" runat="server" onchange="setValue1(this)"
                    ReadOnly="True" Width="70px" />--%>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;">
                <cc1:ucLabel ID="lblSelectRunTime" runat="server" Text="Select run time"></cc1:ucLabel>:
            </td>
            <td style="font-weight: bold;">
                <cc1:ucDropdownList ID="ddlSlotTime" runat="server" Width="60px">
                </cc1:ucDropdownList>
                <cc1:ucLabel ID="lblHHMM1" runat="server"></cc1:ucLabel>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td style="font-weight: bold; width: 70%;">
                <cc1:ucLabel ID="lblSchedulerNoticeMessage" runat="server" Text="Time Slot" isRequired="true"></cc1:ucLabel>
            </td>
        </tr>
         </table>
         
                <div class="button-row">
                    <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
                    <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" 
                        onclick="btnSave_Click" />
                </div>
          


</asp:Content>
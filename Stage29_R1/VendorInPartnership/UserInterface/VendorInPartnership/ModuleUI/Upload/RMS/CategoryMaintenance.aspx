﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" CodeFile="CategoryMaintenance.aspx.cs" Inherits="ModuleUI_Upload_RMS_CategoryMaintainance" %>


<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectRMSDepartmentSearch.ascx" TagName="MultiSelectRMSDepartmentSearch1"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript"> 
       function RequiredCategory() {
         if (document.getElementById("<%= ddlCategory.ClientID%>").value == "0")  {
                alert('<%= CategoryName%>');
                return false;
            }
        }
    </script>
      <h2>      
        <cc1:ucLabel ID="lblCategoryMaintenanceSetup" runat="server"></cc1:ucLabel>
    </h2>
      <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
   
    <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
        <tr>
        <td style="font-weight: bold; width: 10%" ><cc1:ucLabel ID="lblRMSDepartmentSetUp" runat="server"></cc1:ucLabel> </td> <td style="font-weight: bold;width: 2%">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
            <td>  <uc1:MultiSelectRMSDepartmentSearch1 ID="ucRmsdepartment" runat="server"></uc1:MultiSelectRMSDepartmentSearch1></td>
        </tr>
         <tr>
         <td style="font-weight: bold;width: 10%">       <cc1:ucLabel ID="lblAssignedCategory" runat="server"></cc1:ucLabel></td>
          <td style="font-weight: bold;width: 2%">
                            <cc1:ucLabel ID="UcLabel1" runat="server">:</cc1:ucLabel>
                        </td>
            <td>  <cc1:ucDropdownList ID="ddlCategory" runat="server" style="width:150px;">                  
                  </cc1:ucDropdownList></td>
        </tr>
    </table>
 
           <div class="button-row">
        <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"  OnClientClick="return RequiredCategory();"/>
        <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" PostBackUrl="CategoryDepartmentOverview.aspx" />
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
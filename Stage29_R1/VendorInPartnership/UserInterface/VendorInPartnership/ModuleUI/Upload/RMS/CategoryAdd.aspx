﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="CategoryAdd.aspx.cs" Inherits="ModuleUI_Upload_RMS_CategoryAdd" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function RequiredCategory() {
            if (document.getElementById("<%= txtCategoryName.ClientID%>").value == "" || document.getElementById("<%= txtCategoryName.ClientID%>").value == null) {
                alert('<%= CategoryName%>');
                return false;
            }
        }
    </script>

    <asp:ScriptManager ID="sn" runat="server">
    </asp:ScriptManager>

    <h2>
        <cc1:ucLabel ID="lblNewCategory" runat="server"></cc1:ucLabel>
    </h2>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />

            <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                <tr>
                    <td>
                        <table width="90%" cellspacing="5" cellpadding="0" class="form-table">
                            <tr>
                                <td style="font-weight: bold; width: 30%">
                                    <cc1:ucLabel ID="lblCategoryNameTitle" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 5%">
                                    <cc1:ucLabel ID="UcLabel3" runat="server">:</cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold; width: 65%">
                                    <cc1:ucTextbox ID="txtCategoryName" runat="server" MaxLength="100" Width="203px"></cc1:ucTextbox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <div class="button-row">
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" OnClientClick="return RequiredCategory();" />
                <cc1:ucButton ID="btnUpdate" runat="server" Visible="false" CssClass="button"
                    OnClientClick="return RequiredCategory();" OnClick="btnUpdate_Click" />
                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" PostBackUrl="CategoryOverview.aspx" />
            </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
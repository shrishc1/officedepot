﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using DataAccessLayer.ModuleDAL.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;

public partial class ModuleUI_Upload_RMS_CategoryOverview : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindSiteDoorNoSetup();
        }
        btnExportToExcel1.GridViewControl = UcGridView1;
        btnExportToExcel1.FileName = "CategoryOverview";
    }

    #region Methods
     
    
    protected void BindSiteDoorNoSetup()
    {
        MAS_RMSCategoryBE oMAS_RMSCategoryBE = new MAS_RMSCategoryBE();

        MAS_RMSCategoryBAL oMAS_RMSCategoryBAL = new MAS_RMSCategoryBAL();

        oMAS_RMSCategoryBE.Action = "ShowAll";
        oMAS_RMSCategoryBE.CreatedBy = Convert.ToInt32(Session["UserID"]);

        List<MAS_RMSCategoryBE> lstCategory = oMAS_RMSCategoryBAL.GetRMSCategoryBAL(oMAS_RMSCategoryBE);

        if (lstCategory.Count > 0)
        {
            UcGridView1.DataSource = lstCategory;
            UcGridView1.DataBind();
            ViewState["lstCategorys"] = lstCategory;
        }

    }
    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MAS_RMSCategoryBE>.SortList((List<MAS_RMSCategoryBE>)ViewState["lstCategorys"], e.SortExpression, e.SortDirection).ToArray();
    }
    #endregion
}
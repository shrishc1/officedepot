﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using WebUtilities;
using DataAccessLayer.ModuleDAL.Upload;
using System.Reflection;
using BaseControlLibrary;

public partial class ModuleUI_Upload_RMS_CategoryMaintainance : CommonPage
{
    public string CategoryName = WebCommon.getGlobalResourceValue("CategoryName");
    UP_RMSDepartmentBE oUP_RMSDepartmentBE   ;
    UP_RMSDepartmentBAL oUP_RMSDepartmentBAL ;
    MAS_RMSCategoryBE oMAS_RMSCategoryBE     ;
    List<UP_RMSDepartmentBE> lstRMSDepartmentByCategory;
    MAS_RMSCategoryBAL oMAS_RMSCategoryBAL;
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRMSCategoryDepartment();
            if (GetQueryStringValue("RMSCategoryID") != null)
            {
                ddlCategory.SelectedValue = GetQueryStringValue("RMSCategoryID");
                ddlCategory.Enabled = false;
                UP_RMSDepartmentBE oUP_RMSDepartmentBE = new UP_RMSDepartmentBE();
                UP_RMSDepartmentBAL oUP_RMSDepartmentBAL = new UP_RMSDepartmentBAL();
                MAS_RMSCategoryBE oMAS_RMSCategoryBE = new MAS_RMSCategoryBE();
                oUP_RMSDepartmentBE.Action = "GetRmsDepartmentBycategoryID";
                oMAS_RMSCategoryBE.RMSCategoryID = Convert.ToInt32(GetQueryStringValue("RMSCategoryID"));
                oUP_RMSDepartmentBE.RMSCategory = oMAS_RMSCategoryBE;
                lstRMSDepartmentByCategory = new List<UP_RMSDepartmentBE>();
                ucListBox lstright = (ucListBox)ucRmsdepartment.FindControl("lstRight");
                lstRMSDepartmentByCategory = oUP_RMSDepartmentBAL.GetRMSDepartmentByCategoryIDBAL(oUP_RMSDepartmentBE);
                FillControls.FillListBox(ref lstright, lstRMSDepartmentByCategory, "Dept_Name", "Dept_No");
                if (lstRMSDepartmentByCategory != null)
                {                        
                    string RmsDepartmentID = string.Empty;
                    string RmsDepartmentName = string.Empty;
                    string s = string.Empty;
                    for (int i = 0; i <= lstRMSDepartmentByCategory.Count-1; i++)
                    {
                            //RmsDepartmentID += lstRMSDepartmentByCategory.Where(p => p.RMSDepartmentID == i).Select(p => p.Dept_No).FirstOrDefault().ToString() + ",";   
                        RmsDepartmentID += lstRMSDepartmentByCategory[i].Dept_No.ToString() + ",";  
                        //RmsDepartmentName+=lstRMSDepartmentByCategory.Where(p=>p.RMSDepartmentID==i).Select(p => p.Dept_Name).FirstOrDefault().ToString()+ ",";                    
                        RmsDepartmentName += lstRMSDepartmentByCategory[i].Dept_Name.ToString()+",";
                    }
                    ucRmsdepartment.SelectedRMSDepartmentIDs=RmsDepartmentID;
                    ucRmsdepartment.SelectedRMSDepartmentName = RmsDepartmentName;
                }
            }
        }
       
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (ucRmsdepartment.SelectedRMSDepartmentIDs != null)
        {
            oUP_RMSDepartmentBE = new UP_RMSDepartmentBE();
            oUP_RMSDepartmentBAL = new UP_RMSDepartmentBAL();
            oMAS_RMSCategoryBE = new MAS_RMSCategoryBE();

            oUP_RMSDepartmentBE.Action = "UpdateCategoryWiseRMSDepartment";
            oUP_RMSDepartmentBE.RMSDepartmentByIDs = ucRmsdepartment.SelectedRMSDepartmentIDs;

            //string UnSelectedDeptIDs = string.Empty;
            //ListBox lstLeft = (ListBox)ucRmsdepartment.FindControl("lstLeft");
            //foreach (ListItem item in lstLeft.Items)
            //{
            //    UnSelectedDeptIDs += item.Value + ",";
            //}
            //oUP_RMSDepartmentBE.RMSDepartmentByLeftIDs = UnSelectedDeptIDs.Trim(',');
            oMAS_RMSCategoryBE.RMSCategoryID = Convert.ToInt32(ddlCategory.SelectedValue);
            oMAS_RMSCategoryBE.CreatedBy = Convert.ToInt32(Session["UserID"]);
            oUP_RMSDepartmentBE.RMSCategory = oMAS_RMSCategoryBE;
            if (oUP_RMSDepartmentBAL.UpdateRMSDepartmentBAL(oUP_RMSDepartmentBE) > 0)
            {
                Response.Redirect("CategoryDepartmentOverview.aspx");
            }            
        }
        else
        {
            string saveMessage = WebCommon.getGlobalResourceValue("RmsDepartmentCheck");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            ucRmsdepartment.SetRMSDepartmentOnPostBack();
        }
    }

    private void BindRMSCategoryDepartment()
    {
         oMAS_RMSCategoryBE = new MAS_RMSCategoryBE();
         oMAS_RMSCategoryBAL = new MAS_RMSCategoryBAL();

        oMAS_RMSCategoryBE.Action = "GetRmsCategory";

        List<MAS_RMSCategoryBE> lstRMSCategory = new List<MAS_RMSCategoryBE>();
        lstRMSCategory = oMAS_RMSCategoryBAL.GetRMSAllCategoryBAL(oMAS_RMSCategoryBE);

        if (lstRMSCategory.Count > 0)            
            FillControls.FillDropDown(ref ddlCategory, lstRMSCategory, "CategoryName", "RMSCategoryID", "Select Category");
    }

}
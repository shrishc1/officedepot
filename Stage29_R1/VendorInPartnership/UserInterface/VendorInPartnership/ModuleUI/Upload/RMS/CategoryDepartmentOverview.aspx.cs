﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using WebUtilities;

public partial class ModuleUI_Upload_RMS_CategoryDepartmentOverview : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pager1.PageSize = 50;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            pager1.Visible = false;
        }
        btnExportToExcel1.GridViewControl = UcGridView1;
        btnExportToExcel1.FileName = "CategoryDepartmentOverview";
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (ucRmsCategory.SelectedRMSCategoryIDs == null)
        {            
            string saveMessage = WebCommon.getGlobalResourceValue("CategoryName");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
            ucRmsCategory.SetRMSCategoryOnPostBack();
        }
        else
        {
            BindGridview();
        }

    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindGridview(currnetPageIndx);
    }

    public void BindGridview(int Page = 1)
    {
        MAS_RMSCategoryBE oMAS_RMSCategoryBE = new MAS_RMSCategoryBE();
        MAS_RMSCategoryBAL oMAS_RMSCategoryBAL = new MAS_RMSCategoryBAL();
        oMAS_RMSCategoryBE.Action = "ShowCategoryMaintenance";
        oMAS_RMSCategoryBE.RMSCategoryIDs = ucRmsCategory.SelectedRMSCategoryIDs;
        oMAS_RMSCategoryBE.PageCount = Page;
        List<UP_RMSBE> lstCategoryMaintenance=null;
        int RecordCount=0;
        oMAS_RMSCategoryBAL.GetRMSCategoryDepartmentBAL(oMAS_RMSCategoryBE,out lstCategoryMaintenance,out RecordCount);

        if (lstCategoryMaintenance.Count > 0)
        {
            pager1.ItemCount = RecordCount;
            pager1.Visible = true;
            UcGridView1.DataSource = lstCategoryMaintenance;
            UcGridView1.DataBind();
            ViewState["lstCategoryMaintenance"] = lstCategoryMaintenance;
        }
        else
        {
            UcGridView1.DataSource = null;
            UcGridView1.DataBind();
        }
        ucRmsCategory.SetRMSCategoryOnPostBack();
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<UP_RMSBE>.SortList((List<UP_RMSBE>)ViewState["lstCategoryMaintenance"], e.SortExpression, e.SortDirection).ToArray();
    }
}
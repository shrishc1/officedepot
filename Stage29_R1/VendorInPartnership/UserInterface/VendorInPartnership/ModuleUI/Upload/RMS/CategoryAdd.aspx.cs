﻿using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System;
using System.Web.UI;
using WebUtilities;

public partial class ModuleUI_Upload_RMS_CategoryAdd : CommonPage
{
    private string categoryName = WebCommon.getGlobalResourceValue("CategoryName");
    private string categoryNameExists = WebCommon.getGlobalResourceValue("CategoryNameExists");

    public string CategoryName
    {
        get
        {
            return categoryName;
        }
        set
        {
            categoryName = value;
        }
    }

    public string CategoryNameExists
    {
        get
        {
            return categoryNameExists;
        }
        set
        {
            categoryNameExists = value;
        }
    }

    MAS_RMSCategoryBE oMAS_RMSCategoryBE;
    MAS_RMSCategoryBAL oMAS_RMSCategoryBAL;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetQueryStringValue("RMSCategoryID") != null)
            {
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                BindCategoryName();
            }
            else
            {
                btnSave.Visible = true;
                btnUpdate.Visible = false;
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        oMAS_RMSCategoryBE = new MAS_RMSCategoryBE();
        oMAS_RMSCategoryBAL = new MAS_RMSCategoryBAL();
        oMAS_RMSCategoryBE.Action = "Add";
        oMAS_RMSCategoryBE.CategoryName = txtCategoryName.Text;
        oMAS_RMSCategoryBE.CreatedBy = Convert.ToInt32(Session["UserID"]);
        if (oMAS_RMSCategoryBAL.AddRMSCategoryBAL(oMAS_RMSCategoryBE) == 1)
        {
            Response.Redirect("CategoryOverview.aspx");
        }
        else
        {
            string saveMessage = WebCommon.getGlobalResourceValue("CategoryNameExists");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }

    }
    public void BindCategoryName()
    {
        oMAS_RMSCategoryBE = new MAS_RMSCategoryBE();
        oMAS_RMSCategoryBAL = new MAS_RMSCategoryBAL();
        oMAS_RMSCategoryBE.Action = "GetCategoryById";
        oMAS_RMSCategoryBE.RMSCategoryID = Convert.ToInt32(GetQueryStringValue("RMSCategoryID"));
        txtCategoryName.Text = oMAS_RMSCategoryBAL.GetCategoryByIdBAL(oMAS_RMSCategoryBE);
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        oMAS_RMSCategoryBE = new MAS_RMSCategoryBE();
        oMAS_RMSCategoryBAL = new MAS_RMSCategoryBAL();
        oMAS_RMSCategoryBE.Action = "UpdateCategoryByID";
        oMAS_RMSCategoryBE.CategoryName = txtCategoryName.Text;
        oMAS_RMSCategoryBE.RMSCategoryID = Convert.ToInt32(GetQueryStringValue("RMSCategoryID"));
        if (oMAS_RMSCategoryBAL.UpdateRMSCategoryBAL(oMAS_RMSCategoryBE) == 1)
        {
            Response.Redirect("CategoryOverview.aspx");
        }
        else
        {
            string saveMessage = WebCommon.getGlobalResourceValue("CategoryNameExists");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + saveMessage + "')", true);
        }
    }
}
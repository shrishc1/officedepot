﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" CodeFile="CategoryOverview.aspx.cs" Inherits="ModuleUI_Upload_RMS_CategoryOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucAddButton" TagPrefix="uc" Src="~/CommonUI/UserControls/ucAddButton.ascx" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <h2>
        <cc1:ucLabel ID="lblCategoryOverviewSetup" runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <uc:ucAddButton ID="btnAdd" runat="server" NavigateUrl="CategoryAdd.aspx" />
        <uc1:ucExportButton ID="btnExportToExcel1" runat="server" />
    </div>

    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
        
        <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
        <tr>
            <td align="center">
                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" onsorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Category" SortExpression="CategoryName">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                              <asp:HyperLink ID="hpCategory" runat="server" Text='<%#Eval("CategoryName") %>' NavigateUrl='<%# EncryptQuery("CategoryAdd.aspx?RMSCategoryID="+ Eval("RMSCategoryID")) %>'></asp:HyperLink>                              
                              
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Create By" SortExpression="CreatedBy">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                   <asp:Label ID="lblCreateBy" runat="server" Text='<%#Eval("CreateByName") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Created On" SortExpression="CreatedOn">
                            <HeaderStyle Width="15%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblCreatedOn" runat="server" Text=' <%# string.Format("{0:dd/MM/yyyy}", Eval("CreatedOn")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>          
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
   
    </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

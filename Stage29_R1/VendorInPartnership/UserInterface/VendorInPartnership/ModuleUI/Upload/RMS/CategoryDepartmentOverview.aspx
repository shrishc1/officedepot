﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" CodeFile="CategoryDepartmentOverview.aspx.cs" Inherits="ModuleUI_Upload_RMS_CategoryDepartmentOverview" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectRMSCategorySearch.ascx" TagName="MultiSelectRMSCategorySearch1"
    TagPrefix="uc1" %>
<%@ Register TagName="ucAddButton" TagPrefix="uc" Src="~/CommonUI/UserControls/ucAddButton.ascx" %>
<%@ Register TagName="ucExportButton" TagPrefix="uc1" Src="~/CommonUI/UserControls/ucExportToExcel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="1200">
    </asp:ScriptManager>
   <h2>
        <cc1:ucLabel ID="lblCategoryDepartmentOverview" runat="server"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <uc:ucaddbutton ID="btnAdd" runat="server" 
            NavigateUrl="CategoryMaintenance.aspx" />
        <uc1:ucexportbutton ID="btnExportToExcel1" runat="server" />
    </div>
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
   
    <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
        <tr>
         <td style="font-weight: bold; width: 10%" ><cc1:ucLabel ID="lblRMSCategorySetUp" runat="server"></cc1:ucLabel> </td> <td style="font-weight: bold;width: 2%">
                            <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                        </td>
            <td>  <uc1:MultiSelectRMSCategorySearch1 ID="ucRmsCategory" runat="server"></uc1:MultiSelectRMSCategorySearch1></td>
        </tr>
        <tr><td colspan="3">  <div class="button-row">
        <cc1:ucButton ID="btnSearch" runat="server"  CssClass="button" onclick="btnSearch_Click" 
                   /></td></tr>
         <tr>
            <td colspan="3"> 
                <cc1:ucGridView ID="UcGridView1" Width="100%" runat="server" CssClass="grid" onsorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Category" SortExpression="RMSCategory.CategoryName">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                              <asp:HyperLink ID="hpCategory" runat="server" Text='<%#Eval("RMSCategory.CategoryName") %>' NavigateUrl='<%# EncryptQuery("CategoryMaintenance.aspx?RMSCategoryID="+ Eval("RMSCategory.RMSCategoryID")) %>'></asp:HyperLink>                              
                              
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Assigned By">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                   <asp:Label ID="lblCreateBy" runat="server" Text='<%#Eval("RMSCategory.CreateByName") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Assigned On">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblCreatedOn" runat="server" Text=' <%# string.Format("{0:dd/MM/yyyy}", Eval("RMSCategory.CreatedOn")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>         
                        
                        
                        <asp:TemplateField HeaderText="Division">
                            <HeaderStyle Width="5%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                            <asp:Label ID="lblDivision" runat="server" Text='<%#Eval("Division") %>'></asp:Label>
                              
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Division Name">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                   <asp:Label ID="lblDivisionName" runat="server" Text='<%#Eval("Div_Name") %>'></asp:Label>
                            </ItemTemplate>
                           
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Group">
                            <HeaderStyle Width="5%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblGroup" runat="server" Text='<%#Eval("Group_No") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        
                        <asp:TemplateField HeaderText="Group Name">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblGroupName" runat="server" Text='<%#Eval("Group_Name") %>'></asp:Label>                              
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Department">
                            <HeaderStyle Width="10%" HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblDept" runat="server" Text='<%#Eval("Dept") %>'></asp:Label>                                  
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Department Name">
                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                             <asp:Label ID="lblDept" runat="server" Text='<%#Eval("Dept_Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>           
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
        <tr>
        <td>
         <div class="button-row">
      <cc1:PagerV2_8 ID="pager1" runat="server"  OnCommand="pager_Command" GenerateGoToSection="false">
      </cc1:PagerV2_8>
     </div>  
        </td></tr>
    </table>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
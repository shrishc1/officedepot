﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using BaseControlLibrary;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;

using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using System.Linq;


public partial class VendorCountryFreqEdit : CommonPage
{

    protected string deleteMessage = WebCommon.getGlobalResourceValue("DeleteMessage");
    protected bool IsVendorByNameClicked = false;
    protected bool IsVendorByNumberClicked = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCountry();
           // BindVendor();
            LBLEditFreqVendor.Visible = false;
            btnDelete.Visible = false;
            if (GetQueryStringValue("VendorCountryFreqID") != null)
            {
                MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
                MAS_VendorScoreCardBAL objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();
                pnlVendor.Visible = false;
                ddlCountry.Visible = false;
                txtVendorNo.Visible = false;
                btnSearchVendor.Visible = false;
                btnSearchVendorNumber.Visible = false;
                LBLEditFreqVendor.Visible = true;
                btnDelete.Visible = true;
                objVendorScoreCardBE.Action = "GetVendorFreq";
                objVendorScoreCardBE.VendorCountryFreqID = Convert.ToInt32(GetQueryStringValue("VendorCountryFreqID"));

                objVendorScoreCardBE = objVendorScoreCardBAL.GetVendorFreqBAL(objVendorScoreCardBE);
                objVendorScoreCardBAL = null;
                lblEditCountry.Text = objVendorScoreCardBE.CountryName;                
                LBLEditFreqVendor.Text = objVendorScoreCardBE.VendorName;
                if (objVendorScoreCardBE.Frequency == 'M')
                {
                    rdoMonthly.Checked = true;
                    rdoNever.Checked = false;
                }
                else if (objVendorScoreCardBE.Frequency == 'Q')
                {
                    rdoQuarterly.Checked = true;
                    rdoNever.Checked = false;
                }
                else
                    rdoNever.Checked = true;
            }
        }
    }

    #region Method
    private string _selectedVendorIDs = string.Empty;
    public string SelectedVendorIDs
    {
        get
        {
            if (!string.IsNullOrEmpty(hiddenSelectedIDs.Value))
            {
                return hiddenSelectedIDs.Value.Trim(',');
            }
            return null;
        }
        set
        {
            hiddenSelectedIDs.Value += value + ",";
        }
    }

    protected void BindCountry()
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "ShowAll";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();
        lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);
        oMAS_CountryBAL = null;
        if (lstCountry.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCountry, lstCountry.ToList(), "CountryName", "CountryID", "All");
        }
        if (Session["SiteCountryID"] != null)
        {
            ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(Session["SiteCountryID"].ToString()));
        }
    }

    protected void BindVendor()
    {
        MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
        MAS_VendorScoreCardBAL objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();

        objVendorScoreCardBE.Action = "GetVendorByCountry";
        objVendorScoreCardBE.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);

        if (IsVendorByNameClicked == true)
        {
            objVendorScoreCardBE.VendorName = txtVendorNo.Text.Trim(); 
        }
        else if(IsVendorByNumberClicked == true)
        {
            objVendorScoreCardBE.VendorNo = txtVendorNo.Text.Trim();
        }

        List<MAS_VendorScoreCardBE> lstUPVendor = new List<MAS_VendorScoreCardBE>();
        lstUPVendor = objVendorScoreCardBAL.GetVendorByCountryBAL(objVendorScoreCardBE);
        objVendorScoreCardBAL = null;
        if (lstUPVendor.Count > 0)
        {
            FillControls.FillListBox(ref lstLeft, lstUPVendor, "VendorName", "VendorID");
        }
        else
        {
            lstLeft.Items.Clear();
            lstRight.Items.Clear();
        }
        
    }
    #endregion

    #region Events

    protected virtual void btnMoveRightAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstLeft, lstRight);
    }

    protected virtual void btnMoveRight_Click(object sender, EventArgs e)
    {
        if (lstLeft.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstLeft, lstRight);
        }
    }

    protected virtual void btnMoveLeft_Click(object sender, EventArgs e)
    {
        if (lstRight.SelectedItem != null)
        {
            FillControls.MoveOneItem(lstRight, lstLeft);
        }
    }

    protected virtual void btnMoveLeftAll_Click(object sender, EventArgs e)
    {
        FillControls.MoveAllItems(lstRight, lstLeft);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
        MAS_VendorScoreCardBAL objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();
        if (GetQueryStringValue("VendorCountryFreqID") != null)
        {
            objVendorScoreCardBE.VendorCountryFreqID = Convert.ToInt32(GetQueryStringValue("VendorCountryFreqID"));
        }
        else
        {
            objVendorScoreCardBE.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
            objVendorScoreCardBE.SelectedVendorIDs = SelectedVendorIDs;

        }
        objVendorScoreCardBE.Action = "AddEditFrequency";
        if (rdoMonthly.Checked)
            objVendorScoreCardBE.Frequency = 'M';
        else if (rdoQuarterly.Checked)
            objVendorScoreCardBE.Frequency = 'Q';
        else
            objVendorScoreCardBE.Frequency = 'N';
        objVendorScoreCardBAL.AddEditFrequencyBAL(objVendorScoreCardBE);
        objVendorScoreCardBAL = null;
        EncryptQueryString("VendorCountryFreqOverview.aspx");
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
        MAS_VendorScoreCardBAL objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();
        if (GetQueryStringValue("VendorCountryFreqID") != null)
        {
            objVendorScoreCardBE.Action = "DeleteVendorFreq";
            objVendorScoreCardBE.VendorCountryFreqID = Convert.ToInt32(GetQueryStringValue("VendorCountryFreqID"));
            objVendorScoreCardBAL.DeleteVendorFreqBAL(objVendorScoreCardBE);
            objVendorScoreCardBAL = null;
            EncryptQueryString("VendorCountryFreqOverview.aspx");
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("VendorCountryFreqOverview.aspx");
    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
       // BindVendor();
        txtVendorNo.Text = "";
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        IsVendorByNameClicked = true;
        IsVendorByNumberClicked = false;
        BindVendor();
        
    }

    protected void btnSearchByVendorNo_Click(object sender, EventArgs e)
    {
        IsVendorByNameClicked = false;
        IsVendorByNumberClicked = true;
        BindVendor();
    }


    #endregion

}



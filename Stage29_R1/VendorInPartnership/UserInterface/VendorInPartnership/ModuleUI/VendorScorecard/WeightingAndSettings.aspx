﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WeightingAndSettings.aspx.cs"
    Inherits="WeightingAndSettings" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function ValidateCheck() {

            if (document.getElementById('<%=hdnChecksStockWeight.ClientID %>').value == 1 || document.getElementById('<%=hdnChecksDiscWeight.ClientID %>').value == 1) {
                alert('<%=AllWeightSettingAlert%>')
                return false;
            }
        }


        function ClientValidate(source, arguments) {
            if (document.getElementById('<%=hdnChecksStockWeight.ClientID %>').value == 1 || document.getElementById('<%=hdnChecksDiscWeight.ClientID %>').value == 1
            || document.getElementById('<%=hdnChecksOverAllWeight.ClientID %>').value == 1) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }

        function CheckDecimalTwoPlace(txtId, regxPattern) {
            var isValid = false;
            var txtObj = $('#' + txtId);
            if (txtObj.length > 0) {
                var regx = new RegExp(regxPattern);
                isValid = regx.test($.trim(txtObj.val()));
            }
            return isValid;
        }

        function CheckDiscWeight() {

            var TargetValue = 0;
            DiscWeight = parseFloat(document.getElementById('<%=txtDiscWeight.ClientID %>').value, 10).toFixed(2);
            AvgWeight = parseFloat(document.getElementById('<%=txtAvgWeight.ClientID %>').value, 10).toFixed(2);

            if (!isNaN(DiscWeight) && !isNaN(AvgWeight)) {
                TargetValue = parseFloat(DiscWeight) + parseFloat(AvgWeight);
            }
            else {
                if (!isNaN(DiscWeight)) {
                    TargetValue = parseFloat(DiscWeight);
                }
                else if (!isNaN(AvgWeight)) {
                    TargetValue = parseFloat(AvgWeight);

                }
                else {
                    TargetValue = 0.00;
                }
            }
            document.getElementById('<%=txtDescWeight.ClientID %>').value = TargetValue;

            if (TargetValue != 100) {
                alert('<%=WeightSettingAlert%>');
                document.getElementById('<%=hdnChecksDiscWeight.ClientID %>').value = 1;
            } else {
                document.getElementById('<%=hdnChecksDiscWeight.ClientID %>').value = 0;
            }
        }

        function AddCheckDiscWeight() {

            var TargetValue = 0;
            DiscWeight = parseFloat(document.getElementById('<%=txtDiscWeight.ClientID %>').value, 10).toFixed(2);
            AvgWeight = parseFloat(document.getElementById('<%=txtAvgWeight.ClientID %>').value, 10).toFixed(2);

            if (!isNaN(DiscWeight) && !isNaN(AvgWeight)) {
                TargetValue = parseFloat(DiscWeight) + parseFloat(AvgWeight);
            }
            else {
                if (!isNaN(DiscWeight)) {
                    TargetValue = parseFloat(DiscWeight);
                }
                else if (!isNaN(AvgWeight)) {
                    TargetValue = parseFloat(AvgWeight);

                }
                else {
                    TargetValue = 0.00;
                }
            }
            document.getElementById('<%=txtDescWeight.ClientID %>').value = TargetValue;

            if (TargetValue != 100) {
                document.getElementById('<%=hdnChecksDiscWeight.ClientID %>').value = 1;
            } else {
                document.getElementById('<%=hdnChecksDiscWeight.ClientID %>').value = 0;
            }
        }

        function CheckStockWeight() {
            var TargetValue = 0;
            AvgBackorderDaysWeight = parseFloat(document.getElementById('<%=txtAvgBackorderDaysWeight.ClientID %>').value, 10).toFixed(2);
            LeadTimeVarianceWeight = parseFloat(document.getElementById('<%=txtLeadTimeVarianceWeight.ClientID %>').value, 10).toFixed(2);

            if (!isNaN(AvgBackorderDaysWeight) && !isNaN(LeadTimeVarianceWeight)) {
                TargetValue = parseFloat(AvgBackorderDaysWeight) + parseFloat(LeadTimeVarianceWeight);
            }
            else {
                if (!isNaN(AvgBackorderDaysWeight)) {
                    TargetValue = parseFloat(AvgBackorderDaysWeight);
                }
                else if (!isNaN(LeadTimeVarianceWeight)) {
                    TargetValue = parseFloat(LeadTimeVarianceWeight);
                }
                else {
                    TargetValue = 0.00;
                }
            }
            document.getElementById('<%=txtStockWeightCheckWeight.ClientID %>').value = TargetValue;

            if (TargetValue != 100) {
                alert('<%=WeightSettingAlert%>');
                document.getElementById('<%=hdnChecksStockWeight.ClientID %>').value = 1;
            } else {
                document.getElementById('<%=hdnChecksStockWeight.ClientID %>').value = 0;
            }
        }

        function AddCheckStockWeight() {
            var TargetValue = 0;
            AvgBackorderDaysWeight = parseFloat(document.getElementById('<%=txtAvgBackorderDaysWeight.ClientID %>').value, 10).toFixed(2);
            LeadTimeVarianceWeight = parseFloat(document.getElementById('<%=txtLeadTimeVarianceWeight.ClientID %>').value, 10).toFixed(2);

            if (!isNaN(AvgBackorderDaysWeight) && !isNaN(LeadTimeVarianceWeight)) {
                TargetValue = parseFloat(AvgBackorderDaysWeight) + parseFloat(LeadTimeVarianceWeight);
            }
            else {
                if (!isNaN(AvgBackorderDaysWeight)) {
                    TargetValue = parseFloat(AvgBackorderDaysWeight);
                }
                else if (!isNaN(LeadTimeVarianceWeight)) {
                    TargetValue = parseFloat(LeadTimeVarianceWeight);
                }
                else {
                    TargetValue = 0.00;
                }
            }
            document.getElementById('<%=txtStockWeightCheckWeight.ClientID %>').value = TargetValue;

            if (TargetValue != 100) {
                document.getElementById('<%=hdnChecksStockWeight.ClientID %>').value = 1;
            } else {
                document.getElementById('<%=hdnChecksStockWeight.ClientID %>').value = 0;
            }
        }

        function CheckOverAllWeight_New() { }

        var regTwoPlaceDecimal = /^-?[0-9]{0,2}(\.[0-9]{1,2})?$|^-?(100)(\.[0]{1,2})?$/;
        var isTwoPlaceDecimal = function (txtId, regxPattern) {
            return CheckDecimalTwoPlace(txtId, regxPattern);
        };


        function CheckOverAllWeight() {
            var TargetValue = 0;
            OtWeight = parseInt(document.getElementById('<%=txtOtWeight.ClientID %>').value, 10);
            SchedulingWeight = parseInt(document.getElementById('<%=txtSchedulingWeight.ClientID %>').value, 10);
            DiscrepenciesWeight = parseInt(document.getElementById('<%=txtDiscrepenciesWeight.ClientID %>').value, 10);
            StockWeight = parseInt(document.getElementById('<%=txtStockWeight.ClientID %>').value, 10);

            if (!isNaN(OtWeight) && !isNaN(SchedulingWeight) && !isNaN(DiscrepenciesWeight) && !isNaN(StockWeight)) {
                document.getElementById('<%=txtOTIFCheckWeight.ClientID %>').value = OtWeight + SchedulingWeight + DiscrepenciesWeight + StockWeight
                TargetValue = document.getElementById('<%=txtOTIFCheckWeight.ClientID %>').value
            }

            if (parseInt(TargetValue, 10) != 100) {
                alert('<%=WeightSettingAlert%>');
                document.getElementById('<%=hdnChecksOverAllWeight.ClientID %>').value = 1;
            } else {
                document.getElementById('<%=hdnChecksOverAllWeight.ClientID %>').value = 0;
            }
        }

        function ValidateDiscrepanciesWeightAndTarget() {
            $('#<%=txtDiscWeight.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtDiscWeight.ClientID %>', regTwoPlaceDecimal)) {
                    AddCheckDiscWeight();
                    alert('<%=DiscrepancyRateWeightMessage%>');
                }
                else {
                    CheckDiscWeight();
                }
            });
            $('#<%=txtAvgWeight.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtAvgWeight.ClientID %>', regTwoPlaceDecimal)) {
                    AddCheckDiscWeight();
                    alert('<%=DiscrepancyAverageResponseTimeWeightMessage%>');
                }
                else {
                    CheckDiscWeight();
                }
            });

            $('#<%=txtDiscIdeal.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtDiscIdeal.ClientID %>', regTwoPlaceDecimal)) {
                    alert('<%=DiscrepancyRateIdealMessage%>');
                }
            });
            $('#<%=txtAvgIdeal.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtAvgIdeal.ClientID %>', regTwoPlaceDecimal)) {
                    alert('<%=DiscrepancyAverageResponseTimeIdealMessage%>');
                }
            });

            $('#<%=txtDiscTarget.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtDiscTarget.ClientID %>', regTwoPlaceDecimal)) {
                    alert('<%=DiscrepancyRateTargetMessage%>');
                }
            });
            $('#<%=txtAvgTarget.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtAvgTarget.ClientID %>', regTwoPlaceDecimal)) {
                    alert('<%=DiscrepancyAverageResponseTimeTargetMessage%>');
                }
            });

            $('#<%=txtDiscFailure.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtDiscFailure.ClientID %>', regTwoPlaceDecimal)) {
                    alert('<%=DiscrepancyRateFailureMessage%>');
                }
            });
            $('#<%=txtAvgFailure.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtAvgFailure.ClientID %>', regTwoPlaceDecimal)) {
                    alert('<%=DiscrepancyAverageResponseTimeFailureMessage%>');
                }
            });
        }

        function ValidateStockWeightAndTargets() {
            $('#<%=txtAvgBackorderDaysWeight.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtAvgBackorderDaysWeight.ClientID %>', regTwoPlaceDecimal)) {
                    AddCheckStockWeight();
                    alert('<%=StockBackOrderRateWeightMessage%>');
                }
                else {
                    CheckStockWeight();
                }
            });
            $('#<%=txtLeadTimeVarianceWeight.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtLeadTimeVarianceWeight.ClientID %>', regTwoPlaceDecimal)) {
                    AddCheckStockWeight();
                    alert('<%=StockLeadTimeVarianceWeightMessage%>');
                }
                else {
                    CheckStockWeight();
                }
            });
            $('#<%=txtAvgBackorderDaysIdeal.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtAvgBackorderDaysIdeal.ClientID %>', regTwoPlaceDecimal)) {
                    alert('<%=StockBackOrderRateIdealMessage%>');
                }
            });
            $('#<%=txtLeadTimeVarianceIdeal.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtLeadTimeVarianceIdeal.ClientID %>', regTwoPlaceDecimal)) {
                    alert('<%=StockLeadTimeVarianceIdealMessage%>');
                }
            });
            $('#<%=txtAvgBackorderDaysTargetPositive.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtAvgBackorderDaysTargetPositive.ClientID %>', regTwoPlaceDecimal)) {
                    alert('<%=StockBackOrderRateTargetMessage%>');
                }
            });
            $('#<%=txtLeadTimeVarianceTargetPositive.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtLeadTimeVarianceTargetPositive.ClientID %>', regTwoPlaceDecimal)) {
                    alert('<%=StockLeadTimeVarianceTargetMessage%>');
                }
            });
            $('#<%=txtAvgBackorderDaysFailurePositive.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtAvgBackorderDaysFailurePositive.ClientID %>', regTwoPlaceDecimal)) {
                    alert('<%=StockBackOrderRateFailureMessage%>');
                }
            });
            $('#<%=txtLeadTimeVarianceFailurePositive.ClientID %>').blur(function () {
                if (!isTwoPlaceDecimal('<%=txtLeadTimeVarianceFailurePositive.ClientID %>', regTwoPlaceDecimal)) {
                    alert('<%=StockLeadTimeVarianceFailureMessage%>');
                }
            });
        }

        function AllowDecimalTwoPlaceOnly(txtId) {

            var regxPattern = /^[0-9]{1,3}?(\.[0-9]{0,2})?$/;
            var isValid = false;
            var txtObj = $('#' + txtId.id);

            if (txtObj.length > 0) {
                var regx = new RegExp(regxPattern);
                isValid = regx.test($.trim(txtObj.val()));
            }

            //alert(parseFloat(txtObj.val()) > 100.00);
            if (!isValid) {
                var inputString = txtObj.val();
                var shortenedString = inputString.substr(0, (inputString.length - 1));
                txtObj.val(shortenedString);
            }
            //            else if (parseFloat(txtObj.val()) > 100.00) {
            //                txtObj.val('100.00');
            //            }
        }

        $(document).ready(function () {
            ValidateDiscrepanciesWeightAndTarget();
            ValidateStockWeightAndTargets();
        });

       
    </script>
    <h2>
        <cc1:ucLabel ID="lblWeightingandSettings" runat="server" Text="Weighting and Settings"></cc1:ucLabel>
    </h2>
    <asp:HiddenField ID="hdnChecksOverAllWeight" runat="server" Value="0" />
    <asp:HiddenField ID="hdnChecksOTIFWeight" runat="server" Value="0" />
    <asp:HiddenField ID="hdnChecksDiscWeight" runat="server" Value="0" />
    <asp:HiddenField ID="hdnChecksSchedulingAcceptWeight" runat="server" Value="0" />
    <asp:HiddenField ID="hdnChecksSchedulingRefuseWeight" runat="server" Value="0" />
    <asp:HiddenField ID="hdnChecksStockWeight" runat="server" Value="0" />
    <div class="right-shadow">
        <div class="formbox">
            <table style="width: 100%" class="form-table">
                <tr>
                    <td colspan="3">
                        <cc1:ucLabel ID="lblOverallWeightDesc" runat="server"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td style="width: 49%;">
                        <cc1:ucPanel ID="pnlOverallWeight" runat="server" GroupingText="Overall Weight" CssClass="fieldset-form">
                            <table style="width: 100%;" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td style="font-weight: bold; width: 20%;">
                                        <cc1:ucLabel ID="lblSection" runat="server" Text="Section"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 5%;">
                                    </td>
                                    <td style="width: 75%">
                                        <cc1:ucLabel ID="lblOverallWeightPer" Font-Bold="true" runat="server" Text="Overall Weight % "></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblOTIF" runat="server" Text="OTIF"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtOtWeight" onkeyup="AllowNumbersOnly(this);" onkeypress="return IsNumberKey(event, this);"
                                            onchange="Javascript:CheckOverAllWeight()" MaxLength="3" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblScheduling" runat="server" Text="Scheduling"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtSchedulingWeight" onkeyup="AllowNumbersOnly(this);"
                                            onkeypress="return IsNumberKey(event, this);" onchange="Javascript:CheckOverAllWeight()"
                                            MaxLength="3" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblDiscrepencies" runat="server" Text="Discrepencies"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td valign="top">
                                        <cc1:ucNumericTextbox ID="txtDiscrepenciesWeight" onkeyup="AllowNumbersOnly(this);"
                                            onkeypress="return IsNumberKey(event, this);" onchange="Javascript:CheckOverAllWeight()"
                                            MaxLength="3" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblStock" runat="server" Text="Stock"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td valign="top">
                                        <cc1:ucNumericTextbox ID="txtStockWeight" onkeyup="AllowNumbersOnly(this);" MaxLength="3"
                                            onkeypress="return IsNumberKey(event, this);" onchange="Javascript:CheckOverAllWeight()"
                                            runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblCheck" runat="server" Text="Check" isRequired="False" Tag=""></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td valign="top">
                                        <cc1:ucNumericTextbox ID="txtOTIFCheckWeight" onkeyup="AllowNumbersOnly(this);" MaxLength="3"
                                            onkeypress="return IsNumberKey(event, this);" runat="server" Width="40px" ReadOnly="True"></cc1:ucNumericTextbox>%
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <cc1:ucLabel ID="lblOverallScoreDesc" runat="server"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td style="width: 49%;" valign="top">
                        <cc1:ucPanel ID="pnlOverallScore" runat="server" GroupingText="Overall Score" CssClass="fieldset-form">
                            <table style="width: 100%;" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td style="font-weight: bold; width: 20%;">
                                        <cc1:ucLabel ID="lblScores" Font-Bold="true" runat="server" Text="Scores"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 5%;">
                                    </td>
                                    <td valign="top" style="width: 75%">
                                        <cc1:ucLabel ID="lblValue" Font-Bold="true" runat="server" Text="Value"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblIdeal" runat="server" Text="Ideal"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td valign="top">
                                        <cc1:ucNumericTextbox ID="txtIdealScore" onkeyup="AllowDecimalOnly(this);" MaxLength="6"
                                            onkeypress="return IsNumberKey(event, this);" runat="server" Width="40px"></cc1:ucNumericTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblTarget" runat="server" Text="Target"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtTargetScore" onkeyup="AllowDecimalOnly(this);" MaxLength="6"
                                            onkeypress="return IsNumberKey(event, this);" runat="server" Width="40px"></cc1:ucNumericTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblFailure" runat="server" Text="Failure"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td valign="top">
                                        <cc1:ucNumericTextbox ID="txtFailureScore" onkeyup="AllowDecimalOnly(this);" MaxLength="6"
                                            onkeypress="return IsNumberKey(event, this);" runat="server" Width="40px"></cc1:ucNumericTextbox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblAbsoluteFail" runat="server" Text="Absolute Fail"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td valign="top">
                                        <cc1:ucNumericTextbox ID="txtAbsoluteFail" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <cc1:ucLabel ID="lblOtifWeightandTargetDesc" runat="server"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td style="width: 49%;">
                        <cc1:ucPanel ID="pnlOTIFWeightandTargets" runat="server" GroupingText="OTIF Weight and Targets"
                            CssClass="fieldset-form">
                            <table style="width: 100%;" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td style="font-weight: bold; width: 20%;">
                                    </td>
                                    <td style="width: 5%;">
                                    </td>
                                    <td valign="top" style="width: 15%">
                                        <cc1:ucLabel ID="lblIdeal_1" Font-Bold="true" runat="server" Text="Ideal"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="width: 15%">
                                        <cc1:ucLabel ID="lblTarget_1" Font-Bold="true" runat="server" Text="Target"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="width: 15%">
                                        <cc1:ucLabel ID="lblFailure_1" Font-Bold="true" runat="server" Text="Failure"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="width: 30%">
                                        <cc1:ucLabel ID="lblAbsoluteFail_1" runat="server" Text="Absolute Fail"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblOTIFRatelable" Font-Bold="true" runat="server" Text="OTIF Rate"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtOtifIdeal" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvtxtOtifIdeal" runat="server" ControlToValidate="txtOtifIdeal"
                                            ErrorMessage="OTIF Rate Ideal Should not be blank." SetFocusOnError="true" Display="None"
                                            ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvtxtOtifIdeal" runat="server" ControlToValidate="txtOtifIdeal"
                                            MaximumValue="100" MinimumValue="0" ValidationGroup="Apply" SetFocusOnError="true"
                                            Display="None" ErrorMessage="Maximum value of OTIF Rate Ideal Should not be > 100." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtOtifTarget" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvtxtOtifTarget" runat="server" ControlToValidate="txtOtifTarget"
                                            ErrorMessage="OTIF Rate Target Should not be blank." SetFocusOnError="true" Display="None"
                                            ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvtxtOtifTarget" runat="server" ControlToValidate="txtOtifTarget"
                                            MaximumValue="100" MinimumValue="0" ValidationGroup="Apply" SetFocusOnError="true"
                                            Display="None" ErrorMessage="Maximum value of OTIF Rate Target Should not be > 100." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtOtifFailure" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvtxtOtifFailure" runat="server" ControlToValidate="txtOtifFailure"
                                            ErrorMessage="OTIF Rate Failure Should not be blank." SetFocusOnError="true"
                                            Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvtxtOtifFailure" runat="server" ControlToValidate="txtOtifFailure"
                                            MaximumValue="100" MinimumValue="0" ValidationGroup="Apply" SetFocusOnError="true"
                                            Display="None" ErrorMessage="Maximum value of OTIF Rate Failure Should not be > 100." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtOTIFAbsFail" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvtxtOTIFAbsFail" runat="server" ControlToValidate="txtOTIFAbsFail"
                                            ErrorMessage="OTIF Rate Absolute Fail Should not be blank." SetFocusOnError="true"
                                            Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvtxtOTIFAbsFail" runat="server" ControlToValidate="txtOTIFAbsFail"
                                            MaximumValue="100" MinimumValue="0" ValidationGroup="Apply" SetFocusOnError="true"
                                            Display="None" ErrorMessage="Maximum value of OTIF Rate Absolute Fail Should not be > 100." />
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <cc1:ucLabel ID="lblDiscrepancyWeightandTargetDesc" runat="server"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td style="width: 49%;" valign="top">
                        <cc1:ucPanel ID="pnlDiscrepanciesWeightandTarget" runat="server" GroupingText="Discrepancies Weight and Targets"
                            CssClass="fieldset-form">
                            <table style="width: 100%;" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td style="font-weight: bold; width: 20%;">
                                        <cc1:ucLabel ID="lblDiscrepancies" Font-Bold="true" runat="server" Text="Discrepancies"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 5%;">
                                    </td>
                                    <td valign="top" style="width: 15%">
                                        <cc1:ucLabel ID="lblWeight_1" Font-Bold="true" runat="server" Text="Weight"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="width: 15%">
                                        <cc1:ucLabel ID="lblIdeal_2" Font-Bold="true" runat="server" Text="Ideal"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="width: 15%">
                                        <cc1:ucLabel ID="lblTarget_2" Font-Bold="true" runat="server" Text="Target"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="width: 15%">
                                        <cc1:ucLabel ID="lblFailure_2" Font-Bold="true" runat="server" Text="Failure"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="width: 15%">
                                        <cc1:ucLabel ID="lblAbsoluteFail_2" runat="server" Text="Absolute Fail"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblDiscrepancyRate" runat="server" Text="Discrepancy Rate"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtDiscWeight" onkeyup="AllowDecimalOnly(this);" MaxLength="5"
                                            runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtDiscIdeal" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvDiscrepancyRateIdealBlank" runat="server" ControlToValidate="txtDiscIdeal"
                                            ErrorMessage="Discrepancy Rate Ideal can not be blank." SetFocusOnError="true"
                                            Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvDiscrepancyRateIdealMessage" runat="server"
                                            ControlToValidate="txtDiscIdeal" MaximumValue="100" MinimumValue="0" ValidationGroup="Apply"
                                            SetFocusOnError="true" Display="None" ErrorMessage="Discrepancy Rate Ideal can not be more than 100 and can not be more than two decimal places." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtDiscTarget" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvDiscrepancyRateTargetBlank" runat="server" ControlToValidate="txtDiscTarget"
                                            ErrorMessage="Discrepancy Rate Target can not be blank." SetFocusOnError="true"
                                            Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvDiscrepancyRateTargetMessage" runat="server"
                                            ControlToValidate="txtDiscTarget" MaximumValue="100" MinimumValue="0" ValidationGroup="Apply"
                                            SetFocusOnError="true" Display="None" ErrorMessage="Discrepancy Rate Target can not be more than 100 and can not be more than two decimal places." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtDiscFailure" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvDiscrepancyRateFailureBlank" runat="server" ControlToValidate="txtDiscFailure"
                                            ErrorMessage="Discrepancy Rate Failure can not be blank." SetFocusOnError="true"
                                            Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvDiscrepancyRateFailureMessage" runat="server"
                                            ControlToValidate="txtDiscFailure" MaximumValue="100" MinimumValue="0" ValidationGroup="Apply"
                                            SetFocusOnError="true" Display="None" ErrorMessage="Discrepancy Rate Failure can not be more than 100 and can not be more than two decimal places." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtDisAbsFail" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvtxtDisAbsFail" runat="server" ControlToValidate="txtDisAbsFail"
                                            ErrorMessage="OTIF Rate Absolute Fail Should not be blank." SetFocusOnError="true"
                                            Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvtxtDisAbsFail" runat="server" ControlToValidate="txtDisAbsFail"
                                            MaximumValue="100" MinimumValue="0" ValidationGroup="Apply" SetFocusOnError="true"
                                            Display="None" ErrorMessage="Maximum value of Discrepancy Rate Absolute Fail Should not be > 100." />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblAverageResponseTimeInDays" runat="server" Text="Average Response Time (In Days)"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtAvgWeight" onkeyup="AllowDecimalOnly(this);" MaxLength="5"
                                            runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtAvgIdeal" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>
                                        <asp:RequiredFieldValidator ID="rfvDiscrepancyAverageResponseTimeIdealBlank" runat="server"
                                            ControlToValidate="txtAvgIdeal" ErrorMessage="Discrepancy Average Response Time Ideal can not be blank."
                                            SetFocusOnError="true" Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvDiscrepancyAverageResponseTimeIdealMessage"
                                            runat="server" ControlToValidate="txtAvgIdeal" MaximumValue="100" MinimumValue="0"
                                            ValidationGroup="Apply" SetFocusOnError="true" Display="None" ErrorMessage="Discrepancy Average Response Time Ideal can not be more than 100 and can not be more than two decimal places." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtAvgTarget" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>
                                        <asp:RequiredFieldValidator ID="rfvDiscrepancyAverageResponseTimeTargetBlank" runat="server"
                                            ControlToValidate="txtAvgTarget" ErrorMessage="Discrepancy Average Response Time Target can not be blank."
                                            SetFocusOnError="true" Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvDiscrepancyAverageResponseTimeTargetMessage"
                                            runat="server" ControlToValidate="txtAvgTarget" MaximumValue="100" MinimumValue="0"
                                            ValidationGroup="Apply" SetFocusOnError="true" Display="None" ErrorMessage="Discrepancy Average Response Time Target can not be more than 100 and can not be more than two decimal places." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtAvgFailure" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>
                                        <asp:RequiredFieldValidator ID="rfvDiscrepancyAverageResponseTimeFailureBlank" runat="server"
                                            ControlToValidate="txtAvgFailure" ErrorMessage="Discrepancy Average Response Time Failure can not be blank."
                                            SetFocusOnError="true" Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvDiscrepancyAverageResponseTimeFailureMessage"
                                            runat="server" ControlToValidate="txtAvgFailure" MaximumValue="100" MinimumValue="0"
                                            ValidationGroup="Apply" SetFocusOnError="true" Display="None" ErrorMessage="Discrepancy Average Response Time Failure can not be more than 100 and can not be more than two decimal places." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtAvgAbsFail" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>
                                        <asp:RequiredFieldValidator ID="rfvtxtAvgAbsFail" runat="server" ControlToValidate="txtAvgAbsFail"
                                            ErrorMessage="Discrepancy Average Response Time Absolute Fail can not be blank."
                                            SetFocusOnError="true" Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvtxtAvgAbsFail" runat="server" ControlToValidate="txtAvgAbsFail"
                                            MaximumValue="100" MinimumValue="0" ValidationGroup="Apply" SetFocusOnError="true"
                                            Display="None" ErrorMessage="Discrepancy Average Response Time Absolute Fail can not be more than 100 and can not be more than two decimal places." />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblCheck_2" runat="server" Text="Check" isRequired="False" Tag=""></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtDescWeight" onkeyup="AllowDecimalOnly(this);" MaxLength="5"
                                            runat="server" Width="40px" ReadOnly="True"></cc1:ucNumericTextbox>%
                                    </td>
                                    <td colspan="3">
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <cc1:ucLabel ID="lblSchedulingWeightandTargetDesc" runat="server"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td style="width: 49%;" valign="top">
                        <cc1:ucPanel ID="pnlSchedulingWeightandTargets" runat="server" GroupingText="Scheduling Weight and Targets"
                            CssClass="fieldset-form">
                            <table style="width: 100%;" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td style="font-weight: bold; width: 20%;">
                                    </td>
                                    <td style="width: 5%;">
                                    </td>
                                    <td valign="top" style="font-weight: bold; width: 15%;">
                                        <cc1:ucLabel ID="lblIdeal_3" Font-Bold="true" runat="server" Text="Ideal"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="font-weight: bold; width: 15%;">
                                        <cc1:ucLabel ID="lblTarget_3" Font-Bold="true" runat="server" Text="Target"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="font-weight: bold; width: 15%;">
                                        <cc1:ucLabel ID="lblFailure_3" Font-Bold="true" runat="server" Text="Failure"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="width: 30%">
                                        <cc1:ucLabel ID="lblAbsoluteFail_3" runat="server" Text="Absolute Fail"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblSchedulingErrorRate_1" Font-Bold="true" runat="server" Text="Scheduling Error Rate"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtSchedulingIdeal" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvtxtSchedulingIdeal" runat="server" ControlToValidate="txtSchedulingIdeal"
                                            ErrorMessage="Scheduling Error Rate Ideal Should not be blank." SetFocusOnError="true"
                                            Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvtxtSchedulingIdeal" runat="server" ControlToValidate="txtSchedulingIdeal"
                                            MaximumValue="100" MinimumValue="0" ValidationGroup="Apply" SetFocusOnError="true"
                                            Display="None" ErrorMessage="Maximum value of Scheduling Error Rate Ideal Should not be > 100." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtSchedulingTarget" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txtSchedulingTarget"
                                            ErrorMessage="Scheduling Error Rate Target Should not be blank." SetFocusOnError="true"
                                            Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvtxtSchedulingTarget" runat="server" ControlToValidate="txtSchedulingTarget"
                                            MaximumValue="100" MinimumValue="0" ValidationGroup="Apply" SetFocusOnError="true"
                                            Display="None" ErrorMessage="Maximum value of Scheduling Error Rate Target Should not be > 100." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtSchedulingFailure" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvtxtSchedulingFailure" runat="server" ControlToValidate="txtSchedulingFailure"
                                            ErrorMessage="Scheduling Error Rate Failure Should not be blank." SetFocusOnError="true"
                                            Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvtxtSchedulingFailure" runat="server" ControlToValidate="txtSchedulingFailure"
                                            MaximumValue="100" MinimumValue="0" ValidationGroup="Apply" SetFocusOnError="true"
                                            Display="None" ErrorMessage="Maximum value of Scheduling Error Rate Failure Should not be > 100." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtSchedulingAbsFail" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvtxtSchedulingAbsFail" runat="server" ControlToValidate="txtSchedulingAbsFail"
                                            ErrorMessage="Scheduling Error Rate Absolute Fail Should not be blank." SetFocusOnError="true"
                                            Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvtxtSchedulingAbsFail" runat="server" ControlToValidate="txtSchedulingAbsFail"
                                            MaximumValue="100" MinimumValue="0" ValidationGroup="Apply" SetFocusOnError="true"
                                            Display="None" ErrorMessage="Maximum value of Scheduling Error Rate Absolute Fail Should not be > 100." />
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <cc1:ucLabel ID="lblStockWeightandTargetDesc" runat="server"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <cc1:ucPanel ID="pnlStockWeightandTargets" runat="server" GroupingText="Stock Weight and Targets"
                            CssClass="fieldset-form">
                            <table style="width: 100%;" cellspacing="5" cellpadding="0" class="form-table">
                                <tr>
                                    <td style="font-weight: bold; width: 20%;">
                                        <cc1:ucLabel ID="lblStock_2" Font-Bold="true" runat="server" Text="Stock"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold; width: 5%;">
                                    </td>
                                    <td valign="top" style="width: 15%">
                                        <cc1:ucLabel ID="lblWeight_5" Font-Bold="true" runat="server" Text="Weight"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="width: 15%">
                                        <cc1:ucLabel ID="lblIdeal_5" Font-Bold="true" runat="server" Text="Ideal"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="width: 15%">
                                        <cc1:ucLabel ID="lbllblTarget_4" Font-Bold="true" runat="server" Text="Target"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="width: 15%">
                                        <cc1:ucLabel ID="lblFailure_4" Font-Bold="true" runat="server" Text="Failure"></cc1:ucLabel>
                                    </td>
                                    <td valign="top" style="width: 15%">
                                        <cc1:ucLabel ID="lblAbsoluteFail_4" runat="server" Text="Absolute Fail"></cc1:ucLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblBackOrderRate" runat="server" Text="Back Order Rate"></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtAvgBackorderDaysWeight" onkeyup="AllowDecimalOnly(this);"
                                            MaxLength="5" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <%--onkeypress="return IsNumberKey(event, this);" onchange="javascript:CheckStockWeight()"
                                            MaxLength="3" runat="server" Width="40px"></cc1:ucNumericTextbox>--%>
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtAvgBackorderDaysIdeal" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvStockBackOrderRateIdealBlank" runat="server" ControlToValidate="txtAvgBackorderDaysIdeal"
                                            ErrorMessage="Stock Back Order Rate Ideal can not be blank." SetFocusOnError="true"
                                            Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvStockBackOrderRateIdealMessage" runat="server"
                                            ControlToValidate="txtAvgBackorderDaysIdeal" MaximumValue="100" MinimumValue="0"
                                            ValidationGroup="Apply" SetFocusOnError="true" Display="None" ErrorMessage="Stock Back Order Rate Ideal can not be more than 100 and can not be more than two decimal places." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtAvgBackorderDaysTargetPositive" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvStockBackOrderRateTargetBlank" runat="server"
                                            ControlToValidate="txtAvgBackorderDaysTargetPositive" ErrorMessage="Stock Back Order Rate Target can not be blank."
                                            SetFocusOnError="true" Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvStockBackOrderRateTargetMessage" runat="server"
                                            ControlToValidate="txtAvgBackorderDaysTargetPositive" MaximumValue="100" MinimumValue="0"
                                            ValidationGroup="Apply" SetFocusOnError="true" Display="None" ErrorMessage="Stock Back Order Rate Target can not be more than 100 and can not be more than two decimal places." />
                                    </td>
                                    <%-- <td>
                                        <cc1:ucNumericTextbox ID="txtAvgBackorderDaysTargetNegative" onkeyup="AllowNumbersOnly(this);"
                                            onkeypress="return IsNumberKey(event, this);" MaxLength="3" runat="server" Width="40px"
                                            ReadOnly="True"></cc1:ucNumericTextbox>
                                    </td>--%>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtAvgBackorderDaysFailurePositive" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvStockBackOrderRateFailureBlank" runat="server"
                                            ControlToValidate="txtAvgBackorderDaysFailurePositive" ErrorMessage="Stock Back Order Rate Failure can not be blank."
                                            SetFocusOnError="true" Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvStockBackOrderRateFailureMessage" runat="server"
                                            ControlToValidate="txtAvgBackorderDaysFailurePositive" MaximumValue="100" MinimumValue="0"
                                            ValidationGroup="Apply" SetFocusOnError="true" Display="None" ErrorMessage="Stock Back Order Rate Failure can not be more than 100 and can not be more than two decimal places." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtStockBackOrderRateAbsFail" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <asp:RequiredFieldValidator ID="rfvtxtStockBackOrderRateAbsFail" runat="server" ControlToValidate="txtStockBackOrderRateAbsFail"
                                            ErrorMessage="Stock Back Order Rate Absolute Fail Should not be blank." SetFocusOnError="true"
                                            Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvtxtStockBackOrderRateAbsFail" runat="server"
                                            ControlToValidate="txtStockBackOrderRateAbsFail" MaximumValue="100" MinimumValue="0"
                                            ValidationGroup="Apply" SetFocusOnError="true" Display="None" ErrorMessage="Maximum value of Stock Back Order Rate Absolute Fail Should not be > 100." />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:ucLabel ID="lblLeadTimeVarianceInDays" runat="server" Text="Lead Time Variance (In Days)"></cc1:ucLabel>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtLeadTimeVarianceWeight" onkeyup="AllowDecimalOnly(this);"
                                            MaxLength="5" runat="server" Width="40px"></cc1:ucNumericTextbox>%
                                        <%--onkeypress="return IsNumberKey(event, this);" onchange="javascript:CheckStockWeight()"
                                            MaxLength="3" runat="server" Width="40px"></cc1:ucNumericTextbox>--%>
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtLeadTimeVarianceIdeal" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>
                                        <asp:RequiredFieldValidator ID="rfvStockLeadTimeVarianceIdealBlank" runat="server"
                                            ControlToValidate="txtLeadTimeVarianceIdeal" ErrorMessage="Stock Back Order Rate Failure can not be blank."
                                            SetFocusOnError="true" Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvStockLeadTimeVarianceIdealMessage" runat="server"
                                            ControlToValidate="txtLeadTimeVarianceIdeal" MaximumValue="100" MinimumValue="0"
                                            ValidationGroup="Apply" SetFocusOnError="true" Display="None" ErrorMessage="Stock Lead Time Variance Ideal can not be more than 100 and can not be more than two decimal places." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtLeadTimeVarianceTargetPositive" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>
                                        <asp:RequiredFieldValidator ID="rfvStockLeadTimeVarianceTargetBlank" runat="server"
                                            ControlToValidate="txtLeadTimeVarianceTargetPositive" ErrorMessage="Stock Lead Time Variance Target can not be blank."
                                            SetFocusOnError="true" Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvStockLeadTimeVarianceTargetMessage" runat="server"
                                            ControlToValidate="txtLeadTimeVarianceTargetPositive" MaximumValue="100" MinimumValue="0"
                                            ValidationGroup="Apply" SetFocusOnError="true" Display="None" ErrorMessage="Stock Lead Time Variance Target can not be more than 100 and can not be more than two decimal places." />
                                    </td>
                                    <%-- <td>
                                        <cc1:ucNumericTextbox ID="txtLeadTimeVarianceTargetNegative" MaxLength="3" runat="server"
                                            Width="40px"></cc1:ucNumericTextbox>
                                    </td>--%>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtLeadTimeVarianceFailurePositive" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>
                                        <asp:RequiredFieldValidator ID="rfvStockLeadTimeVarianceFailureBlank" runat="server"
                                            ControlToValidate="txtLeadTimeVarianceFailurePositive" ErrorMessage="Stock Lead Time Variance Failure can not be blank."
                                            SetFocusOnError="true" Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvStockLeadTimeVarianceFailureMessage" runat="server"
                                            ControlToValidate="txtLeadTimeVarianceFailurePositive" MaximumValue="100" MinimumValue="0"
                                            ValidationGroup="Apply" SetFocusOnError="true" Display="None" ErrorMessage="Stock Lead Time Variance Failure can not be more than 100 and can not be more than two decimal places." />
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtStockLeadTimeVarianceAbsFail" onkeyup="AllowDecimalTwoPlaceOnly(this);"
                                            MaxLength="6" runat="server" Width="40px"></cc1:ucNumericTextbox>
                                        <asp:RequiredFieldValidator ID="rfvtxtStockLeadTimeVarianceAbsFail" runat="server"
                                            ControlToValidate="txtStockLeadTimeVarianceAbsFail" ErrorMessage="Stock Lead Time Variance Absolute Fail Should not be blank."
                                            SetFocusOnError="true" Display="None" ValidationGroup="Apply">
                                        </asp:RequiredFieldValidator>
                                        <asp:RangeValidator Type="Double" ID="rvtxtStockLeadTimeVarianceAbsFail" runat="server"
                                            ControlToValidate="txtStockLeadTimeVarianceAbsFail" MaximumValue="100" MinimumValue="0"
                                            ValidationGroup="Apply" SetFocusOnError="true" Display="None" ErrorMessage="Maximum value of Stock Lead Time Variance Absolute Fail Should not be > 100." />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">
                                        <cc1:ucLabel ID="lblCheck_4" runat="server" Text="Check" isRequired="False" Tag=""></cc1:ucLabel>
                                    </td>
                                    <td style="font-weight: bold;">
                                        :
                                    </td>
                                    <td>
                                        <cc1:ucNumericTextbox ID="txtStockWeightCheckWeight" onkeyup="AllowDecimalOnly(this);"
                                            MaxLength="5" runat="server" Width="40px" ReadOnly="True"></cc1:ucNumericTextbox>%
                                        <%--onkeypress="return IsNumberKey(event, this);" MaxLength="3" runat="server" Width="40px"
                                            ReadOnly="True"></cc1:ucNumericTextbox>--%>
                                    </td>
                                    <td colspan="3">
                                    </td>
                                </tr>
                            </table>
                        </cc1:ucPanel>
                    </td>
                </tr>
                <tr align="center">
                    <td colspan="5">
                        <asp:CustomValidator ID="CustomValidator1" ControlToValidate="txtStockWeightCheckWeight"
                            ClientValidationFunction="ClientValidate" Display="None" runat="server" ValidationGroup="Apply" />
                        <cc1:ucButton ID="btnSave" runat="server" Text="Save" Style="width: 100px" CssClass="button"
                            OnClick="btnApply_Click" ValidationGroup="Apply" />
                        <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                            Style="color: Red" ValidationGroup="Apply" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="button-row">
        </div>
    </div>
</asp:Content>

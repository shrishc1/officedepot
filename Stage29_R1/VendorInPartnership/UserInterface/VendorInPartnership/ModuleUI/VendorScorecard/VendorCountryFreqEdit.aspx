﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="VendorCountryFreqEdit.aspx.cs" Inherits="VendorCountryFreqEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register Src="~/CommonUI/UserControls/MultiSelectVendor.ascx" TagName="MultiSelectVendor"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        function confirmDelete() {
            return confirm('<%=deleteMessage%>');
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblVendorCountrySetup" runat="server" Text="Vendor Country Frequency Setup"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
                <div>
                    <asp:RequiredFieldValidator ID="rfvVendorRequired" runat="server" ControlToValidate="lstRight"
                        Display="None" ValidationGroup="b"></asp:RequiredFieldValidator>
                    <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                        Style="color: Red" ValidationGroup="b" />
                </div>
                <div class="formbox">
                    <table width="79%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="width: 5%">
                            </td>
                            <td style="font-weight: bold;" width="28%">
                                <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;" width="5%" align="center">
                                :
                            </td>
                            <td style="font-weight: bold;" width="57%">
                                <cc1:ucDropdownList ID="ddlCountry" runat="server" Width="150px" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                </cc1:ucDropdownList>
                                <cc1:ucLabel ID="lblEditCountry" runat="server" Text=""></cc1:ucLabel>
                            </td>
                            <td style="width: 5%">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="font-weight: bold">
                                <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;" align="center">
                                :
                            </td>
                            <td>
                                <cc1:ucLabel ID="LBLEditFreqVendor" runat="server" Text=""></cc1:ucLabel>
                                <cc1:ucTextbox ID="txtVendorNo" runat="server" MaxLength="15" Width="90px"></cc1:ucTextbox>&nbsp;
                                <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendor" OnClick="btnSearch_Click" />&nbsp;
                                &nbsp;<cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendorNumber" OnClick="btnSearchByVendorNo_Click" />
                                <div id="pnlVendor" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                                <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="320px">
                                                </cc1:ucListBox>
                                            </td>
                                            <td align="center">
                                                <div>
                                                    <input type="button" id="Button1" value=">>" class="button" style="width: 35px" onclick="Javascript:MoveAll('<%= lstLeft.ClientID %>', '<%=lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                                </div>
                                                &nbsp;
                                                <div>
                                                    <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                                                        onclick="Javascript:MoveItem('<%=lstLeft.ClientID %>', '<%= lstRight.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                                </div>
                                                &nbsp;
                                                <div>
                                                    <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                                                        onclick="Javascript:MoveItem('<%= lstRight.ClientID %>', '<%=lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                                </div>
                                                &nbsp;
                                                <div>
                                                    <input type="button" id="btnMoveLeftAll" value="<<" class="button" style="width: 35px"
                                                        onclick="Javascript:MoveAll('<%= lstRight.ClientID %>', '<%=lstLeft.ClientID %>','<%= hiddenSelectedIDs.ClientID %>','<%= hiddenSelectedName.ClientID %>');" />
                                                </div>
                                            </td>
                                            <td>
                                                <cc1:ucListBox ID="lstRight" runat="server" Height="150px" Width="320px">
                                                </cc1:ucListBox>
                                                <asp:HiddenField ID="hiddenSelectedIDs" runat="server" />
                                                <asp:HiddenField ID="hiddenSelectedName" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <%--  <cc1:MultiSelectVendor runat="server" ID="msVendor" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="left" colspan="4">
                                <table cellpadding="0" cellspacing="0" width="95%">
                                    <tr>
                                        <td style="font-weight: bold;" width="40%">
                                            <cc1:ucLabel ID="lblFrequency" runat="server" Text="Please select preferred frequency for vendor"
                                                isRequired="true"></cc1:ucLabel>
                                        </td>
                                        <td style="font-weight: bold;" align="center" width="2%">
                                            :
                                        </td>
                                        <td style="font-weight: bold;" class="checkbox-list" width="15%">
                                            <asp:RadioButton ID="rdoMonthly" runat="server" Text="Monthly" GroupName="ReportType" />
                                        </td>
                                        <td style="font-weight: bold;" class="checkbox-list" width="15%">
                                            <asp:RadioButton ID="rdoQuarterly" runat="server" Text="Quarterly" GroupName="ReportType" />
                                        </td>
                                        <td style="font-weight: bold;" class="checkbox-list">
                                            <asp:RadioButton ID="rdoNever" runat="server" GroupName="ReportType" Checked="true"
                                                Text="Never" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
                    ValidationGroup="a" />
                <cc1:ucButton ID="btnDelete" runat="server" Text="Delete" CssClass="button" OnClick="btnDelete_Click"
                    OnClientClick="return confirmDelete();" />
                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
            </div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <asp:UpdateProgress runat="server" ID="PageUpdateProgress" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div align="center" style="background-color: #000; top: 0px; left: 0px; bottom: 0px;
                                    right: 0px; padding-top: 20%; margin: 0; width: 100%; height: 50%; overflow: hidden;
                                    position: absolute; z-index: 1000; filter: alpha(opacity=50); opacity: 0.5;">
                                    <asp:Image ID="imgWait" runat="server" ImageUrl="~/Images/Ajaxloading.gif" ImageAlign="Middle" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

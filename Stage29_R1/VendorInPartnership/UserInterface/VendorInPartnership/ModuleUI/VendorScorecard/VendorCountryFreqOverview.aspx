﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="VendorCountryFreqOverview.aspx.cs" Inherits="VendorCountryFreqOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>

<%@ Register src="../../CommonUI/UserControls/ucAddButton.ascx" tagname="ucAddButton" tagprefix="uc1" %>
<%@ Register src="../../CommonUI/UserControls/ucExportToExcel.ascx" tagname="ucExportToExcel" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
       <cc1:ucLabel ID="lblVendorCountrySetup" runat="server" Text="Vendor Country Frequency Setup"></cc1:ucLabel>
    </h2>
    <div class="button-row">
        <uc1:ucaddbutton ID="btnAdd" runat="server" 
            NavigateUrl="VendorCountryFreqEdit.aspx" />
        <uc2:ucexporttoexcel ID="ucExportToExcel1" runat="server" />
    </div>
    <table width="100%">
        <tr>
            <td align="center">
                <cc1:ucGridView ID="grdVendorCountryFreq" Width="100%" runat="server" CssClass="grid" onsorting="SortGrid" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Country" SortExpression="CountryName">
                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltCountryName" runat="server" Text='<%# Eval("CountryName") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                       

                          <asp:TemplateField HeaderText="Vendor" SortExpression="VendorName">
                            <HeaderStyle Width="25%" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink ID="hpDelivery" runat="server" Text='<%# Eval("VendorName") %>' NavigateUrl='<%# EncryptQuery("VendorCountryFreqEdit.aspx?VendorCountryFreqID="+ Eval("VendorCountryFreqID")) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Communication Frequency" SortExpression="gridFrequency">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <cc1:ucLiteral ID="ltCommunicationFrequency" runat="server" Text='<%# Eval("gridFrequency") %>'></cc1:ucLiteral>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </cc1:ucGridView>
            </td>
        </tr>
    </table>
</asp:Content>

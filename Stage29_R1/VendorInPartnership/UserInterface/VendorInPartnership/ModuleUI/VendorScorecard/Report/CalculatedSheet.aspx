﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CalculatedSheet.aspx.cs"
    Inherits="CalculatedSheet" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../../Css/chartstyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        body
        {
            font-size: 11px;
            font-family: Arial, Helvetica, sans-serif;
        }
        table td
        {
            font-size: 11px;
        }
        .grid
        {
            width: 1002px;
            margin: 0 auto;
            border: solid 1px #d1d1d1;
        }
        .grid th
        {
            padding: 5px 3px;
            background: url(headbg.jpg) repeat-x top #d4d4d4;
            border-right: solid 1px #d1d1d1;
        }
        .row td
        {
            border-bottom: solid 7px #d1d1d1;
            padding: 3px;
        }
        .rowborder
        {
            border-bottom: solid 7px #d1d1d1;
            padding: 3px;
        }
        .row2
        {
            border-bottom: solid 3px #d1d1d1;
            padding: 3px;
        }
        .row td tr td
        {
            border: none;
        }
        .line
        {
            border-bottom: solid 1px #595959 !important;
        }
        .col-form td
        {
            padding: 3px;
        }
        .border-right
        {
            border-right: solid 1px #d1d1d1;
        }
        .txt-rightalign td
        {
            padding-right: 3px;
        }
        .datalist
        {
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .datalist td
        {
            padding: 3px 10px;
        }
    </style>
</head>
<body>
    <div class="main-container" style="min-height: 500px; width: 1023px; margin: 0 auto;
        background: none!important;">
        <form id="form3" runat="server">
        <div class="main-container" style="min-height: 500px; width: 1021px; margin: 0 auto;
            background: none!important;">
            <%--<asp:HiddenField ID="hdnVendorId" runat="server" />--%>
            <a id="ancBacktoMainPage" class="back-to-mainpage" runat="server" style="margin-left: 8px;"></a>
            <br />
            <br />
            <br />
            <div class="grid">
                <table class="table1" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <th>
                            Area
                        </th>
                        <th>
                            Overall<br />
                            Weighting
                        </th>
                        <th>
                            What is being measure
                        </th>
                        <th>
                            How is it being measured
                        </th>
                        <th>
                            Ideal
                        </th>
                        <th>
                            Target
                        </th>
                        <th>
                            Under Target
                        </th>
                        <th>
                            Fail
                        </th>
                    </tr>
                    <tr class="row">
                        <td align="center" class="border-right">
                            On Time In Full<br />
                            (OTIF)
                        </td>
                        <td align="center" class="border-right">
                            60%
                        </td>
                        <td class="border-right">
                            What % of PO lines are delivered<br />
                            on time and in full by the due date
                        </td>
                        <td class="border-right">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="center" class="line">
                                        Number of Order Lines Delivered On time and in full
                                    </td>
                                    <td rowspan="2">
                                        x 100
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td>
                                        Total Number of orders lines
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" class="border-right">
                            100%
                        </td>
                        <td align="center" class="border-right">
                            98.50% to 99.99%
                        </td>
                        <td align="center" class="border-right">
                            80.00% to 98.49%
                        </td>
                        <td align="center" class="border-right">
                            79.99 to 40.00%
                        </td>
                    </tr>
                    <tr class="row">
                        <td align="center" class="border-right">
                            Scheduling
                        </td>
                        <td align="center" class="border-right">
                            15%
                        </td>
                        <td class="border-right">
                            The Scheduling Error Rate
                        </td>
                        <td class="border-right">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="center" class="line">
                                        Total Number of Scheduling Deliveries Planned
                                    </td>
                                    <td rowspan="2">
                                        x 100
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td>
                                        Total Number of Scheduling Issues Recorded
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" class="border-right">
                            0%
                        </td>
                        <td align="center" class="border-right">
                            0.01% to 1.49%
                        </td>
                        <td align="center" class="border-right">
                            1.50% to 3.00%
                        </td>
                        <td align="center" class="border-right">
                            >3.00%
                        </td>
                    </tr>
                    <tr class="row2">
                        <td rowspan="2" align="center" class="border-right rowborder">
                            Discrepancy
                        </td>
                        <td rowspan="2" class="border-right rowborder">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="txt-rightalign">
                                <tr>
                                    <td rowspan="2">
                                        10%
                                    </td>
                                    <td height="40" align="right">
                                        80%
                                    </td>
                                </tr>
                                <tr>
                                    <td height="40" align="right">
                                        20%
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="row2 border-right">
                            The Discrepancy Error Rate
                        </td>
                        <td class="row2 border-right">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="col-form">
                                <tr>
                                    <td align="center" class="line">
                                        Number of Purchase Order Lines Received
                                    </td>
                                    <td rowspan="2">
                                        x 100
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td>
                                        Total Number of Discrepancy Raised
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" class="row2 border-right">
                            0%
                        </td>
                        <td align="center" class="row2 border-right">
                            0.01% to 0.49%
                        </td>
                        <td align="center" class="row2 border-right">
                            0.50% to 1.8%
                        </td>
                        <td align="center" class="row2 border-right">
                            &gt;1.8%
                        </td>
                    </tr>
                    <tr class="row">
                        <td class="border-right">
                            How long it takes to respond to
                            <br />
                            and resolve discrepancy
                        </td>
                        <td align="center" class="border-right">
                            Elapsed time to response to a query
                        </td>
                        <td align="center" class="border-right">
                            1 day
                        </td>
                        <td align="center" class="border-right">
                            2 days
                        </td>
                        <td align="center" class="border-right">
                            3 - 4 days
                        </td>
                        <td align="center" class="border-right">
                            &gt; or = to<br />
                            5 days
                        </td>
                    </tr>
                    <tr class="row2">
                        <td rowspan="2" align="center" class="border-right" style="padding-left: 3px;">
                            Stock
                        </td>
                        <td rowspan="2" valign="top" class="border-right">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="txt-rightalign">
                                <tr>
                                    <td >
                                        15%
                                    </td>
                                    <td height="30" align="right">
                                        (100%)
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                        <td class="row2 border-right">
                            Lead Time Variance (days)
                        </td>
                        <td align="center" class="row2 border-right">
                            The average days variance &gt; Lead Time Days
                        </td>
                        <td align="center" class="row2 border-right">
                            0 day
                        </td>
                        <td align="center" class="row2 border-right">
                            1 day
                        </td>
                        <td align="center" class="row2 border-right">
                            2 days
                        </td>
                        <td align="center" class="row2 border-right">
                            &gt; or = 3<br />
                            days
                        </td>
                    </tr>
                 
                </table>
            </div>
            <div class="grid datalist">
                <table width="100%" cellpadding="0" cellspacing="0" class="datalist">
                    <tr>
                        <td colspan="2">
                            <strong>The maximum score you can achieve is 10. The scoring ranges are noted below</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="10%">
                            <strong>Ideal</strong>
                        </td>
                        <td width="90%">
                            - gets a score of 10
                        </td>
                    </tr>
                    <tr class="altcolor">
                        <td>
                            <strong>Above Target</strong>
                        </td>
                        <td>
                            - gets a score >7 but below 10
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Target</strong>
                        </td>
                        <td>
                            - gets a score of 7
                        </td>
                    </tr>
                    <tr class="altcolor">
                        <td>
                            <strong>Under Target </strong>
                        </td>
                        <td>
                            - get a score > 5 but less than 7
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Fail </strong>
                        </td>
                        <td>
                            - gets a score of 5 or less
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        </form>
    </div>
</body>
<script src="../../../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="../../../Scripts/json2.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    $(function () {
        var vscMainURL = '<%= vscMainURL %>';
        $("#ancBacktoMainPage").attr('href', vscMainURL);
    });
</script>
</html>

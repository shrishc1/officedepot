﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SchedulingScoreCard : CommonPage
{
    protected string vscURL = string.Empty;
    protected int vendorId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetQueryStringValue("vendorId") != null)
        {
            vscURL = EncryptURL(string.Format("VendorScoreCard.aspx?vendorId={0}", GetQueryStringValue("vendorId")));
            vendorId = Convert.ToInt32(GetQueryStringValue("vendorId"));
        }

        if (Request.QueryString["vendorId"] != null)
        {
            vscURL = EncryptURL(string.Format("VendorScoreCard.aspx?vendorId={0}", Request.QueryString["vendorId"]));
            vendorId = Convert.ToInt32(Request.QueryString["vendorId"]);
            ancBacktoMainPage.Visible = false;
        }
    }
}
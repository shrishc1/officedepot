﻿using BaseControlLibrary;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;


public partial class ModuleUI_Scorecard_Report_Scorecard : CommonPage
{
    #region ReportVariables

    Warning[] warnings;
    string[] streamIds;
    string mimeType = string.Empty;
    string encoding = string.Empty;
    string extension = string.Empty;
    string reportFileName = string.Empty;

    #endregion

    DataSet dtScoreCardRPT = new DataSet();
    DataSet dtScoreCardRPTExport = new DataSet();
    DataSet dtScoreCardRPTExportOTIF = new DataSet();
    DataSet dtScoreCardRPTExportScheduling = new DataSet();
    DataSet dtScoreCardRPTExportDiscrepancies = new DataSet();
    DataSet dtScoreCardRPTExportStock = new DataSet();
    DataSet dtOverallSchedulingErrorRate = new DataSet();
    DataSet dtOrderInformation = new DataSet();
    string HistroicPerformance = string.Empty;
    protected string strMustSelectVendorName = WebCommon.getGlobalResourceValue("MustSelectVendorName");
    const int intDtScoreCardRPTCount = 14;
    const string strLocal = "Local";
    const string strGlobal = "Global";

    public string ReportType
    {
        get
        {
            return Convert.ToString(ViewState["ReportType"]);
        }
        set { ViewState["ReportType"] = value; }
    }

    public int MasterParentID
    {
        get
        {
            return Convert.ToInt32(ViewState["MasterParentID"]);
        }
        set { ViewState["MasterParentID"] = value; }
    }

    protected void Page_InIt(object sender, EventArgs e)
    {

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Session["Role"] != null && Session["Role"].ToString() != "")
            {
                string Role = Session["Role"].ToString().Trim().ToLower();
                if (Role == "vendor")
                {
                    UcPanelHeader.Visible = true;
                    UcPanel1.Visible = false;
                    UcPanel2.Visible = false;

                    DataTable dt = GetLastYears();
                    grdReportLinksVendor.DataSource = dt;
                    grdReportLinksVendor.DataBind();

                    this.BindVendor();
                    ddlLocalMonthly.Visible = false;
                    ddlMasterMonthly.Visible = false;
                }
                else
                {
                    UcPanelHeader.Visible = false;
                    UcPanel1.Visible = true;
                    UcPanel2.Visible = false;

                    DataTable dt = GetLastYears();
                    grdReportLinks.DataSource = dt;
                    grdReportLinks.DataBind();

                    ddlLocalMonthly.Visible = true;
                    if (ddlLocalMonthly.Items.Count > 0)
                        ddlLocalMonthly.SelectedIndex = 0;

                    ddlMasterMonthly.Visible = true;
                    if (ddlMasterMonthly.Items.Count > 0)
                        ddlMasterMonthly.SelectedIndex = 0;
                }
            }
        }
    }

    #region Method

    private DataTable GetLastYears()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("Year");

        for (int i = DateTime.Now.Year; i >= 2018; i--)
        {
            if (i == DateTime.Now.Year)
            {
                if (DateTime.Now.Month > 1)
                {
                    DataRow dr = dt.NewRow();
                    dr["Year"] = i;
                    dt.Rows.Add(dr);
                }
            }
            else
            {
                DataRow dr = dt.NewRow();
                dr["Year"] = i;
                dt.Rows.Add(dr);
            }
        }
        return dt;
    }

    protected void btnView_Click(object sender, CommandEventArgs e)
    {

        LinkButton Yearlink = (LinkButton)sender;
        string Year = Yearlink.Text;
        string ReportName = string.Empty;
        string ReportOutputPath = ConfigurationManager.AppSettings["ScoreCardReportPath"];

        if (e.CommandName == "ScoreCardReport")
        {
            if (rdoAllOnly.Checked)
                ReportName = "All";
            else if (rdoEuropeanVendorOnly.Checked)
                ReportName = "European";
            else if (rdoLocalVendorOnly.Checked)
                ReportName = "Local";

            if (rdoOverallScore.Checked)
                ReportName += "_OverallScore";
            if (rdoOTIF.Checked)
                ReportName += "_OTIF";
            if (rdoDiscrepancies.Checked)
                ReportName += "_Discrepancies";
            if (rdoScheduling.Checked)
                ReportName += "_Scheduling";
            if (rdoStock.Checked)
                ReportName += "_Stock";
        }
        else if (e.CommandName == "ScoreCardReportVendor")
        {
            ReportName = Convert.ToString(MasterParentID);
        }

        ReportOutputPath = ReportOutputPath + Year + "\\" + ReportName + "_" + Year + ".xls";

        if (File.Exists(ReportOutputPath))
        {
            FileStream sourceFile = new FileStream(ReportOutputPath, FileMode.Open);
            float FileSize;
            FileSize = sourceFile.Length;
            byte[] getContent = new byte[(int)FileSize];
            sourceFile.Read(getContent, 0, (int)sourceFile.Length);
            sourceFile.Close();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", getContent.Length.ToString());
            Response.AddHeader("Content-Disposition", "attachment; filename=" + ReportName + "_" + Year + ".xls");
            Response.BinaryWrite(getContent);
            Response.Flush();
            Response.End();
        }
        else
        {
            string ExcelFileNotExist = WebCommon.getGlobalResourceValue("ExcelFileNotExist");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + ExcelFileNotExist + "')", true);
            return;
        }
    }

    protected int? getVendorID()
    {
        int? VendorID = null;
        if (Session["Role"] != null && Session["Role"].ToString() != "")
        {
            string Role = Session["Role"].ToString().Trim().ToLower();
            if (Role == "vendor")
            {
                VendorID = Convert.ToInt32(hdnSeletedVendorID.Value);
            }
            else
            {
                switch (ReportType)
                {
                    case "Global":
                        VendorID = Convert.ToInt32(hdnGlobalvendorID.Value);
                        break;
                    case "Local":
                        VendorID = Convert.ToInt32(hdnLocalvendorID.Value);
                        break;
                }
            }
        }
        return VendorID;
    }

    protected void generateReport()
    {
        int? iVendorID = getVendorID();
        if (iVendorID == 0)
        {
            return;
        }

        SqlConnection objSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["sConn"].ToString());
        objSqlConnection.Open();
        SqlCommand objSqlCommand2 = new SqlCommand("spRPT_ScoreCard", objSqlConnection);
        objSqlCommand2.Parameters.Add("@Action", SqlDbType.VarChar).Value = "checkSetting";
        objSqlCommand2.CommandType = CommandType.StoredProcedure;
        objSqlCommand2.CommandTimeout = 4200;
        string ScoreCardMessage = WebCommon.getGlobalResourceValue("ScoreCardMessage");
        if (objSqlCommand2.ExecuteScalar().ToString() == "0")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + ScoreCardMessage + "')", true);
            return;
        }

        UcPanel1.Visible = false;
        UcPanel2.Visible = true;

        SqlCommand objSqlCommand3 = new SqlCommand("spRPT_ScoreCard", objSqlConnection);
        objSqlCommand3.Parameters.Add("@Action", SqlDbType.VarChar).Value = "ScoreCardRPT";
        objSqlCommand3.Parameters.Add("@VendorID", SqlDbType.Int).Value = iVendorID;
        objSqlCommand3.CommandType = CommandType.StoredProcedure;
        objSqlCommand3.CommandTimeout = 4200;
        SqlDataAdapter objSqlDataAdapter3 = new SqlDataAdapter(objSqlCommand3);
        objSqlDataAdapter3.Fill(dtScoreCardRPT);

        ReportDataSource rdsScoreCardRPT = new ReportDataSource();
        ReportDataSource rdsScoreCardChart = new ReportDataSource();
        ReportDataSource rdsReceipt = new ReportDataSource();

        if (dtScoreCardRPT.Tables[0].Columns.Count.Equals(intDtScoreCardRPTCount))
        {
            rdsScoreCardRPT = new ReportDataSource("dtScoreCardRPT", dtScoreCardRPT.Tables[0]);
            rdsScoreCardChart = new ReportDataSource("dtScoreCardChartMonthly", dtScoreCardRPT.Tables[1]);
            rdsReceipt = new ReportDataSource("dtReceiptMonthly", dtScoreCardRPT.Tables[2]);
            ScorecardReportViewer.LocalReport.ReportPath = Server.MapPath("~") + "\\ModuleUI\\VendorScorecard\\Report\\ScorecardMonthly.rdlc";
        }
        else
        {
            rdsScoreCardRPT = new ReportDataSource("dtScoreCardQuaterlyRPT", dtScoreCardRPT.Tables[0]);
            rdsScoreCardChart = new ReportDataSource("dtScoreCardChartQuarterly", dtScoreCardRPT.Tables[1]);
            rdsReceipt = new ReportDataSource("dtReceiptQuarter", dtScoreCardRPT.Tables[2]);
            ScorecardReportViewer.LocalReport.ReportPath = Server.MapPath("~") + "\\ModuleUI\\VendorScorecard\\Report\\ScorecardQuarterly.rdlc";
        }



        this.ScorecardReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SetSubDataSource);

        ScorecardReportViewer.Height = 825;
        ReportParameter[] reportParameter = new ReportParameter[2];
        reportParameter[0] = new ReportParameter("Vendor", iVendorID.ToString());
        reportParameter[1] = new ReportParameter("Action", "ScoreCardRPT");
        ScorecardReportViewer.LocalReport.SetParameters(reportParameter);

        ScorecardReportViewer.LocalReport.DataSources.Clear();
        ScorecardReportViewer.LocalReport.DataSources.Add(rdsScoreCardRPT);
        ScorecardReportViewer.LocalReport.DataSources.Add(rdsScoreCardChart);
        ScorecardReportViewer.LocalReport.DataSources.Add(rdsReceipt);
    }

    #endregion

    #region Events




    #endregion

    protected void ScorecardReportViewer_Drillthrough(object sender, DrillthroughEventArgs e)
    {
        string[] Parameter = new string[4];

        int count = 0;
        ReportParameterInfoCollection DrillThroughValues = e.Report.GetParameters();
        foreach (ReportParameterInfo d in DrillThroughValues)
        {
            Parameter[count] = d.Values[0].ToString().Trim();
            count++;
        }

        LocalReport localreport = (LocalReport)e.Report;
        SqlConnection objSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["sConn"].ToString());
        objSqlConnection.Open();

        HistroicPerformance = Parameter[1];
        if (Parameter[1].ToString() == "Scheduling")
            Parameter[1] = "SchedulingRPT";
        else if (Parameter[1].ToString() == "Discrepancies")
            Parameter[1] = "DiscRPT";
        else if (Parameter[1].ToString() == "On Time In Full")
            Parameter[1] = "OTIFRPT";
        else if (Parameter[1].ToString() == "Stock")
            Parameter[1] = "StockRPT";

        SqlCommand objSqlCommand3 = new SqlCommand("spRPT_ScoreCard", objSqlConnection);
        objSqlCommand3.Parameters.Add("@Action", SqlDbType.VarChar).Value = Parameter[1].ToString();
        objSqlCommand3.Parameters.Add("@VendorID", SqlDbType.Int).Value = Convert.ToInt32(Parameter[0]);
        objSqlCommand3.CommandType = CommandType.StoredProcedure;
        objSqlCommand3.CommandTimeout = 4200;
        SqlDataAdapter objSqlDataAdapter3 = new SqlDataAdapter(objSqlCommand3);
        objSqlDataAdapter3.Fill(dtScoreCardRPT);

        ReportDataSource rdsScoreCardRPT = new ReportDataSource();
        ReportDataSource rdsScoreCardChart = new ReportDataSource();
        ReportDataSource rdsReceipt = new ReportDataSource();
        ReportDataSource rdsOverallSchedulingErrorRate = new ReportDataSource();
        ReportDataSource rdsLinesAndErrors = new ReportDataSource();
        ReportDataSource rdsResponseTime = new ReportDataSource();
        ReportDataSource rdsOrderInformation = new ReportDataSource();
        ReportDataSource rdsOtifRate = new ReportDataSource();

        if (HistroicPerformance == "Scheduling")
        {

            rdsScoreCardChart = new ReportDataSource("dtScoreCardChartMonthlyScheduling", dtScoreCardRPT.Tables[1]);
            rdsScoreCardRPT = new ReportDataSource("dtSchedulingRPT", dtScoreCardRPT.Tables[0]);
            rdsOverallSchedulingErrorRate = new ReportDataSource("dtOverallSchedulingErrorRate", dtScoreCardRPT.Tables[2]);

        }
        else if (HistroicPerformance == "Discrepancies")
        {
            rdsScoreCardChart = new ReportDataSource("dtScoreCardChartMonthlyDiscrepencies", dtScoreCardRPT.Tables[3]);
            rdsLinesAndErrors = new ReportDataSource("dtLinesAndErrors", dtScoreCardRPT.Tables[1]);
            rdsResponseTime = new ReportDataSource("dtResponseTime", dtScoreCardRPT.Tables[2]);
            rdsScoreCardRPT = new ReportDataSource("dtDiscRPT", dtScoreCardRPT.Tables[0]);
        }
        else if (HistroicPerformance == "On Time In Full")
        {
            rdsScoreCardRPT = new ReportDataSource("dtOTIFRPT", dtScoreCardRPT.Tables[0]);
            rdsScoreCardChart = new ReportDataSource("dtScoreCardChartMonthly", dtScoreCardRPT.Tables[1]);
            rdsOrderInformation = new ReportDataSource("dtOrderInformation", dtScoreCardRPT.Tables[2]);
            rdsOtifRate = new ReportDataSource("DataSet1", dtScoreCardRPT.Tables[3]);
        }
        else if (HistroicPerformance == "Stock")
        {
            rdsScoreCardChart = new ReportDataSource("dtScoreCardChartMonthlyStock", dtScoreCardRPT.Tables[1]);
            rdsScoreCardRPT = new ReportDataSource("dtStockRPT", dtScoreCardRPT.Tables[0]);
            localreport.SubreportProcessing += new SubreportProcessingEventHandler(SetSubDataSource);
        }
        else
        {
            if (dtScoreCardRPT.Tables[0].Columns.Count.Equals(intDtScoreCardRPTCount))
            {
                rdsScoreCardRPT = new ReportDataSource("dtScoreCardRPT", dtScoreCardRPT.Tables[0]);
                rdsScoreCardChart = new ReportDataSource("dtScoreCardChartMonthly", dtScoreCardRPT.Tables[1]);
                rdsReceipt = new ReportDataSource("dtReceiptMonthly", dtScoreCardRPT.Tables[2]);
                localreport.SubreportProcessing += new SubreportProcessingEventHandler(SetSubDataSource);
            }
            else
            {
                rdsScoreCardRPT = new ReportDataSource("dtScoreCardQuaterlyRPT", dtScoreCardRPT.Tables[0]);
                rdsScoreCardChart = new ReportDataSource("dtScoreCardChartQuarterly", dtScoreCardRPT.Tables[1]);
                rdsReceipt = new ReportDataSource("dtReceiptQuarter", dtScoreCardRPT.Tables[2]);
                localreport.SubreportProcessing += new SubreportProcessingEventHandler(SetSubDataSource);
            }
        }
        localreport.DataSources.Clear();
        localreport.DataSources.Add(rdsScoreCardRPT);
        localreport.DataSources.Add(rdsScoreCardChart);
        localreport.DataSources.Add(rdsReceipt);
        localreport.DataSources.Add(rdsOverallSchedulingErrorRate);
        localreport.DataSources.Add(rdsLinesAndErrors);
        localreport.DataSources.Add(rdsResponseTime);
        localreport.DataSources.Add(rdsOrderInformation);
        localreport.DataSources.Add(rdsOtifRate);

        localreport.Refresh();

    }

    private void NewGenerateReport(string reportType)
    {
        ReportType = reportType;
        var vendorId = getVendorID();
        if (vendorId.Equals(0))
        {
            return;
        }

        var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["sConn"].ToString());
        sqlConnection.Open();
        var sqlCommand = new SqlCommand("spRPT_ScoreCard", sqlConnection);
        sqlCommand.Parameters.Add("@Action", SqlDbType.VarChar).Value = "checkSetting";
        sqlCommand.CommandType = CommandType.StoredProcedure;
        sqlCommand.CommandTimeout = 4200;
        var ScoreCardMessage = WebCommon.getGlobalResourceValue("ScoreCardMessage");
        if (Convert.ToString(sqlCommand.ExecuteScalar()).Equals("0"))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + ScoreCardMessage + "')", true);
            return;
        }
        else
        {
            EncryptQueryString("~/ModuleUI/VendorScoreCard/Report/VendorScoreCardMainNew2.aspx?vendorId=" + Convert.ToString(vendorId) + "&ValidVendor=true");
        }
    }

    protected void btnGenerateReport_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(Session["Role"]).Equals("vendor") || Convert.ToString(Session["Role"]).Equals("carrier"))
        {
            this.NewGenerateReport(strLocal);
        }
        else
        {
            if (ddlLocalMonthly.SelectedIndex.Equals(0))
            {
                this.NewGenerateReport(strLocal);
            }
            else
            {
                ReportType = strLocal;
                generateReport();
            }
        }
    }

    protected void btnGenerateReport_1_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(Session["Role"]).Equals("vendor") || Convert.ToString(Session["Role"]).Equals("carrier"))
        {
            this.NewGenerateReport(strGlobal);
        }
        else
        {
            if (ddlMasterMonthly.SelectedIndex.Equals(0))
            {
                this.NewGenerateReport(strGlobal);
            }
            else
            {
                ReportType = strGlobal;
                generateReport();
            }
        }

        #region Commented code ...
        #endregion
    }

    protected void btnGenerateReport_2_Click(object sender, EventArgs e)
    {
        UcPanelHeader.Visible = false;
        UcPanel2.Visible = true;
        this.NewGenerateReport(strGlobal);
    }

    public void SetSubDataSource(object sender, SubreportProcessingEventArgs e)
    {
        if (dtScoreCardRPT.Tables[0].Columns.Count.Equals(intDtScoreCardRPTCount))
        {
            e.DataSources.Add(new ReportDataSource("dtScoreCardRPT", dtScoreCardRPT.Tables[0]));
        }
        else
        {
            e.DataSources.Add(new ReportDataSource("dtScoreCardQuaterlyRPT", dtScoreCardRPT.Tables[0]));
        }
        if (HistroicPerformance == "Stock")
        {
            e.DataSources.Add(new ReportDataSource("dtStockRPT", dtScoreCardRPT.Tables[0]));
        }

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("VendorScoreCardSearch.aspx");
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        int? iVendorID = getVendorID();

        SqlConnection objSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["sConn"].ToString());
        objSqlConnection.Open();
        SqlCommand objSqlCommand3 = new SqlCommand("spRPT_ScoreCard", objSqlConnection);
        objSqlCommand3.Parameters.Add("@Action", SqlDbType.VarChar).Value = "ScoreCardRPT";
        objSqlCommand3.Parameters.Add("@VendorID", SqlDbType.Int).Value = iVendorID;
        objSqlCommand3.CommandType = CommandType.StoredProcedure;
        objSqlCommand3.CommandTimeout = 4200;
        SqlDataAdapter objSqlDataAdapter3 = new SqlDataAdapter(objSqlCommand3);
        objSqlDataAdapter3.Fill(dtScoreCardRPTExport);

        SqlCommand objSqlCommand1 = new SqlCommand("spRPT_ScoreCard", objSqlConnection);
        objSqlCommand1.Parameters.Add("@Action", SqlDbType.VarChar).Value = "OTIFRPT";
        objSqlCommand1.Parameters.Add("@VendorID", SqlDbType.Int).Value = iVendorID;
        objSqlCommand1.CommandType = CommandType.StoredProcedure;
        objSqlCommand1.CommandTimeout = 4200;
        SqlDataAdapter objSqlDataAdapter1 = new SqlDataAdapter(objSqlCommand1);
        objSqlDataAdapter1.Fill(dtScoreCardRPTExportOTIF);

        SqlCommand objSqlCommand2 = new SqlCommand("spRPT_ScoreCard", objSqlConnection);
        objSqlCommand2.Parameters.Add("@Action", SqlDbType.VarChar).Value = "SchedulingRPT";
        objSqlCommand2.Parameters.Add("@VendorID", SqlDbType.Int).Value = iVendorID;
        objSqlCommand2.CommandType = CommandType.StoredProcedure;
        objSqlCommand2.CommandTimeout = 4200;
        SqlDataAdapter objSqlDataAdapter2 = new SqlDataAdapter(objSqlCommand2);
        objSqlDataAdapter2.Fill(dtScoreCardRPTExportScheduling);

        SqlCommand objSqlCommand5 = new SqlCommand("spRPT_ScoreCard", objSqlConnection);
        objSqlCommand5.Parameters.Add("@Action", SqlDbType.VarChar).Value = "SchedulingRPT";
        objSqlCommand5.Parameters.Add("@VendorID", SqlDbType.Int).Value = iVendorID;
        objSqlCommand5.CommandType = CommandType.StoredProcedure;
        objSqlCommand5.CommandTimeout = 4200;
        SqlDataAdapter objSqlDataAdapter5 = new SqlDataAdapter(objSqlCommand5);
        objSqlDataAdapter5.Fill(dtOverallSchedulingErrorRate);

        SqlCommand objSqlCommand4 = new SqlCommand("spRPT_ScoreCard", objSqlConnection);
        objSqlCommand4.Parameters.Add("@Action", SqlDbType.VarChar).Value = "DiscRPT";
        objSqlCommand4.Parameters.Add("@VendorID", SqlDbType.Int).Value = iVendorID;
        objSqlCommand4.CommandType = CommandType.StoredProcedure;
        objSqlCommand4.CommandTimeout = 4200;
        SqlDataAdapter objSqlDataAdapter4 = new SqlDataAdapter(objSqlCommand4);
        objSqlDataAdapter4.Fill(dtScoreCardRPTExportDiscrepancies);

        SqlCommand objSqlCommand7 = new SqlCommand("spRPT_ScoreCard", objSqlConnection);
        objSqlCommand7.Parameters.Add("@Action", SqlDbType.VarChar).Value = "StockRPT";
        objSqlCommand7.Parameters.Add("@VendorID", SqlDbType.Int).Value = iVendorID;
        objSqlCommand7.CommandType = CommandType.StoredProcedure;
        objSqlCommand7.CommandTimeout = 4200;
        SqlDataAdapter objSqlDataAdapter7 = new SqlDataAdapter(objSqlCommand7);
        objSqlDataAdapter7.Fill(dtScoreCardRPTExportStock);

        ExportReportViewer.LocalReport.ReportPath = dtScoreCardRPTExport.Tables[0].Columns.Count.Equals(intDtScoreCardRPTCount)
            ? Server.MapPath("~") + "\\ModuleUI\\VendorScorecard\\Report\\ExportScorecardMonthly.rdlc"
            : Server.MapPath("~") + "\\ModuleUI\\VendorScorecard\\Report\\ExportScorecardQuarterly.rdlc";
        this.ExportReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SetSubDataSourceExport);

        ReportParameter[] reportParameter = new ReportParameter[2];
        reportParameter[0] = new ReportParameter("Vendor", iVendorID.ToString());
        reportParameter[1] = new ReportParameter("Action", "ScoreCardRPT");
        ExportReportViewer.LocalReport.SetParameters(reportParameter);
        ExportReportViewer.Visible = true;

        byte[] bytes = ExportReportViewer.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "attachment; filename = " + "ScoreCardRPT" + "." + extension);
        Response.BinaryWrite(bytes);
        Response.Flush();


    }

    public void SetSubDataSourceExport(object sender, SubreportProcessingEventArgs e)
    {
        if (dtScoreCardRPTExport.Tables[0].Columns.Count.Equals(intDtScoreCardRPTCount))
        {
            e.DataSources.Add(new ReportDataSource("dtScoreCardRPT", dtScoreCardRPTExport.Tables[0]));
            e.DataSources.Add(new ReportDataSource("dtScoreCardChartMonthly", dtScoreCardRPTExport.Tables[1]));
            e.DataSources.Add(new ReportDataSource("dtReceiptMonthly", dtScoreCardRPTExport.Tables[2]));
            e.DataSources.Add(new ReportDataSource("dtScoreCardChartMonthly", dtScoreCardRPTExportOTIF.Tables[1]));
            e.DataSources.Add(new ReportDataSource("dtOTIFRPT", dtScoreCardRPTExportOTIF.Tables[0]));
            e.DataSources.Add(new ReportDataSource("dtOrderInformation", dtScoreCardRPTExportOTIF.Tables[2]));
            e.DataSources.Add(new ReportDataSource("DataSet1", dtScoreCardRPTExportOTIF.Tables[3]));
            e.DataSources.Add(new ReportDataSource("dtScoreCardChartMonthlyScheduling", dtScoreCardRPTExportScheduling.Tables[1]));
            e.DataSources.Add(new ReportDataSource("dtSchedulingRPT", dtScoreCardRPTExportScheduling.Tables[0]));
            e.DataSources.Add(new ReportDataSource("dtOverallSchedulingErrorRate", dtOverallSchedulingErrorRate.Tables[2]));
            e.DataSources.Add(new ReportDataSource("dtScoreCardChartMonthlyDiscrepencies", dtScoreCardRPTExportDiscrepancies.Tables[3]));
            e.DataSources.Add(new ReportDataSource("dtLinesAndErrors", dtScoreCardRPTExportDiscrepancies.Tables[1]));
            e.DataSources.Add(new ReportDataSource("dtResponseTime", dtScoreCardRPTExportDiscrepancies.Tables[2]));
            e.DataSources.Add(new ReportDataSource("dtDiscRPT", dtScoreCardRPTExportDiscrepancies.Tables[0]));
            e.DataSources.Add(new ReportDataSource("dtScoreCardChartMonthlyStock", dtScoreCardRPTExportStock.Tables[1]));
            e.DataSources.Add(new ReportDataSource("dtStockRPT", dtScoreCardRPTExportStock.Tables[0]));


        }
        else
        {
            e.DataSources.Add(new ReportDataSource("dtScoreCardQuaterlyRPT", dtScoreCardRPTExport.Tables[0]));
            e.DataSources.Add(new ReportDataSource("dtScoreCardChartQuarterly", dtScoreCardRPTExport.Tables[1]));
            e.DataSources.Add(new ReportDataSource("dtReceiptQuarter", dtScoreCardRPTExport.Tables[2]));
            e.DataSources.Add(new ReportDataSource("dtScoreCardChartMonthly", dtScoreCardRPTExportOTIF.Tables[1]));
            e.DataSources.Add(new ReportDataSource("dtOTIFRPT", dtScoreCardRPTExportOTIF.Tables[0]));
            e.DataSources.Add(new ReportDataSource("dtOrderInformation", dtOrderInformation.Tables[0]));
            e.DataSources.Add(new ReportDataSource("dtScoreCardChartMonthlyScheduling", dtScoreCardRPTExportScheduling.Tables[1]));
            e.DataSources.Add(new ReportDataSource("dtSchedulingRPT", dtScoreCardRPTExportScheduling.Tables[0]));
            e.DataSources.Add(new ReportDataSource("dtOverallSchedulingErrorRate", dtOverallSchedulingErrorRate.Tables[2]));
            e.DataSources.Add(new ReportDataSource("dtScoreCardChartMonthlyDiscrepencies", dtScoreCardRPTExportDiscrepancies.Tables[3]));
            e.DataSources.Add(new ReportDataSource("dtLinesAndErrors", dtScoreCardRPTExportDiscrepancies.Tables[1]));
            e.DataSources.Add(new ReportDataSource("dtResponseTime", dtScoreCardRPTExportDiscrepancies.Tables[2]));
            e.DataSources.Add(new ReportDataSource("dtDiscRPT", dtScoreCardRPTExportDiscrepancies.Tables[0]));
            e.DataSources.Add(new ReportDataSource("dtScoreCardChartMonthlyStock", dtScoreCardRPTExportStock.Tables[1]));
            e.DataSources.Add(new ReportDataSource("dtStockRPT", dtScoreCardRPTExportStock.Tables[0]));
        }

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SearchVendor(ref lstLeft, txtVendorNo.Text, "Local");
    }

    protected void btnSearch_1_Click(object sender, EventArgs e)
    {
        SearchVendor(ref lstGlobalVendor, txtVendorName.Text, "Global");
    }

    private void SearchVendor(ref ucListBox lstList, string VendorName, string ReportType)
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetConsVendorByName";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["Userid"]);
        oUP_VendorBE.VendorName = VendorName;
        if (ReportType == "Local")
            oUP_VendorBE.VendorFlag = 'P';
        else if (ReportType == "Global")
            oUP_VendorBE.VendorFlag = 'G';

        List<UP_VendorBE> lstUPVendor = oUP_VendorBAL.GetVendorByUserCountryBAL(oUP_VendorBE);

        if (lstUPVendor.Count > 0)
        {

            FillControls.FillListBox(ref lstList, lstUPVendor, "VendorName", "VendorID");

            for (int i = 0; i < lstUPVendor.Count; i++)
            {
                if (lstUPVendor[i].VendorID > 0 && Convert.ToInt32(lstList.Items[i].Value) == lstUPVendor[i].VendorID && lstUPVendor[i].IsActiveVendor == "N")
                    lstList.Items[i].Attributes.Add("Style", "background-color:RED;");
            }
        }
        else
        {
            lstList.Items.Clear();
        }
    }

    private void BindVendor()
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetAllLevelVendorDetailsByUserIDNew";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["UserID"]);
        oUP_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
        oUP_VendorBE.Site.SiteID = 0;

        List<UP_VendorBE> lstUPVendor = oUP_VendorBAL.GetVendorByUserIdBAL(oUP_VendorBE);

        List<UP_VendorBE> lstUPVendorName ;
        if (lstUPVendor != null && lstUPVendor.Count > 0)
        {
            lstUPVendorName = lstUPVendor.FindAll(delegate (UP_VendorBE v) { return Convert.ToString(v.VendorFlag) == "G"; });
            if (lstUPVendorName != null && lstUPVendorName.Count > 0)
                MasterParentID = lstUPVendorName[0].VendorID;
            else
            {
                lstUPVendorName = lstUPVendor.FindAll(delegate (UP_VendorBE v) { return Convert.ToString(v.VendorFlag).Equals("P") && v.ParentVendorID == -1; });
                if (lstUPVendorName != null && lstUPVendorName.Count > 0)
                    MasterParentID = lstUPVendorName[0].VendorID;
                else
                {
                    lstUPVendorName = lstUPVendor.FindAll(delegate (UP_VendorBE v) { return v.VendorFlag == (char?)null && v.ParentVendorID == -1; });
                    if (lstUPVendorName != null && lstUPVendorName.Count > 0)
                        MasterParentID = lstUPVendorName[0].VendorID;
                }
            }
        }
        FillControls.FillListBox(ref lstVendorslist, lstUPVendor, "VendorName", "VendorID");
    }

    protected void btnSearchByVendorNo_Click(object sender, EventArgs e)
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetAllLevelVendorDetailsByVendorNo";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["Userid"]);
        oUP_VendorBE.Vendor_No = txtVendorNo.Text;
        ReportType = "Local";
        if (ReportType == "Local")
            oUP_VendorBE.VendorFlag = 'P';
        else if (ReportType == "Global")
            oUP_VendorBE.VendorFlag = 'G';



        List<UP_VendorBE> lstUPVendor = oUP_VendorBAL.GetVendorByUserCountryBAL(oUP_VendorBE);
        if (lstUPVendor.Count > 0)
        {
            txtVendorNo.Text = oUP_VendorBE.Vendor_No;
            FillControls.FillListBox(ref lstLeft, lstUPVendor, "VendorName", "VendorID");

            for (int i = 0; i < lstUPVendor.Count; i++)
            {
                if (lstUPVendor[i].VendorID > 0)
                    if (Convert.ToInt32(lstLeft.Items[i].Value) == lstUPVendor[i].VendorID && lstUPVendor[i].IsActiveVendor == "N")
                        this.lstLeft.Items[i].Attributes.Add("Style", "background-color:RED;");
            }
        }
        else
        {
            lstLeft.Items.Clear();
        }

    }
    protected void btnSearchByVendorNo1_Click(object sender, EventArgs e)
    {
        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
        UP_VendorBE oUP_VendorBE = new UP_VendorBE();

        oUP_VendorBE.Action = "GetAllLevelVendorDetailsByVendorNo";
        oUP_VendorBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oUP_VendorBE.User.UserID = Convert.ToInt32(Session["Userid"]);
        oUP_VendorBE.Vendor_No = txtVendorName.Text;
        ReportType = "Global";
        if (ReportType == "Local")
            oUP_VendorBE.VendorFlag = 'P';
        else if (ReportType == "Global")
            oUP_VendorBE.VendorFlag = 'G';



        List<UP_VendorBE> lstUPVendor = oUP_VendorBAL.GetVendorByUserCountryBAL(oUP_VendorBE);
        if (lstUPVendor.Count > 0)
        {
            txtVendorName.Text = oUP_VendorBE.Vendor_No;
            FillControls.FillListBox(ref lstGlobalVendor, lstUPVendor, "VendorName", "VendorID");

            for (int i = 0; i < lstUPVendor.Count; i++)
            {
                if (lstUPVendor[i].VendorID > 0 && Convert.ToInt32(lstGlobalVendor.Items[i].Value) == lstUPVendor[i].VendorID && lstUPVendor[i].IsActiveVendor == "N")
                    this.lstGlobalVendor.Items[i].Attributes.Add("Style", "background-color:RED;");
            }
        }
        else
        {
            lstGlobalVendor.Items.Clear();
        }
    }
    protected void grdReportLinks_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lb = e.Row.FindControl("lnkReport") as LinkButton;
            ScriptManager.GetCurrent(this).RegisterPostBackControl(lb);
        }
    }
}
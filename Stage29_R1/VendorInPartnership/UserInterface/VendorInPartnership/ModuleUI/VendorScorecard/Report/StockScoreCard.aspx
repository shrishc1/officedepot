﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StockScoreCard.aspx.cs" Inherits="StockScoreCard"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../../Css/chartstyle.css" rel="stylesheet" type="text/css" />
    
    <script src="../../../Scripts/loader.js"></script>
    <style>
        .google-visualization-table-table tr td:first-child {
            width: 70px;
        }
    </style>
</head>
<body>
    <form id="form7" runat="server">
        <div class="main-container">
            <a id="ancBacktoMainPage" class="back-to-mainpage" runat="server"></a>
            <%--<asp:HyperLink ID="hlVendorScoreCardMain" Text="Main Page" NavigateUrl="~/ModuleUI/VendorScorecard/Report/VendorScoreCard.aspx" runat="server"></asp:HyperLink>--%>
            <div class="graph-container">
                <div class="graph-row">
                    <div class="left-col">
                        <div class="heading1">
                            Stock
                        </div>
                        <div id='chart_div' style="width: 300px; margin: 0 auto; padding-top: 6px;">
                        </div>
                    </div>
                    <div class="right-col">
                        <div class="heading2">
                            Stock Score
                        </div>
                        <div id='barchart_div'>
                        </div>
                    </div>
                </div>
                <div class="graph-row" style="margin-top: 10px;">
                    <div class="left-col data-grid">
                        <div class="heading1">
                            Stock Key Figures
                        </div>
                        <div id='otiftable_div'>
                        </div>
                    </div>
                    <div class="right-col">
                        <div class="heading2">
                            Number of Line Recieved
                        </div>
                        <div id='lineChart_div'>
                        </div>
                    </div>
                </div>
            </div>
    </form>
</body>
<script type='text/javascript' src="../../../Scripts/jsapi.js"></script>
<script src="../../../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="../../../Scripts/json2.js" type="text/javascript"></script>
<script type="text/javascript">
    google.load('visualization', '1', { packages: ['gauge'] });
    google.load("visualization", "1", { packages: ["corechart"] });
    google.load('visualization', '1', { packages: ['table'] });
</script>
<script type='text/javascript'>
    var varCurrentMonth = '';
    var varCurrentYear = '';

    function getMonthName(month) {
        var monthName = '';
        switch (month) {
            case 0:
                monthName = 'January';
                break;
            case 1:
                monthName = 'February';
                break;
            case 2:
                monthName = 'March';
                break;
            case 3:
                monthName = 'April';
                break;
            case 4:
                monthName = 'May';
                break;
            case 5:
                monthName = 'June';
                break;
            case 6:
                monthName = 'July';
                break;
            case 7:
                monthName = 'August';
                break;
            case 8:
                monthName = 'September';
                break;
            case 9:
                monthName = 'October';
                break;
            case 10:
                monthName = 'November';
                break;
            case 11:
                monthName = 'December';
                break;
        }
        return monthName;
    }

    function GetActualMonthYear(latestYear, latestMonthNo, actualMonthNo) {
        if (actualMonthNo < latestMonthNo) {
            latestYear = latestYear.toString().substr(2, 2);
            return latestYear;
        }
        else {
            latestYear = (latestYear - 1).toString().substr(2, 2);
            return latestYear;
        }
    }

    function GetTwoDigDecimal(value) {
        var newValue = new String(value)
        if (newValue.indexOf('.') != -1) {
            return parseFloat(newValue).toFixed(2);
        }
        else {
            return value;
        }
    }

    function floorFigure(val) {
        var dotIndex = new String(val).indexOf('.');
        if (dotIndex != -1) {
            var strValue = new String(val);
            var decimalLength = strValue.substr(dotIndex).length;
            if (decimalLength > 1) {
                return parseFloat(strValue.substr(0, dotIndex + 3));
            }
            else {
                return parseFloat(strValue.substr(0, dotIndex + 2));
            }
        }
        else {
            return parseFloat(val);
        }
    }

    function DrawStock(dataValues) {
        var obj = JSON.parse(dataValues);
        var score = 0;

        $.each(obj, function (index, element) {
            switch (this.Stock_Type) {
                case "Lead Time Variance":
                    score = this.Stock_Score;
                    break;
            }
        });

       // var data = google.visualization.arrayToDataTable([['Label', 'Value'], ['Score', floorFigure(score)]]);

        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Label');
        data.addColumn('number', 'Value');
        data.addRows([
            ['Score', floorFigure(score)]
        ]);

        var options = {
            min: 0, max: 10,
            width: 300, height: 300,
            redFrom: 0, redTo: 5,
            yellowFrom: 5, yellowTo: 7,
            greenFrom: 7, greenTo: 10,
            majorTicks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            minorTicks: 5
        };
        var formatter = new google.visualization.NumberFormat({ pattern: '.0' });
        var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
        chart.draw(data, options);
    }

    function DrawStockRate(dataValues) {
        var datas = JSON.parse(dataValues);
        var latestYear = new Date().getFullYear();
        var latestMonthNo = new Date().getMonth();
        var latestMonthName = getMonthName(latestMonthNo);
        varCurrentMonth = latestMonthName;
        varCurrentYear = latestYear;

        var rateVal1 = rateVal2 = rateVal3 = rateVal4 = rateVal5 = rateVal6 =
            rateVal7 = rateVal8 = rateVal9 = rateVal10 = rateVal11 = rateVal12 = 0;

        var varMonthNo = latestMonthNo - 1;

        if (latestMonthNo == 0) {
            latestYear = latestYear - 1;
            latestMonthNo = 12;
            varMonthNo = 11;
        }

        for (var index = 0; index < 12; index++) {
            switch (index) {
                case 0:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 1:
                    Month2 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 2:
                    Month3 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 3:
                    Month4 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 4:
                    Month5 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 5:
                    Month6 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 6:
                    Month7 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 7:
                    Month8 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 8:
                    Month9 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 9:
                    Month10 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 10:
                    Month11 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 11:
                    Month12 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 12:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
            }

            if (varMonthNo == 0) {
                varMonthNo = 11;
            }
            else {
                varMonthNo = varMonthNo - 1;
            }
        }

        $.each(datas, function (index, element) {
            switch (this.Month) {
                case Month1:
                    rateVal1 = rateVal1 + this.Score;
                    break;
                case Month2:
                    rateVal2 = rateVal2 + this.Score;
                    break;
                case Month3:
                    rateVal3 = rateVal3 + this.Score;
                    break;
                case Month4:
                    rateVal4 = rateVal4 + this.Score;
                    break;
                case Month5:
                    rateVal5 = rateVal5 + this.Score;
                    break;
                case Month6:
                    rateVal6 = rateVal6 + this.Score;
                    break;
                case Month7:
                    rateVal7 = rateVal7 + this.Score;
                    break;
                case Month8:
                    rateVal8 = rateVal8 + this.Score;
                    break;
                case Month9:
                    rateVal9 = rateVal9 + this.Score;
                    break;
                case Month10:
                    rateVal10 = rateVal10 + this.Score;
                    break;
                case Month11:
                    rateVal11 = rateVal11 + this.Score;
                    break;
                case Month12:
                    rateVal12 = rateVal12 + this.Score;
                    break;
            }
        });
        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Month');
        data.addColumn('number', 'Stock Score');

        data.addRows([
            [Month1, floorFigure(rateVal1)],
            [Month2, floorFigure(rateVal2)],
            [Month3, floorFigure(rateVal3)],
            [Month4, floorFigure(rateVal4)],
            [Month5, floorFigure(rateVal5)],
            [Month6, floorFigure(rateVal6)],
            [Month7, floorFigure(rateVal7)],
            [Month8, floorFigure(rateVal8)],
            [Month9, floorFigure(rateVal9)],
            [Month10, floorFigure(rateVal10)],
            [Month11, floorFigure(rateVal11)],
            [Month12, floorFigure(rateVal12)]
        ]);
         
        var options = {
            colors: ['#219bd4'],
            width: 590, height: 300,
            vAxis: { ticks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] },
            chartArea: { height: 240, width: 520 },
            legend: { position: 'none' },
            backgroundColor: { fill: 'transparent' },
            hAxis: { titleTextStyle: { color: 'red' }, textStyle: { fontSize: 9, bold: true } }
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('barchart_div'));
        chart.draw(data, options);
    }

    function DrawStockKeyFigures(dataValues) {
        var dataValuesDatas = JSON.parse(dataValues);

        var latestYear = new Date().getFullYear();
        var latestMonthNo = new Date().getMonth();
        var latestMonthName = getMonthName(latestMonthNo);
        varCurrentMonth = latestMonthName;
        varCurrentYear = latestYear;

        var scoreVal1 = scoreVal2 = scoreVal3 = scoreVal4 = scoreVal5 = scoreVal6 = scoreVal7 = scoreVal8 = scoreVal9 = scoreVal10 = scoreVal11 = scoreVal12 = 0;
        var backOrdReateVal1 = backOrdReateVal2 = backOrdReateVal3 = backOrdReateVal4 = backOrdReateVal5 = backOrdReateVal6 = backOrdReateVal7 = backOrdReateVal8 = backOrdReateVal9 = backOrdReateVal10 = backOrdReateVal11 = backOrdReateVal12 = 0;
        var quotedDelDaysVal1 = quotedDelDaysVal2 = quotedDelDaysVal3 = quotedDelDaysVal4 = quotedDelDaysVal5 = quotedDelDaysVal6 = quotedDelDaysVal7 = quotedDelDaysVal8 = quotedDelDaysVal9 = quotedDelDaysVal10 = quotedDelDaysVal11 = quotedDelDaysVal12 = 0;
        var actualDelDaysVal1 = actualDelDaysVal2 = actualDelDaysVal3 = actualDelDaysVal4 = actualDelDaysVal5 = actualDelDaysVal6 = actualDelDaysVal7 = actualDelDaysVal8 = actualDelDaysVal9 = actualDelDaysVal10 = actualDelDaysVal11 = actualDelDaysVal12 = 0;
        var leadTimeVarVal1 = leadTimeVarVal2 = leadTimeVarVal3 = leadTimeVarVal4 = leadTimeVarVal5 = leadTimeVarVal6 = leadTimeVarVal7 = leadTimeVarVal8 = leadTimeVarVal9 = leadTimeVarVal10 = leadTimeVarVal11 = leadTimeVarVal12 = 0;
        var Month1 = Month2 = Month3 = Month4 = Month5 = Month6 = Month7 = Month8 = Month9 = Month10 = Month11 = Month12 = 0;

        /* Logic to setting the month values */
        var varMonthNo = latestMonthNo - 1;

        if (latestMonthNo == 0) {
            latestYear = latestYear - 1;
            latestMonthNo = 12;
            varMonthNo = 11;
        }

        for (var index = 0; index < 12; index++) {
            switch (index) {
                case 0:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 1:
                    Month2 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 2:
                    Month3 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 3:
                    Month4 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 4:
                    Month5 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 5:
                    Month6 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 6:
                    Month7 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 7:
                    Month8 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 8:
                    Month9 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 9:
                    Month10 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 10:
                    Month11 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 11:
                    Month12 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 12:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
            }

            if (varMonthNo == 0) {
                varMonthNo = 11;
            }
            else {
                varMonthNo = varMonthNo - 1;
            }
        }

        /* holding score in variables. */
        $.each(dataValuesDatas, function (index, element) {
            switch (this.Month) {
                case Month1:
                    scoreVal1 = this.Score;
                    backOrdReateVal1 = this.BackOrderRate;
                    quotedDelDaysVal1 = this.QuotedDelDays;
                    actualDelDaysVal1 = this.ActualDelDays;
                    leadTimeVarVal1 = this.LeadTimeVariance;
                    break;
                case Month2:
                    scoreVal2 = this.Score;
                    backOrdReateVal2 = this.BackOrderRate;
                    quotedDelDaysVal2 = this.QuotedDelDays;
                    actualDelDaysVal2 = this.ActualDelDays;
                    leadTimeVarVal2 = this.LeadTimeVariance;
                    break;
                case Month3:
                    scoreVal3 = this.Score;
                    backOrdReateVal3 = this.BackOrderRate;
                    quotedDelDaysVal3 = this.QuotedDelDays;
                    actualDelDaysVal3 = this.ActualDelDays;
                    leadTimeVarVal3 = this.LeadTimeVariance;
                    break;
                case Month4:
                    scoreVal4 = this.Score;
                    backOrdReateVal4 = this.BackOrderRate;
                    quotedDelDaysVal4 = this.QuotedDelDays;
                    actualDelDaysVal4 = this.ActualDelDays;
                    leadTimeVarVal4 = this.LeadTimeVariance;
                    break;
                case Month5:
                    scoreVal5 = this.Score;
                    backOrdReateVal5 = this.BackOrderRate;
                    quotedDelDaysVal5 = this.QuotedDelDays;
                    actualDelDaysVal5 = this.ActualDelDays;
                    leadTimeVarVal5 = this.LeadTimeVariance;
                    break;
                case Month6:
                    scoreVal6 = this.Score;
                    backOrdReateVal6 = this.BackOrderRate;
                    quotedDelDaysVal6 = this.QuotedDelDays;
                    actualDelDaysVal6 = this.ActualDelDays;
                    leadTimeVarVal6 = this.LeadTimeVariance;
                    break;
                case Month7:
                    scoreVal7 = this.Score;
                    backOrdReateVal7 = this.BackOrderRate;
                    quotedDelDaysVal7 = this.QuotedDelDays;
                    actualDelDaysVal7 = this.ActualDelDays;
                    leadTimeVarVal7 = this.LeadTimeVariance;
                    break;
                case Month8:
                    scoreVal8 = this.Score;
                    backOrdReateVal8 = this.BackOrderRate;
                    quotedDelDaysVal8 = this.QuotedDelDays;
                    actualDelDaysVal8 = this.ActualDelDays;
                    leadTimeVarVal8 = this.LeadTimeVariance;
                    break;
                case Month9:
                    scoreVal9 = this.Score;
                    backOrdReateVal9 = this.BackOrderRate;
                    quotedDelDaysVal9 = this.QuotedDelDays;
                    actualDelDaysVal9 = this.ActualDelDays;
                    leadTimeVarVal9 = this.LeadTimeVariance;
                    break;
                case Month10:
                    scoreVal10 = this.Score;
                    backOrdReateVal10 = this.BackOrderRate;
                    quotedDelDaysVal10 = this.QuotedDelDays;
                    actualDelDaysVal10 = this.ActualDelDays;
                    leadTimeVarVal10 = this.LeadTimeVariance;
                    break;
                case Month11:
                    scoreVal11 = this.Score;
                    backOrdReateVal11 = this.BackOrderRate;
                    quotedDelDaysVal11 = this.QuotedDelDays;
                    actualDelDaysVal11 = this.ActualDelDays;
                    leadTimeVarVal11 = this.LeadTimeVariance;
                    break;
                case Month12:
                    scoreVal12 = this.Score;
                    backOrdReateVal12 = this.BackOrderRate;
                    quotedDelDaysVal12 = this.QuotedDelDays;
                    actualDelDaysVal12 = this.ActualDelDays;
                    leadTimeVarVal12 = this.LeadTimeVariance;
                    break;
            }
        });

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Period');
        data.addColumn('string', 'Score');
        data.addColumn('string', 'Back Order Rate');
        data.addColumn('string', 'Quoted Delivery Days');
        data.addColumn('string', 'Actual Delivery Days');
        data.addColumn('string', 'Lead Time Variance');
        data.addRows([
            [Month1, floorFigure(scoreVal1).toString(), GetTwoDigDecimal(backOrdReateVal1).toString(), GetTwoDigDecimal(quotedDelDaysVal1).toString(), GetTwoDigDecimal(actualDelDaysVal1).toString(), GetTwoDigDecimal(leadTimeVarVal1).toString()],
            [Month2, floorFigure(scoreVal2).toString(), GetTwoDigDecimal(backOrdReateVal2).toString(), GetTwoDigDecimal(quotedDelDaysVal2).toString(), GetTwoDigDecimal(actualDelDaysVal2).toString(), GetTwoDigDecimal(leadTimeVarVal2).toString()],
            [Month3, floorFigure(scoreVal3).toString(), GetTwoDigDecimal(backOrdReateVal3).toString(), GetTwoDigDecimal(quotedDelDaysVal3).toString(), GetTwoDigDecimal(actualDelDaysVal3).toString(), GetTwoDigDecimal(leadTimeVarVal3).toString()],
            [Month4, floorFigure(scoreVal4).toString(), GetTwoDigDecimal(backOrdReateVal4).toString(), GetTwoDigDecimal(quotedDelDaysVal4).toString(), GetTwoDigDecimal(actualDelDaysVal4).toString(), GetTwoDigDecimal(leadTimeVarVal4).toString()],
            [Month5, floorFigure(scoreVal5).toString(), GetTwoDigDecimal(backOrdReateVal5).toString(), GetTwoDigDecimal(quotedDelDaysVal5).toString(), GetTwoDigDecimal(actualDelDaysVal5).toString(), GetTwoDigDecimal(leadTimeVarVal5).toString()],
            [Month6, floorFigure(scoreVal6).toString(), GetTwoDigDecimal(backOrdReateVal6).toString(), GetTwoDigDecimal(quotedDelDaysVal6).toString(), GetTwoDigDecimal(actualDelDaysVal6).toString(), GetTwoDigDecimal(leadTimeVarVal6).toString()],
            [Month7, floorFigure(scoreVal7).toString(), GetTwoDigDecimal(backOrdReateVal7).toString(), GetTwoDigDecimal(quotedDelDaysVal7).toString(), GetTwoDigDecimal(actualDelDaysVal7).toString(), GetTwoDigDecimal(leadTimeVarVal7).toString()],
            [Month8, floorFigure(scoreVal8).toString(), GetTwoDigDecimal(backOrdReateVal8).toString(), GetTwoDigDecimal(quotedDelDaysVal8).toString(), GetTwoDigDecimal(actualDelDaysVal8).toString(), GetTwoDigDecimal(leadTimeVarVal8).toString()],
            [Month9, floorFigure(scoreVal9).toString(), GetTwoDigDecimal(backOrdReateVal9).toString(), GetTwoDigDecimal(quotedDelDaysVal9).toString(), GetTwoDigDecimal(actualDelDaysVal9).toString(), GetTwoDigDecimal(leadTimeVarVal9).toString()],
            [Month10, floorFigure(scoreVal10).toString(), GetTwoDigDecimal(backOrdReateVal10).toString(), GetTwoDigDecimal(quotedDelDaysVal10).toString(), GetTwoDigDecimal(actualDelDaysVal10).toString(), GetTwoDigDecimal(leadTimeVarVal10).toString()],
            [Month11, floorFigure(scoreVal11).toString(), GetTwoDigDecimal(backOrdReateVal11).toString(), GetTwoDigDecimal(quotedDelDaysVal11).toString(), GetTwoDigDecimal(actualDelDaysVal11).toString(), GetTwoDigDecimal(leadTimeVarVal11).toString()],
            [Month12, floorFigure(scoreVal12).toString(), GetTwoDigDecimal(backOrdReateVal12).toString(), GetTwoDigDecimal(quotedDelDaysVal12).toString(), GetTwoDigDecimal(actualDelDaysVal12).toString(), GetTwoDigDecimal(leadTimeVarVal12).toString()]
        ]);

        var options = { width: 357, height: 330, showRowNumber: false };
        var table = new google.visualization.Table(document.getElementById('otiftable_div'));
        table.draw(data, options);
    }

    function DrawNumberofLineRecieved(dataValues) {
        var dataValuesDatas = JSON.parse(dataValues);
        var latestYear = new Date().getFullYear();
        var latestMonthNo = new Date().getMonth();
        var latestMonthName = getMonthName(latestMonthNo);
        varCurrentMonth = latestMonthName;
        varCurrentYear = latestYear;

        var noOfLineRecVal1 = noOfLineRecVal2 = noOfLineRecVal3 = noOfLineRecVal4 = noOfLineRecVal5 = noOfLineRecVal6 =
            noOfLineRecVal7 = noOfLineRecVal8 = noOfLineRecVal9 = noOfLineRecVal10 = noOfLineRecVal11 = noOfLineRecVal12 = 0;
        var Month1 = Month2 = Month3 = Month4 = Month5 = Month6 = Month7 = Month8 = Month9 = Month10 = Month11 = Month12 = 0;

        /* Logic to setting the month values */
        var varMonthNo = latestMonthNo - 1;

        if (latestMonthNo == 0) {
            latestYear = latestYear - 1;
            latestMonthNo = 12;
            varMonthNo = 11;
        }

        for (var index = 0; index < 12; index++) {
            switch (index) {
                case 0:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 1:
                    Month2 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 2:
                    Month3 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 3:
                    Month4 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 4:
                    Month5 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 5:
                    Month6 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 6:
                    Month7 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 7:
                    Month8 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 8:
                    Month9 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 9:
                    Month10 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 10:
                    Month11 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 11:
                    Month12 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 12:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
            }

            if (varMonthNo == 0) {
                varMonthNo = 11;
            }
            else {
                varMonthNo = varMonthNo - 1;
            }
        }

        /* holding score in variables. */
        $.each(dataValuesDatas, function (index, element) {
            switch (this.Month) {
                case Month1:
                    noOfLineRecVal1 = this.NoOfLinesReceived;
                    break;
                case Month2:
                    noOfLineRecVal2 = this.NoOfLinesReceived;
                    break;
                case Month3:
                    noOfLineRecVal3 = this.NoOfLinesReceived;
                    break;
                case Month4:
                    noOfLineRecVal4 = this.NoOfLinesReceived;
                    break;
                case Month5:
                    noOfLineRecVal5 = this.NoOfLinesReceived;
                    break;
                case Month6:
                    noOfLineRecVal6 = this.NoOfLinesReceived;
                    break;
                case Month7:
                    noOfLineRecVal7 = this.NoOfLinesReceived;
                    break;
                case Month8:
                    noOfLineRecVal8 = this.NoOfLinesReceived;
                    break;
                case Month9:
                    noOfLineRecVal9 = this.NoOfLinesReceived;
                    break;
                case Month10:
                    noOfLineRecVal10 = this.NoOfLinesReceived;
                    break;
                case Month11:
                    noOfLineRecVal11 = this.NoOfLinesReceived;
                    break;
                case Month12:
                    noOfLineRecVal12 = this.NoOfLinesReceived;
                    break;
            }
        });

        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Month');
        data.addColumn('number', 'Line recieved');

        data.addRows([
            [Month1, floorFigure(noOfLineRecVal1)],
            [Month2, floorFigure(noOfLineRecVal2)],
            [Month3, floorFigure(noOfLineRecVal3)],
            [Month4, floorFigure(noOfLineRecVal4)],
            [Month5, floorFigure(noOfLineRecVal5)],
            [Month6, floorFigure(noOfLineRecVal6)],
            [Month7, floorFigure(noOfLineRecVal7)],
            [Month8, floorFigure(noOfLineRecVal8)],
            [Month9, floorFigure(noOfLineRecVal9)],
            [Month10, floorFigure(noOfLineRecVal10)],
            [Month11, floorFigure(noOfLineRecVal11)],
            [Month12, floorFigure(noOfLineRecVal12)]
        ]);




        /*  
        Logic to maintan the below logic for [Number of Line Recieved graph] :
        -------------------------------------------------------------------------
        If highest number of noOfLineRec is <=200, range = 0-200
        If highest number of noOfLineRec is <=100, range = 0-100
        If highest number of noOfLineRec is <=50, range = 0-50
        If highest number of noOfLineRec is <=20, range = 0-20
        If highest number of noOfLineRec is <=10, range = 0-10
        */
        var Mode1 = Mode2 = Mode3 = Mode4 = Mode5 = Mode6 = 0;
        var GetHighestValue = [];
        GetHighestValue.push(noOfLineRecVal1);
        GetHighestValue.push(noOfLineRecVal2);
        GetHighestValue.push(noOfLineRecVal3);
        GetHighestValue.push(noOfLineRecVal4);
        GetHighestValue.push(noOfLineRecVal5);
        GetHighestValue.push(noOfLineRecVal6);
        GetHighestValue.push(noOfLineRecVal7);
        GetHighestValue.push(noOfLineRecVal8);
        GetHighestValue.push(noOfLineRecVal9);
        GetHighestValue.push(noOfLineRecVal10);
        GetHighestValue.push(noOfLineRecVal11);
        GetHighestValue.push(noOfLineRecVal12);
        GetHighestValue.sort(function (a, b) {
            return a > b ? 1 : a < b ? -1 : 0;
        });
        GetHighestValue.reverse();
        var Finalvalue;
        var value = GetHighestValue[0].toString();

        function GetAddNumber(getsubstringValue) {
            //debugger;
            var AddedValue;
            if (getsubstringValue == "1") {
                AddedValue = parseInt(value) + 9;
            }
            else if (getsubstringValue == "2") {
                AddedValue = parseInt(value) + 8;
            }
            else if (getsubstringValue == "3") {
                AddedValue = parseInt(value) + 7;
            }
            else if (getsubstringValue == "4") {
                AddedValue = parseInt(value) + 6;
            }
            else if (getsubstringValue == "5") {
                AddedValue = parseInt(value) + 5;
            }
            else if (getsubstringValue == "6") {
                AddedValue = parseInt(value) + 4;
            }
            else if (getsubstringValue == "7") {
                AddedValue = parseInt(value) + 3;
            }
            else if (getsubstringValue == "8") {
                AddedValue = parseInt(value) + 2;
            }
            else if (getsubstringValue == "9") {
                AddedValue = parseInt(value) + 1;
            }
            return AddedValue;
        }
        if (parseInt(value) <= 10) {
            Mode1 = 0;
            Mode2 = 2;
            Mode3 = 4;
            Mode4 = 6;
            Mode5 = 8;
            Mode6 = 10;
        }
        else {
            var getsubstringValue = value.substring(value.length - 1, value.length);

            if (getsubstringValue == "0") {
                Finalvalue = value;
            }
            else {
                Finalvalue = GetAddNumber(getsubstringValue);
            }
            var slot = parseInt(Finalvalue) / 5;


            Mode1 = 0;
            Mode2 = slot;
            Mode3 = parseInt(Mode2) + parseInt(slot);
            Mode4 = parseInt(Mode3) + parseInt(slot);
            Mode5 = parseInt(Mode4) + parseInt(slot);
            Mode6 = parseInt(Mode5) + parseInt(slot);
        }




        //        var Mode1 = Mode2 = Mode3 = Mode4 = Mode5 = Mode6 = 0;
        //        if ((floorFigure(noOfLineRecVal1) > 100 && floorFigure(noOfLineRecVal1) <= 200) || (floorFigure(noOfLineRecVal2) > 100 && floorFigure(noOfLineRecVal2) <= 200) ||
        //            (floorFigure(noOfLineRecVal3) > 100 && floorFigure(noOfLineRecVal3) <= 200) || (floorFigure(noOfLineRecVal4) > 100 && floorFigure(noOfLineRecVal4) <= 200) ||
        //            (floorFigure(noOfLineRecVal5) > 100 && floorFigure(noOfLineRecVal5) <= 200) || (floorFigure(noOfLineRecVal6) > 100 && floorFigure(noOfLineRecVal6) <= 200) ||
        //            (floorFigure(noOfLineRecVal7) > 100 && floorFigure(noOfLineRecVal7) <= 200) || (floorFigure(noOfLineRecVal8) > 100 && floorFigure(noOfLineRecVal8) <= 200) ||
        //            (floorFigure(noOfLineRecVal9) > 100 && floorFigure(noOfLineRecVal9) <= 200) || (floorFigure(noOfLineRecVal10) > 100 && floorFigure(noOfLineRecVal10) <= 200) ||
        //            (floorFigure(noOfLineRecVal11) > 100 && floorFigure(noOfLineRecVal11) <= 200) || (floorFigure(noOfLineRecVal12) > 100 && floorFigure(noOfLineRecVal12) <= 200)) {
        //            Mode1 = 0;
        //            Mode2 = 40;
        //            Mode3 = 80;
        //            Mode4 = 120;
        //            Mode5 = 160;
        //            Mode6 = 200;
        //        }
        //        else if ((floorFigure(noOfLineRecVal1) > 50 && floorFigure(noOfLineRecVal1) <= 100) || (floorFigure(noOfLineRecVal2) > 50 && floorFigure(noOfLineRecVal2) <= 100) ||
        //            (floorFigure(noOfLineRecVal3) > 50 && floorFigure(noOfLineRecVal3) <= 100) || (floorFigure(noOfLineRecVal4) > 50 && floorFigure(noOfLineRecVal4) <= 100) ||
        //            (floorFigure(noOfLineRecVal5) > 50 && floorFigure(noOfLineRecVal5) <= 100) || (floorFigure(noOfLineRecVal6) > 50 && floorFigure(noOfLineRecVal6) <= 100) ||
        //            (floorFigure(noOfLineRecVal7) > 50 && floorFigure(noOfLineRecVal7) <= 100) || (floorFigure(noOfLineRecVal8) > 50 && floorFigure(noOfLineRecVal8) <= 100) ||
        //            (floorFigure(noOfLineRecVal9) > 50 && floorFigure(noOfLineRecVal9) <= 100) || (floorFigure(noOfLineRecVal10) > 50 && floorFigure(noOfLineRecVal10) <= 100) ||
        //            (floorFigure(noOfLineRecVal11) > 50 && floorFigure(noOfLineRecVal11) <= 100) || (floorFigure(noOfLineRecVal12) > 50 && floorFigure(noOfLineRecVal12) <= 100)) {
        //            Mode1 = 0;
        //            Mode2 = 20;
        //            Mode3 = 40;
        //            Mode4 = 60;
        //            Mode5 = 80;
        //            Mode6 = 100;
        //        }
        //        else if ((floorFigure(noOfLineRecVal1) > 20 && floorFigure(noOfLineRecVal1) <= 50) || (floorFigure(noOfLineRecVal2) > 20 && floorFigure(noOfLineRecVal2) <= 50) ||
        //            (floorFigure(noOfLineRecVal3) > 20 && floorFigure(noOfLineRecVal3) <= 50) || (floorFigure(noOfLineRecVal4) > 20 && floorFigure(noOfLineRecVal4) <= 50) ||
        //            (floorFigure(noOfLineRecVal5) > 20 && floorFigure(noOfLineRecVal5) <= 50) || (floorFigure(noOfLineRecVal6) > 20 && floorFigure(noOfLineRecVal6) <= 50) ||
        //            (floorFigure(noOfLineRecVal7) > 20 && floorFigure(noOfLineRecVal7) <= 50) || (floorFigure(noOfLineRecVal8) > 20 && floorFigure(noOfLineRecVal8) <= 50) ||
        //            (floorFigure(noOfLineRecVal9) > 20 && floorFigure(noOfLineRecVal9) <= 50) || (floorFigure(noOfLineRecVal10) > 20 && floorFigure(noOfLineRecVal10) <= 50) ||
        //            (floorFigure(noOfLineRecVal11) > 20 && floorFigure(noOfLineRecVal11) <= 50) || (floorFigure(noOfLineRecVal12) > 20 && floorFigure(noOfLineRecVal12) <= 50)) {
        //            Mode1 = 0;
        //            Mode2 = 10;
        //            Mode3 = 20;
        //            Mode4 = 30;
        //            Mode5 = 40;
        //            Mode6 = 50;
        //        }
        //        else if ((floorFigure(noOfLineRecVal1) > 10 && floorFigure(noOfLineRecVal1) <= 20) || (floorFigure(noOfLineRecVal2) > 10 && floorFigure(noOfLineRecVal2) <= 20) ||
        //            (floorFigure(noOfLineRecVal3) > 10 && floorFigure(noOfLineRecVal3) <= 20) || (floorFigure(noOfLineRecVal4) > 10 && floorFigure(noOfLineRecVal4) <= 20) ||
        //            (floorFigure(noOfLineRecVal5) > 10 && floorFigure(noOfLineRecVal5) <= 20) || (floorFigure(noOfLineRecVal6) > 10 && floorFigure(noOfLineRecVal6) <= 20) ||
        //            (floorFigure(noOfLineRecVal7) > 10 && floorFigure(noOfLineRecVal7) <= 20) || (floorFigure(noOfLineRecVal8) > 10 && floorFigure(noOfLineRecVal8) <= 20) ||
        //            (floorFigure(noOfLineRecVal9) > 10 && floorFigure(noOfLineRecVal9) <= 20) || (floorFigure(noOfLineRecVal10) > 10 && floorFigure(noOfLineRecVal10) <= 20) ||
        //            (floorFigure(noOfLineRecVal11) > 10 && floorFigure(noOfLineRecVal11) <= 20) || (floorFigure(noOfLineRecVal12) > 10 && floorFigure(noOfLineRecVal12) <= 20)) {
        //            Mode1 = 0;
        //            Mode2 = 4;
        //            Mode3 = 8;
        //            Mode4 = 12;
        //            Mode5 = 16;
        //            Mode6 = 20;
        //        }
        //        else if ((floorFigure(noOfLineRecVal1) > 0 && floorFigure(noOfLineRecVal1) <= 10) || (floorFigure(noOfLineRecVal2) > 0 && floorFigure(noOfLineRecVal2) <= 10) ||
        //            (floorFigure(noOfLineRecVal3) > 0 && floorFigure(noOfLineRecVal3) <= 10) || (floorFigure(noOfLineRecVal4) > 0 && floorFigure(noOfLineRecVal4) <= 10) ||
        //            (floorFigure(noOfLineRecVal5) > 0 && floorFigure(noOfLineRecVal5) <= 10) || (floorFigure(noOfLineRecVal6) > 0 && floorFigure(noOfLineRecVal6) <= 10) ||
        //            (floorFigure(noOfLineRecVal7) > 0 && floorFigure(noOfLineRecVal7) <= 10) || (floorFigure(noOfLineRecVal8) > 0 && floorFigure(noOfLineRecVal8) <= 10) ||
        //            (floorFigure(noOfLineRecVal9) > 0 && floorFigure(noOfLineRecVal9) <= 10) || (floorFigure(noOfLineRecVal10) > 0 && floorFigure(noOfLineRecVal10) <= 10) ||
        //            (floorFigure(noOfLineRecVal11) > 0 && floorFigure(noOfLineRecVal11) <= 10) || (floorFigure(noOfLineRecVal12) > 0 && floorFigure(noOfLineRecVal12) <= 10)) {
        //            Mode1 = 0;
        //            Mode2 = 2;
        //            Mode3 = 4;
        //            Mode4 = 6;
        //            Mode5 = 8;
        //            Mode6 = 10;
        //        }
        //        else {
        //            Mode1 = 0;
        //            Mode2 = 2;
        //            Mode3 = 4;
        //            Mode4 = 6;
        //            Mode5 = 8;
        //            Mode6 = 10;
        //        }

        var options = {

            width: 590, height: 350,
            vAxis: { ticks: [Mode1, Mode2, Mode3, Mode4, Mode5, Mode6] },
            chartArea: { height: 200, width: 520 },
            legend: { position: 'none' },
            pointSize: 7,
            crosshair: { focused: { color: '#3bc', opacity: 0.8 } },
            backgroundColor: { fill: 'transparent' },
            hAxis: { titleTextStyle: { color: 'red' }, textStyle: { fontSize: 9, bold: true } }
        };

        var chart = new google.visualization.LineChart(document.getElementById('lineChart_div'));
        chart.draw(data, options);
    }

    $(function () {
        var vendorId = '<%= vendorId %>';
        var vscURL = '<%= vscURL %>';
        $("#ancBacktoMainPage").attr('href', vscURL);

        $.ajax({
            type: "POST",
            url: "AjaxCallPage.aspx/GetVendorScoreCardStockReportData",
            data: '{vendorId:' + vendorId + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                DrawStock(response.d.StockMain);
                DrawStockRate(response.d.StockHistPerformance);
                DrawStockKeyFigures(response.d.StockHistPerformance);
                DrawNumberofLineRecieved(response.d.StockHistPerformance);
            }
        });
    });
</script>
</html>

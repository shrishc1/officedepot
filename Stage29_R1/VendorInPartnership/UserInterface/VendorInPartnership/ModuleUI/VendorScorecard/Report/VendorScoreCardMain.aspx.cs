﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtilities;
using BusinessEntities.ModuleBE.VendorScorecard;
using System.Configuration;
using System.Diagnostics;
using System.IO;

public partial class VendorScoreCardMain : CommonPage
{
    protected int vendorId = 0;
    protected string calculatedURL = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            #region Logic to adding the url with querystring in iframe src ...

            //if (Request.QueryString["ValidVendor"] != null)
            if (GetQueryStringValue("ValidVendor") == null)            
            {
                EncryptQueryString("~/ModuleUI/VendorScoreCard/Report/VendorScoreCardSearch.aspx");
            }

            if (GetQueryStringValue("vendorId") != null)
            {
                vendorId = Convert.ToInt32(GetQueryStringValue("vendorId"));
                ViewState["vendorId"] = Convert.ToString(vendorId);
            }

            calculatedURL = EncryptURL(string.Format("CalculatedSheet.aspx?vendorId={0}", vendorId));
            this.ifScoreCard.Attributes.Add("src", EncryptURL(string.Format("VendorScoreCard.aspx?vendorId={0}", vendorId)));

            #endregion
        }
    }

    protected void DeleteOldfiles(string DirPath)
    {
        var directory = new DirectoryInfo(DirPath);
        DateTime from_date = DateTime.Now.AddDays(-1);

        var files = directory.GetFiles()
          .Where(file => file.LastWriteTime <= from_date && file.Extension.ToLower() == ".pdf");

        foreach (FileInfo fi in files)
        {
            string fullpath = string.Format(fi.FullName);
            if (System.IO.File.Exists(fullpath))
                System.IO.File.Delete(fullpath);
        }


    }

    protected void btnExportPages_Click(object sender, EventArgs e)
    {
        #region Approach 1 ....
        /*
        string outputPath = Convert.ToString(ConfigurationManager.AppSettings["pdfOutputPath"]);
        string toolPath = Convert.ToString(ConfigurationManager.AppSettings["pdfToolPath"]);
        string fileNameWithUserId = string.Format("ScoreCardReport_{0}", Convert.ToString(Session["UserID"]));

        string baseUrl = string.Format("{0}://{1}", Request.Url.Scheme, HttpContext.Current.Request.Url.Authority);
        string localPath = Request.Url.LocalPath.Substring(1, Request.Url.LocalPath.LastIndexOf('/'));
        string mainScoreCard = string.Format("{0}/{1}VendorScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
        string otifFScoreCard = string.Format("{0}/{1}OTIFScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
        string discrepancyScoreCard = string.Format("{0}/{1}DiscrepancyScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
        string schedulingScoreCard = string.Format("{0}/{1}SchedulingScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
        string stockScoreCard = string.Format("{0}/{1}StockScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
        string calculatedSheet = string.Format("{0}/{1}CalculatedSheet.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));

        //string[] combinedUrls = new string[] { mainScoreCard, otifFScoreCard, discrepancyScoreCard, schedulingScoreCard, stockScoreCard, calculatedSheet };
        string[] combinedUrls = new string[] { mainScoreCard, otifFScoreCard };

        outputPath = "~/ReportOutput/VendorScoreCardPDF/";
        //toolPath = "C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe";
        //toolPath = "C:\\Program Files (x86)\\wkhtmltopdf\\wkhtmltopdf.exe";        
        var pdfUrl = PdfGenerator.HtmlToPdf(outputPath, fileNameWithUserId, combinedUrls, null, toolPath);
        */
        #endregion

        #region Approach 2 ....

        string baseUrl = string.Format("{0}://{1}", Request.Url.Scheme, HttpContext.Current.Request.Url.Authority);
        string localPath = Request.Url.LocalPath.Substring(1, Request.Url.LocalPath.LastIndexOf('/'));

        string mainScoreCard = string.Format("{0}/{1}VendorScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
        string otifFScoreCard = string.Format("{0}/{1}OTIFScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
        string discrepancyScoreCard = string.Format("{0}/{1}DiscrepancyScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
        string schedulingScoreCard = string.Format("{0}/{1}SchedulingScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
        string stockScoreCard = string.Format("{0}/{1}StockScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
        string calculatedSheet = string.Format("{0}/{1}CalculatedSheet.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
        string combinedUrls = string.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\"",
            mainScoreCard, otifFScoreCard, discrepancyScoreCard, schedulingScoreCard, stockScoreCard, calculatedSheet);

        //string combinedUrls = string.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\"",
        //    mainScoreCard, otifFScoreCard, discrepancyScoreCard, schedulingScoreCard, stockScoreCard);

        string outputPath = Convert.ToString(ConfigurationManager.AppSettings["pdfOutputPath"]);
        string toolPath = Convert.ToString(ConfigurationManager.AppSettings["pdfToolPath"]);
        string fileNameWithUserId = string.Format("ScoreCardReport_{0}.pdf", Convert.ToString(Session["UserID"]));

        string fileNameWithPath = string.Format(@"{0}\{1}", outputPath, fileNameWithUserId);
        if (System.IO.File.Exists(fileNameWithPath))
            System.IO.File.Delete(fileNameWithPath);

        this.GeneratePdf(combinedUrls, outputPath, toolPath, fileNameWithUserId);
        Response.AppendHeader("Content-disposition", string.Format("attachment; filename={0}", fileNameWithUserId));
        Response.Clear();
        Response.WriteFile(fileNameWithPath);

        //// Code to delete pdf file from server.
        DeleteOldfiles(outputPath);

        Response.End();
        #endregion
    }

    public void GeneratePdf(string url, string outputPath, string toolPath, string fileName)
    {
        var process = new Process();
        try
        {
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.FileName = toolPath;

            var switches = string.Empty;
            switches += "--print-media-type ";
            switches += "--margin-top 10mm --margin-bottom 10mm --margin-right 10mm --margin-left 10mm ";
            switches += "--page-size Letter --javascript-delay 10000 ";
            process.StartInfo.Arguments = switches + " " + url + " " + fileName;
            process.StartInfo.WorkingDirectory = outputPath;
            process.Start();

            //read output
            byte[] buffer = new byte[10485760];
            byte[] file;
            using (var ms = new MemoryStream())
            {
                while (true)
                {
                    int read = process.StandardOutput.BaseStream.Read(buffer, 0, buffer.Length);

                    if (read <= 0)
                    {
                        break;
                    }
                    ms.Write(buffer, 0, read);
                }
                file = ms.ToArray();
            }
        }
        catch (Exception ex) { 
        
        }
        finally
        {
            process.Close();
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("VendorScoreCardSearch.aspx");
    }
    //protected void lnkCalculatedSheet_Click(object sender, EventArgs e)
    //{
    //    // 'CalculatedSheet.aspx?vendorId=' + vendorId);
    //    EncryptQueryString("CalculatedSheet.aspx?vendorId=" + ViewState["vendorId"]);
    //}
}



﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SchedulingScoreCard.aspx.cs"
    Inherits="SchedulingScoreCard" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../../Css/chartstyle.css" rel="stylesheet" type="text/css" />
     <script src="../../../Scripts/loader.js"></script>
</head>
<body>
    <form id="form6" runat="server">
    <div class="main-container">
        <a id="ancBacktoMainPage" runat="server" class="back-to-mainpage"></a>
        <%--<asp:HyperLink ID="hlVendorScoreCardMain" Text="Main Page" NavigateUrl="~/ModuleUI/VendorScorecard/Report/VendorScoreCard.aspx" runat="server"></asp:HyperLink>--%>
        <div class="graph-container">
            <div class="graph-row">
                <div class="left-col">
                    <div class="heading1">
                        Scheduling</div>
                    <div id='chart_div' style="width: 300px; margin: 0 auto; padding-top: 6px;">
                    </div>
                </div>
                <div class="right-col">
                    <div class="heading2">
                        Scheduling Score</div>
                    <div id='barchart_div'>
                    </div>
                </div>
            </div>
            <div class="graph-row" style="margin-top: 10px;">
                <div class="left-col data-grid">
                    <div class="heading1">
                        Appointment Scheduling Key Figures</div>
                    <div id='otiftable_div'>
                    </div>
                </div>
                <div class="right-col">
                    <div class="heading2">
                        Number of Deliveries Made</div>
                    <div id='lineChart_div'>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
<script type='text/javascript' src="../../../Scripts/jsapi.js"></script>
<script src="../../../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="../../../Scripts/json2.js" type="text/javascript"></script>
<script type="text/javascript">
    google.load('visualization', '1', { packages: ['gauge'] });
    google.load("visualization", "1", { packages: ["corechart"] });
    google.load('visualization', '1', { packages: ['table'] });
</script>
<script type='text/javascript'>
    var varCurrentMonth = '';
    var varCurrentYear = '';

    function getMonthName(month) {
        var monthName = '';
        switch (month) {
            case 0:
                monthName = 'January';
                break;
            case 1:
                monthName = 'February';
                break;
            case 2:
                monthName = 'March';
                break;
            case 3:
                monthName = 'April';
                break;
            case 4:
                monthName = 'May';
                break;
            case 5:
                monthName = 'June';
                break;
            case 6:
                monthName = 'July';
                break;
            case 7:
                monthName = 'August';
                break;
            case 8:
                monthName = 'September';
                break;
            case 9:
                monthName = 'October';
                break;
            case 10:
                monthName = 'November';
                break;
            case 11:
                monthName = 'December';
                break;
        }
        return monthName;
    }

    function GetActualMonthYear(latestYear, latestMonthNo, actualMonthNo) {
        if (actualMonthNo < latestMonthNo) {
            latestYear = latestYear.toString().substr(2, 2);
            return latestYear;
        }
        else {
            latestYear = (latestYear - 1).toString().substr(2, 2);
            return latestYear;
        }
    }

    function GetTwoDigDecimal(value) {
        var newValue = new String(value)
        if (newValue.indexOf('.') != -1) {
            return parseFloat(newValue).toFixed(2);
        }
        else {
            return value;
        }
    }

    function floorFigure(val) {
        var dotIndex = new String(val).indexOf('.');
        if (dotIndex != -1) {
            var strValue = new String(val);
            var decimalLength = strValue.substr(dotIndex).length;
            if (decimalLength > 1) {
                return parseFloat(strValue.substr(0, dotIndex + 3));
            }
            else {
                return parseFloat(strValue.substr(0, dotIndex + 2));
            }
        }
        else {
            return parseFloat(val);
        }
    }

    function DrawScheduling(dataValues) {
        var obj = JSON.parse(dataValues);
        var score = 0;

        $.each(obj, function (index, element) {
            score = this.Scheduling_TotalScore;
        });

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['Score', floorFigure(score)]
        ]);

        var options = {
            min: 0, max: 10,
            width: 300, height: 300,
            redFrom: 0, redTo: 5,
            yellowFrom: 5, yellowTo: 7,
            greenFrom: 7, greenTo: 10,
            majorTicks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            minorTicks: 5
        };
        var formatter = new google.visualization.NumberFormat(
        { pattern: '.0' });
        var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
        chart.draw(data, options);
    }

    function DrawSchedulingScore(schedulHistPerform) {
        var schedulHistPerformDatas = JSON.parse(schedulHistPerform);
        var latestYear = new Date().getFullYear();
        var latestMonthNo = new Date().getMonth();
        var latestMonthName = getMonthName(latestMonthNo);
        varCurrentMonth = latestMonthName;
        varCurrentYear = latestYear;

        var scoreVal1 = scoreVal2 = scoreVal3 = scoreVal4 = scoreVal5 = scoreVal6 = scoreVal7 = scoreVal8 = scoreVal9 = scoreVal10 = scoreVal11 = scoreVal12 = 0;
        var Month1 = Month2 = Month3 = Month4 = Month5 = Month6 = Month7 = Month8 = Month9 = Month10 = Month11 = Month12 = 0;

        var varMonthNo = latestMonthNo - 1;

        if (latestMonthNo == 0) {
            latestYear = latestYear - 1;
            latestMonthNo = 12;
            varMonthNo = 11;
        }

        for (var index = 0; index < 12; index++) {
            switch (index) {
                case 0:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 1:
                    Month2 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 2:
                    Month3 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 3:
                    Month4 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 4:
                    Month5 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 5:
                    Month6 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 6:
                    Month7 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 7:
                    Month8 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 8:
                    Month9 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 9:
                    Month10 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 10:
                    Month11 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 11:
                    Month12 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 12:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
            }

            if (varMonthNo == 0) {
                varMonthNo = 11;
            }
            else {
                varMonthNo = varMonthNo - 1;
            }
        }

        /* holding score in variables. */
        $.each(schedulHistPerformDatas, function (index, element) {
            switch (this.Month) {
                case Month1:
                    scoreVal1 = this.Score;
                    break;
                case Month2:
                    scoreVal2 = this.Score;
                    break;
                case Month3:
                    scoreVal3 = this.Score;
                    break;
                case Month4:
                    scoreVal4 = this.Score;
                    break;
                case Month5:
                    scoreVal5 = this.Score;
                    break;
                case Month6:
                    scoreVal6 = this.Score;
                    break;
                case Month7:
                    scoreVal7 = this.Score;
                    break;
                case Month8:
                    scoreVal8 = this.Score;
                    break;
                case Month9:
                    scoreVal9 = this.Score;
                    break;
                case Month10:
                    scoreVal10 = this.Score;
                    break;
                case Month11:
                    scoreVal11 = this.Score;
                    break;
                case Month12:
                    scoreVal12 = this.Score;
                    break;
            }
        });

        var data = google.visualization.arrayToDataTable([
            ['Month', 'Scheduling Score'],
            [Month1, floorFigure(scoreVal1)],
            [Month2, floorFigure(scoreVal2)],
            [Month3, floorFigure(scoreVal3)],
            [Month4, floorFigure(scoreVal4)],
            [Month5, floorFigure(scoreVal5)],
            [Month6, floorFigure(scoreVal6)],
            [Month7, floorFigure(scoreVal7)],
            [Month8, floorFigure(scoreVal8)],
            [Month9, floorFigure(scoreVal9)],
            [Month10, floorFigure(scoreVal10)],
            [Month11, floorFigure(scoreVal11)],
            [Month12, floorFigure(scoreVal12)]
        ]);

        var options = {
            colors: ['#219bd4'],
            width: 590, height: 300,
            vAxis: { ticks: [0, 2, 4, 6, 8, 10] },
            chartArea: { height: 240, width: 520 },
            legend: { position: 'none' },
            backgroundColor: { fill: 'transparent' },
            hAxis: { titleTextStyle: { color: 'red' }, textStyle: { fontSize: 9, bold: true} }

        };

        var chart = new google.visualization.ColumnChart(document.getElementById('barchart_div'));
        chart.draw(data, options);
    }

    function DrawAppointmentSchedulingKeyFigures(schedulHistPerform, schedulErrorCounts) {
        var schedulHistPerformDatas = JSON.parse(schedulHistPerform);
        var schedulErrorCountsDatas = JSON.parse(schedulErrorCounts);

        var latestYear = new Date().getFullYear();
        var latestMonthNo = new Date().getMonth();
        var latestMonthName = getMonthName(latestMonthNo);
        varCurrentMonth = latestMonthName;
        varCurrentYear = latestYear;

        var scoreVal1 = scoreVal2 = scoreVal3 = scoreVal4 = scoreVal5 = scoreVal6 = scoreVal7 = scoreVal8 = scoreVal9 = scoreVal10 = scoreVal11 = scoreVal12 = 0;
        var deliveriesVal1 = deliveriesVal2 = deliveriesVal3 = deliveriesVal4 = deliveriesVal5 = deliveriesVal6 = deliveriesVal7 = deliveriesVal8 = deliveriesVal9 = deliveriesVal10 = deliveriesVal11 = deliveriesVal12 = 0;
        var deliveriesIssuesVal1 = deliveriesIssuesVal2 = deliveriesIssuesVal3 = deliveriesIssuesVal4 = deliveriesIssuesVal5 = deliveriesIssuesVal6 = deliveriesIssuesVal7 = deliveriesIssuesVal8 = deliveriesIssuesVal9 = deliveriesIssuesVal10 = deliveriesIssuesVal11 = deliveriesIssuesVal12 = 0;
        var errorRateVal1 = errorRateVal2 = errorRateVal3 = errorRateVal4 = errorRateVal5 = errorRateVal6 = errorRateVal7 = errorRateVal8 = errorRateVal9 = errorRateVal10 = errorRateVal11 = errorRateVal12 = 0;
        var Month1 = Month2 = Month3 = Month4 = Month5 = Month6 = Month7 = Month8 = Month9 = Month10 = Month11 = Month12 = 0;

        /* Logic to setting the month values */
        var varMonthNo = latestMonthNo - 1;

        if (latestMonthNo == 0) {
            latestYear = latestYear - 1;
            latestMonthNo = 12;
            varMonthNo = 11;
        }
        for (var index = 0; index < 12; index++) {
            switch (index) {
                case 0:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 1:
                    Month2 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 2:
                    Month3 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 3:
                    Month4 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 4:
                    Month5 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 5:
                    Month6 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 6:
                    Month7 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 7:
                    Month8 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 8:
                    Month9 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 9:
                    Month10 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 10:
                    Month11 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 11:
                    Month12 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 12:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
            }

            if (varMonthNo == 0) {
                varMonthNo = 11;
            }
            else {
                varMonthNo = varMonthNo - 1;
            }
        }

        /* holding score in variables. */
        $.each(schedulHistPerformDatas, function (index, element) {
            switch (this.Month) {
                case Month1:
                    scoreVal1 = this.Score;
                    break;
                case Month2:
                    scoreVal2 = this.Score;
                    break;
                case Month3:
                    scoreVal3 = this.Score;
                    break;
                case Month4:
                    scoreVal4 = this.Score;
                    break;
                case Month5:
                    scoreVal5 = this.Score;
                    break;
                case Month6:
                    scoreVal6 = this.Score;
                    break;
                case Month7:
                    scoreVal7 = this.Score;
                    break;
                case Month8:
                    scoreVal8 = this.Score;
                    break;
                case Month9:
                    scoreVal9 = this.Score;
                    break;
                case Month10:
                    scoreVal10 = this.Score;
                    break;
                case Month11:
                    scoreVal11 = this.Score;
                    break;
                case Month12:
                    scoreVal12 = this.Score;
                    break;
            }
        });

        /* holding [# Deliveries], [# Deliveries Issues] and [# Delivery Rate] in variables. */
        $.each(schedulErrorCountsDatas, function (index, element) {
            switch (this.Month) {
                case Month1:
                    deliveriesVal1 = this.BookingCount;
                    deliveriesIssuesVal1 = this.ErrorCount;
                    errorRateVal1 = this.ErrorRate;
                    break;
                case Month2:
                    deliveriesVal2 = this.BookingCount;
                    deliveriesIssuesVal2 = this.ErrorCount;
                    errorRateVal2 = this.ErrorRate;
                    break;
                case Month3:
                    deliveriesVal3 = this.BookingCount;
                    deliveriesIssuesVal3 = this.ErrorCount;
                    errorRateVal3 = this.ErrorRate;
                    break;
                case Month4:
                    deliveriesVal4 = this.BookingCount;
                    deliveriesIssuesVal4 = this.ErrorCount;
                    errorRateVal4 = this.ErrorRate;
                    break;
                case Month5:
                    deliveriesVal5 = this.BookingCount;
                    deliveriesIssuesVal5 = this.ErrorCount;
                    errorRateVal5 = this.ErrorRate;
                    break;
                case Month6:
                    deliveriesVal6 = this.BookingCount;
                    deliveriesIssuesVal6 = this.ErrorCount;
                    errorRateVal6 = this.ErrorRate;
                    break;
                case Month7:
                    deliveriesVal7 = this.BookingCount;
                    deliveriesIssuesVal7 = this.ErrorCount;
                    errorRateVal7 = this.ErrorRate;
                    break;
                case Month8:
                    deliveriesVal8 = this.BookingCount;
                    deliveriesIssuesVal8 = this.ErrorCount;
                    errorRateVal8 = this.ErrorRate;
                    break;
                case Month9:
                    deliveriesVal9 = this.BookingCount;
                    deliveriesIssuesVal9 = this.ErrorCount;
                    errorRateVal9 = this.ErrorRate;
                    break;
                case Month10:
                    deliveriesVal10 = this.BookingCount;
                    deliveriesIssuesVal10 = this.ErrorCount;
                    errorRateVal10 = this.ErrorRate;
                    break;
                case Month11:
                    deliveriesVal11 = this.BookingCount;
                    deliveriesIssuesVal11 = this.ErrorCount;
                    errorRateVal11 = this.ErrorRate;
                    break;
                case Month12:
                    deliveriesVal12 = this.BookingCount;
                    deliveriesIssuesVal12 = this.ErrorCount;
                    errorRateVal12 = this.ErrorRate;
                    break;
            }
        });

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Period');
        data.addColumn('string', 'Score');
        data.addColumn('string', '# Deliveries');
        data.addColumn('string', '# Delivery Issues');
        data.addColumn('string', 'Error Rate');
        data.addRows([
          [Month1, floorFigure(scoreVal1).toString(), floorFigure(deliveriesVal1).toString(), GetTwoDigDecimal(deliveriesIssuesVal1).toString(), GetTwoDigDecimal(errorRateVal1).toString()],
          [Month2, floorFigure(scoreVal2).toString(), floorFigure(deliveriesVal2).toString(), GetTwoDigDecimal(deliveriesIssuesVal2).toString(), GetTwoDigDecimal(errorRateVal2).toString()],
          [Month3, floorFigure(scoreVal3).toString(), floorFigure(deliveriesVal3).toString(), GetTwoDigDecimal(deliveriesIssuesVal3).toString(), GetTwoDigDecimal(errorRateVal3).toString()],
          [Month4, floorFigure(scoreVal4).toString(), floorFigure(deliveriesVal4).toString(), GetTwoDigDecimal(deliveriesIssuesVal4).toString(), GetTwoDigDecimal(errorRateVal4).toString()],
          [Month5, floorFigure(scoreVal5).toString(), floorFigure(deliveriesVal5).toString(), GetTwoDigDecimal(deliveriesIssuesVal5).toString(), GetTwoDigDecimal(errorRateVal5).toString()],
          [Month6, floorFigure(scoreVal6).toString(), floorFigure(deliveriesVal6).toString(), GetTwoDigDecimal(deliveriesIssuesVal6).toString(), GetTwoDigDecimal(errorRateVal6).toString()],
          [Month7, floorFigure(scoreVal7).toString(), floorFigure(deliveriesVal7).toString(), GetTwoDigDecimal(deliveriesIssuesVal7).toString(), GetTwoDigDecimal(errorRateVal7).toString()],
          [Month8, floorFigure(scoreVal8).toString(), floorFigure(deliveriesVal8).toString(), GetTwoDigDecimal(deliveriesIssuesVal8).toString(), GetTwoDigDecimal(errorRateVal8).toString()],
          [Month9, floorFigure(scoreVal9).toString(), floorFigure(deliveriesVal9).toString(), GetTwoDigDecimal(deliveriesIssuesVal9).toString(), GetTwoDigDecimal(errorRateVal9).toString()],
          [Month10, floorFigure(scoreVal10).toString(), floorFigure(deliveriesVal10).toString(), GetTwoDigDecimal(deliveriesIssuesVal10).toString(), GetTwoDigDecimal(errorRateVal10).toString()],
          [Month11, floorFigure(scoreVal11).toString(), floorFigure(deliveriesVal11).toString(), GetTwoDigDecimal(deliveriesIssuesVal11).toString(), GetTwoDigDecimal(errorRateVal11).toString()],
          [Month12, floorFigure(scoreVal12).toString(), floorFigure(deliveriesVal12).toString(), GetTwoDigDecimal(deliveriesIssuesVal12).toString(), GetTwoDigDecimal(errorRateVal12).toString()]
        ]);
        var options = { width: 357, height: 306, showRowNumber: false };
        var table = new google.visualization.Table(document.getElementById('otiftable_div'));
        table.draw(data, options);
    }

    function DrawNumberofDeliveriesMade(schedulErrorCounts) {
        var schedulErrorCountsDatas = JSON.parse(schedulErrorCounts);

        var latestYear = new Date().getFullYear();
        var latestMonthNo = new Date().getMonth();
        var latestMonthName = getMonthName(latestMonthNo);
        varCurrentMonth = latestMonthName;
        varCurrentYear = latestYear;

        var deliveriesVal1 = deliveriesVal2 = deliveriesVal3 = deliveriesVal4 = deliveriesVal5 = deliveriesVal6 =
        deliveriesVal7 = deliveriesVal8 = deliveriesVal9 = deliveriesVal10 = deliveriesVal11 = deliveriesVal12 = 0;
        var Month1 = Month2 = Month3 = Month4 = Month5 = Month6 = Month7 = Month8 = Month9 = Month10 = Month11 = Month12 = 0;

        /* Logic to setting the month values */
        var varMonthNo = latestMonthNo - 1;

        if (latestMonthNo == 0) {
            latestYear = latestYear - 1;
            latestMonthNo = 12;
            varMonthNo = 11;
        }

        for (var index = 0; index < 12; index++) {
            switch (index) {
                case 0:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 1:
                    Month2 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 2:
                    Month3 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 3:
                    Month4 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 4:
                    Month5 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 5:
                    Month6 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 6:
                    Month7 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 7:
                    Month8 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 8:
                    Month9 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 9:
                    Month10 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 10:
                    Month11 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 11:
                    Month12 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 12:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
            }

            if (varMonthNo == 0) {
                varMonthNo = 11;
            }
            else {
                varMonthNo = varMonthNo - 1;
            }
        }

        /* holding [# Deliveries], [# Deliveries Issues] and [# Delivery Rate] in variables. */
        $.each(schedulErrorCountsDatas, function (index, element) {
            switch (this.Month) {
                case Month1:
                    deliveriesVal1 = this.BookingCount;
                    break;
                case Month2:
                    deliveriesVal2 = this.BookingCount;
                    break;
                case Month3:
                    deliveriesVal3 = this.BookingCount;
                    break;
                case Month4:
                    deliveriesVal4 = this.BookingCount;
                    break;
                case Month5:
                    deliveriesVal5 = this.BookingCount;
                    break;
                case Month6:
                    deliveriesVal6 = this.BookingCount;
                    break;
                case Month7:
                    deliveriesVal7 = this.BookingCount;
                    break;
                case Month8:
                    deliveriesVal8 = this.BookingCount;
                    break;
                case Month9:
                    deliveriesVal9 = this.BookingCount;
                    break;
                case Month10:
                    deliveriesVal10 = this.BookingCount;
                    break;
                case Month11:
                    deliveriesVal11 = this.BookingCount;
                    break;
                case Month12:
                    deliveriesVal12 = this.BookingCount;
                    break;
            }
        });

        var data = google.visualization.arrayToDataTable([
          ['Month', 'Deliveries Made'],
          [Month1, floorFigure(deliveriesVal1)],
          [Month2, floorFigure(deliveriesVal2)],
          [Month3, floorFigure(deliveriesVal3)],
          [Month4, floorFigure(deliveriesVal4)],
          [Month5, floorFigure(deliveriesVal5)],
          [Month6, floorFigure(deliveriesVal6)],
          [Month7, floorFigure(deliveriesVal7)],
          [Month8, floorFigure(deliveriesVal8)],
          [Month9, floorFigure(deliveriesVal9)],
          [Month10, floorFigure(deliveriesVal10)],
          [Month11, floorFigure(deliveriesVal11)],
          [Month12, floorFigure(deliveriesVal12)]
        ]);

        /*  
        Logic to maintan the below logic for [Number of deliveries made graph] :
        -------------------------------------------------------------------------
        If highest number of deliveries is <=200, range = 0-200
        If highest number of deliveries is <=100, range = 0-100
        If highest number of deliveries is <=50, range = 0-50
        If highest number of deliveries is <=20, range = 0-20
        If highest number of deliveries is <=10, range = 0-10
        */
        var delMode1 = delMode2 = delMode3 = delMode4 = delMode5 = delMode6 = 0;
      //  debugger;
        var GetHighestValue = [];
        GetHighestValue.push(deliveriesVal1);
        GetHighestValue.push(deliveriesVal2);
        GetHighestValue.push(deliveriesVal3);
        GetHighestValue.push(deliveriesVal4);
        GetHighestValue.push(deliveriesVal5);
        GetHighestValue.push(deliveriesVal6);
        GetHighestValue.push(deliveriesVal7);
        GetHighestValue.push(deliveriesVal8);
        GetHighestValue.push(deliveriesVal9);
        GetHighestValue.push(deliveriesVal10);
        GetHighestValue.push(deliveriesVal11);
        GetHighestValue.push(deliveriesVal12);
        GetHighestValue.sort(function (a, b) {
            return a > b ? 1 : a < b ? -1 : 0;
        });       
        GetHighestValue.reverse();
        var Finalvalue;
        var value = GetHighestValue[0].toString();

        function GetAddNumber(getsubstringValue) {
            //debugger;
            var AddedValue;
            if (getsubstringValue == "1") {
                AddedValue = parseInt(value) + 9;
            }
            else if (getsubstringValue == "2") {
                AddedValue = parseInt(value) + 8;
            }
            else if (getsubstringValue == "3") {
                AddedValue = parseInt(value) + 7;
            }
            else if (getsubstringValue == "4") {
                AddedValue = parseInt(value) + 6;
            }
            else if (getsubstringValue == "5") {
                AddedValue = parseInt(value) + 5;
            }
            else if (getsubstringValue == "6") {
                AddedValue = parseInt(value) + 4;
            }
            else if (getsubstringValue == "7") {
                AddedValue = parseInt(value) + 3;
            }
            else if (getsubstringValue == "8") {
                AddedValue = parseInt(value) + 2;
            }
            else if (getsubstringValue == "9") {
                AddedValue = parseInt(value) + 1;
            }
            return AddedValue;
        }
           
        if (parseInt(value) <= 10) {
            delMode1 = 0;
            delMode2 = 2;
            delMode3 = 4;
            delMode4 = 6;
            delMode5 = 8;
            delMode6 = 10;
        }
        else {
            var getsubstringValue = value.substring(value.length - 1, value.length);

            if (getsubstringValue == "0") {
                Finalvalue = value;
            }
            else {
                Finalvalue = GetAddNumber(getsubstringValue);
            }
            var slot = parseInt(Finalvalue) / 5;

           
            delMode1 = 0;
            delMode2 = slot;
            delMode3 = parseInt(delMode2) + parseInt(slot);
            delMode4 = parseInt(delMode3) + parseInt(slot);
            delMode5 = parseInt(delMode4) + parseInt(slot);
            delMode6 = parseInt(delMode5) + parseInt(slot);
        }


        
//        if ((floorFigure(deliveriesVal1) > 100 && floorFigure(deliveriesVal1) <= 200) || (floorFigure(deliveriesVal2) > 100 && floorFigure(deliveriesVal2) <= 200) ||
//            (floorFigure(deliveriesVal3) > 100 && floorFigure(deliveriesVal3) <= 200) || (floorFigure(deliveriesVal4) > 100 && floorFigure(deliveriesVal4) <= 200) ||
//            (floorFigure(deliveriesVal5) > 100 && floorFigure(deliveriesVal5) <= 200) || (floorFigure(deliveriesVal6) > 100 && floorFigure(deliveriesVal6) <= 200) ||
//            (floorFigure(deliveriesVal7) > 100 && floorFigure(deliveriesVal7) <= 200) || (floorFigure(deliveriesVal8) > 100 && floorFigure(deliveriesVal8) <= 200) ||
//            (floorFigure(deliveriesVal9) > 100 && floorFigure(deliveriesVal9) <= 200) || (floorFigure(deliveriesVal10) > 100 && floorFigure(deliveriesVal10) <= 200) ||
//            (floorFigure(deliveriesVal11) > 100 && floorFigure(deliveriesVal11) <= 200) || (floorFigure(deliveriesVal12) > 100 && floorFigure(deliveriesVal12) <= 200)) {
//            delMode1 = 0;
//            delMode2 = 40;
//            delMode3 = 80;
//            delMode4 = 120;
//            delMode5 = 160;
//            delMode6 = 200;
//        }
//        else if ((floorFigure(deliveriesVal1) > 50 && floorFigure(deliveriesVal1) <= 100) || (floorFigure(deliveriesVal2) > 50 && floorFigure(deliveriesVal2) <= 100) ||
//            (floorFigure(deliveriesVal3) > 50 && floorFigure(deliveriesVal3) <= 100) || (floorFigure(deliveriesVal4) > 50 && floorFigure(deliveriesVal4) <= 100) ||
//            (floorFigure(deliveriesVal5) > 50 && floorFigure(deliveriesVal5) <= 100) || (floorFigure(deliveriesVal6) > 50 && floorFigure(deliveriesVal6) <= 100) ||
//            (floorFigure(deliveriesVal7) > 50 && floorFigure(deliveriesVal7) <= 100) || (floorFigure(deliveriesVal8) > 50 && floorFigure(deliveriesVal8) <= 100) ||
//            (floorFigure(deliveriesVal9) > 50 && floorFigure(deliveriesVal9) <= 100) || (floorFigure(deliveriesVal10) > 50 && floorFigure(deliveriesVal10) <= 100) ||
//            (floorFigure(deliveriesVal11) > 50 && floorFigure(deliveriesVal11) <= 100) || (floorFigure(deliveriesVal12) > 50 && floorFigure(deliveriesVal12) <= 100)) {
//            delMode1 = 0;
//            delMode2 = 20;
//            delMode3 = 40;
//            delMode4 = 60;
//            delMode5 = 80;
//            delMode6 = 100;
//        }
//        else if ((floorFigure(deliveriesVal1) > 20 && floorFigure(deliveriesVal1) <= 50) || (floorFigure(deliveriesVal2) > 20 && floorFigure(deliveriesVal2) <= 50) ||
//            (floorFigure(deliveriesVal3) > 20 && floorFigure(deliveriesVal3) <= 50) || (floorFigure(deliveriesVal4) > 20 && floorFigure(deliveriesVal4) <= 50) ||
//            (floorFigure(deliveriesVal5) > 20 && floorFigure(deliveriesVal5) <= 50) || (floorFigure(deliveriesVal6) > 20 && floorFigure(deliveriesVal6) <= 50) ||
//            (floorFigure(deliveriesVal7) > 20 && floorFigure(deliveriesVal7) <= 50) || (floorFigure(deliveriesVal8) > 20 && floorFigure(deliveriesVal8) <= 50) ||
//            (floorFigure(deliveriesVal9) > 20 && floorFigure(deliveriesVal9) <= 50) || (floorFigure(deliveriesVal10) > 20 && floorFigure(deliveriesVal10) <= 50) ||
//            (floorFigure(deliveriesVal11) > 20 && floorFigure(deliveriesVal11) <= 50) || (floorFigure(deliveriesVal12) > 20 && floorFigure(deliveriesVal12) <= 50)) {
//            delMode1 = 0;
//            delMode2 = 10;
//            delMode3 = 20;
//            delMode4 = 30;
//            delMode5 = 40;
//            delMode6 = 50;
//        }
//        else if ((floorFigure(deliveriesVal1) > 10 && floorFigure(deliveriesVal1) <= 20) || (floorFigure(deliveriesVal2) > 10 && floorFigure(deliveriesVal2) <= 20) ||
//            (floorFigure(deliveriesVal3) > 10 && floorFigure(deliveriesVal3) <= 20) || (floorFigure(deliveriesVal4) > 10 && floorFigure(deliveriesVal4) <= 20) ||
//            (floorFigure(deliveriesVal5) > 10 && floorFigure(deliveriesVal5) <= 20) || (floorFigure(deliveriesVal6) > 10 && floorFigure(deliveriesVal6) <= 20) ||
//            (floorFigure(deliveriesVal7) > 10 && floorFigure(deliveriesVal7) <= 20) || (floorFigure(deliveriesVal8) > 10 && floorFigure(deliveriesVal8) <= 20) ||
//            (floorFigure(deliveriesVal9) > 10 && floorFigure(deliveriesVal9) <= 20) || (floorFigure(deliveriesVal10) > 10 && floorFigure(deliveriesVal10) <= 20) ||
//            (floorFigure(deliveriesVal11) > 10 && floorFigure(deliveriesVal11) <= 20) || (floorFigure(deliveriesVal12) > 10 && floorFigure(deliveriesVal12) <= 20)) {
//            delMode1 = 0;
//            delMode2 = 4;
//            delMode3 = 8;
//            delMode4 = 12;
//            delMode5 = 16;
//            delMode6 = 20;
//        }
//        else if ((floorFigure(deliveriesVal1) > 0 && floorFigure(deliveriesVal1) <= 10) || (floorFigure(deliveriesVal2) > 0 && floorFigure(deliveriesVal2) <= 10) ||
//            (floorFigure(deliveriesVal3) > 0 && floorFigure(deliveriesVal3) <= 10) || (floorFigure(deliveriesVal4) > 0 && floorFigure(deliveriesVal4) <= 10) ||
//            (floorFigure(deliveriesVal5) > 0 && floorFigure(deliveriesVal5) <= 10) || (floorFigure(deliveriesVal6) > 0 && floorFigure(deliveriesVal6) <= 10) ||
//            (floorFigure(deliveriesVal7) > 0 && floorFigure(deliveriesVal7) <= 10) || (floorFigure(deliveriesVal8) > 0 && floorFigure(deliveriesVal8) <= 10) ||
//            (floorFigure(deliveriesVal9) > 0 && floorFigure(deliveriesVal9) <= 10) || (floorFigure(deliveriesVal10) > 0 && floorFigure(deliveriesVal10) <= 10) ||
//            (floorFigure(deliveriesVal11) > 0 && floorFigure(deliveriesVal11) <= 10) || (floorFigure(deliveriesVal12) > 0 && floorFigure(deliveriesVal12) <= 10)) {
//            delMode1 = 0;
//            delMode2 = 2;
//            delMode3 = 4;
//            delMode4 = 6;
//            delMode5 = 8;
//            delMode6 = 10;
//        }
//        else {
//            delMode1 = 0;
//            delMode2 = 2;
//            delMode3 = 4;
//            delMode4 = 6;
//            delMode5 = 8;
//            delMode6 = 10;
//        }

        var options = {

            width: 590, height: 350,
            vAxis: { ticks: [delMode1, delMode2, delMode3, delMode4, delMode5, delMode6] },
            chartArea: { height: 200, width: 520 },
            legend: { position: 'none' },
            pointSize: 7,
            crosshair: { focused: { color: '#3bc', opacity: 0.8} },
            backgroundColor: { fill: 'transparent' },
            hAxis: { titleTextStyle: { color: 'red' }, textStyle: { fontSize: 9, bold: true} }
        };

        var chart = new google.visualization.LineChart(document.getElementById('lineChart_div'));
        chart.draw(data, options);
    }

    $(function () {
        var vendorId = '<%= vendorId %>';
        var vscURL = '<%= vscURL %>';
        $("#ancBacktoMainPage").attr('href', vscURL);

        $.ajax({
            type: "POST",
            url: "AjaxCallPage.aspx/GetVendorScoreCardSchedulingReportData",
            data: '{vendorId:' + vendorId + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                debugger;
                DrawScheduling(response.d.SchedulingMain);
                DrawSchedulingScore(response.d.SchedulingHistPerformance);
                DrawAppointmentSchedulingKeyFigures(response.d.SchedulingHistPerformance, response.d.SchedulingErrorCounts);
                DrawNumberofDeliveriesMade(response.d.SchedulingErrorCounts);
            }
        });
    });
</script>
</html>

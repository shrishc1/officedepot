﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DiscrepancyScoreCard.aspx.cs"
    Inherits="DiscrepancyScoreCard" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../../Css/chartstyle.css" rel="stylesheet" type="text/css" />
     <script src="../../../Scripts/loader.js"></script>
    <style>
        .google-visualization-table-table tr td:first-child
        {
            width: 70px;
        }
    </style>
</head>
<body>
    <form id="form4" runat="server">
    <div class="main-container">
        <a id="ancBacktoMainPage" runat="server" class="back-to-mainpage"></a>
        <%--<asp:HyperLink ID="hlVendorScoreCardMain" Text="Main Page" NavigateUrl="~/ModuleUI/VendorScorecard/Report/VendorScoreCard.aspx" runat="server"></asp:HyperLink>--%>
        <div class="graph-container">
            <div class="graph-row">
                <div class="left-col">
                    <div class="heading1">
                        Discrepancy</div>
                    <div id='chart_div' style="width: 300px; margin: 0 auto; padding-top: 6px;">
                    </div>
                </div>
                <div class="right-col">
                    <div class="heading2">
                        Discrepancy Score</div>
                    <div id='barchart_div'>
                    </div>
                </div>
            </div>
            <div class="graph-row" style="margin-top: 10px;">
                <div class="left-col data-grid">
                    <div class="heading1">
                        Discrepancy Key Figures</div>
                    <div id='otiftable_div'>
                    </div>
                </div>
                <div class="right-col">
                    <div class="heading2">
                        Average Response Time
                        (in Hrs.)</div>
                    <div id='lineChart_div'>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
<script type='text/javascript' src="../../../Scripts/jsapi.js"></script>
<script src="../../../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="../../../Scripts/json2.js" type="text/javascript"></script>
<script type="text/javascript">
    google.load('visualization', '1', { packages: ['gauge'] });
    google.load("visualization", "1", { packages: ["corechart"] });
    google.load('visualization', '1', { packages: ['table'] });
</script>
<script type='text/javascript'>
    var varCurrentMonth = '';
    var varCurrentYear = '';

    function getMonthName(month) {
        var monthName = '';
        switch (month) {
            case 0:
                monthName = 'January';
                break;
            case 1:
                monthName = 'February';
                break;
            case 2:
                monthName = 'March';
                break;
            case 3:
                monthName = 'April';
                break;
            case 4:
                monthName = 'May';
                break;
            case 5:
                monthName = 'June';
                break;
            case 6:
                monthName = 'July';
                break;
            case 7:
                monthName = 'August';
                break;
            case 8:
                monthName = 'September';
                break;
            case 9:
                monthName = 'October';
                break;
            case 10:
                monthName = 'November';
                break;
            case 11:
                monthName = 'December';
                break;
        }
        return monthName;
    }

    function GetTwoDigDecimal(value) {
        var newValue = new String(value)
        if (newValue.indexOf('.') != -1) {
            return parseFloat(newValue).toFixed(2);
        }
        else {
            return value;
        }
    }

    function GetActualMonthYear(latestYear, latestMonthNo, actualMonthNo) {
        if (actualMonthNo < latestMonthNo) {
            latestYear = latestYear.toString().substr(2, 2);
            return latestYear;
        }
        else {
            latestYear = (latestYear - 1).toString().substr(2, 2);
            return latestYear;
        }
    }

    function floorFigure(val) {
        var dotIndex = new String(val).indexOf('.');
        if (dotIndex != -1) {
            var strValue = new String(val);
            var decimalLength = strValue.substr(dotIndex).length;
            if (decimalLength > 1) {
                return parseFloat(strValue.substr(0, dotIndex + 3));
            }
            else {
                return parseFloat(strValue.substr(0, dotIndex + 2));
            }
        }
        else {
            return parseFloat(val);
        }
    }

    function DrawDiscrepancy(discHistPerform) {        
        var discHistPerformDatas = JSON.parse(discHistPerform);
        var latestYear = new Date().getFullYear();
        var latestMonthNo = new Date().getMonth();
        var latestMonthName = getMonthName(latestMonthNo);
        varCurrentMonth = latestMonthName;
        varCurrentYear = latestYear;
        var scoreVal1 = 0;
        var Month1 = 0;

        var varMonthNo = latestMonthNo - 1;

        if (latestMonthNo == 0) {
            latestYear = latestYear - 1;
            latestMonthNo = 12;
            varMonthNo = 11;
        }
        for (var index = 0; index < 12; index++) {
            switch (index) {
                case 0:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;                
            }

            if (varMonthNo == 0) {
                varMonthNo = 11;
            }
            else {
                varMonthNo = varMonthNo - 1;
            }
        }

        $.each(discHistPerformDatas, function (index, element) {
            switch (this.Month) {
                case Month1:
                    scoreVal1 = this.Score;
                    break;                
            }
        });

        var data = google.visualization.arrayToDataTable([['Label', 'Value'], ['Score', floorFigure(scoreVal1)]]);
        var options = {
            min: 0, max: 10,
            width: 300, height: 300,
            redFrom: 0, redTo: 5,
            yellowFrom: 5, yellowTo: 7,
            greenFrom: 7, greenTo: 10,
            majorTicks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            minorTicks: 5
        };
        var formatter = new google.visualization.NumberFormat({ pattern: '.0' });
        var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
        chart.draw(data, options);
    }

    function DrawDiscrepancyScore(discHistPerform) {
        var discHistPerformDatas = JSON.parse(discHistPerform);
        var latestYear = new Date().getFullYear();
        var latestMonthNo = new Date().getMonth();
        var latestMonthName = getMonthName(latestMonthNo);
        varCurrentMonth = latestMonthName;
        varCurrentYear = latestYear;

        var scoreVal1 = scoreVal2 = scoreVal3 = scoreVal4 = scoreVal5 = scoreVal6 = scoreVal7 = scoreVal8 = scoreVal9 = scoreVal10 = scoreVal11 = scoreVal12 = 0;
        var Month1 = Month2 = Month3 = Month4 = Month5 = Month6 = Month7 = Month8 = Month9 = Month10 = Month11 = Month12 = 0;

        var varMonthNo = latestMonthNo - 1;

        if (latestMonthNo == 0) {
            latestYear = latestYear - 1;
            latestMonthNo = 12;
            varMonthNo = 11;
        }

        for (var index = 0; index < 12; index++) {
            switch (index) {
                case 0:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 1:
                    Month2 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 2:
                    Month3 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 3:
                    Month4 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 4:
                    Month5 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 5:
                    Month6 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 6:
                    Month7 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 7:
                    Month8 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 8:
                    Month9 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 9:
                    Month10 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 10:
                    Month11 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 11:
                    Month12 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 12:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
            }

            if (varMonthNo == 0) {
                varMonthNo = 11;
            }
            else {
                varMonthNo = varMonthNo - 1;
            }
        }

        /* holding score in variables. */
        $.each(discHistPerformDatas, function (index, element) {
            switch (this.Month) {
                case Month1:
                    scoreVal1 = this.Score;
                    break;
                case Month2:
                    scoreVal2 = this.Score;
                    break;
                case Month3:
                    scoreVal3 = this.Score;
                    break;
                case Month4:
                    scoreVal4 = this.Score;
                    break;
                case Month5:
                    scoreVal5 = this.Score;
                    break;
                case Month6:
                    scoreVal6 = this.Score;
                    break;
                case Month7:
                    scoreVal7 = this.Score;
                    break;
                case Month8:
                    scoreVal8 = this.Score;
                    break;
                case Month9:
                    scoreVal9 = this.Score;
                    break;
                case Month10:
                    scoreVal10 = this.Score;
                    break;
                case Month11:
                    scoreVal11 = this.Score;
                    break;
                case Month12:
                    scoreVal12 = this.Score;
                    break;
            }
        });

        var data = google.visualization.arrayToDataTable([
            ['Month', 'Discrepancy Score'],
            [Month1, floorFigure(scoreVal1)],
            [Month2, floorFigure(scoreVal2)],
            [Month3, floorFigure(scoreVal3)],
            [Month4, floorFigure(scoreVal4)],
            [Month5, floorFigure(scoreVal5)],
            [Month6, floorFigure(scoreVal6)],
            [Month7, floorFigure(scoreVal7)],
            [Month8, floorFigure(scoreVal8)],
            [Month9, floorFigure(scoreVal9)],
            [Month10, floorFigure(scoreVal10)],
            [Month11, floorFigure(scoreVal11)],
            [Month12, floorFigure(scoreVal12)]
        ]);

        var options = {
            colors: ['#219bd4'],
            width: 590, height: 300,
            vAxis: { ticks: [0, 2, 4, 6, 8, 10] },
            chartArea: { height: 240, width: 520 },
            legend: { position: 'none' },
            backgroundColor: { fill: 'transparent' },
            hAxis: { titleTextStyle: { color: 'red' }, textStyle: { fontSize: 9, bold: true} }
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('barchart_div'));
        chart.draw(data, options);
    }

    function DrawDiscrepancyKeyFigures(discHistPerform, discRate, discDays) {
        
        var discHistPerformDatas = JSON.parse(discHistPerform);
        var discRateDatas = JSON.parse(discRate);
        var discDaysDatas = JSON.parse(discDays);

        var latestYear = new Date().getFullYear();
        var latestMonthNo = new Date().getMonth();
        var latestMonthName = getMonthName(latestMonthNo);
        varCurrentMonth = latestMonthName;
        varCurrentYear = latestYear;

        var scoreVal1 = scoreVal2 = scoreVal3 = scoreVal4 = scoreVal5 = scoreVal6 = scoreVal7 = scoreVal8 = scoreVal9 = scoreVal10 = scoreVal11 = scoreVal12 = 0;
        var lineReceiptVal1 = lineReceiptVal2 = lineReceiptVal3 = lineReceiptVal4 = lineReceiptVal5 = lineReceiptVal6 = lineReceiptVal7 = lineReceiptVal8 = lineReceiptVal9 = lineReceiptVal10 = lineReceiptVal11 = lineReceiptVal12 = 0;
        var discRaisedVal1 = discRaisedVal2 = discRaisedVal3 = discRaisedVal4 = discRaisedVal5 = discRaisedVal6 = discRaisedVal7 = discRaisedVal8 = discRaisedVal9 = discRaisedVal10 = discRaisedVal11 = discRaisedVal12 = 0;
        var errorRateVal1 = errorRateVal2 = errorRateVal3 = errorRateVal4 = errorRateVal5 = errorRateVal6 = errorRateVal7 = errorRateVal8 = errorRateVal9 = errorRateVal10 = errorRateVal11 = errorRateVal12 = 0;
        var responseTimeVal1 = responseTimeVal2 = responseTimeVal3 = responseTimeVal4 = responseTimeVal5 = responseTimeVal6 = responseTimeVal7 = responseTimeVal8 = responseTimeVal9 = responseTimeVal10 = responseTimeVal11 = responseTimeVal12 = 0;
        var Month1 = Month2 = Month3 = Month4 = Month5 = Month6 = Month7 = Month8 = Month9 = Month10 = Month11 = Month12 = 0;

        var sresponseTimeVal1 = sresponseTimeVal2 = sresponseTimeVal3 = sresponseTimeVal4 = sresponseTimeVal5 = sresponseTimeVal6 = sresponseTimeVal7 = sresponseTimeVal8 = sresponseTimeVal9 = sresponseTimeVal10 = sresponseTimeVal11 = sresponseTimeVal12 = '';
        /* Logic to setting the month values */
        var varMonthNo = latestMonthNo - 1;

        if (latestMonthNo == 0) {
            latestYear = latestYear - 1;
            latestMonthNo = 12;
            varMonthNo = 11;
        }

        for (var index = 0; index < 12; index++) {
            switch (index) {
                case 0:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 1:
                    Month2 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 2:
                    Month3 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 3:
                    Month4 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 4:
                    Month5 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 5:
                    Month6 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 6:
                    Month7 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 7:
                    Month8 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 8:
                    Month9 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 9:
                    Month10 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 10:
                    Month11 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 11:
                    Month12 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 12:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
            }

            if (varMonthNo == 0) {
                varMonthNo = 11;
            }
            else {
                varMonthNo = varMonthNo - 1;
            }
        }

        /* holding score in variables. */
        $.each(discHistPerformDatas, function (index, element) {
            switch (this.Month) {
                case Month1:
                    scoreVal1 = this.Score;
                    break;
                case Month2:
                    scoreVal2 = this.Score;
                    break;
                case Month3:
                    scoreVal3 = this.Score;
                    break;
                case Month4:
                    scoreVal4 = this.Score;
                    break;
                case Month5:
                    scoreVal5 = this.Score;
                    break;
                case Month6:
                    scoreVal6 = this.Score;
                    break;
                case Month7:
                    scoreVal7 = this.Score;
                    break;
                case Month8:
                    scoreVal8 = this.Score;
                    break;
                case Month9:
                    scoreVal9 = this.Score;
                    break;
                case Month10:
                    scoreVal10 = this.Score;
                    break;
                case Month11:
                    scoreVal11 = this.Score;
                    break;
                case Month12:
                    scoreVal12 = this.Score;
                    break;
            }
        });

        /* holding [Lines Receipted], [Discrepancy Raised] and [Eror Rate] in variables. */
        $.each(discRateDatas, function (index, element) {
            switch (this.Month) {
                case Month1:
                    lineReceiptVal1 = this.LineReciept;
                    discRaisedVal1 = this.DiscrepancyOpen;
                    errorRateVal1 = this.ErrorRate;
                    break;
                case Month2:
                    lineReceiptVal2 = this.LineReciept;
                    discRaisedVal2 = this.DiscrepancyOpen;
                    errorRateVal2 = this.ErrorRate;
                    break;
                case Month3:
                    lineReceiptVal3 = this.LineReciept;
                    discRaisedVal3 = this.DiscrepancyOpen;
                    errorRateVal3 = this.ErrorRate;
                    break;
                case Month4:
                    lineReceiptVal4 = this.LineReciept;
                    discRaisedVal4 = this.DiscrepancyOpen;
                    errorRateVal4 = this.ErrorRate;
                    break;
                case Month5:
                    lineReceiptVal5 = this.LineReciept;
                    discRaisedVal5 = this.DiscrepancyOpen;
                    errorRateVal5 = this.ErrorRate;
                    break;
                case Month6:
                    lineReceiptVal6 = this.LineReciept;
                    discRaisedVal6 = this.DiscrepancyOpen;
                    errorRateVal6 = this.ErrorRate;
                    break;
                case Month7:
                    lineReceiptVal7 = this.LineReciept;
                    discRaisedVal7 = this.DiscrepancyOpen;
                    errorRateVal7 = this.ErrorRate;
                    break;
                case Month8:
                    lineReceiptVal8 = this.LineReciept;
                    discRaisedVal8 = this.DiscrepancyOpen;
                    errorRateVal8 = this.ErrorRate;
                    break;
                case Month9:
                    lineReceiptVal9 = this.LineReciept;
                    discRaisedVal9 = this.DiscrepancyOpen;
                    errorRateVal9 = this.ErrorRate;
                    break;
                case Month10:
                    lineReceiptVal10 = this.LineReciept;
                    discRaisedVal10 = this.DiscrepancyOpen;
                    errorRateVal10 = this.ErrorRate;
                    break;
                case Month11:
                    lineReceiptVal11 = this.LineReciept;
                    discRaisedVal11 = this.DiscrepancyOpen;
                    errorRateVal11 = this.ErrorRate;
                    break;
                case Month12:
                    lineReceiptVal12 = this.LineReciept;
                    discRaisedVal12 = this.DiscrepancyOpen;
                    errorRateVal12 = this.ErrorRate;
                    break;
            }
        });

        /* holding [Response Time] in variables. */
        $.each(discDaysDatas, function (index, element) {
            switch (this.Month) {
                case Month1:
                    responseTimeVal1 = this.Days;
                    sresponseTimeVal1 = this.sDays;
                    break;
                case Month2:
                    responseTimeVal2 = this.Days;
                    sresponseTimeVal2 = this.sDays;
                    break;
                case Month3:
                    responseTimeVal3 = this.Days;
                    sresponseTimeVal3 = this.sDays;
                    break;
                case Month4:
                    responseTimeVal4 = this.Days;
                    sresponseTimeVal4 = this.sDays;
                    break;
                case Month5:
                    responseTimeVal5 = this.Days;
                    sresponseTimeVal5 = this.sDays;
                    break;
                case Month6:
                    responseTimeVal6 = this.Days;
                    sresponseTimeVal6 = this.sDays;
                    break;
                case Month7:
                    responseTimeVal7 = this.Days;
                    sresponseTimeVal7 = this.sDays;
                    break;
                case Month8:
                    responseTimeVal8 = this.Days;
                    sresponseTimeVal8 = this.sDays;
                    break;
                case Month9:
                    responseTimeVal9 = this.Days;
                    sresponseTimeVal9 = this.sDays;
                    break;
                case Month10:
                    responseTimeVal10 = this.Days;
                    sresponseTimeVal10 = this.sDays;
                    break;
                case Month11:
                    responseTimeVal11 = this.Days;
                    sresponseTimeVal11 = this.sDays;
                    break;
                case Month12:
                    responseTimeVal12 = this.Days;
                    sresponseTimeVal12 = this.sDays;
                    break;
            }
        });

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Period');
        data.addColumn('string', 'Score');
        data.addColumn('string', 'Line Reciepted');
        data.addColumn('string', 'Discrepancies Raised');
        data.addColumn('string', 'Error Rate');
        data.addColumn('string', 'Response Time');
        data.addRows([
          [Month1, floorFigure(scoreVal1).toString(), GetTwoDigDecimal(lineReceiptVal1).toString(), GetTwoDigDecimal(discRaisedVal1).toString(), GetTwoDigDecimal(errorRateVal1).toString(), sresponseTimeVal1.toString()],
          [Month2, floorFigure(scoreVal2).toString(), GetTwoDigDecimal(lineReceiptVal2).toString(), GetTwoDigDecimal(discRaisedVal2).toString(), GetTwoDigDecimal(errorRateVal2).toString(), sresponseTimeVal2.toString()],
          [Month3, floorFigure(scoreVal3).toString(), GetTwoDigDecimal(lineReceiptVal3).toString(), GetTwoDigDecimal(discRaisedVal3).toString(), GetTwoDigDecimal(errorRateVal3).toString(), sresponseTimeVal3.toString()],
          [Month4, floorFigure(scoreVal4).toString(), GetTwoDigDecimal(lineReceiptVal4).toString(), GetTwoDigDecimal(discRaisedVal4).toString(), GetTwoDigDecimal(errorRateVal4).toString(), sresponseTimeVal4.toString()],
          [Month5, floorFigure(scoreVal5).toString(), GetTwoDigDecimal(lineReceiptVal5).toString(), GetTwoDigDecimal(discRaisedVal5).toString(), GetTwoDigDecimal(errorRateVal5).toString(), sresponseTimeVal5.toString()],
          [Month6, floorFigure(scoreVal6).toString(), GetTwoDigDecimal(lineReceiptVal6).toString(), GetTwoDigDecimal(discRaisedVal6).toString(), GetTwoDigDecimal(errorRateVal6).toString(), sresponseTimeVal6.toString()],
          [Month7, floorFigure(scoreVal7).toString(), GetTwoDigDecimal(lineReceiptVal7).toString(), GetTwoDigDecimal(discRaisedVal7).toString(), GetTwoDigDecimal(errorRateVal7).toString(), sresponseTimeVal7.toString()],
          [Month8, floorFigure(scoreVal8).toString(), GetTwoDigDecimal(lineReceiptVal8).toString(), GetTwoDigDecimal(discRaisedVal8).toString(), GetTwoDigDecimal(errorRateVal8).toString(), sresponseTimeVal8.toString()],
          [Month9, floorFigure(scoreVal9).toString(), GetTwoDigDecimal(lineReceiptVal9).toString(), GetTwoDigDecimal(discRaisedVal9).toString(), GetTwoDigDecimal(errorRateVal9).toString(), sresponseTimeVal9.toString()],
          [Month10, floorFigure(scoreVal10).toString(), GetTwoDigDecimal(lineReceiptVal10).toString(), GetTwoDigDecimal(discRaisedVal10).toString(), GetTwoDigDecimal(errorRateVal10).toString(), sresponseTimeVal10.toString()],
          [Month11, floorFigure(scoreVal11).toString(), GetTwoDigDecimal(lineReceiptVal11).toString(), GetTwoDigDecimal(discRaisedVal11).toString(), GetTwoDigDecimal(errorRateVal11).toString(), sresponseTimeVal11.toString()],
          [Month12, floorFigure(scoreVal12).toString(), GetTwoDigDecimal(lineReceiptVal12).toString(), GetTwoDigDecimal(discRaisedVal12).toString(), GetTwoDigDecimal(errorRateVal12).toString(), sresponseTimeVal12.toString()]
        ]);

        var options = { width: 357, height: 306, showRowNumber: false };
        var table = new google.visualization.Table(document.getElementById('otiftable_div'));
        table.draw(data, options);
    }

    function DrawDiscrepancyAverageResponseTime(discDays) {
        var discDaysDatas = JSON.parse(discDays);
        var latestYear = new Date().getFullYear();
        var latestMonthNo = new Date().getMonth();
        var latestMonthName = getMonthName(latestMonthNo);
        varCurrentMonth = latestMonthName;
        varCurrentYear = latestYear;

        var responseTimeVal1 = responseTimeVal2 = responseTimeVal3 = responseTimeVal4 = responseTimeVal5 = responseTimeVal6 =
            responseTimeVal7 = responseTimeVal8 = responseTimeVal9 = responseTimeVal10 = responseTimeVal11 = responseTimeVal12 = 0;
        var Month1 = Month2 = Month3 = Month4 = Month5 = Month6 = Month7 = Month8 = Month9 = Month10 = Month11 = Month12 = 0;
        var hresponseTimeVal1 = hresponseTimeVal2 = hresponseTimeVal3 = hresponseTimeVal4 = hresponseTimeVal5 = hresponseTimeVal6 =
            hresponseTimeVal7 = hresponseTimeVal8 = hresponseTimeVal9 = hresponseTimeVal10 = hresponseTimeVal11 = hresponseTimeVal12 = 0.0;

        /* Logic to setting the month values */
        var varMonthNo = latestMonthNo - 1;

        if (latestMonthNo == 0) {
            latestYear = latestYear - 1;
            latestMonthNo = 12;
            varMonthNo = 11;
        }

        for (var index = 0; index < 12; index++) {
            switch (index) {
                case 0:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 1:
                    Month2 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 2:
                    Month3 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 3:
                    Month4 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 4:
                    Month5 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 5:
                    Month6 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 6:
                    Month7 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 7:
                    Month8 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 8:
                    Month9 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 9:
                    Month10 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 10:
                    Month11 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 11:
                    Month12 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 12:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
            }

            if (varMonthNo == 0) {
                varMonthNo = 11;
            }
            else {
                varMonthNo = varMonthNo - 1;
            }
        }

        /* holding [Response Time] in variables. */
        $.each(discDaysDatas, function (index, element) {
           // debugger;
            switch (this.Month) {
                case Month1:
                    responseTimeVal1 = this.Days;
                    hresponseTimeVal1 = this.hDays;
                    break;
                case Month2:
                    responseTimeVal2 = this.Days;
                    hresponseTimeVal2 = this.hDays;
                    break;
                case Month3:
                    responseTimeVal3 = this.Days;
                    hresponseTimeVal3 = this.hDays;
                    break;
                case Month4:
                    responseTimeVal4 = this.Days;
                    hresponseTimeVal4 = this.hDays;
                    break;
                case Month5:
                    responseTimeVal5 = this.Days;
                    hresponseTimeVal5 = this.hDays;
                    break;
                case Month6:
                    responseTimeVal6 = this.Days;
                    hresponseTimeVal6 = this.hDays;
                    break;
                case Month7:
                    responseTimeVal7 = this.Days;
                    hresponseTimeVal7 = this.hDays;
                    break;
                case Month8:
                    responseTimeVal8 = this.Days;
                    hresponseTimeVal8 = this.hDays;
                    break;
                case Month9:
                    responseTimeVal9 = this.Days;
                    hresponseTimeVal9 = this.hDays;
                    break;
                case Month10:
                    responseTimeVal10 = this.Days;
                    hresponseTimeVal10 = this.hDays;
                    break;
                case Month11:
                    responseTimeVal11 = this.Days;
                    hresponseTimeVal11 = this.hDays;
                    break;
                case Month12:
                    responseTimeVal12 = this.Days;
                    hresponseTimeVal12 = this.hDays;
                    break;
            }
        });

        var data = google.visualization.arrayToDataTable([
          ['Month', 'Response Time'],
          [Month1, floorFigure(hresponseTimeVal1)],
          [Month2, floorFigure(hresponseTimeVal2)],
          [Month3, floorFigure(hresponseTimeVal3)],
          [Month4, floorFigure(hresponseTimeVal4)],
          [Month5, floorFigure(hresponseTimeVal5)],
          [Month6, floorFigure(hresponseTimeVal6)],
          [Month7, floorFigure(hresponseTimeVal7)],
          [Month8, floorFigure(hresponseTimeVal8)],
          [Month9, floorFigure(hresponseTimeVal9)],
          [Month10, floorFigure(hresponseTimeVal10)],
          [Month11, floorFigure(hresponseTimeVal11)],
          [Month12, floorFigure(hresponseTimeVal12)]
        ]);

        /*  
        Logic to maintan the below logic for [Average Response Time graph] :
        -------------------------------------------------------------------------
        If highest number of responseTime is <=200, range = 0-200
        If highest number of responseTime is <=100, range = 0-100
        If highest number of responseTime is <=50, range = 0-50
        If highest number of responseTime is <=20, range = 0-20
        If highest number of responseTime is <=10, range = 0-10
        */
        //debugger;      
        var Mode1 = Mode2 = Mode3 = Mode4 = Mode5 = Mode6 = 0;
        var GetHighestValue = [];
        GetHighestValue.push(Math.ceil(hresponseTimeVal1));
        GetHighestValue.push(Math.ceil(hresponseTimeVal2));
        GetHighestValue.push(Math.ceil(hresponseTimeVal3));
        GetHighestValue.push(Math.ceil(hresponseTimeVal4));
        GetHighestValue.push(Math.ceil(hresponseTimeVal5));
        GetHighestValue.push(Math.ceil(hresponseTimeVal6));
        GetHighestValue.push(Math.ceil(hresponseTimeVal7));
        GetHighestValue.push(Math.ceil(hresponseTimeVal8));
        GetHighestValue.push(Math.ceil(hresponseTimeVal9));
        GetHighestValue.push(Math.ceil(hresponseTimeVal10));
        GetHighestValue.push(Math.ceil(hresponseTimeVal11));
        GetHighestValue.push(Math.ceil(hresponseTimeVal12));
        GetHighestValue.sort(function (a, b) {
            return a > b ? 1 : a < b ? -1 : 0;
        });
        GetHighestValue.reverse();
        var Finalvalue;
        var value = GetHighestValue[0].toString();

        function GetAddNumber(getsubstringValue) {
           
            var AddedValue;
            if (getsubstringValue == "1") {
                AddedValue = parseInt(value) + 9;
            }
            else if (getsubstringValue == "2") {
                AddedValue = parseInt(value) + 8;
            }
            else if (getsubstringValue == "3") {
                AddedValue = parseInt(value) + 7;
            }
            else if (getsubstringValue == "4") {
                AddedValue = parseInt(value) + 6;
            }
            else if (getsubstringValue == "5") {
                AddedValue = parseInt(value) + 5;
            }
            else if (getsubstringValue == "6") {
                AddedValue = parseInt(value) + 4;
            }
            else if (getsubstringValue == "7") {
                AddedValue = parseInt(value) + 3;
            }
            else if (getsubstringValue == "8") {
                AddedValue = parseInt(value) + 2;
            }
            else if (getsubstringValue == "9") {
                AddedValue = parseInt(value) + 1;
            }
            return AddedValue;
        }
        if (parseInt(value) <= 10) {
            Mode1 = 0;
            Mode2 = 2;
            Mode3 = 4;
            Mode4 = 6;
            Mode5 = 8;
            Mode6 = 10;
        }
        else {
            var getsubstringValue = value.substring(value.length - 1, value.length);

            if (getsubstringValue == "0") {
                Finalvalue = value;
            }
            else {
                Finalvalue = GetAddNumber(getsubstringValue);
            }
            var slot = parseInt(Finalvalue) / 5;

           
            Mode1 = 0;
            Mode2 = slot;
            Mode3 = parseInt(Mode2) + parseInt(slot);
            Mode4 = parseInt(Mode3) + parseInt(slot);
            Mode5 = parseInt(Mode4) + parseInt(slot);
            Mode6 = parseInt(Mode5) + parseInt(slot);
        }



        
//        if ((floorFigure(responseTimeVal1) > 100 && floorFigure(responseTimeVal1) <= 200) || (floorFigure(responseTimeVal2) > 100 && floorFigure(responseTimeVal2) <= 200) ||
//            (floorFigure(responseTimeVal3) > 100 && floorFigure(responseTimeVal3) <= 200) || (floorFigure(responseTimeVal4) > 100 && floorFigure(responseTimeVal4) <= 200) ||
//            (floorFigure(responseTimeVal5) > 100 && floorFigure(responseTimeVal5) <= 200) || (floorFigure(responseTimeVal6) > 100 && floorFigure(responseTimeVal6) <= 200) ||
//            (floorFigure(responseTimeVal7) > 100 && floorFigure(responseTimeVal7) <= 200) || (floorFigure(responseTimeVal8) > 100 && floorFigure(responseTimeVal8) <= 200) ||
//            (floorFigure(responseTimeVal9) > 100 && floorFigure(responseTimeVal9) <= 200) || (floorFigure(responseTimeVal10) > 100 && floorFigure(responseTimeVal10) <= 200) ||
//            (floorFigure(responseTimeVal11) > 100 && floorFigure(responseTimeVal11) <= 200) || (floorFigure(responseTimeVal12) > 100 && floorFigure(responseTimeVal12) <= 200)) {
//            Mode1 = 0;
//            Mode2 = 40;
//            Mode3 = 80;
//            Mode4 = 120;
//            Mode5 = 160;
//            Mode6 = 200;
//        }
//        else if ((floorFigure(responseTimeVal1) > 50 && floorFigure(responseTimeVal1) <= 100) || (floorFigure(responseTimeVal2) > 50 && floorFigure(responseTimeVal2) <= 100) ||
//            (floorFigure(responseTimeVal3) > 50 && floorFigure(responseTimeVal3) <= 100) || (floorFigure(responseTimeVal4) > 50 && floorFigure(responseTimeVal4) <= 100) ||
//            (floorFigure(responseTimeVal5) > 50 && floorFigure(responseTimeVal5) <= 100) || (floorFigure(responseTimeVal6) > 50 && floorFigure(responseTimeVal6) <= 100) ||
//            (floorFigure(responseTimeVal7) > 50 && floorFigure(responseTimeVal7) <= 100) || (floorFigure(responseTimeVal8) > 50 && floorFigure(responseTimeVal8) <= 100) ||
//            (floorFigure(responseTimeVal9) > 50 && floorFigure(responseTimeVal9) <= 100) || (floorFigure(responseTimeVal10) > 50 && floorFigure(responseTimeVal10) <= 100) ||
//            (floorFigure(responseTimeVal11) > 50 && floorFigure(responseTimeVal11) <= 100) || (floorFigure(responseTimeVal12) > 50 && floorFigure(responseTimeVal12) <= 100)) {
//            Mode1 = 0;
//            Mode2 = 20;
//            Mode3 = 40;
//            Mode4 = 60;
//            Mode5 = 80;
//            Mode6 = 100;
//        }
//        else if ((floorFigure(responseTimeVal1) > 20 && floorFigure(responseTimeVal1) <= 50) || (floorFigure(responseTimeVal2) > 20 && floorFigure(responseTimeVal2) <= 50) ||
//            (floorFigure(responseTimeVal3) > 20 && floorFigure(responseTimeVal3) <= 50) || (floorFigure(responseTimeVal4) > 20 && floorFigure(responseTimeVal4) <= 50) ||
//            (floorFigure(responseTimeVal5) > 20 && floorFigure(responseTimeVal5) <= 50) || (floorFigure(responseTimeVal6) > 20 && floorFigure(responseTimeVal6) <= 50) ||
//            (floorFigure(responseTimeVal7) > 20 && floorFigure(responseTimeVal7) <= 50) || (floorFigure(responseTimeVal8) > 20 && floorFigure(responseTimeVal8) <= 50) ||
//            (floorFigure(responseTimeVal9) > 20 && floorFigure(responseTimeVal9) <= 50) || (floorFigure(responseTimeVal10) > 20 && floorFigure(responseTimeVal10) <= 50) ||
//            (floorFigure(responseTimeVal11) > 20 && floorFigure(responseTimeVal11) <= 50) || (floorFigure(responseTimeVal12) > 20 && floorFigure(responseTimeVal12) <= 50)) {
//            Mode1 = 0;
//            Mode2 = 10;
//            Mode3 = 20;
//            Mode4 = 30;
//            Mode5 = 40;
//            Mode6 = 50;
//        }
//        else if ((floorFigure(responseTimeVal1) > 10 && floorFigure(responseTimeVal1) <= 20) || (floorFigure(responseTimeVal2) > 10 && floorFigure(responseTimeVal2) <= 20) ||
//            (floorFigure(responseTimeVal3) > 10 && floorFigure(responseTimeVal3) <= 20) || (floorFigure(responseTimeVal4) > 10 && floorFigure(responseTimeVal4) <= 20) ||
//            (floorFigure(responseTimeVal5) > 10 && floorFigure(responseTimeVal5) <= 20) || (floorFigure(responseTimeVal6) > 10 && floorFigure(responseTimeVal6) <= 20) ||
//            (floorFigure(responseTimeVal7) > 10 && floorFigure(responseTimeVal7) <= 20) || (floorFigure(responseTimeVal8) > 10 && floorFigure(responseTimeVal8) <= 20) ||
//            (floorFigure(responseTimeVal9) > 10 && floorFigure(responseTimeVal9) <= 20) || (floorFigure(responseTimeVal10) > 10 && floorFigure(responseTimeVal10) <= 20) ||
//            (floorFigure(responseTimeVal11) > 10 && floorFigure(responseTimeVal11) <= 20) || (floorFigure(responseTimeVal12) > 10 && floorFigure(responseTimeVal12) <= 20)) {
//            Mode1 = 0;
//            Mode2 = 4;
//            Mode3 = 8;
//            Mode4 = 12;
//            Mode5 = 16;
//            Mode6 = 20;
//        }
//        else if ((floorFigure(responseTimeVal1) > 0 && floorFigure(responseTimeVal1) <= 10) || (floorFigure(responseTimeVal2) > 0 && floorFigure(responseTimeVal2) <= 10) ||
//            (floorFigure(responseTimeVal3) > 0 && floorFigure(responseTimeVal3) <= 10) || (floorFigure(responseTimeVal4) > 0 && floorFigure(responseTimeVal4) <= 10) ||
//            (floorFigure(responseTimeVal5) > 0 && floorFigure(responseTimeVal5) <= 10) || (floorFigure(responseTimeVal6) > 0 && floorFigure(responseTimeVal6) <= 10) ||
//            (floorFigure(responseTimeVal7) > 0 && floorFigure(responseTimeVal7) <= 10) || (floorFigure(responseTimeVal8) > 0 && floorFigure(responseTimeVal8) <= 10) ||
//            (floorFigure(responseTimeVal9) > 0 && floorFigure(responseTimeVal9) <= 10) || (floorFigure(responseTimeVal10) > 0 && floorFigure(responseTimeVal10) <= 10) ||
//            (floorFigure(responseTimeVal11) > 0 && floorFigure(responseTimeVal11) <= 10) || (floorFigure(responseTimeVal12) > 0 && floorFigure(responseTimeVal12) <= 10)) {
//            Mode1 = 0;
//            Mode2 = 2;
//            Mode3 = 4;
//            Mode4 = 6;
//            Mode5 = 8;
//            Mode6 = 10;
//        }
//        else {
//            Mode1 = 0;
//            Mode2 = 2;
//            Mode3 = 4;
//            Mode4 = 6;
//            Mode5 = 8;
//            Mode6 = 10;
//        }

        var options = {
            width: 590, height: 350,
            vAxis: { ticks: [Mode1, Mode2, Mode3, Mode4, Mode5, Mode6] },
            chartArea: { height: 200, width: 520 },
            legend: { position: 'none' },
            pointSize: 7,
            crosshair: { focused: { color: '#3bc', opacity: 0.8} },
            backgroundColor: { fill: 'transparent' },
            hAxis: { titleTextStyle: { color: 'red' }, textStyle: { fontSize: 9, bold: true} }
        };

        var chart = new google.visualization.LineChart(document.getElementById('lineChart_div'));
        chart.draw(data, options);
    }

    $(function () {
        var vendorId = '<%= vendorId %>';
        var vscURL = '<%= vscURL %>';
        $("#ancBacktoMainPage").attr('href', vscURL);

        $.ajax({
            type: "POST",
            url: "AjaxCallPage.aspx/GetVendorScoreCardDiscrepancyReportData",
            data: '{vendorId:' + vendorId + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                DrawDiscrepancy(response.d.DiscrepancyHistPerformance);
                DrawDiscrepancyScore(response.d.DiscrepancyHistPerformance);
                DrawDiscrepancyKeyFigures(response.d.DiscrepancyHistPerformance, response.d.DiscrepancyRate, response.d.DiscrepancyDays);
                DrawDiscrepancyAverageResponseTime(response.d.DiscrepancyDays);
            }
        });
    });
</script>
</html>

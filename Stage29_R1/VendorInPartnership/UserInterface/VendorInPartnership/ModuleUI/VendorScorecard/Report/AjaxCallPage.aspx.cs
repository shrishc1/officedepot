﻿using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class AjaxCallPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static ScorecardMainBE GetVendorScoreCardMainReportData(int vendorId)
    {
        var scorecardMainBE = new ScorecardMainBE();
        var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["sConn"].ToString());
        if (sqlConnection != null)
        {
            sqlConnection.Open();
            var sqlCommand = new SqlCommand("spRPT_ScoreCard", sqlConnection);
            sqlCommand.Parameters.Add("@Action", SqlDbType.VarChar).Value = "ScoreCardRPT";
            sqlCommand.Parameters.Add("@VendorID", SqlDbType.Int).Value = vendorId;
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 4200;
            var sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            var dsScoreCardRPT = new DataSet();
            sqlDataAdapter.Fill(dsScoreCardRPT);
            scorecardMainBE.HistroicPerformance = Utility.GetJson(dsScoreCardRPT.Tables[0]);
            scorecardMainBE.CurrentPerformance = Utility.GetJson(dsScoreCardRPT.Tables[1]);
            scorecardMainBE.VendorInfo = Utility.GetJson(dsScoreCardRPT.Tables[2]);
        }
        return scorecardMainBE;
    }

    [WebMethod]
    public static ScorecardMainBE GetVendorScoreCardOtifReportData(int vendorId)
    {
        var scorecardMainBE = new ScorecardMainBE();
        var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["sConn"].ToString());
        if (sqlConnection != null)
        {
            sqlConnection.Open();
            var sqlCommand = new SqlCommand("spRPT_ScoreCard", sqlConnection);
            sqlCommand.Parameters.Add("@Action", SqlDbType.VarChar).Value = "OTIFRPT";
            sqlCommand.Parameters.Add("@VendorID", SqlDbType.Int).Value = vendorId;
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 4200;
            var sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            var dsScoreCardRPT = new DataSet();
            sqlDataAdapter.Fill(dsScoreCardRPT);
            scorecardMainBE.OtifMain = Utility.GetJson(dsScoreCardRPT.Tables[0]);
            scorecardMainBE.OtifHistPerformance = Utility.GetJson(dsScoreCardRPT.Tables[1]);
            scorecardMainBE.OtifValues = Utility.GetJson(dsScoreCardRPT.Tables[2]);
            scorecardMainBE.OtifRates = Utility.GetJson(dsScoreCardRPT.Tables[3]);
        }
        return scorecardMainBE;
    }

    [WebMethod]
    public static ScorecardMainBE GetVendorScoreCardDiscrepancyReportData(int vendorId)
    {
        var scorecardMainBE = new ScorecardMainBE();
        var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["sConn"].ToString());
        if (sqlConnection != null)
        {
            sqlConnection.Open();
            var sqlCommand = new SqlCommand("spRPT_ScoreCard", sqlConnection);
            sqlCommand.Parameters.Add("@Action", SqlDbType.VarChar).Value = "DiscRPT";
            sqlCommand.Parameters.Add("@VendorID", SqlDbType.Int).Value = vendorId;
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 4200;
            var sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            var dsScoreCardRPT = new DataSet();
            sqlDataAdapter.Fill(dsScoreCardRPT);
            scorecardMainBE.DiscrepancyMain = Utility.GetJson(dsScoreCardRPT.Tables[0]);
            scorecardMainBE.DiscrepancyRate = Utility.GetJson(dsScoreCardRPT.Tables[1]);
            scorecardMainBE.DiscrepancyDays = Utility.GetJson(dsScoreCardRPT.Tables[2]);
            scorecardMainBE.DiscrepancyHistPerformance = Utility.GetJson(dsScoreCardRPT.Tables[3]);
        }
        return scorecardMainBE;
    }

    [WebMethod]
    public static ScorecardMainBE GetVendorScoreCardSchedulingReportData(int vendorId)
    {
        var scorecardMainBE = new ScorecardMainBE();
        var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["sConn"].ToString());
        if (sqlConnection != null)
        {
            sqlConnection.Open();
            var sqlCommand = new SqlCommand("spRPT_ScoreCard", sqlConnection);
            sqlCommand.Parameters.Add("@Action", SqlDbType.VarChar).Value = "SchedulingRPT";
            sqlCommand.Parameters.Add("@VendorID", SqlDbType.Int).Value = vendorId;
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 4200;
            var sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            var dsScoreCardRPT = new DataSet();
            sqlDataAdapter.Fill(dsScoreCardRPT);
            scorecardMainBE.SchedulingMain = Utility.GetJson(dsScoreCardRPT.Tables[0]);
            scorecardMainBE.SchedulingHistPerformance = Utility.GetJson(dsScoreCardRPT.Tables[1]);
            scorecardMainBE.SchedulingErrorCounts = Utility.GetJson(dsScoreCardRPT.Tables[2]);

        }
        return scorecardMainBE;
    }

    [WebMethod]
    public static ScorecardMainBE GetVendorScoreCardStockReportData(int vendorId)
    {
        var scorecardMainBE = new ScorecardMainBE();
        var sqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["sConn"].ToString());
        if (sqlConnection != null)
        {
            sqlConnection.Open();
            var sqlCommand = new SqlCommand("spRPT_ScoreCard", sqlConnection);
            sqlCommand.Parameters.Add("@Action", SqlDbType.VarChar).Value = "StockRPT";
            sqlCommand.Parameters.Add("@VendorID", SqlDbType.Int).Value = vendorId;
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 4200;
            var sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            var dsScoreCardRPT = new DataSet();
            sqlDataAdapter.Fill(dsScoreCardRPT);
            scorecardMainBE.StockMain = Utility.GetJson(dsScoreCardRPT.Tables[0]);
            scorecardMainBE.StockHistPerformance = Utility.GetJson(dsScoreCardRPT.Tables[1]);
        }
        return scorecardMainBE;
    }

    
}
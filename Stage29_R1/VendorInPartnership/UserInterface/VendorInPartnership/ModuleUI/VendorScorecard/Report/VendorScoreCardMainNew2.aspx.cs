﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using HiQPdf;
using Utilities;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using System.Data;



public partial class VendorScoreCardMain : CommonPage
{
    protected int vendorId = 0;
    protected string calculatedURL = string.Empty;
    protected string ScoreCardPringURL = string.Empty;
    protected string ScoreCardVendorDetailsURL = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            #region Logic to adding the url with querystring in iframe src ...

            //if (GetQueryStringValue("vendorId") != null)
            //{
            //    vendorId = Convert.ToInt32(GetQueryStringValue("vendorId"));
            //    ViewState["vendorId"] = Convert.ToString(vendorId);
            //}

            //if (Request.QueryString["vendorId"] != null)
            //{
            //    vendorId = Convert.ToInt32(Request.QueryString["vendorId"]);
            //    ViewState["vendorId"] = Convert.ToString(vendorId);
            //}

            //var url = "VendorScoreCard.aspx?vendorId={0}";
            //this.ifScoreCard.Attributes.Add("src", string.Format(url, vendorId));

            
            if (GetQueryStringValue("ValidVendor") == null)
            {
                EncryptQueryString("~/ModuleUI/VendorScoreCard/Report/VendorScoreCardSearch.aspx");
            }

            if (GetQueryStringValue("vendorId") != null)
            {
                vendorId = Convert.ToInt32(GetQueryStringValue("vendorId"));
                ViewState["vendorId"] = Convert.ToString(vendorId);
            }

            calculatedURL = EncryptURL(string.Format("CalculatedSheet.aspx?vendorId={0}", vendorId));
            this.ifScoreCard.Attributes.Add("src", EncryptURL(string.Format("VendorScoreCard.aspx?vendorId={0}", vendorId)));


            #endregion
            BindVenodrDetails();
        }

        ScoreCardPringURL = string.Format("PrintingScoreCard.htm?vendorId={0}", vendorId);

       // ScoreCardVendorDetailsURL = string.Format("VendorScoreCardDetails.aspx?vendorId={0}", vendorId);
    }

    public void BindVenodrDetails()
    {
        if (GetQueryStringValue("vendorId") != null)
        {
            MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
            MAS_VendorScoreCardBAL objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();
            DataTable dtVendorScoreCardDetails = new DataTable();
            objVendorScoreCardBE.VendorID = Convert.ToInt32(GetQueryStringValue("vendorId"));
            dtVendorScoreCardDetails = objVendorScoreCardBAL.ScoreCardVendorDetailsBAL(objVendorScoreCardBE);
            if (dtVendorScoreCardDetails.Rows.Count > 0)
            {
                lblVendorNoValue.Text = dtVendorScoreCardDetails.Rows[0]["Vendor#"].ToString();
                lblVendorNameValue.Text = dtVendorScoreCardDetails.Rows[0]["VendorName"].ToString();
                lblCountryValue.Text = dtVendorScoreCardDetails.Rows[0]["Country"].ToString();
                lblAddressValue1.Text = dtVendorScoreCardDetails.Rows[0]["Address1"].ToString();
                lblAddressValue2.Text = dtVendorScoreCardDetails.Rows[0]["Address2"].ToString();
                lblAddressValue3.Text = dtVendorScoreCardDetails.Rows[0]["City"].ToString();
                lblAddressValue4.Text = dtVendorScoreCardDetails.Rows[0]["County"].ToString();
                lblAddressValue5.Text = dtVendorScoreCardDetails.Rows[0]["VMPPIN"].ToString();
                lblAddressValue6.Text = dtVendorScoreCardDetails.Rows[0]["VMPPOU"].ToString();
                lblConnectedToValue.Text = dtVendorScoreCardDetails.Rows[0]["ConnectedTo"].ToString();
                lblStockPlannerValue.Text = dtVendorScoreCardDetails.Rows[0]["StockPlannerName"].ToString();
                lblTelSPValue.Text = dtVendorScoreCardDetails.Rows[0]["StockPlannerContact"].ToString();
                lblAPClerkValue.Text = dtVendorScoreCardDetails.Rows[0]["APClerkName"].ToString();
                lblTelAPValue.Text = dtVendorScoreCardDetails.Rows[0]["APClerkNo"].ToString();
                lblSchedulingValue.Text = dtVendorScoreCardDetails.Rows[0]["SchedulingContact"].ToString();
                lblDiscrepanciesValue.Text = dtVendorScoreCardDetails.Rows[0]["DiscrepancyContact"].ToString();
                lblOtifValue.Text = dtVendorScoreCardDetails.Rows[0]["OTIFContact"].ToString();
                lblScorecardValue.Text = dtVendorScoreCardDetails.Rows[0]["ScorecardContact"].ToString();
                lblInvoiceIssuesValue.Text = dtVendorScoreCardDetails.Rows[0]["InvoiceIssueContact"].ToString();
                lblInventoryValue.Text = dtVendorScoreCardDetails.Rows[0]["Inventory"].ToString();
                lblProcurementValue.Text = dtVendorScoreCardDetails.Rows[0]["Procurement"].ToString();
                lblAccountsPayableValue.Text = dtVendorScoreCardDetails.Rows[0]["AccountsPayable"].ToString();
                lblExecutiveValue.Text = dtVendorScoreCardDetails.Rows[0]["Executive"].ToString();
                lblMPAValue.Text = dtVendorScoreCardDetails.Rows[0]["IsMasterPurchasingAgreement"].ToString();
                lblDocumentValue.Text = dtVendorScoreCardDetails.Rows[0]["MasterPurchasingAgreement"].ToString();
                lblOverstockValue.Text = dtVendorScoreCardDetails.Rows[0]["IsVendorOverstockAgreement"].ToString();
                lblDetailsOverValue.Text = dtVendorScoreCardDetails.Rows[0]["VendorOverstockAgreementDetails"].ToString();
                lblReturnsValue.Text = dtVendorScoreCardDetails.Rows[0]["IsVendorReturnsAgreement"].ToString();
                lblDetailsReturnValue.Text = dtVendorScoreCardDetails.Rows[0]["VendorReturnsAgreementDetails"].ToString();
            }
        }
    }
    protected void DeleteOldfiles(string DirPath)
    {
        var directory = new DirectoryInfo(DirPath);
        DateTime from_date = DateTime.Now.AddDays(-1);

        var files = directory.GetFiles()
          .Where(file => file.LastWriteTime <= from_date && file.Extension.ToLower() == ".pdf");

        foreach (FileInfo fi in files)
        {
            string fullpath = string.Format(fi.FullName);
            if (System.IO.File.Exists(fullpath))
                System.IO.File.Delete(fullpath);
        }


    }

    protected void btnExportPages_Click(object sender, EventArgs e)
    {
        try
        {
            string baseUrl = string.Format("{0}://{1}", Request.Url.Scheme, HttpContext.Current.Request.Url.Authority);
            string localPath = Request.Url.LocalPath.Substring(1, Request.Url.LocalPath.LastIndexOf('/'));


            string mainScoreCard = string.Format("{0}/{1}VendorScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
            string otifFScoreCard = string.Format("{0}/{1}OTIFScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
            string discrepancyScoreCard = string.Format("{0}/{1}DiscrepancyScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
            string schedulingScoreCard = string.Format("{0}/{1}SchedulingScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
            string stockScoreCard = string.Format("{0}/{1}StockScoreCard.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));
            string calculatedSheet = string.Format("{0}/{1}CalculatedSheet.aspx?NoLogin=1&vendorId={2}", baseUrl, localPath, Convert.ToString(ViewState["vendorId"]));


            string fileNameWithUserId = string.Format("ScoreCardReport_{0}_{1}{2}{3}_{4}.pdf", Convert.ToString(ViewState["vendorId"]), DateTime.Now.Day.ToString("00"), DateTime.Now.Month.ToString("00"), DateTime.Now.Year, DateTime.Now.ToString("HH:mm"));

            ConvertMultipleURLToPdf(mainScoreCard, otifFScoreCard, discrepancyScoreCard, schedulingScoreCard, stockScoreCard, calculatedSheet, fileNameWithUserId);
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
       
    }


    protected void ConvertMultipleURLToPdf(string url1, string url2, string url3, string url4, string url5, string url6, string fileNameWithUserId)
    {
        
         // create an empty PDF document
        PdfDocument document = new PdfDocument();

        // add a page to document
        PdfPage page1 = document.AddPage(PdfPageSize.A4, new PdfDocumentMargins(10),
                PdfPageOrientation.Landscape);

        try
        {

            // layout the HTML from URL 1
            PdfHtml html1 = new PdfHtml(url1);
            PdfLayoutInfo html1LayoutInfo = page1.Layout(html1);
            html1.HtmlLoadedTimeout = 300;
           // html1.TriggerMode = ConversionTriggerMode.WaitTime;
           // html1.WaitBeforeConvert = 5;

            // determine the PDF page where to add URL 2
            PdfPage page2 = null;
            System.Drawing.PointF location2 = System.Drawing.PointF.Empty;

            // URL 2 is laid out on a new page with the selected orientation
            page2 = document.AddPage(PdfPageSize.A4, new PdfDocumentMargins(5), PdfPageOrientation.Landscape);
            location2 = System.Drawing.PointF.Empty;

            // layout the HTML from URL 2
            PdfHtml html2 = new PdfHtml(location2.X, location2.Y, url2);
            html2.HtmlLoadedTimeout = 300;
           // html2.TriggerMode = ConversionTriggerMode.WaitTime;
           // html2.WaitBeforeConvert = 5;
            page2.Layout(html2);

            // determine the PDF page where to add URL 3
            PdfPage page3 = null;
            System.Drawing.PointF location3 = System.Drawing.PointF.Empty;

            // URL 3 is laid out on a new page with the selected orientation
            page3 = document.AddPage(PdfPageSize.A4, new PdfDocumentMargins(5), PdfPageOrientation.Landscape);
            location3 = System.Drawing.PointF.Empty;

            // layout the HTML from URL 3
            PdfHtml html3 = new PdfHtml(location2.X, location2.Y, url3);
            html3.HtmlLoadedTimeout = 300;
           // html3.TriggerMode = ConversionTriggerMode.WaitTime;
           // html3.WaitBeforeConvert = 5;
            page3.Layout(html3);

            // determine the PDF page where to add URL 4
            PdfPage page4 = null;
            System.Drawing.PointF location4 = System.Drawing.PointF.Empty;

            // URL 3 is laid out on a new page with the selected orientation
            page4 = document.AddPage(PdfPageSize.A4, new PdfDocumentMargins(5), PdfPageOrientation.Landscape);
            location4 = System.Drawing.PointF.Empty;

            // layout the HTML from URL 4
            PdfHtml html4 = new PdfHtml(location2.X, location2.Y, url4);
            html4.HtmlLoadedTimeout = 300;
           // html4.TriggerMode = ConversionTriggerMode.WaitTime;
           // html4.WaitBeforeConvert = 5;
            page4.Layout(html4);

            // determine the PDF page where to add URL 5
            PdfPage page5 = null;
            System.Drawing.PointF location5 = System.Drawing.PointF.Empty;

            // URL 5 is laid out on a new page with the selected orientation
            page5 = document.AddPage(PdfPageSize.A4, new PdfDocumentMargins(2), PdfPageOrientation.Landscape);
            location5 = System.Drawing.PointF.Empty;

            // layout the HTML from URL 5
            PdfHtml html5 = new PdfHtml(location2.X, location2.Y, url5);
            html5.HtmlLoadedTimeout = 300;
           // html5.TriggerMode = ConversionTriggerMode.WaitTime;
           // html5.WaitBeforeConvert = 5;
            page5.Layout(html5);

            // determine the PDF page where to add URL 6
            PdfPage page6 = null;
            System.Drawing.PointF location6 = System.Drawing.PointF.Empty;

            // URL 6 is laid out on a new page with the selected orientation
            page6 = document.AddPage(PdfPageSize.A4, new PdfDocumentMargins(5), PdfPageOrientation.Landscape);
            location6 = System.Drawing.PointF.Empty;

            // layout the HTML from URL 6
            PdfHtml html6 = new PdfHtml(location2.X, location2.Y, url6);
            html6.HtmlLoadedTimeout = 300;
           // html6.TriggerMode = ConversionTriggerMode.WaitTime;
           // html6.WaitBeforeConvert = 5;
            page6.Layout(html6);


            // write the PDF document to a memory buffer
            byte[] pdfBuffer = document.WriteToMemory();

            HttpResponse httpResponse = HttpContext.Current.Response;

            // add the Content-Type and Content-Disposition HTTP headers         
            httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}; size={1}", fileNameWithUserId, pdfBuffer.Length.ToString()));

            // write the PDF document bytes as attachment to HTTP response 
            httpResponse.BinaryWrite(pdfBuffer);

            // Note: it is important to end the response, otherwise the ASP.NET
            // web page will render its content to PDF document stream
            httpResponse.End();
        }
        catch (Exception ex)
        {
            LogUtility.SaveErrorLogEntry(ex);
        }
        finally
        {
            document.Close();

        }
    }

   protected void btnBack_Click(object sender, EventArgs e)
    {
        EncryptQueryString("VendorScoreCardSearch.aspx");
    }
}
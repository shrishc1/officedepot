﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;

public partial class ModuleUI_VendorScorecard_Report_VendorScoreCardDetails : CommonPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["vendorId"] != null)
        {
            MAS_VendorScoreCardBE  objVendorScoreCardBE = new MAS_VendorScoreCardBE();
            MAS_VendorScoreCardBAL objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();          
            DataTable dtVendorScoreCardDetails = new DataTable();
            objVendorScoreCardBE.VendorID = Convert.ToInt32(Request.QueryString["vendorId"]);
            dtVendorScoreCardDetails=  objVendorScoreCardBAL.ScoreCardVendorDetailsBAL(objVendorScoreCardBE);
            if (dtVendorScoreCardDetails.Rows.Count > 0)
            {
                lblVendorNoValue.Text = dtVendorScoreCardDetails.Rows[0]["Vendor#"].ToString();
                lblVendorNameValue.Text = dtVendorScoreCardDetails.Rows[0]["VendorName"].ToString();
                lblCountryValue.Text = dtVendorScoreCardDetails.Rows[0]["Country"].ToString();
                lblAddressValue1.Text = dtVendorScoreCardDetails.Rows[0]["Address1"].ToString();
                lblAddressValue2.Text = dtVendorScoreCardDetails.Rows[0]["Address2"].ToString();
                lblAddressValue3.Text = dtVendorScoreCardDetails.Rows[0]["City"].ToString();
                lblAddressValue4.Text = dtVendorScoreCardDetails.Rows[0]["County"].ToString();
                lblAddressValue5.Text = dtVendorScoreCardDetails.Rows[0]["VMPPIN"].ToString();
                lblAddressValue6.Text = dtVendorScoreCardDetails.Rows[0]["VMPPOU"].ToString();
                lblConnectedToValue.Text = dtVendorScoreCardDetails.Rows[0]["ConnectedTo"].ToString();
                lblStockPlannerValue.Text = dtVendorScoreCardDetails.Rows[0]["StockPlannerName"].ToString();
                lblTelSPValue.Text = dtVendorScoreCardDetails.Rows[0]["StockPlannerContact"].ToString();
                lblAPClerkValue.Text = dtVendorScoreCardDetails.Rows[0]["APClerkName"].ToString();
                lblTelAPValue.Text = dtVendorScoreCardDetails.Rows[0]["APClerkNo"].ToString();
                lblSchedulingValue.Text = dtVendorScoreCardDetails.Rows[0]["SchedulingContact"].ToString();
                lblDiscrepanciesValue.Text = dtVendorScoreCardDetails.Rows[0]["DiscrepancyContact"].ToString();
                lblOtifValue.Text = dtVendorScoreCardDetails.Rows[0]["OTIFContact"].ToString();
                lblScorecardValue.Text = dtVendorScoreCardDetails.Rows[0]["ScorecardContact"].ToString();
                lblInvoiceIssuesValue.Text = dtVendorScoreCardDetails.Rows[0]["InvoiceIssueContact"].ToString();
                lblInventoryValue.Text = dtVendorScoreCardDetails.Rows[0]["Inventory"].ToString();
                lblProcurementValue.Text = dtVendorScoreCardDetails.Rows[0]["Procurement"].ToString();
                lblAccountsPayableValue.Text = dtVendorScoreCardDetails.Rows[0]["AccountsPayable"].ToString();
                lblExecutiveValue.Text = dtVendorScoreCardDetails.Rows[0]["Executive"].ToString();
                lblMPAValue.Text = dtVendorScoreCardDetails.Rows[0]["IsMasterPurchasingAgreement"].ToString();
                lblDocumentValue.Text = dtVendorScoreCardDetails.Rows[0]["MasterPurchasingAgreement"].ToString();
                lblOverstockValue.Text = dtVendorScoreCardDetails.Rows[0]["IsVendorOverstockAgreement"].ToString();
                lblDetailsOverValue.Text = dtVendorScoreCardDetails.Rows[0]["VendorOverstockAgreementDetails"].ToString();
                lblReturnsValue.Text = dtVendorScoreCardDetails.Rows[0]["IsVendorReturnsAgreement"].ToString();
                lblDetailsReturnValue.Text = dtVendorScoreCardDetails.Rows[0]["VendorReturnsAgreementDetails"].ToString();
            }
        }
    }
}
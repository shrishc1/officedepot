﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CalculatedSheet : CommonPage
{
    protected string vscMainURL = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetQueryStringValue("vendorId") != null)
            vscMainURL = EncryptURL(string.Format("VendorScoreCardMainNew2.aspx?vendorId={0}&ValidVendor=true", GetQueryStringValue("vendorId")));

        if (Request.QueryString["vendorId"] != null)
        {
           ancBacktoMainPage.Visible = false;
        }
    }
}
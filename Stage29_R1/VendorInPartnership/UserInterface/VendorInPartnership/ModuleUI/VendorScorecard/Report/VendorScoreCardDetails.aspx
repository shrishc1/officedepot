﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VendorScoreCardDetails.aspx.cs" Inherits="ModuleUI_VendorScorecard_Report_VendorScoreCardDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>     
     <script src="../../../Scripts/loader.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="1" cellpadding="5" cellspacing="5" width="90%">
        <tr>
        <td colspan="2">
         <table border="0" cellpadding="5" cellspacing="5" width="100%">
                    <tr>
            <td colspan="3" align="center" style="height: 50px; color: #203764; font-size: 20px;
               ">
                <%--<b>
                    <cc1:ucLabel ID="lblVendorDetails" runat="server"></cc1:ucLabel></b>--%>
                <img src="../../../Images/detail_bg.jpg" alt="Manish Singh" width="100%" />
            </td>
            </tr></table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table border="0" cellpadding="5" cellspacing="5" width="100%" >
                    <tr>
                        <td style="width:111px;">
                            <b>
                                <cc1:ucLabel ID="lblVendorNo" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblVendorNoValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:111px">
                            <b>
                                <cc1:ucLabel ID="lblVendorName" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblVendorNameValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:111px">
                            <b>
                                <cc1:ucLabel ID="lblCountry" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblCountryValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:111px">
                            <b>
                                <cc1:ucLabel ID="lblConnectedTo" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblConnectedToValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table border="0" cellpadding="5" cellspacing="5" width="100%">
                    <tr>
                        <td style="width:77px">
                            <b>
                                <cc1:ucLabel ID="lblAddress" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblAddressValue1" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:77px">
                        </td>
                        <td>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblAddressValue2" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:77px">
                        </td>
                        <td>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblAddressValue3" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:77px">
                        </td>
                        <td>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblAddressValue4" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:77px">
                        </td>
                        <td>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblAddressValue5" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:77px">
                        </td>
                        <td>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblAddressValue6" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" cellpadding="5" cellspacing="5" width="100%">
                    <tr>
                        <td colspan="3" style="color: #203764; font-size: 14px">
                            <b>
                                <cc1:ucLabel ID="lblInventoryContact" runat="server"></cc1:ucLabel></b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:113px">
                            <b>
                                <cc1:ucLabel ID="lblStockPlanner" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblStockPlannerValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:113px">
                            <b>
                                <cc1:ucLabel ID="lblTelno" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblTelSPValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table border="0" cellpadding="5" cellspacing="5" width="100%">
                    <tr>
                        <td colspan="3" style="color: #203764; font-size: 14px">
                            <b>
                                <cc1:ucLabel ID="lblAPConatct" runat="server"></cc1:ucLabel>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:89px">
                            <b>
                                <cc1:ucLabel ID="lblAPClerk" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblAPClerkValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:89px">
                            <b>
                                <cc1:ucLabel ID="lblTelno_1" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblTelAPValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table border="0" cellpadding="5" cellspacing="5" width="100%">
                    <tr>
                        <td colspan="6" style="color: #203764; font-size: 14px">
                            <b>
                                <cc1:ucLabel ID="lblVendorPointsofContact" runat="server"></cc1:ucLabel>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:110px">
                            <b>
                                <cc1:ucLabel ID="lblScheduling" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblSchedulingValue" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width:133px">
                            <b>
                                <cc1:ucLabel ID="lblDiscrepancies" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblDiscrepanciesValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:110px">
                            <b>
                                <cc1:ucLabel ID="lblInvoiceIssues" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblInvoiceIssuesValue" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width:133px">
                            <b>
                                <cc1:ucLabel ID="lblOtif" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblOtifValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:110px">
                            <b>
                                <cc1:ucLabel ID="lblScorecard" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblScorecardValue" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width:133px">
                            <b>
                                <cc1:ucLabel ID="lblInventory" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblInventoryValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:110px">
                            <b>
                                <cc1:ucLabel ID="lblProcurement" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblProcurementValue" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width:133px">
                            <b>
                                <cc1:ucLabel ID="lblAccountsPayable" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblAccountsPayableValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:110px">
                            <b>
                                <cc1:ucLabel ID="lblExecutive" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblExecutiveValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table border="0" cellpadding="5" cellspacing="5" width="100%">
                    <tr>
                        <td colspan="6" style="color: #203764; font-size: 14px">
                            <b>
                                <cc1:ucLabel ID="lblAgreements" runat="server"></cc1:ucLabel>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:81px">
                            <b>
                                <cc1:ucLabel ID="lblMPANew" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td style="width:60px;">
                            <cc1:ucLabel ID="lblMPAValue" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width:93px">
                            <b>
                                <cc1:ucLabel ID="lblDocument" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblDocumentValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:81px">
                            <b>
                                <cc1:ucLabel ID="lblOverstock" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblOverstockValue" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width:93px">
                            <b>
                                <cc1:ucLabel ID="lblDetails" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblDetailsOverValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:81px">
                            <b>
                                <cc1:ucLabel ID="lblReturns" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblReturnsValue" runat="server"></cc1:ucLabel>
                        </td>
                        <td style="width:93px">
                            <b>
                                <cc1:ucLabel ID="lblDetails_1" runat="server"></cc1:ucLabel></b>
                        </td>
                        <td>
                            <b>:</b>
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblDetailsReturnValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>

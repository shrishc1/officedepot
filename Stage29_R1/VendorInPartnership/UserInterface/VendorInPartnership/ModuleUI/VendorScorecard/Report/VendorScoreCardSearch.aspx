﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="VendorScoreCardSearch.aspx.cs" Inherits="ModuleUI_Scorecard_Report_Scorecard"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--    *-+<br />
    .0 `&nbsp;&nbsp;&nbsp;--%>
     <script src="../../../Scripts/loader.js"></script>
    <script type="text/javascript" language="javascript">

        function checkVendorSelected(source, args) {
            if (document.getElementById('<%=lstLeft.ClientID%>') != null) {
                var obj = document.getElementById('<%=lstLeft.ClientID%>');
                if (obj.value == '') {
                    args.IsValid = false;
                }
            }
        }

        function checkGlobalVendorSelected(source, args) {
            if (document.getElementById('<%=lstGlobalVendor.ClientID%>') != null) {
                var obj = document.getElementById('<%=lstGlobalVendor.ClientID%>');
                if (obj.value == '') {
                    args.IsValid = false;
                }
            }
        }

        function checkVendorListSelected(source, args) {
            if (document.getElementById('<%=lstVendorslist.ClientID%>') != null) {
                var obj = document.getElementById('<%=lstVendorslist.ClientID%>');
                if (obj.value == '') {
                    args.IsValid = false;
                }
            }
        }


        function ShowSelectedVendorName(type) {
            if (type == 'Local') {
                var txtObj = $('#<%=txtSelectedVendorValue.ClientID%>');
                var lstObj = $('#<%=lstLeft.ClientID%> option:selected'); //.text();
                var hdnID = $('#<%=hdnLocalvendorID.ClientID%>');
            }
            else if (type == 'Global') {
                var txtObj = $('#<%=txtGlobalVendor.ClientID%>');
                var lstObj = $('#<%=lstGlobalVendor.ClientID%> option:selected');  //.text();
                var hdnID = $('#<%=hdnGlobalvendorID.ClientID%>');
            }
            else if (type == 'Vendor') {
                var txtObj = $('#<%=txtSelectedVendor.ClientID%>');
                var lstObj = $('#<%=lstVendorslist.ClientID%> option:selected');  //.text();
                var hdnID = $('#<%=hdnSeletedVendorID.ClientID%>');
            }


            if (lstObj.text() != '') {
                $(txtObj).val(lstObj.text());
                $(hdnID).val(lstObj.val());
            }
        }

        function RemoveSelectedVendorName(type) {
            if (type == 'Local') {
                var txtObj = $('#<%=txtSelectedVendorValue.ClientID%>');
                $('#<%=lstLeft.ClientID%>').find("option").attr("selected", false);
                var hdnID = $('#<%=hdnLocalvendorID.ClientID%>');
            }
            else if (type == 'Global') {
                var txtObj = $('#<%=txtGlobalVendor.ClientID%>');
                $('#<%=lstGlobalVendor.ClientID%>').find("option").attr("selected", false);
                var hdnID = $('#<%=hdnGlobalvendorID.ClientID%>');
            }
            else if (type == 'Vendor') {
                var txtObj = $('#<%=txtSelectedVendor.ClientID%>');
                $('#<%=lstVendorslist.ClientID%>').find("option").attr("selected", false);
                var hdnID = $('#<%=hdnSeletedVendorID.ClientID%>');
            }

            $(txtObj).val('');
            $(hdnID).val('0');
        }
        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGenerateReport.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <h2>
        <cc1:ucLabel ID="lblVendorScoreCard" runat="server" Text="Vendor Score Card"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server" AsyncPostBackTimeout="2400">
    </asp:ScriptManager>
    <div class="right-shadow">
        <div class="formbox">
            <cc1:ucPanel ID="UcPanelHeader" runat="server">
                <cc1:ucPanel ID="pnlSummaryReports_1" runat="server" GroupingText="Summary Report(s)"
                    CssClass="fieldset-form">
                    <cc1:ucLabel ID="lblClickOnYear_1" Text="Please click on year to view report" runat="server"></cc1:ucLabel><br />
                    <br />
                    <div style="width: 18%; vertical-align: top" class="left-nav-month-list">
                        <cc1:ucGridView ID="grdReportLinksVendor" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowHeader="false" CellPadding="0" GridLines="None" CssClass="NoAnchor">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkReport" runat="server" Text='<%# String.Format("{0}",Eval("Year")) %>'
                                            CommandName="ScoreCardReportVendor" OnCommand="btnView_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Height="20px" HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </cc1:ucGridView>
                    </div>
                </cc1:ucPanel>
                <cc1:ucPanel ID="pnlLocalMonthlyReport_3" runat="server" GroupingText="Local Monthly Detailed Report(s)"
                    CssClass="fieldset-form">
                    <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                        <tr>
                            <td colspan="5">
                                <asp:HiddenField ID="hdnSeletedVendorID" runat="server" Value="0" />
                                <cc1:ucLabel ID="lblselectvendortogeneratereports_2" runat="server" Text="Please select vendor from the list and thereafter generate report."></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 2%"></td>
                            <td>
                                <cc1:ucLabel ID="lblVendor_3" runat="server" Text="Vendor" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td>:
                            </td>
                            <td>
                                <table border="0">
                                    <tr>
                                        <td>
                                            <cc1:ucListBox ID="lstVendorslist" runat="server" Height="150px" Width="320px">
                                            </cc1:ucListBox>
                                        </td>
                                        <td align="center">
                                            <div>
                                                <input type="button" id="Button3" value=">" class="button" style="width: 35px" onclick="Javascript: ShowSelectedVendorName('Vendor');" />
                                            </div>
                                            &nbsp;
                                            <div>
                                                <input type="button" id="Button4" value="<" class="button" style="width: 35px" onclick="Javascript: RemoveSelectedVendorName('Vendor');" />
                                            </div>
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <asp:CustomValidator ID="cusvNoSelectedVendor_2" runat="server" ClientValidationFunction="checkVendorListSelected"
                                    ValidationGroup="CheckVendorList" Display="None"></asp:CustomValidator>
                            </td>
                            <td>
                                <cc1:ucTextbox ID="txtSelectedVendor" runat="server" Font-Bold="true" ReadOnly="true"></cc1:ucTextbox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <div class="button-row">
                                    <cc1:ucButton ID="btnGenerateReport_2" runat="server" Text="Generate Report" CssClass="button"
                                        OnClick="btnGenerateReport_2_Click" ValidationGroup="CheckVendorList" />
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                                        ShowSummary="false" Style="color: Red" ValidationGroup="CheckVendorList" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </cc1:ucPanel>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcPanel1" runat="server">
                <asp:HiddenField ID="hdnLocalvendorID" runat="server" Value="0" />
                <asp:HiddenField ID="hdnGlobalvendorID" runat="server" Value="0" />
                <div class="right-shadow">
                    <div class="formbox">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <cc1:ucPanel ID="pnlLocalMonthlyReport" runat="server" GroupingText="Local Monthly Detailed Report(s)"
                                    CssClass="fieldset-form">
                                    <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                                        <tr>
                                            <td colspan="5">
                                                <cc1:ucLabel ID="lblselectvendortogeneratereports" runat="server" Text="Please select vendor from the list and thereafter generate report."></cc1:ucLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 2%"></td>
                                            <td>
                                                <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor" isRequired="true"></cc1:ucLabel>
                                            </td>
                                            <td>:
                                            </td>
                                            <td>
                                                <table border="0">
                                                    <tr>
                                                        <td style="width: 40%">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <cc1:ucTextbox ID="txtVendorNo" runat="server" MaxLength="15" Width="77px"></cc1:ucTextbox>
                                                                    </td>
                                                                    <td>
                                                                        <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendor" OnClick="btnSearch_Click" />&nbsp;&nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendorNumber" OnClick="btnSearchByVendorNo_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 30%;" align="center"></td>
                                                        <td style="width: 30%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <cc1:ucListBox ID="lstLeft" runat="server" Height="150px" Width="370px">
                                                            </cc1:ucListBox>
                                                        </td>
                                                        <td align="center">
                                                            <div>
                                                                <input type="button" id="btnMoveRight" value=">" class="button" style="width: 35px"
                                                                    onclick="Javascript: ShowSelectedVendorName('Local');" />
                                                            </div>
                                                            &nbsp;
                                                            <div>
                                                                <input type="button" id="btnMoveLeft" value="<" class="button" style="width: 35px"
                                                                    onclick="Javascript: RemoveSelectedVendorName('Local');" />
                                                            </div>
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:CustomValidator ID="cusvNoSelectedVendor" runat="server" ClientValidationFunction="checkVendorSelected"
                                                    ValidationGroup="CheckVendor" Display="None"></asp:CustomValidator>
                                            </td>
                                            <td>
                                                <cc1:ucTextbox ID="txtSelectedVendorValue" runat="server" Font-Bold="true" ReadOnly="true"></cc1:ucTextbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <div class="button-row">
                                                    <cc1:ucDropdownList ID="ddlLocalMonthly" runat="server" Width="150px">
                                                        <asp:ListItem>New Report Layout</asp:ListItem>
                                                        <asp:ListItem>Old Report Layout</asp:ListItem>
                                                    </cc1:ucDropdownList>
                                                    <cc1:ucButton ID="btnGenerateReport" runat="server" Text="Generate Report" CssClass="button"
                                                        OnClick="btnGenerateReport_Click" ValidationGroup="CheckVendor" />
                                                    <asp:ValidationSummary ID="vSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
                                                        Style="color: Red" ValidationGroup="CheckVendor" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                                <cc1:ucPanel ID="pnlSummaryReports" runat="server" GroupingText="Summary Report(s)"
                                    CssClass="fieldset-form">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td colspan="10" style="color: #6b6b6b;">
                                                <cc1:ucLabel ID="lblSelectVendorType" runat="server" Text="Select vendor type"></cc1:ucLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="10">
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; width: 25%;" class="checkbox-list">
                                                <cc1:ucRadioButton ID="rdoEuropeanVendorOnly" runat="server" Text="European Vendor Only"
                                                    GroupName="ReportVersion" />
                                            </td>
                                            <td style="font-weight: bold; width: 25%;" class="checkbox-list">
                                                <cc1:ucRadioButton ID="rdoLocalVendorOnly" runat="server" Text="Local Vendor Only"
                                                    GroupName="ReportVersion" />
                                            </td>
                                            <td colspan="10" style="font-weight: bold; width: 25%;" class="checkbox-list">
                                                <cc1:ucRadioButton ID="rdoAllOnly" runat="server" Text="All" GroupName="ReportVersion"
                                                    Checked="true" />
                                            </td>
                                            <td align="right" class="checkbox-list">&nbsp;
                                                <%--  <div class="button-row">
                                                    <cc1:ucButton ID="UcButton1" runat="server" Text="Generate Report" CssClass="button"
                                                        OnClick="btnGenerateReport_Click" />
                                                </div>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8">
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8" style="color: #6b6b6b;">
                                                <cc1:ucLabel ID="lblScorecardModule" runat="server" Text="View overall or module scores"></cc1:ucLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8">
                                                <br />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="font-weight: bold; width: 20%;" class="checkbox-list">
                                                <cc1:ucRadioButton ID="rdoOverallScore" runat="server" Text="Overall Score" GroupName="ReportVersion2"
                                                    Checked="true" />
                                            </td>
                                            <td style="font-weight: bold; width: 20%;" class="checkbox-list">
                                                <cc1:ucRadioButton ID="rdoOTIF" runat="server" Text="OTIF" GroupName="ReportVersion2" />
                                            </td>
                                            <td style="font-weight: bold; width: 20%;" class="checkbox-list">
                                                <cc1:ucRadioButton ID="rdoDiscrepancies" runat="server" Text="Discrepancies" GroupName="ReportVersion2" />
                                            </td>
                                            <td style="font-weight: bold; width: 20%;" class="checkbox-list">
                                                <cc1:ucRadioButton ID="rdoScheduling" runat="server" Text="Scheduling" GroupName="ReportVersion2" />
                                            </td>
                                            <td style="font-weight: bold; width: 20%;" class="checkbox-list">
                                                <cc1:ucRadioButton ID="rdoStock" runat="server" Text="Stock" GroupName="ReportVersion2" />
                                            </td>
                                            <td align="right" class="checkbox-list">&nbsp;
                                                <%--  <div class="button-row">
                                                    <cc1:ucButton ID="UcButton1" runat="server" Text="Generate Report" CssClass="button"
                                                        OnClick="btnGenerateReport_Click" />
                                                </div>--%>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <cc1:ucLabel ID="lblClickOnYear" Text="Please click on year to view report" runat="server"></cc1:ucLabel><br />
                                    <br />
                                    <div style="width: 18%; vertical-align: top" class="left-nav-month-list">
                                        <cc1:ucGridView ID="grdReportLinks" Width="100%" runat="server" AutoGenerateColumns="False"
                                            ShowHeader="false" CellPadding="0" GridLines="None" CssClass="NoAnchor"
                                            OnRowDataBound="grdReportLinks_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkReport" runat="server" Text='<%# String.Format("{0}",Eval("Year")) %>'
                                                            CommandName="ScoreCardReport" OnCommand="btnView_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle Height="20px" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </cc1:ucGridView>
                                    </div>
                                </cc1:ucPanel>
                                <cc1:ucPanel ID="pnlMastervendormonthlydetailed" runat="server" GroupingText="Master Vendor Monthly Detailed"
                                    CssClass="fieldset-form">
                                    <table width="95%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsNoBorder">
                                        <tr>
                                            <td colspan="5">
                                                <cc1:ucLabel ID="lblselectvendortogeneratereports_1" runat="server" Text="Please select vendor from the list and thereafter generate report."></cc1:ucLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 2%"></td>
                                            <td>
                                                <cc1:ucLabel ID="lblVendor_2" runat="server" Text="Vendor" isRequired="true"></cc1:ucLabel>
                                            </td>
                                            <td>:
                                            </td>
                                            <td>
                                                <table border="0">
                                                    <tr>
                                                        <td style="width: 40%">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <cc1:ucTextbox ID="txtVendorName" runat="server" MaxLength="15" Width="77px"></cc1:ucTextbox>
                                                                    </td>
                                                                    <td>
                                                                        <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendor_1" OnClick="btnSearch_1_Click" />&nbsp;&nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <cc1:ucButton CssClass="button" runat="server" ID="btnSearchVendorNumber_1" OnClick="btnSearchByVendorNo1_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 30%;" align="center"></td>
                                                        <td style="width: 30%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <cc1:ucListBox ID="lstGlobalVendor" runat="server" Height="150px" Width="370px">
                                                            </cc1:ucListBox>
                                                        </td>
                                                        <td align="center">
                                                            <div>
                                                                <input type="button" id="Button1" value=">" class="button" style="width: 35px" onclick="Javascript: ShowSelectedVendorName('Global');" />
                                                            </div>
                                                            &nbsp;
                                                            <div>
                                                                <input type="button" id="Button2" value="<" class="button" style="width: 35px" onclick="Javascript: RemoveSelectedVendorName('Global');" />
                                                            </div>
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:CustomValidator ID="cusvNoSelectedVendor_1" runat="server" ClientValidationFunction="checkGlobalVendorSelected"
                                                    ValidationGroup="CheckGlobalVendor" Display="None"></asp:CustomValidator>
                                            </td>
                                            <td>
                                                <cc1:ucTextbox ID="txtGlobalVendor" runat="server" Font-Bold="true" ReadOnly="true"></cc1:ucTextbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <div class="button-row">
                                                    <cc1:ucDropdownList ID="ddlMasterMonthly" runat="server" Width="150px">
                                                        <asp:ListItem>New Report Layout</asp:ListItem>
                                                        <asp:ListItem>Old Report Layout</asp:ListItem>
                                                    </cc1:ucDropdownList>
                                                    <cc1:ucButton ID="btnGenerateReport_1" runat="server" Text="Generate Report" CssClass="button"
                                                        OnClick="btnGenerateReport_1_Click" ValidationGroup="CheckGlobalVendor" />
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                                        ShowSummary="false" Style="color: Red" ValidationGroup="CheckGlobalVendor" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </cc1:ucPanel>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnGenerateReport_1" />
                                <asp:PostBackTrigger ControlID="btnGenerateReport" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="bottom-shadow">
                </div>
            </cc1:ucPanel>
            <cc1:ucPanel ID="UcPanel2" runat="server">
                <div style="display: none;">
                    <rsweb:ReportViewer ID="ExportReportViewer" runat="server" Width="950px" DocumentMapCollapsed="True"
                        Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                        WaitMessageFont-Size="14pt" Height="810px" OnDrillthrough="ScorecardReportViewer_Drillthrough"
                        ShowZoomControl="false" ShowRefreshButton="false" ShowPrintButton="false">
                    </rsweb:ReportViewer>
                </div>
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <rsweb:ReportViewer ID="ScorecardReportViewer" runat="server" Width="950px" DocumentMapCollapsed="True"
                                Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                                WaitMessageFont-Size="14pt" Height="810px" OnDrillthrough="ScorecardReportViewer_Drillthrough"
                                ShowZoomControl="false" ShowRefreshButton="false" ShowPrintButton="false" ShowExportControls="false">
                            </rsweb:ReportViewer>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                                OnClick="btnBack_Click" />&nbsp;&nbsp;
                            <cc1:ucButton ID="btnExportToExcel" runat="server" Text="Export To Excel" CssClass="button"
                                OnClick="btnExportToExcel_Click" />
                        </td>
                    </tr>
                </table>
            </cc1:ucPanel>
        </div>
    </div>
</asp:Content>

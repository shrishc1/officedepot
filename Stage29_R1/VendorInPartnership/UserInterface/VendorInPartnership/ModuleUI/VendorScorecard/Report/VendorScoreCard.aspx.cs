﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using WebUtilities;
using BusinessEntities.ModuleBE.VendorScorecard;

public partial class VendorScoreCard : CommonPage
{
    protected int vendorId = 0;
    protected string otifScoreCardURL = string.Empty;
    protected string discScoreCardURL = string.Empty;
    protected string schScoreCardURL = string.Empty;
    protected string stockScoreCardURL = string.Empty;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetQueryStringValue("vendorId") != null)
        {
            vendorId = Convert.ToInt32(GetQueryStringValue("vendorId"));
            otifScoreCardURL = EncryptURL(string.Format("OTIFScoreCard.aspx?vendorId={0}", vendorId));
            discScoreCardURL = EncryptURL(string.Format("DiscrepancyScoreCard.aspx?vendorId={0}", vendorId));
            schScoreCardURL = EncryptURL(string.Format("SchedulingScoreCard.aspx?vendorId={0}", vendorId));
            stockScoreCardURL = EncryptURL(string.Format("StockScoreCard.aspx?vendorId={0}", vendorId));
        }

        if (Request.QueryString["vendorId"] != null)
        {
            vendorId = Convert.ToInt32(Request.QueryString["vendorId"]);
           
        }
    }
}


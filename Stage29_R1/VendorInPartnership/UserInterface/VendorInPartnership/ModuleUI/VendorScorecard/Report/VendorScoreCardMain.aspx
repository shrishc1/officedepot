﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="VendorScoreCardMain.aspx.cs" Inherits="VendorScoreCardMain" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../../../Css/chartstyle.css" rel="stylesheet" type="text/css" />
   <script src="../../../Scripts/loader.js"></script>
    <div style="width: 991px; margin: 0 auto;">
        <a id="ancCalculatedSheet" class="info-icon" title="Information"></a>
       <%-- <asp:LinkButton ID="lnkCalculatedSheet" runat="server" CssClass="info-icon" 
            onclick="lnkCalculatedSheet_Click"></asp:LinkButton>--%>
        <asp:Button ID="btnExportPages" runat="server" ToolTip="Print" class="print-icon" style="display:none;"
            OnClick="btnExportPages_Click" />
        <iframe id="ifScoreCard" height="950" width="990" frameborder="0" runat="server">
        </iframe>
        <table style="width: 100%;">
            <tr>
                <td align="center">
                    <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                        OnClick="btnBack_Click" />&nbsp;&nbsp;
                </td>
            </tr>
        </table>
    </div>
    <script type='text/javascript'>
        $(function () {
            var calculatedURL = '<%= calculatedURL %>';
            $("#ancCalculatedSheet").attr('href', calculatedURL);
        });
    </script>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VendorScoreCard.aspx.cs"
    Inherits="VendorScoreCard" ValidateRequest="false" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../../Css/chartstyle.css" rel="stylesheet" type="text/css" />
  
    <script src="../../../Scripts/loader.js"></script>
</head>
<body style="margin: 0 auto; padding: 0;">
    <form id="form8" runat="server" method="post">
        <div class="main-container">
            <div class="top-txt">
                <table width="100%">
                    <tr>
                        <th width="120" align="left">
                            <cc1:ucLabel ID="lblVendorNo" runat="server" Text="Vendor No"></cc1:ucLabel>:
                        </th>
                        <th align="left">
                            <cc1:ucLabel ID="lblVendorNoValue" runat="server"></cc1:ucLabel>
                        </th>
                        <th width="250" align="left">
                            <cc1:ucLabel ID="lblVendorNameValue" runat="server"></cc1:ucLabel>
                        </th>
                        <td width="255">
                            <cc1:ucLabel ID="lblPurchaseOrderSpendLastMonth" runat="server" Text="Purchase Order Spend Last Month"></cc1:ucLabel>:
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblPurchaseOrderSpendLastMonthValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <th align="left">
                            <cc1:ucLabel ID="lblPeriod" runat="server" Text="Period"></cc1:ucLabel>:
                        </th>
                        <th colspan="2" align="left">
                            <cc1:ucLabel ID="lblPeriodValue" runat="server"></cc1:ucLabel>
                        </th>
                        <td>
                            <cc1:ucLabel ID="lblPurchaseOrderSpendYTD" runat="server" Text="Purchase Order Spend YTD"></cc1:ucLabel>:
                        </td>
                        <td>
                            <cc1:ucLabel ID="lblPurchaseOrderSpendYTDValue" runat="server"></cc1:ucLabel>
                        </td>
                    </tr>
                    <tr>
                        <th align="left">
                            <cc1:ucLabel ID="lblCountry" runat="server" Text="Country"></cc1:ucLabel>:
                        </th>
                        <th colspan="2" align="left">
                            <cc1:ucLabel ID="lblCountryValue" runat="server"></cc1:ucLabel>
                        </th>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div class="hr-shadow">
            </div>
            <div class="graph-container">
                <div class="left-col">
                    <div class="heading1">
                        Overall Performance Last Month
                    </div>
                    <div id='chart_div' style="margin: 0 auto; width: 306px; padding-top: 6px;">
                    </div>
                </div>
                <div class="right-col">
                    <div class="heading2">
                        Overall Performance
                    </div>
                    <div id='barchart_div'>
                    </div>
                </div>
            </div>
            <h3>Please click on the icon to drill into more detail associated to your performance</h3>
            <div class="bottom-subreport">
                <div class="subreport">
                    <a id="ancOTIFScoreCard" class="linkbtn">OTIF</a> <a id="ancOTIFScoreCardMap">
                        <div id='Subreport_guage' style="width: 200px; margin: 0 auto">
                        </div>
                    </a>
                </div>
                <div class="subreport">
                    <a id="ancDiscrepancyScoreCard" class="linkbtn">Discrepancy</a> <a id="ancDiscrepancyScoreCardMap">
                        <div id='Subreport_guage1' style="width: 200px; margin: 0 auto">
                        </div>
                    </a>
                </div>
                <div class="subreport">
                    <a id="ancSchedulingScoreCard" class="linkbtn">Scheduling</a> <a id="ancSchedulingScoreCardMap">
                        <div id='Subreport_guage2' style="width: 200px; margin: 0 auto">
                        </div>
                    </a>
                </div>
                <div class="subreport">
                    <a id="ancStockScoreCard" class="linkbtn">Stock</a> <a id="ancStockScoreCardMap">
                        <div id='Subreport_guage3' style="width: 200px; margin: 0 auto">
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </form>
</body>
<script type='text/javascript' src="../../../Scripts/jsapi.js"></script>
<script src="../../../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="../../../Scripts/json2.js" type="text/javascript"></script>

<script type="text/javascript">
    google.load('visualization', '1', { packages: ['gauge'] });
    google.load("visualization", "1", { packages: ["corechart"] });
</script>

<script type='text/javascript'>
    var varCurrentMonth = '';
    var varCurrentYear = '';
    var varPeriodMonth = '';

    function getMonthName(month) {
        var monthName = '';
        switch (month) {
            case 0:
                monthName = 'January';
                break;
            case 1:
                monthName = 'February';
                break;
            case 2:
                monthName = 'March';
                break;
            case 3:
                monthName = 'April';
                break;
            case 4:
                monthName = 'May';
                break;
            case 5:
                monthName = 'June';
                break;
            case 6:
                monthName = 'July';
                break;
            case 7:
                monthName = 'August';
                break;
            case 8:
                monthName = 'September';
                break;
            case 9:
                monthName = 'October';
                break;
            case 10:
                monthName = 'November';
                break;
            case 11:
                monthName = 'December';
                break;
        }
        return monthName;
    }

    function SetVendorInfo(dataValues) {
        var obj = JSON.parse(dataValues);
        var vendorCode = '';
        var vendorName = '';
        var receiptValueMonth = 0;
        var receiptValueYear = 0;
        var startMonth = new Date();
        var countryName = '';

        $.each(obj, function (index, element) {
            vendorCode = this.VendorCode;
            vendorName = this.VendorName;
            receiptValueMonth = this.ReceiptValueMonth;
            receiptValueYear = this.ReceiptValueYear;
            startMonth = this.StartMonth;
            countryName = this.CountryName;
        });

        var startDate = new Date(startMonth);
        $('#<%= lblVendorNoValue.ClientID %>').html(vendorCode);
        $('#<%= lblVendorNameValue.ClientID %>').html(vendorName);
        $('#<%= lblPurchaseOrderSpendLastMonthValue.ClientID %>').html(receiptValueMonth);
        $('#<%= lblPurchaseOrderSpendYTDValue.ClientID %>').html(receiptValueYear);
        $('#<%= lblPeriodValue.ClientID %>').html(varPeriodMonth + ' ' + varCurrentYear);
        $('#<%= lblCountryValue.ClientID %>').html(countryName);
    }

    function OtifScore(score) {
    
        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Label');
        data.addColumn('number', 'Value');
        data.addRows([
            ['Score', score]
        ]);


        var options = {
            min: 0, max: 10,
            width: 200, height: 200,
            redFrom: 0, redTo: 5,
            majorTicks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            yellowFrom: 5, yellowTo: 7,
            greenFrom: 7, greenTo: 10,
            minorTicks: 5
        };

        var formatter = new google.visualization.NumberFormat({ pattern: '.0' });

        var chart = new google.visualization.Gauge(document.getElementById('Subreport_guage'));
        chart.draw(data, options);
    }

    function DiscrepancyScore(score) {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Label');
        data.addColumn('number', 'Value');
        data.addRows([
            ['Score', score]
        ]);
        var options = {
            min: 0, max: 10,
            width: 200, height: 200,
            redFrom: 0, redTo: 5,
            majorTicks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            yellowFrom: 5, yellowTo: 7,
            greenFrom: 7, greenTo: 10,
            minorTicks: 5
        };

        var formatter = new google.visualization.NumberFormat({ pattern: '.0' });

        var chart = new google.visualization.Gauge(document.getElementById('Subreport_guage1'));
        chart.draw(data, options);
    }

    function SchedulingScore(score) {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Label');
        data.addColumn('number', 'Value');
        data.addRows([
            ['Score', score]
        ]);


        var options = {
            min: 0, max: 10,
            width: 200, height: 200,
            redFrom: 0, redTo: 5,
            majorTicks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            yellowFrom: 5, yellowTo: 7,
            greenFrom: 7, greenTo: 10,
            minorTicks: 5
        };

        var formatter = new google.visualization.NumberFormat({ pattern: '.0' });

        var chart = new google.visualization.Gauge(document.getElementById('Subreport_guage2'));
        chart.draw(data, options);
    }

    function StockScore(score) { 

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Label');
        data.addColumn('number', 'Value');
        data.addRows([
            ['Score', score]
        ]);


        var options = {
            min: 0, max: 10,
            width: 200, height: 200,
            redFrom: 0, redTo: 5,
            majorTicks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            yellowFrom: 5, yellowTo: 7,
            greenFrom: 7, greenTo: 10,
            minorTicks: 5
        };

        var formatter = new google.visualization.NumberFormat({ pattern: '.0' });

        var chart = new google.visualization.Gauge(document.getElementById('Subreport_guage3'));
        chart.draw(data, options);
    }

    function DrawAllScores(dataValues) {
        ;
        var obj = JSON.parse(dataValues);
        var hpScheduling = 0;
        var hpDiscrepancies = 0;
        var hpOtif = 0;
        var hpStock = 0;
        var hpTotalScheduling = 0;
        var hpTotalDiscrepancies = 0;
        var hpTotalOtif = 0;
        var hpTotalStock = 0;

        var value = 0;
        $.each(obj, function (index, element) {
            //            ;
            switch (this.HistoricPerformance) {
                case 'Scheduling':
                    hpTotalScheduling = (this.ScoreCard1stScore * this.ScoreCardWeight) / 100;
                    hpScheduling = this.ScoreCard1stScore;
                    break;
                case 'Discrepancies':
                    hpTotalDiscrepancies = (this.ScoreCard1stScore * this.ScoreCardWeight) / 100;
                    hpDiscrepancies = this.ScoreCard1stScore;
                    break;
                case 'On Time In Full':
                    hpTotalOtif = (this.ScoreCard1stScore * this.ScoreCardWeight) / 100;
                    hpOtif = this.ScoreCard1stScore;
                    break;
                case 'Stock':
                    hpTotalStock = (this.ScoreCard1stScore * this.ScoreCardWeight) / 100;
                    hpStock = this.ScoreCard1stScore;
                    break;
            }
        });

        OtifScore(hpOtif);
        DiscrepancyScore(hpDiscrepancies);
        SchedulingScore(hpScheduling);
        StockScore(hpStock);
    }

    function GetActualMonthYear(latestYear, latestMonthNo, actualMonthNo) {
        if (actualMonthNo < latestMonthNo) {
            latestYear = latestYear.toString().substr(2, 2);
            return latestYear;
        }
        else {
            latestYear = (latestYear - 1).toString().substr(2, 2);
            return latestYear;
        }
    }

    function floorFigure(val) {
        var dotIndex = new String(val).indexOf('.');
        if (dotIndex != -1) {
            var strValue = new String(val);
            var decimalLength = strValue.substr(dotIndex).length;
            if (decimalLength > 1) {
                return parseFloat(strValue.substr(0, dotIndex + 3));
            }
            else {
                return parseFloat(strValue.substr(0, dotIndex + 2));
            }
        }
        else {
            return parseFloat(val);
        }
    }

    function OverallPerformanceGraphScore(dataValues) {
        ;
        var datas = JSON.parse(dataValues);
        var latestYear = new Date().getFullYear();
        var latestMonthNo = new Date().getMonth();
        var latestMonthName = getMonthName(latestMonthNo);
        varPeriodMonth = getMonthName(latestMonthNo - 1);
        varCurrentMonth = latestMonthName;
        varCurrentYear = latestYear;
        var hpVal1 = hpVal2 = hpVal3 = hpVal4 = hpVal5 = hpVal6 = hpVal7 = hpVal8 = hpVal9 = hpVal10 = hpVal11 = hpVal12 = 0;
        var Month1 = Month2 = Month3 = Month4 = Month5 = Month6 = Month7 = Month8 = Month9 = Month10 = Month11 = Month12 = 0;

        var varMonthNo = latestMonthNo - 1;

        if (latestMonthNo == 0) {
            latestYear = latestYear - 1;
            latestMonthNo = 12;
            varMonthNo = 11;
        }

        for (var index = 0; index < 12; index++) {
            switch (index) {
                case 0:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 1:
                    Month2 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 2:
                    Month3 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 3:
                    Month4 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 4:
                    Month5 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 5:
                    Month6 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 6:
                    Month7 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 7:
                    Month8 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 8:
                    Month9 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 9:
                    Month10 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 10:
                    Month11 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 11:
                    Month12 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
                case 12:
                    Month1 = getMonthName(varMonthNo).toString().substr(0, 3) + '-' + GetActualMonthYear(latestYear, latestMonthNo, varMonthNo);
                    break;
            }

            if (varMonthNo == 0) {
                varMonthNo = 11;
            }
            else {
                varMonthNo = varMonthNo - 1;
            }
        }

        $.each(datas, function (index, element) {
            //  ;
            switch (this.Month) {
                case Month1:
                    hpVal1 = hpVal1 + this.Score;
                    break;
                case Month2:
                    hpVal2 = hpVal2 + this.Score;
                    break;
                case Month3:
                    hpVal3 = hpVal3 + this.Score;
                    break;
                case Month4:
                    hpVal4 = hpVal4 + this.Score;
                    break;
                case Month5:
                    hpVal5 = hpVal5 + this.Score;
                    break;
                case Month6:
                    hpVal6 = hpVal6 + this.Score;
                    break;
                case Month7:
                    hpVal7 = hpVal7 + this.Score;
                    break;
                case Month8:
                    hpVal8 = hpVal8 + this.Score;
                    break;
                case Month9:
                    hpVal9 = hpVal9 + this.Score;
                    break;
                case Month10:
                    hpVal10 = hpVal10 + this.Score;
                    break;
                case Month11:
                    hpVal11 = hpVal11 + this.Score;
                    break;
                case Month12:
                    hpVal12 = hpVal12 + this.Score;
                    break;
            }
        });

        var dataScore = new google.visualization.DataTable();

        dataScore.addColumn('string', 'Label');
        dataScore.addColumn('number', 'Value');
        dataScore.addRows([
            ['Score', floorFigure(hpVal1)]
        ]);

        //           ['Score', parseFloat(hpVal1.toFixed(2))]]);

        var optionsScore = {
            min: 0, max: 10,
            width: 300, height: 300,
            redFrom: 0, redTo: 5,
            majorTicks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            yellowFrom: 5, yellowTo: 7,
            greenFrom: 7, greenTo: 10,
            minorTicks: 5
        };

        var formatter = new google.visualization.NumberFormat({ pattern: '.0' });
        var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
        chart.draw(dataScore, optionsScore);

        var data =new google.visualization.DataTable();

        data.addColumn('string', 'Month');
        data.addColumn('number', 'Overall Performance');

        data.addRows([
            [Month1, floorFigure(hpVal1)],
            [Month2, floorFigure(hpVal2)],
            [Month3, floorFigure(hpVal3)],
            [Month4, floorFigure(hpVal4)],
            [Month5, floorFigure(hpVal5)],
            [Month6, floorFigure(hpVal6)],
            [Month7, floorFigure(hpVal7)],
            [Month8, floorFigure(hpVal8)],
            [Month9, floorFigure(hpVal9)],
            [Month10, floorFigure(hpVal10)],
            [Month11, floorFigure(hpVal11)],
            [Month12, floorFigure(hpVal12)]
        ]);


        var options = {
            colors: ['#219bd4'],
            width: 590, height: 300,
            vAxis: { ticks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] },
            chartArea: { height: 240, width: 520 },
            legend: { position: 'none', alignment: 'end', textStyle: { bold: 'true' } },
            backgroundColor: { fill: 'transparent' },
            hAxis: { titleTextStyle: { color: 'red' }, textStyle: { fontSize: 9, bold: true } }
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('barchart_div'));
        chart.draw(data, options);
    }

    $(function () {
        var vendorId = '<%= vendorId %>';
        var otifScoreCardURL = '<%= otifScoreCardURL %>';
        var discScoreCardURL = '<%= discScoreCardURL %>';
        var schScoreCardURL = '<%= schScoreCardURL %>';
        var stockScoreCardURL = '<%= stockScoreCardURL %>';

        $("#ancOTIFScoreCard").attr('href', otifScoreCardURL);
        $("#ancOTIFScoreCardMap").attr('href', otifScoreCardURL);

        $("#ancDiscrepancyScoreCard").attr('href', discScoreCardURL);
        $("#ancDiscrepancyScoreCardMap").attr('href', discScoreCardURL);

        $("#ancSchedulingScoreCard").attr('href', schScoreCardURL);
        $("#ancSchedulingScoreCardMap").attr('href', schScoreCardURL);

        $("#ancStockScoreCard").attr('href', stockScoreCardURL);
        $("#ancStockScoreCardMap").attr('href', stockScoreCardURL);

        $.ajax({
            type: "POST",
            url: "AjaxCallPage.aspx/GetVendorScoreCardMainReportData",
            data: '{vendorId:' + vendorId + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) { 
                OverallPerformanceGraphScore(response.d.CurrentPerformance);
                DrawAllScores(response.d.HistroicPerformance);
                SetVendorInfo(response.d.VendorInfo);
            }
        });
    });

</script>

</html>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    AutoEventWireup="true" CodeFile="VendorScoreCardMainNew2.aspx.cs" Inherits="VendorScoreCardMain" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../../../Css/chartstyle.css" rel="stylesheet" type="text/css" />
    <script src="../../../Scripts/jquery.tooltip.min.js" type="text/javascript"></script>
     <script src="../../../Scripts/loader.js"></script>
    <style type="text/css">
        .printDetail-icon
        {
            background: url(../../../images/Detail.png) no-repeat 0 0;
            display: inline-block;
            height: 24px;
            width: 24px;
            border: none;
            float: right;
            margin-right: 10px;
        }
    </style>
    <div style="width: 991px; margin: 0 auto;">
        <a id="ancCalculatedSheet" class="info-icon" title="Information"></a>
        <asp:Button ID="btnExportPages" runat="server" ToolTip="Print" class="print-icon"
            OnClick="btnExportPages_Click" Visible="false" />
        <input type="button" title="Print" id="btnPrint" class="print-icon" />
        <input type="button" title="Detail" id="btnDetail" class="printDetail-icon ToolTipControl" />
        <iframe id="ifScoreCard" height="900" width="990" frameborder="0" runat="server">
        </iframe>
        <table style="width: 100%;">
            <tr>
                <td align="center">
                    <cc1:ucButton ID="btnBackSearch" runat="server" Text="Back To Search Page" CssClass="button"
                        OnClick="btnBack_Click" />&nbsp;&nbsp;
                </td>
            </tr>
        </table>
    </div>
    <script type='text/javascript'>
        $(function () {
            var calculatedURL = '<%= calculatedURL %>';
            $("#ancCalculatedSheet").attr('href', calculatedURL);
        });

        $("#btnPrint").live("click", function () {
            window.open('<%= ScoreCardPringURL %>', 'ScoreCard', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=1100,height=900');
        });

    </script>
    <style type="text/css">
        #tooltip
        {
            position: absolute;
            z-index: 3000;
            border: 0px solid #111;
            background-color: #FFFFFF;
            padding: 5px;
            opacity: 1.00;
            top:140px!important;
            left:230px!important;      
        }
        #tooltip h3, #tooltip div
        {
            margin: 0;
        }
        .PopupClass
        {
            width: 60%;           
        }
    </style>
    <%--<script src="../../../Scripts/jquery.tooltip.min.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        $(function () {
            InitializeToolTip();
            function InitializeToolTip() {
                $(".ToolTipControl").tooltip({
                    track: true,
                    delay: 0,
                    showURL: false,
                    fade: 100,
                    extraClass: "PopupClass",
                    bodyHandler: function () {
                        return $("#tooltip1").html();
                    },
                    showURL: false
                });
            }
        });
    </script>
    <div id="tooltip1" style="display: none;">
        <table border="1" cellpadding="5" cellspacing="5" width="100%">
            <tr>
                <td colspan="2">
                    <table border="0" cellpadding="5" cellspacing="5" width="100%">
                        <tr>
                            <td colspan="3" align="center" style="height: 50px; color: #203764; font-size: 20px;
                                background-color: #DEEBF7">
                                <b>
                                    <cc1:ucLabel ID="lblVendorDetails" runat="server"></cc1:ucLabel></b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table border="0" cellpadding="5" cellspacing="5" width="100%">
                        <tr>
                            <td style="width: 111px">
                                <b>
                                    <cc1:ucLabel ID="lblVendorNo" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblVendorNoValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 111px">
                                <b>
                                    <cc1:ucLabel ID="lblVendorName" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblVendorNameValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 111px">
                                <b>
                                    <cc1:ucLabel ID="lblCountry" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblCountryValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 111px">
                                <b>
                                    <cc1:ucLabel ID="lblConnectedTo" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblConnectedToValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table border="0" cellpadding="5" cellspacing="5" width="100%">
                        <tr>
                            <td style="width: 77px">
                                <b>
                                    <cc1:ucLabel ID="lblAddress" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblAddressValue1" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 77px">
                            </td>
                            <td>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblAddressValue2" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 77px">
                            </td>
                            <td>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblAddressValue3" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 77px">
                            </td>
                            <td>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblAddressValue4" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 77px">
                            </td>
                            <td>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblAddressValue5" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 77px">
                            </td>
                            <td>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblAddressValue6" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="5" cellspacing="5" width="100%">
                        <tr>
                            <td colspan="3" style="color: #203764; font-size: 14px">
                                <b>
                                    <cc1:ucLabel ID="lblInventoryContact" runat="server"></cc1:ucLabel></b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 113px">
                                <b>
                                    <cc1:ucLabel ID="lblStockPlanner" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblStockPlannerValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 113px">
                                <b>
                                    <cc1:ucLabel ID="lblTelno" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblTelSPValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table border="0" cellpadding="5" cellspacing="5" width="100%">
                        <tr>
                            <td colspan="3" style="color: #203764; font-size: 14px">
                                <b>
                                    <cc1:ucLabel ID="lblAPConatct" runat="server"></cc1:ucLabel>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 89px">
                                <b>
                                    <cc1:ucLabel ID="lblAPClerk" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblAPClerkValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 89px">
                                <b>
                                    <cc1:ucLabel ID="lblTelno_1" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblTelAPValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table border="0" cellpadding="5" cellspacing="5" width="100%">
                        <tr>
                            <td colspan="6" style="color: #203764; font-size: 14px">
                                <b>
                                    <cc1:ucLabel ID="lblVendorPointsofContact" runat="server"></cc1:ucLabel>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px">
                                <b>
                                    <cc1:ucLabel ID="lblScheduling" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblSchedulingValue" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="width: 133px">
                                <b>
                                    <cc1:ucLabel ID="lblDiscrepancies" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblDiscrepanciesValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px">
                                <b>
                                    <cc1:ucLabel ID="lblInvoiceIssues" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblInvoiceIssuesValue" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="width: 133px">
                                <b>
                                    <cc1:ucLabel ID="lblOtif" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblOtifValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px">
                                <b>
                                    <cc1:ucLabel ID="lblScorecard" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblScorecardValue" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="width: 133px">
                                <b>
                                    <cc1:ucLabel ID="lblInventory" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblInventoryValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px">
                                <b>
                                    <cc1:ucLabel ID="lblProcurement" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblProcurementValue" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="width: 133px">
                                <b>
                                    <cc1:ucLabel ID="lblAccountsPayable" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblAccountsPayableValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px">
                                <b>
                                    <cc1:ucLabel ID="lblExecutive" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblExecutiveValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table border="0" cellpadding="5" cellspacing="5" width="100%">
                        <tr>
                            <td colspan="6" style="color: #203764; font-size: 14px">
                                <b>
                                    <cc1:ucLabel ID="lblAgreements" runat="server"></cc1:ucLabel>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 81px">
                                <b>
                                    <cc1:ucLabel ID="lblMPANew" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td style="width:60px;">
                                <cc1:ucLabel ID="lblMPAValue" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="width: 93px">
                                <b>
                                    <cc1:ucLabel ID="lblDocument" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblDocumentValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 81px">
                                <b>
                                    <cc1:ucLabel ID="lblOverstock" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblOverstockValue" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="width: 93px">
                                <b>
                                    <cc1:ucLabel ID="lblDetails" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblDetailsOverValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 81px">
                                <b>
                                    <cc1:ucLabel ID="lblReturns" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblReturnsValue" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="width: 93px">
                                <b>
                                    <cc1:ucLabel ID="lblDetails_1" runat="server"></cc1:ucLabel></b>
                            </td>
                            <td>
                                <b>:</b>
                            </td>
                            <td>
                                <cc1:ucLabel ID="lblDetailsReturnValue" runat="server"></cc1:ucLabel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

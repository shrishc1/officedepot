﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using BaseControlLibrary;

using Utilities; using WebUtilities;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;

using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using System.Linq;


public partial class VendorCountryFreqEdit : CommonPage
{




    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCountry();
            BindVendor();
        }
    }

    #region Method

    protected void BindCountry()
    {
        MAS_CountryBE oMAS_CountryBE = new MAS_CountryBE();
        MAS_CountryBAL oMAS_CountryBAL = new MAS_CountryBAL();

        oMAS_CountryBE.Action = "ShowAll";
        oMAS_CountryBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
        oMAS_CountryBE.User.UserID = Convert.ToInt32(Session["UserID"]);

        List<MAS_CountryBE> lstCountry = new List<MAS_CountryBE>();
        lstCountry = oMAS_CountryBAL.GetCountryBAL(oMAS_CountryBE);
        oMAS_CountryBAL = null;
        if (lstCountry.Count > 0)
        {
            FillControls.FillDropDown(ref ddlCountry, lstCountry.ToList(), "CountryName", "CountryID");
        }
        if (Session["SiteCountryID"] != null)
        {
            ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(Session["SiteCountryID"].ToString()));
        }
    }

    protected void BindVendor()
    {
        MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
        MAS_VendorScoreCardBAL objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();

        objVendorScoreCardBE.Action = "GetAllVendorByCountry";
        objVendorScoreCardBE.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);

        List<MAS_VendorScoreCardBE> lstUPVendor = new List<MAS_VendorScoreCardBE>();
        lstUPVendor = objVendorScoreCardBAL.GetVendorByCountryBAL(objVendorScoreCardBE);
        objVendorScoreCardBAL = null;
        if (lstUPVendor.Count > 0)
        {
            FillControls.FillDropDown(ref ddlVendor, lstUPVendor.ToList(), "VendorName", "VendorID");
        }
    }
    #endregion

    #region Events
    

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindVendor();
    }

    #endregion

    protected void btnScoreCard_Click(object sender, EventArgs e)
    {
        MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
        MAS_VendorScoreCardBAL objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();

        objVendorScoreCardBE.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
        objVendorScoreCardBE.VendorID = Convert.ToInt32(ddlVendor.SelectedValue); 
    }
}



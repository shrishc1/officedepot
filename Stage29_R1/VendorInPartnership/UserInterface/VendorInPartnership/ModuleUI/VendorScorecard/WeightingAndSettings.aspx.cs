﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using Utilities; using WebUtilities;


public partial class WeightingAndSettings : CommonPage
{
    #region Declarations ...
    protected string WeightSettingAlert = WebCommon.getGlobalResourceValue("WeightSettingAlert");
    protected string AllWeightSettingAlert = WebCommon.getGlobalResourceValue("AllWeightSettingAlert");
    protected string DiscrepancyRateWeightMessage = WebCommon.getGlobalResourceValue("DiscrepancyRateWeightMessage");
    protected string DiscrepancyRateIdealMessage = WebCommon.getGlobalResourceValue("DiscrepancyRateIdealMessage");
    protected string DiscrepancyRateTargetMessage = WebCommon.getGlobalResourceValue("DiscrepancyRateTargetMessage");
    protected string DiscrepancyRateFailureMessage = WebCommon.getGlobalResourceValue("DiscrepancyRateFailureMessage");
    protected string DiscrepancyAverageResponseTimeWeightMessage = WebCommon.getGlobalResourceValue("DiscrepancyAverageResponseTimeWeightMessage");
    protected string DiscrepancyAverageResponseTimeIdealMessage = WebCommon.getGlobalResourceValue("DiscrepancyAverageResponseTimeIdealMessage");
    protected string DiscrepancyAverageResponseTimeTargetMessage = WebCommon.getGlobalResourceValue("DiscrepancyAverageResponseTimeTargetMessage");
    protected string DiscrepancyAverageResponseTimeFailureMessage = WebCommon.getGlobalResourceValue("DiscrepancyAverageResponseTimeFailureMessage");
    protected string StockBackOrderRateWeightMessage = WebCommon.getGlobalResourceValue("StockBackOrderRateWeightMessage");
    protected string StockBackOrderRateIdealMessage = WebCommon.getGlobalResourceValue("StockBackOrderRateIdealMessage");
    protected string StockBackOrderRateTargetMessage = WebCommon.getGlobalResourceValue("StockBackOrderRateTargetMessage");
    protected string StockBackOrderRateFailureMessage = WebCommon.getGlobalResourceValue("StockBackOrderRateFailureMessage");
    protected string StockLeadTimeVarianceWeightMessage = WebCommon.getGlobalResourceValue("StockLeadTimeVarianceWeightMessage");
    protected string StockLeadTimeVarianceIdealMessage = WebCommon.getGlobalResourceValue("StockLeadTimeVarianceIdealMessage");
    protected string StockLeadTimeVarianceTargetMessage = WebCommon.getGlobalResourceValue("StockLeadTimeVarianceTargetMessage");
    protected string StockLeadTimeVarianceFailureMessage = WebCommon.getGlobalResourceValue("StockLeadTimeVarianceFailureMessage");
    #endregion

    #region Events ...
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.Pageload();
        }

        if (string.IsNullOrEmpty(txtOtWeight.Text))
            txtOtWeight.Text = "0";
        if (string.IsNullOrEmpty(txtSchedulingWeight.Text))
            txtSchedulingWeight.Text = "0";
        if (string.IsNullOrEmpty(txtDiscrepenciesWeight.Text))
            txtDiscrepenciesWeight.Text = "0";
        if (string.IsNullOrEmpty(txtStockWeight.Text))
            txtStockWeight.Text = "0";

        txtOTIFCheckWeight.Text = (Convert.ToDecimal(txtOtWeight.Text) + Convert.ToDecimal(txtSchedulingWeight.Text) + Convert.ToDecimal(txtDiscrepenciesWeight.Text) + Convert.ToDecimal(txtStockWeight.Text)).ToString();
        if (Convert.ToDecimal(txtOTIFCheckWeight.Text) != 100)
            hdnChecksOverAllWeight.Value = "1";

        if (string.IsNullOrEmpty(txtAvgWeight.Text))
            txtAvgWeight.Text = "0";
        if (string.IsNullOrEmpty(txtDiscWeight.Text))
            txtDiscWeight.Text = "0";

        txtDescWeight.Text = (Convert.ToDecimal(txtAvgWeight.Text) + Convert.ToDecimal(txtDiscWeight.Text)).ToString();
        if (Convert.ToDecimal(txtDescWeight.Text) != 100)
            hdnChecksDiscWeight.Value = "1";

        if (string.IsNullOrEmpty(txtAvgBackorderDaysWeight.Text))
            txtAvgBackorderDaysWeight.Text = "0";
        if (string.IsNullOrEmpty(txtLeadTimeVarianceWeight.Text))
            txtLeadTimeVarianceWeight.Text = "0";

        txtStockWeightCheckWeight.Text = (Convert.ToDecimal(txtAvgBackorderDaysWeight.Text) + Convert.ToDecimal(txtLeadTimeVarianceWeight.Text)).ToString();
        if (Convert.ToDecimal(txtStockWeightCheckWeight.Text) != 100)
            hdnChecksStockWeight.Value = "1";
    }

    protected void btnApply_Click(object sender, EventArgs e)
    {
        #region Setting Discrepancies & Stock text boxes [0.00] if they were empty.
        if (string.IsNullOrEmpty(txtDiscWeight.Text) || string.IsNullOrWhiteSpace(txtDiscWeight.Text))
            txtDiscWeight.Text = "0.00";

        if (string.IsNullOrEmpty(txtAvgWeight.Text) || string.IsNullOrWhiteSpace(txtAvgWeight.Text))
            txtAvgWeight.Text = "0.00";

        if (string.IsNullOrEmpty(txtDiscIdeal.Text) || string.IsNullOrWhiteSpace(txtDiscIdeal.Text))
            txtDiscIdeal.Text = "0.00";

        if (string.IsNullOrEmpty(txtAvgIdeal.Text) || string.IsNullOrWhiteSpace(txtAvgIdeal.Text))
            txtAvgIdeal.Text = "0.00";

        if (string.IsNullOrEmpty(txtDiscTarget.Text) || string.IsNullOrWhiteSpace(txtDiscTarget.Text))
            txtDiscTarget.Text = "0.00";

        if (string.IsNullOrEmpty(txtAvgTarget.Text) || string.IsNullOrWhiteSpace(txtAvgTarget.Text))
            txtAvgTarget.Text = "0.00";        

        if (string.IsNullOrEmpty(txtDiscFailure.Text) || string.IsNullOrWhiteSpace(txtDiscFailure.Text))
            txtDiscFailure.Text = "0.00";

        if (string.IsNullOrEmpty(txtAvgFailure.Text) || string.IsNullOrWhiteSpace(txtAvgFailure.Text))
            txtAvgFailure.Text = "0.00";

        // Setting Stock [0.00] if text box is empty.
        if (string.IsNullOrEmpty(txtAvgBackorderDaysWeight.Text) || string.IsNullOrWhiteSpace(txtAvgBackorderDaysWeight.Text))
            txtAvgBackorderDaysWeight.Text = "0.00";

        if (string.IsNullOrEmpty(txtLeadTimeVarianceWeight.Text) || string.IsNullOrWhiteSpace(txtLeadTimeVarianceWeight.Text))
            txtLeadTimeVarianceWeight.Text = "0.00";

        if (string.IsNullOrEmpty(txtAvgBackorderDaysIdeal.Text) || string.IsNullOrWhiteSpace(txtAvgBackorderDaysIdeal.Text))
            txtAvgBackorderDaysIdeal.Text = "0.00";

        if (string.IsNullOrEmpty(txtLeadTimeVarianceIdeal.Text) || string.IsNullOrWhiteSpace(txtLeadTimeVarianceIdeal.Text))
            txtLeadTimeVarianceIdeal.Text = "0.00";

        if (string.IsNullOrEmpty(txtAvgBackorderDaysTargetPositive.Text) || string.IsNullOrWhiteSpace(txtAvgBackorderDaysTargetPositive.Text))
            txtAvgBackorderDaysTargetPositive.Text = "0.00";

        if (string.IsNullOrEmpty(txtLeadTimeVarianceTargetPositive.Text) || string.IsNullOrWhiteSpace(txtLeadTimeVarianceTargetPositive.Text))
            txtLeadTimeVarianceTargetPositive.Text = "0.00";

        if (string.IsNullOrEmpty(txtAvgBackorderDaysFailurePositive.Text) || string.IsNullOrWhiteSpace(txtAvgBackorderDaysFailurePositive.Text))
            txtAvgBackorderDaysFailurePositive.Text = "0.00";

        if (string.IsNullOrEmpty(txtLeadTimeVarianceFailurePositive.Text) || string.IsNullOrWhiteSpace(txtLeadTimeVarianceFailurePositive.Text))
            txtLeadTimeVarianceFailurePositive.Text = "0.00";


        #endregion

        #region OTIF Weight and Targets Validation
        if (Convert.ToDecimal(txtOtifIdeal.Text) < Convert.ToDecimal(txtOtifTarget.Text) && Convert.ToDecimal(txtOtifTarget.Text) < Convert.ToDecimal(txtOtifFailure.Text)) {
        }
        else if (Convert.ToDecimal(txtOtifIdeal.Text) > Convert.ToDecimal(txtOtifTarget.Text) && Convert.ToDecimal(txtOtifTarget.Text) > Convert.ToDecimal(txtOtifFailure.Text)) {
        }
        else {
            string WeightingSettingMessage = WebCommon.getGlobalResourceValue("WeightingSettingNewMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + WeightingSettingMessage + "')", true);
            return;
        }
       
        #endregion

        #region Discrepancies Weight and Target Validation
        if (Convert.ToDecimal(txtDiscIdeal.Text) < Convert.ToDecimal(txtDiscTarget.Text) && Convert.ToDecimal(txtDiscTarget.Text) < Convert.ToDecimal(txtDiscFailure.Text))
         {
         }
        else if (Convert.ToDecimal(txtDiscIdeal.Text) > Convert.ToDecimal(txtDiscTarget.Text) && Convert.ToDecimal(txtDiscTarget.Text) > Convert.ToDecimal(txtDiscFailure.Text))
        {
        }
        else
        {
            string WeightingSettingMessage = WebCommon.getGlobalResourceValue("WeightingSettingNewMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + WeightingSettingMessage + "')", true);
            return;
        }

        if (Convert.ToDecimal(txtAvgIdeal.Text) < Convert.ToDecimal(txtAvgTarget.Text) && Convert.ToDecimal(txtAvgTarget.Text) < Convert.ToDecimal(txtAvgFailure.Text))
         {
         }
        else if (Convert.ToDecimal(txtAvgIdeal.Text) > Convert.ToDecimal(txtAvgTarget.Text) && Convert.ToDecimal(txtAvgTarget.Text) > Convert.ToDecimal(txtAvgFailure.Text))
        {
        }
        else
        {
            string WeightingSettingMessage = WebCommon.getGlobalResourceValue("WeightingSettingNewMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + WeightingSettingMessage + "')", true);
            return;
        }

         #endregion

        #region Stock Weight and Targets Validation
        if (Convert.ToDecimal(txtAvgBackorderDaysIdeal.Text) < Convert.ToDecimal(txtAvgBackorderDaysTargetPositive.Text) && Convert.ToDecimal(txtAvgBackorderDaysTargetPositive.Text) < Convert.ToDecimal(txtAvgBackorderDaysFailurePositive.Text))
         {
         }
        else if (Convert.ToDecimal(txtAvgBackorderDaysIdeal.Text) > Convert.ToDecimal(txtAvgBackorderDaysTargetPositive.Text) && Convert.ToDecimal(txtAvgBackorderDaysTargetPositive.Text) > Convert.ToDecimal(txtAvgBackorderDaysFailurePositive.Text))
         {
         }
         else
         {
             string WeightingSettingMessage = WebCommon.getGlobalResourceValue("WeightingSettingNewMessage");
             ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + WeightingSettingMessage + "')", true);
             return;
         }

        if (Convert.ToDecimal(txtLeadTimeVarianceIdeal.Text) < Convert.ToDecimal(txtLeadTimeVarianceTargetPositive.Text) && Convert.ToDecimal(txtLeadTimeVarianceTargetPositive.Text) < Convert.ToDecimal(txtLeadTimeVarianceFailurePositive.Text))
         {
         }
        else if (Convert.ToDecimal(txtLeadTimeVarianceIdeal.Text) > Convert.ToDecimal(txtLeadTimeVarianceTargetPositive.Text) && Convert.ToDecimal(txtLeadTimeVarianceTargetPositive.Text) > Convert.ToDecimal(txtLeadTimeVarianceFailurePositive.Text))
         {
         }
         //else if (Convert.ToInt16(txtLeadTimeVarianceFailureNegative.Text) < Convert.ToInt16(txtLeadTimeVarianceTargetNegative.Text) && Convert.ToInt16(txtLeadTimeVarianceTargetNegative.Text) < Convert.ToInt16(txtLeadTimeVarianceIdeal.Text) && Convert.ToInt16(txtLeadTimeVarianceIdeal.Text) < Convert.ToInt16(txtLeadTimeVarianceTargetPositive.Text) && Convert.ToInt16(txtLeadTimeVarianceTargetPositive.Text) < Convert.ToInt16(txtLeadTimeVarianceFailurePositive.Text))
        else if (Convert.ToDecimal(txtLeadTimeVarianceIdeal.Text) < Convert.ToDecimal(txtLeadTimeVarianceTargetPositive.Text) && Convert.ToDecimal(txtLeadTimeVarianceTargetPositive.Text) < Convert.ToDecimal(txtLeadTimeVarianceFailurePositive.Text))
        {
        }
        else
        {
            string WeightingSettingMessage = WebCommon.getGlobalResourceValue("WeightingSettingNewMessage");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + WeightingSettingMessage + "')", true);
            return;
        }
         #endregion

        #region Scheduling Weight and Targets Validation
        //
         if (Convert.ToDecimal(txtSchedulingIdeal.Text) < Convert.ToDecimal(txtSchedulingTarget.Text) && Convert.ToDecimal(txtSchedulingTarget.Text) < Convert.ToDecimal(txtSchedulingFailure.Text))
         {
         }
         else if (Convert.ToDecimal(txtSchedulingIdeal.Text) > Convert.ToDecimal(txtSchedulingTarget.Text) && Convert.ToDecimal(txtSchedulingTarget.Text) > Convert.ToDecimal(txtSchedulingFailure.Text))
         {
         }
         else
         {
             string WeightingSettingMessage = WebCommon.getGlobalResourceValue("WeightingSettingNewMessage");
             ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('" + WeightingSettingMessage + "')", true);
             return;
         }

       
        #endregion

        var objVendorScoreCardBE = new MAS_VendorScoreCardBE();
        var objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();
        objVendorScoreCardBE.Action = "UpdateVendorScoreCard";
        //Initialize Overall Weight Property
        objVendorScoreCardBE.OtifWeight = Convert.ToInt32(txtOtWeight.Text);
        objVendorScoreCardBE.SchedulingWeight = Convert.ToInt32(txtSchedulingWeight.Text);
        objVendorScoreCardBE.DiscrepenciesWeight = Convert.ToInt32(txtDiscrepenciesWeight.Text);
        objVendorScoreCardBE.StockWeight = Convert.ToInt32(txtStockWeight.Text);
        
        //Initialize Overall Score Property
        objVendorScoreCardBE.FailureScore = Convert.ToDecimal(txtFailureScore.Text);
        objVendorScoreCardBE.TargetScore = Convert.ToDecimal(txtTargetScore.Text);
        objVendorScoreCardBE.IdealScore = Convert.ToDecimal(txtIdealScore.Text);
        objVendorScoreCardBE.AbsoluteFail = Convert.ToDecimal(txtAbsoluteFail.Text);


        //Initialize OTIF Weight and Targets Property
        //Stage 7 V2 point 12
        objVendorScoreCardBE.OtifIdeal = Convert.ToDecimal(txtOtifIdeal.Text);
        objVendorScoreCardBE.OtifTarget = Convert.ToDecimal(txtOtifTarget.Text);
        objVendorScoreCardBE.OtifFailure = Convert.ToDecimal(txtOtifFailure.Text);
        objVendorScoreCardBE.OtifAbsoluteFail = Convert.ToDecimal(txtOTIFAbsFail.Text);
       

        //Initialize Discrepancies Weight and Targets Property
        objVendorScoreCardBE.DiscWeight = Convert.ToDecimal(txtDiscWeight.Text);
        objVendorScoreCardBE.AvgWeight = Convert.ToDecimal(txtAvgWeight.Text);
        objVendorScoreCardBE.DiscIdeal = Convert.ToDecimal(txtDiscIdeal.Text);
        objVendorScoreCardBE.AvgIdeal = Convert.ToDecimal(txtAvgIdeal.Text);
        objVendorScoreCardBE.DiscTarget = Convert.ToDecimal(txtDiscTarget.Text);
        objVendorScoreCardBE.AvgTarget = Convert.ToDecimal(txtAvgTarget.Text);
        objVendorScoreCardBE.DiscFailure = Convert.ToDecimal(txtDiscFailure.Text);
        objVendorScoreCardBE.AvgFailure = Convert.ToDecimal(txtAvgFailure.Text);
        objVendorScoreCardBE.DiscAbsoluteFail = Convert.ToDecimal(txtDisAbsFail.Text);
        objVendorScoreCardBE.AvgAbsoluteFail = Convert.ToDecimal(txtAvgAbsFail.Text);



        //Initialize Stock Weight and Targets Property
        objVendorScoreCardBE.AvgBackorderDaysWeight = Convert.ToDecimal(txtAvgBackorderDaysWeight.Text);
        objVendorScoreCardBE.LeadTimeVarianceWeight = Convert.ToDecimal(txtLeadTimeVarianceWeight.Text);
        objVendorScoreCardBE.AvgBackorderDaysIdeal = Convert.ToDecimal(txtAvgBackorderDaysIdeal.Text);
        objVendorScoreCardBE.LeadTimeVarianceIdeal = Convert.ToDecimal(txtLeadTimeVarianceIdeal.Text);
        objVendorScoreCardBE.AvgBackorderDaysTargetPositive = Convert.ToDecimal(txtAvgBackorderDaysTargetPositive.Text);
        objVendorScoreCardBE.LeadTimeVarianceTargetPositive = Convert.ToDecimal(txtLeadTimeVarianceTargetPositive.Text);
        //objVendorScoreCardBE.AvgBackorderDaysTargetNegative = Convert.ToDecimal(txtAvgBackorderDaysTargetNegative.Text);
        //objVendorScoreCardBE.LeadTimeVarianceTargetNegative = Convert.ToDecimal(txtLeadTimeVarianceTargetNegative.Text);
        objVendorScoreCardBE.AvgBackorderDaysFailurePositive = Convert.ToDecimal(txtAvgBackorderDaysFailurePositive.Text);
        objVendorScoreCardBE.LeadTimeVarianceFailurePositive = Convert.ToDecimal(txtLeadTimeVarianceFailurePositive.Text);
        //objVendorScoreCardBE.AvgBackorderDaysFailureNegative = Convert.ToDecimal(txtAvgBackorderDaysFailureNegative.Text);
        //objVendorScoreCardBE.LeadTimeVarianceFailureNegative = Convert.ToDecimal(txtLeadTimeVarianceFailureNegative.Text);
        objVendorScoreCardBE.BackorderDaysAbsoluteFail = Convert.ToDecimal(txtStockBackOrderRateAbsFail.Text);
        objVendorScoreCardBE.LeadTimeVarianceAbsoluteFail = Convert.ToDecimal(txtStockLeadTimeVarianceAbsFail.Text);
        

        //Initialize Scheduling Weight and Targets Property
        objVendorScoreCardBE.SchedulingIdeal = Convert.ToDecimal(txtSchedulingIdeal.Text);
        objVendorScoreCardBE.SchedulingTarget = Convert.ToDecimal(txtSchedulingTarget.Text);
        objVendorScoreCardBE.SchedulingFailure = Convert.ToDecimal(txtSchedulingFailure.Text);
        objVendorScoreCardBE.SchedulingAbsoluteFail = Convert.ToDecimal(txtSchedulingAbsFail.Text);
       
        int? checkUpdate=objVendorScoreCardBAL.UpdateVendorScoreCardBAL(objVendorScoreCardBE);
        objVendorScoreCardBAL = null;
        if (checkUpdate == 1)
        {
            this.Pageload();
            string VendorUpdatedMessage = WebCommon.getGlobalResourceValue("VendorUpdatedMessage");
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script> alert('" + VendorUpdatedMessage + "');</script>");
        }
    }


    private void Pageload()
    {
        CustomValidator1.ErrorMessage = AllWeightSettingAlert;
        var objVendorScoreCardBE = new MAS_VendorScoreCardBE();
        var objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();
        objVendorScoreCardBE = objVendorScoreCardBAL.GetVendorScoreCardBAL();
        objVendorScoreCardBAL = null;

        //Initialize Overall Weight Property
        txtOtWeight.Text = objVendorScoreCardBE.OtifWeight.ToString();
        txtSchedulingWeight.Text = objVendorScoreCardBE.SchedulingWeight.ToString();
        txtDiscrepenciesWeight.Text = objVendorScoreCardBE.DiscrepenciesWeight.ToString();
        txtStockWeight.Text = objVendorScoreCardBE.StockWeight.ToString();

        //Initialize Overall Score Property
        txtFailureScore.Text = objVendorScoreCardBE.FailureScore.ToString();
        txtTargetScore.Text = objVendorScoreCardBE.TargetScore.ToString();
        txtIdealScore.Text = objVendorScoreCardBE.IdealScore.ToString();
        txtAbsoluteFail.Text = objVendorScoreCardBE.AbsoluteFail.ToString();

        //Initialize OTIF Weight and Targets Property
        //Stage 7 V2 Point 12
        txtOtifIdeal.Text = objVendorScoreCardBE.OtifIdeal.ToString();
        txtOtifTarget.Text = objVendorScoreCardBE.OtifTarget.ToString();
        txtOtifFailure.Text = objVendorScoreCardBE.OtifFailure.ToString();
        txtOTIFAbsFail.Text = objVendorScoreCardBE.OtifAbsoluteFail.ToString();

        //Initialize Discrepancies Weight and Targets Property
        txtDiscWeight.Text = objVendorScoreCardBE.DiscWeight.ToString();
        txtAvgWeight.Text = objVendorScoreCardBE.AvgWeight.ToString();
        txtDiscIdeal.Text = objVendorScoreCardBE.DiscIdeal.ToString();
        txtAvgIdeal.Text = objVendorScoreCardBE.AvgIdeal.ToString();
        txtDiscTarget.Text = objVendorScoreCardBE.DiscTarget.ToString();
        txtAvgTarget.Text = objVendorScoreCardBE.AvgTarget.ToString();
        txtDiscFailure.Text = objVendorScoreCardBE.DiscFailure.ToString();
        txtAvgFailure.Text = objVendorScoreCardBE.AvgFailure.ToString();

        txtDisAbsFail.Text = objVendorScoreCardBE.DiscAbsoluteFail.ToString();
        txtAvgAbsFail.Text = objVendorScoreCardBE.AvgAbsoluteFail.ToString();

        //Initialize Stock Weight and Targets Property
        txtAvgBackorderDaysWeight.Text = objVendorScoreCardBE.AvgBackorderDaysWeight.ToString();
        txtLeadTimeVarianceWeight.Text = objVendorScoreCardBE.LeadTimeVarianceWeight.ToString();
        txtAvgBackorderDaysIdeal.Text = objVendorScoreCardBE.AvgBackorderDaysIdeal.ToString();
        txtLeadTimeVarianceIdeal.Text = objVendorScoreCardBE.LeadTimeVarianceIdeal.ToString();
        txtAvgBackorderDaysTargetPositive.Text = objVendorScoreCardBE.AvgBackorderDaysTargetPositive.ToString();
        txtLeadTimeVarianceTargetPositive.Text = objVendorScoreCardBE.LeadTimeVarianceTargetPositive.ToString();
        //txtAvgBackorderDaysTargetNegative.Text = objVendorScoreCardBE.AvgBackorderDaysTargetNegative.ToString();
        //txtLeadTimeVarianceTargetNegative.Text = objVendorScoreCardBE.LeadTimeVarianceTargetNegative.ToString();
        txtAvgBackorderDaysFailurePositive.Text = objVendorScoreCardBE.AvgBackorderDaysFailurePositive.ToString();
        txtLeadTimeVarianceFailurePositive.Text = objVendorScoreCardBE.LeadTimeVarianceFailurePositive.ToString();
        //txtAvgBackorderDaysFailureNegative.Text = objVendorScoreCardBE.AvgBackorderDaysFailureNegative.ToString();
        //txtLeadTimeVarianceFailureNegative.Text = objVendorScoreCardBE.LeadTimeVarianceFailureNegative.ToString();
        txtStockBackOrderRateAbsFail.Text = objVendorScoreCardBE.BackorderDaysAbsoluteFail.ToString();
        txtStockLeadTimeVarianceAbsFail.Text = objVendorScoreCardBE.LeadTimeVarianceAbsoluteFail.ToString();

        //Initialize Scheduling Weight and Targets Property
        txtSchedulingIdeal.Text = objVendorScoreCardBE.SchedulingIdeal.ToString();
        txtSchedulingTarget.Text = objVendorScoreCardBE.SchedulingTarget.ToString();
        txtSchedulingFailure.Text = objVendorScoreCardBE.SchedulingFailure.ToString();
        txtSchedulingAbsFail.Text = objVendorScoreCardBE.SchedulingAbsoluteFail.ToString();

        if (string.IsNullOrEmpty(txtOtWeight.Text))
            txtOtWeight.Text = "0";
        if (string.IsNullOrEmpty(txtSchedulingWeight.Text))
            txtSchedulingWeight.Text = "0";
        if (string.IsNullOrEmpty(txtDiscrepenciesWeight.Text))
            txtDiscrepenciesWeight.Text = "0";
        if (string.IsNullOrEmpty(txtStockWeight.Text))
            txtStockWeight.Text = "0";

        txtOTIFCheckWeight.Text = (Convert.ToDecimal(txtOtWeight.Text) + Convert.ToDecimal(txtSchedulingWeight.Text) + Convert.ToDecimal(txtDiscrepenciesWeight.Text) + Convert.ToDecimal(txtStockWeight.Text)).ToString();
        if (Convert.ToDecimal(txtOTIFCheckWeight.Text) != 100)
            hdnChecksOverAllWeight.Value = "1";

        if (string.IsNullOrEmpty(txtAvgWeight.Text))
            txtAvgWeight.Text = "0";
        if (string.IsNullOrEmpty(txtDiscWeight.Text))
            txtDiscWeight.Text = "0";

        txtDescWeight.Text = (Convert.ToDecimal(txtAvgWeight.Text) + Convert.ToDecimal(txtDiscWeight.Text)).ToString();
        if (Convert.ToDecimal(txtDescWeight.Text) != 100)
            hdnChecksDiscWeight.Value = "1";

        if (string.IsNullOrEmpty(txtAvgBackorderDaysWeight.Text))
            txtAvgBackorderDaysWeight.Text = "0";
        if (string.IsNullOrEmpty(txtLeadTimeVarianceWeight.Text))
            txtLeadTimeVarianceWeight.Text = "0";

        txtStockWeightCheckWeight.Text = (Convert.ToDecimal(txtAvgBackorderDaysWeight.Text) + Convert.ToDecimal(txtLeadTimeVarianceWeight.Text)).ToString();
        if (Convert.ToDecimal(txtStockWeightCheckWeight.Text) != 100)
            hdnChecksStockWeight.Value = "1";
    }
    #endregion
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master"
    CodeFile="VendorScoreCardSearch.aspx.cs" Inherits="VendorCountryFreqEdit" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h2>
                <cc1:ucLabel ID="lblVendorScoreCardSetup" runat="server" Text="Vendor Score Card"></cc1:ucLabel>
            </h2>
            <div class="right-shadow">
             
                <div class="formbox">
                    <table width="49%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="width: 5%">
                            </td>
                            <td style="font-weight: bold;" width="28%">
                                <cc1:ucLabel ID="lblCountry" runat="server" Text="Country" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;" width="5%" align="center">
                                :
                            </td>
                            <td style="font-weight: bold;" width="57%">
                                <cc1:ucDropdownList ID="ddlCountry" runat="server" Width="150px" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                </cc1:ucDropdownList>
                            </td>
                            <td style="width: 5%">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="font-weight: bold">
                                <cc1:ucLabel ID="lblVendor" runat="server" Text="Vendor" isRequired="true"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;" align="center">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <cc1:ucDropdownList ID="ddlVendor" runat="server" Width="225px">
                                </cc1:ucDropdownList>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
            <div class="button-row">
                <cc1:ucButton ID="btnVendorScoreCard" runat="server" Text="Generate Score Card" CssClass="button" 
                    ValidationGroup="a" onclick="btnScoreCard_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Data;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using Utilities; using WebUtilities;
using BaseControlLibrary;

public partial class VendorCountryFreqOverview : CommonPage
{

    protected void Page_Prerender(object sender, EventArgs e) {        
                 
    }

    protected void Page_Load(object sender, EventArgs e) {

        if (!IsPostBack) {
            BindRegionVendor();
        }
        ucExportToExcel1.GridViewControl = grdVendorCountryFreq;
        ucExportToExcel1.FileName = "VendorCountryFrequency";
    }
        
    #region Methods

    protected void BindRegionVendor() {
        MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
        MAS_VendorScoreCardBAL objVendorScoreCardBAL = new MAS_VendorScoreCardBAL();

        objVendorScoreCardBE.Action = "ShowAll";
        List<MAS_VendorScoreCardBE> lstVendorFreq = objVendorScoreCardBAL.GetALLVendorFreqBAL(objVendorScoreCardBE);
        objVendorScoreCardBAL = null;
        if (lstVendorFreq.Count > 0)
        {
            grdVendorCountryFreq.DataSource = lstVendorFreq;
            grdVendorCountryFreq.DataBind();
            ViewState["lstVenFreq"] = lstVendorFreq;
        }        
    }

    public override BusinessEntities.BaseBe[] SubSort(GridViewSortEventArgs e)
    {
        return Utilities.GenericListHelper<MAS_VendorScoreCardBE>.SortList((List<MAS_VendorScoreCardBE>)ViewState["lstVenFreq"], e.SortExpression, e.SortDirection).ToArray();
    }

    #endregion    
}
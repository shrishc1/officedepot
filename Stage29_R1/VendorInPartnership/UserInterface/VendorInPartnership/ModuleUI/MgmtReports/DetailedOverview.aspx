﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="DetailedOverview.aspx.cs"
    MasterPageFile="~/CommonUI/CMN_MasterPages/NoUser_MasterPage.Master" MaintainScrollPositionOnPostback="true"
    Inherits="DetailedOverview" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .fixedTr
        {
            position: absolute;
            left: 185px;
            border-left: 1px solid gray;
            vertical-align: bottom;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <cc1:ucLabel ID="lblVIPManagementReport" runat="server" ></cc1:ucLabel>
        -
        <asp:Literal ID="ltOverViewType" runat="server" Text=""></asp:Literal>
    </h2>
    <div id="divDetails" runat="server">
    <div class="right-shadow">
        <div class="formbox">
            <table width="90%" cellspacing="0" cellpadding="0" border="0" class="top-settings">
                <tr>
                    <td align="center" style="width: 60%">
                        <table width="100%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settingsnoborder">
                            <tr>
                                <td align="right" style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblStartPoint" Text="Start Point" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td style="font-weight: bold;">
                                    <cc2:ucDate ClientIDMode="Static" ID="txtDate" runat="server" />
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblReportType" Text="ReportType" runat="server" Visible="false"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    <asp:Literal ID="ltColon" runat="server" Text=":" Visible="false"></asp:Literal>
                                </td>
                                <td style="font-weight: bold;">
                                    <cc1:ucDropdownList ID="ddlReportType" runat="server" Width="180px" Visible="false">
                                    </cc1:ucDropdownList>
                                </td>
                                <td align="left">
                                    <cc1:ucButton ID="btnGo" runat="server" CssClass="button" OnClick="btnGo_Click" />
                                </td>
                            </tr>
                            <tr id="trCurrency" runat="server">
                                <td align="right" style="font-weight: bold;">
                                    <cc1:ucLabel ID="lblViewvalueas" Text="Currency values in" runat="server"></cc1:ucLabel>
                                </td>
                                <td style="font-weight: bold;">
                                    :
                                </td>
                                <td colspan="5" style="font-weight: bold;">
                                    <asp:Image ImageUrl="../../Images/euro.png" ID="imgEuro" runat="server" ToolTip="Euro" Height="16px" Width="16px"  />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right" valign="top" style="width: 40%">
                        <table width="90%" cellspacing="5" cellpadding="0" border="0" align="right" class="top-settingsnoborder">
                            <tr>
                                <td align="right">
                                    <cc1:ucButton ID="btnExportToExcel" runat="server" OnClick="btnExport_Click" CssClass="exporttoexcel"
                                        Width="109px" Height="20px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <%--<div style="width: 800px; overflow: hidden; font-size: 15px; font-weight: bold; margin-top: 10px;
                margin-bottom: 10px;" id="divRFC" runat="server" visible="false">
                <cc1:ucLabel ID="lblRFCRequest" runat="server" Text="There is a open EJM to incorpoate the daily sales values into the VIP interface files. Once live the sales / margin details will be reported here."
                    isRequired="true"></cc1:ucLabel>
            </div>--%>
            <div style="width: 950px; overflow-x: scroll;">
                <table border="0" align="center" cellspacing="0" cellpadding="0" class="form-table" id="tblMain"
                    runat="server">
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="0" class="grid gvclass searchgrid-1">
                                <tr>
                                    <th style="width: 50px; height: 36px; text-align: center; vertical-align: middle;">
                                        Date&nbsp;
                                    </th>
                                    <asp:Repeater ID="rptSite" runat="server" OnItemDataBound="rptSite_OnItemDataBound">
                                        <ItemTemplate>
                                            <th style="text-align: right;">
                                                <table border="0">
                                                    <tr>
                                                        <td align="center" runat="server" id="tdControlSite">
                                                            <asp:Literal ID="ltSitePreFix" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SitePrefix") %>'></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <asp:Repeater ID="rptKeyHeader" runat="server">
                                                            <ItemTemplate>
                                                                <td align="right" runat="server" id="tdControl">
                                                                    <asp:Literal ID="ltKeyHeaderText" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KeyHeader") %>'></asp:Literal>
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tr>
                                                </table>
                                            </th>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                                <asp:Repeater ID="rptkeyDetails" runat="server" OnItemDataBound="rptkeyDetails_OnItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td style="height: 20px;">
                                                <asp:HyperLink ID="hpLink" runat="server" Text='<%#  Eval("CalcultionDate", "{0:dd/MM/yyyy}") %>'
                                                    NavigateUrl='<%# Utilities.Common.EncryptQuery("Dashboard.aspx?ReportDate=" + Eval("CalcultionDate", "{0:dd/MM/yyyy}")) %>'></asp:HyperLink>
                                                <asp:Literal Visible="false" ID="hdnDate" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "CalcultionDate") %>'></asp:Literal>
                                            </td>
                                            <asp:Repeater ID="rptKeyvalue" runat="server" OnItemDataBound="rptKeyvalue_OnItemDataBound">
                                                <ItemTemplate>
                                                    <td style="color: #333333; background-color: #F7F6F3; text-align: right;">
                                                        <table border="0">
                                                            <tr>
                                                                <asp:Repeater ID="rptKeyValueDetails" runat="server" OnItemDataBound="rptKeyValueDetails_OnItemDataBound">
                                                                    <ItemTemplate>
                                                                        <td align="right" runat="server" id="tdControl" style="height: 20px;">
                                                                            <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </ItemTemplate>
                                                <AlternatingItemTemplate>
                                                    <td style="text-align: right;">
                                                        <table border="0">
                                                            <tr>
                                                                <asp:Repeater ID="rptKeyValueDetails" runat="server" OnItemDataBound="rptKeyValueDetails_OnItemDataBound">
                                                                    <ItemTemplate>
                                                                        <td align="right" runat="server" id="tdControl" style="height: 20px;">
                                                                            <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </AlternatingItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <table border="0" cellspacing="0" cellpadding="0" class="form-table">
                <tr id="tdExRate" runat="server" style="display: none;">
                    <td>
                        <cc1:ucLabel ID="ltExchaage" runat="server" Text="* exchange rate of 1.113" isRequired="true"></cc1:ucLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <cc1:PagerV2_8 ID="pager1" runat="server" OnCommand="pager_Command" GenerateGoToSection="false">
                        </cc1:PagerV2_8>
                    </td>
                </tr>
            </table>
            <table border="1" cellspacing="0" cellpadding="0" class="form-table" id="tblExport"
                runat="server" style="display: none;">
                <tr>
                    <td>
                        <table border="1" cellspacing="0" cellpadding="0" class="grid gvclass searchgrid-1">
                            <tr>
                                <th style="width: 50px; text-align: center; background-color: #f5f1e8; color: #934500!important;">
                                    Date
                                </th>
                                <asp:Repeater ID="rptSiteExport" runat="server" OnItemDataBound="rptSite_OnItemDataBound">
                                    <ItemTemplate>
                                        <th >
                                            <table border="1">
                                                <tr>
                                                    <td style="text-align: center; background-color: #f5f1e8; color: #934500!important; font-weight:bold; " runat="server" id="tdControlSite">
                                                        <asp:Literal ID="ltSitePreFix" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SitePrefix") %>'></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <asp:Repeater ID="rptKeyHeader" runat="server">
                                                        <ItemTemplate>
                                                            <td style="text-align: right; background-color: #f5f1e8; color: #934500!important; font-weight:bold;" runat="server" id="tdControl">
                                                                <asp:Literal ID="ltKeyHeaderText" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "KeyHeader") %>'></asp:Literal>
                                                            </td>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </tr>
                                            </table>
                                        </th>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                            <asp:Repeater ID="rptkeyDetailsExport" runat="server" OnItemDataBound="rptkeyDetails_OnItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td style="height: 20px;text-align: right;">
                                            <asp:Literal ID="hpLink" runat="server" Text='<%#  Eval("CalcultionDate", "{0:dd/MM/yyyy}") %>'></asp:Literal>
                                            <asp:Literal Visible="false" ID="hdnDate" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "CalcultionDate") %>'></asp:Literal>
                                        </td>
                                        <asp:Repeater ID="rptKeyvalue" runat="server" OnItemDataBound="rptKeyvalue_OnItemDataBound">
                                            <ItemTemplate>
                                                <td >
                                                    <table border="1">
                                                        <tr>
                                                            <asp:Repeater ID="rptKeyValueDetails" runat="server" OnItemDataBound="rptKeyValueDetails_OnItemDataBound">
                                                                <ItemTemplate>
                                                                    <td  runat="server" id="tdControl" style="margin-left:1px; color: #333333; background-color: #F7F6F3!important; text-align: right;">                                                                        
                                                                        <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                    </td>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <td style="text-align: right;">
                                                    <table border="1">
                                                        <tr>
                                                            <asp:Repeater ID="rptKeyValueDetails" runat="server" OnItemDataBound="rptKeyValueDetails_OnItemDataBound">
                                                                <ItemTemplate>
                                                                    <td align="right" runat="server" id="tdControl" style="margin-left:1px;">                                                                        
                                                                        <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                    </td>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </AlternatingItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
            </table>
            <div class="button-row">
                <cc1:ucButton ID="btnBack" runat="server" Text="Back" CssClass="button" OnClientClick="javascript:window.open('', '_self', '');window.close();" />
            </div>
        </div>
    </div>
    <div class="bottom-shadow">
    </div>
    </div>
</asp:Content>

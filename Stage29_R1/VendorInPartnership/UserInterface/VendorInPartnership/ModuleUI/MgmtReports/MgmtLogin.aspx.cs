﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.MgmtReports;
using BusinessLogicLayer.ModuleBAL.MgmtReports;
using WebUtilities;
using System.Configuration;

public partial class MgmtLogin : AA.switchprotocol.UI.SecurePage
{
    void Page_PreInit(Object sender, EventArgs e) {
        if (Session["UserID"] != null)
            this.MasterPageFile = "~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master";
        else
            this.MasterPageFile = "~/CommonUI/CMN_MasterPages/NoUser_MasterPage.Master";

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null) {
            btnChangePassword.Visible = false;
            btnCreateReportLink.Visible = false;
        }
        //if (Session["MgmtLogin"] != null && Convert.ToString(Session["MgmtLogin"]) == "True")
        //    Response.Redirect("Dashboard.aspx");
    }

    protected void btnCreateReportLink_Click(object sender, EventArgs e) {        
        VIPManagementReportBAL oVIPManagementReportBEL = new VIPManagementReportBAL();
        string password = oVIPManagementReportBEL.GetPasswordBAL();

        string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/MgmtReports/Dashboard.aspx?" + password;

        ltLnik.Text = sPortalLink;

        mdInformation.Show();
    }


    protected void btnOK_1_Click(object sender, EventArgs e) {
        mdInformation.Hide();
    }


    //protected void btnGo_Click(object sender, EventArgs e) {

    //    VIPManagementReportBE oVIPManagementReportBE = new VIPManagementReportBE();
    //    VIPManagementReportBAL oVIPManagementReportBEL = new VIPManagementReportBAL();

    //    oVIPManagementReportBE.Action = "CheckPassword";        
    //    oVIPManagementReportBE.Password = txtSecureCode.Text.Trim();
        
    //    int? iFlag = oVIPManagementReportBEL.ChangePasswordBAL(oVIPManagementReportBE);

        
    //    if (iFlag == 1) {
    //        Session["MgmtLogin"] = "True";
    //        Response.Redirect("Dashboard.aspx");
        
    //    }
    //    //else
    //    //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PasswordNotChanged + "')", true);   

    //    //if (txtSecureCode.Text == "speed@od!!!") {
    //    //    Session["MgmtLogin"] = "True";
    //    //    Response.Redirect("Dashboard.aspx");
    //    //}
    //}
    protected void btnChangePassword_Click(object sender, EventArgs e) {
        tblChangePass.Visible = true;

        //tblSecurityCode.Visible = false;
        btnChangePassword.Visible = false;
        btnCreateReportLink.Visible = false;
    }

    protected void btnOK_Click(object sender, EventArgs e) {
        VIPManagementReportBE oVIPManagementReportBE = new VIPManagementReportBE();
        VIPManagementReportBAL oVIPManagementReportBEL = new VIPManagementReportBAL();

        oVIPManagementReportBE.Action = "ChangePassword";
        oVIPManagementReportBE.OldPassword = txtCurrentPassword.Text.Trim();
        oVIPManagementReportBE.Password = txtConfirmPassword.Text.Trim();
        oVIPManagementReportBE.UserID = Convert.ToInt32(Session["UserID"]);
        int? iFlag = oVIPManagementReportBEL.ChangePasswordBAL(oVIPManagementReportBE);

        string PasswordChanged = WebCommon.getGlobalResourceValue("PasswordChanged");
        string PasswordNotChanged = WebCommon.getGlobalResourceValue("PasswordNotChanged");

        if (iFlag == 1) {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PasswordChanged + "')", true);
           //tblSecurityCode.Visible = true;
            btnChangePassword.Visible = true;
            btnCreateReportLink.Visible = true;
            tblChangePass.Visible = false;
        }
        else
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('" + PasswordNotChanged + "')", true);       
        
    }

    protected void btnCancel_Click(object sender, EventArgs e) {
        //tblSecurityCode.Visible = true;
        btnChangePassword.Visible = true;
        btnCreateReportLink.Visible = true;
        tblChangePass.Visible = false;
    }
}
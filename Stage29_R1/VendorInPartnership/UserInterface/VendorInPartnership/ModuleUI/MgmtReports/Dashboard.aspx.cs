﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.MgmtReports;
using BusinessLogicLayer.ModuleBAL.MgmtReports;
using BaseControlLibrary;
using System.IO;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.AdminFunctions;
using System.Data;
using System.Globalization;
using BusinessEntities.ModuleBE.OnTimeInFull;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull;

public partial class Dashboard : CommonPage
{
    public int DefaultCurrencyID {
        get {
            return ViewState["DefaultCurrencyID"] != null ? Convert.ToInt32(ViewState["DefaultCurrencyID"]) : 1;
        }
        set {
            ViewState["DefaultCurrencyID"] = value;
        }
    }

    public int CurrentCurrencyID {
        get {
            return Session["CurrentCurrencyID"] != null ? Convert.ToInt32(Session["CurrentCurrencyID"]) : 1;
        }
        set {
            Session["CurrentCurrencyID"] = value;
        }
    }

    public bool CurrentCurrencyExists {
        get {
            return ViewState["CurrentCurrencyExists"] != null ? Convert.ToBoolean(ViewState["CurrentCurrencyExists"]) : true;
        }
        set {
            ViewState["CurrentCurrencyExists"] = value;
        }
    }

    void Page_PreInit(Object sender, EventArgs e) {       

        if (Session["UserID"] != null)
            this.MasterPageFile = "~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master";
        else
            this.MasterPageFile = "~/CommonUI/CMN_MasterPages/NoUser_MasterPage.Master";

        //if (Session["MgmtLogin"] == null)
        //    Response.Redirect("MgmtLogin.aspx");
    }

    protected void Page_InIt(object sender, EventArgs e) {
        txtDate.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e) {
        if (!Page.IsPostBack) {

            if (Session["UserID"] == null) {
                if (!IsPasswordmatched()) {
                    UpdatePanel6.Visible = false;
                    return;
                }
            }

            Session["MgmtLogin"] = "True";
            if (GetQueryStringValue("ReportDate") != null) {
                txtDate.innerControltxtDate.Value = GetQueryStringValue("ReportDate");  
            }
            BindManagementReport();
            //BindManagementReport(true);
           
        }
    }

    private bool IsPasswordmatched() {

        bool _IsPasswordmatched = false;

        VIPManagementReportBAL oVIPManagementReportBEL = new VIPManagementReportBAL();
        string password = oVIPManagementReportBEL.GetPasswordBAL();

        string strReq = "";
        strReq = Request.Url.ToString();
        strReq = strReq.Substring(strReq.IndexOf('?') + 1);

        if (strReq == password)
            _IsPasswordmatched = true;

        return _IsPasswordmatched;
    }

    protected void btnGo_Click(object sender, EventArgs e) {       
        BindManagementReport();
    }

    private void BindManagementReport(bool isExport = false) {

        if (DefaultCurrencyID == 1) {
            imgEuro.ImageUrl = "../../Images/euro.png";
            imgEuro.Enabled = false;
            imgPound.ImageUrl = "../../Images/pound-dis.png";
            imgPound.Enabled = true;
            imgDollar.ImageUrl = "../../Images/Dollar-dis.png";
            imgDollar.Enabled = true;
            imgCzechkoruna.ImageUrl = "../../Images/Czechkoruna-dis.png";
            imgCzechkoruna.Enabled = true;
            CurrentCurrencyID = 1;
        }
        else if (DefaultCurrencyID == 2) {
            imgPound.ImageUrl = "../../Images/pound.png";
            imgPound.Enabled = false;
            imgEuro.ImageUrl = "../../Images/euro-dis.png";
            imgEuro.Enabled = true;
            imgDollar.ImageUrl = "../../Images/Dollar-dis.png";
            imgDollar.Enabled = true;
            imgCzechkoruna.ImageUrl = "../../Images/Czechkoruna-dis.png";
            imgCzechkoruna.Enabled = true;
            CurrentCurrencyID = 2;
        }
        else if (DefaultCurrencyID == 3) {
            imgDollar.ImageUrl = "../../Images/Dollar.png";
            imgDollar.Enabled = false;
            imgPound.ImageUrl = "../../Images/pound-dis.png";
            imgPound.Enabled = true;
            imgEuro.ImageUrl = "../../Images/euro-dis.png";
            imgEuro.Enabled = true;            
            imgCzechkoruna.ImageUrl = "../../Images/Czechkoruna-dis.png";
            imgCzechkoruna.Enabled = true;
            CurrentCurrencyID = 3;
            
        }
        else if (DefaultCurrencyID == 4)
        {
            imgCzechkoruna.ImageUrl = "../../Images/Czechkoruna.png";
            imgCzechkoruna.Enabled = false;           
            imgPound.ImageUrl = "../../Images/pound-dis.png";
            imgPound.Enabled = true;
            imgEuro.ImageUrl = "../../Images/euro-dis.png";
            imgEuro.Enabled = true;
            imgDollar.ImageUrl = "../../Images/Dollar-dis.png";
            imgDollar.Enabled = true;
            CurrentCurrencyID = 4;
        }

        List<VIPManagementReportBE> oVIPManagementReportBEList = new List<VIPManagementReportBE>();

        VIPManagementReportBE oVIPManagementReportBE = new VIPManagementReportBE();
        VIPManagementReportBAL oVIPManagementReportBAL = new VIPManagementReportBAL();

        oVIPManagementReportBE.Action = "GetReportDetails";
        if (! string.IsNullOrEmpty(txtDate.innerControltxtDate.Value)) 
            oVIPManagementReportBE.CalcultionDate = txtDate.GetDate.Date;

        oVIPManagementReportBEList = oVIPManagementReportBAL.GetVIPReportDetailsBAL(oVIPManagementReportBE);

        //Hide Sale and Margin as per Anthony's suggestion temp
        oVIPManagementReportBEList = oVIPManagementReportBEList.Where(o => o.AreaKey != "Sale" && o.AreaKey != "Margin").ToList();
        //-----------

        ViewState["oVIPManagementReportBEList"] = oVIPManagementReportBEList;

        if (oVIPManagementReportBEList != null && oVIPManagementReportBEList.Count > 0)
            txtDate.innerControltxtDate.Value = Convert.ToDateTime(oVIPManagementReportBEList[0].CalcultionDate).ToString("dd/MM/yyyy");

        var oVIPManagementReport = oVIPManagementReportBEList.OrderBy(o => o.KeyOrder).Select(c => new { c.Area }).Distinct().ToList();

        if (isExport == false) {
            rptVIPMgmtReports.DataSource = oVIPManagementReport;
            rptVIPMgmtReports.DataBind();
        }
        else {
            rptVIPMgmtReportsExport.DataSource = oVIPManagementReport;
            rptVIPMgmtReportsExport.DataBind();
        }
    }    

    protected void rptVIPMgmtReports_OnItemDataBound(object sender, RepeaterItemEventArgs e) {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
            
            Literal ltArea = (Literal)e.Item.FindControl("ltArea");
            if ((Literal)e.Item.FindControl("ltInfo") != null) {
                Literal ltInfo = (Literal)e.Item.FindControl("ltInfo");
                SetInfoText(ref ltInfo, ltArea.Text);
            }
           

            List<VIPManagementReportBE> oVIPManagementReportBEList = (List<VIPManagementReportBE>)ViewState["oVIPManagementReportBEList"];
            //var oVIPManagementReport = oVIPManagementReportBEList.Select(c => { c.Area = ucPnl.GroupingText; return c; }).Distinct().Take(1).ToList();

            var oVIPManagementReport = oVIPManagementReportBEList.Where(c => c.Area == ltArea.Text).ToList();


            if (ltArea.Text == "Daily Sales") {
                if ((ucPanel)e.Item.FindControl("ucPnl") != null) {
                    ucPanel ucPnl = (ucPanel)e.Item.FindControl("ucPnl");
                    ucPnl.GroupingText = "Daily COGs";
                }
                else {
                    ltArea.Text = "Daily COGs";
                }
            }

            Repeater rptSite = (Repeater)e.Item.FindControl("rptSite");
            //var uniqueSites1 = oVIPManagementReport.Select(c => new { c.SiteID, c.SitePrefix, c.SiteCountryID }).Distinct().OrderBy(o => o.SiteCountryID).ThenBy(o => o.SitePrefix).ToList();
            var uniqueSites = oVIPManagementReportBEList.Where(c => c.SiteID != 9999).Select(c => new { c.SiteID, c.SitePrefix, c.SiteCountryID }).Distinct().OrderBy(o => o.SiteCountryID).ThenBy(o => o.SitePrefix).ToList();
                   
            rptSite.DataSource = uniqueSites;
            rptSite.DataBind();

            Repeater rptkeyDetails = (Repeater)e.Item.FindControl("rptkeyDetails");         
            var uniqueAreaKeys = oVIPManagementReport.Select(c => new { c.AreaKey, c.KeyOrder }).Distinct().OrderBy(o => o.KeyOrder).ToList();
            rptkeyDetails.DataSource = uniqueAreaKeys;
            rptkeyDetails.DataBind();
        }
    }

    private void SetInfoText(ref Literal ltInfo, string strArea) {

        switch (strArea) {
            case "Stock Overview":
            ltInfo.Text = "The value stated is based on the Stock On Hand position at the end of play from the previous day.<br> It is calculated as follows-<br><b>Current Item Cost Price x Qty On Hand</b> (from previous working day)";
            break;
            case "Daily Sales":
            ltInfo.Text = "The value stated here is derived based on <b>Item Cost Price x Qty Shipped</b> (for the previous working day)";
            break;
            case "OTIF Rate":
            string readText = string.Empty; // File.ReadAllText("OTIFSetting.htm");
             string templatePath = null;
             string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
             string templatePathName = @"/ModuleUI/MgmtReports/";
            templatePath = path + templatePathName;

            OTIFRulesSettingBE oOTIFRulesSettingBE = new OTIFRulesSettingBE();
            OTIFRulesSettingBAL OTIFRulesSettingBAL = new OTIFRulesSettingBAL();
            oOTIFRulesSettingBE.Action = "GetOTIFRules";

            OTIFRulesSettingBE oNewOTIFRulesSettingBE = OTIFRulesSettingBAL.GetOTIFRules(oOTIFRulesSettingBE);
            OTIFRulesSettingBAL = null;
          

            using (StreamReader sReader = new StreamReader(templatePath + "/OTIFSetting.htm")) {
              
                readText = sReader.ReadToEnd();

                readText = readText.Replace("Absolute1", oNewOTIFRulesSettingBE.AbsoluteDaysAllowedEarly.ToString());
                readText = readText.Replace("Absolute2", oNewOTIFRulesSettingBE.AbsoluteDaysAllowedLate.ToString());
                readText = readText.Replace("Absolute3", oNewOTIFRulesSettingBE.AbsoluteNumberOfDeliveriesAllowed.ToString());

                readText = readText.Replace("NewOTIF1", oNewOTIFRulesSettingBE.ToleratedDaysAllowedEarly1.ToString());
                readText = readText.Replace("NewOTIF2", oNewOTIFRulesSettingBE.ToleratedDaysAllowedLate1.ToString());
                readText = readText.Replace("NewOTIF3", oNewOTIFRulesSettingBE.ToleratedNumberOfDeliveriesAllowed1.ToString());


                readText = readText.Replace("OldOTIF1", oNewOTIFRulesSettingBE.ODMeasureDaysAllowedEarly11.ToString());
                readText = readText.Replace("OldOTIF2", oNewOTIFRulesSettingBE.ODMeasureDaysAllowedLate11.ToString());
                readText = readText.Replace("OldOTIF3", oNewOTIFRulesSettingBE.ODMeasureNumberOfDeliveriesAllowed11.ToString());
            }
            ltInfo.Text = readText;
            break;
            case "PO Overview":
            ltInfo.Text = "The details below are  based on the Open Purchase Order position at the start of current working day";
            break;
            case "Back Order Overview":
            ltInfo.Text = "The back order postion is based on back order status  at the start of current working day";
            break;
            case "Scheduling Overview":
            ltInfo.Text = "These details relate to the bookings and deliveries made for the previous working day";
            break;
            case "Discrepancy Overview":
            ltInfo.Text = "Discrepancies Raised relates to the number of discrepancies created on the previous working day.  The remaining values are based on the start position of the current working day";
            break;
        }

    }

    protected void rptkeyDetails_OnItemDataBound(object sender, RepeaterItemEventArgs e) {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {

            List<VIPManagementReportBE> oVIPManagementReportBEList = (List<VIPManagementReportBE>)ViewState["oVIPManagementReportBEList"];
            Repeater rptKeyvalue = (Repeater)e.Item.FindControl("rptKeyvalue");
            HyperLink hpLink = (HyperLink)e.Item.FindControl("hpLink");            

            var oVIPManagementReportBE = oVIPManagementReportBEList.Where(c => c.AreaKey == hpLink.Text).OrderBy(o => o.SiteCountryID).ThenBy(o => o.SitePrefix).ToList();

            hpLink.NavigateUrl = Utilities.Common.EncryptQuery("DetailedOverview.aspx?ReportType=" + oVIPManagementReportBE[0].ReportType + "&ReportDate=" + txtDate.GetDate.ToString("dd/MM/yyyy"));

            double? Total = 0;

            if (oVIPManagementReportBE[0].Area == "OTIF Rate") {
                Total = Convert.ToDouble(oVIPManagementReportBE.Where(c => c.SitePrefix == "Overall").ToList()[0].KeyValue);
                oVIPManagementReportBE = oVIPManagementReportBE.Where(c => c.SitePrefix != "Overall").ToList();
            }
            else if (oVIPManagementReportBE[0].Area == "Stock Overview" || oVIPManagementReportBE[0].Area == "Daily Sales") {
                if (hpLink.Text == "Current Stock Value" || hpLink.Text == "COGS" || hpLink.Text == "Sale" || hpLink.Text == "Margin") 
                    oVIPManagementReportBE.ToList().ForEach(i => i.KeyValue = GetCurrencyConversion(i.SiteID,i.KeyValue)); 
                //Total = Convert.ToDouble(oVIPManagementReportBE.Where(c => c.SitePrefix == "Total").ToList()[0].KeyValue);
                Total = oVIPManagementReportBE.Where(c => c.SitePrefix != "Total").Sum(q => q.KeyValue);
                oVIPManagementReportBE = oVIPManagementReportBE.Where(c => c.SitePrefix != "Total").ToList();
            }
            else if (oVIPManagementReportBE[0].Area == "Back Order Overview" || oVIPManagementReportBE[0].Area == "Stock Turn Details") {                
                Total = Convert.ToDouble(oVIPManagementReportBE.Where(c => c.SitePrefix == "Total").ToList()[0].KeyValue);
                oVIPManagementReportBE = oVIPManagementReportBE.Where(c => c.SitePrefix != "Total").ToList();
            }
            else {
                if (hpLink.Text != "% Overdue" && hpLink.Text != "% Line Overdue" && hpLink.Text != "% Line Overdue today" && hpLink.Text != "% booking made by Vendors") {
                    Total = oVIPManagementReportBE.Sum(q => q.KeyValue);
                }
                else {
                    Total = oVIPManagementReportBE[0].TotalKeyPct;
                }
            }

            Literal ltSiteTotal = (Literal)e.Item.FindControl("ltSiteTotal");
            if ((Total > 0) || (hpLink.Text == "Sale" || hpLink.Text == "Margin")) 
                ltSiteTotal.Text = Math.Round(Convert.ToDouble(Total), 2).ToString(); // string.Format("{0:0.00}", Total);
            else
                ltSiteTotal.Text = "0";

            if (hpLink.Text == "Current Stock Value" || hpLink.Text == "COGS" || hpLink.Text == "Sale" || hpLink.Text == "Margin") {
                ltSiteTotal.Text = getCurrencyHTML(Convert.ToDecimal(ltSiteTotal.Text)).Replace(" ", ""); 
            }

            if (oVIPManagementReportBE[0].Area == "OTIF Rate") {
                ltSiteTotal.Text = StringFormatTwoDecimal(ltSiteTotal.Text);  
            }

            var uniqueSites = oVIPManagementReportBEList.Where(c => c.SiteID != 9999).Select(c => new { c.SiteID, c.SitePrefix, c.SiteCountryID }).Distinct().OrderBy(o => o.SiteCountryID).ThenBy(o => o.SitePrefix).ToList();
            VIPManagementReportBE oNewVIPManagementReportBE = new VIPManagementReportBE();
            for (int iCount = 0; iCount < uniqueSites.Count; iCount++) {

                if (!oVIPManagementReportBE.Exists(x => x.SiteID == uniqueSites[iCount].SiteID)) {
                    
                        oNewVIPManagementReportBE = new VIPManagementReportBE();
                        oNewVIPManagementReportBE.SitePrefix = uniqueSites[iCount].SitePrefix;
                        oNewVIPManagementReportBE.SiteID = uniqueSites[iCount].SiteID;
                        oNewVIPManagementReportBE.SiteCountryID = uniqueSites[iCount].SiteCountryID;
                        oNewVIPManagementReportBE.Area = oVIPManagementReportBE[0].Area;
                        oVIPManagementReportBE.Add(oNewVIPManagementReportBE);                    
                }
            }
            oVIPManagementReportBE = oVIPManagementReportBE.GroupBy(x => x.SiteID).Select(o => o.First()).OrderBy(o => o.SiteCountryID).ThenBy(o => o.SitePrefix).ToList();
            rptKeyvalue.DataSource = oVIPManagementReportBE;
            rptKeyvalue.DataBind();
        }
    }

    protected void rptKeyvalue_OnItemDataBound(object sender, RepeaterItemEventArgs e) {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
            Literal hdnArea = (Literal)e.Item.FindControl("hdnArea");
            Literal ltKeyvalue = (Literal)e.Item.FindControl("ltKeyvalue");           


            if (hdnArea.Text == "OTIF Rate") {
                ltKeyvalue.Text = StringFormatTwoDecimal(ltKeyvalue.Text);  
            }

            if (
                ((VIPManagementReportBE)e.Item.DataItem).AreaKey == "Current Stock Value" 
                
                || ((VIPManagementReportBE)e.Item.DataItem).AreaKey == "COGS"

                || ((VIPManagementReportBE)e.Item.DataItem).AreaKey == "Sale"

                || ((VIPManagementReportBE)e.Item.DataItem).AreaKey == "Margin"
                
                ) {
                ltKeyvalue.Text = getCurrencyHTML(Convert.ToDecimal(ltKeyvalue.Text)).Replace(" ", ""); 
            }
        }
    }

    private string FormatPrice(decimal price, string cultureName) {
        string formatted = null;
        CultureInfo info = new CultureInfo(cultureName);
        switch (info.Name) {
            case "en-US":
            formatted = string.Format(CultureInfo.CreateSpecificCulture("en-US"),
                "{0:C0}", price);
            break;
            case "en-GB":
            formatted = string.Format(CultureInfo.CreateSpecificCulture("en-GB"),
                "{0:C0}", price);
            break;
            case "fr-FR":
            formatted = string.Format(CultureInfo.CreateSpecificCulture("fr-FR"),
                "{0:C0}", price);
            break;
            case "el-GR":
            formatted = string.Format(CultureInfo.CreateSpecificCulture("el-GR"),
                "{0:C0}", price);
            break;
            case "cs-CZ":
            formatted = string.Format(CultureInfo.CreateSpecificCulture("cs-CZ"),
                "{0:C0}", price);
            break;
        }

        return formatted;
    }

    private string getCurrencyHTML(decimal price) {
        string CurrencyHTML = string.Empty;

        if (CurrentCurrencyID == 1)
            CurrencyHTML = "€" + FormatPrice(price, "el-GR").Replace(",", "*").Replace(".", ",").Replace("*", ".").Replace("€", "");
        if (CurrentCurrencyID == 2)
            if (price < 0)
                CurrencyHTML = "£-" + FormatPrice(price * -1, "en-GB").Replace("£", "");
            else
                CurrencyHTML = FormatPrice(price, "en-GB");            
        if (CurrentCurrencyID == 3)
            if (price < 0)
                CurrencyHTML = "$-" + FormatPrice(price * -1, "en-US").Replace("$","");
            else
                CurrencyHTML = FormatPrice(price, "en-US");

        if (CurrentCurrencyID == 4)
            if (price < 0)
                CurrencyHTML = "Kč" + FormatPrice(price * -1, "cs-CZ").Replace("Kč", "");
            else
                CurrencyHTML = FormatPrice(price, "cs-CZ");
        return CurrencyHTML;
    }

    private string StringFormatTwoDecimal(string inputString) {
        if (!string.IsNullOrEmpty(inputString)) {
            string[] OT = inputString.Split('.');
            if (OT.Length == 1)
                inputString = inputString + ".00%";
            else if (OT.Length == 2)
                if (OT[1].Length == 1)
                    inputString = inputString + "0%";
                else if (OT[1].Length == 2)
                    inputString = inputString + "%";
        }
        return inputString;
    }

    private double? GetCurrencyConversion(int SiteID, double? StockValue) {

        if (SiteID == 9999 || StockValue == 0)
            return StockValue;

        int SiteCurrencyID = 0;
        DataTable SiteCurrency = null;

        if (ViewState["SiteCurrency"] != null) {
            SiteCurrency = (DataTable)ViewState["SiteCurrency"];
        }
        else {
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

            oMAS_SiteBE.Action = "GetSiteCurrency";
            SiteCurrency = oMAS_SiteBAL.GetCountrySite(oMAS_SiteBE);
            ViewState["SiteCurrency"] = SiteCurrency;
        }

        DataView dv = SiteCurrency.DefaultView;
        dv.RowFilter = "SiteID = '" + SiteID.ToString() + "'";
        SiteCurrency = dv.ToTable();

        if (SiteCurrency.Rows.Count > 0) {
            foreach (DataRow dr in SiteCurrency.Rows) {
                SiteCurrencyID = Convert.ToInt32(dr["CurrencyID"]);
            }
        }


        CurrencyConverterBAL o_currencyBAL = new CurrencyConverterBAL();
        CurrencyConverterBE o_CurrencyBE = new CurrencyConverterBE();

        List<CurrencyConverterBE> lstCurrencyBE = new List<CurrencyConverterBE>();

        if (ViewState["lstCurrencyBE"] != null) {
            lstCurrencyBE = (List<CurrencyConverterBE>)ViewState["lstCurrencyBE"];
            if (lstCurrencyBE == null || lstCurrencyBE.Count == 0) {
                o_CurrencyBE.Action = "ShowAll";
                lstCurrencyBE = o_currencyBAL.BindCurrencyBAL(o_CurrencyBE);
                lstCurrencyBE = lstCurrencyBE.Where(cc => cc.IsPrevious == false).ToList();
                ViewState["lstCurrencyBE"] = lstCurrencyBE;
            }
        }
        else {
            o_CurrencyBE.Action = "ShowAll";
            lstCurrencyBE = o_currencyBAL.BindCurrencyBAL(o_CurrencyBE);
            lstCurrencyBE = lstCurrencyBE.Where(cc => cc.IsPrevious == false).ToList();
            ViewState["lstCurrencyBE"] = lstCurrencyBE;
        }

        if (SiteCurrency.Rows.Count > 0 && lstCurrencyBE.Count > 0 && SiteCurrencyID != DefaultCurrencyID) {

            lstCurrencyBE = lstCurrencyBE.Where(cc => cc.CurrencyID == SiteCurrencyID && cc.ConvertCurrencyID == DefaultCurrencyID).ToList();

            if (lstCurrencyBE.Count > 0) {
                return StockValue * lstCurrencyBE[0].ExchangeRate;
            }
            else {
                return 0; // StockValue;
            }
        }
        else {
            return StockValue;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e) {
        BindManagementReport(true);

        Response.ClearContent();
        Response.Buffer = true;
        Response.ContentType = "application/force-download";
        Response.AddHeader("content-disposition", "attachment; filename=VIPMgmtReport.xls");
        Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
        Response.Write("<head>");
        Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
        Response.Write("<!--[if gte mso 9]><xml>");
        Response.Write("<x:ExcelWorkbook>");
        Response.Write("<x:ExcelWorksheets>");
        Response.Write("<x:ExcelWorksheet>");
        Response.Write("<x:Name>VIP Management Report Overview</x:Name>");
        Response.Write("<x:WorksheetOptions>");
        Response.Write("<x:Print>");
        Response.Write("<x:ValidPrinterInfo/>");
        Response.Write("</x:Print>");
        Response.Write("</x:WorksheetOptions>");
        Response.Write("</x:ExcelWorksheet>");
        Response.Write("</x:ExcelWorksheets>");
        Response.Write("</x:ExcelWorkbook>");
        Response.Write("</xml>");
        Response.Write("<![endif]--> ");
        StringWriter tw = new StringWriter(); HtmlTextWriter hw = new HtmlTextWriter(tw); tblExport.RenderControl(hw);
        Response.Write(tw.ToString());
        Response.Write("</head>");
        Response.End();

        //Response.ClearContent();
        //Response.Buffer = true;
        //Response.AddHeader("content-disposition", "attachment;filename=Details.xls");
        //Response.Charset = "";
        //Response.ContentType = "application/excel";
        //System.IO.StringWriter sw = new System.IO.StringWriter();
        //HtmlTextWriter htm = new HtmlTextWriter(sw);
        //tblExport.RenderControl(htm);
        //Response.Write(sw.ToString());
        //Response.End();       
    }

    protected void imgEuro_Click(object sender, ImageClickEventArgs e) {
        CurrentCurrencyID = DefaultCurrencyID = 1;        
        BindManagementReport();
    }

    protected void imgPound_Click(object sender, ImageClickEventArgs e) {
        CurrentCurrencyID = DefaultCurrencyID = 2;
        BindManagementReport();
    }

    protected void imgDollar_Click(object sender, ImageClickEventArgs e) {
        CurrentCurrencyID = DefaultCurrencyID = 3;
        BindManagementReport();
    }

    protected void imgCzechkoruna_Click(object sender, ImageClickEventArgs e)
    {
        CurrentCurrencyID = DefaultCurrencyID = 4;
        BindManagementReport();
    }
}
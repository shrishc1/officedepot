﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/NoUser_MasterPage.Master"
    AutoEventWireup="true" CodeFile="MgmtLogin.aspx.cs" Inherits="MgmtLogin" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="sp1" runat="server">
    </asp:ScriptManager>
    <h2>
        <cc1:ucLabel ID="lblVIPManagementReport" runat="server" Text="VIP Management Report"></cc1:ucLabel>
    </h2>
    <div class="right-shadow">
        <div class="formbox">
            <div class="button-row" style="text-align: center;">
                <cc1:ucButton ID="btnCreateReportLink" runat="server" CssClass="button" OnClick="btnCreateReportLink_Click" />
                &nbsp;&nbsp;&nbsp;
                <cc1:ucButton ID="btnChangePassword" runat="server" CssClass="button" OnClick="btnChangePassword_Click" />
            </div>
            <%--<table width="35%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings" id="tblSecurityCode" runat="server">
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblEnterSecureCode" runat="server" Text="Enter Secure Code"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        :
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox ID="txtSecureCode" clientidmode="Static" runat="server" onkeydown = "return (event.keyCode!=13);"  TextMode="Password" Width="80"></cc1:ucTextbox>
                    </td>
                    <td>
                        <cc1:ucButton ID="btnGo" runat="server" CssClass="button" OnClick="btnGo_Click" />
                    </td>
                </tr>
            </table>--%>
            <table width="65%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings"
                id="tblChangePass" runat="server" visible="false">
                <tr>
                    <td style="font-weight: bold; width: 30%">
                        <cc1:ucLabel ID="lblCurrentPassword" runat="server" Text="Please enter Current Password"
                            isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 1%">
                        <cc1:ucLabel ID="Label17" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold; width: 69%">
                        <cc1:ucTextbox ID="txtCurrentPassword" TextMode="Password" runat="server" Width="200px"
                            MaxLength="50"></cc1:ucTextbox>
                        <asp:RequiredFieldValidator ID="rfvCurrentPasswordRequired" runat="server" ErrorMessage="Please enter Current Password"
                            Display="None" ValidationGroup="OK" ControlToValidate="txtCurrentPassword"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblNewPassword" runat="server" Text="Enter New Password" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel2" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox ID="txtNewPassword" TextMode="Password" runat="server" Width="200px"
                            MaxLength="50"></cc1:ucTextbox>
                        <asp:RequiredFieldValidator ID="rfvNewPasswordRequired" runat="server" ErrorMessage="Please enter New Password"
                            Display="None" ValidationGroup="OK" ControlToValidate="txtNewPassword"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="lblConfirmPassword" runat="server" Text="Confirm New Password" isRequired="true"></cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucLabel ID="UcLabel4" runat="server">:</cc1:ucLabel>
                    </td>
                    <td style="font-weight: bold;">
                        <cc1:ucTextbox ID="txtConfirmPassword" TextMode="Password" runat="server" Width="200px"
                            MaxLength="50"></cc1:ucTextbox>
                        <asp:RequiredFieldValidator ID="rfvConfirmPasswordRequired" runat="server" ErrorMessage="Please enter Confirm Password"
                            Display="None" ValidationGroup="OK" ControlToValidate="txtConfirmPassword"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cmpvPasswordCompare" runat="server" ErrorMessage="New Password and Confirm Password does not match"
                            Display="None" ValidationGroup="OK" Type="String" Operator="Equal" ControlToValidate="txtConfirmPassword"
                            ControlToCompare="txtNewPassword" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="2" align="right">
                        <cc1:ucButton ID="btnOK" runat="server" Text="OK" CssClass="button" OnClick="btnOK_Click"
                            ValidationGroup="OK" />
                        <cc1:ucButton ID="btnCancel" runat="server" Text="Cancel" class="button" OnClick="btnCancel_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:ValidationSummary ID="vs1" runat="server" ValidationGroup="OK" ShowSummary="false"
                            ShowMessageBox="true" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!-- Phase 13 R3 Point 11 --->
    <asp:UpdatePanel ID="updInfromation" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnInfo" runat="Server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdInformation" runat="server" TargetControlID="btnInfo"
                PopupControlID="pnlbtnInfo" BackgroundCssClass="modalBackground" BehaviorID="MsgbtnInfo"
                DropShadow="false" />
            <asp:Panel ID="pnlbtnInfo" runat="server" Style="display: none;">
                <div class="popup-maincontainer" style="overflow-y: hidden; overflow-x: hidden;">
                    <h3>
                        <cc1:ucLabel ID="lblWarningInfo" runat="server"></cc1:ucLabel></h3>
                    <table cellspacing="5" cellpadding="0" border="0" align="center" class="popup-maincontainer">
                        <tr>
                            <td>
                                <div class="popup-innercontainer top-setting-Popup">
                                    <div class="row1">
                                        <div style="text-align: center;">
                                            <cc1:ucLabel ID="lblCreateLinkURL" Font-Size="Small" runat="server" Text="Please copy the below text and paste into your email"></cc1:ucLabel>
                                        </div>
                                        <br />
                                        <br />
                                        <cc1:ucLabel ID="ltLnik" Font-Bold="true" Font-Size="Small" runat="server"></cc1:ucLabel>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div class="row">
                                    <cc1:ucButton ID="btnOK_1" runat="server" Text="" CssClass="button" OnCommand="btnOK_1_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnOK_1" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

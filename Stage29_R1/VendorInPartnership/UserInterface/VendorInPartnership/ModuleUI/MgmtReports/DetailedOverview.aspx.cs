﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities.ModuleBE.MgmtReports;
using BusinessLogicLayer.ModuleBAL.MgmtReports;
using BaseControlLibrary;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessEntities.ModuleBE.AdminFunctions;
using System.Data;
using System.Globalization;

public partial class DetailedOverview : CommonPage
{
    public int CurrentCurrencyID
    {
        get
        {
            return Session["CurrentCurrencyID"] != null ? Convert.ToInt32(Session["CurrentCurrencyID"]) : 1;
        }
        set
        {
            Session["CurrentCurrencyID"] = value;
        }
    }

    string strExchnageRateSite = ",ASH,LEI,NTH,";

    public int tblColSpan
    {
        get
        {
            return ViewState["tblColSpan"] != null ? Convert.ToInt32(ViewState["tblColSpan"]) : 0;
        }
        set
        {
            ViewState["tblColSpan"] = value;
        }
    }

    void Page_PreInit(Object sender, EventArgs e)
    {
        if (Session["UserID"] != null)
            this.MasterPageFile = "~/CommonUI/CMN_MasterPages/CRUD_MasterPage.Master";
        else
            this.MasterPageFile = "~/CommonUI/CMN_MasterPages/NoUser_MasterPage.Master";

        if (Session["MgmtLogin"] == null)
            Response.Redirect("Dashboard.aspx");
    }

    protected void Page_InIt(object sender, EventArgs e)
    {
        txtDate.CurrentPage = this;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            pager1.PageSize = 340;
            pager1.GenerateGoToSection = false;
            pager1.GeneratePagerInfoSection = false;
            ViewState["isExchangeRateShown"] = false;
            if (GetQueryStringValue("ReportDate") != null)
            {
                txtDate.innerControltxtDate.Value = GetQueryStringValue("ReportDate");
                if (GetQueryStringValue("ReportType") != null)
                {
                    string ReportType = GetQueryStringValue("ReportType");
                    ViewState["ReportType"] = ReportType;
                    switch (ReportType)
                    {
                        case "SO":
                            ltOverViewType.Text = "Stock Overview";
                            ViewState["isExchangeRateShown"] = true;
                            //tdExRate.Style.Add("display", "");
                            break;
                        case "DS":
                            ltOverViewType.Text = "COGS";
                            ViewState["isExchangeRateShown"] = true;
                            //tdExRate.Style.Add("display", "");
                            break;
                        case "SA":
                            ltOverViewType.Text = "Sale";
                            ViewState["isExchangeRateShown"] = true;
                            //tdExRate.Style.Add("display", "");
                            //divRFC.Visible = true;
                            break;
                        case "MA":
                            ltOverViewType.Text = "Margin";
                            ViewState["isExchangeRateShown"] = true;
                            //tdExRate.Style.Add("display", "");
                            //divRFC.Visible = true;
                            break;
                        case "OR":
                            ltOverViewType.Text = "OTIF Overview";
                            break;
                        case "PO":
                        case "LINE":
                        case "TOLI":
                            ddlReportType.Items.Add(new ListItem("PO Due and Over Due", "PO"));
                            ddlReportType.Items.Add(new ListItem("Lines Due and Over Due", "LINE"));
                            ddlReportType.Items.Add(new ListItem("Lines Due and Over Due Today", "TOLI"));
                            ddlReportType.SelectedIndex = ddlReportType.Items.IndexOf(ddlReportType.Items.FindByValue(GetQueryStringValue("ReportType")));
                            lblReportType.Visible = ltColon.Visible = ddlReportType.Visible = true;
                            ltOverViewType.Text = "Purchase Order Overview";
                            break;
                        case "BO":
                            ltOverViewType.Text = "Back Orders Overview";
                            break;
                        case "BOK":
                        case "BOV":
                        case "BOI":
                            ddlReportType.Items.Add(new ListItem("Bookings", "BOK"));
                            ddlReportType.Items.Add(new ListItem("Volume", "BOV"));
                            ddlReportType.Items.Add(new ListItem("Issues", "BOI"));
                            ddlReportType.SelectedIndex = ddlReportType.Items.IndexOf(ddlReportType.Items.FindByValue(GetQueryStringValue("ReportType")));
                            lblReportType.Visible = ltColon.Visible = ddlReportType.Visible = true;
                            ltOverViewType.Text = "Scheduling Overview";
                            break;
                        case "DO":
                            ltOverViewType.Text = "Discrepancy Overview";
                            break;
                        case "STD":
                            ltOverViewType.Text = "Stock Turn Details";
                            break;
                    }
                }
                BindManagementReport();
            }


        }
    }

    public void pager_Command(object sender, CommandEventArgs e)
    {
        int currnetPageIndx = Convert.ToInt32(e.CommandArgument);
        pager1.CurrentIndex = currnetPageIndx;
        BindManagementReport(currnetPageIndx);
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        BindManagementReport();
    }

    private void BindManagementReport(int Page = 1)
    {

        if (Convert.ToBoolean(ViewState["isExchangeRateShown"]))
        {
            if (CurrentCurrencyID == 1)
            {
                imgEuro.ImageUrl = "../../Images/euro.png";
                imgEuro.ToolTip = "Euro";
            }
            else if (CurrentCurrencyID == 2)
            {
                imgEuro.ImageUrl = "../../Images/pound.png";
                imgEuro.ToolTip = "UK Pound";
            }
            else if (CurrentCurrencyID == 3)
            {
                imgEuro.ImageUrl = "../../Images/Dollar.png";
                imgEuro.ToolTip = "US Dollar";
            }
            else if (CurrentCurrencyID == 4)
            {
                imgEuro.ImageUrl = "../../Images/CzechKoruna.png";
                imgEuro.ToolTip = "Czech koruna";
            }
            trCurrency.Visible = true;
        }
        else
        {
            trCurrency.Visible = false;
        }



        //return;
        List<VIPManagementReportBE> oVIPManagementReportBEList = new List<VIPManagementReportBE>();

        VIPManagementReportBE oVIPManagementReportBE = new VIPManagementReportBE();
        VIPManagementReportBAL oVIPManagementReportBAL = new VIPManagementReportBAL();

        oVIPManagementReportBE.Action = "GetSpecificReportDetails";
        oVIPManagementReportBE.CalcultionDate = txtDate.GetDate.Date;

        if (ddlReportType.Visible)
            oVIPManagementReportBE.ReportType = Convert.ToString(ddlReportType.SelectedItem.Value);
        else
            oVIPManagementReportBE.ReportType = Convert.ToString(ViewState["ReportType"]);

        oVIPManagementReportBE.Page = Page;

        oVIPManagementReportBEList = oVIPManagementReportBAL.GetVIPReportDetailsBAL(oVIPManagementReportBE);

        if (oVIPManagementReportBEList != null && oVIPManagementReportBEList.Count > 0)
        {
            ViewState["oVIPManagementReportBEList"] = oVIPManagementReportBEList;

            var uniqueSites = oVIPManagementReportBEList.Select(c => new { c.SiteID, c.SitePrefix, c.SiteCountryID }).Distinct().OrderBy(o => o.SiteCountryID).ThenBy(o => o.SitePrefix).ToList();
            rptSite.DataSource = uniqueSites;

            var uniqueAreaKeys = oVIPManagementReportBEList.Select(c => new { c.CalcultionDate }).Distinct().OrderByDescending(o => o.CalcultionDate).ToList();
            rptkeyDetails.DataSource = uniqueAreaKeys;

            pager1.ItemCount = oVIPManagementReportBEList[0].TotalRecords;
            pager1.Visible = true;
            ViewState["TotalRecords"] = oVIPManagementReportBEList[0].TotalRecords;
        }
        else
        {
            rptSite.DataSource = null;
            rptkeyDetails.DataSource = null;
            pager1.ItemCount = 0;
            pager1.Visible = false;
        }

        rptSite.DataBind();
        rptkeyDetails.DataBind();
        pager1.CurrentIndex = Page;
    }

    protected void rptSite_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            //if (Convert.ToBoolean(ViewState["isExchangeRateShown"])) {
            //    Literal ltSitePreFix = (Literal)e.Item.FindControl("ltSitePreFix");
            //    if (strExchnageRateSite.Contains("," + ltSitePreFix.Text + ","))
            //        ltSitePreFix.Text = ltSitePreFix.Text + "*";
            //}

            List<VIPManagementReportBE> oVIPManagementReportBEList = (List<VIPManagementReportBE>)ViewState["oVIPManagementReportBEList"];

            Repeater rptKeyHeader = (Repeater)e.Item.FindControl("rptKeyHeader");
            string[] oVIPManagementReportBE = oVIPManagementReportBEList.Select(c => c.KeyHeader).ToList()[0].Split('~');

            List<VIPManagementReportBE> lst = new List<VIPManagementReportBE>();
            for (int iHeaderCount = 0; iHeaderCount < oVIPManagementReportBE.Length; iHeaderCount++)
                lst.Add(new VIPManagementReportBE() { KeyHeader = oVIPManagementReportBE[iHeaderCount] });

            rptKeyHeader.DataSource = lst;
            rptKeyHeader.DataBind();

            //var tblSite = (HtmlTable)e.Item.FindControl("tblSite");
            tblMain.Attributes.Add("width", (lst.Count * 870).ToString() + "px");


            var tdSite = (HtmlTableCell)e.Item.FindControl("tdControlSite");
            tdSite.Attributes.Add("colspan", lst.Count.ToString());

            tblColSpan = lst.Count;

            //int rowWidth = 100 / lst.Count;
            for (int iRowCount = 0; iRowCount < lst.Count; iRowCount++)
            {
                var td = (HtmlTableCell)rptKeyHeader.Items[iRowCount].FindControl("tdControl");
                td.Style.Add("width", "42px");
            }
        }
    }

    protected void rptkeyDetails_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            List<VIPManagementReportBE> oVIPManagementReportBEList = (List<VIPManagementReportBE>)ViewState["oVIPManagementReportBEList"];
            Repeater rptKeyvalue = (Repeater)e.Item.FindControl("rptKeyvalue");
            //HyperLink hpLink = (HyperLink)e.Item.FindControl("hpLink");
            Literal hdnDate = (Literal)e.Item.FindControl("hdnDate");

            var uniqueSites = oVIPManagementReportBEList.Select(c => new { c.SiteID, c.SitePrefix, c.SiteCountryID }).Distinct().OrderBy(o => o.SiteCountryID).ThenBy(o => o.SitePrefix).ToList();

            //var minDate = oVIPManagementReportBEList.Min(x => x.CalcultionDate);

            oVIPManagementReportBEList = oVIPManagementReportBEList.Where(c => c.CalcultionDate == Utilities.Common.GetDateTime(hdnDate.Text)).ToList();

            //if (Utilities.Common.GetDateTime(hdnDate.Text) == minDate) {
            //    if (oVIPManagementReportBEList.Count < uniqueSites.Count) {
            //        return;
            //    }
            //}

            VIPManagementReportBE oNewVIPManagementReportBE = new VIPManagementReportBE();
            for (int iCount = 0; iCount < uniqueSites.Count; iCount++)
            {

                if (!oVIPManagementReportBEList.Exists(x => x.SiteID == uniqueSites[iCount].SiteID))
                {

                    oNewVIPManagementReportBE = new VIPManagementReportBE();
                    oNewVIPManagementReportBE.CalcultionDate = Convert.ToDateTime(Utilities.Common.GetDateTime(hdnDate.Text));
                    oNewVIPManagementReportBE.SitePrefix = uniqueSites[iCount].SitePrefix;
                    oNewVIPManagementReportBE.SiteID = uniqueSites[iCount].SiteID;
                    oNewVIPManagementReportBE.SiteCountryID = uniqueSites[iCount].SiteCountryID;
                    //oNewVIPManagementReportBE.AreaKey = uniqueSites[iCount];
                    oVIPManagementReportBEList.Add(oNewVIPManagementReportBE);
                }
            }


            oVIPManagementReportBEList = oVIPManagementReportBEList.OrderBy(o => o.SiteCountryID).ThenBy(o => o.SitePrefix).ToList();

            rptKeyvalue.DataSource = oVIPManagementReportBEList;
            rptKeyvalue.DataBind();
        }
    }

    protected void rptKeyvalue_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            VIPManagementReportBE i = (VIPManagementReportBE)e.Item.DataItem;
            Repeater rptKeyValueDetails = (Repeater)e.Item.FindControl("rptKeyValueDetails");



            //Repeater repeaterChild = (Repeater)e.Item.Parent;            
            //Repeater repeaterParent = (Repeater)repeaterChild.Parent.Parent;


            //ControlCollection ctrlColl = repeaterParent.Controls;
            //Literal hdnDate = null;

            //foreach (Control ctrl in ctrlColl) {
            //    hdnDate = (Literal)ctrl.FindControl("hdnDate");
            //}

            //List<VIPManagementReportBE> oVIPManagementReportBEList = (List<VIPManagementReportBE>)ViewState["oVIPManagementReportBEList"];
            //var uniqueSites = oVIPManagementReportBEList.Select(c => new { c.SiteID, c.SitePrefix, c.SiteCountryID }).Distinct().OrderBy(o => o.SiteCountryID).ThenBy(o => o.SitePrefix).ToList();

            List<VIPManagementReportBE> lst = new List<VIPManagementReportBE>();

            //if (oVIPManagementReportBEList[e.Item.ItemIndex].SiteID == 1)
            //    lst = new List<VIPManagementReportBE>();

            //bool rec = uniqueSites[e.Item.ItemIndex + Convert.ToInt32(ViewState["SiteSkipCount"])].SiteID == i.SiteID;

            //if (true) {


            if (i.KeyValue1 != (double?)null)
                lst.Add(new VIPManagementReportBE() { KeyValue = GetCurrencyConversion(i.SiteID, Convert.ToDouble(i.KeyValue1)), SiteID = i.SiteID, KeyHeader = i.KeyHeader });
            else
                lst.Add(new VIPManagementReportBE() { KeyValue = -1.001, SiteID = i.SiteID, KeyHeader = i.KeyHeader });

            if (tblColSpan >= 2)
            {
                if (i.KeyValue2 != (double?)null)
                    lst.Add(new VIPManagementReportBE() { KeyValue = Convert.ToDouble(i.KeyValue2), SiteID = i.SiteID, KeyHeader = i.KeyHeader });
                else
                    lst.Add(new VIPManagementReportBE() { KeyValue = -1.001, SiteID = i.SiteID, KeyHeader = i.KeyHeader });
            }
            if (tblColSpan >= 3)
            {
                if (i.KeyValue3 != (double?)null)
                    lst.Add(new VIPManagementReportBE() { KeyValue = Convert.ToDouble(i.KeyValue3), SiteID = i.SiteID, KeyHeader = i.KeyHeader });
                else
                    lst.Add(new VIPManagementReportBE() { KeyValue = -1.001, SiteID = i.SiteID, KeyHeader = i.KeyHeader });
            }


            rptKeyValueDetails.DataSource = lst;
            rptKeyValueDetails.DataBind();

            //int rowWidth = 100 / lst.Count;
            for (int iRowCount = 0; iRowCount < lst.Count; iRowCount++)
            {
                var td = (HtmlTableCell)rptKeyValueDetails.Items[iRowCount].FindControl("tdControl");
                td.Style.Add("width", "42px");
            }
            //}
            //else {
            //    ViewState["SiteSkipCount"] = Convert.ToInt32(ViewState["SiteSkipCount"]) + 1;

            //    lst.Add(new VIPManagementReportBE() { KeyValue = (double?)null });

            //    lst.Add(new VIPManagementReportBE() { KeyValue = (double?)null });

            //    lst.Add(new VIPManagementReportBE() { KeyValue = (double?)null });

            //    rptKeyValueDetails.DataSource = lst;
            //    rptKeyValueDetails.DataBind();

            //    //int rowWidth = 100 / lst.Count;
            //    for (int iRowCount = 0; iRowCount < lst.Count; iRowCount++) {
            //        var td = (HtmlTableCell)rptKeyValueDetails.Items[iRowCount].FindControl("tdControl");
            //        td.Style.Add("width", "42px");
            //    }
            //}           
        }
    }

    private double? TotalItem = 0;
    private double? TotalAlternateItem = 0;
    protected void rptKeyValueDetails_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item)
        {

            Literal ltKeyvalue = (Literal)e.Item.FindControl("ltKeyvalue");



            if (ltKeyvalue.Text == "-1.001")
                ltKeyvalue.Text = string.Empty;
            if (ltOverViewType.Text == "OTIF Overview")
            {
                ltKeyvalue.Text = StringFormatTwoDecimal(ltKeyvalue.Text);
            }

            if (ViewState["ReportType"].ToString() == "SO" || ViewState["ReportType"].ToString() == "DS"
                || ViewState["ReportType"].ToString() == "SA" || ViewState["ReportType"].ToString() == "MA")
            {
                VIPManagementReportBE i = (VIPManagementReportBE)e.Item.DataItem;

                if (i.SiteID == 9999)
                {
                    ltKeyvalue.Text = TotalItem.ToString();
                    TotalItem = 0;
                }
                else
                {
                    if (i.KeyValue != -1.001)
                        TotalItem += i.KeyValue;
                }

                if (!string.IsNullOrWhiteSpace(ltKeyvalue.Text))
                    ltKeyvalue.Text = getCurrencyHTML(Convert.ToDecimal(ltKeyvalue.Text)).Replace(" ", "");  //ltKeyvalue.Text;
            }
        }
        else if (e.Item.ItemType == ListItemType.AlternatingItem)
        {

            Literal ltKeyvalue = (Literal)e.Item.FindControl("ltKeyvalue");

            if (ltKeyvalue.Text == "-1.001")
                ltKeyvalue.Text = string.Empty;
            if (ltOverViewType.Text == "OTIF Overview")
            {
                ltKeyvalue.Text = StringFormatTwoDecimal(ltKeyvalue.Text);
            }

            if (ViewState["ReportType"].ToString() == "SO" || ViewState["ReportType"].ToString() == "DS"
                 || ViewState["ReportType"].ToString() == "SA" || ViewState["ReportType"].ToString() == "MA")
            {
                VIPManagementReportBE i = (VIPManagementReportBE)e.Item.DataItem;
                if (i.SiteID == 9999)
                {
                    ltKeyvalue.Text = TotalAlternateItem.ToString();
                    TotalAlternateItem = 0;
                }
                else
                {
                    if (i.KeyValue != -1.001)
                        TotalAlternateItem += i.KeyValue;
                }
            }
        }
    }

    private string FormatPrice(decimal price, string cultureName)
    {
        string formatted = null;
        CultureInfo info = new CultureInfo(cultureName);
        switch (info.Name)
        {
            case "en-US":
                formatted = string.Format(CultureInfo.CreateSpecificCulture("en-US"),
                    "{0:C0}", price);
                break;
            case "en-GB":
                formatted = string.Format(CultureInfo.CreateSpecificCulture("en-GB"),
                    "{0:C0}", price);
                break;
            case "el-GR":
                formatted = string.Format(CultureInfo.CreateSpecificCulture("el-GR"),
                    "{0:C0}", price);
                break;
            case "cs-CZ":
                formatted = string.Format(CultureInfo.CreateSpecificCulture("cs-CZ"),
                    "{0:C0}", price);
                break;
        }

        return formatted;
    }

    private string getCurrencyHTML(decimal price)
    {
        string CurrencyHTML = string.Empty;

        if (CurrentCurrencyID == 1)
            CurrencyHTML = "€" + FormatPrice(price, "el-GR").Replace(",", "*").Replace(".", ",").Replace("*", ".").Replace("€", "");
        if (CurrentCurrencyID == 2)
            if (price < 0)
                CurrencyHTML = "£-" + FormatPrice(price * -1, "en-GB").Replace("£", "");
            else
                CurrencyHTML = FormatPrice(price, "en-GB");
        if (CurrentCurrencyID == 3)
            if (price < 0)
                CurrencyHTML = "$-" + FormatPrice(price * -1, "en-US").Replace("$", "");
            else
                CurrencyHTML = FormatPrice(price, "en-US");

        if (CurrentCurrencyID == 4)
            if (price < 0)
                CurrencyHTML = "Kč" + FormatPrice(price * -1, "cs-CZ").Replace("Kč", "");
            else
                CurrencyHTML = FormatPrice(price, "cs-CZ");
        return CurrencyHTML;
    }
    private void BindManagementReportExport(int Page = 1)
    {

        List<VIPManagementReportBE> oVIPManagementReportBEList = new List<VIPManagementReportBE>();

        VIPManagementReportBE oVIPManagementReportBE = new VIPManagementReportBE();
        VIPManagementReportBAL oVIPManagementReportBAL = new VIPManagementReportBAL();

        oVIPManagementReportBE.Action = "GetSpecificReportDetails";
        oVIPManagementReportBE.CalcultionDate = txtDate.GetDate.Date;

        if (ddlReportType.Visible)
            oVIPManagementReportBE.ReportType = Convert.ToString(ddlReportType.SelectedItem.Value);
        else
            oVIPManagementReportBE.ReportType = Convert.ToString(ViewState["ReportType"]);

        oVIPManagementReportBE.Page = Page;
        oVIPManagementReportBE.PageSize = Convert.ToInt32(ViewState["TotalRecords"]);

        oVIPManagementReportBEList = oVIPManagementReportBAL.GetVIPReportDetailsBAL(oVIPManagementReportBE);

        if (oVIPManagementReportBEList != null && oVIPManagementReportBEList.Count > 0)
        {
            ViewState["oVIPManagementReportBEList"] = oVIPManagementReportBEList;

            var uniqueSites = oVIPManagementReportBEList.Select(c => new { c.SiteID, c.SitePrefix, c.SiteCountryID }).Distinct().OrderBy(o => o.SiteCountryID).ThenBy(o => o.SitePrefix).ToList();
            rptSiteExport.DataSource = uniqueSites;


            var uniqueAreaKeys = oVIPManagementReportBEList.Select(c => new { c.CalcultionDate }).Distinct().OrderByDescending(o => o.CalcultionDate).ToList();
            rptkeyDetailsExport.DataSource = uniqueAreaKeys;
        }
        else
        {
            rptSiteExport.DataSource = null;
            rptkeyDetailsExport.DataSource = null;
        }

        rptSiteExport.DataBind();
        rptkeyDetailsExport.DataBind();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        BindManagementReportExport();

        Response.ClearContent();
        Response.Buffer = true;
        Response.ContentType = "application/force-download";
        Response.AddHeader("content-disposition", "attachment; filename=VIPMgmtReport.xls");
        Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
        Response.Write("<head>");
        Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
        Response.Write("<!--[if gte mso 9]><xml>");
        Response.Write("<x:ExcelWorkbook>");
        Response.Write("<x:ExcelWorksheets>");
        Response.Write("<x:ExcelWorksheet>");
        Response.Write("<x:Name>" + ltOverViewType.Text + "</x:Name>");
        Response.Write("<x:WorksheetOptions>");
        Response.Write("<x:Print>");
        Response.Write("<x:ValidPrinterInfo/>");
        Response.Write("</x:Print>");
        Response.Write("</x:WorksheetOptions>");
        Response.Write("</x:ExcelWorksheet>");
        Response.Write("</x:ExcelWorksheets>");
        Response.Write("</x:ExcelWorkbook>");
        Response.Write("</xml>");
        Response.Write("<![endif]--> ");
        StringWriter tw = new StringWriter(); HtmlTextWriter hw = new HtmlTextWriter(tw); tblExport.RenderControl(hw);
        Response.Write(tw.ToString());
        Response.Write("</head>");
        Response.End();

        //Response.ClearContent();
        //Response.Buffer = true;
        //Response.AddHeader("content-disposition", "attachment;filename=Details.xls");
        //Response.Charset = "";
        //Response.ContentType = "application/excel";
        //System.IO.StringWriter sw = new System.IO.StringWriter();
        //HtmlTextWriter htm = new HtmlTextWriter(sw);
        //tblExport.RenderControl(htm);
        //Response.Write(sw.ToString());
        //Response.End();       
    }

    private string StringFormatTwoDecimal(string inputString)
    {
        if (!string.IsNullOrEmpty(inputString))
        {
            string[] OT = inputString.Split('.');
            if (OT.Length == 1)
                inputString = inputString + ".00%";
            else if (OT.Length == 2)
                if (OT[1].Length == 1)
                    inputString = inputString + "0%";
                else if (OT[1].Length == 2)
                    inputString = inputString + "%";
        }
        return inputString;
    }

    private double? GetCurrencyConversion(int SiteID, double? StockValue)
    {

        if (!(ViewState["ReportType"].ToString() == "SO" || ViewState["ReportType"].ToString() == "DS"
             || ViewState["ReportType"].ToString() == "SA" || ViewState["ReportType"].ToString() == "MA"
            ))
            return StockValue;

        if (SiteID == 9999 || StockValue == 0)
            return StockValue;

        int SiteCurrencyID = 0;

        DataTable SiteCurrency = null;

        if (ViewState["SiteCurrency"] != null)
        {
            SiteCurrency = (DataTable)ViewState["SiteCurrency"];
        }
        else
        {
            MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
            MAS_SiteBAL oMAS_SiteBAL = new MAS_SiteBAL();

            oMAS_SiteBE.Action = "GetSiteCurrency";
            SiteCurrency = oMAS_SiteBAL.GetCountrySite(oMAS_SiteBE);
            ViewState["SiteCurrency"] = SiteCurrency;
        }

        DataView dv = SiteCurrency.DefaultView;
        dv.RowFilter = "SiteID = '" + SiteID.ToString() + "'";
        SiteCurrency = dv.ToTable();

        if (SiteCurrency.Rows.Count > 0)
        {
            foreach (DataRow dr in SiteCurrency.Rows)
            {
                SiteCurrencyID = Convert.ToInt32(dr["CurrencyID"]);
            }
        }


        CurrencyConverterBAL o_currencyBAL = new CurrencyConverterBAL();
        CurrencyConverterBE o_CurrencyBE = new CurrencyConverterBE();

        List<CurrencyConverterBE> lstCurrencyBE = new List<CurrencyConverterBE>();

        if (ViewState["lstCurrencyBE"] != null)
        {
            lstCurrencyBE = (List<CurrencyConverterBE>)ViewState["lstCurrencyBE"];
            if (lstCurrencyBE == null || lstCurrencyBE.Count == 0)
            {
                o_CurrencyBE.Action = "ShowAll";
                lstCurrencyBE = o_currencyBAL.BindCurrencyBAL(o_CurrencyBE);
                lstCurrencyBE = lstCurrencyBE.Where(cc => cc.IsPrevious == false).ToList();
                ViewState["lstCurrencyBE"] = lstCurrencyBE;
            }
        }
        else
        {
            o_CurrencyBE.Action = "ShowAll";
            lstCurrencyBE = o_currencyBAL.BindCurrencyBAL(o_CurrencyBE);
            lstCurrencyBE = lstCurrencyBE.Where(cc => cc.IsPrevious == false).ToList();
            ViewState["lstCurrencyBE"] = lstCurrencyBE;
        }

        if (SiteCurrency.Rows.Count > 0 && lstCurrencyBE.Count > 0 && SiteCurrencyID != CurrentCurrencyID)
        {

            lstCurrencyBE = lstCurrencyBE.Where(cc => cc.CurrencyID == SiteCurrencyID && cc.ConvertCurrencyID == CurrentCurrencyID).ToList();

            if (lstCurrencyBE.Count > 0)
            {
                return StockValue * lstCurrencyBE[0].ExchangeRate;
            }
            else
            {
                return 0; // StockValue;
            }
        }
        else
        {
            return StockValue;
        }
    }
}
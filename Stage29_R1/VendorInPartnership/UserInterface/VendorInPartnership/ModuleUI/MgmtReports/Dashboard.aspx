﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CommonUI/CMN_MasterPages/NoUser_MasterPage.Master"
    AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" %>

<%@ Register Assembly="BaseControlLibrary" Namespace="BaseControlLibrary" TagPrefix="cc1" %>
<%@ Register TagName="ucDate" TagPrefix="cc2" Src="~/CommonUI/UserControls/ucDate.ascx" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        document.onmousemove = positiontip;

        $(document).ready(function () {
            $("body").keypress(function (e) {
                if (e.which == 13 || e.keyCode == 13) {
                    $('#<%=btnGo.ClientID %>').click();
                    return false;
                }
            });
        });
    </script>
    <div id="dhtmltooltip" class="balloonstyle">
    </div>
    <iframe id="iframetop" scrolling="no" frameborder="0" class="iballoonstyle" width="0"
        height="0"></iframe>
    <h2>
        <cc1:ucLabel ID="lblVIPManagementReport" runat="server" Text="VIP Management Report"></cc1:ucLabel>
    </h2>
    <asp:ScriptManager ID="SM1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <div class="right-shadow">
                <div class="formbox">
                    <table width="90%" cellspacing="5" cellpadding="0" border="0" align="right" class="top-settingsnoborder">
                        <tr>
                            <td align="right">
                                <cc1:ucButton ID="btnExportToExcel" runat="server" OnClick="btnExport_Click" CssClass="exporttoexcel"
                                    Width="109px" Height="20px" />
                            </td>
                        </tr>
                    </table>
                    <table width="30%" cellspacing="5" cellpadding="0" border="0" align="center" class="top-settings">
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblDate" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td style="font-weight: bold;">
                                <cc2:ucDate ClientIDMode="Static" ID="txtDate" runat="server" />
                            </td>
                            <td>
                                <cc1:ucButton ID="btnGo" runat="server" CssClass="button" OnClick="btnGo_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                <cc1:ucLabel ID="lblViewvalueas" Text="View currency values in" runat="server"></cc1:ucLabel>
                            </td>
                            <td style="font-weight: bold;">
                                :
                            </td>
                            <td colspan="2" style="font-weight: bold;">
                                <cc1:ucImageButton ImageUrl="../../Images/euro.png" ID="imgEuro" runat="server" ToolTip="Euro"
                                    OnClick="imgEuro_Click" />&nbsp;
                                <cc1:ucImageButton ImageUrl="../../Images/pound.png" ID="imgPound" runat="server"
                                    ToolTip="UK Pound" OnClick="imgPound_Click" />&nbsp;
                                <cc1:ucImageButton ImageUrl="../../Images/CzechKoruna.png" ID="imgCzechkoruna" runat="server"
                                    ToolTip="Czech koruna" OnClick="imgCzechkoruna_Click" Height="16px" Width="16px"  />&nbsp;                                   
                                <cc1:ucImageButton ImageUrl="../../Images/Dollar.png" ID="imgDollar" runat="server"
                                    ToolTip="US Dollar" OnClick="imgDollar_Click" />&nbsp;
                            </td>
                        </tr>
                    </table>
                    <asp:Repeater ID="rptVIPMgmtReports" runat="server" OnItemDataBound="rptVIPMgmtReports_OnItemDataBound">
                        <ItemTemplate>
                            <table border="0" cellspacing="0" cellpadding="0" class="form-table" width="100%">
                                <tr>
                                    <td>
                                        <asp:Literal ID="ltArea" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Area") %>'></asp:Literal>
                                        <cc1:ucPanel runat="server" ID="ucPnl" GroupingText='<%# DataBinder.Eval(Container.DataItem, "Area") %>'
                                            CssClass="fieldset-form" style="position:relative;">
                                            
                                            <div style="position:absolute;top:6px;left:150px;">
                                            <a href="javascript:void(0)" rel='<%# DataBinder.Eval(Container.DataItem, "Area") %>'
                                                tipwidth="750" onmouseover="ddrivetip('<%# DataBinder.Eval(Container.DataItem, "Area") %>','#ededed','750');"
                                                onmouseout="hideddrivetip();">
                                                <img src="../../Images/info_button1.gif" align="absMiddle" border="0" /></a></div>

                                                <div class="balloonstyle" id='<%# DataBinder.Eval(Container.DataItem, "Area") %>'>
                                                <span class="bla8blue">
                                                    <asp:Literal ID="ltInfo" runat="server"></asp:Literal>
                                                </span>
                                            </div>
                                            <div style="width: 950px; overflow-x: scroll; overflow: auto;">
                                                <table style="min-width: 950px;" border="0" cellspacing="0" cellpadding="0" class="grid gvclass searchgrid-1">
                                                    <tr>
                                                        <th style="min-width: 100px; text-align: left;">
                                                            &nbsp;
                                                        </th>
                                                        <asp:Repeater ID="rptSite" runat="server">
                                                            <ItemTemplate>
                                                                <th style="width: 45px; text-align: center;">
                                                                    <asp:Literal ID="ltSitePreFix" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SitePrefix") %>'></asp:Literal>
                                                                    <asp:Literal ID="hdnSiteID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SiteID") %>'
                                                                        Visible="false" />
                                                                </th>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <th style="width: 55px; text-align: center;">
                                                            Total
                                                        </th>
                                                    </tr>
                                                    <asp:Repeater ID="rptkeyDetails" runat="server" OnItemDataBound="rptkeyDetails_OnItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="height: 25px;">
                                                                    <asp:HyperLink ID="hpLink" Target="_blank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AreaKey") %>'
                                                                        NavigateUrl='#'></asp:HyperLink>
                                                                </td>
                                                                <asp:Repeater ID="rptKeyvalue" runat="server" OnItemDataBound="rptKeyvalue_OnItemDataBound">
                                                                    <ItemTemplate>
                                                                        <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                                                                            <asp:Literal ID="hdnArea" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Area") %>'
                                                                                Visible="false" />
                                                                            <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                    <AlternatingItemTemplate>
                                                                        <td style="text-align: center;">
                                                                            <asp:Literal ID="hdnArea" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Area") %>'
                                                                                Visible="false" />
                                                                            <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                        </td>
                                                                    </AlternatingItemTemplate>
                                                                </asp:Repeater>
                                                                <td style="text-align: center;">
                                                                    <asp:Literal ID="ltSiteTotal" runat="server" Text=''></asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <%-- <AlternatingItemTemplate>
                                                <tr style="color:#333333;background-color:#F7F6F3;">
                                                    <td style="height: 25px;">
                                                        <asp:HyperLink ID="hpLink" Target="_blank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AreaKey") %>'
                                                            NavigateUrl='#'></asp:HyperLink>
                                                    </td>
                                                    <asp:Repeater ID="rptKeyvalue" runat="server" OnItemDataBound="rptKeyvalue_OnItemDataBound">
                                                        <ItemTemplate>
                                                            <td style="text-align: center;">
                                                                <asp:HiddenField ID="hdnArea" runat="server" Value='<%#  DataBinder.Eval(Container.DataItem, "Area") %>' />
                                                                <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                            </td>
                                                        </ItemTemplate>
                                                       
                                                    </asp:Repeater>
                                                    <td style="text-align: center;">
                                                        <asp:Literal ID="ltSiteTotal" runat="server" Text=''></asp:Literal>
                                                    </td>
                                                </tr>
                                            </AlternatingItemTemplate>--%>
                                                    </asp:Repeater>
                                                </table>
                                            </div>
                                        </cc1:ucPanel>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:Repeater>
                    <table border="1" cellspacing="0" cellpadding="0" class="form-table" id="tblExport"
                        runat="server" style="display: none;">
                        <tr>
                            <td>
                                <asp:Repeater ID="rptVIPMgmtReportsExport" runat="server" OnItemDataBound="rptVIPMgmtReports_OnItemDataBound">
                                    <ItemTemplate>
                                        <table border="1" cellspacing="0" cellpadding="0" class="form-table" width="100%">
                                            <tr>
                                                <th style="color: #333333; background-color: #F7F6F3; text-align: center;">
                                                    <asp:Literal ID="ltArea" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Area") %>'></asp:Literal>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="1" cellspacing="0" cellpadding="0" class="grid gvclass searchgrid-1">
                                                        <tr>
                                                            <th style="width: 180px; text-align: left; background-color: #EEE5D6;">
                                                                &nbsp;
                                                            </th>
                                                            <asp:Repeater ID="rptSite" runat="server">
                                                                <ItemTemplate>
                                                                    <th style="width: 55px; text-align: center; background-color: #EEE5D6;">
                                                                        <asp:Literal ID="ltSitePreFix" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SitePrefix") %>'></asp:Literal>
                                                                    </th>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                            <th style="width: 55px; text-align: center; background-color: #EEE5D6;">
                                                                Total
                                                            </th>
                                                        </tr>
                                                        <asp:Repeater ID="rptkeyDetails" runat="server" OnItemDataBound="rptkeyDetails_OnItemDataBound">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="height: 35px;">
                                                                        <asp:HyperLink ID="hpLink" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AreaKey") %>'></asp:HyperLink>
                                                                        <asp:Literal ID="ltLink" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "AreaKey") %>'></asp:Literal>
                                                                    </td>
                                                                    <asp:Repeater ID="rptKeyvalue" runat="server" OnItemDataBound="rptKeyvalue_OnItemDataBound">
                                                                        <ItemTemplate>
                                                                            <td style="color: #333333; background-color: #F7F6F3; text-align: center;">
                                                                                <asp:Literal ID="hdnArea" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Area") %>'
                                                                                    Visible="false" />
                                                                                <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                            </td>
                                                                        </ItemTemplate>
                                                                        <AlternatingItemTemplate>
                                                                            <td style="text-align: center;">
                                                                                <asp:Literal ID="hdnArea" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Area") %>'
                                                                                    Visible="false" />
                                                                                <asp:Literal ID="ltKeyvalue" runat="server" Text='<%#  DataBinder.Eval(Container.DataItem, "Keyvalue") %>'></asp:Literal>
                                                                            </td>
                                                                        </AlternatingItemTemplate>
                                                                    </asp:Repeater>
                                                                    <td style="text-align: center;">
                                                                        <asp:Literal ID="ltSiteTotal" runat="server" Text=''></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 35px;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="bottom-shadow">
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnGo" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

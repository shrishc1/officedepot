﻿using System;
using System.Web.UI.WebControls;

namespace BaseControlLibrary
{
    [Serializable]
    public class ucTextbox : TextBox
    {
        public ucTextbox()
        {
            this.CssClass = "inputbox";
            this.Attributes.Add("autocomplete", "off");
        }
    }
}

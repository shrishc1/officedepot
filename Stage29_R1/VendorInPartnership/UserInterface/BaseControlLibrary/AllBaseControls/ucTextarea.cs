﻿using System.Web.UI.WebControls;

namespace BaseControlLibrary
{
    public class ucTextarea : TextBox
    {
        public ucTextarea()
        {
            this.TextMode = TextBoxMode.MultiLine;

        }
    }
}

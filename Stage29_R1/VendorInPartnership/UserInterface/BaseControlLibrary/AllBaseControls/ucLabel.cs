﻿using System;
using System.Web.UI.WebControls;


namespace BaseControlLibrary
{
    public class ucLabel : Label
    {

        private bool _isRequired = false;
        private string _tag = "";

        public bool isRequired
        {
            get
            {
                return _isRequired;
            }
            set
            {
                _isRequired = value;
            }
        }

        public string Tag
        {
            get
            {
                return _tag;
            }
            set
            {
                if (!string.IsNullOrEmpty(WebLocalize.Resource.getGlobalResourceValue(value)))
                {
                    this.Text = (String)WebLocalize.Resource.getGlobalResourceValue(value);
                }
                _tag = value;
            }
        }

        public ucLabel()
        {

        }


    }
}

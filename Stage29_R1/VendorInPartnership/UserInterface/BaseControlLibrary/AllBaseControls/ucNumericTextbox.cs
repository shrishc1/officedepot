﻿using System.Web.UI.WebControls;

namespace BaseControlLibrary
{
    public class ucNumericTextbox : TextBox
    {
        public ucNumericTextbox()
        {
            this.CssClass = "inputbox";
            this.Attributes.Add("oncontextmenu", "return false;");
        }
    }
}

﻿using System;
using System.Web.UI.WebControls;

namespace BaseControlLibrary
{
    [Serializable]
    public class ucGridView : GridView
    {
        public ucGridView()
        {
            this.AutoGenerateColumns = false;
            this.GridLines = GridLines.None;
            this.CellSpacing = 0;
            this.CellPadding = 0;
            this.RowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#F7F6F3");
            this.RowStyle.ForeColor = System.Drawing.ColorTranslator.FromHtml("#333333");
            this.AlternatingRowStyle.ForeColor = System.Drawing.ColorTranslator.FromHtml("#284775");
            this.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
            this.EnableViewState = true;

        }
    }
}

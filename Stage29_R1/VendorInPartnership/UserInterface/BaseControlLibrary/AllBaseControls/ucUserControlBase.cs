﻿using System.Web.UI;

namespace BaseControlLibrary
{
    public class ucUserControlBase : UserControl
    {


        public virtual void SiteSelectedIndexChanged()
        {
            return;
        }

        public virtual bool CountryPrePage_Load()
        {
            return true;
        }

        public virtual void CountryPost_Load()
        {

        }

        public virtual void CountryCustomAction()
        {

        }

        public virtual bool SitePrePage_Load()
        {
            return true;
        }

        public virtual void SitePost_Load()
        {

        }

        public virtual bool PreVendorNo_Change()
        {
            return true;
        }

        public virtual void PostVendorNo_Change()
        {
        }

        public virtual bool PreDate_Change()
        {
            return true;
        }

        public virtual void PostDate_Change()
        {
        }


    }
}
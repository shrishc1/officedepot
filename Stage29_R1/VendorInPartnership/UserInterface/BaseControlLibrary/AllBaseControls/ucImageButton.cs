﻿using System;
using System.Collections.Specialized;
using System.Web.UI.WebControls;

namespace BaseControlLibrary
{
    public class ucImageButton : ImageButton
    {
        public ucImageButton()
        {

        }

        protected override bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            // Control coordinates are sent in decimal by IE10 
            // Recreating the collection with corrected values            
            NameValueCollection modifiedPostCollection = new NameValueCollection();
            for (int i = 0; i < postCollection.Count; i++)
            {
                string actualKey = postCollection.GetKey(i);
                if (actualKey != null)
                {
                    string[] actualValueTab = postCollection.GetValues(i);
                    if (actualKey.EndsWith(".x") || actualKey.EndsWith(".y"))
                    {
                        string value = actualValueTab[0];
                        decimal dec;
                        Decimal.TryParse(value, out dec);
                        modifiedPostCollection.Add(actualKey, ((int)Math.Round(dec)).ToString());
                    }
                    else
                    {
                        foreach (string actualValue in actualValueTab)
                        {
                            modifiedPostCollection.Add(actualKey, actualValue);
                        }
                    }
                }
            }
            return base.LoadPostData(postDataKey, modifiedPostCollection);
        }
    }
}
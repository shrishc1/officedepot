﻿using System.Configuration;

/*****************************************************************
 Purpose        :   To Create Connection String... its a singleton class
 *****************************************************************/

namespace DataAccessLayer
{
    public class DBConnection
    {
        private static string ConStr = ConfigurationManager.AppSettings["sConn"];

        private DBConnection() { }

        public static string Connection
        {
            get { return ConStr; }
        }
    }


}







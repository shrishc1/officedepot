﻿using BusinessEntities.ModuleBE.AdminFunctions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
    public class CommunicationLibraryDAL : DataAccessLayer.BaseDAL
    {
        public int? AddCommunicationLibDAL(CommunicationLibraryBE communicationLib)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", communicationLib.Action);
                param[index++] = new SqlParameter("@Subject", communicationLib.Subject);
                param[index++] = new SqlParameter("@Body", communicationLib.Body);
                param[index++] = new SqlParameter("@SentBy", communicationLib.SentBy);
                param[index++] = new SqlParameter("@Title", communicationLib.Title);
                param[index++] = new SqlParameter("@TypeofCommunication", communicationLib.TypeofCommunication);
                param[index++] = new SqlParameter("@UserIds", communicationLib.UserIds);
                param[index++] = new SqlParameter("@CreatedByID", communicationLib.CreatedByID);
                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<CommunicationLibraryBE> GetAllCommunicationLibDAL(CommunicationLibraryBE communicationLib)
        {
            var lstCommunicationLib = new List<CommunicationLibraryBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", communicationLib.Action);
                param[index++] = new SqlParameter("@DateFrom", communicationLib.DateFrom);
                param[index++] = new SqlParameter("@DateTo", communicationLib.DateTo);
                param[index++] = new SqlParameter("@CommunicationIds", communicationLib.CommunicationIds);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        var commLib = new CommunicationLibraryBE();
                        commLib.CommunicationID = dr["CommunicationID"] != DBNull.Value ? Convert.ToInt32(dr["CommunicationID"]) : (int?)null;
                        commLib.SentDate = dr["SentDate"] != DBNull.Value ? Convert.ToDateTime(dr["SentDate"]) : (DateTime?)null;
                        commLib.SentBy = dr["SentBy"] != DBNull.Value ? Convert.ToString(dr["SentBy"]) : string.Empty;
                        commLib.TypeofCommunication = dr["TypeofCommunication"] != DBNull.Value ? Convert.ToString(dr["TypeofCommunication"]) : string.Empty;
                        commLib.Subject = dr["Subject"] != DBNull.Value ? Convert.ToString(dr["Subject"]) : string.Empty;
                        commLib.Users = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                        commLib.CreatedByID = dr["CreatedByID"] != DBNull.Value ? Convert.ToInt32(dr["CreatedByID"]) : 0;
                        commLib.CreatedBy = dr["CreatedBy"] != DBNull.Value ? Convert.ToString(dr["CreatedBy"]) : string.Empty;
                        commLib.CreatedByEmail = dr["CreatedByEmail"] != DBNull.Value ? Convert.ToString(dr["CreatedByEmail"]) : string.Empty;
                        commLib.SentTo = dr["SentTo"] != DBNull.Value ? Convert.ToString(dr["SentTo"]) : string.Empty;
                        lstCommunicationLib.Add(commLib);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstCommunicationLib;
        }

        public List<CommunicationLibraryBE> GetEmailCommunicationDAL(CommunicationLibraryBE communicationLib)
        {
            var lstCommunicationLib = new List<CommunicationLibraryBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", communicationLib.Action);
                param[index++] = new SqlParameter("@CommunicationId", communicationLib.CommunicationID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        var commLib = new CommunicationLibraryBE();
                        commLib.CommunicationID = dr["CommunicationID"] != DBNull.Value ? Convert.ToInt32(dr["CommunicationID"]) : (int?)null;
                        commLib.Users = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                        commLib.Users.EmailId = dr["UserEmails"] != DBNull.Value ? Convert.ToString(dr["UserEmails"]) : string.Empty;
                        commLib.Body = dr["Body"] != DBNull.Value ? Convert.ToString(dr["Body"]) : string.Empty;
                        lstCommunicationLib.Add(commLib);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstCommunicationLib;
        }

        public DataSet GetStockPlanarDiscrepancyTrackerDAL(DiscrepancyNotification discrepancyNotification)
        {
            DataSet ds = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", discrepancyNotification.Action);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spDiscrepancyTrackerCommunication", param);

                return ds;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public DataSet GetStockPlanarDiscrepancyTrackerByTrackerIdDAL(DiscrepancyNotification discrepancyNotification)
        {
            DataSet ds = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", discrepancyNotification.Action);
                param[index++] = new SqlParameter("@DiscrepancyTrackerId", discrepancyNotification.DiscrepancyTrackerId);
                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spDiscrepancyTrackerCommunication", param);

                return ds;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public DataSet UpdateStockPlanarDiscrepancyTrackerByTrackerIdDAL(DiscrepancyNotification discrepancyNotification)
        {
            DataSet ds = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", discrepancyNotification.Action);
                param[index++] = new SqlParameter("@EmailSubject", discrepancyNotification.EmailSubject);
                param[index++] = new SqlParameter("@EmailContent", discrepancyNotification.EmailContent);
                param[index++] = new SqlParameter("@DiscrepancyTrackerId", discrepancyNotification.DiscrepancyTrackerId);
                param[index++] = new SqlParameter("@EditedBy", discrepancyNotification.EditedBy);
                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spDiscrepancyTrackerCommunication", param);

                return ds;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public string AddDiscrepancyTrackerEmailDAL(DiscrepancyEmailTracker discrepancyEmailTracker)
        {
            string reader = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", discrepancyEmailTracker.Action);
                param[index++] = new SqlParameter("@Type", discrepancyEmailTracker.EmailType);
                param[index++] = new SqlParameter("@Emailids", discrepancyEmailTracker.EmailID);
                param[index++] = new SqlParameter("@Userid", discrepancyEmailTracker.UserID);
                param[index++] = new SqlParameter("@PID", discrepancyEmailTracker.ID);

                reader = SqlHelper.ExecuteScalar(DBConnection.Connection, CommandType.StoredProcedure, "spDiscreapncyEscalationEmailRecipients", param).ToString();

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return reader;
        }

        public List<DiscrepancyEmailTracker> GetALLDiscrepancyTrackerEmailDAL(DiscrepancyEmailTracker discrepancyEmailTracker)
        {
            var lstCommunicationLib = new List<DiscrepancyEmailTracker>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", discrepancyEmailTracker.Action);
                param[index++] = new SqlParameter("@Userid", discrepancyEmailTracker.UserID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spDiscreapncyEscalationEmailRecipients", param);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        var commLib = new DiscrepancyEmailTracker();
                        commLib.ID = dr["ID"] != DBNull.Value ? Convert.ToInt32(dr["ID"]) : (int?)null;
                        commLib.EmailType = dr["Type"] != DBNull.Value ? Convert.ToString(dr["Type"]) : null;
                        commLib.EmailID = dr["EmailId"] != DBNull.Value ? Convert.ToString(dr["EmailId"]) : string.Empty;
                        commLib.IsActive = dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false;
                        lstCommunicationLib.Add(commLib);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstCommunicationLib;
        }

    }
}

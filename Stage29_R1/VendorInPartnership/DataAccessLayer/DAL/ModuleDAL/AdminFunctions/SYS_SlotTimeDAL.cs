﻿using BusinessEntities.ModuleBE.AdminFunctions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
    public class SYS_SlotTimeDAL
    {

        public List<SYS_SlotTimeBE> GetSlotTimeDAL(SYS_SlotTimeBE oSYS_SlotTimeBE)
        {
            DataTable dt = new DataTable();
            List<SYS_SlotTimeBE> oSYS_SlotTimeBEList = new List<SYS_SlotTimeBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oSYS_SlotTimeBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSYS_SlotTime", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    SYS_SlotTimeBE oNewSYS_SlotTimeBE = new SYS_SlotTimeBE();
                    oNewSYS_SlotTimeBE.SlotTimeID = Convert.ToInt32(dr["SlotTimeID"]);
                    oNewSYS_SlotTimeBE.SlotTime = dr["SlotTime"].ToString();
                    oNewSYS_SlotTimeBE.OrderBYID = Convert.ToInt32(dr["OrderBY"]);
                    oSYS_SlotTimeBEList.Add(oNewSYS_SlotTimeBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oSYS_SlotTimeBEList;
        }

        public List<SYS_SlotTimeBE> GetTimeDAL()
        {
            DataTable dt = new DataTable();
            List<SYS_SlotTimeBE> oSYS_SlotTimeBEList = new List<SYS_SlotTimeBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", "GetTime");

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSYS_SlotTime", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    SYS_SlotTimeBE oNewSYS_SlotTimeBE = new SYS_SlotTimeBE();
                    oNewSYS_SlotTimeBE.SlotTimeID = Convert.ToInt32(dr["SlotTimeID"]);
                    oNewSYS_SlotTimeBE.SlotTime = dr["SlotTime"].ToString();
                    oSYS_SlotTimeBEList.Add(oNewSYS_SlotTimeBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oSYS_SlotTimeBEList;
        }
    }
}

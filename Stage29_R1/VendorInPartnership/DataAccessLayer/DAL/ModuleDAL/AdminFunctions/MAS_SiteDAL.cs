﻿using BusinessEntities.ModuleBE.AdminFunctions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
    public class MAS_SiteDAL : DataAccessLayer.BaseDAL
    {
        public string GetSiteTimeSlotWindowFlagDAL(string SiteID)
        {

            string ReturnSiteTimeSlotWindowFlag = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", "GetSiteTimeSlotWindowFlag");
                param[index++] = new SqlParameter("@SiteID", SiteID);

                ReturnSiteTimeSlotWindowFlag = Convert.ToString(SqlHelper.ExecuteScalar(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Site", param));

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ReturnSiteTimeSlotWindowFlag;
        }

        public List<MAS_SiteBE> GetSiteDAL(MAS_SiteBE oMAS_SiteBE)
        {

            List<MAS_SiteBE> oMAS_SiteBEList = new List<MAS_SiteBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMAS_SiteBE.Action);
                param[index++] = new SqlParameter("@UserID", oMAS_SiteBE.User.UserID);
                param[index++] = new SqlParameter("@CountryID", oMAS_SiteBE.SiteCountryID);

                if (oMAS_SiteBE.SchedulingContact)
                    param[index++] = new SqlParameter("@SchedulingContact", "Y");
                else
                    param[index++] = new SqlParameter("@SchedulingContact", "N");

                if (oMAS_SiteBE.DiscrepancyContact)
                    param[index++] = new SqlParameter("@DiscrepancyContact", "Y");
                else
                    param[index++] = new SqlParameter("@DiscrepancyContact", "N");

                param[index++] = new SqlParameter("@TimeSlotWindow", oMAS_SiteBE.TimeSlotWindow);

                param[index++] = new SqlParameter("@IsHubRequired", oMAS_SiteBE.IsHubRequired);
                param[index++] = new SqlParameter("@SiteIds", oMAS_SiteBE.SiteIDs);
                DataTable dt = new DataTable();
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Site", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MAS_SiteBE oNewMAS_SiteBE = new MAS_SiteBE();
                    oNewMAS_SiteBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMAS_SiteBE.SitePrefix = dr["SitePrefix"].ToString();
                    oNewMAS_SiteBE.SiteName = dr["SiteName"].ToString();
                    oNewMAS_SiteBE.SiteDescription = dr["SiteDescription"].ToString();
                    oNewMAS_SiteBE.SiteCountryName = Convert.ToString(dr["SiteCountryName"]);
                    oNewMAS_SiteBE.SiteCountryID = Convert.ToInt32(dr["SiteCountryID"]);
                    oNewMAS_SiteBE.IsPreAdviseNoteRequired = dr["APP_IsPreAdviseNoteRequired"] != DBNull.Value ? Convert.ToBoolean(dr["APP_IsPreAdviseNoteRequired"]) : (bool?)null;
                    oNewMAS_SiteBE.NonTimeDeliveryCartonVolume = dr["APP_NonTimeDeliveryCartonVolume"] != DBNull.Value ? Convert.ToInt32(dr["APP_NonTimeDeliveryCartonVolume"]) : (int?)null;
                    oNewMAS_SiteBE.NonTimeDeliveryPalletVolume = dr["APP_NonTimeDeliveryPalletVolume"] != DBNull.Value ? Convert.ToInt32(dr["APP_NonTimeDeliveryPalletVolume"]) : (int?)null;
                    if (dr.Table.Columns.Contains("IsBookingExclusions"))
                        oNewMAS_SiteBE.IsBookingExclusions = dr["IsBookingExclusions"] != DBNull.Value ? Convert.ToBoolean(dr["IsBookingExclusions"]) : false;
                    if (dr.Table.Columns.Contains("IsEnableHazardouesItemPrompt"))
                        oNewMAS_SiteBE.IsEnableHazardouesItemPrompt = dr["IsEnableHazardouesItemPrompt"] != DBNull.Value ? Convert.ToBoolean(dr["IsEnableHazardouesItemPrompt"]) : false;
                    oMAS_SiteBEList.Add(oNewMAS_SiteBE);
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_SiteBEList;
        }

        public DataSet GetSiteForCountryDAL(MAS_SiteBE oMAS_SiteBE)
        {

            DataSet ds = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_SiteBE.Action);
                param[index++] = new SqlParameter("@SiteCountryID", oMAS_SiteBE.SiteCountryID);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        /// <summary>
        /// Call from Edit user to show those sites which are not selected by user
        /// </summary>
        /// <param name="oMAS_SiteBE"></param>
        /// <returns></returns>
        public List<MAS_SiteBE> GetSelectedSiteDAL(MAS_SiteBE oMAS_SiteBE)
        {

            List<MAS_SiteBE> oMAS_SiteBEList = new List<MAS_SiteBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oMAS_SiteBE.Action);
                param[index++] = new SqlParameter("@UserID", oMAS_SiteBE.User.UserID);// its a logeed user id
                param[index++] = new SqlParameter("@CountryID", oMAS_SiteBE.SiteCountryID);
                param[index++] = new SqlParameter("@EditUserID", oMAS_SiteBE.SiteMangerUserID.Value);//its a selected user id in edit mode

                DataTable dt = new DataTable();
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Site", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MAS_SiteBE oNewMAS_SiteBE = new MAS_SiteBE();
                    oNewMAS_SiteBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMAS_SiteBE.SiteDescription = dr["SiteDescription"].ToString();
                    oMAS_SiteBEList.Add(oNewMAS_SiteBE);
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_SiteBEList;
        }

        public List<MAS_SiteBE> GetSiteAddressForSiteIdDAL(MAS_SiteBE oMAS_SiteBE)
        {
            List<MAS_SiteBE> oMAS_SiteBEList = new List<MAS_SiteBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oMAS_SiteBE.Action);
                param[index++] = new SqlParameter("@SiteIds", oMAS_SiteBE.SiteNumberList);


                DataTable dt = new DataTable();
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Site", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MAS_SiteBE oNewMAS_SiteBE = new MAS_SiteBE();
                    oNewMAS_SiteBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMAS_SiteBE.SiteAddressLine1 = dr["SiteAddressLine1"].ToString();
                    oNewMAS_SiteBE.SiteAddressLine2 = dr["SiteAddressLine2"].ToString();
                    oNewMAS_SiteBE.SiteAddressLine3 = dr["SiteAddressLine3"].ToString();
                    oNewMAS_SiteBE.SiteAddressLine4 = dr["SiteAddressLine4"].ToString();
                    oNewMAS_SiteBE.SiteAddressLine5 = dr["SiteAddressLine5"].ToString();
                    oNewMAS_SiteBE.SiteAddressLine6 = dr["SiteAddressLine6"].ToString();
                    oNewMAS_SiteBE.SitePincode = dr["SitePincode"].ToString();

                    oNewMAS_SiteBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    oNewMAS_SiteBE.Country.CountryName = dr["CountryName"].ToString();


                    oMAS_SiteBEList.Add(oNewMAS_SiteBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_SiteBEList;
        }

        public List<MAS_SiteBE> GetSiteMisSettingDAL(MAS_SiteBE oMAS_SiteBE)
        {

            List<MAS_SiteBE> oMAS_SiteBEList = new List<MAS_SiteBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oMAS_SiteBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMAS_SiteBE.SiteID);
                param[index++] = new SqlParameter("@UserID", oMAS_SiteBE.User.UserID);

                DataTable dt = new DataTable();
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Site", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MAS_SiteBE oNewMAS_SiteBE = new MAS_SiteBE();
                    oNewMAS_SiteBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMAS_SiteBE.SiteName = dr["SiteName"].ToString();
                    oNewMAS_SiteBE.SitePrefix = dr["SitePrefix"].ToString();
                    oNewMAS_SiteBE.SiteNumber = dr["SiteNumber"].ToString();
                    oNewMAS_SiteBE.SiteDescription = dr["SiteDescription"].ToString();
                    oNewMAS_SiteBE.SiteAddressLine1 = dr["SiteAddressLine1"].ToString();
                    oNewMAS_SiteBE.SiteAddressLine2 = dr["SiteAddressLine2"].ToString();
                    oNewMAS_SiteBE.SiteAddressLine3 = dr["SiteAddressLine3"].ToString();
                    oNewMAS_SiteBE.SiteAddressLine4 = dr["SiteAddressLine4"].ToString();
                    oNewMAS_SiteBE.SiteAddressLine5 = dr["SiteAddressLine5"].ToString();
                    oNewMAS_SiteBE.SiteAddressLine6 = dr["SiteAddressLine6"].ToString();
                    oNewMAS_SiteBE.SiteCountryID = dr["SiteCountryID"] != DBNull.Value ? Convert.ToInt32(dr["SiteCountryID"]) : (int?)null;
                    oNewMAS_SiteBE.SitePincode = dr["SitePincode"].ToString();
                    oNewMAS_SiteBE.APAddressLine1 = dr["APAddressLine1"].ToString();
                    oNewMAS_SiteBE.APAddressLine2 = dr["APAddressLine2"].ToString();
                    oNewMAS_SiteBE.APAddressLine3 = dr["APAddressLine3"].ToString();
                    oNewMAS_SiteBE.APAddressLine4 = dr["APAddressLine4"].ToString();
                    oNewMAS_SiteBE.APAddressLine5 = dr["APAddressLine5"].ToString();
                    oNewMAS_SiteBE.APAddressLine6 = dr["APAddressLine6"].ToString();
                    oNewMAS_SiteBE.APCountryID = dr["APCountryID"] != DBNull.Value ? Convert.ToInt32(dr["APCountryID"]) : (int?)null;
                    oNewMAS_SiteBE.APPincode = dr["APPincode"].ToString();
                    oNewMAS_SiteBE.PhoneNumbers = dr["PhoneNumbers"].ToString();
                    oNewMAS_SiteBE.FaxNumber = dr["FaxNumber"].ToString();
                    oNewMAS_SiteBE.EmailID = dr["EmailID"].ToString();
                    oNewMAS_SiteBE.SiteManagerName = dr["ManagerName"].ToString();
                    oNewMAS_SiteBE.SiteMangerUserID = dr["SiteMangerUserID"] != DBNull.Value ? Convert.ToInt32(dr["SiteMangerUserID"].ToString()) : (int?)null;
                    oNewMAS_SiteBE.DIS_CommunicationWithEscalationType = dr["DIS_CommunicationWithEscalationType"] != DBNull.Value ? Convert.ToString(dr["DIS_CommunicationWithEscalationType"]) : string.Empty;
                    oNewMAS_SiteBE.DIS_DaysBeforeEscalationType2 = dr["DIS_DaysBeforeEscalationType2"] != DBNull.Value ? Convert.ToInt32(dr["DIS_DaysBeforeEscalationType2"]) : (int?)null;
                    oNewMAS_SiteBE.DIS_DaysBeforeFinalEscalation = dr["DIS_DaysBeforeFinalEscalation"] != DBNull.Value ? Convert.ToInt32(dr["DIS_DaysBeforeFinalEscalation"]) : (int?)null;
                    oNewMAS_SiteBE.DIS_DaysAfterGoodsReturned = dr["DIS_DaysAfterGoodsReturned"] != DBNull.Value ? Convert.ToInt32(dr["DIS_DaysAfterGoodsReturned"]) : (int?)null;
                    oNewMAS_SiteBE.NonTimeDeliveryPalletVolume = dr["APP_NonTimeDeliveryPalletVolume"] != DBNull.Value ? Convert.ToInt32(dr["APP_NonTimeDeliveryPalletVolume"]) : (int?)null;
                    oNewMAS_SiteBE.NonTimeDeliveryCartonVolume = dr["APP_NonTimeDeliveryCartonVolume"] != DBNull.Value ? Convert.ToInt32(dr["APP_NonTimeDeliveryCartonVolume"]) : (int?)null;
                    oNewMAS_SiteBE.IsPreAdviseNoteRequired = dr["APP_IsPreAdviseNoteRequired"] != DBNull.Value ? Convert.ToBoolean(dr["APP_IsPreAdviseNoteRequired"]) : (bool?)null;
                    oNewMAS_SiteBE.ToleranceDueDay = dr["APP_ToleranceDueDay"] != DBNull.Value ? Convert.ToInt32(dr["APP_ToleranceDueDay"]) : (int?)null;
                    oNewMAS_SiteBE.LinePerFTE = dr["APP_LinePerFTE"] != DBNull.Value ? Convert.ToDecimal(dr["APP_LinePerFTE"]) : (decimal?)null;
                    oNewMAS_SiteBE.AveragePalletUnloadedPerManHour = dr["APP_AveragePalletUnloadedPerManHour"] != DBNull.Value ? Convert.ToInt32(dr["APP_AveragePalletUnloadedPerManHour"]) : (int?)null;
                    oNewMAS_SiteBE.AverageCartonUnloadedPerManHour = dr["APP_AverageCartonUnloadedPerManHour"] != DBNull.Value ? Convert.ToInt32(dr["APP_AverageCartonUnloadedPerManHour"]) : (int?)null;
                    oNewMAS_SiteBE.AverageLinesReceiving = dr["APP_AverageLinesReceiving"] != DBNull.Value ? Convert.ToInt32(dr["APP_AverageLinesReceiving"]) : (int?)null;
                    oNewMAS_SiteBE.BookingNoticePeriodInDays = dr["APP_BookingNoticePeriodInDays"] != DBNull.Value ? Convert.ToInt32(dr["APP_BookingNoticePeriodInDays"]) : (int?)null;
                    oNewMAS_SiteBE.BookingNoticeTime = dr["APP_BookingNoticeTime"] != DBNull.Value ? Convert.ToDateTime(dr["APP_BookingNoticeTime"]) : (DateTime?)null;
                    oNewMAS_SiteBE.Remarks = dr["Remarks"].ToString();
                    oNewMAS_SiteBE.SiteNumberDescription = dr["SiteNumberDescription"].ToString();
                    oNewMAS_SiteBE.SiteNumberList = dr["SiteNumberList"].ToString();

                    oNewMAS_SiteBE.ByDatePO = dr["ByDatePO"] != DBNull.Value ? Convert.ToString(dr["ByDatePO"]) : string.Empty;
                    oNewMAS_SiteBE.PerBookingVendor = dr["PerBookingVendor"] != DBNull.Value ? Convert.ToString(dr["PerBookingVendor"]) : string.Empty;
                    oNewMAS_SiteBE.DeliveryTypeId = dr["DeliveryTypeId"] != DBNull.Value ? Convert.ToInt32(dr["DeliveryTypeId"]) : (int?)null;
                    //oNewMAS_SiteBE.DeliveryType = dr["DeliveryType"] != DBNull.Value ? Convert.ToString(dr["DeliveryType"]) : string.Empty;
                    oNewMAS_SiteBE.VehicleTypeId = dr["VehicleTypeId"] != DBNull.Value ? Convert.ToInt32(dr["VehicleTypeId"]) : (int?)null;
                    //oNewMAS_SiteBE.VehicleType = dr["VehicleType"] != DBNull.Value ? Convert.ToString(dr["VehicleType"]) : string.Empty;
                    oNewMAS_SiteBE.AlternateSlotOnOff = dr["AlternateSlotOnOff"] != DBNull.Value ? Convert.ToString(dr["AlternateSlotOnOff"]) : string.Empty;
                    oNewMAS_SiteBE.DeliveryArrived = dr["DeliveryArrived"] != DBNull.Value ? Convert.ToString(dr["DeliveryArrived"]) : string.Empty;
                    oNewMAS_SiteBE.DeliveryUnload = dr["DeliveryUnload"] != DBNull.Value ? Convert.ToString(dr["DeliveryUnload"]) : string.Empty;
                    oNewMAS_SiteBE.UnexpectedDelivery = dr["UnexpectedDelivery"] != DBNull.Value ? Convert.ToString(dr["UnexpectedDelivery"]) : string.Empty;
                    oNewMAS_SiteBE.TimeSlotWindow = dr["TimeSlotWindow"] != DBNull.Value ? Convert.ToString(dr["TimeSlotWindow"]) : string.Empty;

                    //----Stage 4 Point 13----//
                    if (dt.Columns.Contains("MaxCartonsPerDelivery"))
                        oNewMAS_SiteBE.MaxCartonsPerDelivery = dr["MaxCartonsPerDelivery"] != DBNull.Value ? Convert.ToInt32(dr["MaxCartonsPerDelivery"]) : (int?)null;
                    if (dt.Columns.Contains("MaxLinesPerDelivery"))
                        oNewMAS_SiteBE.MaxLinesPerDelivery = dr["MaxLinesPerDelivery"] != DBNull.Value ? Convert.ToInt32(dr["MaxLinesPerDelivery"]) : (int?)null;
                    if (dt.Columns.Contains("MaxPalletsPerDelivery"))
                        oNewMAS_SiteBE.MaxPalletsPerDelivery = dr["MaxPalletsPerDelivery"] != DBNull.Value ? Convert.ToInt32(dr["MaxPalletsPerDelivery"]) : (int?)null;
                    //------------------------//

                    //----Stage 5 Point 1----//
                    if (dt.Columns.Contains("IsBookingExclusions"))
                        oNewMAS_SiteBE.IsBookingExclusions = dr["IsBookingExclusions"] != DBNull.Value ? Convert.ToBoolean(dr["IsBookingExclusions"]) : false;
                    //------------------------//

                    //---Stage 7 Point 29-----//
                    if (dt.Columns.Contains("NonTimeEnd"))
                        oNewMAS_SiteBE.NonTimeEnd = dr["NonTimeEnd"] != DBNull.Value ? Convert.ToString(dr["NonTimeEnd"]) : string.Empty;
                    if (dt.Columns.Contains("NonTimeStart"))
                        oNewMAS_SiteBE.NonTimeStart = dr["NonTimeStart"] != DBNull.Value ? Convert.ToString(dr["NonTimeStart"]) : string.Empty;
                    //------------------------//

                    //---Stage 10 R1 Point 4.5-----//
                    if (dt.Columns.Contains("SlotWindow"))
                        oNewMAS_SiteBE.SlotWindow = dr["SlotWindow"] != DBNull.Value ? Convert.ToString(dr["SlotWindow"]) : string.Empty;

                    if (dt.Columns.Contains("AlternateSlot"))
                        oNewMAS_SiteBE.AlternateSlot = dr["AlternateSlot"] != DBNull.Value ? Convert.ToString(dr["AlternateSlot"]) : string.Empty;

                    if (dt.Columns.Contains("DeliveryType"))
                        oNewMAS_SiteBE.DeliveryType = dr["DeliveryType"] != DBNull.Value ? Convert.ToString(dr["DeliveryType"]) : string.Empty;

                    if (dt.Columns.Contains("VehicleType"))
                        oNewMAS_SiteBE.VehicleType = dr["VehicleType"] != DBNull.Value ? Convert.ToString(dr["VehicleType"]) : string.Empty;

                    if (dt.Columns.Contains("PreAdviceNote"))
                        oNewMAS_SiteBE.PreAdviceNote = dr["PreAdviceNote"] != DBNull.Value ? Convert.ToString(dr["PreAdviceNote"]) : string.Empty;

                    if (dt.Columns.Contains("BookingExclusion"))
                        oNewMAS_SiteBE.BookingExclusion = dr["BookingExclusion"] != DBNull.Value ? Convert.ToString(dr["BookingExclusion"]) : string.Empty;
                    //-----------------------------//

                    //---Stage 11 Point 17-----//
                    if (dt.Columns.Contains("CurrencyID"))
                        oNewMAS_SiteBE.CurrencyID = dr["CurrencyID"] != DBNull.Value ? Convert.ToInt32(dr["CurrencyID"]) : 0;
                    //------------------------//

                    //---Stage 14 R2 Point 28-----//
                    if (dt.Columns.Contains("APP_AveragePalletPutaway"))
                        oNewMAS_SiteBE.APP_AveragePalletPutaway = dr["APP_AveragePalletPutaway"] != DBNull.Value ? Convert.ToInt32(dr["APP_AveragePalletPutaway"]) : (int?)null;
                    //------------------------//
                    if (dt.Columns.Contains("IsODToReturn"))
                        oNewMAS_SiteBE.IsOdToReturn = dr["IsODToReturn"] != DBNull.Value ? Convert.ToBoolean(dr["IsODToReturn"]) : true;

                    if (dt.Columns.Contains("CostCenter"))
                        oNewMAS_SiteBE.CostCenter = dr["CostCenter"] != DBNull.Value ? Convert.ToString(dr["CostCenter"]) : string.Empty;

                    if (dt.Columns.Contains("WindowVolumePrcentageSetting"))
                        oNewMAS_SiteBE.WindowVolumePrcentageSetting = dr["WindowVolumePrcentageSetting"] != DBNull.Value ? Convert.ToInt32(dr["WindowVolumePrcentageSetting"]) : 0;

                    if (dt.Columns.Contains("officeHour"))
                        oNewMAS_SiteBE.OfficeHour = dr["officeHour"] != DBNull.Value ? Convert.ToString(dr["officeHour"]) : string.Empty;


                    oMAS_SiteBEList.Add(oNewMAS_SiteBE);
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_SiteBEList;
        }

        /// <summary>
        /// Used for managing Site Setings
        /// </summary>
        /// <param name="oMAS_SiteBE"></param>
        /// <returns></returns>
        public int? addEditSiteSettingsDAL(MAS_SiteBE oMAS_SiteBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[64];
                param[index++] = new SqlParameter("@Action", oMAS_SiteBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMAS_SiteBE.SiteID);
                param[index++] = new SqlParameter("@SiteName", oMAS_SiteBE.SiteName);
                param[index++] = new SqlParameter("@SitePrefix", oMAS_SiteBE.SitePrefix);
                param[index++] = new SqlParameter("@SiteNumber", oMAS_SiteBE.SiteNumber);
                param[index++] = new SqlParameter("@SiteAddressLine1", oMAS_SiteBE.SiteAddressLine1);
                param[index++] = new SqlParameter("@SiteAddressLine2", oMAS_SiteBE.SiteAddressLine2);
                param[index++] = new SqlParameter("@SiteAddressLine3", oMAS_SiteBE.SiteAddressLine3);
                param[index++] = new SqlParameter("@SiteAddressLine4", oMAS_SiteBE.SiteAddressLine4);
                param[index++] = new SqlParameter("@SiteAddressLine5", oMAS_SiteBE.SiteAddressLine5);
                param[index++] = new SqlParameter("@SiteAddressLine6", oMAS_SiteBE.SiteAddressLine6);
                param[index++] = new SqlParameter("@SiteCountryID", oMAS_SiteBE.SiteCountryID);
                param[index++] = new SqlParameter("@SitePincode", oMAS_SiteBE.SitePincode);
                param[index++] = new SqlParameter("@APAddressLine1", oMAS_SiteBE.APAddressLine1);
                param[index++] = new SqlParameter("@APAddressLine2", oMAS_SiteBE.APAddressLine2);
                param[index++] = new SqlParameter("@APAddressLine3", oMAS_SiteBE.APAddressLine3);
                param[index++] = new SqlParameter("@APAddressLine4", oMAS_SiteBE.APAddressLine4);
                param[index++] = new SqlParameter("@APAddressLine5", oMAS_SiteBE.APAddressLine5);
                param[index++] = new SqlParameter("@APAddressLine6", oMAS_SiteBE.APAddressLine6);
                param[index++] = new SqlParameter("@APPincode", oMAS_SiteBE.APPincode);
                param[index++] = new SqlParameter("@APCountryID", oMAS_SiteBE.APCountryID);
                param[index++] = new SqlParameter("@PhoneNumbers", oMAS_SiteBE.PhoneNumbers);
                param[index++] = new SqlParameter("@FaxNumber", oMAS_SiteBE.FaxNumber);
                param[index++] = new SqlParameter("@EmailID", oMAS_SiteBE.EmailID);
                param[index++] = new SqlParameter("@OfficeHour", oMAS_SiteBE.OfficeHour);
                param[index++] = new SqlParameter("@SiteMangerUserID", oMAS_SiteBE.SiteMangerUserID != null ? oMAS_SiteBE.SiteMangerUserID.Value : (int?)null);
                param[index++] = new SqlParameter("@DIS_CommunicationWithEscalationType", oMAS_SiteBE.DIS_CommunicationWithEscalationType);
                param[index++] = new SqlParameter("@DIS_DaysBeforeEscalationType2", oMAS_SiteBE.DIS_DaysBeforeEscalationType2);
                param[index++] = new SqlParameter("@DIS_DaysBeforeFinalEscalation", oMAS_SiteBE.DIS_DaysBeforeFinalEscalation);
                param[index++] = new SqlParameter("@DIS_DaysAfterGoodsReturned", oMAS_SiteBE.DIS_DaysAfterGoodsReturned);
                param[index++] = new SqlParameter("@APP_NonTimeDeliveryPalletVolume", oMAS_SiteBE.NonTimeDeliveryPalletVolume);
                param[index++] = new SqlParameter("@APP_NonTimeDeliveryCartonVolume", oMAS_SiteBE.NonTimeDeliveryCartonVolume);
                param[index++] = new SqlParameter("@APP_IsPreAdviseNoteRequired", oMAS_SiteBE.IsPreAdviseNoteRequired);
                param[index++] = new SqlParameter("@APP_ToleranceDueDay", oMAS_SiteBE.ToleranceDueDay);
                param[index++] = new SqlParameter("@APP_LinePerFTE", oMAS_SiteBE.LinePerFTE);
                param[index++] = new SqlParameter("@APP_AveragePalletUnloadedPerManHour", oMAS_SiteBE.AveragePalletUnloadedPerManHour);
                param[index++] = new SqlParameter("@APP_AverageCartonUnloadedPerManHour", oMAS_SiteBE.AverageCartonUnloadedPerManHour);
                param[index++] = new SqlParameter("@APP_AverageLinesReceiving", oMAS_SiteBE.AverageLinesReceiving);
                param[index++] = new SqlParameter("@APP_BookingNoticePeriodInDays", oMAS_SiteBE.BookingNoticePeriodInDays);
                param[index++] = new SqlParameter("@APP_BookingNoticeTime", oMAS_SiteBE.BookingNoticeTime);
                param[index++] = new SqlParameter("@Remarks", oMAS_SiteBE.Remarks);
                param[index++] = new SqlParameter("@IsActive", oMAS_SiteBE.IsActive);
                param[index++] = new SqlParameter("@SiteNumberList", oMAS_SiteBE.SiteNumberList);
                param[index++] = new SqlParameter("@UserID", oMAS_SiteBE.User.UserID);
                param[index++] = new SqlParameter("@ByDatePO", oMAS_SiteBE.ByDatePO);
                param[index++] = new SqlParameter("@PerBookingVendor", oMAS_SiteBE.PerBookingVendor);
                param[index++] = new SqlParameter("@DeliveryTypeId", oMAS_SiteBE.DeliveryTypeId);
                param[index++] = new SqlParameter("@VehicleTypeId", oMAS_SiteBE.VehicleTypeId);
                param[index++] = new SqlParameter("@AlternateSlotOnOff", oMAS_SiteBE.AlternateSlotOnOff);
                param[index++] = new SqlParameter("@DeliveryArrived", oMAS_SiteBE.DeliveryArrived);
                param[index++] = new SqlParameter("@DeliveryUnload", oMAS_SiteBE.DeliveryUnload);
                param[index++] = new SqlParameter("@UnexpectedDelivery", oMAS_SiteBE.UnexpectedDelivery);
                param[index++] = new SqlParameter("@TimeSlotWindow", oMAS_SiteBE.TimeSlotWindow);
                //----Stage 4 Point 13----//
                param[index++] = new SqlParameter("@MaxCartonsPerDelivery", oMAS_SiteBE.MaxCartonsPerDelivery);
                param[index++] = new SqlParameter("@MaxLinesPerDelivery", oMAS_SiteBE.MaxLinesPerDelivery);
                param[index++] = new SqlParameter("@MaxPalletsPerDelivery", oMAS_SiteBE.MaxPalletsPerDelivery);
                //------------------------//
                //---Stage 5 Point 1-----//
                param[index++] = new SqlParameter("@IsBookingExclusions", oMAS_SiteBE.IsBookingExclusions);
                //------------------------//

                //---Stage 7 Point 29-----//
                param[index++] = new SqlParameter("@NonTimeStart", oMAS_SiteBE.NonTimeStart);
                param[index++] = new SqlParameter("@NonTimeEnd", oMAS_SiteBE.NonTimeEnd);
                //------------------------//

                //---Stage 11 R3 Point 17---//
                param[index++] = new SqlParameter("@CurrencyID", oMAS_SiteBE.CurrencyID);
                //-------------------------//

                //---Stage 14 R2 Point 28---//
                param[index++] = new SqlParameter("@APP_AveragePalletPutaway", oMAS_SiteBE.APP_AveragePalletPutaway);
                //-------------------------//
                param[index++] = new SqlParameter("@IsODToReturn", oMAS_SiteBE.IsOdToReturn);

                param[index++] = new SqlParameter("@CostCenter", oMAS_SiteBE.CostCenter);

                param[index++] = new SqlParameter("@WindowVolumePrcentageSetting", oMAS_SiteBE.WindowVolumePrcentageSetting);


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_Site", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteSiteSettingsDAL(MAS_SiteBE oMAS_SiteBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_SiteBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMAS_SiteBE.SiteID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_Site", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataTable GetCountrySite(MAS_SiteBE oMAS_SiteBE)
        {
            DataSet ds = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_SiteBE.Action);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Site", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            if (ds != null)
            {
                return ds.Tables[0];
            }
            return (DataTable)null;
        }

        public List<MAS_SiteBE> GetSiteByIDDAL(MAS_SiteBE oMAS_SiteBE)
        {

            List<MAS_SiteBE> oMAS_SiteBEList = new List<MAS_SiteBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oMAS_SiteBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMAS_SiteBE.SiteID);

                DataTable dt = new DataTable();
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Site", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MAS_SiteBE oNewMAS_SiteBE = new MAS_SiteBE();

                    oNewMAS_SiteBE.LinePerFTE = dr["APP_LinePerFTE"] != DBNull.Value ? Convert.ToDecimal(dr["APP_LinePerFTE"]) : (decimal?)null;


                    oMAS_SiteBEList.Add(oNewMAS_SiteBE);
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_SiteBEList;
        }

        public List<MAS_SiteBE> GetAllSitesBasedCountryDAL(MAS_SiteBE mas_SiteBE)
        {
            List<MAS_SiteBE> lstUP_VendorBE = new List<MAS_SiteBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", mas_SiteBE.Action);
                param[index++] = new SqlParameter("@CountryID", mas_SiteBE.SiteCountryID);

                DataTable dt = new DataTable();
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Site", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MAS_SiteBE masNew_SiteBE = new MAS_SiteBE();
                    masNew_SiteBE.SiteName = Convert.ToString(dr["SiteName"]);
                    masNew_SiteBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    lstUP_VendorBE.Add(masNew_SiteBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstUP_VendorBE;
        }

        public MAS_SiteBE GetSingleSiteSettingDAL(MAS_SiteBE oMAS_SiteBE)
        {
            MAS_SiteBE masNew_SiteBE = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oMAS_SiteBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMAS_SiteBE.SiteID);

                DataTable dt = new DataTable();
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Site", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    masNew_SiteBE = new MAS_SiteBE();
                    masNew_SiteBE.ByDatePO = dr["ByDatePO"] != DBNull.Value ? Convert.ToString(dr["ByDatePO"]) : string.Empty;
                    masNew_SiteBE.PerBookingVendor = dr["PerBookingVendor"] != DBNull.Value ? Convert.ToString(dr["PerBookingVendor"]) : string.Empty;
                    masNew_SiteBE.DeliveryTypeId = dr["DeliveryTypeId"] != DBNull.Value ? Convert.ToInt32(dr["DeliveryTypeId"]) : (int?)null;
                    masNew_SiteBE.VehicleTypeId = dr["VehicleTypeId"] != DBNull.Value ? Convert.ToInt32(dr["VehicleTypeId"]) : (int?)null;
                    masNew_SiteBE.AlternateSlotOnOff = dr["AlternateSlotOnOff"] != DBNull.Value ? Convert.ToString(dr["AlternateSlotOnOff"]) : string.Empty;
                    masNew_SiteBE.DeliveryArrived = dr["DeliveryArrived"] != DBNull.Value ? Convert.ToString(dr["DeliveryArrived"]) : string.Empty;
                    masNew_SiteBE.DeliveryUnload = dr["DeliveryUnload"] != DBNull.Value ? Convert.ToString(dr["DeliveryUnload"]) : string.Empty;
                    masNew_SiteBE.UnexpectedDelivery = dr["UnexpectedDelivery"] != DBNull.Value ? Convert.ToString(dr["UnexpectedDelivery"]) : string.Empty;
                    masNew_SiteBE.SiteCountryID = dr["SiteCountryID"] != DBNull.Value ? Convert.ToInt32(dr["SiteCountryID"]) : (int?)null;
                    masNew_SiteBE.SiteName = dr["SiteName"] != DBNull.Value ? Convert.ToString(dr["SiteName"]) : string.Empty;
                    masNew_SiteBE.SitePrefix = dr["SitePrefix"] != DBNull.Value ? Convert.ToString(dr["SitePrefix"]) : string.Empty;
                    masNew_SiteBE.SiteNumber = dr["SiteNumber"] != DBNull.Value ? Convert.ToString(dr["SiteNumber"]) : string.Empty;
                    masNew_SiteBE.TimeSlotWindow = dr["TimeSlotWindow"] != DBNull.Value ? Convert.ToString(dr["TimeSlotWindow"]) : string.Empty;
                    masNew_SiteBE.Country = new MAS_CountryBE();
                    masNew_SiteBE.Country.CountryName = dr["CountryName"] != DBNull.Value ? Convert.ToString(dr["CountryName"]) : string.Empty;
                    //----Stage 4 Point 13----//
                    if (dt.Columns.Contains("MaxCartonsPerDelivery"))
                        masNew_SiteBE.MaxCartonsPerDelivery = dr["MaxCartonsPerDelivery"] != DBNull.Value ? Convert.ToInt32(dr["MaxCartonsPerDelivery"]) : 0;
                    if (dt.Columns.Contains("MaxLinesPerDelivery"))
                        masNew_SiteBE.MaxLinesPerDelivery = dr["MaxLinesPerDelivery"] != DBNull.Value ? Convert.ToInt32(dr["MaxLinesPerDelivery"]) : 0;
                    if (dt.Columns.Contains("MaxPalletsPerDelivery"))
                        masNew_SiteBE.MaxPalletsPerDelivery = dr["MaxPalletsPerDelivery"] != DBNull.Value ? Convert.ToInt32(dr["MaxPalletsPerDelivery"]) : 0;
                    //------------------------//

                    //---Stage 7 Point 29-----//
                    if (dt.Columns.Contains("NonTimeEnd"))
                        masNew_SiteBE.NonTimeEnd = dr["NonTimeEnd"] != DBNull.Value ? Convert.ToString(dr["NonTimeEnd"]) : string.Empty;
                    if (dt.Columns.Contains("NonTimeStart"))
                        masNew_SiteBE.NonTimeStart = dr["NonTimeStart"] != DBNull.Value ? Convert.ToString(dr["NonTimeStart"]) : string.Empty;
                    //------------------------//
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return masNew_SiteBE;
        }
    }
}




﻿using BusinessEntities.ModuleBE.AdminFunctions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
    public class MAS_TradeEntitiesDAL : DataAccessLayer.BaseDAL
    {
        public List<MAS_TradeEntitiesBE> GetTradeEntitiesDAL(MAS_TradeEntitiesBE oMAS_TradeEntitiesBE)
        {

            List<MAS_TradeEntitiesBE> oMAS_TradeEntitiesBEList = new List<MAS_TradeEntitiesBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_TradeEntitiesBE.Action);
                param[index++] = new SqlParameter("@TradeEntitiesID_int", oMAS_TradeEntitiesBE.TradeEntitiesID);

                DataTable dt = new DataTable();

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "usp_TradeEntities", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MAS_TradeEntitiesBE oNewMAS_TradeEntitiesBE = new MAS_TradeEntitiesBE();
                    oNewMAS_TradeEntitiesBE.RowNo = dr["RowNo"] != DBNull.Value ? Convert.ToInt32(dr["RowNo"]) : (int?)null;
                    oNewMAS_TradeEntitiesBE.TradeEntitiesID = dr["TradeEntitiesID"] != DBNull.Value ? Convert.ToInt32(dr["TradeEntitiesID"]) : (int?)null;
                    oNewMAS_TradeEntitiesBE.Area = dr["Area"] != DBNull.Value ? Convert.ToString(dr["Area"]) : null;
                    oNewMAS_TradeEntitiesBE.OD_Country = dr["Country"] != DBNull.Value ? Convert.ToString(dr["Country"]) : null;
                    oNewMAS_TradeEntitiesBE.SendPdfInvoicesTo = dr["SendPdfTo"] != DBNull.Value ? Convert.ToString(dr["SendPdfTo"]) : null;
                    oNewMAS_TradeEntitiesBE.Company = dr["Company"] != DBNull.Value ? Convert.ToString(dr["Company"]) : null;
                    oNewMAS_TradeEntitiesBE.InvoiceAddress = dr["Address"] != DBNull.Value ? Convert.ToString(dr["Address"]) : null;
                    oNewMAS_TradeEntitiesBE.VAT_No = dr["VAT_No"] != DBNull.Value ? Convert.ToString(dr["VAT_No"]) : null;
                    oNewMAS_TradeEntitiesBE.COC_No = dr["COC_No"] != DBNull.Value ? Convert.ToString(dr["COC_No"]) : null;
                    oNewMAS_TradeEntitiesBE.Type = dr["Type"] != DBNull.Value ? Convert.ToString(dr["Type"]) : null;
                    oNewMAS_TradeEntitiesBE.ExampleOrderNo = dr["Example_Order_No"] != DBNull.Value ? Convert.ToString(dr["Example_Order_No"]) : null;

                    oMAS_TradeEntitiesBEList.Add(oNewMAS_TradeEntitiesBE);
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_TradeEntitiesBEList;
        }

        public int? addEditTradeEntitiesDetailsDAL(MAS_TradeEntitiesBE oMAS_TradeEntitiesBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oMAS_TradeEntitiesBE.Action);
                param[index++] = new SqlParameter("@TradeEntitiesID_int", oMAS_TradeEntitiesBE.TradeEntitiesID);
                param[index++] = new SqlParameter("@Area_nvarchar", oMAS_TradeEntitiesBE.Area);
                param[index++] = new SqlParameter("@Country_nvarchar", oMAS_TradeEntitiesBE.OD_Country);
                param[index++] = new SqlParameter("@SendPdfTo_nvarchar", oMAS_TradeEntitiesBE.SendPdfInvoicesTo);
                param[index++] = new SqlParameter("@Company_nvarchar", oMAS_TradeEntitiesBE.Company);
                param[index++] = new SqlParameter("@Address_nvarchar", oMAS_TradeEntitiesBE.InvoiceAddress);
                param[index++] = new SqlParameter("@VAT_No_nvarchar", oMAS_TradeEntitiesBE.VAT_No);
                param[index++] = new SqlParameter("@COC_No_nvarchar", oMAS_TradeEntitiesBE.COC_No);
                param[index++] = new SqlParameter("@Type_nvarchar", oMAS_TradeEntitiesBE.Type);
                param[index++] = new SqlParameter("@Example_Order_No_nvarchar", oMAS_TradeEntitiesBE.ExampleOrderNo);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "usp_TradeEntities", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }



    }
}

﻿using BusinessEntities.ModuleBE.AdminFunctions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
    public class MAS_VatCodeDAL
    {
        /// <summary>
        /// Returns list of OD Vat
        /// </summary>
        /// <param name="MAS_VatCodeBE"></param>
        /// <returns></returns>
        public List<MAS_VatCodeBE> GetODVatCodeDAL(MAS_VatCodeBE MAS_VatCodeBE)
        {
            List<MAS_VatCodeBE> oSYS_MAS_VatCodeBEList = new List<MAS_VatCodeBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", MAS_VatCodeBE.Action);
                param[index++] = new SqlParameter("@ODVatCodeID", MAS_VatCodeBE.ODVatCodeID);
                param[index++] = new SqlParameter("@SiteId", MAS_VatCodeBE.SiteId);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVatCode", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MAS_VatCodeBE oMAS_VatCodeBE = new MAS_VatCodeBE();
                    oMAS_VatCodeBE.ODVatCodeID = Convert.ToInt32(dr["ODVatCodeID"]);
                    oMAS_VatCodeBE.SiteId = Convert.ToInt32(dr["SiteId"]);
                    oMAS_VatCodeBE.SiteName = dr["SiteName"].ToString();
                    oMAS_VatCodeBE.VatCode = dr["VatCode"].ToString();
                    oSYS_MAS_VatCodeBEList.Add(oMAS_VatCodeBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oSYS_MAS_VatCodeBEList;
        }


        /// <summary>
        /// Add OD VAT for Site
        /// </summary>
        /// <param name="MAS_VatCodeBE"></param>
        /// <returns></returns>
        public string AddODVatCodeDAL(MAS_VatCodeBE MAS_VatCodeBE)
        {
            string strResult = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", MAS_VatCodeBE.Action);
                param[index++] = new SqlParameter("@ODVatCodeID", MAS_VatCodeBE.ODVatCodeID);
                param[index++] = new SqlParameter("@SiteId", MAS_VatCodeBE.SiteId);
                param[index++] = new SqlParameter("@VatCode", MAS_VatCodeBE.VatCode);
                param[index++] = new SqlParameter("@IsActive", MAS_VatCodeBE.IsActive);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVatCode", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    strResult = Convert.ToString(Result.Tables[0].Rows[0][0]);
                else
                    strResult = string.Empty;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }


        public int? EditDeleteODVatCodeDAL(MAS_VatCodeBE MAS_VatCodeBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", MAS_VatCodeBE.Action);
                param[index++] = new SqlParameter("@ODVatCodeID", MAS_VatCodeBE.ODVatCodeID);
                param[index++] = new SqlParameter("@SiteId", MAS_VatCodeBE.SiteId);
                param[index++] = new SqlParameter("@VatCode", MAS_VatCodeBE.VatCode);
                param[index++] = new SqlParameter("@IsActive", MAS_VatCodeBE.IsActive);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVatCode", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Get list of all Vendors and their VAT Code
        /// </summary>
        /// <param name="MAS_VatCodeBE"></param>
        /// <returns></returns>
        public List<MAS_VatCodeBE> GetVendorVatCodeDAL(MAS_VatCodeBE MAS_VatCodeBE)
        {
            List<MAS_VatCodeBE> oSYS_MAS_VatCodeBEList = new List<MAS_VatCodeBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", MAS_VatCodeBE.Action);
                param[index++] = new SqlParameter("@VendorID", MAS_VatCodeBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SelectedCountryIDs", MAS_VatCodeBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", MAS_VatCodeBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@IsVendorVatCodeMaintained", MAS_VatCodeBE.IsVendorVatCodeMaintained);
                param[index++] = new SqlParameter("@GridCurrentPageNo", MAS_VatCodeBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", MAS_VatCodeBE.GridPageSize);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVatCode", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MAS_VatCodeBE oMAS_VatCodeBE = new MAS_VatCodeBE();

                    oMAS_VatCodeBE.Vendor = new BusinessEntities.ModuleBE.AdminFunctions.MAS_VendorBE();
                    oMAS_VatCodeBE.Vendor.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oMAS_VatCodeBE.Vendor.Vendor_Country = dr["Vendor_Country"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Country"]);
                    oMAS_VatCodeBE.Vendor.Vendor_No = dr["Vendor_No"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_No"]);
                    oMAS_VatCodeBE.Vendor.Vendor_Name = dr["Vendor_Name"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Name"]);
                    oMAS_VatCodeBE.Vendor.VatCode = dr["VatCode"] == DBNull.Value ? null : Convert.ToString(dr["VatCode"]);
                    oMAS_VatCodeBE.TotalRecords = dr["TotalRecords"] == DBNull.Value ? null : Convert.ToString(dr["TotalRecords"]);


                    oSYS_MAS_VatCodeBEList.Add(oMAS_VatCodeBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oSYS_MAS_VatCodeBEList;
        }

        public int? EditDeleteVendorVatCodeDAL(MAS_VatCodeBE MAS_VatCodeBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", MAS_VatCodeBE.Action);
                param[index++] = new SqlParameter("@VendorID", MAS_VatCodeBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@VatCode", MAS_VatCodeBE.Vendor.VatCode);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVatCode", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public MAS_VatCodeBE GetVendorVatCodeByVendorIdDAL(MAS_VatCodeBE MAS_VatCodeBE)
        {
            var oMAS_VatCodeBE = new MAS_VatCodeBE();
            try
            {
                var dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", MAS_VatCodeBE.Action);
                param[index++] = new SqlParameter("@VendorID", MAS_VatCodeBE.Vendor.VendorID);
                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVatCode", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    oMAS_VatCodeBE.Vendor = new BusinessEntities.ModuleBE.AdminFunctions.MAS_VendorBE();
                    oMAS_VatCodeBE.Vendor.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oMAS_VatCodeBE.Vendor.Vendor_Country = dr["Vendor_Country"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Country"]);
                    oMAS_VatCodeBE.Vendor.Vendor_No = dr["Vendor_No"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_No"]);
                    oMAS_VatCodeBE.Vendor.Vendor_Name = dr["Vendor_Name"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Name"]);
                    oMAS_VatCodeBE.Vendor.VatCode = dr["VatCode"] == DBNull.Value ? null : Convert.ToString(dr["VatCode"]);
                    oMAS_VatCodeBE.Vendor.NewCurrencyID = dr["NewCurrencyID"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["NewCurrencyID"]);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_VatCodeBE;
        }

    }
}

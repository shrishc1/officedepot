﻿using BusinessEntities.ModuleBE.AdminFunctions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
    public class CurrencyConverterDAL : DataAccessLayer.BaseDAL
    {

        CurrencyConverterBE o_CurrencyBE = new CurrencyConverterBE();
        public List<CurrencyConverterBE> BindCurrencyDAL(CurrencyConverterBE o_CurrencyBE)
        {
            List<CurrencyConverterBE> o_CurrencyBElist = new List<CurrencyConverterBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", o_CurrencyBE.Action);
                param[index++] = new SqlParameter("@CurrencyConverterID", o_CurrencyBE.CurrencyConverterID);
                param[index++] = new SqlParameter("@CurrencyID", o_CurrencyBE.CurrencyID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_CurrencyConvertor", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {

                    CurrencyConverterBE objCurrBE = new CurrencyConverterBE();
                    objCurrBE.CurrencyConverterID = Convert.ToInt32(dr["CurrencyConverterID"]);
                    objCurrBE.Currency = Convert.ToString(dr["Currency"]);
                    //objCurrBE.CurrencyID = Convert.ToInt32(dr["CurrencyID"]);
                    objCurrBE.CurrencyID = dr["CurrencyID"] != DBNull.Value ? Convert.ToInt32(dr["CurrencyID"]) : 0;
                    objCurrBE.CurrencyConvertTo = Convert.ToString(dr["CurrencyConvertTo"]);
                    objCurrBE.ConvertCurrencyID = Convert.ToInt32(dr["ConvertCurrencyID"]);
                    objCurrBE.ExchangeRate = Convert.ToDouble(dr["ExchangeRate"]);
                    objCurrBE.DateApplied = Convert.ToDateTime(dr["DateApplied"] != DBNull.Value ? Convert.ToDateTime(dr["DateApplied"]) : (DateTime?)null);
                    objCurrBE.IsPrevious = dr["IsPrevious"] != DBNull.Value ? Convert.ToBoolean(dr["IsPrevious"]) : false;
                    objCurrBE.UserName = Convert.ToString(dr["UserName"]);
                    o_CurrencyBElist.Add(objCurrBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return o_CurrencyBElist;
        }

        public List<CurrencyConverterBE> BindCurrencyddlDAL(CurrencyConverterBE o_CurrencyBE)
        {

            List<CurrencyConverterBE> o_CurrencyBElist = new List<CurrencyConverterBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", o_CurrencyBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_CurrencyConvertor", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    CurrencyConverterBE obj_CurrencyBE = new CurrencyConverterBE();
                    obj_CurrencyBE.Currency = Convert.ToString(dr["Currency"]);
                    obj_CurrencyBE.CurrencyID = dr["CurrencyID"] != DBNull.Value ? Convert.ToInt32(dr["CurrencyID"]) : 0;
                    o_CurrencyBElist.Add(obj_CurrencyBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return o_CurrencyBElist;
        }

        public int? CheckCurrencyDAL(CurrencyConverterBE o_CurrencyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", o_CurrencyBE.Action);
                param[index++] = new SqlParameter("@CurrencyID", o_CurrencyBE.CurrencyID);
                param[index++] = new SqlParameter("@ConvertCurrencyID", o_CurrencyBE.ConvertCurrencyID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_CurrencyConvertor", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return intResult;
        }

        public int? AddEditCurrencyConverterDAL(CurrencyConverterBE o_CurrencyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];

                param[index++] = new SqlParameter("@Action", o_CurrencyBE.Action);
                param[index++] = new SqlParameter("@CurrencyID", o_CurrencyBE.CurrencyID);
                param[index++] = new SqlParameter("@ConvertCurrencyID", o_CurrencyBE.ConvertCurrencyID);
                param[index++] = new SqlParameter("@ExchangeRate", o_CurrencyBE.ExchangeRate);
                param[index++] = new SqlParameter("@UserID", o_CurrencyBE.UserID);
                //param[index++] = new SqlParameter("@DateApplied", o_CurrencyBE.DateApplied);
                //param[index++] = new SqlParameter("@IsPrevious", o_CurrencyBE.IsPrevious);

                if (o_CurrencyBE.CurrencyConverterID != 0)
                {
                    param[index++] = new SqlParameter("@CurrencyConverterID", o_CurrencyBE.CurrencyConverterID);
                }

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_CurrencyConvertor", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}




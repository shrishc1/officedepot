﻿using System;
using System.Data;
using System.Data.SqlClient;
using Utilities;


using BusinessEntities.ModuleBE.AdminFunctions;
using System.Collections.Generic;

namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
   public class MAS_RegionDAL : DataAccessLayer.BaseDAL
    {

       public List<MAS_RegionBE> GetRegionsDAL(MAS_RegionBE oMas_RegionBE)
       {
           List<MAS_RegionBE> oMAS_RegionBEList = new List<MAS_RegionBE>();
           try
           {
               DataTable dt = new DataTable();
               int index = 0;
               SqlParameter[] param = new SqlParameter[1];
               param[index++] = new SqlParameter("@Action", oMas_RegionBE.Action);

               DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Region", param);

               dt = ds.Tables[0];

               foreach (DataRow dr in dt.Rows)
               {
                   MAS_RegionBE oNewMAS_RegionBE = new MAS_RegionBE();
                   oNewMAS_RegionBE.RegionID = Convert.ToInt32(dr["RegionID"]);
                   oNewMAS_RegionBE.RegionName = dr["RegionName"].ToString();

                   oMAS_RegionBEList.Add(oNewMAS_RegionBE);
               }
           }
           catch (Exception ex)
           {
               LogUtility.SaveLogEntry(ex.Message + " |      Error Source:- " + ex.Source);
           }
           return oMAS_RegionBEList;
       }


       public int? addEditRegionDAL(MAS_RegionBE oMas_RegionBE)
       {
           int? intResult = 0;
           try
           {
               int index = 0;
               SqlParameter[] param = new SqlParameter[4];
               param[index++] = new SqlParameter("@Action", oMas_RegionBE.Action);
               param[index++] = new SqlParameter("@RegionName", oMas_RegionBE.RegionName);
               param[index++] = new SqlParameter("@SelectedCountry", oMas_RegionBE.SelectedCountries);
               param[index++] = new SqlParameter("@RegionID", oMas_RegionBE.RegionID);

               DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_Region", param);

               if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                   intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
               else
                   intResult = 0;

           }
           catch (Exception ex)
           {
               LogUtility.SaveLogEntry(ex.Message + " |      Error Source:- " + ex.Source);
           }
           finally { }
           return intResult;
       }
    }
}

﻿using BusinessEntities.ModuleBE.AdminFunctions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
    public class MAS_CountryDAL : DataAccessLayer.BaseDAL
    {
        public List<MAS_CountryBE> GetCountryDAL(MAS_CountryBE oMAS_CountryBE)
        {

            List<MAS_CountryBE> oMAS_CountryBEList = new List<MAS_CountryBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_CountryBE.Action);
                param[index++] = new SqlParameter("@UserID", oMAS_CountryBE.User.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Country", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MAS_CountryBE oNewMAS_CountryBE = new MAS_CountryBE();
                    oNewMAS_CountryBE.CountryID = Convert.ToInt32(dr["CountryID"]);
                    oNewMAS_CountryBE.CountryName = dr["CountryName"].ToString();

                    oMAS_CountryBEList.Add(oNewMAS_CountryBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_CountryBEList;
        }

        public List<MAS_CountryBE> GetCountryISPM15DAL(MAS_CountryBE oMAS_CountryBE)
        {
            List<MAS_CountryBE> oMAS_CountryBEList = new List<MAS_CountryBE>();

            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oMAS_CountryBE.Action);
                param[index++] = new SqlParameter("@UserID", oMAS_CountryBE.User.UserID);
                param[index++] = new SqlParameter("@CountryName", oMAS_CountryBE.CountryName);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spCountryISPM15", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MAS_CountryBE oNewMAS_CountryBE = new MAS_CountryBE();
                    oNewMAS_CountryBE.CountryID = Convert.ToInt32(dr["CountryID"]);
                    oNewMAS_CountryBE.CountryName = dr["CountryName"].ToString();
                    oMAS_CountryBEList.Add(oNewMAS_CountryBE);
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_CountryBEList;
        }

        public  int InsertCountryISPM15DAL(MAS_CountryBE oMAS_CountryBE)
        {
            int intResult = 0;

            try
            { 
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oMAS_CountryBE.Action);
                param[index++] = new SqlParameter("@CountryName", oMAS_CountryBE.CountryName);
                param[index++] = new SqlParameter("@UserID", oMAS_CountryBE.User.UserID);
                param[index++] = new SqlParameter("@IsActive", oMAS_CountryBE.IsActive);
                param[index++] = new SqlParameter("@CountryID", oMAS_CountryBE.CountryID);
                DataSet dt = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spCountryISPM15", param);
            
                if (dt != null && dt.Tables.Count > 0 && dt.Tables[0].Rows.Count > 0 && dt.Tables[0].Rows[0][0].ToString() !="999")
                    intResult = Convert.ToInt32(dt.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return intResult;
        }
    }


}

﻿using System;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.AdminFunctions;
using System.Data.SqlClient;
using System.Data;
using Utilities;

namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
    public class SupplierGuidelineDAL
    {
        public DataTable GetSupplierGuideline(SupplierGuidelineBE oSupplierGuidelineBE)
        {
            DataTable dt=new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oSupplierGuidelineBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spDSH_SupplierGuideline", param);
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveLogEntry(ex.Message + " |      Error Source:- " + ex.Source);
            }
            return dt;

        }
    }
}

﻿using BusinessEntities.ModuleBE.AdminFunctions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;
namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
    public class MAS_VendorDAL : DataAccessLayer.BaseDAL
    {
        public List<MAS_VendorBE> GetVendorDAL(MAS_VendorBE oMAS_VendorBE)
        {

            List<MAS_VendorBE> oMAS_VendorBEList = new List<MAS_VendorBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", oMAS_VendorBE.Action);
                param[index++] = new SqlParameter("@CountryID", oMAS_VendorBE.CountryID);
                param[index++] = new SqlParameter("@RegistrationStatus", oMAS_VendorBE.RegistrationStatus);
                param[index++] = new SqlParameter("@VendorsMissingDetails", oMAS_VendorBE.VendorsMissingDetails);
                param[index++] = new SqlParameter("@SiteID", oMAS_VendorBE.SiteID);
                param[index++] = new SqlParameter("@IsActiveVendor", oMAS_VendorBE.IsActiveVendor); /// Added on 21 Feb 2013
                param[index++] = new SqlParameter("@VendorIds", oMAS_VendorBE.VendorIDs);
                param[index++] = new SqlParameter("@EuropeanorLocal", oMAS_VendorBE.EuropeanorLocal);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Vendor", param);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        MAS_VendorBE oNewMAS_VendorBE = new MAS_VendorBE();
                        oNewMAS_VendorBE.VendorID = dr["VendorID"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["VendorID"]);
                        oNewMAS_VendorBE.Vendor_No = dr["Vendor_No"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_No"]);
                        oNewMAS_VendorBE.Vendor_Name = dr["Vendor_Name"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Name"]);
                        oNewMAS_VendorBE.Vendor_Country = dr["Vendor_Country"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Country"]);
                        oNewMAS_VendorBE.NoOfSKU = dr["NoOfSKU"] == DBNull.Value ? null : Convert.ToString(dr["NoOfSKU"]);
                        oNewMAS_VendorBE.Vendor = Convert.ToString(dr["Vendor_No"]) + " - " + Convert.ToString(dr["Vendor_Name"]);
                        oNewMAS_VendorBE.RegistrationStatus = dr["RegistrationStatus"] == DBNull.Value ? null : Convert.ToString(dr["RegistrationStatus"]);
                        oNewMAS_VendorBE.ContactDefined = dr["ContactDefined"] == DBNull.Value ? null : Convert.ToString(dr["ContactDefined"]);
                        oNewMAS_VendorBE.VendorSite = dr["VendorsSite"] == DBNull.Value ? null : Convert.ToString(dr["VendorsSite"]);
                        oNewMAS_VendorBE.IsActiveVendor = dr["IsActiveVendor"] == DBNull.Value ? null : Convert.ToString(dr["IsActiveVendor"]); /// Added on 21 Feb 2013
                        //------Stage 7 Point 1------//
                        if (dr.Table.Columns.Contains("EuropeanorLocal"))
                            oNewMAS_VendorBE.EuropeanorLocal = dr["EuropeanorLocal"] == DBNull.Value ? 2 : Convert.ToInt32(dr["EuropeanorLocal"]);
                        //-------------------------------//

                        //------Stage 23 Point 41------//
                        if (dr.Table.Columns.Contains("ConsVendorVendor_No"))
                            oNewMAS_VendorBE.ConsVendorVendor_No = dr["ConsVendorVendor_No"] == DBNull.Value ? null : Convert.ToString(dr["ConsVendorVendor_No"]);

                        if (dr.Table.Columns.Contains("SchedulingContact"))
                            oNewMAS_VendorBE.SchedulingContact = dr["SchedulingContact"] == DBNull.Value ? null : Convert.ToString(dr["SchedulingContact"]);

                        if (dr.Table.Columns.Contains("DiscrepancyContact"))
                            oNewMAS_VendorBE.DiscrepancyContact = dr["DiscrepancyContact"] == DBNull.Value ? null : Convert.ToString(dr["DiscrepancyContact"]);

                        if (dr.Table.Columns.Contains("OTIFContact"))
                            oNewMAS_VendorBE.OTIFContact = dr["OTIFContact"] == DBNull.Value ? null : Convert.ToString(dr["OTIFContact"]);

                        if (dr.Table.Columns.Contains("ScorecardContact"))
                            oNewMAS_VendorBE.ScorecardContact = dr["ScorecardContact"] == DBNull.Value ? null : Convert.ToString(dr["ScorecardContact"]);

                        if (dr.Table.Columns.Contains("InvoiceIssueContact"))
                            oNewMAS_VendorBE.InvoiceIssueContact = dr["InvoiceIssueContact"] == DBNull.Value ? null : Convert.ToString(dr["InvoiceIssueContact"]);

                        if (dr.Table.Columns.Contains("InventoryPOC"))
                            oNewMAS_VendorBE.InventoryPOC = dr["InventoryPOC"] == DBNull.Value ? null : Convert.ToString(dr["InventoryPOC"]);

                        if (dr.Table.Columns.Contains("ProcurementPOC"))
                            oNewMAS_VendorBE.ProcurementPOC = dr["ProcurementPOC"] == DBNull.Value ? null : Convert.ToString(dr["ProcurementPOC"]);

                        if (dr.Table.Columns.Contains("MerchandisingPOC"))
                            oNewMAS_VendorBE.MerchandisingPOC = dr["MerchandisingPOC"] == DBNull.Value ? null : Convert.ToString(dr["MerchandisingPOC"]);

                        if (dr.Table.Columns.Contains("ExecutivePOC"))
                            oNewMAS_VendorBE.ExecutivePOC = dr["ExecutivePOC"] == DBNull.Value ? null : Convert.ToString(dr["ExecutivePOC"]);

                        if (dr.Table.Columns.Contains("StockPlannerName"))
                            oNewMAS_VendorBE.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);

                        if (dr.Table.Columns.Contains("AccounPlannerName"))
                            oNewMAS_VendorBE.AccounPlannerName = dr["AccounPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["AccounPlannerName"]);

                        if (dr.Table.Columns.Contains("IsVendorSubscribeToPenalty"))
                            oNewMAS_VendorBE.IsVendorSubscribeToPenalty = dr["IsVendorSubscribeToPenalty"] == DBNull.Value ? null : Convert.ToString(dr["IsVendorSubscribeToPenalty"]);


                        if (dr.Table.Columns.Contains("IsVendorOverstockAgreement"))
                            oNewMAS_VendorBE.IsVendorOverstockAgreement = dr["IsVendorOverstockAgreement"] == DBNull.Value ? null : Convert.ToString(dr["IsVendorOverstockAgreement"]);


                        if (dr.Table.Columns.Contains("VendorOverstockAgreementDetails"))
                            oNewMAS_VendorBE.VendorOverstockAgreementDetails = dr["VendorOverstockAgreementDetails"] == DBNull.Value ? null : Convert.ToString(dr["VendorOverstockAgreementDetails"]);


                        if (dr.Table.Columns.Contains("IsVendorReturnsAgreement"))
                            oNewMAS_VendorBE.IsVendorReturnsAgreement = dr["IsVendorReturnsAgreement"] == DBNull.Value ? null : Convert.ToString(dr["IsVendorReturnsAgreement"]);


                        if (dr.Table.Columns.Contains("VendorReturnsAgreementDetails"))
                            oNewMAS_VendorBE.VendorReturnsAgreementDetails = dr["VendorReturnsAgreementDetails"] == DBNull.Value ? null : Convert.ToString(dr["VendorReturnsAgreementDetails"]);

                        oMAS_VendorBEList.Add(oNewMAS_VendorBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_VendorBEList;
        }



        public MAS_VendorBE UpdateVendorBAL(MAS_VendorBE oMAS_VendorBE)
        {

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oMAS_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oMAS_VendorBE.VendorID);
                param[index++] = new SqlParameter("@IsActiveVendor", oMAS_VendorBE.IsActiveVendor); /// Added on 21 Feb 2013
                //------Stage 7 Point 1------//                                                                                     /// 
                param[index++] = new SqlParameter("@EuropeanorLocal", oMAS_VendorBE.EuropeanorLocal);
                //---------------------------// 

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Vendor", param);
                oMAS_VendorBE = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_VendorBE;
        }


        public int? UpdateOverstockReturnsDetailsDAL(MAS_VendorBE oMAS_VendorBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMAS_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oMAS_VendorBE.VendorID);
                param[index++] = new SqlParameter("@IsVendorOverstockAgreement", oMAS_VendorBE.IsVendorOverstockAgreement);
                param[index++] = new SqlParameter("@VendorOverstockAgreementDetails", oMAS_VendorBE.VendorOverstockAgreementDetails);
                param[index++] = new SqlParameter("@IsVendorReturnsAgreement", oMAS_VendorBE.IsVendorReturnsAgreement);
                param[index++] = new SqlParameter("@VendorReturnsAgreementDetails", oMAS_VendorBE.VendorReturnsAgreementDetails);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


        public List<MAS_VendorBE> GetOverstockReturnsDetailsDAL(MAS_VendorBE oMAS_VendorBE)
        {

            List<MAS_VendorBE> oMAS_VendorBEList = new List<MAS_VendorBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oMAS_VendorBE.VendorID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        MAS_VendorBE oNewMAS_VendorBE = new MAS_VendorBE();
                        oNewMAS_VendorBE.VendorID = dr["VendorID"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["VendorID"]);
                        oNewMAS_VendorBE.IsVendorOverstockAgreement = dr["IsVendorOverstockAgreement"] == DBNull.Value ? null : Convert.ToString(dr["IsVendorOverstockAgreement"]);
                        oNewMAS_VendorBE.VendorOverstockAgreementDetails = dr["VendorOverstockAgreementDetails"] == DBNull.Value ? null : Convert.ToString(dr["VendorOverstockAgreementDetails"]);
                        oNewMAS_VendorBE.IsVendorReturnsAgreement = dr["IsVendorReturnsAgreement"] == DBNull.Value ? null : Convert.ToString(dr["IsVendorReturnsAgreement"]);
                        oNewMAS_VendorBE.VendorReturnsAgreementDetails = dr["VendorReturnsAgreementDetails"] == DBNull.Value ? null : Convert.ToString(dr["VendorReturnsAgreementDetails"]);


                        oMAS_VendorBEList.Add(oNewMAS_VendorBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_VendorBEList;
        }

        public List<MAS_VendorBE> GetSiteSchedulingDataDAL(MAS_VendorBE oMAS_VendorBE)
        {

            List<MAS_VendorBE> oMAS_VendorBEList = new List<MAS_VendorBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oMAS_VendorBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Vendor", param);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        MAS_VendorBE oNewMAS_VendorBE = new MAS_VendorBE();
                        oNewMAS_VendorBE.CountryID = dr["CountryID"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["CountryID"]);
                        oNewMAS_VendorBE.CountryName = dr["CountryName"] == DBNull.Value ? null : Convert.ToString(dr["CountryName"]);
                        oNewMAS_VendorBE.SiteID = dr["SiteID"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["SiteID"]);
                        oNewMAS_VendorBE.SiteName = dr["SiteName"] == DBNull.Value ? null : Convert.ToString(dr["SiteName"]);
                        oNewMAS_VendorBE.IsSiteSchedulingEnabled = dr["IsSiteSchedulingEnabled"] == DBNull.Value ? false : Convert.ToBoolean(dr["IsSiteSchedulingEnabled"]);
                        oNewMAS_VendorBE.IsEnableHazardouesItemPrompt = dr["IsEnableHazardouesItemPrompt"] == DBNull.Value ? false : Convert.ToBoolean(dr["IsEnableHazardouesItemPrompt"]);
                        oMAS_VendorBEList.Add(oNewMAS_VendorBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_VendorBEList;
        }

        public int? UpdateSiteSchedulingSettingsDAL(MAS_VendorBE oMAS_VendorBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oMAS_VendorBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMAS_VendorBE.SiteID);
                param[index++] = new SqlParameter("@IsSiteSchedulingEnabled", oMAS_VendorBE.IsSiteSchedulingEnabled);
                param[index++] = new SqlParameter("@IsEnableHazardouesItemPrompt", oMAS_VendorBE.IsEnableHazardouesItemPrompt);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_Vendor", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MAS_VendorBE> GetOverstockReturnsAgreementOverviewDAL(MAS_VendorBE oMAS_VendorBE)
        {

            List<MAS_VendorBE> oMAS_VendorBEList = new List<MAS_VendorBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", oMAS_VendorBE.Action);
                param[index++] = new SqlParameter("@CountryID", oMAS_VendorBE.CountryID);
                param[index++] = new SqlParameter("@VendorIds", oMAS_VendorBE.VendorIDs);
                param[index++] = new SqlParameter("@AgreementType", oMAS_VendorBE.AgreementType);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Vendor", param);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        MAS_VendorBE oNewMAS_VendorBE = new MAS_VendorBE();
                        oNewMAS_VendorBE.VendorID = dr["VendorID"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["VendorID"]);
                        oNewMAS_VendorBE.Vendor_No = dr["Vendor_No"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_No"]);
                        oNewMAS_VendorBE.Vendor_Name = dr["Vendor_Name"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Name"]);
                        oNewMAS_VendorBE.Vendor_Country = dr["Vendor_Country"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Country"]);
                        oNewMAS_VendorBE.Vendor = Convert.ToString(dr["Vendor_No"]) + " - " + Convert.ToString(dr["Vendor_Name"]);
                        oNewMAS_VendorBE.CountryID = dr["CountryID"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["CountryID"]);

                        if (dr.Table.Columns.Contains("IsVendorOverstockAgreement"))
                            oNewMAS_VendorBE.IsVendorOverstockAgreement = dr["IsVendorOverstockAgreement"] == DBNull.Value ? "N" : Convert.ToString(dr["IsVendorOverstockAgreement"]);

                        if (dr.Table.Columns.Contains("VendorOverstockAgreementDetails"))
                            oNewMAS_VendorBE.VendorOverstockAgreementDetails = dr["VendorOverstockAgreementDetails"] == DBNull.Value ? null : Convert.ToString(dr["VendorOverstockAgreementDetails"]);

                        if (dr.Table.Columns.Contains("IsVendorReturnsAgreement"))
                            oNewMAS_VendorBE.IsVendorReturnsAgreement = dr["IsVendorReturnsAgreement"] == DBNull.Value ? "N" : Convert.ToString(dr["IsVendorReturnsAgreement"]);


                        if (dr.Table.Columns.Contains("VendorReturnsAgreementDetails"))
                            oNewMAS_VendorBE.VendorReturnsAgreementDetails = dr["VendorReturnsAgreementDetails"] == DBNull.Value ? null : Convert.ToString(dr["VendorReturnsAgreementDetails"]);

                        if (dr.Table.Columns.Contains("Lines"))
                            oNewMAS_VendorBE.Lines = dr["Lines"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Lines"]);

                        if (dr.Table.Columns.Contains("Weightage"))
                            oNewMAS_VendorBE.Weightage = dr["Weightage"] == DBNull.Value ? null : Convert.ToString(dr["Weightage"]);

                        oMAS_VendorBEList.Add(oNewMAS_VendorBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_VendorBEList;
        }


        public DataSet GetSiteSchedulingSettingsForHazardousDAL(MAS_VendorBE oMAS_VendorBE)
        {
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_VendorBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMAS_VendorBE.SiteID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_Vendor", param);

                return Result;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                return null;
            }
            finally { }

        }

    }
}

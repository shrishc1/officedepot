﻿using BusinessEntities.ModuleBE.OnTimeInFull;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.OnTimeInFull
{
    public class ApplyOTIFExclusionDAL : BaseDAL
    {
        public int? addEditUP_ApplyOTIFExclusionDAL(ApplyOTIFExclusionBE applyOTIFExclusionBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", applyOTIFExclusionBE.Action);
                param[index++] = new SqlParameter("@ExclusionID", applyOTIFExclusionBE.ApplyExclusionID);
                param[index++] = new SqlParameter("@SiteID", applyOTIFExclusionBE.SiteID);
                param[index++] = new SqlParameter("@UserName", applyOTIFExclusionBE.UserName);
                param[index++] = new SqlParameter("@Reason", applyOTIFExclusionBE.Reason);
                param[index++] = new SqlParameter("@SelectedProductCodes", applyOTIFExclusionBE.SelectedProductCodes);
                param[index++] = new SqlParameter("@PurchaseOrderID", applyOTIFExclusionBE.Purchase_order);
                param[index++] = new SqlParameter("@Order_raised", applyOTIFExclusionBE.Order_raised);
                param[index++] = new SqlParameter("@VendorID", applyOTIFExclusionBE.VendorID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<ApplyOTIFExclusionBE> GetApplySKUExclusionDAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            List<ApplyOTIFExclusionBE> Up_PurchaseOrderDetailBEList = new List<ApplyOTIFExclusionBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[14];

                param[index++] = new SqlParameter("@Action", oApplyOTIFExclusionBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrderID", oApplyOTIFExclusionBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@Purchase_order", oApplyOTIFExclusionBE.Purchase_order);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oApplyOTIFExclusionBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedSiteID", oApplyOTIFExclusionBE.SiteID);
                param[index++] = new SqlParameter("@Order_raised", oApplyOTIFExclusionBE.Order_raised);
                param[index++] = new SqlParameter("@GridCurrentPageNo", oApplyOTIFExclusionBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", oApplyOTIFExclusionBE.GridPageSize);
                param[index++] = new SqlParameter("@DateFrom", oApplyOTIFExclusionBE.DateFrom);
                param[index++] = new SqlParameter("@Dateto", oApplyOTIFExclusionBE.DateTo);
                param[index++] = new SqlParameter("@IsDateCheck", oApplyOTIFExclusionBE.IsDateCheck);
                param[index++] = new SqlParameter("@IsFromExcludedPoPage", oApplyOTIFExclusionBE.IsFromExcludedPoPage);

                if (oApplyOTIFExclusionBE.SKU != null)
                {
                    param[index++] = new SqlParameter("@SKU", oApplyOTIFExclusionBE.SKU.OD_SKU_NO);
                }
                if (oApplyOTIFExclusionBE.SKU != null)
                {
                    param[index++] = new SqlParameter("@Viking", oApplyOTIFExclusionBE.SKU.Direct_SKU);
                }
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    ApplyOTIFExclusionBE oNewApplyOTIFExclusionBE = new ApplyOTIFExclusionBE();
                    oNewApplyOTIFExclusionBE.ApplyExclusionID = Convert.ToInt32(dr["ApplyExclusionID"]);
                    oNewApplyOTIFExclusionBE.PurchaseOrderID = Convert.ToInt32(dr["ApplyExclusionID"]);
                    oNewApplyOTIFExclusionBE.Purchase_order = dr["PurchaseNumber"].ToString();
                    oNewApplyOTIFExclusionBE.DateApplied = dr["DateApplied"] != DBNull.Value ? Convert.ToDateTime(dr["DateApplied"]) : (DateTime?)null;
                    oNewApplyOTIFExclusionBE.ProductDescription = dr["ProductDescription"].ToString();
                    oNewApplyOTIFExclusionBE.CountryName = dr["CountryName"].ToString();
                    oNewApplyOTIFExclusionBE.SiteName = dr["SiteName"].ToString();
                    oNewApplyOTIFExclusionBE.UserName = dr["UserName"].ToString();
                    oNewApplyOTIFExclusionBE.Reason = dr["Reason"].ToString();
                    oNewApplyOTIFExclusionBE.VendorName = dr["VendorName"].ToString();
                    oNewApplyOTIFExclusionBE.Order_raised = dr["OrderRaised"] != DBNull.Value ? Convert.ToDateTime(dr["OrderRaised"]) : (DateTime?)null;
                    if (dr.Table.Columns.Contains("DATE"))
                        oNewApplyOTIFExclusionBE.OriginalDueDate = dr["DATE"].ToString();

                    if (dr.Table.Columns.Contains("TotalRecords"))
                        oNewApplyOTIFExclusionBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;

                    if (dr.Table.Columns.Contains("Warehouse"))
                        oNewApplyOTIFExclusionBE.Warehouse = dr["Warehouse"] != DBNull.Value ? Convert.ToString(dr["Warehouse"]) : string.Empty;

                    if (dr.Table.Columns.Contains("SiteID"))
                        oNewApplyOTIFExclusionBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;

                    if (dr.Table.Columns.Contains("VendorID"))
                        oNewApplyOTIFExclusionBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;

                    if (dr.Table.Columns.Contains("WhoAppliedTheOtifExclusion"))
                        oNewApplyOTIFExclusionBE.WhoAppliedTheOtifExclusion = dr["WhoAppliedTheOtifExclusion"] != DBNull.Value ? Convert.ToString(dr["WhoAppliedTheOtifExclusion"]) : string.Empty;

                    Up_PurchaseOrderDetailBEList.Add(oNewApplyOTIFExclusionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<ApplyOTIFExclusionBE> GetSelectedApplySKUExclusionDAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            List<ApplyOTIFExclusionBE> Up_PurchaseOrderDetailBEList = new List<ApplyOTIFExclusionBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oApplyOTIFExclusionBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrderID", oApplyOTIFExclusionBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oApplyOTIFExclusionBE.SelectedVendorIDs);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    ApplyOTIFExclusionBE oNewOTIF_LeadTimeSkuBEList = new ApplyOTIFExclusionBE();
                    oNewOTIF_LeadTimeSkuBEList.ApplyExclusionID = Convert.ToInt32(dr["ApplyExclusionID"]);
                    oNewOTIF_LeadTimeSkuBEList.PurchaseOrderID = Convert.ToInt32(dr["ApplyExclusionID"]);
                    oNewOTIF_LeadTimeSkuBEList.Purchase_order = dr["PurchaseNumber"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.DateApplied = dr["DateApplied"] != DBNull.Value ? Convert.ToDateTime(dr["DateApplied"]) : (DateTime?)null;
                    oNewOTIF_LeadTimeSkuBEList.ProductDescription = dr["ProductDescription"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.CountryName = dr["CountryName"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.SiteName = dr["SiteName"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.UserName = dr["UserName"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.Reason = dr["Reason"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.VendorName = dr["VendorName"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.Order_raised = dr["OrderRaised"] != DBNull.Value ? Convert.ToDateTime(dr["OrderRaised"]) : (DateTime?)null;
                    Up_PurchaseOrderDetailBEList.Add(oNewOTIF_LeadTimeSkuBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<ApplyOTIFExclusionBE> GetSelectedApplySKUCodesDAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            List<ApplyOTIFExclusionBE> Up_PurchaseOrderDetailBEList = new List<ApplyOTIFExclusionBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oApplyOTIFExclusionBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oApplyOTIFExclusionBE.Purchase_order);
                //param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site );
                param[index++] = new SqlParameter("@Order_raised", oApplyOTIFExclusionBE.Order_raised);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    ApplyOTIFExclusionBE oNewApplyOTIFExclusionBE = new ApplyOTIFExclusionBE();
                    oNewApplyOTIFExclusionBE.SKUID = Convert.ToInt32(dr["SKUID"]);
                    oNewApplyOTIFExclusionBE.Purchase_order = dr["PurchaseNumber"].ToString();
                    // oNewApplyOTIFExclusionBE.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevisedDueDate"]) : (DateTime?)null;
                    oNewApplyOTIFExclusionBE.ProductDescription = dr["ProductDescription"].ToString();

                    Up_PurchaseOrderDetailBEList.Add(oNewApplyOTIFExclusionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public bool CheckApplyExclusionExistanceDAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            bool IsExclusionExist = false;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oApplyOTIFExclusionBE.Action);
                param[index++] = new SqlParameter("@ExclusionID", oApplyOTIFExclusionBE.ApplyExclusionID);
                param[index++] = new SqlParameter("@SelectedExcludedPurchaseNo", oApplyOTIFExclusionBE.Purchase_order);
                param[index++] = new SqlParameter("@SelectedExcludedSiteId", oApplyOTIFExclusionBE.SiteID);

                DataSet dsIsExclusionExist = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_VendorReportingExclusion", param);
                if (dsIsExclusionExist != null && dsIsExclusionExist.Tables.Count > 0)
                {
                    DataTable dtIsExclusionExist = dsIsExclusionExist.Tables[0];
                    if (dtIsExclusionExist.Rows.Count > 0)
                        IsExclusionExist = true;
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return IsExclusionExist;
        }

        public List<ApplyOTIFExclusionBE> GetExcludedPoDAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            List<ApplyOTIFExclusionBE> Up_PurchaseOrderDetailBEList = new List<ApplyOTIFExclusionBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oApplyOTIFExclusionBE.Action);
                param[index++] = new SqlParameter("@SiteID", oApplyOTIFExclusionBE.SiteID);
                param[index++] = new SqlParameter("@DateFrom", oApplyOTIFExclusionBE.DateFrom);
                param[index++] = new SqlParameter("@DateTo", oApplyOTIFExclusionBE.DateTo);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "OTIF_ExcludedPOsReport", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    ApplyOTIFExclusionBE oNewApplyOTIFExclusionBE = new ApplyOTIFExclusionBE();
                    oNewApplyOTIFExclusionBE.DateFrom = Convert.ToString(dr["DateApplied"]);
                    oNewApplyOTIFExclusionBE.CapacityFull = dr["CapacityFull"].ToString();
                    oNewApplyOTIFExclusionBE.LinesDue = Convert.ToInt32(dr["LinesDue"]);
                    oNewApplyOTIFExclusionBE.LinesExcluded = Convert.ToInt32(dr["LinesExcludedAutomated"]);
                    oNewApplyOTIFExclusionBE.PerExcluded = dr["PerExcluded"].ToString();
                    oNewApplyOTIFExclusionBE.CapacityReachDate = dr["CapacityReachDate"] != DBNull.Value ? dr["CapacityReachDate"].ToString() : string.Empty;
                    oNewApplyOTIFExclusionBE.WorkinDaysCount = dr["WorkinDaysCount"] != DBNull.Value ? Convert.ToInt32(dr["WorkinDaysCount"]) : (int?)null;
                    oNewApplyOTIFExclusionBE.LinesExcludedManually = Convert.ToInt32(dr["LinesExcludedManually"]);
                    oNewApplyOTIFExclusionBE.AppliedHitAutomated = Convert.ToInt32(dr["AppliedHitAutomated"]);
                    oNewApplyOTIFExclusionBE.AppliedHitManually = Convert.ToInt32(dr["AppliedHitManually"]);
                    oNewApplyOTIFExclusionBE.Exceptions = Convert.ToInt32(dr["Exceptions"]);
                    oNewApplyOTIFExclusionBE.PercentwithHits = Convert.ToString(dr["PercentwithHits"]);
                    oNewApplyOTIFExclusionBE.OverallExceptions = Convert.ToString(dr["OverallExceptions"]);
                    oNewApplyOTIFExclusionBE.SiteName = Convert.ToString(dr["Site"]);
                    Up_PurchaseOrderDetailBEList.Add(oNewApplyOTIFExclusionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public DataTable GetExcludedPoExportDAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oApplyOTIFExclusionBE.Action);
                param[index++] = new SqlParameter("@SiteID", oApplyOTIFExclusionBE.SiteID);
                param[index++] = new SqlParameter("@DateFrom", oApplyOTIFExclusionBE.DateFrom);
                param[index++] = new SqlParameter("@DateTo", oApplyOTIFExclusionBE.DateTo);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "OTIF_ExcludedPOsReport", param);

                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public DataSet GetVendorExclusionDAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            DataSet dt = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", oApplyOTIFExclusionBE.Action);
                param[index++] = new SqlParameter("@SelectedExcludedVendorId", oApplyOTIFExclusionBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedExcludedCountry", oApplyOTIFExclusionBE.CountryID);
                param[index++] = new SqlParameter("@SelectedExcludedStatus", oApplyOTIFExclusionBE.ExcludedStatus);
                param[index++] = new SqlParameter("@GridPageSize", oApplyOTIFExclusionBE.GridPageSize);
                param[index++] = new SqlParameter("@GridCurrentPageNo", oApplyOTIFExclusionBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@SelectedVendor", oApplyOTIFExclusionBE.VendorID);
                param[index++] = new SqlParameter("@UserID", oApplyOTIFExclusionBE.UserID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_VendorReportingExclusion", param);

                dt = ds;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public int? DeleteApplyOtifHitDetailsDAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", oApplyOTIFExclusionBE.Action);
                param[index++] = new SqlParameter("@SelectedProductCodes", oApplyOTIFExclusionBE.SelectedProductCodes);
                param[index++] = new SqlParameter("@ExclusionID", oApplyOTIFExclusionBE.ApplyExclusionID);
                param[index++] = new SqlParameter("@Purchase_order", oApplyOTIFExclusionBE.Purchase_order);
                param[index++] = new SqlParameter("@Order_raised", oApplyOTIFExclusionBE.Order_raised);
                param[index++] = new SqlParameter("@DateApplied", oApplyOTIFExclusionBE.DateApplied);
                param[index++] = new SqlParameter("@Reason", oApplyOTIFExclusionBE.Reason);
                param[index++] = new SqlParameter("@UserID", oApplyOTIFExclusionBE.UserID);
                param[index++] = new SqlParameter("@SiteID", oApplyOTIFExclusionBE.SiteID);
                param[index++] = new SqlParameter("@VendorID", oApplyOTIFExclusionBE.VendorID);
                param[index++] = new SqlParameter("@UserName", oApplyOTIFExclusionBE.UserName);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

    }
}

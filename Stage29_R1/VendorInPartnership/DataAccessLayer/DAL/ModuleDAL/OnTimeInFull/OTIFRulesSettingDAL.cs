﻿using BusinessEntities.ModuleBE.OnTimeInFull;
using System;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.OnTimeInFull
{
    public class OTIFRulesSettingDAL
    {
        public int? AddOTIFRulesDAL(OTIFRulesSettingBE oOTIFRulesSettingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", oOTIFRulesSettingBE.Action);
                param[index++] = new SqlParameter("@RuleSettingID", oOTIFRulesSettingBE.RuleSettingID);
                param[index++] = new SqlParameter("@AbsoluteDaysAllowedEarly", oOTIFRulesSettingBE.AbsoluteDaysAllowedEarly);
                param[index++] = new SqlParameter("@AbsoluteDaysAllowedLate", oOTIFRulesSettingBE.AbsoluteDaysAllowedLate);
                param[index++] = new SqlParameter("@AbsoluteNumberOfDeliveriesAllowed", oOTIFRulesSettingBE.AbsoluteNumberOfDeliveriesAllowed);
                param[index++] = new SqlParameter("@ODMeasureDaysAllowedEarly11", oOTIFRulesSettingBE.ODMeasureDaysAllowedEarly11);
                param[index++] = new SqlParameter("@ODMeasureDaysAllowedLate11", oOTIFRulesSettingBE.ODMeasureDaysAllowedLate11);
                param[index++] = new SqlParameter("@ODMeasureNumberOfDeliveriesAllowed11", oOTIFRulesSettingBE.ODMeasureNumberOfDeliveriesAllowed11);
                param[index++] = new SqlParameter("@ToleratedDaysAllowedEarly1", oOTIFRulesSettingBE.ToleratedDaysAllowedEarly1);
                param[index++] = new SqlParameter("@ToleratedDaysAllowedLate1", oOTIFRulesSettingBE.ToleratedDaysAllowedLate1);
                param[index++] = new SqlParameter("@ToleratedNumberOfDeliveriesAllowed1", oOTIFRulesSettingBE.ToleratedNumberOfDeliveriesAllowed1);

                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spOTIF_RulesSetting", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public OTIFRulesSettingBE GetOTIFRules(OTIFRulesSettingBE oOTIFRulesSettingBE)
        {
            OTIFRulesSettingBE oNewOTIFRulesSettingBE = new OTIFRulesSettingBE();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oOTIFRulesSettingBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spOTIF_RulesSetting", param);
                DataTable dt = ds.Tables[0];
                DataRow dr = dt.Rows[0];
                oNewOTIFRulesSettingBE.RuleSettingID = dr["RuleSettingID"] != DBNull.Value ? Convert.ToInt32(dr["RuleSettingID"]) : 0;
                oNewOTIFRulesSettingBE.ODMeasureDaysAllowedEarly11 = dr["ODMeasureDaysAllowedEarly11"] != DBNull.Value ? Convert.ToInt32(dr["ODMeasureDaysAllowedEarly11"]) : 0;
                oNewOTIFRulesSettingBE.ODMeasureDaysAllowedLate11 = dr["ODMeasureDaysAllowedLate11"] != DBNull.Value ? Convert.ToInt32(dr["ODMeasureDaysAllowedLate11"]) : 0;
                oNewOTIFRulesSettingBE.ODMeasureNumberOfDeliveriesAllowed11 = dr["ODMeasureNumberOfDeliveriesAllowed11"] != DBNull.Value ? Convert.ToInt32(dr["ODMeasureNumberOfDeliveriesAllowed11"]) : 0;
                oNewOTIFRulesSettingBE.AbsoluteDaysAllowedEarly = dr["AbsoluteDaysAllowedEarly"] != DBNull.Value ? Convert.ToInt32(dr["AbsoluteDaysAllowedEarly"]) : 0;
                oNewOTIFRulesSettingBE.AbsoluteDaysAllowedLate = dr["AbsoluteDaysAllowedLate"] != DBNull.Value ? Convert.ToInt32(dr["AbsoluteDaysAllowedLate"]) : 0;
                oNewOTIFRulesSettingBE.AbsoluteNumberOfDeliveriesAllowed = dr["AbsoluteNumberOfDeliveriesAllowed"] != DBNull.Value ? Convert.ToInt32(dr["AbsoluteNumberOfDeliveriesAllowed"]) : 0;
                oNewOTIFRulesSettingBE.ToleratedDaysAllowedEarly1 = dr["ToleratedDaysAllowedEarly1"] != DBNull.Value ? Convert.ToInt32(dr["ToleratedDaysAllowedEarly1"]) : 0;
                oNewOTIFRulesSettingBE.ToleratedDaysAllowedLate1 = dr["ToleratedDaysAllowedLate1"] != DBNull.Value ? Convert.ToInt32(dr["ToleratedDaysAllowedLate1"]) : 0;
                oNewOTIFRulesSettingBE.ToleratedNumberOfDeliveriesAllowed1 = dr["ToleratedNumberOfDeliveriesAllowed1"] != DBNull.Value ? Convert.ToInt32(dr["ToleratedNumberOfDeliveriesAllowed1"]) : 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewOTIFRulesSettingBE;
        }

        public int? AddOTIFExclusionAutoDAL(OTIFRulesSettingBE oOTIFRulesSettingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oOTIFRulesSettingBE.Action);
                param[index++] = new SqlParameter("@BookingProcess", oOTIFRulesSettingBE.BookingProcess);
                param[index++] = new SqlParameter("@DailyScheduler", oOTIFRulesSettingBE.DailyScheduler);

                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spOTIF_RulesSetting", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public OTIFRulesSettingBE GetOTIFExclusionAutoDAL(OTIFRulesSettingBE oOTIFRulesSettingBE)
        {
            OTIFRulesSettingBE oNewOTIFRulesSettingBE = new OTIFRulesSettingBE();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oOTIFRulesSettingBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spOTIF_RulesSetting", param);
                DataTable dt = ds.Tables[0];
                DataRow dr = dt.Rows[0];
                oNewOTIFRulesSettingBE.BookingProcess = dr["BookingProcess"] != DBNull.Value ? Convert.ToBoolean(dr["BookingProcess"]) : false;
                oNewOTIFRulesSettingBE.DailyScheduler = dr["DailyScheduler"] != DBNull.Value ? Convert.ToBoolean(dr["DailyScheduler"]) : false;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewOTIFRulesSettingBE;
        }
    }
}

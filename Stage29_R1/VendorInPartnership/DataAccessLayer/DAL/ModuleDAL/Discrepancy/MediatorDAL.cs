﻿using BusinessEntities.ModuleBE.Discrepancy;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Discrepancy
{
    public class MediatorDAL : BaseDAL
    {

        public List<MediatorBE> GetMediatorDAL(MediatorBE MediatorBE)
        {
            var dataTable = new DataTable();
            var lstAPActionBE = new List<MediatorBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", MediatorBE.Action);
                param[index++] = new SqlParameter("@MediatorID", MediatorBE.MediatorID);
                var dataSet = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Mediator", param);
                dataTable = dataSet.Tables[0];

                foreach (DataRow dr in dataTable.Rows)
                {
                    var oAPActionBE = new MediatorBE();
                    oAPActionBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    oAPActionBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oAPActionBE.MediatorID = dr["CountryID"] != DBNull.Value ? Convert.ToInt32(dr["CountryID"].ToString()) : 0;
                    oAPActionBE.Country.CountryID = dr["CountryID"] != DBNull.Value ? Convert.ToInt32(dr["CountryID"].ToString()) : 0;
                    oAPActionBE.Country.CountryName = dr["CountryName"] != DBNull.Value ? Convert.ToString(dr["CountryName"]) : string.Empty;
                    oAPActionBE.User.UserID = dr["UserId"] != DBNull.Value ? Convert.ToInt32(dr["UserId"]) : 0;
                    oAPActionBE.User.UserName = dr["UserName"] != DBNull.Value ? Convert.ToString(dr["UserName"]) : string.Empty;

                    lstAPActionBE.Add(oAPActionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstAPActionBE;
        }

        public DataTable GetMediatorUserDAL(MediatorBE MediatorBE)
        {
            var dataTable = new DataTable();
            var lstAPActionBE = new List<MediatorBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", MediatorBE.Action);
                param[index++] = new SqlParameter("@CountryID", MediatorBE.Country.CountryID);
                param[index++] = new SqlParameter("@UserName", MediatorBE.User.UserName);
                var dataSet = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Mediator", param);
                if (dataSet.Tables.Count > 0)
                    dataTable = dataSet.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dataTable;
        }

        public int? addEditMediatorDAL(MediatorBE MediatorBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", MediatorBE.Action);
                param[index++] = new SqlParameter("@UserID", MediatorBE.User.UserID);
                param[index++] = new SqlParameter("@CountryID", MediatorBE.Country.CountryID);

                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_Mediator", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

﻿using BusinessEntities.ModuleBE.Discrepancy.APAction;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Discrepancy.CountrySetting
{
    public class DIS_DebitReasonTypeDAL : BaseDAL
    {
        public int CheckExistanceDAL(APActionBE apActionBE)
        {
            int checkExistance = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", apActionBE.Action);
                param[index++] = new SqlParameter("@Reason", apActionBE.Reason);
                param[index++] = new SqlParameter("@ReasonType", apActionBE.ReasonType);
                param[index++] = new SqlParameter("@ReasonTypeID", apActionBE.ReasonTypeID);
                checkExistance = Convert.ToInt32(SqlHelper.ExecuteScalar(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_DebitCancelReasonTypeSetup", param));
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return checkExistance;
        }

        public int? addEditReasonTypeDAL(APActionBE apActionBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", apActionBE.Action);
                param[index++] = new SqlParameter("@Reason", apActionBE.Reason);
                param[index++] = new SqlParameter("@ReasonType", apActionBE.ReasonType);
                param[index++] = new SqlParameter("@ReasonTypeID", apActionBE.ReasonTypeID);
                param[index++] = new SqlParameter("@UserID", apActionBE.UserID);
                param[index++] = new SqlParameter("@IsBOPenaltyRelated", apActionBE.IsBOPenaltyRelated);

                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_DebitCancelReasonTypeSetup", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APActionBE> GetReasonTypeDAL(APActionBE apActionBE)
        {
            var dataTable = new DataTable();
            var lstAPActionBE = new List<APActionBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", apActionBE.Action);
                param[index++] = new SqlParameter("@ReasonType", apActionBE.ReasonType);
                var dataSet = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_DebitCancelReasonTypeSetup", param);
                dataTable = dataSet.Tables[0];

                foreach (DataRow dr in dataTable.Rows)
                {
                    var oAPActionBE = new APActionBE();
                    oAPActionBE.ReasonTypeID = dr["ReasonTypeID"] != DBNull.Value ? Convert.ToInt32(dr["ReasonTypeID"].ToString()) : (int?)null;
                    oAPActionBE.Reason = dr["Reason"] != DBNull.Value ? Convert.ToString(dr["Reason"]) : string.Empty;
                    oAPActionBE.ReasonType = dr["ReasonType"] != DBNull.Value ? Convert.ToString(dr["ReasonType"]) : string.Empty;
                    oAPActionBE.ReasonDate = dr["ReasonDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReasonDate"].ToString()) : (DateTime?)null;
                    oAPActionBE.UserID = dr["UserId"] != DBNull.Value ? Convert.ToInt32(dr["UserId"]) : (int?)null;
                    oAPActionBE.UserName = dr["UserName"] != DBNull.Value ? Convert.ToString(dr["UserName"]) : string.Empty;
                    oAPActionBE.IsBOPenaltyRelated = dr["IsBOPenaltyRelated"] != DBNull.Value ? Convert.ToBoolean(dr["IsBOPenaltyRelated"]) : false;
                    lstAPActionBE.Add(oAPActionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstAPActionBE;
        }
    }
}

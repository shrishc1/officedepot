﻿using BusinessEntities.ModuleBE.Discrepancy.Report;
using System;
using System.Data;
using System.Data.SqlClient;
using Utilities;


namespace DataAccessLayer.ModuleDAL.Discrepancy.Report
{
    public class DiscrepancyReportDAL
    {
        DiscrepancyReportBE oDiscrepancyReportBE = new DiscrepancyReportBE();
        public DataSet getDiscrepancyReportDAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", oDiscrepancyReportBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", oDiscrepancyReportBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oDiscrepancyReportBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oDiscrepancyReportBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oDiscrepancyReportBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedAPIDs", oDiscrepancyReportBE.SelectedAPIDs);
                param[index++] = new SqlParameter("@VendorNumber", oDiscrepancyReportBE.VendorNumber);
                param[index++] = new SqlParameter("@VendorName", oDiscrepancyReportBE.VendorName);
                param[index++] = new SqlParameter("@DateTo", oDiscrepancyReportBE.DateTo);
                param[index++] = new SqlParameter("@DateFrom", oDiscrepancyReportBE.DateFrom);
                param[index++] = new SqlParameter("@ReportView", oDiscrepancyReportBE.SelectedTypes);
                param[index++] = new SqlParameter("@DiscrepancyTypeId", oDiscrepancyReportBE.DiscrepancyTypeId);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spRPTDIS_Reports", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet getAssignedVendorsReportDAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyReportBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", oDiscrepancyReportBE.SelectedCountryIDs);
                //param[index++] = new SqlParameter("@SelectedSiteIDs", oDiscrepancyReportBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oDiscrepancyReportBE.SelectedVendorIDs);
                //param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oDiscrepancyReportBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedAPIDs", oDiscrepancyReportBE.SelectedAPIDs);
                //param[index++] = new SqlParameter("@VendorNumber", oDiscrepancyReportBE.VendorNumber);
                //param[index++] = new SqlParameter("@VendorName", oDiscrepancyReportBE.VendorName);
                //param[index++] = new SqlParameter("@DateTo", oDiscrepancyReportBE.DateTo);
                //param[index++] = new SqlParameter("@DateFrom", oDiscrepancyReportBE.DateFrom);
                //param[index++] = new SqlParameter("@DiscrepancyTypeId", oDiscrepancyReportBE.DiscrepancyTypeId);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spRPTDIS_Reports", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetShuttleDiscrepancyReportDAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                param[index++] = new SqlParameter("@Action", oDiscrepancyReportBE.Action);
                param[index++] = new SqlParameter("@SelectedReceivingSiteIDs", oDiscrepancyReportBE.SelectedReceivingSiteIDs);
                param[index++] = new SqlParameter("@SelectedSendingSiteIDs", oDiscrepancyReportBE.SelectedSendingSiteIDs);
                param[index++] = new SqlParameter("@SelectedDiscrepancyType", oDiscrepancyReportBE.SelectedTypes);
                param[index++] = new SqlParameter("@SelectedPO", oDiscrepancyReportBE.SelectedPO);
                param[index++] = new SqlParameter("@SelectedSKU", oDiscrepancyReportBE.SelectedSKU);
                param[index++] = new SqlParameter("@DateTo", oDiscrepancyReportBE.DateTo);
                param[index++] = new SqlParameter("@DateFrom", oDiscrepancyReportBE.DateFrom);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spRPTDIS_Reports", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetDeletedDiscrepanciesReportDAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                param[index++] = new SqlParameter("@Action", oDiscrepancyReportBE.Action);
                param[index++] = new SqlParameter("@SelectedDiscrepancyNos", oDiscrepancyReportBE.SelectedDiscrepancyNos);
                param[index++] = new SqlParameter("@SelectedCountryIDs", oDiscrepancyReportBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oDiscrepancyReportBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oDiscrepancyReportBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedUserIDs", oDiscrepancyReportBE.SelectedUserIDs);
                param[index++] = new SqlParameter("@DateTo", oDiscrepancyReportBE.DateTo);
                param[index++] = new SqlParameter("@DateFrom", oDiscrepancyReportBE.DateFrom);
                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spRPTDIS_Reports", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }


        public DataSet GetVDRDiscrepanciesReportDAL(DiscrepancyReportBE oDiscrepancyReportBE, out int RecordCount)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[14];
                param[index++] = new SqlParameter("@Action", oDiscrepancyReportBE.Action);
                param[index++] = new SqlParameter("@Dateto", oDiscrepancyReportBE.DateTo);
                param[index++] = new SqlParameter("@Datefrom", oDiscrepancyReportBE.DateFrom);
                param[index++] = new SqlParameter("@SiteIDs", oDiscrepancyReportBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@CountryIDs", oDiscrepancyReportBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@StockPlannerIDs", oDiscrepancyReportBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@VendorIDs", oDiscrepancyReportBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@AccountPayableIDs", oDiscrepancyReportBE.SelectedAPIDs);
                param[index++] = new SqlParameter("@DiscrepancyStatus", oDiscrepancyReportBE.DiscrepancyStatus);
                param[index++] = new SqlParameter("@StockPlannerName", oDiscrepancyReportBE.StockPlannerName);
                param[index++] = new SqlParameter("@APName", oDiscrepancyReportBE.APName);
                param[index++] = new SqlParameter("@PageCount", oDiscrepancyReportBE.PageCount);
                param[index++] = new SqlParameter("@RecordCount", SqlDbType.Int);
                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "sp_NewVDRTracker", param);
                RecordCount = Result.Tables[1].Rows[0]["RecordCount"] != DBNull.Value ? Convert.ToInt32(Result.Tables[1].Rows[0]["RecordCount"]) : 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                RecordCount = 0;
            }
            finally { }
            if (oDiscrepancyReportBE.Action == "VendorWiseDisc" || oDiscrepancyReportBE.Action == "VendorWiseSite" || oDiscrepancyReportBE.Action == "VendorWiseOpen")
            {
                Result.Tables[0].Columns.Remove("Vendor_Name");
            }
            return Result;
        }
    }
}

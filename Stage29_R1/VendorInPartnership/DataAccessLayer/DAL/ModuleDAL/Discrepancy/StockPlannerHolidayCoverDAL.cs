﻿using BusinessEntities.ModuleBE.Discrepancy;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;
namespace DataAccessLayer.ModuleDAL.Discrepancy
{
    public class StockPlannerHolidayCoverDAL : BaseDAL
    {
        public List<StockPlannerHolidayCoverBE> GetStockPlannerHolidaysDAL(StockPlannerHolidayCoverBE StockPlannerHolidayCoverBE)
        {
            var dataTable = new DataTable();
            var lstAPActionBE = new List<StockPlannerHolidayCoverBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", StockPlannerHolidayCoverBE.Action);
                param[index++] = new SqlParameter("@UserID", StockPlannerHolidayCoverBE.SCT_UserBE.UserID);
                var dataSet = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_StockPlannerHolidayCover", param);
                dataTable = dataSet.Tables[0];

                foreach (DataRow dr in dataTable.Rows)
                {
                    var oAPActionBE = new StockPlannerHolidayCoverBE();
                    oAPActionBE.DateOfReturn = dr["DateOfReturn"] != DBNull.Value ? Convert.ToString(dr["DateOfReturn"]) : null;
                    oAPActionBE.IsAbsent = dr["IsAbsent"] != DBNull.Value ? Convert.ToBoolean(dr["IsAbsent"]) : false;
                    oAPActionBE.StockPlannerToCover = dr["StockPlannerToCover"] != DBNull.Value ? Convert.ToString(dr["StockPlannerToCover"]) : null;
                    oAPActionBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"]) : 0;
                    oAPActionBE.StockPlanner = dr["StockPlanner"] != DBNull.Value ? Convert.ToString(dr["StockPlanner"]) : "";
                    lstAPActionBE.Add(oAPActionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstAPActionBE;
        }


        public int? addEditStockPlannerHolidayCoveDAL(StockPlannerHolidayCoverBE StockPlannerHolidayCoverBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", StockPlannerHolidayCoverBE.Action);
                param[index++] = new SqlParameter("@IsAbsent", StockPlannerHolidayCoverBE.IsAbsent);
                param[index++] = new SqlParameter("@DateOfReturn", StockPlannerHolidayCoverBE.DateOfReturn);
                param[index++] = new SqlParameter("@StockPlannerID", StockPlannerHolidayCoverBE.StockPlannerID);
                param[index++] = new SqlParameter("@SPToCoverIDs", StockPlannerHolidayCoverBE.SPToCoverIDs);
                param[index++] = new SqlParameter("@Priority", StockPlannerHolidayCoverBE.Priority);

                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_StockPlannerHolidayCover", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public StockPlannerHolidayCoverBE GetAllStockPlannerHolidaysDAL(StockPlannerHolidayCoverBE StockPlannerHolidayCoverBE)
        {
            var dataTable = new DataTable();
            var lstAPActionBE = new StockPlannerHolidayCoverBE();
            var objStockPlannerHolidayCoverBE = new StockPlannerHolidayCoverBE();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", StockPlannerHolidayCoverBE.Action);
                param[index++] = new SqlParameter("@StockPlannerID", StockPlannerHolidayCoverBE.StockPlannerID);
                var dataSet = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_StockPlannerHolidayCover", param);
                dataTable = dataSet.Tables[0];
                foreach (DataRow dr in dataTable.Rows)
                {
                    objStockPlannerHolidayCoverBE.DateOfReturn = dr["DateOfReturn"] != DBNull.Value ? Convert.ToString(dr["DateOfReturn"]) : null;
                    objStockPlannerHolidayCoverBE.IsAbsent = dr["IsAbsent"] != DBNull.Value ? Convert.ToBoolean(dr["IsAbsent"]) : false;
                    objStockPlannerHolidayCoverBE.SPToCoverIDs = dr["StockPlannerToCover"] != DBNull.Value ? Convert.ToString(dr["StockPlannerToCover"]) : null;
                    objStockPlannerHolidayCoverBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"]) : 0;
                    objStockPlannerHolidayCoverBE.StockPlanner = dr["StockPlanner"] != DBNull.Value ? Convert.ToString(dr["StockPlanner"]) : "";
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return objStockPlannerHolidayCoverBE;
        }

        public DataTable GetStockPlannerWithWithUserCountrysDAL(StockPlannerHolidayCoverBE StockPlannerHolidayCoverBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", StockPlannerHolidayCoverBE.Action);
                param[index++] = new SqlParameter("@UserID", StockPlannerHolidayCoverBE.SCT_UserBE.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_StockPlannerHolidayCover", param);
                if (ds != null && ds.Tables.Count > 0)
                    dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

    }
}

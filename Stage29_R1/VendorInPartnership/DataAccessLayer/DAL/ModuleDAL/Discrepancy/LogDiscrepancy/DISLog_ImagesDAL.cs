﻿using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Discrepancy.LogDiscrepancy
{
    public class DISLog_ImagesDAL : BaseDAL
    {
        public List<DISLog_ImagesBE> GetDISLogImageDetailsDAL(DISLog_ImagesBE oDISLog_ImagesBE)
        {
            List<DISLog_ImagesBE> DISLog_ImagesBEList = new List<DISLog_ImagesBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDISLog_ImagesBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDISLog_ImagesBE.Discrepancy.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyImages", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DISLog_ImagesBE oNewDISLog_ImagesBE = new DISLog_ImagesBE();
                    oNewDISLog_ImagesBE.Discrepancy = new DiscrepancyBE();

                    oNewDISLog_ImagesBE.DiscrepancyLogImageID = dr["DiscrepancyLogImageID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyLogImageID"]) : (int?)null;
                    oNewDISLog_ImagesBE.Discrepancy.DiscrepancyLogID = dr["DiscrepancyLogID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyLogID"]) : (int?)null;
                    oNewDISLog_ImagesBE.ImageName = dr["ImageName"] != DBNull.Value ? dr["ImageName"].ToString() : "";

                    DISLog_ImagesBEList.Add(oNewDISLog_ImagesBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DISLog_ImagesBEList;
        }

        public int? addEditDISLogImageDetailsDAL(DISLog_ImagesBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[15];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.Discrepancy.DiscrepancyLogID);
                param[index++] = new SqlParameter("@ImageNames", oDiscrepancyBE.ImageNames);
                param[index++] = new SqlParameter("@DiscrepancyLogImageID", oDiscrepancyBE.DiscrepancyLogImageID);
                param[index++] = new SqlParameter("@Freight_Charges", oDiscrepancyBE.Freight_Charges);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancyImages", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


        public List<DISLog_ImagesBE> GetDISLogPODDetailsDAL(DISLog_ImagesBE oDISLog_ImagesBE)
        {
            List<DISLog_ImagesBE> DISLog_ImagesBEList = new List<DISLog_ImagesBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDISLog_ImagesBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDISLog_ImagesBE.Discrepancy.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyImages", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DISLog_ImagesBE oNewDISLog_ImagesBE = new DISLog_ImagesBE();
                    oNewDISLog_ImagesBE.Discrepancy = new DiscrepancyBE();

                    oNewDISLog_ImagesBE.Discrepancy.DiscrepancyLogID = dr["DiscrepancyLogID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyLogID"]) : (int?)null;
                    oNewDISLog_ImagesBE.ImageName = dr["DocPath"] != DBNull.Value ? dr["DocPath"].ToString() : "";

                    DISLog_ImagesBEList.Add(oNewDISLog_ImagesBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DISLog_ImagesBEList;
        }
    }
}

﻿using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Discrepancy.LogDiscrepancy
{
    public class DISLog_QueryDiscrepancyDAL : BaseDAL
    {
        public int? addQueryDiscrepancyDAL(DISLog_QueryDiscrepancyBE queryDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", queryDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@QueryDiscrepancyID", queryDiscrepancyBE.QueryDiscrepancyID);
                param[index++] = new SqlParameter("@DiscrepancyLogID", queryDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@VendorComment", queryDiscrepancyBE.VendorComment);
                param[index++] = new SqlParameter("@GoodsInComment", queryDiscrepancyBE.GoodsInComment);
                param[index++] = new SqlParameter("@GoodsInAction", queryDiscrepancyBE.GoodsInAction);
                param[index++] = new SqlParameter("@VendorUserId", queryDiscrepancyBE.VendorUserId);
                param[index++] = new SqlParameter("@GoodsInUserId", queryDiscrepancyBE.GoodsInUserId);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? IsAlreadyDisputeExistDAL(DISLog_QueryDiscrepancyBE queryDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", queryDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", queryDiscrepancyBE.DiscrepancyLogID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyBE> GetDiscrepancyLogDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            var DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                var dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.UserID);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                if (oDiscrepancyBE.User != null)
                    param[index++] = new SqlParameter("@RoleTypeFlag", oDiscrepancyBE.User.RoleTypeFlag);

                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    var oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : "";
                    oNewDiscrepancyBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewDiscrepancyBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oNewDiscrepancyBE.User.FirstName = dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString() : "";
                    oNewDiscrepancyBE.User.PhoneNumber = dr["PhoneNumber"] != DBNull.Value ? dr["PhoneNumber"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerNO = dr["StockPlanner"] != DBNull.Value ? dr["StockPlanner"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerContact = dr["StockContactNo"] != DBNull.Value ? dr["StockContactNo"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerName = dr["StockPlannerName"] != DBNull.Value ? dr["StockPlannerName"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.ProductDescription = dr["DiscrepancyDiscripton"] != DBNull.Value ? dr["DiscrepancyDiscripton"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyLogDate = dr["DiscrepancyLogDate"] != DBNull.Value ? Convert.ToDateTime(dr["DiscrepancyLogDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.DiscrepancyStatus = dr["DiscrepancyStatus"] != DBNull.Value ? dr["DiscrepancyStatus"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyStatusDiscreption = dr["DiscrepancyStatusDiscreption"] != DBNull.Value ? dr["DiscrepancyStatusDiscreption"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["PurchaseOrderNumber"] != DBNull.Value ? dr["PurchaseOrderNumber"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrderDate = dr["PurchaseOrderDate"] != DBNull.Value ? Convert.ToDateTime(dr["PurchaseOrderDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.InternalComments = dr["InternalComments"] != DBNull.Value ? dr["InternalComments"].ToString() : "";
                    oNewDiscrepancyBE.DeliveryNoteNumber = dr["DeliveryNoteNumber"] != DBNull.Value ? dr["DeliveryNoteNumber"].ToString() : "";
                    oNewDiscrepancyBE.DeliveryArrivedDate = dr["DeliveryArrivedDate"] != DBNull.Value ? Convert.ToDateTime(dr["DeliveryArrivedDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.CommunicationType = dr["CommunicationType"] != DBNull.Value ? Convert.ToChar(dr["CommunicationType"]) : (char?)null;
                    oNewDiscrepancyBE.CommunicationTo = dr["CommunicationTo"] != DBNull.Value ? dr["CommunicationTo"].ToString() : "";
                    oNewDiscrepancyBE.VendorNoName = dr["VendorNoName"] != DBNull.Value ? dr["VendorNoName"].ToString() : "";
                    oNewDiscrepancyBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewDiscrepancyBE.Vendor.VendorContactNumber = dr["VendorContactNumber"] != DBNull.Value ? dr["VendorContactNumber"].ToString() : "";
                    oNewDiscrepancyBE.CarrierName = dr["CarrierName"] != DBNull.Value ? dr["CarrierName"].ToString() : "";
                    oNewDiscrepancyBE.PersentationIssueComment = dr["PersentationIssueComment"] != DBNull.Value ? dr["PersentationIssueComment"].ToString() : "";
                    oNewDiscrepancyBE.QualityIssueComment = dr["QualityIssueComment"] != DBNull.Value ? dr["QualityIssueComment"].ToString() : "";
                    oNewDiscrepancyBE.FailPalletSpecificationComment = dr["FailPalletSpecificationComment"] != DBNull.Value ? dr["FailPalletSpecificationComment"].ToString() : "";
                    oNewDiscrepancyBE.PrematureReceiptInvoiceComment = dr["PrematureReceiptInvoiceComment"] != DBNull.Value ? dr["PrematureReceiptInvoiceComment"].ToString() : "";
                    oNewDiscrepancyBE.GenericDiscrepancyActionRequired = dr["GenericDiscrepancyActionRequired"] != DBNull.Value ? dr["GenericDiscrepancyActionRequired"].ToString() : "";
                    oNewDiscrepancyBE.OfficeDepotContactName = dr["OfficeDepotContactName"] != DBNull.Value ? dr["OfficeDepotContactName"].ToString() : "";
                    oNewDiscrepancyBE.OfficeDepotContactPhone = dr["OfficeDepotContactPhone"] != DBNull.Value ? dr["OfficeDepotContactPhone"].ToString() : "";
                    oNewDiscrepancyBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    oNewDiscrepancyBE.POSiteID = dr["POSiteID"] != DBNull.Value ? Convert.ToInt32(dr["POSiteID"]) : 0;
                    oNewDiscrepancyBE.POSiteName = dr["POSiteName"] != DBNull.Value ? Convert.ToString(dr["POSiteName"]) : "";
                    oNewDiscrepancyBE.Freight_Charges = dr["Freight_Charges"] != DBNull.Value ? Convert.ToDecimal(dr["Freight_Charges"].ToString()) : (decimal?)null;
                    oNewDiscrepancyBE.NumberOfPallets = dr["NumberOfPallets"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallets"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.TotalLabourCost = dr["TotalLabourCost"] != DBNull.Value ? Convert.ToDecimal(dr["TotalLabourCost"].ToString()) : (decimal?)null;
                    oNewDiscrepancyBE.StockPlannerEmailID = dr["StockPlannerEmailID"] != DBNull.Value ? Convert.ToString(dr["StockPlannerEmailID"]) : null;
                    oNewDiscrepancyBE.GenericDiscrepancyIssueComment = dr["GenericDiscrepancyIssueComment"] != DBNull.Value ? dr["GenericDiscrepancyIssueComment"].ToString() : "";
                    if (oDiscrepancyBE.User != null && oDiscrepancyBE.User.RoleTypeFlag != null && !string.Equals(oDiscrepancyBE.User.RoleTypeFlag, "A"))
                    {
                        oNewDiscrepancyBE.DiscrepancyWorkFlowID = dr["DiscrepancyWorkflowID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyWorkflowID"]) : (int?)null;
                    }

                    if (dr.Table.Columns.Contains("GI"))
                        oNewDiscrepancyBE.GI = dr["GI"] != DBNull.Value ? Convert.ToString(dr["GI"]) : string.Empty;

                    if (dr.Table.Columns.Contains("INV"))
                        oNewDiscrepancyBE.INV = dr["INV"] != DBNull.Value ? Convert.ToString(dr["INV"]) : string.Empty;

                    if (dr.Table.Columns.Contains("AP"))
                        oNewDiscrepancyBE.AP = dr["AP"] != DBNull.Value ? Convert.ToString(dr["AP"]) : string.Empty;

                    if (dr.Table.Columns.Contains("VEN"))
                        oNewDiscrepancyBE.VEN = dr["VEN"] != DBNull.Value ? Convert.ToString(dr["VEN"]) : string.Empty;

                    if (dr.Table.Columns.Contains("PickersName"))
                        oNewDiscrepancyBE.PickersName = dr["PickersName"] != DBNull.Value ? Convert.ToString(dr["PickersName"]) : string.Empty;

                    if (dr.Table.Columns.Contains("Query"))
                    {
                        oNewDiscrepancyBE.QueryDiscrepancy = new DISLog_QueryDiscrepancyBE();
                        oNewDiscrepancyBE.QueryDiscrepancy.QueryDiscrepancyID = dr["QueryDiscrepancyID"] != DBNull.Value ? Convert.ToInt32(dr["QueryDiscrepancyID"]) : 0;
                        oNewDiscrepancyBE.QueryDiscrepancy.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : string.Empty;
                    }

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DISLog_QueryDiscrepancyBE> GetQueryDiscrepancyDAL(DISLog_QueryDiscrepancyBE oQueryDiscrepancyBE)
        {
            var lstQueryDiscrepancy = new List<DISLog_QueryDiscrepancyBE>();
            try
            {
                var dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oQueryDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@QueryDiscrepancyID", oQueryDiscrepancyBE.QueryDiscrepancyID);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oQueryDiscrepancyBE.DiscrepancyLogID);
                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();
                    queryDiscrepancyBE.QueryDiscrepancyID = dr["QueryDiscrepancyID"] != DBNull.Value ? Convert.ToInt32(dr["QueryDiscrepancyID"]) : (int?)null;
                    queryDiscrepancyBE.DiscrepancyLogID = dr["DiscrepancyLogID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyLogID"]) : (int?)null;
                    queryDiscrepancyBE.VendorComment = dr["VendorComment"] != DBNull.Value ? Convert.ToString(dr["VendorComment"]) : string.Empty;
                    queryDiscrepancyBE.GoodsInComment = dr["GoodsInComment"] != DBNull.Value ? Convert.ToString(dr["GoodsInComment"]) : string.Empty;
                    queryDiscrepancyBE.GoodsInAction = dr["GoodsInAction"] != DBNull.Value ? Convert.ToString(dr["GoodsInAction"]) : string.Empty;
                    queryDiscrepancyBE.VendorQueryDate = dr["VendorQueryDate"] != DBNull.Value ? Convert.ToDateTime(dr["VendorQueryDate"]) : (DateTime?)null;
                    queryDiscrepancyBE.GoodsInQueryDate = dr["GoodsInQueryDate"] != DBNull.Value ? Convert.ToDateTime(dr["GoodsInQueryDate"]) : (DateTime?)null;
                    queryDiscrepancyBE.VendorUserId = dr["VendorUserId"] != DBNull.Value ? Convert.ToInt32(dr["VendorUserId"]) : (int?)null;
                    queryDiscrepancyBE.VendorUserName = dr["VendorUserName"] != DBNull.Value ? Convert.ToString(dr["VendorUserName"]) : string.Empty;
                    queryDiscrepancyBE.GoodsInUserId = dr["GoodsInUserId"] != DBNull.Value ? Convert.ToInt32(dr["GoodsInUserId"]) : (int?)null;
                    queryDiscrepancyBE.GoodsInUserName = dr["GoodsInUserName"] != DBNull.Value ? Convert.ToString(dr["GoodsInUserName"]) : string.Empty;
                    queryDiscrepancyBE.QueryAction = dr["QueryAction"] != DBNull.Value ? Convert.ToString(dr["QueryAction"]) : string.Empty;

                    queryDiscrepancyBE.DiscrepancyMail = new BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyMailBE();
                    queryDiscrepancyBE.DiscrepancyMail.discrepancyCommunicationID = dr["DiscrepancyCommunicationID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyCommunicationID"]) : 0;
                    queryDiscrepancyBE.DiscrepancyMail.communicationLevel = dr["CommunicationLevel"] != DBNull.Value ? Convert.ToString(dr["CommunicationLevel"]) : string.Empty;

                    queryDiscrepancyBE.IsQueryClosedManually = dr["IsQueryClosedManually"] != DBNull.Value ? Convert.ToBoolean(dr["IsQueryClosedManually"].ToString()) : (bool?)null;

                    lstQueryDiscrepancy.Add(queryDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstQueryDiscrepancy;
        }

        public DiscrepancyMailBE GetQueryDiscrepancyEmailContentDAL(DiscrepancyMailBE discrepancyMail)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", discrepancyMail.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", discrepancyMail.QueryDiscrepancy.DiscrepancyLogID);
                param[index++] = new SqlParameter("@QueryDiscrepancyID", discrepancyMail.QueryDiscrepancy.QueryDiscrepancyID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    var oNewAPPBOK_CommunicationBE = new DiscrepancyMailBE();
                    discrepancyMail.sentTo = dr["SentTo"] != DBNull.Value ? Convert.ToString(dr["SentTo"]) : string.Empty;
                    discrepancyMail.mailSubject = dr["Subject"] != DBNull.Value ? Convert.ToString(dr["Subject"]) : string.Empty;
                    discrepancyMail.mailBody = dr["Body"] != DBNull.Value ? Convert.ToString(dr["Body"]) : string.Empty;
                    discrepancyMail.languageID = dr["LanguageID"] != DBNull.Value ? Convert.ToInt32(dr["LanguageID"]) : (int?)null;
                    discrepancyMail.communicationLevel = dr["CommunicationLevel"] != DBNull.Value ? Convert.ToString(dr["CommunicationLevel"]) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return discrepancyMail;
        }


        public List<DISLog_QueryDiscrepancyBE> GetDiscrepancyDisputesDAL(DISLog_QueryDiscrepancyBE oQueryDiscrepancyBE)
        {
            var lstQueryDiscrepancy = new List<DISLog_QueryDiscrepancyBE>();
            try
            {
                var dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oQueryDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@SCSelectedSiteIDs", oQueryDiscrepancyBE.SCSelectedSiteIDs);
                param[index++] = new SqlParameter("@VendorID", oQueryDiscrepancyBE.VendorID);
                param[index++] = new SqlParameter("@SCSelectedVendorIDs", oQueryDiscrepancyBE.SCSelectedVendorIDs);
                param[index++] = new SqlParameter("@SCDiscrepancyDateFrom", oQueryDiscrepancyBE.SCDiscrepancyDateFrom);
                param[index++] = new SqlParameter("@SCDiscrepancyDateTo", oQueryDiscrepancyBE.SCDiscrepancyDateTo);
                param[index++] = new SqlParameter("@Query", oQueryDiscrepancyBE.Query);
                param[index++] = new SqlParameter("@UserIdForConsVendors", oQueryDiscrepancyBE.UserIdForConsVendors);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    var queryDiscrepancyBE = new DISLog_QueryDiscrepancyBE();

                    queryDiscrepancyBE.Discrepancy = new BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyBE();

                    queryDiscrepancyBE.Vendor_No = dr["VendorNo"] != DBNull.Value ? Convert.ToString(dr["VendorNo"]) : string.Empty;
                    queryDiscrepancyBE.Vendor_Name = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                    queryDiscrepancyBE.VendorQueryDate = dr["QueryRaised"] != DBNull.Value ? Convert.ToDateTime(dr["QueryRaised"]) : (DateTime?)null;
                    queryDiscrepancyBE.VendorUserName = dr["Whoraisedtheissue"] != DBNull.Value ? Convert.ToString(dr["Whoraisedtheissue"]) : string.Empty;
                    queryDiscrepancyBE.Query = dr["QueryStatus"] != DBNull.Value ? Convert.ToString(dr["QueryStatus"]) : string.Empty;
                    queryDiscrepancyBE.Discrepancy.DiscrepancyLogDate = dr["DateDiscrepancyRaised"] != DBNull.Value ? Convert.ToDateTime(dr["DateDiscrepancyRaised"]) : (DateTime?)null;
                    queryDiscrepancyBE.Discrepancy.VDRNo = dr["DiscrepancyNumber"] != DBNull.Value ? Convert.ToString(dr["DiscrepancyNumber"]) : string.Empty;
                    queryDiscrepancyBE.VendorComment = dr["DetailofQuery"] != DBNull.Value ? Convert.ToString(dr["DetailofQuery"]) : string.Empty;
                    queryDiscrepancyBE.GoodsInComment = dr["OfficeDepotResponse"] != DBNull.Value ? Convert.ToString(dr["OfficeDepotResponse"]) : string.Empty;
                    queryDiscrepancyBE.GoodsInQueryDate = dr["Dateofresponse"] != DBNull.Value ? Convert.ToDateTime(dr["Dateofresponse"]) : (DateTime?)null;
                    queryDiscrepancyBE.GoodsInUserName = dr["Respondedby"] != DBNull.Value ? Convert.ToString(dr["Respondedby"]) : string.Empty;
                    queryDiscrepancyBE.ElaspedTime = dr["ElaspedTime"] != DBNull.Value ? Convert.ToString(dr["ElaspedTime"]) : string.Empty;
                    queryDiscrepancyBE.QueryDiscrepancyID = dr["QueryDiscrepancyID"] != DBNull.Value ? Convert.ToInt32(dr["QueryDiscrepancyID"]) : (int?)null;
                    queryDiscrepancyBE.Discrepancy.DiscrepancyLogID = dr["DiscrepancyLogID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyLogID"]) : (int?)null;
                    lstQueryDiscrepancy.Add(queryDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstQueryDiscrepancy;
        }
        //RETURN COUNT NO QUERY HAS BEEN CLOSED OR NOT
        public int? IsQueryClosed(DISLog_QueryDiscrepancyBE queryDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", queryDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", queryDiscrepancyBE.DiscrepancyLogID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[1].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

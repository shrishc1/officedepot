﻿using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;


namespace DataAccessLayer.ModuleDAL.Discrepancy.LogDiscrepancy
{
    public class DiscrepancyDAL
    {

        /// <summary>
        /// Used for checking valid purchase order number
        /// </summary>
        /// <param name="oDiscrepancyBE"></param>
        /// <returns></returns>
        public List<DiscrepancyBE> CheckValidPurchaseOrderDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@SiteID", oDiscrepancyBE.SiteID);
                param[index++] = new SqlParameter("@Purchase_order", oDiscrepancyBE.PurchaseOrderNumber);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["Purchase_order"].ToString();

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetPurchaseOrderDateListDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@SiteID", oDiscrepancyBE.SiteID);
                param[index++] = new SqlParameter("@Purchase_order", oDiscrepancyBE.PurchaseOrderNumber);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oNewDiscrepancyBE.PurchaseOrder_Order_raised = dr["Order_raised"].ToString();
                    oNewDiscrepancyBE.VendorNoName = dr["VendorNo"].ToString();
                    oNewDiscrepancyBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewDiscrepancyBE.StockPlannerName = Convert.ToString(dr["StockPlannerName"]);
                    if (dr.Table.Columns.Contains("RowNumber"))
                        oNewDiscrepancyBE.RowNumber = Convert.ToInt32(dr["RowNumber"]);
                    if (dr.Table.Columns.Contains("Original_due_date"))
                        oNewDiscrepancyBE.VendorNoName = oNewDiscrepancyBE.VendorNoName + "~" + dr["Original_due_date"].ToString();
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetStockPlannerListByUserIDDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.StockPlannerEmailID = dr["StockPlannerEmailID"].ToString();
                    oNewDiscrepancyBE.StockPlannerNO = Convert.ToString(dr["StockPlannerNO"]);
                    oNewDiscrepancyBE.StockPlannerContact = Convert.ToString(dr["StockPlannerContact"]);
                    oNewDiscrepancyBE.StockPlannerID = Convert.ToInt32(dr["StockPlannerID"]);

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }


        public List<DiscrepancyBE> GetStockPlannerListDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@SiteID", oDiscrepancyBE.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.StockPlannerName = dr["StockPlannerName"].ToString();
                    oNewDiscrepancyBE.StockPlannerID = Convert.ToInt32(dr["StockPlannerID"].ToString());
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        /// <summary>
        /// Used for getting Vendor Contact details and Purcahse order details
        /// </summary>
        /// <param name="oDiscrepancyBE"></param>
        /// <returns></returns>
        public List<DiscrepancyBE> GetContactPurchaseOrderDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oDiscrepancyBE.PurchaseOrderNumber);
                param[index++] = new SqlParameter("@Order_raised", oDiscrepancyBE.PurchaseOrder.Order_raised);
                param[index++] = new SqlParameter("@SiteID", oDiscrepancyBE.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"].ToString());
                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["Purchase_order"] != DBNull.Value ? dr["Purchase_order"].ToString() : "";

                    //Vendor Detail
                    oNewDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewDiscrepancyBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : (int?)null;
                    oNewDiscrepancyBE.VendorNoName = dr["VendorNo"] != DBNull.Value ? dr["VendorNo"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VendorContactNumber = dr["VendorContactNumber"] != DBNull.Value ? dr["VendorContactNumber"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VendorContactFax = dr["VendorFax"] != DBNull.Value ? dr["VendorFax"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VendorContactEmail = dr["VendorEmail"] != DBNull.Value ? dr["VendorEmail"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.NewCurrencyID = dr["NewCurrencyID"] != DBNull.Value ? Convert.ToInt32(dr["NewCurrencyID"]) : (int?)null;

                    //Stock Planner Detail
                    oNewDiscrepancyBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"]) : (int?)null;
                    oNewDiscrepancyBE.StockPlannerNO = dr["StockPlannerNumber"] != DBNull.Value ? dr["StockPlannerNumber"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerContact = dr["StockPlannerContact"] != DBNull.Value ? dr["StockPlannerContact"].ToString() : "";

                    //Purchase Order Detail
                    oNewDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oNewDiscrepancyBE.PurchaseOrder.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"].ToString());
                    oNewDiscrepancyBE.PurchaseOrder.Line_No = dr["Line_No"] != DBNull.Value ? Convert.ToInt32(dr["Line_No"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.PurchaseOrder.Direct_code = dr["Direct_code"] != DBNull.Value ? dr["Direct_code"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrder.OD_Code = dr["OD_Code"] != DBNull.Value ? dr["OD_Code"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrder.Vendor_Code = dr["Vendor_Code"] != DBNull.Value ? dr["Vendor_Code"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrder.ProductDescription = dr["Product_description"] != DBNull.Value ? dr["Product_description"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrder.UOM = dr["UOM"] != DBNull.Value ? dr["UOM"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrder.Outstanding_Qty = dr["Outstanding_Qty"] != DBNull.Value ? dr["Outstanding_Qty"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrder.Original_quantity = dr["Original_quantity"] != DBNull.Value ? dr["Original_quantity"].ToString() : "";

                    if (dr.Table.Columns.Contains("POItemCost"))
                        oNewDiscrepancyBE.PurchaseOrder.PO_cost = dr["POItemCost"] != DBNull.Value ? Convert.ToDecimal(dr["POItemCost"]) : (decimal?)null;

                    if (dr.Table.Columns.Contains("POTotalCost"))
                        oNewDiscrepancyBE.PurchaseOrder.PO_Totalcost = dr["POTotalCost"] != DBNull.Value ? Convert.ToDecimal(dr["POTotalCost"]) : (decimal?)null;

                    if (dr.Table.Columns.Contains("StockPlannerEmailID"))
                        oNewDiscrepancyBE.StockPlannerEmailID = dr["StockPlannerEmailID"] != DBNull.Value ? dr["StockPlannerEmailID"].ToString() : string.Empty;
                    else
                        oNewDiscrepancyBE.StockPlannerEmailID = string.Empty;
                    oNewDiscrepancyBE.IncorrectAddress = dr["IncorrectAddress"].ToString();
                    oNewDiscrepancyBE.IncorrectPackLabour = dr["IncorrectPackLabour"].ToString();
                    oNewDiscrepancyBE.FailedPalletSpec = dr["FailedPalletSpec"].ToString();
                    oNewDiscrepancyBE.QualityIssueLabour = dr["QualityIssueLabour"].ToString();
                    oNewDiscrepancyBE.PrematureInvoice = dr["PrematureInvoice"].ToString();

                    if (dr.Table.Columns.Contains("AccountPaybleName"))
                        oNewDiscrepancyBE.AccountPaybleName = dr["AccountPaybleName"] != DBNull.Value ? Convert.ToString(dr["AccountPaybleName"]) : string.Empty;

                    if (dr.Table.Columns.Contains("AccountPayblePhoneNo"))
                        oNewDiscrepancyBE.AccountPayblePhoneNo = dr["AccountPayblePhoneNo"] != DBNull.Value ? Convert.ToString(dr["AccountPayblePhoneNo"]) : string.Empty;


                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> CheckVendorChargeStatusDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDiscrepancyBE.IncorrectAddress = dr["IncorrectAddress"].ToString();
                    oNewDiscrepancyBE.IncorrectPackLabour = dr["IncorrectPackLabour"].ToString();
                    oNewDiscrepancyBE.FailedPalletSpec = dr["FailedPalletSpec"].ToString();
                    oNewDiscrepancyBE.QualityIssueLabour = dr["QualityIssueLabour"].ToString();
                    oNewDiscrepancyBE.PrematureInvoice = dr["PrematureInvoice"].ToString();
                    oNewDiscrepancyBE.DiscrepancyTypeID = Convert.ToInt32(dr["DiscrepancyTypeID"].ToString());
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }
        public string addDiscrepancyLogDAL(DiscrepancyBE oDiscrepancyBE)
        {
            string sResult = "0";
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[37];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@SiteID", oDiscrepancyBE.SiteID);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.User.UserID);
                param[index++] = new SqlParameter("@POSiteID", oDiscrepancyBE.POSiteID);
                param[index++] = new SqlParameter("@StockPlannerID", oDiscrepancyBE.StockPlannerID);
                param[index++] = new SqlParameter("@DiscrepancyTypeID", oDiscrepancyBE.DiscrepancyTypeID);
                param[index++] = new SqlParameter("@DiscrepancyLogDate", oDiscrepancyBE.DiscrepancyLogDate);
                param[index++] = new SqlParameter("@PurchaseOrderNumberID", oDiscrepancyBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@Purchase_order", oDiscrepancyBE.PurchaseOrderNumber);
                param[index++] = new SqlParameter("@PurchaseOrderDate", oDiscrepancyBE.PurchaseOrderDate);
                param[index++] = new SqlParameter("@InternalComments", oDiscrepancyBE.InternalComments);
                param[index++] = new SqlParameter("@DeliveryNoteNumber", oDiscrepancyBE.DeliveryNoteNumber);
                param[index++] = new SqlParameter("@DeliveryArrivedDate", oDiscrepancyBE.DeliveryArrivedDate);
                param[index++] = new SqlParameter("@CommunicationType", oDiscrepancyBE.CommunicationType);
                param[index++] = new SqlParameter("@CommunicationTo", oDiscrepancyBE.CommunicationTo);
                param[index++] = new SqlParameter("@VendorID", oDiscrepancyBE.VendorID);
                param[index++] = new SqlParameter("@CarrierID", oDiscrepancyBE.CarrierID);

                param[index++] = new SqlParameter("@PersentationIssueComment", oDiscrepancyBE.PersentationIssueComment);
                param[index++] = new SqlParameter("@QualityIssueComment", oDiscrepancyBE.QualityIssueComment);
                param[index++] = new SqlParameter("@FailPalletSpecificationComment", oDiscrepancyBE.FailPalletSpecificationComment);
                param[index++] = new SqlParameter("@PrematureReceiptInvoiceComment", oDiscrepancyBE.PrematureReceiptInvoiceComment);
                param[index++] = new SqlParameter("@GenericDiscrepancyActionRequired", oDiscrepancyBE.GenericDiscrepancyActionRequired);
                param[index++] = new SqlParameter("@OfficeDepotContactName", oDiscrepancyBE.OfficeDepotContactName);
                param[index++] = new SqlParameter("@OfficeDepotContactPhone", oDiscrepancyBE.OfficeDepotContactPhone);
                param[index++] = new SqlParameter("@Freight_Charges", oDiscrepancyBE.Freight_Charges);
                param[index++] = new SqlParameter("@NumberOfPallets", oDiscrepancyBE.NumberOfPallets);
                param[index++] = new SqlParameter("@NoEscalation", oDiscrepancyBE.NoEscalation);
                param[index++] = new SqlParameter("@NoPaperworkComment", oDiscrepancyBE.NoPaperworkComment);
                param[index++] = new SqlParameter("@GenericDiscrepancyIssueComment", oDiscrepancyBE.GenericDiscrepancyIssueComment);
                param[index++] = new SqlParameter("@PickersName", oDiscrepancyBE.PickersName);
                param[index++] = new SqlParameter("@Location", oDiscrepancyBE.Location);
                param[index++] = new SqlParameter("@GoodsReturnedDriver", oDiscrepancyBE.GoodsReturnedDriver);
                param[index++] = new SqlParameter("@QualityIssueLabourCostCentre", oDiscrepancyBE.QualityIssueLabourCostCentre);
                param[index++] = new SqlParameter("@ItemNotOnPOComment", oDiscrepancyBE.ItemNotOnPOComment);
                param[index++] = new SqlParameter("@StockPlannerIDIfNotAbsent", oDiscrepancyBE.StockPlannerIDIfNotAbsent);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    sResult = Result.Tables[0].Rows[0][0].ToString();
                else
                    sResult = "0";

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }
        public string updateDiscrepancyLocationDAL(DiscrepancyBE oDiscrepancyBE)
        {
            string sResult = "0";
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@Location", oDiscrepancyBE.Location);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.UserID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    sResult = Result.Tables[0].Rows[0][0].ToString();
                else
                    sResult = "0";

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }
        public string GetEsclationTypeDAL(DiscrepancyBE oDiscrepancyBE)
        {
            string sResult = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorSubsToCharges", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    sResult = Result.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }
        public string GetCarrierDAL(DiscrepancyBE oDiscrepancyBE)
        {
            string sResult = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    sResult = Result.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }
        public string GetVendorSubscriptionStatusDAL(DiscrepancyBE oDiscrepancyBE)
        {
            string sResult = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorSubsToCharges", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    sResult = Result.Tables[0].Rows[0][1].ToString();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }
        public int? addDiscrepancyItemDAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[32];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@PurchaseOrderNumberID", oDiscrepancyBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@Line_No", oDiscrepancyBE.Line_no);
                param[index++] = new SqlParameter("@ODSKUCode", oDiscrepancyBE.ODSKUCode);
                param[index++] = new SqlParameter("@DirectCode", oDiscrepancyBE.DirectCode);
                param[index++] = new SqlParameter("@VendorCode", oDiscrepancyBE.VendorCode);
                param[index++] = new SqlParameter("@ProductDescription", oDiscrepancyBE.ProductDescription);
                param[index++] = new SqlParameter("@OriginalPOQuantity", oDiscrepancyBE.OriginalQuantity);
                param[index++] = new SqlParameter("@OutstandingQuantity", oDiscrepancyBE.OutstandingQuantity);
                param[index++] = new SqlParameter("@UOM", oDiscrepancyBE.UOM);
                param[index++] = new SqlParameter("@DeliveryNoteQuantity", oDiscrepancyBE.DeliveredNoteQuantity);
                param[index++] = new SqlParameter("@DeliveredQuantity", oDiscrepancyBE.DeliveredQuantity);
                param[index++] = new SqlParameter("@OversQuantity", oDiscrepancyBE.OversQuantity);
                param[index++] = new SqlParameter("@ShortageQuantity", oDiscrepancyBE.ShortageQuantity);
                param[index++] = new SqlParameter("@DamageQuantity", oDiscrepancyBE.DamageQuantity);
                param[index++] = new SqlParameter("@DamageDescription", oDiscrepancyBE.DamageDescription);
                param[index++] = new SqlParameter("@AmendedQuantity", oDiscrepancyBE.AmendedQuantity);
                param[index++] = new SqlParameter("@NoPOEscalate", oDiscrepancyBE.NoPOEscalate);
                param[index++] = new SqlParameter("@PackSizeOrdered", oDiscrepancyBE.PackSizeOrdered);
                param[index++] = new SqlParameter("@PackSizeReceived", oDiscrepancyBE.PackSizeReceived);
                param[index++] = new SqlParameter("@WrongPackReceived", oDiscrepancyBE.WrongPackReceived);
                param[index++] = new SqlParameter("@CorrectODSKUCode", oDiscrepancyBE.CorrectODSKUCode);
                param[index++] = new SqlParameter("@CorrectVendorCode", oDiscrepancyBE.CorrectVendorCode);
                param[index++] = new SqlParameter("@CorrectProductDescription", oDiscrepancyBE.CorrectProductDescription);
                param[index++] = new SqlParameter("@CorrectUOM", oDiscrepancyBE.CorrectUOM);
                param[index++] = new SqlParameter("@ShuttleType", oDiscrepancyBE.ShuttleType);
                param[index++] = new SqlParameter("@DiscrepancyQty", oDiscrepancyBE.DiscrepancyQty);
                param[index++] = new SqlParameter("@ChaseQuantity", oDiscrepancyBE.ChaseQuantity);
                param[index++] = new SqlParameter("@InvoiceQTY", oDiscrepancyBE.InvoiceQty);
                param[index++] = new SqlParameter("@ReceiptedQty", oDiscrepancyBE.ReceiptedQty);
                param[index++] = new SqlParameter("@QtyToBeSelected", oDiscrepancyBE.QtyToBeSelected);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataSet getLanguageIDforCommunicationDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@VendorID", oDiscrepancyMailBE.vendorID);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyMailBE.UserID);
                param[index++] = new SqlParameter("@sentTo", oDiscrepancyMailBE.sentTo);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetDiscrepancyDetailDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@discrepancyLogID", oDiscrepancyMailBE.discrepancyLogID);
                param[index++] = new SqlParameter("@CommunicationLevel", oDiscrepancyMailBE.communicationLevel);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetDiscrepancyCommunicationDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@discrepancyCommunicationID", oDiscrepancyMailBE.discrepancyCommunicationID);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetDiscrepancyCommunicationItemDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@discrepancyCommunicationID", oDiscrepancyMailBE.discrepancyCommunicationID);
                param[index++] = new SqlParameter("@languageID", oDiscrepancyMailBE.languageID);
                param[index++] = new SqlParameter("@CommunicationLevel", oDiscrepancyMailBE.communicationLevel);
                param[index++] = new SqlParameter("@SentTo", oDiscrepancyMailBE.sentTo);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetDiscrepancyImagesDAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public int? saveDiscrepancyMailDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            int? Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@discrepancyLogID", oDiscrepancyMailBE.discrepancyLogID);
                param[index++] = new SqlParameter("@sentTo", oDiscrepancyMailBE.sentTo);
                param[index++] = new SqlParameter("@mailBody", oDiscrepancyMailBE.mailBody);
                param[index++] = new SqlParameter("@mailSubject", oDiscrepancyMailBE.mailSubject);
                param[index++] = new SqlParameter("@sentDate", oDiscrepancyMailBE.sentDate);
                param[index++] = new SqlParameter("@languageID", oDiscrepancyMailBE.languageID);
                param[index++] = new SqlParameter("@communicationLevel", oDiscrepancyMailBE.communicationLevel);
                param[index++] = new SqlParameter("@MailSentInLanguage", oDiscrepancyMailBE.MailSentInLanguage);


                if (oDiscrepancyMailBE.QueryDiscrepancy.QueryDiscrepancyID != null)
                    param[index++] = new SqlParameter("@QueryDiscrepancyID", oDiscrepancyMailBE.QueryDiscrepancy.QueryDiscrepancyID);

                param[index++] = new SqlParameter("@IsFromQuery", oDiscrepancyMailBE.IsFromQuery);

                object oResult = SqlHelper.ExecuteScalar(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
                if (oResult != null)
                    Result = Convert.ToInt32(oResult);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public int? saveDiscrepancyLetterDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            int? Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@discrepancyLogID", oDiscrepancyMailBE.discrepancyLogID);
                param[index++] = new SqlParameter("@DiscrepancyLetterID", oDiscrepancyMailBE.DiscrepancyLetterID);
                param[index++] = new SqlParameter("@letterBody", oDiscrepancyMailBE.LetterBody);
                param[index++] = new SqlParameter("@loggedDate", oDiscrepancyMailBE.LoggedDate);

                object oResult = SqlHelper.ExecuteScalar(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
                if (oResult != null)
                    Result = Convert.ToInt32(oResult);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetDiscrepancyLetterDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLetterID", oDiscrepancyMailBE.DiscrepancyLetterID);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public void errorLogDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@discrepancyLogID", oDiscrepancyMailBE.discrepancyLogID);
                param[index++] = new SqlParameter("@sentTo", oDiscrepancyMailBE.sentTo);
                param[index++] = new SqlParameter("@mailBody", oDiscrepancyMailBE.mailBody);
                param[index++] = new SqlParameter("@mailSubject", oDiscrepancyMailBE.mailSubject);
                param[index++] = new SqlParameter("@sentDate", oDiscrepancyMailBE.sentDate);
                param[index++] = new SqlParameter("@errorDescription", oDiscrepancyMailBE.errorDescription);
                param[index++] = new SqlParameter("@Status", oDiscrepancyMailBE.Status);

                object oResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public List<DiscrepancyBE> GetODProductDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@ODSKUCode", oDiscrepancyBE.ODSKUCode);
                param[index++] = new SqlParameter("@SiteId", oDiscrepancyBE.SiteID);
                param[index++] = new SqlParameter("@PurchaseOrder", oDiscrepancyBE.PurchaseOrderNumber);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spGetODProductDetails", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.ODSKUCode = dr["ODSKUCode"] != DBNull.Value ? dr["ODSKUCode"].ToString() : "";
                    oNewDiscrepancyBE.DirectCode = dr["Direct_code"] != DBNull.Value ? dr["Direct_code"].ToString() : "";
                    oNewDiscrepancyBE.VendorCode = dr["Vendor_Code"] != DBNull.Value ? dr["Vendor_Code"].ToString() : "";
                    oNewDiscrepancyBE.ProductDescription = dr["Product_description"] != DBNull.Value ? dr["Product_description"].ToString() : "";
                    oNewDiscrepancyBE.UOM = dr["UOM"] != DBNull.Value ? dr["UOM"].ToString() : "";
                    if (dr.Table.Columns.Contains("SiteID"))
                        oNewDiscrepancyBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    if (dr.Table.Columns.Contains("OutstandingQuantity"))
                        oNewDiscrepancyBE.OutstandingQuantity = dr["OutstandingQuantity"] != DBNull.Value ? Convert.ToInt32(dr["OutstandingQuantity"]) : 0;
                    if (dr.Table.Columns.Contains("OriginalQuantity"))
                        oNewDiscrepancyBE.OriginalQuantity = dr["OriginalQuantity"] != DBNull.Value ? Convert.ToInt32(dr["OriginalQuantity"]) : 0;

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetODProductDetailsBySiteVendorDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@ODSKUCode", oDiscrepancyBE.ODSKUCode);
                param[index++] = new SqlParameter("@SiteId", oDiscrepancyBE.SiteID);
                param[index++] = new SqlParameter("@VendorId", oDiscrepancyBE.VendorID);
                param[index++] = new SqlParameter("@PurchaseOrder", oDiscrepancyBE.PurchaseOrderNumber);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spGetODProductDetails", param);
                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.ODSKUCode = dr["ODSKUCode"] != DBNull.Value ? dr["ODSKUCode"].ToString() : "";
                    oNewDiscrepancyBE.DirectCode = dr["Direct_code"] != DBNull.Value ? dr["Direct_code"].ToString() : "";
                    oNewDiscrepancyBE.VendorCode = dr["Vendor_Code"] != DBNull.Value ? dr["Vendor_Code"].ToString() : "";
                    oNewDiscrepancyBE.ProductDescription = dr["Product_description"] != DBNull.Value ? dr["Product_description"].ToString() : "";
                    oNewDiscrepancyBE.UOM = dr["UOM"] != DBNull.Value ? dr["UOM"].ToString() : "";
                    if (dr.Table.Columns.Contains("OutstandingQuantity"))
                        oNewDiscrepancyBE.OutstandingQuantity = dr["OutstandingQuantity"] != DBNull.Value ? Convert.ToInt32(dr["OutstandingQuantity"]) : 0;
                    if (dr.Table.Columns.Contains("OriginalQuantity"))
                        oNewDiscrepancyBE.OriginalQuantity = dr["OriginalQuantity"] != DBNull.Value ? Convert.ToInt32(dr["OriginalQuantity"]) : 0;

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyLogDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[25];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.UserID);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@SCDeliveryNoteNumber", oDiscrepancyBE.SCDeliveryNoteNumber);
                param[index++] = new SqlParameter("@SCDiscrepancyDateFrom", oDiscrepancyBE.SCDiscrepancyDateFrom);
                param[index++] = new SqlParameter("@SCDiscrepancyDateTo", oDiscrepancyBE.SCDiscrepancyDateTo);
                param[index++] = new SqlParameter("@SCDiscrepancyTypeID", oDiscrepancyBE.SCDiscrepancyTypeID);
                param[index++] = new SqlParameter("@SCDiscrepancyNo", oDiscrepancyBE.SCDiscrepancyNo);
                param[index++] = new SqlParameter("@SCPurchaseOrderDate", oDiscrepancyBE.SCPurchaseOrderDate);
                param[index++] = new SqlParameter("@SCPurchaseOrderNo", oDiscrepancyBE.SCPurchaseOrderNo);
                param[index++] = new SqlParameter("@SCSelectedSiteIDs", oDiscrepancyBE.SCSelectedSiteIDs);
                param[index++] = new SqlParameter("@SCSelectedVendorIDs", oDiscrepancyBE.SCSelectedVendorIDs);
                param[index++] = new SqlParameter("@SCSPUserID", oDiscrepancyBE.SCSPUserID);
                param[index++] = new SqlParameter("@DiscrepancyStatus", oDiscrepancyBE.DiscrepancyStatus);
                param[index++] = new SqlParameter("@VendorID", oDiscrepancyBE.VendorID);
                param[index++] = new SqlParameter("@IsDelete", oDiscrepancyBE.IsDelete);
                param[index++] = new SqlParameter("@Flag", oDiscrepancyBE.Flag);
                param[index++] = new SqlParameter("@Page", oDiscrepancyBE.Page);
                if (oDiscrepancyBE.User != null)
                    param[index++] = new SqlParameter("@RoleTypeFlag", oDiscrepancyBE.User.RoleTypeFlag);

                if (oDiscrepancyBE.QueryDiscrepancy != null)
                    param[index++] = new SqlParameter("@Query", oDiscrepancyBE.QueryDiscrepancy.Query);

                if (oDiscrepancyBE.UserIdForConsVendors != null)
                    param[index++] = new SqlParameter("@UserIdForConsVendors", oDiscrepancyBE.UserIdForConsVendors);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.EscalationLevel))
                    param[index++] = new SqlParameter("@EscalationLevel", oDiscrepancyBE.EscalationLevel);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.SearchByODSku))
                    param[index++] = new SqlParameter("@SearchByODSku", oDiscrepancyBE.SearchByODSku);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.SearchByCatCode))
                    param[index++] = new SqlParameter("@SearchByCatCode", oDiscrepancyBE.SearchByCatCode);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.SCSelectedSPIDs))
                    param[index++] = new SqlParameter("@SCSelectedSPIDs", oDiscrepancyBE.SCSelectedSPIDs);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                DataTable dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : string.Empty;

                    oNewDiscrepancyBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewDiscrepancyBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : string.Empty;
                    oNewDiscrepancyBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oNewDiscrepancyBE.User.FirstName = dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString() : string.Empty;
                    oNewDiscrepancyBE.User.PhoneNumber = dr["PhoneNumber"] != DBNull.Value ? dr["PhoneNumber"].ToString() : string.Empty;
                    oNewDiscrepancyBE.StockPlannerNO = dr["StockPlanner"] != DBNull.Value ? dr["StockPlanner"].ToString() : string.Empty;
                    oNewDiscrepancyBE.StockPlannerContact = dr["StockContactNo"] != DBNull.Value ? dr["StockContactNo"].ToString() : string.Empty;
                    oNewDiscrepancyBE.StockPlannerName = dr["StockPlannerName"] != DBNull.Value ? dr["StockPlannerName"].ToString() : string.Empty;
                    oNewDiscrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.ProductDescription = dr["DiscrepancyDiscripton"] != DBNull.Value ? dr["DiscrepancyDiscripton"].ToString() : string.Empty;
                    oNewDiscrepancyBE.DiscrepancyLogDate = dr["DiscrepancyLogDate"] != DBNull.Value ? Convert.ToDateTime(dr["DiscrepancyLogDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.DiscrepancyStatus = dr["DiscrepancyStatus"] != DBNull.Value ? dr["DiscrepancyStatus"].ToString() : string.Empty;
                    oNewDiscrepancyBE.DiscrepancyStatusDiscreption = dr["DiscrepancyStatusDiscreption"] != DBNull.Value ? dr["DiscrepancyStatusDiscreption"].ToString() : string.Empty;
                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["PurchaseOrderNumber"] != DBNull.Value ? dr["PurchaseOrderNumber"].ToString() : string.Empty;
                    oNewDiscrepancyBE.PurchaseOrderDate = dr["PurchaseOrderDate"] != DBNull.Value ? Convert.ToDateTime(dr["PurchaseOrderDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.InternalComments = dr["InternalComments"] != DBNull.Value ? dr["InternalComments"].ToString() : string.Empty;
                    oNewDiscrepancyBE.DeliveryNoteNumber = dr["DeliveryNoteNumber"] != DBNull.Value ? dr["DeliveryNoteNumber"].ToString() : string.Empty;
                    oNewDiscrepancyBE.DeliveryArrivedDate = dr["DeliveryArrivedDate"] != DBNull.Value ? Convert.ToDateTime(dr["DeliveryArrivedDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.CommunicationType = dr["CommunicationType"] != DBNull.Value ? Convert.ToChar(dr["CommunicationType"]) : (char?)null;
                    oNewDiscrepancyBE.CommunicationTo = dr["CommunicationTo"] != DBNull.Value ? dr["CommunicationTo"].ToString() : string.Empty;
                    oNewDiscrepancyBE.VendorNoName = dr["VendorNoName"] != DBNull.Value ? dr["VendorNoName"].ToString() : string.Empty;
                    oNewDiscrepancyBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewDiscrepancyBE.Vendor.VendorContactNumber = dr["VendorContactNumber"] != DBNull.Value ? dr["VendorContactNumber"].ToString() : string.Empty;
                    oNewDiscrepancyBE.CarrierName = dr["CarrierName"] != DBNull.Value ? dr["CarrierName"].ToString() : string.Empty;
                    oNewDiscrepancyBE.GoodsReturnedDriver = dr["GoodsReturnedDriver"] != DBNull.Value ? Convert.ToBoolean(dr["GoodsReturnedDriver"]) : false;
                    oNewDiscrepancyBE.Location = dr["Location"] != DBNull.Value ? dr["Location"].ToString() : string.Empty;
                    oNewDiscrepancyBE.PersentationIssueComment = dr["PersentationIssueComment"] != DBNull.Value ? dr["PersentationIssueComment"].ToString() : string.Empty;
                    oNewDiscrepancyBE.QualityIssueComment = dr["QualityIssueComment"] != DBNull.Value ? dr["QualityIssueComment"].ToString() : "";
                    oNewDiscrepancyBE.FailPalletSpecificationComment = dr["FailPalletSpecificationComment"] != DBNull.Value ? dr["FailPalletSpecificationComment"].ToString() : string.Empty;
                    oNewDiscrepancyBE.PrematureReceiptInvoiceComment = dr["PrematureReceiptInvoiceComment"] != DBNull.Value ? dr["PrematureReceiptInvoiceComment"].ToString() : string.Empty;
                    oNewDiscrepancyBE.GenericDiscrepancyActionRequired = dr["GenericDiscrepancyActionRequired"] != DBNull.Value ? dr["GenericDiscrepancyActionRequired"].ToString() : string.Empty;
                    oNewDiscrepancyBE.OfficeDepotContactName = dr["OfficeDepotContactName"] != DBNull.Value ? dr["OfficeDepotContactName"].ToString() : string.Empty;
                    oNewDiscrepancyBE.OfficeDepotContactPhone = dr["OfficeDepotContactPhone"] != DBNull.Value ? dr["OfficeDepotContactPhone"].ToString() : string.Empty;
                    oNewDiscrepancyBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    oNewDiscrepancyBE.POSiteID = dr["POSiteID"] != DBNull.Value ? Convert.ToInt32(dr["POSiteID"]) : 0;
                    oNewDiscrepancyBE.POSiteName = dr["POSiteName"] != DBNull.Value ? Convert.ToString(dr["POSiteName"]) : string.Empty;
                    oNewDiscrepancyBE.Freight_Charges = dr["Freight_Charges"] != DBNull.Value ? Convert.ToDecimal(dr["Freight_Charges"].ToString()) : (decimal?)null;
                    oNewDiscrepancyBE.NumberOfPallets = dr["NumberOfPallets"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallets"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.TotalLabourCost = dr["TotalLabourCost"] != DBNull.Value ? Convert.ToDecimal(dr["TotalLabourCost"].ToString()) : (decimal?)null;
                    oNewDiscrepancyBE.StockPlannerEmailID = dr["StockPlannerEmailID"] != DBNull.Value ? Convert.ToString(dr["StockPlannerEmailID"]) : null;

                    oNewDiscrepancyBE.StockPlannerUserID = dr["SPDUserID"] != DBNull.Value ? Convert.ToInt32(dr["SPDUserID"]) : (int?)null;

                    oNewDiscrepancyBE.CreatedById = dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : (int?)null;

                    oNewDiscrepancyBE.GoodsIn = dr["GoodsIn"] != DBNull.Value ? Convert.ToString(dr["GoodsIn"]) : string.Empty;

                    if (dr.Table.Columns.Contains("TotalRecords"))
                    {
                        oNewDiscrepancyBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;
                    }

                    oNewDiscrepancyBE.GoodsInContactNo = dr["GoodsInContactNo"] != DBNull.Value ? Convert.ToString(dr["GoodsInContactNo"]) : string.Empty;

                    oNewDiscrepancyBE.GenericDiscrepancyIssueComment = dr["GenericDiscrepancyIssueComment"] != DBNull.Value ? dr["GenericDiscrepancyIssueComment"].ToString() : string.Empty;

                    if (oDiscrepancyBE.User != null && oDiscrepancyBE.User.RoleTypeFlag != null && !string.Equals(oDiscrepancyBE.User.RoleTypeFlag, "A"))
                    {
                        oNewDiscrepancyBE.DiscrepancyWorkFlowID = dr["DiscrepancyWorkflowID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyWorkflowID"]) : (int?)null;
                    }

                    if (dr.Table.Columns.Contains("GI"))
                        oNewDiscrepancyBE.GI = dr["GI"] != DBNull.Value ? Convert.ToString(dr["GI"]) : string.Empty;

                    if (dr.Table.Columns.Contains("INV"))
                        oNewDiscrepancyBE.INV = dr["INV"] != DBNull.Value ? Convert.ToString(dr["INV"]) : string.Empty;

                    if (dr.Table.Columns.Contains("AP"))
                        oNewDiscrepancyBE.AP = dr["AP"] != DBNull.Value ? Convert.ToString(dr["AP"]) : string.Empty;

                    if (dr.Table.Columns.Contains("VEN"))
                        oNewDiscrepancyBE.VEN = dr["VEN"] != DBNull.Value ? Convert.ToString(dr["VEN"]) : string.Empty;

                    if (dr.Table.Columns.Contains("MED"))
                        oNewDiscrepancyBE.MED = dr["MED"] != DBNull.Value ? Convert.ToString(dr["MED"]) : string.Empty;

                    if (dr.Table.Columns.Contains("PickersName"))
                        oNewDiscrepancyBE.PickersName = dr["PickersName"] != DBNull.Value ? Convert.ToString(dr["PickersName"]) : string.Empty;

                    if (dr.Table.Columns.Contains("Query"))
                    {
                        oNewDiscrepancyBE.QueryDiscrepancy = new DISLog_QueryDiscrepancyBE();
                        oNewDiscrepancyBE.QueryDiscrepancy.QueryDiscrepancyID = dr["QueryDiscrepancyID"] != DBNull.Value ? Convert.ToInt32(dr["QueryDiscrepancyID"]) : 0;
                        oNewDiscrepancyBE.QueryDiscrepancy.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : string.Empty;
                    }

                    if (dr.Table.Columns.Contains("JobTitle"))
                        oNewDiscrepancyBE.JobTitle = dr["JobTitle"] != DBNull.Value ? Convert.ToString(dr["JobTitle"]) : string.Empty;

                    if (dr.Table.Columns.Contains("AccountPaybleName"))
                        oNewDiscrepancyBE.AccountPaybleName = dr["AccountPaybleName"] != DBNull.Value ? Convert.ToString(dr["AccountPaybleName"]) : string.Empty;
                    if (dr.Table.Columns.Contains("PTotal"))
                        oNewDiscrepancyBE.DiscrepancyCharge = dr["PTotal"] != DBNull.Value ? Convert.ToString(dr["PTotal"]) : string.Empty;

                    if (dr.Table.Columns.Contains("AccountPayblePhoneNo"))
                        oNewDiscrepancyBE.AccountPayblePhoneNo = dr["AccountPayblePhoneNo"] != DBNull.Value ? Convert.ToString(dr["AccountPayblePhoneNo"]) : string.Empty;

                    if (dr.Table.Columns.Contains("QtyToBeSelected"))
                        oNewDiscrepancyBE.QtyToBeSelected = dr["QtyToBeSelected"] != DBNull.Value ? Convert.ToString(dr["QtyToBeSelected"]) : string.Empty;

                    if (dr.Table.Columns.Contains("CostCenter"))
                    {
                        oNewDiscrepancyBE.Site.CostCenter = dr["CostCenter"] != DBNull.Value ? Convert.ToString(dr["CostCenter"]) : string.Empty;
                    }

                    if (dr.Table.Columns.Contains("ActionRequired"))
                        oNewDiscrepancyBE.ActionRequired = dr["ActionRequired"] != DBNull.Value ?
                                    Convert.ToString(dr["ActionRequired"]) == "AR" ? "Action Required" : "Remedial Action"
                            : string.Empty;

                    if (dr.Table.Columns.Contains("GINActionCategory"))
                        oNewDiscrepancyBE.GINActionCategory = dr["GINActionCategory"] != DBNull.Value ?
                                    Convert.ToString(dr["GINActionCategory"]) == "AR" ? "Action Required" : "Remedial Action"
                            : string.Empty;

                    if (dr.Table.Columns.Contains("InventoryActionCategory"))
                        oNewDiscrepancyBE.InventoryActionCategory = dr["InventoryActionCategory"] != DBNull.Value ?
                                    Convert.ToString(dr["InventoryActionCategory"]) == "AR" ? "Action Required" : "Remedial Action"
                            : string.Empty;

                    if (dr.Table.Columns.Contains("APActionCategory"))
                        oNewDiscrepancyBE.APActionCategory = dr["APActionCategory"] != DBNull.Value ?
                                    Convert.ToString(dr["APActionCategory"]) == "AR" ? "Action Required" : "Remedial Action"
                            : string.Empty;

                    if (dr.Table.Columns.Contains("VendorActionCategory"))
                        oNewDiscrepancyBE.VendorActionCategory = dr["VendorActionCategory"] != DBNull.Value ?
                                    Convert.ToString(dr["VendorActionCategory"]) == "AR" ? "Action Required" : "Remedial Action"
                            : string.Empty;

                    if (dr.Table.Columns.Contains("ActionComments"))
                    {
                        oNewDiscrepancyBE.ActionComments = dr["ActionComments"] != DBNull.Value
                            ? Convert.ToString(dr["ActionComments"])
                            : string.Empty;
                    }
                    if (dr.Table.Columns.Contains("Decision"))
                    {
                        oNewDiscrepancyBE.Decision = dr["Decision"] != DBNull.Value
                            ? Convert.ToString(dr["Decision"])
                            : string.Empty;
                    }



                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }
        public DataTable GetDiscrepancyLogDetailDAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[25];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.UserID);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@SCDeliveryNoteNumber", oDiscrepancyBE.SCDeliveryNoteNumber);
                param[index++] = new SqlParameter("@SCDiscrepancyDateFrom", oDiscrepancyBE.SCDiscrepancyDateFrom);
                param[index++] = new SqlParameter("@SCDiscrepancyDateTo", oDiscrepancyBE.SCDiscrepancyDateTo);
                param[index++] = new SqlParameter("@SCDiscrepancyTypeID", oDiscrepancyBE.SCDiscrepancyTypeID);
                param[index++] = new SqlParameter("@SCDiscrepancyNo", oDiscrepancyBE.SCDiscrepancyNo);
                param[index++] = new SqlParameter("@SCPurchaseOrderDate", oDiscrepancyBE.SCPurchaseOrderDate);
                param[index++] = new SqlParameter("@SCPurchaseOrderNo", oDiscrepancyBE.SCPurchaseOrderNo);
                param[index++] = new SqlParameter("@SCSelectedSiteIDs", oDiscrepancyBE.SCSelectedSiteIDs);
                param[index++] = new SqlParameter("@SCSelectedVendorIDs", oDiscrepancyBE.SCSelectedVendorIDs);
                param[index++] = new SqlParameter("@SCSPUserID", oDiscrepancyBE.SCSPUserID);
                param[index++] = new SqlParameter("@DiscrepancyStatus", oDiscrepancyBE.DiscrepancyStatus);
                param[index++] = new SqlParameter("@VendorID", oDiscrepancyBE.VendorID);
                param[index++] = new SqlParameter("@IsDelete", oDiscrepancyBE.IsDelete);
                param[index++] = new SqlParameter("@Flag", oDiscrepancyBE.Flag);

                if (oDiscrepancyBE.User != null)
                    param[index++] = new SqlParameter("@RoleTypeFlag", oDiscrepancyBE.User.RoleTypeFlag);

                if (oDiscrepancyBE.QueryDiscrepancy != null)
                    param[index++] = new SqlParameter("@Query", oDiscrepancyBE.QueryDiscrepancy.Query);

                if (oDiscrepancyBE.UserIdForConsVendors != null)
                    param[index++] = new SqlParameter("@UserIdForConsVendors", oDiscrepancyBE.UserIdForConsVendors);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.EscalationLevel))
                    param[index++] = new SqlParameter("@EscalationLevel", oDiscrepancyBE.EscalationLevel);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.SearchByODSku))
                    param[index++] = new SqlParameter("@SearchByODSku", oDiscrepancyBE.SearchByODSku);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.SearchByCatCode))
                    param[index++] = new SqlParameter("@SearchByCatCode", oDiscrepancyBE.SearchByCatCode);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.SCSelectedSPIDs))
                    param[index++] = new SqlParameter("@SCSelectedSPIDs", oDiscrepancyBE.SCSelectedSPIDs);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return dt;
        }
        public List<DiscrepancyBE> GetDiscrepancyForReassignDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@SCSelectedVendorIDs", oDiscrepancyBE.SCSelectedVendorIDs);
                param[index++] = new SqlParameter("@SCSelectedSPIDs", oDiscrepancyBE.SCSelectedSPIDs);
                param[index++] = new SqlParameter("@CountryId", oDiscrepancyBE.APCountryID);
                param[index++] = new SqlParameter("@DiscrepancyNo", oDiscrepancyBE.VDRNo);
                param[index++] = new SqlParameter("@APID", oDiscrepancyBE.SCSelectedAPIDs);
                param[index++] = new SqlParameter("@isAPFlag", oDiscrepancyBE.IsAPFlag);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "sp_ReassignDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : string.Empty;
                    oNewDiscrepancyBE.StockPlannerName = dr["StockPlannerName"] != DBNull.Value ? dr["StockPlannerName"].ToString() : string.Empty;
                    oNewDiscrepancyBE.VendorNoName = dr["VendorNo"] != DBNull.Value ? dr["VendorNo"].ToString() : string.Empty;
                    oNewDiscrepancyBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"]) : (int?)null;// Convert.ToInt32(dr["StockPlannerID"].ToString());
                    oNewDiscrepancyBE.DiscrepancyType = dr["DiscrepancyType"] != DBNull.Value ? dr["DiscrepancyType"].ToString() : string.Empty;

                    if (dt.Columns.Contains("AccountPaybleName"))
                    {
                        oNewDiscrepancyBE.AccountPaybleName = dr["AccountPaybleName"] != DBNull.Value ? dr["AccountPaybleName"].ToString() : string.Empty;
                    }
                    if (dt.Columns.Contains("APID"))
                    {
                        oNewDiscrepancyBE.UserID = dr["APID"] != DBNull.Value ? Convert.ToInt32(dr["APID"]) : (int?)null;
                    }

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }
        public List<DiscrepancyBE> GetDiscrepancyLocationReportDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@SiteID", oDiscrepancyBE.SiteID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : string.Empty;
                    oNewDiscrepancyBE.StockPlannerName = dr["StockPlanner"] != DBNull.Value ? dr["StockPlanner"].ToString() : string.Empty;
                    oNewDiscrepancyBE.VendorNoName = dr["VendorNoName"] != DBNull.Value ? dr["VendorNoName"].ToString() : string.Empty;
                    oNewDiscrepancyBE.DiscrepancyType = dr["DiscrepancyType"] != DBNull.Value ? dr["DiscrepancyType"].ToString() : string.Empty;
                    oNewDiscrepancyBE.DeliveryNoteNumber = dr["DeliveryNoteNumber"] != DBNull.Value ? dr["DeliveryNoteNumber"].ToString() : string.Empty;
                    oNewDiscrepancyBE.Location = dr["Location"] != DBNull.Value ? dr["Location"].ToString() : string.Empty;
                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["PurchaseOrderNumber"] != DBNull.Value ? dr["PurchaseOrderNumber"].ToString() : string.Empty;
                    oNewDiscrepancyBE.CreatedBy = dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString().Replace("-", " ") : string.Empty;
                    oNewDiscrepancyBE.LogDate = dr["LogDate"] != DBNull.Value ? dr["LogDate"].ToString() : string.Empty;
                    oNewDiscrepancyBE.UpdatedBy = dr["UpdatedBy"] != DBNull.Value ? dr["UpdatedBy"].ToString() : string.Empty;
                    oNewDiscrepancyBE.DiscrepancyTypeID = Convert.ToInt32(dr["DiscrepancyTypeID"].ToString());
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }
        public int? saveReassignDiscrepancyDAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@discrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@StockPlannerId", oDiscrepancyBE.StockPlannerID);
                param[index++] = new SqlParameter("@UserId", oDiscrepancyBE.UserID);
                param[index++] = new SqlParameter("@APID", oDiscrepancyBE.AccountPayableID);
                param[index++] = new SqlParameter("@isAPFlag", oDiscrepancyBE.IsAPFlag);
                object oResult = SqlHelper.ExecuteScalar(DBConnection.Connection, CommandType.StoredProcedure, "sp_ReassignDiscrepancy", param);
                if (oResult != null)
                    Result = Convert.ToInt32(oResult);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public List<DiscrepancyBE> GetDiscrepancyLogSiteVendorDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    //oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    //oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : "";
                    oNewDiscrepancyBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;

                    oNewDiscrepancyBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewDiscrepancyBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oNewDiscrepancyBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteAddressLine1 = dr["SiteAddressLine1"] != DBNull.Value ? dr["SiteAddressLine1"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteAddressLine2 = dr["SiteAddressLine2"] != DBNull.Value ? dr["SiteAddressLine2"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteAddressLine3 = dr["SiteAddressLine3"] != DBNull.Value ? dr["SiteAddressLine3"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteAddressLine4 = dr["SiteAddressLine4"] != DBNull.Value ? dr["SiteAddressLine4"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteAddressLine5 = dr["SiteAddressLine5"] != DBNull.Value ? dr["SiteAddressLine5"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteAddressLine6 = dr["SiteAddressLine6"] != DBNull.Value ? dr["SiteAddressLine6"].ToString() : "";
                    oNewDiscrepancyBE.Site.SitePincode = dr["SitePincode"] != DBNull.Value ? dr["SitePincode"].ToString() : "";

                    oNewDiscrepancyBE.Site.APAddressLine1 = dr["APAddressLine1"] != DBNull.Value ? dr["APAddressLine1"].ToString() : "";
                    oNewDiscrepancyBE.Site.APAddressLine2 = dr["APAddressLine2"] != DBNull.Value ? dr["APAddressLine2"].ToString() : "";
                    oNewDiscrepancyBE.Site.APAddressLine3 = dr["APAddressLine3"] != DBNull.Value ? dr["APAddressLine3"].ToString() : "";
                    oNewDiscrepancyBE.Site.APAddressLine4 = dr["APAddressLine4"] != DBNull.Value ? dr["APAddressLine4"].ToString() : "";
                    oNewDiscrepancyBE.Site.APAddressLine5 = dr["APAddressLine5"] != DBNull.Value ? dr["APAddressLine5"].ToString() : "";
                    oNewDiscrepancyBE.Site.APAddressLine6 = dr["APAddressLine6"] != DBNull.Value ? dr["APAddressLine6"].ToString() : "";
                    oNewDiscrepancyBE.Site.APPincode = dr["APPincode"] != DBNull.Value ? dr["APPincode"].ToString() : "";
                    oNewDiscrepancyBE.Site.SiteMangerUserID = dr["SiteMangerUserID"] != DBNull.Value ? Convert.ToInt32(dr["SiteMangerUserID"]) : (int?)null;

                    //oNewDiscrepancyBE.VendorNoName = dr["VendorNoName"] != DBNull.Value ? dr["VendorNoName"].ToString() : "";
                    oNewDiscrepancyBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewDiscrepancyBE.Vendor.Vendor_No = dr["Vendor_No"] != DBNull.Value ? dr["Vendor_No"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VendorName = dr["Vendor_Name"] != DBNull.Value ? dr["Vendor_Name"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.address1 = dr["address 1"] != DBNull.Value ? dr["address 1"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.address2 = dr["address 2"] != DBNull.Value ? dr["address 2"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.city = dr["city"] != DBNull.Value ? dr["city"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.county = dr["county"] != DBNull.Value ? dr["county"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VMPPIN = dr["VMPPIN"] != DBNull.Value ? dr["VMPPIN"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VMPPOU = dr["VMPPOU"] != DBNull.Value ? dr["VMPPOU"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.Fax_country_code = dr["Fax_country_code"] != DBNull.Value ? dr["Fax_country_code"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.Fax_no = dr["Fax_no"] != DBNull.Value ? dr["Fax_no"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.VendorContactNumber = dr["Vendor Contact Number"] != DBNull.Value ? dr["Vendor Contact Number"].ToString() : "";

                    oNewDiscrepancyBE.DiscrepancyLogDate = dr["DiscrepancyLogDate"] != DBNull.Value ? Convert.ToDateTime(dr["Logdate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : "";
                    oNewDiscrepancyBE.Vendor.ReturnAddress = dr["ReturnAddress"] != DBNull.Value ? dr["ReturnAddress"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyType = dr["DiscrepancyType"] != DBNull.Value ? dr["DiscrepancyType"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.Vendor.Language = dr["VendorLanguage"] != DBNull.Value ? dr["VendorLanguage"].ToString() : "";
                    oNewDiscrepancyBE.DeliveryNoteNumber = dr["DeliveryNoteNumber"] != DBNull.Value ? dr["DeliveryNoteNumber"].ToString() : "";
                    oNewDiscrepancyBE.CommunicationTo = dr["CommunicationTo"] != DBNull.Value ? dr["CommunicationTo"].ToString() : "";
                    oNewDiscrepancyBE.Username = dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerContact = dr["StockContactNo"] != DBNull.Value ? dr["StockContactNo"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerName = dr["StockPlanner"] != DBNull.Value ? dr["StockPlanner"].ToString() : "";
                    oNewDiscrepancyBE.Location = dr["Location"] != DBNull.Value ? dr["Location"].ToString() : "";
                    if (dr.Table.Columns.Contains("PurchaseOrderNumber"))
                    {
                        oNewDiscrepancyBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                        oNewDiscrepancyBE.PurchaseOrder.Purchase_order = dr["PurchaseOrderNumber"] != DBNull.Value ? dr["PurchaseOrderNumber"].ToString() : string.Empty;
                    }
                    if (dr.Table.Columns.Contains("AccountPaybleName"))
                    {
                        oNewDiscrepancyBE.AccountPaybleName = dr["AccountPaybleName"] != DBNull.Value ? dr["AccountPaybleName"].ToString() : string.Empty;
                    }
                    if (dr.Table.Columns.Contains("AccountPayblePhoneNo"))
                    {
                        oNewDiscrepancyBE.AccountPayblePhoneNo = dr["AccountPayblePhoneNo"] != DBNull.Value ? dr["AccountPayblePhoneNo"].ToString() : string.Empty;
                    }
                    //ActionComments
                    if (dr.Table.Columns.Contains("ActionComments"))
                    {
                        oNewDiscrepancyBE.ActionComments = dr["ActionComments"] != DBNull.Value ? dr["ActionComments"].ToString() : string.Empty;
                    }

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyWorkListForCurrentUserDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.User.UserID);
                param[index++] = new SqlParameter("@Role", oDiscrepancyBE.User.RoleName);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@GreaterThan3DaysFlag", oDiscrepancyBE.GreaterThan3DaysFlag);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : "";

                    oNewDiscrepancyBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewDiscrepancyBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : "";


                    oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oNewDiscrepancyBE.User.FirstName = dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerNO = dr["StockPlanner"] != DBNull.Value ? dr["StockPlanner"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerContact = dr["StockContactNo"] != DBNull.Value ? dr["StockContactNo"].ToString() : "";
                    oNewDiscrepancyBE.StockPlannerName = dr["StockPlannerName"] != DBNull.Value ? dr["StockPlannerName"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.ProductDescription = dr["DiscrepancyDiscripton"] != DBNull.Value ? dr["DiscrepancyDiscripton"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyLogDate = dr["DiscrepancyLogDate"] != DBNull.Value ? Convert.ToDateTime(dr["DiscrepancyLogDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.DiscrepancyStatus = dr["DiscrepancyStatus"] != DBNull.Value ? dr["DiscrepancyStatus"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["PurchaseOrderNumber"] != DBNull.Value ? dr["PurchaseOrderNumber"].ToString() : "";
                    oNewDiscrepancyBE.PurchaseOrderDate = dr["PurchaseOrderDate"] != DBNull.Value ? Convert.ToDateTime(dr["PurchaseOrderDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.InternalComments = dr["InternalComments"] != DBNull.Value ? dr["InternalComments"].ToString() : "";
                    oNewDiscrepancyBE.DeliveryNoteNumber = dr["DeliveryNoteNumber"] != DBNull.Value ? dr["DeliveryNoteNumber"].ToString() : "";
                    oNewDiscrepancyBE.DeliveryArrivedDate = dr["DeliveryArrivedDate"] != DBNull.Value ? Convert.ToDateTime(dr["DeliveryArrivedDate"].ToString()) : (DateTime?)null;
                    oNewDiscrepancyBE.CommunicationType = dr["CommunicationType"] != DBNull.Value ? Convert.ToChar(dr["CommunicationType"]) : (char?)null;
                    oNewDiscrepancyBE.CommunicationTo = dr["CommunicationTo"] != DBNull.Value ? dr["CommunicationTo"].ToString() : "";
                    oNewDiscrepancyBE.VendorNoName = dr["VendorNoName"] != DBNull.Value ? dr["VendorNoName"].ToString() : "";
                    oNewDiscrepancyBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewDiscrepancyBE.Vendor.VendorContactNumber = dr["VendorContactNumber"] != DBNull.Value ? dr["VendorContactNumber"].ToString() : "";
                    oNewDiscrepancyBE.CarrierName = dr["CarrierName"] != DBNull.Value ? dr["CarrierName"].ToString() : "";

                    oNewDiscrepancyBE.PersentationIssueComment = dr["PersentationIssueComment"] != DBNull.Value ? dr["PersentationIssueComment"].ToString() : "";
                    oNewDiscrepancyBE.QualityIssueComment = dr["QualityIssueComment"] != DBNull.Value ? dr["QualityIssueComment"].ToString() : "";
                    oNewDiscrepancyBE.FailPalletSpecificationComment = dr["FailPalletSpecificationComment"] != DBNull.Value ? dr["FailPalletSpecificationComment"].ToString() : "";
                    oNewDiscrepancyBE.PrematureReceiptInvoiceComment = dr["PrematureReceiptInvoiceComment"] != DBNull.Value ? dr["PrematureReceiptInvoiceComment"].ToString() : "";
                    oNewDiscrepancyBE.GenericDiscrepancyActionRequired = dr["GenericDiscrepancyActionRequired"] != DBNull.Value ? dr["GenericDiscrepancyActionRequired"].ToString() : "";
                    oNewDiscrepancyBE.OfficeDepotContactName = dr["OfficeDepotContactName"] != DBNull.Value ? dr["OfficeDepotContactName"].ToString() : "";
                    oNewDiscrepancyBE.OfficeDepotContactPhone = dr["OfficeDepotContactPhone"] != DBNull.Value ? dr["OfficeDepotContactPhone"].ToString() : "";
                    oNewDiscrepancyBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    oNewDiscrepancyBE.POSiteID = dr["POSiteID"] != DBNull.Value ? Convert.ToInt32(dr["POSiteID"]) : 0;
                    oNewDiscrepancyBE.DiscrepancyWorkFlowID = dr["DiscrepancyWorkflowID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyWorkflowID"]) : (int?)null;
                    oNewDiscrepancyBE.DiscrepancyStatusDiscreption = dr["DiscrepancyStatusDiscreption"] != DBNull.Value ? dr["DiscrepancyStatusDiscreption"].ToString() : "";

                    if (dr.Table.Columns.Contains("GI"))
                        oNewDiscrepancyBE.GI = dr["GI"] != DBNull.Value ? Convert.ToString(dr["GI"]) : string.Empty;

                    if (dr.Table.Columns.Contains("INV"))
                        oNewDiscrepancyBE.INV = dr["INV"] != DBNull.Value ? Convert.ToString(dr["INV"]) : string.Empty;

                    if (dr.Table.Columns.Contains("AP"))
                        oNewDiscrepancyBE.AP = dr["AP"] != DBNull.Value ? Convert.ToString(dr["AP"]) : string.Empty;

                    if (dr.Table.Columns.Contains("VEN"))
                        oNewDiscrepancyBE.VEN = dr["VEN"] != DBNull.Value ? Convert.ToString(dr["VEN"]) : string.Empty;

                    if (dr.Table.Columns.Contains("MED"))
                        oNewDiscrepancyBE.MED = dr["MED"] != DBNull.Value ? Convert.ToString(dr["MED"]) : string.Empty;

                    if (dr.Table.Columns.Contains("Query"))
                    {
                        oNewDiscrepancyBE.QueryDiscrepancy = new DISLog_QueryDiscrepancyBE();
                        oNewDiscrepancyBE.QueryDiscrepancy.QueryDiscrepancyID = dr["QueryDiscrepancyID"] != DBNull.Value ? Convert.ToInt32(dr["QueryDiscrepancyID"]) : 0;
                        oNewDiscrepancyBE.QueryDiscrepancy.Query = dr["Query"] != DBNull.Value ? Convert.ToString(dr["Query"]) : string.Empty;
                    }

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyItemDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {

            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDiscrepancyBE.Line_no = dr["Line_No"] != DBNull.Value ? Convert.ToInt32(dr["Line_No"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.ODSKUCode = dr["ODSKUCode"] != DBNull.Value ? dr["ODSKUCode"].ToString() : "";
                    oNewDiscrepancyBE.DirectCode = dr["DirectCode"] != DBNull.Value ? dr["DirectCode"].ToString() : "";
                    oNewDiscrepancyBE.VendorCode = dr["VendorCode"] != DBNull.Value ? dr["VendorCode"].ToString() : "";
                    oNewDiscrepancyBE.ProductDescription = dr["ProductDescription"] != DBNull.Value ? dr["ProductDescription"].ToString() : "";
                    oNewDiscrepancyBE.OutstandingQuantity = dr["OutstandingQuantity"] != DBNull.Value ? Convert.ToInt32(dr["OutstandingQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.UOM = dr["UOM"] != DBNull.Value ? dr["UOM"].ToString() : "";
                    oNewDiscrepancyBE.DeliveredNoteQuantity = dr["DeliveryNoteQuantity"] != DBNull.Value ? Convert.ToInt32(dr["DeliveryNoteQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.DeliveredQuantity = dr["DeliveredQuantity"] != DBNull.Value ? Convert.ToInt32(dr["DeliveredQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.OversQuantity = dr["OversQuantity"] != DBNull.Value ? Convert.ToInt32(dr["OversQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.ShortageQuantity = dr["ShortageQuantity"] != DBNull.Value ? Convert.ToInt32(dr["ShortageQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.DamageQuantity = dr["DamageQuantity"] != DBNull.Value ? Convert.ToInt32(dr["DamageQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.DamageDescription = dr["DamageDescription"] != DBNull.Value ? dr["DamageDescription"].ToString() : "";
                    oNewDiscrepancyBE.AmendedQuantity = dr["AmendedQuantity"] != DBNull.Value ? Convert.ToInt32(dr["AmendedQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.NoPOEscalate = dr["NoPOEscalate"] != DBNull.Value ? Convert.ToBoolean(dr["NoPOEscalate"].ToString()) : (bool?)null;
                    oNewDiscrepancyBE.PackSizeOrdered = dr["PackSizeOrdered"] != DBNull.Value ? dr["PackSizeOrdered"].ToString() : "";
                    oNewDiscrepancyBE.PackSizeReceived = dr["PackSizeReceived"] != DBNull.Value ? dr["PackSizeReceived"].ToString() : "";
                    oNewDiscrepancyBE.CorrectODSKUCode = dr["CorrectODSKUCode"] != DBNull.Value ? dr["CorrectODSKUCode"].ToString() : "";
                    oNewDiscrepancyBE.CorrectProductDescription = dr["CorrectProductDescription"] != DBNull.Value ? dr["CorrectProductDescription"].ToString() : "";
                    oNewDiscrepancyBE.CorrectUOM = dr["CorrectUOM"] != DBNull.Value ? dr["CorrectUOM"].ToString() : "";
                    oNewDiscrepancyBE.CorrectVendorCode = dr["CorrectVendorCode"] != DBNull.Value ? dr["CorrectVendorCode"].ToString() : "";
                    oNewDiscrepancyBE.OriginalQuantity = dr["OriginalPOQuantity"] != DBNull.Value ? Convert.ToInt32(dr["OriginalPOQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.WrongPackReceived = dr["WrongPackReceived"] != DBNull.Value ? Convert.ToInt32(dr["WrongPackReceived"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.PurchaseOrderID = dr["PurchaseOrderID"] != DBNull.Value ? Convert.ToInt32(dr["PurchaseOrderID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.Comment = dr["InternalComments"] != DBNull.Value ? dr["InternalComments"].ToString() : "";
                    if (dr.Table.Columns.Contains("ShuttleType"))
                        oNewDiscrepancyBE.ShuttleType = dr["ShuttleType"] != DBNull.Value ? dr["ShuttleType"].ToString() : string.Empty;

                    if (dr.Table.Columns.Contains("DiscrepancyQty"))
                        oNewDiscrepancyBE.DiscrepancyQty = dr["DiscrepancyQty"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyQty"].ToString()) : (int?)null;

                    if (dr.Table.Columns.Contains("ChaseQuantity"))
                        oNewDiscrepancyBE.ChaseQuantity = dr["ChaseQuantity"] != DBNull.Value ? Convert.ToInt32(dr["ChaseQuantity"].ToString()) : (int?)null;

                    if (dr.Table.Columns.Contains("InvoiceQty"))
                        oNewDiscrepancyBE.InvoiceQty = dr["InvoiceQty"] != DBNull.Value ? Convert.ToInt32(dr["InvoiceQty"].ToString()) : (int?)null;

                    if (dr.Table.Columns.Contains("ReceiptedQty"))
                        oNewDiscrepancyBE.ReceiptedQty = dr["ReceiptedQty"] != DBNull.Value ? Convert.ToInt32(dr["ReceiptedQty"].ToString()) : (int?)null;

                    if (dr.Table.Columns.Contains("NaxPONo"))
                        oNewDiscrepancyBE.NaxPONo = dr["NaxPONo"] != DBNull.Value ? dr["NaxPONo"].ToString() : string.Empty;

                    if (dr.Table.Columns.Contains("ActualStockPlannerID"))
                        oNewDiscrepancyBE.ActualStockPlannerID = dr["ActualStockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["ActualStockPlannerID"].ToString()) : (int?)null;


                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyTypeDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_DiscrepancyType", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.DiscrepancyTypeID = Convert.ToInt32(dr["DiscrepancyTypeID"].ToString());
                    oNewDiscrepancyBE.DiscrepancyType = dr["DiscrepancyType"].ToString();
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetStockPlannersDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.UserID = Convert.ToInt32(dr["UserID"].ToString());
                    oNewDiscrepancyBE.StockPlannerNO = dr["StockPlannerNo"].ToString();
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetAllPurchaseOrderDateListDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.PurchaseOrder_Order_raised = dr["Order_raised"] != DBNull.Value ? dr["Order_raised"].ToString() : "";

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public int? addWorkFlowHTMLsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[34];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@LoggedDateTime", oDiscrepancyBE.LoggedDateTime);
                param[index++] = new SqlParameter("@LevelNumber", oDiscrepancyBE.LevelNumber);
                param[index++] = new SqlParameter("@GINHTML", oDiscrepancyBE.GINHTML);
                param[index++] = new SqlParameter("@GINActionRequired", oDiscrepancyBE.GINActionRequired);
                param[index++] = new SqlParameter("@GINUserControl", oDiscrepancyBE.GINUserControl);
                param[index++] = new SqlParameter("@INVHTML", oDiscrepancyBE.INVHTML);
                param[index++] = new SqlParameter("@INVActionRequired", oDiscrepancyBE.INVActionRequired);
                param[index++] = new SqlParameter("@INVUserControl", oDiscrepancyBE.INVUserControl);
                param[index++] = new SqlParameter("@APHTML", oDiscrepancyBE.APHTML);
                param[index++] = new SqlParameter("@APActionRequired", oDiscrepancyBE.APActionRequired);
                param[index++] = new SqlParameter("@APUserControl", oDiscrepancyBE.APUserControl);
                param[index++] = new SqlParameter("@VENHTML", oDiscrepancyBE.VENHTML);
                param[index++] = new SqlParameter("@VenActionRequired", oDiscrepancyBE.VenActionRequired);
                param[index++] = new SqlParameter("@VENUserControl", oDiscrepancyBE.VENUserControl);
                param[index++] = new SqlParameter("@CloseDiscrepancy", oDiscrepancyBE.CloseDiscrepancy);
                param[index++] = new SqlParameter("@NextEscalationLevel", oDiscrepancyBE.NextEscalationLevel);

                param[index++] = new SqlParameter("@MEDActionRequired", oDiscrepancyBE.MEDActionRequired);
                param[index++] = new SqlParameter("@MEDUserControl", oDiscrepancyBE.MEDUserControl);
                param[index++] = new SqlParameter("@MEDHTML", oDiscrepancyBE.MEDHTML);
                if (oDiscrepancyBE.QueryDiscrepancy != null)
                    param[index++] = new SqlParameter("@QueryDiscrepancyID", oDiscrepancyBE.QueryDiscrepancy.QueryDiscrepancyID);

                if (oDiscrepancyBE.DELHTML != null)
                    param[index++] = new SqlParameter("@DELHTML", oDiscrepancyBE.DELHTML);

                if (oDiscrepancyBE.ACP001Control != null)
                    param[index++] = new SqlParameter("@ACP001Control", oDiscrepancyBE.ACP001Control);

                if (oDiscrepancyBE.IsACP001Done != null)
                    param[index++] = new SqlParameter("@IsACP001Done", oDiscrepancyBE.IsACP001Done);

                param[index++] = new SqlParameter("@ActionTakenBy", oDiscrepancyBE.ActionTakenBy);
                param[index++] = new SqlParameter("@APActionCategory", oDiscrepancyBE.APActionCategory);
                param[index++] = new SqlParameter("@GINActionCategory", oDiscrepancyBE.GINActionCategory);
                param[index++] = new SqlParameter("@InventoryActionCategory", oDiscrepancyBE.InventoryActionCategory);
                param[index++] = new SqlParameter("@VendorActionCategory", oDiscrepancyBE.VendorActionCategory);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyBE> GetWorkFlowHTMLsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                DataTable dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {

                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.APHTML = dr["APHTML"] != DBNull.Value ? dr["APHTML"].ToString() : "&nbsp;";
                    oNewDiscrepancyBE.VENHTML = dr["VENHTML"] != DBNull.Value ? dr["VENHTML"].ToString() : "&nbsp;";
                    oNewDiscrepancyBE.INVHTML = dr["INVHTML"] != DBNull.Value ? dr["INVHTML"].ToString() : "&nbsp;";
                    oNewDiscrepancyBE.GINHTML = dr["GINHTML"] != DBNull.Value ? dr["GINHTML"].ToString() : "&nbsp;";
                    oNewDiscrepancyBE.MEDHTML = dr["MEDHTML"] != DBNull.Value ? dr["MEDHTML"].ToString() : "&nbsp;";
                    oNewDiscrepancyBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    oNewDiscrepancyBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                    oNewDiscrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"]) : 0;
                    oNewDiscrepancyBE.DiscrepancyStatus = dr["DiscrepancyStatus"] != DBNull.Value ? dr["DiscrepancyStatus"].ToString() : "&nbsp;";
                    oNewDiscrepancyBE.ForceClosedDate = dr["ForceClosedDate"] != DBNull.Value ? dr["ForceClosedDate"].ToString() : null;
                    oNewDiscrepancyBE.Username = dr["Username"] != DBNull.Value ? dr["Username"].ToString() : null;
                    if (dr.Table.Columns.Contains("ForceClosedUserName"))
                        oNewDiscrepancyBE.ForceClosedUserName = dr["ForceClosedUserName"] != DBNull.Value ? dr["ForceClosedUserName"].ToString() : string.Empty;
                    if (dr.Table.Columns.Contains("DELHTML"))
                        oNewDiscrepancyBE.DELHTML = dr["DELHTML"] != DBNull.Value ? dr["DELHTML"].ToString() : string.Empty;

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetUserDetailsForWorkFlowDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.User.UserID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {

                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyLogID = dr["DiscrepancyLogID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyLogID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : (int?)null;
                    oNewDiscrepancyBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["PurchaseOrderNumber"] != DBNull.Value ? dr["DiscrepancyTypeID"].ToString() : "";

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public DataTable GetActionRequiredDetailsForWorkFlowDAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.User.UserID);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@GINActionRequired", oDiscrepancyBE.GINActionRequired);
                param[index++] = new SqlParameter("@INVActionRequired", oDiscrepancyBE.INVActionRequired);
                param[index++] = new SqlParameter("@APActionRequired", oDiscrepancyBE.APActionRequired);
                param[index++] = new SqlParameter("@VenActionRequired", oDiscrepancyBE.VenActionRequired);
                param[index++] = new SqlParameter("@DiscrepancyWorkFlowID", oDiscrepancyBE.DiscrepancyWorkFlowID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public DataTable GetDiscrepancyReturnNoteCountDAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public DataTable GetCheckForGIN003VisibilityDAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@DiscrepancyWorkFlowID", oDiscrepancyBE.DiscrepancyWorkFlowID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public int? UpdateDiscrepancyStatus(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@DiscrepancyStatus", oDiscrepancyBE.DiscrepancyStatus);
                param[index++] = new SqlParameter("@ForceClosedUserId", oDiscrepancyBE.ForceClosedUserId);
                param[index++] = new SqlParameter("@DeletedUserId", oDiscrepancyBE.DeletedUserId);
                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateVendorActionElapsedTimeDAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@VendorActionElapsedTime", oDiscrepancyBE.VendorActionElapsedTime);

                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataTable GetOpenWorkflowEntry(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.User.UserID);
                param[index++] = new SqlParameter("@Role", oDiscrepancyBE.User.RoleName);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public DataSet GetDiscrepancyLogPrintLabelDAL(DiscrepancyBE oDiscrepancyBE2)
        {
            DataSet ds = new DataSet();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE2.Action);

                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE2.DiscrepancyLogID);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);


            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public DataSet GetDiscrepancyLogRePrintDAL(DiscrepancyBE oDiscrepancyBE2)
        {
            DataSet ds = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE2.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE2.DiscrepancyLogID);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public int? AddDiscrepancyReturnNote(DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE)
        {
            int? intResult = null;
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyReturnNoteBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyReturnNoteBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@LoggedDate", oDiscrepancyReturnNoteBE.LoggedDate);
                param[index++] = new SqlParameter("@ReturnNoteBody", oDiscrepancyReturnNoteBE.ReturnNoteBody);

                object obj = SqlHelper.ExecuteScalar(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
                if (obj != null)
                {
                    intResult = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyBE> GetDiscrepancyItemsForReturnNote(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.ODSKUCode = dr["ODSKUCode"] != DBNull.Value ? dr["ODSKUCode"].ToString() : "";
                    oNewDiscrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"]) : (int?)null;
                    oNewDiscrepancyBE.DirectCode = dr["DirectCode"] != DBNull.Value ? dr["DirectCode"].ToString() : "";
                    oNewDiscrepancyBE.ProductDescription = dr["ProductDescription"] != DBNull.Value ? dr["ProductDescription"].ToString() : "";
                    oNewDiscrepancyBE.UOM = dr["UOM"] != DBNull.Value ? dr["UOM"].ToString() : "";
                    oNewDiscrepancyBE.DeliveredQuantity = dr["DeliveredQuantity"] != DBNull.Value ? Convert.ToInt32(dr["DeliveredQuantity"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.ItemVal = dr["Item_Val"] != DBNull.Value ? dr["Item_Val"].ToString() : "";
                    oNewDiscrepancyBE.CollectionAuthNumber = dr["CollectionAuthNumber"] != DBNull.Value ? dr["CollectionAuthNumber"].ToString() : "";
                    if (dr["Original_quantity"] != DBNull.Value)
                    {
                        oNewDiscrepancyBE.OriginalQuantity = dr["Original_quantity"] != DBNull.Value ? Convert.ToInt32(dr["Original_quantity"]) : 0;
                    }
                    if (dr["OutstandingQuantity"] != DBNull.Value)
                    {
                        oNewDiscrepancyBE.OutstandingQuantity = dr["OutstandingQuantity"] != DBNull.Value ? Convert.ToInt32(dr["OutstandingQuantity"]) : 0;
                    }

                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["Purchase_order"] != DBNull.Value ? dr["Purchase_order"].ToString() : "";

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    dt = ds.Tables[1];
                    foreach (DataRow dr in dt.Rows)
                    {
                        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                        oNewDiscrepancyBE.ODSKUCode = dr["ODSKUCode"] != DBNull.Value ? dr["ODSKUCode"].ToString() : "";
                        oNewDiscrepancyBE.DirectCode = dr["DirectCode"] != DBNull.Value ? dr["DirectCode"].ToString() : "";
                        oNewDiscrepancyBE.ProductDescription = dr["ProductDescription"] != DBNull.Value ? dr["ProductDescription"].ToString() : "";
                        oNewDiscrepancyBE.UOM = dr["UOM"] != DBNull.Value ? dr["UOM"].ToString() : "";
                        oNewDiscrepancyBE.DeliveredQuantity = dr["DeliveredQuantity"] != DBNull.Value ? Convert.ToInt32(dr["DeliveredQuantity"].ToString()) : (int?)null;
                        oNewDiscrepancyBE.ItemVal = dr["Item_Val"] != DBNull.Value ? dr["Item_Val"].ToString() : "";
                        oNewDiscrepancyBE.CollectionAuthNumber = dr["CollectionAuthNumber"] != DBNull.Value ? dr["CollectionAuthNumber"].ToString() : "";

                        DiscrepancyBEList.Add(oNewDiscrepancyBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;

        }

        public DiscrepancyReturnNoteBE GetDiscrepancyReturnNote(DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE)
        {
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyReturnNoteBE.Action);
                param[index++] = new SqlParameter("@ReturnNoteID", oDiscrepancyReturnNoteBE.ReturnNoteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (ds != null)
                {
                    DataTable dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
                    foreach (DataRow dr in dt.Rows)
                    {
                        oDiscrepancyReturnNoteBE.ReturnNoteBody = dr["ReturnNoteBody"] != null ? dr["ReturnNoteBody"].ToString() : "";
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oDiscrepancyReturnNoteBE;
        }
        public DiscrepancyReturnNoteBE GetDiscrepancyReturnNoteByLogID(DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE)
        {
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyReturnNoteBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyReturnNoteBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (ds != null)
                {
                    DataTable dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
                    foreach (DataRow dr in dt.Rows)
                    {
                        oDiscrepancyReturnNoteBE.ReturnNoteBody = dr["ReturnNoteBody"] != null ? dr["ReturnNoteBody"].ToString() : "";
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oDiscrepancyReturnNoteBE;
        }
        public DiscrepancyBE GetDiscrepancyReturnNote(DiscrepancyBE oDiscrepancyBE)
        {
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (ds != null)
                {
                    DataTable dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];

                        oDiscrepancyBE.Username = dr["Vendor_Contact_Name"] != null ? dr["Vendor_Contact_Name"].ToString() : "";

                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oDiscrepancyBE;
        }

        public DataSet GetCommunicationDataToSendMailDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetDiscrepancyDetailsDAL(DiscrepancyBE oDiscrepancyBE2)
        {
            DataSet ds = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE2.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE2.DiscrepancyLogID);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE2.UserID);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public int? AddDiscrepancyLogCommentDAL(DiscrepancyBE oDiscrepancy)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancy.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancy.DiscrepancyLogID);
                param[index++] = new SqlParameter("@UserId", oDiscrepancy.UserID);
                param[index++] = new SqlParameter("@CommentDate", oDiscrepancy.CommentDateTime);
                param[index++] = new SqlParameter("@Comment", oDiscrepancy.Comment);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyBE> GetDiscrepancyLogCommentsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.CommentDateTime = dr["CommentDate"] != DBNull.Value ? Convert.ToDateTime(dr["CommentDate"]) : (DateTime?)null;
                    oNewDiscrepancyBE.Username = dr["UserFullName"] != DBNull.Value ? dr["UserFullName"].ToString() : string.Empty;
                    oNewDiscrepancyBE.Comment = dr["Comment"] != DBNull.Value ? dr["Comment"].ToString() : string.Empty;
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public int? addDebitRaiseDAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[21];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@CountryShortName", oDiscrepancyBE.CountryShortName);
                param[index++] = new SqlParameter("@DebitRaseType", oDiscrepancyBE.DebitRaseType);
                param[index++] = new SqlParameter("@Purchase_order", oDiscrepancyBE.PurchaseOrderNumber);
                param[index++] = new SqlParameter("@SiteId", oDiscrepancyBE.SiteID);
                param[index++] = new SqlParameter("@VendorId", oDiscrepancyBE.VendorID);
                param[index++] = new SqlParameter("@InvoiceNo", oDiscrepancyBE.InvoiceNo);
                param[index++] = new SqlParameter("@ReferenceNo", oDiscrepancyBE.ReferenceNo);
                param[index++] = new SqlParameter("@CommunicationEmails", oDiscrepancyBE.CommunicationEmails);
                param[index++] = new SqlParameter("@Comments", oDiscrepancyBE.InternalComments);

                if (oDiscrepancyBE.Currency != null)
                    param[index++] = new SqlParameter("@CurrencyId", oDiscrepancyBE.Currency.CurrencyId);

                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@Status", oDiscrepancyBE.Status);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.UserID);

                if (oDiscrepancyBE.Country != null)
                    param[index++] = new SqlParameter("@CountryID", oDiscrepancyBE.Country.CountryID);

                param[index++] = new SqlParameter("@APActionRef", oDiscrepancyBE.APActionRef);
                param[index++] = new SqlParameter("@ODVatReferenceID", oDiscrepancyBE.ODVatReferenceID);
                param[index++] = new SqlParameter("@VendorVatReference", oDiscrepancyBE.VendorVatReference);
                param[index++] = new SqlParameter("@DebitValue", oDiscrepancyBE.DebitTotalValue);
                param[index++] = new SqlParameter("@PORaisedDate", oDiscrepancyBE.PORaisedDate);

                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? addDebitRaiseItemDAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[17];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DebitRaiseId", oDiscrepancyBE.DebitRaiseId);
                param[index++] = new SqlParameter("@Line_No", oDiscrepancyBE.Line_no);
                param[index++] = new SqlParameter("@ODSKUCode", oDiscrepancyBE.ODSKUCode);
                param[index++] = new SqlParameter("@DirectCode", oDiscrepancyBE.DirectCode);
                param[index++] = new SqlParameter("@VendorCode", oDiscrepancyBE.VendorCode);
                param[index++] = new SqlParameter("@ProductDescription", oDiscrepancyBE.ProductDescription);
                param[index++] = new SqlParameter("@UOM", oDiscrepancyBE.UOM);
                param[index++] = new SqlParameter("@POItemCost", oDiscrepancyBE.POItemCost);
                param[index++] = new SqlParameter("@POTotalCost", oDiscrepancyBE.POTotalCost);
                param[index++] = new SqlParameter("@StateValueDebited", oDiscrepancyBE.StateValueDebited);
                param[index++] = new SqlParameter("@DebitReasonId", oDiscrepancyBE.DebitReasonId);
                param[index++] = new SqlParameter("@OriginalPOQuantity", oDiscrepancyBE.OriginalQuantity);
                param[index++] = new SqlParameter("@OutstandingQuantity", oDiscrepancyBE.OutstandingQuantity);
                param[index++] = new SqlParameter("@InvoicedItemPrice", oDiscrepancyBE.InvoicedItemPrice);
                param[index++] = new SqlParameter("@QtyInvoiced", oDiscrepancyBE.QtyInvoiced);
                param[index++] = new SqlParameter("@PriceDifference", oDiscrepancyBE.PriceDifference);

                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? addDebitRaiseCommunicationDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            int? intResult = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@DebitRaiseId", oDiscrepancyMailBE.DebitRaiseId);
                param[index++] = new SqlParameter("@mailSubject", oDiscrepancyMailBE.mailSubject);
                param[index++] = new SqlParameter("@SentTo", oDiscrepancyMailBE.sentTo);
                param[index++] = new SqlParameter("@mailBody", oDiscrepancyMailBE.mailBody);
                param[index++] = new SqlParameter("@SendByID", oDiscrepancyMailBE.SendByID);
                param[index++] = new SqlParameter("@IsMailSent", oDiscrepancyMailBE.IsMailSent);
                param[index++] = new SqlParameter("@LanguageId", oDiscrepancyMailBE.languageID);
                param[index++] = new SqlParameter("@MailSentInLanguage", oDiscrepancyMailBE.MailSentInLanguage);
                param[index++] = new SqlParameter("@CommTitle", oDiscrepancyMailBE.CommTitle);
                param[index++] = new SqlParameter("@SentToWithLink", oDiscrepancyMailBE.SentToWithLink);

                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyBE> GetAllDebitRaiseDAL(DiscrepancyBE oDiscrepancyBE)
        {
            var lstDiscrepancyBE = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[16];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DebitRaiseId", oDiscrepancyBE.DebitRaiseId);
                param[index++] = new SqlParameter("@DebitNoteNo", oDiscrepancyBE.DebitNoteNo);
                param[index++] = new SqlParameter("@CreatedByIds", oDiscrepancyBE.CreatedByIds);
                param[index++] = new SqlParameter("@CancelByIds", oDiscrepancyBE.CancelByIds);
                param[index++] = new SqlParameter("@VendorIds", oDiscrepancyBE.VendorIds);
                param[index++] = new SqlParameter("@DebitReasonIds", oDiscrepancyBE.DebitReasonIds);
                param[index++] = new SqlParameter("@Status", oDiscrepancyBE.Status);
                param[index++] = new SqlParameter("@DebitRaiseFromDate", oDiscrepancyBE.DebitRaiseFromDate);
                param[index++] = new SqlParameter("@DebitRaiseToDate", oDiscrepancyBE.DebitRaiseToDate);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@PONumbers", oDiscrepancyBE.PONumbers);
                param[index++] = new SqlParameter("@DebitNoteNumbers", oDiscrepancyBE.DebitNoteNumbers);
                param[index++] = new SqlParameter("@UserRoleID", oDiscrepancyBE.UserRoleID);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.UserID);

                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
                foreach (DataRow dr in dt.Rows)
                {
                    var discrepancyBE = new DiscrepancyBE();
                    discrepancyBE.DebitRaiseId = dr["DebitRaiseId"] != DBNull.Value ? Convert.ToInt32(dr["DebitRaiseId"].ToString()) : (int?)null;
                    discrepancyBE.DebitRaiseDate = dr["DebitRaiseDate"] != DBNull.Value ? Convert.ToDateTime(dr["DebitRaiseDate"].ToString()) : (DateTime?)null;
                    discrepancyBE.DebitNoteNo = dr["DebitNoteNo"] != DBNull.Value ? dr["DebitNoteNo"].ToString() : string.Empty;
                    discrepancyBE.DebitRaseType = dr["DebitRaseType"] != DBNull.Value ? dr["DebitRaseType"].ToString() : string.Empty;
                    discrepancyBE.PurchaseOrderNumber = dr["PONumber"] != DBNull.Value ? dr["PONumber"].ToString() : string.Empty;
                    discrepancyBE.PORaisedDate = dr["PORaisedDate"] != DBNull.Value ? Convert.ToDateTime(dr["PORaisedDate"].ToString()) : (DateTime?)null;
                    //discrepancyBE.PurchaseOrderDate = dr["PODate"] != DBNull.Value ? Convert.ToDateTime(dr["PODate"].ToString()) : (DateTime?)null;
                    discrepancyBE.SiteID = dr["SiteId"] != DBNull.Value ? Convert.ToInt32(dr["SiteId"].ToString()) : (int?)null;
                    discrepancyBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    discrepancyBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : string.Empty;
                    discrepancyBE.VendorID = dr["VendorId"] != DBNull.Value ? Convert.ToInt32(dr["VendorId"].ToString()) : (int?)null;
                    discrepancyBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    discrepancyBE.Vendor.Vendor_No = dr["Vendor_No"] != DBNull.Value ? dr["Vendor_No"].ToString() : string.Empty;
                    discrepancyBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : string.Empty;
                    discrepancyBE.Vendor.address1 = dr["VendorAddress1"] != DBNull.Value ? dr["VendorAddress1"].ToString() : string.Empty;
                    discrepancyBE.Vendor.address2 = dr["VendorAddress2"] != DBNull.Value ? dr["VendorAddress2"].ToString() : string.Empty;
                    discrepancyBE.Vendor.city = dr["VendorCity"] != DBNull.Value ? dr["VendorCity"].ToString() : string.Empty;
                    discrepancyBE.Vendor.VMPPIN = dr["VMPPIN"] != DBNull.Value ? dr["VMPPIN"].ToString() : string.Empty;
                    discrepancyBE.Vendor.VMPPOU = dr["VMPPOU"] != DBNull.Value ? dr["VMPPOU"].ToString() : string.Empty;
                    discrepancyBE.Vendor.LanguageID = dr["VendorLanguageID"] != DBNull.Value ? Convert.ToInt32(dr["VendorLanguageID"].ToString()) : 1;
                    discrepancyBE.InvoiceNo = dr["InvoiceNo"] != DBNull.Value ? dr["InvoiceNo"].ToString() : string.Empty;
                    discrepancyBE.ReferenceNo = dr["ReferenceNo"] != DBNull.Value ? dr["ReferenceNo"].ToString() : string.Empty;
                    discrepancyBE.CommunicationEmails = dr["CommunicationEmails"] != DBNull.Value ? dr["CommunicationEmails"].ToString() : string.Empty;
                    discrepancyBE.Comment = dr["Comments"] != DBNull.Value ? dr["Comments"].ToString() : string.Empty;
                    discrepancyBE.Currency = new BusinessEntities.ModuleBE.StockOverview.CurrencyBE();
                    discrepancyBE.Currency.CurrencyId = dr["CurrencyId"] != DBNull.Value ? Convert.ToInt32(dr["CurrencyId"].ToString()) : 0;
                    discrepancyBE.Currency.CurrencyName = dr["CurrencyName"] != DBNull.Value ? dr["CurrencyName"].ToString() : string.Empty;
                    discrepancyBE.DiscrepancyLogID = dr["DiscrepancyId"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyId"].ToString()) : (int?)null;
                    discrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? Convert.ToString(dr["VDRNo"]) : string.Empty;
                    discrepancyBE.DiscrepancyLogDate = dr["DiscrepancyLogDate"] != DBNull.Value ? Convert.ToDateTime(dr["DiscrepancyLogDate"].ToString()) : (DateTime?)null;
                    discrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"].ToString()) : (int?)null;
                    discrepancyBE.APAddress1 = dr["APAddress1"] != DBNull.Value ? dr["APAddress1"].ToString() : string.Empty;
                    discrepancyBE.APAddress2 = dr["APAddress2"] != DBNull.Value ? dr["APAddress2"].ToString() : string.Empty;
                    discrepancyBE.APAddress3 = dr["APAddress3"] != DBNull.Value ? dr["APAddress3"].ToString() : string.Empty;
                    discrepancyBE.APAddress4 = dr["APAddress4"] != DBNull.Value ? dr["APAddress4"].ToString() : string.Empty;
                    discrepancyBE.APAddress5 = dr["APAddress5"] != DBNull.Value ? dr["APAddress5"].ToString() : string.Empty;
                    discrepancyBE.APAddress6 = dr["APAddress6"] != DBNull.Value ? dr["APAddress6"].ToString() : string.Empty;
                    discrepancyBE.APCountryID = dr["APCountryID"] != DBNull.Value ? Convert.ToInt32(dr["APCountryID"].ToString()) : (int?)null;
                    discrepancyBE.APCountryName = dr["APCountryName"] != DBNull.Value ? dr["APCountryName"].ToString() : string.Empty;
                    discrepancyBE.APPincode = dr["APPincode"] != DBNull.Value ? dr["APPincode"].ToString() : string.Empty;
                    discrepancyBE.Status = dr["Status"] != DBNull.Value ? dr["Status"].ToString() : string.Empty;
                    discrepancyBE.CreatedById = dr["CreatedById"] != DBNull.Value ? Convert.ToInt32(dr["CreatedById"].ToString()) : (int?)null;
                    discrepancyBE.CreatedBy = dr["CreatedBy"] != DBNull.Value ? dr["CreatedBy"].ToString() : string.Empty;
                    discrepancyBE.CreatedByContactNumber = dr["CreatedByContactNumber"] != DBNull.Value ? dr["CreatedByContactNumber"].ToString() : string.Empty;
                    discrepancyBE.CancelById = dr["CancelById"] != DBNull.Value ? Convert.ToInt32(dr["CancelById"].ToString()) : (int?)null;
                    discrepancyBE.CancelBy = dr["CanceledBy"] != DBNull.Value ? dr["CanceledBy"].ToString() : string.Empty;
                    discrepancyBE.CancelByContactNumber = dr["CancelByContactNumber"] != DBNull.Value ? dr["CancelByContactNumber"].ToString() : string.Empty;
                    discrepancyBE.CanceledDate = dr["CanceledDate"] != DBNull.Value ? Convert.ToDateTime(dr["CanceledDate"].ToString()) : (DateTime?)null;
                    discrepancyBE.CountryShortName = dr["CountryShortName"] != DBNull.Value ? dr["CountryShortName"].ToString() : string.Empty;
                    discrepancyBE.DebitReasonId = dr["DebitReasonId"] != DBNull.Value ? Convert.ToInt32(dr["DebitReasonId"].ToString()) : (int?)null;
                    discrepancyBE.Reason = dr["Reason"] != DBNull.Value ? dr["Reason"].ToString() : string.Empty;
                    discrepancyBE.ReasonType = dr["ReasonType"] != DBNull.Value ? dr["ReasonType"].ToString() : string.Empty;
                    discrepancyBE.StateValueDebited = dr["ValueofDebited"] != DBNull.Value ? Convert.ToDecimal(dr["ValueofDebited"].ToString()) : (decimal?)null;
                    discrepancyBE.CancelDebitReasonId = dr["CancelDebitReasonId"] != DBNull.Value ? Convert.ToInt32(dr["CancelDebitReasonId"].ToString()) : (int?)null;
                    discrepancyBE.CancelReason = dr["CancelReason"] != DBNull.Value ? dr["CancelReason"].ToString() : string.Empty;
                    discrepancyBE.CancelReasonType = dr["CancelReasonType"] != DBNull.Value ? dr["CancelReasonType"].ToString() : string.Empty;
                    discrepancyBE.CancelCommunicationEmails = dr["CancelCommunicationEmails"] != DBNull.Value ? dr["CancelCommunicationEmails"].ToString() : string.Empty;
                    discrepancyBE.CancelVendorComments = dr["CancelVendorComments"] != DBNull.Value ? dr["CancelVendorComments"].ToString() : string.Empty;
                    discrepancyBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    discrepancyBE.Country.CountryID = dr["CountryID"] != DBNull.Value ? Convert.ToInt32(dr["CountryID"].ToString()) : 0;
                    discrepancyBE.Country.CountryName = dr["CountryName"] != DBNull.Value ? dr["CountryName"].ToString() : string.Empty;
                    discrepancyBE.APActionRef = dr["APActionRef"] != DBNull.Value ? dr["APActionRef"].ToString() : string.Empty;
                    discrepancyBE.CommEmailsWithLink = dr["CommEmailsWithLink"] != DBNull.Value ? dr["CommEmailsWithLink"].ToString() : string.Empty;
                    discrepancyBE.CancelCommEmailsWithLink = dr["CancelCommEmailsWithLink"] != DBNull.Value ? dr["CancelCommEmailsWithLink"].ToString() : string.Empty;
                    discrepancyBE.YourVATReference = dr["VendorVatReference"] != DBNull.Value ? dr["VendorVatReference"].ToString() : string.Empty;
                    discrepancyBE.OurVATReference = dr["OurVATReference"] != DBNull.Value ? dr["OurVATReference"].ToString() : string.Empty;
                    discrepancyBE.DebitTotalValue = dr["TotalDebitValue"] != DBNull.Value ? Convert.ToDecimal(dr["TotalDebitValue"].ToString()) : (decimal?)null;
                    lstDiscrepancyBE.Add(discrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstDiscrepancyBE;

        }

        public int? CancelDebitDAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@CancelById", oDiscrepancyBE.CancelById);
                param[index++] = new SqlParameter("@CancelDebitReasonId", oDiscrepancyBE.CancelDebitReasonId);
                param[index++] = new SqlParameter("@CancelCommunicationEmails", oDiscrepancyBE.CancelCommunicationEmails);
                param[index++] = new SqlParameter("@CancelVendorComments", oDiscrepancyBE.CancelVendorComments);
                param[index++] = new SqlParameter("@DebitRaiseId", oDiscrepancyBE.DebitRaiseId);

                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyMailBE> GetDebitCommunicationDAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            var lstDiscrepancyMailBEList = new List<DiscrepancyMailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyMailBE.Action);
                param[index++] = new SqlParameter("@DebitRaiseId", oDiscrepancyMailBE.DebitRaiseId);
                param[index++] = new SqlParameter("@CommTitle", oDiscrepancyMailBE.CommTitle);
                param[index++] = new SqlParameter("@languageID", oDiscrepancyMailBE.LanguageId);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
                foreach (DataRow dr in dt.Rows)
                {
                    var discrepancyMailBE = new DiscrepancyMailBE();
                    discrepancyMailBE.DebitRaiseCommId = dr["DebitRaiseCommId"] != DBNull.Value ? Convert.ToInt32(dr["DebitRaiseCommId"].ToString()) : (int?)null;
                    discrepancyMailBE.DebitRaiseId = dr["DebitRaiseId"] != DBNull.Value ? Convert.ToInt32(dr["DebitRaiseId"].ToString()) : (int?)null;
                    discrepancyMailBE.mailSubject = dr["Subject"] != DBNull.Value ? dr["Subject"].ToString() : "";
                    discrepancyMailBE.sentDate = dr["SentDate"] != DBNull.Value ? Convert.ToDateTime(dr["SentDate"]) : DateTime.Now;
                    discrepancyMailBE.sentTo = dr["SentTo"] != DBNull.Value ? Convert.ToString(dr["SentTo"].ToString()) : string.Empty;
                    discrepancyMailBE.mailBody = dr["Body"] != DBNull.Value ? dr["Body"].ToString() : "";
                    discrepancyMailBE.SendByID = dr["SendByID"] != DBNull.Value ? Convert.ToInt32(dr["SendByID"]) : (int?)null;
                    discrepancyMailBE.IsMailSent = dr["IsMailSent"] != DBNull.Value ? Convert.ToBoolean(dr["IsMailSent"]) : false;
                    discrepancyMailBE.LanguageId = dr["LanguageId"] != DBNull.Value ? Convert.ToInt32(dr["LanguageId"]) : (int?)null;
                    discrepancyMailBE.MailSentInLanguage = dr["MailSentInLanguage"] != DBNull.Value ? Convert.ToBoolean(dr["MailSentInLanguage"]) : false;
                    discrepancyMailBE.CommTitle = dr["CommTitle"] != DBNull.Value ? dr["CommTitle"].ToString() : "";
                    discrepancyMailBE.SentToWithLink = dr["SentToWithLink"] != DBNull.Value ? dr["SentToWithLink"].ToString() : string.Empty;
                    discrepancyMailBE.DebitNoteNo = dr["DebitNoteNo"] != DBNull.Value ? dr["DebitNoteNo"].ToString() : string.Empty;
                    lstDiscrepancyMailBEList.Add(discrepancyMailBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstDiscrepancyMailBEList;

        }

        public List<DiscrepancyBE> GetDebitResentCommunicationDAL(DiscrepancyBE oDiscrepancyBE)
        {
            var lstDiscrepancyBE = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DebitRaiseId", oDiscrepancyBE.DebitRaiseId);
                param[index++] = new SqlParameter("@languageID", oDiscrepancyBE.LanguageID);

                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
                foreach (DataRow dr in dt.Rows)
                {
                    var discrepancyBE = new DiscrepancyBE();
                    discrepancyBE.DebitRaiseId = dr["DebitRaiseId"] != DBNull.Value ? Convert.ToInt32(dr["DebitRaiseId"].ToString()) : (int?)null;
                    discrepancyBE.ReSentDate = dr["ReSentDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReSentDate"].ToString()) : (DateTime?)null;
                    discrepancyBE.ReSentById = dr["ReSentById"] != DBNull.Value ? Convert.ToInt32(dr["ReSentById"].ToString()) : (int?)null;
                    discrepancyBE.ReSentBy = dr["ReSentBy"] != DBNull.Value ? dr["ReSentBy"].ToString() : string.Empty;
                    discrepancyBE.DebitNoteNo = dr["DebitNoteNo"] != DBNull.Value ? dr["DebitNoteNo"].ToString() : string.Empty;
                    discrepancyBE.ResentCommEmailsWithLink = dr["ResentCommEmailsWithLink"] != DBNull.Value ? dr["ResentCommEmailsWithLink"].ToString() : string.Empty;
                    lstDiscrepancyBE.Add(discrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstDiscrepancyBE;

        }

        public List<DiscrepancyBE> GetAllDebitRaiseDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            var lstDiscrepancyBE = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DebitRaiseId", oDiscrepancyBE.DebitRaiseId);

                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
                foreach (DataRow dr in dt.Rows)
                {
                    var discrepancyBE = new DiscrepancyBE();
                    discrepancyBE.DebitRaiseItemId = dr["DebitRaiseItemId"] != DBNull.Value ? Convert.ToInt32(dr["DebitRaiseItemId"].ToString()) : (int?)null;
                    discrepancyBE.DebitRaiseId = dr["DebitRaiseId"] != DBNull.Value ? Convert.ToInt32(dr["DebitRaiseId"].ToString()) : (int?)null;
                    discrepancyBE.Line_no = dr["Line_No"] != DBNull.Value ? Convert.ToInt32(dr["Line_No"].ToString()) : (int?)null;
                    discrepancyBE.ODSKUCode = dr["ODSKUCode"] != DBNull.Value ? Convert.ToString(dr["ODSKUCode"]) : string.Empty;
                    discrepancyBE.VikingCode = dr["VikingCode"] != DBNull.Value ? dr["VikingCode"].ToString() : string.Empty;
                    discrepancyBE.VendorCode = dr["VendorCode"] != DBNull.Value ? dr["VendorCode"].ToString() : string.Empty;
                    discrepancyBE.ProductDescription = dr["ProductDescription"] != DBNull.Value ? Convert.ToString(dr["ProductDescription"]) : string.Empty;
                    discrepancyBE.UOM = dr["UOM"] != DBNull.Value ? Convert.ToString(dr["UOM"]) : string.Empty;
                    discrepancyBE.POItemCost = dr["POItemCost"] != DBNull.Value ? Convert.ToDecimal(dr["POItemCost"]) : (decimal?)null;
                    discrepancyBE.POTotalCost = dr["POTotalCost"] != DBNull.Value ? Convert.ToDecimal(dr["POTotalCost"]) : (decimal?)null;
                    discrepancyBE.StateValueDebited = dr["StateValueDebited"] != DBNull.Value ? Convert.ToDecimal(dr["StateValueDebited"]) : (decimal?)null;
                    discrepancyBE.DebitReasonId = dr["DebitReasonId"] != DBNull.Value ? Convert.ToInt32(dr["DebitReasonId"]) : (int?)null;
                    discrepancyBE.Reason = dr["Reason"] != DBNull.Value ? dr["Reason"].ToString() : string.Empty;
                    discrepancyBE.OriginalQuantity = dr["OriginalPOQty"] != DBNull.Value ? Convert.ToInt32(dr["OriginalPOQty"]) : (int?)null;
                    discrepancyBE.OutstandingQuantity = dr["OutstandingPOQty"] != DBNull.Value ? Convert.ToInt32(dr["OutstandingPOQty"]) : (int?)null;
                    discrepancyBE.InvoicedItemPrice = dr["InvoicedItemPrice"] != DBNull.Value ? Convert.ToDecimal(dr["InvoicedItemPrice"]) : (decimal?)null;
                    discrepancyBE.QtyInvoiced = dr["QtyInvoiced"] != DBNull.Value ? Convert.ToDecimal(dr["QtyInvoiced"]) : (decimal?)null;
                    discrepancyBE.PriceDifference = dr["PriceDifference"] != DBNull.Value ? Convert.ToDecimal(dr["PriceDifference"]) : (decimal?)null;
                    lstDiscrepancyBE.Add(discrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstDiscrepancyBE;

        }

        /* REQUIREMENT OF STAGE-11 POINT-16 */
        /* CHECK ROLE BASED TEMPLATE FOR RAISE QUERY*/
        public int? CheckUserQueryTemplate(DiscrepancyBE oDiscrepancyBE)
        {
            int intResult = 0;
            var lstDiscrepancyBE = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.UserID);
                param[index++] = new SqlParameter("@SiteID", oDiscrepancyBE.SiteID);

                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
                    intResult = dt.Rows.Count;
                }
                //var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
                //if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                //    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                //else
                //    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;

        }

        public DataSet CheckForGin0012DuplicateDisc(DiscrepancyBE oDiscrepancyBE2)
        {
            DataSet ds = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE2.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE2.DiscrepancyLogID);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public DataSet GetCheckDiscrepancyExistsDAL(DiscrepancyBE oDiscrepancyBE2)
        {
            DataSet dt = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                //  oDiscrepancyBE2.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();

                param[index++] = new SqlParameter("@Action", oDiscrepancyBE2.Action);
                param[index++] = new SqlParameter("@Purchase_order", oDiscrepancyBE2.PurchaseOrderNumber);
                param[index++] = new SqlParameter("@VendorID", oDiscrepancyBE2.VendorID);
                param[index++] = new SqlParameter("@PurchaseOrderDate", oDiscrepancyBE2.PurchaseOrder.Order_raised);
                dt = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }


        public DataSet GetInvoiceDiscrepancyCommentsDAL(DiscrepancyBE oDiscrepancyBE2)
        {
            DataSet dt = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                //  oDiscrepancyBE2.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();

                param[index++] = new SqlParameter("@Action", oDiscrepancyBE2.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE2.DiscrepancyLogID);
                dt = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }


        public List<DiscrepancyBE> GetQueryDiscrepancyLogDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[24];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.UserID);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@SCDeliveryNoteNumber", oDiscrepancyBE.SCDeliveryNoteNumber);
                param[index++] = new SqlParameter("@SCDiscrepancyDateFrom", oDiscrepancyBE.SCDiscrepancyDateFrom);
                param[index++] = new SqlParameter("@SCDiscrepancyDateTo", oDiscrepancyBE.SCDiscrepancyDateTo);
                param[index++] = new SqlParameter("@SCDiscrepancyTypeID", oDiscrepancyBE.SCDiscrepancyTypeID);
                param[index++] = new SqlParameter("@SCDiscrepancyNo", oDiscrepancyBE.SCDiscrepancyNo);
                param[index++] = new SqlParameter("@SCPurchaseOrderDate", oDiscrepancyBE.SCPurchaseOrderDate);
                param[index++] = new SqlParameter("@SCPurchaseOrderNo", oDiscrepancyBE.SCPurchaseOrderNo);
                param[index++] = new SqlParameter("@SCSelectedSiteIDs", oDiscrepancyBE.SCSelectedSiteIDs);
                param[index++] = new SqlParameter("@SCSelectedVendorIDs", oDiscrepancyBE.SCSelectedVendorIDs);
                param[index++] = new SqlParameter("@SCSPUserID", oDiscrepancyBE.SCSPUserID);
                param[index++] = new SqlParameter("@DiscrepancyStatus", oDiscrepancyBE.DiscrepancyStatus);
                param[index++] = new SqlParameter("@VendorID", oDiscrepancyBE.VendorID);
                param[index++] = new SqlParameter("@IsDelete", oDiscrepancyBE.IsDelete);
                if (oDiscrepancyBE.User != null)
                    param[index++] = new SqlParameter("@RoleTypeFlag", oDiscrepancyBE.User.RoleTypeFlag);

                if (oDiscrepancyBE.QueryDiscrepancy != null)
                    param[index++] = new SqlParameter("@Query", oDiscrepancyBE.QueryDiscrepancy.Query);

                if (oDiscrepancyBE.UserIdForConsVendors != null)
                    param[index++] = new SqlParameter("@UserIdForConsVendors", oDiscrepancyBE.UserIdForConsVendors);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.EscalationLevel))
                    param[index++] = new SqlParameter("@EscalationLevel", oDiscrepancyBE.EscalationLevel);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.SearchByODSku))
                    param[index++] = new SqlParameter("@SearchByODSku", oDiscrepancyBE.SearchByODSku);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.SearchByCatCode))
                    param[index++] = new SqlParameter("@SearchByCatCode", oDiscrepancyBE.SearchByCatCode);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.SCSelectedSPIDs))
                    param[index++] = new SqlParameter("@SCSelectedSPIDs", oDiscrepancyBE.SCSelectedSPIDs);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.APIds))
                    param[index++] = new SqlParameter("@APIds", oDiscrepancyBE.APIds);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.OpenWith))
                    param[index++] = new SqlParameter("@OpenWith", oDiscrepancyBE.OpenWith);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.POSiteName = dr["SiteName"].ToString();
                    oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : string.Empty;
                    oNewDiscrepancyBE.DiscrepancyStatus = dr["DiscrepancyStatus"] != DBNull.Value ? dr["DiscrepancyStatus"].ToString() : string.Empty;
                    oNewDiscrepancyBE.OpenWith = dr["OpenWith"] != DBNull.Value ? dr["OpenWith"].ToString() : string.Empty;
                    oNewDiscrepancyBE.PurchaseOrderNumber = dr["PurchaseOrderNumber"] != DBNull.Value ? dr["PurchaseOrderNumber"].ToString() : string.Empty;
                    oNewDiscrepancyBE.AP = dr["APClerk"] != DBNull.Value ? dr["APClerk"].ToString() : string.Empty;
                    oNewDiscrepancyBE.CreatedBy = dr["CreatedBy"] != DBNull.Value ? dr["CreatedBy"].ToString() : string.Empty;
                    oNewDiscrepancyBE.DeliveryNoteNumber = dr["Invoice"] != DBNull.Value ? dr["Invoice"].ToString() : string.Empty;
                    oNewDiscrepancyBE.VendorNoName = dr["Vendor_Name"] != DBNull.Value ? dr["Vendor_Name"].ToString() : string.Empty;

                    oNewDiscrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.DiscrepancyLogID = dr["DiscrepancyLogID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyLogID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.UserID = dr["UserID"] != DBNull.Value ? Convert.ToInt32(dr["UserID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.DiscrepancyWorkflowID = dr["DiscrepancyWorkflowID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyWorkflowID"].ToString()) : (int?)null;
                    oNewDiscrepancyBE.CreatedDate = dr["DiscrepancyLogDate"] != DBNull.Value ? dr["DiscrepancyLogDate"].ToString() : string.Empty;
                    oNewDiscrepancyBE.Resolution = dr["Resolution"] != DBNull.Value ? dr["Resolution"].ToString() : string.Empty;
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public DataTable GetUserPhoneNumberDAL(DiscrepancyBE oDISLog_WorkFlowBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDISLog_WorkFlowBE.Action);
                param[index++] = new SqlParameter("@UserID", oDISLog_WorkFlowBE.UserID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public string GetDebitCreditStatusDAL(DiscrepancyBE oDiscrepancy)
        {
            string strResult = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancy.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancy.DiscrepancyLogID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    strResult = Result.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }

        public DataSet GetIsodToReturnDAL(DiscrepancyBE oDiscrepancyBE2)
        {
            DataSet dt = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                //  oDiscrepancyBE2.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();

                param[index++] = new SqlParameter("@Action", oDiscrepancyBE2.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE2.DiscrepancyLogID);
                dt = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public DataSet GetItemDetailDiscrepancyReportDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            DataSet ds = new DataSet();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[22];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@UserID", oDiscrepancyBE.UserID);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@SCDeliveryNoteNumber", oDiscrepancyBE.SCDeliveryNoteNumber);
                param[index++] = new SqlParameter("@SCDiscrepancyDateFrom", oDiscrepancyBE.SCDiscrepancyDateFrom);
                param[index++] = new SqlParameter("@SCDiscrepancyDateTo", oDiscrepancyBE.SCDiscrepancyDateTo);
                param[index++] = new SqlParameter("@SCDiscrepancyTypeID", oDiscrepancyBE.SCDiscrepancyTypeID);
                param[index++] = new SqlParameter("@SCDiscrepancyNo", oDiscrepancyBE.SCDiscrepancyNo);
                param[index++] = new SqlParameter("@SCPurchaseOrderDate", oDiscrepancyBE.SCPurchaseOrderDate);
                param[index++] = new SqlParameter("@SCPurchaseOrderNo", oDiscrepancyBE.SCPurchaseOrderNo);
                param[index++] = new SqlParameter("@SCSelectedSiteIDs", oDiscrepancyBE.SCSelectedSiteIDs);
                param[index++] = new SqlParameter("@SCSelectedVendorIDs", oDiscrepancyBE.SCSelectedVendorIDs);
                param[index++] = new SqlParameter("@SCSPUserID", oDiscrepancyBE.SCSPUserID);
                param[index++] = new SqlParameter("@DiscrepancyStatus", oDiscrepancyBE.DiscrepancyStatus);
                param[index++] = new SqlParameter("@VendorID", oDiscrepancyBE.VendorID);
                param[index++] = new SqlParameter("@IsDelete", oDiscrepancyBE.IsDelete);
                if (oDiscrepancyBE.User != null)
                    param[index++] = new SqlParameter("@RoleTypeFlag", oDiscrepancyBE.User.RoleTypeFlag);

                if (oDiscrepancyBE.QueryDiscrepancy != null)
                    param[index++] = new SqlParameter("@Query", oDiscrepancyBE.QueryDiscrepancy.Query);

                if (oDiscrepancyBE.UserIdForConsVendors != null)
                    param[index++] = new SqlParameter("@UserIdForConsVendors", oDiscrepancyBE.UserIdForConsVendors);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.EscalationLevel))
                    param[index++] = new SqlParameter("@EscalationLevel", oDiscrepancyBE.EscalationLevel);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.SearchByODSku))
                    param[index++] = new SqlParameter("@SearchByODSku", oDiscrepancyBE.SearchByODSku);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.SearchByCatCode))
                    param[index++] = new SqlParameter("@SearchByCatCode", oDiscrepancyBE.SearchByCatCode);

                if (!string.IsNullOrEmpty(oDiscrepancyBE.SCSelectedSPIDs))
                    param[index++] = new SqlParameter("@SCSelectedSPIDs", oDiscrepancyBE.SCSelectedSPIDs);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;

        }

        public DataTable GetCommentsLoopDAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public string GetCurrencyDAL(DiscrepancyBE oDiscrepancy)
        {
            string strResult = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancy.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancy.DiscrepancyLogID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    strResult = Result.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }

        public int? DeleteBlankWorkflowEntriesDAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyBE> GetUnclosedDiscrepancyDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                    oNewDiscrepancyBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDiscrepancyBE.VDRNo = dr["VDRNo"] != DBNull.Value ? dr["VDRNo"].ToString() : string.Empty;
                    oNewDiscrepancyBE.DiscrepancyLogDate = dr["DiscrepancyLogDate"] != DBNull.Value ? Convert.ToDateTime(dr["DiscrepancyLogDate"].ToString()) : (DateTime?)null;
                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public int? UpdateUnclosedDiscrepancyDAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@UserId", oDiscrepancyBE.UserID);
                object oResult = SqlHelper.ExecuteScalar(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);
                if (oResult != null)
                    Result = Convert.ToInt32(oResult);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public int? UpdateVENHtmlDAL(DiscrepancyBE oNewDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oNewDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oNewDiscrepancyBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@VENHTML", oNewDiscrepancyBE.VENHTML);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyBE> GetCostCenterDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@SiteID", oDiscrepancyBE.SiteID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();
                    oNewDiscrepancyBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();

                    oNewDiscrepancyBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : (int?)null;
                    oNewDiscrepancyBE.POSiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : string.Empty;
                    oNewDiscrepancyBE.Site.CostCenter = dr["CostCenter"] != DBNull.Value ? dr["CostCenter"].ToString() : string.Empty;

                    DiscrepancyBEList.Add(oNewDiscrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public string GetPurchaseOrderByPOIdDAL(DiscrepancyBE oDiscrepancyBE)
        {
            string sResult = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrderNumberID", oDiscrepancyBE.PurchaseOrderID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancy", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    sResult = Result.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }


        public int? UpdateWorkflowForQualityIssueDiscDAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[15];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyWorkFlowID_New", oDiscrepancyBE.DiscrepancyWorkFlowID);
                param[index++] = new SqlParameter("@DiscrepancyWorkflowID", oDiscrepancyBE.DiscrepancyWorkflowID);
                param[index++] = new SqlParameter("@GINActionRequired", oDiscrepancyBE.GINActionRequired);
                param[index++] = new SqlParameter("@GINUserControl", oDiscrepancyBE.GINUserControl);
                param[index++] = new SqlParameter("@INVActionRequired", oDiscrepancyBE.INVActionRequired);
                param[index++] = new SqlParameter("@INVUserControl", oDiscrepancyBE.INVUserControl);
                param[index++] = new SqlParameter("@APActionRequired", oDiscrepancyBE.APActionRequired);
                param[index++] = new SqlParameter("@APUserControl", oDiscrepancyBE.APUserControl);
                param[index++] = new SqlParameter("@VenActionRequired", oDiscrepancyBE.VenActionRequired);
                param[index++] = new SqlParameter("@VENUserControl", oDiscrepancyBE.VENUserControl);
                param[index++] = new SqlParameter("@MEDActionRequired", oDiscrepancyBE.MEDActionRequired);
                param[index++] = new SqlParameter("@MEDUserControl", oDiscrepancyBE.MEDUserControl);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataTable GetIsVendorAcceptRefuseChargeDAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDiscrepancyBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public List<DiscrepancyBE> GetStockPlannerOnHolidayCoverDAL(DiscrepancyBE oDiscrepancyBE)
        {

            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@StockPlannerID", oDiscrepancyBE.StockPlannerID);
                param[index++] = new SqlParameter("@SiteID", oDiscrepancyBE.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();

                if (dt.Rows.Count > 0 && dt.Rows[0][0] != null && dt.Rows[0][0].ToString() != "0")
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                        oNewDiscrepancyBE.StockPlannerToCoverEmailID = dr["StockPlannerToCoverEmailID"] != DBNull.Value ? dr["StockPlannerToCoverEmailID"].ToString() : string.Empty;
                        oNewDiscrepancyBE.StockPlannerToCoverUserID = dr["StockPlannerToCoverUserID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerToCoverUserID"]) : 0;
                        oNewDiscrepancyBE.SPToCoverStockPlannerID = dr["SPToCoverStockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["SPToCoverStockPlannerID"]) : 0;
                        oNewDiscrepancyBE.StockPlannerNO = dr["StockPlannerNumber"] != DBNull.Value ? dr["StockPlannerNumber"].ToString() : "";
                        oNewDiscrepancyBE.StockPlannerContact = dr["StockPlannerContact"] != DBNull.Value ? dr["StockPlannerContact"].ToString() : "";

                        DiscrepancyBEList.Add(oNewDiscrepancyBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetActualStockPlannerDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {

            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@StockPlannerID", oDiscrepancyBE.StockPlannerID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
                if (dt.Rows.Count > 0 && dt.Rows[0][0] != null && dt.Rows[0][0].ToString() != "0")
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                        oNewDiscrepancyBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"]) : 0;
                        oNewDiscrepancyBE.StockPlannerNO = dr["StockPlannerNO"] != DBNull.Value ? dr["StockPlannerNO"].ToString() : "";
                        oNewDiscrepancyBE.StockPlannerContact = dr["StockPlannerContact"] != DBNull.Value ? dr["StockPlannerContact"].ToString() : "";

                        DiscrepancyBEList.Add(oNewDiscrepancyBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetSPInitialCommDetailsDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@StockPlannerID", oDiscrepancyBE.StockPlannerID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancy", param);

                dt = ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
                if (dt.Rows.Count > 0 && dt.Rows[0][0] != null && dt.Rows[0][0].ToString() != "0")
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        DiscrepancyBE oNewDiscrepancyBE = new DiscrepancyBE();

                        oNewDiscrepancyBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"]) : 0;
                        oNewDiscrepancyBE.StockPlannerNO = dr["StockPlannerNo"] != DBNull.Value ? dr["StockPlannerNo"].ToString() : "";
                        oNewDiscrepancyBE.StockPlannerEmailID = dr["StockPlannerEmailID"] != DBNull.Value ? Convert.ToString(dr["StockPlannerEmailID"]) : null;
                        oNewDiscrepancyBE.StockPlannerName = dr["StockPlannerName"] != DBNull.Value ? dr["StockPlannerName"].ToString() : string.Empty;

                        oNewDiscrepancyBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                        oNewDiscrepancyBE.User.LanguageID = dr["LanguageID"] != DBNull.Value ? Convert.ToInt32(dr["LanguageID"].ToString()) : 1;
                        oNewDiscrepancyBE.User.Language = dr["Language"] != DBNull.Value ? dr["Language"].ToString() : string.Empty;

                        DiscrepancyBEList.Add(oNewDiscrepancyBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DiscrepancyBEList;
        }


    }
}
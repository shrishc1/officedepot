﻿using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Discrepancy.LogDiscrepancy
{
    public class VendorSubsToChargesDAL
    {

        public List<VendorSubsToChargesBE> GetvendorSubscribeDAL(VendorSubsToChargesBE oVendorSubsToChargesBE)
        {
            List<VendorSubsToChargesBE> VendorSubsToChargesBEList = new List<VendorSubsToChargesBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oVendorSubsToChargesBE.Action);
                param[index++] = new SqlParameter("@CountryIDs", oVendorSubsToChargesBE.CountryIDs);
                param[index++] = new SqlParameter("@VendorIDs", oVendorSubsToChargesBE.VendorIDs);
                param[index++] = new SqlParameter("@ChargeType", oVendorSubsToChargesBE.ChargeType);
                param[index++] = new SqlParameter("@Subscribe", oVendorSubsToChargesBE.Subscribed);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorSubsToCharges", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    VendorSubsToChargesBE oNewVendorSubsToChargesBE = new VendorSubsToChargesBE();

                    oNewVendorSubsToChargesBE.VendorID = Convert.ToInt32(dr["UPVendorID"]);
                    oNewVendorSubsToChargesBE.Vendor_Country = dr["Vendor_Country"] != null ? Convert.ToString(dr["Vendor_Country"]) : string.Empty;
                    oNewVendorSubsToChargesBE.Vendor_Name = dr["Vendor_Name"] != null ? Convert.ToString(dr["Vendor_Name"]) : string.Empty;

                    oNewVendorSubsToChargesBE.Overs = dr["Overs"] != DBNull.Value ? Convert.ToBoolean(dr["Overs"]) : true;
                    oNewVendorSubsToChargesBE.Damage = dr["Damage"] != DBNull.Value ? Convert.ToBoolean(dr["Damage"]) : true;
                    oNewVendorSubsToChargesBE.NoPO = dr["NoPO"] != DBNull.Value ? Convert.ToBoolean(dr["NoPO"]) : true;
                    oNewVendorSubsToChargesBE.NoPaperwork = dr["NoPaperwork"] != DBNull.Value ? Convert.ToBoolean(dr["NoPaperwork"]) : true;
                    oNewVendorSubsToChargesBE.IncorrectProduct = dr["IncorrectProduct"] != DBNull.Value ? Convert.ToBoolean(dr["IncorrectProduct"]) : true;
                    oNewVendorSubsToChargesBE.IncorrectAddress = dr["IncorrectAddress"] != DBNull.Value ? Convert.ToBoolean(dr["IncorrectAddress"]) : true;
                    oNewVendorSubsToChargesBE.FailedPalletSpec = dr["FailedPalletSpec"] != DBNull.Value ? Convert.ToBoolean(dr["FailedPalletSpec"]) : true;
                    oNewVendorSubsToChargesBE.IncorrectPackCarriage = dr["IncorrectPackCarriage"] != DBNull.Value ? Convert.ToBoolean(dr["IncorrectPackCarriage"]) : true;
                    oNewVendorSubsToChargesBE.IncorrectPackLabour = dr["IncorrectPackLabour"] != DBNull.Value ? Convert.ToBoolean(dr["IncorrectPackLabour"]) : true;
                    oNewVendorSubsToChargesBE.PrematureInvoice = dr["PrematureInvoice"] != DBNull.Value ? Convert.ToBoolean(dr["PrematureInvoice"]) : true;
                    oNewVendorSubsToChargesBE.QualityIssueCarriage = dr["QualityIssueCarriage"] != DBNull.Value ? Convert.ToBoolean(dr["QualityIssueCarriage"]) : true;
                    oNewVendorSubsToChargesBE.QualityIssueLabour = dr["QualityIssueLabour"] != DBNull.Value ? Convert.ToBoolean(dr["QualityIssueLabour"]) : true;


                    VendorSubsToChargesBEList.Add(oNewVendorSubsToChargesBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return VendorSubsToChargesBEList;
        }

        public int? UpdatevendorSubscribeDAL(VendorSubsToChargesBE oVendorSubsToChargesBE)
        {
            int? intResult = 0;
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[15];
                param[index++] = new SqlParameter("@Action", oVendorSubsToChargesBE.Action);
                param[index++] = new SqlParameter("@VendorID", oVendorSubsToChargesBE.VendorID);
                param[index++] = new SqlParameter("@Overs", oVendorSubsToChargesBE.Overs);
                param[index++] = new SqlParameter("@Damage", oVendorSubsToChargesBE.Damage);
                param[index++] = new SqlParameter("@NoPO", oVendorSubsToChargesBE.NoPO);
                param[index++] = new SqlParameter("@NoPaperwork", oVendorSubsToChargesBE.NoPaperwork);
                param[index++] = new SqlParameter("@IncorrectProduct", oVendorSubsToChargesBE.IncorrectProduct);
                param[index++] = new SqlParameter("@IncorrectAddress", oVendorSubsToChargesBE.IncorrectAddress);
                param[index++] = new SqlParameter("@FailedPalletSpec", oVendorSubsToChargesBE.FailedPalletSpec);
                param[index++] = new SqlParameter("@IncorrectPackCarriage", oVendorSubsToChargesBE.IncorrectPackCarriage);
                param[index++] = new SqlParameter("@IncorrectPackLabour", oVendorSubsToChargesBE.IncorrectPackLabour);
                param[index++] = new SqlParameter("@PrematureInvoice", oVendorSubsToChargesBE.PrematureInvoice);
                param[index++] = new SqlParameter("@QualityIssueCarriage", oVendorSubsToChargesBE.QualityIssueCarriage);
                param[index++] = new SqlParameter("@QualityIssueLabour", oVendorSubsToChargesBE.QualityIssueLabour);
                param[index++] = new SqlParameter("@UpdateBy", oVendorSubsToChargesBE.UpdateBy);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorSubsToCharges", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return intResult;
        }
    }
}

﻿using BusinessEntities.ModuleBE.Upload;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Upload
{
    public class ScheduleDAL : BaseDAL
    {
        public void addSchedulingInfoDAL(ScheduleBE scheduleBE)
        {
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", scheduleBE.Action);
                param[index++] = new SqlParameter("@ApplicationName", scheduleBE.JobName);
                param[index++] = new SqlParameter("@Month", scheduleBE.Month);
                param[index++] = new SqlParameter("@Year", scheduleBE.Year);
                param[index++] = new SqlParameter("@IsExecuted", scheduleBE.IsExecuted);
                param[index++] = new SqlParameter("@UserID", scheduleBE.UserID);
                param[index++] = new SqlParameter("@InputDate", scheduleBE.InputDate);
                param[index++] = new SqlParameter("@SchedulerTime", scheduleBE.SchedulerTime);
                param[index++] = new SqlParameter("@SchedulerDate", scheduleBE.SchedulerDate);
                SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spSchedulingApp", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public int? isAlreadySchedulingInfoDAL(ScheduleBE scheduleBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", scheduleBE.Action);
                param[index++] = new SqlParameter("@ApplicationName", scheduleBE.JobName);
                param[index++] = new SqlParameter("@Month", scheduleBE.Month);
                param[index++] = new SqlParameter("@Year", scheduleBE.Year);
                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spSchedulingApp", param);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spSchedulingApp", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows.Count);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<ScheduleBE> GetUpcomingJobDAL(ScheduleBE scheduleBE)
        {
            DataTable dt = new DataTable();
            List<ScheduleBE> lstScheduleBE = new List<ScheduleBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", scheduleBE.Action);
                param[index++] = new SqlParameter("@SchedulerId", scheduleBE.SchedulerId);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSchedulingApp", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    ScheduleBE oScheduleBE = new ScheduleBE();

                    oScheduleBE.Month = dr["Month"] != DBNull.Value ? Convert.ToInt32(dr["Month"]) : 0;
                    oScheduleBE.Year = dr["Year"] != DBNull.Value ? Convert.ToInt32(dr["Year"]) : 0;
                    oScheduleBE.SchedulerId = dr["SchedulerId"] != DBNull.Value ? Convert.ToInt32(dr["SchedulerId"]) : 0;
                    oScheduleBE.SchedulerDate = dr["SchedulerDate"] != DBNull.Value ? Convert.ToDateTime(dr["SchedulerDate"]) : (DateTime?)null;
                    oScheduleBE.InputDate = Convert.ToDateTime(dr["InputDate"]);
                    oScheduleBE.JobName = dr["ApplicationName"].ToString();
                    oScheduleBE.UserName = dr["UserName"].ToString();
                    oScheduleBE.SchedulerTime = dr["SchedulerTime"] != DBNull.Value ? TimeSpan.Parse(dr["SchedulerTime"].ToString()) : (TimeSpan?)null;
                    if (oScheduleBE.SchedulerTime != null)
                        oScheduleBE.SchedulerDate = oScheduleBE.SchedulerDate + oScheduleBE.SchedulerTime;

                    if (oScheduleBE.Month > 0)
                        oScheduleBE.DisplayMonthYear = oScheduleBE.InputDate.AddMonths(-(oScheduleBE.Month)); //new DateTime(oScheduleBE.Year, oScheduleBE.Month, 1);

                    lstScheduleBE.Add(oScheduleBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstScheduleBE;
        }

        public List<ScheduleBE> GetJobHistoryDAL(ScheduleBE scheduleBE)
        {
            DataTable dt = new DataTable();
            List<ScheduleBE> lstScheduleBE = new List<ScheduleBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", scheduleBE.Action);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSchedulingApp", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    ScheduleBE oScheduleBE = new ScheduleBE();

                    oScheduleBE.UserName = dr["UserName"].ToString();
                    oScheduleBE.SchedulerId = dr["SchedulerId"] != DBNull.Value ? Convert.ToInt32(dr["SchedulerId"]) : 0;
                    oScheduleBE.JobName = dr["ApplicationName"].ToString();
                    oScheduleBE.Month = dr["Month"] != DBNull.Value ? Convert.ToInt32(dr["Month"]) : 0;
                    oScheduleBE.Year = dr["Year"] != DBNull.Value ? Convert.ToInt32(dr["Year"]) : 0;
                    oScheduleBE.InputDate = Convert.ToDateTime(dr["InputDate"]);
                    oScheduleBE.StartTime = dr["StartTime"] != DBNull.Value ? Convert.ToDateTime(dr["StartTime"]) : (DateTime?)null;
                    oScheduleBE.EndTime = dr["EndTime"] != DBNull.Value ? Convert.ToDateTime(dr["EndTime"]) : (DateTime?)null;
                    oScheduleBE.Status = dr["Status"].ToString();

                    if (oScheduleBE.Month > 0)
                        oScheduleBE.DisplayMonthYear = oScheduleBE.InputDate.AddMonths(-(oScheduleBE.Month));

                    lstScheduleBE.Add(oScheduleBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstScheduleBE;
        }


        public int? EditSchedulingInfoDAL(ScheduleBE scheduleBE)
        {
            int? Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                param[index++] = new SqlParameter("@Action", scheduleBE.Action);
                param[index++] = new SqlParameter("@ApplicationName", scheduleBE.JobName);
                param[index++] = new SqlParameter("@Month", scheduleBE.Month);
                param[index++] = new SqlParameter("@Year", scheduleBE.Year);
                param[index++] = new SqlParameter("@IsExecuted", scheduleBE.IsExecuted);
                param[index++] = new SqlParameter("@UserID", scheduleBE.UserID);
                param[index++] = new SqlParameter("@InputDate", scheduleBE.InputDate);
                param[index++] = new SqlParameter("@SchedulerTime", scheduleBE.SchedulerTime);
                param[index++] = new SqlParameter("@SchedulerDate", scheduleBE.SchedulerDate);
                param[index++] = new SqlParameter("@SchedulerId", scheduleBE.SchedulerId);

                object oResult = SqlHelper.ExecuteScalar(DBConnection.Connection, CommandType.StoredProcedure, "spSchedulingApp", param);
                if (oResult != null)
                    Result = Convert.ToInt32(oResult);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }


        public int? DeleteSchedulingInfoDAL(ScheduleBE scheduleBE)
        {
            int? Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", scheduleBE.Action);
                param[index++] = new SqlParameter("@SchedulerId", scheduleBE.SchedulerId);

                object oResult = SqlHelper.ExecuteScalar(DBConnection.Connection, CommandType.StoredProcedure, "spSchedulingApp", param);
                if (oResult != null)
                    Result = Convert.ToInt32(oResult);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }


        public DataSet GetProcessesingDataOTIFDAL()
        {
            DataSet ds = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", "GetProcessesedOTIFVendor");
                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_Vendor", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }
    }
}

﻿using BusinessEntities.ModuleBE.Upload;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Upload
{

    public class UP_ReceiptInformationDAL : BaseDAL
    {

        public void AddReceiptInformation(List<UP_ReceiptInformationBE> pUP_ReceiptInformationBE)
        {
            try
            {

                DataTable dt = new DataTable();
                DataRow dr;

                dt.Columns.Add(new DataColumn("Viking_sku_no"));
                dt.Columns.Add(new DataColumn("OD_SKU_Number"));
                dt.Columns.Add(new DataColumn("Vendor_prod_code"));
                dt.Columns.Add(new DataColumn("Warehouse"));
                dt.Columns.Add(new DataColumn("PO_number"));
                dt.Columns.Add(new DataColumn("Qty_receipted", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("Other"));
                dt.Columns.Add(new DataColumn("Date", typeof(System.DateTime)));
                dt.Columns.Add(new DataColumn("Country"));
                dt.Columns.Add(new DataColumn("ORGOrdDate", typeof(System.DateTime)));

                dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));

                foreach (UP_ReceiptInformationBE oUP_ReceiptInformationBE in pUP_ReceiptInformationBE)
                {
                    dr = dt.NewRow();
                    dr["Viking_sku_no"] = oUP_ReceiptInformationBE.Viking_sku_no;
                    dr["OD_SKU_Number"] = oUP_ReceiptInformationBE.OD_SKU_Number;
                    dr["Vendor_prod_code"] = oUP_ReceiptInformationBE.Vendor_prod_code;
                    dr["Warehouse"] = oUP_ReceiptInformationBE.Warehouse;
                    dr["PO_number"] = oUP_ReceiptInformationBE.PO_number;

                    if (oUP_ReceiptInformationBE.Qty_receipted != null)
                        dr["Qty_receipted"] = oUP_ReceiptInformationBE.Qty_receipted;
                    else
                        dr["Qty_receipted"] = DBNull.Value;

                    dr["Other"] = oUP_ReceiptInformationBE.Other;

                    if (oUP_ReceiptInformationBE.Date != null)
                        dr["Date"] = oUP_ReceiptInformationBE.Date;
                    else
                        dr["Date"] = DBNull.Value;


                    dr["Country"] = oUP_ReceiptInformationBE.Country;

                    if (oUP_ReceiptInformationBE.ORGOrdDate != null)
                        dr["ORGOrdDate"] = oUP_ReceiptInformationBE.ORGOrdDate;
                    else
                        dr["ORGOrdDate"] = DBNull.Value;

                    if (oUP_ReceiptInformationBE.UpdatedDate != null)
                        dr["UpdatedDate"] = oUP_ReceiptInformationBE.UpdatedDate;
                    else
                        dr["UpdatedDate"] = DBNull.Value;

                    dt.Rows.Add(dr);
                }
                pUP_ReceiptInformationBE = null;


                SqlConnection sqlConn = new SqlConnection(DBConnection.Connection);
                sqlConn.Open();

                SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);
                oSqlBulkCopy.DestinationTableName = "UP_StageReceiptInformation";

                oSqlBulkCopy.ColumnMappings.Add("Viking_sku_no", "Viking_sku_no");
                oSqlBulkCopy.ColumnMappings.Add("OD_SKU_Number", "OD_SKU_Number");
                oSqlBulkCopy.ColumnMappings.Add("Vendor_prod_code", "Vendor_prod_code");
                oSqlBulkCopy.ColumnMappings.Add("Warehouse", "Warehouse");
                oSqlBulkCopy.ColumnMappings.Add("PO_number", "PO_number");
                oSqlBulkCopy.ColumnMappings.Add("Qty_receipted", "Qty_receipted");
                oSqlBulkCopy.ColumnMappings.Add("Other", "Other");
                oSqlBulkCopy.ColumnMappings.Add("Date", "Date");
                oSqlBulkCopy.ColumnMappings.Add("Country", "Country");
                oSqlBulkCopy.ColumnMappings.Add("ORGOrdDate", "ORGOrdDate");
                oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");

                oSqlBulkCopy.BulkCopyTimeout = 300;
                oSqlBulkCopy.BatchSize = dt.Rows.Count;

                oSqlBulkCopy.WriteToServer(dt);
                oSqlBulkCopy.Close();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="up_ReceiptInformationBEErrorList"></param>
        public void AddReceiptInformationErrors(List<UP_DataImportErrorBE> up_ReceiptInformationBEErrorList)
        {
            try
            {
                DataTable dt = new DataTable();
                DataRow dr;

                dt.Columns.Add(new DataColumn("FolderDownloadID", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("ManualFileUploadID", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));


                foreach (UP_DataImportErrorBE obj_DataImportErrorBE in up_ReceiptInformationBEErrorList)
                {
                    dr = dt.NewRow();

                    dr["FolderDownloadID"] = obj_DataImportErrorBE.FolderDownloadID;
                    dr["ManualFileUploadID"] = obj_DataImportErrorBE.ManualFileUploadID;
                    dr["ErrorDescription"] = obj_DataImportErrorBE.ErrorDescription;
                    dr["UpdatedDate"] = obj_DataImportErrorBE.UpdatedDate;


                    //if (oUP_SKUBE.Valuated_Stock != null)
                    //    dr["Valuated_Stock"] = oUP_SKUBE.Valuated_Stock;
                    //else
                    //    dr["Valuated_Stock"] = DBNull.Value;


                    dt.Rows.Add(dr);
                }
                up_ReceiptInformationBEErrorList = null;

                SqlConnection sqlConn = new SqlConnection(DBConnection.Connection);
                sqlConn.Open();

                SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);
                oSqlBulkCopy.DestinationTableName = "UP_ErrorReceiptInformation";

                oSqlBulkCopy.ColumnMappings.Add("FolderDownloadID", "FolderDownloadID");
                oSqlBulkCopy.ColumnMappings.Add("ManualFileUploadID", "ManualFileUploadID");
                oSqlBulkCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");
                oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");


                oSqlBulkCopy.BulkCopyTimeout = 300;
                oSqlBulkCopy.BatchSize = dt.Rows.Count;

                oSqlBulkCopy.WriteToServer(dt);
                oSqlBulkCopy.Close();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }


        }

    }
}

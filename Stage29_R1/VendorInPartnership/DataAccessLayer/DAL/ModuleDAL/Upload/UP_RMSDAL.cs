﻿using BusinessEntities.ModuleBE.Upload;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Upload
{
    public class UP_RMSDAL : BaseDAL
    {

        public void AddRMSs(List<UP_RMSBE> pUP_RMSBEList)
        {
            try
            {

                DataTable dt = new DataTable();
                DataRow dr;

                dt.Columns.Add(new DataColumn("Country"));
                dt.Columns.Add(new DataColumn("Viking_Code"));
                dt.Columns.Add(new DataColumn("OD_Code"));
                dt.Columns.Add(new DataColumn("Sap_Code"));
                dt.Columns.Add(new DataColumn("RMS_Code"));
                dt.Columns.Add(new DataColumn("Brand_Type"));
                dt.Columns.Add(new DataColumn("Brand_Label_Name"));
                dt.Columns.Add(new DataColumn("Master_Vendor_Number"));
                dt.Columns.Add(new DataColumn("Master_Vendor_Name"));
                dt.Columns.Add(new DataColumn("Division"));
                dt.Columns.Add(new DataColumn("Div_Name"));
                dt.Columns.Add(new DataColumn("Group_No"));
                dt.Columns.Add(new DataColumn("Group_Name"));
                dt.Columns.Add(new DataColumn("Dept"));
                dt.Columns.Add(new DataColumn("Dept_Name"));
                dt.Columns.Add(new DataColumn("Class"));
                dt.Columns.Add(new DataColumn("Class_Name"));
                dt.Columns.Add(new DataColumn("Subclass"));
                dt.Columns.Add(new DataColumn("Subclass_Name"));
                dt.Columns.Add(new DataColumn("Discontinued_Indicator"));
                dt.Columns.Add(new DataColumn("UpdatedDate"));

                foreach (UP_RMSBE oUP_RMSBE in pUP_RMSBEList)
                {
                    dr = dt.NewRow();
                    dr["Country"] = oUP_RMSBE.Country;
                    dr["Viking_Code"] = oUP_RMSBE.Viking_Code;
                    dr["OD_Code"] = oUP_RMSBE.OD_Code;
                    dr["Sap_Code"] = oUP_RMSBE.Sap_Code;
                    dr["RMS_Code"] = oUP_RMSBE.RMS_Code;
                    dr["Brand_Type"] = oUP_RMSBE.Brand_Type;
                    dr["Brand_Label_Name"] = oUP_RMSBE.Brand_Label_Name;
                    dr["Master_Vendor_Number"] = oUP_RMSBE.Master_Vendor_Number;
                    dr["Master_Vendor_Name"] = oUP_RMSBE.Master_Vendor_Name;
                    dr["Division"] = oUP_RMSBE.Division;
                    dr["Div_Name"] = oUP_RMSBE.Div_Name;
                    dr["Group_No"] = oUP_RMSBE.Group_No;
                    dr["Group_Name"] = oUP_RMSBE.Group_Name;
                    dr["Dept"] = oUP_RMSBE.Dept;
                    dr["Dept_Name"] = oUP_RMSBE.Dept_Name;
                    dr["Class"] = oUP_RMSBE.Class;
                    dr["Class_Name"] = oUP_RMSBE.Class_Name;
                    dr["Subclass"] = oUP_RMSBE.Subclass;
                    dr["Subclass_Name"] = oUP_RMSBE.Subclass_Name;
                    dr["Discontinued_Indicator"] = oUP_RMSBE.Discontinued_Indicator;
                    dr["UpdatedDate"] = oUP_RMSBE.UpdatedDate;

                    dt.Rows.Add(dr);
                }
                pUP_RMSBEList = null;
                SqlConnection sqlConn = new SqlConnection(DBConnection.Connection);
                sqlConn.Open();

                SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);
                oSqlBulkCopy.DestinationTableName = "UP_StageRMS";

                oSqlBulkCopy.ColumnMappings.Add("Country", "Country");
                oSqlBulkCopy.ColumnMappings.Add("Viking_Code", "Viking_Code");
                oSqlBulkCopy.ColumnMappings.Add("OD_Code", "OD_Code");
                oSqlBulkCopy.ColumnMappings.Add("Sap_Code", "Sap_Code");
                oSqlBulkCopy.ColumnMappings.Add("RMS_Code", "RMS_Code");
                oSqlBulkCopy.ColumnMappings.Add("Brand_Type", "Brand_Type");
                oSqlBulkCopy.ColumnMappings.Add("Brand_Label_Name", "Brand_Label_Name");
                oSqlBulkCopy.ColumnMappings.Add("Master_Vendor_Number", "Master_Vendor_Number");
                oSqlBulkCopy.ColumnMappings.Add("Master_Vendor_Name", "Master_Vendor_Name");
                oSqlBulkCopy.ColumnMappings.Add("Division", "Division");
                oSqlBulkCopy.ColumnMappings.Add("Div_Name", "Div_Name");
                oSqlBulkCopy.ColumnMappings.Add("Group_No", "Group_No");
                oSqlBulkCopy.ColumnMappings.Add("Group_Name", "Group_Name");
                oSqlBulkCopy.ColumnMappings.Add("Dept", "Dept");
                oSqlBulkCopy.ColumnMappings.Add("Dept_Name", "Dept_Name");
                oSqlBulkCopy.ColumnMappings.Add("Class", "Class");
                oSqlBulkCopy.ColumnMappings.Add("Class_Name", "Class_Name");
                oSqlBulkCopy.ColumnMappings.Add("Subclass", "Subclass");
                oSqlBulkCopy.ColumnMappings.Add("Subclass_Name", "Subclass_Name");
                oSqlBulkCopy.ColumnMappings.Add("Discontinued_Indicator", "Discontinued_Indicator");
                oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");

                oSqlBulkCopy.BulkCopyTimeout = 300;
                oSqlBulkCopy.BatchSize = dt.Rows.Count;

                oSqlBulkCopy.WriteToServer(dt);
                oSqlBulkCopy.Close();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UP_RMSBEErrorList"></param>
        public void AddRMSErrors(List<UP_DataImportErrorBE> UP_RMSBEErrorList)
        {
            try
            {
                DataTable dt = new DataTable();
                DataRow dr;

                dt.Columns.Add(new DataColumn("FolderDownloadID", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("ManualFileUploadID", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));


                foreach (UP_DataImportErrorBE obj_DataImportErrorBE in UP_RMSBEErrorList)
                {
                    dr = dt.NewRow();

                    dr["FolderDownloadID"] = obj_DataImportErrorBE.FolderDownloadID;
                    dr["ManualFileUploadID"] = obj_DataImportErrorBE.ManualFileUploadID;
                    dr["ErrorDescription"] = obj_DataImportErrorBE.ErrorDescription;
                    dr["UpdatedDate"] = obj_DataImportErrorBE.UpdatedDate;


                    //if (oUP_RMSBE.Valuated_Stock != null)
                    //    dr["Valuated_Stock"] = oUP_RMSBE.Valuated_Stock;
                    //else
                    //    dr["Valuated_Stock"] = DBNull.Value;


                    dt.Rows.Add(dr);
                }
                UP_RMSBEErrorList = null;

                SqlConnection sqlConn = new SqlConnection(DBConnection.Connection);
                sqlConn.Open();

                SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);
                oSqlBulkCopy.DestinationTableName = "UP_ErrorRMS";

                oSqlBulkCopy.ColumnMappings.Add("FolderDownloadID", "FolderDownloadID");
                oSqlBulkCopy.ColumnMappings.Add("ManualFileUploadID", "ManualFileUploadID");
                oSqlBulkCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");
                oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");


                oSqlBulkCopy.BulkCopyTimeout = 300;
                oSqlBulkCopy.BatchSize = dt.Rows.Count;

                oSqlBulkCopy.WriteToServer(dt);
                oSqlBulkCopy.Close();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }


        }
    }
}

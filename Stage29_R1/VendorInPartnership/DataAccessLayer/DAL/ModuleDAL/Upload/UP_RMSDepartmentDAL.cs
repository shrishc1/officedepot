﻿using BusinessEntities.ModuleBE.Upload;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Upload
{
    public class UP_RMSDepartmentDAL
    {

        public List<UP_RMSDepartmentBE> GetRMSDepartmentBAL(UP_RMSDepartmentBE oUP_RMSDepartmentBE)
        {

            List<UP_RMSDepartmentBE> lstUP_RMSDepartmentBE = new List<UP_RMSDepartmentBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUP_RMSDepartmentBE.Action);
                param[index++] = new SqlParameter("@RMSDepartment", oUP_RMSDepartmentBE.Dept_Name);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_RMSCategory", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    UP_RMSDepartmentBE oNewUP_RMSDepartmentBE = new UP_RMSDepartmentBE();
                    oNewUP_RMSDepartmentBE.Dept_Name = dr["Dept_Name"].ToString();
                    oNewUP_RMSDepartmentBE.Dept_No = dr["Dept_No"].ToString();
                    oNewUP_RMSDepartmentBE.RMSDepartmentID = Convert.ToInt32(dr["RMSDepartmentID"]);
                    lstUP_RMSDepartmentBE.Add(oNewUP_RMSDepartmentBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstUP_RMSDepartmentBE;

        }

        public int? UpdateRMSDepartmentBAL(UP_RMSDepartmentBE oUP_RMSDepartmentBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUP_RMSDepartmentBE.Action);
                param[index++] = new SqlParameter("@RMSCategoryID", oUP_RMSDepartmentBE.RMSCategory.RMSCategoryID);
                param[index++] = new SqlParameter("@UserID", oUP_RMSDepartmentBE.RMSCategory.CreatedBy);
                param[index++] = new SqlParameter("@RMSDepartmentIDs", oUP_RMSDepartmentBE.RMSDepartmentByIDs);
                //param[index++] = new SqlParameter("@RMSDepartmentByLeftIDs", oUP_RMSDepartmentBE.RMSDepartmentByLeftIDs);    

                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "[spMAS_RMSCategory]", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<UP_RMSDepartmentBE> GetRMSDepartmentByCategoryIDBAL(UP_RMSDepartmentBE oUP_RMSDepartmentBE)
        {

            List<UP_RMSDepartmentBE> lstUP_RMSDepartmentBE = new List<UP_RMSDepartmentBE>();
            UP_RMSDepartmentBE oNewUP_RMSDepartmentBE;
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUP_RMSDepartmentBE.Action);
                param[index++] = new SqlParameter("@RMSCategoryID", oUP_RMSDepartmentBE.RMSCategory.RMSCategoryID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_RMSCategory", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    oNewUP_RMSDepartmentBE = new UP_RMSDepartmentBE();
                    oNewUP_RMSDepartmentBE.Dept_Name = dr["Dept_Name"].ToString();
                    oNewUP_RMSDepartmentBE.Dept_No = dr["Dept_No"].ToString();
                    oNewUP_RMSDepartmentBE.RMSDepartmentID = Convert.ToInt32(dr["RMSDepartmentID"]);
                    lstUP_RMSDepartmentBE.Add(oNewUP_RMSDepartmentBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstUP_RMSDepartmentBE;

        }

    }
}

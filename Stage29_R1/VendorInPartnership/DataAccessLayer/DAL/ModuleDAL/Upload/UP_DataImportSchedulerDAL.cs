﻿using BusinessEntities.ModuleBE.Upload;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;
using BusinessEntities.ModuleBE.AdminFunctions;

namespace DataAccessLayer.ModuleDAL.Upload
{
    public class UP_DataImportSchedulerDAL
    {

        public List<UP_DataImportSchedulerBE> GetDataImportDetailDAL(UP_DataImportSchedulerBE oUP_DataImportSchedulerBE)
        {
            List<UP_DataImportSchedulerBE> UP_DataImportSchedulerBEList = new List<UP_DataImportSchedulerBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUP_DataImportSchedulerBE.Action);
                param[index++] = new SqlParameter("@DateFrom", oUP_DataImportSchedulerBE.DateFrom);
                param[index++] = new SqlParameter("@DateTo", oUP_DataImportSchedulerBE.DateTo);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_DataImportScheduler", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    UP_DataImportSchedulerBE oNewUP_DataImportSchedulerBEList = new UP_DataImportSchedulerBE();
                    oNewUP_DataImportSchedulerBEList.DataImportSchedulerID = Convert.ToInt32(dr["DataImportSchedulerID"]);
                    oNewUP_DataImportSchedulerBEList.FolderDownloadID = Convert.ToInt32(dr["FolderDownloadID"]);
                    oNewUP_DataImportSchedulerBEList.FolderName = dr["FolderName"].ToString();
                    oNewUP_DataImportSchedulerBEList.StartTime = dr["StartTime"] != DBNull.Value ? Convert.ToDateTime(dr["StartTime"]) : (DateTime?)null;
                    oNewUP_DataImportSchedulerBEList.EndTime = dr["EndTime"] != DBNull.Value ? Convert.ToDateTime(dr["EndTime"]) : (DateTime?)null;
                    oNewUP_DataImportSchedulerBEList.TimeTaken = dr["TimeTaken"].ToString();
                    oNewUP_DataImportSchedulerBEList.TotalVendorRecord = dr["TotalVendorRecord"].ToString();
                    oNewUP_DataImportSchedulerBEList.ErrorVendorCount = dr["ErrorVendorCount"].ToString();
                    oNewUP_DataImportSchedulerBEList.TotalSKURecord = dr["TotalSKURecord"].ToString();
                    oNewUP_DataImportSchedulerBEList.ErrorSKUCount = dr["ErrorSKUCount"].ToString();
                    oNewUP_DataImportSchedulerBEList.TotalPORecord = dr["TotalPORecord"].ToString();
                    oNewUP_DataImportSchedulerBEList.ErrorPOCount = dr["ErrorPOCount"].ToString();
                    oNewUP_DataImportSchedulerBEList.TotalReceiptInformationRecord = dr["TotalReceiptInformationRecord"].ToString();
                    oNewUP_DataImportSchedulerBEList.ErrorReceiptCount = dr["ErrorReceiptCount"].ToString();
                    oNewUP_DataImportSchedulerBEList.ExecutionStatus = dr["ExecutionStatus"].ToString();

                    oNewUP_DataImportSchedulerBEList.TotalExpediteRecord = dr["TotalExpediteRecord"].ToString();
                    oNewUP_DataImportSchedulerBEList.ErrorExpediteCount = dr["ErrorExpediteCount"].ToString();

                    oNewUP_DataImportSchedulerBEList.ErrorBOCount = dr["ErrorBOCount"].ToString();

                    //oNewUP_DataImportSchedulerBEList.ErrorMessage = dr["ErrorMessage"].ToString();
                    oNewUP_DataImportSchedulerBEList.InvokedBy = dr["InvokedBy"].ToString();
                    UP_DataImportSchedulerBEList.Add(oNewUP_DataImportSchedulerBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return UP_DataImportSchedulerBEList;
        }

        public int? GetDataImportDetailErrorCountDAL(UP_DataImportSchedulerBE oUP_DataImportSchedulerBE)
        {
            int? intResult = 0;
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUP_DataImportSchedulerBE.Action);
                param[index++] = new SqlParameter("@DateFrom", oUP_DataImportSchedulerBE.DateFrom);
                param[index++] = new SqlParameter("@DateTo", oUP_DataImportSchedulerBE.DateTo);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_DataImportScheduler", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<UP_DataImportSchedulerBE> GetGetFileProcessedDataDAL(UP_DataImportSchedulerBE oUP_DataImportSchedulerBE)
        {
            List<UP_DataImportSchedulerBE> UP_DataImportSchedulerBEList = new List<UP_DataImportSchedulerBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUP_DataImportSchedulerBE.Action);
                param[index++] = new SqlParameter("@DataImportSchedulerID", oUP_DataImportSchedulerBE.DataImportSchedulerID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_DataImportScheduler", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    UP_DataImportSchedulerBE oNewUP_DataImportSchedulerBEList = new UP_DataImportSchedulerBE();

                    oNewUP_DataImportSchedulerBEList.FileName = dr["FileName"].ToString();
                    oNewUP_DataImportSchedulerBEList.NoOfRecordProcessed = dr["NoOfRecordProcessed"] != DBNull.Value ? Convert.ToInt32(dr["NoOfRecordProcessed"]) : (int?)null;
                    oNewUP_DataImportSchedulerBEList.ErrorRecordCount = dr["ErrorRecordCount"] != DBNull.Value ? Convert.ToInt32(dr["ErrorRecordCount"]) : (int?)null;
                    oNewUP_DataImportSchedulerBEList.FolderName = dr["FolderName"] != DBNull.Value ? dr["FolderName"].ToString() : "";
                    oNewUP_DataImportSchedulerBEList.CountryName = dr["CountryName"] != DBNull.Value ? dr["CountryName"].ToString() : "";
                    oNewUP_DataImportSchedulerBEList.LogFilePath = dr["LogFilePath"] != DBNull.Value ? dr["LogFilePath"].ToString() : "";

                    UP_DataImportSchedulerBEList.Add(oNewUP_DataImportSchedulerBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return UP_DataImportSchedulerBEList;
        }


        public List<UP_DataImportSchedulerBE> GetErrorMessage(UP_DataImportSchedulerBE oUP_DataImportSchedulerBE)
        {
            List<UP_DataImportSchedulerBE> UP_DataImportSchedulerBEList = new List<UP_DataImportSchedulerBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUP_DataImportSchedulerBE.Action);
                param[index++] = new SqlParameter("@DataImportSchedulerID", oUP_DataImportSchedulerBE.DataImportSchedulerID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_DataImportScheduler", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    UP_DataImportSchedulerBE oNewUP_DataImportSchedulerBEList = new UP_DataImportSchedulerBE();

                    oNewUP_DataImportSchedulerBEList.ErrorMessage = dr["ErrorMessage"].ToString();

                    UP_DataImportSchedulerBEList.Add(oNewUP_DataImportSchedulerBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return UP_DataImportSchedulerBEList;
        }

        public DataSet GetErrorDetailDAL(UP_DataImportSchedulerBE oUP_DataImportSchedulerBE)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUP_DataImportSchedulerBE.Action);
                param[index++] = new SqlParameter("@FolderDownloadID", oUP_DataImportSchedulerBE.FolderDownloadID);
                param[index++] = new SqlParameter("@ManualFileUploadID", oUP_DataImportSchedulerBE.ManualFileUploadID);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_DataImportScheduler", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public DataSet CheckIsDataImportRunningDAL(string Action)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                if (Action == "")
                    param[index++] = new SqlParameter("@Action", "CheckByStageTable");
                else
                    param[index++] = new SqlParameter("@Action", Action);
                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spCheckDataImportRunning", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public string CheckUpdateImportStatusDAL(UP_DataImportSchedulerBE oUP_DataImportSchedulerBE)
        {
            string strResult = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUP_DataImportSchedulerBE.Action);
                param[index++] = new SqlParameter("@ProcessType", oUP_DataImportSchedulerBE.ProcessType);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spCheckDataImportRunning", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    strResult = Convert.ToString(Result.Tables[0].Rows[0][0]);
                else
                    strResult = string.Empty;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }

        public string DeleteImportStatusDAL(UP_DataImportSchedulerBE oUP_DataImportSchedulerBE)
        {
            string strResult = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oUP_DataImportSchedulerBE.Action);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spCheckDataImportRunning", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    strResult = Convert.ToString(Result.Tables[0].Rows[0][0]);
                else
                    strResult = string.Empty;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }

        public List<Up_PurchaseOrderDetailBE> GetImportNewPOLetterMainDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.VendorId);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@Order_raised", oUp_PurchaseOrderDetailBE.Order_raised);

                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    var purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                    purchaseOrderDetailBE.Purchase_order = dr["PO"] != DBNull.Value ? dr["PO"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Warehouse = dr["Warehouse"] != DBNull.Value ? dr["Warehouse"].ToString() : string.Empty;
                    purchaseOrderDetailBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"]) : (int?)null;
                    purchaseOrderDetailBE.StockPlannerNo = dr["StockPlannerNo"] != DBNull.Value ? dr["StockPlannerNo"].ToString() : string.Empty;
                    purchaseOrderDetailBE.StockPlannerName = dr["StockPlannerName"] != DBNull.Value ? dr["StockPlannerName"].ToString() : string.Empty;
                    purchaseOrderDetailBE.StockPlannerContactNo = dr["StockPlannerContactNo"] != DBNull.Value ? dr["StockPlannerContactNo"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Buyer_no = dr["Buyer_no"] != DBNull.Value ? dr["Buyer_no"].ToString() : string.Empty;

                    purchaseOrderDetailBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    purchaseOrderDetailBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                    purchaseOrderDetailBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Vendor.address1 = dr["VendorAdd1"] != DBNull.Value ? dr["VendorAdd1"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Vendor.address2 = dr["VendorAdd2"] != DBNull.Value ? dr["VendorAdd2"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Vendor.city = dr["VendorCity"] != DBNull.Value ? dr["VendorCity"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Vendor.county = dr["VendorCounty"] != DBNull.Value ? dr["VendorCounty"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Vendor.VMPPIN = dr["VendorPin"] != DBNull.Value ? dr["VendorPin"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Vendor.Fax_no = dr["VendorFax"] != DBNull.Value ? dr["VendorFax"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Vendor.LanguageID = dr["VendorLanguageID"] != DBNull.Value ? Convert.ToInt32(dr["VendorLanguageID"]) : 0;
                    purchaseOrderDetailBE.Vendor.Language = dr["VendorLanguage"] != DBNull.Value ? dr["VendorLanguage"].ToString() : string.Empty;

                    purchaseOrderDetailBE.Order_raised = dr["OrderRaised"] != DBNull.Value ? Convert.ToDateTime(dr["OrderRaised"]) : (DateTime?)null;
                    purchaseOrderDetailBE.Original_due_date = dr["OriginalDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["OriginalDueDate"]) : (DateTime?)null;
                    purchaseOrderDetailBE.EntryType = dr["EntryType"] != DBNull.Value ? dr["EntryType"].ToString() : string.Empty;

                    purchaseOrderDetailBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    purchaseOrderDetailBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    purchaseOrderDetailBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.SiteAddressLine1 = dr["ODAdd1"] != DBNull.Value ? dr["ODAdd1"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.SiteAddressLine2 = dr["ODAdd2"] != DBNull.Value ? dr["ODAdd2"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.SiteAddressLine3 = dr["ODAdd3"] != DBNull.Value ? dr["ODAdd3"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.SiteAddressLine4 = dr["ODAdd4"] != DBNull.Value ? dr["ODAdd4"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.SiteAddressLine5 = dr["ODAdd5"] != DBNull.Value ? dr["ODAdd5"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.SiteAddressLine6 = dr["ODAdd6"] != DBNull.Value ? dr["ODAdd6"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.SiteCountryName = dr["ODCountryName"] != DBNull.Value ? dr["ODCountryName"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.SitePincode = dr["ODPinCode"] != DBNull.Value ? dr["ODPinCode"].ToString() : string.Empty;

                    purchaseOrderDetailBE.Site.APAddressLine1 = dr["APAdd1"] != DBNull.Value ? dr["APAdd1"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.APAddressLine2 = dr["APAdd2"] != DBNull.Value ? dr["APAdd2"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.APAddressLine3 = dr["APAdd3"] != DBNull.Value ? dr["APAdd3"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.APAddressLine4 = dr["APAdd4"] != DBNull.Value ? dr["APAdd4"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.APAddressLine5 = dr["APAdd5"] != DBNull.Value ? dr["APAdd5"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.APAddressLine6 = dr["APAdd6"] != DBNull.Value ? dr["APAdd6"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.APCountry = dr["APCountryName"] != DBNull.Value ? dr["APCountryName"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Site.APPincode = dr["APPinCode"] != DBNull.Value ? dr["APPinCode"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Snooty = dr["Snooty"] != DBNull.Value ? dr["Snooty"].ToString() : string.Empty;

                    if (dt.Columns.Contains("DeliveryInstructions"))
                        purchaseOrderDetailBE.Vendor.DeliveryInstructions= dr["DeliveryInstructions"] != DBNull.Value ? dr["DeliveryInstructions"].ToString() : string.Empty;
                    if (dt.Columns.Contains("PaymentTerms"))
                        purchaseOrderDetailBE.Vendor.PaymentTerms = dr["PaymentTerms"] != DBNull.Value ? dr["PaymentTerms"].ToString() : string.Empty;

                    purchaseOrderDetailBE.VendorPOC = new MAS_MaintainVendorPointsContactBE();

                    if (dt.Columns.Contains("RoleTitle"))
                        purchaseOrderDetailBE.VendorPOC.RoleTitle = dr["RoleTitle"] != DBNull.Value ? Convert.ToString(dr["RoleTitle"]) : string.Empty;
                    if (dt.Columns.Contains("PhoneNumber"))
                        purchaseOrderDetailBE.VendorPOC.PhoneNumber = dr["PhoneNumber"] != DBNull.Value ? Convert.ToString(dr["PhoneNumber"]) : string.Empty;

                    lstPurchaseOrderDetailBE.Add(purchaseOrderDetailBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstPurchaseOrderDetailBE;
        }

        public List<Up_PurchaseOrderDetailBE> GetImportInsertNewSentPOEmailDataDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> lstUp_PurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@PrmWarehouse", oUp_PurchaseOrderDetailBE.Warehouse);
                param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@Order_raised", oUp_PurchaseOrderDetailBE.Order_raised);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    var purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                    purchaseOrderDetailBE.Line_No = dr["Line"] != DBNull.Value ? Convert.ToInt32(dr["Line"]) : (int?)null;
                    purchaseOrderDetailBE.Direct_code = dr["VikingCode"] != DBNull.Value ? Convert.ToString(dr["VikingCode"]) : string.Empty;
                    purchaseOrderDetailBE.OD_Code = dr["ODCode"] != DBNull.Value ? Convert.ToString(dr["ODCode"]) : string.Empty;
                    purchaseOrderDetailBE.Vendor_Code = dr["VendorCode"] != DBNull.Value ? dr["VendorCode"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Product_description = dr["Description"] != DBNull.Value ? dr["Description"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Original_quantity = dr["UnitsRequired"] != DBNull.Value ? dr["UnitsRequired"].ToString() : string.Empty;
                    purchaseOrderDetailBE.Outstanding_Qty = dr["Outstanding_Qty"] != DBNull.Value ? dr["Outstanding_Qty"].ToString() : string.Empty;
                    purchaseOrderDetailBE.UOM = dr["Unit"] != DBNull.Value ? dr["Unit"].ToString() : string.Empty;
                    purchaseOrderDetailBE.PO_cost = dr["UnitCost"] != DBNull.Value ? Convert.ToDecimal(dr["UnitCost"]) : (decimal?)null;
                    purchaseOrderDetailBE.TotalCost = dr["TotalCost"] != DBNull.Value ? Convert.ToDecimal(dr["TotalCost"]) : (decimal?)null;
                    purchaseOrderDetailBE.Original_due_date = dr["DeliveryDate"] != DBNull.Value ? Convert.ToDateTime(dr["DeliveryDate"]) : (DateTime?)null;
                    purchaseOrderDetailBE.Currency = dr["Currency"] != DBNull.Value ? Convert.ToString(dr["Currency"]) : string.Empty;
                    purchaseOrderDetailBE.Snooty = dr["Snooty"] != DBNull.Value ? Convert.ToString(dr["Snooty"]) : string.Empty;
                    purchaseOrderDetailBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                    if (ds.Tables[0].Columns.Contains("DeliveryInstructions"))
                        purchaseOrderDetailBE.Vendor.DeliveryInstructions = dr["DeliveryInstructions"] != DBNull.Value ? Convert.ToString(dr["DeliveryInstructions"]) : string.Empty;
                    if (ds.Tables[0].Columns.Contains("PaymentTerms"))
                        purchaseOrderDetailBE.Vendor.PaymentTerms = dr["PaymentTerms"] != DBNull.Value ? Convert.ToString(dr["PaymentTerms"]) : string.Empty;

                    purchaseOrderDetailBE.VendorPOC = new MAS_MaintainVendorPointsContactBE();
                    if (ds.Tables[0].Columns.Contains("RoleTitle"))
                        purchaseOrderDetailBE.VendorPOC.RoleTitle = dr["RoleTitle"] != DBNull.Value ? Convert.ToString(dr["RoleTitle"]) : string.Empty;
                    if (ds.Tables[0].Columns.Contains("PhoneNumber"))
                        purchaseOrderDetailBE.VendorPOC.PhoneNumber = dr["PhoneNumber"] != DBNull.Value ? Convert.ToString(dr["PhoneNumber"]) : string.Empty;

                    lstUp_PurchaseOrderDetailBE.Add(purchaseOrderDetailBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstUp_PurchaseOrderDetailBE;
        }

        public DataImportLogBE GetImportDataLogDAL(DataImportLogBE odataImportLogBE)
        {
            var dataImportLog = new DataImportLogBE();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", "Logs");
                param[index++] = new SqlParameter("@DataImportSchedulerID", odataImportLogBE.DataImportSchedulerID);
                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spImportData", param);
                dt = ds.Tables[0];

                dataImportLog.Id = dt.Rows[0]["Id"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["Id"]) : 0;
                dataImportLog.DataImportSchedulerID = dt.Rows[0]["DataImportSchedulerID"] != DBNull.Value ?
                    Convert.ToInt32(dt.Rows[0]["DataImportSchedulerID"]) : 0;
                dataImportLog.TaskRunning = dt.Rows[0]["TaskRunning"] != DBNull.Value ? dt.Rows[0]["TaskRunning"].ToString() : null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dataImportLog;
        }
    }
}


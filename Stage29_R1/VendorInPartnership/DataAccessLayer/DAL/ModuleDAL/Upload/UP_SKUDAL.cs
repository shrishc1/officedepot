﻿using BusinessEntities.ModuleBE.Upload;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Upload
{
    public class UP_SKUDAL : BaseDAL
    {

        public List<UP_SKUBE> GetSKU(UP_SKUBE oUP_SKUBE)
        {
            List<UP_SKUBE> lstUP_SKU = new List<UP_SKUBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUP_SKUBE.Action);
                param[index++] = new SqlParameter("@SKUID", oUP_SKUBE.SKUID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_SKU", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    UP_SKUBE oNewUP_SKUBE = new UP_SKUBE();
                    oNewUP_SKUBE.SKUID = Convert.ToInt32(dr["SKUID"]);
                    oNewUP_SKUBE.DESCRIPTION = dr["DESCRIPTION"].ToString();
                    lstUP_SKU.Add(oNewUP_SKUBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstUP_SKU;
        }

        public void AddSKUs(List<UP_SKUBE> pUP_SKUBEList)
        {
            try
            {

                DataTable dt = new DataTable();
                DataRow dr;

                dt.Columns.Add(new DataColumn("Direct_SKU"));
                dt.Columns.Add(new DataColumn("OD_SKU_NO"));
                dt.Columns.Add(new DataColumn("DESCRIPTION"));
                dt.Columns.Add(new DataColumn("Vendor_Code"));
                dt.Columns.Add(new DataColumn("Warehouse"));
                dt.Columns.Add(new DataColumn("Date"));
                dt.Columns.Add(new DataColumn("Qty_Sold_Yesterday", typeof(System.Decimal)));
                dt.Columns.Add(new DataColumn("Qty_On_Hand", typeof(System.Decimal)));
                dt.Columns.Add(new DataColumn("qty_on_backorder", typeof(System.Decimal)));
                dt.Columns.Add(new DataColumn("Balance", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("Item_Val", typeof(System.Decimal)));
                dt.Columns.Add(new DataColumn("Valuated_Stock"));
                dt.Columns.Add(new DataColumn("Currency"));
                dt.Columns.Add(new DataColumn("Vendor_no"));
                dt.Columns.Add(new DataColumn("Vendor_Name"));
                dt.Columns.Add(new DataColumn("subvndr"));
                dt.Columns.Add(new DataColumn("Product_Lead_time", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("Leadtime_Variance", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("UOM"));
                dt.Columns.Add(new DataColumn("Buyer_no"));
                dt.Columns.Add(new DataColumn("Item_Category"));
                dt.Columns.Add(new DataColumn("Item_classification"));
                dt.Columns.Add(new DataColumn("New_Item"));
                dt.Columns.Add(new DataColumn("ICASUN"));
                dt.Columns.Add(new DataColumn("ILAYUN"));
                dt.Columns.Add(new DataColumn("IPALUN"));
                dt.Columns.Add(new DataColumn("IMINQT"));
                dt.Columns.Add(new DataColumn("IBMULT"));
                dt.Columns.Add(new DataColumn("Category"));
                dt.Columns.Add(new DataColumn("Back Order", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("Country"));
                dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));
                dt.Columns.Add(new DataColumn("Total_Qty_BO", typeof(System.Decimal)));
                dt.Columns.Add(new DataColumn("Total_Count_BO", typeof(System.Decimal)));

                dt.Columns.Add(new DataColumn("Total_Orders", typeof(System.Decimal)));
                dt.Columns.Add(new DataColumn("Total_Sales", typeof(System.Decimal)));


                foreach (UP_SKUBE oUP_SKUBE in pUP_SKUBEList)
                {
                    dr = dt.NewRow();
                    dr["Direct_SKU"] = oUP_SKUBE.Direct_SKU;
                    dr["OD_SKU_NO"] = oUP_SKUBE.OD_SKU_NO;
                    dr["DESCRIPTION"] = oUP_SKUBE.DESCRIPTION;
                    dr["Vendor_Code"] = oUP_SKUBE.Vendor_Code;
                    dr["Warehouse"] = oUP_SKUBE.Warehouse;
                    dr["Date"] = oUP_SKUBE.Date;

                    if (!string.IsNullOrWhiteSpace(oUP_SKUBE.Qty_Sold_Yesterday))
                        dr["Qty_Sold_Yesterday"] = oUP_SKUBE.Qty_Sold_Yesterday;
                    else
                        dr["Qty_Sold_Yesterday"] = DBNull.Value;

                    if (!string.IsNullOrWhiteSpace(oUP_SKUBE.Qty_On_Hand))
                        dr["Qty_On_Hand"] = oUP_SKUBE.Qty_On_Hand;
                    else
                        dr["Qty_On_Hand"] = DBNull.Value;

                    if (!string.IsNullOrWhiteSpace(oUP_SKUBE.qty_on_backorder))
                        dr["qty_on_backorder"] = oUP_SKUBE.qty_on_backorder;
                    else
                        dr["qty_on_backorder"] = DBNull.Value;

                    if (oUP_SKUBE.Balance != null)
                        dr["Balance"] = oUP_SKUBE.Balance;
                    else
                        dr["Balance"] = DBNull.Value;

                    if (!string.IsNullOrWhiteSpace(oUP_SKUBE.Item_Val))
                        dr["Item_Val"] = oUP_SKUBE.Item_Val;
                    else
                        dr["Item_Val"] = DBNull.Value;

                    if (!string.IsNullOrWhiteSpace(oUP_SKUBE.Valuated_Stock))
                        dr["Valuated_Stock"] = oUP_SKUBE.Valuated_Stock;
                    else
                        dr["Valuated_Stock"] = DBNull.Value;

                    dr["Currency"] = oUP_SKUBE.Currency;
                    dr["Vendor_no"] = oUP_SKUBE.Vendor_no;
                    dr["Vendor_Name"] = oUP_SKUBE.Vendor_Name;
                    dr["subvndr"] = oUP_SKUBE.subvndr;

                    if (!string.IsNullOrWhiteSpace(oUP_SKUBE.Product_Lead_time))
                        dr["Product_Lead_time"] = oUP_SKUBE.Product_Lead_time;
                    else
                        dr["Product_Lead_time"] = DBNull.Value;

                    if (!string.IsNullOrWhiteSpace(oUP_SKUBE.Leadtime_Variance))
                        dr["Leadtime_Variance"] = oUP_SKUBE.Leadtime_Variance;
                    else
                        dr["Leadtime_Variance"] = DBNull.Value;

                    dr["UOM"] = oUP_SKUBE.UOM;
                    dr["Buyer_no"] = oUP_SKUBE.Buyer_no;
                    dr["Item_Category"] = oUP_SKUBE.Item_Category;
                    dr["Item_classification"] = oUP_SKUBE.Item_classification;
                    dr["New_Item"] = oUP_SKUBE.New_Item;
                    dr["ICASUN"] = oUP_SKUBE.ICASUN;
                    dr["ILAYUN"] = oUP_SKUBE.ILAYUN;
                    dr["IPALUN"] = oUP_SKUBE.IPALUN;
                    dr["IMINQT"] = oUP_SKUBE.IMINQT;
                    dr["IBMULT"] = oUP_SKUBE.IBMULT;
                    dr["Category"] = oUP_SKUBE.Category;

                    if (!string.IsNullOrWhiteSpace(oUP_SKUBE.BackOrder))
                        dr["Back Order"] = oUP_SKUBE.BackOrder;
                    else
                        dr["Back Order"] = DBNull.Value;

                    dr["Country"] = oUP_SKUBE.Country;
                    dr["UpdatedDate"] = oUP_SKUBE.UpdatedDate;

                    if (!string.IsNullOrWhiteSpace(oUP_SKUBE.Total_Qty_BO))
                        dr["Total_Qty_BO"] = oUP_SKUBE.Total_Qty_BO;
                    else
                        dr["Total_Qty_BO"] = DBNull.Value;

                    if (!string.IsNullOrWhiteSpace(oUP_SKUBE.Total_Count_BO))
                        dr["Total_Count_BO"] = oUP_SKUBE.Total_Count_BO;
                    else
                        dr["Total_Count_BO"] = DBNull.Value;

                    if (!string.IsNullOrWhiteSpace(oUP_SKUBE.Total_Orders))
                        dr["Total_Orders"] = oUP_SKUBE.Total_Orders;
                    else
                        dr["Total_Orders"] = DBNull.Value;

                    if (!string.IsNullOrWhiteSpace(oUP_SKUBE.Total_Sales))
                        dr["Total_Sales"] = oUP_SKUBE.Total_Sales;
                    else
                        dr["Total_Sales"] = DBNull.Value;

                    dt.Rows.Add(dr);
                }
                pUP_SKUBEList = null;

                SqlConnection sqlConn = new SqlConnection(DBConnection.Connection);
                sqlConn.Open();

                SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);
                oSqlBulkCopy.DestinationTableName = "UP_StageSKU";

                oSqlBulkCopy.ColumnMappings.Add("Direct_SKU", "Direct_SKU");
                oSqlBulkCopy.ColumnMappings.Add("OD_SKU_NO", "OD_SKU_NO");
                oSqlBulkCopy.ColumnMappings.Add("DESCRIPTION", "DESCRIPTION");
                oSqlBulkCopy.ColumnMappings.Add("Vendor_Code", "Vendor_Code");
                oSqlBulkCopy.ColumnMappings.Add("Warehouse", "Warehouse");
                oSqlBulkCopy.ColumnMappings.Add("Date", "Date");
                oSqlBulkCopy.ColumnMappings.Add("Qty_Sold_Yesterday", "Qty_Sold_Yesterday");
                oSqlBulkCopy.ColumnMappings.Add("Qty_On_Hand", "Qty_On_Hand");
                oSqlBulkCopy.ColumnMappings.Add("qty_on_backorder", "qty_on_backorder");
                oSqlBulkCopy.ColumnMappings.Add("Balance", "Balance");
                oSqlBulkCopy.ColumnMappings.Add("Item_Val", "Item_Val");
                oSqlBulkCopy.ColumnMappings.Add("Valuated_Stock", "Valuated_Stock");
                oSqlBulkCopy.ColumnMappings.Add("Currency", "Currency");
                oSqlBulkCopy.ColumnMappings.Add("Vendor_no", "Vendor_no");
                oSqlBulkCopy.ColumnMappings.Add("Vendor_Name", "Vendor_Name");
                oSqlBulkCopy.ColumnMappings.Add("subvndr", "subvndr");
                oSqlBulkCopy.ColumnMappings.Add("Product_Lead_time", "Product_Lead_time");
                oSqlBulkCopy.ColumnMappings.Add("Leadtime_Variance", "Leadtime_Variance");
                oSqlBulkCopy.ColumnMappings.Add("UOM", "UOM");
                oSqlBulkCopy.ColumnMappings.Add("Buyer_no", "Buyer_no");
                oSqlBulkCopy.ColumnMappings.Add("Item_Category", "Item_Category");
                oSqlBulkCopy.ColumnMappings.Add("Item_classification", "Item_classification");
                oSqlBulkCopy.ColumnMappings.Add("New_Item", "New_Item");
                oSqlBulkCopy.ColumnMappings.Add("ICASUN", "ICASUN");
                oSqlBulkCopy.ColumnMappings.Add("ILAYUN", "ILAYUN");
                oSqlBulkCopy.ColumnMappings.Add("IPALUN", "IPALUN");
                oSqlBulkCopy.ColumnMappings.Add("IMINQT", "IMINQT");
                oSqlBulkCopy.ColumnMappings.Add("IBMULT", "IBMULT");
                oSqlBulkCopy.ColumnMappings.Add("Category", "Category");
                oSqlBulkCopy.ColumnMappings.Add("Back Order", "Back Order");
                oSqlBulkCopy.ColumnMappings.Add("Country", "Country");
                oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                oSqlBulkCopy.ColumnMappings.Add("Total_Qty_BO", "Total_Qty_BO");
                oSqlBulkCopy.ColumnMappings.Add("Total_Count_BO", "Total_Count_BO");

                oSqlBulkCopy.ColumnMappings.Add("Total_Orders", "Total_Orders");
                oSqlBulkCopy.ColumnMappings.Add("Total_Sales", "Total_Sales");

                oSqlBulkCopy.BulkCopyTimeout = 300;
                oSqlBulkCopy.BatchSize = dt.Rows.Count;

                oSqlBulkCopy.WriteToServer(dt);
                oSqlBulkCopy.Close();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="up_SKUBEErrorList"></param>
        public void AddSKUErrors(List<UP_DataImportErrorBE> up_SKUBEErrorList)
        {
            try
            {
                DataTable dt = new DataTable();
                DataRow dr;

                dt.Columns.Add(new DataColumn("FolderDownloadID", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("ManualFileUploadID", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));


                foreach (UP_DataImportErrorBE obj_DataImportErrorBE in up_SKUBEErrorList)
                {
                    dr = dt.NewRow();

                    dr["FolderDownloadID"] = obj_DataImportErrorBE.FolderDownloadID;
                    dr["ManualFileUploadID"] = obj_DataImportErrorBE.ManualFileUploadID;
                    dr["ErrorDescription"] = obj_DataImportErrorBE.ErrorDescription;
                    dr["UpdatedDate"] = obj_DataImportErrorBE.UpdatedDate;


                    //if (oUP_SKUBE.Valuated_Stock != null)
                    //    dr["Valuated_Stock"] = oUP_SKUBE.Valuated_Stock;
                    //else
                    //    dr["Valuated_Stock"] = DBNull.Value;


                    dt.Rows.Add(dr);
                }
                up_SKUBEErrorList = null;

                SqlConnection sqlConn = new SqlConnection(DBConnection.Connection);
                sqlConn.Open();

                SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);
                oSqlBulkCopy.DestinationTableName = "UP_ErrorSKU";

                oSqlBulkCopy.ColumnMappings.Add("FolderDownloadID", "FolderDownloadID");
                oSqlBulkCopy.ColumnMappings.Add("ManualFileUploadID", "ManualFileUploadID");
                oSqlBulkCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");
                oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");


                oSqlBulkCopy.BulkCopyTimeout = 300;
                oSqlBulkCopy.BatchSize = dt.Rows.Count;

                oSqlBulkCopy.WriteToServer(dt);
                oSqlBulkCopy.Close();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }


        }

    }
}

﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Upload;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;


namespace DataAccessLayer.ModuleDAL.Upload
{
    public class UP_PurchaseOrderDetailDAL
    {
        public List<Up_PurchaseOrderDetailBE> GetAllPODetailsDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@CountryID", oUp_PurchaseOrderDetailBE.CountryID);
                if (oUp_PurchaseOrderDetailBE.Site != null)
                {
                    param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site.SiteID);
                }

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.SKUID = dr["SKUID"] != DBNull.Value ? Convert.ToInt32(dr["SKUID"]) : (int?)null;
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevisedDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Vendor_Code = dr["Vendor_Code"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Vendor_No = dr["Vendor_No"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Line_No = dr["Line_No"] != DBNull.Value ? Convert.ToInt32(dr["Line_No"].ToString()) : (int?)null;
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteID = Convert.ToInt32(dr["SiteID"].ToString());
                    oNewUp_PurchaseOrderDetailBEList.Site.ToleranceDueDay = dr["APP_ToleranceDueDay"] != DBNull.Value ? Convert.ToInt32(dr["APP_ToleranceDueDay"]) : (int?)null;
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.ParentVendorID = dr["ParentVendorID"] != DBNull.Value ? Convert.ToInt32(dr["ParentVendorID"]) : (int?)null;
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewUp_PurchaseOrderDetailBEList.LastUploadedFlag = dr["LastUploadedFlag"] != DBNull.Value ? dr["LastUploadedFlag"].ToString() : "N";
                    // Sprint 1 - Point 14 - Begin - Adding [Expected Date]
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["Expected_Date"] != DBNull.Value ? Convert.ToDateTime(dr["Expected_Date"]) : (DateTime?)null;
                    // Sprint 1 - Point 14 - End - Adding [Expected Date]
                    //---------------Stage 7 Point-- 21-----------------
                    if (ds.Tables[0].Columns.Contains("Vendor_Name"))
                        oNewUp_PurchaseOrderDetailBEList.Vendor_Name = dr["Vendor_Name"] != DBNull.Value ? Convert.ToString(dr["Vendor_Name"]) : string.Empty;
                    //---------------Stage 7 Point-- 21-----------------
                    //---------------Stage 11 Point-- 1-----------------
                    if (ds.Tables[0].Columns.Contains("SiteName"))
                        oNewUp_PurchaseOrderDetailBEList.Site.SiteName = dr["SiteName"] != DBNull.Value ? Convert.ToString(dr["SiteName"]) : string.Empty;
                    //---------------Stage 7 Point-- 21-----------------


                    if (ds.Tables[0].Columns.Contains("PO"))
                        oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["PO"] != DBNull.Value ? Convert.ToString(dr["PO"]) : string.Empty;

                    if (ds.Tables[0].Columns.Contains("Vendor"))
                        oNewUp_PurchaseOrderDetailBEList.Vendor.VendorName = dr["Vendor"] != DBNull.Value ? Convert.ToString(dr["Vendor"]) : string.Empty;

                    if (ds.Tables[0].Columns.Contains("status"))
                        oNewUp_PurchaseOrderDetailBEList.Status = dr["status"] != DBNull.Value ? Convert.ToString(dr["status"]) : string.Empty;

                    if (ds.Tables[0].Columns.Contains("CreatedOn"))
                        oNewUp_PurchaseOrderDetailBEList.CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToString(dr["CreatedOn"]) : string.Empty;

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetOpenPODetailsDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[15];

                param[index++] = new SqlParameter("@SelectedVendorIDs", oUp_PurchaseOrderDetailBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oUp_PurchaseOrderDetailBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedInventoryMgrIDs", oUp_PurchaseOrderDetailBE.SelectedInventoryMgrIDs);
                param[index++] = new SqlParameter("@PurchaseOrder", oUp_PurchaseOrderDetailBE.PurchaseOrderIds);
                param[index++] = new SqlParameter("@Date", oUp_PurchaseOrderDetailBE.SelectedDate);
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@OnTime", oUp_PurchaseOrderDetailBE.OnTime);
                param[index++] = new SqlParameter("@VikingCode", oUp_PurchaseOrderDetailBE.Direct_code);
                param[index++] = new SqlParameter("@ODCode", oUp_PurchaseOrderDetailBE.OD_Code);
                param[index++] = new SqlParameter("@StockPlannerCategoryID", oUp_PurchaseOrderDetailBE.StockPlannerGroupingID);
                param[index++] = new SqlParameter("@SkugroupingID", oUp_PurchaseOrderDetailBE.SkugroupingID);
                param[index++] = new SqlParameter("@CountryIDs", oUp_PurchaseOrderDetailBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@IsFromSummary", oUp_PurchaseOrderDetailBE.IsFromSummary);
                param[index++] = new SqlParameter("@Daysfilter", oUp_PurchaseOrderDetailBE.Daysfilter);
                param[index++] = new SqlParameter("@SubVendorIds", oUp_PurchaseOrderDetailBE.SubVendorIDs);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPTBOK_OpenPOOverview", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Original_due_dateString = dr["Original_due_date"] != DBNull.Value ? dr["Original_due_date"].ToString() : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.CreatedBy = dr["CreatedBy"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.StockPlannerNo = dr["StockPlannerNo"] != DBNull.Value ? dr["StockPlannerNo"].ToString() : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.StockPlannerGroupingName = dr["StockPlannerGroupings"] != DBNull.Value ? dr["StockPlannerGroupings"].ToString() : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.OD_Code = dr["OD_Code"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Direct_code = dr["Direct_code"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Vendor_Code = dr["Vendor_code"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Product_description = dr["Product_description"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.UOM = dr["UOM"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Original_quantity = dr["Original_quantity"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Outstanding_Qty = dr["Outstanding_Qty"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Qty_On_Hand = dr["Qty_On_Hand"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Vendor_Name = dr["Vendor_Name"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Vendor_No = dr["Vendor_No"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Other = dr["Other"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.SiteName = dr["SiteName"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Expected_dateString = dr["Expected_Date"] != DBNull.Value ? dr["Expected_Date"].ToString() : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.ScheduleDateString = dr["ScheduleDate"] != DBNull.Value ? dr["ScheduleDate"].ToString() : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.Order_raisedString = dr["Order_raised"] != DBNull.Value ? dr["Order_raised"].ToString() : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.OnTime = dr["OnTime"] != DBNull.Value ? dr["OnTime"].ToString() : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.DeliveryStatus = string.Empty;
                    if (dr.Table.Columns.Contains("BookingStatus"))
                        oNewUp_PurchaseOrderDetailBEList.DeliveryStatus = dr["BookingStatus"] != DBNull.Value ? dr["BookingStatus"].ToString() : string.Empty;

                    if (dr.Table.Columns.Contains("Total_Qty_BO"))
                        oNewUp_PurchaseOrderDetailBEList.Total_Qty_BO = dr["Total_Qty_BO"] != DBNull.Value ? Convert.ToString(dr["Total_Qty_BO"]) : string.Empty;

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetRemainingBookingPODetails(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site.SiteID);
                param[index++] = new SqlParameter("@SelectedProductCodes", oUp_PurchaseOrderDetailBE.SelectedProductCodes);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.ToleranceDueDay = dr["APP_ToleranceDueDay"] != DBNull.Value ? Convert.ToInt32(dr["APP_ToleranceDueDay"]) : (int?)null;
                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetProductOrderDetailsDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@CountryID", oUp_PurchaseOrderDetailBE.CountryID);
                if (oUp_PurchaseOrderDetailBE.Vendor != null)
                    param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                if (oUp_PurchaseOrderDetailBE.Site != null)
                    param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorID = Convert.ToInt32(dr["VendorID"].ToString());
                    oNewUp_PurchaseOrderDetailBEList.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorName = dr["VendorName"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.Vendor_No = dr["Vendor_No"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.OutstandingLines = Convert.ToInt32(dr["OutstandingLines"].ToString());
                    oNewUp_PurchaseOrderDetailBEList.SKU = new UP_SKUBE();
                    oNewUp_PurchaseOrderDetailBEList.SKU.Qty_On_Hand = dr["Qty_On_Hand"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.SKU.qty_on_backorder = dr["qty_on_backorder"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteName = dr["SiteName"].ToString();
                    if (ds.Tables[0].Columns.Contains("SiteID"))
                    {
                        oNewUp_PurchaseOrderDetailBEList.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    }

                    oNewUp_PurchaseOrderDetailBEList.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"].ToString());

                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["Expected_Date"] != DBNull.Value ? Convert.ToDateTime(dr["Expected_Date"]) : (DateTime?)null;
                    // Sprint 1 - Point 14 - End - Adding [Expected Date]

                    if (dr.Table.Columns.Contains("POPriority"))
                    {
                        string[] strPOPriority = dr["POPriority"].ToString().Split('/');
                        if (strPOPriority.Length > 0)
                        {
                            oNewUp_PurchaseOrderDetailBEList.OutstandingLines = Convert.ToInt32(strPOPriority[0]);
                            oNewUp_PurchaseOrderDetailBEList.Stockouts = Convert.ToInt32(strPOPriority[1]);
                            oNewUp_PurchaseOrderDetailBEList.Backorders = Convert.ToInt32(strPOPriority[2]);
                        }
                    }


                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public int? addEditRevisedLeadTimeDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@RevisedDueDate", oUp_PurchaseOrderDetailBE.RevisedDueDate);
                param[index++] = new SqlParameter("@SelectedProductCodes", oUp_PurchaseOrderDetailBE.SelectedProductCodes);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@CurrentRevisedDueDate", oUp_PurchaseOrderDetailBE.CurrentRevisedDueDate);
                param[index++] = new SqlParameter("@RevisedDueDateUserName", oUp_PurchaseOrderDetailBE.RevisedDueDateUserName);
                param[index++] = new SqlParameter("@Reason", oUp_PurchaseOrderDetailBE.Reason);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<Up_PurchaseOrderDetailBE> GetODProductCodesDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@RevisedDueDate", oUp_PurchaseOrderDetailBE.RevisedDueDate);
                param[index++] = new SqlParameter("@Order_raised", oUp_PurchaseOrderDetailBE.Order_raised);
                param[index++] = new SqlParameter("@SelectedWarehouse", oUp_PurchaseOrderDetailBE.Warehouse);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.SKUID = Convert.ToInt32(dr["SKUID"]);
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevisedDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.ProductDescription = dr["ProductDescription"].ToString();
                    if (dr.Table.Columns.Contains("Reason"))
                        oNewUp_PurchaseOrderDetailBEList.Reason = dr["Reason"] != DBNull.Value ? Convert.ToString(dr["Reason"]) : string.Empty;
                    if (dr.Table.Columns.Contains("SiteID"))
                        oNewUp_PurchaseOrderDetailBEList.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    if (dr.Table.Columns.Contains("SKUExclusionID"))
                        oNewUp_PurchaseOrderDetailBEList.SKUExclusionID = dr["SKUExclusionID"] != DBNull.Value ? Convert.ToInt32(dr["SKUExclusionID"]) : 0;
                    if (dr.Table.Columns.Contains("ApplyExclusionID"))
                        oNewUp_PurchaseOrderDetailBEList.ApplyExclusionID = dr["ApplyExclusionID"] != DBNull.Value ? Convert.ToInt32(dr["ApplyExclusionID"]) : 0;

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetSKUCodesDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);

                param[index++] = new SqlParameter("@Order_raised", oUp_PurchaseOrderDetailBE.Order_raised);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.SKUID = Convert.ToInt32(dr["SKUID"]);
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["PurchaseNumber"].ToString();

                    oNewUp_PurchaseOrderDetailBEList.ProductDescription = dr["ProductDescription"].ToString();

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetRevisedLeadListDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@CountryID", oUp_PurchaseOrderDetailBE.CountryID);
                param[index++] = new SqlParameter("@GridCurrentPageNo", oUp_PurchaseOrderDetailBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", oUp_PurchaseOrderDetailBE.GridPageSize);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@DateFrom", oUp_PurchaseOrderDetailBE.DateFrom);
                param[index++] = new SqlParameter("@DateTo", oUp_PurchaseOrderDetailBE.DateTo);
                if (oUp_PurchaseOrderDetailBE.SKU != null)
                {
                    param[index++] = new SqlParameter("@SKU", oUp_PurchaseOrderDetailBE.SKU.Direct_SKU);
                }
                if (oUp_PurchaseOrderDetailBE.VikingSku != null)
                {
                    param[index++] = new SqlParameter("@Viking", oUp_PurchaseOrderDetailBE.VikingSku);
                }

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevisedDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.ProductDescription = dr["ProductDescription"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.CountryName = dr["CountryName"].ToString();
                    if (dr.Table.Columns.Contains("TotalRecords"))
                        oNewUp_PurchaseOrderDetailBEList.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;
                    if (dr.Table.Columns.Contains("CountryID"))
                        oNewUp_PurchaseOrderDetailBEList.CountryID = Convert.ToInt32(dr["CountryID"]);
                    if (dr.Table.Columns.Contains("Warehouse"))
                        oNewUp_PurchaseOrderDetailBEList.Warehouse = dr["Warehouse"].ToString().Trim();
                    if (dr.Table.Columns.Contains("OriginalDueDate"))
                        oNewUp_PurchaseOrderDetailBEList.OriginalDueDate = dr["OriginalDueDate"].ToString();
                    if (dr.Table.Columns.Contains("SiteID"))
                        oNewUp_PurchaseOrderDetailBEList.SiteId = Convert.ToInt32(dr["SiteID"].ToString());

                    oNewUp_PurchaseOrderDetailBEList.Order_raised = dr["OrderRaised"] != DBNull.Value ? Convert.ToDateTime(dr["OrderRaised"]) : (DateTime?)null;

                    if (dr.Table.Columns.Contains("Reason"))
                        oNewUp_PurchaseOrderDetailBEList.Reason = dr["Reason"] != DBNull.Value ? Convert.ToString(dr["Reason"]) : string.Empty;

                    if (dr.Table.Columns.Contains("AppliedBy"))
                        oNewUp_PurchaseOrderDetailBEList.AppliedBy = dr["AppliedBy"] != DBNull.Value ? Convert.ToString(dr["AppliedBy"]) : string.Empty;

                    if (dr.Table.Columns.Contains("RevisedDueDateAppliedOn"))
                        oNewUp_PurchaseOrderDetailBEList.RevisedDueDateAppliedOn = dr["RevisedDueDateAppliedOn"] != DBNull.Value ? Convert.ToString(dr["RevisedDueDateAppliedOn"]) : string.Empty;

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetRevisedSKULeadListDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevisedDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.ProductDescription = dr["ProductDescription"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.CountryName = dr["CountryName"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteName = dr["SiteName"].ToString();

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetAllBookedProductOrderDetailsDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@BookingID", oUp_PurchaseOrderDetailBE.BookingID);
                param[index++] = new SqlParameter("@UserID", oUp_PurchaseOrderDetailBE.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorID = Convert.ToInt32(dr["VendorID"].ToString());
                    oNewUp_PurchaseOrderDetailBEList.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorName = dr["VendorName"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.OutstandingLines = Convert.ToInt32(dr["OutstandingLines"].ToString());
                    oNewUp_PurchaseOrderDetailBEList.SKU = new UP_SKUBE();
                    oNewUp_PurchaseOrderDetailBEList.SKU.Qty_On_Hand = dr["Qty_On_Hand"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.SKU.qty_on_backorder = dr["qty_on_backorder"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.IsProvisionResonChangesUpdate = dr["IsProvisionResonChangesUpdate"] != DBNull.Value ? Convert.ToBoolean(dr["IsProvisionResonChangesUpdate"]) : (bool?)null; ;
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteName = dr["SiteName"].ToString();
                    if (ds.Tables[0].Columns.Contains("SiteID"))
                    {
                        oNewUp_PurchaseOrderDetailBEList.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    }
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"].ToString());
                    // Sprint 1 Changes Begin
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["Expected_date"] != DBNull.Value ? Convert.ToDateTime(dr["Expected_date"]) : (DateTime?)null;
                    // Sprint 1 Changes End

                    if (ds.Tables[0].Columns.Contains("IsPOManuallyAdded"))
                    {
                        oNewUp_PurchaseOrderDetailBEList.IsPOManuallyAdded = dr["IsPOManuallyAdded"] != DBNull.Value ? Convert.ToBoolean(dr["IsPOManuallyAdded"]) : false;
                    }

                    if (dr.Table.Columns.Contains("POPriority"))
                    {
                        string[] strPOPriority = dr["POPriority"].ToString().Split('/');
                        if (strPOPriority.Length > 0)
                        {
                            oNewUp_PurchaseOrderDetailBEList.OutstandingLines = Convert.ToInt32(strPOPriority[0]);
                            oNewUp_PurchaseOrderDetailBEList.Stockouts = Convert.ToInt32(strPOPriority[1]);
                            oNewUp_PurchaseOrderDetailBEList.Backorders = Convert.ToInt32(strPOPriority[2]);
                        }
                    }

                    if (dr.Table.Columns.Contains("StockPlanner"))
                        oNewUp_PurchaseOrderDetailBEList.StockPlannerName = dr["StockPlanner"] != DBNull.Value ? dr["StockPlanner"].ToString() : string.Empty;
                    else
                        oNewUp_PurchaseOrderDetailBEList.StockPlannerName = string.Empty;


                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetAllPOList(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"].ToString());
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetPriorityDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@BookingID", oUp_PurchaseOrderDetailBE.BookingID);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site.SiteID);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                if (oUp_PurchaseOrderDetailBE.Vendor != null)
                    param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                if (oUp_PurchaseOrderDetailBE.IsPOManuallyAdded != null)
                    param[index++] = new SqlParameter("@IsPOManuallyAdded", oUp_PurchaseOrderDetailBE.IsPOManuallyAdded);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.SKU = new UP_SKUBE();

                    oNewUp_PurchaseOrderDetailBEList.SKU.Qty_On_Hand = dr["Qty_On_Hand"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.SKU.qty_on_backorder = dr["qty_on_backorder"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Original_quantity = dr["Original_quantity"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Qty_receipted = dr["Qty_receipted"] != DBNull.Value ? Convert.ToInt32(dr["Qty_receipted"]) : (int?)null;
                    oNewUp_PurchaseOrderDetailBEList.LineCount = dr["LineCount"] != DBNull.Value ? Convert.ToInt32(dr["LineCount"]) : (int?)null;
                    // Sprint 1 Changes Begin
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["Expected_date"] != DBNull.Value ? Convert.ToDateTime(dr["Expected_date"]) : (DateTime?)null;
                    // Sprint 1 Changes End
                    // Stage 8 - Point 6 Changes Begin
                    if (dr.Table.Columns.Contains("StockPlanner"))
                        oNewUp_PurchaseOrderDetailBEList.StockPlannerName = dr["StockPlanner"] != DBNull.Value ? dr["StockPlanner"].ToString() : string.Empty;
                    else
                        oNewUp_PurchaseOrderDetailBEList.StockPlannerName = string.Empty;

                    // Stage 8 - Point 6 Changes End

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public void AddPurchaseOrderDetails(List<Up_PurchaseOrderDetailBE> pUp_PurchaseOrderDetailBEList, bool IsSnooty = false)
        {
            try
            {

                DataTable dt = new DataTable();
                DataRow dr;

                dt.Columns.Add(new DataColumn("Purchase_order"));
                dt.Columns.Add(new DataColumn("Line_No", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("Warehouse"));
                dt.Columns.Add(new DataColumn("Vendor_No"));
                dt.Columns.Add(new DataColumn("subvndr"));
                dt.Columns.Add(new DataColumn("Direct_code"));
                dt.Columns.Add(new DataColumn("OD_Code"));
                dt.Columns.Add(new DataColumn("Vendor_Code"));
                dt.Columns.Add(new DataColumn("Product_description"));
                dt.Columns.Add(new DataColumn("UOM"));
                dt.Columns.Add(new DataColumn("Original_quantity", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("Outstanding_Qty", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("PO_cost", typeof(System.Decimal)));
                dt.Columns.Add(new DataColumn("Order_raised", typeof(System.DateTime)));
                dt.Columns.Add(new DataColumn("Original_due_date", typeof(System.DateTime)));
                dt.Columns.Add(new DataColumn("Expected_date", typeof(System.DateTime)));
                dt.Columns.Add(new DataColumn("Buyer_no"));
                dt.Columns.Add(new DataColumn("Item_classification"));
                dt.Columns.Add(new DataColumn("Item_type"));
                dt.Columns.Add(new DataColumn("Item_Category"));
                dt.Columns.Add(new DataColumn("Other"));
                dt.Columns.Add(new DataColumn("Country"));
                dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));
                dt.Columns.Add(new DataColumn("Snooty"));

                foreach (Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE in pUp_PurchaseOrderDetailBEList)
                {
                    dr = dt.NewRow();
                    dr["Purchase_order"] = oUp_PurchaseOrderDetailBE.Purchase_order;
                    dr["Line_No"] = oUp_PurchaseOrderDetailBE.Line_No != null ? oUp_PurchaseOrderDetailBE.Line_No : (object)DBNull.Value;
                    dr["Warehouse"] = oUp_PurchaseOrderDetailBE.Warehouse;
                    dr["Vendor_No"] = oUp_PurchaseOrderDetailBE.Vendor_No;
                    dr["subvndr"] = oUp_PurchaseOrderDetailBE.SubVendor;
                    dr["Direct_code"] = oUp_PurchaseOrderDetailBE.Direct_code;
                    dr["OD_Code"] = oUp_PurchaseOrderDetailBE.OD_Code;
                    dr["Vendor_Code"] = oUp_PurchaseOrderDetailBE.Vendor_Code;
                    dr["Product_description"] = oUp_PurchaseOrderDetailBE.Product_description;
                    dr["UOM"] = oUp_PurchaseOrderDetailBE.UOM;
                    dr["Original_quantity"] = !string.IsNullOrWhiteSpace(oUp_PurchaseOrderDetailBE.Original_quantity) ? oUp_PurchaseOrderDetailBE.Original_quantity : (object)DBNull.Value;
                    dr["Outstanding_Qty"] = !string.IsNullOrWhiteSpace(oUp_PurchaseOrderDetailBE.Outstanding_Qty) ? oUp_PurchaseOrderDetailBE.Outstanding_Qty : (object)DBNull.Value;
                    dr["PO_cost"] = oUp_PurchaseOrderDetailBE.PO_cost != null ? oUp_PurchaseOrderDetailBE.PO_cost : (object)DBNull.Value;
                    dr["Order_raised"] = oUp_PurchaseOrderDetailBE.Order_raised != null ? oUp_PurchaseOrderDetailBE.Order_raised : (object)DBNull.Value;
                    dr["Original_due_date"] = oUp_PurchaseOrderDetailBE.Original_due_date != null ? oUp_PurchaseOrderDetailBE.Original_due_date : (object)DBNull.Value;
                    dr["Expected_date"] = oUp_PurchaseOrderDetailBE.Expected_date != null ? oUp_PurchaseOrderDetailBE.Expected_date : (object)DBNull.Value;
                    dr["Buyer_no"] = oUp_PurchaseOrderDetailBE.Buyer_no;
                    dr["Item_classification"] = oUp_PurchaseOrderDetailBE.Item_classification;
                    dr["Item_type"] = oUp_PurchaseOrderDetailBE.Item_type;
                    dr["Item_Category"] = oUp_PurchaseOrderDetailBE.Item_Category;
                    dr["Other"] = oUp_PurchaseOrderDetailBE.Other;
                    dr["Country"] = oUp_PurchaseOrderDetailBE.CountryName;
                    dr["UpdatedDate"] = oUp_PurchaseOrderDetailBE.UpdatedDate;
                    dr["Snooty"] = oUp_PurchaseOrderDetailBE.Snooty;
                    dt.Rows.Add(dr);
                }

                pUp_PurchaseOrderDetailBEList = null;

                SqlConnection sqlConn = new SqlConnection(DBConnection.Connection);
                sqlConn.Open();
                SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);
                oSqlBulkCopy.DestinationTableName = "UP_StagePurchaseOrderDetails";
                oSqlBulkCopy.ColumnMappings.Add("Purchase_order", "Purchase_order");
                oSqlBulkCopy.ColumnMappings.Add("Line_No", "Line_No");
                oSqlBulkCopy.ColumnMappings.Add("Warehouse", "Warehouse");
                oSqlBulkCopy.ColumnMappings.Add("Vendor_No", "Vendor_No");
                oSqlBulkCopy.ColumnMappings.Add("subvndr", "subvndr");
                oSqlBulkCopy.ColumnMappings.Add("Direct_code", "Direct_code");
                oSqlBulkCopy.ColumnMappings.Add("OD_Code", "OD_Code");
                oSqlBulkCopy.ColumnMappings.Add("Vendor_Code", "Vendor_Code");
                oSqlBulkCopy.ColumnMappings.Add("Product_description", "Product_description");
                oSqlBulkCopy.ColumnMappings.Add("UOM", "UOM");
                oSqlBulkCopy.ColumnMappings.Add("Original_quantity", "Original_quantity");
                oSqlBulkCopy.ColumnMappings.Add("Outstanding_Qty", "Outstanding_Qty");
                oSqlBulkCopy.ColumnMappings.Add("PO_cost", "PO_cost");
                oSqlBulkCopy.ColumnMappings.Add("Order_raised", "Order_raised");
                oSqlBulkCopy.ColumnMappings.Add("Original_due_date", "Original_due_date");
                oSqlBulkCopy.ColumnMappings.Add("Expected_date", "Expected_date");
                oSqlBulkCopy.ColumnMappings.Add("Buyer_no", "Buyer_no");
                oSqlBulkCopy.ColumnMappings.Add("Item_classification", "Item_classification");
                oSqlBulkCopy.ColumnMappings.Add("Item_type", "Item_type");
                oSqlBulkCopy.ColumnMappings.Add("Item_Category", "Item_Category");
                oSqlBulkCopy.ColumnMappings.Add("Other", "Other");
                oSqlBulkCopy.ColumnMappings.Add("Country", "Country");
                oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                oSqlBulkCopy.ColumnMappings.Add("Snooty", "Snooty");
                oSqlBulkCopy.BulkCopyTimeout = 300;
                oSqlBulkCopy.BatchSize = dt.Rows.Count;
                oSqlBulkCopy.WriteToServer(dt);
                oSqlBulkCopy.Close();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
        }

        public DataTable GetPoForSchedularDAL()
        {
            DataSet dsValidation = null;
            try
            {
                SqlParameter[] sqlParam = new SqlParameter[1];
                sqlParam[0] = new SqlParameter("@Action", "GetPOForScheduler");

                dsValidation = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", sqlParam);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dsValidation.Tables[0];
        }

        // Sprint 1 - Point 7 - Start
        public List<Up_PurchaseOrderDetailBE> GetAllPODetailsOfSiteAndVendor(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.PurchaseOrderID = dr["PurchaseOrderID"] != DBNull.Value ? Convert.ToInt32(dr["PurchaseOrderID"]) : 0;
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["Expected_date"] != DBNull.Value ? Convert.ToDateTime(dr["Expected_date"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.ToleranceDueDay = dr["APP_ToleranceDueDay"] != DBNull.Value ? Convert.ToInt32(dr["APP_ToleranceDueDay"]) : (int?)null;
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteName = dr["SiteName"] != null ? Convert.ToString(dr["SiteName"]) : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorName = dr["Vendor_Name"] != null ? Convert.ToString(dr["Vendor_Name"]) : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"]) : 0;
                    oNewUp_PurchaseOrderDetailBEList.BookingDeliveryDate = dr["ScheduleDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduleDate"]) : (DateTime?)null;
                    if (dr.Table.Columns.Contains("POPriority"))
                    {
                        string[] strPOPriority = dr["POPriority"].ToString().Split('/');
                        if (strPOPriority.Length > 0)
                        {
                            oNewUp_PurchaseOrderDetailBEList.OutstandingLines = Convert.ToInt32(strPOPriority[0]);
                            oNewUp_PurchaseOrderDetailBEList.Stockouts = Convert.ToInt32(strPOPriority[1]);
                            oNewUp_PurchaseOrderDetailBEList.Backorders = Convert.ToInt32(strPOPriority[2]);
                        }
                    }
                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }
        // Sprint 1 - Point 7 - End

        public List<UP_VendorBE> GetConsolidateVendorsDAL(UP_VendorBE oUP_VendorBE)
        {
            List<UP_VendorBE> lstVendors = new List<UP_VendorBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUP_VendorBE.Action);
                param[index++] = new SqlParameter("@VendorID", oUP_VendorBE.VendorID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    UP_VendorBE oUP_VendorBENew = new UP_VendorBE();
                    oUP_VendorBENew.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;

                    lstVendors.Add(oUP_VendorBENew);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstVendors;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="up_PurchaseOrderBEErrorList"></param>
        public void AddPurchaseOrderDetailErrors(List<UP_DataImportErrorBE> up_PurchaseOrderBEErrorList)
        {
            try
            {
                DataTable dt = new DataTable();
                DataRow dr;

                dt.Columns.Add(new DataColumn("FolderDownloadID", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("ManualFileUploadID", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));


                foreach (UP_DataImportErrorBE obj_DataImportErrorBE in up_PurchaseOrderBEErrorList)
                {
                    dr = dt.NewRow();

                    dr["FolderDownloadID"] = obj_DataImportErrorBE.FolderDownloadID;
                    dr["ManualFileUploadID"] = obj_DataImportErrorBE.ManualFileUploadID;
                    dr["ErrorDescription"] = obj_DataImportErrorBE.ErrorDescription;
                    dr["UpdatedDate"] = obj_DataImportErrorBE.UpdatedDate;

                    dt.Rows.Add(dr);
                }
                up_PurchaseOrderBEErrorList = null;

                SqlConnection sqlConn = new SqlConnection(DBConnection.Connection);
                sqlConn.Open();

                SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(sqlConn, SqlBulkCopyOptions.TableLock, null);
                oSqlBulkCopy.DestinationTableName = "UP_ErrorPurchaseOrderDetails";

                oSqlBulkCopy.ColumnMappings.Add("FolderDownloadID", "FolderDownloadID");
                oSqlBulkCopy.ColumnMappings.Add("ManualFileUploadID", "ManualFileUploadID");
                oSqlBulkCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");
                oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");


                oSqlBulkCopy.BulkCopyTimeout = 300;
                oSqlBulkCopy.BatchSize = dt.Rows.Count;

                oSqlBulkCopy.WriteToServer(dt);
                oSqlBulkCopy.Close();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }


        }

        public List<Up_PurchaseOrderDetailBE> GetPurchaseOrderInquiryDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@SKU", oUp_PurchaseOrderDetailBE.SKU.Direct_SKU);
                param[index++] = new SqlParameter("@VendorId", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SiteId", oUp_PurchaseOrderDetailBE.Site.SiteID);
                param[index++] = new SqlParameter("@Status", oUp_PurchaseOrderDetailBE.Status);

                param[index++] = new SqlParameter("@ReceiptStatus", oUp_PurchaseOrderDetailBE.ReceiptStatus);
                param[index++] = new SqlParameter("@Viking", oUp_PurchaseOrderDetailBE.VikingSku);
                param[index++] = new SqlParameter("@OrderRaisedDateTo", oUp_PurchaseOrderDetailBE.OrderRaisedToDate);
                param[index++] = new SqlParameter("@OrderRaisedDateFrom", oUp_PurchaseOrderDetailBE.OrderRaisedFromDate);
                param[index++] = new SqlParameter("@Page", oUp_PurchaseOrderDetailBE.Page);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = Convert.ToString(dr["PO"]);
                    oNewUp_PurchaseOrderDetailBEList.Vendor_No = Convert.ToString(dr["Vendor"]);
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                    oNewUp_PurchaseOrderDetailBEList.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"].ToString()) : 0;
                    oNewUp_PurchaseOrderDetailBEList.Status = Convert.ToString(dr["Status"]);
                    oNewUp_PurchaseOrderDetailBEList.Line_No = dr["OpenLines"] != DBNull.Value ? Convert.ToInt32(dr["OpenLines"].ToString()) : 0;
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["ExpectedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["ExpectedDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Order_raised = dr["OrderRaisedDate"] != DBNull.Value ? Convert.ToDateTime(dr["OrderRaisedDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteName = Convert.ToString(dr["SiteName"]);
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oNewUp_PurchaseOrderDetailBEList.Snooty = dr["Snooty"] != DBNull.Value ? Convert.ToString(dr["Snooty"].ToString()) : string.Empty;
                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetPurchaseOrderInquiryMainDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@VendorId", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SiteId", oUp_PurchaseOrderDetailBE.Site.SiteID);
                param[index++] = new SqlParameter("@Status", oUp_PurchaseOrderDetailBE.Status);
                param[index++] = new SqlParameter("@Order_raised", oUp_PurchaseOrderDetailBE.Order_raised);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = Convert.ToString(dr["PO"]);
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorName = Convert.ToString(dr["Vendor"]);
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteName = Convert.ToString(dr["SiteName"]);
                    oNewUp_PurchaseOrderDetailBEList.Status = Convert.ToString(dr["Status"]);
                    oNewUp_PurchaseOrderDetailBEList.Original_due_date = dr["OriginalDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["OriginalDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Buyer_no = Convert.ToString(dr["Buyer_no"]);
                    oNewUp_PurchaseOrderDetailBEList.StockPlannerName = Convert.ToString(dr["StockPlannerName"]);
                    oNewUp_PurchaseOrderDetailBEList.CountryID = Convert.ToInt32(dr["CountryID"]);
                    oNewUp_PurchaseOrderDetailBEList.CountryName = Convert.ToString(dr["CountryName"]);
                    oNewUp_PurchaseOrderDetailBEList.Order_raised = dr["OrderCreated"] != DBNull.Value ? Convert.ToDateTime(dr["OrderCreated"]) : (DateTime?)null;
                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetPurchaseOrderInquiryDetailsDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@VendorId", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SiteId", oUp_PurchaseOrderDetailBE.Site.SiteID);
                param[index++] = new SqlParameter("@Status", oUp_PurchaseOrderDetailBE.Status);
                param[index++] = new SqlParameter("@Order_raised", oUp_PurchaseOrderDetailBE.Order_raised);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Line_No = dr["LineNo"] != DBNull.Value ? Convert.ToInt32(dr["LineNo"].ToString()) : 0;
                    oNewUp_PurchaseOrderDetailBEList.OD_Code = Convert.ToString(dr["ODSku"]);
                    oNewUp_PurchaseOrderDetailBEList.Direct_code = Convert.ToString(dr["VikingSKU"]);
                    oNewUp_PurchaseOrderDetailBEList.Vendor_Code = Convert.ToString(dr["VendorCode"]);
                    oNewUp_PurchaseOrderDetailBEList.Product_description = Convert.ToString(dr["Description"]);
                    oNewUp_PurchaseOrderDetailBEList.Original_quantity = Convert.ToString(dr["OriginalQty"]);
                    oNewUp_PurchaseOrderDetailBEList.Outstanding_Qty = Convert.ToString(dr["OutstandingQty"]);
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["ExpectedDate"] != DBNull.Value ? Convert.ToDateTime(dr["ExpectedDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.LastReceiptDate = dr["LastReceipt"] != DBNull.Value ? Convert.ToDateTime(dr["LastReceipt"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Comments = Convert.ToString(dr["Comments"]);
                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<APPBOK_BookingBE> GetPurchaseOrderInquiryBookingDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = new List<APPBOK_BookingBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oAPPBOK_BookingBE.PurchaseOrders);
                param[index++] = new SqlParameter("@VendorId", oAPPBOK_BookingBE.VendorID);
                param[index++] = new SqlParameter("@SiteId", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@Order_raised", oAPPBOK_BookingBE.PurchaseOrder.Order_raised);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE appbok_BookingBE = new APPBOK_BookingBE();
                    appbok_BookingBE.ScheduleDate = dr["Date"] != DBNull.Value ? Convert.ToDateTime(dr["Date"]) : (DateTime?)null;
                    appbok_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    appbok_BookingBE.SlotTime.SlotTime = Convert.ToString(dr["Time"]);
                    appbok_BookingBE.BookingStatus = Convert.ToString(dr["Status"]);
                    appbok_BookingBE.BookingRef = Convert.ToString(dr["BookingRef"]);
                    appbok_BookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"].ToString()) : 0;
                    appbok_BookingBE.NumberOfPallet = dr["Pallets"] != DBNull.Value ? Convert.ToInt32(dr["Pallets"].ToString()) : 0;
                    appbok_BookingBE.NumberOfCartons = dr["Cartons"] != DBNull.Value ? Convert.ToInt32(dr["Cartons"].ToString()) : 0;
                    appbok_BookingBE.NumberOfLines = dr["Lines"] != DBNull.Value ? Convert.ToInt32(dr["Lines"].ToString()) : 0;
                    appbok_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    appbok_BookingBE.Carrier.CarrierName = Convert.ToString(dr["Carrier"]);
                    appbok_BookingBE.SupplierType = Convert.ToString(dr["SupplierType"]);
                    appbok_BookingBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    if (dr.Table.Columns.Contains("WeekDay"))
                        appbok_BookingBE.WeekDay = dr["WeekDay"] != DBNull.Value ? Convert.ToInt32(dr["WeekDay"].ToString()) : 0;

                    lstAPPBOK_BookingBE.Add(appbok_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstAPPBOK_BookingBE;
        }

        public List<DiscrepancyBE> GetPurchaseOrderInquiryDiscrepanciesDAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> lstDiscrepancyBE = new List<DiscrepancyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oDiscrepancyBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oDiscrepancyBE.PurchaseOrder.Purchase_order);
                param[index++] = new SqlParameter("@VendorId", oDiscrepancyBE.VendorID);
                param[index++] = new SqlParameter("@SiteId", oDiscrepancyBE.POSiteID);
                param[index++] = new SqlParameter("@Order_raised", oDiscrepancyBE.PurchaseOrder.Order_raised);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DiscrepancyBE discrepancyBE = new DiscrepancyBE();
                    discrepancyBE.DiscrepancyStatus = Convert.ToString(dr["Status"]);
                    discrepancyBE.VDRNo = Convert.ToString(dr["VDRNo"]);
                    discrepancyBE.DiscrepancyLogID = dr["DiscrepancyID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyID"].ToString()) : 0;
                    discrepancyBE.DiscrepancyType = Convert.ToString(dr["VDRDescription"]);
                    discrepancyBE.Username = Convert.ToString(dr["CreatedBy"]);
                    discrepancyBE.DiscrepancyLogDate = dr["VDRDate"] != DBNull.Value ? Convert.ToDateTime(dr["VDRDate"]) : (DateTime?)null;
                    discrepancyBE.UserID = dr["UserID"] != DBNull.Value ? Convert.ToInt32(dr["UserID"].ToString()) : 0;
                    discrepancyBE.DiscrepancyTypeID = dr["DiscrepancyTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepancyTypeID"].ToString()) : 0;
                    lstDiscrepancyBE.Add(discrepancyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstDiscrepancyBE;
        }

        public int? GetPOInquiryReceiptVisibilityCountDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@Order_raised", oUp_PurchaseOrderDetailBE.Order_raised);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site.SiteID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<Up_PurchaseOrderDetailBE> GetPOInquiryReceiptDataDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetail = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@Order_raised", oUp_PurchaseOrderDetailBE.Order_raised);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site.SiteID);
                var dataTable = new DataTable();
                dataTable = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param).Tables[0];

                foreach (DataRow dr in dataTable.Rows)
                {
                    var poDetailBE = new Up_PurchaseOrderDetailBE();
                    poDetailBE.ReceiptDate = dr["ReceiptDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReceiptDate"]) : (DateTime?)null;
                    poDetailBE.Line_No = dr["LineNo"] != DBNull.Value ? Convert.ToInt32(dr["LineNo"].ToString()) : (int?)null;
                    poDetailBE.ODSKU_No = dr["SKU"] != DBNull.Value ? Convert.ToString(dr["SKU"]) : string.Empty;
                    poDetailBE.Vendor_Code = dr["VendorCode"] != DBNull.Value ? Convert.ToString(dr["VendorCode"]) : string.Empty;
                    poDetailBE.ProductDescription = dr["Description"] != DBNull.Value ? Convert.ToString(dr["Description"]) : string.Empty;
                    poDetailBE.Qty_receipted = dr["QtyReceipted"] != DBNull.Value ? Convert.ToInt32(dr["QtyReceipted"].ToString()) : (int?)null;
                    lstPurchaseOrderDetail.Add(poDetailBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstPurchaseOrderDetail;
        }

        public List<Up_PurchaseOrderDetailBE> GetPurchaseOrderVendorDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@VendorId", oUp_PurchaseOrderDetailBE.Vendor.VendorID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.VendorPOC = new MAS_MaintainVendorPointsContactBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.Vendor_No = Convert.ToString(dr["Vendor_No"]);
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorName = Convert.ToString(dr["VendorName"]);
                    oNewUp_PurchaseOrderDetailBEList.VendorPOC.FullName = Convert.ToString(dr["POCContact"]);
                    oNewUp_PurchaseOrderDetailBEList.VendorPOC.PhoneNumber = Convert.ToString(dr["POCPhoneNumber"]);
                    oNewUp_PurchaseOrderDetailBEList.VendorPOC.EmailAddress = Convert.ToString(dr["POCEmail"]);

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<APPBOK_BookingBE> GetExpeditePOEnquiryBookingDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = new List<APPBOK_BookingBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SKUID", oAPPBOK_BookingBE.SkuId);
                param[index++] = new SqlParameter("@VendorId", oAPPBOK_BookingBE.VendorID);
                param[index++] = new SqlParameter("@SiteId", oAPPBOK_BookingBE.SiteId);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE appbok_BookingBE = new APPBOK_BookingBE();
                    appbok_BookingBE.ScheduleDate = dr["Date"] != DBNull.Value ? Convert.ToDateTime(dr["Date"]) : (DateTime?)null;
                    appbok_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    appbok_BookingBE.SlotTime.SlotTime = Convert.ToString(dr["Time"]);
                    appbok_BookingBE.BookingStatus = Convert.ToString(dr["Status"]);
                    appbok_BookingBE.BookingRef = Convert.ToString(dr["BookingRef"]);
                    appbok_BookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"].ToString()) : 0;
                    appbok_BookingBE.NumberOfPallet = dr["Pallets"] != DBNull.Value ? Convert.ToInt32(dr["Pallets"].ToString()) : 0;
                    appbok_BookingBE.NumberOfCartons = dr["Cartons"] != DBNull.Value ? Convert.ToInt32(dr["Cartons"].ToString()) : 0;
                    appbok_BookingBE.NumberOfLines = dr["Lines"] != DBNull.Value ? Convert.ToInt32(dr["Lines"].ToString()) : 0;
                    appbok_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    appbok_BookingBE.Carrier.CarrierName = Convert.ToString(dr["Carrier"]);
                    appbok_BookingBE.SupplierType = Convert.ToString(dr["SupplierType"]);
                    appbok_BookingBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    lstAPPBOK_BookingBE.Add(appbok_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstAPPBOK_BookingBE;
        }

        public List<Up_PurchaseOrderDetailBE> GetExpediteOpenPOListingDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@SKU", oUp_PurchaseOrderDetailBE.SKU.Direct_SKU);
                param[index++] = new SqlParameter("@OdCode", oUp_PurchaseOrderDetailBE.OD_Code);
                param[index++] = new SqlParameter("@SiteId", oUp_PurchaseOrderDetailBE.Site.SiteID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = Convert.ToString(dr["PO"]);
                    oNewUp_PurchaseOrderDetailBEList.Vendor_No = Convert.ToString(dr["Vendor"]);
                    oNewUp_PurchaseOrderDetailBEList.Vendor = new UP_VendorBE();
                    oNewUp_PurchaseOrderDetailBEList.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                    oNewUp_PurchaseOrderDetailBEList.Status = Convert.ToString(dr["Status"]);
                    oNewUp_PurchaseOrderDetailBEList.Line_No = dr["OpenLines"] != DBNull.Value ? Convert.ToInt32(dr["OpenLines"].ToString()) : 0;
                    oNewUp_PurchaseOrderDetailBEList.Expected_date = dr["ExpectedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["ExpectedDueDate"]) : (DateTime?)null;
                    oNewUp_PurchaseOrderDetailBEList.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteName = Convert.ToString(dr["SiteName"]);
                    oNewUp_PurchaseOrderDetailBEList.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oNewUp_PurchaseOrderDetailBEList.Order_raised = dr["OrderRaised"] != DBNull.Value ? Convert.ToDateTime(dr["OrderRaised"]) : (DateTime?)null;
                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetDelPerPOSummaryReportDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", oUp_PurchaseOrderDetailBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oUp_PurchaseOrderDetailBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oUp_PurchaseOrderDetailBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@FromDate", oUp_PurchaseOrderDetailBE.FromDate);
                param[index++] = new SqlParameter("@ToDate", oUp_PurchaseOrderDetailBE.ToDate);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    var purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                    purchaseOrderDetailBE.Site = new MAS_SiteBE();
                    purchaseOrderDetailBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    purchaseOrderDetailBE.Site.SiteName = dr["Site"] != DBNull.Value ? Convert.ToString(dr["Site"]) : string.Empty;
                    purchaseOrderDetailBE.Vendor = new UP_VendorBE();
                    purchaseOrderDetailBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                    purchaseOrderDetailBE.Vendor.VendorName = dr["Vendor"] != DBNull.Value ? Convert.ToString(dr["Vendor"]) : string.Empty;
                    purchaseOrderDetailBE.PODue = dr["PODue"] != DBNull.Value ? Convert.ToInt32(dr["PODue"]) : (int?)null;
                    purchaseOrderDetailBE.PODelivered = dr["PODelivered"] != DBNull.Value ? Convert.ToInt32(dr["PODelivered"]) : (int?)null;
                    purchaseOrderDetailBE.Receipts = dr["Receipts"] != DBNull.Value ? Convert.ToInt32(dr["Receipts"]) : (int?)null;
                    purchaseOrderDetailBE.ReceiptsPerDeliveredPO = dr["ReceiptsPerDeliveredPO"] != DBNull.Value ? float.Parse(Convert.ToString(dr["ReceiptsPerDeliveredPO"])) : (float?)null;
                    lstPurchaseOrderDetailBE.Add(purchaseOrderDetailBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstPurchaseOrderDetailBE;
        }

        public List<Up_PurchaseOrderDetailBE> GetDelPerPODetailByReceiptReportDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", oUp_PurchaseOrderDetailBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oUp_PurchaseOrderDetailBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oUp_PurchaseOrderDetailBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@FromDate", oUp_PurchaseOrderDetailBE.FromDate);
                param[index++] = new SqlParameter("@ToDate", oUp_PurchaseOrderDetailBE.ToDate);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    var purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                    purchaseOrderDetailBE.Site = new MAS_SiteBE();
                    purchaseOrderDetailBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    purchaseOrderDetailBE.Site.SiteName = dr["Site"] != DBNull.Value ? Convert.ToString(dr["Site"]) : string.Empty;
                    purchaseOrderDetailBE.Vendor = new UP_VendorBE();
                    purchaseOrderDetailBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                    purchaseOrderDetailBE.Vendor.VendorName = dr["Vendor"] != DBNull.Value ? Convert.ToString(dr["Vendor"]) : string.Empty;
                    purchaseOrderDetailBE.Purchase_order = dr["PO"] != DBNull.Value ? Convert.ToString(dr["PO"]) : string.Empty;
                    purchaseOrderDetailBE.Original_due_date = dr["POOrgDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["POOrgDueDate"]) : (DateTime?)null;
                    purchaseOrderDetailBE.Order_raised = dr["POOrderRaised"] != DBNull.Value ? Convert.ToDateTime(dr["POOrderRaised"]) : (DateTime?)null;
                    purchaseOrderDetailBE.ReceiptDate = dr["ReceiptDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReceiptDate"]) : (DateTime?)null;
                    lstPurchaseOrderDetailBE.Add(purchaseOrderDetailBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstPurchaseOrderDetailBE;
        }

        public List<Up_PurchaseOrderDetailBE> GetDelPerPODetailByBookingReportDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", oUp_PurchaseOrderDetailBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oUp_PurchaseOrderDetailBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oUp_PurchaseOrderDetailBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@FromDate", oUp_PurchaseOrderDetailBE.FromDate);
                param[index++] = new SqlParameter("@ToDate", oUp_PurchaseOrderDetailBE.ToDate);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    var purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                    purchaseOrderDetailBE.Site = new MAS_SiteBE();
                    purchaseOrderDetailBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    purchaseOrderDetailBE.Site.SiteName = dr["Site"] != DBNull.Value ? Convert.ToString(dr["Site"]) : string.Empty;
                    purchaseOrderDetailBE.Vendor = new UP_VendorBE();
                    purchaseOrderDetailBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                    purchaseOrderDetailBE.Vendor.VendorName = dr["Vendor"] != DBNull.Value ? Convert.ToString(dr["Vendor"]) : string.Empty;

                    purchaseOrderDetailBE.Purchase_order = dr["PO"] != DBNull.Value ? Convert.ToString(dr["PO"]) : string.Empty;
                    purchaseOrderDetailBE.Original_due_date = dr["POOrgDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["POOrgDueDate"]) : (DateTime?)null;

                    purchaseOrderDetailBE.Booking = new APPBOK_BookingBE();
                    purchaseOrderDetailBE.Booking.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"]) : 0;
                    purchaseOrderDetailBE.Booking.BookingRef = dr["BookingRef"] != DBNull.Value ? Convert.ToString(dr["BookingRef"]) : string.Empty;
                    purchaseOrderDetailBE.Booking.ScheduleDate = dr["ScheduleDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduleDate"]) : (DateTime?)null;
                    purchaseOrderDetailBE.Booking.BookingDay = dr["SchDay"] != DBNull.Value ? Convert.ToString(dr["SchDay"]) : string.Empty;
                    purchaseOrderDetailBE.Booking.SupplierType = dr["SupplierType"] != DBNull.Value ? Convert.ToString(dr["SupplierType"]) : string.Empty;

                    purchaseOrderDetailBE.Booking.SlotTime = new SYS_SlotTimeBE();
                    purchaseOrderDetailBE.Booking.SlotTime.SlotTime = dr["ScheduleTime"] != DBNull.Value ? Convert.ToString(dr["ScheduleTime"]) : string.Empty;
                    lstPurchaseOrderDetailBE.Add(purchaseOrderDetailBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstPurchaseOrderDetailBE;
        }

        public List<Up_PurchaseOrderDetailBE> GetGetDelPerPOReceiptDataDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetail = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@OriginalDueDate", oUp_PurchaseOrderDetailBE.Original_due_date);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site.SiteID);
                param[index++] = new SqlParameter("@ReceiptDate", oUp_PurchaseOrderDetailBE.ReceiptDate);
                var dataTable = new DataTable();
                dataTable = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param).Tables[0];

                foreach (DataRow dr in dataTable.Rows)
                {
                    var poDetailBE = new Up_PurchaseOrderDetailBE();
                    poDetailBE.ReceiptDate = dr["ReceiptDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReceiptDate"]) : (DateTime?)null;
                    poDetailBE.Line_No = dr["LineNo"] != DBNull.Value ? Convert.ToInt32(dr["LineNo"].ToString()) : (int?)null;
                    poDetailBE.ODSKU_No = dr["SKU"] != DBNull.Value ? Convert.ToString(dr["SKU"]) : string.Empty;
                    poDetailBE.Vendor_Code = dr["VendorCode"] != DBNull.Value ? Convert.ToString(dr["VendorCode"]) : string.Empty;
                    poDetailBE.ProductDescription = dr["Description"] != DBNull.Value ? Convert.ToString(dr["Description"]) : string.Empty;
                    poDetailBE.Qty_receipted = dr["QtyReceipted"] != DBNull.Value ? Convert.ToInt32(dr["QtyReceipted"].ToString()) : (int?)null;
                    lstPurchaseOrderDetail.Add(poDetailBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstPurchaseOrderDetail;
        }

        public bool CheckIfOTIFHitDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            bool IsOTIFHIt = false;
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.Site.SiteID);
                param[index++] = new SqlParameter("@Order_raised", oUp_PurchaseOrderDetailBE.Order_raised);
                DataSet dsOTIFHIT = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);


                if (dsOTIFHIT != null && dsOTIFHIT.Tables.Count > 0)
                {
                    DataTable dtOTIFHIT = dsOTIFHIT.Tables[0];
                    if (dtOTIFHIT.Rows.Count > 0)
                        IsOTIFHIt = true;
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return IsOTIFHIt;
        }

        public List<Up_PurchaseOrderDetailBE> GetOpenPOVendorDetailsDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> Up_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];

                param[index++] = new SqlParameter("@SelectedVendorIDs", oUp_PurchaseOrderDetailBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oUp_PurchaseOrderDetailBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@PurchaseOrder", oUp_PurchaseOrderDetailBE.PurchaseOrderIds);
                param[index++] = new SqlParameter("@Date", oUp_PurchaseOrderDetailBE.SelectedDate);
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@UserRoleID", oUp_PurchaseOrderDetailBE.UserRoleID);
                param[index++] = new SqlParameter("@UserID", oUp_PurchaseOrderDetailBE.UserID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPTBOK_OpenPOOrderVendorview", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    Up_PurchaseOrderDetailBE oNewUp_PurchaseOrderDetailBEList = new Up_PurchaseOrderDetailBE();

                    oNewUp_PurchaseOrderDetailBEList.Vendor_No = dr["Vendor_No"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Vendor_Name = dr["Vendor_Name"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Purchase_order = dr["Purchase_order"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Line_No = (!string.IsNullOrEmpty(dr["Line_No"].ToString())) ? Convert.ToInt32(dr["Line_No"]) : (int?)null;
                    oNewUp_PurchaseOrderDetailBEList.Order_raisedString = dr["Order_raised"] != DBNull.Value ? dr["Order_raised"].ToString() : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.Original_due_dateString = dr["Original_due_date"] != DBNull.Value ? dr["Original_due_date"].ToString() : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.SiteName = dr["SiteName"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.OD_Code = dr["OD_Code"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Direct_code = dr["Direct_code"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Vendor_Code = dr["Vendor_code"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Product_description = dr["Product_description"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.UOM = dr["UOM"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Original_quantity = dr["Original_quantity"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Outstanding_Qty = dr["Outstanding_Qty"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.PO_cost = (!string.IsNullOrEmpty(dr["UnitCost"].ToString())) ? Convert.ToDecimal(dr["UnitCost"].ToString()) : 0;
                    oNewUp_PurchaseOrderDetailBEList.PO_Totalcost = (!string.IsNullOrEmpty(dr["TotalLineCost"].ToString())) ? Convert.ToDecimal(dr["TotalLineCost"].ToString()) : 0;
                    oNewUp_PurchaseOrderDetailBEList.Currency = dr["Currency"].ToString();

                    oNewUp_PurchaseOrderDetailBEList.StockPlannerNo = dr["StockPlannerNo"] != DBNull.Value ? dr["StockPlannerNo"].ToString() : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.StockPlannerName = dr["StockPlannerName"] != DBNull.Value ? dr["StockPlannerName"].ToString() : string.Empty;


                    oNewUp_PurchaseOrderDetailBEList.CreatedBy = dr["CreatedBy"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Qty_On_Hand = dr["Qty_On_Hand"].ToString();
                    oNewUp_PurchaseOrderDetailBEList.Other = dr["Other"].ToString();

                    oNewUp_PurchaseOrderDetailBEList.Expected_dateString = dr["Expected_Date"] != DBNull.Value ? dr["Expected_Date"].ToString() : string.Empty;
                    oNewUp_PurchaseOrderDetailBEList.ScheduleDateString = dr["ScheduleDate"] != DBNull.Value ? dr["ScheduleDate"].ToString() : string.Empty;


                    oNewUp_PurchaseOrderDetailBEList.DeliveryStatus = string.Empty;
                    if (dr.Table.Columns.Contains("BookingStatus"))
                        oNewUp_PurchaseOrderDetailBEList.DeliveryStatus = dr["BookingStatus"] != DBNull.Value ? dr["BookingStatus"].ToString() : string.Empty;

                    Up_PurchaseOrderDetailBEList.Add(oNewUp_PurchaseOrderDetailBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> SentPurchaseOrderReportDataDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@SelectedPONos", oUp_PurchaseOrderDetailBE.SelectedPONos);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oUp_PurchaseOrderDetailBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oUp_PurchaseOrderDetailBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oUp_PurchaseOrderDetailBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@DisplayOption", oUp_PurchaseOrderDetailBE.DisplayOption);
                param[index++] = new SqlParameter("@DueBy", oUp_PurchaseOrderDetailBE.DueBy);
                param[index++] = new SqlParameter("@FromDate", oUp_PurchaseOrderDetailBE.FromDate);
                param[index++] = new SqlParameter("@ToDate", oUp_PurchaseOrderDetailBE.ToDate);
                param[index++] = new SqlParameter("@PageCount", oUp_PurchaseOrderDetailBE.PageCount);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    var purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                    purchaseOrderDetailBE.Purchase_order = dr["PONo"] != DBNull.Value ? Convert.ToString(dr["PONo"]) : string.Empty;
                    purchaseOrderDetailBE.Status = dr["Status"] != DBNull.Value ? Convert.ToString(dr["Status"]) : string.Empty;

                    purchaseOrderDetailBE.Site = new MAS_SiteBE();
                    purchaseOrderDetailBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    purchaseOrderDetailBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? Convert.ToString(dr["SiteName"]) : string.Empty;

                    purchaseOrderDetailBE.Vendor = new UP_VendorBE();
                    purchaseOrderDetailBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                    purchaseOrderDetailBE.Vendor.Vendor_No = dr["VendorNo"] != DBNull.Value ? Convert.ToString(dr["VendorNo"]) : string.Empty;
                    purchaseOrderDetailBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                    purchaseOrderDetailBE.Vendor.StockPlannerID = dr["StockPlannerId"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerId"]) : (int?)null;
                    purchaseOrderDetailBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] != DBNull.Value ? Convert.ToString(dr["StockPlannerNo"]) : string.Empty;
                    purchaseOrderDetailBE.Vendor.StockPlannerName = dr["StockPlannerName"] != DBNull.Value ? Convert.ToString(dr["StockPlannerName"]) : string.Empty;

                    purchaseOrderDetailBE.Order_raised = dr["OrderRaised"] != DBNull.Value ? Convert.ToDateTime(dr["OrderRaised"]) : (DateTime?)null;
                    purchaseOrderDetailBE.Original_due_date = dr["OriginalDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["OriginalDueDate"]) : (DateTime?)null;
                    purchaseOrderDetailBE.FirstSentToEmail = dr["FirstSentToEmail"] != DBNull.Value ? Convert.ToString(dr["FirstSentToEmail"]) : string.Empty;
                    purchaseOrderDetailBE.SentDate = dr["SentDate"] != DBNull.Value ? Convert.ToDateTime(dr["SentDate"]) : (DateTime?)null;
                    purchaseOrderDetailBE.CommunicationType = dr["CommunicationType"] != DBNull.Value ? Convert.ToString(dr["CommunicationType"]) : string.Empty;

                    purchaseOrderDetailBE.SentTo = dr["SentTo"] != DBNull.Value ? Convert.ToString(dr["SentTo"]) : string.Empty;
                    purchaseOrderDetailBE.LanguageID = dr["LanguageID"] != DBNull.Value ? Convert.ToInt32(dr["LanguageID"]) : 1;
                    if (dt.Columns.Contains("RecordCount"))
                        purchaseOrderDetailBE.RecordCount = dr["RecordCount"] != DBNull.Value ? Convert.ToInt32(dr["RecordCount"]) : 1;

                    purchaseOrderDetailBE.SentFrom = dr["SentFrom"] != DBNull.Value ? Convert.ToString(dr["SentFrom"]) : string.Empty;
                    purchaseOrderDetailBE.SendMethod = dr["SendMethod"] != DBNull.Value ? Convert.ToString(dr["SendMethod"]) : string.Empty;

                    lstPurchaseOrderDetailBE.Add(purchaseOrderDetailBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstPurchaseOrderDetailBE;
        }

        public List<Up_PurchaseOrderDetailBE> GetSentPOEmailDetailsDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@SiteId", oUp_PurchaseOrderDetailBE.SiteId);
                param[index++] = new SqlParameter("@VendorId", oUp_PurchaseOrderDetailBE.VendorId);
                param[index++] = new SqlParameter("@CommunicationType", oUp_PurchaseOrderDetailBE.CommunicationType);
                param[index++] = new SqlParameter("@SentDate", oUp_PurchaseOrderDetailBE.SentDate);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    var purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                    purchaseOrderDetailBE.Purchase_order = dr["PONo"] != DBNull.Value ? Convert.ToString(dr["PONo"]) : string.Empty;
                    purchaseOrderDetailBE.SentFrom = dr["SentFrom"] != DBNull.Value ? Convert.ToString(dr["SentFrom"]) : string.Empty;
                    purchaseOrderDetailBE.SentTo = dr["SentTo"] != DBNull.Value ? Convert.ToString(dr["SentTo"]) : string.Empty;
                    purchaseOrderDetailBE.Subject = dr["Subject"] != DBNull.Value ? Convert.ToString(dr["Subject"]) : string.Empty;
                    purchaseOrderDetailBE.Body = dr["Body"] != DBNull.Value ? Convert.ToString(dr["Body"]) : string.Empty;
                    purchaseOrderDetailBE.SentDate = dr["SentDate"] != DBNull.Value ? Convert.ToDateTime(dr["SentDate"]) : (DateTime?)null;
                    purchaseOrderDetailBE.LanguageID = dr["LanguageID"] != DBNull.Value ? Convert.ToInt32(dr["LanguageID"]) : 0;
                    purchaseOrderDetailBE.IsMailSentInLanguage = dr["IsMailSentInLanguage"] != DBNull.Value ? Convert.ToBoolean(dr["IsMailSentInLanguage"]) : false;
                    purchaseOrderDetailBE.CommunicationType = dr["CommunicationType"] != DBNull.Value ? Convert.ToString(dr["CommunicationType"]) : string.Empty;
                    purchaseOrderDetailBE.FirstSentToEmail = dr["FirstSentToEmail"] != DBNull.Value ? Convert.ToString(dr["FirstSentToEmail"]) : string.Empty;

                    lstPurchaseOrderDetailBE.Add(purchaseOrderDetailBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstPurchaseOrderDetailBE;
        }

        public int? SaveImportNewSentPOEmailDataDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.SiteId);
                param[index++] = new SqlParameter("@VendorId", oUp_PurchaseOrderDetailBE.VendorId);
                param[index++] = new SqlParameter("@LanguageId", oUp_PurchaseOrderDetailBE.LanguageID);
                param[index++] = new SqlParameter("@SentTo", oUp_PurchaseOrderDetailBE.SentTo);
                param[index++] = new SqlParameter("@FirstSentToEmail", oUp_PurchaseOrderDetailBE.FirstSentToEmail);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<Up_PurchaseOrderDetailBE> GetRushReportDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oUp_PurchaseOrderDetailBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oUp_PurchaseOrderDetailBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oUp_PurchaseOrderDetailBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@DisplayOption", oUp_PurchaseOrderDetailBE.DisplayOption);
                param[index++] = new SqlParameter("@FromDate", oUp_PurchaseOrderDetailBE.FromDate);
                param[index++] = new SqlParameter("@ToDate", oUp_PurchaseOrderDetailBE.ToDate);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    var purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();

                    purchaseOrderDetailBE.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"].ToString());
                    purchaseOrderDetailBE.Purchase_order = dr["Purchase_order"] != DBNull.Value ? Convert.ToString(dr["Purchase_order"]) : string.Empty;

                    purchaseOrderDetailBE.StockPlannerID = dr["StockPlannerId"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerId"]) : (int?)null;
                    purchaseOrderDetailBE.StockPlannerNo = dr["StockPlannerNumber"] != DBNull.Value ? Convert.ToString(dr["StockPlannerNumber"]) : string.Empty;
                    purchaseOrderDetailBE.StockPlannerName = dr["StockPlannerName"] != DBNull.Value ? Convert.ToString(dr["StockPlannerName"]) : string.Empty;
                    purchaseOrderDetailBE.Product_description = dr["Product_description"] != DBNull.Value ? Convert.ToString(dr["Product_description"]) : string.Empty;

                    purchaseOrderDetailBE.VendorId = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                    purchaseOrderDetailBE.Vendor_No = dr["Vendor_No"] != DBNull.Value ? Convert.ToString(dr["Vendor_No"]) : string.Empty;
                    purchaseOrderDetailBE.VendorName = dr["Vendor_Name"] != DBNull.Value ? Convert.ToString(dr["Vendor_Name"]) : string.Empty;


                    purchaseOrderDetailBE.OD_SKU_NO = dr["OD_SKU_NO"] != DBNull.Value ? Convert.ToString(dr["OD_SKU_NO"]) : string.Empty;
                    purchaseOrderDetailBE.Direct_SKU = dr["Direct_SKU"] != DBNull.Value ? Convert.ToString(dr["Direct_SKU"]) : string.Empty;
                    purchaseOrderDetailBE.Product_Lead_time = dr["Product_Lead_time"] != DBNull.Value ? Convert.ToString(dr["Product_Lead_time"]) : string.Empty;

                    purchaseOrderDetailBE.Order_raised = dr["Order_raised"] != DBNull.Value ? Convert.ToDateTime(dr["Order_raised"]) : (DateTime?)null;
                    purchaseOrderDetailBE.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    purchaseOrderDetailBE.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevisedDueDate"]) : (DateTime?)null;

                    purchaseOrderDetailBE.Variance = dr["Variance"] != DBNull.Value ? Convert.ToInt32(dr["Variance"]) : (int?)null;


                    purchaseOrderDetailBE.SiteID = Convert.ToInt32(dr["SiteID"].ToString());
                    purchaseOrderDetailBE.Warehouse = dr["Warehouse"].ToString();

                    lstPurchaseOrderDetailBE.Add(purchaseOrderDetailBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstPurchaseOrderDetailBE;
        }


        public int? UpdatePOReconciliationDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrderIds", oUp_PurchaseOrderDetailBE.PurchaseOrderIds);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataTable SentPurchaseOrderReportExportDataDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@SelectedPONos", oUp_PurchaseOrderDetailBE.SelectedPONos);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oUp_PurchaseOrderDetailBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oUp_PurchaseOrderDetailBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oUp_PurchaseOrderDetailBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@DisplayOption", oUp_PurchaseOrderDetailBE.DisplayOption);
                param[index++] = new SqlParameter("@DueBy", oUp_PurchaseOrderDetailBE.DueBy);
                param[index++] = new SqlParameter("@FromDate", oUp_PurchaseOrderDetailBE.FromDate);
                param[index++] = new SqlParameter("@ToDate", oUp_PurchaseOrderDetailBE.ToDate);
                param[index++] = new SqlParameter("@PageCount", oUp_PurchaseOrderDetailBE.PageCount);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }


        public List<Up_PurchaseOrderDetailBE> GetUnknownVendorIDForUnexpectedCarrierBookingDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;

                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    var purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();

                    purchaseOrderDetailBE.VendorId = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;

                    purchaseOrderDetailBE.VendorName = dr["Vendor_Name"] != DBNull.Value ? Convert.ToString(dr["Vendor_Name"]) : string.Empty;

                    lstPurchaseOrderDetailBE.Add(purchaseOrderDetailBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstPurchaseOrderDetailBE;
        }


        public DataTable GetOpenPOReportDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@SITEID", oUp_PurchaseOrderDetailBE.SiteID);
                param[index++] = new SqlParameter("@StockPlannerGroupingID", Convert.ToInt32(oUp_PurchaseOrderDetailBE.StockPlannerGroupingID));
                param[index++] = new SqlParameter("@CountryID", oUp_PurchaseOrderDetailBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@VendorID", oUp_PurchaseOrderDetailBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@USERID", oUp_PurchaseOrderDetailBE.StockPlannerID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "USP_OpenBackOrderSummary", param);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public int? DeleteRevisedDueDatesDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@SelectedProductCodes", oUp_PurchaseOrderDetailBE.SelectedProductCodes);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@CurrentRevisedDueDate", oUp_PurchaseOrderDetailBE.CurrentRevisedDueDate);
                param[index++] = new SqlParameter("@UserID", oUp_PurchaseOrderDetailBE.UserID);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.SiteID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataSet GetDeletedOtifExclusionRevisedDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            DataSet ds = new DataSet();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.SiteID);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@DateFrom", oUp_PurchaseOrderDetailBE.DateFrom);
                param[index++] = new SqlParameter("@DateTo", oUp_PurchaseOrderDetailBE.DateTo);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oUp_PurchaseOrderDetailBE.SelectedVendorIDs);
                if (oUp_PurchaseOrderDetailBE.SKU != null && oUp_PurchaseOrderDetailBE.SKU.Direct_SKU != "")
                {
                    param[index++] = new SqlParameter("@SKU", oUp_PurchaseOrderDetailBE.SKU.Direct_SKU);
                }
                if (oUp_PurchaseOrderDetailBE.VikingSku != null && oUp_PurchaseOrderDetailBE.VikingSku != "")
                {
                    param[index++] = new SqlParameter("@Viking", oUp_PurchaseOrderDetailBE.VikingSku);
                }

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public int? SaveManualSentPOEmailDataDAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[20];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@Purchase_order", oUp_PurchaseOrderDetailBE.Purchase_order);
                param[index++] = new SqlParameter("@SiteID", oUp_PurchaseOrderDetailBE.SiteID);
                param[index++] = new SqlParameter("@StockPlannerId", oUp_PurchaseOrderDetailBE.StockPlannerID);
                param[index++] = new SqlParameter("@VendorId", oUp_PurchaseOrderDetailBE.VendorId);
                param[index++] = new SqlParameter("@Order_raised", oUp_PurchaseOrderDetailBE.Order_raised);
                param[index++] = new SqlParameter("@OriginalDueDate", oUp_PurchaseOrderDetailBE.Original_due_date);
                param[index++] = new SqlParameter("@EntryType", oUp_PurchaseOrderDetailBE.EntryType);
                param[index++] = new SqlParameter("@SentFrom", oUp_PurchaseOrderDetailBE.SentFrom);
                param[index++] = new SqlParameter("@SentTo", oUp_PurchaseOrderDetailBE.SentTo);
                param[index++] = new SqlParameter("@Subject", oUp_PurchaseOrderDetailBE.Subject);
                param[index++] = new SqlParameter("@Body", oUp_PurchaseOrderDetailBE.Body);
                param[index++] = new SqlParameter("@LanguageID", oUp_PurchaseOrderDetailBE.LanguageID);
                param[index++] = new SqlParameter("@IsMailSentInLanguage", oUp_PurchaseOrderDetailBE.IsMailSentInLanguage);
                param[index++] = new SqlParameter("@CommunicationType", oUp_PurchaseOrderDetailBE.CommunicationType);
                param[index++] = new SqlParameter("@PrmWarehouse", oUp_PurchaseOrderDetailBE.Warehouse);
                param[index++] = new SqlParameter("@FirstSentToEmail", oUp_PurchaseOrderDetailBE.FirstSentToEmail);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


    }
}

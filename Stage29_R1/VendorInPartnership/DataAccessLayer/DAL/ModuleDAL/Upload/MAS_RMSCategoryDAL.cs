﻿using BusinessEntities.ModuleBE.Upload;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Upload
{
    public class MAS_RMSCategoryDAL
    {

        public List<MAS_RMSCategoryBE> GetRMSCategoryDAL(MAS_RMSCategoryBE oMAS_RMSCategoryBE)
        {

            List<MAS_RMSCategoryBE> lstMAS_RMSCategoryBE = new List<MAS_RMSCategoryBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_RMSCategoryBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_RMSCategory", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MAS_RMSCategoryBE oNewMAS_RMSCategoryBE = new MAS_RMSCategoryBE();
                    oNewMAS_RMSCategoryBE.CategoryName = dr["CategoryName"].ToString();
                    oNewMAS_RMSCategoryBE.CreateByName = dr["CreateByName"].ToString();
                    oNewMAS_RMSCategoryBE.CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]) : (DateTime?)null;
                    oNewMAS_RMSCategoryBE.RMSCategoryID = Convert.ToInt32(dr["RMSCategoryID"]);
                    lstMAS_RMSCategoryBE.Add(oNewMAS_RMSCategoryBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstMAS_RMSCategoryBE;

        }

        public int? AddRMSCategoryDAL(MAS_RMSCategoryBE oMAS_RMSCategoryBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oMAS_RMSCategoryBE.Action);
                param[index++] = new SqlParameter("@CategoryName", oMAS_RMSCategoryBE.CategoryName);
                param[index++] = new SqlParameter("@CreatedBy", oMAS_RMSCategoryBE.CreatedBy);

                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "[spMAS_RMSCategory]", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateRMSCategoryDAL(MAS_RMSCategoryBE oMAS_RMSCategoryBE)
        {
            int? updated = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oMAS_RMSCategoryBE.Action);
                param[index++] = new SqlParameter("@CategoryName", oMAS_RMSCategoryBE.CategoryName);
                param[index++] = new SqlParameter("@RMSCategoryID", oMAS_RMSCategoryBE.RMSCategoryID);
                updated = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "[spMAS_RMSCategory]", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return updated;
        }

        public string GetCategoryByIdDAL(MAS_RMSCategoryBE oMAS_RMSCategoryBE)
        {
            string result = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_RMSCategoryBE.Action);
                param[index++] = new SqlParameter("@RMSCategoryID", oMAS_RMSCategoryBE.RMSCategoryID);
                result = Convert.ToString(SqlHelper.ExecuteScalar(DBConnection.Connection, CommandType.StoredProcedure, "[spMAS_RMSCategory]", param));
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return result;
        }

        public List<MAS_RMSCategoryBE> GetRMSAllCategoryBAL(MAS_RMSCategoryBE oMAS_RMSCategoryBE)
        {

            List<MAS_RMSCategoryBE> lstMAS_RMSCategoryBE = new List<MAS_RMSCategoryBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_RMSCategoryBE.Action);
                param[index++] = new SqlParameter("@CategoryName", oMAS_RMSCategoryBE.CategoryName);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_RMSCategory", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MAS_RMSCategoryBE oNewMAS_RMSCategoryBE = new MAS_RMSCategoryBE();
                    oNewMAS_RMSCategoryBE.CategoryName = dr["CategoryName"].ToString();
                    oNewMAS_RMSCategoryBE.RMSCategoryID = Convert.ToInt32(dr["RMSCategoryID"]);
                    lstMAS_RMSCategoryBE.Add(oNewMAS_RMSCategoryBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstMAS_RMSCategoryBE;

        }


        public void GetRMSCategoryDepartmentDAL(MAS_RMSCategoryBE oMAS_RMSCategoryDepartmentBE, out List<UP_RMSBE> lstMAS_RMSCategoryDepartmentBE, out int RecordCount)
        {

            lstMAS_RMSCategoryDepartmentBE = new List<UP_RMSBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oMAS_RMSCategoryDepartmentBE.Action);
                param[index++] = new SqlParameter("@RMSCategoryIDs", oMAS_RMSCategoryDepartmentBE.RMSCategoryIDs);
                param[index++] = new SqlParameter("@PageIndex", oMAS_RMSCategoryDepartmentBE.PageCount);
                param[index++] = new SqlParameter("@RecordCount", SqlDbType.Int);
                param[index - 1].Direction = ParameterDirection.Output;

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_RMSCategory", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {

                    //a.RMSDepartmentID, c.Division,c.Div_Name DivisionName,c.Group_Name GroupName,
                    //c.Group_No  [GroupNo],c.Dept,c.Dept_Name DepartmentName 

                    UP_RMSBE oNewUP_RMSBE = new UP_RMSBE();
                    MAS_RMSCategoryBE oNewMAS_RMSCategoryBE = new MAS_RMSCategoryBE();
                    oNewMAS_RMSCategoryBE.RMSCategoryID = Convert.ToInt32(dr["RMSCategoryID"]);
                    oNewMAS_RMSCategoryBE.CategoryName = dr["CategoryName"].ToString();
                    oNewMAS_RMSCategoryBE.CreateByName = dr["CreateByName"].ToString();
                    oNewMAS_RMSCategoryBE.CreatedOn = dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]) : (DateTime?)null;
                    oNewUP_RMSBE.Division = dr["Division"].ToString();
                    oNewUP_RMSBE.Div_Name = dr["DivisionName"].ToString();
                    oNewUP_RMSBE.Group_Name = dr["GroupName"].ToString();
                    oNewUP_RMSBE.Group_No = dr["GroupNo"].ToString();
                    oNewUP_RMSBE.Dept = dr["Dept"].ToString();
                    oNewUP_RMSBE.Dept_Name = dr["DepartmentName"].ToString();
                    oNewUP_RMSBE.RMSCategory = oNewMAS_RMSCategoryBE;
                    lstMAS_RMSCategoryDepartmentBE.Add(oNewUP_RMSBE);
                }
                RecordCount = Convert.ToInt32(param[3].Value);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                lstMAS_RMSCategoryDepartmentBE = null;
                RecordCount = 0;
            }


        }


    }
}

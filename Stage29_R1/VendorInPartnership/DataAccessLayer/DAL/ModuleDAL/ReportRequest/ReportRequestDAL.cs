﻿using BusinessEntities.ModuleBE.ReportRequest;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.ReportRequest
{
    public class ReportRequestDAL
    {

        public int? addReportRequestDAL(ReportRequestBE oReportRequestBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[36];
                param[index++] = new SqlParameter("@Action", oReportRequestBE.Action);
                param[index++] = new SqlParameter("@ModuleName", oReportRequestBE.ModuleName);
                param[index++] = new SqlParameter("@ReportName", oReportRequestBE.ReportName);
                param[index++] = new SqlParameter("@ReportNamePath", oReportRequestBE.ReportNamePath);
                param[index++] = new SqlParameter("@ReportAction", oReportRequestBE.ReportAction);
                param[index++] = new SqlParameter("@ReportDatatableName", oReportRequestBE.ReportDatatableName);
                param[index++] = new SqlParameter("@CountryIDs", oReportRequestBE.CountryIDs);
                param[index++] = new SqlParameter("@CountryName", oReportRequestBE.CountryName);
                param[index++] = new SqlParameter("@SiteIDs", oReportRequestBE.SiteIDs);
                param[index++] = new SqlParameter("@SiteName", oReportRequestBE.SiteName);
                param[index++] = new SqlParameter("@StockPlannerIDs", oReportRequestBE.StockPlannerIDs);
                param[index++] = new SqlParameter("@StockPlannerName", oReportRequestBE.StockPlannerName);
                param[index++] = new SqlParameter("@VendorIDs", oReportRequestBE.VendorIDs);
                param[index++] = new SqlParameter("@VendorName", oReportRequestBE.VendorName);
                param[index++] = new SqlParameter("@OD_SKU_No", oReportRequestBE.OfficeDepotSKU);
                param[index++] = new SqlParameter("@ItemClassification", oReportRequestBE.ItemClassification);
                param[index++] = new SqlParameter("@DateFrom", oReportRequestBE.DateFrom);
                param[index++] = new SqlParameter("@DateTo", oReportRequestBE.DateTo);
                param[index++] = new SqlParameter("@ReportType", oReportRequestBE.ReportType);
                param[index++] = new SqlParameter("@PendingSKUs", oReportRequestBE.PendingSKUs);
                param[index++] = new SqlParameter("@DiscSKUs", oReportRequestBE.DiscSKUs);
                param[index++] = new SqlParameter("@ActiveSKUs", oReportRequestBE.ActiveSKUs);
                param[index++] = new SqlParameter("@PurchaseOrder", oReportRequestBE.PurchaseOrder);
                param[index++] = new SqlParameter("@PurchaseOrderDue", oReportRequestBE.PurchaseOrderDue);
                param[index++] = new SqlParameter("@PurchaseOrderReceived", oReportRequestBE.PurchaseOrderReceived);
                param[index++] = new SqlParameter("@RequestStatus", oReportRequestBE.RequestStatus);
                param[index++] = new SqlParameter("@UserID", oReportRequestBE.UserID);
                param[index++] = new SqlParameter("@RequestTime", oReportRequestBE.RequestTime);
                param[index++] = new SqlParameter("@EuropeanorLocal", oReportRequestBE.EuropeanorLocal);
                param[index++] = new SqlParameter("@SelectedRMSCategoryIDs", oReportRequestBE.SelectedRMSCategoryIDs);
                param[index++] = new SqlParameter("@SelectedRMSCategoryName", oReportRequestBE.SelectedRMSCategoryName);
                param[index++] = new SqlParameter("@SkugroupingID", oReportRequestBE.SkugroupingID);
                param[index++] = new SqlParameter("@UnderReviewSKUs", oReportRequestBE.UnderReviewSKUs);
                param[index++] = new SqlParameter("@StockPlannerGroupingIDs", oReportRequestBE.StockPlannerGroupingIDs);
                param[index++] = new SqlParameter("@ODCatCode", oReportRequestBE.ODCatCode);
                param[index++] = new SqlParameter("@CustomerIDs", oReportRequestBE.CustomerIDs);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spRPT_ReportRequest", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateReportStatusDAL(ReportRequestBE oReportRequestBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oReportRequestBE.Action);
                param[index++] = new SqlParameter("@RequestStatus", oReportRequestBE.RequestStatus);
                param[index++] = new SqlParameter("@ReportRequestID", oReportRequestBE.ReportRequestID);
                param[index++] = new SqlParameter("@ReportGeneratedTime", oReportRequestBE.ReportGeneratedTime);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spRPT_ReportRequest", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteReportRequestDAL(ReportRequestBE oReportRequestBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oReportRequestBE.Action);
                param[index++] = new SqlParameter("@ReportRequestID", oReportRequestBE.ReportRequestID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spRPT_ReportRequest", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<ReportRequestBE> GetReportRequestDAL(ReportRequestBE oReportRequestBE)
        {
            DataTable dt = new DataTable();
            List<ReportRequestBE> ReportRequestBE = new List<ReportRequestBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oReportRequestBE.Action);
                param[index++] = new SqlParameter("@UserID", oReportRequestBE.UserID);
                param[index++] = new SqlParameter("@ModuleName", oReportRequestBE.ModuleName);
                param[index++] = new SqlParameter("@RequestStatus", oReportRequestBE.RequestStatus);
                param[index++] = new SqlParameter("@ReportRequestID", oReportRequestBE.ReportRequestID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPT_ReportRequest", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    ReportRequestBE oNewReportRequestBE = new ReportRequestBE();

                    oNewReportRequestBE.ReportRequestID = Convert.ToInt32(dr["ReportRequestID"]);
                    oNewReportRequestBE.ModuleName = dr["ModuleName"].ToString();
                    oNewReportRequestBE.ReportName = dr["ReportName"].ToString();
                    oNewReportRequestBE.ReportNamePath = dr["ReportNamePath"].ToString();
                    oNewReportRequestBE.ReportAction = dr["ReportAction"].ToString();
                    oNewReportRequestBE.ReportDatatableName = dr["ReportDatatableName"].ToString();
                    oNewReportRequestBE.CountryIDs = dr["CountryIDs"].ToString();
                    oNewReportRequestBE.CountryName = dr["CountryName"].ToString();
                    oNewReportRequestBE.SiteIDs = dr["SiteIDs"].ToString();
                    oNewReportRequestBE.SiteName = dr["SiteName"].ToString();
                    oNewReportRequestBE.StockPlannerIDs = dr["StockPlannerIDs"].ToString();
                    oNewReportRequestBE.StockPlannerName = dr["StockPlannerName"].ToString();
                    oNewReportRequestBE.VendorIDs = dr["VendorIDs"].ToString();
                    oNewReportRequestBE.VendorName = dr["VendorName"].ToString();
                    oNewReportRequestBE.OD_SKU_No = dr["OD_SKU_No"].ToString();
                    oNewReportRequestBE.ItemClassification = dr["ItemClassification"].ToString();
                    oNewReportRequestBE.DateTo = dr["DateTo"] != DBNull.Value ? Convert.ToDateTime(dr["DateTo"]) : (DateTime?)null;
                    oNewReportRequestBE.DateFrom = dr["DateFrom"] != DBNull.Value ? Convert.ToDateTime(dr["DateFrom"]) : (DateTime?)null;
                    oNewReportRequestBE.ReportType = dr["ReportType"].ToString();
                    oNewReportRequestBE.PendingSKUs = Convert.ToInt32(dr["PendingSKUs"]);
                    oNewReportRequestBE.DiscSKUs = Convert.ToInt32(dr["DiscSKUs"]);
                    oNewReportRequestBE.ActiveSKUs = Convert.ToInt32(dr["ActiveSKUs"]);
                    oNewReportRequestBE.PurchaseOrder = dr["PurchaseOrder"].ToString();
                    oNewReportRequestBE.PurchaseOrderDue = dr["PurchaseOrderDue"] != DBNull.Value ? Convert.ToInt32(dr["PurchaseOrderDue"].ToString()) : (int?)null;
                    oNewReportRequestBE.PurchaseOrderReceived = dr["PurchaseOrderReceived"] != DBNull.Value ? Convert.ToInt32(dr["PurchaseOrderReceived"].ToString()) : (int?)null;
                    oNewReportRequestBE.SKUMarkedAs = dr["SKUMarkedAs"].ToString();
                    oNewReportRequestBE.RequestStatus = dr["RequestStatus"].ToString();
                    oNewReportRequestBE.UserID = Convert.ToInt32(dr["UserID"]);
                    oNewReportRequestBE.RequestTime = Convert.ToDateTime(dr["RequestTime"]);
                    oNewReportRequestBE.ReportPosition = dr["ReportPosition"].ToString();
                    oNewReportRequestBE.UserName = dr["UserName"].ToString();

                    oNewReportRequestBE.ReportNameAndType = dr["ReportName"].ToString();
                    if ((dr["ReportType"] != null))
                        oNewReportRequestBE.ReportNameAndType = dr["ReportName"].ToString() + " - " + dr["ReportType"].ToString();
                    if ((dr["DateFrom"] != DBNull.Value))
                    {
                        oNewReportRequestBE.DateRange = Convert.ToDateTime(dr["DateFrom"].ToString()).Day.ToString();
                        oNewReportRequestBE.DateRange = oNewReportRequestBE.DateRange + "/" + Convert.ToDateTime(dr["DateFrom"].ToString()).Month.ToString();
                        oNewReportRequestBE.DateRange = oNewReportRequestBE.DateRange + "/" + Convert.ToDateTime(dr["DateFrom"].ToString()).Year.ToString();
                    }
                    if ((dr["DateTo"] != DBNull.Value))
                    {
                        oNewReportRequestBE.DateRange = oNewReportRequestBE.DateRange + " - " + Convert.ToDateTime(dr["DateTo"].ToString()).Day.ToString();
                        oNewReportRequestBE.DateRange = oNewReportRequestBE.DateRange + "/" + Convert.ToDateTime(dr["DateTo"].ToString()).Month.ToString();
                        oNewReportRequestBE.DateRange = oNewReportRequestBE.DateRange + "/" + Convert.ToDateTime(dr["DateTo"].ToString()).Year.ToString();
                    }

                    oNewReportRequestBE.ReportGeneratedTime = dr["ReportGeneratedTime"] != DBNull.Value ? Convert.ToDateTime(dr["ReportGeneratedTime"]) : (DateTime?)null;

                    ReportRequestBE.Add(oNewReportRequestBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ReportRequestBE;
        }

        public List<ReportRequestBE> GetReportRequestByReportStatusDAL(ReportRequestBE oReportRequestBE)
        {
            List<ReportRequestBE> ReportRequestBE = new List<ReportRequestBE>();
            try
            {
                int index = 0;
                DataTable dt = new DataTable();
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oReportRequestBE.Action);
                param[index++] = new SqlParameter("@RequestStatus", oReportRequestBE.RequestStatus);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPT_ReportRequest", param);

                dt = ds.Tables[0];
                ReportRequestBE oNewReportRequestBE;
                foreach (DataRow dr in dt.Rows)
                {
                    oNewReportRequestBE = new ReportRequestBE();

                    oNewReportRequestBE.ReportRequestID = Convert.ToInt32(dr["ReportRequestID"]);
                    oNewReportRequestBE.ModuleName = dr["ModuleName"].ToString();
                    oNewReportRequestBE.ReportName = dr["ReportName"].ToString();
                    oNewReportRequestBE.ReportNamePath = dr["ReportNamePath"].ToString();
                    oNewReportRequestBE.ReportAction = dr["ReportAction"].ToString();
                    oNewReportRequestBE.ReportDatatableName = dr["ReportDatatableName"].ToString();
                    oNewReportRequestBE.CountryIDs = dr["CountryIDs"].ToString();
                    oNewReportRequestBE.CountryName = dr["CountryName"].ToString();
                    oNewReportRequestBE.SiteIDs = dr["SiteIDs"].ToString();
                    oNewReportRequestBE.SiteName = dr["SiteName"].ToString();
                    oNewReportRequestBE.StockPlannerIDs = dr["StockPlannerIDs"].ToString();
                    oNewReportRequestBE.StockPlannerName = dr["StockPlannerName"].ToString();
                    oNewReportRequestBE.VendorIDs = dr["VendorIDs"].ToString();
                    oNewReportRequestBE.VendorName = dr["VendorName"].ToString();
                    oNewReportRequestBE.OD_SKU_No = dr["OD_SKU_No"].ToString();
                    oNewReportRequestBE.ItemClassification = dr["ItemClassification"].ToString();
                    oNewReportRequestBE.DateTo = dr["DateTo"] != DBNull.Value ? Convert.ToDateTime(dr["DateTo"]) : (DateTime?)null;
                    oNewReportRequestBE.DateFrom = dr["DateFrom"] != DBNull.Value ? Convert.ToDateTime(dr["DateFrom"]) : (DateTime?)null;
                    oNewReportRequestBE.ReportType = dr["ReportType"].ToString();
                    oNewReportRequestBE.PendingSKUs = Convert.ToInt32(dr["PendingSKUs"]);
                    oNewReportRequestBE.DiscSKUs = Convert.ToInt32(dr["DiscSKUs"]);
                    oNewReportRequestBE.ActiveSKUs = Convert.ToInt32(dr["ActiveSKUs"]);
                    oNewReportRequestBE.PurchaseOrder = dr["PurchaseOrder"].ToString();
                    oNewReportRequestBE.PurchaseOrderDue = dr["PurchaseOrderDue"] != DBNull.Value ? Convert.ToInt32(dr["PurchaseOrderDue"].ToString()) : (int?)null;
                    oNewReportRequestBE.PurchaseOrderReceived = dr["PurchaseOrderReceived"] != DBNull.Value ? Convert.ToInt32(dr["PurchaseOrderReceived"].ToString()) : (int?)null;
                    oNewReportRequestBE.SKUMarkedAs = dr["SKUMarkedAs"].ToString();
                    oNewReportRequestBE.RequestStatus = dr["RequestStatus"].ToString();
                    oNewReportRequestBE.UserID = Convert.ToInt32(dr["UserID"]);
                    oNewReportRequestBE.RequestTime = Convert.ToDateTime(dr["RequestTime"]);
                    oNewReportRequestBE.ReportPosition = dr["ReportPosition"].ToString();
                    oNewReportRequestBE.UserName = dr["UserName"].ToString();

                    oNewReportRequestBE.ReportNameAndType = dr["ReportName"].ToString();
                    if ((dr["ReportType"] != DBNull.Value))
                        oNewReportRequestBE.ReportNameAndType = dr["ReportName"].ToString() + " - " + dr["ReportType"].ToString();

                    if ((dr["DateFrom"] != DBNull.Value))
                    {
                        oNewReportRequestBE.DateRange = Convert.ToDateTime(dr["DateFrom"].ToString()).Day.ToString();
                        oNewReportRequestBE.DateRange = oNewReportRequestBE.DateRange + "/" + Convert.ToDateTime(dr["DateFrom"].ToString()).Month.ToString();
                        oNewReportRequestBE.DateRange = oNewReportRequestBE.DateRange + "/" + Convert.ToDateTime(dr["DateFrom"].ToString()).Year.ToString();
                    }
                    if ((dr["DateTo"] != DBNull.Value))
                    {
                        oNewReportRequestBE.DateRange = oNewReportRequestBE.DateRange + " - " + Convert.ToDateTime(dr["DateTo"].ToString()).Day.ToString();
                        oNewReportRequestBE.DateRange = oNewReportRequestBE.DateRange + "/" + Convert.ToDateTime(dr["DateTo"].ToString()).Month.ToString();
                        oNewReportRequestBE.DateRange = oNewReportRequestBE.DateRange + "/" + Convert.ToDateTime(dr["DateTo"].ToString()).Year.ToString();
                    }
                    ReportRequestBE.Add(oNewReportRequestBE);
                    oNewReportRequestBE = null;
                }
                param = null;
                ds = null;
                dt = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return ReportRequestBE;
        }

        public List<ReportRequestBE> GetGeneratedReportCountDAL(ReportRequestBE oReportRequestBE)
        {
            DataTable dt = new DataTable();
            List<ReportRequestBE> ReportRequestBE = new List<ReportRequestBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oReportRequestBE.Action);
                param[index++] = new SqlParameter("@UserID", oReportRequestBE.UserID);



                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPT_ReportRequest", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    ReportRequestBE oNewReportRequestBE = new ReportRequestBE();
                    oNewReportRequestBE.GeneratedReportCount = dr["GeneratedReportCount"].ToString();
                    ReportRequestBE.Add(oNewReportRequestBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ReportRequestBE;
        }

        public List<int> GetOldReportRequestIDsDAL(ReportRequestBE oReportRequestBE)
        {
            DataTable dt = new DataTable();
            var lstRequestID = new List<int>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oReportRequestBE.Action);
                param[index++] = new SqlParameter("@UserID", oReportRequestBE.UserID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPT_ReportRequest", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    int intRequestID = new int();
                    intRequestID = dr["ReportRequestID"] != DBNull.Value ? Convert.ToInt32(dr["ReportRequestID"].ToString()) : 0;
                    lstRequestID.Add(intRequestID);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstRequestID;
        }


        public DataSet GetOtifExclusionReportDAL(ReportRequestBE oReportRequestBE)
        {
            DataSet Result = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", oReportRequestBE.Action);
                param[index++] = new SqlParameter("@ReportAction", oReportRequestBE.ReportAction);
                param[index++] = new SqlParameter("@CountryIDs", oReportRequestBE.CountryIDs);
                param[index++] = new SqlParameter("@SiteIDs", oReportRequestBE.SiteIDs);
                param[index++] = new SqlParameter("@StockPlannerIDs", oReportRequestBE.StockPlannerIDs);
                param[index++] = new SqlParameter("@VendorIDs", oReportRequestBE.VendorIDs);
                param[index++] = new SqlParameter("@OD_SKU_No", oReportRequestBE.OfficeDepotSKU);
                param[index++] = new SqlParameter("@DateFrom", oReportRequestBE.DateFrom);
                param[index++] = new SqlParameter("@DateTo", oReportRequestBE.DateTo);
                // param[index++] = new SqlParameter("@ODCatCode", oReportRequestBE.ODCatCode);           
                param[index++] = new SqlParameter("@StockPlannerGroupingIDs", oReportRequestBE.StockPlannerGroupingIDs);
                param[index++] = new SqlParameter("@SkugroupingID", oReportRequestBE.SkugroupingID);
                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spGetOTIFExclusionReport", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
    }
}

﻿using BusinessEntities.ModuleBE.GlobalSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.GlobalSettings
{
    public class PointsofContactSummaryDAL
    {

        public List<PointsofContactSummaryBE> GetPOCSummaryDAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            List<PointsofContactSummaryBE> oPointsofContactSummaryBEList = new List<PointsofContactSummaryBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oPointsofContactSummaryBE.Action);
                param[index++] = new SqlParameter("@UserID", oPointsofContactSummaryBE.UserID);
                param[index++] = new SqlParameter("@IsOpen", oPointsofContactSummaryBE.IsOpen);
                param[index++] = new SqlParameter("@IsExcludeVendor", oPointsofContactSummaryBE.IsExcludeVendor);
                param[index++] = new SqlParameter("@IsAll", oPointsofContactSummaryBE.IsAll);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPT_POCTrackerReport", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    PointsofContactSummaryBE oNewPointsofContactSummaryBE = new PointsofContactSummaryBE();
                    oNewPointsofContactSummaryBE.CountryID = dr["CountryID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CountryID"]);
                    oNewPointsofContactSummaryBE.Country = dr["Country"] == DBNull.Value ? null : Convert.ToString(dr["Country"]);
                    oNewPointsofContactSummaryBE.Scheduling = dr["Scheduling"] == DBNull.Value ? (double?)null : Convert.ToDouble(dr["Scheduling"]);
                    oNewPointsofContactSummaryBE.Discrepancies = dr["Discrepancies"] == DBNull.Value ? (double?)null : Convert.ToDouble(dr["Discrepancies"]);
                    oNewPointsofContactSummaryBE.OTIF = dr["OTIF"] == DBNull.Value ? (double?)null : Convert.ToDouble(dr["OTIF"]);
                    oNewPointsofContactSummaryBE.Scorecard = dr["Scorecard"] == DBNull.Value ? (double?)null : Convert.ToDouble(dr["Scorecard"]);
                    oNewPointsofContactSummaryBE.Inventory = dr["Inventory"] == DBNull.Value ? (double?)null : Convert.ToDouble(dr["Inventory"]);
                    oNewPointsofContactSummaryBE.Procurement = dr["Procurement"] == DBNull.Value ? (double?)null : Convert.ToDouble(dr["Procurement"]);
                    oNewPointsofContactSummaryBE.AccountsPayable = dr["AccountsPayable"] == DBNull.Value ? (double?)null : Convert.ToDouble(dr["AccountsPayable"]);
                    oNewPointsofContactSummaryBE.Executive = dr["Executive"] == DBNull.Value ? (double?)null : Convert.ToDouble(dr["Executive"]);
                    oPointsofContactSummaryBEList.Add(oNewPointsofContactSummaryBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oPointsofContactSummaryBEList;
        }

        public List<PointsofContactSummaryBE> GetPOCDetailsDAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            List<PointsofContactSummaryBE> oPointsofContactSummaryBEList = new List<PointsofContactSummaryBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oPointsofContactSummaryBE.Action);
                param[index++] = new SqlParameter("@CountryID", oPointsofContactSummaryBE.CountryID);
                param[index++] = new SqlParameter("@Module", oPointsofContactSummaryBE.Module);
                param[index++] = new SqlParameter("@Captured", oPointsofContactSummaryBE.Captured);
                param[index++] = new SqlParameter("@IsOpen", oPointsofContactSummaryBE.IsOpen);
                param[index++] = new SqlParameter("@IsAll", oPointsofContactSummaryBE.IsAll);
                param[index++] = new SqlParameter("@IsExcludeVendor", oPointsofContactSummaryBE.IsExcludeVendor);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPT_POCTrackerReport", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    PointsofContactSummaryBE oNewPointsofContactSummaryBE = new PointsofContactSummaryBE();
                    oNewPointsofContactSummaryBE.VendorID = dr["VendorID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["VendorID"]);
                    oNewPointsofContactSummaryBE.VendorName = dr["VendorName"] == DBNull.Value ? null : Convert.ToString(dr["VendorName"]);
                    oNewPointsofContactSummaryBE.Lines = dr["Lines"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Lines"]);
                    oNewPointsofContactSummaryBE.Weightage = dr["Weightage"] == DBNull.Value ? (double?)null : Convert.ToDouble(dr["Weightage"]);
                    oNewPointsofContactSummaryBE.SchedulingPOC = dr["SchedulingPOC"] == DBNull.Value ? 'N' : Convert.ToChar(dr["SchedulingPOC"]);
                    oNewPointsofContactSummaryBE.DiscrepancyPOC = dr["DiscrepancyPOC"] == DBNull.Value ? 'N' : Convert.ToChar(dr["DiscrepancyPOC"]);
                    oNewPointsofContactSummaryBE.OTIFPOC = dr["OTIFPOC"] == DBNull.Value ? 'N' : Convert.ToChar(dr["OTIFPOC"]);
                    oNewPointsofContactSummaryBE.ScorecardPOC = dr["ScorecardPOC"] == DBNull.Value ? 'N' : Convert.ToChar(dr["ScorecardPOC"]);
                    oNewPointsofContactSummaryBE.InventoryPOC = dr["InventoryPOC"] == DBNull.Value ? 'N' : Convert.ToChar(dr["InventoryPOC"]);
                    oNewPointsofContactSummaryBE.ProcurementPOC = dr["ProcurementPOC"] == DBNull.Value ? 'N' : Convert.ToChar(dr["ProcurementPOC"]);
                    oNewPointsofContactSummaryBE.AccountsPayablePOC = dr["AccountsPayablePOC"] == DBNull.Value ? 'N' : Convert.ToChar(dr["AccountsPayablePOC"]);
                    oNewPointsofContactSummaryBE.ExecutivePOC = dr["ExecutivePOC"] == DBNull.Value ? 'N' : Convert.ToChar(dr["ExecutivePOC"]);
                    oPointsofContactSummaryBEList.Add(oNewPointsofContactSummaryBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oPointsofContactSummaryBEList;
        }

        public Dictionary<string, List<PointsofContactSummaryBE>> GetPOCDetailsSiteDAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            List<PointsofContactSummaryBE> oPointsofContactDetailBEList = new List<PointsofContactSummaryBE>();
            List<PointsofContactSummaryBE> oPointsofContactdetailSiteBEList = new List<PointsofContactSummaryBE>();

            Dictionary<string, List<PointsofContactSummaryBE>> myLists = new Dictionary<string, List<PointsofContactSummaryBE>>();

            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oPointsofContactSummaryBE.Action);
                param[index++] = new SqlParameter("@CountryID", oPointsofContactSummaryBE.CountryID);
                param[index++] = new SqlParameter("@Module", oPointsofContactSummaryBE.Module);
                param[index++] = new SqlParameter("@Captured", oPointsofContactSummaryBE.Captured);
                param[index++] = new SqlParameter("@IsOpen", oPointsofContactSummaryBE.IsOpen);
                param[index++] = new SqlParameter("@IsAll", oPointsofContactSummaryBE.IsAll);
                param[index++] = new SqlParameter("@IsExcludeVendor", oPointsofContactSummaryBE.IsExcludeVendor);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPT_POCTrackerReport", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    PointsofContactSummaryBE oNewPointsofContactSummaryBE = new PointsofContactSummaryBE();
                    oNewPointsofContactSummaryBE.VendorID = dr["VendorID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["VendorID"]);
                    oNewPointsofContactSummaryBE.POCtrackerDetailID = dr["POCtrackerDetailID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["POCtrackerDetailID"]);
                    oNewPointsofContactSummaryBE.VendorName = dr["VendorName"] == DBNull.Value ? null : Convert.ToString(dr["VendorName"]);
                    oNewPointsofContactSummaryBE.Lines = dr["Lines"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Lines"]);
                    oNewPointsofContactSummaryBE.Weightage = dr["Weightage"] == DBNull.Value ? (double?)null : Convert.ToDouble(dr["Weightage"]);
                    if (dr.Table.Columns.Contains("SchedulingPOC"))
                        oNewPointsofContactSummaryBE.POCCountry = dr["SchedulingPOC"] == DBNull.Value ? 'N' : Convert.ToChar(dr["SchedulingPOC"]);
                    if (dr.Table.Columns.Contains("DiscrepancyPOC"))
                        oNewPointsofContactSummaryBE.POCCountry = dr["DiscrepancyPOC"] == DBNull.Value ? 'N' : Convert.ToChar(dr["DiscrepancyPOC"]);

                    oPointsofContactDetailBEList.Add(oNewPointsofContactSummaryBE);
                }

                myLists.Add("PointsofContactDetailBEList", oPointsofContactDetailBEList);


                dt = ds.Tables[1];

                foreach (DataRow dr in dt.Rows)
                {
                    PointsofContactSummaryBE oNewPointsofContactSummaryBE = new PointsofContactSummaryBE();
                    oNewPointsofContactSummaryBE.POCtrackerDetailID = dr["POCtrackerDetailID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["POCtrackerDetailID"]);
                    oNewPointsofContactSummaryBE.SiteName = dr["SiteName"] == DBNull.Value ? null : Convert.ToString(dr["SiteName"]);
                    if (dr.Table.Columns.Contains("SchedulingPOC"))
                        oNewPointsofContactSummaryBE.SchedulingPOC = dr["SchedulingPOC"] == DBNull.Value ? 'N' : Convert.ToChar(dr["SchedulingPOC"]);
                    else
                        oNewPointsofContactSummaryBE.SchedulingPOC = (char?)null;

                    if (dr.Table.Columns.Contains("DiscrepancyPOC"))
                        oNewPointsofContactSummaryBE.DiscrepancyPOC = dr["DiscrepancyPOC"] == DBNull.Value ? 'N' : Convert.ToChar(dr["DiscrepancyPOC"]);
                    else
                        oNewPointsofContactSummaryBE.DiscrepancyPOC = (char?)null;

                    oPointsofContactdetailSiteBEList.Add(oNewPointsofContactSummaryBE);
                }

                myLists.Add("PointsofContactdetailSiteBEList", oPointsofContactdetailSiteBEList);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return myLists;
        }


        public List<PointsofContactSummaryBE> GetVendorAPContactSummaryDetailsDAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            List<PointsofContactSummaryBE> oPointsofContactSummaryBEList = new List<PointsofContactSummaryBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oPointsofContactSummaryBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorSummaryAssignments", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    PointsofContactSummaryBE oNewPointsofContactSummaryBE = new PointsofContactSummaryBE();
                    oNewPointsofContactSummaryBE.CountryID = dr["CountryID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CountryID"]);
                    oNewPointsofContactSummaryBE.Country = dr["CountryName"] == DBNull.Value ? null : Convert.ToString(dr["CountryName"]);
                    oNewPointsofContactSummaryBE.NoofAPUser = dr["NoofAPuser"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NoofAPuser"]);
                    oNewPointsofContactSummaryBE.Weight = dr["Weight"] == DBNull.Value ? null : Convert.ToString(dr["Weight"]);
                    oPointsofContactSummaryBEList.Add(oNewPointsofContactSummaryBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oPointsofContactSummaryBEList;
        }


        public List<PointsofContactSummaryBE> GetVendorAPContactDetailsDAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            List<PointsofContactSummaryBE> oPointsofContactSummaryBEList = new List<PointsofContactSummaryBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oPointsofContactSummaryBE.Action);
                param[index++] = new SqlParameter("@CountryID", oPointsofContactSummaryBE.CountryID);
                param[index++] = new SqlParameter("@SCSelectedVendorIDs", oPointsofContactSummaryBE.SCSelectedVendorIDs);
                param[index++] = new SqlParameter("@IsAllContact", oPointsofContactSummaryBE.IsAllContact);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorSummaryAssignments", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    PointsofContactSummaryBE oNewPointsofContactSummaryBE = new PointsofContactSummaryBE();
                    oNewPointsofContactSummaryBE.CountryID = dr["CountryID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CountryID"]);
                    oNewPointsofContactSummaryBE.VendorName = dr["Vendor"] == DBNull.Value ? null : Convert.ToString(dr["Vendor"]);
                    oNewPointsofContactSummaryBE.VendorID = dr["VendorID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["VendorID"]);
                    oNewPointsofContactSummaryBE.NoOfLines = dr["Lines"] == DBNull.Value ? null : Convert.ToString(dr["Lines"]);
                    oNewPointsofContactSummaryBE.Weight = dr["Weight"] == DBNull.Value ? null : Convert.ToString(dr["Weight"]);
                    oNewPointsofContactSummaryBE.ContactName = dr["ContactName"] == DBNull.Value ? null : Convert.ToString(dr["ContactName"]);
                    if (dr.Table.Columns.Contains("Tel#"))
                    {

                        oNewPointsofContactSummaryBE.VendorPhoneNumber = dr["Tel#"] == DBNull.Value ? null : Convert.ToString(dr["Tel#"]);
                    }

                    oPointsofContactSummaryBEList.Add(oNewPointsofContactSummaryBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oPointsofContactSummaryBEList;
        }

        public int? SaveAPClerkDAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oPointsofContactSummaryBE.Action);
                param[index++] = new SqlParameter("@UserID", oPointsofContactSummaryBE.UserID);
                param[index++] = new SqlParameter("@VendorID", oPointsofContactSummaryBE.VendorID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorSummaryAssignments", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<PointsofContactSummaryBE> GetVendorSPSummaryDAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            List<PointsofContactSummaryBE> oPointsofContactSummaryBEList = new List<PointsofContactSummaryBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oPointsofContactSummaryBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorSummaryAssignments", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    PointsofContactSummaryBE oNewPointsofContactSummaryBE = new PointsofContactSummaryBE();
                    oNewPointsofContactSummaryBE.CountryID = dr["CountryID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CountryID"]);
                    oNewPointsofContactSummaryBE.Country = dr["CountryName"] == DBNull.Value ? null : Convert.ToString(dr["CountryName"]);
                    oNewPointsofContactSummaryBE.NoofSPUser = dr["NoOfSP"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NoOfSP"]);
                    oNewPointsofContactSummaryBE.Weight = dr["Weight"] == DBNull.Value ? null : Convert.ToString(dr["Weight"]);
                    oPointsofContactSummaryBEList.Add(oNewPointsofContactSummaryBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oPointsofContactSummaryBEList;
        }


        public List<PointsofContactSummaryBE> GetVendorSPDetailBAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            List<PointsofContactSummaryBE> oPointsofContactSummaryBEList = new List<PointsofContactSummaryBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oPointsofContactSummaryBE.Action);
                param[index++] = new SqlParameter("@CountryID", oPointsofContactSummaryBE.CountryID);
                param[index++] = new SqlParameter("@SCSelectedVendorIDs", oPointsofContactSummaryBE.SCSelectedVendorIDs);
                param[index++] = new SqlParameter("@IsAllContact", oPointsofContactSummaryBE.IsAllContact);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorSummaryAssignments", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    PointsofContactSummaryBE oNewPointsofContactSummaryBE = new PointsofContactSummaryBE();
                    oNewPointsofContactSummaryBE.CountryID = dr["CountryID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CountryID"]);
                    oNewPointsofContactSummaryBE.VendorName = dr["Vendor"] == DBNull.Value ? null : Convert.ToString(dr["Vendor"]);
                    oNewPointsofContactSummaryBE.VendorID = dr["VendorID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["VendorID"]);
                    oNewPointsofContactSummaryBE.NoOfLines = dr["Lines"] == DBNull.Value ? null : Convert.ToString(dr["Lines"]);
                    oNewPointsofContactSummaryBE.StockPlannerNo = dr["StockPlannerNo"] == DBNull.Value ? string.Empty : Convert.ToString(dr["StockPlannerNo"]);
                    oNewPointsofContactSummaryBE.Weight = dr["Weight"] == DBNull.Value ? null : Convert.ToString(dr["Weight"]);
                    oNewPointsofContactSummaryBE.ContactName = dr["ContactName"] == DBNull.Value ? null : Convert.ToString(dr["ContactName"]);
                    if (dr.Table.Columns.Contains("Tel#"))
                    {

                        oNewPointsofContactSummaryBE.VendorPhoneNumber = dr["Tel#"] == DBNull.Value ? null : Convert.ToString(dr["Tel#"]);
                    }

                    oPointsofContactSummaryBEList.Add(oNewPointsofContactSummaryBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oPointsofContactSummaryBEList;
        }

    }
}
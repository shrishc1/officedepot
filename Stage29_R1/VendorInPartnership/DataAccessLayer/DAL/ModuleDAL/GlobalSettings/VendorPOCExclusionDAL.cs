﻿using BusinessEntities.ModuleBE.GlobalSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.GlobalSettings
{
    public class VendorPOCExclusionDAL : BaseDAL
    {
        public int? SaveVendorPOCExclusionDAL(VendorPOCExclusionBE oVendorPOCExclusion)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oVendorPOCExclusion.Action);
                param[index++] = new SqlParameter("@CreatedBy", oVendorPOCExclusion.CreatedBy);
                param[index++] = new SqlParameter("@IsDeleted", oVendorPOCExclusion.IsDeleted);
                param[index++] = new SqlParameter("@ReasonForExclusion", oVendorPOCExclusion.ReasonForExclusion);
                param[index++] = new SqlParameter("@VendorID", oVendorPOCExclusion.VendorID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorPOCExclusion", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public List<VendorPOCExclusionBE> GetVendorPOCExclusionDAL(VendorPOCExclusionBE oVendorPOCExclusion)
        {
            List<VendorPOCExclusionBE> lstVendorPOCExclusion = new List<VendorPOCExclusionBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oVendorPOCExclusion.Action);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPOCExclusion", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    VendorPOCExclusionBE objVendorPOCExclusion = new VendorPOCExclusionBE();
                    objVendorPOCExclusion.VendorExclusionID = dr["VendorExclusionID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["VendorExclusionID"]);
                    objVendorPOCExclusion.VendorID = dr["VendorID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["VendorID"]);
                    objVendorPOCExclusion.VendorName = dr["VendorName"].ToString();
                    objVendorPOCExclusion.IsDeleted = dr["IsDeleted"] == DBNull.Value ? true : Convert.ToBoolean(dr["IsDeleted"]);
                    objVendorPOCExclusion.ReasonForExclusion = dr["ReasonForExclusion"] == DBNull.Value ? null : Convert.ToString(dr["ReasonForExclusion"]);
                    objVendorPOCExclusion.CountryName = dr["CountryName"] == DBNull.Value ? "" : Convert.ToString(dr["CountryName"]);
                    objVendorPOCExclusion.UserName = dr["UserName"] == DBNull.Value ? null : Convert.ToString(dr["UserName"]);
                    lstVendorPOCExclusion.Add(objVendorPOCExclusion);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstVendorPOCExclusion;
        }

        public VendorPOCExclusionBE GetEditVendorPOCExclusionDAL(VendorPOCExclusionBE oVendorPOCExclusion)
        {
            VendorPOCExclusionBE objVendorPOCExclusion = new VendorPOCExclusionBE();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oVendorPOCExclusion.Action);
                param[index++] = new SqlParameter("@VendorExclusionID", oVendorPOCExclusion.VendorExclusionID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPOCExclusion", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    objVendorPOCExclusion.VendorExclusionID = dr["VendorExclusionID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["VendorExclusionID"]);
                    objVendorPOCExclusion.VendorID = dr["VendorID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["VendorID"]);
                    objVendorPOCExclusion.VendorName = dr["VendorName"].ToString();
                    objVendorPOCExclusion.VendorNo = dr["Vendor_No"].ToString();
                    objVendorPOCExclusion.IsDeleted = dr["IsDeleted"] == DBNull.Value ? true : Convert.ToBoolean(dr["IsDeleted"]);
                    objVendorPOCExclusion.ReasonForExclusion = dr["ReasonForExclusion"] == DBNull.Value ? null : Convert.ToString(dr["ReasonForExclusion"]);
                    objVendorPOCExclusion.CountryName = dr["CountryName"] == DBNull.Value ? "" : Convert.ToString(dr["CountryName"]);
                    objVendorPOCExclusion.UserName = dr["UserName"] == DBNull.Value ? null : Convert.ToString(dr["UserName"]);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return objVendorPOCExclusion;
        }
        public int? DeleteVendorPOCExclusionDAL(VendorPOCExclusionBE oVendorPOCExclusion)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oVendorPOCExclusion.Action);
                param[index++] = new SqlParameter("@CreatedBy", oVendorPOCExclusion.CreatedBy);
                param[index++] = new SqlParameter("@IsDeleted", oVendorPOCExclusion.IsDeleted);
                param[index++] = new SqlParameter("@VendorExclusionID", oVendorPOCExclusion.VendorExclusionID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorPOCExclusion", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

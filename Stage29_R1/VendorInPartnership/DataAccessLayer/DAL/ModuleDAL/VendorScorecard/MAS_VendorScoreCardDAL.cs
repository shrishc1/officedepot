﻿using BusinessEntities.ModuleBE.VendorScorecard;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.VendorScorecard
{
    public class MAS_VendorScoreCardDAL : BaseDAL
    {
        public int? UpdateVendorScoreCardDAL(MAS_VendorScoreCardBE objVendorScoreCardBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[123];

                param[index++] = new SqlParameter("@Action", objVendorScoreCardBE.Action);
                //Overall Weight Property
                param[index++] = new SqlParameter("@OtifWeight", objVendorScoreCardBE.OtifWeight);
                param[index++] = new SqlParameter("@SchedulingWeight", objVendorScoreCardBE.SchedulingWeight);
                param[index++] = new SqlParameter("@DiscrepenciesWeight", objVendorScoreCardBE.DiscrepenciesWeight);
                param[index++] = new SqlParameter("@StockWeight", objVendorScoreCardBE.StockWeight);

                //Overall Score Property
                param[index++] = new SqlParameter("@FailureScore", objVendorScoreCardBE.FailureScore);
                param[index++] = new SqlParameter("@TargetScore", objVendorScoreCardBE.TargetScore);
                param[index++] = new SqlParameter("@IdealScore", objVendorScoreCardBE.IdealScore);

                //OTIF Weight and Targets Property             

                param[index++] = new SqlParameter("@OtifIdeal", objVendorScoreCardBE.OtifIdeal);
                param[index++] = new SqlParameter("@OtifTarget", objVendorScoreCardBE.OtifTarget);
                param[index++] = new SqlParameter("@OtifFailure", objVendorScoreCardBE.OtifFailure);


                //Discrepancies Weight and Targets Property
                param[index++] = new SqlParameter("@DiscWeight", objVendorScoreCardBE.DiscWeight);
                param[index++] = new SqlParameter("@AvgWeight", objVendorScoreCardBE.AvgWeight);

                param[index++] = new SqlParameter("@DiscIdeal", objVendorScoreCardBE.DiscIdeal);
                param[index++] = new SqlParameter("@AvgIdeal", objVendorScoreCardBE.AvgIdeal);

                param[index++] = new SqlParameter("@DiscTarget", objVendorScoreCardBE.DiscTarget);
                param[index++] = new SqlParameter("@AvgTarget", objVendorScoreCardBE.AvgTarget);

                param[index++] = new SqlParameter("@DiscFailure", objVendorScoreCardBE.DiscFailure);
                param[index++] = new SqlParameter("@AvgFailure", objVendorScoreCardBE.AvgFailure);

                //Scheduling Weight and Targets Property

                param[index++] = new SqlParameter("@SchedulingIdeal", objVendorScoreCardBE.SchedulingIdeal);
                param[index++] = new SqlParameter("@SchedulingTarget", objVendorScoreCardBE.SchedulingTarget);
                param[index++] = new SqlParameter("@SchedulingFailure", objVendorScoreCardBE.SchedulingFailure);


                //Stock Weight and Targets Property
                param[index++] = new SqlParameter("@AvgBackorderDaysWeight", objVendorScoreCardBE.AvgBackorderDaysWeight);
                param[index++] = new SqlParameter("@LeadTimeVarianceWeight", objVendorScoreCardBE.LeadTimeVarianceWeight);

                param[index++] = new SqlParameter("@AvgBackorderDaysIdeal", objVendorScoreCardBE.AvgBackorderDaysIdeal);
                param[index++] = new SqlParameter("@LeadTimeVarianceIdeal", objVendorScoreCardBE.LeadTimeVarianceIdeal);

                param[index++] = new SqlParameter("@AvgBackorderDaysTargetPositive", objVendorScoreCardBE.AvgBackorderDaysTargetPositive);
                param[index++] = new SqlParameter("@LeadTimeVarianceTargetPositive", objVendorScoreCardBE.LeadTimeVarianceTargetPositive);

                param[index++] = new SqlParameter("@AvgBackorderDaysTargetNegative", objVendorScoreCardBE.AvgBackorderDaysTargetNegative);
                param[index++] = new SqlParameter("@LeadTimeVarianceTargetNegative", objVendorScoreCardBE.LeadTimeVarianceTargetNegative);

                param[index++] = new SqlParameter("@AvgBackorderDaysFailurePositive", objVendorScoreCardBE.AvgBackorderDaysFailurePositive);
                param[index++] = new SqlParameter("@LeadTimeVarianceFailurePositive", objVendorScoreCardBE.LeadTimeVarianceFailurePositive);

                param[index++] = new SqlParameter("@AvgBackorderDaysFailureNegative", objVendorScoreCardBE.AvgBackorderDaysFailureNegative);
                param[index++] = new SqlParameter("@LeadTimeVarianceFailureNegative", objVendorScoreCardBE.LeadTimeVarianceFailureNegative);

                param[index++] = new SqlParameter("@AbsoluteFail", objVendorScoreCardBE.AbsoluteFail);
                param[index++] = new SqlParameter("@OtifAbsoluteFail", objVendorScoreCardBE.OtifAbsoluteFail);
                param[index++] = new SqlParameter("@DiscAbsoluteFail", objVendorScoreCardBE.DiscAbsoluteFail);
                param[index++] = new SqlParameter("@AvgAbsoluteFail", objVendorScoreCardBE.AvgAbsoluteFail);
                param[index++] = new SqlParameter("@BackorderDaysAbsoluteFail", objVendorScoreCardBE.BackorderDaysAbsoluteFail);
                param[index++] = new SqlParameter("@LeadTimeVarianceAbsoluteFail", objVendorScoreCardBE.LeadTimeVarianceAbsoluteFail);
                param[index++] = new SqlParameter("@SchedulingAbsoluteFail", objVendorScoreCardBE.SchedulingAbsoluteFail);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorScoreCard", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? AddEditFrequencyDAL(MAS_VendorScoreCardBE objVendorScoreCardBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", objVendorScoreCardBE.Action);
                param[index++] = new SqlParameter("@CountryID", objVendorScoreCardBE.CountryID);
                param[index++] = new SqlParameter("@SelectedVendorIDs", objVendorScoreCardBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@Frequency", objVendorScoreCardBE.Frequency);

                param[index++] = new SqlParameter("@VendorCountryFreqID", objVendorScoreCardBE.VendorCountryFreqID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorScoreCard", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteVendorFreqDAL(MAS_VendorScoreCardBE objVendorScoreCardBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", objVendorScoreCardBE.Action);
                param[index++] = new SqlParameter("@VendorCountryFreqID", objVendorScoreCardBE.VendorCountryFreqID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorScoreCard", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public MAS_VendorScoreCardBE GetVendorScoreCardDAL(string Action)
        {
            MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
            int index = 0;
            DataTable dtOverallWeight = new DataTable();
            DataTable dtOverallScore = new DataTable();
            DataTable dtOTIFWeight = new DataTable();
            DataTable dtDiscrepanciesWeight = new DataTable();
            DataTable dtStockWeight = new DataTable();
            DataTable dtSchedulingWeight = new DataTable();

            SqlParameter[] param = new SqlParameter[1];
            param[index++] = new SqlParameter("@Action", Action);
            DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorScoreCard", param);

            dtOverallWeight = ds.Tables[0];
            if (dtOverallWeight.Rows.Count > 0)
            {
                foreach (DataRow dr in dtOverallWeight.Rows)
                {
                    objVendorScoreCardBE.OtifWeight = dr["OtifWeight"] != DBNull.Value ? Convert.ToInt32(dr["OtifWeight"]) : (int?)null;
                    objVendorScoreCardBE.SchedulingWeight = dr["SchedulingWeight"] != DBNull.Value ? Convert.ToInt32(dr["SchedulingWeight"]) : (int?)null;
                    objVendorScoreCardBE.DiscrepenciesWeight = dr["DiscrepenciesWeight"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepenciesWeight"]) : (int?)null;
                    objVendorScoreCardBE.StockWeight = dr["StockWeight"] != DBNull.Value ? Convert.ToInt32(dr["StockWeight"]) : (int?)null;
                }
            }
            dtOverallScore = ds.Tables[1];
            if (dtOverallScore.Rows.Count > 0)
            {
                foreach (DataRow dr in dtOverallScore.Rows)
                {
                    objVendorScoreCardBE.FailureScore = dr["FailureScore"] != DBNull.Value ? Convert.ToDecimal(dr["FailureScore"]) : (decimal?)null;
                    objVendorScoreCardBE.TargetScore = dr["TargetScore"] != DBNull.Value ? Convert.ToDecimal(dr["TargetScore"]) : (decimal?)null;
                    objVendorScoreCardBE.IdealScore = dr["IdealScore"] != DBNull.Value ? Convert.ToDecimal(dr["IdealScore"]) : (decimal?)null;
                    objVendorScoreCardBE.AbsoluteFail = dr["AbsoluteFail"] != DBNull.Value ? Convert.ToDecimal(dr["AbsoluteFail"]) : (decimal?)null;
                }
            }
            dtOTIFWeight = ds.Tables[2];
            if (dtOTIFWeight.Rows.Count > 0)
            {
                foreach (DataRow dr in dtOTIFWeight.Rows)
                {
                    objVendorScoreCardBE.OtifIdeal = dr["OtifIdeal"] != DBNull.Value ? Convert.ToDecimal(dr["OtifIdeal"]) : (decimal?)null;
                    objVendorScoreCardBE.OtifTarget = dr["OtifTarget"] != DBNull.Value ? Convert.ToDecimal(dr["OtifTarget"]) : (decimal?)null;
                    objVendorScoreCardBE.OtifFailure = dr["OtifFailure"] != DBNull.Value ? Convert.ToDecimal(dr["OtifFailure"]) : (decimal?)null;
                    objVendorScoreCardBE.OtifAbsoluteFail = dr["OtifAbsoluteFail"] != DBNull.Value ? Convert.ToDecimal(dr["OtifAbsoluteFail"]) : (decimal?)null;
                }
            }
            dtDiscrepanciesWeight = ds.Tables[3];
            if (dtDiscrepanciesWeight.Rows.Count > 0)
            {
                foreach (DataRow dr in dtDiscrepanciesWeight.Rows)
                {
                    objVendorScoreCardBE.DiscWeight = dr["DiscWeight"] != DBNull.Value ? Convert.ToDecimal(dr["DiscWeight"]) : (decimal?)null;
                    objVendorScoreCardBE.AvgWeight = dr["AvgWeight"] != DBNull.Value ? Convert.ToDecimal(dr["AvgWeight"]) : (decimal?)null;
                    objVendorScoreCardBE.DiscIdeal = dr["DiscIdeal"] != DBNull.Value ? Convert.ToDecimal(dr["DiscIdeal"]) : (decimal?)null;
                    objVendorScoreCardBE.AvgIdeal = dr["AvgIdeal"] != DBNull.Value ? Convert.ToDecimal(dr["AvgIdeal"]) : (decimal?)null;
                    objVendorScoreCardBE.DiscTarget = dr["DiscTarget"] != DBNull.Value ? Convert.ToDecimal(dr["DiscTarget"]) : (decimal?)null;
                    objVendorScoreCardBE.AvgTarget = dr["AvgTarget"] != DBNull.Value ? Convert.ToDecimal(dr["AvgTarget"]) : (decimal?)null;
                    objVendorScoreCardBE.DiscFailure = dr["DiscFailure"] != DBNull.Value ? Convert.ToDecimal(dr["DiscFailure"]) : (decimal?)null;
                    objVendorScoreCardBE.AvgFailure = dr["AvgFailure"] != DBNull.Value ? Convert.ToDecimal(dr["AvgFailure"]) : (decimal?)null;

                    objVendorScoreCardBE.DiscAbsoluteFail = dr["DiscAbsoluteFail"] != DBNull.Value ? Convert.ToDecimal(dr["DiscAbsoluteFail"]) : (decimal?)null;
                    objVendorScoreCardBE.AvgAbsoluteFail = dr["AvgAbsoluteFail"] != DBNull.Value ? Convert.ToDecimal(dr["AvgAbsoluteFail"]) : (decimal?)null;
                }
            }
            dtStockWeight = ds.Tables[4];
            if (dtStockWeight.Rows.Count > 0)
            {
                foreach (DataRow dr in dtStockWeight.Rows)
                {
                    objVendorScoreCardBE.AvgBackorderDaysWeight = dr["AvgBackorderDaysWeight"] != DBNull.Value ? Convert.ToDecimal(dr["AvgBackorderDaysWeight"]) : (decimal?)null;
                    objVendorScoreCardBE.LeadTimeVarianceWeight = dr["LeadTimeVarianceWeight"] != DBNull.Value ? Convert.ToDecimal(dr["LeadTimeVarianceWeight"]) : (decimal?)null;
                    objVendorScoreCardBE.AvgBackorderDaysIdeal = dr["AvgBackorderDaysIdeal"] != DBNull.Value ? Convert.ToDecimal(dr["AvgBackorderDaysIdeal"]) : (decimal?)null;
                    objVendorScoreCardBE.LeadTimeVarianceIdeal = dr["LeadTimeVarianceIdeal"] != DBNull.Value ? Convert.ToDecimal(dr["LeadTimeVarianceIdeal"]) : (decimal?)null;
                    objVendorScoreCardBE.AvgBackorderDaysTargetPositive = dr["AvgBackorderDaysTargetPos"] != DBNull.Value ? Convert.ToDecimal(dr["AvgBackorderDaysTargetPos"]) : (decimal?)null;
                    objVendorScoreCardBE.LeadTimeVarianceTargetPositive = dr["LeadTimeVarianceTargetPos"] != DBNull.Value ? Convert.ToDecimal(dr["LeadTimeVarianceTargetPos"]) : (decimal?)null;
                    //objVendorScoreCardBE.AvgBackorderDaysTargetNegative = dr["AvgBackorderDaysTargetNeg"] != DBNull.Value ? Convert.ToDecimal(dr["AvgBackorderDaysTargetNeg"]) : (decimal?)null;
                    //objVendorScoreCardBE.LeadTimeVarianceTargetNegative = dr["LeadTimeVarianceTargetNeg"] != DBNull.Value ? Convert.ToDecimal(dr["LeadTimeVarianceTargetNeg"]) : (decimal?)null;
                    objVendorScoreCardBE.AvgBackorderDaysFailurePositive = dr["AvgBackorderDaysFailurePos"] != DBNull.Value ? Convert.ToDecimal(dr["AvgBackorderDaysFailurePos"]) : (decimal?)null;
                    objVendorScoreCardBE.LeadTimeVarianceFailurePositive = dr["LeadTimeVarianceFailurePos"] != DBNull.Value ? Convert.ToDecimal(dr["LeadTimeVarianceFailurePos"]) : (decimal?)null;
                    //objVendorScoreCardBE.AvgBackorderDaysFailureNegative = dr["AvgBackorderDaysFailureNeg"] != DBNull.Value ? Convert.ToDecimal(dr["AvgBackorderDaysFailureNeg"]) : (decimal?)null;
                    //objVendorScoreCardBE.LeadTimeVarianceFailureNegative = dr["LeadTimeVarianceFailureNeg"] != DBNull.Value ? Convert.ToDecimal(dr["LeadTimeVarianceFailureNeg"]) : (decimal?)null;
                    objVendorScoreCardBE.BackorderDaysAbsoluteFail = dr["BackorderDaysAbsoluteFail"] != DBNull.Value ? Convert.ToDecimal(dr["BackorderDaysAbsoluteFail"]) : (decimal?)null;
                    objVendorScoreCardBE.LeadTimeVarianceAbsoluteFail = dr["LeadTimeVarianceAbsoluteFail"] != DBNull.Value ? Convert.ToDecimal(dr["LeadTimeVarianceAbsoluteFail"]) : (decimal?)null;


                }
            }
            dtSchedulingWeight = ds.Tables[5];
            if (dtSchedulingWeight.Rows.Count > 0)
            {
                objVendorScoreCardBE.SchedulingIdeal = dtSchedulingWeight.Rows[0]["SchedulingIdeal"] != DBNull.Value ? Convert.ToDecimal(dtSchedulingWeight.Rows[0]["SchedulingIdeal"]) : (decimal?)null;
                objVendorScoreCardBE.SchedulingTarget = dtSchedulingWeight.Rows[0]["SchedulingTarget"] != DBNull.Value ? Convert.ToDecimal(dtSchedulingWeight.Rows[0]["SchedulingTarget"]) : (decimal?)null;
                objVendorScoreCardBE.SchedulingFailure = dtSchedulingWeight.Rows[0]["SchedulingFailure"] != DBNull.Value ? Convert.ToDecimal(dtSchedulingWeight.Rows[0]["SchedulingFailure"]) : (decimal?)null;
                objVendorScoreCardBE.SchedulingAbsoluteFail = dtSchedulingWeight.Rows[0]["SchedulingAbsoluteFail"] != DBNull.Value ? Convert.ToDecimal(dtSchedulingWeight.Rows[0]["SchedulingAbsoluteFail"]) : (decimal?)null;
            }
            return objVendorScoreCardBE;
        }

        public MAS_VendorScoreCardBE GetVendorFreqDAL(MAS_VendorScoreCardBE objVendorScoreCardBE)
        {
            MAS_VendorScoreCardBE objRetVendorScoreCardBE = new MAS_VendorScoreCardBE();
            int index = 0;
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[2];
            param[index++] = new SqlParameter("@Action", objVendorScoreCardBE.Action);
            param[index++] = new SqlParameter("@VendorCountryFreqID", objVendorScoreCardBE.VendorCountryFreqID);

            DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorScoreCard", param);

            dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    objRetVendorScoreCardBE.VendorName = dr["VendorName"] != DBNull.Value ? (dr["VendorName"]).ToString() : "";
                    objRetVendorScoreCardBE.CountryName = dr["CountryName"] != DBNull.Value ? (dr["CountryName"]).ToString() : "";
                    objRetVendorScoreCardBE.Frequency = dr["Frequency"] != DBNull.Value ? Convert.ToChar(dr["Frequency"]) : 'N';
                }
            }

            return objRetVendorScoreCardBE;
        }

        public List<MAS_VendorScoreCardBE> GetVendorByCountryDAL(MAS_VendorScoreCardBE objVendorScoreCardBE)
        {
            DataTable dt = new DataTable();
            List<MAS_VendorScoreCardBE> objListVendorScoreCardBE = new List<MAS_VendorScoreCardBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", objVendorScoreCardBE.Action);
                param[index++] = new SqlParameter("@CountryID", objVendorScoreCardBE.CountryID);
                param[index++] = new SqlParameter("@VendorName", objVendorScoreCardBE.VendorName);
                param[index++] = new SqlParameter("@Vendor_No_nvarchar", objVendorScoreCardBE.VendorNo);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorScoreCard", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        MAS_VendorScoreCardBE oNew_VendorScoreCardBE = new MAS_VendorScoreCardBE();
                        oNew_VendorScoreCardBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : (int?)null;
                        oNew_VendorScoreCardBE.VendorName = dr["VendorName"] != DBNull.Value ? (dr["VendorName"]).ToString() : "";
                        oNew_VendorScoreCardBE.IsActiveVendor = dr["IsActiveVendor"] == DBNull.Value ? null : Convert.ToString(dr["IsActiveVendor"]); /// Added on 4 march 2013
                        oNew_VendorScoreCardBE.CountryID = dr["CountryID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CountryID"]);
                        objListVendorScoreCardBE.Add(oNew_VendorScoreCardBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return objListVendorScoreCardBE;
        }

        public List<MAS_VendorScoreCardBE> GetALLVendorFreqDAL(MAS_VendorScoreCardBE objVendorScoreCardBE)
        {
            DataTable dt = new DataTable();
            List<MAS_VendorScoreCardBE> objListVendorFreqBE = new List<MAS_VendorScoreCardBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", objVendorScoreCardBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorScoreCard", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        MAS_VendorScoreCardBE oNew_VendorScoreCardBE = new MAS_VendorScoreCardBE();
                        oNew_VendorScoreCardBE.VendorCountryFreqID = dr["VendorCountryFreqID"] != DBNull.Value ? Convert.ToInt32(dr["VendorCountryFreqID"]) : (int?)null;
                        oNew_VendorScoreCardBE.VendorName = dr["VendorName"] != DBNull.Value ? (dr["VendorName"]).ToString() : "";
                        oNew_VendorScoreCardBE.CountryName = dr["CountryName"] != DBNull.Value ? (dr["CountryName"]).ToString() : "";
                        oNew_VendorScoreCardBE.gridFrequency = dr["gridFrequency"] != DBNull.Value ? (dr["gridFrequency"]).ToString() : "";
                        objListVendorFreqBE.Add(oNew_VendorScoreCardBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return objListVendorFreqBE;
        }

        public void ExcecuteScorecardDAL()
        {
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[1];
                param[index] = new SqlParameter("@Date", DateTime.Today.AddMonths(-1));
                SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spScoreCardSchedular", param);

                param[index] = new SqlParameter("@Date", DateTime.Today.AddMonths(-2));
                SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spScoreCardSchedular", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }


        }

        public DataTable ScoreCardVendorDetailsDAL(MAS_VendorScoreCardBE objVendorScoreCardBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@VendorID", objVendorScoreCardBE.VendorID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "USP_ScorecardVendorDetails", param);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }
    }
}

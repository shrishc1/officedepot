﻿using BusinessEntities.ModuleBE.StockOverview;
using BusinessEntities.ModuleBE.Upload;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.StockOverview
{
    public class ExpediteStockDAL
    {
        public DataTable GetAllReasonTypeDetailsDAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                param[index++] = new SqlParameter("@Reason", oMAS_ExpStockBE.Reason);
                param[index++] = new SqlParameter("@IsCommentRequired", oMAS_ExpStockBE.IsCommentRequired);
                param[index++] = new SqlParameter("@CountryID", oMAS_ExpStockBE.CountryID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASCNT_ReasonTypeSetup", param);

                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public List<ExpediteStockBE> GetAllReasonTypesDAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();
            List<ExpediteStockBE> oMAS_ExpediteBEList = new List<ExpediteStockBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(),
                    CommandType.StoredProcedure, "spMASCNT_ReasonTypeSetup", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    ExpediteStockBE oNew_ExpediteStockBE = new ExpediteStockBE();
                    oNew_ExpediteStockBE.ReasonTypeID = Convert.ToInt32(dr["ReasonTypeID"]);
                    oNew_ExpediteStockBE.Reason = Convert.ToString(dr["Reason"]);
                    oNew_ExpediteStockBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    oNew_ExpediteStockBE.Country.CountryName = Convert.ToString(dr["CountryName"]);
                    oNew_ExpediteStockBE.CountryID = Convert.ToInt32(dr["CountryID"]);
                    oNew_ExpediteStockBE.UpdateCommentRequired = Convert.ToString(dr["IsCommentRequired"]);
                    oNew_ExpediteStockBE.IsActiveType = Convert.ToString(dr["IsActiveType"]);
                    oMAS_ExpediteBEList.Add(oNew_ExpediteStockBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_ExpediteBEList;
        }

        public int? addEditReasonTypeSetupDAL(ExpediteStockBE oMAS_ExpediteBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMAS_ExpediteBE.Action);
                if (oMAS_ExpediteBE.ReasonTypeID != null)
                {
                    param[index++] = new SqlParameter("@ReasonTypeID", oMAS_ExpediteBE.ReasonTypeID);
                }

                param[index++] = new SqlParameter("@CountryID", oMAS_ExpediteBE.CountryID);
                param[index++] = new SqlParameter("@Reason", oMAS_ExpediteBE.Reason);
                param[index++] = new SqlParameter("@IsCommentRequired", oMAS_ExpediteBE.IsCommentRequired);
                param[index++] = new SqlParameter("@IsActice", oMAS_ExpediteBE.IsActive);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASCNT_ReasonTypeSetup", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<ExpediteStockBE> GetExpediteDetailsDAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();
            List<ExpediteStockBE> oMASCNT_ExpediteBEList = new List<ExpediteStockBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                param[index++] = new SqlParameter("@ReasonTypeID", oMAS_ExpStockBE.ReasonTypeID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASCNT_ReasonTypeSetup", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    ExpediteStockBE oNewMASCNT_ExpediteBE = new ExpediteStockBE();
                    oNewMASCNT_ExpediteBE.ReasonTypeID = Convert.ToInt32(dr["ReasonTypeID"]);
                    oNewMASCNT_ExpediteBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    oNewMASCNT_ExpediteBE.Country.CountryName = dr["CountryName"].ToString();
                    oNewMASCNT_ExpediteBE.Reason = dr["Reason"].ToString();
                    oNewMASCNT_ExpediteBE.CountryID = Convert.ToInt32(dr["CountryID"]);
                    oNewMASCNT_ExpediteBE.UpdateCommentRequired = dr["IsCommentRequired"].ToString();
                    oNewMASCNT_ExpediteBE.IsActive = Convert.ToInt32(dr["IsActive"].ToString());

                    oMASCNT_ExpediteBEList.Add(oNewMASCNT_ExpediteBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASCNT_ExpediteBEList;
        }

        public int? SaveExpediteCommentsDAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();
            //oMAS_ExpStockBE.PurchaseOrder = new Up_PurchaseOrderDetailBE();
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[13];
                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                param[index++] = new SqlParameter("@SKUID", oMAS_ExpStockBE.PurchaseOrder.SKUID);
                param[index++] = new SqlParameter("@ReasonTypeID", oMAS_ExpStockBE.ReasonTypeID);
                param[index++] = new SqlParameter("@Comments", oMAS_ExpStockBE.Comments);
                param[index++] = new SqlParameter("@CommentsValidDate", oMAS_ExpStockBE.CommentsValid_Date);
                param[index++] = new SqlParameter("@CommentsAddedDate", oMAS_ExpStockBE.CommentsAdded_Date);
                param[index++] = new SqlParameter("@CommentsAddedBy", oMAS_ExpStockBE.PurchaseOrder.UserID);
                param[index++] = new SqlParameter("@IsExpediteReview", oMAS_ExpStockBE.IsExpediteReview);
                param[index++] = new SqlParameter("@SiteID", oMAS_ExpStockBE.SiteID);
                param[index++] = new SqlParameter("@VendorID", oMAS_ExpStockBE.VendorID);
                param[index++] = new SqlParameter("@VikingCode", oMAS_ExpStockBE.Viking_Code);
                param[index++] = new SqlParameter("@ODCode", oMAS_ExpStockBE.OD_Code);
                param[index++] = new SqlParameter("@CommentID", oMAS_ExpStockBE.CommentID);
                DataSet Result;
                if (oMAS_ExpStockBE.Isbotop20 == true)
                {
                    Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "Boreporting_Top20", param);
                }
                else
                { Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_ExpediteComments", param); }



                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<ExpediteStockBE> GetExpediteCommentsDAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();
            List<ExpediteStockBE> oMASCNT_ExpediteBEList = new List<ExpediteStockBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                param[index++] = new SqlParameter("@SKUID", oMAS_ExpStockBE.PurchaseOrder.SKUID);
                param[index++] = new SqlParameter("@VendorID", oMAS_ExpStockBE.VendorID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_ExpediteComments", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    ExpediteStockBE oNewMASCNT_ExpediteBE = new ExpediteStockBE();
                    oNewMASCNT_ExpediteBE.Comments = Convert.ToString(dr["Comments"]);
                    oNewMASCNT_ExpediteBE.CommentsAddedBy = dr["CommentBy"].ToString();
                    oNewMASCNT_ExpediteBE.Reason = dr["Reason"].ToString();
                    oNewMASCNT_ExpediteBE.CommentsAdded_Date = dr["CommentsAdded_Date"] != DBNull.Value ? Convert.ToDateTime(dr["CommentsAdded_Date"]) : (DateTime?)null;
                    oNewMASCNT_ExpediteBE.CommentsValid_Date = dr["CommentsValid_Date"] != DBNull.Value ? Convert.ToDateTime(dr["CommentsValid_Date"]) : (DateTime?)null;
                    oNewMASCNT_ExpediteBE.IsExpediteReview = Convert.ToBoolean(dr["IsExpediteReview"]);
                    oNewMASCNT_ExpediteBE.ReasonTypeID = Convert.ToInt32(dr["ReasonTypeID"]);
                    oNewMASCNT_ExpediteBE.CommentID = Convert.ToInt32(dr["CommentID"]);
                    oNewMASCNT_ExpediteBE.LastEdited = dr["LastEdited"] != DBNull.Value ? Convert.ToDateTime(dr["LastEdited"]) : (DateTime?)null;
                    oMASCNT_ExpediteBEList.Add(oNewMASCNT_ExpediteBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASCNT_ExpediteBEList;
        }

        public DataTable GetAllReasonDAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                param[index++] = new SqlParameter("@SiteId", oMAS_ExpStockBE.SiteID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_ExpediteComments", param);

                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public DataTable ViewHistoricCommntsDAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                param[index++] = new SqlParameter("@CommentsAddedBy", oMAS_ExpStockBE.PurchaseOrder.UserID);
                param[index++] = new SqlParameter("@SKUID", oMAS_ExpStockBE.PurchaseOrder.SKUID);
                param[index++] = new SqlParameter("@VendorID", oMAS_ExpStockBE.VendorID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_ExpediteComments", param);

                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public List<ExpediteStockBE> GetPotentialOutputDAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            var dt = new DataTable();
            var oMAS_ExpediteBEList = new List<ExpediteStockBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[24];
                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oMAS_ExpStockBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedIncludedVendorIDs", oMAS_ExpStockBE.SelectedIncludedVendorIDs);
                param[index++] = new SqlParameter("@SelectedExcludedVendorIDs", oMAS_ExpStockBE.SelectedExcludedVendorIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oMAS_ExpStockBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedItemClassification", oMAS_ExpStockBE.SelectedItemClassification);
                param[index++] = new SqlParameter("@SelectedMRPType", oMAS_ExpStockBE.SelectedMRPType);
                param[index++] = new SqlParameter("@SelectedPurcGroup", oMAS_ExpStockBE.SelectedPurcGroup);
                param[index++] = new SqlParameter("@SelectedMatGrp", oMAS_ExpStockBE.SelectedMatGrp);
                param[index++] = new SqlParameter("@SelectedVIKINGSKU", oMAS_ExpStockBE.SelectedVIKINGSKU);
                param[index++] = new SqlParameter("@SelectedODSKU", oMAS_ExpStockBE.SelectedODSKU);
                param[index++] = new SqlParameter("@SelectedSkuType", oMAS_ExpStockBE.SelectedSkuType);
                param[index++] = new SqlParameter("@SelectedDisplay", oMAS_ExpStockBE.SelectedDisplay);
                param[index++] = new SqlParameter("@Page", oMAS_ExpStockBE.Page);
                param[index++] = new SqlParameter("@SortBy", oMAS_ExpStockBE.SortBy);
                param[index++] = new SqlParameter("@POStatusSearchCriteria", oMAS_ExpStockBE.POStatusSearchCriteria);
                param[index++] = new SqlParameter("@CommentStatus", oMAS_ExpStockBE.CommentStatus);
                param[index++] = new SqlParameter("@DaysStock", oMAS_ExpStockBE.DaysStock);
                param[index++] = new SqlParameter("@MinForecastedSales", oMAS_ExpStockBE.MinForecastedSales);
                param[index++] = new SqlParameter("@SortingDay", oMAS_ExpStockBE.SortingDay);
                param[index++] = new SqlParameter("@StockPlannerGroupingID", oMAS_ExpStockBE.StockPlannerGroupingID);
                param[index++] = new SqlParameter("@SkugroupingID", oMAS_ExpStockBE.SkugroupingID);
                param[index++] = new SqlParameter("@IsPriority", oMAS_ExpStockBE.IsPriority);
                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spExpediteStock", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    if (oMAS_ExpStockBE.Action == "GetPotentialOutput")
                    {
                        var oNew_ExpediteStockBE = new ExpediteStockBE();
                        oNew_ExpediteStockBE.Comment = dr["Comment"] != DBNull.Value ? Convert.ToString(dr["Comment"]) : string.Empty;

                        oNew_ExpediteStockBE.Comments = dr["Comments"] != DBNull.Value ? Convert.ToString(dr["Comments"]) : string.Empty;
                        oNew_ExpediteStockBE.CommentsAdded_Date = dr["CommentsAdded_Date"] != DBNull.Value ? Convert.ToDateTime(dr["CommentsAdded_Date"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.Reason = dr["Reason"] != DBNull.Value ? Convert.ToString(dr["Reason"]) : string.Empty;
                        oNew_ExpediteStockBE.CommentsValid_Date = dr["CommentsValid_Date"] != DBNull.Value ? Convert.ToDateTime(dr["CommentsValid_Date"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.CommentsAddedBy = dr["CommentBy"] != DBNull.Value ? Convert.ToString(dr["CommentBy"]) : string.Empty;

                        oNew_ExpediteStockBE.Status = dr["Status"] != DBNull.Value ? Convert.ToString(dr["Status"]) : string.Empty;
                        oNew_ExpediteStockBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNew_ExpediteStockBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                        oNew_ExpediteStockBE.Vendor.Vendor_No = dr["VendorNo"] != DBNull.Value ? Convert.ToString(dr["VendorNo"]) : string.Empty;
                        oNew_ExpediteStockBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                        oNew_ExpediteStockBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                        oNew_ExpediteStockBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                        oNew_ExpediteStockBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? Convert.ToString(dr["SiteName"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                        oNew_ExpediteStockBE.PurchaseOrder.OD_Code = dr["ODCode"] != DBNull.Value ? Convert.ToString(dr["ODCode"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder.Direct_code = dr["VikingCode"] != DBNull.Value ? Convert.ToString(dr["VikingCode"]) : string.Empty;
                        oNew_ExpediteStockBE.POStatus = dr["POStatus"] != DBNull.Value ? Convert.ToString(dr["POStatus"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder.Product_description = dr["Description"] != DBNull.Value ? Convert.ToString(dr["Description"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder.StockPlannerNo = dr["StockPlannerNo"] != DBNull.Value ? Convert.ToString(dr["StockPlannerNo"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder.StockPlannerName = dr["StockPlanner"] != DBNull.Value ? Convert.ToString(dr["StockPlanner"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder.Vendor_Code = dr["VendorCode"] != DBNull.Value ? Convert.ToString(dr["VendorCode"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder.Item_classification = dr["ItemClassification"] != DBNull.Value ? Convert.ToString(dr["ItemClassification"]) : string.Empty;
                        oNew_ExpediteStockBE.EarliestPO = dr["EarliastPO"] != DBNull.Value ? Convert.ToString(dr["EarliastPO"]) : string.Empty;
                        oNew_ExpediteStockBE.DueDate = dr["DueDate"] != DBNull.Value ? Convert.ToDateTime(dr["DueDate"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.MRP = dr["MRP"] != DBNull.Value ? Convert.ToString(dr["MRP"]) : string.Empty;
                        oNew_ExpediteStockBE.PurGrp = dr["PurGrp"] != DBNull.Value ? Convert.ToString(dr["PurGrp"]) : string.Empty;
                        oNew_ExpediteStockBE.MatGrp = dr["MatGroup"] != DBNull.Value ? Convert.ToString(dr["MatGroup"]) : string.Empty;
                        oNew_ExpediteStockBE.CommentDateLastUpdated = dr["CommentDateLastUpdated"] != DBNull.Value ? Convert.ToDateTime(dr["CommentDateLastUpdated"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.CommentUpdateValidTo = dr["CommentUpdateValidTo"] != DBNull.Value ? Convert.ToDateTime(dr["CommentUpdateValidTo"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.MostCurrentComment = dr["MostCurrentComment"] != DBNull.Value ? Convert.ToString(dr["MostCurrentComment"]) : string.Empty;
                        oNew_ExpediteStockBE.EarliestAdviseDate = dr["EarliestAdviseDate"] != DBNull.Value ? Convert.ToDateTime(dr["EarliestAdviseDate"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.SOH = dr["SOH"] != DBNull.Value ? Convert.ToString(dr["SOH"]) : string.Empty;
                        oNew_ExpediteStockBE.LeadTimeVariance = dr["Leadtime_Variance"] != DBNull.Value ? Convert.ToString(dr["Leadtime_Variance"]) : string.Empty;
                        oNew_ExpediteStockBE.DaysStock = dr["DaysStock"] != DBNull.Value ? Convert.ToString(dr["DaysStock"]) : string.Empty;
                        oNew_ExpediteStockBE.CountryID = dr["SiteCountryID"] != DBNull.Value ? Convert.ToInt32(dr["SiteCountryID"]) : 0;
                        oNew_ExpediteStockBE.StockPlannerGrouping = dr["StockPlannerGroupings"] != DBNull.Value ? Convert.ToString(dr["StockPlannerGroupings"]) : string.Empty;
                        oNew_ExpediteStockBE.FirstWeekMON = dr["D1"] != DBNull.Value ? Convert.ToDecimal(dr["D1"]) : 0;
                        oNew_ExpediteStockBE.FirstWeekMONColor = dr["Day1Color"] != DBNull.Value ? Convert.ToString(dr["Day1Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FirstWeekTUE = dr["D2"] != DBNull.Value ? Convert.ToDecimal(dr["D2"]) : 0;
                        oNew_ExpediteStockBE.FirstWeekTUEColor = dr["Day2Color"] != DBNull.Value ? Convert.ToString(dr["Day2Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FirstWeekWED = dr["D3"] != DBNull.Value ? Convert.ToDecimal(dr["D3"]) : 0;
                        oNew_ExpediteStockBE.FirstWeekWEDColor = dr["Day3Color"] != DBNull.Value ? Convert.ToString(dr["Day3Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FirstWeekTHU = dr["D4"] != DBNull.Value ? Convert.ToDecimal(dr["D4"]) : 0;
                        oNew_ExpediteStockBE.FirstWeekTHUColor = dr["Day4Color"] != DBNull.Value ? Convert.ToString(dr["Day4Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FirstWeekFRI = dr["D5"] != DBNull.Value ? Convert.ToDecimal(dr["D5"]) : 0;
                        oNew_ExpediteStockBE.FirstWeekFRIColor = dr["Day5Color"] != DBNull.Value ? Convert.ToString(dr["Day5Color"]) : string.Empty;

                        oNew_ExpediteStockBE.SecondWeekMON = dr["D6"] != DBNull.Value ? Convert.ToDecimal(dr["D6"]) : 0;
                        oNew_ExpediteStockBE.SecondWeekMONColor = dr["Day6Color"] != DBNull.Value ? Convert.ToString(dr["Day6Color"]) : string.Empty;

                        oNew_ExpediteStockBE.SecondWeekTUE = dr["D7"] != DBNull.Value ? Convert.ToDecimal(dr["D7"]) : 0;
                        oNew_ExpediteStockBE.SecondWeekTUEColor = dr["Day7Color"] != DBNull.Value ? Convert.ToString(dr["Day7Color"]) : string.Empty;

                        oNew_ExpediteStockBE.SecondWeekWED = dr["D8"] != DBNull.Value ? Convert.ToDecimal(dr["D8"]) : 0;
                        oNew_ExpediteStockBE.SecondWeekWEDColor = dr["Day8Color"] != DBNull.Value ? Convert.ToString(dr["Day8Color"]) : string.Empty;

                        oNew_ExpediteStockBE.SecondWeekTHU = dr["D9"] != DBNull.Value ? Convert.ToDecimal(dr["D9"]) : 0;
                        oNew_ExpediteStockBE.SecondWeekTHUColor = dr["Day9Color"] != DBNull.Value ? Convert.ToString(dr["Day9Color"]) : string.Empty;

                        oNew_ExpediteStockBE.SecondWeekFRI = dr["D10"] != DBNull.Value ? Convert.ToDecimal(dr["D10"]) : 0;
                        oNew_ExpediteStockBE.SecondWeekFRIColor = dr["Day10Color"] != DBNull.Value ? Convert.ToString(dr["Day10Color"]) : string.Empty;

                        oNew_ExpediteStockBE.ThirdWeekMON = dr["D11"] != DBNull.Value ? Convert.ToDecimal(dr["D11"]) : 0;
                        oNew_ExpediteStockBE.ThirdWeekMONColor = dr["Day11Color"] != DBNull.Value ? Convert.ToString(dr["Day11Color"]) : string.Empty;

                        oNew_ExpediteStockBE.ThirdWeekTUE = dr["D12"] != DBNull.Value ? Convert.ToDecimal(dr["D12"]) : 0;
                        oNew_ExpediteStockBE.ThirdWeekTUEColor = dr["Day12Color"] != DBNull.Value ? Convert.ToString(dr["Day12Color"]) : string.Empty;

                        oNew_ExpediteStockBE.ThirdWeekWED = dr["D13"] != DBNull.Value ? Convert.ToDecimal(dr["D13"]) : 0;
                        oNew_ExpediteStockBE.ThirdWeekWEDColor = dr["Day13Color"] != DBNull.Value ? Convert.ToString(dr["Day13Color"]) : string.Empty;

                        oNew_ExpediteStockBE.ThirdWeekTHU = dr["D14"] != DBNull.Value ? Convert.ToDecimal(dr["D14"]) : 0;
                        oNew_ExpediteStockBE.ThirdWeekTHUColor = dr["Day14Color"] != DBNull.Value ? Convert.ToString(dr["Day14Color"]) : string.Empty;

                        oNew_ExpediteStockBE.ThirdWeekFRI = dr["D15"] != DBNull.Value ? Convert.ToDecimal(dr["D15"]) : 0;
                        oNew_ExpediteStockBE.ThirdWeekFRIColor = dr["Day15Color"] != DBNull.Value ? Convert.ToString(dr["Day15Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FourthWeekMON = dr["D16"] != DBNull.Value ? Convert.ToDecimal(dr["D16"]) : 0;
                        oNew_ExpediteStockBE.FourthWeekMONColor = dr["Day16Color"] != DBNull.Value ? Convert.ToString(dr["Day16Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FourthWeekTUE = dr["D17"] != DBNull.Value ? Convert.ToDecimal(dr["D17"]) : 0;
                        oNew_ExpediteStockBE.FourthWeekTUEColor = dr["Day17Color"] != DBNull.Value ? Convert.ToString(dr["Day17Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FourthWeekWED = dr["D18"] != DBNull.Value ? Convert.ToDecimal(dr["D18"]) : 0;
                        oNew_ExpediteStockBE.FourthWeekWEDColor = dr["Day18Color"] != DBNull.Value ? Convert.ToString(dr["Day18Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FourthWeekTHU = dr["D19"] != DBNull.Value ? Convert.ToDecimal(dr["D19"]) : 0;
                        oNew_ExpediteStockBE.FourthWeekTHUColor = dr["Day19Color"] != DBNull.Value ? Convert.ToString(dr["Day19Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FourthWeekFRI = dr["D20"] != DBNull.Value ? Convert.ToDecimal(dr["D20"]) : 0;
                        oNew_ExpediteStockBE.FourthWeekFRIColor = dr["Day20Color"] != DBNull.Value ? Convert.ToString(dr["Day20Color"]) : string.Empty;

                        oNew_ExpediteStockBE.UpdatedDate = dr["UpdatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["UpdatedDate"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.SkuID = dr["SKUID"] != DBNull.Value ? Convert.ToInt32(dr["SKUID"]) : 0;
                        oNew_ExpediteStockBE.SiteNames_SOHPOP = dr["SiteName_SOH"] != DBNull.Value ? Convert.ToString(dr["SiteName_SOH"]) : string.Empty;
                        oNew_ExpediteStockBE.SOH_SOHPOP = dr["SOH_POPUP"] != DBNull.Value ? Convert.ToString(dr["SOH_POPUP"]) : string.Empty;
                        oNew_ExpediteStockBE.AVGForcast_SOHPOP = dr["Avg_MinimumForcast_SOH"] != DBNull.Value ? Convert.ToString(dr["Avg_MinimumForcast_SOH"]) : string.Empty;
                        oNew_ExpediteStockBE.SKuGrouping = dr["SKuGroupName"] != DBNull.Value ? Convert.ToString(dr["SKuGroupName"]) : string.Empty;
                        oNew_ExpediteStockBE.subvndr = dr["subvndr"] != DBNull.Value ? Convert.ToString(dr["subvndr"]) : string.Empty;

                        if (dr.Table.Columns.Contains("TotalRecords"))
                            oNew_ExpediteStockBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;

                        //if (dr.Table.Columns.Contains("FB"))
                        //    oNew_ExpediteStockBE.FB = dr["FB"] != DBNull.Value ? Convert.ToDecimal(dr["FB"]) : 0;

                        if (dr.Table.Columns.Contains("FB"))
                            oNew_ExpediteStockBE.FB = dr["FB"] != DBNull.Value ? Convert.ToDecimal(dr["FB"]) >= 0 ? string.Empty : Convert.ToDecimal(dr["FB"]).ToString() : string.Empty;

                        if (dr.Table.Columns.Contains("CBC"))
                            oNew_ExpediteStockBE.CB = dr["CBC"] != DBNull.Value ? Convert.ToString(dr["CBC"]) : "0";
                        if (dr.Table.Columns.Contains("BackOrderQty"))
                            oNew_ExpediteStockBE.CBQ = dr["BackOrderQty"] != DBNull.Value ? Convert.ToString(dr["BackOrderQty"]) : "0";

                        if (dr.Table.Columns.Contains("CommentColor"))
                            oNew_ExpediteStockBE.CommentColor = dr["CommentColor"] != DBNull.Value ? Convert.ToString(dr["CommentColor"]) : "W";

                        if (dr.Table.Columns.Contains("PriorityItemColor"))
                            oNew_ExpediteStockBE.PriorityItemColor = dr["PriorityItemColor"] != DBNull.Value ? Convert.ToString(dr["PriorityItemColor"]) : "W";

                        oMAS_ExpediteBEList.Add(oNew_ExpediteStockBE);
                    }
                    else
                    {
                        var oNew_ExpediteStockBE = new ExpediteStockBE();
                        oNew_ExpediteStockBE.Comment = dr["Comment"] != DBNull.Value ? Convert.ToString(dr["Comment"]) : string.Empty;

                        oNew_ExpediteStockBE.Comments = dr["Comments"] != DBNull.Value ? Convert.ToString(dr["Comments"]) : string.Empty;
                        oNew_ExpediteStockBE.CommentsAdded_Date = dr["CommentsAdded_Date"] != DBNull.Value ? Convert.ToDateTime(dr["CommentsAdded_Date"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.Reason = dr["Reason"] != DBNull.Value ? Convert.ToString(dr["Reason"]) : string.Empty;
                        oNew_ExpediteStockBE.CommentsValid_Date = dr["CommentsValid_Date"] != DBNull.Value ? Convert.ToDateTime(dr["CommentsValid_Date"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.CommentsAddedBy = dr["CommentBy"] != DBNull.Value ? Convert.ToString(dr["CommentBy"]) : string.Empty;

                        oNew_ExpediteStockBE.Status = dr["Status"] != DBNull.Value ? Convert.ToString(dr["Status"]) : string.Empty;
                        oNew_ExpediteStockBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNew_ExpediteStockBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                        oNew_ExpediteStockBE.Vendor.Vendor_No = dr["VendorNo"] != DBNull.Value ? Convert.ToString(dr["VendorNo"]) : string.Empty;
                        oNew_ExpediteStockBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                        oNew_ExpediteStockBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                        oNew_ExpediteStockBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                        oNew_ExpediteStockBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? Convert.ToString(dr["SiteName"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                        oNew_ExpediteStockBE.PurchaseOrder.OD_Code = dr["ODCode"] != DBNull.Value ? Convert.ToString(dr["ODCode"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder.Direct_code = dr["VikingCode"] != DBNull.Value ? Convert.ToString(dr["VikingCode"]) : string.Empty;
                        oNew_ExpediteStockBE.POStatus = dr["POStatus"] != DBNull.Value ? Convert.ToString(dr["POStatus"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder.Product_description = dr["Description"] != DBNull.Value ? Convert.ToString(dr["Description"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder.StockPlannerNo = dr["StockPlannerNo"] != DBNull.Value ? Convert.ToString(dr["StockPlannerNo"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder.StockPlannerName = dr["StockPlanner"] != DBNull.Value ? Convert.ToString(dr["StockPlanner"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder.Vendor_Code = dr["VendorCode"] != DBNull.Value ? Convert.ToString(dr["VendorCode"]) : string.Empty;
                        oNew_ExpediteStockBE.PurchaseOrder.Item_classification = dr["ItemClassification"] != DBNull.Value ? Convert.ToString(dr["ItemClassification"]) : string.Empty;
                        oNew_ExpediteStockBE.EarliestPO = dr["EarliastPO"] != DBNull.Value ? Convert.ToString(dr["EarliastPO"]) : string.Empty;
                        oNew_ExpediteStockBE.DueDate = dr["DueDate"] != DBNull.Value ? Convert.ToDateTime(dr["DueDate"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.MRP = dr["MRP"] != DBNull.Value ? Convert.ToString(dr["MRP"]) : string.Empty;
                        oNew_ExpediteStockBE.PurGrp = dr["PurGrp"] != DBNull.Value ? Convert.ToString(dr["PurGrp"]) : string.Empty;
                        oNew_ExpediteStockBE.MatGrp = dr["MatGroup"] != DBNull.Value ? Convert.ToString(dr["MatGroup"]) : string.Empty;
                        oNew_ExpediteStockBE.CommentDateLastUpdated = dr["CommentDateLastUpdated"] != DBNull.Value ? Convert.ToDateTime(dr["CommentDateLastUpdated"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.CommentUpdateValidTo = dr["CommentUpdateValidTo"] != DBNull.Value ? Convert.ToDateTime(dr["CommentUpdateValidTo"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.MostCurrentComment = dr["MostCurrentComment"] != DBNull.Value ? Convert.ToString(dr["MostCurrentComment"]) : string.Empty;
                        oNew_ExpediteStockBE.EarliestAdviseDate = dr["EarliestAdviseDate"] != DBNull.Value ? Convert.ToDateTime(dr["EarliestAdviseDate"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.SOH = dr["SOH"] != DBNull.Value ? Convert.ToString(dr["SOH"]) : string.Empty;
                        oNew_ExpediteStockBE.LeadTimeVariance = dr["Leadtime_Variance"] != DBNull.Value ? Convert.ToString(dr["Leadtime_Variance"]) : string.Empty;
                        oNew_ExpediteStockBE.DaysStock = dr["WeeksStock"] != DBNull.Value ? Convert.ToString(dr["WeeksStock"]) : string.Empty;
                        oNew_ExpediteStockBE.CountryID = dr["SiteCountryID"] != DBNull.Value ? Convert.ToInt32(dr["SiteCountryID"]) : 0;
                        oNew_ExpediteStockBE.StockPlannerGrouping = dr["StockPlannerGroupings"] != DBNull.Value ? Convert.ToString(dr["StockPlannerGroupings"]) : string.Empty;
                        oNew_ExpediteStockBE.FirstWeekMON = dr["Week1"] != DBNull.Value ? Convert.ToDecimal(dr["Week1"]) : 0;
                        oNew_ExpediteStockBE.FirstWeekMONColor = dr["Week1Color"] != DBNull.Value ? Convert.ToString(dr["Week1Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FirstWeekTUE = dr["Week2"] != DBNull.Value ? Convert.ToDecimal(dr["Week2"]) : 0;
                        oNew_ExpediteStockBE.FirstWeekTUEColor = dr["Week2Color"] != DBNull.Value ? Convert.ToString(dr["Week2Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FirstWeekWED = dr["Week3"] != DBNull.Value ? Convert.ToDecimal(dr["Week3"]) : 0;
                        oNew_ExpediteStockBE.FirstWeekWEDColor = dr["Week3Color"] != DBNull.Value ? Convert.ToString(dr["Week3Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FirstWeekTHU = dr["Week4"] != DBNull.Value ? Convert.ToDecimal(dr["Week4"]) : 0;
                        oNew_ExpediteStockBE.FirstWeekTHUColor = dr["Week4Color"] != DBNull.Value ? Convert.ToString(dr["Week4Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FirstWeekFRI = dr["Week5"] != DBNull.Value ? Convert.ToDecimal(dr["Week5"]) : 0;
                        oNew_ExpediteStockBE.FirstWeekFRIColor = dr["Week5Color"] != DBNull.Value ? Convert.ToString(dr["Week5Color"]) : string.Empty;

                        oNew_ExpediteStockBE.SecondWeekMON = dr["Week6"] != DBNull.Value ? Convert.ToDecimal(dr["Week6"]) : 0;
                        oNew_ExpediteStockBE.SecondWeekMONColor = dr["Week6Color"] != DBNull.Value ? Convert.ToString(dr["Week6Color"]) : string.Empty;

                        oNew_ExpediteStockBE.SecondWeekTUE = dr["Week7"] != DBNull.Value ? Convert.ToDecimal(dr["Week7"]) : 0;
                        oNew_ExpediteStockBE.SecondWeekTUEColor = dr["Week7Color"] != DBNull.Value ? Convert.ToString(dr["Week7Color"]) : string.Empty;

                        oNew_ExpediteStockBE.SecondWeekWED = dr["Week8"] != DBNull.Value ? Convert.ToDecimal(dr["Week8"]) : 0;
                        oNew_ExpediteStockBE.SecondWeekWEDColor = dr["Week8Color"] != DBNull.Value ? Convert.ToString(dr["Week8Color"]) : string.Empty;

                        oNew_ExpediteStockBE.SecondWeekTHU = dr["Week9"] != DBNull.Value ? Convert.ToDecimal(dr["Week9"]) : 0;
                        oNew_ExpediteStockBE.SecondWeekTHUColor = dr["Week9Color"] != DBNull.Value ? Convert.ToString(dr["Week9Color"]) : string.Empty;

                        oNew_ExpediteStockBE.SecondWeekFRI = dr["Week10"] != DBNull.Value ? Convert.ToDecimal(dr["Week10"]) : 0;
                        oNew_ExpediteStockBE.SecondWeekFRIColor = dr["Week10Color"] != DBNull.Value ? Convert.ToString(dr["Week10Color"]) : string.Empty;

                        oNew_ExpediteStockBE.ThirdWeekMON = dr["Week11"] != DBNull.Value ? Convert.ToDecimal(dr["Week11"]) : 0;
                        oNew_ExpediteStockBE.ThirdWeekMONColor = dr["Week11Color"] != DBNull.Value ? Convert.ToString(dr["Week11Color"]) : string.Empty;

                        oNew_ExpediteStockBE.ThirdWeekTUE = dr["Week12"] != DBNull.Value ? Convert.ToDecimal(dr["Week12"]) : 0;
                        oNew_ExpediteStockBE.ThirdWeekTUEColor = dr["Week12Color"] != DBNull.Value ? Convert.ToString(dr["Week12Color"]) : string.Empty;

                        oNew_ExpediteStockBE.ThirdWeekWED = dr["Week13"] != DBNull.Value ? Convert.ToDecimal(dr["Week13"]) : 0;
                        oNew_ExpediteStockBE.ThirdWeekWEDColor = dr["Week13Color"] != DBNull.Value ? Convert.ToString(dr["Week13Color"]) : string.Empty;

                        oNew_ExpediteStockBE.ThirdWeekTHU = dr["Week14"] != DBNull.Value ? Convert.ToDecimal(dr["Week14"]) : 0;
                        oNew_ExpediteStockBE.ThirdWeekTHUColor = dr["Week14Color"] != DBNull.Value ? Convert.ToString(dr["Week14Color"]) : string.Empty;

                        oNew_ExpediteStockBE.ThirdWeekFRI = dr["Week15"] != DBNull.Value ? Convert.ToDecimal(dr["Week15"]) : 0;
                        oNew_ExpediteStockBE.ThirdWeekFRIColor = dr["Week15Color"] != DBNull.Value ? Convert.ToString(dr["Week15Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FourthWeekMON = dr["Week16"] != DBNull.Value ? Convert.ToDecimal(dr["Week16"]) : 0;
                        oNew_ExpediteStockBE.FourthWeekMONColor = dr["Week16Color"] != DBNull.Value ? Convert.ToString(dr["Week16Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FourthWeekTUE = dr["Week17"] != DBNull.Value ? Convert.ToDecimal(dr["Week17"]) : 0;
                        oNew_ExpediteStockBE.FourthWeekTUEColor = dr["Week17Color"] != DBNull.Value ? Convert.ToString(dr["Week17Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FourthWeekWED = dr["Week18"] != DBNull.Value ? Convert.ToDecimal(dr["Week18"]) : 0;
                        oNew_ExpediteStockBE.FourthWeekWEDColor = dr["Week18Color"] != DBNull.Value ? Convert.ToString(dr["Week18Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FourthWeekTHU = dr["Week19"] != DBNull.Value ? Convert.ToDecimal(dr["Week19"]) : 0;
                        oNew_ExpediteStockBE.FourthWeekTHUColor = dr["Week19Color"] != DBNull.Value ? Convert.ToString(dr["Week19Color"]) : string.Empty;

                        oNew_ExpediteStockBE.FourthWeekFRI = dr["Week20"] != DBNull.Value ? Convert.ToDecimal(dr["Week20"]) : 0;
                        oNew_ExpediteStockBE.FourthWeekFRIColor = dr["Week20Color"] != DBNull.Value ? Convert.ToString(dr["Week20Color"]) : string.Empty;

                        oNew_ExpediteStockBE.UpdatedDate = dr["UpdatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["UpdatedDate"]) : (DateTime?)null;
                        oNew_ExpediteStockBE.SkuID = dr["SKUID"] != DBNull.Value ? Convert.ToInt32(dr["SKUID"]) : 0;
                        oNew_ExpediteStockBE.SiteNames_SOHPOP = dr["SiteName_SOH"] != DBNull.Value ? Convert.ToString(dr["SiteName_SOH"]) : string.Empty;
                        oNew_ExpediteStockBE.SOH_SOHPOP = dr["SOH_POPUP"] != DBNull.Value ? Convert.ToString(dr["SOH_POPUP"]) : string.Empty;
                        oNew_ExpediteStockBE.AVGForcast_SOHPOP = dr["Avg_MinimumForcast_SOH"] != DBNull.Value ? Convert.ToString(dr["Avg_MinimumForcast_SOH"]) : string.Empty;
                        oNew_ExpediteStockBE.SKuGrouping = dr["SKuGroupName"] != DBNull.Value ? Convert.ToString(dr["SKuGroupName"]) : string.Empty;
                        oNew_ExpediteStockBE.subvndr = dr["subvndr"] != DBNull.Value ? Convert.ToString(dr["subvndr"]) : string.Empty;

                        if (dr.Table.Columns.Contains("TotalRecords"))
                            oNew_ExpediteStockBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;

                        //if (dr.Table.Columns.Contains("FB"))
                        //    oNew_ExpediteStockBE.FB = dr["FB"] != DBNull.Value ? Convert.ToDecimal(dr["FB"]) : 0;

                        if (dr.Table.Columns.Contains("FB"))
                            oNew_ExpediteStockBE.FB = dr["FB"] != DBNull.Value ? Convert.ToDecimal(dr["FB"]) >= 0 ? string.Empty : Convert.ToDecimal(dr["FB"]).ToString() : string.Empty;

                        if (dr.Table.Columns.Contains("CBC"))
                            oNew_ExpediteStockBE.CB = dr["CBC"] != DBNull.Value ? Convert.ToString(dr["CBC"]) : "0";
                        if (dr.Table.Columns.Contains("BackOrderQty"))
                            oNew_ExpediteStockBE.CBQ = dr["BackOrderQty"] != DBNull.Value ? Convert.ToString(dr["BackOrderQty"]) : "0";

                        if (dr.Table.Columns.Contains("CommentColor"))
                            oNew_ExpediteStockBE.CommentColor = dr["CommentColor"] != DBNull.Value ? Convert.ToString(dr["CommentColor"]) : "W";

                        if (dr.Table.Columns.Contains("PriorityItemColor"))
                            oNew_ExpediteStockBE.PriorityItemColor = dr["PriorityItemColor"] != DBNull.Value ? Convert.ToString(dr["PriorityItemColor"]) : "W";

                        oMAS_ExpediteBEList.Add(oNew_ExpediteStockBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_ExpediteBEList;
        }

        public DataTable GetPotentialOutputExportDAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            var dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[24];
                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oMAS_ExpStockBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedIncludedVendorIDs", oMAS_ExpStockBE.SelectedIncludedVendorIDs);
                param[index++] = new SqlParameter("@SelectedExcludedVendorIDs", oMAS_ExpStockBE.SelectedExcludedVendorIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oMAS_ExpStockBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedItemClassification", oMAS_ExpStockBE.SelectedItemClassification);
                param[index++] = new SqlParameter("@SelectedMRPType", oMAS_ExpStockBE.SelectedMRPType);
                param[index++] = new SqlParameter("@SelectedPurcGroup", oMAS_ExpStockBE.SelectedPurcGroup);
                param[index++] = new SqlParameter("@SelectedMatGrp", oMAS_ExpStockBE.SelectedMatGrp);
                param[index++] = new SqlParameter("@SelectedVIKINGSKU", oMAS_ExpStockBE.SelectedVIKINGSKU);
                param[index++] = new SqlParameter("@SelectedODSKU", oMAS_ExpStockBE.SelectedODSKU);
                param[index++] = new SqlParameter("@SelectedSkuType", oMAS_ExpStockBE.SelectedSkuType);
                param[index++] = new SqlParameter("@SelectedDisplay", oMAS_ExpStockBE.SelectedDisplay);
                param[index++] = new SqlParameter("@Page", oMAS_ExpStockBE.Page);
                param[index++] = new SqlParameter("@SortBy", oMAS_ExpStockBE.SortBy);
                param[index++] = new SqlParameter("@POStatusSearchCriteria", oMAS_ExpStockBE.POStatusSearchCriteria);
                param[index++] = new SqlParameter("@CommentStatus", oMAS_ExpStockBE.CommentStatus);
                param[index++] = new SqlParameter("@DaysStock", oMAS_ExpStockBE.DaysStock);
                param[index++] = new SqlParameter("@MinForecastedSales", oMAS_ExpStockBE.MinForecastedSales);
                param[index++] = new SqlParameter("@SortingDay", oMAS_ExpStockBE.SortingDay);
                param[index++] = new SqlParameter("@StockPlannerGroupingID", oMAS_ExpStockBE.StockPlannerGroupingID);
                param[index++] = new SqlParameter("@SkugroupingID", oMAS_ExpStockBE.SkugroupingID);
                param[index++] = new SqlParameter("@IsPriority", oMAS_ExpStockBE.IsPriority);
                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spExpediteStock", param);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }
        public List<ExpediteStockBE> GetTrackingReportDAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            var dt = new DataTable();
            var oMAS_ExpediteBEList = new List<ExpediteStockBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oMAS_ExpStockBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedIncludedVendorIDs", oMAS_ExpStockBE.SelectedIncludedVendorIDs);
                param[index++] = new SqlParameter("@SelectedExcludedVendorIDs", oMAS_ExpStockBE.SelectedExcludedVendorIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oMAS_ExpStockBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedItemClassification", oMAS_ExpStockBE.SelectedItemClassification);
                param[index++] = new SqlParameter("@SelectedSkuType", oMAS_ExpStockBE.SelectedSkuType);
                param[index++] = new SqlParameter("@SelectedDisplay", oMAS_ExpStockBE.SelectedDisplay);
                param[index++] = new SqlParameter("@SelectedUpdate", oMAS_ExpStockBE.SelectedUpdate);
                param[index++] = new SqlParameter("@Page", oMAS_ExpStockBE.Page);
                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spExpediteStock", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    var oNew_ExpediteStockBE = new ExpediteStockBE();
                    oNew_ExpediteStockBE.Comment = dr["Comment"] != DBNull.Value ? Convert.ToString(dr["Comment"]) : string.Empty;
                    oNew_ExpediteStockBE.Status = dr["Status"] != DBNull.Value ? Convert.ToString(dr["Status"]) : string.Empty;
                    oNew_ExpediteStockBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNew_ExpediteStockBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                    oNew_ExpediteStockBE.Vendor.Vendor_No = dr["VendorNo"] != DBNull.Value ? Convert.ToString(dr["VendorNo"]) : string.Empty;
                    oNew_ExpediteStockBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                    oNew_ExpediteStockBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNew_ExpediteStockBE.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    oNew_ExpediteStockBE.Site.SiteName = dr["SiteName"] != DBNull.Value ? Convert.ToString(dr["SiteName"]) : string.Empty;
                    oNew_ExpediteStockBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oNew_ExpediteStockBE.PurchaseOrder.OD_Code = dr["ODCode"] != DBNull.Value ? Convert.ToString(dr["ODCode"]) : string.Empty;
                    oNew_ExpediteStockBE.PurchaseOrder.Direct_code = dr["VikingCode"] != DBNull.Value ? Convert.ToString(dr["VikingCode"]) : string.Empty;
                    oNew_ExpediteStockBE.POStatus = dr["POStatus"] != DBNull.Value ? Convert.ToString(dr["POStatus"]) : string.Empty;
                    oNew_ExpediteStockBE.PurchaseOrder.Product_description = dr["Description"] != DBNull.Value ? Convert.ToString(dr["Description"]) : string.Empty;
                    oNew_ExpediteStockBE.PurchaseOrder.StockPlannerNo = dr["StockPlannerNo"] != DBNull.Value ? Convert.ToString(dr["StockPlannerNo"]) : string.Empty;
                    oNew_ExpediteStockBE.PurchaseOrder.StockPlannerName = dr["StockPlanner"] != DBNull.Value ? Convert.ToString(dr["StockPlanner"]) : string.Empty;
                    oNew_ExpediteStockBE.PurchaseOrder.Vendor_Code = dr["VendorCode"] != DBNull.Value ? Convert.ToString(dr["VendorCode"]) : string.Empty;
                    oNew_ExpediteStockBE.PurchaseOrder.Item_classification = dr["ItemClassification"] != DBNull.Value ? Convert.ToString(dr["ItemClassification"]) : string.Empty;
                    oNew_ExpediteStockBE.CommentDateLastUpdated = dr["CommentDateLastUpdated"] != DBNull.Value ? Convert.ToDateTime(dr["CommentDateLastUpdated"]) : (DateTime?)null;
                    oNew_ExpediteStockBE.CommentUpdateValidTo = dr["CommentUpdateValidTo"] != DBNull.Value ? Convert.ToDateTime(dr["CommentUpdateValidTo"]) : (DateTime?)null;
                    oNew_ExpediteStockBE.MostCurrentComment = dr["MostCurrentComment"] != DBNull.Value ? Convert.ToString(dr["MostCurrentComment"]) : string.Empty;
                    oNew_ExpediteStockBE.SkuID = dr["SKUID"] != DBNull.Value ? Convert.ToInt32(dr["SKUID"]) : 0;
                    if (dr.Table.Columns.Contains("TotalRecords"))
                        oNew_ExpediteStockBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;
                    oMAS_ExpediteBEList.Add(oNew_ExpediteStockBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_ExpediteBEList;
        }

        public List<ExpediteStockBE> GetExpediateStockDAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();
            List<ExpediteStockBE> oMAS_ExpediteBEList = new List<ExpediteStockBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oMAS_ExpStockBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oMAS_ExpStockBE.SelectedIncludedVendorIDs);
                param[index++] = new SqlParameter("@StockPlannerIDs", oMAS_ExpStockBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@ExpediteVendorID", oMAS_ExpStockBE.VendorName);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(),
                    CommandType.StoredProcedure, "spMASCNT_ExpediteStock", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    ExpediteStockBE oNew_ExpediteStockBE = new ExpediteStockBE();
                    oNew_ExpediteStockBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNew_ExpediteStockBE.VendorName = Convert.ToString(dr["Vendor_Name"]);
                    oNew_ExpediteStockBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                    oNew_ExpediteStockBE.Vendor_No = Convert.ToString(dr["Vendor_no"]);
                    oNew_ExpediteStockBE.SiteName = Convert.ToString(dr["SiteName"]);
                    oNew_ExpediteStockBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNew_ExpediteStockBE.OD_Code = dr["OD_SKU_NO"].ToString();
                    oNew_ExpediteStockBE.Direct_code = dr["Direct_SKU"].ToString();
                    oNew_ExpediteStockBE.Product_description = dr["DESCRIPTION"].ToString();
                    oNew_ExpediteStockBE.StockPlannerNo = dr["StockPlannerNo"].ToString();
                    oNew_ExpediteStockBE.StockPlannerName = dr["StockPlannerName"].ToString();
                    oNew_ExpediteStockBE.Vendor_Code = dr["Vendor_Code"].ToString();
                    oNew_ExpediteStockBE.Comments = dr["COMMENTS"].ToString();
                    oNew_ExpediteStockBE.ExpStatus = Convert.ToString(dr["Exp_Status"]);//CommentsAdded_Date
                    oNew_ExpediteStockBE.CommentsAdded_Date = dr["CommentsAdded_Date"] != DBNull.Value ? Convert.ToDateTime(dr["CommentsAdded_Date"]) : (DateTime?)null;
                    oNew_ExpediteStockBE.CommentID = dr["CommentID"] != DBNull.Value ? Convert.ToInt32(dr["CommentID"]) : 0;
                    oNew_ExpediteStockBE.PurchaseOrder = new Up_PurchaseOrderDetailBE();
                    oNew_ExpediteStockBE.PurchaseOrder.Purchase_order = Convert.ToString(dr["Purchase_order"]);
                    oNew_ExpediteStockBE.PurchaseOrder.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    oNew_ExpediteStockBE.CommentID = dr["CommentID"] != DBNull.Value ? Convert.ToInt32(dr["CommentID"]) : 0;
                    oNew_ExpediteStockBE.CommunicationID = dr["CommunicationID"] != DBNull.Value ? Convert.ToInt32(dr["CommunicationID"]) : 0;
                    oNew_ExpediteStockBE.CountryName = dr["Country"] != DBNull.Value ? dr["Country"].ToString() : string.Empty;
                    oNew_ExpediteStockBE.SelectedStockPlannerIDs = dr["StockPlannerID"] != DBNull.Value ? Convert.ToString(dr["StockPlannerID"]) : "0";
                    oNew_ExpediteStockBE.SkuID = dr["SKUID"] != DBNull.Value ? Convert.ToInt32(dr["SKUID"]) : 0;

                    oMAS_ExpediteBEList.Add(oNew_ExpediteStockBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_ExpediteBEList;
        }

        public List<ExpediteStockBE> GetAllMrpPurcMatGroupDAL(ExpediteStockBE ExpediteStock)
        {
            var dt = new DataTable();
            var lstExpediteStock = new List<ExpediteStockBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", ExpediteStock.Action);
                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spExpediteStock", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    var expediteStock = new ExpediteStockBE();
                    if (ExpediteStock.Action.Equals("GetAllMRP"))
                        expediteStock.MRP = dr["MRP"] != DBNull.Value ? Convert.ToString(dr["MRP"]) : string.Empty;
                    else if (ExpediteStock.Action.Equals("GetAllPurGrp"))
                        expediteStock.PurGrp = dr["PurGrp"] != DBNull.Value ? Convert.ToString(dr["PurGrp"]) : string.Empty;
                    else if (ExpediteStock.Action.Equals("GetAllMatGroup"))
                        expediteStock.MatGrp = dr["MatGroup"] != DBNull.Value ? Convert.ToString(dr["MatGroup"]) : string.Empty;

                    lstExpediteStock.Add(expediteStock);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstExpediteStock;
        }

        public List<ExpediteStockBE> GetAllSOHDAL(ExpediteStockBE ExpediteStock)
        {
            var dt = new DataTable();
            var lstExpediteStock = new List<ExpediteStockBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", ExpediteStock.Action);
                param[index++] = new SqlParameter("@SKUID", ExpediteStock.SkuID);
                param[index++] = new SqlParameter("@ActualSoldAmt", ExpediteStock.ActualSoldAmt);
                param[index++] = new SqlParameter("@MoveSiteId", ExpediteStock.MoveSiteId);
                param[index++] = new SqlParameter("@MoveToSiteId", ExpediteStock.MoveToSiteId);
                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spExpediteStock", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    var expediteStock = new ExpediteStockBE();
                    expediteStock.DayUploaded = dr["DayUploaded"] != DBNull.Value ? Convert.ToString(dr["DayUploaded"]) : string.Empty;
                    expediteStock.DateUploaded = dr["DateUploaded"] != DBNull.Value ? Convert.ToDateTime(dr["DateUploaded"]) : (DateTime?)null;
                    expediteStock.OHQtyMove = dr["OHQtyMove"] != DBNull.Value ? Convert.ToDecimal(dr["OHQtyMove"]) : 0;
                    expediteStock.ForecastSoldAmtMove = dr["ForecastSoldAmtMove"] != DBNull.Value ? Convert.ToDecimal(dr["ForecastSoldAmtMove"]) : 0;
                    expediteStock.ActualSoldAmtMove = dr["ActualSoldAmtMove"] != DBNull.Value ? Convert.ToDecimal(dr["ActualSoldAmtMove"]) : 0;
                    expediteStock.OHQtyMoveTo = dr["OHQtyMoveTo"] != DBNull.Value ? Convert.ToDecimal(dr["OHQtyMoveTo"]) : 0;
                    expediteStock.ForecastSoldAmtMoveTo = dr["ForecastSoldAmtMoveTo"] != DBNull.Value ? Convert.ToDecimal(dr["ForecastSoldAmtMoveTo"]) : 0;
                    expediteStock.ActualSoldAmtMoveTo = dr["ActualSoldAmtMoveTo"] != DBNull.Value ? Convert.ToDecimal(dr["ActualSoldAmtMoveTo"]) : 0;
                    expediteStock.CurrentDate = dr["CurrentDate"] != DBNull.Value ? Convert.ToDateTime(dr["CurrentDate"]) : (DateTime?)null;
                    lstExpediteStock.Add(expediteStock);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstExpediteStock;
        }

        public List<ExpediteStockBE> GetSKUOnHandQtyDAL(ExpediteStockBE ExpediteStock)
        {
            var dt = new DataTable();
            var lstExpediteStock = new List<ExpediteStockBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", ExpediteStock.Action);
                param[index++] = new SqlParameter("@SKUID", ExpediteStock.SkuID);
                param[index++] = new SqlParameter("@SiteID", ExpediteStock.SiteID);
                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spExpediteStock", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    var expediteStock = new ExpediteStockBE();
                    expediteStock.SkuID = dr["SKUID"] != DBNull.Value ? Convert.ToInt32(dr["SKUID"]) : 0;
                    expediteStock.QtyOnHand = dr["QtyOnHand"] != DBNull.Value ? Convert.ToDecimal(dr["QtyOnHand"]) : 0;
                    lstExpediteStock.Add(expediteStock);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstExpediteStock;
        }

        public int? SaveExpediteSKUDAL(ExpediteStockBE oMAS_ExpediteBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", oMAS_ExpediteBE.Action);
                param[index++] = new SqlParameter("@UserID", oMAS_ExpediteBE.UserID);
                param[index++] = new SqlParameter("@Comm_Status", oMAS_ExpediteBE.Comm_Status);
                param[index++] = new SqlParameter("@CommentID", oMAS_ExpediteBE.CommentID);
                param[index++] = new SqlParameter("@VendorID", oMAS_ExpediteBE.VendorID);
                param[index++] = new SqlParameter("@SiteID", oMAS_ExpediteBE.SiteID);
                param[index++] = new SqlParameter("@SKUID", oMAS_ExpediteBE.SkuID);
                param[index++] = new SqlParameter("@VikingCode", oMAS_ExpediteBE.Viking_Code);
                param[index++] = new SqlParameter("@ODCode", oMAS_ExpediteBE.OD_Code);
                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASCNT_ExpediteStock", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? SaveExpCommunicationDAL(ExpediteStockBE oMAS_ExpediteBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oMAS_ExpediteBE.Action);
                //param[index++] = new SqlParameter("@CommunicationType", oMAS_ExpediteBE.CommunicationType);
                param[index++] = new SqlParameter("@Subject", oMAS_ExpediteBE.Subject);
                param[index++] = new SqlParameter("@SentTo", oMAS_ExpediteBE.SentTo);
                param[index++] = new SqlParameter("@Body", oMAS_ExpediteBE.Body);
                param[index++] = new SqlParameter("@Comm_By", oMAS_ExpediteBE.Comm_By);
                param[index++] = new SqlParameter("@Comm_On", oMAS_ExpediteBE.Comm_On);
                //param[index++] = new SqlParameter("@IsMailSent", oMAS_ExpediteBE.IsMailSent);
                param[index++] = new SqlParameter("@language", oMAS_ExpediteBE.language);
                param[index++] = new SqlParameter("@CommentsIds", oMAS_ExpediteBE.CommentIDs);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASCNT_ExpediteStock", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataTable GetVendorLanguageDAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                param[index++] = new SqlParameter("@VendorId", oMAS_ExpStockBE.Vendor.VendorID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASCNT_ExpediteStock", param);
                if (oMAS_ExpStockBE.Comments != "Vendor")
                    dt = ds.Tables[0];
                else
                    dt = ds.Tables[1];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public List<ExpediteStockBE> GetExpediteCommentsHistoryDAL(ExpediteStockBE ExpediteStock)
        {
            var dt = new DataTable();
            var lstExpediteStock = new List<ExpediteStockBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", ExpediteStock.Action);
                param[index++] = new SqlParameter("@SKUID", ExpediteStock.SkuID);
                param[index++] = new SqlParameter("@CommunicationID", ExpediteStock.CommunicationID);
                param[index++] = new SqlParameter("@VendorId", ExpediteStock.VendorID);
                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_ExpediteComments", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    var expediteStock = new ExpediteStockBE();
                    expediteStock.Comm_On = dr["Comm_On"] != DBNull.Value ? Convert.ToDateTime(dr["Comm_On"]) : (DateTime?)null;
                    expediteStock.Comm_By = dr["Comm_By"] != DBNull.Value ? Convert.ToString(dr["Comm_By"]) : string.Empty;
                    expediteStock.SentTo = dr["SentTo"] != DBNull.Value ? Convert.ToString(dr["SentTo"]) : string.Empty;
                    expediteStock.Body = dr["Body"] != DBNull.Value ? Convert.ToString(dr["Body"]) : string.Empty;
                    expediteStock.CommunicationID = dr["CommunicationID"] != DBNull.Value ? Convert.ToInt32(dr["CommunicationID"]) : 0;
                    expediteStock.Subject = dr["Subject"] != DBNull.Value ? Convert.ToString(dr["Subject"]) : string.Empty;
                    expediteStock.Option = dr["Option"] != DBNull.Value ? Convert.ToString(dr["Option"]) : string.Empty;
                    expediteStock.VendorID = dr["VendorId"] != DBNull.Value ? Convert.ToInt32(dr["VendorId"]) : 0;
                    lstExpediteStock.Add(expediteStock);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstExpediteStock;
        }

        public List<ExpediteStockBE> GetBackOrderHistoryDAL(ExpediteStockBE ExpediteStock)
        {
            var dt = new DataTable();
            var lstExpediteStock = new List<ExpediteStockBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", ExpediteStock.Action);
                param[index++] = new SqlParameter("@SKUID", ExpediteStock.SkuID);
                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spExpediteStock", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    var expediteStock = new ExpediteStockBE();
                    expediteStock.BackOrder = new BackOrderBE();
                    expediteStock.BackOrder.BOHDate = dr["BOHDate"] != DBNull.Value ? Convert.ToDateTime(dr["BOHDate"]) : (DateTime?)null;
                    expediteStock.BackOrder.CountofBackOrder = dr["CountofBackOrder"] != DBNull.Value ? Convert.ToInt32(dr["CountofBackOrder"]) : (int?)null;
                    expediteStock.BackOrder.QtyonBackOrder = dr["QtyonBackOrder"] != DBNull.Value ? Convert.ToInt32(dr["QtyonBackOrder"]) : (int?)null;
                    lstExpediteStock.Add(expediteStock);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstExpediteStock;
        }

        public List<ExpediteStockBE> GetEmailCommunicationDAL(ExpediteStockBE oExpediteStockBE)
        {
            List<ExpediteStockBE> lstExpediteStockBE = new List<ExpediteStockBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oExpediteStockBE.Action);
                param[index++] = new SqlParameter("@CommunicationID", oExpediteStockBE.CommunicationID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASCNT_ExpediteStock", param);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ExpediteStockBE newExpediteStockBE = new ExpediteStockBE();
                        newExpediteStockBE.Body = dr["Body"] != DBNull.Value ? Convert.ToString(dr["Body"]) : string.Empty;
                        lstExpediteStockBE.Add(newExpediteStockBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstExpediteStockBE;
        }

        public void GetPriorityItemsListDAL(ExpediteStockBE oMAS_ExpStockBE, out List<ExpediteStockBE> ExpediteBEList, out int RecordCount)
        {
            DataTable dt = new DataTable();
            List<ExpediteStockBE> oMASCNT_ExpediteBEList = new List<ExpediteStockBE>();
            SqlParameter[] param = new SqlParameter[6];
            DataSet ds = null;
            try
            {
                int index = 0;

                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                param[index++] = new SqlParameter("@CountryID", oMAS_ExpStockBE.CountryID);
                param[index++] = new SqlParameter("@OD_Code", oMAS_ExpStockBE.OD_Code);
                param[index++] = new SqlParameter("@Viking_code", oMAS_ExpStockBE.Viking_Code);
                param[index++] = new SqlParameter("@PageIndex", oMAS_ExpStockBE.PageCount);
                param[index++] = new SqlParameter("@RecordCount", SqlDbType.Int);
                param[index - 1].Direction = ParameterDirection.Output;
                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spExpediteStock", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    ExpediteStockBE oNewMASCNT_ExpediteBE = new ExpediteStockBE();
                    oNewMASCNT_ExpediteBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    oNewMASCNT_ExpediteBE.Country.CountryID = Convert.ToInt32(dr["CountryID"]);
                    oNewMASCNT_ExpediteBE.Country.CountryName = Convert.ToString(dr["CountryName"]);
                    oNewMASCNT_ExpediteBE.OD_Code = Convert.ToString(dr["OD_Code"]);
                    oNewMASCNT_ExpediteBE.Viking_Code = Convert.ToString(dr["Viking_Code"]);
                    oNewMASCNT_ExpediteBE.Product_description = Convert.ToString(dr["Product_description"]);
                    oNewMASCNT_ExpediteBE.Reason = Convert.ToString(dr["Reason"]);
                    oNewMASCNT_ExpediteBE.CommentsAddedBy = Convert.ToString(dr["CommentsAddedBy"]);

                    oNewMASCNT_ExpediteBE.Date = Convert.ToDateTime(dr["Date"]);
                    //oNewMASCNT_ExpediteBE.Date = dr["CommentsAdded_Date"] != DBNull.Value ? Convert.ToDateTime(dr["CommentsAdded_Date"]) : (DateTime?)null;
                    //  oNewMASCNT_ExpediteBE.SkuCode = dr["SKU"].ToString();

                    oMASCNT_ExpediteBEList.Add(oNewMASCNT_ExpediteBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            finally { }
            ExpediteBEList = oMASCNT_ExpediteBEList;
            if (ds.Tables.Count > 1)
            {
                RecordCount = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
            }
            else
            {
                RecordCount = 0;
            }

        }

        public List<ExpediteStockBE> GetDescriptionDAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();
            List<ExpediteStockBE> oMASCNT_ExpediteBEList = new List<ExpediteStockBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oMAS_ExpStockBE.Action);
                param[index++] = new SqlParameter("@CountryID", oMAS_ExpStockBE.CountryID);
                param[index++] = new SqlParameter("@OD_Code", oMAS_ExpStockBE.OD_Code);
                param[index++] = new SqlParameter("@Viking_code", oMAS_ExpStockBE.Viking_Code);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spExpediteStock", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    ExpediteStockBE oNewMASCNT_ExpediteBE = new ExpediteStockBE();
                    oNewMASCNT_ExpediteBE.Product_description = dr["Product_description"] != DBNull.Value ? Convert.ToString(dr["Product_description"]) : string.Empty;
                    oNewMASCNT_ExpediteBE.Viking_Code = dr["Viking_Code"] != DBNull.Value ? Convert.ToString(dr["Viking_Code"]) : string.Empty;
                    oNewMASCNT_ExpediteBE.OD_Code = dr["OD_Code"] != DBNull.Value ? Convert.ToString(dr["OD_Code"]) : string.Empty;

                    oMASCNT_ExpediteBEList.Add(oNewMASCNT_ExpediteBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASCNT_ExpediteBEList;
        }


        public int? SavePriorityItemReasonDAL(ExpediteStockBE oMAS_ExpediteBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMAS_ExpediteBE.Action);
                param[index++] = new SqlParameter("@CountryID", oMAS_ExpediteBE.CountryID);
                param[index++] = new SqlParameter("@isPriorityReason", oMAS_ExpediteBE.Reason);
                param[index++] = new SqlParameter("@OD_Code", oMAS_ExpediteBE.OD_Code);
                param[index++] = new SqlParameter("@Viking_code", oMAS_ExpediteBE.Viking_Code);
                param[index++] = new SqlParameter("@IsPriorityAddedBy", oMAS_ExpediteBE.IsPriorityAddedBy);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spExpediteStock", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

    }
}

﻿using BusinessEntities.ModuleBE.StockOverview;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.StockOverview
{
    public class SubVendorDAL
    {
        public List<POSubVendorBe> GetPOSubVendorDAL(POSubVendorBe pOSubVendor)
        {
            DataTable dt = new DataTable();
            var POSubVendorList = new List<POSubVendorBe>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", pOSubVendor.Action);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSubVendor", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    POSubVendorBe objPOSubVendor = new POSubVendorBe
                    {
                        POSubVendorID = dr["POSubVendorID"] != DBNull.Value ? Convert.ToInt32(dr["POSubVendorID"]) : 0,
                        Subvndr = dr["Subvndr"] != DBNull.Value ? Convert.ToString(dr["Subvndr"]) : string.Empty
                    };
                    POSubVendorList.Add(objPOSubVendor);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return POSubVendorList;
        }
    }
}

﻿using BusinessEntities.ModuleBE.StockOverview.Stock_Turn;
using System;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.StockOverview.StockTurn
{
    public class StockTurnDAL
    {
        public int? ADDStockTurnSettings(StockTurnSettingBE stockTurnSettingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", stockTurnSettingBE.Action);
                param[index++] = new SqlParameter("@ODMeasure_COG", stockTurnSettingBE.ODMeasure_COG);
                param[index++] = new SqlParameter("@StockTurnAverage_COG", stockTurnSettingBE.StockTurnAverage_COG);
                param[index++] = new SqlParameter("@StockTurnAverage_INVENTORY", stockTurnSettingBE.StockTurnAverage_INVENTORY);
                param[index++] = new SqlParameter("@StockTurnActuals_COG", stockTurnSettingBE.StockTurnActuals_COG);
                param[index++] = new SqlParameter("@StockTurnActuals_INVENTORY", stockTurnSettingBE.StockTurnActuals_INVENTORY);

                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spStockTurn_Settings", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public StockTurnSettingBE ViewStockTurn(StockTurnSettingBE stockTurnSettingBE)
        {
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", stockTurnSettingBE.Action);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spStockTurn_Settings", param);
                DataTable dt = ds.Tables[0];
                DataRow dr = dt.Rows[0];
                stockTurnSettingBE.ODMeasure_COG = dr["ODMeasure_COG"] != DBNull.Value ? Convert.ToInt32(dr["ODMeasure_COG"]) : 0;
                stockTurnSettingBE.StockTurnAverage_COG = dr["StockTurnAverage_COG"] != DBNull.Value ? Convert.ToInt32(dr["StockTurnAverage_COG"]) : 0;
                stockTurnSettingBE.StockTurnAverage_INVENTORY = dr["StockTurnAverage_INVENTORY"] != DBNull.Value ? Convert.ToInt32(dr["StockTurnAverage_INVENTORY"]) : 0;
                stockTurnSettingBE.StockTurnActuals_COG = dr["StockTurnActuals_COG"] != DBNull.Value ? Convert.ToInt32(dr["StockTurnActuals_COG"]) : 0;
                stockTurnSettingBE.StockTurnActuals_INVENTORY = dr["StockTurnActuals_INVENTORY"] != DBNull.Value ? Convert.ToInt32(dr["StockTurnActuals_INVENTORY"]) : 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return stockTurnSettingBE;
        }

        public void StockTurnVendorReport(StockTurnSettingBE stockTurnSettingBE, out DataSet dsResult, out int RecordCount)
        {
            SqlParameter[] param = new SqlParameter[5];
            try
            {
                int index = 0;
                param[index++] = new SqlParameter("@Action", stockTurnSettingBE.Action);
                param[index++] = new SqlParameter("@COUNTRYIDS", stockTurnSettingBE.countryIDs);
                param[index++] = new SqlParameter("@VENDORIDS", stockTurnSettingBE.vendorIDs);
                param[index++] = new SqlParameter("@PageIndex", stockTurnSettingBE.PageCount);
                param[index++] = new SqlParameter("@RecordCount", SqlDbType.Int);
                param[index - 1].Direction = ParameterDirection.Output;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            dsResult = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "sp_Stockturn_Reports", param);
            RecordCount = Convert.ToInt32(param[4].Value);
        }

    }
}

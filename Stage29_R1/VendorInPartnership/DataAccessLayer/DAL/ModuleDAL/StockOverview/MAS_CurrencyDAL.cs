﻿using BusinessEntities.ModuleBE.StockOverview;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.StockOverview
{
    public class MAS_CurrencyDAL
    {

        public List<CurrencyBE> GetCurrenyDAL(CurrencyBE CurrencyBE)
        {
            List<CurrencyBE> oSYS_CurrencyBEList = new List<CurrencyBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", CurrencyBE.Action);
                param[index++] = new SqlParameter("@CurrencyID", CurrencyBE.CurrencyId);
                param[index++] = new SqlParameter("@CurrencyName", CurrencyBE.CurrencyName);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Currency", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    CurrencyBE oCurrencyBE = new CurrencyBE();
                    oCurrencyBE.CurrencyId = Convert.ToInt32(dr["CurrencyID"]);
                    oCurrencyBE.CurrencyName = dr["CurrencyName"].ToString();
                    oCurrencyBE.DateCreated = dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : (DateTime?)null;
                    oCurrencyBE.CreatedBy = dr["CreatedBy"].ToString();

                    oSYS_CurrencyBEList.Add(oCurrencyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oSYS_CurrencyBEList;
        }



        public int? addEditCurrencyDAL(CurrencyBE CurrencyBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", CurrencyBE.Action);
                param[index++] = new SqlParameter("@CurrencyName", CurrencyBE.CurrencyName);
                param[index++] = new SqlParameter("@CreatedBy", CurrencyBE.CreatedBy);


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_Currency", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

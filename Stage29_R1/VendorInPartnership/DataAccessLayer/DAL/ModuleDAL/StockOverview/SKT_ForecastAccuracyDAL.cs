﻿using BusinessEntities.ModuleBE.StockOverview;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;
namespace DataAccessLayer.ModuleDAL.StockOverview
{
    public class SKT_ForecastAccuracyDAL
    {
        public void GetForcastAccuracyDataDAL(STK_ForecastAccuracyBE ForecastAccuracyBE, out List<STK_ForecastAccuracyBE> forecastAccBEList, out int RecordCount)
        {
            List<STK_ForecastAccuracyBE> forecastAccuracyBEList = new List<STK_ForecastAccuracyBE>();
            SqlParameter[] param = new SqlParameter[7];
            try
            {
                DataTable dt = new DataTable();
                int index = 0;

                param[index++] = new SqlParameter("@Action", ForecastAccuracyBE.Action);
                param[index++] = new SqlParameter("@CountryIds", ForecastAccuracyBE.CountryIds);
                param[index++] = new SqlParameter("@VendorIds", ForecastAccuracyBE.VendorIds);
                param[index++] = new SqlParameter("@ItemCategory", ForecastAccuracyBE.ItemCategoryIds);
                param[index++] = new SqlParameter("@ItemClassification", ForecastAccuracyBE.ItemClassification);
                param[index++] = new SqlParameter("@PageIndex", ForecastAccuracyBE.PageSize);
                param[index++] = new SqlParameter("@RecordCount", DbType.Int32);
                param[index - 1].Direction = ParameterDirection.Output;
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "sp_GetForecastAccuracyReport", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    STK_ForecastAccuracyBE oNewforecastAccuracyBE = new STK_ForecastAccuracyBE();
                    oNewforecastAccuracyBE.VendorNo = dr["Vendor_No"] != DBNull.Value ? dr["Vendor_No"].ToString() : "";
                    oNewforecastAccuracyBE.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : "";
                    oNewforecastAccuracyBE.Country = dr["Country"] != DBNull.Value ? dr["Country"].ToString() : "";
                    oNewforecastAccuracyBE.OD_Sku = dr["OD_SKU"] != DBNull.Value ? dr["OD_SKU"].ToString() : "";
                    oNewforecastAccuracyBE.Viking_Sku = dr["Viking_SKU"] != DBNull.Value ? dr["Viking_SKU"].ToString() : "";
                    oNewforecastAccuracyBE.Description = dr["DESCRIPTION"] != DBNull.Value ? dr["DESCRIPTION"].ToString() : "";
                    oNewforecastAccuracyBE.ItemClassification = dr["Item_classification"] != DBNull.Value ? dr["Item_classification"].ToString() : "";
                    oNewforecastAccuracyBE.ItemCategory = dr["Category"] != DBNull.Value ? dr["Category"].ToString() : "";
                    oNewforecastAccuracyBE.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : "";
                    oNewforecastAccuracyBE.Next20DaysForecastValue = dr["Next20DaysForecastValue"] != DBNull.Value ? Convert.ToDecimal(dr["Next20DaysForecastValue"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.YTDForecastDemand = dr["YTDForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["YTDForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.YTDActualSale = dr["YTDActualSale"] != DBNull.Value ? Convert.ToDecimal(dr["YTDActualSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.YTDAbsoluteDiff = dr["YTDAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["YTDAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.YTDForecastedAcuracyRate = dr["YTDForecastedAcuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["YTDForecastedAcuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.JanForecastDemand = dr["JanForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["JanForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.JanActualSale = dr["JanActualSale"] != DBNull.Value ? Convert.ToDecimal(dr["JanActualSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.JanAbsoluteDiff = dr["JanAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["JanAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.JanForecastedAcuracyRate = dr["JanForecastedAcuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["JanForecastedAcuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.FebForecastDemand = dr["FebForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["FebForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.FebActualSale = dr["FebActualSale"] != DBNull.Value ? Convert.ToDecimal(dr["FebActualSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.FebAbsoluteDiff = dr["FebAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["FebAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.FebForecastedAcuracyRate = dr["FebForecastedAcuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["FebForecastedAcuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.MarForecastDemand = dr["MarForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["MarForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.MarActualSale = dr["MarActualSale"] != DBNull.Value ? Convert.ToDecimal(dr["MarActualSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.MarAbsoluteDiff = dr["MarAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["MarAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.MarForecastedAcuracyRate = dr["MarForecastedAcuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["MarForecastedAcuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.AprForecastDemand = dr["AprForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["AprForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.AprActualSale = dr["AprActualSale"] != DBNull.Value ? Convert.ToDecimal(dr["AprActualSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.AprAbsoluteDiff = dr["AprAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["AprAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.AprForecastedAcuracyRate = dr["AprForecastedAcuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["AprForecastedAcuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.MayForecastDemand = dr["MayForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["MayForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.MayActualSale = dr["MayActualSale"] != DBNull.Value ? Convert.ToDecimal(dr["MayActualSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.MayAbsoluteDiff = dr["MayAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["MayAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.MayForecastedAcuracyRate = dr["MayForecastedAcuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["MayForecastedAcuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.JunForecastDemand = dr["JunForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["JunForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.JunActualSale = dr["JunActualSale"] != DBNull.Value ? Convert.ToDecimal(dr["JunActualSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.JunAbsoluteDiff = dr["JunAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["JunAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.JunForecastedAcuracyRate = dr["JunForecastedAcuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["JunForecastedAcuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.JulForecastDemand = dr["JulForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["JulForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.JulActualSale = dr["JulActualSale"] != DBNull.Value ? Convert.ToDecimal(dr["JulActualSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.JulAbsoluteDiff = dr["JulAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["JulAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.JulForecastedAcuracyRate = dr["JulForecastedAcuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["JulForecastedAcuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.AugForecastDemand = dr["AugForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["AugForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.AugActualSale = dr["AugActualSale"] != DBNull.Value ? Convert.ToDecimal(dr["AugActualSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.AugAbsoluteDiff = dr["AugAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["AugAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.AugForecastedAcuracyRate = dr["AugForecastedAcuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["AugForecastedAcuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.SepForecastDemand = dr["SepForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["SepForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.SepActualSale = dr["SepActualSale"] != DBNull.Value ? Convert.ToDecimal(dr["SepActualSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.SepAbsoluteDiff = dr["SepAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["SepAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.SepForecastedAcuracyRate = dr["SepForecastedAcuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["SepForecastedAcuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.OctForecastDemand = dr["OctForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["OctForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.OctActualSale = dr["OctActualSale"] != DBNull.Value ? Convert.ToDecimal(dr["OctActualSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.OctAbsoluteDiff = dr["OctAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["OctAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.OctForecastedAcuracyRate = dr["OctForecastedAcuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["OctForecastedAcuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.NovForecastDemand = dr["NovForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["NovForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.NovActualSale = dr["NovActualSale"] != DBNull.Value ? Convert.ToDecimal(dr["NovActualSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.NovAbsoluteDiff = dr["NovAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["NovAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.NovForecastedAcuracyRate = dr["NovForecastedAcuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["NovForecastedAcuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.DecForecastDemand = dr["DecForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["DecForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.DecActualSale = dr["DecActualSale"] != DBNull.Value ? Convert.ToDecimal(dr["DecActualSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.DecAbsoluteDiff = dr["DecAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["DecAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.DecForecastedAcuracyRate = dr["DecForecastedAcuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["DecForecastedAcuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.YTDAvg = dr["YTDAvg"] != DBNull.Value ? Convert.ToDecimal(dr["YTDAvg"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.JanAvg = dr["JanAvg"] != DBNull.Value ? Convert.ToDecimal(dr["JanAvg"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.FebAvg = dr["FebAvg"] != DBNull.Value ? Convert.ToDecimal(dr["FebAvg"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.MarAvg = dr["MarAvg"] != DBNull.Value ? Convert.ToDecimal(dr["MarAvg"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.AprAvg = dr["AprAvg"] != DBNull.Value ? Convert.ToDecimal(dr["AprAvg"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.MayAvg = dr["MayAvg"] != DBNull.Value ? Convert.ToDecimal(dr["MayAvg"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.JunAvg = dr["JunAvg"] != DBNull.Value ? Convert.ToDecimal(dr["JunAvg"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.JulAvg = dr["JulAvg"] != DBNull.Value ? Convert.ToDecimal(dr["JulAvg"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.AugAvg = dr["AugAvg"] != DBNull.Value ? Convert.ToDecimal(dr["AugAvg"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.SepAvg = dr["SepAvg"] != DBNull.Value ? Convert.ToDecimal(dr["SepAvg"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.OctAvg = dr["OctAvg"] != DBNull.Value ? Convert.ToDecimal(dr["OctAvg"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.NovAvg = dr["NovAvg"] != DBNull.Value ? Convert.ToDecimal(dr["NovAvg"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.DecAvg = dr["DecAvg"] != DBNull.Value ? Convert.ToDecimal(dr["DecAvg"].ToString()) : (decimal?)null;
                    forecastAccuracyBEList.Add(oNewforecastAccuracyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            forecastAccBEList = forecastAccuracyBEList;
            RecordCount = Convert.ToInt32(param[6].Value);

        }


        public void GetLongTermForcastAccuracyDataDAL(STK_ForecastAccuracyBE ForecastAccuracyBE, out List<STK_ForecastAccuracyBE> lstForecastLongTerm, out int RecordCount)
        {
            List<STK_ForecastAccuracyBE> forecastAccuracyBEList = new List<STK_ForecastAccuracyBE>();
            SqlParameter[] param = new SqlParameter[7];
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                param[index++] = new SqlParameter("@Action", ForecastAccuracyBE.Action);
                param[index++] = new SqlParameter("@CountryIds", ForecastAccuracyBE.CountryIds);
                param[index++] = new SqlParameter("@VendorIds", ForecastAccuracyBE.VendorIds);
                param[index++] = new SqlParameter("@ItemCategory", ForecastAccuracyBE.ItemCategoryIds);
                param[index++] = new SqlParameter("@ItemClassification", ForecastAccuracyBE.ItemClassification);
                param[index++] = new SqlParameter("@PageIndex", ForecastAccuracyBE.PageSize);
                param[index++] = new SqlParameter("@RecordCount", DbType.Int32);
                param[index - 1].Direction = ParameterDirection.Output;
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "sp_GetForecastAccuracyReport", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    STK_ForecastAccuracyBE oNewforecastAccuracyBE = new STK_ForecastAccuracyBE();
                    oNewforecastAccuracyBE.VendorNo = dr["Vendor_No"] != DBNull.Value ? dr["Vendor_No"].ToString() : "";
                    oNewforecastAccuracyBE.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : "";
                    oNewforecastAccuracyBE.Country = dr["Country"] != DBNull.Value ? dr["Country"].ToString() : "";
                    oNewforecastAccuracyBE.OD_Sku = dr["OD_SKU"] != DBNull.Value ? dr["OD_SKU"].ToString() : "";
                    oNewforecastAccuracyBE.Viking_Sku = dr["Viking_SKU"] != DBNull.Value ? dr["Viking_SKU"].ToString() : "";
                    oNewforecastAccuracyBE.Description = dr["DESCRIPTION"] != DBNull.Value ? dr["DESCRIPTION"].ToString() : "";
                    oNewforecastAccuracyBE.ItemClassification = dr["Item_classification"] != DBNull.Value ? dr["Item_classification"].ToString() : "";
                    oNewforecastAccuracyBE.ItemCategory = dr["CategoryName"] != DBNull.Value ? dr["CategoryName"].ToString() : "";
                    oNewforecastAccuracyBE.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : "";
                    oNewforecastAccuracyBE.HForecastDemand = dr["HForecastDemand"] != DBNull.Value ? Convert.ToDecimal(dr["HForecastDemand"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.HActualSale = dr["HAcutalSale"] != DBNull.Value ? Convert.ToDecimal(dr["HAcutalSale"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.HAbsoluteDiff = dr["HAbsoluteDiff"] != DBNull.Value ? Convert.ToDecimal(dr["HAbsoluteDiff"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.HForecastedAcuracyRate = dr["HForecastAccuracyRate"] != DBNull.Value ? Convert.ToDecimal(dr["HForecastAccuracyRate"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.Next12MonthForecastDemand = dr["Nxt12MonthForecast"] != DBNull.Value ? Convert.ToDecimal(dr["Nxt12MonthForecast"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.Month1 = dr["month1"] != DBNull.Value ? Convert.ToDecimal(dr["month1"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.Month2 = dr["month2"] != DBNull.Value ? Convert.ToDecimal(dr["month2"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.Month3 = dr["month3"] != DBNull.Value ? Convert.ToDecimal(dr["month3"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.Month4 = dr["month4"] != DBNull.Value ? Convert.ToDecimal(dr["month4"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.Month5 = dr["month5"] != DBNull.Value ? Convert.ToDecimal(dr["month5"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.Month6 = dr["month6"] != DBNull.Value ? Convert.ToDecimal(dr["month6"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.Month7 = dr["month7"] != DBNull.Value ? Convert.ToDecimal(dr["month7"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.Month8 = dr["month8"] != DBNull.Value ? Convert.ToDecimal(dr["month8"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.Month9 = dr["month9"] != DBNull.Value ? Convert.ToDecimal(dr["month9"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.Month10 = dr["month10"] != DBNull.Value ? Convert.ToDecimal(dr["month10"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.Month11 = dr["month11"] != DBNull.Value ? Convert.ToDecimal(dr["month11"].ToString()) : (decimal?)null;

                    oNewforecastAccuracyBE.Month12 = dr["month12"] != DBNull.Value ? Convert.ToDecimal(dr["month12"].ToString()) : (decimal?)null;
                    oNewforecastAccuracyBE.HAvg = dr["HAvg"] != DBNull.Value ? Convert.ToDecimal(dr["HAvg"].ToString()) : (decimal?)null;

                    forecastAccuracyBEList.Add(oNewforecastAccuracyBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            lstForecastLongTerm = forecastAccuracyBEList;
            RecordCount = Convert.ToInt32(param[6].Value);
        }

        public void GetForcastAccuracyDataExportDAL(STK_ForecastAccuracyBE ForecastAccuracyBE, out DataTable forecastAccBEList, out int RecordCount)
        {
            List<STK_ForecastAccuracyBE> forecastAccuracyBEList = new List<STK_ForecastAccuracyBE>();
            SqlParameter[] param = new SqlParameter[7];
            try
            {
                DataTable dt = new DataTable();
                int index = 0;

                param[index++] = new SqlParameter("@Action", ForecastAccuracyBE.Action);
                param[index++] = new SqlParameter("@CountryIds", ForecastAccuracyBE.CountryIds);
                param[index++] = new SqlParameter("@VendorIds", ForecastAccuracyBE.VendorIds);
                param[index++] = new SqlParameter("@ItemCategory", ForecastAccuracyBE.ItemCategoryIds);
                param[index++] = new SqlParameter("@ItemClassification", ForecastAccuracyBE.ItemClassification);
                param[index++] = new SqlParameter("@PageIndex", ForecastAccuracyBE.PageSize);
                param[index++] = new SqlParameter("@RecordCount", DbType.Int32);
                param[index - 1].Direction = ParameterDirection.Output;
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "sp_GetForecastAccuracyReport", param);
                dt = ds.Tables[0];
                forecastAccBEList = dt;
                RecordCount = Convert.ToInt32(param[6].Value);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                forecastAccBEList = null;
                RecordCount = 0;
            }
        }
    }
}

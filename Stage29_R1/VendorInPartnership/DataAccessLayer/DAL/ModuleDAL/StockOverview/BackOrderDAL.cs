﻿using BusinessEntities.ModuleBE.StockOverview;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.StockOverview
{
    public class BackOrderDAL
    {
        /// <summary>
        /// List Of all Vendor to all Penalty
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public List<BackOrderBE> GetVendorPenaltyDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@VendorId", BackOrderBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SelectedCountryIDs", BackOrderBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", BackOrderBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@IsVendorSubscribeToPenalty", BackOrderBE.IsVendorSubscribeToPenalty);
                param[index++] = new SqlParameter("@GridCurrentPageNo", BackOrderBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", BackOrderBE.GridPageSize);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();


                    oBackOrderBE.Vendor.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oBackOrderBE.Vendor.Vendor_No = dr["Vendor_No"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_No"]);
                    oBackOrderBE.Vendor.VendorName = dr["Vendor_Name"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Name"]);
                    oBackOrderBE.Vendor.VendorCountryName = dr["Vendor_Country"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Country"]);

                    oBackOrderBE.PenaltyID = dr["PenaltyID"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["PenaltyID"]);
                    oBackOrderBE.PenaltyType = dr["PenaltyType"] == DBNull.Value ? null : Convert.ToString(dr["PenaltyType"]);
                    oBackOrderBE.IsVendorSubscribeToPenalty = dr["IsVendorSubscribeToPenalty"] == DBNull.Value ? null : Convert.ToString(dr["IsVendorSubscribeToPenalty"]);
                    oBackOrderBE.PenaltyType = dr["PenaltyType"] == DBNull.Value ? null : Convert.ToString(dr["PenaltyType"]);
                    oBackOrderBE.OTIFRate = dr["OTIFRate"] == DBNull.Value ? (double?)null : Convert.ToDouble(dr["OTIFRate"]);
                    oBackOrderBE.OTIFLineValuePenalty = dr["OTIFLineValuePenalty"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["OTIFLineValuePenalty"]);
                    oBackOrderBE.ValueOfBackOrder = dr["ValueOfBackOrder"] == DBNull.Value ? (double?)null : Convert.ToDouble(dr["ValueOfBackOrder"]);

                    oBackOrderBE.Currency = new CurrencyBE();
                    oBackOrderBE.Currency.CurrencyId = dr["CurrencyID"] != DBNull.Value ? Convert.ToInt32(dr["CurrencyID"]) : 0;
                    oBackOrderBE.Currency.CurrencyName = dr["CurrencyName"] == DBNull.Value ? null : Convert.ToString(dr["CurrencyName"]);
                    oBackOrderBE.PenaltyAppliedId = dr["PenaltyAppliedId"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["PenaltyAppliedId"]);
                    oBackOrderBE.PenaltyAppliedName = dr["PenaltyAppliedName"] == DBNull.Value ? null : Convert.ToString(dr["PenaltyAppliedName"]);


                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNumber"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNumber"]);

                    oBackOrderBE.LocalConslolidation = dr["LocalConslolidation"] == DBNull.Value ? null : Convert.ToString(dr["LocalConslolidation"]);
                    oBackOrderBE.EUConsolidation = dr["EUConsolidation"] == DBNull.Value ? null : Convert.ToString(dr["EUConsolidation"]);

                    if (dr.Table.Columns.Contains("TotalRecords"))
                        oBackOrderBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;

                    if (dr.Table.Columns.Contains("Lines"))
                        oBackOrderBE.Lines = dr["Lines"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["Lines"]);

                    if (dr.Table.Columns.Contains("Weightage"))
                        oBackOrderBE.Weightage = dr["Weightage"] == DBNull.Value ? null : Convert.ToString(dr["Weightage"]);

                    if (dr.Table.Columns.Contains("SpendAmount"))
                        oBackOrderBE.SpendAmount = dr["SpendAmount"] == DBNull.Value ? null : Convert.ToString(dr["SpendAmount"]);

                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        /// <summary>
        /// Subscribe and add Penalty Type of Vendor
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public int? AddEditVendorPenaltyDAL(BackOrderBE BackOrderBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@VendorID", BackOrderBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@IsVendorSubscribeToPenalty", BackOrderBE.IsVendorSubscribeToPenalty);
                param[index++] = new SqlParameter("@PenaltyType", BackOrderBE.PenaltyType);
                param[index++] = new SqlParameter("@ValueOfBackOrder", BackOrderBE.ValueOfBackOrder);
                param[index++] = new SqlParameter("@CurrencyID", BackOrderBE.Currency.CurrencyId);
                param[index++] = new SqlParameter("@OTIFRate", BackOrderBE.OTIFRate);
                param[index++] = new SqlParameter("@OTIFLineValuePenalty", BackOrderBE.OTIFLineValuePenalty);
                param[index++] = new SqlParameter("@PenaltyAppliedBy", BackOrderBE.PenaltyAppliedId);
                if (BackOrderBE.Action.ToLower() == "updatevendorpenalty")
                {
                    param[index++] = new SqlParameter("@ModifiedBy", BackOrderBE.PenaltyModifiedId);
                }



                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorPenalty", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Returns list of Vendors whose penalty needs to be Approved
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public List<BackOrderBE> GetPendingPenaltyApprovalDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", BackOrderBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", BackOrderBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@UserID", BackOrderBE.User.UserID);
                param[index++] = new SqlParameter("@RoleName", BackOrderBE.User.RoleName);
                param[index++] = new SqlParameter("@GridCurrentPageNo", BackOrderBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", BackOrderBE.GridPageSize);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();


                    oBackOrderBE.Vendor.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oBackOrderBE.Vendor.Vendor_No = dr["Vendor_No"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_No"]);
                    oBackOrderBE.Vendor.VendorName = dr["Vendor_Name"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Name"]);
                    oBackOrderBE.Vendor.VendorCountryName = dr["Vendor_Country"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Country"]);
                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNumber"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNumber"]);
                    oBackOrderBE.CurrentMonthCount = dr["CurrentMonth"] != DBNull.Value ? Convert.ToInt32(dr["CurrentMonth"]) : 0;
                    oBackOrderBE.PreviousMonthCount = dr["PreviousMonth"] != DBNull.Value ? Convert.ToInt32(dr["PreviousMonth"]) : 0;


                    if (dr.Table.Columns.Contains("TotalRecords"))
                        oBackOrderBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;

                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        /// <summary>
        /// Returns List of Open BAck Order..Used on Inventory Review 1 above grid
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public List<BackOrderBE> GetOpenBackOrderDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[25];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedSiteIds", BackOrderBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", BackOrderBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedODSKUCode", BackOrderBE.SelectedODSkUCode);
                param[index++] = new SqlParameter("@InventoryReviewStatus", BackOrderBE.InventoryReviewStatus);
                param[index++] = new SqlParameter("@VendorBOReviewStatus", BackOrderBE.VendorBOReviewStatus);
                param[index++] = new SqlParameter("@VendorID", BackOrderBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@UserID", BackOrderBE.User.UserID);
                param[index++] = new SqlParameter("@RoleName", BackOrderBE.User.RoleName);
                param[index++] = new SqlParameter("@GridCurrentPageNo", BackOrderBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", BackOrderBE.GridPageSize);
                param[index++] = new SqlParameter("@SelectedCountryIDs", BackOrderBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@StockPlannerGroupings", BackOrderBE.StockPlannerGroupings);
                if (BackOrderBE.SelectedDateTo.Year != 1)
                {
                    param[index++] = new SqlParameter("@SelectedTo", BackOrderBE.SelectedDateTo);
                    param[index++] = new SqlParameter("@SelectedFrom", BackOrderBE.SelectedDateFrom);
                }

                param[index++] = new SqlParameter("@Purchase_order", BackOrderBE.PurchaseOrder.Purchase_order);
                param[index++] = new SqlParameter("@MediatorBOReviewStatus", BackOrderBE.MediatorBOReviewStatus);
                param[index++] = new SqlParameter("@ViewByTotalsPenaltiesCharged", BackOrderBE.ViewByTotalsPenaltiesCharged);
                param[index++] = new SqlParameter("@ViewByTotalsPenaltiesDeclined", BackOrderBE.ViewByTotalsPenaltiesDeclined);
                param[index++] = new SqlParameter("@ViewByTotalsPenaltiesPending", BackOrderBE.ViewByTotalsPenaltiesPending);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
                    oBackOrderBE.DeclineCode = new BusinessEntities.ModuleBE.StockOverview.MAS_DeclineReasonCodeBE();
                    oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();



                    oBackOrderBE.SKU.OD_SKU_NO = dr["OD_SKU_NO"] == DBNull.Value ? null : Convert.ToString(dr["OD_SKU_NO"]);
                    oBackOrderBE.SKU.Direct_SKU = dr["VikingSKU"] == DBNull.Value ? null : Convert.ToString(dr["VikingSKU"]);
                    oBackOrderBE.SKU.Vendor_Code = dr["Vendor_Code"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Code"]);
                    oBackOrderBE.SKU.DESCRIPTION = dr["DESCRIPTION"] == DBNull.Value ? null : Convert.ToString(dr["DESCRIPTION"]);
                    oBackOrderBE.NoofBO = dr["NumberofBO"] != DBNull.Value ? Convert.ToInt32(dr["NumberofBO"]) : 0;
                    oBackOrderBE.QtyonBackOrder = dr["QtyonBackOrder"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["QtyonBackOrder"]);
                    oBackOrderBE.NoofBOWithPenalities = dr["NoofBOwithPenalties"] != DBNull.Value ? Convert.ToInt32(dr["NoofBOwithPenalties"]) : 0;
                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.Vendor.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oBackOrderBE.Vendor.Vendor_No = dr["VendorNo"] == DBNull.Value ? null : Convert.ToString(dr["VendorNo"]);
                    oBackOrderBE.Vendor.VendorName = dr["VendorName"] == DBNull.Value ? null : Convert.ToString(dr["VendorName"]);
                    oBackOrderBE.PotentialPenaltyCharge = dr["PotentialPenaltyCharge"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["PotentialPenaltyCharge"]);
                    if (BackOrderBE.InventoryReviewStatus == "Declined" || BackOrderBE.InventoryReviewStatus == "Approved")
                    {
                        oBackOrderBE.InventoryReviewStatus = dr["OriginalIRStatus"] == DBNull.Value ? null : Convert.ToString(dr["OriginalIRStatus"]);
                    }
                    else
                    {
                        oBackOrderBE.InventoryReviewStatus = dr["InventoryReviewStatus"] == DBNull.Value ? null : Convert.ToString(dr["InventoryReviewStatus"]);
                    }
                    oBackOrderBE.IsVendorCommuncationSent = dr["IsVendorCommuncationSent"] == DBNull.Value ? null : Convert.ToString(dr["IsVendorCommuncationSent"]);

                    if (dr.Table.Columns.Contains("SiteName"))
                        oBackOrderBE.SKU.SiteName = dr["SiteName"] == DBNull.Value ? null : Convert.ToString(dr["SiteName"]);


                    if (dr.Table.Columns.Contains("TotalRecords"))
                        oBackOrderBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;
                    if (dr.Table.Columns.Contains("siteid"))
                        oBackOrderBE.SiteID = dr["siteid"] != DBNull.Value ? Convert.ToInt32(dr["siteid"]) : 0;

                    if (dr.Table.Columns.Contains("InventoryInitialReview"))
                        oBackOrderBE.InventoryInitialReview = dr["InventoryInitialReview"] == DBNull.Value ? null : Convert.ToString(dr["InventoryInitialReview"]);

                    if (dr.Table.Columns.Contains("DeclineUser"))
                        oBackOrderBE.PlannerInitialUser = dr["DeclineUser"] == DBNull.Value ? dr["PlannerInitialUser"] == DBNull.Value ? null : Convert.ToString(dr["PlannerInitialUser"]) : Convert.ToString(dr["DeclineUser"]);


                    if (dr.Table.Columns.Contains("InventoryActionDate"))
                        oBackOrderBE.InventoryInitialActionDate = dr["InventoryActionDate"] == DBNull.Value ? null : Convert.ToString(dr["InventoryActionDate"]);

                    if (dr.Table.Columns.Contains("VendorReview"))
                        oBackOrderBE.VendorReview = dr["VendorReview"] == DBNull.Value ? null : Convert.ToString(dr["VendorReview"]);

                    if (dr.Table.Columns.Contains("VendorAmount"))
                        oBackOrderBE.VendorAmount = dr["VendorAmount"] != DBNull.Value ? Convert.ToDecimal(dr["VendorAmount"]) : (decimal?)null;

                    if (dr.Table.Columns.Contains("VendorUser"))
                        oBackOrderBE.VendorUser = dr["VendorUser"] == DBNull.Value ? null : Convert.ToString(dr["VendorUser"]);

                    if (dr.Table.Columns.Contains("VendorActionDate"))
                        oBackOrderBE.VendorActionTakenDate = dr["VendorActionDate"] == DBNull.Value ? null : Convert.ToString(dr["VendorActionDate"]);


                    if (dr.Table.Columns.Contains("PlannerSecondaryReview"))
                        oBackOrderBE.PlannerSecondaryReview = dr["PlannerSecondaryReview"] == DBNull.Value ? null : Convert.ToString(dr["PlannerSecondaryReview"]);

                    if (dr.Table.Columns.Contains("PlannerAmount"))
                        oBackOrderBE.PlannerAmount = dr["PlannerAmount"] != DBNull.Value ? Convert.ToDecimal(dr["PlannerAmount"]) : (decimal?)null;

                    if (dr.Table.Columns.Contains("DisputeUser"))
                        oBackOrderBE.PlannerSecondaryUser = dr["DisputeUser"] == DBNull.Value ? null : Convert.ToString(dr["DisputeUser"]);

                    if (dr.Table.Columns.Contains("DisputeReviewActionDate"))
                        oBackOrderBE.PlannerSecondaryActionTakenDate = dr["DisputeReviewActionDate"] == DBNull.Value ? null : Convert.ToString(dr["DisputeReviewActionDate"]);


                    if (dr.Table.Columns.Contains("MediatorReview"))
                        oBackOrderBE.MediatorReview = dr["MediatorReview"] == DBNull.Value ? null : Convert.ToString(dr["MediatorReview"]);

                    if (dr.Table.Columns.Contains("MediatorAmount"))
                        oBackOrderBE.MediatorAmount = dr["MediatorAmount"] != DBNull.Value ? Convert.ToDecimal(dr["MediatorAmount"]) : (decimal?)null;

                    if (dr.Table.Columns.Contains("MediatorUser"))
                        oBackOrderBE.MediatorUser = dr["MediatorUser"] == DBNull.Value ? null : Convert.ToString(dr["MediatorUser"]);

                    if (dr.Table.Columns.Contains("MediatorActionDate"))
                        oBackOrderBE.MediatorActionTakenDate = dr["MediatorActionDate"] == DBNull.Value ? null : Convert.ToString(dr["MediatorActionDate"]);


                    if (dr.Table.Columns.Contains("Date"))
                        oBackOrderBE.LastBackOrderDate = dr["Date"] == DBNull.Value ? null : Convert.ToString(dr["Date"]);


                    if (dr.Table.Columns.Contains("PlannerInitialActionDays"))
                        oBackOrderBE.PlannerInitialActionDays = dr["PlannerInitialActionDays"] != DBNull.Value ? Convert.ToInt32(dr["PlannerInitialActionDays"]) : 0;

                    if (dr.Table.Columns.Contains("VendorActionDays"))
                        oBackOrderBE.VendorActionDays = dr["VendorActionDays"] != DBNull.Value ? Convert.ToInt32(dr["VendorActionDays"]) : 0;

                    if (dr.Table.Columns.Contains("PlannerSecondaryActionDays"))
                        oBackOrderBE.PlannerSecondaryActionDays = dr["PlannerSecondaryActionDays"] != DBNull.Value ? Convert.ToInt32(dr["PlannerSecondaryActionDays"]) : 0;

                    if (dr.Table.Columns.Contains("MediatorActionDays"))
                        oBackOrderBE.MediatorActionDays = dr["MediatorActionDays"] != DBNull.Value ? Convert.ToInt32(dr["MediatorActionDays"]) : 0;

                    if (dr.Table.Columns.Contains("CompletedDate"))
                        oBackOrderBE.CompletedDate = dr["CompletedDate"] == DBNull.Value ? null : Convert.ToString(dr["CompletedDate"]);

                    if (dr.Table.Columns.Contains("TotalDays"))
                        oBackOrderBE.TotalDays = dr["TotalDays"] != DBNull.Value ? Convert.ToInt32(dr["TotalDays"]) : 0;

                    if (dr.Table.Columns.Contains("OriginalIRStatus"))
                    {
                        if (dr["OriginalIRStatus"].Equals("Declined") || string.IsNullOrEmpty(dr["OriginalIRStatus"].ToString()) || dr["OriginalIRStatus"].Equals("Approved"))
                        {
                            if (dr.Table.Columns.Contains("CurrentInventoryStatus"))
                                oBackOrderBE.CurrentStatus = dr["CurrentInventoryStatus"] == DBNull.Value ? null : Convert.ToString(dr["CurrentInventoryStatus"]);
                        }
                        else
                        {
                            if (dr.Table.Columns.Contains("CurrentStatus"))
                                oBackOrderBE.CurrentStatus = dr["CurrentStatus"] == DBNull.Value ? null : Convert.ToString(dr["CurrentStatus"]);
                        }
                    }

                    if (dr.Table.Columns.Contains("StockPlannerGroupings"))
                        oBackOrderBE.StockPlannerGroupings = dr["StockPlannerGroupings"] != DBNull.Value ? Convert.ToString(dr["StockPlannerGroupings"]) : string.Empty;

                    if (dr.Table.Columns.Contains("ReasonCode"))
                        oBackOrderBE.DeclineCode.ReasonCode = dr["ReasonCode"] != DBNull.Value ? Convert.ToString(dr["ReasonCode"]) : string.Empty;

                    if (dr.Table.Columns.Contains("VendorComment"))
                        oBackOrderBE.VendorComment = dr["VendorComment"] != DBNull.Value ? Convert.ToString(dr["VendorComment"]) : string.Empty;

                    if (dr.Table.Columns.Contains("SPComment"))
                        oBackOrderBE.SPComment = dr["SPComment"] != DBNull.Value ? Convert.ToString(dr["SPComment"]) : string.Empty;

                    if (dr.Table.Columns.Contains("MediatorComments"))
                        oBackOrderBE.MediatorComments = dr["MediatorComments"] != DBNull.Value ? Convert.ToString(dr["MediatorComments"]) : string.Empty;
                    if (dr.Table.Columns.Contains("PenaltyChargeID"))
                        oBackOrderBE.PenaltyChargeID = Convert.ToInt32(dr["PenaltyChargeID"]);
                    if (dr.Table.Columns.Contains("SKUID"))
                        oBackOrderBE.SKUID = dr["SKUID"] != DBNull.Value ? Convert.ToInt32(dr["SKUID"]) : 0;
                    //PurchaseOrder 
                    if (dr.Table.Columns.Contains("Purchase_Order"))
                        oBackOrderBE.PurchaseOrder.Purchase_order = dr["Purchase_Order"] != DBNull.Value ? Convert.ToString(dr["Purchase_Order"]) : string.Empty;

                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        /// <summary>
        /// Returns List of single Open Back Order..Used on Inventory Review 1 below grid
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public List<BackOrderBE> GetOpenBackOrderListingDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@OD_SKU_NO", BackOrderBE.SKU.OD_SKU_NO);
                param[index++] = new SqlParameter("@InventoryReviewStatus", BackOrderBE.InventoryReviewStatus);
                param[index++] = new SqlParameter("@VikingCode", BackOrderBE.SKU.Direct_SKU);
                param[index++] = new SqlParameter("@IsVendorCommuncationSent", BackOrderBE.IsVendorCommuncationSent);
                if (BackOrderBE.SelectedDateTo.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    param[index++] = new SqlParameter("@SelectedTo", BackOrderBE.SelectedDateTo);
                    param[index++] = new SqlParameter("@SelectedFrom", BackOrderBE.SelectedDateFrom);
                }
                param[index++] = new SqlParameter("@GridCurrentPageNo", BackOrderBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", BackOrderBE.GridPageSize);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
                    oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oBackOrderBE.PurchaseOrder.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                    oBackOrderBE.BOReportingID = Convert.ToInt32(dr["BOReportingID"]);
                    oBackOrderBE.SKU.OD_SKU_NO = dr["OD_SKU_NO"] == DBNull.Value ? null : Convert.ToString(dr["OD_SKU_NO"]);
                    oBackOrderBE.SKU.Direct_SKU = dr["VikingSKU"] == DBNull.Value ? null : Convert.ToString(dr["VikingSKU"]);
                    oBackOrderBE.SKU.DESCRIPTION = dr["DESCRIPTION"] == DBNull.Value ? null : Convert.ToString(dr["DESCRIPTION"]);
                    oBackOrderBE.Vendor.VendorID = Convert.ToInt32(dr["BOVendorID"]);
                    oBackOrderBE.Vendor.Vendor_No = dr["BOVendorNo"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorNo"]);
                    oBackOrderBE.Vendor.VendorName = dr["BOVendorName"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorName"]);
                    oBackOrderBE.BOIncurredDate = dr["DateBOIncurred"] != DBNull.Value ? Convert.ToDateTime(dr["DateBOIncurred"]) : (DateTime?)null;
                    oBackOrderBE.SKU.SiteName = dr["Site"] == DBNull.Value ? null : Convert.ToString(dr["Site"]);
                    oBackOrderBE.NoofBO = dr["NumberofBO"] != DBNull.Value ? Convert.ToInt32(dr["NumberofBO"]) : 0;
                    oBackOrderBE.QtyonBackOrder = dr["QtyonBackOrder"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["QtyonBackOrder"]);
                    oBackOrderBE.NoofBOWithPenalities = dr["NoofBOwithPenalties"] != DBNull.Value ? Convert.ToInt32(dr["NoofBOwithPenalties"]) : 0;
                    oBackOrderBE.PurchaseOrder.Purchase_order = dr["OverduePO"] == DBNull.Value ? null : Convert.ToString(dr["OverduePO"]);
                    oBackOrderBE.PurchaseOrder.Outstanding_Qty = dr["OutstandingQty"] == DBNull.Value ? null : Convert.ToString(dr["OutstandingQty"]);
                    oBackOrderBE.PurchaseOrder.Order_raised = dr["DateRaisedDate"] != DBNull.Value ? Convert.ToDateTime(dr["DateRaisedDate"]) : (DateTime?)null;
                    oBackOrderBE.PurchaseOrder.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    oBackOrderBE.PurchaseOrder.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevisedDueDate"]) : (DateTime?)null;
                    oBackOrderBE.PurchaseOrder.Vendor.VendorID = Convert.ToInt32(dr["POVendorID"]);
                    oBackOrderBE.PurchaseOrder.Vendor.Vendor_No = dr["POVendorNO"] == DBNull.Value ? null : Convert.ToString(dr["POVendorNO"]);
                    oBackOrderBE.PurchaseOrder.Vendor.VendorName = dr["POVendorName"] == DBNull.Value ? null : Convert.ToString(dr["POVendorName"]);
                    oBackOrderBE.InventoryReviewStatus = dr["InventoryReviewStatus"] == DBNull.Value ? null : Convert.ToString(dr["InventoryReviewStatus"]);
                    oBackOrderBE.IsVendorCommuncationSent = dr["IsVendorCommuncationSent"] == DBNull.Value ? null : Convert.ToString(dr["IsVendorCommuncationSent"]);
                    oBackOrderBE.TotalQtyReceived = dr["TotalQtyReceived"] == DBNull.Value ? "-" : Convert.ToString(dr["TotalQtyReceived"]);
                    oBackOrderBE.LastReceiptDate = dr["LastReceiptDate"] == DBNull.Value ? "-" : Convert.ToString(dr["LastReceiptDate"]);


                    if (dr.Table.Columns.Contains("TotalRecords"))
                        oBackOrderBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;

                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        /// <summary>
        /// Used to update Inventory Review Status
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public int? UpdateODSKUStatusDAL(BackOrderBE BackOrderBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[14];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedODSkUCode", BackOrderBE.SelectedODSkUCode);
                param[index++] = new SqlParameter("@SelectedBOReportingID", BackOrderBE.SelectedBOReportingID);
                param[index++] = new SqlParameter("@InventoryReviewStatus", BackOrderBE.InventoryReviewStatus);
                param[index++] = new SqlParameter("@InventoryActionTakenBy", Convert.ToInt32(System.Web.HttpContext.Current.Session["UserID"]));
                param[index++] = new SqlParameter("@InventoryActionDate", DateTime.Now);
                param[index++] = new SqlParameter("@SiteID", BackOrderBE.SiteID);
                param[index++] = new SqlParameter("@CurrentBoStatus", BackOrderBE.CurrentBoStatus);
                if (BackOrderBE.InventoryReviewStatus != "Approved")
                {
                    param[index++] = new SqlParameter("@DeclineReasonCodeID", BackOrderBE.DeclineCode.DeclineReasonCodeID);
                    param[index++] = new SqlParameter("@DeclineReasonComments", BackOrderBE.Comments);
                }

                if (BackOrderBE.SelectedDateTo.Year != 1)
                {
                    param[index++] = new SqlParameter("@SelectedFrom", BackOrderBE.SelectedDateFrom);
                    param[index++] = new SqlParameter("@SelectedTo", BackOrderBE.SelectedDateTo);
                }
                param[index++] = new SqlParameter("@VendorID", BackOrderBE.VendorID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorPenalty", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Returns list to Vendor which are Accepted by Inventory
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public List<BackOrderBE> GetVendorViewDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[17];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedSiteIds", BackOrderBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedMonth", BackOrderBE.SelectedMonth);
                param[index++] = new SqlParameter("@VendorBOReviewStatus", BackOrderBE.VendorBOReviewStatus);

                param[index++] = new SqlParameter("@VikingCode", BackOrderBE.SelectedVikingSkUCode);
                param[index++] = new SqlParameter("@OD_SKU_No", BackOrderBE.SelectedODSkUCode);
                param[index++] = new SqlParameter("@Vendor_Code", BackOrderBE.SelectedVendorCode);

                param[index++] = new SqlParameter("@UserID", BackOrderBE.User.UserID);
                param[index++] = new SqlParameter("@RoleName", BackOrderBE.User.RoleName);
                param[index++] = new SqlParameter("@GridCurrentPageNo", BackOrderBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@IsReminderCommSent", BackOrderBE.IsReminderCommSent);
                param[index++] = new SqlParameter("@StockPlannerID", BackOrderBE.StockPlannerID);
                param[index++] = new SqlParameter("@MediatorBOReviewStatus", BackOrderBE.MediatorBOReviewStatus);

                param[index++] = new SqlParameter("@GridPageSize", BackOrderBE.GridPageSize);
                if (BackOrderBE.Action != "GetVendorViewDetail")
                {
                    if (BackOrderBE.SelectedDateTo.ToString("dd/MM/yyyy") != "01/01/0001")
                    {
                        param[index++] = new SqlParameter("@SelectedTo", BackOrderBE.SelectedDateTo);
                        param[index++] = new SqlParameter("@SelectedFrom", BackOrderBE.SelectedDateFrom);
                    }
                }

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];




                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
                    oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oBackOrderBE.PurchaseOrder.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                    oBackOrderBE.BOReportingID = Convert.ToInt32(dr["BOReportingID"]);
                    oBackOrderBE.SKU.OD_SKU_NO = dr["OD_SKU_NO"] == DBNull.Value ? null : Convert.ToString(dr["OD_SKU_NO"]);
                    oBackOrderBE.SKU.Direct_SKU = dr["VikingSKU"] == DBNull.Value ? null : Convert.ToString(dr["VikingSKU"]);
                    oBackOrderBE.SKU.Vendor_Code = dr["Vendor_Code"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Code"]);
                    oBackOrderBE.SKU.DESCRIPTION = dr["DESCRIPTION"] == DBNull.Value ? null : Convert.ToString(dr["DESCRIPTION"]);

                    if (dr.Table.Columns.Contains("PenaltyRelatingTo"))
                        oBackOrderBE.PenaltyRelatingTo = dr["PenaltyRelatingTo"] == DBNull.Value ? null : Convert.ToString(dr["PenaltyRelatingTo"]);
                    if (dr.Table.Columns.Contains("DateBOIncurred"))
                        oBackOrderBE.BOIncurredDate = dr["DateBOIncurred"] != DBNull.Value ? Convert.ToDateTime(dr["DateBOIncurred"]) : (DateTime?)null;
                    if (dr.Table.Columns.Contains("Year"))
                        oBackOrderBE.Year = dr["Year"] != DBNull.Value ? Convert.ToString(dr["Year"]) : null;

                    oBackOrderBE.SKU.SiteName = dr["Site"] == DBNull.Value ? null : Convert.ToString(dr["Site"]);
                    oBackOrderBE.NoofBO = dr["NumberofBO"] != DBNull.Value ? Convert.ToInt32(dr["NumberofBO"]) : 0;
                    oBackOrderBE.QtyonBackOrder = dr["QtyonBackOrder"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["QtyonBackOrder"]);

                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.VendorID = Convert.ToInt32(dr["BOVendorID"]);
                    oBackOrderBE.VendorID = Convert.ToInt32(dr["BOVendorID"]);
                    oBackOrderBE.Vendor.Vendor_No = dr["BOVendorNo"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorNo"]);
                    oBackOrderBE.vendor_No = dr["BOVendorNo"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorNo"]);
                    oBackOrderBE.Vendor.VendorName = dr["BOVendorName"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorName"]);
                    oBackOrderBE.Vendor_Country = dr["BOVendorNo"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorNo"]);
                    oBackOrderBE.PurchaseOrder.Purchase_order = dr["OverduePO"] == DBNull.Value ? null : Convert.ToString(dr["OverduePO"]);
                    oBackOrderBE.PurchaseOrder.Outstanding_Qty = dr["OutstandingQty"] == DBNull.Value ? null : Convert.ToString(dr["OutstandingQty"]);
                    oBackOrderBE.PurchaseOrder.Order_raised = dr["DateRaisedDate"] != DBNull.Value ? Convert.ToDateTime(dr["DateRaisedDate"]) : (DateTime?)null;
                    oBackOrderBE.PurchaseOrder.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    oBackOrderBE.PotentialPenaltyCharge = dr["PotentialPenaltyCharge"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["PotentialPenaltyCharge"]);

                    oBackOrderBE.VendorBOReviewStatus = dr["VendorBOReviewStatus"] == DBNull.Value ? null : Convert.ToString(dr["VendorBOReviewStatus"]);


                    if (dr.Table.Columns.Contains("TotalRecords"))
                        oBackOrderBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;
                    if (dr.Table.Columns.Contains("StockPlannerID"))
                        oBackOrderBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"]) : 0;
                    if (dr.Table.Columns.Contains("CurrentStatus"))
                        oBackOrderBE.CurrentStatus = dr["CurrentStatus"] == DBNull.Value ? null : Convert.ToString(dr["CurrentStatus"]);
                    if (dr.Table.Columns.Contains("Country"))
                        oBackOrderBE.Country = dr["Country"] != DBNull.Value ? Convert.ToString(dr["Country"]) : "";

                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        /// -----------DATA FOR POPUP------------------
        /// 
        public List<BackOrderBE> GetVendorViewDALPopUp(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedSiteIds", BackOrderBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedMonth", BackOrderBE.SelectedMonth);
                param[index++] = new SqlParameter("@VendorBOReviewStatus", BackOrderBE.VendorBOReviewStatus);
                param[index++] = new SqlParameter("@UserID", BackOrderBE.User.UserID);
                param[index++] = new SqlParameter("@RoleName", BackOrderBE.User.RoleName);
                param[index++] = new SqlParameter("@GridCurrentPageNo", BackOrderBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", BackOrderBE.GridPageSize);
                param[index++] = new SqlParameter("@BOReportingID", BackOrderBE.BOReportingID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];




                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
                    oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oBackOrderBE.PurchaseOrder.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                    oBackOrderBE.BOReportingID = Convert.ToInt32(dr["BOReportingID"]);
                    oBackOrderBE.SKU.OD_SKU_NO = dr["OD_SKU_NO"] == DBNull.Value ? null : Convert.ToString(dr["OD_SKU_NO"]);
                    // oBackOrderBE.SKU.Direct_SKU = dr["VikingSKU"] == DBNull.Value ? null : Convert.ToString(dr["VikingSKU"]);
                    oBackOrderBE.SKU.Vendor_Code = dr["Vendor_Code"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Code"]);
                    oBackOrderBE.VendorActionTakenName = dr["VendorActionUser"] == DBNull.Value ? null : Convert.ToString(dr["VendorActionUser"]);
                    oBackOrderBE.VendorActionDate = dr["VendorActionDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime((dr["VendorActionDate"]));
                    oBackOrderBE.DisputeReviewStatus = dr["DisputeReviewStatus"] == DBNull.Value ? null : Convert.ToString(dr["DisputeReviewStatus"]);
                    oBackOrderBE.PotentialPenaltyCharge = dr["PotentialPenaltyCharge"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["PotentialPenaltyCharge"]);
                    oBackOrderBE.DisputedValue = dr["DisputedValue"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["DisputedValue"]);
                    oBackOrderBE.DisputeReviewActionDate = dr["DisputeReviewActionDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime((dr["DisputeReviewActionDate"]));
                    oBackOrderBE.DisputeReviewActionTakenName = dr["DisputeReviewUser"] == DBNull.Value ? null : Convert.ToString(dr["DisputeReviewUser"]);
                    oBackOrderBE.SPComment = dr["SPComment"] == DBNull.Value ? null : Convert.ToString(dr["SPComment"]);
                    oBackOrderBE.VendorComment = dr["VendorComment"] == DBNull.Value ? null : Convert.ToString(dr["VendorComment"]);
                    oBackOrderBE.MediatorActionDate = dr["MediatorActionDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime((dr["MediatorActionDate"]));
                    oBackOrderBE.MediatorActionTakenName = dr["MediatorActionUser"] == DBNull.Value ? null : Convert.ToString(dr["MediatorActionUser"]);
                    oBackOrderBE.MediatorComments = dr["MediatorComments"] == DBNull.Value ? null : Convert.ToString(dr["MediatorComments"]);
                    oBackOrderBE.VendorComment = dr["VendorComment"] == DBNull.Value ? null : Convert.ToString(dr["VendorComment"]);
                    // oBackOrderBE.SKU.DESCRIPTION = dr["DESCRIPTION"] == DBNull.Value ? null : Convert.ToString(dr["DESCRIPTION"]);

                    //if (dr.Table.Columns.Contains("PenaltyRelatingTo"))
                    //    oBackOrderBE.PenaltyRelatingTo = dr["PenaltyRelatingTo"] == DBNull.Value ? null : Convert.ToString(dr["PenaltyRelatingTo"]);
                    //if (dr.Table.Columns.Contains("DateBOIncurred"))
                    //    oBackOrderBE.BOIncurredDate = dr["DateBOIncurred"] != DBNull.Value ? Convert.ToDateTime(dr["DateBOIncurred"]) : (DateTime?)null;
                    //if (dr.Table.Columns.Contains("Year"))
                    //    oBackOrderBE.Year = dr["Year"] != DBNull.Value ? Convert.ToString(dr["Year"]) : null;

                    //oBackOrderBE.SKU.SiteName = dr["Site"] == DBNull.Value ? null : Convert.ToString(dr["Site"]);
                    //oBackOrderBE.NoofBO = dr["NumberofBO"] != DBNull.Value ? Convert.ToInt32(dr["NumberofBO"]) : 0;
                    //oBackOrderBE.QtyonBackOrder = dr["QtyonBackOrder"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["QtyonBackOrder"]);

                    //oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    //oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    //oBackOrderBE.Vendor.VendorID = Convert.ToInt32(dr["BOVendorID"]);
                    //oBackOrderBE.Vendor.Vendor_No = dr["BOVendorNo"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorNo"]);
                    //oBackOrderBE.Vendor.VendorName = dr["BOVendorName"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorName"]);

                    //oBackOrderBE.PurchaseOrder.Purchase_order = dr["OverduePO"] == DBNull.Value ? null : Convert.ToString(dr["OverduePO"]);
                    //oBackOrderBE.PurchaseOrder.Outstanding_Qty = dr["OutstandingQty"] == DBNull.Value ? null : Convert.ToString(dr["OutstandingQty"]);
                    //oBackOrderBE.PurchaseOrder.Order_raised = dr["DateRaisedDate"] != DBNull.Value ? Convert.ToDateTime(dr["DateRaisedDate"]) : (DateTime?)null;
                    //oBackOrderBE.PurchaseOrder.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;
                    //oBackOrderBE.PotentialPenaltyCharge = dr["PotentialPenaltyCharge"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["PotentialPenaltyCharge"]);

                    //oBackOrderBE.VendorBOReviewStatus = dr["VendorBOReviewStatus"] == DBNull.Value ? null : Convert.ToString(dr["VendorBOReviewStatus"]);


                    //if (dr.Table.Columns.Contains("TotalRecords"))
                    //    oBackOrderBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;

                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        /// <summary>
        /// Used to update Vendor Review Status..(Accepted / Query)
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public int? UpdateVendorReviewStatusDAL(BackOrderBE BackOrderBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[18];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedBOReportingID", BackOrderBE.SelectedBOReportingID);
                param[index++] = new SqlParameter("@VendorBOReviewStatus", BackOrderBE.VendorBOReviewStatus);
                param[index++] = new SqlParameter("@VendorActionTakenBy", Convert.ToInt32(System.Web.HttpContext.Current.Session["UserID"]));
                param[index++] = new SqlParameter("@VendorActionDate", DateTime.Now);
                param[index++] = new SqlParameter("@SelectedBOIDPC", BackOrderBE.SelectedBOIDPC);

                if (BackOrderBE.VendorBOReviewStatus == "Query")
                {
                    param[index++] = new SqlParameter("@VendorID", BackOrderBE.Vendor.VendorID);
                    param[index++] = new SqlParameter("@OD_SKU_No", BackOrderBE.SKU.OD_SKU_NO);
                    param[index++] = new SqlParameter("@BOIncurredDate", BackOrderBE.BOIncurredDate);
                    param[index++] = new SqlParameter("@Month", BackOrderBE.SelectedMonth);
                    param[index++] = new SqlParameter("@Year", BackOrderBE.Year);
                    param[index++] = new SqlParameter("@TotalCountofBO", BackOrderBE.NoofBO);
                    param[index++] = new SqlParameter("@TotalPenaltyCharges", BackOrderBE.PotentialPenaltyCharge);
                    param[index++] = new SqlParameter("@DisputedValue", BackOrderBE.DisputedValue);
                    param[index++] = new SqlParameter("@RevisedPenalty", BackOrderBE.RevisedPenalty);
                    param[index++] = new SqlParameter("@VendorComment", BackOrderBE.VendorComment);
                    param[index++] = new SqlParameter("@PenaltyType", BackOrderBE.PenaltyType);
                    param[index++] = new SqlParameter("@BOReportingID", BackOrderBE.BOReportingID);

                }


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorPenalty", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Used on Dispute Review Main Screen
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public List<BackOrderBE> GetDisputeReviewListingDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", BackOrderBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedODSKUCode", BackOrderBE.SelectedODSkUCode);
                param[index++] = new SqlParameter("@DisputeReviewStatus", BackOrderBE.DisputeReviewStatus);
                param[index++] = new SqlParameter("@PenaltyType", BackOrderBE.PenaltyType);
                if (BackOrderBE.SelectedDateTo.Year != 1)
                {
                    if (BackOrderBE.SelectedDateTo.ToString("dd/MM/yyyy") != "01/01/0001")
                    {
                        param[index++] = new SqlParameter("@SelectedTo", BackOrderBE.SelectedDateTo);
                        param[index++] = new SqlParameter("@SelectedFrom", BackOrderBE.SelectedDateFrom);
                    }
                }
                param[index++] = new SqlParameter("@UserID", BackOrderBE.User.UserID);
                param[index++] = new SqlParameter("@RoleName", BackOrderBE.User.RoleName);
                param[index++] = new SqlParameter("@GridCurrentPageNo", BackOrderBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", BackOrderBE.GridPageSize);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();



                    oBackOrderBE.Vendor.VendorID = Convert.ToInt32(dr["BOVendorID"]);
                    oBackOrderBE.Vendor.Vendor_No = dr["BOVendorNo"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorNo"]);
                    oBackOrderBE.Vendor.VendorName = dr["BOVendorName"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorName"]);
                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.PotentialPenaltyCharge = dr["Valueofquery"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["Valueofquery"]);
                    oBackOrderBE.SelectedMonth = dr["Month"] == DBNull.Value ? null : Convert.ToString(dr["Month"]);
                    oBackOrderBE.PenaltyType = dr["PenaltyType"] == DBNull.Value ? null : Convert.ToString(dr["PenaltyType"]);
                    oBackOrderBE.DisputeReviewStatus = dr["Status"] == DBNull.Value ? null : Convert.ToString(dr["Status"]);
                    oBackOrderBE.Resolution = dr["Resolution"] == DBNull.Value ? null : Convert.ToString(dr["Resolution"]);
                    if (dr.Table.Columns.Contains("PenaltyChargesID"))
                        oBackOrderBE.PenaltyChargeID = Convert.ToInt32(dr["PenaltyChargesID"]);
                    if (dr.Table.Columns.Contains("TotalRecords"))
                        oBackOrderBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;
                    if (dr.Table.Columns.Contains("SiteName"))
                        oBackOrderBE.SKU.SiteName = dr["SiteName"] == DBNull.Value ? null : Convert.ToString(dr["SiteName"]);
                    if (dr.Table.Columns.Contains("OD_SKU_No"))
                        oBackOrderBE.SKU.OD_SKU_NO = dr["OD_SKU_No"] == DBNull.Value ? null : Convert.ToString(dr["OD_SKU_No"]);
                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        /// <summary>
        /// Used on Dispute and Esclation detail - query detail on grid
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public List<BackOrderBE> GetDisputeDetailDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[13];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@VendorID", BackOrderBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@PenaltyChargeID", BackOrderBE.PenaltyChargeID);//Used
                param[index++] = new SqlParameter("@SelectedMonth", BackOrderBE.SelectedMonth);
                param[index++] = new SqlParameter("@DisputeReviewStatus", BackOrderBE.DisputeReviewStatus);
                param[index++] = new SqlParameter("@Resolution", BackOrderBE.Resolution);
                param[index++] = new SqlParameter("@SiteId", BackOrderBE.SiteID);
                param[index++] = new SqlParameter("@InventoryActionDate", BackOrderBE.InventoryActionDate);
                param[index++] = new SqlParameter("@SKUId", BackOrderBE.SKUID);
                param[index++] = new SqlParameter("@BOIncurredDate", BackOrderBE.BOIncurredDate);
                param[index++] = new SqlParameter("@GridCurrentPageNo", BackOrderBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", BackOrderBE.GridPageSize);

                if (BackOrderBE.SKU != null)
                {
                    param[index++] = new SqlParameter("@OD_SKU_No", BackOrderBE.SKU.OD_SKU_NO);
                }

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
                    oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oBackOrderBE.PurchaseOrder.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                    oBackOrderBE.BOReportingID = Convert.ToInt32(dr["BOReportingID"]);
                    oBackOrderBE.SKU.OD_SKU_NO = dr["OD_SKU_NO"] == DBNull.Value ? null : Convert.ToString(dr["OD_SKU_NO"]);
                    oBackOrderBE.SKU.Direct_SKU = dr["VikingSKU"] == DBNull.Value ? null : Convert.ToString(dr["VikingSKU"]);
                    oBackOrderBE.SKU.Vendor_Code = dr["Vendor_Code"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Code"]);
                    oBackOrderBE.SKU.DESCRIPTION = dr["DESCRIPTION"] == DBNull.Value ? null : Convert.ToString(dr["DESCRIPTION"]);

                    if (dr.Table.Columns.Contains("PenaltyRelatingTo"))
                        oBackOrderBE.PenaltyRelatingTo = dr["PenaltyRelatingTo"] == DBNull.Value ? null : Convert.ToString(dr["PenaltyRelatingTo"]);
                    if (dr.Table.Columns.Contains("DateBOIncurred"))
                        oBackOrderBE.BOIncurredDate = dr["DateBOIncurred"] != DBNull.Value ? Convert.ToDateTime(dr["DateBOIncurred"]) : (DateTime?)null;
                    if (dr.Table.Columns.Contains("Year"))
                        oBackOrderBE.Year = dr["Year"] != DBNull.Value ? Convert.ToString(dr["Year"]) : null;

                    oBackOrderBE.SKU.SiteName = dr["Site"] == DBNull.Value ? null : Convert.ToString(dr["Site"]);
                    oBackOrderBE.NoofBO = dr["NumberofBO"] != DBNull.Value ? Convert.ToInt32(dr["NumberofBO"]) : 0;
                    oBackOrderBE.QtyonBackOrder = dr["QtyonBackOrder"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["QtyonBackOrder"]);

                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.Vendor.VendorID = Convert.ToInt32(dr["BOVendorID"]);
                    oBackOrderBE.Vendor.Vendor_No = dr["BOVendorNo"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorNo"]);
                    oBackOrderBE.Vendor.VendorName = dr["BOVendorName"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorName"]);

                    oBackOrderBE.PurchaseOrder.Purchase_order = dr["OverduePO"] == DBNull.Value ? null : Convert.ToString(dr["OverduePO"]);
                    oBackOrderBE.PurchaseOrder.Outstanding_Qty = dr["OutstandingQty"] == DBNull.Value ? null : Convert.ToString(dr["OutstandingQty"]);
                    oBackOrderBE.PurchaseOrder.Order_raised = dr["DateRaisedDate"] != DBNull.Value ? Convert.ToDateTime(dr["DateRaisedDate"]) : (DateTime?)null;
                    oBackOrderBE.PurchaseOrder.Original_due_date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;

                    oBackOrderBE.PotentialPenaltyCharge = dr["PotentialPenaltyCharge"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["PotentialPenaltyCharge"]);
                    oBackOrderBE.DisputeReviewStatus = dr["DisputeReviewStatus"] == DBNull.Value ? null : Convert.ToString(dr["DisputeReviewStatus"]);
                    oBackOrderBE.VendorBOReviewStatus = dr["VendorBOReviewStatus"] == DBNull.Value ? null : Convert.ToString(dr["VendorBOReviewStatus"]);
                    oBackOrderBE.PenaltyChargeID = dr["PenaltyChargeID"] != DBNull.Value ? Convert.ToInt32(dr["PenaltyChargeID"]) : 0;


                    if (dr.Table.Columns.Contains("TotalRecords"))
                        oBackOrderBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;
                    if (dr.Table.Columns.Contains("DisputedValue"))
                        oBackOrderBE.DisputedValue = dr["DisputedValue"] != DBNull.Value ? Convert.ToDecimal(dr["DisputedValue"]) : (decimal?)null;
                    //if (dr.Table.Columns.Contains("DisputeTotal"))
                    //    oBackOrderBE.DisputeTotal = dr["DisputeTotal"] != DBNull.Value ? Convert.ToDecimal(dr["DisputeTotal"]) : (decimal?)null;
                    if (dr.Table.Columns.Contains("PotentialPenaltyChargeTotal"))
                        oBackOrderBE.PotentialPenaltyChargeTotal = dr["PotentialPenaltyChargeTotal"] != DBNull.Value ? Convert.ToDecimal(dr["PotentialPenaltyChargeTotal"]) : (decimal?)null;
                    if (dr.Table.Columns.Contains("ImposedPenaltyChargeTotal"))
                        oBackOrderBE.ImposedPenaltyChargeTotal = dr["ImposedPenaltyChargeTotal"] != DBNull.Value ? Convert.ToDecimal(dr["ImposedPenaltyChargeTotal"]) : (decimal?)null;

                    if (dr.Table.Columns.Contains("MediatorAction"))
                        oBackOrderBE.MediatorAction = dr["MediatorAction"] == DBNull.Value ? null : Convert.ToString(dr["MediatorAction"]);

                    if (dr.Table.Columns.Contains("MediatorComments"))
                        oBackOrderBE.MediatorComments = dr["MediatorComments"] == DBNull.Value ? null : Convert.ToString(dr["MediatorComments"]);

                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        /// <summary>
        /// Used on Dispute detail to get Query Details
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public List<BackOrderBE> GetQueryDetailDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@PenaltyChargeID", BackOrderBE.PenaltyChargeID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
                    oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oBackOrderBE.PurchaseOrder.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();


                    oBackOrderBE.PenaltyChargeID = Convert.ToInt32(dr["PenaltyChargesID"]);
                    oBackOrderBE.PotentialPenaltyCharge = dr["OriginalPenaltyValue"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["OriginalPenaltyValue"]);
                    oBackOrderBE.DisputedValue = dr["DisputedValue"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["DisputedValue"]);
                    oBackOrderBE.RevisedPenalty = dr["RevisedPenalty"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["RevisedPenalty"]);
                    oBackOrderBE.VendorComment = dr["VendorComment"] == DBNull.Value ? null : Convert.ToString(dr["VendorComment"]);
                    oBackOrderBE.VendorActionDate = dr["QueryRaisedOn"] != DBNull.Value ? Convert.ToDateTime(dr["QueryRaisedOn"]) : (DateTime?)null;
                    oBackOrderBE.VendorActionTakenBy = Convert.ToInt32(dr["VendorActionTakenBy"]);
                    oBackOrderBE.VendorActionTakenName = dr["VendorActionTakenName"] == DBNull.Value ? null : Convert.ToString(dr["VendorActionTakenName"]);
                    oBackOrderBE.Vendor.VendorContactEmail = dr["ContactDetail"] == DBNull.Value ? null : Convert.ToString(dr["ContactDetail"]);
                    oBackOrderBE.Vendor.VendorContactNumber = dr["PhoneNumber"] == DBNull.Value ? null : Convert.ToString(dr["PhoneNumber"]);
                    oBackOrderBE.SPAction = dr["SPAction"] == DBNull.Value ? null : Convert.ToString(dr["SPAction"]);
                    oBackOrderBE.SPComment = dr["SPComment"] == DBNull.Value ? null : Convert.ToString(dr["SPComment"]);
                    oBackOrderBE.ImposedPenaltyCharge = dr["ImposedPenaltyCharge"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["ImposedPenaltyCharge"]);
                    oBackOrderBE.DisputeForwaredTo = dr["DisputeForwaredTo"] == DBNull.Value ? null : Convert.ToString(dr["DisputeForwaredTo"]);
                    oBackOrderBE.DisputeReviewActionDate = dr["DisputeReviewOn"] != DBNull.Value ? Convert.ToDateTime(dr["DisputeReviewOn"]) : (DateTime?)null;
                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.SiteID = Convert.ToInt32(dr["SiteID"]);


                    if (dr.Table.Columns.Contains("TotalRecords"))
                        oBackOrderBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;

                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        /// <summary>
        /// Used to update Dispute Review Status..(Agree / Declined / Partial Agree)
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public int? UpdateDisputeReviewStatusDAL(BackOrderBE BackOrderBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@PenaltyChargeID", BackOrderBE.PenaltyChargeID);
                param[index++] = new SqlParameter("@DisputeReviewStatus", BackOrderBE.DisputeReviewStatus);
                param[index++] = new SqlParameter("@DisputeReviewActionTakenBy", Convert.ToInt32(System.Web.HttpContext.Current.Session["UserID"]));
                param[index++] = new SqlParameter("@DisputeReviewActionDate", DateTime.Now);
                param[index++] = new SqlParameter("@SPAction", BackOrderBE.SPAction);
                param[index++] = new SqlParameter("@SPComment", BackOrderBE.SPComment);
                param[index++] = new SqlParameter("@ImposedPenaltyCharge", BackOrderBE.ImposedPenaltyCharge);
                param[index++] = new SqlParameter("@DisputeForwaredTo", BackOrderBE.DisputeForwaredTo);
                param[index++] = new SqlParameter("@PotentialPenaltyChargeTotal", BackOrderBE.PotentialPenaltyChargeTotal);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorPenalty", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Returns list of Vendor to Esclation Reviewer
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public List<BackOrderBE> GetEsclationOverviewDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", BackOrderBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@UserID", BackOrderBE.User.UserID);
                param[index++] = new SqlParameter("@RoleName", BackOrderBE.User.RoleName);
                param[index++] = new SqlParameter("@GridCurrentPageNo", BackOrderBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", BackOrderBE.GridPageSize);
                if (BackOrderBE.VendorID != null && BackOrderBE.PenaltyChargeID != null)
                {
                    param[index++] = new SqlParameter("@PenaltyChargeID ", BackOrderBE.PenaltyChargeID);
                    param[index++] = new SqlParameter("@VendorID", BackOrderBE.VendorID);
                }
                param[index++] = new SqlParameter("@SelectedMediatorIDs", BackOrderBE.SelectedMediatorIDs);

                param[index++] = new SqlParameter("@MedaitorActionSearchBy", BackOrderBE.MedaitorActionSearchBy);
                param[index++] = new SqlParameter("@DisputeType", BackOrderBE.DisputeType);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];


                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
                    oBackOrderBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oBackOrderBE.Vendor.VendorID = Convert.ToInt32(dr["BOVendorID"]);
                    oBackOrderBE.Vendor.VendorCountryName = dr["Country"] == DBNull.Value ? null : Convert.ToString(dr["Country"]);
                    oBackOrderBE.Vendor.Vendor_No = dr["BOVendorNo"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorNo"]);
                    oBackOrderBE.Vendor.VendorName = dr["BOVendorName"] == DBNull.Value ? null : Convert.ToString(dr["BOVendorName"]);
                    oBackOrderBE.InventoryActionDate = dr["DatePenaltyRaised"] != DBNull.Value ? Convert.ToDateTime(dr["DatePenaltyRaised"]) : (DateTime?)null;
                    oBackOrderBE.VendorActionDate = dr["DateDisputeRaised"] != DBNull.Value ? Convert.ToDateTime(dr["DateDisputeRaised"]) : (DateTime?)null;
                    oBackOrderBE.DisputeReviewActionDate = dr["DateInitialReviewed"] != DBNull.Value ? Convert.ToDateTime(dr["DateInitialReviewed"]) : (DateTime?)null;
                    oBackOrderBE.DisputeReviewActionTakenBy = dr["InitialDisputeReviewedBy"] != DBNull.Value ? Convert.ToInt32(dr["InitialDisputeReviewedBy"]) : 0;
                    oBackOrderBE.DisputeReviewActionTakenName = dr["InitialDisputeReviewedName"] == DBNull.Value ? null : Convert.ToString(dr["InitialDisputeReviewedName"]);
                    oBackOrderBE.DisputeForwaredTo = dr["DisputeEsclateTo"] == DBNull.Value ? null : Convert.ToString(dr["DisputeEsclateTo"]);
                    oBackOrderBE.PotentialPenaltyCharge = dr["PenaltyValue"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["PenaltyValue"]);
                    oBackOrderBE.PenaltyChargeID = dr["PenaltyChargeID"] != DBNull.Value ? Convert.ToInt32(dr["PenaltyChargeID"]) : 0;
                    if (dr.Table.Columns.Contains("TotalRecords"))
                        oBackOrderBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;
                    if (dr.Table.Columns.Contains("SiteName"))
                        oBackOrderBE.SKU.SiteName = dr["SiteName"] == DBNull.Value ? null : Convert.ToString(dr["SiteName"]);
                    if (dr.Table.Columns.Contains("SiteID"))
                        oBackOrderBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    if (dr.Table.Columns.Contains("DisputeType"))
                        oBackOrderBE.DisputeType = dr["DisputeType"] == DBNull.Value ? null : Convert.ToString(dr["DisputeType"]);
                    if (dr.Table.Columns.Contains("IsPenaltyReviewed"))
                        oBackOrderBE.IsPenaltyReviewed = dr["IsPenaltyReviewed"] == DBNull.Value ? null : Convert.ToString(dr["IsPenaltyReviewed"]);
                    if (dr.Table.Columns.Contains("DisputeEscalatedToLoginID"))
                        oBackOrderBE.User.LoginID = dr["DisputeEscalatedToLoginID"] == DBNull.Value ? null : Convert.ToString(dr["DisputeEscalatedToLoginID"]);
                    if (dr.Table.Columns.Contains("SKUID"))
                        oBackOrderBE.SKUID = dr["SKUID"] != DBNull.Value ? Convert.ToInt32(dr["SKUID"]) : 0;
                    if (dr.Table.Columns.Contains("Date"))
                        oBackOrderBE.BOIncurredDate = dr["Date"] != DBNull.Value ? Convert.ToDateTime(dr["Date"]) : (DateTime?)null;
                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        /// <summary>
        /// Used to update Mediator Review Status..(Penalty / No Penalty)
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public int? UpdateMediatorReviewStatusDAL(BackOrderBE BackOrderBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[17];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@PenaltyChargeID", BackOrderBE.PenaltyChargeID);
                param[index++] = new SqlParameter("@MediatorStatus", BackOrderBE.MediatorStatus);
                param[index++] = new SqlParameter("@MediatorActionTakenBy", Convert.ToInt32(System.Web.HttpContext.Current.Session["UserID"]));
                param[index++] = new SqlParameter("@MediatorActionDate", DateTime.Now);
                param[index++] = new SqlParameter("@MediatorAction", BackOrderBE.MediatorAction);
                param[index++] = new SqlParameter("@MediatorComments", BackOrderBE.MediatorComments);
                param[index++] = new SqlParameter("@AgreedPenaltyCharge", BackOrderBE.AgreedPenaltyCharge);
                param[index++] = new SqlParameter("@PenaltyChargeAgreedWith", BackOrderBE.PenaltyChargeAgreedWith);
                param[index++] = new SqlParameter("@PenaltyChargeAgreedDatetime", BackOrderBE.PenaltyChargeAgreedDatetime);
                param[index++] = new SqlParameter("@IsMedaitionRequiredDailyEmail", BackOrderBE.IsMedaitionRequiredDailyEmail);
                param[index++] = new SqlParameter("@VendorID", BackOrderBE.VendorID);
                param[index++] = new SqlParameter("@SiteId", BackOrderBE.SiteID);
                param[index++] = new SqlParameter("@InventoryActionDate", BackOrderBE.InventoryActionDate);
                param[index++] = new SqlParameter("@SKUId", BackOrderBE.SKUID);
                param[index++] = new SqlParameter("@BOIncurredDate", BackOrderBE.BOIncurredDate);
                param[index++] = new SqlParameter("@IsMedActionFromBOPenaltyOverview", BackOrderBE.IsMedActionFromBOPenaltyOverview);


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorPenalty", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Used to Dashboard to Display Counts
        /// </summary>
        /// <param name="BackOrderBE"></param>
        /// <returns></returns>
        public List<BackOrderBE> GetPenaltyCountOnDashnoardDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@UserID", BackOrderBE.User.UserID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.SKUCountOnDashboard = Convert.ToInt32(dr["SKUCount"]);
                    if (dr.Table.Columns.Contains("BackOrder"))
                        oBackOrderBE.BackOrder = dr["BackOrder"] == DBNull.Value ? null : Convert.ToString(dr["BackOrder"]);


                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public bool CheckVendorSubscribeDAL(BackOrderBE BackOrderBE)
        {
            bool IsVendorSubscribe = false;
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@UserID", BackOrderBE.User.UserID);
                DataSet dsVendorSubscribe = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);


                if (dsVendorSubscribe != null && dsVendorSubscribe.Tables.Count > 0)
                {
                    DataTable dtVendorSubscribe = dsVendorSubscribe.Tables[0];
                    if (dtVendorSubscribe.Rows.Count > 0)
                        IsVendorSubscribe = true;
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return IsVendorSubscribe;
        }


        /// <summary>
        /// Used to save communication detail
        /// </summary>
        /// <param name="oBackOrderMailBE"></param>
        /// <returns></returns>
        public int? AddCommunicationDetailDAL(BackOrderMailBE oBackOrderMailBE)
        {
            int? intResult = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[15];
                param[index++] = new SqlParameter("@Action", oBackOrderMailBE.Action);
                param[index++] = new SqlParameter("@BOReportingID", oBackOrderMailBE.BOReportingID);
                param[index++] = new SqlParameter("@CommunicationType", oBackOrderMailBE.CommunicationType);
                param[index++] = new SqlParameter("@Subject", oBackOrderMailBE.Subject);
                param[index++] = new SqlParameter("@SentTo", oBackOrderMailBE.SentTo);
                param[index++] = new SqlParameter("@SentDate", oBackOrderMailBE.SentDate);
                param[index++] = new SqlParameter("@Body", oBackOrderMailBE.Body);
                param[index++] = new SqlParameter("@SendByID", oBackOrderMailBE.SendByID);
                param[index++] = new SqlParameter("@LanguageId", oBackOrderMailBE.LanguageId);
                param[index++] = new SqlParameter("@MailSentInLanguage", oBackOrderMailBE.MailSentInLanguage);
                param[index++] = new SqlParameter("@CommunicationStatus", oBackOrderMailBE.CommunicationStatus);
                param[index++] = new SqlParameter("@SentToWithLink", oBackOrderMailBE.SentToWithLink);
                param[index++] = new SqlParameter("@VendorID", oBackOrderMailBE.VendorId);
                param[index++] = new SqlParameter("@SelectedBOReportingID", oBackOrderMailBE.SelectedBOIds);
                param[index++] = new SqlParameter("@VendorEmailIds", oBackOrderMailBE.VendorEmailIds);

                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorPenalty", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<BackOrderBE> GetVendorCommunication2DetailDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@VendorID", BackOrderBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SiteId", BackOrderBE.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                    oBackOrderBE.User.FirstName = dr["FirstName"] == DBNull.Value ? null : Convert.ToString(dr["FirstName"]);
                    oBackOrderBE.User.Lastname = dr["Lastname"] == DBNull.Value ? null : Convert.ToString(dr["Lastname"]);
                    oBackOrderBE.User.EmailId = dr["EmailId"] == DBNull.Value ? null : Convert.ToString(dr["EmailId"]);
                    oBackOrderBE.User.LanguageID = Convert.ToInt32(dr["LanguageID"]);
                    oBackOrderBE.Vendor.Vendor_No = dr["Vendor_No"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_No"]);
                    oBackOrderBE.Vendor.VendorName = dr["VendorName"] == DBNull.Value ? null : Convert.ToString(dr["VendorName"]);
                    oBackOrderBE.Vendor.address1 = dr["VendorAddress1"] == DBNull.Value ? null : Convert.ToString(dr["VendorAddress1"]);
                    oBackOrderBE.Vendor.address2 = dr["VendorAddress2"] == DBNull.Value ? null : Convert.ToString(dr["VendorAddress2"]);
                    oBackOrderBE.Vendor.city = dr["VendorCity"] == DBNull.Value ? null : Convert.ToString(dr["VendorCity"]);
                    oBackOrderBE.Vendor.VMPPIN = dr["VMPPIN"] == DBNull.Value ? null : Convert.ToString(dr["VMPPIN"]);
                    oBackOrderBE.Vendor.VMPPOU = dr["VMPPOU"] == DBNull.Value ? null : Convert.ToString(dr["VMPPOU"]);
                    oBackOrderBE.Vendor.StockPlannerName = string.IsNullOrWhiteSpace(dr["StockPlannerName"].ToString()) ? Convert.ToString(dr["StockPlannerNo"]) : Convert.ToString(dr["StockPlannerNo"] + " - " + dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.Vendor.StockPlannerContact = dr["SPContactNo"] == DBNull.Value ? null : Convert.ToString(dr["SPContactNo"]);
                    oBackOrderBE.APAddress1 = dr["APAddress1"] != DBNull.Value ? dr["APAddress1"].ToString() : string.Empty;
                    oBackOrderBE.APAddress2 = dr["APAddress2"] != DBNull.Value ? dr["APAddress2"].ToString() : string.Empty;
                    oBackOrderBE.APAddress3 = dr["APAddress3"] != DBNull.Value ? dr["APAddress3"].ToString() : string.Empty;
                    oBackOrderBE.APAddress4 = dr["APAddress4"] != DBNull.Value ? dr["APAddress4"].ToString() : string.Empty;
                    oBackOrderBE.APAddress5 = dr["APAddress5"] != DBNull.Value ? dr["APAddress5"].ToString() : string.Empty;
                    oBackOrderBE.APAddress6 = dr["APAddress6"] != DBNull.Value ? dr["APAddress6"].ToString() : string.Empty;
                    oBackOrderBE.APCountryID = dr["APCountryID"] != DBNull.Value ? Convert.ToInt32(dr["APCountryID"].ToString()) : (int?)null;
                    oBackOrderBE.APCountryName = dr["APCountryName"] != DBNull.Value ? dr["APCountryName"].ToString() : string.Empty;
                    oBackOrderBE.APPincode = dr["APPincode"] != DBNull.Value ? dr["APPincode"].ToString() : string.Empty;

                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public List<BackOrderBE> GetVendorDetailDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@VendorID", BackOrderBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SiteId", BackOrderBE.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                    oBackOrderBE.Vendor.Vendor_No = dr["Vendor_No"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_No"]);
                    oBackOrderBE.Vendor.VendorName = dr["VendorName"] == DBNull.Value ? null : Convert.ToString(dr["VendorName"]);
                    oBackOrderBE.Vendor.address1 = dr["VendorAddress1"] == DBNull.Value ? null : Convert.ToString(dr["VendorAddress1"]);
                    oBackOrderBE.Vendor.address2 = dr["VendorAddress2"] == DBNull.Value ? null : Convert.ToString(dr["VendorAddress2"]);
                    oBackOrderBE.Vendor.city = dr["VendorCity"] == DBNull.Value ? null : Convert.ToString(dr["VendorCity"]);
                    oBackOrderBE.Vendor.VMPPIN = dr["VMPPIN"] == DBNull.Value ? null : Convert.ToString(dr["VMPPIN"]);
                    oBackOrderBE.Vendor.VMPPOU = dr["VMPPOU"] == DBNull.Value ? null : Convert.ToString(dr["VMPPOU"]);
                    oBackOrderBE.Vendor.VendorContactEmail = dr["VendorEmail"] == DBNull.Value ? ConfigurationManager.AppSettings["DefaultEmailAddress"] : Convert.ToString(dr["VendorEmail"]);
                    oBackOrderBE.Vendor.LanguageID = dr["LanguageID"] == DBNull.Value ? 1 : Convert.ToInt32(dr["LanguageID"]);
                    oBackOrderBE.Vendor.StockPlannerName = string.IsNullOrWhiteSpace(dr["StockPlannerName"].ToString()) ? Convert.ToString(dr["StockPlannerNo"]) : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.Vendor.StockPlannerContact = dr["SPContactNo"] == DBNull.Value ? null : Convert.ToString(dr["SPContactNo"]);
                    oBackOrderBE.APAddress1 = dr["APAddress1"] != DBNull.Value ? dr["APAddress1"].ToString() : string.Empty;
                    oBackOrderBE.APAddress2 = dr["APAddress2"] != DBNull.Value ? dr["APAddress2"].ToString() : string.Empty;
                    oBackOrderBE.APAddress3 = dr["APAddress3"] != DBNull.Value ? dr["APAddress3"].ToString() : string.Empty;
                    oBackOrderBE.APAddress4 = dr["APAddress4"] != DBNull.Value ? dr["APAddress4"].ToString() : string.Empty;
                    oBackOrderBE.APAddress5 = dr["APAddress5"] != DBNull.Value ? dr["APAddress5"].ToString() : string.Empty;
                    oBackOrderBE.APAddress6 = dr["APAddress6"] != DBNull.Value ? dr["APAddress6"].ToString() : string.Empty;
                    oBackOrderBE.APCountryID = dr["APCountryID"] != DBNull.Value ? Convert.ToInt32(dr["APCountryID"].ToString()) : (int?)null;
                    oBackOrderBE.APCountryName = dr["APCountryName"] != DBNull.Value ? dr["APCountryName"].ToString() : string.Empty;
                    oBackOrderBE.APPincode = dr["APPincode"] != DBNull.Value ? dr["APPincode"].ToString() : string.Empty;
                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public List<BackOrderBE> GetBOPenaltyTrackerDataDAL(BackOrderBE BackOrderBE)
        {
            var lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", BackOrderBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", BackOrderBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@IsVendorSubscribeToPenalty", BackOrderBE.IsVendorSubscribeToPenalty);
                param[index++] = new SqlParameter("@SelectedDeclineReasonCodeIDs", BackOrderBE.SelectedDeclineReasonCodeIDs);
                if (BackOrderBE.SelectedDateTo.Year != 1)
                {
                    param[index++] = new SqlParameter("@SelectedFrom", BackOrderBE.SelectedDateFrom);
                    param[index++] = new SqlParameter("@SelectedTo", BackOrderBE.SelectedDateTo);
                }
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Country = dr["Country"] == DBNull.Value ? null : Convert.ToString(dr["Country"]);
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                    oBackOrderBE.Vendor.Vendor_No = dr["VendorNo"] == DBNull.Value ? null : Convert.ToString(dr["VendorNo"]);
                    oBackOrderBE.Vendor.VendorName = dr["VendorName"] == DBNull.Value ? null : Convert.ToString(dr["VendorName"]);
                    oBackOrderBE.Subscribe = dr["Subscribe"] == DBNull.Value ? null : Convert.ToString(dr["Subscribe"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.YTDTotalNoOfBO = dr["YTDTotalNoOfBO"] != DBNull.Value ? Convert.ToInt32(dr["YTDTotalNoOfBO"]) : 0;
                    oBackOrderBE.YTDTotalNoOfVendorBO = dr["YTDTotalNoOfVendorBO"] != DBNull.Value ? Convert.ToInt32(dr["YTDTotalNoOfVendorBO"]) : 0;
                    oBackOrderBE.YTDPenaltyCharges = dr["YTDPenaltyCharges"] != DBNull.Value ? Convert.ToDecimal(dr["YTDPenaltyCharges"]) : (decimal?)null;
                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public List<BackOrderBE> GetBOChargesDetailsDAL(BackOrderBE BackOrderBE)
        {
            var lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", BackOrderBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedAPClerkIDs", BackOrderBE.SelectedAPClerkIDs);
                if (BackOrderBE.SelectedDateTo.Year != 1)
                {
                    param[index++] = new SqlParameter("@SelectedTo", BackOrderBE.SelectedDateTo);
                    param[index++] = new SqlParameter("@SelectedFrom", BackOrderBE.SelectedDateFrom);
                }

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                    oBackOrderBE.Vendor.Vendor_No = dr["VendorNo"] == DBNull.Value ? null : Convert.ToString(dr["VendorNo"]);
                    oBackOrderBE.Vendor.VendorName = dr["VendorName"] == DBNull.Value ? null : Convert.ToString(dr["VendorName"]);
                    oBackOrderBE.CountryID = dr["CountryID"] != DBNull.Value ? Convert.ToInt32(dr["CountryID"]) : (int?)null;
                    oBackOrderBE.Country = dr["Country"] == DBNull.Value ? null : Convert.ToString(dr["Country"]);
                    oBackOrderBE.PenaltyType = dr["PenaltyType"] == DBNull.Value ? null : Convert.ToString(dr["PenaltyType"]);
                    oBackOrderBE.Rate = dr["Rate"] != DBNull.Value ? Convert.ToDecimal(dr["Rate"]) : (decimal?)null;
                    oBackOrderBE.NoofBO = dr["NoOfBO"] != DBNull.Value ? Convert.ToInt32(dr["NoOfBO"]) : (int?)null;
                    oBackOrderBE.QtyOnBO = dr["QtyOnBO"] != DBNull.Value ? Convert.ToDecimal(dr["QtyOnBO"]) : (decimal?)null;
                    oBackOrderBE.Month = dr["Month"] == DBNull.Value ? null : Convert.ToString(dr["Month"]);
                    oBackOrderBE.AmountDebited = dr["AmountDebited"] != DBNull.Value ? Convert.ToDecimal(dr["AmountDebited"]) : (decimal?)null;
                    oBackOrderBE.Currency = new CurrencyBE();
                    oBackOrderBE.Currency.CurrencyName = dr["CurrencyName"] == DBNull.Value ? null : Convert.ToString(dr["CurrencyName"]);
                    oBackOrderBE.APClerk = dr["APClerk"] == DBNull.Value ? null : Convert.ToString(dr["APClerk"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.VendorVATRef = dr["VendorVATRef"] == DBNull.Value ? null : Convert.ToString(dr["VendorVATRef"]);
                    if (dr.Table.Columns.Contains("ReasonForCharge"))
                    {
                        oBackOrderBE.ReasonForCharge = dr["ReasonForCharge"] == DBNull.Value ? null : Convert.ToString(dr["ReasonForCharge"]);
                    }
                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public List<BackOrderBE> GetBOChargesEmailDetailsDAL(BackOrderBE BackOrderBE)
        {
            var lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@VendorID", BackOrderBE.Vendor.VendorID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.BOReportingID = dr["BOReportingID"] != DBNull.Value ? Convert.ToInt32(dr["BOReportingID"]) : (int?)null;
                    oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oBackOrderBE.PurchaseOrder.OD_Code = dr["ODCode"] == DBNull.Value ? null : Convert.ToString(dr["ODCode"]);
                    oBackOrderBE.PurchaseOrder.Vendor_Code = dr["VendorCode"] == DBNull.Value ? null : Convert.ToString(dr["VendorCode"]);
                    oBackOrderBE.PurchaseOrder.ProductDescription = dr["DESCRIPTION"] == DBNull.Value ? null : Convert.ToString(dr["DESCRIPTION"]);
                    oBackOrderBE.DateBOIncurred = dr["DateBOIncurred"] != DBNull.Value ? Convert.ToDateTime(dr["DateBOIncurred"]) : (DateTime?)null;
                    oBackOrderBE.PurchaseOrder.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oBackOrderBE.PurchaseOrder.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    oBackOrderBE.PurchaseOrder.SiteName = dr["SiteName"] == DBNull.Value ? null : Convert.ToString(dr["SiteName"]);
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                    oBackOrderBE.Vendor.Vendor_No = dr["VendorNo"] == DBNull.Value ? null : Convert.ToString(dr["VendorNo"]);
                    oBackOrderBE.Vendor.VendorName = dr["VendorName"] == DBNull.Value ? null : Convert.ToString(dr["VendorName"]);
                    oBackOrderBE.CountryID = dr["CountryID"] != DBNull.Value ? Convert.ToInt32(dr["CountryID"]) : (int?)null;
                    oBackOrderBE.Country = dr["Country"] == DBNull.Value ? null : Convert.ToString(dr["Country"]);
                    oBackOrderBE.NoofBO = dr["NoOfBO"] != DBNull.Value ? Convert.ToInt32(dr["NoOfBO"]) : (int?)null;
                    oBackOrderBE.QtyOnBO = dr["QtyOnBO"] != DBNull.Value ? Convert.ToDecimal(dr["QtyOnBO"]) : (decimal?)null;
                    oBackOrderBE.Month = dr["Month"] == DBNull.Value ? null : Convert.ToString(dr["Month"]);
                    oBackOrderBE.AmountDebited = dr["AmountDebited"] != DBNull.Value ? Convert.ToDecimal(dr["AmountDebited"]) : (decimal?)null;
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.APAddress1 = dr["APAddress1"] == DBNull.Value ? null : Convert.ToString(dr["APAddress1"]);
                    oBackOrderBE.APAddress2 = dr["APAddress2"] == DBNull.Value ? null : Convert.ToString(dr["APAddress2"]);
                    oBackOrderBE.APAddress3 = dr["APAddress3"] == DBNull.Value ? null : Convert.ToString(dr["APAddress3"]);
                    oBackOrderBE.APAddress4 = dr["APAddress4"] == DBNull.Value ? null : Convert.ToString(dr["APAddress4"]);
                    oBackOrderBE.APAddress5 = dr["APAddress5"] == DBNull.Value ? null : Convert.ToString(dr["APAddress5"]);
                    oBackOrderBE.APAddress6 = dr["APAddress6"] == DBNull.Value ? null : Convert.ToString(dr["APAddress6"]);
                    oBackOrderBE.APCountryID = dr["APCountryID"] != DBNull.Value ? Convert.ToInt32(dr["APCountryID"]) : (int?)null;
                    oBackOrderBE.APCountryName = dr["APCountryName"] == DBNull.Value ? null : Convert.ToString(dr["APCountryName"]);
                    oBackOrderBE.APPincode = dr["APPincode"] == DBNull.Value ? null : Convert.ToString(dr["APPincode"]);
                    oBackOrderBE.AgreedApprovedBy = dr["AgreedApprovedBy"] == DBNull.Value ? null : Convert.ToString(dr["AgreedApprovedBy"]);
                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public int? UpdateBOReportingByAPDAL(BackOrderBE BackOrderBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@APActionTakenBy", BackOrderBE.APActionTakenBy);
                param[index++] = new SqlParameter("@DebitRaiseId", BackOrderBE.DebitRaiseId);
                param[index++] = new SqlParameter("@BOReportingIDs", BackOrderBE.BOReportingIDs);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorPenalty", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? IsVendorPendingForApproveBODAL(BackOrderBE BackOrderBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@VendorID", BackOrderBE.Vendor.VendorID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorPenalty", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<BackOrderBE> GetOpenBOChargesDetailsDAL(BackOrderBE BackOrderBE)
        {
            var lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", BackOrderBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedAPClerkIDs", BackOrderBE.SelectedAPClerkIDs);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                    oBackOrderBE.Vendor.Vendor_No = dr["VendorNo"] == DBNull.Value ? null : Convert.ToString(dr["VendorNo"]);
                    oBackOrderBE.Vendor.VendorName = dr["VendorName"] == DBNull.Value ? null : Convert.ToString(dr["VendorName"]);
                    oBackOrderBE.CountryID = dr["CountryID"] != DBNull.Value ? Convert.ToInt32(dr["CountryID"]) : (int?)null;
                    oBackOrderBE.Country = dr["Country"] == DBNull.Value ? null : Convert.ToString(dr["Country"]);
                    oBackOrderBE.PenaltyType = dr["PenaltyType"] == DBNull.Value ? null : Convert.ToString(dr["PenaltyType"]);
                    oBackOrderBE.Rate = dr["Rate"] != DBNull.Value ? Convert.ToDecimal(dr["Rate"]) : (decimal?)null;
                    oBackOrderBE.NoofBO = dr["NoOfBO"] != DBNull.Value ? Convert.ToInt32(dr["NoOfBO"]) : (int?)null;
                    oBackOrderBE.QtyOnBO = dr["QtyOnBO"] != DBNull.Value ? Convert.ToDecimal(dr["QtyOnBO"]) : (decimal?)null;
                    oBackOrderBE.AmountDebited = dr["AmountDebited"] != DBNull.Value ? Convert.ToDecimal(dr["AmountDebited"]) : (decimal?)null;
                    oBackOrderBE.DateofEscalation = dr["DateofEscalation"] != DBNull.Value ? Convert.ToDateTime(dr["DateofEscalation"]) : (DateTime?)null;
                    oBackOrderBE.ValueApproved = dr["ValueApproved"] != DBNull.Value ? Convert.ToDecimal(dr["ValueApproved"]) : (decimal?)null;
                    oBackOrderBE.ValueDisputed = dr["ValueDisputed"] != DBNull.Value ? Convert.ToDecimal(dr["ValueDisputed"]) : (decimal?)null;
                    oBackOrderBE.ValuePending = dr["ValuePending"] != DBNull.Value ? Convert.ToDecimal(dr["ValuePending"]) : (decimal?)null;
                    oBackOrderBE.Currency = new CurrencyBE();
                    oBackOrderBE.Currency.CurrencyName = dr["CurrencyName"] == DBNull.Value ? null : Convert.ToString(dr["CurrencyName"]);
                    oBackOrderBE.APClerk = dr["APClerk"] == DBNull.Value ? null : Convert.ToString(dr["APClerk"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.VendorVATRef = dr["VendorVATRef"] == DBNull.Value ? null : Convert.ToString(dr["VendorVATRef"]);
                    if (dr.Table.Columns.Contains("StockPlannerID"))
                        oBackOrderBE.StockPlannerID = dr["StockPlannerID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["StockPlannerID"]);

                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public List<BackOrderBE> GetOpenBOChargesEmailDetailsDAL(BackOrderBE BackOrderBE)
        {
            var lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@VendorID", BackOrderBE.Vendor.VendorID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.BOReportingID = dr["BOReportingID"] != DBNull.Value ? Convert.ToInt32(dr["BOReportingID"]) : (int?)null;
                    oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oBackOrderBE.PurchaseOrder.OD_Code = dr["ODCode"] == DBNull.Value ? null : Convert.ToString(dr["ODCode"]);
                    oBackOrderBE.PurchaseOrder.Vendor_Code = dr["VendorCode"] == DBNull.Value ? null : Convert.ToString(dr["VendorCode"]);
                    oBackOrderBE.PurchaseOrder.ProductDescription = dr["DESCRIPTION"] == DBNull.Value ? null : Convert.ToString(dr["DESCRIPTION"]);
                    oBackOrderBE.DateBOIncurred = dr["DateBOIncurred"] != DBNull.Value ? Convert.ToDateTime(dr["DateBOIncurred"]) : (DateTime?)null;
                    oBackOrderBE.PurchaseOrder.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oBackOrderBE.PurchaseOrder.Site.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    oBackOrderBE.PurchaseOrder.SiteName = dr["SiteName"] == DBNull.Value ? null : Convert.ToString(dr["SiteName"]);
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                    oBackOrderBE.Vendor.Vendor_No = dr["VendorNo"] == DBNull.Value ? null : Convert.ToString(dr["VendorNo"]);
                    oBackOrderBE.Vendor.VendorName = dr["VendorName"] == DBNull.Value ? null : Convert.ToString(dr["VendorName"]);
                    oBackOrderBE.CountryID = dr["CountryID"] != DBNull.Value ? Convert.ToInt32(dr["CountryID"]) : (int?)null;
                    oBackOrderBE.Country = dr["Country"] == DBNull.Value ? null : Convert.ToString(dr["Country"]);
                    oBackOrderBE.NoofBO = dr["NoOfBO"] != DBNull.Value ? Convert.ToInt32(dr["NoOfBO"]) : (int?)null;
                    oBackOrderBE.QtyOnBO = dr["QtyOnBO"] != DBNull.Value ? Convert.ToDecimal(dr["QtyOnBO"]) : (decimal?)null;
                    oBackOrderBE.AmountDebited = dr["AmountDebited"] != DBNull.Value ? Convert.ToDecimal(dr["AmountDebited"]) : (decimal?)null;
                    oBackOrderBE.ValueApproved = dr["ValueApproved"] != DBNull.Value ? Convert.ToDecimal(dr["ValueApproved"]) : (decimal?)null;
                    oBackOrderBE.ValueDisputed = dr["ValueDisputed"] != DBNull.Value ? Convert.ToDecimal(dr["ValueDisputed"]) : (decimal?)null;
                    oBackOrderBE.ValuePending = dr["ValuePending"] != DBNull.Value ? Convert.ToDecimal(dr["ValuePending"]) : (decimal?)null;
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.APAddress1 = dr["APAddress1"] == DBNull.Value ? null : Convert.ToString(dr["APAddress1"]);
                    oBackOrderBE.APAddress2 = dr["APAddress2"] == DBNull.Value ? null : Convert.ToString(dr["APAddress2"]);
                    oBackOrderBE.APAddress3 = dr["APAddress3"] == DBNull.Value ? null : Convert.ToString(dr["APAddress3"]);
                    oBackOrderBE.APAddress4 = dr["APAddress4"] == DBNull.Value ? null : Convert.ToString(dr["APAddress4"]);
                    oBackOrderBE.APAddress5 = dr["APAddress5"] == DBNull.Value ? null : Convert.ToString(dr["APAddress5"]);
                    oBackOrderBE.APAddress6 = dr["APAddress6"] == DBNull.Value ? null : Convert.ToString(dr["APAddress6"]);
                    oBackOrderBE.APCountryID = dr["APCountryID"] != DBNull.Value ? Convert.ToInt32(dr["APCountryID"]) : (int?)null;
                    oBackOrderBE.APCountryName = dr["APCountryName"] == DBNull.Value ? null : Convert.ToString(dr["APCountryName"]);
                    oBackOrderBE.APPincode = dr["APPincode"] == DBNull.Value ? null : Convert.ToString(dr["APPincode"]);
                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public List<BackOrderBE> GetGetBOEmailEscalationDataDAL(BackOrderBE BackOrderBE)
        {
            var lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@VendorID", BackOrderBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@SentDate", BackOrderBE.BackOrderMail.SentDate);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.BackOrderMail = new BackOrderMailBE();
                    oBackOrderBE.BackOrderMail.PenaltyCommunicationId = dr["PenaltyCommunicationId"] != DBNull.Value ? Convert.ToInt32(dr["PenaltyCommunicationId"]) : 0;
                    oBackOrderBE.BackOrderMail.CommunicationType = dr["CommunicationType"] != DBNull.Value ? Convert.ToString(dr["CommunicationType"]) : string.Empty;
                    oBackOrderBE.BackOrderMail.Subject = dr["Subject"] != DBNull.Value ? Convert.ToString(dr["Subject"]) : string.Empty;
                    oBackOrderBE.BackOrderMail.SentTo = dr["SentTo"] != DBNull.Value ? Convert.ToString(dr["SentTo"]) : string.Empty;
                    oBackOrderBE.BackOrderMail.SentDate = dr["SentDate"] != DBNull.Value ? Convert.ToDateTime(dr["SentDate"]) : DateTime.Now;
                    oBackOrderBE.BackOrderMail.Body = dr["Body"] != DBNull.Value ? Convert.ToString(dr["Body"]) : string.Empty;
                    oBackOrderBE.BackOrderMail.LanguageId = dr["LanguageId"] != DBNull.Value ? Convert.ToInt32(dr["LanguageId"]) : (int?)null;
                    oBackOrderBE.BackOrderMail.MailSentInLanguage = dr["MailSentInLanguage"] != DBNull.Value ? Convert.ToBoolean(dr["MailSentInLanguage"].ToString()) : false;
                    oBackOrderBE.BackOrderMail.CommunicationStatus = dr["CommunicationStatus"] != DBNull.Value ? Convert.ToString(dr["CommunicationStatus"]) : string.Empty;
                    oBackOrderBE.BackOrderMail.SentToWithLink = dr["SentToWithLink"] != DBNull.Value ? Convert.ToString(dr["SentToWithLink"]) : string.Empty;
                    oBackOrderBE.BackOrderMail.SelectedBOIds = dr["SelectedBOIds"] != DBNull.Value ? Convert.ToString(dr["SelectedBOIds"]) : string.Empty;
                    oBackOrderBE.BackOrderMail.VendorEmailIds = dr["VendorEmailIds"] != DBNull.Value ? Convert.ToString(dr["VendorEmailIds"]) : string.Empty;
                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }


        public List<BackOrderBE> GetDeclineReportDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", BackOrderBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", BackOrderBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedDeclineReasonCodeIDs", BackOrderBE.SelectedDeclineReasonCodeIDs);
                param[index++] = new SqlParameter("@GridCurrentPageNo", BackOrderBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", BackOrderBE.GridPageSize);
                param[index++] = new SqlParameter("@DeclineDateFrom", BackOrderBE.DeclineDateFrom);
                param[index++] = new SqlParameter("@DeclineDateTo", BackOrderBE.DeclineDateTo);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
                    oBackOrderBE.DeclineCode = new MAS_DeclineReasonCodeBE();
                    oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();

                    oBackOrderBE.Country = dr["Country"] == DBNull.Value ? null : Convert.ToString(dr["Country"]);
                    oBackOrderBE.SKU.SiteName = dr["SiteName"] == DBNull.Value ? null : Convert.ToString(dr["SiteName"]);
                    oBackOrderBE.SKU.OD_SKU_NO = dr["OD_SKU_NO"] == DBNull.Value ? null : Convert.ToString(dr["OD_SKU_NO"]);
                    oBackOrderBE.SKU.Direct_SKU = dr["VikingSKU"] == DBNull.Value ? null : Convert.ToString(dr["VikingSKU"]);
                    oBackOrderBE.SKU.Vendor_Code = dr["Vendor_Code"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Code"]);
                    oBackOrderBE.SKU.DESCRIPTION = dr["DESCRIPTION"] == DBNull.Value ? null : Convert.ToString(dr["DESCRIPTION"]);
                    oBackOrderBE.NoofBO = dr["NumberofBO"] != DBNull.Value ? Convert.ToInt32(dr["NumberofBO"]) : 0;
                    oBackOrderBE.QtyonBackOrder = dr["QtyonBackOrder"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["QtyonBackOrder"]);

                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlannerName"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.Vendor.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oBackOrderBE.Vendor.Vendor_No = dr["VendorNo"] == DBNull.Value ? null : Convert.ToString(dr["VendorNo"]);
                    oBackOrderBE.Vendor.VendorName = dr["VendorName"] == DBNull.Value ? null : Convert.ToString(dr["VendorName"]);
                    oBackOrderBE.PotentialPenaltyCharge = dr["PotentialPenaltyCharge"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["PotentialPenaltyCharge"]);

                    oBackOrderBE.PurchaseOrderValue = dr["PurchaseOrder"] == DBNull.Value ? null : Convert.ToString(dr["PurchaseOrder"]);
                    oBackOrderBE.DeclineCode.ReasonCode = dr["ReasonCode"] == DBNull.Value ? null : Convert.ToString(dr["ReasonCode"]);
                    oBackOrderBE.DeclineCode.DeclinedBy = dr["DeclinedBy"] == DBNull.Value ? null : Convert.ToString(dr["DeclinedBy"]);
                    oBackOrderBE.DeclineCode.DeclinedDate = dr["DeclinedDate"] != DBNull.Value ? Convert.ToDateTime(dr["DeclinedDate"]) : (DateTime?)null;
                    oBackOrderBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"]) : 0;
                    if (dr.Table.Columns.Contains("DeclineReasonComments"))
                        oBackOrderBE.DeclineReasonComments = dr["DeclineReasonComments"] == DBNull.Value ? null : Convert.ToString(dr["DeclineReasonComments"]);

                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public List<BackOrderBE> GetBackOrderTop20DAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@siteID", BackOrderBE.SiteID);
                param[index++] = new SqlParameter("@countryID", BackOrderBE.CountryID);
                param[index++] = new SqlParameter("@UserID", BackOrderBE.StockPlannerID);
                param[index++] = new SqlParameter("@OpenOverdue", BackOrderBE.OpenOverdue);
                param[index++] = new SqlParameter("@OrderBy", BackOrderBE.OrderBy);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "Boreporting_Top20", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.SKUID = Convert.ToInt32(dr["SKUID"]);
                    oBackOrderBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oBackOrderBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oBackOrderBE.SiteName = dr["Site"].ToString();
                    oBackOrderBE.VendorNo = dr["Vendor"].ToString();
                    oBackOrderBE.SKU_No = dr["SKU"].ToString();
                    oBackOrderBE.Viking = dr["Viking"].ToString();
                    oBackOrderBE.Description = dr["Description"].ToString();
                    oBackOrderBE.Qty = Convert.ToInt32(dr["Qty"].ToString());
                    oBackOrderBE.Orders = Convert.ToInt32(dr["Orders"].ToString());
                    oBackOrderBE.Value = Convert.ToDecimal(dr["Value"]);
                    oBackOrderBE.CommentDate = dr["CommentDate"] != DBNull.Value ? Convert.ToDateTime(dr["CommentDate"]) : (DateTime?)null;
                    oBackOrderBE.Comments = dr["Comments"].ToString();
                    oBackOrderBE.BoSince = dr["BoSince"] != DBNull.Value ? Convert.ToDateTime(dr["BoSince"]) : (DateTime?)null;
                    oBackOrderBE.QtyOnBO = dr["qty_on_backorder"] != DBNull.Value ? Convert.ToDecimal(dr["qty_on_backorder"]) : (Decimal?)null;
                    oBackOrderBE.BackOrder = dr["backorder"] != DBNull.Value ? Convert.ToString(dr["backorder"]) : string.Empty;
                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public List<BackOrderBE> GetBackOrderTop20_ManagerViewDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@siteID", BackOrderBE.SiteID);
                param[index++] = new SqlParameter("@countryID", BackOrderBE.CountryID);
                param[index++] = new SqlParameter("@StockPlannerGroupingID", BackOrderBE.StockPlannerGroupingID);
                param[index++] = new SqlParameter("@OpenOverdue", BackOrderBE.OpenOverdue);
                param[index++] = new SqlParameter("@OrderBy", BackOrderBE.OrderBy);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "Boreporting_Top20", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.SiteName = dr["Site"].ToString();
                    oBackOrderBE.PlannerName = dr["PlannerName"].ToString();
                    oBackOrderBE.VendorNo = dr["Vendor"].ToString();
                    oBackOrderBE.SKU_No = dr["SKU"].ToString();
                    oBackOrderBE.Viking = dr["Viking"].ToString();
                    oBackOrderBE.Description = dr["Description"].ToString();
                    oBackOrderBE.Qty = Convert.ToInt32(dr["Qty"].ToString());
                    oBackOrderBE.Orders = Convert.ToInt32(dr["Orders"].ToString());
                    oBackOrderBE.Value = Convert.ToDecimal(dr["Value"]);
                    oBackOrderBE.CommentDate = dr["CommentDate"] != DBNull.Value ? Convert.ToDateTime(dr["CommentDate"]) : (DateTime?)null;
                    oBackOrderBE.Comments = dr["Comments"].ToString();
                    oBackOrderBE.BoSince = dr["BoSince"] != DBNull.Value ? Convert.ToDateTime(dr["BoSince"]) : (DateTime?)null;
                    oBackOrderBE.QtyOnBO = dr["qty_on_backorder"] != DBNull.Value ? Convert.ToDecimal(dr["qty_on_backorder"]) : (Decimal?)null;
                    oBackOrderBE.BackOrder = dr["backorder"] != DBNull.Value ? Convert.ToString(dr["backorder"]) : string.Empty;
                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public List<BackOrderBE> GetSKU_PODetailsDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@siteID", BackOrderBE.SiteID);
                param[index++] = new SqlParameter("@ODCode", BackOrderBE.SKU_No);
                param[index++] = new SqlParameter("@VikingCode", BackOrderBE.Viking);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "Boreporting_Top20", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();

                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                    oBackOrderBE.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    oBackOrderBE.SiteName = Convert.ToString(dr["SiteName"]);
                    oBackOrderBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oBackOrderBE.Description = Convert.ToString(dr["Description"]);
                    oBackOrderBE.Viking = Convert.ToString(dr["VikingCode"]);
                    oBackOrderBE.SKU_No = Convert.ToString(dr["ODCode"]);
                    oBackOrderBE.PurchaseOrderValue = Convert.ToString(dr["PO#"]);
                    oBackOrderBE.Vendor.VendorName = Convert.ToString(dr["Vendor"]);
                    oBackOrderBE.OrderRaised = dr["OrderRaised"] != DBNull.Value ? Convert.ToString(dr["OrderRaised"]) : string.Empty;
                    oBackOrderBE.OriginalDuedate = dr["OriginalDuedate"] != DBNull.Value ? Convert.ToString(dr["OriginalDuedate"]) : string.Empty;
                    oBackOrderBE.RevisedDueDate = dr["RevisedDueDate"] != DBNull.Value ? Convert.ToString(dr["RevisedDueDate"]) : string.Empty;
                    oBackOrderBE.BookingDate = dr["BookingDate"] != DBNull.Value ? Convert.ToString(dr["BookingDate"]) : string.Empty;
                    oBackOrderBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }
        public DataSet GetPenaltyReviewDAL(BackOrderBE BackOrderBE)
        {
            DataSet ds = new DataSet();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", BackOrderBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedSiteIds", BackOrderBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", BackOrderBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerGroupID", BackOrderBE.SelectedStockPlannerGroupID);
                param[index++] = new SqlParameter("@Year", Convert.ToInt32(BackOrderBE.Year));
                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "PenaltyReviewReport", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }


        public List<BackOrderBE> GetStockPlannerContactDetailsDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@StockPlannerID", BackOrderBE.StockPlannerID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                    if (dr.Table.Columns.Contains("LanguageID"))
                        oBackOrderBE.Vendor.LanguageID = dr["LanguageID"] != DBNull.Value ? Convert.ToInt32(dr["LanguageID"]) : 0;

                    if (dr.Table.Columns.Contains("LoginID"))
                        oBackOrderBE.Vendor.StockPlannerContact = dr["LoginID"] != DBNull.Value ? dr["LoginID"].ToString() : "";

                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }


        public DataSet GetBackOrderPenaltyReportDAL(BackOrderBE BackOrderBE)
        {
            DataSet ds = new DataSet();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", BackOrderBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedSiteIds", BackOrderBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", BackOrderBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerGroupID", BackOrderBE.SelectedStockPlannerGroupID);
                param[index++] = new SqlParameter("@StockPlannerGroupings", BackOrderBE.StockPlannerGroupings);
                param[index++] = new SqlParameter("@SelectedStockPlannerName", BackOrderBE.SelectedStockPlannerName);
                param[index++] = new SqlParameter("@SelectedTo", BackOrderBE.SelectedDateTo);
                param[index++] = new SqlParameter("@SelectedFrom", BackOrderBE.SelectedDateFrom);
                param[index++] = new SqlParameter("@SummaryBy", BackOrderBE.SummaryBy);
                param[index++] = new SqlParameter("@InventoryReviewStatus", BackOrderBE.InventoryReviewStatus);
                param[index++] = new SqlParameter("@UserID", BackOrderBE.UserID);
                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }


        public List<BackOrderBE> GetBackOrderPenaltyDetailedReportDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[13];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", BackOrderBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedSiteIds", BackOrderBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", BackOrderBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerGroupID", BackOrderBE.SelectedStockPlannerGroupID);
                param[index++] = new SqlParameter("@StockPlannerGroupings", BackOrderBE.StockPlannerGroupings);
                param[index++] = new SqlParameter("@SelectedStockPlannerName", BackOrderBE.SelectedStockPlannerName);
                param[index++] = new SqlParameter("@SelectedTo", BackOrderBE.SelectedDateTo);
                param[index++] = new SqlParameter("@SelectedFrom", BackOrderBE.SelectedDateFrom);
                param[index++] = new SqlParameter("@SummaryBy", BackOrderBE.SummaryBy);
                param[index++] = new SqlParameter("@InventoryReviewStatus", BackOrderBE.InventoryReviewStatus);
                param[index++] = new SqlParameter("@UserID", BackOrderBE.UserID);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();

                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                    oBackOrderBE.SiteName = Convert.ToString(dr["SiteName"]);
                    oBackOrderBE.Description = Convert.ToString(dr["DESCRIPTION"]);
                    oBackOrderBE.Viking = Convert.ToString(dr["VikingSKU"]);
                    oBackOrderBE.SKU_No = Convert.ToString(dr["OD_SKU_NO"]);
                    oBackOrderBE.Vendor.VendorName = Convert.ToString(dr["Vendor"]);
                    oBackOrderBE.NoofBO = dr["BOCount"] != DBNull.Value ? Convert.ToInt32(dr["BOCount"]) : (int?)null;
                    oBackOrderBE.QtyOnBO = dr["BOQty"] != DBNull.Value ? Convert.ToDecimal(dr["BOQty"]) : (decimal?)null;
                    oBackOrderBE.FirstBO = Convert.ToString(dr["FirstBO"]);
                    oBackOrderBE.TotalDays = Convert.ToInt32(dr["#Days"]);
                    oBackOrderBE.Vendor.StockPlannerName = dr["StockPlanner"] == DBNull.Value ? null : Convert.ToString(dr["StockPlanner"]);

                    oBackOrderBE.NoofBOWithPenalities = dr["NoofBOwithPenalties"] != DBNull.Value ? Convert.ToInt32(dr["NoofBOwithPenalties"]) : 0;

                    oBackOrderBE.PotentialPenaltyCharge = dr["Charge"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["Charge"]);

                    if (dr.Table.Columns.Contains("SPAction"))
                        oBackOrderBE.InventoryInitialReview = dr["SPAction"] == DBNull.Value ? null : Convert.ToString(dr["SPAction"]);

                    if (dr.Table.Columns.Contains("SP1stAction"))
                        oBackOrderBE.InventoryInitialActionDate = dr["SP1stAction"] == DBNull.Value ? null : Convert.ToString(dr["SP1stAction"]);

                    if (dr.Table.Columns.Contains("VendorAction"))
                        oBackOrderBE.VendorReview = dr["VendorAction"] == DBNull.Value ? null : Convert.ToString(dr["VendorAction"]);

                    if (dr.Table.Columns.Contains("MediatorStatus"))
                        oBackOrderBE.MediatorReview = dr["MediatorStatus"] == DBNull.Value ? null : Convert.ToString(dr["MediatorStatus"]);

                    if (dr.Table.Columns.Contains("Pendingwith"))
                        oBackOrderBE.PenaltyPendingWith = dr["Pendingwith"] == DBNull.Value ? null : Convert.ToString(dr["Pendingwith"]);

                    if (dr.Table.Columns.Contains("Status"))
                        oBackOrderBE.PenaltyStatus = dr["Status"] == DBNull.Value ? null : Convert.ToString(dr["Status"]);


                    if (dr.Table.Columns.Contains("OriginalIRStatus"))
                        oBackOrderBE.InventoryReviewStatus = dr["OriginalIRStatus"] == DBNull.Value ? null : Convert.ToString(dr["OriginalIRStatus"]);


                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public DataSet GetPlannerPenaltyReportDAL(BackOrderBE BackOrderBE)
        {
            DataSet ds = new DataSet();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", BackOrderBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", BackOrderBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@StockPlannerGroupings", BackOrderBE.StockPlannerGroupings);
                param[index++] = new SqlParameter("@SelectedTo", BackOrderBE.SelectedDateTo);
                param[index++] = new SqlParameter("@SelectedFrom", BackOrderBE.SelectedDateFrom);
                param[index++] = new SqlParameter("@SummaryBy", BackOrderBE.SummaryBy);
                param[index++] = new SqlParameter("@ReviewBy", BackOrderBE.ReviewBy);
                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public DataSet GetMediatorPenaltyReportDAL(BackOrderBE BackOrderBE)
        {
            DataSet ds = new DataSet();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedCountryIDs", BackOrderBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", BackOrderBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@StockPlannerGroupings", BackOrderBE.StockPlannerGroupings);
                param[index++] = new SqlParameter("@SelectedTo", BackOrderBE.SelectedDateTo);
                param[index++] = new SqlParameter("@SelectedFrom", BackOrderBE.SelectedDateFrom);
                param[index++] = new SqlParameter("@SelectedMediatorIDs", BackOrderBE.SelectedMediatorIDs);
                param[index++] = new SqlParameter("@DisputeType", BackOrderBE.DisputeType);


                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }


    }
}

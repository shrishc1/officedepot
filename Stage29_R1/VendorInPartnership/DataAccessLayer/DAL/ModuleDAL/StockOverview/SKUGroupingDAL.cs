﻿using BusinessEntities.ModuleBE.StockOverview;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.StockOverview
{
    public class SKUGroupingDAL
    {
        public DataTable SaveSkuGroupingDAL(SKUGroupingBE oSKUGroupingBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oSKUGroupingBE.Action);
                param[index++] = new SqlParameter("@GroupName", oSKUGroupingBE.GroupName);
                param[index++] = new SqlParameter("@countryID", oSKUGroupingBE.CountryID);
                param[index++] = new SqlParameter("@IsUpdateGroup", oSKUGroupingBE.Isupdate);
                param[index++] = new SqlParameter("@GroupID", oSKUGroupingBE.SKugroupingID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "SP_SkuGrouping", param);

                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public DataTable SearchSkuGroupingDAL(SKUGroupingBE oSKUGroupingBE, out int RecordCount)
        {
            DataTable dt = new DataTable();

            int index = 0;
            SqlParameter[] param = new SqlParameter[8];
            param[index++] = new SqlParameter("@Action", oSKUGroupingBE.Action);
            param[index++] = new SqlParameter("@OD_SKU", oSKUGroupingBE.ODSKU);
            param[index++] = new SqlParameter("@VikingCode", oSKUGroupingBE.VikingSKU);
            param[index++] = new SqlParameter("@Description", oSKUGroupingBE.Description);
            param[index++] = new SqlParameter("@GroupID", oSKUGroupingBE.SKugroupingID);
            param[index++] = new SqlParameter("@PageIndex", oSKUGroupingBE.PageCount);
            param[index++] = new SqlParameter("@RecordCount", SqlDbType.Int);
            param[index - 1].Direction = ParameterDirection.Output;
            DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "SP_SkuGrouping", param);

            dt = ds.Tables[0];
            RecordCount = Convert.ToInt32(param[6].Value);
            return dt;
        }

        public DataTable ADDRemoveSkuGroupingDAL(SKUGroupingBE oSKUGroupingBE, out int RecordCount)
        {
            DataTable dt = new DataTable();
            int index = 0;
            SqlParameter[] param = new SqlParameter[6];
            param[index++] = new SqlParameter("@Action", oSKUGroupingBE.Action);
            param[index++] = new SqlParameter("@SkuGroupingType", oSKUGroupingBE.Data);
            param[index++] = new SqlParameter("@RecordCount", SqlDbType.Int);
            param[index - 1].Direction = ParameterDirection.Output;
            DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "SP_SkuGrouping", param);
            dt = ds.Tables[0];
            RecordCount = Convert.ToInt32(param[2].Value);
            return dt;
        }

        public List<SKUGroupingBE> GetStockPlannerGroupingsDAL(SKUGroupingBE oSKUGroupingBEBEListBE)
        {
            DataTable dt = new DataTable();
            List<SKUGroupingBE> oSKUGroupingBEBEList = new List<SKUGroupingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oSKUGroupingBEBEListBE.Action);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "SP_SkuGrouping", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SKUGroupingBE oSKUGroupingBE = new SKUGroupingBE();
                    oSKUGroupingBE.SKugroupingID = dr["SkuGroupingsID"] != DBNull.Value ? Convert.ToInt32(dr["SkuGroupingsID"]) : 0;
                    oSKUGroupingBE.GroupName = dr["SkuGroupings"] != DBNull.Value ? Convert.ToString(dr["SkuGroupings"]) : string.Empty;
                    oSKUGroupingBEBEList.Add(oSKUGroupingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oSKUGroupingBEBEList;
        }
    }
}

﻿using BusinessEntities.ModuleBE.StockOverview;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.StockOverview
{
    public class MAS_DeclineReasonCodeDAL : BaseDAL
    {

        public List<MAS_DeclineReasonCodeBE> GetDeclineReasonCodeDAL(MAS_DeclineReasonCodeBE _MAS_DeclineReasonCodeBE)
        {
            List<MAS_DeclineReasonCodeBE> oSYS_MAS_DeclineReasonCodeBEList = new List<MAS_DeclineReasonCodeBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", _MAS_DeclineReasonCodeBE.Action);
                param[index++] = new SqlParameter("@DeclineReasonCodeID", _MAS_DeclineReasonCodeBE.DeclineReasonCodeID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_DeclineReasonCode", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MAS_DeclineReasonCodeBE oMAS_DeclineReasonCodeBE = new MAS_DeclineReasonCodeBE();
                    oMAS_DeclineReasonCodeBE.DeclineReasonCodeID = Convert.ToInt32(dr["DeclineReasonCodeID"]);
                    oMAS_DeclineReasonCodeBE.Code = dr["Code"].ToString();
                    oMAS_DeclineReasonCodeBE.Reason = dr["Reason"] != DBNull.Value ? Convert.ToString(dr["Reason"]) : string.Empty;
                    oMAS_DeclineReasonCodeBE.ReasonCode = dr["Code"].ToString() + " - " + Convert.ToString(dr["Reason"]);
                    if (dr.Table.Columns.Contains("IsMandatoryComment"))
                    {
                        oMAS_DeclineReasonCodeBE.IsMandatoryComment = dr["IsMandatoryComment"] != DBNull.Value ? Convert.ToBoolean(dr["IsMandatoryComment"]) : false;
                        oMAS_DeclineReasonCodeBE.IsDeclineReasonCommentMandatory = Convert.ToString(dr["DeclineReasonCodeID"]) + "-" + Convert.ToString(dr["IsMandatoryComment"]);
                    }

                    oSYS_MAS_DeclineReasonCodeBEList.Add(oMAS_DeclineReasonCodeBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oSYS_MAS_DeclineReasonCodeBEList;
        }

        public int? addEditDeclineReasonCodeDAL(MAS_DeclineReasonCodeBE oMAS_DeclineReasonCodeBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oMAS_DeclineReasonCodeBE.Action);
                param[index++] = new SqlParameter("@Code", oMAS_DeclineReasonCodeBE.Code);
                param[index++] = new SqlParameter("@Reason", oMAS_DeclineReasonCodeBE.Reason);
                param[index++] = new SqlParameter("@DeclineReasonCodeID", oMAS_DeclineReasonCodeBE.DeclineReasonCodeID);
                param[index++] = new SqlParameter("@IsMandatoryComment", oMAS_DeclineReasonCodeBE.IsMandatoryComment);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_DeclineReasonCode", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

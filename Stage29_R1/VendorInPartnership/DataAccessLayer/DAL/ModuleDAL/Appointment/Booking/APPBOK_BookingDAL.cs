﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.Booking
{
    public class APPBOK_BookingDAL : BaseDAL
    {
        public List<APPBOK_BookingBE> GetGetVolumeProcessedDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@TimeWindowID", oAPPBOK_BookingBE.TimeWindow.TimeWindowID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                    oNewAPPBOK_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.SupplierType = dr["SupplierType"].ToString();
                    oNewAPPBOK_BookingBE.VendorCarrierID = dr["VendorCarrierID"] != DBNull.Value ? Convert.ToInt32(dr["VendorCarrierID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.VehicleType = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE();
                    oNewAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(dr["VehicleTypeID"]);

                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public int? AddVendorWindowBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[42];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@BookingReferenceID", oAPPBOK_BookingBE.BookingRef);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@SupplierType", oAPPBOK_BookingBE.SupplierType);
                param[index++] = new SqlParameter("@DeliveryTypeID", oAPPBOK_BookingBE.Delivery.DeliveryTypeID);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@CarrierID", oAPPBOK_BookingBE.Carrier.CarrierID);
                param[index++] = new SqlParameter("@OtherCarrier", oAPPBOK_BookingBE.OtherCarrier);
                param[index++] = new SqlParameter("@VehicleTypeID", oAPPBOK_BookingBE.VehicleType.VehicleTypeID);
                param[index++] = new SqlParameter("@OtherVehicle", oAPPBOK_BookingBE.OtherVehicle);
                param[index++] = new SqlParameter("@PreAdviseNotification", oAPPBOK_BookingBE.PreAdviseNotification);


                param[index++] = new SqlParameter("@SlotTimeID", oAPPBOK_BookingBE.SlotTime.SlotTimeID);
                param[index++] = new SqlParameter("@SiteDoorNumberID", oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID);
                param[index++] = new SqlParameter("@DoorNumber", oAPPBOK_BookingBE.DoorNoSetup.DoorNumber);

                param[index++] = new SqlParameter("@BookingStatusID", oAPPBOK_BookingBE.BookingStatusID);

                param[index++] = new SqlParameter("@BookingTypeID", oAPPBOK_BookingBE.BookingTypeID);
                param[index++] = new SqlParameter("@FixedSlotID", oAPPBOK_BookingBE.FixedSlot.FixedSlotID);


                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.VendorID);

                param[index++] = new SqlParameter("@NumberOfPallet", oAPPBOK_BookingBE.NumberOfPallet);
                param[index++] = new SqlParameter("@NumberOfCartons", oAPPBOK_BookingBE.NumberOfCartons);
                param[index++] = new SqlParameter("@NumberOfLift", oAPPBOK_BookingBE.NumberOfLift);
                param[index++] = new SqlParameter("@NumberOfLine", oAPPBOK_BookingBE.NumberOfLines);

                param[index++] = new SqlParameter("@PurchaseOrdersIds", oAPPBOK_BookingBE.PurchaseOrdersIDs);
                param[index++] = new SqlParameter("@PurchaseOrders", oAPPBOK_BookingBE.PurchaseOrders);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.UserID);
                param[index++] = new SqlParameter("@WeekDay", oAPPBOK_BookingBE.WeekDay);
                param[index++] = new SqlParameter("@IsBookingAmended", oAPPBOK_BookingBE.IsBookingAmended);
                param[index++] = new SqlParameter("@IsOnlineBooking", oAPPBOK_BookingBE.IsOnlineBooking);
                param[index++] = new SqlParameter("@BookingComments", oAPPBOK_BookingBE.BookingComments);
                param[index++] = new SqlParameter("@TimeWindowID", oAPPBOK_BookingBE.TimeWindow.TimeWindowID);
                param[index++] = new SqlParameter("@NonWindowFromTimeID", oAPPBOK_BookingBE.NonWindowFromTimeID);
                param[index++] = new SqlParameter("@NonWindowToTimeID", oAPPBOK_BookingBE.NonWindowToTimeID);
                param[index++] = new SqlParameter("@ProvisionalReason", oAPPBOK_BookingBE.ProvisionalReason);
                param[index++] = new SqlParameter("@IsBookingAccepted", oAPPBOK_BookingBE.IsBookingAccepted);
                param[index++] = new SqlParameter("@IsEnableHazardouesItemPrompt", oAPPBOK_BookingBE.IsEnableHazardouesItemPrompt);

                param[index++] = new SqlParameter("@ISPM15CountryPalletChecking", oAPPBOK_BookingBE.ISPM15CountryPalletChecking);
                param[index++] = new SqlParameter("@ISPM15FromCountryID", oAPPBOK_BookingBE.ISPM15FromCountryID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetBookingDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.FixedSlot.SiteID);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@PKID", oAPPBOK_BookingBE.PKID);
                param[index++] = new SqlParameter("@StockPlannerID", oAPPBOK_BookingBE.StockPlannerID);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.UserID);
                param[index++] = new SqlParameter("@FromDate", oAPPBOK_BookingBE.FromDate);
                param[index++] = new SqlParameter("@ToDate", oAPPBOK_BookingBE.ToDate);
                param[index++] = new SqlParameter("@SupplierType", oAPPBOK_BookingBE.SupplierType);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                        oNewAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
                        oNewAPPBOK_BookingBE.SiteId = dr["Siteid"] != DBNull.Value ? Convert.ToInt32(dr["Siteid"]) : (int?)null;
                        oNewAPPBOK_BookingBE.SiteName = dr["SiteName"].ToString();
                        oNewAPPBOK_BookingBE.PKID = dr["PKID"].ToString();
                        oNewAPPBOK_BookingBE.FixedSlot.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : (int?)null;
                        oNewAPPBOK_BookingBE.FixedSlot.CarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"]) : (int?)null;
                        oNewAPPBOK_BookingBE.BookingStatus = dr["BookingStatus"].ToString();
                        oNewAPPBOK_BookingBE.BookingRef = dr["BookingRef"].ToString();
                        oNewAPPBOK_BookingBE.Prioirty = dr["Prioirty"] != DBNull.Value ? dr["Prioirty"].ToString() : "";
                        oNewAPPBOK_BookingBE.BookingType = dr["BookingType"] != DBNull.Value ? dr["BookingType"].ToString() : "";

                        oNewAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
                        oNewAPPBOK_BookingBE.Delivery.DeliveryType = dr["DeliveryType"] != DBNull.Value ? dr["DeliveryType"].ToString() : "";

                        oNewAPPBOK_BookingBE.ExpectedDeliveryTime = dr["ExpectedDeliveryTime"].ToString();

                        oNewAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNewAPPBOK_BookingBE.FixedSlot.Vendor.VendorName = dr["VendorName"].ToString();
                        oNewAPPBOK_BookingBE.FixedSlot.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                        oNewAPPBOK_BookingBE.FixedSlot.Vendor.VendorContactEmail = dr["VendorContactEmail"] != DBNull.Value ? dr["VendorContactEmail"].ToString() : "";

                        oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                        oNewAPPBOK_BookingBE.Carrier.CarrierName = dr["CarrierName"].ToString();
                        oNewAPPBOK_BookingBE.Carrier.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                        oNewAPPBOK_BookingBE.Carrier.User.EmailId = dr["CarrierEmail"] != DBNull.Value ? dr["CarrierEmail"].ToString() : "";

                        if (dr.Table.Columns.Contains("OtherCarrier"))
                            oNewAPPBOK_BookingBE.OtherCarrier = dr["OtherCarrier"] != DBNull.Value ? dr["OtherCarrier"].ToString() : "";

                        oNewAPPBOK_BookingBE.LiftsScheduled = dr["LiftsScheduled"] != DBNull.Value ? Convert.ToInt32(dr["LiftsScheduled"].ToString()) : (int?)null;

                        oNewAPPBOK_BookingBE.FixedSlot.MaximumPallets = dr["PalletsScheduled"] != DBNull.Value ? Convert.ToInt32(dr["PalletsScheduled"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.FixedSlot.MaximumLines = dr["LinesScheduled"] != DBNull.Value ? Convert.ToInt32(dr["LinesScheduled"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.FixedSlot.MaximumCatrons = dr["CatronsScheduled"] != DBNull.Value ? Convert.ToInt32(dr["CatronsScheduled"].ToString()) : (int?)null;

                        oNewAPPBOK_BookingBE.InventoryManager = dr["InventoryManager"] != DBNull.Value ? dr["InventoryManager"].ToString() : "";

                        oNewAPPBOK_BookingBE.BookingStatusID = Convert.ToInt32(dr["BookingStatusID"]);

                        oNewAPPBOK_BookingBE.SupplierType = dr["SupplierType"].ToString();

                        oNewAPPBOK_BookingBE.VehicleType = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE();
                        oNewAPPBOK_BookingBE.VehicleType.VehicleType = dr["VehicleType"] != DBNull.Value ? dr["VehicleType"].ToString() : "";

                        if (dr.Table.Columns.Contains("OtherVehicle"))
                            oNewAPPBOK_BookingBE.OtherVehicle = dr["OtherVehicle"] != DBNull.Value ? dr["OtherVehicle"].ToString() : "";

                        oNewAPPBOK_BookingBE.DoorNoSetup = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_DoorNoSetupBE();
                        oNewAPPBOK_BookingBE.DoorNoSetup.DoorNumber = dr["DoorNumber"] != DBNull.Value ? Convert.ToString(dr["DoorNumber"].ToString()) : string.Empty;
                        oNewAPPBOK_BookingBE.DLYUNL_OperatorInital = dr["DLYUNL_OperatorInital"] != DBNull.Value ? dr["DLYUNL_OperatorInital"].ToString() : "";
                        oNewAPPBOK_BookingBE.DLYUNL_DateTime = dr["DLYUNL_DateTime"] != DBNull.Value ? Convert.ToDateTime(dr["DLYUNL_DateTime"]) : (DateTime?)null;
                        oNewAPPBOK_BookingBE.DeliveryDate = dr["DeliveryDate"] != DBNull.Value ? Convert.ToDateTime(dr["DeliveryDate"]) : (DateTime?)null;
                        oNewAPPBOK_BookingBE.DLYREF_ReasonComments = dr["DLYREF_ReasonComments"] != DBNull.Value ? Convert.ToString(dr["DLYREF_ReasonComments"]) : string.Empty;

                        // Sprint 1 - Point 8 - Start 
                        oNewAPPBOK_BookingBE.DLYREF_ReasonArrivedEarly = dr["DLYREF_ReasonArrivedEarly"] != DBNull.Value ? Convert.ToBoolean(dr["DLYREF_ReasonArrivedEarly"]) : false;
                        oNewAPPBOK_BookingBE.DLYREF_ReasonArrivedLate = dr["DLYREF_ReasonArrivedLate"] != DBNull.Value ? Convert.ToBoolean(dr["DLYREF_ReasonArrivedLate"]) : false;
                        oNewAPPBOK_BookingBE.DLYREF_ReasonNoPaperworkOndisplay = dr["DLYREF_ReasonNoPaperworkOndisplay"] != DBNull.Value ? Convert.ToBoolean(dr["DLYREF_ReasonNoPaperworkOndisplay"]) : false;
                        oNewAPPBOK_BookingBE.DLYREF_ReasonPalletsDamaged = dr["DLYREF_ReasonPalletsDamaged"] != DBNull.Value ? Convert.ToBoolean(dr["DLYREF_ReasonPalletsDamaged"]) : false;
                        oNewAPPBOK_BookingBE.DLYREF_ReasonPackagingDamaged = dr["DLYREF_ReasonPackagingDamaged"] != DBNull.Value ? Convert.ToBoolean(dr["DLYREF_ReasonPackagingDamaged"]) : false;
                        oNewAPPBOK_BookingBE.DLYREF_ReasonUnsafeLoad = dr["DLYREF_ReasonUnsafeLoad"] != DBNull.Value ? Convert.ToBoolean(dr["DLYREF_ReasonUnsafeLoad"]) : false;
                        oNewAPPBOK_BookingBE.DLYREF_ReasonWrongAddress = dr["DLYREF_ReasonWrongAddress"] != DBNull.Value ? Convert.ToBoolean(dr["DLYREF_ReasonWrongAddress"]) : false;
                        oNewAPPBOK_BookingBE.DLYREF_ReasonRefusedTowait = dr["DLYREF_ReasonRefusedTowait"] != DBNull.Value ? Convert.ToBoolean(dr["DLYREF_ReasonRefusedTowait"]) : false;
                        oNewAPPBOK_BookingBE.DLYREF_ReasonNottoODSpecification = dr["DLYREF_ReasonNottoODSpecification"] != DBNull.Value ? Convert.ToBoolean(dr["DLYREF_ReasonNottoODSpecification"]) : false;
                        oNewAPPBOK_BookingBE.DLYREF_ReasonOther = dr["DLYREF_ReasonOther"] != DBNull.Value ? Convert.ToBoolean(dr["DLYREF_ReasonOther"]) : false;
                        // Sprint 1 - Point 8 - End


                        if (oAPPBOK_BookingBE.Action == "DeliveryArrived")
                            oNewAPPBOK_BookingBE.VehicleType.VehicleTypeID = dr["VehicleTypeID"] != DBNull.Value ? Convert.ToInt32(dr["VehicleTypeID"]) : -1;

                        // Sprint 1 - Point 4 - Start - For Booking Comments.
                        oNewAPPBOK_BookingBE.BookingComments = Convert.ToString(dr["BookingComments"]);
                        // Sprint 1 - Point 4 - End - For Booking Comments.

                        if (dt.Columns.Contains("IsVenodrPalletChecking"))
                            oNewAPPBOK_BookingBE.IsVenodrPalletChecking = dr["IsVenodrPalletChecking"] != DBNull.Value ? Convert.ToBoolean(dr["IsVenodrPalletChecking"]) : false;

                        if (dt.Columns.Contains("IsStandardPalletChecking"))
                            oNewAPPBOK_BookingBE.IsStandardPalletChecking = dr["IsStandardPalletChecking"] != DBNull.Value ? Convert.ToBoolean(dr["IsStandardPalletChecking"]) : false;

                        if (dt.Columns.Contains("IsEnableHazardouesItemPrompt"))
                            oNewAPPBOK_BookingBE.IsEnableHazardouesItemPrompt = dr["IsEnableHazardouesItemPrompt"] != DBNull.Value ? Convert.ToBoolean(dr["IsEnableHazardouesItemPrompt"]) : false;

                        if (dt.Columns.Contains("ISPM15FromCountryID"))
                            oNewAPPBOK_BookingBE.ISPM15FromCountryID = dr["ISPM15FromCountryID"] != DBNull.Value ? Convert.ToInt32(dr["ISPM15FromCountryID"]) : 0;
                        
                        if (dt.Columns.Contains("IsISPM15Applied"))
                            oNewAPPBOK_BookingBE.ISPM15CountryPalletChecking = dr["IsISPM15Applied"] != DBNull.Value ? Convert.ToBoolean(dr["IsISPM15Applied"]) : false;

                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetDeliveryUnloadDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                        oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : "";

                        oNewAPPBOK_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.NumberOfLift = dr["NumberOfLift"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLift"].ToString()) : (int?)null;

                        oNewAPPBOK_BookingBE.DLYUNL_NumberOfPallet = dr["DLYUNL_NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["DLYUNL_NumberOfPallet"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.DLYUNL_NumberOfCartons = dr["DLYUNL_NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["DLYUNL_NumberOfCartons"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.DLYUNL_RefusedPallet = dr["DLYUNL_RefusedPallet"] != DBNull.Value ? Convert.ToInt32(dr["DLYUNL_RefusedPallet"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.DLYUNL_RefusedCarton = dr["DLYUNL_RefusedCarton"] != DBNull.Value ? Convert.ToInt32(dr["DLYUNL_RefusedCarton"].ToString()) : (int?)null;


                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<string> GetUnloadImageDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<string> oAPPBOK_BookingBEList = new List<string>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        string ImageName = dr["ImageName"] != DBNull.Value ? dr["ImageName"].ToString() : string.Empty;
                        oAPPBOK_BookingBEList.Add(ImageName);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public int? DeleteBookingDetailDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@FixedSlotID", oAPPBOK_BookingBE.FixedSlot.FixedSlotID);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.UserID);
                param[index++] = new SqlParameter("@IsCrossDayBefore", oAPPBOK_BookingBE.IsCrossDayBefore);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? addEditBookingDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingStatusID", oAPPBOK_BookingBE.BookingStatusID);
                param[index++] = new SqlParameter("@DLYARR_OperatorInital", oAPPBOK_BookingBE.DLYARR_OperatorInital);
                param[index++] = new SqlParameter("@PKID", Convert.ToInt32(oAPPBOK_BookingBE.PKID.Trim()));
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.FixedSlot.User.UserID);
                param[index++] = new SqlParameter("@TransactionComments", oAPPBOK_BookingBE.TransactionComments);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


        /// <summary>
        /// Used for Managing Delivery Unloaded First step
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? updateDeliveryUnloadedFirstStepDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@DLYUNL_OperatorInital", oAPPBOK_BookingBE.DLYUNL_OperatorInital);
                param[index++] = new SqlParameter("@DLYUNL_DateTime", oAPPBOK_BookingBE.DLYUNL_DateTime);
                param[index++] = new SqlParameter("@PKID", Convert.ToInt32(oAPPBOK_BookingBE.PKID.Trim()));

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Used for managing Delivery Unloaded Accept All
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? updateDeliveryUnloadedAcceptAllDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@PKID", Convert.ToInt32(oAPPBOK_BookingBE.PKID.Trim()));
                param[index++] = new SqlParameter("@DLYUNL_NumberOfLift", oAPPBOK_BookingBE.DLYUNL_NumberOfLift);
                param[index++] = new SqlParameter("@DLYUNL_NumberOfPallet", oAPPBOK_BookingBE.DLYUNL_NumberOfPallet);
                param[index++] = new SqlParameter("@DLYUNL_NumberOfCartons", oAPPBOK_BookingBE.DLYUNL_NumberOfCartons);
                param[index++] = new SqlParameter("@DLYUNL_RefusedCarton", oAPPBOK_BookingBE.DLYUNL_RefusedCarton);
                param[index++] = new SqlParameter("@DLYUNL_RefusedPallet", oAPPBOK_BookingBE.DLYUNL_RefusedPallet);
                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.FixedSlot.VendorID);
                param[index++] = new SqlParameter("@BookingStatusID", oAPPBOK_BookingBE.BookingStatusID);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.FixedSlot.User.UserID);
                param[index++] = new SqlParameter("@TransactionComments", oAPPBOK_BookingBE.TransactionComments);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


        /// <summary>
        /// User for Managing Delivery Unloaded Final step
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? updateDeliveryUnloadedFinalStepDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[31];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@PKID", Convert.ToInt32(oAPPBOK_BookingBE.PKID.Trim()));
                param[index++] = new SqlParameter("@BookingStatusID", oAPPBOK_BookingBE.BookingStatusID);
                param[index++] = new SqlParameter("@DLYREF_ReasonArrivedEarly", oAPPBOK_BookingBE.DLYREF_ReasonArrivedEarly);
                param[index++] = new SqlParameter("@DLYREF_ReasonArrivedLate", oAPPBOK_BookingBE.DLYREF_ReasonArrivedLate);
                param[index++] = new SqlParameter("@DLYREF_ReasonNoPaperworkOndisplay", oAPPBOK_BookingBE.DLYREF_ReasonNoPaperworkOndisplay);
                param[index++] = new SqlParameter("@DLYREF_ReasonPalletsDamaged", oAPPBOK_BookingBE.DLYREF_ReasonPalletsDamaged);
                param[index++] = new SqlParameter("@DLYREF_ReasonPackagingDamaged", oAPPBOK_BookingBE.DLYREF_ReasonPackagingDamaged);
                param[index++] = new SqlParameter("@DLYREF_ReasonUnsafeLoad", oAPPBOK_BookingBE.DLYREF_ReasonUnsafeLoad);
                param[index++] = new SqlParameter("@DLYREF_ReasonWrongAddress", oAPPBOK_BookingBE.DLYREF_ReasonWrongAddress);
                param[index++] = new SqlParameter("@DLYREF_ReasonRefusedTowait", oAPPBOK_BookingBE.DLYREF_ReasonRefusedTowait);
                param[index++] = new SqlParameter("@DLYREF_ReasonNottoODSpecification", oAPPBOK_BookingBE.DLYREF_ReasonNottoODSpecification);
                param[index++] = new SqlParameter("@DLYREF_ReasonOther", oAPPBOK_BookingBE.DLYREF_ReasonOther);
                param[index++] = new SqlParameter("@DLYREF_ReasonComments", oAPPBOK_BookingBE.DLYREF_ReasonComments);
                param[index++] = new SqlParameter("@DLYREF_EuroPalletsDelivered", oAPPBOK_BookingBE.DLYREF_EuroPalletsDelivered);
                param[index++] = new SqlParameter("@DLYREF_EuroPalletsReturned", oAPPBOK_BookingBE.DLYREF_EuroPalletsReturned);
                param[index++] = new SqlParameter("@DLYREF_EuroPalletsComments", oAPPBOK_BookingBE.DLYREF_EuroPalletsComments);
                param[index++] = new SqlParameter("@DLYREF_UKStandardDelivered", oAPPBOK_BookingBE.DLYREF_UKStandardDelivered);
                param[index++] = new SqlParameter("@DLYREF_UKStandardReturned", oAPPBOK_BookingBE.DLYREF_UKStandardReturned);
                param[index++] = new SqlParameter("@DLYREF_UKStandardComments", oAPPBOK_BookingBE.DLYREF_UKStandardComments);
                param[index++] = new SqlParameter("@DLYREF_CHEPDelivered", oAPPBOK_BookingBE.DLYREF_CHEPDelivered);
                param[index++] = new SqlParameter("@DLYREF_CHEPReturned", oAPPBOK_BookingBE.DLYREF_CHEPReturned);
                param[index++] = new SqlParameter("@DLYREF_CHEPComments", oAPPBOK_BookingBE.DLYREF_CHEPComments);
                param[index++] = new SqlParameter("@DLYREF_OthersDelivered", oAPPBOK_BookingBE.DLYREF_OthersDelivered);
                param[index++] = new SqlParameter("@DLYREF_OthersReturned", oAPPBOK_BookingBE.DLYREF_OthersReturned);
                param[index++] = new SqlParameter("@DLYREF_OthersComments", oAPPBOK_BookingBE.DLYREF_OthersComments);

                param[index++] = new SqlParameter("@DLYREF_SupervisorID", oAPPBOK_BookingBE.DLYREF_SupervisorID);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.FixedSlot.User.UserID);
                param[index++] = new SqlParameter("@TransactionComments", oAPPBOK_BookingBE.TransactionComments);
                param[index++] = new SqlParameter("@DLYREF_FailedToDeclareHazardousGoods", oAPPBOK_BookingBE.DLYREF_FailedToDeclareHazardousGoods);
                param[index++] = new SqlParameter("@IsUnloadImagesUploaded", oAPPBOK_BookingBE.IsUnloadImagesUploaded);


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


        public int? UpdateDeliveryUnloadedImagesDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@PKID", Convert.ToInt32(oAPPBOK_BookingBE.PKID.Trim()));
                param[index++] = new SqlParameter("@DLYUNL_UploadedFileName", oAPPBOK_BookingBE.DLYUNL_UploadedFileName);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Used for managing Delivery Refusal
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? addEditDeliveryRefusalDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[14];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@PKID", Convert.ToInt32(oAPPBOK_BookingBE.PKID.Trim()));
                param[index++] = new SqlParameter("@DLYREF_ReasonArrivedEarly", oAPPBOK_BookingBE.DLYREF_ReasonArrivedEarly);
                param[index++] = new SqlParameter("@DLYREF_ReasonArrivedLate", oAPPBOK_BookingBE.DLYREF_ReasonArrivedLate);
                param[index++] = new SqlParameter("@DLYREF_ReasonNoPaperworkOndisplay", oAPPBOK_BookingBE.DLYREF_ReasonNoPaperworkOndisplay);
                param[index++] = new SqlParameter("@DLYREF_ReasonPalletsDamaged", oAPPBOK_BookingBE.DLYREF_ReasonPalletsDamaged);
                param[index++] = new SqlParameter("@DLYREF_ReasonPackagingDamaged", oAPPBOK_BookingBE.DLYREF_ReasonPackagingDamaged);
                param[index++] = new SqlParameter("@DLYREF_ReasonWrongAddress", oAPPBOK_BookingBE.DLYREF_ReasonWrongAddress);
                param[index++] = new SqlParameter("@DLYREF_ReasonRefusedTowait", oAPPBOK_BookingBE.DLYREF_ReasonRefusedTowait);
                param[index++] = new SqlParameter("@DLYREF_ReasonNottoODSpecification", oAPPBOK_BookingBE.DLYREF_ReasonNottoODSpecification);
                param[index++] = new SqlParameter("@DLYREF_ReasonOther", oAPPBOK_BookingBE.DLYREF_ReasonOther);
                param[index++] = new SqlParameter("@DLYREF_ReasonComments", oAPPBOK_BookingBE.DLYREF_ReasonComments);
                param[index++] = new SqlParameter("@DLYREF_ReasonUnsafeLoad", oAPPBOK_BookingBE.DLYREF_ReasonUnsafeLoad);
                param[index++] = new SqlParameter("@DLYREF_SupervisorID", oAPPBOK_BookingBE.DLYREF_SupervisorID);



                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? addEditUnexpectedDeliveryDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[23];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.FixedSlot.SiteID);
                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.FixedSlot.Vendor.VendorID);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@SlotTimeID", oAPPBOK_BookingBE.FixedSlot.SlotTimeID);
                param[index++] = new SqlParameter("@NumberOfPallet", oAPPBOK_BookingBE.NumberOfPallet);
                param[index++] = new SqlParameter("@NumberOfCartons", oAPPBOK_BookingBE.NumberOfCartons);
                param[index++] = new SqlParameter("@DLYARR_OperatorInital", oAPPBOK_BookingBE.DLYARR_OperatorInital);
                param[index++] = new SqlParameter("@SupplierType", oAPPBOK_BookingBE.SupplierType);
                param[index++] = new SqlParameter("@PreAdviseNotification", oAPPBOK_BookingBE.PreAdviseNotification);
                param[index++] = new SqlParameter("@BookingTypeID", oAPPBOK_BookingBE.BookingTypeID);
                param[index++] = new SqlParameter("@BookingStatusID", oAPPBOK_BookingBE.BookingStatusID);
                param[index++] = new SqlParameter("@BookingRef", oAPPBOK_BookingBE.BookingRef);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.UserID);
                param[index++] = new SqlParameter("@WeekDay", oAPPBOK_BookingBE.WeekDay);
                param[index++] = new SqlParameter("@TransactionComments", oAPPBOK_BookingBE.TransactionComments);
                param[index++] = new SqlParameter("@NumberOfLine", oAPPBOK_BookingBE.NumberOfLines);
                param[index++] = new SqlParameter("@PurchaseOrdersIds", oAPPBOK_BookingBE.PurchaseOrdersIDs);
                param[index++] = new SqlParameter("@PurchaseOrders", oAPPBOK_BookingBE.PurchaseOrders);
                param[index++] = new SqlParameter("@CarrierID", oAPPBOK_BookingBE.VendorCarrierID);
                param[index++] = new SqlParameter("@UnknownVendorName", oAPPBOK_BookingBE.FixedSlot.Vendor.VendorName);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@ISPM15CountryPalletChecking", oAPPBOK_BookingBE.ISPM15CountryPalletChecking);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? addEditUnexpectedDeliveryCarrierDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[18];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.FixedSlot.SiteID);
                param[index++] = new SqlParameter("@CarrierID", oAPPBOK_BookingBE.FixedSlot.CarrierID);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@SlotTimeID", oAPPBOK_BookingBE.FixedSlot.SlotTimeID);
                param[index++] = new SqlParameter("@NumberOfPallet", oAPPBOK_BookingBE.NumberOfPallet);
                param[index++] = new SqlParameter("@NumberOfCartons", oAPPBOK_BookingBE.NumberOfCartons);
                param[index++] = new SqlParameter("@DLYARR_OperatorInital", oAPPBOK_BookingBE.DLYARR_OperatorInital);
                param[index++] = new SqlParameter("@SupplierType", oAPPBOK_BookingBE.SupplierType);
                param[index++] = new SqlParameter("@PreAdviseNotification", oAPPBOK_BookingBE.PreAdviseNotification);
                param[index++] = new SqlParameter("@BookingTypeID", oAPPBOK_BookingBE.BookingTypeID);
                param[index++] = new SqlParameter("@BookingStatusID", oAPPBOK_BookingBE.BookingStatusID);
                param[index++] = new SqlParameter("@BookingRef", oAPPBOK_BookingBE.BookingRef);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.UserID);
                param[index++] = new SqlParameter("@WeekDay", oAPPBOK_BookingBE.WeekDay);
                param[index++] = new SqlParameter("@TransactionComments", oAPPBOK_BookingBE.TransactionComments);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@ISPM15CountryPalletChecking", oAPPBOK_BookingBE.ISPM15CountryPalletChecking);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? addEditQualityCheckDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@PKID", oAPPBOK_BookingBE.PKID);
                param[index++] = new SqlParameter("@BookingStatusID", oAPPBOK_BookingBE.BookingStatusID);
                param[index++] = new SqlParameter("@DLYQCHK_DeliveryBookedCorrectly", oAPPBOK_BookingBE.DLYQCHK_DeliveryBookedCorrectly);
                param[index++] = new SqlParameter("@DLYQCHK_IncorrectlyBookedComments", oAPPBOK_BookingBE.DLYQCHK_IncorrectlyBookedComments);
                param[index++] = new SqlParameter("@DLYARR_OperatorInital", oAPPBOK_BookingBE.DLYARR_OperatorInital);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.FixedSlot.User.UserID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetBookingHistoryDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@PKID", oAPPBOK_BookingBE.PKID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
                    oNewAPPBOK_BookingBE.FixedSlot.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    oNewAPPBOK_BookingBE.VehicleType = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE();
                    oNewAPPBOK_BookingBE.DoorNoSetup = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_DoorNoSetupBE();
                    oNewAPPBOK_BookingBE.FixedSlot.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();


                    oNewAPPBOK_BookingBE.BookingStatus = dr["BookingStatus"].ToString();
                    oNewAPPBOK_BookingBE.FixedSlot.User.FullName = dr["USERNAME"] != DBNull.Value ? dr["USERNAME"].ToString() : "";
                    oNewAPPBOK_BookingBE.HistoryOperatorInitials = dr["HistoryOperatorInitials"] != DBNull.Value ? dr["HistoryOperatorInitials"].ToString() : "";
                    oNewAPPBOK_BookingBE.HistoryDate = dr["HistoryDate"] != DBNull.Value ? Convert.ToDateTime(dr["HistoryDate"].ToString()).ToString("dd/MM/yyyy - HH:mm") : "";
                    oNewAPPBOK_BookingBE.HistoryComments = dr["HistoryComments"] != DBNull.Value ? dr["HistoryComments"].ToString() : "";


                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetTimeSlotInfoDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@PKID", oAPPBOK_BookingBE.PKID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.TimeSlotWindow = dr["TimeSlotWindow"].ToString();
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }


        public DataTable GetVolumeDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.FixedSlot.SiteID);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@WeekDay", oAPPBOK_BookingBE.WeekDay);


                DataSet ds;
                if (oAPPBOK_BookingBE.SupplierType.Equals("W"))
                    ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_TimeWindow_Volume", param);
                else
                    ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking_Volume", param);

                if (ds != null && ds.Tables.Count > 0)
                    dt = ds.Tables[0];


            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }
        public DataTable GetAllBookingDetailDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[17];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.FixedSlot.SiteID);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@PKID", oAPPBOK_BookingBE.PKID);
                param[index++] = new SqlParameter("@StockPlannerID", oAPPBOK_BookingBE.StockPlannerID);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.UserID);
                param[index++] = new SqlParameter("@FromDate", oAPPBOK_BookingBE.FromDate);
                param[index++] = new SqlParameter("@ToDate", oAPPBOK_BookingBE.ToDate);
                param[index++] = new SqlParameter("@SupplierType", oAPPBOK_BookingBE.SupplierType);
                param[index++] = new SqlParameter("@SelectedScheduleDate", oAPPBOK_BookingBE.SelectedScheduleDate);
                param[index++] = new SqlParameter("@ViewType", oAPPBOK_BookingBE.ViewType);
                param[index++] = new SqlParameter("@SiteIDs", oAPPBOK_BookingBE.FixedSlot.SiteIDs);
                param[index++] = new SqlParameter("@BookingRef", oAPPBOK_BookingBE.BookingRef);
                param[index++] = new SqlParameter("@SKUD", oAPPBOK_BookingBE.SKU);
                param[index++] = new SqlParameter("@Viking", oAPPBOK_BookingBE.CatCode);
                param[index++] = new SqlParameter("@IssueFound", oAPPBOK_BookingBE.IssuesFound);

                if (oAPPBOK_BookingBE.Action == "ShowVendorBooking")
                {
                    param[index++] = new SqlParameter("@PageIndex", oAPPBOK_BookingBE.PageIndex);
                }
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }


        //****************************************************************
        public DataTable GetSchedulePOLineReport(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.FixedSlot.SiteID);
                param[index++] = new SqlParameter("@Lines", oAPPBOK_BookingBE.FixedSlot.FixedSlotID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "sp_SchedulePOLineReport", param);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }
        //****************************************************************

        public List<APPBOK_BookingBE> GetVendorBookingFixedSlotDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.VendorID);
                param[index++] = new SqlParameter("@CarrierID", oAPPBOK_BookingBE.Carrier.CarrierID);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@SelectedScheduleDate", oAPPBOK_BookingBE.SelectedScheduleDate);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["SlotTime"].ToString();
                    oNewAPPBOK_BookingBE.SlotTime.SlotTimeID = dr["SlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["SlotTimeID"].ToString()) : 0; // Convert.ToInt32(dr["SlotTimeID"].ToString());
                    oNewAPPBOK_BookingBE.BookingRef = dr["BookingReferenceID"].ToString();
                    oNewAPPBOK_BookingBE.DoorNoSetup = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_DoorNoSetupBE();
                    oNewAPPBOK_BookingBE.DoorNoSetup.DoorNumber = dr["DoorNumber"] != DBNull.Value ? Convert.ToString(dr["DoorNumber"].ToString()) : string.Empty;
                    oNewAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = dr["SiteDoorNumberID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorNumberID"].ToString()) : 0; //Convert.ToInt32(dr["SiteDoorNumberID"].ToString());
                    oNewAPPBOK_BookingBE.SlotType = dr["SlotType"].ToString();
                    oNewAPPBOK_BookingBE.Status = dr["Status"].ToString();
                    oNewAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
                    oNewAPPBOK_BookingBE.FixedSlot.FixedSlotID = dr["FixedSlotID"] != DBNull.Value ? Convert.ToInt32(dr["FixedSlotID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.BookingDay = dr["BookingDay"].ToString();
                    oNewAPPBOK_BookingBE.BookingDate = dr["BookingDate"] != DBNull.Value ? dr["BookingDate"].ToString() : "";
                    if (ds.Tables[0].Columns.Contains("AllocationType"))
                        oNewAPPBOK_BookingBE.FixedSlot.AllocationType = dr["AllocationType"] != DBNull.Value ? dr["AllocationType"].ToString() : string.Empty;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }


            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }


        public List<APPBOK_BookingBE> GetVendorBookingWindowDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@VendorId", oAPPBOK_BookingBE.VendorIDs);
                param[index++] = new SqlParameter("@CarrierId", oAPPBOK_BookingBE.Carrier.CarrierID);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@Pallet", oAPPBOK_BookingBE.NumberOfPallet.ToString());
                param[index++] = new SqlParameter("@Cartons", oAPPBOK_BookingBE.NumberOfCartons.ToString());
                param[index++] = new SqlParameter("@Line", oAPPBOK_BookingBE.NumberOfLines.ToString());

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_GetTimeWindowForCarrier", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.TimeWindow = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_TimeWindowBE();

                    oNewAPPBOK_BookingBE.TimeWindow.TimeWindowID = Convert.ToInt32(dr["TimeWindowID"].ToString());
                    oNewAPPBOK_BookingBE.TimeWindow.TotalHrsMinsAvailable = dr["TotalHrsMinsAvailable"].ToString();
                    oNewAPPBOK_BookingBE.TimeWindow.Priority = Convert.ToInt32(dr["Priority"].ToString());
                    oNewAPPBOK_BookingBE.TimeWindow.MaximumTime = dr["MaximumTime"].ToString();
                    oNewAPPBOK_BookingBE.TimeWindow.IsCheckSKUTable = Convert.ToBoolean(dr["IsCheckSKUTable"].ToString());
                    oNewAPPBOK_BookingBE.TimeWindow.DoorNumber = dr["DoorNumber"].ToString();
                    oNewAPPBOK_BookingBE.TimeWindow.StartTime = dr["StartSlotTime"].ToString();
                    oNewAPPBOK_BookingBE.TimeWindow.EndTime = dr["EndSlotTime"].ToString();
                    oNewAPPBOK_BookingBE.TimeWindow.MaximumPallets = Convert.ToInt32(dr["MaxOfPallet"].ToString());
                    oNewAPPBOK_BookingBE.TimeWindow.MaximumCartons = Convert.ToInt32(dr["MaxOfCartons"].ToString());
                    oNewAPPBOK_BookingBE.TimeWindow.MaximumLines = Convert.ToInt32(dr["MaxOfLine"].ToString());
                    oNewAPPBOK_BookingBE.ScheduleDate = oAPPBOK_BookingBE.ScheduleDate;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);

                    //oNewAPPBOK_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    //oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["SlotTime"].ToString();
                    //oNewAPPBOK_BookingBE.SlotTime.SlotTimeID = dr["SlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["SlotTimeID"].ToString()) : 0; // Convert.ToInt32(dr["SlotTimeID"].ToString());
                    //oNewAPPBOK_BookingBE.BookingRef = dr["BookingReferenceID"].ToString();
                    //oNewAPPBOK_BookingBE.DoorNoSetup = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_DoorNoSetupBE();
                    //oNewAPPBOK_BookingBE.DoorNoSetup.DoorNumber = dr["DoorNumber"] != DBNull.Value ? Convert.ToString(dr["DoorNumber"].ToString()) : string.Empty;
                    //oNewAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = dr["SiteDoorNumberID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorNumberID"].ToString()) : 0; //Convert.ToInt32(dr["SiteDoorNumberID"].ToString());
                    //oNewAPPBOK_BookingBE.SlotType = dr["SlotType"].ToString();
                    //oNewAPPBOK_BookingBE.Status = dr["Status"].ToString();
                    //oNewAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
                    //oNewAPPBOK_BookingBE.FixedSlot.FixedSlotID = dr["FixedSlotID"] != DBNull.Value ? Convert.ToInt32(dr["FixedSlotID"].ToString()) : -1;
                    //oNewAPPBOK_BookingBE.BookingDay = dr["BookingDay"].ToString();
                    //oNewAPPBOK_BookingBE.BookingDate = dr["BookingDate"] != DBNull.Value ? dr["BookingDate"].ToString() : "";
                    //oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }


            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetVendorBookingFixedSlotDoorDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.VendorID);
                param[index++] = new SqlParameter("@CarrierID", oAPPBOK_BookingBE.Carrier.CarrierID);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@VehicleTypeID", oAPPBOK_BookingBE.VehicleType.VehicleTypeID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.VehicleType = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE();
                    oNewAPPBOK_BookingBE.VehicleType.VehicleTypeID = dr["VehicleTypeID"] != DBNull.Value ? Convert.ToInt32(dr["VehicleTypeID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["SlotTime"].ToString();
                    oNewAPPBOK_BookingBE.SlotTime.SlotTimeID = dr["SlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["SlotTimeID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.Prioirty = dr["Priority"].ToString();
                    oNewAPPBOK_BookingBE.DoorNoSetup = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_DoorNoSetupBE();
                    oNewAPPBOK_BookingBE.DoorNoSetup.DoorNumber = dr["DoorNumber"] != DBNull.Value ? Convert.ToString(dr["DoorNumber"]) : string.Empty;
                    oNewAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = dr["SiteDoorNumberID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorNumberID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
                    oNewAPPBOK_BookingBE.FixedSlot.FixedSlotID = dr["FixedSlotID"] != DBNull.Value ? Convert.ToInt32(dr["FixedSlotID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.FixedSlot.MaximumCatrons = dr["MaximumCatrons"] != DBNull.Value ? Convert.ToInt32(dr["MaximumCatrons"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.FixedSlot.MaximumLines = dr["MaximumLines"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLines"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.FixedSlot.MaximumPallets = dr["MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["MaximumPallets"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.ScheduleDate = dr["ScheduleDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduleDate"]) : (DateTime?)null;
                    if (ds.Tables[0].Columns.Contains("OrderBY"))
                        oNewAPPBOK_BookingBE.SlotTime.OrderBYID = dr["OrderBY"] != DBNull.Value ? Convert.ToInt32(dr["OrderBY"].ToString()) : -1;
                    if (ds.Tables[0].Columns.Contains("AllocationType"))
                        oNewAPPBOK_BookingBE.FixedSlot.AllocationType = dr["AllocationType"] != DBNull.Value ? dr["AllocationType"].ToString() : string.Empty;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }


            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }


        public int? AddVendorBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[42];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@BookingReferenceID", oAPPBOK_BookingBE.BookingRef);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@SupplierType", oAPPBOK_BookingBE.SupplierType);
                param[index++] = new SqlParameter("@DeliveryTypeID", oAPPBOK_BookingBE.Delivery.DeliveryTypeID);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@CarrierID", oAPPBOK_BookingBE.Carrier.CarrierID);
                param[index++] = new SqlParameter("@OtherCarrier", oAPPBOK_BookingBE.OtherCarrier);
                param[index++] = new SqlParameter("@VehicleTypeID", oAPPBOK_BookingBE.VehicleType.VehicleTypeID);
                param[index++] = new SqlParameter("@OtherVehicle", oAPPBOK_BookingBE.OtherVehicle);
                param[index++] = new SqlParameter("@PreAdviseNotification", oAPPBOK_BookingBE.PreAdviseNotification);


                param[index++] = new SqlParameter("@SlotTimeID", oAPPBOK_BookingBE.SlotTime.SlotTimeID);
                param[index++] = new SqlParameter("@SiteDoorNumberID", oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID);
                param[index++] = new SqlParameter("@DoorNumber", oAPPBOK_BookingBE.DoorNoSetup.DoorNumber);

                param[index++] = new SqlParameter("@BookingStatusID", oAPPBOK_BookingBE.BookingStatusID);

                param[index++] = new SqlParameter("@BookingTypeID", oAPPBOK_BookingBE.BookingTypeID);
                param[index++] = new SqlParameter("@FixedSlotID", oAPPBOK_BookingBE.FixedSlot.FixedSlotID);


                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.VendorID);

                param[index++] = new SqlParameter("@NumberOfPallet", oAPPBOK_BookingBE.NumberOfPallet);
                param[index++] = new SqlParameter("@NumberOfCartons", oAPPBOK_BookingBE.NumberOfCartons);
                param[index++] = new SqlParameter("@NumberOfLift", oAPPBOK_BookingBE.NumberOfLift);
                param[index++] = new SqlParameter("@NumberOfLine", oAPPBOK_BookingBE.NumberOfLines);

                param[index++] = new SqlParameter("@PurchaseOrdersIds", oAPPBOK_BookingBE.PurchaseOrdersIDs);
                param[index++] = new SqlParameter("@PurchaseOrders", oAPPBOK_BookingBE.PurchaseOrders);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.UserID);
                param[index++] = new SqlParameter("@WeekDay", oAPPBOK_BookingBE.WeekDay);
                param[index++] = new SqlParameter("@IsBookingAmended", oAPPBOK_BookingBE.IsBookingAmended);
                param[index++] = new SqlParameter("@IsOnlineBooking", oAPPBOK_BookingBE.IsOnlineBooking);
                param[index++] = new SqlParameter("@BookingComments", oAPPBOK_BookingBE.BookingComments);

                param[index++] = new SqlParameter("@ProvisionalReason", oAPPBOK_BookingBE.ProvisionalReason);
                param[index++] = new SqlParameter("@ProvisionalReasonType", oAPPBOK_BookingBE.ProvisionalReasonType);
                param[index++] = new SqlParameter("@IsBookingAccepted", oAPPBOK_BookingBE.IsBookingAccepted);
                param[index++] = new SqlParameter("@IsEnableHazardouesItemPrompt", oAPPBOK_BookingBE.IsEnableHazardouesItemPrompt);
                param[index++] = new SqlParameter("@ISPM15CountryPalletChecking", oAPPBOK_BookingBE.ISPM15CountryPalletChecking);
                param[index++] = new SqlParameter("@ISPM15FromCountryID", oAPPBOK_BookingBE.ISPM15FromCountryID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? AddCarrierBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[33];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@BookingReferenceID", oAPPBOK_BookingBE.BookingRef);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@SupplierType", oAPPBOK_BookingBE.SupplierType);
                param[index++] = new SqlParameter("@DeliveryTypeID", oAPPBOK_BookingBE.Delivery.DeliveryTypeID);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@CarrierID", oAPPBOK_BookingBE.Carrier.CarrierID);
                param[index++] = new SqlParameter("@OtherCarrier", oAPPBOK_BookingBE.OtherCarrier);
                param[index++] = new SqlParameter("@VehicleTypeID", oAPPBOK_BookingBE.VehicleType.VehicleTypeID);
                param[index++] = new SqlParameter("@OtherVehicle", oAPPBOK_BookingBE.OtherVehicle);
                param[index++] = new SqlParameter("@PreAdviseNotification", oAPPBOK_BookingBE.PreAdviseNotification);


                param[index++] = new SqlParameter("@SlotTimeID", oAPPBOK_BookingBE.SlotTime.SlotTimeID);
                param[index++] = new SqlParameter("@SiteDoorNumberID", oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID);
                param[index++] = new SqlParameter("@DoorNumber", oAPPBOK_BookingBE.DoorNoSetup.DoorNumber);

                param[index++] = new SqlParameter("@BookingStatusID", oAPPBOK_BookingBE.BookingStatusID);

                param[index++] = new SqlParameter("@BookingTypeID", oAPPBOK_BookingBE.BookingTypeID);
                param[index++] = new SqlParameter("@FixedSlotID", oAPPBOK_BookingBE.FixedSlot.FixedSlotID);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.UserID);
                param[index++] = new SqlParameter("@WeekDay", oAPPBOK_BookingBE.WeekDay);

                param[index++] = new SqlParameter("@IsBookingAmended", oAPPBOK_BookingBE.IsBookingAmended);
                param[index++] = new SqlParameter("@IsOnlineBooking", oAPPBOK_BookingBE.IsOnlineBooking);
                param[index++] = new SqlParameter("@BookingComments", oAPPBOK_BookingBE.BookingComments);
                param[index++] = new SqlParameter("@IsMultiVendCarrier", oAPPBOK_BookingBE.IsMultiVendCarrier);

                param[index++] = new SqlParameter("@ProvisionalReason", oAPPBOK_BookingBE.ProvisionalReason);
                param[index++] = new SqlParameter("@ProvisionalReasonType", oAPPBOK_BookingBE.ProvisionalReasonType);
                param[index++] = new SqlParameter("@IsBookingAccepted", oAPPBOK_BookingBE.IsBookingAccepted);
                param[index++] = new SqlParameter("@IsEnableHazardouesItemPrompt", oAPPBOK_BookingBE.IsEnableHazardouesItemPrompt);
                param[index++] = new SqlParameter("@ISPM15CountryPalletChecking", oAPPBOK_BookingBE.ISPM15CountryPalletChecking);
                param[index++] = new SqlParameter("@ISPM15FromCountryID", oAPPBOK_BookingBE.ISPM15FromCountryID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? AddCarrierWindowBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[42];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@BookingReferenceID", oAPPBOK_BookingBE.BookingRef);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@SupplierType", oAPPBOK_BookingBE.SupplierType);
                param[index++] = new SqlParameter("@DeliveryTypeID", oAPPBOK_BookingBE.Delivery.DeliveryTypeID);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@CarrierID", oAPPBOK_BookingBE.Carrier.CarrierID);
                param[index++] = new SqlParameter("@OtherCarrier", oAPPBOK_BookingBE.OtherCarrier);
                param[index++] = new SqlParameter("@VehicleTypeID", oAPPBOK_BookingBE.VehicleType.VehicleTypeID);
                param[index++] = new SqlParameter("@OtherVehicle", oAPPBOK_BookingBE.OtherVehicle);
                param[index++] = new SqlParameter("@PreAdviseNotification", oAPPBOK_BookingBE.PreAdviseNotification);


                param[index++] = new SqlParameter("@SlotTimeID", oAPPBOK_BookingBE.SlotTime.SlotTimeID);
                param[index++] = new SqlParameter("@SiteDoorNumberID", oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID);
                param[index++] = new SqlParameter("@DoorNumber", oAPPBOK_BookingBE.DoorNoSetup.DoorNumber);

                param[index++] = new SqlParameter("@BookingStatusID", oAPPBOK_BookingBE.BookingStatusID);

                param[index++] = new SqlParameter("@BookingTypeID", oAPPBOK_BookingBE.BookingTypeID);
                param[index++] = new SqlParameter("@FixedSlotID", oAPPBOK_BookingBE.FixedSlot.FixedSlotID);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.UserID);
                param[index++] = new SqlParameter("@WeekDay", oAPPBOK_BookingBE.WeekDay);

                param[index++] = new SqlParameter("@IsBookingAmended", oAPPBOK_BookingBE.IsBookingAmended);
                param[index++] = new SqlParameter("@IsOnlineBooking", oAPPBOK_BookingBE.IsOnlineBooking);
                param[index++] = new SqlParameter("@BookingComments", oAPPBOK_BookingBE.BookingComments);
                param[index++] = new SqlParameter("@IsMultiVendCarrier", oAPPBOK_BookingBE.IsMultiVendCarrier);
                param[index++] = new SqlParameter("@TimeWindowID", oAPPBOK_BookingBE.TimeWindow.TimeWindowID);
                param[index++] = new SqlParameter("@NonWindowFromTimeID", oAPPBOK_BookingBE.NonWindowFromTimeID);
                param[index++] = new SqlParameter("@NonWindowToTimeID", oAPPBOK_BookingBE.NonWindowToTimeID);
                param[index++] = new SqlParameter("@ProvisionalReason", oAPPBOK_BookingBE.ProvisionalReason);
                param[index++] = new SqlParameter("@IsBookingAccepted", oAPPBOK_BookingBE.IsBookingAccepted);
                param[index++] = new SqlParameter("@IsEnableHazardouesItemPrompt", oAPPBOK_BookingBE.IsEnableHazardouesItemPrompt);
                param[index++] = new SqlParameter("@ISPM15CountryPalletChecking", oAPPBOK_BookingBE.ISPM15CountryPalletChecking);
                param[index++] = new SqlParameter("@ISPM15FromCountryID", oAPPBOK_BookingBE.ISPM15FromCountryID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        StringBuilder sbMessage = new StringBuilder();
        public int? AddCarrierBookingDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.VendorID);
                param[index++] = new SqlParameter("@NumberOfPallet", oAPPBOK_BookingBE.NumberOfPallet);
                param[index++] = new SqlParameter("@NumberOfCartons", oAPPBOK_BookingBE.NumberOfCartons);
                param[index++] = new SqlParameter("@NumberOfLift", oAPPBOK_BookingBE.NumberOfLift);
                param[index++] = new SqlParameter("@NumberOfLine", oAPPBOK_BookingBE.NumberOfLines);
                param[index++] = new SqlParameter("@PurchaseOrdersIds", oAPPBOK_BookingBE.PurchaseOrdersIDs);
                param[index++] = new SqlParameter("@PurchaseOrders", oAPPBOK_BookingBE.PurchaseOrders);
                //param[index++] = new SqlParameter("@ISPM15CountryPalletChecking", oAPPBOK_BookingBE.ISPM15CountryPalletChecking);
                //param[index++] = new SqlParameter("@ISPM15FromCountryID", oAPPBOK_BookingBE.ISPM15FromCountryID);
                sbMessage.Append("DAL SP name           : spMASSIT_Booking");
                sbMessage.Append("\r\n");
                for (int i = 0; i <= param.Length - 1; i++)
                {
                    sbMessage.Append("DAL Param Values      : " + i.ToString() + ") " + param[i].ParameterName + " - " + Convert.ToString(param[i].Value));
                    sbMessage.Append("\r\n");
                }


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetBookingDetailsByIDDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                    oNewAPPBOK_BookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.BookingRef = dr["BookingRef"].ToString();
                    oNewAPPBOK_BookingBE.BookingTypeID = dr["BookingTypeID"] != DBNull.Value ? Convert.ToInt32(dr["BookingTypeID"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.ScheduleDate = Convert.ToDateTime(dr["ScheduleDate"]);
                    oNewAPPBOK_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["SlotTime"].ToString();
                    oNewAPPBOK_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfLift = dr["NumberOfLift"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLift"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfLines = dr["NumberOfLine"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLine"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.SupplierType = dr["SupplierType"] != DBNull.Value ? dr["SupplierType"].ToString() : "";
                    oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    oNewAPPBOK_BookingBE.Carrier.CarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.Carrier.CarrierName = dr["CarrierName"] != DBNull.Value ? dr["CarrierName"].ToString() : "-";
                    oNewAPPBOK_BookingBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.VehicleType = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE();
                    oNewAPPBOK_BookingBE.VehicleType.VehicleType = dr["VehicleType"] != DBNull.Value ? dr["VehicleType"].ToString() : "-";
                    oNewAPPBOK_BookingBE.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : "-";
                    oNewAPPBOK_BookingBE.PreAdviseNotification = dr["PreAdviseNotification"] != DBNull.Value ? dr["PreAdviseNotification"].ToString() : "N";
                    oNewAPPBOK_BookingBE.WeekDay = dr["WeekDay"] != DBNull.Value ? Convert.ToInt32(dr["WeekDay"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.SiteId = dr["SiteId"] != DBNull.Value ? Convert.ToInt32(dr["SiteId"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.TimeWindow = new MASSIT_TimeWindowBE();
                    oNewAPPBOK_BookingBE.TimeWindow.TimeWindowID = dr["TimeWindowID"] != DBNull.Value ? Convert.ToInt32(dr["TimeWindowID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.PurchaseOrders = dr["PurchaseOrders"] != DBNull.Value ? dr["PurchaseOrders"].ToString() : "-";
                    oNewAPPBOK_BookingBE.Vendor = new UP_VendorBE();
                    oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : "-";
                    //============== Stage 6 Point 8 ===========//
                    if (ds.Tables[0].Columns.Contains("IsBookingAmended"))
                    {
                        oNewAPPBOK_BookingBE.IsBookingAmended = dr["IsBookingAmended"] != DBNull.Value ? Convert.ToBoolean(dr["IsBookingAmended"]) : false;
                        oNewAPPBOK_BookingBE.SlotTime.SlotTimeID = dr["SlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["SlotTimeID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.FixedSlot = new MASSIT_FixedSlotBE();
                        oNewAPPBOK_BookingBE.FixedSlot.FixedSlotID = dr["FixedSlotID"] != DBNull.Value ? Convert.ToInt32(dr["FixedSlotID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.DoorNoSetup = new MASSIT_DoorNoSetupBE();
                        oNewAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = dr["SiteDoorNumberID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorNumberID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
                        oNewAPPBOK_BookingBE.Delivery.DeliveryTypeID = dr["DeliveryTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DeliveryTypeID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.VehicleType.VehicleTypeID = dr["VehicleTypeID"] != DBNull.Value ? Convert.ToInt32(dr["VehicleTypeID"].ToString()) : 0;
                    }
                    //=========================================//
                    //============== Stage 18 Point 3 ===========//
                    if (ds.Tables[0].Columns.Contains("BookingComments"))
                    {
                        oNewAPPBOK_BookingBE.BookingComments = dr["BookingComments"] != DBNull.Value ? dr["BookingComments"].ToString() : string.Empty;
                    }
                    //=========================================//
                    if (ds.Tables[0].Columns.Contains("PO"))
                    {
                        oNewAPPBOK_BookingBE.PO = dr["PO"] != DBNull.Value ? dr["PO"].ToString() : string.Empty;
                    }
                    if (ds.Tables[0].Columns.Contains("IsVenodrPalletChecking"))
                        oNewAPPBOK_BookingBE.IsVenodrPalletChecking = dr["IsVenodrPalletChecking"] != DBNull.Value ? Convert.ToBoolean(dr["IsVenodrPalletChecking"]) : false;
                    if (ds.Tables[0].Columns.Contains("IsStandardPalletChecking"))
                        oNewAPPBOK_BookingBE.IsStandardPalletChecking = dr["IsStandardPalletChecking"] != DBNull.Value ? Convert.ToBoolean(dr["IsStandardPalletChecking"]) : false;


                    if (ds.Tables[0].Columns.Contains("IsEnableHazardouesItemPrompt"))
                        oNewAPPBOK_BookingBE.IsEnableHazardouesItemPrompt = dr["IsEnableHazardouesItemPrompt"] != DBNull.Value ? Convert.ToBoolean(dr["IsEnableHazardouesItemPrompt"]) : false;

                    if (ds.Tables[0].Columns.Contains("IsISPM15Applied"))
                        oNewAPPBOK_BookingBE.ISPM15CountryPalletChecking = dr["IsISPM15Applied"] != DBNull.Value ? Convert.ToBoolean(dr["IsISPM15Applied"]) : false;

                    if (ds.Tables[0].Columns.Contains("ISPM15FromCountryID"))
                        oNewAPPBOK_BookingBE.ISPM15FromCountryID = dr["ISPM15FromCountryID"] != DBNull.Value ? Convert.ToInt32(dr["ISPM15FromCountryID"]) : 0;

                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }


        public List<APPBOK_BookingBE> GetAllBookingFixedSlotDoorDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                //param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.VendorID);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@SelectedScheduleDate", oAPPBOK_BookingBE.SelectedScheduleDate);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    oNewAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(dr["CarrierID"].ToString());
                    oNewAPPBOK_BookingBE.VendorID = Convert.ToInt32(dr["VendorID"].ToString());
                    oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"].ToString();
                    oNewAPPBOK_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["SlotTime"].ToString();
                    oNewAPPBOK_BookingBE.SlotTime.SlotTimeID = dr["SlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["SlotTimeID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.BookingRef = dr["BookingReferenceID"].ToString();
                    oNewAPPBOK_BookingBE.DoorNoSetup = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_DoorNoSetupBE();
                    oNewAPPBOK_BookingBE.DoorNoSetup.DoorNumber = dr["DoorNumber"] != DBNull.Value ? Convert.ToString(dr["DoorNumber"].ToString()) : string.Empty;
                    oNewAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = dr["SiteDoorNumberID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorNumberID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.SlotType = dr["SlotType"].ToString();
                    oNewAPPBOK_BookingBE.Status = dr["Status"].ToString();
                    oNewAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
                    oNewAPPBOK_BookingBE.FixedSlot.FixedSlotID = dr["FixedSlotID"] != DBNull.Value ? Convert.ToInt32(dr["FixedSlotID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfLift = dr["NumberOfLift"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLift"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfLines = dr["NumberOfLine"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLine"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.SupplierType = dr["SupplierType"].ToString();
                    oNewAPPBOK_BookingBE.VehicleType = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE();
                    oNewAPPBOK_BookingBE.VehicleType.VehicleTypeID = dr["VehicleTypeID"] != DBNull.Value ? Convert.ToInt32(dr["VehicleTypeID"].ToString()) : -1;
                    if (ds.Tables[0].Columns.Contains("OrderBY"))
                        oNewAPPBOK_BookingBE.SlotTime.OrderBYID = dr["OrderBY"] != DBNull.Value ? Convert.ToInt32(dr["OrderBY"].ToString()) : -1;
                    if (ds.Tables[0].Columns.Contains("AllocationType"))
                        oNewAPPBOK_BookingBE.FixedSlot.AllocationType = dr["AllocationType"] != DBNull.Value ? dr["AllocationType"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("BookingID"))
                        oNewAPPBOK_BookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"]) : 0;

                    if (ds.Tables[0].Columns.Contains("BookingStatus"))
                        oNewAPPBOK_BookingBE.BookingStatus = dr["BookingStatus"] != DBNull.Value ? Convert.ToString(dr["BookingStatus"]) : string.Empty;

                    if (ds.Tables[0].Columns.Contains("BookingStatusID"))
                        oNewAPPBOK_BookingBE.BookingStatusID = dr["BookingStatusID"] != DBNull.Value ? Convert.ToInt32(dr["BookingStatusID"]) : 0;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }


            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetBookedVolumeDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@BookingType", oAPPBOK_BookingBE.BookingType);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                    if (dt.Columns.Contains("NumberOfDeliveries"))
                        oNewAPPBOK_BookingBE.NumberOfDeliveries = dr["NumberOfDeliveries"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfDeliveries"].ToString()) : 0;

                    oNewAPPBOK_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NumberOfLift = dr["NumberOfLift"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLift"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NumberOfLines = dr["NumberOfLine"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLine"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"].ToString()) : 0;
                    if (dt.Columns.Contains("NumberOfContainer"))
                        oNewAPPBOK_BookingBE.NumberOfContainer = dr["NumberOfContainer"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfContainer"].ToString()) : 0;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }


        public List<APPBOK_BookingBE> GetBookingVolumeDeatilsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@WeekDay", oAPPBOK_BookingBE.WeekDay);
                param[index++] = new SqlParameter("@StartTime", oAPPBOK_BookingBE.StartTime);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.BookingCount = dr["BookingCount"] != DBNull.Value ? Convert.ToInt32(dr["BookingCount"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NumberOfLift = dr["NumberOfLift"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLift"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NumberOfLines = dr["NumberOfLine"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLine"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"].ToString()) : 0;

                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public DataTable GetSchedulingPrintOutDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@SiteId", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.UserID);
                param[index++] = new SqlParameter("@StockPlannerID", oAPPBOK_BookingBE.StockPlannerID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "rptSchedulingPrintOut", param);

                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }


        public List<APPBOK_BookingBE> GetNonTimeBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"].ToString();
                    oNewAPPBOK_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfLift = dr["NumberOfLift"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLift"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfLines = dr["NumberOfLine"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLine"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"].ToString()) : (int?)null;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }


            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetVendorBookingDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                    oNewAPPBOK_BookingBE.BookingRef = dr["BookingReferenceID"].ToString();

                    oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewAPPBOK_BookingBE.Vendor.Vendor_No = dr["Vendor_No"].ToString();
                    oNewAPPBOK_BookingBE.Vendor.VendorID = Convert.ToInt32(dr["VendorID"]);
                    oNewAPPBOK_BookingBE.Vendor.VendorName = dr["Vendor_Name"].ToString();
                    oNewAPPBOK_BookingBE.Vendor.ParentVendorID = dr["ParentVendorID"] != DBNull.Value ? Convert.ToInt32(dr["ParentVendorID"]) : -1;

                    oNewAPPBOK_BookingBE.BookingTypeID = dr["BookingTypeID"] != DBNull.Value ? Convert.ToInt32(dr["BookingTypeID"]) : (int?)null;
                    oNewAPPBOK_BookingBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : (int?)null;

                    oNewAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
                    oNewAPPBOK_BookingBE.Delivery.DeliveryTypeID = Convert.ToInt32(dr["DeliveryTypeID"]);

                    oNewAPPBOK_BookingBE.ScheduleDate = Convert.ToDateTime(dr["ScheduleDate"]);

                    oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    oNewAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(dr["CarrierID"]);

                    oNewAPPBOK_BookingBE.VehicleType = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE();
                    oNewAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(dr["VehicleTypeID"]);

                    oNewAPPBOK_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NumberOfLift = dr["NumberOfLift"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLift"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NumberOfLines = dr["NumberOfLine"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLine"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"].ToString()) : 0;


                    oNewAPPBOK_BookingBE.OldNumberOfCartons = dr["OldNumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["OldNumberOfCartons"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.OldNumberOfLift = dr["OldNumberOfLift"] != DBNull.Value ? Convert.ToInt32(dr["OldNumberOfLift"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.OldNumberOfLines = dr["OldNumberOfLine"] != DBNull.Value ? Convert.ToInt32(dr["OldNumberOfLine"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.OldNumberOfPallet = dr["OldNumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["OldNumberOfPallet"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.ProvisionalReason = dr["ProvisionalReason"] != DBNull.Value ? dr["ProvisionalReason"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["SlotTime"] != DBNull.Value ? dr["SlotTime"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.SlotTime.SlotTimeID = dr["SlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["SlotTimeID"].ToString()) : 0;

                    oNewAPPBOK_BookingBE.DoorNoSetup = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_DoorNoSetupBE();
                    oNewAPPBOK_BookingBE.DoorNoSetup.DoorNumber = dr["DoorNumber"] != DBNull.Value ? Convert.ToString(dr["DoorNumber"].ToString()) : string.Empty;
                    oNewAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = dr["SiteDoorNumberID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorNumberID"].ToString()) : 0;

                    oNewAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
                    oNewAPPBOK_BookingBE.FixedSlot.FixedSlotID = dr["FixedSlotID"] != DBNull.Value ? Convert.ToInt32(dr["FixedSlotID"].ToString()) : -1;

                    oNewAPPBOK_BookingBE.WeekDay = Convert.ToInt32(dr["WeekDay"].ToString());
                    oNewAPPBOK_BookingBE.BookingComments = Convert.ToString(dr["BookingComments"]);

                    oNewAPPBOK_BookingBE.TimeWindow = new MASSIT_TimeWindowBE();
                    oNewAPPBOK_BookingBE.TimeWindow.TimeWindowID = dr["TimeWindowID"] != DBNull.Value ? Convert.ToInt32(dr["TimeWindowID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.NonWindowFromTimeID = dr["NonWindowFromTimeID"] != DBNull.Value ? Convert.ToInt32(dr["NonWindowFromTimeID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NonWindowToTimeID = dr["NonWindowToTimeID"] != DBNull.Value ? Convert.ToInt32(dr["NonWindowToTimeID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.IsBookingAccepted = dr["IsBookingAccepted"] != DBNull.Value ? Convert.ToBoolean(dr["IsBookingAccepted"].ToString()) : false;
                    if (ds.Tables[0].Columns.Contains("OtherCarrier"))
                        oNewAPPBOK_BookingBE.OtherCarrier = dr["OtherCarrier"].ToString();

                    if (ds.Tables[0].Columns.Contains("OtherVehicle"))
                        oNewAPPBOK_BookingBE.OtherVehicle = dr["OtherVehicle"].ToString();
                    oNewAPPBOK_BookingBE.IsEnableHazardouesItemPrompt = dr["IsEnableHazardouesItemPrompt"] != DBNull.Value ? Convert.ToBoolean(dr["IsEnableHazardouesItemPrompt"].ToString()) : false;

                    oNewAPPBOK_BookingBE.ISPM15CountryPalletChecking = dr["IsISPM15Applied"] != DBNull.Value ? Convert.ToBoolean(dr["IsISPM15Applied"].ToString()) : false;
                    oNewAPPBOK_BookingBE.ISPM15FromCountryID = dr["ISPM15FromCountryID"] != DBNull.Value ? Convert.ToInt32(dr["ISPM15FromCountryID"]) : 0;

                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetCarrierBookingDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                    oNewAPPBOK_BookingBE.BookingRef = dr["BookingReferenceID"].ToString();

                    oNewAPPBOK_BookingBE.BookingTypeID = dr["BookingTypeID"] != DBNull.Value ? Convert.ToInt32(dr["BookingTypeID"]) : (int?)null;
                    oNewAPPBOK_BookingBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : (int?)null;

                    oNewAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
                    oNewAPPBOK_BookingBE.Delivery.DeliveryTypeID = Convert.ToInt32(dr["DeliveryTypeID"]);

                    oNewAPPBOK_BookingBE.ScheduleDate = Convert.ToDateTime(dr["ScheduleDate"]);

                    oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    oNewAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(dr["CarrierID"]);
                    oNewAPPBOK_BookingBE.Carrier.CarrierName = dr["CarrierName"].ToString();

                    oNewAPPBOK_BookingBE.VehicleType = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE();
                    oNewAPPBOK_BookingBE.VehicleType.VehicleTypeID = Convert.ToInt32(dr["VehicleTypeID"]);
                    oNewAPPBOK_BookingBE.BookingTypeID = dr["BookingTypeID"] != DBNull.Value ? Convert.ToInt32(dr["BookingTypeID"]) : (int?)null;
                    oNewAPPBOK_BookingBE.ProvisionalReason = dr["ProvisionalReason"] != DBNull.Value ? dr["ProvisionalReason"].ToString() : string.Empty;

                    oNewAPPBOK_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["SlotTime"] != DBNull.Value ? dr["SlotTime"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.SlotTime.SlotTimeID = dr["SlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["SlotTimeID"].ToString()) : 0;

                    oNewAPPBOK_BookingBE.DoorNoSetup = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_DoorNoSetupBE();
                    oNewAPPBOK_BookingBE.DoorNoSetup.DoorNumber = dr["DoorNumber"] != DBNull.Value ? Convert.ToString(dr["DoorNumber"].ToString()) : string.Empty;
                    oNewAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = dr["SiteDoorNumberID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorNumberID"].ToString()) : 0;

                    oNewAPPBOK_BookingBE.FixedSlot = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_FixedSlotBE();
                    oNewAPPBOK_BookingBE.FixedSlot.FixedSlotID = dr["FixedSlotID"] != DBNull.Value ? Convert.ToInt32(dr["FixedSlotID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.WeekDay = Convert.ToInt32(dr["WeekDay"].ToString());

                    oNewAPPBOK_BookingBE.TimeWindow = new MASSIT_TimeWindowBE();
                    oNewAPPBOK_BookingBE.TimeWindow.TimeWindowID = dr["TimeWindowID"] != DBNull.Value ? Convert.ToInt32(dr["TimeWindowID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.NonWindowFromTimeID = dr["NonWindowFromTimeID"] != DBNull.Value ? Convert.ToInt32(dr["NonWindowFromTimeID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NonWindowToTimeID = dr["NonWindowToTimeID"] != DBNull.Value ? Convert.ToInt32(dr["NonWindowToTimeID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.IsBookingAccepted = dr["IsBookingAccepted"] != DBNull.Value ? Convert.ToBoolean(dr["IsBookingAccepted"].ToString()) : false;
                    if (ds.Tables[0].Columns.Contains("OtherCarrier"))
                        oNewAPPBOK_BookingBE.OtherCarrier = dr["OtherCarrier"].ToString();

                    if (ds.Tables[0].Columns.Contains("OtherVehicle"))
                        oNewAPPBOK_BookingBE.OtherVehicle = dr["OtherVehicle"].ToString();

                    if (ds.Tables[0].Columns.Contains("BookingComments"))
                        oNewAPPBOK_BookingBE.BookingComments = dr["BookingComments"] != DBNull.Value ? Convert.ToString(dr["BookingComments"].ToString()) : string.Empty;
                    oNewAPPBOK_BookingBE.IsEnableHazardouesItemPrompt = dr["IsEnableHazardouesItemPrompt"] != DBNull.Value ? Convert.ToBoolean(dr["IsEnableHazardouesItemPrompt"].ToString()) : false;
                    oNewAPPBOK_BookingBE.ISPM15CountryPalletChecking = dr["IsISPM15Applied"] != DBNull.Value ? Convert.ToBoolean(dr["IsISPM15Applied"].ToString()) : false;
                    oNewAPPBOK_BookingBE.ISPM15FromCountryID = dr["ISPM15FromCountryID"] != DBNull.Value ? Convert.ToInt32(dr["ISPM15FromCountryID"]) : 0;

                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetCarrierVolumeDeatilsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                    oNewAPPBOK_BookingBE.VendorID = Convert.ToInt32(dr["VendorID"]);

                    oNewAPPBOK_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NumberOfLines = dr["NumberOfLine"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLine"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"].ToString()) : 0;


                    oNewAPPBOK_BookingBE.OldNumberOfCartons = dr["OldNumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["OldNumberOfCartons"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.OldNumberOfLines = dr["OldNumberOfLine"] != DBNull.Value ? Convert.ToInt32(dr["OldNumberOfLine"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.OldNumberOfPallet = dr["OldNumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["OldNumberOfPallet"].ToString()) : 0;

                    if (ds.Tables[0].Columns.Contains("VendorName"))
                    {
                        oNewAPPBOK_BookingBE.Vendor = new UP_VendorBE();
                        oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? (dr["VendorName"].ToString()) : string.Empty;
                    }

                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }


        public int? CheckBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@SlotTimeID", oAPPBOK_BookingBE.SlotTime.SlotTimeID);
                param[index++] = new SqlParameter("@SiteDoorNumberID", oAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


        public int? CheckVendorBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.VendorID);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                if (oAPPBOK_BookingBE.Carrier != null)
                    param[index++] = new SqlParameter("@CarrierID", oAPPBOK_BookingBE.Carrier.CarrierID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        // Sprint 1 - Point 1 - Start - Checking PO is existing or not.
        public List<APPBOK_BookingBE> IsExistingPO(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrders", oAPPBOK_BookingBE.PurchaseOrders);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.VendorID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE appbok_BookingBE = new APPBOK_BookingBE();
                    appbok_BookingBE.SlotTime = new SYS_SlotTimeBE();
                    appbok_BookingBE.ScheduleDate = dr["ScheduleDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduleDate"]) : DateTime.Now.Date;
                    appbok_BookingBE.SlotTime.SlotTime = dr["SlotTime"] != DBNull.Value ? Convert.ToString(dr["SlotTime"]) : string.Empty;
                    lstAPPBOK_BookingBE.Add(appbok_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }
        // Sprint 1 - Point 1 - End

        // Sprint 1 - Point 9 - Begin
        public List<APPBOK_BookingBE> GetEmailIdsForStockPlannerDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();

            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                        oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNewAPPBOK_BookingBE.Vendor.VendorContactEmail = dr["StockPlannerEmailId"] != DBNull.Value ? dr["StockPlannerEmailId"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.Language = dr["Language"] != DBNull.Value ? dr["Language"].ToString() : string.Empty;
                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetEmailDataForRefusedDeliveriesDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();

            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    bool blnReasonStatus = false;
                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                        // Site Name
                        oNewAPPBOK_BookingBE.SiteName = dr["Site"] != DBNull.Value ? dr["Site"].ToString() : string.Empty;
                        // Booking Reference
                        oNewAPPBOK_BookingBE.BookingRef = dr["BookingReference"] != DBNull.Value ? dr["BookingReference"].ToString() : string.Empty;
                        // Vendor Name
                        oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNewAPPBOK_BookingBE.Vendor.VendorName = dr["Vendor"] != DBNull.Value ? dr["Vendor"].ToString() : string.Empty;
                        // Carrier Name
                        oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                        oNewAPPBOK_BookingBE.Carrier.CarrierName = dr["Carrier"] != DBNull.Value ? dr["Carrier"].ToString() : string.Empty;
                        // Booked In For [Slot Time]
                        oNewAPPBOK_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                        oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["BookedInFor"] != DBNull.Value ? dr["BookedInFor"].ToString() : string.Empty;
                        // Refused At [Status]
                        oNewAPPBOK_BookingBE.Status = dr["RefusedAt"] != DBNull.Value ? dr["RefusedAt"].ToString() : string.Empty;
                        // Authorised By [VendorContactEmail]
                        oNewAPPBOK_BookingBE.Vendor.VendorContactEmail = dr["AuthorisedBy"] != DBNull.Value ? dr["AuthorisedBy"].ToString() : string.Empty;
                        // Reason1ArrivedEarly
                        blnReasonStatus = false;
                        if (Convert.ToInt32(dr["Reason1ArrivedEarly"]) > 0) { blnReasonStatus = true; } else { blnReasonStatus = false; }
                        oNewAPPBOK_BookingBE.DLYREF_ReasonArrivedEarly = blnReasonStatus;
                        blnReasonStatus = false;
                        // Reason2ArrivedLate
                        if (Convert.ToInt32(dr["Reason2ArrivedLate"]) > 0) { blnReasonStatus = true; } else { blnReasonStatus = false; }
                        oNewAPPBOK_BookingBE.DLYREF_ReasonArrivedLate = blnReasonStatus;
                        blnReasonStatus = false;
                        // Reason3NoPaperworkOndisplay
                        if (Convert.ToInt32(dr["Reason3NoPaperworkOndisplay"]) > 0) { blnReasonStatus = true; } else { blnReasonStatus = false; }
                        oNewAPPBOK_BookingBE.DLYREF_ReasonNoPaperworkOndisplay = blnReasonStatus;
                        blnReasonStatus = false;
                        // Reason4PalletsDamaged
                        if (Convert.ToInt32(dr["Reason4PalletsDamaged"]) > 0) { blnReasonStatus = true; } else { blnReasonStatus = false; }
                        oNewAPPBOK_BookingBE.DLYREF_ReasonPalletsDamaged = blnReasonStatus;
                        blnReasonStatus = false;
                        // Reason5PackagingDamaged
                        if (Convert.ToInt32(dr["Reason5PackagingDamaged"]) > 0) { blnReasonStatus = true; } else { blnReasonStatus = false; }
                        oNewAPPBOK_BookingBE.DLYREF_ReasonPackagingDamaged = blnReasonStatus;
                        blnReasonStatus = false;
                        // Reason6UnsafeLoad
                        if (Convert.ToInt32(dr["Reason6UnsafeLoad"]) > 0) { blnReasonStatus = true; } else { blnReasonStatus = false; }
                        oNewAPPBOK_BookingBE.DLYREF_ReasonUnsafeLoad = blnReasonStatus;
                        blnReasonStatus = false;
                        // Reason7WrongAddress
                        if (Convert.ToInt32(dr["Reason7WrongAddress"]) > 0) { blnReasonStatus = true; } else { blnReasonStatus = false; }
                        oNewAPPBOK_BookingBE.DLYREF_ReasonWrongAddress = blnReasonStatus;
                        blnReasonStatus = false;
                        // Reason8RefusedTowait
                        if (Convert.ToInt32(dr["Reason8RefusedTowait"]) > 0) { blnReasonStatus = true; } else { blnReasonStatus = false; }
                        oNewAPPBOK_BookingBE.DLYREF_ReasonRefusedTowait = blnReasonStatus;
                        blnReasonStatus = false;
                        // Reason9NottoODSpecification
                        if (Convert.ToInt32(dr["Reason9NottoODSpecification"]) > 0) { blnReasonStatus = true; } else { blnReasonStatus = false; }
                        oNewAPPBOK_BookingBE.DLYREF_ReasonNottoODSpecification = blnReasonStatus;
                        blnReasonStatus = false;
                        // Reason10Other
                        if (Convert.ToInt32(dr["Reason10Other"]) > 0) { blnReasonStatus = true; } else { blnReasonStatus = false; }
                        oNewAPPBOK_BookingBE.DLYREF_ReasonOther = blnReasonStatus;
                        blnReasonStatus = false;
                        // Comment
                        oNewAPPBOK_BookingBE.DLYREF_ReasonComments = dr["Comment"] != DBNull.Value ? dr["Comment"].ToString() : string.Empty;

                        string strPurchaseOrderNo = dr["PurchaseOrder"] != DBNull.Value ? dr["PurchaseOrder"].ToString() : string.Empty;
                        if (!string.IsNullOrWhiteSpace(strPurchaseOrderNo))
                        {
                            // PurchaseOrder
                            oNewAPPBOK_BookingBE.PurchaseOrders = dr["PurchaseOrder"] != DBNull.Value ? dr["PurchaseOrder"].ToString() : string.Empty;
                            // PurchaseOrderVendor
                            oNewAPPBOK_BookingBE.Prioirty = dr["PurchaseOrderVendor"] != DBNull.Value ? dr["PurchaseOrderVendor"].ToString() : string.Empty;
                        }
                        else { oNewAPPBOK_BookingBE.PurchaseOrders = oNewAPPBOK_BookingBE.Prioirty = string.Empty; }

                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }
        // Sprint 1 - Point 9 - End

        public int? UpdateBookingStatusByIdDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.UserID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetNoShowDeliveryLetterDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();

            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                        oNewAPPBOK_BookingBE.SiteAddress = dr["SiteAddress"] != DBNull.Value ? dr["SiteAddress"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.DeliveryDate = dr["CurrentDate"] != DBNull.Value ? Convert.ToDateTime(dr["CurrentDate"]) : (DateTime?)null;
                        oNewAPPBOK_BookingBE.BookingRef = dr["BookingReferenceID"] != DBNull.Value ? dr["BookingReferenceID"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.ScheduleDate = dr["ScheduleDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduleDate"]) : (DateTime?)null;
                        oNewAPPBOK_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                        oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["ScheduleTime"] != DBNull.Value ? dr["ScheduleTime"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNewAPPBOK_BookingBE.Vendor.VendorContactEmail = dr["EmailId"] != DBNull.Value ? dr["EmailId"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                        oNewAPPBOK_BookingBE.Vendor.Vendor_No = dr["VendorNo"] != DBNull.Value ? dr["VendorNo"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.Vendor.address1 = dr["Address1"] != DBNull.Value ? dr["Address1"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.Vendor.address2 = dr["Address2"] != DBNull.Value ? dr["Address2"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.Vendor.city = dr["City"] != DBNull.Value ? dr["City"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.LanguageID = dr["LanguageID"] != DBNull.Value ? Convert.ToInt32(dr["LanguageID"]) : 0;
                        oNewAPPBOK_BookingBE.Language = dr["Language"] != DBNull.Value ? dr["Language"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.WeekDay = dr["WeekDay"] != DBNull.Value ? Convert.ToInt32(dr["WeekDay"]) : 0;
                        oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                        if (dr.Table.Columns.Contains("CarrierName"))
                        {
                            oNewAPPBOK_BookingBE.Carrier.CarrierName = dr["CarrierName"] != DBNull.Value ? dr["CarrierName"].ToString() : string.Empty;
                        }
                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetNoShowSPUsersDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();

            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                        oNewAPPBOK_BookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.BookingRef = dr["BookingReferenceID"] != DBNull.Value ? dr["BookingReferenceID"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNewAPPBOK_BookingBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                        oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.StockPlannerID = dr["StockPlannerID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.StockPlannerEmail = dr["EmailId"] != DBNull.Value ? dr["EmailId"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.Language = dr["Language"] != DBNull.Value ? dr["Language"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.NumberOfLift = dr["Lift"] != DBNull.Value ? Convert.ToInt32(dr["Lift"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.NumberOfLines = dr["Line"] != DBNull.Value ? Convert.ToInt32(dr["Line"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.NumberOfPallet = dr["Pallet"] != DBNull.Value ? Convert.ToInt32(dr["Pallet"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.NumberOfCartons = dr["Cartons"] != DBNull.Value ? Convert.ToInt32(dr["Cartons"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                        oNewAPPBOK_BookingBE.Carrier.CarrierID = Convert.ToInt32(dr["CarrierID"]);
                        oNewAPPBOK_BookingBE.Carrier.CarrierName = dr["CarrierName"] != DBNull.Value ? dr["CarrierName"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.ScheduleDate = dr["BookingDate"] != DBNull.Value ? Convert.ToDateTime(dr["BookingDate"]) : (DateTime?)null;
                        oNewAPPBOK_BookingBE.HistoryDate = dr["HistoryDate"] != DBNull.Value ? dr["HistoryDate"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.UserName = dr["SetBy"] != DBNull.Value ? dr["SetBy"].ToString() : string.Empty;

                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetPurchaseOrderForBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();

            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                        oNewAPPBOK_BookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.BookingRef = dr["BookingReferenceID"] != DBNull.Value ? dr["BookingReferenceID"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.PurchaseOrders = dr["Purchase_order"] != DBNull.Value ? dr["Purchase_order"].ToString() : "-";
                        oNewAPPBOK_BookingBE.Vendor = new UP_VendorBE();
                        oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : "-";

                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }



        public List<APPBOK_BookingBE> GetMultiVendorBookingDataDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@MultiBookingIDs", oAPPBOK_BookingBE.MultiBookingIDs);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                    oNewAPPBOK_BookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.ScheduleDate = Convert.ToDateTime(dr["ScheduleDate"]);
                    oNewAPPBOK_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["SlotTime"].ToString();
                    oNewAPPBOK_BookingBE.BookingRef = dr["BookingReferenceID"].ToString();
                    oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.NumberOfLift = dr["Lift"] != DBNull.Value ? Convert.ToInt32(dr["Lift"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfLines = dr["Line"] != DBNull.Value ? Convert.ToInt32(dr["Line"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfPallet = dr["Pallet"] != DBNull.Value ? Convert.ToInt32(dr["Pallet"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfCartons = dr["Cartons"] != DBNull.Value ? Convert.ToInt32(dr["Cartons"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.WeekDay = dr["WeekDay"] != DBNull.Value ? Convert.ToInt32(dr["WeekDay"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.BookingTypeID = dr["BookingTypeID"] != DBNull.Value ? Convert.ToInt32(dr["BookingTypeID"].ToString()) : (int?)null;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        //Stage 4 Point 2--
        public DataTable GetBookingInqueryDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@BookingRef", oAPPBOK_BookingBE.BookingRef);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.FixedSlot.SiteID);
                param[index++] = new SqlParameter("@SiteIDs", oAPPBOK_BookingBE.FixedSlot.SiteIDs);
                param[index++] = new SqlParameter("@FromDate", oAPPBOK_BookingBE.FromDate);
                param[index++] = new SqlParameter("@ToDate", oAPPBOK_BookingBE.ToDate);
                param[index++] = new SqlParameter("@CarrierID", oAPPBOK_BookingBE.Carrier.CarrierID);
                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.VendorID);
                param[index++] = new SqlParameter("@VendorIDs", oAPPBOK_BookingBE.VendorIDs);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spBookingInquery", param);

                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        //------------------------//


        //Stage 8 Point 13--
        /*
        public DataTable GetOverdueBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@DisplayType", oAPPBOK_BookingBE.DisplayType);
                param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oAPPBOK_BookingBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oAPPBOK_BookingBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedSiteIds", oAPPBOK_BookingBE.SelectedSiteIds);
                param[index++] = new SqlParameter("@FromDate", oAPPBOK_BookingBE.FromDate);
                param[index++] = new SqlParameter("@ToDate", oAPPBOK_BookingBE.ToDate);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOverdueBooking", param);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }*/

        public List<APPBOK_BookingBE> GetOverdueBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@DisplayType", oAPPBOK_BookingBE.DisplayType);
                param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oAPPBOK_BookingBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oAPPBOK_BookingBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedSiteIds", oAPPBOK_BookingBE.SelectedSiteIds);
                param[index++] = new SqlParameter("@FromDate", oAPPBOK_BookingBE.FromDate);
                param[index++] = new SqlParameter("@ToDate", oAPPBOK_BookingBE.ToDate);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOverdueBooking", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                    oNewAPPBOK_BookingBE.PKID = dr["PKID"] != DBNull.Value ? dr["PKID"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.BookingRef = dr["BookingRef"] != DBNull.Value ? dr["BookingRef"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : string.Empty;

                    oNewAPPBOK_BookingBE.TimeWindow = new MASSIT_TimeWindowBE();
                    oNewAPPBOK_BookingBE.TimeWindow.TimeWindowID = dr["TimeWindowID"] != DBNull.Value ? Convert.ToInt32(dr["TimeWindowID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.NonWindowFromToTime = dr["NonWindowFromToTime"] != DBNull.Value ? dr["NonWindowFromToTime"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.WindowStartEndTime = dr["WindowStartEndTime"] != DBNull.Value ? dr["WindowStartEndTime"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.ExpectedDeliveryTime = dr["ExpectedDeliveryTime"] != DBNull.Value ? dr["ExpectedDeliveryTime"].ToString() : string.Empty;

                    oNewAPPBOK_BookingBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oNewAPPBOK_BookingBE.PurchaseOrder.Purchase_order = dr["PurchaseOrder"] != DBNull.Value ? dr["PurchaseOrder"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.Vendor = new UP_VendorBE();
                    oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.DeliveryDate = dr["DeliveryDate"] != DBNull.Value ? Convert.ToDateTime(dr["DeliveryDate"]) : (DateTime?)null;
                    oNewAPPBOK_BookingBE.BookingDat = dr["BookingDate"] != DBNull.Value ? Convert.ToDateTime(dr["BookingDate"]) : (DateTime?)null;
                    oNewAPPBOK_BookingBE.OriginalDueDate = dr["OriginalDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["OriginalDueDate"]) : (DateTime?)null;

                    oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    oNewAPPBOK_BookingBE.Carrier.CarrierName = dr["CarrierName"] != DBNull.Value ? dr["CarrierName"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.Variance = dr["Variance"] != DBNull.Value ? Convert.ToInt32(dr["Variance"]) : 0;
                    oNewAPPBOK_BookingBE.ProvBookedBy = dr["BookedBy"] != DBNull.Value ? dr["BookedBy"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.ContactName = dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.ContactNumber = dr["ContactNumber"] != DBNull.Value ? Convert.ToString(dr["ContactNumber"]) : string.Empty;
                    oNewAPPBOK_BookingBE.IsReviewed = dr["IsReviewed"] != DBNull.Value ? Convert.ToBoolean(dr["IsReviewed"].ToString()) : (bool?)null;
                    oNewAPPBOK_BookingBE.DueDateFull = dr["DueDateFull"] != DBNull.Value ? Convert.ToString(dr["DueDateFull"]) : string.Empty;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        //------------------------//

        public List<APPBOK_BookingBE> GetWindowVolumeDataDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.TimeWindow = new MASSIT_TimeWindowBE();
                    oNewAPPBOK_BookingBE.TimeWindow.TimeWindowID = dr["TimeWindowID"] != DBNull.Value ? Convert.ToInt32(dr["TimeWindowID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.TimeWindow.WindowName = dr["WindowName"] != DBNull.Value ? dr["WindowName"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.TimeWindow.StartTime = dr["StartTime"] != DBNull.Value ? dr["StartTime"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.TimeWindow.EndTime = dr["EndTime"] != DBNull.Value ? dr["EndTime"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.TimeWindow.DoorNumber = dr["DoorNumber"] != DBNull.Value ? dr["DoorNumber"].ToString() : string.Empty;
                    oNewAPPBOK_BookingBE.TimeWindow.Priority = dr["Priority"] != DBNull.Value ? Convert.ToInt32(dr["Priority"]) : 0;
                    oNewAPPBOK_BookingBE.TimeWindow.MaximumPallets = dr["MaxPallets"] != DBNull.Value ? Convert.ToInt32(dr["MaxPallets"]) : 0;
                    oNewAPPBOK_BookingBE.TimeWindow.MaximumCartons = dr["MaxCartons"] != DBNull.Value ? Convert.ToInt32(dr["MaxCartons"]) : 0;
                    oNewAPPBOK_BookingBE.TimeWindow.MaximumLines = dr["MaxLines"] != DBNull.Value ? Convert.ToInt32(dr["MaxLines"]) : 0;
                    oNewAPPBOK_BookingBE.NumberOfPallet = dr["CurrentPallets"] != DBNull.Value ? Convert.ToInt32(dr["CurrentPallets"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfCartons = dr["CurrentCartons"] != DBNull.Value ? Convert.ToInt32(dr["CurrentCartons"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfLines = dr["CurrentLines"] != DBNull.Value ? Convert.ToInt32(dr["CurrentLines"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.TimeWindow.TotalHrsMinsAvailable = dr["TotalTime"] != DBNull.Value ? dr["TotalTime"].ToString() : string.Empty;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        //Stage 4 Point 5--
        public DataTable GetProvisionalBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.FixedSlot.SiteID);
                param[index++] = new SqlParameter("@SiteIDs", oAPPBOK_BookingBE.FixedSlot.SiteIDs);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        //------------------------//


        //Stage 4 Point 2--
        public DataTable GetExpediteBookingDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrders", oAPPBOK_BookingBE.PurchaseOrders);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }


        public int? addUnknownVendorDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@UnknownVendorName", oAPPBOK_BookingBE.FixedSlot.Vendor.VendorName);


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        //------------------------//

        public List<APPBOK_BookingBE> GetConfirmPurchaseOrderArrivalDataDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE bookingBE = new APPBOK_BookingBE();
                    bookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"]) : 0;
                    bookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    bookingBE.Carrier.CarrierName = dr["CarrierName"] != DBNull.Value ? Convert.ToString(dr["CarrierName"]) : string.Empty;
                    bookingBE.ScheduleDate = dr["ScheduleDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduleDate"]) : (DateTime?)null;
                    bookingBE.VehicleType = new MASSIT_VehicleTypeBE();
                    bookingBE.VehicleType.VehicleType = dr["VehicleType"] != DBNull.Value ? Convert.ToString(dr["VehicleType"]) : string.Empty;
                    bookingBE.BookingStatus = dr["BookingStatus"] != DBNull.Value ? Convert.ToString(dr["BookingStatus"]) : string.Empty;
                    bookingBE.NumberOfPallet = dr["Pallet"] != DBNull.Value ? Convert.ToInt32(dr["Pallet"]) : 0;
                    bookingBE.NumberOfCartons = dr["Cartons"] != DBNull.Value ? Convert.ToInt32(dr["Cartons"]) : 0;
                    bookingBE.NumberOfLines = dr["Line"] != DBNull.Value ? Convert.ToInt32(dr["Line"]) : 0;
                    bookingBE.NumberOfLift = dr["Lift"] != DBNull.Value ? Convert.ToInt32(dr["Lift"]) : 0;
                    bookingBE.PurchaseOrders = dr["PO"] != DBNull.Value ? Convert.ToString(dr["PO"]) : string.Empty;
                    bookingBE.Vendor = new UP_VendorBE();
                    bookingBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                    bookingBE.Vendor.Vendor_No = dr["VendorNo"] != DBNull.Value ? Convert.ToString(dr["VendorNo"]) : string.Empty;
                    bookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                    bookingBE.ConfirmPOArrival = dr["ConfirmPOArrival"] != DBNull.Value ? Convert.ToString(dr["ConfirmPOArrival"]) : string.Empty;

                    if (dt.Columns.Contains("SiteId"))
                    {
                        bookingBE.SiteId = dr["SiteId"] != DBNull.Value ? Convert.ToInt32(dr["SiteId"]) : 0;
                    }
                    if (dt.Columns.Contains("BookingReferenceID"))
                    {
                        bookingBE.BookingRef = dr["BookingReferenceID"] != DBNull.Value ? Convert.ToString(dr["BookingReferenceID"]) : string.Empty;
                    }
                    if (dt.Columns.Contains("IsNotDelieveredEmailSent"))
                    {
                        bookingBE.IsNotDelieveredEmailSent = dr["IsNotDelieveredEmailSent"] != DBNull.Value ? Convert.ToBoolean(dr["IsNotDelieveredEmailSent"]) : false;
                    }
                    if (dt.Columns.Contains("PurchaseOrderID"))
                    {
                        bookingBE.PurchaseOrderID = dr["PurchaseOrderID"] != DBNull.Value ? Convert.ToInt32(dr["PurchaseOrderID"]) : 0;
                    }
                    oAPPBOK_BookingBEList.Add(bookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public int? UpdateConfirmPurchaseOrderArrivalDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@DeliveredPO", oAPPBOK_BookingBE.DeliveredPO);
                param[index++] = new SqlParameter("@DeliveredVendorIds", oAPPBOK_BookingBE.DeliveredVendorIds);
                param[index++] = new SqlParameter("@NotDeliveredPO", oAPPBOK_BookingBE.NotDeliveredPO);
                param[index++] = new SqlParameter("@NotDeliveredVendorIds", oAPPBOK_BookingBE.NotDeliveredVendorIds);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetDeletedBookingDetailsByIDDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                    oNewAPPBOK_BookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.BookingRef = dr["BookingRef"].ToString();
                    oNewAPPBOK_BookingBE.BookingTypeID = dr["BookingTypeID"] != DBNull.Value ? Convert.ToInt32(dr["BookingTypeID"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.ScheduleDate = Convert.ToDateTime(dr["ScheduleDate"]);
                    oNewAPPBOK_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["SlotTime"].ToString();
                    oNewAPPBOK_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfLift = dr["NumberOfLift"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLift"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfLines = dr["NumberOfLine"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLine"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.SupplierType = dr["SupplierType"] != DBNull.Value ? dr["SupplierType"].ToString() : "";
                    oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    oNewAPPBOK_BookingBE.Carrier.CarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.Carrier.CarrierName = dr["CarrierName"] != DBNull.Value ? dr["CarrierName"].ToString() : "-";
                    oNewAPPBOK_BookingBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : -1;
                    oNewAPPBOK_BookingBE.VehicleType = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE();
                    oNewAPPBOK_BookingBE.VehicleType.VehicleType = dr["VehicleType"] != DBNull.Value ? dr["VehicleType"].ToString() : "-";
                    oNewAPPBOK_BookingBE.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : "-";
                    oNewAPPBOK_BookingBE.PreAdviseNotification = dr["PreAdviseNotification"] != DBNull.Value ? dr["PreAdviseNotification"].ToString() : "N";
                    oNewAPPBOK_BookingBE.WeekDay = dr["WeekDay"] != DBNull.Value ? Convert.ToInt32(dr["WeekDay"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.SiteId = dr["SiteId"] != DBNull.Value ? Convert.ToInt32(dr["SiteId"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.TimeWindow = new MASSIT_TimeWindowBE();
                    oNewAPPBOK_BookingBE.TimeWindow.TimeWindowID = dr["TimeWindowID"] != DBNull.Value ? Convert.ToInt32(dr["TimeWindowID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.PurchaseOrders = dr["PurchaseOrders"] != DBNull.Value ? dr["PurchaseOrders"].ToString() : "-";
                    oNewAPPBOK_BookingBE.Vendor = new UP_VendorBE();
                    oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : "-";
                    //============== Stage 6 Point 8 ===========//
                    if (ds.Tables[0].Columns.Contains("IsBookingAmended"))
                    {
                        oNewAPPBOK_BookingBE.IsBookingAmended = dr["IsBookingAmended"] != DBNull.Value ? Convert.ToBoolean(dr["IsBookingAmended"]) : false;
                        oNewAPPBOK_BookingBE.SlotTime.SlotTimeID = dr["SlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["SlotTimeID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.FixedSlot = new MASSIT_FixedSlotBE();
                        oNewAPPBOK_BookingBE.FixedSlot.FixedSlotID = dr["FixedSlotID"] != DBNull.Value ? Convert.ToInt32(dr["FixedSlotID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.DoorNoSetup = new MASSIT_DoorNoSetupBE();
                        oNewAPPBOK_BookingBE.DoorNoSetup.SiteDoorNumberID = dr["SiteDoorNumberID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorNumberID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();
                        oNewAPPBOK_BookingBE.Delivery.DeliveryTypeID = dr["DeliveryTypeID"] != DBNull.Value ? Convert.ToInt32(dr["DeliveryTypeID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.VehicleType.VehicleTypeID = dr["VehicleTypeID"] != DBNull.Value ? Convert.ToInt32(dr["VehicleTypeID"].ToString()) : 0;
                    }
                    //=========================================//

                    oNewAPPBOK_BookingBE.BookingDate = Convert.ToString(dr["BookingDate"]);
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetProvisionalBookingReportViewByTypeDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryID", oAPPBOK_BookingBE.SelectedCountryID);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oAPPBOK_BookingBE.SelectedSiteIds);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oAPPBOK_BookingBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedCarrierIDs", oAPPBOK_BookingBE.SelectedCarrierIDs);
                param[index++] = new SqlParameter("@DateFrom", oAPPBOK_BookingBE.FromDate);
                param[index++] = new SqlParameter("@DateTo", oAPPBOK_BookingBE.ToDate);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPTBOK_ProvisionalBookingsReport", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.Vendor = new UP_VendorBE();
                    oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                    oNewAPPBOK_BookingBE.Accepted = dr["Accepted"] != DBNull.Value ? Convert.ToInt32(dr["Accepted"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.Rejected = dr["Rejected"] != DBNull.Value ? Convert.ToInt32(dr["Rejected"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.Total = dr["Total"] != DBNull.Value ? Convert.ToInt32(dr["Total"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.ProvNoSpace = dr["NoSpace"] != DBNull.Value ? Convert.ToInt32(dr["NoSpace"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.ProvWindowCapacity = dr["WindowCapacity"] != DBNull.Value ? Convert.ToInt32(dr["WindowCapacity"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.ProvFixedSlot = dr["FixedSlot"] != DBNull.Value ? Convert.ToInt32(dr["FixedSlot"].ToString()) : (int?)null;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetProvisionalBookingReportSummaryViewBySiteDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryID", oAPPBOK_BookingBE.SelectedCountryID);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oAPPBOK_BookingBE.SelectedSiteIds);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oAPPBOK_BookingBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedCarrierIDs", oAPPBOK_BookingBE.SelectedCarrierIDs);
                param[index++] = new SqlParameter("@DateFrom", oAPPBOK_BookingBE.FromDate);
                param[index++] = new SqlParameter("@DateTo", oAPPBOK_BookingBE.ToDate);
                param[index++] = new SqlParameter("@PageNo", oAPPBOK_BookingBE.PageNumber);
                param[index++] = new SqlParameter("@PageSize", oAPPBOK_BookingBE.PageSize);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPTBOK_ProvisionalBookingsReportViewBySite", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.Vendor = new UP_VendorBE();
                    oNewAPPBOK_BookingBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                    oNewAPPBOK_BookingBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.SiteName = dr["SiteName"] != DBNull.Value ? Convert.ToString(dr["SiteName"]) : string.Empty;
                    oNewAPPBOK_BookingBE.SiteCountryID = dr["SiteCountryID"] != DBNull.Value ? Convert.ToInt32(dr["SiteCountryID"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.Accepted = dr["Accepted"] != DBNull.Value ? Convert.ToInt32(dr["Accepted"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.Rejected = dr["Rejected"] != DBNull.Value ? Convert.ToInt32(dr["Rejected"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.KeyHeader = dr["KeyHeader"] != DBNull.Value ? Convert.ToString(dr["KeyHeader"]) : string.Empty;
                    oNewAPPBOK_BookingBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"].ToString()) : 0;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetProvisionalBookingReportDetailViewDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryID", oAPPBOK_BookingBE.SelectedCountryID);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oAPPBOK_BookingBE.SelectedSiteIds);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oAPPBOK_BookingBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedCarrierIDs", oAPPBOK_BookingBE.SelectedCarrierIDs);
                param[index++] = new SqlParameter("@DateFrom", oAPPBOK_BookingBE.FromDate);
                param[index++] = new SqlParameter("@DateTo", oAPPBOK_BookingBE.ToDate);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPTBOK_ProvisionalBookingsReport", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.Vendor = new UP_VendorBE();
                    oNewAPPBOK_BookingBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;

                    oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    oNewAPPBOK_BookingBE.Carrier.CarrierID = dr["CarrierId"] != DBNull.Value ? Convert.ToInt32(dr["CarrierId"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.Carrier.CarrierName = dr["CarrierName"] != DBNull.Value ? Convert.ToString(dr["CarrierName"]) : string.Empty;

                    oNewAPPBOK_BookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.BookingRef = dr["BookingReferenceID"] != DBNull.Value ? Convert.ToString(dr["BookingReferenceID"]) : string.Empty;

                    oNewAPPBOK_BookingBE.ProvBookedById = dr["BookedById"] != DBNull.Value ? Convert.ToInt32(dr["BookedById"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.ProvBookedBy = dr["BookedBy"] != DBNull.Value ? Convert.ToString(dr["BookedBy"]) : string.Empty;
                    oNewAPPBOK_BookingBE.ProvType = dr["Type"] != DBNull.Value ? Convert.ToString(dr["Type"]) : string.Empty;
                    oNewAPPBOK_BookingBE.ProvAction = dr["Action"] != DBNull.Value ? Convert.ToString(dr["Action"]) : string.Empty;
                    oNewAPPBOK_BookingBE.ProvODUserId = dr["ODUserId"] != DBNull.Value ? Convert.ToInt32(dr["ODUserId"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.ProvODUser = dr["ODUser"] != DBNull.Value ? Convert.ToString(dr["ODUser"]) : string.Empty;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetNoShowReportDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", oAPPBOK_BookingBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oAPPBOK_BookingBE.SelectedSiteIds);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oAPPBOK_BookingBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@DateFrom", oAPPBOK_BookingBE.FromDate);
                param[index++] = new SqlParameter("@DateTo", oAPPBOK_BookingBE.ToDate);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.SiteName = dr["SiteName"] != DBNull.Value ? Convert.ToString(dr["SiteName"]) : string.Empty;
                    oNewAPPBOK_BookingBE.Vendor = new UP_VendorBE();
                    oNewAPPBOK_BookingBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                    oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    oNewAPPBOK_BookingBE.Carrier.CarrierID = dr["CarrierId"] != DBNull.Value ? Convert.ToInt32(dr["CarrierId"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.Carrier.CarrierName = dr["CarrierName"] != DBNull.Value ? Convert.ToString(dr["CarrierName"]) : string.Empty;
                    oNewAPPBOK_BookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.BookingRef = dr["BookingReferenceID"] != DBNull.Value ? Convert.ToString(dr["BookingReferenceID"]) : string.Empty;
                    oNewAPPBOK_BookingBE.ScheduleDate = dr["ScheduleDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduleDate"]) : (DateTime?)null;
                    oNewAPPBOK_BookingBE.SlotTime = new SYS_SlotTimeBE();
                    oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["ScheduleTime"] != DBNull.Value ? Convert.ToString(dr["ScheduleTime"]) : string.Empty;
                    oNewAPPBOK_BookingBE.ProvBookedById = dr["BookedById"] != DBNull.Value ? Convert.ToInt32(dr["BookedById"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.ProvBookedBy = dr["BookedBy"] != DBNull.Value ? Convert.ToString(dr["BookedBy"]) : string.Empty;
                    oNewAPPBOK_BookingBE.NoShowByID = dr["NoShowByID"] != DBNull.Value ? Convert.ToInt32(dr["NoShowByID"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NoShowBy = dr["NoShowBy"] != DBNull.Value ? Convert.ToString(dr["NoShowBy"]) : string.Empty;
                    oNewAPPBOK_BookingBE.NoShowByUser = dr["NoShowByUser"] != DBNull.Value ? Convert.ToString(dr["NoShowByUser"]) : string.Empty;
                    oNewAPPBOK_BookingBE.NoShowDate = dr["NoShowDate"] != DBNull.Value ? Convert.ToDateTime(dr["NoShowDate"]) : (DateTime?)null;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetPOReconciliationDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@SelectedSiteIDs", oAPPBOK_BookingBE.SelectedSiteIds);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oAPPBOK_BookingBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@DateFrom", oAPPBOK_BookingBE.FromDate);
                param[index++] = new SqlParameter("@DateTo", oAPPBOK_BookingBE.ToDate);
                param[index++] = new SqlParameter("@Type", oAPPBOK_BookingBE.BookingType);
                param[index++] = new SqlParameter("@POStatus", oAPPBOK_BookingBE.PurchaseOrder.ChaseStatus);
                param[index++] = new SqlParameter("@Purchase_order", oAPPBOK_BookingBE.PurchaseOrder.Purchase_order);
                param[index++] = new SqlParameter("@VikingCode", oAPPBOK_BookingBE.PurchaseOrder.Direct_code);
                param[index++] = new SqlParameter("@ODCode", oAPPBOK_BookingBE.PurchaseOrder.OD_Code);
                param[index++] = new SqlParameter("@PurchaseOrder", oAPPBOK_BookingBE.PurchaseOrder.PurchaseOrderIds);
                param[index++] = new SqlParameter("@PageIndex", oAPPBOK_BookingBE.PageCount);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spPOReconciliation", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                    oNewAPPBOK_BookingBE.SiteName = dr["SiteName"] != DBNull.Value ? Convert.ToString(dr["SiteName"]) : string.Empty;
                    oNewAPPBOK_BookingBE.BookingRef = dr["BookingReferenceID"] != DBNull.Value ? Convert.ToString(dr["BookingReferenceID"]) : string.Empty;
                    oNewAPPBOK_BookingBE.Vendor = new UP_VendorBE();
                    oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                    oNewAPPBOK_BookingBE.ScheduleDate = dr["ScheduleDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduleDate"]) : (DateTime?)null;
                    oNewAPPBOK_BookingBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();
                    oNewAPPBOK_BookingBE.PurchaseOrder.Purchase_order = dr["Purchase_order"] != DBNull.Value ? dr["Purchase_order"].ToString() : "-";
                    oNewAPPBOK_BookingBE.PurchaseOrder.PurchaseOrderID = dr["PurchaseOrderID"] != DBNull.Value ? Convert.ToInt32(dr["PurchaseOrderID"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.DeliveryDate = dr["DateDelivered"] != DBNull.Value ? Convert.ToDateTime(dr["DateDelivered"]) : (DateTime?)null;
                    oNewAPPBOK_BookingBE.ScheduledNotBookedIn = dr["Scheduled"] != DBNull.Value ? Convert.ToString(dr["Scheduled"]) : string.Empty;
                    oNewAPPBOK_BookingBE.NotScheduledBookedIn = dr["NotScheduled"] != DBNull.Value ? Convert.ToString(dr["NotScheduled"]) : string.Empty;
                    oNewAPPBOK_BookingBE.BookingStatus = dr["BookingnSatusDescription"] != DBNull.Value ? Convert.ToString(dr["BookingnSatusDescription"]) : string.Empty;
                    oNewAPPBOK_BookingBE.DueDate = dr["DueDate"] != DBNull.Value ? Convert.ToDateTime(dr["DueDate"]) : (DateTime?)null;
                    oNewAPPBOK_BookingBE.PurchaseOrder.Status = dr["POStatus"] != DBNull.Value ? Convert.ToString(dr["POStatus"]) : string.Empty;
                    oNewAPPBOK_BookingBE.PurchaseOrder.ChaseStatus = dr["ChaseStatus"] != DBNull.Value ? Convert.ToString(dr["ChaseStatus"]) : "0";
                    oNewAPPBOK_BookingBE.PurchaseOrder.Order_raised = dr["Order_raised"] != DBNull.Value ? Convert.ToDateTime(dr["Order_raised"]) : (DateTime?)null;
                    oNewAPPBOK_BookingBE.PurchaseOrder.IsPOManuallyAdded = Convert.ToBoolean(dr["IsPOManuallyAdded"]);
                    oNewAPPBOK_BookingBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : (int?)null;
                    oNewAPPBOK_BookingBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                    oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    oNewAPPBOK_BookingBE.Carrier.CarrierName = dr["CarrierName"] != DBNull.Value ? Convert.ToString(dr["CarrierName"]) : string.Empty;
                    oNewAPPBOK_BookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"]) : 0;
                    oNewAPPBOK_BookingBE.MultiPONO = dr["MultiplePONo"] != DBNull.Value ? Convert.ToString(dr["MultiplePONo"]) : string.Empty;
                    oNewAPPBOK_BookingBE.MultiPOIDs = dr["MultiplePOids"] != DBNull.Value ? Convert.ToString(dr["MultiplePOids"]) : string.Empty;
                    oNewAPPBOK_BookingBE.POChaseCommunicationId = dr["POChaseCommunicationId"] != DBNull.Value ? Convert.ToInt32(dr["POChaseCommunicationId"]) : 0;
                    oNewAPPBOK_BookingBE.Country = dr["Country"] != DBNull.Value ? Convert.ToString(dr["Country"]) : string.Empty;
                    if (dr.Table.Columns.Contains("WeekDay"))
                        oNewAPPBOK_BookingBE.WeekDay = dr["WeekDay"] != DBNull.Value ? Convert.ToInt32(dr["WeekDay"].ToString()) : 0;
                    oNewAPPBOK_BookingBE.SlotTime = new SYS_SlotTimeBE();
                    if (dr.Table.Columns.Contains("SlotTime"))
                        oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["SlotTime"] != DBNull.Value ? Convert.ToString(dr["SlotTime"]) : string.Empty;

                    oNewAPPBOK_BookingBE.NoOfPallets = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToString(dr["NumberOfPallet"]) : string.Empty;
                    oNewAPPBOK_BookingBE.NoOfCartoons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToString(dr["NumberOfCartons"]) : string.Empty;

                    oNewAPPBOK_BookingBE.RecordCount = ds.Tables[1].Rows[0]["RecordCount"] != DBNull.Value ? Convert.ToInt32(ds.Tables[1].Rows[0]["RecordCount"]) : 0;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public void UpdateBookingReviewedStatusDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);

                if (oAPPBOK_BookingBE.Action == "UpdateAdvisedDate")
                    SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);
                else
                    SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spOverdueBooking", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }


        public List<APPBOK_BookingBE> GetDailyVolumeDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPTBOK_DailyVolume", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                    oNewAPPBOK_BookingBE.NumberOfDeliveries = dr["NumberOfDeliveries"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfDeliveries"]) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"]) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"]) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfLines = dr["NumberOfDeliveries"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLines"]) : (int?)null;
                    oNewAPPBOK_BookingBE.SlotTime = new SYS_SlotTimeBE();
                    oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["SlotTime"] != DBNull.Value ? Convert.ToString(dr["SlotTime"]) : string.Empty;
                    oNewAPPBOK_BookingBE.GraphType = dr["GraphType"] != DBNull.Value ? Convert.ToString(dr["GraphType"]) : string.Empty;
                    oNewAPPBOK_BookingBE.ScheduleDate = dr["ScheduleDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduleDate"]) : (DateTime?)null;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        // Srage 14 R2 - Point 21 - Start - Checking booking by PO is existing or not.
        public List<APPBOK_BookingBE> CheckExistingBookingsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrders", oAPPBOK_BookingBE.PurchaseOrders);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.VendorID);
                param[index++] = new SqlParameter("@CarrierID", oAPPBOK_BookingBE.VendorCarrierID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE appbok_BookingBE = new APPBOK_BookingBE();
                    appbok_BookingBE.BookingID = dr["BookingID"] != DBNull.Value ? Convert.ToInt32(dr["BookingID"]) : -1;
                    appbok_BookingBE.BookingRef = dr["BookingReferenceID"] != DBNull.Value ? Convert.ToString(dr["BookingReferenceID"]) : string.Empty;
                    appbok_BookingBE.Vendor = new UP_VendorBE();
                    appbok_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                    appbok_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    appbok_BookingBE.Carrier.CarrierName = dr["CarrierName"] != DBNull.Value ? Convert.ToString(dr["CarrierName"]) : string.Empty;
                    appbok_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"]) : (int?)null;
                    appbok_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"]) : (int?)null;
                    appbok_BookingBE.NumberOfLines = dr["NumberOfLine"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLine"]) : (int?)null;
                    appbok_BookingBE.ScheduleDate = dr["ScheduleDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduleDate"]) : (DateTime?)null;
                    appbok_BookingBE.SupplierType = dr["SupplierType"] != DBNull.Value ? Convert.ToString(dr["SupplierType"]) : string.Empty;
                    lstAPPBOK_BookingBE.Add(appbok_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }
        // Sprint 1 - Point 1 - End

        public int? RejectChangesBookingDetailDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


        public int? OverdueBookingReportDetailDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }



        public DataTable GetScheduleAvailabilityDAL(int SiteId, int iYear, int iMonth)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@SiteID", SiteId);
                param[index++] = new SqlParameter("@iMonth", iMonth);
                param[index++] = new SqlParameter("@iYear", iYear);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_ScheduleAvailablity", param);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataSet GetDailyVolumeTrackerDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataSet ds = new DataSet();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "sp_DailyVolumeReportOverview", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        //Sprint 18 R1 point 2
        public List<APPBOK_BookingBE> DeletedBookingDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE appbok_BookingBE = new APPBOK_BookingBE();

                    appbok_BookingBE.BookingRef = dr["BookingReferenceID"] != DBNull.Value ? Convert.ToString(dr["BookingReferenceID"]) : string.Empty;
                    appbok_BookingBE.ScheduleDate = dr["ScheduleDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduleDate"]) : (DateTime?)null;
                    appbok_BookingBE.UserName = dr["UserName"] != DBNull.Value ? Convert.ToString(dr["UserName"]) : string.Empty;
                    appbok_BookingBE.DeletedDate = dr["DeletedDate"] != DBNull.Value ? Convert.ToDateTime(dr["DeletedDate"]) : (DateTime?)null;
                    appbok_BookingBE.PurchaseOrdersIDs = dr["PurchaseOrdersIDs"] != DBNull.Value ? Convert.ToString(dr["PurchaseOrdersIDs"]) : string.Empty;

                    appbok_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    appbok_BookingBE.SlotTime.SlotTime = dr["SlotTime"].ToString();

                    appbok_BookingBE.SupplierType = dr["SupplierType"] != DBNull.Value ? dr["SupplierType"].ToString() : "";
                    appbok_BookingBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : -1;
                    appbok_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    appbok_BookingBE.Carrier.CarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"].ToString()) : -1;
                    appbok_BookingBE.SiteId = dr["SiteId"] != DBNull.Value ? Convert.ToInt32(dr["SiteId"].ToString()) : (int?)null;

                    lstAPPBOK_BookingBE.Add(appbok_BookingBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }

        public void InsertISPM15DAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[16];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.VendorID);
                param[index++] = new SqlParameter("@DeliveryChecked", oAPPBOK_BookingBE.DeliveryChecked);
                param[index++] = new SqlParameter("@IssuesFound", oAPPBOK_BookingBE.IssuesFound);
                param[index++] = new SqlParameter("@CheckedBy", oAPPBOK_BookingBE.CheckedBy);
                param[index++] = new SqlParameter("@ISPM15Comments", oAPPBOK_BookingBE.ISPM15Comments);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@IsStandardPalletChecking", oAPPBOK_BookingBE.IsStandardPalletChecking);
                param[index++] = new SqlParameter("@IsVenodrPalletChecking", oAPPBOK_BookingBE.IsVenodrPalletChecking);
                param[index++] = new SqlParameter("@ISPM15CountryPalletChecking", oAPPBOK_BookingBE.ISPM15CountryPalletChecking);
                SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public void InsertHazardousItemDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[16];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.VendorID);
                param[index++] = new SqlParameter("@DeliveryChecked", oAPPBOK_BookingBE.DeliveryChecked);
                param[index++] = new SqlParameter("@IssuesFound", oAPPBOK_BookingBE.IssuesFound);
                param[index++] = new SqlParameter("@CheckedBy", oAPPBOK_BookingBE.CheckedBy);
                param[index++] = new SqlParameter("@HazardousCommments", oAPPBOK_BookingBE.HazardousCommments);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);

                SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spUP_Vendor", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }


        public DataSet GetVolumeDetailsAlternateSlotDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataSet ds = new DataSet();

            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@WeekDay", oAPPBOK_BookingBE.WeekDay);
                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "USP_VolumeDetailsAlternateslot", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public DataTable GetAllTodayBookingWithVendorDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteId", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataTable GetWindowAlternateSlot(int SiteId, string ScheduleDate, string WeekDay, string action)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@siteid", SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", ScheduleDate);
                param[index++] = new SqlParameter("@Weekday", WeekDay);
                param[index++] = new SqlParameter("@Action", action);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "uspAlternateTimeWindow", param);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }



        public DataTable GetWindowSlotHoverData(int SiteId, string ScheduleDate, string action)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@siteid", SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", ScheduleDate);
                param[index++] = new SqlParameter("@Action", action);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "uspAlternateTimeWindow", param);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public List<APPBOK_BookingBE> GetEmailDataForISPMIssueDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();

            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                        oNewAPPBOK_BookingBE.BookingRef = dr["BookingRef"] != DBNull.Value ? dr["BookingRef"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.SupplierType = dr["SupplierType"] != DBNull.Value ? dr["SupplierType"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.VendorCarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"].ToString()) : 0;

                        oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNewAPPBOK_BookingBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : string.Empty;

                        oNewAPPBOK_BookingBE.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.PurchaseOrders = dr["PurchaseOrders"] != DBNull.Value ? dr["PurchaseOrders"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.CheckedBy = dr["CheckedBy"] != DBNull.Value ? dr["CheckedBy"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.ISPM15Comments = dr["ISPM15Comments"] != DBNull.Value ? dr["ISPM15Comments"].ToString() : string.Empty;

                        oNewAPPBOK_BookingBE.IsStandardPalletChecking = dr["IsStandardPalletChecking"] != DBNull.Value
                            ? Convert.ToBoolean(dr["IsStandardPalletChecking"]) : false;
                        oNewAPPBOK_BookingBE.IsVenodrPalletChecking = dr["IsVenodrPalletChecking"] != DBNull.Value
                           ? Convert.ToBoolean(dr["IsVenodrPalletChecking"]) : false;
                        oNewAPPBOK_BookingBE.ISPM15CountryPalletChecking = dr["ISPM15CountryPalletChecking"] != DBNull.Value
                           ? Convert.ToBoolean(dr["ISPM15CountryPalletChecking"]) : false;
                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetEmailIdsForStockPlannerISPMIssueDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();

            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                        oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNewAPPBOK_BookingBE.StockPlannerEmail = dr["StockPlannerEmailId"] != DBNull.Value ? dr["StockPlannerEmailId"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.Language = dr["Language"] != DBNull.Value ? dr["Language"].ToString() : string.Empty;
                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public DataSet GetCurrentBookingInformationDAL(string Booking)
        {
            DataSet ds = new DataSet();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Booking", Booking.Split('-')[1]);
                param[index++] = new SqlParameter("@Action", Booking.Split('-')[0]);
                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "GetCurrentBookingInformation", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public DataSet GetVolumeWindowDetailsAlternateSlotDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataSet ds = new DataSet();

            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);
                param[index++] = new SqlParameter("@WeekDay", oAPPBOK_BookingBE.WeekDay);
                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "USP_WindowVolumeDetailsAlternateslot", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public List<APPBOK_BookingBE> GetNonStandardWindowBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SiteId);
                param[index++] = new SqlParameter("@ScheduleDate", oAPPBOK_BookingBE.ScheduleDate);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                    oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"].ToString();
                    oNewAPPBOK_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfLift = dr["NumberOfLift"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLift"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfLines = dr["NumberOfLine"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLine"].ToString()) : (int?)null;
                    oNewAPPBOK_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"].ToString()) : (int?)null;
                    oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                }


            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetUnexpectedBookingDataForStockPlannerDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();

            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();

                        oNewAPPBOK_BookingBE.BookingRef = dr["BookingRef"] != DBNull.Value ? dr["BookingRef"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.ScheduleDate = Convert.ToDateTime(dr["ScheduleDate"]);
                        oNewAPPBOK_BookingBE.SupplierType = dr["SupplierType"] != DBNull.Value ? dr["SupplierType"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.VendorCarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"].ToString()) : 0;

                        oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNewAPPBOK_BookingBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : string.Empty;

                        oNewAPPBOK_BookingBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                        oNewAPPBOK_BookingBE.Carrier.CarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"].ToString()) : -1;
                        oNewAPPBOK_BookingBE.Carrier.CarrierName = dr["CarrierName"] != DBNull.Value ? dr["CarrierName"].ToString() : "-";

                        oNewAPPBOK_BookingBE.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.PurchaseOrders = dr["PurchaseOrders"] != DBNull.Value ? dr["PurchaseOrders"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                        oNewAPPBOK_BookingBE.SlotTime.SlotTime = dr["SlotTime"].ToString();
                        oNewAPPBOK_BookingBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.NumberOfLift = dr["NumberOfLift"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLift"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.NumberOfLines = dr["NumberOfLine"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfLine"].ToString()) : (int?)null;
                        oNewAPPBOK_BookingBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"].ToString()) : (int?)null;

                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetEmailIdsForStockPlannerUnexpectedBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();

            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                        oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNewAPPBOK_BookingBE.StockPlannerEmail = dr["StockPlannerEmailId"] != DBNull.Value ? dr["StockPlannerEmailId"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.Language = dr["Language"] != DBNull.Value ? dr["Language"].ToString() : string.Empty;
                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetHazardousBookingDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable dt = new DataTable();
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@SiteID", oAPPBOK_BookingBE.SelectedSiteIds);
                param[index++] = new SqlParameter("@FromDate", oAPPBOK_BookingBE.FromDate);
                param[index++] = new SqlParameter("@ToDate", oAPPBOK_BookingBE.ToDate);
                param[index++] = new SqlParameter("@VendorID", oAPPBOK_BookingBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@CarrierID", oAPPBOK_BookingBE.SelectedCarrierIDs);
                param[index++] = new SqlParameter("@GridCurrentPageNo", oAPPBOK_BookingBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", oAPPBOK_BookingBE.GridPageSize);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMassit_HazardousBooking", param);

                dt = ds.Tables[0];
                if (oAPPBOK_BookingBE.Action == "GetHazrdousBooking")
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                        oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.NumberOfDeliveries = dr["#BOOKING"] != DBNull.Value ? Convert.ToInt32(dr["#BOOKING"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"].ToString()) : 0;
                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }

                if (oAPPBOK_BookingBE.Action == "GetHazrdousBookingByVendorid")
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        APPBOK_BookingBE oNewAPPBOK_BookingBE = new APPBOK_BookingBE();
                        oNewAPPBOK_BookingBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oNewAPPBOK_BookingBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.BookingRef = dr["ReferenceID"] != DBNull.Value ? dr["ReferenceID"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.ScheduleDate = dr["ScheduleDate"] != DBNull.Value ? Convert.ToDateTime(dr["ScheduleDate"].ToString()) : (DateTime?)null;
                        oNewAPPBOK_BookingBE.BookingComments = dr["BookingComments"] != DBNull.Value ? dr["BookingComments"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.UserName = dr["Uploadedby"] != DBNull.Value ? dr["Uploadedby"].ToString() : string.Empty;
                        oNewAPPBOK_BookingBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecords"].ToString()) : 0;
                        oNewAPPBOK_BookingBE.IsEnableHazardouesItemPrompt = dr["IsEnableHazardouesItemPrompt"] != DBNull.Value ? Convert.ToBoolean(dr["IsEnableHazardouesItemPrompt"]) : false;
                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public int? UndoBookingDeliveryArrivedStatusDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.UserID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UndoBookingDeliveryUnLoadStatusDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oAPPBOK_BookingBE.Action);
                param[index++] = new SqlParameter("@BookingID", oAPPBOK_BookingBE.BookingID);
                param[index++] = new SqlParameter("@UserID", oAPPBOK_BookingBE.UserID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Booking", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DashboardBooking> GetDashboardBookingsDAL(string action, int Siteid, DateTime Date)
        {
            DataTable dt = new DataTable();

            List<DashboardBooking> oAPPBOK_BookingBEList = new List<DashboardBooking>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", action);
                param[index++] = new SqlParameter("@Siteid", Siteid);
                param[index++] = new SqlParameter("@Date", Date);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "GetWeeklyBookingReport", param);

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        DashboardBooking oNewAPPBOK_BookingBE = new DashboardBooking();
                        oNewAPPBOK_BookingBE.SiteId = dr["Siteid"] != DBNull.Value ? Convert.ToInt32(dr["Siteid"]) : 0;
                        oNewAPPBOK_BookingBE.Bookings = dr["Bookings"] != DBNull.Value ? Convert.ToInt32(dr["Bookings"]) : 0;
                        oNewAPPBOK_BookingBE.BookingsWithIssues = dr["BookingsWithIssues"] != DBNull.Value ? Convert.ToInt32(dr["BookingsWithIssues"]) : 0;
                        oNewAPPBOK_BookingBE.Cartons = dr["Cartons"] != DBNull.Value ? Convert.ToInt32(dr["Cartons"]) : 0;
                        oNewAPPBOK_BookingBE.Date = dr["Date"] != DBNull.Value ? Convert.ToDateTime(dr["Date"]) : (DateTime?)null;
                        oNewAPPBOK_BookingBE.Lifts = dr["Lift"] != DBNull.Value ? Convert.ToInt32(dr["Lift"]) : 0;
                        oNewAPPBOK_BookingBE.Lines = dr["Line"] != DBNull.Value ? Convert.ToInt32(dr["Line"]) : 0;
                        oNewAPPBOK_BookingBE.OpenDiscrepancies = dr["OpenDiscrepancies"] != DBNull.Value ? Convert.ToInt32(dr["OpenDiscrepancies"]) : 0;
                        oNewAPPBOK_BookingBE.TotalDiscrepancies = dr["DiscrepanciesCreated"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepanciesCreated"]) : 0;
                        oNewAPPBOK_BookingBE.Pos = dr["POs"] != DBNull.Value ? Convert.ToInt32(dr["POs"]) : 0;
                        oNewAPPBOK_BookingBE.Pallets = dr["Pallet"] != DBNull.Value ? Convert.ToInt32(dr["Pallet"]) : 0;
                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }

        public List<DashboardBooking> GetDashboardOneBookingsDAL(string action, int Siteid, DateTime Date)
        {
            DataTable dt = new DataTable();

            List<DashboardBooking> oAPPBOK_BookingBEList = new List<DashboardBooking>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", action);
                param[index++] = new SqlParameter("@Siteid", Siteid);
                param[index++] = new SqlParameter("@Date", Date);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "GetWeeklyBookingReport", param);

                if (ds.Tables.Count > 0)
                {
                  
                    dt = ds.Tables[1];

                    List<DateDashBoard> DateDashBoards = new List<DateDashBoard>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        DateDashBoard dateDashBoard = new DateDashBoard();
                        dateDashBoard.ID = dr["ID"] != DBNull.Value ? dr["ID"].ToString() : "0";
                        dateDashBoard.Date = dr["Date"] != DBNull.Value ? Convert.ToDateTime(dr["Date"]) : (DateTime?)null;
                        dateDashBoard.IsActive = dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false;
                        DateDashBoards.Add(dateDashBoard);
                    }

                    dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        DashboardBooking oNewAPPBOK_BookingBE = new DashboardBooking();
                        oNewAPPBOK_BookingBE.ID = dr["ID"] != DBNull.Value ? dr["ID"].ToString() : "0";
                        oNewAPPBOK_BookingBE.Cartons = dr["CARTONS"] != DBNull.Value ? Convert.ToInt32(dr["Cartons"]) : 0;
                        oNewAPPBOK_BookingBE.Lifts = dr["LIFT"] != DBNull.Value ? Convert.ToInt32(dr["Lift"]) : 0;
                        oNewAPPBOK_BookingBE.Lines = dr["LINE"] != DBNull.Value ? Convert.ToInt32(dr["Line"]) : 0;
                        oNewAPPBOK_BookingBE.Pallets = dr["PALLET"] != DBNull.Value ? Convert.ToInt32(dr["Pallet"]) : 0;
                        oNewAPPBOK_BookingBE.ArrivedPallets = dr["Arrived"] != DBNull.Value ? Convert.ToInt32(dr["Arrived"]) : 0;
                        oNewAPPBOK_BookingBE.NotArrivedPallets = dr["NotArrived"] != DBNull.Value ? Convert.ToInt32(dr["NotArrived"]) : 0;

                        oNewAPPBOK_BookingBE.ArrivedLines = dr["ArrivedLines"] != DBNull.Value ? Convert.ToInt32(dr["ArrivedLines"]) : 0;
                        oNewAPPBOK_BookingBE.NotArrivedLines = dr["NotArrivedLines"] != DBNull.Value ? Convert.ToInt32(dr["NotArrivedLines"]) : 0;

                        oNewAPPBOK_BookingBE.NoShowPallets = dr["NoShow"] != DBNull.Value ? Convert.ToInt32(dr["NoShow"]) : 0;
                        oNewAPPBOK_BookingBE.DateTimes = new List<DateDashBoard>();
                        oNewAPPBOK_BookingBE.DateTimes.AddRange(DateDashBoards);
                        oAPPBOK_BookingBEList.Add(oNewAPPBOK_BookingBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oAPPBOK_BookingBEList;
        }
    }
}

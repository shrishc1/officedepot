﻿using BusinessEntities.ModuleBE.Appointment.Reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.Reports
{
    public class FixedSlotUsageReportDAL : BaseDAL
    {

        public List<FixedSlotUsageReportBE> GetFixedSlotUsageReportDAL(FixedSlotUsageReportBE oFixedSlotUsageReportBE)
        {
            DataTable dt = new DataTable();
            List<FixedSlotUsageReportBE> oFixedSlotUsageReportBEList = new List<FixedSlotUsageReportBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oFixedSlotUsageReportBE.Action);
                param[index++] = new SqlParameter("@SiteID", oFixedSlotUsageReportBE.SiteID);
                param[index++] = new SqlParameter("@Period", oFixedSlotUsageReportBE.Period);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPT_FixedSlotUsage", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    FixedSlotUsageReportBE oNewFixedSlotUsageReportBE = new FixedSlotUsageReportBE();
                    oNewFixedSlotUsageReportBE.AllocationType = dr["AllocationType"] != DBNull.Value ? Convert.ToString(dr["AllocationType"]) : string.Empty;
                    oNewFixedSlotUsageReportBE.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                    oNewFixedSlotUsageReportBE.SlotTime = dr["SlotTime"] != DBNull.Value ? Convert.ToString(dr["SlotTime"]) : string.Empty;
                    oNewFixedSlotUsageReportBE.DoorNumber = dr["DoorNumber"] != DBNull.Value ? Convert.ToString(dr["DoorNumber"]) : string.Empty;

                    oNewFixedSlotUsageReportBE.MaximumCatrons = dr["MaximumCatrons"] != DBNull.Value ? Convert.ToInt32(dr["MaximumCatrons"]) : 0;
                    oNewFixedSlotUsageReportBE.MaximumLines = dr["MaximumLines"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLines"]) : 0;
                    oNewFixedSlotUsageReportBE.MaximumPallets = dr["MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["MaximumPallets"]) : 0;


                    oNewFixedSlotUsageReportBE.Monday = dr["Monday"] != DBNull.Value ? Convert.ToString(dr["Monday"]) == "True" ? "Y" : string.Empty : string.Empty;
                    oNewFixedSlotUsageReportBE.Tuesday = dr["Tuesday"] != DBNull.Value ? Convert.ToString(dr["Tuesday"]) == "True" ? "Y" : string.Empty : string.Empty;
                    oNewFixedSlotUsageReportBE.Wednesday = dr["Wednesday"] != DBNull.Value ? Convert.ToString(dr["Wednesday"]) == "True" ? "Y" : string.Empty : string.Empty;
                    oNewFixedSlotUsageReportBE.Thursday = dr["Thursday"] != DBNull.Value ? Convert.ToString(dr["Thursday"]) == "True" ? "Y" : string.Empty : string.Empty;
                    oNewFixedSlotUsageReportBE.Friday = dr["Friday"] != DBNull.Value ? Convert.ToString(dr["Friday"]) == "True" ? "Y" : string.Empty : string.Empty;

                    oNewFixedSlotUsageReportBE.TOTALCOUNT = dr["TOTALCOUNT"] != DBNull.Value ? Convert.ToInt32(dr["TOTALCOUNT"]) : 0;
                    oNewFixedSlotUsageReportBE.USED = dr["USED"] != DBNull.Value ? Convert.ToInt32(dr["USED"]) : 0;
                    oNewFixedSlotUsageReportBE.PercUsed = dr["PercUsed"] != DBNull.Value ? Convert.ToDecimal(dr["PercUsed"]) : 0;

                    oNewFixedSlotUsageReportBE.UsedCartons = dr["UsedCartons"] != DBNull.Value ? Convert.ToString(dr["UsedCartons"]) : "0.00";
                    oNewFixedSlotUsageReportBE.UsedLines = dr["UsedLines"] != DBNull.Value ? Convert.ToString(dr["UsedLines"]) : "0.00";
                    oNewFixedSlotUsageReportBE.UsedPallets = dr["UsedPallets"] != DBNull.Value ? Convert.ToString(dr["UsedPallets"]) : "0.00";

                    oNewFixedSlotUsageReportBE.FixedSlotID = dr["FixedSlotID"] != DBNull.Value ? Convert.ToInt32(dr["FixedSlotID"]) : 0;

                    oFixedSlotUsageReportBEList.Add(oNewFixedSlotUsageReportBE);
                }

                return oFixedSlotUsageReportBEList;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oFixedSlotUsageReportBEList;
        }


        public List<FixedSlotUsageReportBE> GetFixedSlotUsageDetailsDAL(FixedSlotUsageReportBE oFixedSlotUsageReportBE)
        {
            DataTable dt = new DataTable();
            List<FixedSlotUsageReportBE> oFixedSlotUsageReportBEList = new List<FixedSlotUsageReportBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oFixedSlotUsageReportBE.Action);
                param[index++] = new SqlParameter("@SiteID", oFixedSlotUsageReportBE.SiteID);
                param[index++] = new SqlParameter("@FixedSlotID", oFixedSlotUsageReportBE.FixedSlotID);
                param[index++] = new SqlParameter("@Period", oFixedSlotUsageReportBE.Period);
                param[index++] = new SqlParameter("@VendorID", oFixedSlotUsageReportBE.VendorID);
                param[index++] = new SqlParameter("@CarrierID", oFixedSlotUsageReportBE.CarrierID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPT_FixedSlotUsage", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    FixedSlotUsageReportBE oNewFixedSlotUsageReportBE = new FixedSlotUsageReportBE();
                    oNewFixedSlotUsageReportBE.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                    oNewFixedSlotUsageReportBE.SlotTime = dr["SlotTime"] != DBNull.Value ? Convert.ToString(dr["SlotTime"]) : string.Empty;
                    oNewFixedSlotUsageReportBE.DoorNumber = dr["DoorNumber"] != DBNull.Value ? Convert.ToString(dr["DoorNumber"]) : string.Empty;

                    oNewFixedSlotUsageReportBE.MaximumCatrons = dr["MaximumCatrons"] != DBNull.Value ? Convert.ToInt32(dr["MaximumCatrons"]) : (int?)null;
                    oNewFixedSlotUsageReportBE.MaximumLines = dr["MaximumLines"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLines"]) : (int?)null;
                    oNewFixedSlotUsageReportBE.MaximumPallets = dr["MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["MaximumPallets"]) : (int?)null;

                    oNewFixedSlotUsageReportBE.Day = dr["Day"] != DBNull.Value ? Convert.ToString(dr["Day"]) : string.Empty;

                    oNewFixedSlotUsageReportBE.TOTALCOUNT = dr["TOTALCOUNT"] != DBNull.Value ? Convert.ToInt32(dr["TOTALCOUNT"]) : 0;
                    oNewFixedSlotUsageReportBE.USED = dr["USED"] != DBNull.Value ? Convert.ToInt32(dr["USED"]) : 0;
                    oNewFixedSlotUsageReportBE.PercUsed = dr["PercUsed"] != DBNull.Value ? Convert.ToDecimal(dr["PercUsed"]) : 0;

                    oNewFixedSlotUsageReportBE.UsedCartons = dr["UsedCartons"] != DBNull.Value ? Convert.ToString(dr["UsedCartons"]) : "0.00";
                    oNewFixedSlotUsageReportBE.UsedLines = dr["UsedLines"] != DBNull.Value ? Convert.ToString(dr["UsedLines"]) : "0.00";
                    oNewFixedSlotUsageReportBE.UsedPallets = dr["UsedPallets"] != DBNull.Value ? Convert.ToString(dr["UsedPallets"]) : "0.00";



                    oFixedSlotUsageReportBEList.Add(oNewFixedSlotUsageReportBE);
                }

                return oFixedSlotUsageReportBEList;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oFixedSlotUsageReportBEList;
        }


        public List<FixedSlotUsageReportBE> GetFixedSlotUsageVendorDAL(FixedSlotUsageReportBE oFixedSlotUsageReportBE)
        {
            DataTable dt = new DataTable();
            List<FixedSlotUsageReportBE> oFixedSlotUsageReportBEList = new List<FixedSlotUsageReportBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oFixedSlotUsageReportBE.Action);
                param[index++] = new SqlParameter("@SiteID", oFixedSlotUsageReportBE.SiteID);
                param[index++] = new SqlParameter("@Period", oFixedSlotUsageReportBE.Period);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPT_FixedSlotUsage", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    FixedSlotUsageReportBE oNewFixedSlotUsageReportBE = new FixedSlotUsageReportBE();
                    oNewFixedSlotUsageReportBE.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;

                    oNewFixedSlotUsageReportBE.TOTALCOUNT = dr["TOTALCOUNT"] != DBNull.Value ? Convert.ToInt32(dr["TOTALCOUNT"]) : 0;
                    oNewFixedSlotUsageReportBE.USED = dr["USED"] != DBNull.Value ? Convert.ToInt32(dr["USED"]) : 0;
                    oNewFixedSlotUsageReportBE.PercUsed = dr["PercUsed"] != DBNull.Value ? Convert.ToDecimal(dr["PercUsed"]) : 0;

                    oNewFixedSlotUsageReportBE.UsedCartons = dr["UsedCartons"] != DBNull.Value ? Convert.ToString(dr["UsedCartons"]) : "0.00";
                    oNewFixedSlotUsageReportBE.UsedLines = dr["UsedLines"] != DBNull.Value ? Convert.ToString(dr["UsedLines"]) : "0.00";
                    oNewFixedSlotUsageReportBE.UsedPallets = dr["UsedPallets"] != DBNull.Value ? Convert.ToString(dr["UsedPallets"]) : "0.00";

                    oNewFixedSlotUsageReportBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                    oNewFixedSlotUsageReportBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;

                    oNewFixedSlotUsageReportBE.ScheduleType = dr["ScheduleType"] != DBNull.Value ? Convert.ToString(dr["ScheduleType"]) : string.Empty;

                    oFixedSlotUsageReportBEList.Add(oNewFixedSlotUsageReportBE);
                }

                return oFixedSlotUsageReportBEList;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oFixedSlotUsageReportBEList;
        }

    }
}

﻿using BusinessEntities.ModuleBE.Appointment.Reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.Reports
{
    public class ScheduledReceiptedReportDAL : BaseDAL
    {
        public List<ScheduledReceiptedReportDetailsBE> ScheduledReceiptedReportDetailsDAL(ScheduledReceiptedReportDetailsBE oScheduledReceiptedReportDetailsBE)
        {
            DataTable dt = new DataTable();
            List<ScheduledReceiptedReportDetailsBE> oScheduledReceiptedReportDetailsBEList = new List<ScheduledReceiptedReportDetailsBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oScheduledReceiptedReportDetailsBE.Action);
                param[index++] = new SqlParameter("@DATE", oScheduledReceiptedReportDetailsBE.Date);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spScheduledReceiptedReport", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    ScheduledReceiptedReportDetailsBE oNewScheduledReceiptedReportDetailsBE = new ScheduledReceiptedReportDetailsBE();
                    oNewScheduledReceiptedReportDetailsBE.SiteName = dr["SiteName"] != DBNull.Value ? Convert.ToString(dr["SiteName"].ToString()) : string.Empty;
                    oNewScheduledReceiptedReportDetailsBE.LinesDue = dr["LineNumber"] != DBNull.Value ? Convert.ToInt32(dr["LineNumber"].ToString()) : 0;
                    oNewScheduledReceiptedReportDetailsBE.LinesBooked = dr["LinesBooked"] != DBNull.Value ? Convert.ToInt32(dr["LinesBooked"].ToString()) : 0;
                    oNewScheduledReceiptedReportDetailsBE.LinesReceipted = dr["LinesReceipted"] != DBNull.Value ? Convert.ToInt32(dr["LinesReceipted"].ToString()) : 0;
                    oNewScheduledReceiptedReportDetailsBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oScheduledReceiptedReportDetailsBEList.Add(oNewScheduledReceiptedReportDetailsBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oScheduledReceiptedReportDetailsBEList;
        }


        public void ScheduledReceiptedReportDetailsVendorDAL(ScheduledReceiptedReportDetailsBE oScheduledReceiptedReportDetailsBE, out List<ScheduledReceiptedReportDetailsBE> oScheduledReceiptedReportDetailsBEList, out int RecordCount)
        {
            DataTable dt = new DataTable();
            oScheduledReceiptedReportDetailsBEList = new List<ScheduledReceiptedReportDetailsBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oScheduledReceiptedReportDetailsBE.Action);
                param[index++] = new SqlParameter("@DATE", oScheduledReceiptedReportDetailsBE.Date);
                param[index++] = new SqlParameter("@SITEID", oScheduledReceiptedReportDetailsBE.SiteId);
                param[index++] = new SqlParameter("@PageIndex", oScheduledReceiptedReportDetailsBE.PageCount);
                param[index++] = new SqlParameter("@RecordCount", SqlDbType.Int);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spScheduledReceiptedReport", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    ScheduledReceiptedReportDetailsBE oNewScheduledReceiptedReportDetailsBE = new ScheduledReceiptedReportDetailsBE();
                    oNewScheduledReceiptedReportDetailsBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oNewScheduledReceiptedReportDetailsBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                    oNewScheduledReceiptedReportDetailsBE.LinesBooked = dr["LinesBooked"] != DBNull.Value ? Convert.ToInt32(dr["LinesBooked"].ToString()) : 0;
                    oNewScheduledReceiptedReportDetailsBE.LinesReceipted = dr["LinesReceipted"] != DBNull.Value ? Convert.ToInt32(dr["LinesReceipted"].ToString()) : 0;
                    oNewScheduledReceiptedReportDetailsBE.LinesDue = dr["LineNumber"] != DBNull.Value ? Convert.ToInt32(dr["LineNumber"].ToString()) : 0;
                    oNewScheduledReceiptedReportDetailsBE.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"].ToString()) : string.Empty;
                    oScheduledReceiptedReportDetailsBEList.Add(oNewScheduledReceiptedReportDetailsBE);
                }
                RecordCount = ds.Tables[1].Rows[0]["RecordCount"] != DBNull.Value ? Convert.ToInt32(ds.Tables[1].Rows[0]["RecordCount"]) : 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                RecordCount = 0;
                oScheduledReceiptedReportDetailsBEList = null;
            }
        }


        public List<ScheduledReceiptedReportDetailsBE> ScheduledReceiptedReportDetailsVendorBySiteDAL(ScheduledReceiptedReportDetailsBE oScheduledReceiptedReportDetailsBE)
        {
            DataTable dt = new DataTable();
            List<ScheduledReceiptedReportDetailsBE> oScheduledReceiptedReportDetailsBEList = new List<ScheduledReceiptedReportDetailsBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oScheduledReceiptedReportDetailsBE.Action);
                param[index++] = new SqlParameter("@DATE", oScheduledReceiptedReportDetailsBE.Date);
                param[index++] = new SqlParameter("@SITEID", oScheduledReceiptedReportDetailsBE.SiteId);
                param[index++] = new SqlParameter("@VENDORID", oScheduledReceiptedReportDetailsBE.VendorID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spScheduledReceiptedReport", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    ScheduledReceiptedReportDetailsBE oNewScheduledReceiptedReportDetailsBE = new ScheduledReceiptedReportDetailsBE();
                    oNewScheduledReceiptedReportDetailsBE.BookingReferenceID = dr["BOOKINGREFERENCEID"] != DBNull.Value ? Convert.ToString(dr["BOOKINGREFERENCEID"].ToString()) : string.Empty;
                    oNewScheduledReceiptedReportDetailsBE.PO_NO = dr["PURCHASE_ORDER"] != DBNull.Value ? Convert.ToString(dr["PURCHASE_ORDER"].ToString()) : string.Empty;
                    oNewScheduledReceiptedReportDetailsBE.LinesBooked = dr["LINESBOOKED"] != DBNull.Value ? Convert.ToInt32(dr["LINESBOOKED"].ToString()) : 0;
                    oNewScheduledReceiptedReportDetailsBE.LinesReceipted = dr["LINESRECIEPT"] != DBNull.Value ? Convert.ToInt32(dr["LINESRECIEPT"].ToString()) : 0;
                    oNewScheduledReceiptedReportDetailsBE.TotalReceipt = dr["TotalReceipt"] != DBNull.Value ? Convert.ToInt32(dr["TotalReceipt"].ToString()) : 0;
                    oScheduledReceiptedReportDetailsBEList.Add(oNewScheduledReceiptedReportDetailsBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oScheduledReceiptedReportDetailsBEList;
        }
    }
}

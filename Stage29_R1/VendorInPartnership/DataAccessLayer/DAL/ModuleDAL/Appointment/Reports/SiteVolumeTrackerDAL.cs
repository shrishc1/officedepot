﻿using BusinessEntities.ModuleBE.Appointment.Reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.Reports
{
    public class SiteVolumeTrackerDAL : BaseDAL
    {

        public List<SiteVolumeTrackerBE> GetSiteVolumeReportDetailsDAL(SiteVolumeTrackerBE oSiteVolumeTrackerBE)
        {
            DataTable dt = new DataTable();
            List<SiteVolumeTrackerBE> oNewSiteVolumeTrackerBEList = new List<SiteVolumeTrackerBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@SiteID", oSiteVolumeTrackerBE.SiteID);


                DataSet ds = null;
                if (oSiteVolumeTrackerBE.Action == "CapacityReport")
                {
                    param[index++] = new SqlParameter("@WeekCounter", oSiteVolumeTrackerBE.WeekCounter);
                    ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPT_SiteVolumeTrackerReport", param);
                    dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        SiteVolumeTrackerBE oNewSiteVolumeTrackerBE = new SiteVolumeTrackerBE();


                        oNewSiteVolumeTrackerBE.CalculationDate = Convert.ToDateTime(dr["CalculationDate"].ToString());

                        oNewSiteVolumeTrackerBE.Area = Convert.ToString(dr["Area"].ToString());

                        oNewSiteVolumeTrackerBE.AreaKey = Convert.ToString(dr["AreaKey"].ToString());
                        oNewSiteVolumeTrackerBE.KeyValue = dr["KeyValue"] != DBNull.Value ? Convert.ToDouble(dr["KeyValue"].ToString()) : (double?)null;
                        oNewSiteVolumeTrackerBE.KeyOrder = dr["KeyOrder"] != DBNull.Value ? Convert.ToInt16(dr["KeyOrder"].ToString()) : 0;

                        oNewSiteVolumeTrackerBEList.Add(oNewSiteVolumeTrackerBE);
                    }
                }
                else
                {
                    param[index++] = new SqlParameter("@DateType", oSiteVolumeTrackerBE.DateType);
                    ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPT_SiteVolumeProjectedBacklogReport", param);
                    dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        SiteVolumeTrackerBE oNewSiteVolumeTrackerBE = new SiteVolumeTrackerBE();

                        oNewSiteVolumeTrackerBE.CalculationDate = Convert.ToDateTime(dr["CalculationDate"].ToString());

                        oNewSiteVolumeTrackerBE.OnBackorder = dr["OnBackorder"] != DBNull.Value ? Convert.ToInt32(dr["OnBackorder"].ToString()) : (int?)null;
                        oNewSiteVolumeTrackerBE.LinesDue = dr["LinesDue"] != DBNull.Value ? Convert.ToInt32(dr["LinesDue"].ToString()) : (int?)null;
                        oNewSiteVolumeTrackerBE.ProjectedBacklog = dr["ProjectedBacklog"] != DBNull.Value ? Convert.ToInt32(dr["ProjectedBacklog"].ToString()) : (int?)null;
                        oNewSiteVolumeTrackerBE.MaxLines = dr["MaxLines"] != DBNull.Value ? Convert.ToInt32(dr["MaxLines"].ToString()) : (int?)null;
                        oNewSiteVolumeTrackerBE.AvgLinesDue = dr["AvgLinesDue"] != DBNull.Value ? Convert.ToInt32(dr["AvgLinesDue"].ToString()) : (int?)null;
                        oNewSiteVolumeTrackerBE.AvgProjectedBacklog = dr["BacklogProjection"] != DBNull.Value ? Convert.ToInt32(dr["BacklogProjection"].ToString()) : (int?)null;
                        oNewSiteVolumeTrackerBEList.Add(oNewSiteVolumeTrackerBE);
                    }
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oNewSiteVolumeTrackerBEList;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.CountrySetting
{
    public class ISPM15PalletCheckingDAL : BaseDAL
    {
        public List<ISPM15PalletCheckingBE> GetISPM15PalletCheckingDetailsDAL(ISPM15PalletCheckingBE ispm15)
        {
            var dataTable = new DataTable();
            var lstAPActionBE = new List<ISPM15PalletCheckingBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", ispm15.Action);
                param[index++] = new SqlParameter("@ID", ispm15.ID);
                param[index++] = new SqlParameter("@GridCurrentPageNo", ispm15.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", ispm15.GridPageSize);
                var dataSet = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spISPM15PalletCheck", param);
                dataTable = dataSet.Tables[0];
                foreach (DataRow dr in dataTable.Rows)
                {
                    var oAPActionBE = new ISPM15PalletCheckingBE();
                    oAPActionBE.SourceCountryID = dr["DeliveringFromCountryID"] != DBNull.Value ? Convert.ToInt32(dr["DeliveringFromCountryID"]) : 0;
                    oAPActionBE.DestinationCountryID = dr["DeliveringIntoCountryID"] != DBNull.Value ? Convert.ToInt32(dr["DeliveringIntoCountryID"]) : 0;
                    oAPActionBE.ISPM15Check = dr["ISPM15Check"] != DBNull.Value ? Convert.ToBoolean(dr["ISPM15Check"]) : false;
                    oAPActionBE.SourceCountryName = dr["SourceCountryName"] != DBNull.Value ? Convert.ToString(dr["SourceCountryName"]) : string.Empty;
                    oAPActionBE.DestinationCountryName = dr["DestinationCountryName"] != DBNull.Value ? Convert.ToString(dr["DestinationCountryName"]) : string.Empty;
                    oAPActionBE.AddedBy = dr["AddedBy"] != DBNull.Value ? Convert.ToString(dr["AddedBy"]) : string.Empty;
                    oAPActionBE.ModifiedBy = dr["ModifiedBy"] != DBNull.Value ? Convert.ToString(dr["ModifiedBy"]) : string.Empty;
                    oAPActionBE.AddedDate = dr["AddedDate"] != DBNull.Value ? Utilities.Common.ConvertDateToString(Convert.ToDateTime(dr["AddedDate"])) : null;
                    oAPActionBE.ModifiedDate = dr["Modifieddate"] != DBNull.Value ? Utilities.Common.ConvertDateToString(Convert.ToDateTime(dr["Modifieddate"])) : null;
                    oAPActionBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? Convert.ToDouble(dr["TotalRecords"]) : 0;
                    oAPActionBE.ID = dr["ID"] != DBNull.Value ? Convert.ToInt32(dr["ID"]) : 0;
                    lstAPActionBE.Add(oAPActionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstAPActionBE;
        }

        public int? SaveISPM15PalletCheckingDAL(ISPM15PalletCheckingBE ispm15PalletCheckingBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", ispm15PalletCheckingBE.Action);
                param[index++] = new SqlParameter("@ID", ispm15PalletCheckingBE.ID);
                param[index++] = new SqlParameter("@SourceCountryID", ispm15PalletCheckingBE.SourceCountryID);
                param[index++] = new SqlParameter("@DestinationCountryID", ispm15PalletCheckingBE.DestinationCountryID);
                param[index++] = new SqlParameter("@ISPM15Check", ispm15PalletCheckingBE.ISPM15Check);
                param[index++] = new SqlParameter("@UserID", ispm15PalletCheckingBE.UserID);
                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spISPM15PalletCheck", param);
                intResult = Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0 ? Convert.ToInt32(Result.Tables[0].Rows[0][0]) : 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public bool IsSiteCountryExistsDAL(ISPM15PalletCheckingBE ispm15)
        {
            var isExists = false;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", ispm15.Action);
                param[index++] = new SqlParameter("@siteid", ispm15.SiteID); 
                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spISPM15PalletCheck", param);
                isExists = Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0 && Result.Tables[0].Rows[0][0].ToString()=="1" ? 
                    Convert.ToBoolean(Result.Tables[0].Rows[0][0]) : false;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return isExists;
        }
    }
}

﻿using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;


namespace DataAccessLayer.ModuleDAL.Appointment.CountrySetting
{
    public class APPCNT_DeliveryTypeDAL : DataAccessLayer.BaseDAL
    {

        public List<MASCNT_DeliveryTypeBE> GetDeliveryTypeDetailsDAL(MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE)
        {
            DataTable dt = new DataTable();
            List<MASCNT_DeliveryTypeBE> oMASCNT_DeliveryTypeBEList = new List<MASCNT_DeliveryTypeBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASCNT_DeliveryTypeBE.Action);
                param[index++] = new SqlParameter("@DeliveryTypeID", oMASCNT_DeliveryTypeBE.DeliveryTypeID);
                param[index++] = new SqlParameter("@DeliveryType", oMASCNT_DeliveryTypeBE.DeliveryType);
                param[index++] = new SqlParameter("@CountryID", oMASCNT_DeliveryTypeBE.CountryID);
                //oMASCNT_DeliveryTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                param[index++] = new SqlParameter("@UserID", oMASCNT_DeliveryTypeBE.User.UserID);
                param[index++] = new SqlParameter("@EnabledAsVendorOption", string.IsNullOrEmpty(oMASCNT_DeliveryTypeBE.EnabledAsVendorOption) ? "0" : oMASCNT_DeliveryTypeBE.EnabledAsVendorOption);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASCNT_DeliveryType", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASCNT_DeliveryTypeBE oNewMASCNT_DeliveryTypeBE = new MASCNT_DeliveryTypeBE();
                    oNewMASCNT_DeliveryTypeBE.DeliveryTypeID = Convert.ToInt32(dr["DeliveryTypeID"]);
                    oNewMASCNT_DeliveryTypeBE.CountryID = Convert.ToInt32(dr["CountryID"]);
                    oNewMASCNT_DeliveryTypeBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    oNewMASCNT_DeliveryTypeBE.Country.CountryName = dr["CountryName"].ToString();
                    oNewMASCNT_DeliveryTypeBE.IsEnabledAsVendorOption = dr["EnabledAsVendorOption"] != DBNull.Value ? Convert.ToBoolean(dr["EnabledAsVendorOption"]) : false;
                    oNewMASCNT_DeliveryTypeBE.DeliveryType = dr["DeliveryType"].ToString();
                    oNewMASCNT_DeliveryTypeBE.CountryDeliveryType = dr["CountryDeliveryType"].ToString();
                    //if (dr["EnabledAsVendorOption"] == null) {
                    //    oNewMASCNT_DeliveryTypeBE.EnabledAsVendorOption = Utilities.Common.getGlobalResourceValue("No");
                    //}
                    //else {
                    //    if (Convert.ToBoolean(dr["EnabledAsVendorOption"].ToString()) == true) {
                    //        oNewMASCNT_DeliveryTypeBE.EnabledAsVendorOption = Utilities.Common.getGlobalResourceValue("Yes");
                    //    }
                    //    else {
                    //        oNewMASCNT_DeliveryTypeBE.EnabledAsVendorOption = Utilities.Common.getGlobalResourceValue("No");
                    //    }
                    //}

                    oMASCNT_DeliveryTypeBEList.Add(oNewMASCNT_DeliveryTypeBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASCNT_DeliveryTypeBEList;
        }

        public int? addEditDeliveryTypeDetailsDAL(MASCNT_DeliveryTypeBE oMASCNT_DeliveryTypeBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASCNT_DeliveryTypeBE.Action);
                param[index++] = new SqlParameter("@DeliveryTypeID", oMASCNT_DeliveryTypeBE.DeliveryTypeID);
                param[index++] = new SqlParameter("@CountryID", oMASCNT_DeliveryTypeBE.CountryID);
                param[index++] = new SqlParameter("@DeliveryType", oMASCNT_DeliveryTypeBE.DeliveryType);
                param[index++] = new SqlParameter("@EnabledAsVendorOption", oMASCNT_DeliveryTypeBE.EnabledAsVendorOption == "Yes" ? 1 : 0);
                param[index++] = new SqlParameter("@IsActive", oMASCNT_DeliveryTypeBE.IsActive);


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASCNT_DeliveryType", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

﻿using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.CountrySetting
{
    public class APPCNT_AutoEmailDAL : BaseDAL
    {
        public List<MASCNT_AutoEmailBE> GetAutoEmailPODetailsDAL(MASCNT_AutoEmailBE AutoEmailBE)
        {
            var dataTable = new DataTable();
            var lstAPActionBE = new List<MASCNT_AutoEmailBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", AutoEmailBE.Action);
                param[index++] = new SqlParameter("@VendorIds", AutoEmailBE.UP_VendorBE.VendorIDs);
                param[index++] = new SqlParameter("@CountryID", AutoEmailBE.UP_VendorBE.CountryID);
                param[index++] = new SqlParameter("@GridCurrentPageNo", AutoEmailBE.GridCurrentPageNo);
                param[index++] = new SqlParameter("@GridPageSize", AutoEmailBE.GridPageSize);
                param[index++] = new SqlParameter("@EmailStatus", AutoEmailBE.EmailStatus);
                var dataSet = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_AutoEmailPOToVendor", param);
                dataTable = dataSet.Tables[0];

                foreach (DataRow dr in dataTable.Rows)
                {
                    var oAPActionBE = new MASCNT_AutoEmailBE();
                    oAPActionBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    oAPActionBE.UP_VendorBE = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oAPActionBE.Country.CountryID = dr["CountryID"] != DBNull.Value ? Convert.ToInt32(dr["CountryID"].ToString()) : 0;
                    oAPActionBE.Country.CountryName = dr["CountryName"] != DBNull.Value ? Convert.ToString(dr["CountryName"]) : string.Empty;
                    //oAPActionBE.IsAutoEmailNewPO = dr["IsAutoEmailNewPO"] != DBNull.Value ? Convert.ToBoolean(dr["IsAutoEmailNewPO"]) : false;
                    oAPActionBE.IsAutoEmailNewPO = dr["AutoEmailNewPO"] != DBNull.Value ? Convert.ToInt32(dr["AutoEmailNewPO"]) : 0;
                    oAPActionBE.UP_VendorBE.Vendor_No = dr["Vendor_No"] != DBNull.Value ? Convert.ToString(dr["Vendor_No"]) : string.Empty;
                    oAPActionBE.UP_VendorBE.VendorName = dr["Vendor_Name"] != DBNull.Value ? Convert.ToString(dr["Vendor_Name"]) : string.Empty;
                    oAPActionBE.TotalRecord = dr["TotalRecord"] != DBNull.Value ? Convert.ToInt32(dr["TotalRecord"]) : 0;
                    oAPActionBE.UP_VendorBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : 0;
                    lstAPActionBE.Add(oAPActionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstAPActionBE;
        }

        public int? SaveAutoEmailPOToVendorStatusDAL(MASCNT_AutoEmailBE AutoEmailBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", AutoEmailBE.Action);
                param[index++] = new SqlParameter("@ModifiedByID", AutoEmailBE.ModifiedByID.UserID);
                param[index++] = new SqlParameter("@CountryID", AutoEmailBE.UP_VendorBE.CountryID);
                param[index++] = new SqlParameter("@IsAutoEmailNewPO", AutoEmailBE.IsAutoEmailNewPO);
                param[index++] = new SqlParameter("@ModifiedOn", AutoEmailBE.ModifiedOn);
                param[index++] = new SqlParameter("@VendorIDs", AutoEmailBE.UP_VendorBE.VendorIDs);

                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_AutoEmailPOToVendor", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

    }
}

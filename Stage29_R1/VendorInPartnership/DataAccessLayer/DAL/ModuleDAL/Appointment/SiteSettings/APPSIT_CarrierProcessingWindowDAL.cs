﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings
{
    public class APPSIT_CarrierProcessingWindowDAL : DataAccessLayer.BaseDAL
    {
        public List<MASSIT_CarrierProcessingWindowBE> GetCarrierDetailsDAL(MASSIT_CarrierProcessingWindowBE oMASSIT_CarrierProcessingWindowBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_CarrierProcessingWindowBE> oMASSIT_CarrierProcessingWindowBEList = new List<MASSIT_CarrierProcessingWindowBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oMASSIT_CarrierProcessingWindowBE.Action);
                param[index++] = new SqlParameter("@SiteCarrierID", oMASSIT_CarrierProcessingWindowBE.SiteCarrierID);
                param[index++] = new SqlParameter("@CarrierID", oMASSIT_CarrierProcessingWindowBE.CarrierID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_CarrierProcessingWindowBE.SiteID);
                param[index++] = new SqlParameter("@APP_PalletsUnloadedPerHour", oMASSIT_CarrierProcessingWindowBE.APP_PalletsUnloadedPerHour);

                param[index++] = new SqlParameter("@APP_ReceiptingLinesPerHour", oMASSIT_CarrierProcessingWindowBE.APP_ReceiptingLinesPerHour);
                param[index++] = new SqlParameter("@UserID", oMASSIT_CarrierProcessingWindowBE.User.UserID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_CarrierProcessing", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_CarrierProcessingWindowBE oNewMASSIT_CarrierProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
                    oNewMASSIT_CarrierProcessingWindowBE.SiteCarrierID = Convert.ToInt32(dr["SiteCarrierID"]);
                    oNewMASSIT_CarrierProcessingWindowBE.CarrierID = Convert.ToInt32(dr["CarrierID"]);
                    oNewMASSIT_CarrierProcessingWindowBE.SiteID = Convert.ToInt32(dr["SiteID"]);

                    oNewMASSIT_CarrierProcessingWindowBE.APP_PalletsUnloadedPerHour = dr["APP_PalletsUnloadedPerHour"] != DBNull.Value ? Convert.ToInt32(dr["APP_PalletsUnloadedPerHour"]) : 0;
                    oNewMASSIT_CarrierProcessingWindowBE.APP_CartonsUnloadedPerHour = dr["APP_CartonsUnloadedPerHour"] != DBNull.Value ? Convert.ToInt32(dr["APP_CartonsUnloadedPerHour"]) : 0;
                    oNewMASSIT_CarrierProcessingWindowBE.APP_ReceiptingLinesPerHour = dr["APP_ReceiptingLinesPerHour"] != DBNull.Value ? Convert.ToInt32(dr["APP_ReceiptingLinesPerHour"]) : 0;
                    oNewMASSIT_CarrierProcessingWindowBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();

                    oNewMASSIT_CarrierProcessingWindowBE.Site.SiteName = dr["SiteDescription"].ToString();

                    oNewMASSIT_CarrierProcessingWindowBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    oNewMASSIT_CarrierProcessingWindowBE.Carrier.CarrierName = dr["CarrierName"].ToString();

                    oMASSIT_CarrierProcessingWindowBEList.Add(oNewMASSIT_CarrierProcessingWindowBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_CarrierProcessingWindowBEList;
        }


        public DataTable GetCarrierDetailsDAL(MASSIT_CarrierProcessingWindowBE oMASSIT_CarrierProcessingWindowBE, string Nothing = "")
        {
            DataTable dt = new DataTable();
            List<MASSIT_CarrierProcessingWindowBE> oMASSIT_CarrierProcessingWindowBEList = new List<MASSIT_CarrierProcessingWindowBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oMASSIT_CarrierProcessingWindowBE.Action);
                param[index++] = new SqlParameter("@SiteCarrierID", oMASSIT_CarrierProcessingWindowBE.SiteCarrierID);
                param[index++] = new SqlParameter("@CarrierID", oMASSIT_CarrierProcessingWindowBE.CarrierID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_CarrierProcessingWindowBE.SiteID);

                param[index++] = new SqlParameter("@APP_PalletsUnloadedPerHour", oMASSIT_CarrierProcessingWindowBE.APP_PalletsUnloadedPerHour);

                param[index++] = new SqlParameter("@APP_ReceiptingLinesPerHour", oMASSIT_CarrierProcessingWindowBE.APP_ReceiptingLinesPerHour);



                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_CarrierProcessing", param);

                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public int? addEditCarrierDetailsDAL(MASSIT_CarrierProcessingWindowBE oMASSIT_CarrierProcessingWindowBE)
        {
            int? intResult = 0;
            List<MASSIT_CarrierProcessingWindowBE> oMASSIT_CarrierProcessingWindowBEList = new List<MASSIT_CarrierProcessingWindowBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", oMASSIT_CarrierProcessingWindowBE.Action);
                param[index++] = new SqlParameter("@SiteCarrierID", oMASSIT_CarrierProcessingWindowBE.SiteCarrierID);
                param[index++] = new SqlParameter("@CarrierID", oMASSIT_CarrierProcessingWindowBE.CarrierID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_CarrierProcessingWindowBE.SiteID);

                param[index++] = new SqlParameter("@APP_PalletsUnloadedPerHour", oMASSIT_CarrierProcessingWindowBE.APP_PalletsUnloadedPerHour);
                param[index++] = new SqlParameter("@APP_CartonsUnloadedPerHour", oMASSIT_CarrierProcessingWindowBE.APP_CartonsUnloadedPerHour);
                param[index++] = new SqlParameter("@APP_ReceiptingLinesPerHour", oMASSIT_CarrierProcessingWindowBE.APP_ReceiptingLinesPerHour);


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_CarrierProcessing", param);


                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return intResult;

        }


        public List<MASSIT_CarrierProcessingWindowBE> GetConstraintsDAL(MASSIT_CarrierProcessingWindowBE oMASSIT_CarrierProcessingWindowBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_CarrierProcessingWindowBE> oMASSIT_CarrierProcessingWindowBEList = new List<MASSIT_CarrierProcessingWindowBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oMASSIT_CarrierProcessingWindowBE.Action);
                param[index++] = new SqlParameter("@CarrierID", oMASSIT_CarrierProcessingWindowBE.CarrierID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_CarrierProcessingWindowBE.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_CarrierProcessing", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_CarrierProcessingWindowBE oNewMASSIT_CarrierProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();


                    oNewMASSIT_CarrierProcessingWindowBE.APP_PalletsUnloadedPerHour = dr["APP_PalletsUnloadedPerHour"] != DBNull.Value ? Convert.ToInt32(dr["APP_PalletsUnloadedPerHour"]) : 0;
                    oNewMASSIT_CarrierProcessingWindowBE.APP_CartonsUnloadedPerHour = dr["APP_CartonsUnloadedPerHour"] != DBNull.Value ? Convert.ToInt32(dr["APP_CartonsUnloadedPerHour"]) : 0;
                    oNewMASSIT_CarrierProcessingWindowBE.APP_ReceiptingLinesPerHour = dr["APP_ReceiptingLinesPerHour"] != DBNull.Value ? Convert.ToInt32(dr["APP_ReceiptingLinesPerHour"]) : 0;

                    oMASSIT_CarrierProcessingWindowBEList.Add(oNewMASSIT_CarrierProcessingWindowBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_CarrierProcessingWindowBEList;
        }


    }
}

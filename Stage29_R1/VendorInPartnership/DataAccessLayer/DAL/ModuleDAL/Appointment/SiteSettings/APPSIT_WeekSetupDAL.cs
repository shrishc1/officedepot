﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings
{
    public class APPSIT_WeekSetupDAL : BaseDAL
    {
        public List<MASSIT_WeekSetupBE> GetWeekSetupDetailsDAL(MASSIT_WeekSetupBE oMASSIT_WeekSetupBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_WeekSetupBE> oMASSIT_WeekSetupBEList = new List<MASSIT_WeekSetupBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oMASSIT_WeekSetupBE.Action);
                param[index++] = new SqlParameter("@SiteScheduleDiaryID", oMASSIT_WeekSetupBE.SiteScheduleDiaryID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_WeekSetupBE.SiteID);
                param[index++] = new SqlParameter("@EndWeekday", oMASSIT_WeekSetupBE.EndWeekday);
                param[index++] = new SqlParameter("@DayDisabledFor", oMASSIT_WeekSetupBE.DayDisabledFor);
                param[index++] = new SqlParameter("@StartWeekday", oMASSIT_WeekSetupBE.StartWeekday);
                param[index++] = new SqlParameter("@ScheduleDate", oMASSIT_WeekSetupBE.ScheduleDate);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_WeekSetup", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_WeekSetupBE oNewMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
                    oNewMASSIT_WeekSetupBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMASSIT_WeekSetupBE.SiteScheduleDiaryID = Convert.ToInt32(dr["SiteScheduleDiaryID"]);
                    oNewMASSIT_WeekSetupBE.StartWeekday = dr["StartWeekday"] != DBNull.Value ? dr["StartWeekday"].ToString() : "";
                    oNewMASSIT_WeekSetupBE.StartTime = dr["StartTime"] != DBNull.Value ? Convert.ToDateTime(dr["StartTime"]) : (DateTime?)null;
                    oNewMASSIT_WeekSetupBE.EndWeekday = dr["EndWeekday"] != DBNull.Value ? dr["EndWeekday"].ToString() : "";
                    oNewMASSIT_WeekSetupBE.EndTime = dr["EndTime"] != DBNull.Value ? Convert.ToDateTime(dr["EndTime"]) : (DateTime?)null;
                    oNewMASSIT_WeekSetupBE.MaximumLift = dr["MaximumLift"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLift"].ToString()) : (int?)null;
                    oNewMASSIT_WeekSetupBE.MaximumPallet = dr["MaximumPallet"] != DBNull.Value ? Convert.ToInt32(dr["MaximumPallet"].ToString()) : (int?)null;
                    oNewMASSIT_WeekSetupBE.MaximumContainer = dr["MaximumContainer"] != DBNull.Value ? Convert.ToInt32(dr["MaximumContainer"].ToString()) : (int?)null;
                    oNewMASSIT_WeekSetupBE.MaximumLine = dr["MaximumLine"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLine"].ToString()) : (int?)null;
                    oNewMASSIT_WeekSetupBE.IsDayDisabled = dr["IsDayDisabled"] != DBNull.Value ? (Convert.ToString(dr["IsDayDisabled"]).ToLower() == "true" ? true : false) : false;

                    oNewMASSIT_WeekSetupBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewMASSIT_WeekSetupBE.Site.SiteName = dr["SiteName"].ToString();

                    if (dt.Columns.Contains("WeekSetupSpecificID"))
                        oNewMASSIT_WeekSetupBE.WeekSetupSpecificID = dr["WeekSetupSpecificID"] != DBNull.Value ? Convert.ToInt32(dr["WeekSetupSpecificID"].ToString()) : (int?)null;

                    //Stage 6 Point 19
                    if (dt.Columns.Contains("MaximumDeliveries"))
                        oNewMASSIT_WeekSetupBE.MaximumDeliveries = dr["MaximumDeliveries"] != DBNull.Value ? Convert.ToInt32(dr["MaximumDeliveries"].ToString()) : (int?)null;

                    oMASSIT_WeekSetupBEList.Add(oNewMASSIT_WeekSetupBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_WeekSetupBEList;
        }

        public int? addEditWeekSetupDAL(MASSIT_WeekSetupBE oMASSIT_WeekSetupBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[15];
                param[index++] = new SqlParameter("@Action", oMASSIT_WeekSetupBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_WeekSetupBE.SiteID);
                param[index++] = new SqlParameter("@SiteScheduleDiaryID", oMASSIT_WeekSetupBE.SiteScheduleDiaryID);
                param[index++] = new SqlParameter("@StartWeekday", oMASSIT_WeekSetupBE.StartWeekday);
                param[index++] = new SqlParameter("@StartTime", oMASSIT_WeekSetupBE.StartTime);
                param[index++] = new SqlParameter("@EndWeekday", oMASSIT_WeekSetupBE.EndWeekday);
                param[index++] = new SqlParameter("@EndTime", oMASSIT_WeekSetupBE.EndTime);
                param[index++] = new SqlParameter("@MaximumLift", oMASSIT_WeekSetupBE.MaximumLift);
                param[index++] = new SqlParameter("@MaximumPallet", oMASSIT_WeekSetupBE.MaximumPallet);
                param[index++] = new SqlParameter("@MaximumContainer", oMASSIT_WeekSetupBE.MaximumContainer);
                param[index++] = new SqlParameter("@MaximumLine", oMASSIT_WeekSetupBE.MaximumLine);
                param[index++] = new SqlParameter("@DayDisabledFor", oMASSIT_WeekSetupBE.DayDisabledFor);
                param[index++] = new SqlParameter("@StartSlotTimeID", oMASSIT_WeekSetupBE.StartSlotTimeID);
                param[index++] = new SqlParameter("@EndSlotTimeID", oMASSIT_WeekSetupBE.EndSlotTimeID);
                //Stage 6 Point 19
                param[index++] = new SqlParameter("@MaximumDeliveries", oMASSIT_WeekSetupBE.MaximumDeliveries);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_WeekSetup", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public int? openAllDoorDAL(MASSIT_WeekSetupBE oMASSIT_WeekSetupBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASSIT_WeekSetupBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_WeekSetupBE.SiteID);
                param[index++] = new SqlParameter("@StartWeekday", oMASSIT_WeekSetupBE.StartWeekday);
                param[index++] = new SqlParameter("@EndWeekday", oMASSIT_WeekSetupBE.EndWeekday);
                param[index++] = new SqlParameter("@StartSlotTimeID", oMASSIT_WeekSetupBE.StartSlotTimeID);
                param[index++] = new SqlParameter("@EndSlotTimeID", oMASSIT_WeekSetupBE.EndSlotTimeID);

                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_DoorOpenTime", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MASSIT_WeekSetupBE> GetTotalsDAL(MASSIT_WeekSetupBE oMASSIT_WeekSetupBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_WeekSetupBE> oMASSIT_WeekSetupBEList = new List<MASSIT_WeekSetupBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", "GetTotals");
                param[index++] = new SqlParameter("@SiteID", oMASSIT_WeekSetupBE.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_WeekSetup", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_WeekSetupBE oNewMASSIT_WeekSetupBE = new MASSIT_WeekSetupBE();
                    oNewMASSIT_WeekSetupBE.TOTAL_MaximumLift = Convert.ToInt32(dr["TOTAL_MaximumLift"]);
                    oNewMASSIT_WeekSetupBE.TOTAL_MaximumPallet = Convert.ToInt32(dr["TOTAL_MaximumPallet"]);
                    oNewMASSIT_WeekSetupBE.TOTAL_MaximumContainer = Convert.ToInt32(dr["TOTAL_MaximumContainer"]);
                    oNewMASSIT_WeekSetupBE.TOTAL_MaximumLine = Convert.ToInt32(dr["TOTAL_MaximumLine"]);
                    oMASSIT_WeekSetupBEList.Add(oNewMASSIT_WeekSetupBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_WeekSetupBEList;
        }
    }
}

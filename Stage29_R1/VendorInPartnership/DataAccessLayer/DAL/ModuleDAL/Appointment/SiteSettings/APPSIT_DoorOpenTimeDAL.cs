﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings
{
    public class APPSIT_DoorOpenTimeDAL
    {

        public List<MASSIT_DoorOpenTimeBE> GetDoorNoAndTypeDAL(MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_DoorOpenTimeBE> oMASSIT_DoorOpenTimeBEList = new List<MASSIT_DoorOpenTimeBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_DoorOpenTimeBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DoorOpenTimeBE.DoorNo.SiteID);
                param[index++] = new SqlParameter("@PurchaseNumbers", oMASSIT_DoorOpenTimeBE.PurchaseNumbers);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_DoorOpenTime", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_DoorOpenTimeBE oNewMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();
                    oNewMASSIT_DoorOpenTimeBE.SiteDoorNumberID = Convert.ToInt32(dr["SiteDoorNumberID"]);

                    oNewMASSIT_DoorOpenTimeBE.DoorNo = new MASSIT_DoorNoSetupBE();
                    oNewMASSIT_DoorOpenTimeBE.DoorNo.DoorNumber = Convert.ToString(dr["DoorNumber"]);
                    oNewMASSIT_DoorOpenTimeBE.DoorNo.SiteDoorTypeID = dr["SiteDoorTypeID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorTypeID"]) : 0;

                    oNewMASSIT_DoorOpenTimeBE.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
                    oNewMASSIT_DoorOpenTimeBE.DoorType.DoorType = dr["DoorType"].ToString();
                    oNewMASSIT_DoorOpenTimeBE.DoorType.DoorTypeID = Convert.ToInt32(dr["DoorTypeID"]);


                    oMASSIT_DoorOpenTimeBEList.Add(oNewMASSIT_DoorOpenTimeBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_DoorOpenTimeBEList;
        }
        public List<MASSIT_DoorOpenTimeBE> GetDoorOpenTimeDAL(MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_DoorOpenTimeBE> oMASSIT_DoorOpenTimeBEList = new List<MASSIT_DoorOpenTimeBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMASSIT_DoorOpenTimeBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DoorOpenTimeBE.DoorNo.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_DoorOpenTime", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_DoorOpenTimeBE oNewMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();
                    oNewMASSIT_DoorOpenTimeBE.SiteDoorNumberID = Convert.ToInt32(dr["SiteDoorNumberID"]);
                    oNewMASSIT_DoorOpenTimeBE.Weekday = dr["StartWeekday"].ToString();
                    oNewMASSIT_DoorOpenTimeBE.SlotTimeID = Convert.ToInt32(dr["SlotTimeID"]);
                    oMASSIT_DoorOpenTimeBEList.Add(oNewMASSIT_DoorOpenTimeBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_DoorOpenTimeBEList;
        }

        public List<MASSIT_DoorOpenTimeBE> GetDoorConstraintsDAL(MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_DoorOpenTimeBE> oMASSIT_DoorOpenTimeBEList = new List<MASSIT_DoorOpenTimeBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_DoorOpenTimeBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DoorOpenTimeBE.DoorNo.SiteID);
                param[index++] = new SqlParameter("@Weekday", oMASSIT_DoorOpenTimeBE.Weekday);
                param[index++] = new SqlParameter("@StartWeekDay", oMASSIT_DoorOpenTimeBE.StartWeekday);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_DoorOpenTime", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_DoorOpenTimeBE oNewMASSIT_DoorOpenTimeBE = new MASSIT_DoorOpenTimeBE();
                    oNewMASSIT_DoorOpenTimeBE.SiteDoorNumberID = Convert.ToInt32(dr["SiteDoorNumberID"]);
                    oNewMASSIT_DoorOpenTimeBE.Weekday = dr["Weekday"].ToString();
                    oNewMASSIT_DoorOpenTimeBE.StartWeekday = dr["StartWeekday"].ToString();
                    oNewMASSIT_DoorOpenTimeBE.SlotTimeID = Convert.ToInt32(dr["SlotTimeID"]);
                    oNewMASSIT_DoorOpenTimeBE.DoorConstraintSpecificID = dr["DoorConstraintSpecificID"] != DBNull.Value ? Convert.ToInt32(dr["DoorConstraintSpecificID"]) : (int?)null;
                    oMASSIT_DoorOpenTimeBEList.Add(oNewMASSIT_DoorOpenTimeBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_DoorOpenTimeBEList;
        }

        public int? AddEditDoorConstraintsDAL(MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oMASSIT_DoorOpenTimeBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DoorOpenTimeBE.DoorNo.SiteID);
                param[index++] = new SqlParameter("@DoorConstraintIDs", oMASSIT_DoorOpenTimeBE.DoorConstraintIDs);
                param[index++] = new SqlParameter("@DoorConstraintIDsForEndDay", oMASSIT_DoorOpenTimeBE.DoorConstraintIDsForEndDay);
                param[index++] = new SqlParameter("@Weekday", oMASSIT_DoorOpenTimeBE.Weekday);
                param[index++] = new SqlParameter("@StartWeekDay", oMASSIT_DoorOpenTimeBE.StartWeekday);
                param[index++] = new SqlParameter("@EndWeekDay", oMASSIT_DoorOpenTimeBE.EndWeekday);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_DoorOpenTime", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

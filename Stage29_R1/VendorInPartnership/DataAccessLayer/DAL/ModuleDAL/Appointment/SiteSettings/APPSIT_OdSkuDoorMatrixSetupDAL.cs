﻿// -----------------------------------------------------------------------
// <copyright file="APPSIT_OdSkuDoorMatrixSetupDAL.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings
{
    using BusinessEntities.ModuleBE.AdminFunctions;
    using BusinessEntities.ModuleBE.Appointment.SiteSettings;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Utilities;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class APPSIT_OdSkuDoorMatrixSetupDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oMASSIT_ODSKUDoorMatrixSetupBE">MASSIT_ODSKUDoorMatrixSetupBE</param>
        /// <returns>List[MASSIT_ODSKUDoorMatrixSetupBE]</returns>
        public List<MASSIT_ODSKUDoorMatrixSetupBE> GetODSKUDoorMatrixSetupDetailsDAL(MASSIT_ODSKUDoorMatrixSetupBE oMASSIT_ODSKUDoorMatrixSetupBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_ODSKUDoorMatrixSetupBE> oMASSIT_ODSKUDoorMatrixSetupBEList = new List<MASSIT_ODSKUDoorMatrixSetupBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oMASSIT_ODSKUDoorMatrixSetupBE.Action);
                //param[index++] = new SqlParameter("@SkuDoorMatrixID", oMASSIT_ODSKUDoorMatrixSetupBE.SkuDoorMatrixID);
                if (oMASSIT_ODSKUDoorMatrixSetupBE.Action.ToUpper() == "SHOW_BY_ID")
                    param[index++] = new SqlParameter("@SKUDoorMatrixID", oMASSIT_ODSKUDoorMatrixSetupBE.SkuDoorMatrixID);
                else
                    param[index++] = new SqlParameter("@SiteID", oMASSIT_ODSKUDoorMatrixSetupBE.Site.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_OdSkuDoorMatrix", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    oMASSIT_ODSKUDoorMatrixSetupBE = new MASSIT_ODSKUDoorMatrixSetupBE();
                    oMASSIT_ODSKUDoorMatrixSetupBE.SkuDoorMatrixID = Convert.ToInt32(dr["SkuDoorMatrixID"]);

                    MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE = new MASSIT_DoorNoSetupBE();

                    oMASSIT_DoorNoSetupBE.DoorNumber = Convert.ToString(dr["DoorNumber"]);
                    oMASSIT_DoorNoSetupBE.SiteDoorNumberID = Convert.ToInt32(dr["SiteDoorNumberID"]);
                    oMASSIT_ODSKUDoorMatrixSetupBE.DoorNumber = oMASSIT_DoorNoSetupBE;

                    oMASSIT_ODSKUDoorMatrixSetupBE.ODSkuNumber = Convert.ToString(dr["ODSkuNumber"]);

                    MAS_SiteBE oMAS_SiteBE = new MAS_SiteBE();
                    oMAS_SiteBE.SiteID = Convert.ToInt32(dr["SiteID"]);

                    oMASSIT_ODSKUDoorMatrixSetupBE.Site = oMAS_SiteBE;
                    oMASSIT_ODSKUDoorMatrixSetupBEList.Add(oMASSIT_ODSKUDoorMatrixSetupBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_ODSKUDoorMatrixSetupBEList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oMASSIT_ODSKUDoorMatrixSetupBE">MASSIT_ODSKUDoorMatrixSetupBE</param>
        /// <returns>int</returns>
        public int? addEditODSKUDoorMatrixSetupDetailsDAL(MASSIT_ODSKUDoorMatrixSetupBE oMASSIT_ODSKUDoorMatrixSetupBE)
        {
            int? intResult = 0;
            List<MASSIT_DoorNoSetupBE> oMASSIT_DoorNoSetupBEList = new List<MASSIT_DoorNoSetupBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oMASSIT_ODSKUDoorMatrixSetupBE.Action);
                if (oMASSIT_ODSKUDoorMatrixSetupBE.Action.ToUpper() == "DELETE")
                    param[index++] = new SqlParameter("@SKUDoorMatrixID", oMASSIT_ODSKUDoorMatrixSetupBE.SkuDoorMatrixID);
                else
                {
                    param[index++] = new SqlParameter("@SKUDoorMatrixID", oMASSIT_ODSKUDoorMatrixSetupBE.SkuDoorMatrixID);
                    param[index++] = new SqlParameter("@SiteID", oMASSIT_ODSKUDoorMatrixSetupBE.Site.SiteID);
                    param[index++] = new SqlParameter("@OdSkuNumber", oMASSIT_ODSKUDoorMatrixSetupBE.ODSkuNumber);
                    param[index++] = new SqlParameter("@SiteDoorNumberID", oMASSIT_ODSKUDoorMatrixSetupBE.DoorNumber.SiteDoorNumberID);
                }
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_OdSkuDoorMatrix", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                intResult = -1;
                LogUtility.SaveErrorLogEntry(ex);
            }

            return intResult;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oMASSIT_ODSKUDoorMatrixSetupBE"></param>
        /// <returns></returns>
        public List<MASSIT_DoorNoSetupBE> GetDoorNameDAL(MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_DoorNoSetupBE> oMASSIT_DoorNoSetupBEList = new List<MASSIT_DoorNoSetupBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMASSIT_DoorNoSetupBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DoorNoSetupBE.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_OdSkuDoorMatrix", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_DoorNoSetupBE oNewMASSIT_DoorNoSetupBE = new MASSIT_DoorNoSetupBE();
                    oNewMASSIT_DoorNoSetupBE.SiteDoorNumberID = Convert.ToInt32(dr["SiteDoorNumberID"]);
                    oNewMASSIT_DoorNoSetupBE.DoorNumber = Convert.ToString(dr["DoorNumber"]);
                    oMASSIT_DoorNoSetupBEList.Add(oNewMASSIT_DoorNoSetupBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_DoorNoSetupBEList;
        }


    }
}

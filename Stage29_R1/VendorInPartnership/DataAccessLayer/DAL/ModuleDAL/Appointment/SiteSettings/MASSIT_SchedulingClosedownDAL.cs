﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings
{
    public class MASSIT_SchedulingClosedownDAL
    {

        public List<MASSIT_SchedulingClosedownBE> GetSchedulingClosedownDAL(MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_SchedulingClosedownBE> oMASSIT_SchedulingClosedownBEList = new List<MASSIT_SchedulingClosedownBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMASSIT_SchedulingClosedownBE.Action);
                param[index++] = new SqlParameter("@SchedulingClosedownID", oMASSIT_SchedulingClosedownBE.SchedulingClosedownID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_SchedulingClosedown", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_SchedulingClosedownBE oNewMASSIT_SchedulingClosedownBE = new MASSIT_SchedulingClosedownBE();
                    oNewMASSIT_SchedulingClosedownBE.SchedulingClosedownID = dr["SchedulingClosedownID"] != DBNull.Value ? Convert.ToInt32(dr["SchedulingClosedownID"].ToString()) : 0;
                    oNewMASSIT_SchedulingClosedownBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"].ToString()) : 0;
                    oNewMASSIT_SchedulingClosedownBE.SiteName = dr["SiteName"] != DBNull.Value ? dr["SiteName"].ToString() : string.Empty;
                    oNewMASSIT_SchedulingClosedownBE.WarningMessage = dr["WarningMessage"] != DBNull.Value ? dr["WarningMessage"].ToString() : string.Empty;
                    oNewMASSIT_SchedulingClosedownBE.CreateDate = dr["CreateDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreateDate"].ToString()) : (DateTime?)null;
                    oNewMASSIT_SchedulingClosedownBE.CreateBy = dr["CreateBy"] != DBNull.Value ? dr["CreateBy"].ToString() : string.Empty;
                    oMASSIT_SchedulingClosedownBEList.Add(oNewMASSIT_SchedulingClosedownBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_SchedulingClosedownBEList;
        }


        public int? AddEditSchedulingClosedownDAL(MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oMASSIT_SchedulingClosedownBE.Action);
                param[index++] = new SqlParameter("@SchedulingClosedownID", oMASSIT_SchedulingClosedownBE.SchedulingClosedownID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_SchedulingClosedownBE.SiteID);
                param[index++] = new SqlParameter("@WarningMessage", oMASSIT_SchedulingClosedownBE.WarningMessage);
                param[index++] = new SqlParameter("@CreateById", oMASSIT_SchedulingClosedownBE.CreateById);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_SchedulingClosedown", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings
{
    public class APPSIT_VehicleTypeDAL
    {
        public List<MASSIT_VehicleTypeBE> GetVehicleTypeDetailsDAL(MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_VehicleTypeBE> oMASSIT_VehicleTypeBEList = new List<MASSIT_VehicleTypeBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", oMASSIT_VehicleTypeBE.Action);
                param[index++] = new SqlParameter("@VehicleTypeID", oMASSIT_VehicleTypeBE.VehicleTypeID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_VehicleTypeBE.SiteID);
                param[index++] = new SqlParameter("@VehicleType", oMASSIT_VehicleTypeBE.VehicleType);
                param[index++] = new SqlParameter("@APP_PalletsUnloadedPerHour", oMASSIT_VehicleTypeBE.APP_PalletsUnloadedPerHour);
                param[index++] = new SqlParameter("@APP_CartonsUnloadedPerHour", oMASSIT_VehicleTypeBE.APP_CartonsUnloadedPerHour);
                param[index++] = new SqlParameter("@TotalUnloadTimeInMinute", oMASSIT_VehicleTypeBE.TotalUnloadTimeInMinute);
                param[index++] = new SqlParameter("@IsActive", oMASSIT_VehicleTypeBE.IsActive);
                //oMASSIT_VehicleTypeBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                if (oMASSIT_VehicleTypeBE.User != null)
                    param[index++] = new SqlParameter("@UserID", oMASSIT_VehicleTypeBE.User.UserID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_VehicleType", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_VehicleTypeBE oNewMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
                    oNewMASSIT_VehicleTypeBE.VehicleTypeID = Convert.ToInt32(dr["VehicleTypeID"]);
                    oNewMASSIT_VehicleTypeBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMASSIT_VehicleTypeBE.VehicleType = dr["VehicleType"].ToString();
                    oNewMASSIT_VehicleTypeBE.APP_CartonsUnloadedPerHour = dr["APP_CartonsUnloadedPerHour"] != DBNull.Value ? Convert.ToInt32(dr["APP_CartonsUnloadedPerHour"].ToString()) : 0;
                    oNewMASSIT_VehicleTypeBE.APP_PalletsUnloadedPerHour = dr["APP_PalletsUnloadedPerHour"] != DBNull.Value ? Convert.ToInt32(dr["APP_PalletsUnloadedPerHour"].ToString()) : 0;
                    oNewMASSIT_VehicleTypeBE.TotalUnloadTimeInMinute = Convert.ToInt32(dr["TotalUnloadTimeInMinute"].ToString());
                    oNewMASSIT_VehicleTypeBE.TotalUnloadTimeString = Convert.ToInt32(Convert.ToInt32(dr["TotalUnloadTimeInMinute"].ToString()) / 60).ToString() + ":" + Convert.ToInt32(Convert.ToInt32(dr["TotalUnloadTimeInMinute"].ToString()) % 60).ToString();

                    oNewMASSIT_VehicleTypeBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewMASSIT_VehicleTypeBE.Site.SiteName = dr["SiteName"].ToString();
                    oNewMASSIT_VehicleTypeBE.IsNarrativeRequired = dr["IsNarrativeRequired"] != DBNull.Value ? Convert.ToBoolean(dr["IsNarrativeRequired"]) : false;
                    oNewMASSIT_VehicleTypeBE.IsContainer = dr["IsContainer"] != DBNull.Value ? Convert.ToBoolean(dr["IsContainer"]) : false;
                    if (dr.Table.Columns.Contains("MinimumTimePerDeliveryInMinute"))
                    {
                        oNewMASSIT_VehicleTypeBE.MinimumTimePerDeliveryInMinute = Convert.ToInt32(dr["MinimumTimePerDeliveryInMinute"].ToString());
                        oNewMASSIT_VehicleTypeBE.MinimumTimePerDeliveryString = Convert.ToInt32(Convert.ToInt32(dr["MinimumTimePerDeliveryInMinute"].ToString()) / 60).ToString() + ":" + Convert.ToInt32(Convert.ToInt32(dr["MinimumTimePerDeliveryInMinute"].ToString()) % 60).ToString();
                    }
                    oMASSIT_VehicleTypeBEList.Add(oNewMASSIT_VehicleTypeBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_VehicleTypeBEList;
        }
        public List<MASSIT_VehicleTypeBE> GetVehicleTypDAL(MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_VehicleTypeBE> oMASSIT_VehicleTypeBEList = new List<MASSIT_VehicleTypeBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMASSIT_VehicleTypeBE.Action);
                param[index++] = new SqlParameter("@UserID", oMASSIT_VehicleTypeBE.User.UserID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_VehicleType", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_VehicleTypeBE oNewMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
                    oNewMASSIT_VehicleTypeBE.VehicleTypeID = Convert.ToInt32(dr["VehicleTypeID"]);
                    oNewMASSIT_VehicleTypeBE.VehicleType = dr["VehicleType"].ToString();
                    oMASSIT_VehicleTypeBEList.Add(oNewMASSIT_VehicleTypeBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_VehicleTypeBEList;
        }

        public int? addEditVehicleTypeDetailsDAL(MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oMASSIT_VehicleTypeBE.Action);
                param[index++] = new SqlParameter("@VehicleTypeID", oMASSIT_VehicleTypeBE.VehicleTypeID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_VehicleTypeBE.SiteID);
                param[index++] = new SqlParameter("@VehicleType", oMASSIT_VehicleTypeBE.VehicleType);
                param[index++] = new SqlParameter("@APP_PalletsUnloadedPerHour", oMASSIT_VehicleTypeBE.APP_PalletsUnloadedPerHour);
                param[index++] = new SqlParameter("@APP_CartonsUnloadedPerHour", oMASSIT_VehicleTypeBE.APP_CartonsUnloadedPerHour);
                param[index++] = new SqlParameter("@TotalUnloadTimeInMinute", oMASSIT_VehicleTypeBE.TotalUnloadTimeInMinute);
                param[index++] = new SqlParameter("@IsActive", oMASSIT_VehicleTypeBE.IsActive);
                param[index++] = new SqlParameter("@IsNarrativeRequired", oMASSIT_VehicleTypeBE.IsNarrativeRequired);
                param[index++] = new SqlParameter("@IsContainer", oMASSIT_VehicleTypeBE.IsContainer);
                param[index++] = new SqlParameter("@MinimumTimePerDeliveryInMinute", oMASSIT_VehicleTypeBE.MinimumTimePerDeliveryInMinute);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_VehicleType", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings
{
    public class APPSIT_FixedSlotDAL
    {


        public List<MASSIT_FixedSlotBE> GetAllFixedSlotDAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_FixedSlotBE> oMASSIT_FixedSlotBEList = new List<MASSIT_FixedSlotBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_FixedSlotBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_FixedSlotBE.SiteID);
                param[index++] = new SqlParameter("@FixedSlotID", oMASSIT_FixedSlotBE.FixedSlotID);
                if (oMASSIT_FixedSlotBE.User != null)
                    param[index++] = new SqlParameter("@UserID", oMASSIT_FixedSlotBE.User.UserID);
                param[index++] = new SqlParameter("@SlotTimeID", oMASSIT_FixedSlotBE.SlotTimeID);
                param[index++] = new SqlParameter("@SiteDoorNumberID", oMASSIT_FixedSlotBE.SiteDoorNumberID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_FixedSlot", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_FixedSlotBE oNewMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();

                    oNewMASSIT_FixedSlotBE.FixedSlotID = Convert.ToInt32(dr["FixedSlotID"]);
                    oNewMASSIT_FixedSlotBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMASSIT_FixedSlotBE.AllocationType = Convert.ToString(dr["AllocationType"]);
                    oNewMASSIT_FixedSlotBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewMASSIT_FixedSlotBE.Vendor.VendorName = dr["VendorName"].ToString();
                    oNewMASSIT_FixedSlotBE.Vendor.Vendor_No = dr["Vendor_No"].ToString();
                    oNewMASSIT_FixedSlotBE.Vendor.ParentVendorID = dr["ParentVendorID"] != DBNull.Value ? Convert.ToInt32(dr["ParentVendorID"]) : -1;
                    oNewMASSIT_FixedSlotBE.CarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.ScheduleType = dr["ScheduleType"].ToString();
                    oNewMASSIT_FixedSlotBE.SlotTimeID = dr["SlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["SlotTimeID"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    oNewMASSIT_FixedSlotBE.SlotTime.SlotTime = dr["SlotTime"].ToString();
                    oNewMASSIT_FixedSlotBE.DoorNo = new MASSIT_DoorNoSetupBE();
                    oNewMASSIT_FixedSlotBE.DoorNo.DoorNumber = Convert.ToString(dr["DoorNumber"]);
                    oNewMASSIT_FixedSlotBE.SiteDoorNumberID = dr["SiteDoorNumberID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorNumberID"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.Monday = dr["Monday"] != DBNull.Value ? Convert.ToBoolean(dr["Monday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Tuesday = dr["Tuesday"] != DBNull.Value ? Convert.ToBoolean(dr["Tuesday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Wednesday = dr["Wednesday"] != DBNull.Value ? Convert.ToBoolean(dr["Wednesday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Thursday = dr["Thursday"] != DBNull.Value ? Convert.ToBoolean(dr["Thursday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Friday = dr["Friday"] != DBNull.Value ? Convert.ToBoolean(dr["Friday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Saturday = dr["Saturday"] != DBNull.Value ? Convert.ToBoolean(dr["Saturday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Sunday = dr["Sunday"] != DBNull.Value ? Convert.ToBoolean(dr["Sunday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.MaximumCatrons = dr["MaximumCatrons"] != DBNull.Value ? Convert.ToInt32(dr["MaximumCatrons"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.MaximumLines = dr["MaximumLines"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLines"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.MaximumPallets = dr["MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["MaximumPallets"]) : (int?)null;

                    oMASSIT_FixedSlotBEList.Add(oNewMASSIT_FixedSlotBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_FixedSlotBEList;
        }
        public List<MASSIT_FixedSlotBE> GetMaxDeliveriesPerDayDAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_FixedSlotBE> oMASSIT_FixedSlotBEList = new List<MASSIT_FixedSlotBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMASSIT_FixedSlotBE.Action);
                param[index++] = new SqlParameter("@VendorID", oMASSIT_FixedSlotBE.VendorID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_FixedSlot", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_FixedSlotBE oNewMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();

                    oNewMASSIT_FixedSlotBE.Monday = dr["Monday"] != DBNull.Value ? Convert.ToBoolean(dr["Monday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Tuesday = dr["Tuesday"] != DBNull.Value ? Convert.ToBoolean(dr["Tuesday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Wednesday = dr["Wednesday"] != DBNull.Value ? Convert.ToBoolean(dr["Wednesday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Thursday = dr["Thursday"] != DBNull.Value ? Convert.ToBoolean(dr["Thursday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Friday = dr["Friday"] != DBNull.Value ? Convert.ToBoolean(dr["Friday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Saturday = dr["Saturday"] != DBNull.Value ? Convert.ToBoolean(dr["Saturday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Sunday = dr["Sunday"] != DBNull.Value ? Convert.ToBoolean(dr["Sunday"]) : (bool?)null;
                    oMASSIT_FixedSlotBEList.Add(oNewMASSIT_FixedSlotBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_FixedSlotBEList;
        }

        public List<MASSIT_FixedSlotBE> GetAllFixedSlotByVendorIDDAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_FixedSlotBE> oMASSIT_FixedSlotBEList = new List<MASSIT_FixedSlotBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oMASSIT_FixedSlotBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_FixedSlotBE.SiteID);
                param[index++] = new SqlParameter("@FixedSlotID", oMASSIT_FixedSlotBE.FixedSlotID);
                param[index++] = new SqlParameter("@UserID", oMASSIT_FixedSlotBE.User.UserID);
                param[index++] = new SqlParameter("@VendorID", oMASSIT_FixedSlotBE.VendorID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_FixedSlot", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_FixedSlotBE oNewMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();

                    oNewMASSIT_FixedSlotBE.FixedSlotID = Convert.ToInt32(dr["FixedSlotID"]);
                    oNewMASSIT_FixedSlotBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMASSIT_FixedSlotBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oNewMASSIT_FixedSlotBE.CarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.SlotTimeID = dr["SlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["SlotTimeID"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.SlotTime = new BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE();
                    oNewMASSIT_FixedSlotBE.SlotTime.SlotTime = dr["SlotTime"].ToString();
                    oNewMASSIT_FixedSlotBE.Monday = dr["Monday"] != DBNull.Value ? Convert.ToBoolean(dr["Monday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Tuesday = dr["Tuesday"] != DBNull.Value ? Convert.ToBoolean(dr["Tuesday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Wednesday = dr["Wednesday"] != DBNull.Value ? Convert.ToBoolean(dr["Wednesday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Thursday = dr["Thursday"] != DBNull.Value ? Convert.ToBoolean(dr["Thursday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Friday = dr["Friday"] != DBNull.Value ? Convert.ToBoolean(dr["Friday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Saturday = dr["Saturday"] != DBNull.Value ? Convert.ToBoolean(dr["Saturday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.Sunday = dr["Sunday"] != DBNull.Value ? Convert.ToBoolean(dr["Sunday"]) : (bool?)null;
                    oNewMASSIT_FixedSlotBE.MaximumCatrons = dr["MaximumCatrons"] != DBNull.Value ? Convert.ToInt32(dr["MaximumCatrons"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.MaximumLines = dr["MaximumLines"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLines"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.MaximumPallets = dr["MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["MaximumPallets"]) : (int?)null;

                    oMASSIT_FixedSlotBEList.Add(oNewMASSIT_FixedSlotBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_FixedSlotBEList;
        }


        public List<MASSIT_FixedSlotBE> GetMaximumDAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_FixedSlotBE> oMASSIT_FixedSlotBEList = new List<MASSIT_FixedSlotBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oMASSIT_FixedSlotBE.Action);
                param[index++] = new SqlParameter("@WeekDay", oMASSIT_FixedSlotBE.WeekDay);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_FixedSlotBE.SiteID);
                param[index++] = new SqlParameter("@AllocationType", oMASSIT_FixedSlotBE.AllocationType);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_FixedSlot", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_FixedSlotBE oNewMASSIT_FixedSlotBE = new MASSIT_FixedSlotBE();


                    oNewMASSIT_FixedSlotBE.MaximumCatrons = dr["MaximumCatrons"] != DBNull.Value ? Convert.ToInt32(dr["MaximumCatrons"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.MaximumLines = dr["MaximumLines"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLines"]) : (int?)null;
                    oNewMASSIT_FixedSlotBE.MaximumPallets = dr["MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["MaximumPallets"]) : (int?)null;

                    oMASSIT_FixedSlotBEList.Add(oNewMASSIT_FixedSlotBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_FixedSlotBEList;
        }

        public int? addEdiFixedSlotDAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[19];
                param[index++] = new SqlParameter("@Action", oMASSIT_FixedSlotBE.Action);
                param[index++] = new SqlParameter("@FixedSlotID", oMASSIT_FixedSlotBE.FixedSlotID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_FixedSlotBE.SiteID);
                param[index++] = new SqlParameter("@VendorID", oMASSIT_FixedSlotBE.VendorID);
                param[index++] = new SqlParameter("@CarrierID", oMASSIT_FixedSlotBE.CarrierID);
                param[index++] = new SqlParameter("@ScheduleType", oMASSIT_FixedSlotBE.ScheduleType);
                param[index++] = new SqlParameter("@SlotTimeID", oMASSIT_FixedSlotBE.SlotTimeID);
                param[index++] = new SqlParameter("@SiteDoorNumberID", oMASSIT_FixedSlotBE.SiteDoorNumberID);
                param[index++] = new SqlParameter("@Monday", oMASSIT_FixedSlotBE.Monday);
                param[index++] = new SqlParameter("@Tuesday", oMASSIT_FixedSlotBE.Tuesday);
                param[index++] = new SqlParameter("@Wednesday", oMASSIT_FixedSlotBE.Wednesday);
                param[index++] = new SqlParameter("@Thursday", oMASSIT_FixedSlotBE.Thursday);
                param[index++] = new SqlParameter("@Friday", oMASSIT_FixedSlotBE.Friday);
                param[index++] = new SqlParameter("@Saturday", oMASSIT_FixedSlotBE.Saturday);
                param[index++] = new SqlParameter("@Sunday", oMASSIT_FixedSlotBE.Sunday);
                param[index++] = new SqlParameter("@MaximumCatrons", oMASSIT_FixedSlotBE.MaximumCatrons);
                param[index++] = new SqlParameter("@MaximumLines", oMASSIT_FixedSlotBE.MaximumLines);
                param[index++] = new SqlParameter("@MaximumPallets", oMASSIT_FixedSlotBE.MaximumPallets);
                param[index++] = new SqlParameter("@AllocationType", oMASSIT_FixedSlotBE.AllocationType);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_FixedSlot", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public DataSet getUnloadTimeDAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            DataSet dsResult = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASSIT_FixedSlotBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_FixedSlotBE.SiteID);
                param[index++] = new SqlParameter("@UserID", oMASSIT_FixedSlotBE.User.UserID);
                param[index++] = new SqlParameter("@VendorID", oMASSIT_FixedSlotBE.VendorID);
                param[index++] = new SqlParameter("@CarrierID", oMASSIT_FixedSlotBE.CarrierID);
                param[index++] = new SqlParameter("@MaximumCatrons", oMASSIT_FixedSlotBE.MaximumCatrons);
                param[index++] = new SqlParameter("@MaximumLines", oMASSIT_FixedSlotBE.MaximumLines);
                param[index++] = new SqlParameter("@MaximumPallets", oMASSIT_FixedSlotBE.MaximumPallets);

                dsResult = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_FixedSlot", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dsResult;
        }
    }
}

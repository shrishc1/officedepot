﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings
{
    public class APPSIT_BookingTypeExclusionDAL : BaseDAL
    {

        public List<MASSIT_BookingTypeExclusionBE> GetBookingTypeExclusionsDAL(MASSIT_BookingTypeExclusionBE oMASSIT_BookingTypeExclusionBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_BookingTypeExclusionBE> oMASSIT_BookingTypeExclusionBEList = new List<MASSIT_BookingTypeExclusionBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASSIT_BookingTypeExclusionBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_BookingTypeExclusionBE.SiteID);
                param[index++] = new SqlParameter("@SiteVendorID", oMASSIT_BookingTypeExclusionBE.SiteVendorID);
                param[index++] = new SqlParameter("@SiteCarrierID", oMASSIT_BookingTypeExclusionBE.SiteCarrierID);
                param[index++] = new SqlParameter("@SiteDeliveryID", oMASSIT_BookingTypeExclusionBE.SiteDeliveryID);
                param[index++] = new SqlParameter("@SiteVendorIDs", oMASSIT_BookingTypeExclusionBE.SiteVendorIDs);
                param[index++] = new SqlParameter("@SiteCarrierIDs", oMASSIT_BookingTypeExclusionBE.SiteCarrierIDs);
                param[index++] = new SqlParameter("@SiteDeliveryIDs", oMASSIT_BookingTypeExclusionBE.SiteDeliveryIDs);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_BookingValidationExclusion", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_BookingTypeExclusionBE oNewMASSIT_BookingTypeExclusionBE = new MASSIT_BookingTypeExclusionBE();

                    oNewMASSIT_BookingTypeExclusionBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMASSIT_BookingTypeExclusionBE.SiteVendorID = Convert.ToInt32(dr["SiteVendorID"].ToString());
                    oNewMASSIT_BookingTypeExclusionBE.SiteCarrierID = Convert.ToInt32(dr["SiteCarrierID"].ToString());
                    oNewMASSIT_BookingTypeExclusionBE.SiteDeliveryID = Convert.ToInt32(dr["SiteDeliveryID"].ToString());

                    oNewMASSIT_BookingTypeExclusionBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    oNewMASSIT_BookingTypeExclusionBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();

                    oNewMASSIT_BookingTypeExclusionBE.Carrier.CarrierName = dr["CarrierName"].ToString();
                    oNewMASSIT_BookingTypeExclusionBE.Delivery.DeliveryType = dr["DeliveryType"].ToString();

                    oNewMASSIT_BookingTypeExclusionBE.APP_IsBookingValidationExcluded = (dr["APP_IsBookingValidationExcluded"] != null && dr["APP_IsBookingValidationExcluded"].ToString() != "") ? Convert.ToBoolean(dr["APP_IsBookingValidationExcluded"].ToString()) : false;

                    oNewMASSIT_BookingTypeExclusionBE.Vendor = new UP_VendorBE();
                    oNewMASSIT_BookingTypeExclusionBE.Vendor.VendorName = dr["VENDOR"].ToString();
                    oNewMASSIT_BookingTypeExclusionBE.SiteVendorID = Convert.ToInt32(dr["SiteVendorID"].ToString());
                    oNewMASSIT_BookingTypeExclusionBE.Vendor_Country = Convert.ToInt32(dr["Vendor_Country"].ToString());

                    oMASSIT_BookingTypeExclusionBEList.Add(oNewMASSIT_BookingTypeExclusionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_BookingTypeExclusionBEList;
        }
        public DataTable GetBookingTypeExclusionsDAL(MASSIT_BookingTypeExclusionBE oMASSIT_BookingTypeExclusionBE, string Nothing = "Nothing")
        {
            DataTable dt = new DataTable();
            List<MASSIT_BookingTypeExclusionBE> oMASSIT_BookingTypeExclusionBEList = new List<MASSIT_BookingTypeExclusionBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASSIT_BookingTypeExclusionBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_BookingTypeExclusionBE.SiteID);
                param[index++] = new SqlParameter("@SiteVendorID", oMASSIT_BookingTypeExclusionBE.SiteVendorID);
                param[index++] = new SqlParameter("@SiteCarrierID", oMASSIT_BookingTypeExclusionBE.SiteCarrierID);
                param[index++] = new SqlParameter("@SiteDeliveryID", oMASSIT_BookingTypeExclusionBE.SiteDeliveryID);
                param[index++] = new SqlParameter("@SiteVendorIDs", oMASSIT_BookingTypeExclusionBE.SiteVendorIDs);
                param[index++] = new SqlParameter("@SiteCarrierIDs", oMASSIT_BookingTypeExclusionBE.SiteCarrierIDs);
                param[index++] = new SqlParameter("@SiteDeliveryIDs", oMASSIT_BookingTypeExclusionBE.SiteDeliveryIDs);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_BookingValidationExclusion", param);

                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public int? addEditBookingTypeExclusionDetailsDAL(MASSIT_BookingTypeExclusionBE oMASSIT_BookingTypeExclusionBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASSIT_BookingTypeExclusionBE.Action);
                param[index++] = new SqlParameter("@SITEID", oMASSIT_BookingTypeExclusionBE.SiteID);
                param[index++] = new SqlParameter("@SiteVendorID", oMASSIT_BookingTypeExclusionBE.SiteVendorID);
                param[index++] = new SqlParameter("@SiteCarrierID", oMASSIT_BookingTypeExclusionBE.SiteCarrierID);
                param[index++] = new SqlParameter("@SiteDeliveryID", oMASSIT_BookingTypeExclusionBE.SiteDeliveryID);
                param[index++] = new SqlParameter("@SiteVendorIDs", oMASSIT_BookingTypeExclusionBE.SiteVendorIDs);
                param[index++] = new SqlParameter("@SiteCarrierIDs", oMASSIT_BookingTypeExclusionBE.SiteCarrierIDs);
                param[index++] = new SqlParameter("@SiteDeliveryIDs", oMASSIT_BookingTypeExclusionBE.SiteDeliveryIDs);


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_BookingValidationExclusion", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MASSIT_BookingTypeExclusionBE> GetBookingTypeDAL(MASSIT_BookingTypeExclusionBE oMASSIT_BookingTypeExclusionBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_BookingTypeExclusionBE> oMASSIT_BookingTypeExclusionBEList = new List<MASSIT_BookingTypeExclusionBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASSIT_BookingTypeExclusionBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_BookingValidationExclusion", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_BookingTypeExclusionBE oNewMASSIT_BookingTypeExclusionBE = new MASSIT_BookingTypeExclusionBE();

                    oNewMASSIT_BookingTypeExclusionBE.BookingTypeId = Convert.ToInt32(dr["BookingTypeId"]);
                    oNewMASSIT_BookingTypeExclusionBE.BookingTypeDescription = dr["BookingTypeDescription"].ToString();


                    oMASSIT_BookingTypeExclusionBEList.Add(oNewMASSIT_BookingTypeExclusionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_BookingTypeExclusionBEList;
        }
    }
}

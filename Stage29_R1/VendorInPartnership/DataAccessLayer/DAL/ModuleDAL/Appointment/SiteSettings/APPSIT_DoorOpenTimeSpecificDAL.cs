﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;


namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings
{
    public class APPSIT_DoorOpenTimeSpecificDAL : BaseDAL
    {

        public List<MASSIT_DoorOpenTimeSpecificBE> GetDoorNoAndTypeSpecificDAL(MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_DoorOpenTimeSpecificBE> oMASSIT_DoorOpenTimeSpecificBEList = new List<MASSIT_DoorOpenTimeSpecificBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_DoorOpenTimeSpecificBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_DoorOpenTimeSpecific", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_DoorOpenTimeSpecificBE oNewMASSIT_DoorOpenTimeSpecificBE = new MASSIT_DoorOpenTimeSpecificBE();
                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.SiteDoorNumberID = Convert.ToInt32(dr["SiteDoorNumberID"]);

                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.DoorNumber = Convert.ToString(dr["DoorNumber"]);

                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType.DoorType = dr["DoorType"].ToString();

                    oMASSIT_DoorOpenTimeSpecificBEList.Add(oNewMASSIT_DoorOpenTimeSpecificBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_DoorOpenTimeSpecificBEList;
        }

        public List<MASSIT_DoorOpenTimeSpecificBE> GetDoorConstraintsSpecificDAL(MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_DoorOpenTimeSpecificBE> oMASSIT_DoorOpenTimeSpecificBEList = new List<MASSIT_DoorOpenTimeSpecificBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_DoorOpenTimeSpecificBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID);
                param[index++] = new SqlParameter("@Weekday", oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday);
                param[index++] = new SqlParameter("@StartWeekday", oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.StartWeekday);
                param[index++] = new SqlParameter("@WeekStartDate", oMASSIT_DoorOpenTimeSpecificBE.WeekStartDate);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_DoorOpenTimeSpecific", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_DoorOpenTimeSpecificBE oNewMASSIT_DoorOpenTimeSpecificBE = new MASSIT_DoorOpenTimeSpecificBE();
                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime = new MASSIT_DoorOpenTimeBE();
                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo = new MASSIT_DoorNoSetupBE();
                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.DoorNumber = dr["DoorNumber"] != DBNull.Value ? Convert.ToString(dr["DoorNumber"]) : string.Empty;
                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.SiteDoorNumberID = Convert.ToInt32(dr["SiteDoorNumberID"]);
                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorType.DoorType = dr["DoorType"] != DBNull.Value ? dr["DoorType"].ToString() : "";
                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday = dr["Weekday"].ToString();
                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.StartWeekday = dr["StartWeekday"].ToString();
                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.SlotTimeID = Convert.ToInt32(dr["SlotTimeID"]);
                    oNewMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorConstraintSpecificID = dr["DoorConstraintSpecificID"] != DBNull.Value ? Convert.ToInt32(dr["DoorConstraintSpecificID"]) : (int?)null;
                    oMASSIT_DoorOpenTimeSpecificBEList.Add(oNewMASSIT_DoorOpenTimeSpecificBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_DoorOpenTimeSpecificBEList;
        }

        public int? AddEditDoorConstraintsSpecificDAL(MASSIT_DoorOpenTimeSpecificBE oMASSIT_DoorOpenTimeSpecificBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_DoorOpenTimeSpecificBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorNo.SiteID);
                param[index++] = new SqlParameter("@DoorConstraintIDs", oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.DoorConstraintIDs);
                param[index++] = new SqlParameter("@Weekday", oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.Weekday);
                param[index++] = new SqlParameter("@WeekStartDate", oMASSIT_DoorOpenTimeSpecificBE.WeekStartDate);
                param[index++] = new SqlParameter("@StartWeekDays", oMASSIT_DoorOpenTimeSpecificBE.DoorOpenTime.StartWeekDays);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_DoorOpenTimeSpecific", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

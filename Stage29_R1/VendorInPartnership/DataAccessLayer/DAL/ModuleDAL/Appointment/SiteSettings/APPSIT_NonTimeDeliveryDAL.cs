﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings
{
    public class APPSIT_NonTimeDeliveryDAL : BaseDAL
    {

        public List<MASSIT_NonTimeDeliveryBE> GetNonTimeDeliverysDAL(MASSIT_NonTimeDeliveryBE oMASSIT_NonTimeDeliveryBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_NonTimeDeliveryBE> oMASSIT_NonTimeDeliveryBEList = new List<MASSIT_NonTimeDeliveryBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oMASSIT_NonTimeDeliveryBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_NonTimeDeliveryBE.SiteID);
                param[index++] = new SqlParameter("@SiteVendorID", oMASSIT_NonTimeDeliveryBE.SiteVendorID);
                param[index++] = new SqlParameter("@SiteCarrierID", oMASSIT_NonTimeDeliveryBE.SiteCarrierID);
                param[index++] = new SqlParameter("@SiteDeliveryID", oMASSIT_NonTimeDeliveryBE.SiteDeliveryID);
                param[index++] = new SqlParameter("@SiteVendorIDs", oMASSIT_NonTimeDeliveryBE.SiteVendorIDs);
                param[index++] = new SqlParameter("@SiteCarrierIDs", oMASSIT_NonTimeDeliveryBE.SiteCarrierIDs);
                param[index++] = new SqlParameter("@SiteDeliveryIDs", oMASSIT_NonTimeDeliveryBE.SiteDeliveryIDs);
                param[index++] = new SqlParameter("@Vendor_No", oMASSIT_NonTimeDeliveryBE.Vendor_No);
                param[index++] = new SqlParameter("@APP_NonTimeDeliveryPalletVolume", oMASSIT_NonTimeDeliveryBE.APP_NonTimeDeliveryPalletVolume);
                param[index++] = new SqlParameter("@APP_NonTimeDeliveryCartonVolume", oMASSIT_NonTimeDeliveryBE.APP_NonTimeDeliveryCartonVolume);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_NonTimeDelivery", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_NonTimeDeliveryBE oNewMASSIT_NonTimeDeliveryBE = new MASSIT_NonTimeDeliveryBE();

                    oNewMASSIT_NonTimeDeliveryBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMASSIT_NonTimeDeliveryBE.SiteVendorID = Convert.ToInt32(dr["SiteVendorID"].ToString());
                    oNewMASSIT_NonTimeDeliveryBE.SiteCarrierID = Convert.ToInt32(dr["SiteCarrierID"].ToString());
                    oNewMASSIT_NonTimeDeliveryBE.SiteDeliveryID = Convert.ToInt32(dr["SiteDeliveryID"].ToString());

                    oNewMASSIT_NonTimeDeliveryBE.Carrier = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_CarrierBE();
                    oNewMASSIT_NonTimeDeliveryBE.Delivery = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DeliveryTypeBE();

                    oNewMASSIT_NonTimeDeliveryBE.Carrier.CarrierName = dr["CarrierName"].ToString();
                    oNewMASSIT_NonTimeDeliveryBE.Delivery.DeliveryType = dr["DeliveryType"].ToString();

                    oNewMASSIT_NonTimeDeliveryBE.APP_NonTimeDeliveryPalletVolume = (dr["APP_NonTimeDeliveryPalletVolume"] != null && dr["APP_NonTimeDeliveryPalletVolume"].ToString() != "") ? Convert.ToInt32(dr["APP_NonTimeDeliveryPalletVolume"].ToString()) : 0;
                    oNewMASSIT_NonTimeDeliveryBE.APP_NonTimeDeliveryCartonVolume = (dr["APP_NonTimeDeliveryCartonVolume"] != null && dr["APP_IsBookingValidationExcluded"].ToString() != "") ? Convert.ToInt32(dr["APP_IsBookingValidationExcluded"].ToString()) : 0;

                    oNewMASSIT_NonTimeDeliveryBE.Vendor = new UP_VendorBE();
                    oNewMASSIT_NonTimeDeliveryBE.Vendor.VendorName = dr["VENDOR"].ToString();
                    oNewMASSIT_NonTimeDeliveryBE.SiteVendorID = Convert.ToInt32(dr["SiteVendorID"].ToString());
                    oNewMASSIT_NonTimeDeliveryBE.Vendor_Country = Convert.ToInt32(dr["Vendor_Country"].ToString());

                    oMASSIT_NonTimeDeliveryBEList.Add(oNewMASSIT_NonTimeDeliveryBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_NonTimeDeliveryBEList;
        }

        public DataTable GetNonTimeDeliverysDAL(MASSIT_NonTimeDeliveryBE oMASSIT_NonTimeDeliveryBE, string Nothing = "Nothing")
        {
            DataTable dt = new DataTable();
            List<MASSIT_NonTimeDeliveryBE> oMASSIT_NonTimeDeliveryBEList = new List<MASSIT_NonTimeDeliveryBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oMASSIT_NonTimeDeliveryBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_NonTimeDeliveryBE.SiteID);
                param[index++] = new SqlParameter("@SiteVendorID", oMASSIT_NonTimeDeliveryBE.SiteVendorID);
                param[index++] = new SqlParameter("@SiteCarrierID", oMASSIT_NonTimeDeliveryBE.SiteCarrierID);
                param[index++] = new SqlParameter("@SiteDeliveryID", oMASSIT_NonTimeDeliveryBE.SiteDeliveryID);
                param[index++] = new SqlParameter("@SiteVendorIDs", oMASSIT_NonTimeDeliveryBE.SiteVendorIDs);
                param[index++] = new SqlParameter("@SiteCarrierIDs", oMASSIT_NonTimeDeliveryBE.SiteCarrierIDs);
                param[index++] = new SqlParameter("@SiteDeliveryIDs", oMASSIT_NonTimeDeliveryBE.SiteDeliveryIDs);
                param[index++] = new SqlParameter("@Vendor_No", oMASSIT_NonTimeDeliveryBE.Vendor_No);
                param[index++] = new SqlParameter("@APP_NonTimeDeliveryPalletVolume", oMASSIT_NonTimeDeliveryBE.APP_NonTimeDeliveryPalletVolume);
                param[index++] = new SqlParameter("@APP_NonTimeDeliveryCartonVolume", oMASSIT_NonTimeDeliveryBE.APP_NonTimeDeliveryCartonVolume);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_NonTimeDelivery", param);

                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public int? addEditNonTimeDeliveryDetailsDAL(MASSIT_NonTimeDeliveryBE oMASSIT_NonTimeDeliveryBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oMASSIT_NonTimeDeliveryBE.Action);
                param[index++] = new SqlParameter("@SITEID", oMASSIT_NonTimeDeliveryBE.SiteID);
                param[index++] = new SqlParameter("@SiteVendorID", oMASSIT_NonTimeDeliveryBE.SiteVendorID);
                param[index++] = new SqlParameter("@SiteCarrierID", oMASSIT_NonTimeDeliveryBE.SiteCarrierID);
                param[index++] = new SqlParameter("@SiteDeliveryID", oMASSIT_NonTimeDeliveryBE.SiteDeliveryID);
                param[index++] = new SqlParameter("@SiteVendorIDs", oMASSIT_NonTimeDeliveryBE.SiteVendorIDs);
                param[index++] = new SqlParameter("@SiteCarrierIDs", oMASSIT_NonTimeDeliveryBE.SiteCarrierIDs);
                param[index++] = new SqlParameter("@SiteDeliveryIDs", oMASSIT_NonTimeDeliveryBE.SiteDeliveryIDs);
                param[index++] = new SqlParameter("@Vendor_No", oMASSIT_NonTimeDeliveryBE.Vendor_No);
                param[index++] = new SqlParameter("@APP_NonTimeDeliveryPalletVolume", oMASSIT_NonTimeDeliveryBE.APP_NonTimeDeliveryPalletVolume);
                param[index++] = new SqlParameter("@APP_NonTimeDeliveryCartonVolume", oMASSIT_NonTimeDeliveryBE.APP_NonTimeDeliveryCartonVolume);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_NonTimeDelivery", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

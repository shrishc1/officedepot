﻿using BusinessEntities.ModuleBE.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Security
{
    public class SCT_ModuleDAL
    {

        public List<SCT_ModuleBE> GetModuleDAL(SCT_ModuleBE oSCT_ModuleBE)
        {
            DataTable dt = new DataTable();
            List<SCT_ModuleBE> SCT_ModuleBEList = new List<SCT_ModuleBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oSCT_ModuleBE.Action);
                param[index++] = new SqlParameter("@ParentModuleID", oSCT_ModuleBE.ParentModuleID);
                param[index++] = new SqlParameter("@UserID", Convert.ToInt32(System.Web.HttpContext.Current.Session["UserID"]));

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_Module", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_ModuleBE oNewSCT_ModuleBE = new SCT_ModuleBE();

                    oNewSCT_ModuleBE.ModuleID = Convert.ToInt32(dr["ModuleID"]);
                    oNewSCT_ModuleBE.ModuleName = dr["ModuleName"].ToString();

                    if (!string.IsNullOrEmpty(Convert.ToString(dr["OrderBy"])))
                        oNewSCT_ModuleBE.OrderBy = Convert.ToInt32(dr["OrderBy"]);
                    SCT_ModuleBEList.Add(oNewSCT_ModuleBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_ModuleBEList;
        }

        public DataTable GetReadOnlyModuleDAL(SCT_ModuleBE oSCT_ModuleBE)
        {
            DataTable dt = new DataTable();
            List<SCT_ModuleBE> SCT_ModuleBEList = new List<SCT_ModuleBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oSCT_ModuleBE.Action);
                param[index++] = new SqlParameter("@ScreenName", oSCT_ModuleBE.ScreenName);
                param[index++] = new SqlParameter("@UserID", Convert.ToInt32(System.Web.HttpContext.Current.Session["UserID"]));
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_Module", param);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }
    }
}

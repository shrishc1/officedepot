﻿using BusinessEntities.ModuleBE.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Security
{
    public class SCT_ScreenDAL
    {

        public List<SCT_ScreenBE> GetScreenDAL(SCT_ScreenBE oSCT_ScreenBE)
        {
            DataTable dt = new DataTable();
            List<SCT_ScreenBE> SCT_ScreenBEList = new List<SCT_ScreenBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oSCT_ScreenBE.Action);
                param[index++] = new SqlParameter("@ModuleID", oSCT_ScreenBE.ModuleID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_Screen", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_ScreenBE oNewSCT_ScreenBE = new SCT_ScreenBE();

                    oNewSCT_ScreenBE.ModuleID = Convert.ToInt32(dr["ModuleID"]);
                    oNewSCT_ScreenBE.ScreenName = dr["ScreenName"].ToString();
                    oNewSCT_ScreenBE.ScreenID = Convert.ToInt32(dr["ScreenID"]);
                    oNewSCT_ScreenBE.ScreenUrl = dr["ScreenUrl"].ToString();

                    SCT_ScreenBEList.Add(oNewSCT_ScreenBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_ScreenBEList;
        }
    }
}

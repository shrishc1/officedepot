﻿using BusinessEntities.ModuleBE.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Security
{
    public class MASCNT_CustomerSetupDAL
    {
        public List<MASCNT_CustomerSetupBE> GetCustomersDAL(MASCNT_CustomerSetupBE mASCNT_CustomerSetupBE)
        {
            DataTable dt = new DataTable();
            List<MASCNT_CustomerSetupBE> mASCNT_CustomerSetupBEs = new List<MASCNT_CustomerSetupBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", mASCNT_CustomerSetupBE.Action);
                param[index++] = new SqlParameter("@CountryID", mASCNT_CustomerSetupBE.CountryID);
                param[index++] = new SqlParameter("@CustomerName", mASCNT_CustomerSetupBE.CustomerName);
                param[index++] = new SqlParameter("@CustomerID", mASCNT_CustomerSetupBE.CustomerID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASCNT_CustomerSetup", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASCNT_CustomerSetupBE mASCNT = new MASCNT_CustomerSetupBE();

                    mASCNT.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                    mASCNT.CountryID = Convert.ToInt32(dr["CountryID"]);
                    mASCNT.CountryName = dr["CountryName"].ToString();
                    mASCNT.CustomerName = dr["CustomerName"].ToString();
                    mASCNT.UpdatedByUser = dr["UpdatedByUser"].ToString();
                    mASCNT.UpdateDate = dr["UpdateDate"].ToString();

                    mASCNT_CustomerSetupBEs.Add(mASCNT);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return mASCNT_CustomerSetupBEs;
        }

        public List<MASCNT_CustomerSetupBE> SearchCustomersDAL(MASCNT_CustomerSetupBE mASCNT_CustomerSetupBE)
        {
            DataTable dt = new DataTable();
            List<MASCNT_CustomerSetupBE> mASCNT_CustomerSetupBEs = new List<MASCNT_CustomerSetupBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", mASCNT_CustomerSetupBE.Action);
                param[index++] = new SqlParameter("@CustomerName", mASCNT_CustomerSetupBE.CustomerName);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASCNT_CustomerSetup", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASCNT_CustomerSetupBE mASCNT = new MASCNT_CustomerSetupBE();

                    mASCNT.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                    mASCNT.CustomerName = dr["CustomerName"].ToString();

                    mASCNT_CustomerSetupBEs.Add(mASCNT);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return mASCNT_CustomerSetupBEs;
        }

        public List<MASCNT_CustomerSetupBE> GetCustomersBySiteDAL(string Action)
        {
            DataTable dt = new DataTable();
            List<MASCNT_CustomerSetupBE> mASCNT_CustomerSetupBEs = new List<MASCNT_CustomerSetupBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASCNT_CustomerSetup", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASCNT_CustomerSetupBE mASCNT = new MASCNT_CustomerSetupBE();

                    mASCNT.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                    mASCNT.CustomerName = dr["CustomerName"].ToString();
                    mASCNT.SiteID = Convert.ToInt32(dr["SiteID"]);

                    mASCNT_CustomerSetupBEs.Add(mASCNT);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return mASCNT_CustomerSetupBEs;
        }

        public int? addEditCustomerDetailsDAL(MASCNT_CustomerSetupBE mASCNT_CustomerSetupBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", mASCNT_CustomerSetupBE.Action);
                param[index++] = new SqlParameter("@CustomerName", mASCNT_CustomerSetupBE.CustomerName);
                param[index++] = new SqlParameter("@CountryID", mASCNT_CustomerSetupBE.CountryID);
                param[index++] = new SqlParameter("@CustomerID", mASCNT_CustomerSetupBE.CustomerID);
                param[index++] = new SqlParameter("@UserID", mASCNT_CustomerSetupBE.UpdatedBy);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASCNT_CustomerSetup", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataTable GetDefaultStockPlannerReportDAL(DefaultStockPlannerBE defaultStockPlannerBE)
        {
            DataTable dt = new DataTable();
            List<MASCNT_CustomerSetupBE> mASCNT_CustomerSetupBEs = new List<MASCNT_CustomerSetupBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", defaultStockPlannerBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", defaultStockPlannerBE.CountryIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", defaultStockPlannerBE.SiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", defaultStockPlannerBE.VendorIDs);
                param[index++] = new SqlParameter("@SelectedCustomerIDs", defaultStockPlannerBE.CustomerIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIDs", defaultStockPlannerBE.StockPlannerIDs);
                param[index++] = new SqlParameter("@VIPDefaultStockPlannerIDs", defaultStockPlannerBE.VIPDefaultStockPlannerIDs);
                param[index++] = new SqlParameter("@OD_SKU_No", defaultStockPlannerBE.OD_SKU_No);
                param[index++] = new SqlParameter("@Viking_SKU", defaultStockPlannerBE.Viking_SKU);
                param[index++] = new SqlParameter("@ExcludeItems", defaultStockPlannerBE.ExcludeItems);
                param[index++] = new SqlParameter("@PlannerMatch", defaultStockPlannerBE.PlannerMatch);
                param[index++] = new SqlParameter("@SOH", defaultStockPlannerBE.SOH);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spCNT_DefaultStockPlannerUpdate", param);

                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }


        public List<StockPlannerWithSiteBE> GetStockPlannerWithSiteDAL(string Action)
        {
            DataTable dt = new DataTable();
            List<StockPlannerWithSiteBE> stockPlannerWithSites = new List<StockPlannerWithSiteBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spCNT_DefaultStockPlannerUpdate", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    StockPlannerWithSiteBE stockPlannerWithSite = new StockPlannerWithSiteBE();

                    stockPlannerWithSite.SiteID = Convert.ToInt32(dr["SiteID"]);
                    stockPlannerWithSite.StockPlanner = dr["StockPlanner"].ToString();
                    stockPlannerWithSite.StockPlannerID = Convert.ToInt32(dr["StockPlannerID"]);

                    stockPlannerWithSites.Add(stockPlannerWithSite);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return stockPlannerWithSites;
        }


        public int? SaveDefaultStockPlannerDAL(DataTable spData)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@Action", "SaveStockPlanner");
                param[1] = new SqlParameter("@SPTable", SqlDbType.Structured);
                param[1].Value = spData;

                SqlHelper.ExecuteNonQuery(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spCNT_DefaultStockPlannerUpdate", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return 1;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.Security;
using System.Data;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Security
{
    public class SCT_AssignScreenDAL
    {
        public List<SCT_AssignScreenBE> GetRoleTemplateDAL(int RoleID)
        {
            List<SCT_AssignScreenBE> lstRoleTemplates = new List<SCT_AssignScreenBE>();
            DataTable dt = new DataTable();
            try
            {
                string query = string.Empty;
                if (RoleID != null)
                    query = string.Format("SELECT * FROM SCT_Template WHERE UserRoleID={0}", RoleID);
                else
                    query = "SELECT * FROM SCT_Template";
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.Text, query);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_AssignScreenBE oRoleTemplate = new SCT_AssignScreenBE();

                    oRoleTemplate.TemplateID = Convert.ToInt32(dr["TemplateID"]);
                    oRoleTemplate.TemplateName = dr["TemplateName"].ToString();

                    lstRoleTemplates.Add(oRoleTemplate);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveLogEntry(ex.Message + " |      Error Source:- " + ex.Source);
            }
            return lstRoleTemplates;
        }

        public SCT_AssignScreenBE GetTemplateInfoDAL(int TemplateID)
        {
            SCT_AssignScreenBE Templates = new SCT_AssignScreenBE();
            DataTable dt = new DataTable();
            try
            {
                string WritableScreen = string.Empty;
                string ReadableScreen = string.Empty;
                string query = string.Empty;
                query = string.Format("SELECT * FROM SCT_TemplateScreen WHERE TemplateID={0}", TemplateID);
          
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.Text, query);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    string screenid = dr["ScreenID"].ToString();
                    ReadableScreen += "," + screenid;
                    if (Convert.ToBoolean(dr["IsWriteAccessAllowed"]))
                    {
                        WritableScreen += "," + screenid;
                    }
                }
                Templates.ScreenReadPermission = ReadableScreen.Trim(',');
                Templates.ScreenWritePermission = WritableScreen.Trim(',');
            }
            catch (Exception ex)
            {
                LogUtility.SaveLogEntry(ex.Message + " |      Error Source:- " + ex.Source);
            }
            return Templates;
        }
    }
}

﻿using BusinessEntities.ModuleBE.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Security
{
    public class SCT_TemplateDAL
    {
        public List<SCT_UserRoleBE> GetUserRolesDAL(SCT_UserRoleBE oSCT_UserRoleBE)
        {
            List<SCT_UserRoleBE> lstRoles = new List<SCT_UserRoleBE>();
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("Action", oSCT_UserRoleBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUserRole", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserRoleBE oUserRole = new SCT_UserRoleBE();

                    oUserRole.UserRoleID = Convert.ToInt32(dr["UserRoleID"]);
                    oUserRole.RoleName = dr["RoleName"].ToString();

                    lstRoles.Add(oUserRole);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstRoles;
        }

        public void SavePermissionToDB(SCT_TemplateBE oSCT_TemplateBE)
        {
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("Action", oSCT_TemplateBE.Action);
                param[index++] = new SqlParameter("@ForRole", oSCT_TemplateBE.ForRole);
                param[index++] = new SqlParameter("@TemplateName", oSCT_TemplateBE.TemplateName);
                param[index++] = new SqlParameter("@SelectedScreen", oSCT_TemplateBE.SelectedScreen);
                param[index++] = new SqlParameter("@TemplateID", oSCT_TemplateBE.TemplateID != null ? oSCT_TemplateBE.TemplateID : null);


                SqlHelper.ExecuteNonQuery(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_Template", param);
            }
            catch { }

        }

        public List<SCT_TemplateBE> GetTemplate(SCT_TemplateBE oSCT_TemplateBE)
        {
            List<SCT_TemplateBE> lstTemplate = new List<SCT_TemplateBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_TemplateBE.Action);
                param[index++] = new SqlParameter("@ForRole", oSCT_TemplateBE.ForRole);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_Template", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_TemplateBE oNewSCT_TemplateBE = new SCT_TemplateBE();
                    oNewSCT_TemplateBE.TemplateName = dr["TemplateName"].ToString();
                    oNewSCT_TemplateBE.TemplateID = Convert.ToInt32(dr["TemplateID"]);
                    oNewSCT_TemplateBE.RoleName = dr["RoleName"].ToString();
                    lstTemplate.Add(oNewSCT_TemplateBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstTemplate;
        }

        public SCT_TemplateBE GetTemplateInfo(SCT_TemplateBE oSCT_TemplateBE)
        {
            SCT_TemplateBE oNewSCT_TemplateBE = new SCT_TemplateBE();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_TemplateBE.Action);
                param[index++] = new SqlParameter("@TemplateID", oSCT_TemplateBE.TemplateID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_Template", param);

                dt = ds.Tables[0];
                string SelectedScreen = string.Empty;
                foreach (DataRow dr in dt.Rows)
                {
                    oNewSCT_TemplateBE.TemplateName = dr["TemplateName"].ToString();
                    oNewSCT_TemplateBE.RoleName = dr["RoleName"].ToString();
                    string ScreenID = dr["ScreenID"].ToString();
                    SelectedScreen += ScreenID + ",";
                }
                oNewSCT_TemplateBE.SelectedScreen = SelectedScreen;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oNewSCT_TemplateBE;
        }

        public List<SCT_TemplateBE> GetRoleTemplateDAL(SCT_TemplateBE oSCT_TemplateBE)
        {
            List<SCT_TemplateBE> lstRoleTemplates = new List<SCT_TemplateBE>();
            DataTable dt = new DataTable();
            int? RoleID = oSCT_TemplateBE.ForRole;
            try
            {
                string query = string.Empty;
                if (RoleID != null)
                    query = string.Format("SELECT * FROM SCT_Template WHERE UserRoleID={0}", RoleID);
                else
                    query = "SELECT * FROM SCT_Template";
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.Text, query);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_TemplateBE oRoleTemplate = new SCT_TemplateBE();

                    oRoleTemplate.TemplateID = Convert.ToInt32(dr["TemplateID"]);
                    oRoleTemplate.TemplateName = dr["TemplateName"].ToString();

                    lstRoleTemplates.Add(oRoleTemplate);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstRoleTemplates;
        }

        //public SCT_TemplateBE GetTemplateInfoDAL(SCT_TemplateBE oSCT_TemplateBE)
        //{
        //    SCT_TemplateBE Templates = new SCT_TemplateBE();
        //    DataTable dt = new DataTable();
        //    try
        //    {
        //        string WritableScreen = string.Empty;
        //        string ReadableScreen = string.Empty;
        //        string query = string.Empty;
        //        query = string.Format("SELECT * FROM SCT_TemplateScreen WHERE TemplateID={0}", oSCT_TemplateBE.TemplateID);

        //        DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.Text, query);

        //        dt = ds.Tables[0];

        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            string screenid = dr["ScreenID"].ToString();
        //            ReadableScreen += "," + screenid;
        //            if (Convert.ToBoolean(dr["IsWriteAccessAllowed"]))
        //            {
        //                WritableScreen += "," + screenid;
        //            }
        //        }
        //        Templates.ScreenWithReadPermission = ReadableScreen.Trim(',');
        //        Templates.ScreenWithWritePermission = WritableScreen.Trim(',');
        //    }
        //    catch (Exception ex)
        //    {
        //        LogUtility.SaveLogEntry(ex);
        //    }
        //    return Templates;
        //}

        public SCT_TemplateBE GetTemplateForReportLibDAL(SCT_TemplateBE oSCT_TemplateBE)
        {
            var templateBE = new SCT_TemplateBE();
            try
            {
                var dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oSCT_TemplateBE.Action);
                param[index++] = new SqlParameter("@RoleId", oSCT_TemplateBE.RoleId);
                param[index++] = new SqlParameter("@ScreenId", oSCT_TemplateBE.ScreenID);
                param[index++] = new SqlParameter("@UserId", oSCT_TemplateBE.UserId);
                var ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_Template", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    templateBE.TemplateScreenID = dr["TemplateScreenID"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["TemplateScreenID"]);
                    templateBE.TemplateID = dr["TemplateID"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["TemplateID"]);
                    templateBE.ScreenID = dr["ScreenID"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["ScreenID"]);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return templateBE;
        }

        public List<SCT_TemplateBE> GetVendorTemplate(SCT_TemplateBE oSCT_TemplateBE)
        {
            List<SCT_TemplateBE> lstTemplate = new List<SCT_TemplateBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_TemplateBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_VendorTemplate", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_TemplateBE oNewSCT_TemplateBE = new SCT_TemplateBE();
                    oNewSCT_TemplateBE.TemplateName = dr["TemplateName"].ToString();
                    oNewSCT_TemplateBE.VendorTemplateID = dr["VendorTemplateID"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["VendorTemplateID"]);
                    oNewSCT_TemplateBE.CreatedBy = dr["CreatedBy"].ToString();
                    oNewSCT_TemplateBE.CreatedOn = dr["VendorTemplateID"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["CreatedOn"]);
                    lstTemplate.Add(oNewSCT_TemplateBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstTemplate;
        }
        public DataSet SaveVendorTemplate(SCT_TemplateBE oSCT_TemplateBE)
        {
            DataSet ds = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oSCT_TemplateBE.Action);
                param[index++] = new SqlParameter("@TemplateName", oSCT_TemplateBE.TemplateName);
                param[index++] = new SqlParameter("@CreatedBy", oSCT_TemplateBE.CreatedBy);
                param[index++] = new SqlParameter("@CreatedOn", oSCT_TemplateBE.CreatedOn);
                param[index++] = new SqlParameter("@SelectedVendorId", oSCT_TemplateBE.SelectedVendorIds);
                param[index++] = new SqlParameter("@VendorTemplateID", oSCT_TemplateBE.VendorTemplateID != null ? oSCT_TemplateBE.VendorTemplateID : null);


                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_VendorTemplate", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public List<SCT_TemplateBE> GetVendorTemplateById(SCT_TemplateBE oSCT_TemplateBE)
        {
            List<SCT_TemplateBE> lstVendorTemplate = new List<SCT_TemplateBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_TemplateBE.Action);
                param[index++] = new SqlParameter("@VendorTemplateID", oSCT_TemplateBE.VendorTemplateID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_VendorTemplate", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_TemplateBE oNewSCT_TemplateBE = new SCT_TemplateBE();
                    oNewSCT_TemplateBE.Vendor = new BusinessEntities.ModuleBE.AdminFunctions.MAS_VendorBE();

                    oNewSCT_TemplateBE.TemplateName = dr["TemplateName"].ToString();
                    oNewSCT_TemplateBE.Vendor.VendorID = dr["VendorId"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["VendorId"]);
                    oNewSCT_TemplateBE.Vendor.Vendor_Name = dr["VendorName"].ToString();
                    oNewSCT_TemplateBE.Vendor.Vendor_No = dr["Vendor_No"].ToString();
                    oNewSCT_TemplateBE.Vendor.VendorContactName = dr["Vendor_Name"].ToString();
                    lstVendorTemplate.Add(oNewSCT_TemplateBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstVendorTemplate;
        }


        public int? DeleteTemplateByIdDAL(SCT_TemplateBE oSCT_TemplateBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oSCT_TemplateBE.Action);
                param[index++] = new SqlParameter("@TemplateName", oSCT_TemplateBE.TemplateName);
                param[index++] = new SqlParameter("@TemplateID", oSCT_TemplateBE.TemplateID != null ? oSCT_TemplateBE.TemplateID : null);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_Template", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataSet GetVendorTemplateByIdForExportDAL(SCT_TemplateBE oSCT_TemplateBE)
        {
            DataSet Result = null;
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_TemplateBE.Action);
                param[index++] = new SqlParameter("@VendorTemplateID", oSCT_TemplateBE.VendorTemplateID);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_VendorTemplate", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return Result;
        }


        public int? DeleteVendorTemplateByIdDAL(SCT_TemplateBE oSCT_TemplateBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oSCT_TemplateBE.Action);
                param[index++] = new SqlParameter("@VendorTemplateID", oSCT_TemplateBE.VendorTemplateID);


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_VendorTemplate", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


    }


}

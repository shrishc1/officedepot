﻿using BusinessEntities.ModuleBE.GlobalSettings;
using BusinessEntities.ModuleBE.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Security
{
    public class SCT_UserDAL
    {

        public List<SCT_UserBE> GetUserDetailsDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[10];
                oSCT_UserBE.Password = Common.EncryptPassword(oSCT_UserBE.Password);
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@LoginID", oSCT_UserBE.LoginID);
                param[index++] = new SqlParameter("@SiteId", oSCT_UserBE.SiteId);
                param[index++] = new SqlParameter("@Password", oSCT_UserBE.Password);
                param[index++] = new SqlParameter("@RoleName", oSCT_UserBE.RoleName);
                param[index++] = new SqlParameter("@IsDeliveryRefusalAuthorization", oSCT_UserBE.IsDeliveryRefusalAuthorization);
                param[index++] = new SqlParameter("@VendorID", oSCT_UserBE.VendorID);
                param[index++] = new SqlParameter("@LanguageID", oSCT_UserBE.LanguageID);
                param[index++] = new SqlParameter("@UserSearch", oSCT_UserBE.UserSearchText);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();

                    oNewSCT_UserBE.UserID = Convert.ToInt32(dr["UserID"]);
                    oNewSCT_UserBE.LoginID = dr["LoginID"].ToString();
                    oNewSCT_UserBE.FullName = dr["FirstName"].ToString() + " " + dr["Lastname"].ToString();
                    oNewSCT_UserBE.FirstName = dr["FirstName"].ToString();
                    oNewSCT_UserBE.Lastname = dr["Lastname"].ToString();
                    oNewSCT_UserBE.EmailId = dr["EmailId"].ToString();
                    oNewSCT_UserBE.PhoneNumber = dr["PhoneNumber"].ToString();
                    oNewSCT_UserBE.FaxNumber = dr["FaxNumber"].ToString();
                    oNewSCT_UserBE.UserRoleID = dr["UserRoleID"] != DBNull.Value ? Convert.ToInt32(dr["UserRoleID"]) : (int?)null;
                    oNewSCT_UserBE.RoleName = dr["RoleName"].ToString();
                    oNewSCT_UserBE.Password = dr["Password"].ToString();
                    oNewSCT_UserBE.UserIDPassword = dr["UserID"].ToString().Trim() + "-" + dr["Password"].ToString().Trim();
                    oNewSCT_UserBE.AccountStatus = dr["AccountStatus"].ToString();
                    oNewSCT_UserBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : (int?)null;
                    oNewSCT_UserBE.CarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"]) : (int?)null;

                    SCT_UserBEList.Add(oNewSCT_UserBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;
        }

        /// <summary>
        /// Returns list of managers(user where userroleid=4)
        /// </summary>
        /// <param name="oSCT_UserBE"></param>
        /// <returns></returns>
        public List<SCT_UserBE> GetMangersDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {

                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.UserID = Convert.ToInt32(dr["UserID"]);
                    oNewSCT_UserBE.FirstName = dr["ManagerName"].ToString();

                    SCT_UserBEList.Add(oNewSCT_UserBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;

        }
        /// <summary>
        /// Returns list of OD managers(user where userroleid=9)
        /// </summary>
        /// <param name="oSCT_UserBE"></param>
        /// <returns></returns>
        public List<SCT_UserBE> GetODMangersDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@VendorId", oSCT_UserBE.VendorID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {

                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.UserID = Convert.ToInt32(dr["UserID"]);
                    oNewSCT_UserBE.LoginID = dr["UserID"].ToString() + "-" + dr["LoginID"].ToString() + "-" + dr["LanguageID"].ToString();
                    oNewSCT_UserBE.FirstName = dr["ManagerName"].ToString();
                    oNewSCT_UserBE.LanguageID = Convert.ToInt32(dr["LanguageID"]);

                    SCT_UserBEList.Add(oNewSCT_UserBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;

        }



        public List<SCT_UserBE> GetCarrierDetailsDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@CarrierName", oSCT_UserBE.CarrierName);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.CarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"]) : (int?)null;
                    oNewSCT_UserBE.CarrierName = Common.HtmlDecode(Convert.ToString(dr["CarrierName"]));
                    SCT_UserBEList.Add(oNewSCT_UserBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;

        }
        public List<SCT_UserBE> GetPlannerDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {

                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.UserID = Convert.ToInt32(dr["UserID"]);
                    oNewSCT_UserBE.FullName = dr["StockPalnner"].ToString();
                    SCT_UserBEList.Add(oNewSCT_UserBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;

        }
        public List<SCT_UserBE> GetUserDefaultsDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@RoleName", oSCT_UserBE.RoleName);
                param[index++] = new SqlParameter("@VendorID", oSCT_UserBE.VendorID);
                param[index++] = new SqlParameter("@CarrierID", oSCT_UserBE.CarrierID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();

                    oNewSCT_UserBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewSCT_UserBE.Site.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewSCT_UserBE.Site.SiteCountryID = Convert.ToInt32(dr["SiteCountryID"]);


                    SCT_UserBEList.Add(oNewSCT_UserBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;
        }

        public int? ChangePasswordDAL(SCT_UserBE oSCT_UserBE, string NewPassword)
        {
            int? statusFlag = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                NewPassword = Common.EncryptPassword(NewPassword);
                oSCT_UserBE.Password = Common.EncryptPassword(oSCT_UserBE.Password);
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@Password", oSCT_UserBE.Password);
                param[index++] = new SqlParameter("@NewPassword", NewPassword);
                param[index++] = new SqlParameter("@AccountStatus", oSCT_UserBE.AccountStatus);

                //statusFlag = SqlHelper.ExecuteNonQuery(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                statusFlag = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return statusFlag;
        }


        public DataTable GetUserSitesDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);

                dt = ds.Tables[0];
                //return dt;

                //foreach (DataRow dr in dt.Rows)
                //{
                //    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();

                //    oNewSCT_UserBE.UserID =  Convert.ToInt32(dr["UserID"].ToString());
                //    oNewSCT_UserBE.SiteId = Convert.ToInt32(dr["SiteID"].ToString());
                //    oNewSCT_UserBE.IsDefaultSite = Convert.ToBoolean( dr["IsDefaultSite"].ToString());
                //    oNewSCT_UserBE.TemplateID = Convert.ToInt32(dr["TemplateID"].ToString());
                //    oNewSCT_UserBE.SelectedSites = dr["SiteName"].ToString();
                //    SCT_UserBEList.Add(oNewSCT_UserBE);

                //}
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public List<SCT_UserBE> GetUsersDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@VendorIds", oSCT_UserBE.VendorIDs);
                param[index++] = new SqlParameter("@AccountStatus", oSCT_UserBE.AccountStatus);
                param[index++] = new SqlParameter("@CountryID", oSCT_UserBE.CountryId);
                param[index++] = new SqlParameter("@UserRoleID", oSCT_UserBE.UserRoleID);
                param[index++] = new SqlParameter("@PageIndex", oSCT_UserBE.PageIndex);
                param[index++] = new SqlParameter("@PageSize", oSCT_UserBE.PageSize);
                param[index++] = new SqlParameter("@CarrierIds", oSCT_UserBE.CarrierIDs);
                param[index++] = new SqlParameter("@UserSearch", oSCT_UserBE.UserSearchText);
                param[index++] = new SqlParameter("@ReportType", oSCT_UserBE.ReportType);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();

                    //---Stage 7 V1 point 10-----//
                    oNewSCT_UserBE.Country = dr["Country"].ToString();
                    if (ds.Tables[0].Columns.Contains("DefaultSite"))
                        oNewSCT_UserBE.DefaultSite = dr["DefaultSite"] != DBNull.Value ? dr["DefaultSite"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("TemplateName"))
                        oNewSCT_UserBE.TemplateName = dr["TemplateName"] != DBNull.Value ? dr["TemplateName"].ToString() : string.Empty;
                    //----------------------//

                    if (ds.Tables[0].Columns.Contains("UserID"))
                        oNewSCT_UserBE.UserID = dr["UserID"] != DBNull.Value ? Convert.ToInt32(dr["UserID"].ToString()) : 0;

                    if (ds.Tables[0].Columns.Contains("FirstName"))
                        oNewSCT_UserBE.FirstName = dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString() : string.Empty;

                    if (ds.Tables[0].Columns.Contains("Lastname"))
                        oNewSCT_UserBE.Lastname = dr["Lastname"] != DBNull.Value ? dr["Lastname"].ToString() : string.Empty;

                    if (ds.Tables[0].Columns.Contains("LoginID"))
                        oNewSCT_UserBE.LoginID = dr["LoginID"] != DBNull.Value ? dr["LoginID"].ToString() : "--NA--";

                    if (ds.Tables[0].Columns.Contains("EmailId"))
                        oNewSCT_UserBE.EmailId = dr["EmailId"] != DBNull.Value ? dr["EmailId"].ToString() : string.Empty;

                    if (ds.Tables[0].Columns.Contains("PhoneNumber"))
                        oNewSCT_UserBE.PhoneNumber = dr["PhoneNumber"] != DBNull.Value ? dr["PhoneNumber"].ToString() : string.Empty;

                    if (ds.Tables[0].Columns.Contains("FaxNumber"))
                        oNewSCT_UserBE.FaxNumber = dr["FaxNumber"] != DBNull.Value ? dr["FaxNumber"].ToString() : string.Empty;

                    if (ds.Tables[0].Columns.Contains("RoleName"))
                        oNewSCT_UserBE.RoleName = dr["RoleName"] != DBNull.Value ? dr["RoleName"].ToString() : string.Empty;

                    if (ds.Tables[0].Columns.Contains("CarrierName"))
                        oNewSCT_UserBE.CarrierName = dr["CarrierName"] != DBNull.Value ? dr["CarrierName"].ToString() : string.Empty;

                    if (ds.Tables[0].Columns.Contains("SchedulingContact"))
                        oNewSCT_UserBE.SchedulingContact = dr["SchedulingContact"] != DBNull.Value ? Convert.ToChar(dr["SchedulingContact"]) : (char?)null;
                    if (ds.Tables[0].Columns.Contains("ScorecardContact"))
                        oNewSCT_UserBE.ScorecardContact = dr["ScorecardContact"] != DBNull.Value ? Convert.ToChar(dr["ScorecardContact"]) : (char?)null;
                    if (ds.Tables[0].Columns.Contains("DiscrepancyContact"))
                        oNewSCT_UserBE.DiscrepancyContact = dr["DiscrepancyContact"] != DBNull.Value ? Convert.ToChar(dr["DiscrepancyContact"]) : (char?)null;
                    if (ds.Tables[0].Columns.Contains("OTIFContact"))
                        oNewSCT_UserBE.OTIFContact = dr["OTIFContact"] != DBNull.Value ? Convert.ToChar(dr["OTIFContact"]) : (char?)null;

                    if (ds.Tables[0].Columns.Contains("AccountStatus"))
                        oNewSCT_UserBE.AccountStatus = dr["AccountStatus"] != DBNull.Value ? dr["AccountStatus"].ToString() : string.Empty;

                    if (ds.Tables[0].Columns.Contains("UserRoleID"))
                        oNewSCT_UserBE.UserRoleID = dr["UserRoleID"] != DBNull.Value ? Convert.ToInt32(dr["UserRoleID"].ToString()) : 0; // (int?)null;

                    if (ds.Tables[0].Columns.Contains("CratedDate"))
                        oNewSCT_UserBE.CratedDate = dr["CratedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CratedDate"].ToString()) : (DateTime?)null;

                    if (ds.Tables[0].Columns.Contains("LastLoggedOn"))
                        oNewSCT_UserBE.LastLoggedOn = dr["LastLoggedOn"] != DBNull.Value ? Convert.ToDateTime(dr["LastLoggedOn"].ToString()) : (DateTime?)null;

                    if (ds.Tables[0].Columns.Contains("Elapseddays"))
                        oNewSCT_UserBE.Elapseddays = dr["Elapseddays"] != DBNull.Value ? Convert.ToInt32(dr["Elapseddays"].ToString()) : (int?)null;

                    oNewSCT_UserBE.FullName = string.Format("{0} {1}", Convert.ToString(dr["FirstName"]), Convert.ToString(dr["Lastname"]));

                    oNewSCT_UserBE.Vendor = new BusinessEntities.ModuleBE.AdminFunctions.MAS_VendorBE();

                    if (ds.Tables[0].Columns.Contains("Vendor_Name"))
                        oNewSCT_UserBE.Vendor.Vendor_Name = dr["Vendor_Name"] != DBNull.Value ? dr["Vendor_Name"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("Vendor_No"))
                        oNewSCT_UserBE.Vendor.Vendor_No = dr["Vendor_No"] != DBNull.Value ? dr["Vendor_No"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("VendorEuropeanorLocal"))
                        oNewSCT_UserBE.Vendor.VendorEuropeanorLocal = dr["VendorEuropeanorLocal"] != DBNull.Value ? dr["VendorEuropeanorLocal"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("EUConsolidationCode"))
                        oNewSCT_UserBE.Vendor.EUConsolidationCode = dr["EUConsolidationCode"] != DBNull.Value ? dr["EUConsolidationCode"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("LocalConsolidationCode"))
                        oNewSCT_UserBE.Vendor.LocalConsolidationCode = dr["LocalConsolidationCode"] != DBNull.Value ? dr["LocalConsolidationCode"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("NumberofLines"))
                        oNewSCT_UserBE.Vendor.NumberofLines = dr["NumberofLines"] != DBNull.Value ? dr["NumberofLines"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("AssignedSite"))
                        oNewSCT_UserBE.Vendor.AssignedSite = dr["AssignedSite"] != DBNull.Value ? dr["AssignedSite"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("EnabledSite"))
                        oNewSCT_UserBE.Vendor.EnabledSite = dr["EnabledSite"] != DBNull.Value ? dr["EnabledSite"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("VendorID"))
                        oNewSCT_UserBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : (int?)null;

                    oNewSCT_UserBE.VendorPOC = new BusinessEntities.ModuleBE.AdminFunctions.MAS_MaintainVendorPointsContactBE();

                    if (ds.Tables[0].Columns.Contains("RoleTitle"))
                        oNewSCT_UserBE.VendorPOC.RoleTitle = dr["RoleTitle"] != DBNull.Value ? dr["RoleTitle"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("ExecutivePOC"))
                        oNewSCT_UserBE.VendorPOC.ExecutivePOC = dr["ExecutivePOC"] != DBNull.Value ? dr["ExecutivePOC"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("MerchandisingPOC"))
                        oNewSCT_UserBE.VendorPOC.MerchandisingPOC = dr["MerchandisingPOC"] != DBNull.Value ? dr["MerchandisingPOC"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("ProcurementPOC"))
                        oNewSCT_UserBE.VendorPOC.ProcurementPOC = dr["ProcurementPOC"] != DBNull.Value ? dr["ProcurementPOC"].ToString() : string.Empty;
                    if (ds.Tables[0].Columns.Contains("InventoryPOC"))
                        oNewSCT_UserBE.VendorPOC.InventoryPOC = dr["InventoryPOC"] != DBNull.Value ? dr["InventoryPOC"].ToString() : string.Empty;

                    if (ds.Tables[0].Columns.Contains("JobTitle"))
                        oNewSCT_UserBE.JobTitle = dr["JobTitle"] != DBNull.Value ? dr["JobTitle"].ToString() : string.Empty;

                    if (ds.Tables[0].Columns.Contains("InvoiceIssueContact"))
                        oNewSCT_UserBE.InvoiceIssueContact = dr["InvoiceIssueContact"] != DBNull.Value ? Convert.ToChar(dr["InvoiceIssueContact"]) : (char?)null;

                    SCT_UserBEList.Add(oNewSCT_UserBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;
        }

        public List<SCT_UserBE> GetUsers(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@VendorIds", oSCT_UserBE.VendorIDs);
                param[index++] = new SqlParameter("@AccountStatus", oSCT_UserBE.AccountStatus);
                param[index++] = new SqlParameter("@CountryID", oSCT_UserBE.CountryId);
                param[index++] = new SqlParameter("@UserRoleID", oSCT_UserBE.UserRoleID);
                param[index++] = new SqlParameter("@PageIndex", oSCT_UserBE.PageIndex);
                param[index++] = new SqlParameter("@PageSize", oSCT_UserBE.PageSize);
                param[index++] = new SqlParameter("@CarrierIds", oSCT_UserBE.CarrierIDs);
                param[index++] = new SqlParameter("@UserSearch", oSCT_UserBE.UserSearchText);
                param[index++] = new SqlParameter("@CountryIDs", oSCT_UserBE.SelectedSites);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();

                    oNewSCT_UserBE.UserID = Convert.ToInt32(dr["UserID"].ToString());
                    oNewSCT_UserBE.FirstName = dr["FirstName"].ToString();
                    oNewSCT_UserBE.Lastname = dr["Lastname"].ToString();
                    oNewSCT_UserBE.LoginID = dr["LoginID"].ToString();
                    oNewSCT_UserBE.PhoneNumber = dr["PhoneNumber"].ToString();
                    oNewSCT_UserBE.FaxNumber = dr["FaxNumber"].ToString();
                    //if (!string.IsNullOrEmpty(dr["VendorID"].ToString()))
                    //{
                    //    oNewSCT_UserBE.VendorID = Convert.ToInt32(dr["VendorID"].ToString());
                    //}
                    //if (!string.IsNullOrEmpty(dr["CarrierID"].ToString()))
                    //{
                    //    oNewSCT_UserBE.CarrierID = Convert.ToInt32(dr["CarrierID"].ToString());
                    //}
                    oNewSCT_UserBE.Language = dr["Language"].ToString();
                    oNewSCT_UserBE.RoleName = dr["RoleName"].ToString();

                    //oNewSCT_UserBE.VendorName = dr["Vendor_Name"].ToString();
                    //oNewSCT_UserBE.CarrierName = dr["CarrierName"].ToString();

                    oNewSCT_UserBE.SchedulingContact = dr["SchedulingContact"] != DBNull.Value ? Convert.ToChar(dr["SchedulingContact"]) : (char?)null;
                    oNewSCT_UserBE.ScorecardContact = dr["ScorecardContact"] != DBNull.Value ? Convert.ToChar(dr["ScorecardContact"]) : (char?)null;
                    oNewSCT_UserBE.DiscrepancyContact = dr["DiscrepancyContact"] != DBNull.Value ? Convert.ToChar(dr["DiscrepancyContact"]) : (char?)null;
                    oNewSCT_UserBE.OTIFContact = dr["OTIFContact"] != DBNull.Value ? Convert.ToChar(dr["OTIFContact"]) : (char?)null;

                    oNewSCT_UserBE.AccountStatus = dr["AccountStatus"].ToString();

                    oNewSCT_UserBE.LanguageID = dr["LanguageID"] != DBNull.Value ? Convert.ToInt32(dr["LanguageID"].ToString()) : (int?)null;
                    oNewSCT_UserBE.UserRoleID = dr["UserRoleID"] != DBNull.Value ? Convert.ToInt32(dr["UserRoleID"].ToString()) : (int?)null;
                    oNewSCT_UserBE.Password = dr["Password"].ToString();
                    oNewSCT_UserBE.IsDeliveryRefusalAuthorization = dr["DeliveryRefusalAuthorization"] != DBNull.Value ? Convert.ToInt16(dr["DeliveryRefusalAuthorization"]) : 0;
                    oNewSCT_UserBE.IsActAsMediator = dr["IsActAsMediator"] != DBNull.Value ? Convert.ToInt16(dr["IsActAsMediator"]) : 0;

                    oNewSCT_UserBE.MasterTemplateID = dr["MasterTemplateID"] != DBNull.Value ? Convert.ToInt32(dr["MasterTemplateID"].ToString()) : (int?)null;
                    oNewSCT_UserBE.ReportTemplateID = dr["ReportTemplateID"] != DBNull.Value ? Convert.ToInt32(dr["ReportTemplateID"].ToString()) : (int?)null;
                    //oNewSCT_UserBE.CountryId = dr["VendorCountryID"] != DBNull.Value ? Convert.ToInt32(dr["VendorCountryID"].ToString()) : (int?)null;
                    //oNewSCT_UserBE.CarrierCountryID = dr["CarrierCountryID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierCountryID"].ToString()) : (int?)null;
                    oNewSCT_UserBE.OtherCarrierName = dr["OtherCarrierName"].ToString();
                    oNewSCT_UserBE.RejectionComments = dr["RejectionComments"].ToString();
                    oNewSCT_UserBE.CratedDate = dr["CratedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CratedDate"].ToString()) : (DateTime?)null;
                    oNewSCT_UserBE.LastLoggedOn = dr["LastLoggedOn"] != DBNull.Value ? Convert.ToDateTime(dr["LastLoggedOn"].ToString()) : (DateTime?)null;
                    oNewSCT_UserBE.Elapseddays = dr["Elapseddays"] != DBNull.Value ? Convert.ToInt32(dr["Elapseddays"].ToString()) : (int?)null;

                    oNewSCT_UserBE.FullName = string.Format("{0} {1}", Convert.ToString(dr["FirstName"]), Convert.ToString(dr["Lastname"]));

                    if (dr.Table.Columns.Contains("JobTitle"))
                        oNewSCT_UserBE.JobTitle = Convert.ToString(dr["JobTitle"]);

                    if (dr.Table.Columns.Contains("StockPlannerGroupingsID"))
                    {
                        oNewSCT_UserBE.oStockPlannerGroupings = new StockPlannerGroupingsBE();
                        oNewSCT_UserBE.oStockPlannerGroupings.StockPlannerGroupingsID = dr["StockPlannerGroupingsID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerGroupingsID"].ToString()) : 0;
                    }

                    if (dr.Table.Columns.Contains("IsInitialDiscCommunicationToSP"))
                    {
                        oNewSCT_UserBE.Discrepancy = new BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyBE();
                        oNewSCT_UserBE.Discrepancy.IsInitialDiscCommunicationToSP = dr["IsInitialDiscCommunicationToSP"] != DBNull.Value ? Convert.ToBoolean(dr["IsInitialDiscCommunicationToSP"].ToString()) : false;
                    }

                    SCT_UserBEList.Add(oNewSCT_UserBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;
        }

        public List<SCT_UserBE> GetUserDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();

                    oNewSCT_UserBE.UserID = Convert.ToInt32(dr["UserID"].ToString());
                    oNewSCT_UserBE.FirstName = dr["FirstName"].ToString();
                    oNewSCT_UserBE.Lastname = dr["Lastname"].ToString();
                    oNewSCT_UserBE.LoginID = dr["LoginID"].ToString();
                    oNewSCT_UserBE.PhoneNumber = dr["PhoneNumber"].ToString();
                    oNewSCT_UserBE.FaxNumber = dr["FaxNumber"].ToString();
                    //if (!string.IsNullOrEmpty(dr["VendorID"].ToString()))
                    //{
                    //    oNewSCT_UserBE.VendorID = Convert.ToInt32(dr["VendorID"].ToString());
                    //}
                    //if (!string.IsNullOrEmpty(dr["CarrierID"].ToString()))
                    //{
                    //    oNewSCT_UserBE.CarrierID = Convert.ToInt32(dr["CarrierID"].ToString());
                    //}
                    oNewSCT_UserBE.Language = dr["Language"].ToString();
                    oNewSCT_UserBE.RoleName = dr["RoleName"].ToString();

                    //oNewSCT_UserBE.VendorNo = dr["Vendor_No"].ToString();
                    //oNewSCT_UserBE.VendorName = dr["Vendor_Name"].ToString();
                    //oNewSCT_UserBE.CarrierName = dr["CarrierName"].ToString();

                    //oNewSCT_UserBE.SchedulingContact = dr["SchedulingContact"] != DBNull.Value ? Convert.ToChar(dr["SchedulingContact"]) : (char?)null;
                    //oNewSCT_UserBE.ScorecardContact = dr["ScorecardContact"] != DBNull.Value ? Convert.ToChar(dr["ScorecardContact"]) : (char?)null;
                    //oNewSCT_UserBE.DiscrepancyContact = dr["DiscrepancyContact"] != DBNull.Value ? Convert.ToChar(dr["DiscrepancyContact"]) : (char?)null;
                    //oNewSCT_UserBE.OTIFContact = dr["OTIFContact"] != DBNull.Value ? Convert.ToChar(dr["OTIFContact"]) : (char?)null;
                    oNewSCT_UserBE.AccountStatus = dr["AccountStatus"].ToString();
                    //oNewSCT_UserBE.Country = dr["Vendor_Country"] != DBNull.Value ? Convert.ToString(dr["Vendor_Country"]) : "";
                    oNewSCT_UserBE.Password = dr["Password"] != DBNull.Value ? Convert.ToString(dr["Password"]) : "";
                    oNewSCT_UserBE.UserRoleID = dr["UserRoleID"] != DBNull.Value ? Convert.ToChar(dr["UserRoleID"]) : (int?)null;

                    SCT_UserBEList.Add(oNewSCT_UserBE);
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;
        }


        public int? addEditUserDAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[27];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@FirstName", oSCT_UserBE.FirstName);
                param[index++] = new SqlParameter("@Lastname", oSCT_UserBE.Lastname);
                param[index++] = new SqlParameter("@LoginID", oSCT_UserBE.LoginID);
                param[index++] = new SqlParameter("@Password", Common.EncryptPassword(oSCT_UserBE.Password));
                param[index++] = new SqlParameter("@EmailId", oSCT_UserBE.EmailId);
                param[index++] = new SqlParameter("@PhoneNumber", oSCT_UserBE.PhoneNumber);
                param[index++] = new SqlParameter("@FaxNumber", oSCT_UserBE.FaxNumber);
                param[index++] = new SqlParameter("@IsActive", oSCT_UserBE.IsActive);
                param[index++] = new SqlParameter("@UserRoleID", oSCT_UserBE.UserRoleID);
                param[index++] = new SqlParameter("@VendorID", oSCT_UserBE.VendorID);
                param[index++] = new SqlParameter("@CarrierID", oSCT_UserBE.CarrierID);
                param[index++] = new SqlParameter("@AccountsPayablePNumber", oSCT_UserBE.AccountsPayablePNumber);
                param[index++] = new SqlParameter("@DeliveryRefusalAuthorization", oSCT_UserBE.IsDeliveryRefusalAuthorization);
                param[index++] = new SqlParameter("@IsActAsMediator", oSCT_UserBE.IsActAsMediator);
                param[index++] = new SqlParameter("@SchedulingContact", oSCT_UserBE.SchedulingContact);
                param[index++] = new SqlParameter("@DiscrepancyContact", oSCT_UserBE.DiscrepancyContact);
                param[index++] = new SqlParameter("@OTIFContact", oSCT_UserBE.OTIFContact);
                param[index++] = new SqlParameter("@ScorecardContact", oSCT_UserBE.ScorecardContact);
                param[index++] = new SqlParameter("@LanguageID", oSCT_UserBE.LanguageID);
                param[index++] = new SqlParameter("@AccountStatus", oSCT_UserBE.AccountStatus);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", oSCT_UserBE.SelectedStockPlannerIds);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@OtherCarrierName", oSCT_UserBE.OtherCarrierName);
                param[index++] = new SqlParameter("@JobTitle", oSCT_UserBE.JobTitle);
                param[index++] = new SqlParameter("@StockPlannerGroupingsID", oSCT_UserBE.oStockPlannerGroupings.StockPlannerGroupingsID);

                if (oSCT_UserBE.Discrepancy != null)
                {
                    param[index++] = new SqlParameter("@IsInitialDiscCommunicationToSP", oSCT_UserBE.Discrepancy.IsInitialDiscCommunicationToSP);
                }
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateUserDAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[23];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@FirstName", oSCT_UserBE.FirstName);
                param[index++] = new SqlParameter("@Lastname", oSCT_UserBE.Lastname);
                param[index++] = new SqlParameter("@LoginID", oSCT_UserBE.LoginID);

                param[index++] = new SqlParameter("@EmailId", oSCT_UserBE.EmailId);
                param[index++] = new SqlParameter("@PhoneNumber", oSCT_UserBE.PhoneNumber);
                param[index++] = new SqlParameter("@FaxNumber", oSCT_UserBE.FaxNumber);
                param[index++] = new SqlParameter("@UserRoleID", oSCT_UserBE.UserRoleID);
                param[index++] = new SqlParameter("@DeliveryRefusalAuthorization", oSCT_UserBE.IsDeliveryRefusalAuthorization);
                param[index++] = new SqlParameter("@IsActAsMediator", oSCT_UserBE.IsActAsMediator);
                param[index++] = new SqlParameter("@LanguageID", oSCT_UserBE.LanguageID);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", oSCT_UserBE.SelectedStockPlannerIds);

                param[index++] = new SqlParameter("@SchedulingContact", oSCT_UserBE.SchedulingContact);
                param[index++] = new SqlParameter("@DiscrepancyContact", oSCT_UserBE.DiscrepancyContact);
                param[index++] = new SqlParameter("@OTIFContact", oSCT_UserBE.OTIFContact);
                param[index++] = new SqlParameter("@ScorecardContact", oSCT_UserBE.ScorecardContact);

                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@OtherCarrierName", oSCT_UserBE.OtherCarrierName);
                param[index++] = new SqlParameter("@CarrierID", oSCT_UserBE.CarrierID);
                param[index++] = new SqlParameter("@JobTitle", oSCT_UserBE.JobTitle);
                param[index++] = new SqlParameter("@StockPlannerGroupingsID", oSCT_UserBE.oStockPlannerGroupings.StockPlannerGroupingsID);
                param[index++] = new SqlParameter("@IsInitialDiscCommunicationToSP", oSCT_UserBE.Discrepancy.IsInitialDiscCommunicationToSP);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


        public List<SCT_UserBE> GetLanguagesDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {

                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.LanguageID = Convert.ToInt32(dr["LanguageID"]);
                    oNewSCT_UserBE.Language = dr["Language"].ToString();
                    oNewSCT_UserBE.ISOLanguageName = dr["ISOLanguageName"].ToString();

                    SCT_UserBEList.Add(oNewSCT_UserBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;

        }

        public List<SCT_UserBE> GetStockPlannerDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@CountryID", oSCT_UserBE.CountryId);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {

                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.StockPlannerId = Convert.ToInt32(dr["StockPlannerID"]);
                    oNewSCT_UserBE.StockPlannerCountry = dr["StockPlannerCountry"].ToString();
                    oNewSCT_UserBE.CountryId = Convert.ToInt32(dr["CountryID"]);

                    SCT_UserBEList.Add(oNewSCT_UserBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;

        }



        public int? UpdateAccountStatus(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                oSCT_UserBE.Password = Common.EncryptPassword(oSCT_UserBE.Password);
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@AccountStatus", oSCT_UserBE.AccountStatus);
                param[index++] = new SqlParameter("@RejectionComments", oSCT_UserBE.RejectionComments);
                param[index++] = new SqlParameter("@LoginID", oSCT_UserBE.LoginID);
                param[index++] = new SqlParameter("@Password", oSCT_UserBE.Password);
                param[index++] = new SqlParameter("@Loggedin_Browser", oSCT_UserBE.Loggedin_Browser);
                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateUserTemplatesDAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@MasterTemplateID", oSCT_UserBE.MasterTemplateID);
                param[index++] = new SqlParameter("@ReportTemplateID", oSCT_UserBE.ReportTemplateID);
                param[index++] = new SqlParameter("@AccountStatus", oSCT_UserBE.AccountStatus);


                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


        public int? AddUserSitesDAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@IsDefaultSite", oSCT_UserBE.IsDefaultSite);
                param[index++] = new SqlParameter("@TemplateID", oSCT_UserBE.TemplateID);
                param[index++] = new SqlParameter("@SiteID", oSCT_UserBE.SiteId);


                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public void UpdateCarrierActiveSites(SCT_UserBE oSCT_UserBE)
        {
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@SelectedCarrierSites", oSCT_UserBE.SelectedSites);

                SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }
        public DataTable GetCarrierActiveSitesDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dtVendorActiveSites = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);

                DataSet dsVendorActiveSites = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                if (dsVendorActiveSites != null && dsVendorActiveSites.Tables.Count > 0)
                    dtVendorActiveSites = dsVendorActiveSites.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dtVendorActiveSites;
        }

        public bool IsVendorSiteExist(SCT_UserBE oNewoSCT_UserBE)
        {
            bool VendorSiteExist = false;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oNewoSCT_UserBE.Action);
                param[index++] = new SqlParameter("@VendorID", oNewoSCT_UserBE.VendorID);

                DataSet dsVendorActiveSites = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                if (dsVendorActiveSites != null && dsVendorActiveSites.Tables.Count > 0)
                {
                    DataTable dtVendorActiveSites = dsVendorActiveSites.Tables[0];
                    if (dtVendorActiveSites.Rows.Count > 0)
                        VendorSiteExist = true;
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return VendorSiteExist;
        }
        public bool IsCarrierSiteExist(SCT_UserBE oNewoSCT_UserBE)
        {
            bool CarrierSiteExist = false;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oNewoSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oNewoSCT_UserBE.UserID);

                DataSet dsVendorActiveSites = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                if (dsVendorActiveSites != null && dsVendorActiveSites.Tables.Count > 0)
                {
                    DataTable dtVendorActiveSites = dsVendorActiveSites.Tables[0];
                    if (dtVendorActiveSites.Rows.Count > 0)
                        CarrierSiteExist = true;
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return CarrierSiteExist;
        }

        public DataTable GetAdminDashboardData(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@AccountStatus", oSCT_UserBE.AccountStatus);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);

                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }
        public DataTable GetStockPlannerWithCountryDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@UserSearch", oSCT_UserBE.UserSearchText);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                if (ds != null && ds.Tables.Count > 0)
                    dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public DataTable GetStockPlannerWithCountryByUserIdsDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserIDs", oSCT_UserBE.UserIds);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                if (ds != null && ds.Tables.Count > 0)
                    dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public DataTable GetStockPlannerByCountryIdDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@UserSearch", oSCT_UserBE.UserSearchText);
                param[index++] = new SqlParameter("@CountryID", oSCT_UserBE.CountryId);
                param[index++] = new SqlParameter("@VendorIds", oSCT_UserBE.VendorIDs);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                if (ds != null && ds.Tables.Count > 0)
                    dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }
        public bool CheckForUserExistance(SCT_UserBE oSCT_UserBE)
        {
            bool IsUserExist = false;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@EmailId", oSCT_UserBE.EmailId);

                DataSet dsIsUserExist = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                if (dsIsUserExist != null && dsIsUserExist.Tables.Count > 0)
                {
                    DataTable dtIsUserExist = dsIsUserExist.Tables[0];
                    if (dtIsUserExist.Rows.Count > 0)
                        IsUserExist = true;
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return IsUserExist;
        }

        public DataSet GetLanguageIdDAL(SCT_UserBE oSCT_UserBE)
        {
            DataSet dsLanguage = new DataSet();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@EmailId", oSCT_UserBE.EmailId);

                dsLanguage = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                if (dsLanguage != null && dsLanguage.Tables.Count > 0)
                {
                    return dsLanguage;
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dsLanguage;
        }

        public List<SCT_UserBE> GetAPContactDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@VendorId", oSCT_UserBE.VendorID);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();

                    oNewSCT_UserBE.Lastname = dr["Lastname"] != DBNull.Value ? dr["Lastname"].ToString() : "";
                    oNewSCT_UserBE.FirstName = dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString() : "";
                    oNewSCT_UserBE.PhoneNumber = dr["PhoneNumber"] != DBNull.Value ? dr["PhoneNumber"].ToString() : "";

                    SCT_UserBEList.Add(oNewSCT_UserBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;
        }


        public List<SCT_UserBE> GetUserEmailByIDDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {

                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.EmailId = dr["EmailId"].ToString();
                    oNewSCT_UserBE.Language = dr["Language"].ToString();

                    SCT_UserBEList.Add(oNewSCT_UserBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;

        }
        public DataTable GetValidlinksForUserDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);

                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        // Sprint 1 - Point 8 - Start
        public List<SCT_UserBE> GetManageDeliveryUsersDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                var sqlParams = new[]
                {
                    new SqlParameter("@Action", oSCT_UserBE.Action),
                    new SqlParameter("@SiteId", oSCT_UserBE.SiteId)
                };
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", sqlParams);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();

                    oNewSCT_UserBE.UserID = Convert.ToInt32(dr["UserID"]);
                    oNewSCT_UserBE.LoginID = dr["LoginID"].ToString();
                    oNewSCT_UserBE.FullName = dr["FirstName"].ToString() + " " + dr["Lastname"].ToString();
                    oNewSCT_UserBE.FirstName = dr["FirstName"].ToString();
                    oNewSCT_UserBE.Lastname = dr["Lastname"].ToString();
                    oNewSCT_UserBE.EmailId = dr["EmailId"].ToString();
                    oNewSCT_UserBE.PhoneNumber = dr["PhoneNumber"].ToString();
                    oNewSCT_UserBE.FaxNumber = dr["FaxNumber"].ToString();
                    oNewSCT_UserBE.UserRoleID = dr["UserRoleID"] != DBNull.Value ? Convert.ToInt32(dr["UserRoleID"]) : (int?)null;
                    oNewSCT_UserBE.AccountStatus = dr["AccountStatus"].ToString();
                    oNewSCT_UserBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : (int?)null;
                    oNewSCT_UserBE.CarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"]) : (int?)null;

                    SCT_UserBEList.Add(oNewSCT_UserBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;
        }
        // Sprint 1 - Point 8 - End

        // Sprint 1 - Point 9 - Start
        public int? GetTodayRefusedDeliveriesCount(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[23];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        // Sprint 1 - Point 9 - End

        public List<SCT_UserBE> GetCountrySpecificSettingsDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                var sqlParams = new[]
                {
                    new SqlParameter("@Action", oSCT_UserBE.Action),
                    new SqlParameter("@UserID", oSCT_UserBE.UserID),
                    new SqlParameter("@UpdatedRoleBy", oSCT_UserBE.UpdatedRoleBy)
                };
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", sqlParams);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();

                    oNewSCT_UserBE.UserID = dr["UserID"] != DBNull.Value ? Convert.ToInt32(dr["UserID"]) : 0;
                    oNewSCT_UserBE.CountryId = dr["CountryId"] != DBNull.Value ? Convert.ToInt32(dr["CountryId"]) : 0;
                    oNewSCT_UserBE.Country = dr["CountryName"] != DBNull.Value ? Convert.ToString(dr["CountryName"]) : string.Empty;
                    oNewSCT_UserBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : (int?)null;
                    oNewSCT_UserBE.VendorName = dr["Vendor_Name"] != DBNull.Value ? Convert.ToString(dr["Vendor_Name"]) : string.Empty;
                    oNewSCT_UserBE.CarrierID = dr["CarrierID"] != DBNull.Value ? Convert.ToInt32(dr["CarrierID"]) : (int?)null;
                    oNewSCT_UserBE.CarrierName = dr["CarrierName"] != DBNull.Value ? Convert.ToString(dr["CarrierName"]) : string.Empty;
                    oNewSCT_UserBE.SiteId = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : (int?)null;
                    oNewSCT_UserBE.SiteName = dr["SiteName"] != DBNull.Value ? Convert.ToString(dr["SiteName"]) : string.Empty;
                    oNewSCT_UserBE.SchedulingContact = dr["SchedulingContact"] != DBNull.Value ? Convert.ToChar(dr["SchedulingContact"]) : (char?)null;
                    oNewSCT_UserBE.DiscrepancyContact = dr["DiscrepancyContact"] != DBNull.Value ? Convert.ToChar(dr["DiscrepancyContact"]) : (char?)null;
                    oNewSCT_UserBE.OTIFContact = dr["OTIFContact"] != DBNull.Value ? Convert.ToChar(dr["OTIFContact"]) : (char?)null;
                    oNewSCT_UserBE.ScorecardContact = dr["ScorecardContact"] != DBNull.Value ? Convert.ToChar(dr["ScorecardContact"]) : (char?)null;
                    if (ds.Tables[0].Columns.Contains("InvoiceIssueContact"))
                        oNewSCT_UserBE.InvoiceIssueContact = dr["InvoiceIssueContact"] != DBNull.Value ? Convert.ToChar(dr["InvoiceIssueContact"]) : (char?)null;
                    SCT_UserBEList.Add(oNewSCT_UserBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;
        }

        public int UpdateCountriesDAL(SCT_UserBE oSCT_UserBE)
        {
            int? iResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@CountryIDs", oSCT_UserBE.CountryIds);
                param[index++] = new SqlParameter("@UpdatedRoleBy", oSCT_UserBE.UpdatedRoleBy);
                iResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return iResult.Value;
        }

        public int UpdateModulesDAL(SCT_UserBE oSCT_UserBE)
        {
            int? iResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@AppointmentSites", oSCT_UserBE.AppointmentSites);
                param[index++] = new SqlParameter("@DiscrepancySites", oSCT_UserBE.DiscrepancySites);
                param[index++] = new SqlParameter("@InvoiceIssueSites", oSCT_UserBE.InvoiceIssueSites);
                param[index++] = new SqlParameter("@OTIFCountries", oSCT_UserBE.OTIFCountries);
                param[index++] = new SqlParameter("@ScorecardCountries", oSCT_UserBE.ScorecardCountries);
                param[index++] = new SqlParameter("@UpdatedRoleBy", oSCT_UserBE.UpdatedRoleBy);
                iResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return iResult.Value;
        }

        public int UpdateUserCountryDAL(SCT_UserBE oSCT_UserBE)
        {
            int? iResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                param[index++] = new SqlParameter("@CountryID", oSCT_UserBE.CountryId);
                param[index++] = new SqlParameter("@VendorID", oSCT_UserBE.VendorID);
                param[index++] = new SqlParameter("@CarrierID", oSCT_UserBE.CarrierID);
                param[index++] = new SqlParameter("@UpdatedRoleBy", oSCT_UserBE.UpdatedRoleBy);
                iResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return iResult.Value;
        }

        public int? RegisterNewUserDAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@FirstName", oSCT_UserBE.FirstName);
                param[index++] = new SqlParameter("@Lastname", oSCT_UserBE.Lastname);
                param[index++] = new SqlParameter("@LoginID", oSCT_UserBE.LoginID);
                param[index++] = new SqlParameter("@Password", Common.EncryptPassword(oSCT_UserBE.Password));
                param[index++] = new SqlParameter("@EmailId", oSCT_UserBE.EmailId);
                param[index++] = new SqlParameter("@PhoneNumber", oSCT_UserBE.PhoneNumber);
                param[index++] = new SqlParameter("@FaxNumber", oSCT_UserBE.FaxNumber);
                param[index++] = new SqlParameter("@IsActive", oSCT_UserBE.IsActive);
                param[index++] = new SqlParameter("@UserRoleID", oSCT_UserBE.UserRoleID);
                param[index++] = new SqlParameter("@AccountStatus", oSCT_UserBE.AccountStatus);
                param[index++] = new SqlParameter("@LanguageID", oSCT_UserBE.LanguageID);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<SCT_UserBE> GetAllUsersDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                var sqlParams = new[]
                {
                    new SqlParameter("@Action", oSCT_UserBE.Action),
                    new SqlParameter("@UserSearch", oSCT_UserBE.FirstName)
                };
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", sqlParams);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.UserID = dr["UserID"] != DBNull.Value ? Convert.ToInt32(dr["UserID"]) : 0;
                    oNewSCT_UserBE.FirstName = dr["FirstName"] != DBNull.Value ? Convert.ToString(dr["FirstName"]) : string.Empty;
                    oNewSCT_UserBE.Lastname = dr["Lastname"] != DBNull.Value ? Convert.ToString(dr["Lastname"]) : string.Empty;
                    oNewSCT_UserBE.UserName = dr["UserName"] != DBNull.Value ? Convert.ToString(dr["UserName"]) : string.Empty;
                    SCT_UserBEList.Add(oNewSCT_UserBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;
        }

        public int? IsValidEmailIdDAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@EmailId", oSCT_UserBE.EmailId);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteUserPendingRejectedDataDAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<SCT_UserBE> GetCommunicationUpdateVendorsDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                var sqlParams = new[]
                {
                    new SqlParameter("@Action", oSCT_UserBE.Action),
                    new SqlParameter("@CountryIDs", oSCT_UserBE.CountryIds),
                    new SqlParameter("@VendorIDs", oSCT_UserBE.VendorIDs),
                    new SqlParameter("@Roles", oSCT_UserBE.Roles),
                    new SqlParameter("@EuropeanLocal", oSCT_UserBE.EuropeanLocal)
                };
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", sqlParams);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.Country = dr["Country"] != DBNull.Value ? Convert.ToString(dr["Country"]) : string.Empty;
                    oNewSCT_UserBE.VendorNo = dr["VendorNo"] != DBNull.Value ? Convert.ToString(dr["VendorNo"]) : string.Empty;
                    oNewSCT_UserBE.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                    oNewSCT_UserBE.UserName = dr["UserName"] != DBNull.Value ? Convert.ToString(dr["UserName"]) : string.Empty;
                    oNewSCT_UserBE.UserID = dr["UserID"] != DBNull.Value ? Convert.ToInt32(dr["UserID"]) : 0;
                    oNewSCT_UserBE.EmailId = dr["UserEmail"] != DBNull.Value ? Convert.ToString(dr["UserEmail"]) : string.Empty;
                    oNewSCT_UserBE.RowNum = dr["RowNum"] != DBNull.Value ? Convert.ToInt32(dr["RowNum"]) : 0;
                    SCT_UserBEList.Add(oNewSCT_UserBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;
        }

        public List<SCT_UserBE> GetCommunicationUpdateCarriersDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                var sqlParams = new[]
                {
                    new SqlParameter("@Action", oSCT_UserBE.Action),
                    new SqlParameter("@CountryIDs", oSCT_UserBE.CountryIds),
                    new SqlParameter("@CarrierIDs", oSCT_UserBE.CarrierIDs)
                };
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", sqlParams);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.Country = dr["CountryName"] != DBNull.Value ? Convert.ToString(dr["CountryName"]) : string.Empty;
                    oNewSCT_UserBE.CarrierName = dr["CarrierName"] != DBNull.Value ? Convert.ToString(dr["CarrierName"]) : string.Empty;
                    oNewSCT_UserBE.UserName = dr["UserName"] != DBNull.Value ? Convert.ToString(dr["UserName"]) : string.Empty;
                    oNewSCT_UserBE.UserID = dr["UserID"] != DBNull.Value ? Convert.ToInt32(dr["UserID"]) : 0;
                    oNewSCT_UserBE.EmailId = dr["UserEmail"] != DBNull.Value ? Convert.ToString(dr["UserEmail"]) : string.Empty;
                    oNewSCT_UserBE.RowNum = dr["RowNum"] != DBNull.Value ? Convert.ToInt32(dr["RowNum"]) : 0;
                    SCT_UserBEList.Add(oNewSCT_UserBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;
        }

        public List<SCT_UserBE> GetCommunicationUpdateOfficeDepotDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                var sqlParams = new[]
                {
                    new SqlParameter("@Action", oSCT_UserBE.Action),
                    new SqlParameter("@CountryIDs", oSCT_UserBE.CountryIds),
                    new SqlParameter("@Roles", oSCT_UserBE.Roles),
                    new SqlParameter("@SelectedStockPlannerIds",oSCT_UserBE.SelectedStockPlannerIds)
                };
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", sqlParams);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.Country = dr["CountryName"] != DBNull.Value ? Convert.ToString(dr["CountryName"]) : string.Empty;
                    oNewSCT_UserBE.UserName = dr["UserName"] != DBNull.Value ? Convert.ToString(dr["UserName"]) : string.Empty;
                    oNewSCT_UserBE.UserID = dr["UserID"] != DBNull.Value ? Convert.ToInt32(dr["UserID"]) : 0;
                    oNewSCT_UserBE.EmailId = dr["UserEmail"] != DBNull.Value ? Convert.ToString(dr["UserEmail"]) : string.Empty;
                    oNewSCT_UserBE.RoleName = dr["RoleName"] != DBNull.Value ? Convert.ToString(dr["RoleName"]) : string.Empty;
                    oNewSCT_UserBE.RowNum = dr["RowNum"] != DBNull.Value ? Convert.ToInt32(dr["RowNum"]) : 0;
                    SCT_UserBEList.Add(oNewSCT_UserBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;
        }

        public List<SCT_UserBE> GetODMediatorDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@MediatorName", oSCT_UserBE.FirstName);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {

                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.UserID = Convert.ToInt32(dr["UserID"]);
                    oNewSCT_UserBE.FirstName = dr["ODMediatorName"] != DBNull.Value ? Convert.ToString(dr["ODMediatorName"]) : string.Empty;
                    oNewSCT_UserBE.Lastname = dr["Lastname"] != DBNull.Value ? Convert.ToString(dr["Lastname"]) : string.Empty;
                    oNewSCT_UserBE.LoginID = dr["LoginID"] != DBNull.Value ? Convert.ToString(dr["LoginID"]) : string.Empty;
                    oNewSCT_UserBE.UserRoleID = dr["UserRoleID"] != DBNull.Value ? Convert.ToInt32(dr["UserRoleID"]) : 0;
                    oNewSCT_UserBE.LanguageID = dr["LanguageID"] != DBNull.Value ? Convert.ToInt32(dr["LanguageID"]) : 0;

                    SCT_UserBEList.Add(oNewSCT_UserBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;

        }


        public List<SCT_UserBE> GetSPBySPNumberDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@StockPlannerNumber", oSCT_UserBE.StockPlannerNumber);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {

                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
                    oNewSCT_UserBE.UserID = Convert.ToInt32(dr["UserID"]);
                    oNewSCT_UserBE.StockPlannerId = Convert.ToInt32(dr["StockPlannerID"]);
                    oNewSCT_UserBE.FirstName = dr["StockPlanner"] != DBNull.Value ? Convert.ToString(dr["StockPlanner"]) : string.Empty;
                    oNewSCT_UserBE.Lastname = dr["Lastname"] != DBNull.Value ? Convert.ToString(dr["Lastname"]) : string.Empty;

                    SCT_UserBEList.Add(oNewSCT_UserBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;

        }


        public DataSet GetSPNumberReportDAL(SCT_UserBE oSCT_UserBE)
        {
            DataSet ds = new DataSet();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@SelectedStockPlannerIds", oSCT_UserBE.SelectedStockPlannerIds);
                param[index++] = new SqlParameter("@CountryIDs", oSCT_UserBE.CountryIds);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.SPNumberUserIDs);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return ds;

        }

        public int? UpdateUsersStatusDAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                oSCT_UserBE.Password = Common.EncryptPassword(oSCT_UserBE.Password);
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserIDs", oSCT_UserBE.UserIds);
                param[index++] = new SqlParameter("@UserID", oSCT_UserBE.UserID);
                intResult = SqlHelper.ExecuteNonQuery(DBConnection.Connection, CommandType.StoredProcedure, "spSCT_User", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public List<SCT_UserBE> GetLastLoginUsersDAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oSCT_UserBE.Action);
                param[index++] = new SqlParameter("@UserIds", oSCT_UserBE.UserIds);
                param[index++] = new SqlParameter("@LastLogindays", oSCT_UserBE.LastLoginDays);
                param[index++] = new SqlParameter("@CountryID", oSCT_UserBE.CountryId);
                param[index++] = new SqlParameter("@UserSearch", oSCT_UserBE.UserSearchText);
                param[index++] = new SqlParameter("@UserRoleID", oSCT_UserBE.UserRoleID);
                param[index++] = new SqlParameter("@ReportType", oSCT_UserBE.ReportType);
                param[index++] = new SqlParameter("@PageIndex", oSCT_UserBE.PageIndex);
                param[index++] = new SqlParameter("@PageSize", oSCT_UserBE.PageSize);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_User", param);

                dt = ds.Tables.Count > 0 ? ds.Tables[0] : null;

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserBE oNewSCT_UserBE = new SCT_UserBE(); 

                    if (ds.Tables[0].Columns.Contains("UserID"))
                        oNewSCT_UserBE.UserID = dr["UserID"] != DBNull.Value ? Convert.ToInt32(dr["UserID"].ToString()) : 0;

                    if (ds.Tables[0].Columns.Contains("FirstName"))
                        oNewSCT_UserBE.FirstName = dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString() : string.Empty;

                    if (ds.Tables[0].Columns.Contains("Lastname"))
                        oNewSCT_UserBE.Lastname = dr["Lastname"] != DBNull.Value ? dr["Lastname"].ToString() : string.Empty;

                    if (ds.Tables[0].Columns.Contains("LoginID"))
                        oNewSCT_UserBE.LoginID = dr["LoginID"] != DBNull.Value ? dr["LoginID"].ToString() : "--NA--";

                    if (ds.Tables[0].Columns.Contains("UserRoleID"))
                        oNewSCT_UserBE.UserRoleID = dr["UserRoleID"] != DBNull.Value ?
                            Convert.ToInt32(dr["UserRoleID"].ToString()) : 0;

                    oNewSCT_UserBE.Vendor = new BusinessEntities.ModuleBE.AdminFunctions.MAS_VendorBE();
                    if (ds.Tables[0].Columns.Contains("VendorId"))
                        oNewSCT_UserBE.Vendor.VendorID = dr["VendorId"] != DBNull.Value ?
                            Convert.ToInt32(dr["VendorId"].ToString()) : 0;

                    if (ds.Tables[0].Columns.Contains("RoleName"))
                        oNewSCT_UserBE.RoleName = dr["RoleName"] != DBNull.Value ?
                            dr["RoleName"].ToString() : string.Empty;

                    if (ds.Tables[0].Columns.Contains("AccountStatus"))
                        oNewSCT_UserBE.AccountStatus = dr["AccountStatus"] != DBNull.Value ? dr["AccountStatus"].ToString() : string.Empty;
  
                    if (ds.Tables[0].Columns.Contains("Elapseddays"))
                        oNewSCT_UserBE.Elapseddays = dr["Elapseddays"] != DBNull.Value ? Convert.ToInt32(dr["Elapseddays"].ToString()) : (int?)null;

                    oNewSCT_UserBE.FullName = string.Format("{0} {1}", Convert.ToString(dr["FirstName"]), Convert.ToString(dr["Lastname"]));

                    if (ds.Tables[0].Columns.Contains("TotalRecords"))
                        oNewSCT_UserBE.TotalRecords = dr["TotalRecords"] != DBNull.Value ? 
                            Convert.ToInt32(dr["TotalRecords"].ToString()) : 0;

                    SCT_UserBEList.Add(oNewSCT_UserBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserBEList;
        }

        public DataSet GetPointOfContactDAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            DataSet ds = new DataSet();
            try
            {
                DataTable dt = new DataTable();                  
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oPointsofContactSummaryBE.Action);
                param[index++] = new SqlParameter("@CountryID", oPointsofContactSummaryBE.CountryID);
                param[index++] = new SqlParameter("@Vendorsids", oPointsofContactSummaryBE.VendorIDs);

                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), 
                    CommandType.StoredProcedure, "spSCT_User", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return ds;

        }

    }
}

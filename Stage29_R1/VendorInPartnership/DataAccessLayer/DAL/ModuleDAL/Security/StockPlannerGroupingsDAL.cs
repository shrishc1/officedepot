﻿using BusinessEntities.ModuleBE.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Security
{
    public class StockPlannerGroupingsDAL
    {

        public List<StockPlannerGroupingsBE> GetStockPlannerGroupingsDAL(StockPlannerGroupingsBE oStockPlannerGroupingsBE)
        {
            DataTable dt = new DataTable();
            List<StockPlannerGroupingsBE> StockPlannerGroupingsBEList = new List<StockPlannerGroupingsBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oStockPlannerGroupingsBE.Action);
                param[index++] = new SqlParameter("@StockPlannerGroupingsID", oStockPlannerGroupingsBE.StockPlannerGroupingsID);
                param[index++] = new SqlParameter("@UserID", Convert.ToInt32(System.Web.HttpContext.Current.Session["UserID"]));

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_StockPlannerGroupings", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    StockPlannerGroupingsBE oNewStockPlannerGroupingsBE = new StockPlannerGroupingsBE();

                    oNewStockPlannerGroupingsBE.oUserBE = new SCT_UserBE();

                    if (dr.Table.Columns.Contains("StockPlannerGroupingsID"))
                        oNewStockPlannerGroupingsBE.StockPlannerGroupingsID = dr["StockPlannerGroupingsID"] != DBNull.Value ? Convert.ToInt32(dr["StockPlannerGroupingsID"]) : 0;

                    if (dr.Table.Columns.Contains("StockPlannerGroupings"))
                        oNewStockPlannerGroupingsBE.StockPlannerGroupings = dr["StockPlannerGroupings"] != DBNull.Value ? dr["StockPlannerGroupings"].ToString() : string.Empty;

                    if (dr.Table.Columns.Contains("UserName"))
                        oNewStockPlannerGroupingsBE.oUserBE.UserName = dr["UserName"] != DBNull.Value ? Convert.ToString(dr["UserName"]) : string.Empty;

                    if (dr.Table.Columns.Contains("UserCount"))
                        oNewStockPlannerGroupingsBE.UserCount = dr["UserCount"] != DBNull.Value ? Convert.ToInt32(dr["UserCount"]) : 0;

                    StockPlannerGroupingsBEList.Add(oNewStockPlannerGroupingsBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return StockPlannerGroupingsBEList;
        }




        public int? addEditStockPlannerGroupingsDAL(StockPlannerGroupingsBE StockPlannerGroupingsBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", StockPlannerGroupingsBE.Action);
                param[index++] = new SqlParameter("@StockPlannerGroupings", StockPlannerGroupingsBE.StockPlannerGroupings);
                param[index++] = new SqlParameter("@UserID", StockPlannerGroupingsBE.oUserBE.UserID);
                param[index++] = new SqlParameter("@StockPlannerGroupingsID", StockPlannerGroupingsBE.StockPlannerGroupingsID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_StockPlannerGroupings", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public void ExpediteTrackerReportDetailsVendorDAL(StockPlannerGroupingsBE oStockPlannerGroupingsBE, out List<StockPlannerGroupingsBE> oStockPlannerGroupingsBEList, out int RecordCount)
        {
            DataTable dt = new DataTable();
            List<StockPlannerGroupingsBE> oStockPlannerGroupingsBELists = new List<StockPlannerGroupingsBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[13];
                param[index++] = new SqlParameter("@Action", oStockPlannerGroupingsBE.Action);
                param[index++] = new SqlParameter("@GroupingIDs", oStockPlannerGroupingsBE.StockPlannerGroupingIDs);
                param[index++] = new SqlParameter("@SiteIds", oStockPlannerGroupingsBE.SiteIDs);
                param[index++] = new SqlParameter("@VendorIDs", oStockPlannerGroupingsBE.VendorIDs);
                param[index++] = new SqlParameter("@ItemClassification", oStockPlannerGroupingsBE.ItemClassification);
                param[index++] = new SqlParameter("@StockPlannerIDs", oStockPlannerGroupingsBE.StockPlannerIDs);
                param[index++] = new SqlParameter("@PageCount", oStockPlannerGroupingsBE.PageCount);
                param[index++] = new SqlParameter("@CountryID", oStockPlannerGroupingsBE.CountryID);
                param[index++] = new SqlParameter("@SiteId", oStockPlannerGroupingsBE.SiteID);
                param[index++] = new SqlParameter("@StockPlannerName", oStockPlannerGroupingsBE.StockPlanner);
                param[index++] = new SqlParameter("@DateTo", oStockPlannerGroupingsBE.DateTo);
                param[index++] = new SqlParameter("@DateFrom", oStockPlannerGroupingsBE.Datefrom);
                param[index++] = new SqlParameter("@RecordCount", SqlDbType.Int);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spExpediteTracker", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    oStockPlannerGroupingsBE = new StockPlannerGroupingsBE();
                    oStockPlannerGroupingsBE.StockPlanner = dr["StockPlannerName"] != DBNull.Value ? Convert.ToString(dr["StockPlannerName"]) : string.Empty;
                    oStockPlannerGroupingsBE.StockPlannerGrouping = dr["Grouping"] != DBNull.Value ? Convert.ToString(dr["Grouping"]) : string.Empty;
                    oStockPlannerGroupingsBE.SiteName = dr["sitename"] != DBNull.Value ? Convert.ToString(dr["sitename"]) : string.Empty;
                    oStockPlannerGroupingsBE.VendorName = dr["VendorName"] != DBNull.Value ? Convert.ToString(dr["VendorName"]) : string.Empty;
                    oStockPlannerGroupingsBE.CountryID = dr["CountryID"] != DBNull.Value ? Convert.ToInt32(dr["CountryID"]) : 0;
                    oStockPlannerGroupingsBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    oStockPlannerGroupingsBE.TotalLines = dr["TotalLines"] != DBNull.Value ? Convert.ToInt32(dr["TotalLines"]) : 0;
                    oStockPlannerGroupingsBE.AtRisk = dr["Risk"] != DBNull.Value ? Convert.ToInt32(dr["Risk"]) : 0;
                    oStockPlannerGroupingsBE.Comment = dr["Comment"] != DBNull.Value ? Convert.ToInt32(dr["Comment"]) : 0;
                    oStockPlannerGroupingsBE.Expedited = dr["ExpediteComment"] != DBNull.Value ? Convert.ToInt32(dr["ExpediteComment"]) : 0;
                    oStockPlannerGroupingsBE.Per_Expedited = dr["PercentExpedite"] != DBNull.Value ? Convert.ToDecimal(dr["PercentExpedite"]) : 0;
                    oStockPlannerGroupingsBE.Unavoidable = dr["UnAvoidable"] != DBNull.Value ? Convert.ToInt32(dr["UnAvoidable"]) : 0;
                    oStockPlannerGroupingsBE.UnavoidableComment = dr["UnAvoidableComment"] != DBNull.Value ? Convert.ToInt32(dr["UnAvoidableComment"]) : 0;
                    oStockPlannerGroupingsBE.UnavoidableExpedited = dr["UnAvoidableExpediteComment"] != DBNull.Value ? Convert.ToInt32(dr["UnAvoidableExpediteComment"]) : 0;
                    oStockPlannerGroupingsBE.UnvoidablePer_Expedited = dr["PercentExpediteUnAvoidable"] != DBNull.Value ? Convert.ToDecimal(dr["PercentExpediteUnAvoidable"]) : 0;
                    oStockPlannerGroupingsBE.Avoidable = dr["Avoidable"] != DBNull.Value ? Convert.ToInt32(dr["Avoidable"]) : 0;
                    oStockPlannerGroupingsBE.AvoidableComment = dr["AvoidableComment"] != DBNull.Value ? Convert.ToInt32(dr["AvoidableComment"]) : 0;
                    oStockPlannerGroupingsBE.AvoidableExpedited = dr["AvoidableExpediteComment"] != DBNull.Value ? Convert.ToInt32(dr["AvoidableExpediteComment"]) : 0;
                    oStockPlannerGroupingsBE.AvoidablePer_Expedited = dr["PercentExpediteAvoidable"] != DBNull.Value ? Convert.ToDecimal(dr["PercentExpediteAvoidable"]) : 0;
                    oStockPlannerGroupingsBE.BackOrder = dr["BackOrder"] != DBNull.Value ? Convert.ToInt32(dr["BackOrder"]) : 0;
                    oStockPlannerGroupingsBE.BackOrderComment = dr["BackOrderComment"] != DBNull.Value ? Convert.ToInt32(dr["BackOrderComment"]) : 0;
                    oStockPlannerGroupingsBE.BackOrderExpedited = dr["BackOrderExpediteComment"] != DBNull.Value ? Convert.ToInt32(dr["BackOrderExpediteComment"]) : 0;
                    oStockPlannerGroupingsBE.BackOrderExpeditedPer = dr["PercentExpediteBackOrder"] != DBNull.Value ? Convert.ToDecimal(dr["PercentExpediteBackOrder"]) : 0;
                    //oStockPlannerGroupingsBE.BackOrder_Avoidable = dr["AvoidableBackOrder"] != DBNull.Value ? Convert.ToInt32(dr["AvoidableBackOrder"]) : 0;
                    //oStockPlannerGroupingsBE.BackOrderComment_Avoidable = dr["AvoidableBackOrderComment"] != DBNull.Value ? Convert.ToInt32(dr["AvoidableBackOrderComment"]) : 0;
                    //oStockPlannerGroupingsBE.BackOrderExpedited_Avoidable = dr["AvoidableBackOrderExpediteComment"] != DBNull.Value ? Convert.ToInt32(dr["AvoidableBackOrderExpediteComment"]) : 0;
                    //oStockPlannerGroupingsBE.BackOrderExpeditedPer_Unavoidable = dr["AvoidablePercentExpediteBackOrder"] != DBNull.Value ? Convert.ToInt32(dr["AvoidablePercentExpediteBackOrder"]) : 0;
                    oStockPlannerGroupingsBE.SOH = dr["SOH"] != DBNull.Value ? Convert.ToInt32(dr["SOH"]) : 0;
                    oStockPlannerGroupingsBE.SOHComment = dr["SOHComment"] != DBNull.Value ? Convert.ToInt32(dr["SOHComment"]) : 0;
                    oStockPlannerGroupingsBE.SOHExpedited = dr["SOHExpediteComment"] != DBNull.Value ? Convert.ToInt32(dr["SOHExpediteComment"]) : 0;
                    oStockPlannerGroupingsBE.SOHPer_Expedited = dr["PercentExpediteSOH"] != DBNull.Value ? Convert.ToDecimal(dr["PercentExpediteSOH"]) : 0;
                    oStockPlannerGroupingsBELists.Add(oStockPlannerGroupingsBE);
                }
                oStockPlannerGroupingsBEList = oStockPlannerGroupingsBELists;
                RecordCount = ds.Tables[1].Rows[0]["RecordCount"] != DBNull.Value ? Convert.ToInt32(ds.Tables[1].Rows[0]["RecordCount"]) : 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                RecordCount = 0;
                oStockPlannerGroupingsBEList = null;
            }
        }

        public void ExpediteTrackerMonthlyReportDetailsDAL(StockPlannerGroupingsBE oStockPlannerGroupingsBE, out List<StockPlannerGroupingsBE> oStockPlannerGroupingsBEList, out int RecordCount, out DataTable DTexport)
        {
            DataTable dt = new DataTable();
            List<StockPlannerGroupingsBE> oStockPlannerGroupingsBELists = new List<StockPlannerGroupingsBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[13];
                param[index++] = new SqlParameter("@Action", oStockPlannerGroupingsBE.Action);
                param[index++] = new SqlParameter("@GroupingIDs", oStockPlannerGroupingsBE.StockPlannerGroupingIDs);
                param[index++] = new SqlParameter("@SiteIds", oStockPlannerGroupingsBE.SiteIDs);
                param[index++] = new SqlParameter("@VendorIDs", oStockPlannerGroupingsBE.VendorIDs);
                param[index++] = new SqlParameter("@ItemClassification", oStockPlannerGroupingsBE.ItemClassification);
                param[index++] = new SqlParameter("@StockPlannerIDs", oStockPlannerGroupingsBE.StockPlannerIDs);
                param[index++] = new SqlParameter("@PageCount", oStockPlannerGroupingsBE.PageCount);
                param[index++] = new SqlParameter("@CountryID", oStockPlannerGroupingsBE.CountryID);
                param[index++] = new SqlParameter("@SiteId", oStockPlannerGroupingsBE.SiteID);
                param[index++] = new SqlParameter("@StockPlannerName", oStockPlannerGroupingsBE.StockPlanner);
                param[index++] = new SqlParameter("@DateTo", oStockPlannerGroupingsBE.DateTo);
                param[index++] = new SqlParameter("@DateFrom", oStockPlannerGroupingsBE.Datefrom);
                param[index++] = new SqlParameter("@RecordCount", SqlDbType.Int);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spExpediteTracker", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    oStockPlannerGroupingsBE = new StockPlannerGroupingsBE();
                    oStockPlannerGroupingsBE.StockPlanner = dr["StockPlannerName"] != DBNull.Value ? Convert.ToString(dr["StockPlannerName"]) : string.Empty;
                    oStockPlannerGroupingsBE.Month1_AtRisk = dr["Month1_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Month1_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Month1_Date = dr["Month1_Date"] != DBNull.Value ? Convert.ToString(dr["Month1_Date"]) : string.Empty;
                    oStockPlannerGroupingsBE.Month1_Expdtd = dr["Month1_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Month1_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Month1_Items = dr["Month1_Items"] != DBNull.Value ? Convert.ToInt32(dr["Month1_Items"]) : 0;
                    oStockPlannerGroupingsBE.Month1_PercentExpdtd = dr["Month1_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Month1_PercentExpdtd"]) : 0;

                    oStockPlannerGroupingsBE.Month2_AtRisk = dr["Month2_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Month2_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Month2_Date = dr["Month2_Date"] != DBNull.Value ? Convert.ToString(dr["Month2_Date"]) : string.Empty;
                    oStockPlannerGroupingsBE.Month2_Expdtd = dr["Month2_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Month2_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Month2_Items = dr["Month2_Items"] != DBNull.Value ? Convert.ToInt32(dr["Month2_Items"]) : 0;
                    oStockPlannerGroupingsBE.Month2_PercentExpdtd = dr["Month2_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Month2_PercentExpdtd"]) : 0;

                    oStockPlannerGroupingsBE.Month3_AtRisk = dr["Month3_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Month3_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Month3_Date = dr["Month3_Date"] != DBNull.Value ? Convert.ToString(dr["Month3_Date"]) : string.Empty;
                    oStockPlannerGroupingsBE.Month3_Expdtd = dr["Month3_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Month3_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Month3_Items = dr["Month3_Items"] != DBNull.Value ? Convert.ToInt32(dr["Month3_Items"]) : 0;
                    oStockPlannerGroupingsBE.Month3_PercentExpdtd = dr["Month3_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Month3_PercentExpdtd"]) : 0;

                    oStockPlannerGroupingsBE.Month4_AtRisk = dr["Month4_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Month4_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Month4_Date = dr["Month4_Date"] != DBNull.Value ? Convert.ToString(dr["Month4_Date"]) : string.Empty;
                    oStockPlannerGroupingsBE.Month4_Expdtd = dr["Month4_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Month4_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Month4_Items = dr["Month4_Items"] != DBNull.Value ? Convert.ToInt32(dr["Month4_Items"]) : 0;
                    oStockPlannerGroupingsBE.Month4_PercentExpdtd = dr["Month4_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Month4_PercentExpdtd"]) : 0;

                    oStockPlannerGroupingsBE.Month5_AtRisk = dr["Month5_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Month5_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Month5_Date = dr["Month5_Date"] != DBNull.Value ? Convert.ToString(dr["Month5_Date"]) : string.Empty;
                    oStockPlannerGroupingsBE.Month5_Expdtd = dr["Month5_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Month5_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Month5_Items = dr["Month5_Items"] != DBNull.Value ? Convert.ToInt32(dr["Month5_Items"]) : 0;
                    oStockPlannerGroupingsBE.Month5_PercentExpdtd = dr["Month5_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Month5_PercentExpdtd"]) : 0;

                    oStockPlannerGroupingsBE.Month6_AtRisk = dr["Month6_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Month6_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Month6_Date = dr["Month6_Date"] != DBNull.Value ? Convert.ToString(dr["Month6_Date"]) : string.Empty;
                    oStockPlannerGroupingsBE.Month6_Expdtd = dr["Month6_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Month6_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Month6_Items = dr["Month6_Items"] != DBNull.Value ? Convert.ToInt32(dr["Month6_Items"]) : 0;
                    oStockPlannerGroupingsBE.Month6_PercentExpdtd = dr["Month6_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Month6_PercentExpdtd"]) : 0;

                    oStockPlannerGroupingsBE.Month7_AtRisk = dr["Month7_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Month7_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Month7_Date = dr["Month7_Date"] != DBNull.Value ? Convert.ToString(dr["Month7_Date"]) : string.Empty;
                    oStockPlannerGroupingsBE.Month7_Expdtd = dr["Month7_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Month7_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Month7_Items = dr["Month7_Items"] != DBNull.Value ? Convert.ToInt32(dr["Month7_Items"]) : 0;
                    oStockPlannerGroupingsBE.Month7_PercentExpdtd = dr["Month7_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Month7_PercentExpdtd"]) : 0;

                    oStockPlannerGroupingsBE.Month8_AtRisk = dr["Month8_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Month8_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Month8_Date = dr["Month8_Date"] != DBNull.Value ? Convert.ToString(dr["Month8_Date"]) : string.Empty;
                    oStockPlannerGroupingsBE.Month8_Expdtd = dr["Month8_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Month8_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Month8_Items = dr["Month8_Items"] != DBNull.Value ? Convert.ToInt32(dr["Month8_Items"]) : 0;
                    oStockPlannerGroupingsBE.Month8_PercentExpdtd = dr["Month8_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Month8_PercentExpdtd"]) : 0;

                    oStockPlannerGroupingsBE.Month9_AtRisk = dr["Month9_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Month9_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Month9_Date = dr["Month9_Date"] != DBNull.Value ? Convert.ToString(dr["Month9_Date"]) : string.Empty;
                    oStockPlannerGroupingsBE.Month9_Expdtd = dr["Month9_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Month9_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Month9_Items = dr["Month9_Items"] != DBNull.Value ? Convert.ToInt32(dr["Month9_Items"]) : 0;
                    oStockPlannerGroupingsBE.Month9_PercentExpdtd = dr["Month9_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Month9_PercentExpdtd"]) : 0;

                    oStockPlannerGroupingsBE.Month10_AtRisk = dr["Month10_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Month10_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Month10_Date = dr["Month10_Date"] != DBNull.Value ? Convert.ToString(dr["Month10_Date"]) : string.Empty;
                    oStockPlannerGroupingsBE.Month10_Expdtd = dr["Month10_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Month10_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Month10_Items = dr["Month10_Items"] != DBNull.Value ? Convert.ToInt32(dr["Month10_Items"]) : 0;
                    oStockPlannerGroupingsBE.Month10_PercentExpdtd = dr["Month10_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Month10_PercentExpdtd"]) : 0;

                    oStockPlannerGroupingsBE.Month11_AtRisk = dr["Month11_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Month11_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Month11_Date = dr["Month11_Date"] != DBNull.Value ? Convert.ToString(dr["Month11_Date"]) : string.Empty;
                    oStockPlannerGroupingsBE.Month11_Expdtd = dr["Month11_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Month11_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Month11_Items = dr["Month11_Items"] != DBNull.Value ? Convert.ToInt32(dr["Month11_Items"]) : 0;
                    oStockPlannerGroupingsBE.Month11_PercentExpdtd = dr["Month11_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Month11_PercentExpdtd"]) : 0;

                    oStockPlannerGroupingsBE.Month12_AtRisk = dr["Month12_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Month12_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Month12_Date = dr["Month12_Date"] != DBNull.Value ? Convert.ToString(dr["Month12_Date"]) : string.Empty;
                    oStockPlannerGroupingsBE.Month12_Expdtd = dr["Month12_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Month12_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Month12_Items = dr["Month12_Items"] != DBNull.Value ? Convert.ToInt32(dr["Month12_Items"]) : 0;
                    oStockPlannerGroupingsBE.Month12_PercentExpdtd = dr["Month12_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Month12_PercentExpdtd"]) : 0;
                    oStockPlannerGroupingsBELists.Add(oStockPlannerGroupingsBE);
                }
                oStockPlannerGroupingsBEList = oStockPlannerGroupingsBELists;
                RecordCount = ds.Tables[1].Rows[0]["RecordCount"] != DBNull.Value ? Convert.ToInt32(ds.Tables[1].Rows[0]["RecordCount"]) : 0;
                DTexport = dt;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                RecordCount = 0;
                oStockPlannerGroupingsBEList = null;
                DTexport = null;
            }
        }

        public void ExpediteTrackerDailyReportDetailsDAL(StockPlannerGroupingsBE oStockPlannerGroupingsBE, out List<StockPlannerGroupingsBE> oStockPlannerGroupingsBEList, out int RecordCount, out DataTable DTexport)
        {
            DataTable dt = new DataTable();
            List<StockPlannerGroupingsBE> oStockPlannerGroupingsBELists = new List<StockPlannerGroupingsBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[13];
                param[index++] = new SqlParameter("@Action", oStockPlannerGroupingsBE.Action);
                param[index++] = new SqlParameter("@GroupingIDs", oStockPlannerGroupingsBE.StockPlannerGroupingIDs);
                param[index++] = new SqlParameter("@SiteIds", oStockPlannerGroupingsBE.SiteIDs);
                param[index++] = new SqlParameter("@VendorIDs", oStockPlannerGroupingsBE.VendorIDs);
                param[index++] = new SqlParameter("@ItemClassification", oStockPlannerGroupingsBE.ItemClassification);
                param[index++] = new SqlParameter("@StockPlannerIDs", oStockPlannerGroupingsBE.StockPlannerIDs);
                param[index++] = new SqlParameter("@PageCount", oStockPlannerGroupingsBE.PageCount);
                param[index++] = new SqlParameter("@CountryID", oStockPlannerGroupingsBE.CountryID);
                param[index++] = new SqlParameter("@SiteId", oStockPlannerGroupingsBE.SiteID);
                param[index++] = new SqlParameter("@StockPlannerName", oStockPlannerGroupingsBE.StockPlanner);
                param[index++] = new SqlParameter("@DateTo", oStockPlannerGroupingsBE.DateTo);
                param[index++] = new SqlParameter("@DateFrom", oStockPlannerGroupingsBE.Datefrom);
                param[index++] = new SqlParameter("@RecordCount", SqlDbType.Int);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spExpediteTracker", param);
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    oStockPlannerGroupingsBE = new StockPlannerGroupingsBE();
                    oStockPlannerGroupingsBE.StockPlanner = dr["StockPlannerName"] != DBNull.Value ? Convert.ToString(dr["StockPlannerName"]) : string.Empty;
                    oStockPlannerGroupingsBE.Day1_Items = dr["Day1_Items"] != DBNull.Value ? Convert.ToInt32(dr["Day1_Items"]) : 0;
                    oStockPlannerGroupingsBE.Day1_AtRisk = dr["Day1_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Day1_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Day1_Expdtd = dr["Day1_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Day1_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day1_PercentExpdtd = dr["Day1_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Day1_PercentExpdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day1_Date = dr["Day1_Date"] != DBNull.Value ? Convert.ToString(dr["Day1_Date"]) : string.Empty;

                    oStockPlannerGroupingsBE.Day2_Items = dr["Day2_Items"] != DBNull.Value ? Convert.ToInt32(dr["Day2_Items"]) : 0;
                    oStockPlannerGroupingsBE.Day2_AtRisk = dr["Day2_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Day2_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Day2_Expdtd = dr["Day2_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Day2_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day2_PercentExpdtd = dr["Day2_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Day2_PercentExpdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day2_Date = dr["Day2_Date"] != DBNull.Value ? Convert.ToString(dr["Day2_Date"]) : string.Empty;

                    oStockPlannerGroupingsBE.Day3_Items = dr["Day3_Items"] != DBNull.Value ? Convert.ToInt32(dr["Day3_Items"]) : 0;
                    oStockPlannerGroupingsBE.Day3_AtRisk = dr["Day3_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Day3_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Day3_Expdtd = dr["Day3_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Day3_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day3_PercentExpdtd = dr["Day3_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Day3_PercentExpdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day3_Date = dr["Day3_Date"] != DBNull.Value ? Convert.ToString(dr["Day3_Date"]) : string.Empty;

                    oStockPlannerGroupingsBE.Day4_Items = dr["Day4_Items"] != DBNull.Value ? Convert.ToInt32(dr["Day4_Items"]) : 0;
                    oStockPlannerGroupingsBE.Day4_AtRisk = dr["Day4_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Day4_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Day4_Expdtd = dr["Day4_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Day4_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day4_PercentExpdtd = dr["Day4_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Day4_PercentExpdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day4_Date = dr["Day4_Date"] != DBNull.Value ? Convert.ToString(dr["Day4_Date"]) : string.Empty;

                    oStockPlannerGroupingsBE.Day5_Items = dr["Day5_Items"] != DBNull.Value ? Convert.ToInt32(dr["Day5_Items"]) : 0;
                    oStockPlannerGroupingsBE.Day5_AtRisk = dr["Day5_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Day5_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Day5_Expdtd = dr["Day5_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Day5_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day5_PercentExpdtd = dr["Day5_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Day5_PercentExpdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day5_Date = dr["Day5_Date"] != DBNull.Value ? Convert.ToString(dr["Day5_Date"]) : string.Empty;

                    oStockPlannerGroupingsBE.Day6_Items = dr["Day6_Items"] != DBNull.Value ? Convert.ToInt32(dr["Day6_Items"]) : 0;
                    oStockPlannerGroupingsBE.Day6_AtRisk = dr["Day6_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Day6_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Day6_Expdtd = dr["Day6_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Day6_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day6_PercentExpdtd = dr["Day6_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Day6_PercentExpdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day6_Date = dr["Day6_Date"] != DBNull.Value ? Convert.ToString(dr["Day6_Date"]) : string.Empty;

                    oStockPlannerGroupingsBE.Day7_Items = dr["Day7_Items"] != DBNull.Value ? Convert.ToInt32(dr["Day7_Items"]) : 0;
                    oStockPlannerGroupingsBE.Day7_AtRisk = dr["Day7_AtRisk"] != DBNull.Value ? Convert.ToInt32(dr["Day7_AtRisk"]) : 0;
                    oStockPlannerGroupingsBE.Day7_Expdtd = dr["Day7_Expdtd"] != DBNull.Value ? Convert.ToInt32(dr["Day7_Expdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day7_PercentExpdtd = dr["Day7_PercentExpdtd"] != DBNull.Value ? Convert.ToDecimal(dr["Day7_PercentExpdtd"]) : 0;
                    oStockPlannerGroupingsBE.Day7_Date = dr["Day7_Date"] != DBNull.Value ? Convert.ToString(dr["Day7_Date"]) : string.Empty;
                    oStockPlannerGroupingsBELists.Add(oStockPlannerGroupingsBE);
                }
                oStockPlannerGroupingsBEList = oStockPlannerGroupingsBELists;
                RecordCount = ds.Tables[1].Rows[0]["RecordCount"] != DBNull.Value ? Convert.ToInt32(ds.Tables[1].Rows[0]["RecordCount"]) : 0;
                DTexport = dt;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                RecordCount = 0;
                oStockPlannerGroupingsBEList = null;
                DTexport = null;
            }
        }


        public DataTable BindStockPlannerGroupingDAL(StockPlannerGroupingsBE oStockPlannerGroupingsBE)
        {
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oStockPlannerGroupingsBE.Action);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_StockPlannerGroupings", param);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

    }
}

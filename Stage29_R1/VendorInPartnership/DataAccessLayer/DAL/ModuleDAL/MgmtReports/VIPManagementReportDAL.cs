﻿using BusinessEntities.ModuleBE.MgmtReports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.MgmtReports
{
    public class VIPManagementReportDAL : BaseDAL
    {

        public List<VIPManagementReportBE> GetVIPReportDetailsDAL(VIPManagementReportBE oVIPManagementReportBE)
        {
            DataTable dt = new DataTable();
            List<VIPManagementReportBE> oNewVIPManagementReportBEList = new List<VIPManagementReportBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oVIPManagementReportBE.Action);
                param[index++] = new SqlParameter("@CalcultionDate", oVIPManagementReportBE.CalcultionDate);
                param[index++] = new SqlParameter("@ReportType", oVIPManagementReportBE.ReportType);
                param[index++] = new SqlParameter("@Page", oVIPManagementReportBE.Page);
                param[index++] = new SqlParameter("@PageSize", oVIPManagementReportBE.PageSize);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_VIPManagementReport", param);
                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    VIPManagementReportBE oNewVIPManagementReportBE = new VIPManagementReportBE();

                    if (dt.Columns.Contains("CalcultionDate"))
                        oNewVIPManagementReportBE.CalcultionDate = Convert.ToDateTime(dr["CalcultionDate"].ToString());
                    if (dt.Columns.Contains("Area"))
                        oNewVIPManagementReportBE.Area = Convert.ToString(dr["Area"].ToString());
                    if (dt.Columns.Contains("AreaKey"))
                        oNewVIPManagementReportBE.AreaKey = Convert.ToString(dr["AreaKey"].ToString());
                    if (dt.Columns.Contains("SitePrefix"))
                        oNewVIPManagementReportBE.SitePrefix = Convert.ToString(dr["SitePrefix"].ToString());
                    if (dt.Columns.Contains("SiteID"))
                        oNewVIPManagementReportBE.SiteID = Convert.ToInt32(dr["SiteID"].ToString());
                    if (dt.Columns.Contains("SiteCountryID"))
                        oNewVIPManagementReportBE.SiteCountryID = Convert.ToInt32(dr["SiteCountryID"].ToString());
                    if (dt.Columns.Contains("KeyOrder"))
                        oNewVIPManagementReportBE.KeyOrder = Convert.ToInt32(dr["KeyOrder"].ToString());
                    if (dt.Columns.Contains("KeyValue"))
                        oNewVIPManagementReportBE.KeyValue = dr["KeyValue"] != DBNull.Value ? Convert.ToDouble(dr["KeyValue"].ToString()) : (double?)null;
                    if (dt.Columns.Contains("KeyValue1"))
                        oNewVIPManagementReportBE.KeyValue1 = dr["KeyValue1"] != DBNull.Value ? Convert.ToDouble(dr["KeyValue1"].ToString()) : (double?)null;
                    if (dt.Columns.Contains("KeyValue2"))
                        oNewVIPManagementReportBE.KeyValue2 = dr["KeyValue2"] != DBNull.Value ? Convert.ToDouble(dr["KeyValue2"].ToString()) : (double?)null;
                    if (dt.Columns.Contains("KeyValue3"))
                        oNewVIPManagementReportBE.KeyValue3 = dr["KeyValue3"] != DBNull.Value ? Convert.ToDouble(dr["KeyValue3"].ToString()) : (double?)null;
                    if (dt.Columns.Contains("TotalRecords"))
                        oNewVIPManagementReportBE.TotalRecords = Convert.ToInt32(dr["TotalRecords"].ToString());
                    if (dt.Columns.Contains("KeyHeader"))
                        oNewVIPManagementReportBE.KeyHeader = Convert.ToString(dr["KeyHeader"].ToString());
                    if (dt.Columns.Contains("ReportType"))
                        oNewVIPManagementReportBE.ReportType = Convert.ToString(dr["ReportType"].ToString());
                    if (dt.Columns.Contains("TotalKeyPct"))
                        oNewVIPManagementReportBE.TotalKeyPct = dr["TotalKeyPct"] != DBNull.Value ? Convert.ToDouble(dr["TotalKeyPct"].ToString()) : (double?)null;
                    oNewVIPManagementReportBEList.Add(oNewVIPManagementReportBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oNewVIPManagementReportBEList;
        }


        public int? ChangePasswordDAL(VIPManagementReportBE oVIPManagementReportBE)
        {
            int? statusFlag = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];

                param[index++] = new SqlParameter("@Action", oVIPManagementReportBE.Action);
                param[index++] = new SqlParameter("@UserID", oVIPManagementReportBE.UserID);
                param[index++] = new SqlParameter("@Password", Common.EncryptPassword(oVIPManagementReportBE.Password));
                param[index++] = new SqlParameter("@OldPassword", Common.EncryptPassword(oVIPManagementReportBE.OldPassword));

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_VIPManagementReport", param);
                statusFlag = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return statusFlag;
        }

        public string GetPasswordDAL()
        {
            string password = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];

                param[index++] = new SqlParameter("@Action", "getPassword");


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_VIPManagementReport", param);
                password = Convert.ToString(ds.Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return password;
        }
    }
}

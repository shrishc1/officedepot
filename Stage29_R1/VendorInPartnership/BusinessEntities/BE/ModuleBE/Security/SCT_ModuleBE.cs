﻿using System;

namespace BusinessEntities.ModuleBE.Security
{
    [Serializable]
    public class SCT_ModuleBE : BaseBe
    {

        public int ModuleID { get; set; }

        public string ModuleName { get; set; }

        public int ParentModuleID { get; set; }

        public int? OrderBy { get; set; }

        public string ScreenName { get; set; }
    }
}

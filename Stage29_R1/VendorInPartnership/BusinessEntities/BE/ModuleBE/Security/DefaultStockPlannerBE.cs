﻿using System;

namespace BusinessEntities.ModuleBE.Security
{
    public class DefaultStockPlannerBE : BaseBe
    {

        public string CountryIDs { get; set; }
        public string SiteIDs { get; set; }
        public string VendorIDs { get; set; }
        public string CustomerIDs { get; set; }
        public string StockPlannerIDs { get; set; }
        public string VIPDefaultStockPlannerIDs { get; set; }
        public string OD_SKU_No { get; set; }
        public string Viking_SKU { get; set; }
        public string ExcludeItems { get; set; }
        public int PlannerMatch { get; set; }
        public int SOH { get; set; }

    }

    [Serializable]
    public class StockPlannerWithSiteBE
    {
        public int SiteID { get; set; }
        public int StockPlannerID { get; set; }
        public string StockPlanner { get; set; }
    }

}

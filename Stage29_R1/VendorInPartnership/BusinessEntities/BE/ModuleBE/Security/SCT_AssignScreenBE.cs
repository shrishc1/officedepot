﻿
namespace BusinessEntities.ModuleBE.Security
{
    public class SCT_AssignScreenBE : BaseBe
    {
        public int TemplateID { get; set; }

        public string ScreenReadPermission { get; set; }

        public string ScreenWritePermission { get; set; }

        public string TemplateName { get; set; }
    }
}

﻿using System;

namespace BusinessEntities.ModuleBE.Security
{
    [Serializable]
    public class StockPlannerGroupingsBE : BaseBe
    {
        public int StockPlannerGroupingsID { get; set; }
        public string StockPlannerGroupings { get; set; }
        public int UserCount { get; set; }

        public SCT_UserBE oUserBE { get; set; }


        public string StockPlanner { get; set; }
        public string StockPlannerGrouping { get; set; }
        public int TotalLines { get; set; }
        public int AtRisk { get; set; }
        public int Comment { get; set; }
        public int Expedited { get; set; }
        public decimal Per_Expedited { get; set; }
        public int Unavoidable { get; set; }
        public int UnavoidableComment { get; set; }
        public int UnavoidableExpedited { get; set; }
        public decimal UnvoidablePer_Expedited { get; set; }
        public int Avoidable { get; set; }
        public int AvoidableComment { get; set; }
        public int AvoidableExpedited { get; set; }
        public decimal AvoidablePer_Expedited { get; set; }
        public int BackOrder { get; set; }
        public int BackOrderComment { get; set; }
        public int BackOrderExpedited { get; set; }
        public decimal BackOrderExpeditedPer { get; set; }
        //public int BackOrder_Avoidable { get; set; }
        //public int BackOrderComment_Avoidable { get; set; }
        //public int BackOrderExpedited_Avoidable { get; set; }
        //public int BackOrderPer_Avoidable { get; set; }
        public int SOH { get; set; }
        public int SOHComment { get; set; }
        public int SOHExpedited { get; set; }
        public decimal SOHPer_Expedited { get; set; }
        public string SiteIDs { get; set; }
        public string VendorIDs { get; set; }
        public string StockPlannerIDs { get; set; }
        public string ItemClassification { get; set; }
        public int PageCount { get; set; }
        public string StockPlannerGroupingIDs { get; set; }
        public string SiteName { get; set; }
        public string CountryIDs { get; set; }
        public int CountryID { get; set; }
        public int SiteID { get; set; }
        public string VendorName { get; set; }
        public string DateTo { get; set; }
        public string Datefrom { get; set; }

        public int Month1_Items { get; set; }
        public int Month1_AtRisk { get; set; }
        public int Month1_Expdtd { get; set; }
        public decimal Month1_PercentExpdtd { get; set; }
        public string Month1_Date { get; set; }

        public int Month2_Items { get; set; }
        public int Month2_AtRisk { get; set; }
        public int Month2_Expdtd { get; set; }
        public decimal Month2_PercentExpdtd { get; set; }
        public string Month2_Date { get; set; }


        public int Month3_Items { get; set; }
        public int Month3_AtRisk { get; set; }
        public int Month3_Expdtd { get; set; }
        public decimal Month3_PercentExpdtd { get; set; }
        public string Month3_Date { get; set; }

        public int Month4_Items { get; set; }
        public int Month4_AtRisk { get; set; }
        public int Month4_Expdtd { get; set; }
        public decimal Month4_PercentExpdtd { get; set; }
        public string Month4_Date { get; set; }

        public int Month5_Items { get; set; }
        public int Month5_AtRisk { get; set; }
        public int Month5_Expdtd { get; set; }
        public decimal Month5_PercentExpdtd { get; set; }
        public string Month5_Date { get; set; }

        public int Month6_Items { get; set; }
        public int Month6_AtRisk { get; set; }
        public int Month6_Expdtd { get; set; }
        public decimal Month6_PercentExpdtd { get; set; }
        public string Month6_Date { get; set; }

        public int Month7_Items { get; set; }
        public int Month7_AtRisk { get; set; }
        public int Month7_Expdtd { get; set; }
        public decimal Month7_PercentExpdtd { get; set; }
        public string Month7_Date { get; set; }

        public int Month8_Items { get; set; }
        public int Month8_AtRisk { get; set; }
        public int Month8_Expdtd { get; set; }
        public decimal Month8_PercentExpdtd { get; set; }
        public string Month8_Date { get; set; }

        public int Month9_Items { get; set; }
        public int Month9_AtRisk { get; set; }
        public int Month9_Expdtd { get; set; }
        public decimal Month9_PercentExpdtd { get; set; }
        public string Month9_Date { get; set; }

        public int Month10_Items { get; set; }
        public int Month10_AtRisk { get; set; }
        public int Month10_Expdtd { get; set; }
        public decimal Month10_PercentExpdtd { get; set; }
        public string Month10_Date { get; set; }

        public int Month11_Items { get; set; }
        public int Month11_AtRisk { get; set; }
        public int Month11_Expdtd { get; set; }
        public decimal Month11_PercentExpdtd { get; set; }
        public string Month11_Date { get; set; }


        public int Month12_Items { get; set; }
        public int Month12_AtRisk { get; set; }
        public int Month12_Expdtd { get; set; }
        public decimal Month12_PercentExpdtd { get; set; }
        public string Month12_Date { get; set; }


        public int Day1_Items { get; set; }
        public int Day1_AtRisk { get; set; }
        public int Day1_Expdtd { get; set; }
        public decimal Day1_PercentExpdtd { get; set; }
        public string Day1_Date { get; set; }

        public int Day2_Items { get; set; }
        public int Day2_AtRisk { get; set; }
        public int Day2_Expdtd { get; set; }
        public decimal Day2_PercentExpdtd { get; set; }
        public string Day2_Date { get; set; }


        public int Day3_Items { get; set; }
        public int Day3_AtRisk { get; set; }
        public int Day3_Expdtd { get; set; }
        public decimal Day3_PercentExpdtd { get; set; }
        public string Day3_Date { get; set; }

        public int Day4_Items { get; set; }
        public int Day4_AtRisk { get; set; }
        public int Day4_Expdtd { get; set; }
        public decimal Day4_PercentExpdtd { get; set; }
        public string Day4_Date { get; set; }

        public int Day5_Items { get; set; }
        public int Day5_AtRisk { get; set; }
        public int Day5_Expdtd { get; set; }
        public decimal Day5_PercentExpdtd { get; set; }
        public string Day5_Date { get; set; }

        public int Day6_Items { get; set; }
        public int Day6_AtRisk { get; set; }
        public int Day6_Expdtd { get; set; }
        public decimal Day6_PercentExpdtd { get; set; }
        public string Day6_Date { get; set; }

        public int Day7_Items { get; set; }
        public int Day7_AtRisk { get; set; }
        public int Day7_Expdtd { get; set; }
        public decimal Day7_PercentExpdtd { get; set; }
        public string Day7_Date { get; set; }


    }
}

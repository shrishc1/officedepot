﻿using System;

namespace BusinessEntities.ModuleBE.Security
{
    [Serializable]
    public class SCT_ScreenBE : BaseBe
    {

        public int ScreenID { get; set; }

        public string ScreenName { get; set; }

        public string ScreenUrl { get; set; }

        public int ModuleID { get; set; }

        public char FunctionType { get; set; }
    }
}

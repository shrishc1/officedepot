﻿using System;
namespace BusinessEntities.ModuleBE.Security
{
    [Serializable]
    public class SCT_UserScreenBE : BaseBe
    {

        public int UserScreenID { get; set; }

        public int UserID { get; set; }

        public int ScreenID { get; set; }

        public int? OrderBy { get; set; }

        ///  public bool IsWriteAccessAllowed { get; set; }

        public SCT_ScreenBE Screen { get; set; }

        public SCT_ModuleBE Module { get; set; }

    }
}

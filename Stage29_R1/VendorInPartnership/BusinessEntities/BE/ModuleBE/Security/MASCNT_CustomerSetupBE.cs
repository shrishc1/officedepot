﻿using System;

namespace BusinessEntities.ModuleBE.Security
{
    [Serializable]
    public class MASCNT_CustomerSetupBE : BaseBe
    {
        public int CustomerID
        { get; set; }

        public int CountryID
        { get; set; }

        public string CustomerName
        { get; set; }

        public bool IsActive
        { get; set; }

        public DateTime UpdatedOn
        { get; set; }

        public int UpdatedBy
        { get; set; }

        public string CountryName
        { get; set; }

        public string UpdatedByUser
        { get; set; }

        public string UpdateDate
        { get; set; }

        public int SiteID
        { get; set; }

    }
}

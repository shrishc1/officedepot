﻿using System;

namespace BusinessEntities.ModuleBE.Upload
{
    [Serializable]
    public class MAS_RMSCategoryBE : BaseBe
    {

        public int RMSCategoryID { get; set; }

        public string CategoryName { get; set; }

        public int CreatedBy { get; set; }

        public string CreateByName { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string RMSCategoryIDs { get; set; }

        public int PageCount { get; set; }


    }
}

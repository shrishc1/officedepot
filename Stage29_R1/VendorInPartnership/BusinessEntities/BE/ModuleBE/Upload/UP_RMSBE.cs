﻿using System;

namespace BusinessEntities.ModuleBE.Upload
{
    [Serializable]
    public class UP_RMSBE : BaseBe
    {
        public string Country { get; set; }

        public string Viking_Code { get; set; }

        public string OD_Code { get; set; }

        public string Sap_Code { get; set; }

        public string RMS_Code { get; set; }

        public string Brand_Type { get; set; }

        public string Brand_Label_Name { get; set; }

        public string Master_Vendor_Number { get; set; }

        public string Master_Vendor_Name { get; set; }

        public string Division { get; set; }

        public string Div_Name { get; set; }

        public string Group_No { get; set; }

        public string Group_Name { get; set; }

        public string Dept { get; set; }

        public string Dept_Name { get; set; }

        public string Class { get; set; }

        public string Class_Name { get; set; }

        public string Subclass { get; set; }

        public string Subclass_Name { get; set; }

        public string Discontinued_Indicator { get; set; }

        public int SKUID { get; set; }

        public DateTime UpdatedDate { get; set; }

        public MAS_RMSCategoryBE RMSCategory { get; set; }

    }
}

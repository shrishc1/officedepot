﻿using System;

namespace BusinessEntities.ModuleBE.Upload
{
    [Serializable]
    public class UP_DataImportSchedulerBE : BaseBe
    {
        //UP_DataImportScheduler
        public int? DataImportSchedulerID { get; set; }
        public string CountryName { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string TimeTaken { get; set; }
        public string TotalVendorRecord { get; set; }
        public string ErrorVendorCount { get; set; }
        public string TotalSKURecord { get; set; }
        public string ErrorSKUCount { get; set; }
        public string TotalPORecord { get; set; }
        public string ErrorPOCount { get; set; }

        public string TotalExpediteRecord { get; set; }
        public string ErrorExpediteCount { get; set; }

        public string TotalReceiptInformationRecord { get; set; }
        public string ErrorReceiptCount { get; set; }
        public string ExecutionStatus { get; set; }
        public DateTime? DateTo { get; set; }
        public DateTime? DateFrom { get; set; }
        public string InvokedBy { get; set; }

        //UP_FolderDownload
        public int? FolderDownloadID { get; set; }
        public string FolderName { get; set; }
        public int? AvailableFilesInFolder { get; set; }
        public int? TotalDownloadedFile { get; set; }
        public int? ManualFileUploadID { get; set; }

        //UP_FileProcessed
        public string FileName { get; set; }
        public int? NoOfRecordProcessed { get; set; }
        public int? ErrorRecordCount { get; set; }

        //UP_ErrorLog
        public string ErrorMessage { get; set; }

        public string LogFilePath { get; set; }

        public string ProcessType { get; set; }

        public string ErrorBOCount { get; set; }
    }
}

﻿using System;

namespace BusinessEntities.ModuleBE.Upload
{
    [Serializable]
    public class ScheduleBE : BaseBe
    {
        public string JobName { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public bool IsExecuted { get; set; }
        public int UserID { get; set; }
        public DateTime InputDate { get; set; }
        public TimeSpan? SchedulerTime { get; set; }
        public DateTime? SchedulerDate { get; set; }
        public string UserName { get; set; }
        public int SchedulerId { get; set; }
        public DateTime DisplayMonthYear { get; set; }

        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Status { get; set; }
    }
}

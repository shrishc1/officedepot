﻿using System;

namespace BusinessEntities.ModuleBE.Upload
{
    [Serializable]
    public class UP_ManualFileUploadBE : BaseBe
    {

        public int? ManualFileUploadID { get; set; }
        public DateTime? DateUploaded { get; set; }
        public string DownloadedFilename { get; set; }
        public int? UserID { get; set; }
        public int? TotalVendorRecord { get; set; }
        public int? TotalSKURecord { get; set; }
        public int? TotalPORecord { get; set; }
        public int? TotalExpediteRecord { get; set; }
        public int? TotalReceiptInformationRecord { get; set; }
        public string UserName { get; set; }

        public int? ErrorVendorCount { get; set; }
        public int? ErrorSKUCount { get; set; }
        public int? ErrorPOCount { get; set; }
        public int? ErrorReceiptCount { get; set; }
        public int? ErrorExpediteCount { get; set; }
        public DateTime? TransactionDate { get; set; }

        public int? TotalRMSRecord { get; set; }
        public int? ErrorRMSCount { get; set; }

        public int? TotalBORecord { get; set; }
        public int? ErrorBOCount { get; set; }
    }
}

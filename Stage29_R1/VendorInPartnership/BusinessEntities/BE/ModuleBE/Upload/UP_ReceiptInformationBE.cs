﻿using System;

namespace BusinessEntities.ModuleBE.Upload
{
    [Serializable]
    public class UP_ReceiptInformationBE : BaseBe
    {
        public string Viking_sku_no { get; set; }
        public string OD_SKU_Number { get; set; }
        public string Vendor_prod_code { get; set; }
        public string Warehouse { get; set; }
        public string PO_number { get; set; }
        public int? Qty_receipted { get; set; }
        public string Other { get; set; }
        public DateTime? Date { get; set; }
        public string Country { get; set; }
        public DateTime? ORGOrdDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

    }
}

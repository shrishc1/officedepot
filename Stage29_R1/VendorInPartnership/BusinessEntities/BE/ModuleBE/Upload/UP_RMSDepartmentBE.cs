﻿using System;

namespace BusinessEntities.ModuleBE.Upload
{
    [Serializable]
    public class UP_RMSDepartmentBE : BaseBe
    {
        public string RMSDepartmentByIDs { get; set; }
        public int RMSDepartmentID { get; set; }
        public string Dept_No { get; set; }
        public string Dept_Name { get; set; }
        public MAS_RMSCategoryBE RMSCategory { get; set; }
    }
}

﻿using BusinessEntities.ModuleBE.Appointment.Booking;
using System;


namespace BusinessEntities.ModuleBE.Upload
{
    [Serializable]
    public class Up_PurchaseOrderDetailBE : BaseBe
    {
        public string PurchaseOrderIds { get; set; }
        public string SelectedVendorIDs { get; set; }
        public string SelectedSiteIDs { get; set; }
        public string SelectedCountryIDs { get; set; }
        public string SelectedInventoryMgrIDs { get; set; }
        public string SelectedDate { get; set; }
        public int? PurchaseOrderID { get; set; }
        public string Purchase_order { get; set; }
        public int? SKUID { get; set; }
        public int? Line_No { get; set; }
        public string Warehouse { get; set; }
        public string CreatedBy { get; set; }
        public string StockPlannerNo { get; set; }
        public string StockPlannerName { get; set; }
        public string Vendor_No { get; set; }
        public string Vendor_Name { get; set; }
        public string SiteName { get; set; }
        public string Direct_code { get; set; }
        public string OD_Code { get; set; }
        public string Vendor_Code { get; set; }
        public string Product_description { get; set; }
        public string UOM { get; set; }
        public string Original_quantity { get; set; }
        public string Outstanding_Qty { get; set; }
        public string Qty_On_Hand { get; set; }
        public decimal? PO_cost { get; set; }

        public decimal? PO_Totalcost { get; set; }

        public DateTime? Order_raised { get; set; }

        public string Order_raisedString { get; set; }

        public DateTime? Original_due_date { get; set; }

        public string Original_due_dateString { get; set; }

        public DateTime? Expected_date { get; set; }

        public string Expected_dateString { get; set; }

        public DateTime? ScheduleDate { get; set; }

        public string ScheduleDateString { get; set; }

        public string Buyer_no { get; set; }

        public string Item_classification { get; set; }

        public string Item_type { get; set; }

        public string Item_Category { get; set; }

        public string Other { get; set; }

        public DateTime? RevisedDueDate { get; set; }

        public DateTime? CurrentRevisedDueDate { get; set; }

        public string LastUploadedFlag { get; set; }

        public string SelectedProductCodes { get; set; }

        public string ProductDescription { get; set; }

        public string CountryName { get; set; }

        public BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE Vendor { get; set; }
        public BusinessEntities.ModuleBE.AdminFunctions.MAS_MaintainVendorPointsContactBE VendorPOC { get; set; }

        public int OutstandingLines { get; set; }

        public BusinessEntities.ModuleBE.Upload.UP_SKUBE SKU { get; set; }

        public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }

        public int BookingID { get; set; }

        public int? Qty_receipted { get; set; }

        public int? LineCount { get; set; }

        public int CountryID { get; set; }
        public string SubVendor { get; set; }

        // Sprint 1 - Point 7
        public DateTime? BookingDeliveryDate { get; set; }
        public string Status { get; set; }

        public DateTime UpdatedDate { get; set; }
        public DateTime? LastReceiptDate { get; set; }

        public int UserID { get; set; }
        public int UserRoleID { get; set; }

        public bool? IsPOManuallyAdded { get; set; }
        public DateTime? ReceiptDate { get; set; }
        public string ODSKU_No { get; set; }

        public string Comments { get; set; }
        public string DeliveryStatus { get; set; }

        public int GridCurrentPageNo { get; set; }
        public int GridPageSize { get; set; }
        public int TotalRecords { get; set; }

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        public int? PODue { get; set; }
        public int? PODelivered { get; set; }
        public int? Receipts { get; set; }
        public float? ReceiptsPerDeliveredPO { get; set; }
        public APPBOK_BookingBE Booking { get; set; }

        public string ChaseStatus { get; set; }

        public string Reason { get; set; }
        public string Currency { get; set; }
        public string FirstSentToEmail { get; set; }
        public string SelectedPONos { get; set; }
        public string SelectedStockPlannerIDs { get; set; }
        public DateTime? DueBy { get; set; }
        public string DisplayOption { get; set; }

        public int? SiteId { get; set; }
        public int? VendorId { get; set; }
        public string VendorName { get; set; }
        public int SiteID { get; set; }


        public int? StockPlannerID { get; set; }
        public string StockPlannerContactNo { get; set; }
        public string EntryType { get; set; }

        public decimal? TotalCost { get; set; }

        public string SentFrom { get; set; }
        public string SentTo { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int LanguageID { get; set; }
        public bool IsMailSentInLanguage { get; set; }
        public string CommunicationType { get; set; }
        public DateTime? SentDate { get; set; }
        public int? Variance { get; set; }
        public string OD_SKU_NO { get; set; }
        public string Direct_SKU { get; set; }
        public string Product_Lead_time { get; set; }

        public int Backorders { get; set; }
        public int Stockouts { get; set; }
        public string OnTime { get; set; }
        public bool? IsProvisionResonChangesUpdate { get; set; }

        public string StockPlannerGroupingID { get; set; }
        public string StockPlannerGroupingName { get; set; }
        public string OrderRaisedFromDate { get; set; }
        public string OrderRaisedToDate { get; set; }
        public string ReceiptStatus { get; set; }
        public string VikingSku { get; set; }

        public string SkugroupingID { get; set; }

        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string OriginalDueDate { get; set; }

        public int PageCount { get; set; }
        public int RecordCount { get; set; }
        public int SKUExclusionID { get; set; }
        public int ApplyExclusionID { get; set; }

        public string CreatedOn { get; set; }
        public bool IsFromSummary { get; set; }
        public string Total_Qty_BO { get; set; }
        public string SendMethod { get; set; }

        public string Daysfilter { get; set; }

        public string RevisedDueDateUserName { get; set; }
        public string AppliedBy { get; set; }
        public string RevisedDueDateAppliedOn { get; set; }

        public string Snooty { get; set; }

        public string SubVendorIDs { get; set; }

        public int Page { get; set; }
    }
}

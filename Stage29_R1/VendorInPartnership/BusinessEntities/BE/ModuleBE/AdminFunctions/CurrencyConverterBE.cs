﻿using System;

namespace BusinessEntities.ModuleBE.AdminFunctions
{
    [Serializable]
    public class CurrencyConverterBE : BaseBe
    {
        public int CurrencyConverterID { get; set; }

        public int CurrencyID { get; set; }

        public int ConvertCurrencyID { get; set; }

        public double? ExchangeRate { get; set; }

        public int UserID { get; set; }

        public DateTime DateApplied { get; set; }

        public bool IsPrevious { get; set; }

        public string UserName { get; set; }

        public string Currency { get; set; }

        public string CurrencyConvertTo { get; set; }
    }
}
﻿using BusinessEntities.ModuleBE.Security;
using System;

namespace BusinessEntities.ModuleBE.AdminFunctions
{
    [Serializable]
    public class CommunicationLibraryBE : BaseBe
    {
        public int? CommunicationID { get; set; }
        public DateTime? SentDate { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string SentBy { get; set; }
        public string Title { get; set; }
        public string TypeofCommunication { get; set; }
        public SCT_UserBE Users { get; set; }
        public string UserIds { get; set; }
        public int? CreatedByID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByEmail { get; set; }
        public string SentTo { get; set; }

        public DateTime? DateTo { get; set; }
        public DateTime? DateFrom { get; set; }
        public string CommunicationIds { get; set; }
    }
}
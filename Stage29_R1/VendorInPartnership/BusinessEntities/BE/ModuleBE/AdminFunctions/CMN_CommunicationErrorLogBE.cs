﻿using System;

namespace BusinessEntities.ModuleBE.AdminFunctions
{
    public class CMN_CommunicationErrorLogBE : BaseBe
    {
        public string Subject { get; set; }

        public string SentTo { get; set; }

        public string Body { get; set; }

        public string ErrorDescription { get; set; }

        public string DiscrepancyLogID { get; set; }

        public DateTime sentDate { get; set; }

        public int CommunicationErrorID { get; set; }

        public string VDRNo { get; set; }

        public char Status { get; set; }
    }
}
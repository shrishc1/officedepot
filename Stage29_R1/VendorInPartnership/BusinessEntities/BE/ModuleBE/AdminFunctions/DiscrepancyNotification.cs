﻿using System;

namespace BusinessEntities.ModuleBE.AdminFunctions
{
    public class DiscrepancyNotification : BaseBe
    {
        public Int64 DiscrepancyTrackerId { get; set; }
        public int Week { get; set; }
        public int Year { get; set; }
        public string EmailContent { get; set; }
        public int EditedBy { get; set; }
        public int AddedBy { get; set; }
        public bool MailSent { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string ExcelPath { get; set; }
        public string ImagePath { get; set; }
        public string StockPlanersEmailIds { get; set; }

        public string EmailSubject { get; set; }
    }
}
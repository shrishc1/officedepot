﻿using System;

namespace BusinessEntities.ModuleBE.AdminFunctions
{
    [Serializable]
    public class MAS_CountryBE : BaseBe
    {
        public int CountryID { get; set; }

        public string CountryName { get; set; }

        public int DefaultLanguage { get; set; }

        public int BookingNoticePeriodDays { get; set; }

        public int PODueDateToleranceDays { get; set; }

        public DateTime FutureDueDate { get; set; }

        public int AverageReciepting { get; set; }

        public int LinesPerFTE { get; set; }

        public int AverageUnloading { get; set; }

        public bool DeliveryNoteRequired { get; set; }

        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }

        public bool IsActive { get; set; }

    }
}
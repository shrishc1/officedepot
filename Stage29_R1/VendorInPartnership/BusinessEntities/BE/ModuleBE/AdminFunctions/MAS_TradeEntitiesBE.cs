﻿using System;

namespace BusinessEntities.ModuleBE.AdminFunctions
{
    [Serializable]
    public class MAS_TradeEntitiesBE : BaseBe
    {
        public int? RowNo { get; set; }
        public int? TradeEntitiesID { get; set; }
        public string Area { get; set; }
        public string OD_Country { get; set; }
        public string SendPdfInvoicesTo { get; set; }
        public string Company { get; set; }
        public string InvoiceAddress { get; set; }
        public string VAT_No { get; set; }
        public string COC_No { get; set; }
        public string Type { get; set; }
        public string ExampleOrderNo { get; set; }
    }
}
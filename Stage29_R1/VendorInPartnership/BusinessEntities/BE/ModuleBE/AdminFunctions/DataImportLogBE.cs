﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.AdminFunctions
{
    public class DataImportLogBE : BaseBe
    {
        public int Id { get; set; }
        public int DataImportSchedulerID { get; set; }
        public string TaskRunning { get; set; }
    }
}

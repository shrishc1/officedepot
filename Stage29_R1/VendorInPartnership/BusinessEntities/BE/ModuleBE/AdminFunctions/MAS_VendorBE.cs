﻿using System;

namespace BusinessEntities.ModuleBE.AdminFunctions
{
    [Serializable]
    public class MAS_VendorBE : BaseBe
    {
        public int? VendorID { get; set; }
        public string Vendor_Country { get; set; }
        public string Vendor_No { get; set; }
        public string Vendor_Name { get; set; }
        public string VendorContactName { get; set; }
        public string VendorContactNumber { get; set; }
        public string VendorContactFax { get; set; }
        public string VendorContactEmail { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string county { get; set; }
        public string VMPPIN { get; set; }
        public string VMPPOU { get; set; }
        public string VMTCC1 { get; set; }
        public string VMTEL1 { get; set; }
        public string Fax_country_code { get; set; }
        public string Fax_no { get; set; }
        public string Buyer { get; set; }
        public int? StockPlannerID { get; set; }
        public int? CountryID { get; set; }
        public char VendorFlag { get; set; }
        public int? ParentVendorID { get; set; }
        public int? OTIF_BatchIndicator { get; set; }
        public string OTIF_PreferedFrequency { get; set; }
        public decimal OTIF_AbsolutePenaltyBelowPercentage { get; set; }
        public decimal OTIF_AbsoluteAppliedPenaltyPercentage { get; set; }
        public decimal OTIF_ToleratedPenaltyBelowPercentage { get; set; }
        public decimal OTIF_ToleratedAppliedPenaltyPercentage { get; set; }
        public decimal OTIF_ODMeasurePenaltyBelowPercentage { get; set; }
        public decimal OTIF_ODMeasureAppliedPenaltyPercentage { get; set; }
        public decimal OTIF_BackOrderValue { get; set; }
        public int? CurrencyID { get; set; }
        public int? LanguageID { get; set; }
        public int? OverrideLanguageID { get; set; }
        public string NoOfSKU { get; set; }
        public string Vendor { get; set; }
        public string RegistrationStatus { get; set; }
        public string ContactDefined { get; set; }
        public string VendorSite { get; set; }

        public char? VendorsMissingDetails { get; set; }

        public int? SiteID { get; set; }

        public string IsActiveVendor { get; set; }
        public string VendorIDs { get; set; }
        public string SearchedVendorText { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }

        public int EuropeanorLocal { get; set; }

        public string VendorEuropeanorLocal { get; set; }

        public string EUConsolidationCode { get; set; }

        public string LocalConsolidationCode { get; set; }

        public string NumberofLines { get; set; }

        public string AssignedSite { get; set; }

        public string EnabledSite { get; set; }

        public string VatCode { get; set; }

        public int? NewCurrencyID { get; set; }

        public string ConsVendorVendor_No { get; set; }
        public string SchedulingContact { get; set; }
        public string DiscrepancyContact { get; set; }
        public string OTIFContact { get; set; }
        public string ScorecardContact { get; set; }
        public string InvoiceIssueContact { get; set; }
        public string InventoryPOC { get; set; }
        public string ProcurementPOC { get; set; }
        public string MerchandisingPOC { get; set; }
        public string ExecutivePOC { get; set; }
        public string StockPlannerName { get; set; }
        public string AccounPlannerName { get; set; }
        public string IsVendorSubscribeToPenalty { get; set; }
        public string IsVendorOverstockAgreement { get; set; }
        public string VendorOverstockAgreementDetails { get; set; }
        public string IsVendorReturnsAgreement { get; set; }
        public string VendorReturnsAgreementDetails { get; set; }

        public string CountryName { get; set; }
        public string SiteName { get; set; }
        public bool IsSiteSchedulingEnabled { get; set; }
        public bool IsEnableHazardouesItemPrompt { get; set; }
        public int Lines { get; set; }
        public string Weightage { get; set; }

        public string AgreementType { get; set; }
    }
}
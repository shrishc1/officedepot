﻿using System;


namespace BusinessEntities.ModuleBE.AdminFunctions
{
  [Serializable]
    public class MAS_RegionBE : BaseBe
    {
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public int CountryID { get; set; }
        public string SelectedCountries { get; set; }

    }
}

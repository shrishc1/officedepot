﻿using System;

namespace BusinessEntities.ModuleBE.AdminFunctions
{
    [Serializable]
    public class MAS_VatCodeBE : BaseBe
    {
        public int ODVatCodeID { get; set; }

        public int SiteId { get; set; }

        public string SiteName { get; set; }

        public string VatCode { get; set; }

        public bool IsActive { get; set; }

        public BusinessEntities.ModuleBE.AdminFunctions.MAS_VendorBE Vendor { get; set; }

        public string SelectedCountryIDs { get; set; }

        public string SelectedVendorIDs { get; set; }

        public int IsVendorVatCodeMaintained { get; set; }

        public int GridCurrentPageNo { get; set; }

        public int GridPageSize { get; set; }

        public string TotalRecords { get; set; }
    }
}
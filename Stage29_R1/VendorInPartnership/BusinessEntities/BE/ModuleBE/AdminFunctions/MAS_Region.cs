﻿using System;


namespace BusinessEntities.ModuleBE.AdminFunctions
{
    class MAS_Region : BaseBe 
    {
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public int CountryID { get; set; }
        public string SelectedCountries { get; set; }
    }
}

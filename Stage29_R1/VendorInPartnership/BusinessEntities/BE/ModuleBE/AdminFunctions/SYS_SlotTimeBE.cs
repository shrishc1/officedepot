﻿using System;

namespace BusinessEntities.ModuleBE.AdminFunctions
{
    [Serializable]
    public class SYS_SlotTimeBE : BaseBe
    {
        public int SlotTimeID { get; set; }
        public string SlotTime { get; set; }
        public int OrderBYID { get; set; }
    }
}
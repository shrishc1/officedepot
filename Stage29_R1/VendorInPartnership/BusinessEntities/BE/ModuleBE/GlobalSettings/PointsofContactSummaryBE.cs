﻿using System;

namespace BusinessEntities.ModuleBE.GlobalSettings
{
    [Serializable]
    public class PointsofContactSummaryBE : BaseBe
    {
        public string Country
        { get; set; }

        public int CountryID
        { get; set; }

        public double? Scheduling
        { get; set; }

        public double? Discrepancies
        { get; set; }

        public double? OTIF
        { get; set; }

        public double? Scorecard
        { get; set; }

        public double? Inventory
        { get; set; }

        public double? Procurement
        { get; set; }

        public double? AccountsPayable
        { get; set; }

        public double? Executive
        { get; set; }


        public string VendorName { get; set; }

        public int VendorID { get; set; }

        public int Lines { get; set; }

        public double? Weightage { get; set; }

        public char? SchedulingPOC { get; set; }

        public char? DiscrepancyPOC { get; set; }

        public char OTIFPOC { get; set; }

        public char ScorecardPOC { get; set; }

        public char InventoryPOC { get; set; }

        public char ProcurementPOC { get; set; }

        public char AccountsPayablePOC { get; set; }

        public char ExecutivePOC { get; set; }

        public string Module { get; set; }

        public string Captured { get; set; }

        public int? UserID { get; set; }

        public string SiteName { get; set; }

        public int POCtrackerDetailID { get; set; }

        public char POCCountry { get; set; }

        public int NoofAPUser { get; set; }

        public string Weight { get; set; }

        public bool IsAllContact { get; set; }

        public string SCSelectedVendorIDs { get; set; }

        public string ContactName { get; set; }

        public string VendorPhoneNumber { get; set; }

        public int NoofSPUser { get; set; }

        public string StockPlannerNo { get; set; }

        public string NoOfLines { get; set; }

        public string PoCreated { get; set; }

        public bool IsOpen { get; set; }

        public bool IsExcludeVendor { get; set; }

        public int? IsAll { get; set; }
        public string VendorIDs { get; set; }
    }
}

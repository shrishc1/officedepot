﻿using System;

namespace BusinessEntities.ModuleBE.GlobalSettings
{
    public class VendorPOCExclusionBE : BaseBe
    {
        public int VendorExclusionID { get; set; }
        public int VendorID { get; set; }
        public string VendorNo { get; set; }
        public string VendorName { get; set; }
        public string ReasonForExclusion { get; set; }
        public int? CreatedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string UserName { get; set; }
        public string CountryName { get; set; }
    }
}

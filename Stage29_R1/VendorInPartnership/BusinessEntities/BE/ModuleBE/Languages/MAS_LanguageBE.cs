﻿namespace BusinessEntities.ModuleBE.Languages.Languages
{
    //[Serializable]
    public class MAS_LanguageBE : BaseBe
    {
        public int LanguageID { get; set; }
        public string Language { get; set; }
        public string ISOLanguageName { get; set; }
        public bool IsEnable { get; set; }
        public bool IsActiveForRegistration { get; set; }
        public string ImagePath { get; set; }
    }
}

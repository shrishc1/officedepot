﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Upload;
using System;
using System.Collections.Generic;


namespace BusinessEntities.ModuleBE.StockOverview
{
    [Serializable]
    public class ExpediteStockBE : BaseBe
    {
        public string Comment { get; set; }
        public string Status { get; set; }
        public UP_VendorBE Vendor { get; set; }
        public Up_PurchaseOrderDetailBE PurchaseOrder { get; set; }
        public MAS_SiteBE Site { get; set; }
        public DateTime? CommentDateLastUpdated { get; set; }
        public DateTime? CommentUpdateValidTo { get; set; }
        public DateTime? EarliestAdviseDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string MostCurrentComment { get; set; }
        public string SOH { get; set; }
        public decimal FirstWeekMON { get; set; }
        public decimal FirstWeekTUE { get; set; }
        public decimal FirstWeekWED { get; set; }
        public decimal FirstWeekTHU { get; set; }
        public decimal FirstWeekFRI { get; set; }
        public decimal SecondWeekMON { get; set; }
        public decimal SecondWeekTUE { get; set; }
        public decimal SecondWeekWED { get; set; }
        public decimal SecondWeekTHU { get; set; }
        public decimal SecondWeekFRI { get; set; }
        public decimal ThirdWeekMON { get; set; }
        public decimal ThirdWeekTUE { get; set; }
        public decimal ThirdWeekWED { get; set; }
        public decimal ThirdWeekTHU { get; set; }
        public decimal ThirdWeekFRI { get; set; }
        public decimal FourthWeekMON { get; set; }
        public decimal FourthWeekTUE { get; set; }
        public decimal FourthWeekWED { get; set; }
        public decimal FourthWeekTHU { get; set; }
        public decimal FourthWeekFRI { get; set; }

        public string FirstWeekMONColor { get; set; }
        public string FirstWeekTUEColor { get; set; }
        public string FirstWeekWEDColor { get; set; }
        public string FirstWeekTHUColor { get; set; }
        public string FirstWeekFRIColor { get; set; }
        public string SecondWeekMONColor { get; set; }
        public string SecondWeekTUEColor { get; set; }
        public string SecondWeekWEDColor { get; set; }
        public string SecondWeekTHUColor { get; set; }
        public string SecondWeekFRIColor { get; set; }
        public string ThirdWeekMONColor { get; set; }
        public string ThirdWeekTUEColor { get; set; }
        public string ThirdWeekWEDColor { get; set; }
        public string ThirdWeekTHUColor { get; set; }
        public string ThirdWeekFRIColor { get; set; }
        public string FourthWeekMONColor { get; set; }
        public string FourthWeekTUEColor { get; set; }
        public string FourthWeekWEDColor { get; set; }
        public string FourthWeekTHUColor { get; set; }
        public string FourthWeekFRIColor { get; set; }

        public string UpdateCommentRequired { get; set; }

        public DateTime Date { get; set; }
        public string Option { get; set; }
        public string MatGrp { get; set; }
        public string MRP { get; set; }
        public string PurGrp { get; set; }
        public string WhoWasContact { get; set; }
        public string WhoExpedite { get; set; }
        public DateTime NextUpdateDue { get; set; }

        public DateTime CommentDate { get; set; }

        //For Reason Type setup//
        public int? ReasonTypeID { get; set; }

        public string Reason { get; set; }
        public bool IsCommentRequired { get; set; }

        public BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE Country { get; set; }
        public int CountryID { get; set; }
        public int UserID { get; set; }
        public string CountryName { get; set; }
        public int SkuID { get; set; }
        public DateTime? CommentsValid_Date { get; set; }
        public DateTime? CommentsAdded_Date { get; set; }
        public DateTime ExpediteOn { get; set; }
        public string CommentsAddedBy { get; set; }
        public bool IsExpediteReview { get; set; }
        public string Comments { get; set; }
        public int CommentID { get; set; }
        public string CommentIDs { get; set; }
        public int IsActive { get; set; }

        //For Search Selection
        public string SelectedStockPlannerIDs { get; set; }
        public string SelectedStockPlannerName { get; set; }
        public string SelectedIncludedVendorIDs { get; set; }
        public string SelectedExcludedVendorIDs { get; set; }
        public string SelectedSiteIDs { get; set; }
        public string SelectedItemClassification { get; set; }
        public string SelectedMRPType { get; set; }
        public string SelectedPurcGroup { get; set; }
        public string SelectedMatGrp { get; set; }
        public string SelectedVIKINGSKU { get; set; }
        public string SelectedODSKU { get; set; }

        public string VendorName { get; set; }
        public int VendorID { get; set; }
        public string Vendor_No { get; set; }
        public string SiteName { get; set; }
        public string OD_Code { get; set; }
        public string Direct_code { get; set; }
        public string Product_description { get; set; }
        public string StockPlannerNo { get; set; }
        public string StockPlannerName { get; set; }
        public int SiteID { get; set; }
        public string Vendor_Code { get; set; }
        public string Viking_Code { get; set; }
        // public string Viking_Code { get; set; }
        public List<int> VendorIDs { get; set; }

        //For Expedite Communication
        public string Comm_By { get; set; }
        public string CommunicationType { get; set; }
        public bool Comm_Status { get; set; }
        public int ExpediteID { get; set; }
        public string Subject { get; set; }
        public string SentFrom { get; set; }
        public string SentTo { get; set; }
        public string Body { get; set; }
        public DateTime? Comm_On { get; set; }
        public bool IsMailSent { get; set; }
        public string language { get; set; }
        public int SelectedSkuType { get; set; }
        public int SelectedDisplay { get; set; }
        public int SelectedUpdate { get; set; }
        public string ExpStatus { get; set; }
        public string POStatus { get; set; }
        public string UpdateDay { get; set; }
        public string UpdateDate { get; set; }

        // SOH Properties
        public string DayUploaded { get; set; }
        public DateTime? DateUploaded { get; set; }
        public decimal OHQtyMove { get; set; }
        public decimal ForecastSoldAmtMove { get; set; }
        public decimal ActualSoldAmtMove { get; set; }
        public decimal OHQtyMoveTo { get; set; }
        public decimal ForecastSoldAmtMoveTo { get; set; }
        public decimal ActualSoldAmtMoveTo { get; set; }
        public DateTime? CurrentDate { get; set; }
        public decimal ActualSoldAmt { get; set; }
        public int MoveSiteId { get; set; }
        public int MoveToSiteId { get; set; }
        public int CommunicationID { get; set; }

        public int Page { get; set; }
        public int TotalRecords { get; set; }

        public string SortBy { get; set; }
        public string FB { get; set; }
        public string CB { get; set; }
        public string CBQ { get; set; }
        public int POStatusSearchCriteria { get; set; }

        public BackOrderBE BackOrder { get; set; }
        public string CommentColor { get; set; }
        public int? CommentStatus { get; set; }

        public string StockPlannerEmail { get; set; }

        public string IsActiveType { get; set; }
        public string LeadTimeVariance { get; set; }
        public string DaysStock { get; set; }


        public string skuids { get; set; }
        public string Vendors { get; set; }
        public string DirectCodes { get; set; }
        public string ODCodes { get; set; }
        public string Statuss { get; set; }
        public string Discs { get; set; }
        public string SiteIds { get; set; }
        public string CountryIds { get; set; }

        public DateTime? LastEdited { get; set; }
        public string EarliestPO { get; set; }
        public DateTime? DueDate { get; set; }
        // This data type is string because if client want change for decimal, we need to change only in SP
        public string MinForecastedSales { get; set; }

        public string SortingDay { get; set; }
        public string StockPlannerGroupingID { get; set; }
        public string StockPlannerGrouping { get; set; }

        public decimal QtyOnHand { get; set; }
        public string SelectedStockPlannerUserIDs { get; set; }
        public string SiteNames_SOHPOP { get; set; }
        public string SOH_SOHPOP { get; set; }
        public string AVGForcast_SOHPOP { get; set; }

        public bool Isbotop20 { get; set; }

        public string SkugroupingID { get; set; }
        public string SKuGrouping { get; set; }
        public string PriorityItemColor { get; set; }
        public bool IsPriority { get; set; }
        public string IsPriorityAddedBy { get; set; }

        public int PageCount { get; set; }

        public string subvndr { get; set; }
    }
}

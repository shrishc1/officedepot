﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Security;
using BusinessEntities.ModuleBE.Upload;
using System;

namespace BusinessEntities.ModuleBE.StockOverview
{

    [Serializable]
    public class BackOrderBE : BaseBe
    {
        public UP_VendorBE Vendor { get; set; }
        public Up_PurchaseOrderDetailBE PurchaseOrder { get; set; }
        public UP_SKUBE SKU { get; set; }
        public SCT_UserBE User { get; set; }
        public CurrencyBE Currency { get; set; }

        //Penalty Maintenance Parameters
        public int? PenaltyID { get; set; }
        public string IsVendorSubscribeToPenalty { get; set; }
        public string PenaltyType { get; set; }
        public double? OTIFRate { get; set; }
        public decimal? OTIFLineValuePenalty { get; set; }
        public double? ValueOfBackOrder { get; set; }
        public int? PenaltyAppliedId { get; set; }
        public int? PenaltyModifiedId { get; set; }

        public string PenaltyAppliedName { get; set; }
        public string LocalConslolidation { get; set; }
        public string EUConsolidation { get; set; }

        // 
        public int CurrentMonthCount { get; set; }
        public int PreviousMonthCount { get; set; }
        public int? NoofBO { get; set; }
        public decimal? QtyonBackOrder { get; set; }
        public int? NoofBOWithPenalities { get; set; }
        public decimal? PotentialPenaltyCharge { get; set; }
        public DateTime? BOIncurredDate { get; set; }
        public string Year { get; set; }
        public string PenaltyRelatingTo { get; set; }
        public int SKUCountOnDashboard { get; set; }
        public int SiteID { get; set; }

        //BackOrder Penalty Flow Parameters
        public int? BOReportingID { get; set; }
        public string InventoryReviewStatus { get; set; }
        public string VendorBOReviewStatus { get; set; }
        public string DisputeReviewStatus { get; set; }
        public string MediatorStatus { get; set; }
        public string AccountsPayableAction { get; set; }
        public string OverallStatus { get; set; }
        public int? InventoryActionTakenBy { get; set; }
        public int? VendorActionTakenBy { get; set; }
        public string VendorActionTakenName { get; set; }
        public int? DisputeReviewActionTakenBy { get; set; }
        public int? MediatorActionTakenBy { get; set; }
        public string MediatorActionTakenName { get; set; }
        public int? APActionTakenBy { get; set; }
        public DateTime? InventoryActionDate { get; set; }
        public DateTime? VendorActionDate { get; set; }
        public DateTime? DisputeReviewActionDate { get; set; }
        public DateTime? MediatorActionDate { get; set; }
        public DateTime? APActionDate { get; set; }
        public string DisputeReviewActionTakenName { get; set; }
        public string IsVendorCommuncationSent { get; set; }

        //PenaltyCharge Parameters
        public int? PenaltyChargeID { get; set; }
        public decimal? DisputedValue { get; set; }
        public decimal? RevisedPenalty { get; set; }
        public string VendorComment { get; set; }
        public string SPAction { get; set; }
        public string SPComment { get; set; }
        public decimal? ImposedPenaltyCharge { get; set; }
        public string DisputeForwaredTo { get; set; }
        public string MediatorAction { get; set; }
        public string MediatorComments { get; set; }
        public decimal? AgreedPenaltyCharge { get; set; }
        public string PenaltyChargeAgreedWith { get; set; }
        public DateTime? PenaltyChargeAgreedDatetime { get; set; }

        //AP Adress
        public string APAddress1 { get; set; }
        public string APAddress2 { get; set; }
        public string APAddress3 { get; set; }
        public string APAddress4 { get; set; }
        public string APAddress5 { get; set; }
        public string APAddress6 { get; set; }
        public int? APCountryID { get; set; }
        public string APCountryName { get; set; }
        public string APPincode { get; set; }

        //Filter Parameters
        public string SelectedCountryIDs { get; set; }
        public string SelectedVendorIDs { get; set; }
        public string SelectedStockPlannerIDs { get; set; }
        public string SelectedSiteIDs { get; set; }
        public string SelectedODSkUCode { get; set; }
        public string SelectedBOIncurredDate { get; set; }
        public string SelectedMonth { get; set; }
        public DateTime SelectedDateTo { get; set; }
        public DateTime SelectedDateFrom { get; set; }
        public string SelectedBOReportingID { get; set; }
        public string SelectedSKUID { get; set; }
        public string SelectedBOIDPC { get; set; }

        //Grid Paging Parameters
        public int GridCurrentPageNo { get; set; }
        public int GridPageSize { get; set; }
        public int TotalRecords { get; set; }

        //
        public string Status { get; set; }
        public DateTime? DateBOIncurred { get; set; }
        public string Resolution { get; set; }
        public string QueryMonth { get; set; }
        public string APClerk { get; set; }
        public double? OTIFPenaltyCharges { get; set; }
        public string PurchaseOrderValue { get; set; }
        public int? CountofBackOrder { get; set; }
        public DateTime? BOHDate { get; set; }

        //To be deleted later
        public string Country { get; set; }
        public double? OTIFPenaltyRate { get; set; }
        public string Currency1 { get; set; }
        public string Suscribe { get; set; }

        public string Subscribe { get; set; }
        public int YTDTotalNoOfBO { get; set; }
        public int YTDTotalNoOfVendorBO { get; set; }
        public decimal? YTDPenaltyCharges { get; set; }

        public int? CountryID { get; set; }
        public decimal? Rate { get; set; }
        public decimal? QtyOnBO { get; set; }
        public string Month { get; set; }
        public decimal? AmountDebited { get; set; }
        public string SelectedAPClerkIDs { get; set; }
        public string AgreedApprovedBy { get; set; }
        public string BOReportingIDs { get; set; }
        public int? DebitRaiseId { get; set; }
        public DateTime? DateofEscalation { get; set; }
        public decimal? ValueApproved { get; set; }
        public decimal? ValueDisputed { get; set; }
        public decimal? ValuePending { get; set; }
        public int? VendorID { get; set; }
        public string vendor_No { get; set; }
        public string Vendor_Country { get; set; }
        public BackOrderMailBE BackOrderMail { get; set; }

        public MAS_DeclineReasonCodeBE DeclineCode { get; set; }

        public string SelectedDeclineReasonCodeIDs { get; set; }

        public string LastReceiptDate { get; set; }
        public string TotalQtyReceived { get; set; }
        public string SelectedVikingSkUCode { get; set; }
        public string SelectedVendorCode { get; set; }

        //BackOrder Report Parameters

        public int? StockPlannerID { get; set; }
        public int UserID { get; set; }
        public string OpenOverdue { get; set; }
        public string OrderBy { get; set; }
        public string SiteName { get; set; }
        public string VendorNo { get; set; }
        public string SKU_No { get; set; }
        public string Viking { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }
        public int Orders { get; set; }
        public decimal Value { get; set; }
        public DateTime? CommentDate { get; set; }
        public string Comments { get; set; }
        public DateTime? BoSince { get; set; }


        public int StockPlannerGroupingID { get; set; }
        public string StockPlannerGroupings { get; set; }
        public string PlannerName { get; set; }

        public int SKUID { get; set; }
        public string BackOrder { get; set; }
        public int PurchaseOrderID { get; set; }
        public string OrderRaised { get; set; }
        public string OriginalDuedate { get; set; }
        public string RevisedDueDate { get; set; }
        public string BookingDate { get; set; }

        public string CurrentBoStatus { get; set; }

        public int IsReminderCommSent { get; set; }

        public string DeclineReasonComments { get; set; }

        public string DeclineDateFrom { get; set; }
        public string DeclineDateTo { get; set; }

        public string SelectedCountryName { get; set; }
        public string SelectedVendorName { get; set; }
        public string SelectedStockPlannerName { get; set; }
        public string SelectedStockPlannerGroupID { get; set; }
        public string SelectedStockPlannerGroupName { get; set; }
        public string MediatorBOReviewStatus { get; set; }
        public string ViewByTotalsPenaltiesCharged { get; set; }
        public string ViewByTotalsPenaltiesDeclined { get; set; }
        public string ViewByTotalsPenaltiesPending { get; set; }

        public string InventoryInitialReview { get; set; }
        public string PlannerInitialUser { get; set; }
        public string InventoryInitialActionDate { get; set; }
        public string VendorReview { get; set; }
        public string VendorUser { get; set; }
        public string VendorActionTakenDate { get; set; }
        public string PlannerSecondaryReview { get; set; }
        public string PlannerSecondaryUser { get; set; }
        public string PlannerSecondaryActionTakenDate { get; set; }

        public string MediatorReview { get; set; }
        public string MediatorUser { get; set; }
        public string MediatorActionTakenDate { get; set; }

        public decimal? VendorAmount { get; set; }
        public decimal? PlannerAmount { get; set; }
        public decimal? MediatorAmount { get; set; }

        public int PlannerInitialActionDays { get; set; }
        public int VendorActionDays { get; set; }
        public int PlannerSecondaryActionDays { get; set; }
        public int MediatorActionDays { get; set; }
        public string LastBackOrderDate { get; set; }
        public string CompletedDate { get; set; }
        public int TotalDays { get; set; }

        public decimal? ImposedPenaltyChargeTotal { get; set; }
        public decimal? PotentialPenaltyChargeTotal { get; set; }

        public string ReasonForCharge { get; set; }
        public string CurrentStatus { get; set; }

        public string SummaryBy { get; set; }

        public string FirstBO { get; set; }
        public string PenaltyPendingWith { get; set; }
        public string PenaltyStatus { get; set; }

        public string SelectedMediatorIDs { get; set; }

        public bool IsMedaitionRequiredDailyEmail { get; set; }

        public string DisputeType { get; set; }

        public string IsPenaltyReviewed { get; set; }

        public string MedaitorActionSearchBy { get; set; }

        public string ReviewBy { get; set; }

        public int? Lines { get; set; }

        public string Weightage { get; set; }

        public string SpendAmount { get; set; }

        public bool IsMedActionFromBOPenaltyOverview { get; set; }
    }
}


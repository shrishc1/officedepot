﻿namespace BusinessEntities.ModuleBE.StockOverview.Stock_Turn
{
    public class StockTurnSettingBE : BaseBe
    {

        public int ODMeasure_COG { get; set; }
        public int StockTurnAverage_COG { get; set; }
        public int StockTurnAverage_INVENTORY { get; set; }
        public int StockTurnActuals_COG { get; set; }
        public int StockTurnActuals_INVENTORY { get; set; }
        public string vendorIDs { get; set; }
        public string countryIDs { get; set; }
        public int PageCount { get; set; }

    }
}

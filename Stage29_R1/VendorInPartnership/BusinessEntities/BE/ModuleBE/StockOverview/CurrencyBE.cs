﻿using System;


namespace BusinessEntities.ModuleBE.StockOverview
{
    [Serializable]
    public class CurrencyBE : BaseBe
    {
        public int CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateCreated { get; set; }
    }
}

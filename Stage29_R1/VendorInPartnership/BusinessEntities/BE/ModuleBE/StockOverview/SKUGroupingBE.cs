﻿using System;
using System.Data;

namespace BusinessEntities.ModuleBE.StockOverview
{
    [Serializable]
    public class SKUGroupingBE : BaseBe
    {

        public string GroupName { get; set; }
        public int CountryID { get; set; }
        public int SKUID { get; set; }
        public string ODSKU { get; set; }
        public string VikingSKU { get; set; }
        public string Description { get; set; }
        public DataTable Data { get; set; }
        public int? SKugroupingID { get; set; }
        public int PageCount { get; set; }
        public bool Isupdate { get; set; }
    }
}

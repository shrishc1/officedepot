﻿namespace BusinessEntities.ModuleBE.StockOverview
{
    public class POSubVendorBe : BaseBe
    {
        public int POSubVendorID { get; set; }
        public string Subvndr { get; set; }
    }
}


﻿using System;

namespace BusinessEntities.ModuleBE.StockOverview.Report
{
    public class StockOverviewReportBE : BaseBe
    {
        public string SelectedCountryIDs { get; set; }
        public string SelectedSiteIDs { get; set; }
        public string SelectedVendorIDs { get; set; }
        public DateTime? DateTo { get; set; }
        public DateTime? DateFrom { get; set; }
        public string ItemClassification { get; set; }
        public string OfficeDepotSKU { get; set; }
        public string SKUMarkedAs { get; set; }
        public int PendingSKUs { get; set; }
        public int DiscSKUs { get; set; }
        public int ActiveSKUs { get; set; }
        public int UnderReviewSKUs { get; set; }
    }
}

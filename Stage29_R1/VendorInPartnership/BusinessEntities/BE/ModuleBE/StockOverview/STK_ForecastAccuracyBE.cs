﻿using System;

namespace BusinessEntities.ModuleBE.StockOverview
{
    [Serializable]
    public class STK_ForecastAccuracyBE : BaseBe
    {

        public string CountryIds { get; set; }
        public string VendorIds { get; set; }
        public string ItemCategoryIds { get; set; }
        public string ItemCategory { get; set; }
        public string ItemClassification { get; set; }
        public string ReportType { get; set; }
        public string VendorNo { get; set; }
        public string VendorName { get; set; }
        public string Country { get; set; }
        public string OD_Sku { get; set; }
        public string Viking_Sku { get; set; }
        public string Description { get; set; }
        public string SiteName { get; set; }
        public decimal? Next20DaysForecastValue { get; set; }
        public decimal? YTDForecastDemand { get; set; }
        public decimal? YTDActualSale { get; set; }
        public decimal? YTDAbsoluteDiff { get; set; }
        public decimal? YTDForecastedAcuracyRate { get; set; }

        public decimal? JanForecastDemand { get; set; }
        public decimal? JanActualSale { get; set; }
        public decimal? JanAbsoluteDiff { get; set; }
        public decimal? JanForecastedAcuracyRate { get; set; }

        public decimal? FebForecastDemand { get; set; }
        public decimal? FebActualSale { get; set; }
        public decimal? FebAbsoluteDiff { get; set; }
        public decimal? FebForecastedAcuracyRate { get; set; }

        public decimal? MarForecastDemand { get; set; }
        public decimal? MarActualSale { get; set; }
        public decimal? MarAbsoluteDiff { get; set; }
        public decimal? MarForecastedAcuracyRate { get; set; }

        public decimal? AprForecastDemand { get; set; }
        public decimal? AprActualSale { get; set; }
        public decimal? AprAbsoluteDiff { get; set; }
        public decimal? AprForecastedAcuracyRate { get; set; }

        public decimal? MayForecastDemand { get; set; }
        public decimal? MayActualSale { get; set; }
        public decimal? MayAbsoluteDiff { get; set; }
        public decimal? MayForecastedAcuracyRate { get; set; }

        public decimal? JunForecastDemand { get; set; }
        public decimal? JunActualSale { get; set; }
        public decimal? JunAbsoluteDiff { get; set; }
        public decimal? JunForecastedAcuracyRate { get; set; }

        public decimal? JulForecastDemand { get; set; }
        public decimal? JulActualSale { get; set; }
        public decimal? JulAbsoluteDiff { get; set; }
        public decimal? JulForecastedAcuracyRate { get; set; }

        public decimal? AugForecastDemand { get; set; }
        public decimal? AugActualSale { get; set; }
        public decimal? AugAbsoluteDiff { get; set; }
        public decimal? AugForecastedAcuracyRate { get; set; }

        public decimal? SepForecastDemand { get; set; }
        public decimal? SepActualSale { get; set; }
        public decimal? SepAbsoluteDiff { get; set; }
        public decimal? SepForecastedAcuracyRate { get; set; }

        public decimal? OctForecastDemand { get; set; }
        public decimal? OctActualSale { get; set; }
        public decimal? OctAbsoluteDiff { get; set; }
        public decimal? OctForecastedAcuracyRate { get; set; }

        public decimal? NovForecastDemand { get; set; }
        public decimal? NovActualSale { get; set; }
        public decimal? NovAbsoluteDiff { get; set; }
        public decimal? NovForecastedAcuracyRate { get; set; }

        public decimal? DecForecastDemand { get; set; }
        public decimal? DecActualSale { get; set; }
        public decimal? DecAbsoluteDiff { get; set; }
        public decimal? DecForecastedAcuracyRate { get; set; }
        public decimal? Month1 { get; set; }
        public decimal? Month2 { get; set; }
        public decimal? Month3 { get; set; }
        public decimal? Month4 { get; set; }
        public decimal? Month5 { get; set; }
        public decimal? Month6 { get; set; }
        public decimal? Month7 { get; set; }
        public decimal? Month8 { get; set; }
        public decimal? Month9 { get; set; }
        public decimal? Month10 { get; set; }
        public decimal? Month11 { get; set; }
        public decimal? Month12 { get; set; }
        public decimal? Next12MonthForecastDemand { get; set; }

        public decimal? HForecastDemand { get; set; }
        public decimal? HActualSale { get; set; }
        public decimal? HAbsoluteDiff { get; set; }
        public decimal? HForecastedAcuracyRate { get; set; }
        public int PageSize { get; set; }
        public decimal? YTDAvg { get; set; }
        public decimal? JanAvg { get; set; }
        public decimal? FebAvg { get; set; }
        public decimal? MarAvg { get; set; }
        public decimal? AprAvg { get; set; }
        public decimal? MayAvg { get; set; }
        public decimal? JunAvg { get; set; }
        public decimal? JulAvg { get; set; }
        public decimal? AugAvg { get; set; }
        public decimal? SepAvg { get; set; }
        public decimal? OctAvg { get; set; }
        public decimal? NovAvg { get; set; }
        public decimal? DecAvg { get; set; }
        public decimal? HAvg { get; set; }
    }
}

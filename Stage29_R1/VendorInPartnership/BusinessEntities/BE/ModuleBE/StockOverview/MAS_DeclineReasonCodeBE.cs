﻿using System;

namespace BusinessEntities.ModuleBE.StockOverview
{
    [Serializable]
    public class MAS_DeclineReasonCodeBE : BaseBe
    {

        public int DeclineReasonCodeID { get; set; }

        public string Code { get; set; }

        public string Reason { get; set; }

        public bool IsActive { get; set; }

        public string ReasonCode { get; set; }

        public string DeclinedBy { get; set; }

        public DateTime? DeclinedDate { get; set; }

        public bool IsMandatoryComment { get; set; }

        public string IsDeclineReasonCommentMandatory { get; set; }
    }
}

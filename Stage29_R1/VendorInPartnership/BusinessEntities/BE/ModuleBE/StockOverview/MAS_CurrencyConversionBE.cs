﻿using System;
namespace BusinessEntities.ModuleBE.StockOverview
{
    [Serializable]
    public class MAS_CurrencyConversionBE : BaseBe
    {

        public int CurrencyConversionID { get; set; }
        public DateTime? RateApplicableDate { get; set; }
        public decimal ConversionRate { get; set; }
        public DateTime? FromDate { get; set; }
    }
}

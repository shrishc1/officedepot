﻿using System;

namespace BusinessEntities.ModuleBE.MgmtReports
{
    [Serializable]
    public class VIPManagementReportBE : BaseBe
    {
        public DateTime? CalcultionDate { get; set; }

        public string Area { get; set; }

        public string AreaKey { get; set; }

        public string SitePrefix { get; set; }

        public int SiteID { get; set; }

        public int SiteCountryID { get; set; }

        public double? KeyValue { get; set; }

        public int KeyOrder { get; set; }

        public DateTime UpdateDate { get; set; }

        public double? KeyValue1 { get; set; }

        public double? KeyValue2 { get; set; }

        public double? KeyValue3 { get; set; }

        public string ReportType { get; set; }

        public int Page { get; set; }

        public int TotalRecords { get; set; }

        public string KeyHeader { get; set; }

        public int PageSize { get; set; }

        public double? TotalKeyPct { get; set; }

        public string Password { get; set; }

        public string OldPassword { get; set; }

        public int UserID { get; set; }
    }
}

﻿using System;

namespace BusinessEntities.ModuleBE.VendorScorecard
{
    [Serializable]
    public class SummaryScoreCardBE : BusinessEntities.BaseBe
    {
        public int? ParentVendorID { get; set; }
        public int? VendorID { get; set; }
        public int? Year { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string CountryName { get; set; }
        public string EuropeanorLocal { get; set; }
        public decimal? NoOfPOReceived { get; set; }
        public decimal? NoOfLinesReceived { get; set; }
        public decimal? YTDScore { get; set; }
        public decimal? JanuaryScore { get; set; }
        public decimal? FebruaryScore { get; set; }
        public decimal? MarchScore { get; set; }
        public decimal? AprilScore { get; set; }
        public decimal? MayScore { get; set; }
        public decimal? JuneScore { get; set; }
        public decimal? JulyScore { get; set; }
        public decimal? AugustScore { get; set; }
        public decimal? SeptemberScore { get; set; }
        public decimal? OctoberScore { get; set; }
        public decimal? NovemberScore { get; set; }
        public decimal? DecemberScore { get; set; }
    }
}

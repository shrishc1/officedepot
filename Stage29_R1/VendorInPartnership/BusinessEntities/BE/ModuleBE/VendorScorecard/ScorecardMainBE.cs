﻿using System;

namespace BusinessEntities.ModuleBE.VendorScorecard
{
    [Serializable]
    public class ScorecardMainBE : BusinessEntities.BaseBe
    {
        public string VendorInfo { get; set; }
        public string CurrentPerformance { get; set; }
        public string HistroicPerformance { get; set; }

        public string OtifMain { get; set; }
        public string OtifHistPerformance { get; set; }
        public string OtifValues { get; set; }
        public string OtifRates { get; set; }

        public string DiscrepancyMain { get; set; }
        public string DiscrepancyRate { get; set; }
        public string DiscrepancyDays { get; set; }
        public string DiscrepancyHistPerformance { get; set; }

        public string SchedulingMain { get; set; }
        public string SchedulingHistPerformance { get; set; }
        public string SchedulingErrorCounts { get; set; }

        public string StockMain { get; set; }
        public string StockHistPerformance { get; set; }
    }
}

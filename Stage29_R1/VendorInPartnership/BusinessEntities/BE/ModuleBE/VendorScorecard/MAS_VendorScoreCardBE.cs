﻿using System;

namespace BusinessEntities.ModuleBE.VendorScorecard
{
    [Serializable]
    public class MAS_VendorScoreCardBE : BusinessEntities.BaseBe
    {
        //Overall Weight Property
        public int? OtifWeight { get; set; }
        public int? SchedulingWeight { get; set; }
        public int? DiscrepenciesWeight { get; set; }
        public int? StockWeight { get; set; }

        //Overall Score Property
        public decimal? FailureScore { get; set; }
        public decimal? TargetScore { get; set; }
        public decimal? IdealScore { get; set; }

        //OTIF Weight and Targets Property
        public int? CatAWeight { get; set; }
        public int? CatBWeight { get; set; }
        public int? CatCWeight { get; set; }
        public int? CatDWeight { get; set; }
        public int? CatOWeight { get; set; }

        public int? CatAIdeal { get; set; }
        public int? CatBIdeal { get; set; }
        public int? CatCIdeal { get; set; }
        public int? CatDIdeal { get; set; }
        public int? CatOIdeal { get; set; }

        public int? CatATarget { get; set; }
        public int? CatBTarget { get; set; }
        public int? CatCTarget { get; set; }
        public int? CatDTarget { get; set; }
        public int? CatOTarget { get; set; }

        public int? CatAFailure { get; set; }
        public int? CatBFailure { get; set; }
        public int? CatCFailure { get; set; }
        public int? CatDFailure { get; set; }
        public int? CatOFailure { get; set; }

        //Stage 7 V2 point 12
        public decimal? OtifIdeal { get; set; }
        public decimal? OtifTarget { get; set; }
        public decimal? OtifFailure { get; set; }
        //------------------//

        //Discrepancies Weight and Targets Property
        public decimal? DiscWeight { get; set; }
        public decimal? AvgWeight { get; set; }
        public decimal? DiscIdeal { get; set; }
        public decimal? AvgIdeal { get; set; }
        public decimal? DiscTarget { get; set; }
        public decimal? AvgTarget { get; set; }
        public decimal? DiscFailure { get; set; }
        public decimal? AvgFailure { get; set; }

        /// <summary>
        /// This is for Discrepancy Section For [Discrepancy Rate] 
        /// </summary>
        public int? DiscrepancyRateWeightId { get; set; }

        /// <summary>
        /// This is for Discrepancy Section For [Average Response Time]
        /// </summary>
        public int? AverageResponseTimeWeightId { get; set; }

        //Stock Weight and Targets Property
        public decimal? AvgBackorderDaysWeight { get; set; }
        public decimal? LeadTimeVarianceWeight { get; set; }

        public decimal? AvgBackorderDaysIdeal { get; set; }
        public decimal? LeadTimeVarianceIdeal { get; set; }

        public decimal? AvgBackorderDaysTargetPositive { get; set; }
        public decimal? LeadTimeVarianceTargetPositive { get; set; }

        public decimal? AvgBackorderDaysTargetNegative { get; set; }
        public decimal? LeadTimeVarianceTargetNegative { get; set; }

        public decimal? AvgBackorderDaysFailurePositive { get; set; }
        public decimal? LeadTimeVarianceFailurePositive { get; set; }

        public decimal? AvgBackorderDaysFailureNegative { get; set; }
        public decimal? LeadTimeVarianceFailureNegative { get; set; }

        //Scheduling Weight and Targets Property
        public int? Accepted { get; set; }
        public int? Refused { get; set; }

        public int? AcceptedInsufficientNoticeWeight { get; set; }
        public int? AcceptedRefusedToWaitWeight { get; set; }
        public int? AcceptedVolumeMismatchWeight { get; set; }
        public int? AcceptedIncorrecTimeWeight { get; set; }
        public int? AcceptedDamagePakagingWeight { get; set; }
        public int? AcceptedUnsafeWeight { get; set; }
        public int? AcceptedPaperworkWeight { get; set; }
        public int? AcceptedUnscheduleWeight { get; set; }
        public int? AcceptedOtherWeight { get; set; }

        public int? AcceptedInsufficientNoticeIdeal { get; set; }
        public int? AcceptedRefusedToWaitIdeal { get; set; }
        public int? AcceptedVolumeMismatchIdeal { get; set; }
        public int? AcceptedIncorrecTimeIdeal { get; set; }
        public int? AcceptedDamagePakagingIdeal { get; set; }
        public int? AcceptedUnsafeIdeal { get; set; }
        public int? AcceptedPaperworkIdeal { get; set; }
        public int? AcceptedUnscheduleIdeal { get; set; }
        public int? AcceptedOtherIdeal { get; set; }

        public int? AcceptedInsufficientNoticeTarget { get; set; }
        public int? AcceptedRefusedToWaitTarget { get; set; }
        public int? AcceptedVolumeMismatchTarget { get; set; }
        public int? AcceptedIncorrecTimeTarget { get; set; }
        public int? AcceptedDamagePakagingTarget { get; set; }
        public int? AcceptedUnsafeTarget { get; set; }
        public int? AcceptedPaperworkTarget { get; set; }
        public int? AcceptedUnscheduleTarget { get; set; }
        public int? AcceptedOtherTarget { get; set; }

        public int? AcceptedInsufficientNoticeFailure { get; set; }
        public int? AcceptedRefusedToWaitFailure { get; set; }
        public int? AcceptedVolumeMismatchFailure { get; set; }
        public int? AcceptedIncorrecTimeFailure { get; set; }
        public int? AcceptedDamagePakagingFailure { get; set; }
        public int? AcceptedUnsafeFailure { get; set; }
        public int? AcceptedPaperworkFailure { get; set; }
        public int? AcceptedUnscheduleFailure { get; set; }
        public int? AcceptedOtherFailure { get; set; }

        public int? RefusedInsufficientNoticeWeight { get; set; }
        public int? RefusedRefusedToWaitWeight { get; set; }
        public int? RefusedVolumeMismatchWeight { get; set; }
        public int? RefusedIncorrecTimeWeight { get; set; }
        public int? RefusedDamagePakagingWeight { get; set; }
        public int? RefusedUnsafeWeight { get; set; }
        public int? RefusedPaperworkWeight { get; set; }
        public int? RefusedUnscheduleWeight { get; set; }
        public int? RefusedOtherWeight { get; set; }

        public int? RefusedInsufficientNoticeIdeal { get; set; }
        public int? RefusedRefusedToWaitIdeal { get; set; }
        public int? RefusedVolumeMismatchIdeal { get; set; }
        public int? RefusedIncorrecTimeIdeal { get; set; }
        public int? RefusedDamagePakagingIdeal { get; set; }
        public int? RefusedUnsafeIdeal { get; set; }
        public int? RefusedPaperworkIdeal { get; set; }
        public int? RefusedUnscheduleIdeal { get; set; }
        public int? RefusedOtherIdeal { get; set; }

        public int? RefusedInsufficientNoticeTarget { get; set; }
        public int? RefusedRefusedToWaitTarget { get; set; }
        public int? RefusedVolumeMismatchTarget { get; set; }
        public int? RefusedIncorrecTimeTarget { get; set; }
        public int? RefusedDamagePakagingTarget { get; set; }
        public int? RefusedUnsafeTarget { get; set; }
        public int? RefusedPaperworkTarget { get; set; }
        public int? RefusedUnscheduleTarget { get; set; }
        public int? RefusedOtherTarget { get; set; }

        public int? RefusedInsufficientNoticeFailure { get; set; }
        public int? RefusedRefusedToWaitFailure { get; set; }
        public int? RefusedVolumeMismatchFailure { get; set; }
        public int? RefusedIncorrecTimeFailure { get; set; }
        public int? RefusedDamagePakagingFailure { get; set; }
        public int? RefusedUnsafeFailure { get; set; }
        public int? RefusedPaperworkFailure { get; set; }
        public int? RefusedUnscheduleFailure { get; set; }
        public int? RefusedOtherFailure { get; set; }

        //Stage 7 V2 point 12
        public decimal? SchedulingIdeal { get; set; }
        public decimal? SchedulingTarget { get; set; }
        public decimal? SchedulingFailure { get; set; }
        //------------------//

        //Vendor Country Frequency Property
        public int? VendorCountryFreqID { get; set; }
        public int? CountryID { get; set; }
        public int? VendorID { get; set; }
        public char Frequency { get; set; }

        public string VendorName { get; set; }
        public string CountryName { get; set; }
        public string SelectedVendorIDs { get; set; }
        public string gridFrequency { get; set; }
        public string IsActiveVendor { get; set; }

        public decimal? AbsoluteFail { get; set; }
        public decimal? OtifAbsoluteFail { get; set; }
        public decimal? DiscAbsoluteFail { get; set; }
        public decimal? AvgAbsoluteFail { get; set; }
        public decimal? BackorderDaysAbsoluteFail { get; set; }
        public decimal? LeadTimeVarianceAbsoluteFail { get; set; }
        public decimal? SchedulingAbsoluteFail { get; set; }

        public string VendorNo { get; set; }
    }
}

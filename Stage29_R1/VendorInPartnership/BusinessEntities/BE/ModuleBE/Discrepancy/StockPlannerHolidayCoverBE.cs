﻿using BusinessEntities.ModuleBE.Security;
using System;

namespace BusinessEntities.ModuleBE.Discrepancy
{
    [Serializable]
    public class StockPlannerHolidayCoverBE : BaseBe
    {
        public int SPHolidayID { get; set; }
        public bool IsAbsent { get; set; }
        public int? StockPlannerID { get; set; }
        public string SPToCoverIDs { get; set; }
        public string StockPlannerToCover { get; set; }
        public string DateOfReturn { get; set; }
        public int? Priority { get; set; }
        public string StockPlanner { get; set; }
        public SCT_UserBE SCT_UserBE { get; set; }
    }
}

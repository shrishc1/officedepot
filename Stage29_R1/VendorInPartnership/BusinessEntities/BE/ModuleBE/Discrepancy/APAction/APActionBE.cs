﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using System;


namespace BusinessEntities.ModuleBE.Discrepancy.APAction
{

    [Serializable]
    public class APActionBE : BaseBe
    {

        public UP_VendorBE Vendor { get; set; }
        public DiscrepancyBE Discrepancy { get; set; }

        // For Debit and Cancel Reason Type Setup
        public int? ReasonTypeID { get; set; }
        public string Reason { get; set; }
        public string ReasonType { get; set; }
        public DateTime? ReasonDate { get; set; }
        public string UserName { get; set; }

        // Debit Note Number
        public string DebitStatus { get; set; }
        public string DiscrepancyNoteNo { get; set; }
        public DateTime? DateDebitRaised { get; set; }
        public string RaisedBy { get; set; }
        public string DebitNoteNumber { get; set; }
        public decimal ValueofDebit { get; set; }
        public string Currency { get; set; }
        public string DebitSentTo { get; set; }
        public int TotalRecords { get; set; }

        //Cancel Debit Note
        public string CancelledBy { get; set; }
        public DateTime? CancelledDate { get; set; }
        public string CancelledSentTo { get; set; }
        public int? UserID { get; set; }
        public bool IsBOPenaltyRelated { get; set; }
    }
}

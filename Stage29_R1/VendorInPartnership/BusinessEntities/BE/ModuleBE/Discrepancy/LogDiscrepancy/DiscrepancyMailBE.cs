﻿using System;

namespace BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy
{
    [Serializable]
    public class DiscrepancyMailBE : BaseBe
    {

        public int discrepancyCommunicationID { get; set; }
        public int discrepancyLogID { get; set; }
        public string sentTo { get; set; }
        public string mailBody { get; set; }
        public string mailSubject { get; set; }
        public int? vendorID { get; set; }
        public int? languageID { get; set; }
        public DateTime sentDate { get; set; }
        public string communicationLevel { get; set; }
        public string errorDescription { get; set; }
        public bool MailSentInLanguage { get; set; }

        public char Status { get; set; }
        //For letter
        public DateTime LoggedDate { get; set; }
        public string LetterBody { get; set; }
        public string DiscrepancyLetterID { get; set; }
        public DISLog_QueryDiscrepancyBE QueryDiscrepancy { get; set; }

        public int? DebitRaiseCommId { get; set; }
        public int? DebitRaiseId { get; set; }
        public int? SendByID { get; set; }
        public bool IsMailSent { get; set; }
        public string CommTitle { get; set; }
        public string SentToWithLink { get; set; }
        public int? LanguageId { get; set; }
        public string DebitNoteNo { get; set; }

        public bool IsFromQuery { get; set; }

        public int? UserID { get; set; }
    }
}

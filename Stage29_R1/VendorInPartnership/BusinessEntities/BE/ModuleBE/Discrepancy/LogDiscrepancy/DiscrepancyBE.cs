﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.StockOverview;
using System;

namespace BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy
{
    [Serializable]
    public class DiscrepancyBE : BaseBe
    {
        //DiscrepancyLog Columns
        public int? DiscrepancyLogID { get; set; }
        public string VDRNo { get; set; }
        public int? SiteID { get; set; }
        public int? DiscrepancyTypeID { get; set; }
        public DateTime? DiscrepancyLogDate { get; set; }
        public int? PurchaseOrderID { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public DateTime? PurchaseOrderDate { get; set; }
        public string InternalComments { get; set; }
        public string DeliveryNoteNumber { get; set; }
        public DateTime? DeliveryArrivedDate { get; set; }
        public char? CommunicationType { get; set; }
        public string CommunicationTo { get; set; }
        public int? POSiteID { get; set; }
        public int? VendorID { get; set; }
        public int? CarrierID { get; set; }
        public string CarrierName { get; set; }

        public string PersentationIssueComment { get; set; }
        public string QualityIssueComment { get; set; }
        public string FailPalletSpecificationComment { get; set; }
        public string PrematureReceiptInvoiceComment { get; set; }
        public string GenericDiscrepancyActionRequired { get; set; }
        public string OfficeDepotContactName { get; set; }
        public string OfficeDepotContactPhone { get; set; }
        public string POSiteName { get; set; }
        public string GenericDiscrepancyIssueComment { get; set; }
        public string Location { get; set; }
        public string LogDate { get; set; }
        public string UpdatedBy { get; set; }
        //DiscrepancyItem Columns
        public int DiscrepancyItemID { get; set; }
        public int? ItemPurchaseOrderID { get; set; }
        public int? Line_no { get; set; }
        public string ODSKUCode { get; set; }
        public string DirectCode { get; set; }
        public string VendorCode { get; set; }
        public string ProductDescription { get; set; }
        public int? OutstandingQuantity { get; set; }
        public int? OriginalQuantity { get; set; }
        public string UOM { get; set; }
        public int? DeliveredNoteQuantity { get; set; }
        public int? DeliveredQuantity { get; set; }
        public int? OversQuantity { get; set; }
        public int? ShortageQuantity { get; set; }
        public int? DamageQuantity { get; set; }
        public string DamageDescription { get; set; }
        public int? AmendedQuantity { get; set; }
        public Boolean? NoPOEscalate { get; set; }
        public string PackSizeOrdered { get; set; }
        public string PackSizeReceived { get; set; }
        public int? WrongPackReceived { get; set; }
        public int? ChaseQuantity { get; set; }

        public int? StockPlannerID { get; set; }
        public string StockPlannerNO { get; set; }
        public string StockPlannerContact { get; set; }
        public string StockPlannerName { get; set; }
        public string StockPlannerEmailID { get; set; }

        public int? StockPlannerUserID { get; set; }

        public string JobTitle { get; set; }

        public string PurchaseOrder_Order_raised { get; set; }
        public string VendorNoName { get; set; }

        public string CorrectODSKUCode { get; set; }
        public string CorrectProductDescription { get; set; }
        public string CorrectUOM { get; set; }
        public string CorrectVendorCode { get; set; }

        public DateTime? Order_raised { get; set; }

        public BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE PurchaseOrder { get; set; }
        public BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE Vendor { get; set; }
        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
        public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }
        public string DiscrepancyStatus { get; set; }

        //Discrepancy Type

        public string DiscrepancyType { get; set; }

        //Discrepancy Search Parameters
        public int? SCDiscrepancyTypeID { get; set; }
        public string SCDiscrepancyNo { get; set; }
        public string SCSelectedSiteIDs { get; set; }
        public int? SCSPUserID { get; set; }
        public string SCPurchaseOrderNo { get; set; }
        public DateTime? SCPurchaseOrderDate { get; set; }
        public DateTime? SCDiscrepancyDateFrom { get; set; }
        public DateTime? SCDiscrepancyDateTo { get; set; }
        public string SCSelectedVendorIDs { get; set; }
        public string SCDeliveryNoteNumber { get; set; }
        public string SCSelectedSPIDs { get; set; }
        public string @APIds { get; set; }
        public string @OpenWith { get; set; }

        public int? UserID { get; set; }
        public int? UserRoleID { get; set; }

        //RPT_DiscrepancyWorkflow table
        public int? DiscrepancyWorkFlowID { get; set; }
        public DateTime? LoggedDateTime { get; set; }
        public int? LevelNumber { get; set; }

        public string GINHTML { get; set; }
        public bool GINActionRequired { get; set; }
        public string GINUserControl { get; set; }
        public string INVHTML { get; set; }
        public bool INVActionRequired { get; set; }
        public string INVUserControl { get; set; }
        public string APHTML { get; set; }
        public bool APActionRequired { get; set; }
        public string APUserControl { get; set; }
        public string VENHTML { get; set; }
        public bool VenActionRequired { get; set; }
        public string VENUserControl { get; set; }
        public bool CloseDiscrepancy { get; set; }
        public string NextEscalationLevel { get; set; }
        public string DELHTML { get; set; }

        public decimal? Freight_Charges { get; set; }
        public int? NumberOfPallets { get; set; }
        public decimal? TotalLabourCost { get; set; }
        public string DiscrepancyStatusDiscreption { get; set; }
        public bool? NoEscalation { get; set; }
        public string NoPaperworkComment { get; set; }

        public char? GreaterThan3DaysFlag { get; set; }

        public decimal? VendorActionElapsedTime { get; set; }

        public int? ForceClosedUserId { get; set; }
        public string ForceClosedUserName { get; set; }
        public string ForceClosedDate { get; set; }
        public string Username { get; set; }


        public string ItemVal { get; set; }
        public string CollectionAuthNumber { get; set; }

        public long? DiscrepancyLogCommentID { get; set; }
        public DateTime? CommentDateTime { get; set; }
        public string Comment { get; set; }
        public int? RowNumber { get; set; }

        public string GI { get; set; }
        public string INV { get; set; }
        public string AP { get; set; }
        public string VEN { get; set; }
        public string PickersName { get; set; }
        public string ShuttleType { get; set; }
        public int? DiscrepancyQty { get; set; }
        public DISLog_QueryDiscrepancyBE QueryDiscrepancy { get; set; }
        public int? UserIdForConsVendors { get; set; }
        public string EscalationLevel { get; set; }
        public int? DeletedUserId { get; set; }
        public DateTime? DeletedUserDate { get; set; }
        public string AccountPaybleName { get; set; }
        public string AccountPayblePhoneNo { get; set; }
        public string SearchByODSku { get; set; }
        public string SearchByCatCode { get; set; }

        public string NaxPONo { get; set; }

        public string CountryShortName { get; set; }
        public string DebitRaseType { get; set; }
        public string InvoiceNo { get; set; }
        public string ReferenceNo { get; set; }
        public string CommunicationEmails { get; set; }
        public CurrencyBE Currency { get; set; }
        public int? DebitRaiseId { get; set; }
        public decimal? POItemCost { get; set; }
        public decimal? POTotalCost { get; set; }
        public decimal? StateValueDebited { get; set; }
        public int? DebitReasonId { get; set; }
        public string Status { get; set; }
        public string DebitNoteNo { get; set; }
        public string CreatedByIds { get; set; }
        public string CancelByIds { get; set; }
        public string VendorIds { get; set; }
        public string DebitReasonIds { get; set; }
        public DateTime? DebitRaiseFromDate { get; set; }
        public DateTime? DebitRaiseToDate { get; set; }
        public DateTime? DebitRaiseDate { get; set; }
        public int? CreatedById { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByContactNumber { get; set; }
        public int? CancelById { get; set; }
        public string CancelBy { get; set; }
        public string CancelByContactNumber { get; set; }
        public DateTime? CanceledDate { get; set; }
        public string Reason { get; set; }
        public string ReasonType { get; set; }
        public int? CancelDebitReasonId { get; set; }
        public string CancelCommunicationEmails { get; set; }
        public string CancelVendorComments { get; set; }

        public string CancelReason { get; set; }
        public string CancelReasonType { get; set; }
        public MAS_CountryBE Country { get; set; }
        public string APActionRef { get; set; }

        public string APAddress1 { get; set; }
        public string APAddress2 { get; set; }
        public string APAddress3 { get; set; }
        public string APAddress4 { get; set; }
        public string APAddress5 { get; set; }
        public string APAddress6 { get; set; }
        public int? APCountryID { get; set; }
        public string APCountryName { get; set; }
        public string APPincode { get; set; }
        public string OurVATReference { get; set; }
        public string YourVATReference { get; set; }
        public string CommEmailsWithLink { get; set; }
        public string CancelCommEmailsWithLink { get; set; }
        public string ResentCommEmailsWithLink { get; set; }

        public string LanguageID { get; set; }
        public DateTime? ReSentDate { get; set; }
        public int? ReSentById { get; set; }
        public string ReSentBy { get; set; }

        public int? ODVatReferenceID { get; set; }
        public string VendorVatReference { get; set; }
        public decimal? DebitTotalValue { get; set; }
        public decimal? InvoicedItemPrice { get; set; }
        public decimal? QtyInvoiced { get; set; }
        public decimal? PriceDifference { get; set; }

        public string PONumbers { get; set; }
        public string DebitNoteNumbers { get; set; }
        public int? DebitRaiseItemId { get; set; }
        public string VikingCode { get; set; }
        public DateTime? PORaisedDate { get; set; }
        public string ACP001Control { get; set; }
        public bool? IsACP001Done { get; set; }
         
        public string IncorrectAddress { get; set; }
        public string IncorrectPackLabour { get; set; }
        public string FailedPalletSpec { get; set; }
        public string QualityIssueLabour { get; set; }
        public string PrematureInvoice { get; set; }

        public bool IsDelete { get; set; }
        public bool GoodsReturnedDriver { get; set; }


        public int? InvoiceQty { get; set; }
        public int? ReceiptedQty { get; set; }
        public string MED { get; set; }

        public string MEDHTML { get; set; }
        public bool MEDActionRequired { get; set; }
        public string MEDUserControl { get; set; }

        public string Resolution { get; set; }
        public int? DiscrepancyWorkflowID { get; set; }
        public string CreatedDate { get; set; }

        public string SCSelectedAPIDs { get; set; }
        public string DiscrepancyCharge { get; set; }

        public bool IsAPFlag { get; set; }

        public int AccountPayableID { get; set; }

        public string QtyToBeSelected { get; set; }

        public string QualityIssueLabourCostCentre { get; set; }

        public string ItemNotOnPOComment { get; set; }
        public string StockPlannerToCoverEmailID { get; set; }
        public int StockPlannerToCoverUserID { get; set; }
        public int SPToCoverStockPlannerID { get; set; }

        public int? StockPlannerIDIfNotAbsent { get; set; }

        public int? ActualStockPlannerID { get; set; }
        public bool IsInitialDiscCommunicationToSP { get; set; }

        public string Original_due_date { get; set; }

        public string ActionTakenBy { get; set; }
        public string GINActionCategory { get; set; }
        public string InventoryActionCategory { get; set; }
        public string APActionCategory { get; set; }
        public string VendorActionCategory { get; set; }
        public string ActionRequired { get; set; }

        //ActionComments
        public string ActionComments { get; set; }
        public string Decision { get; set; }
        public string GoodsIn { get; set; }
        public string GoodsInContactNo { get; set; }
        public int PageCount { get; set; }
        public bool Flag { get; set; }
        public int Page { get; set; }
        public int TotalRecords { get; set; }
    }
}


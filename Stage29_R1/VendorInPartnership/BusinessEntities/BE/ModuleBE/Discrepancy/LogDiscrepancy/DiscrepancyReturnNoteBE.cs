﻿using System;

namespace BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy
{
    public class DiscrepancyReturnNoteBE : BaseBe
    {
        public int DiscrepancyReturnNoteID { get; set; }

        public int DiscrepancyLogID { get; set; }

        public DateTime LoggedDate { get; set; }

        public string ReturnNoteBody { get; set; }

        public int? ReturnNoteID { get; set; }
    }
}

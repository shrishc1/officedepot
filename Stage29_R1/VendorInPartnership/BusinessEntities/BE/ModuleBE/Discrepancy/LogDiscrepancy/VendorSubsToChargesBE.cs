﻿using System;

namespace BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy
{

    [Serializable]
    public class VendorSubsToChargesBE : BaseBe
    {


        public int VendorID
        { get; set; }

        public bool Overs
        { get; set; }

        public bool Damage
        { get; set; }

        public bool NoPO
        { get; set; }

        public bool NoPaperwork
        { get; set; }

        public bool IncorrectProduct
        { get; set; }

        public bool IncorrectAddress
        { get; set; }

        public bool FailedPalletSpec
        { get; set; }

        public bool IncorrectPackCarriage
        { get; set; }

        public bool IncorrectPackLabour
        { get; set; }

        public bool PrematureInvoice
        { get; set; }

        public bool QualityIssueCarriage
        { get; set; }

        public bool QualityIssueLabour
        { get; set; }

        public string CountryIDs { get; set; }
        public string VendorIDs { get; set; }
        public string ChargeType { get; set; }
        public string Subscribed { get; set; }
        public string Vendor_Name { get; set; }
        public string Vendor_Country { get; set; }

        public int UpdateBy { get; set; }
    }
}

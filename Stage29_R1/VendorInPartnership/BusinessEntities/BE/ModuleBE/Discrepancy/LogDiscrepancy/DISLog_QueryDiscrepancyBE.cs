﻿using System;


namespace BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy
{
    [Serializable]
    public class DISLog_QueryDiscrepancyBE : BaseBe
    {
        public int? QueryDiscrepancyID { get; set; }
        public int? DiscrepancyLogID { get; set; }
        public string VendorComment { get; set; }
        public string GoodsInComment { get; set; }
        public string GoodsInAction { get; set; }
        public DateTime? VendorQueryDate { get; set; }
        public DateTime? GoodsInQueryDate { get; set; }
        public string Query { get; set; }
        public int? VendorUserId { get; set; }
        public string VendorUserName { get; set; }
        public int? GoodsInUserId { get; set; }
        public string GoodsInUserName { get; set; }
        public string QueryAction { get; set; }



        //Search Creteria
        public string SCSelectedSiteIDs { get; set; }
        public int? VendorID { get; set; }
        public string SCSelectedVendorIDs { get; set; }
        public DateTime? SCDiscrepancyDateFrom { get; set; }
        public DateTime? SCDiscrepancyDateTo { get; set; }
        public string ElaspedTime { get; set; }

        public string Vendor_No { get; set; }
        public string Vendor_Name { get; set; }
        public int? UserIdForConsVendors { get; set; }
        public BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyBE Discrepancy { get; set; }

        public BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy.DiscrepancyMailBE DiscrepancyMail { get; set; }

        public bool? IsQueryClosedManually { get; set; }

    }
}

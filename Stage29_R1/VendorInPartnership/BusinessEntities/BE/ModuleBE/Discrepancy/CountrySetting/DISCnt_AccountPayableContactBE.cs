﻿using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Security;
using System;
namespace BusinessEntities.ModuleBE.Discrepancy.CountrySetting
{
    [Serializable]
    public class DISCnt_AccountPayableContactBE : BaseBe
    {
        public int? VendorAccountPayableID { get; set; }
        public int? VendorID { get; set; }
        public int? UserID { get; set; }
        public int? CountryID { get; set; }
        public int IsAllVendors { get; set; }
        public SCT_UserBE User { get; set; }
        public UP_VendorBE Vendor { get; set; }
        public MAS_CountryBE Country { get; set; }
        public string UserName { get; set; }
        public string VendorIDs { get; set; }
        public int? MoveTo { get; set; }
    }
}

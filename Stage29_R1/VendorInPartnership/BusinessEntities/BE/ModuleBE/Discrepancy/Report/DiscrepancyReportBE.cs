﻿using System;

namespace BusinessEntities.ModuleBE.Discrepancy.Report
{
    [Serializable]
    public class DiscrepancyReportBE : BaseBe
    {
        public string SelectedCountryIDs { get; set; }
        public string SelectedSiteIDs { get; set; }
        public string SelectedVendorIDs { get; set; }
        public string SelectedStockPlannerIDs { get; set; }
        public string SelectedAPIDs { get; set; }
        public string VendorNumber { get; set; }
        public string VendorName { get; set; }
        public DateTime? DateTo { get; set; }
        public DateTime? DateFrom { get; set; }
        public int? DiscrepancyTypeId { get; set; }

        public string SelectedReceivingSiteIDs { get; set; }
        public string SelectedSendingSiteIDs { get; set; }
        public string SelectedTypes { get; set; }
        public string SelectedPO { get; set; }
        public string SelectedSKU { get; set; }
        public string SelectedDiscrepancyNos { get; set; }
        public string SelectedUserIDs { get; set; }
        public string DiscrepancyStatus { get; set; }
        public string StockPlannerName { get; set; }
        public string APName { get; set; }
        public int PageCount { get; set; }
    }

}

﻿using System;

namespace BusinessEntities.ModuleBE.Discrepancy
{
    [Serializable]
    public class MediatorBE : BaseBe
    {
        public int MediatorID { get; set; }
        public BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE Country { get; set; }
        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
    }
}

﻿using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System;

namespace BusinessEntities.ModuleBE.Appointment.Booking
{
    [Serializable]
    public class APPBOK_BookingBE : BaseBe
    {
        public string PurchaseOrdersIDs;
        public string PKID { get; set; }
        public int? SiteId { get; set; }
        public string SiteName { get; set; }
        public DateTime? ScheduleDate { get; set; }
        public DateTime? DeliveryDate { get; set; }

        public string BookingStatus { get; set; }
        public string BookingRef { get; set; }
        public string Prioirty { get; set; }
        public string BookingType { get; set; }
        public MASCNT_DeliveryTypeBE Delivery { get; set; }
        public string ExpectedDeliveryTime { get; set; }
        public MASSIT_FixedSlotBE FixedSlot { get; set; }
        public MASSIT_TimeWindowBE TimeWindow { get; set; }
        public MASCNT_CarrierBE Carrier { get; set; }
        public int? LiftsScheduled { get; set; }
        public string InventoryManager { get; set; }
        public int BookingStatusID { get; set; }
        public string OtherVehicle { get; set; }
        public string OtherCarrier { get; set; }
        public string PurchaseOrders { get; set; }
        public int BookingID { get; set; }
        public int StockPlannerID { get; set; }
        public string StockPlannerEmail { get; set; }

        //Delivery Arrival
        public int DLYARR_NumberOfLine { get; set; }

        public int DLYARR_NumberOfPallet { get; set; }
        public int DLYARR_NumberOfCartons { get; set; }
        public int DLYARR_NumberOfLift { get; set; }
        public string DLYARR_OperatorInital { get; set; }

        //Delivery Refusal
        public bool DLYREF_ReasonArrivedEarly { get; set; }

        public bool DLYREF_ReasonArrivedLate { get; set; }
        public bool DLYREF_ReasonNoPaperworkOndisplay { get; set; }
        public bool DLYREF_ReasonPalletsDamaged { get; set; }
        public bool DLYREF_ReasonPackagingDamaged { get; set; }
        public bool DLYREF_ReasonUnsafeLoad { get; set; }
        public bool DLYREF_ReasonWrongAddress { get; set; }
        public bool DLYREF_ReasonRefusedTowait { get; set; }
        public bool DLYREF_ReasonNottoODSpecification { get; set; }
        public bool DLYREF_ReasonOther { get; set; }
        public bool DLYREF_FailedToDeclareHazardousGoods { get; set; }

        public string DLYREF_ReasonComments { get; set; }
        public int DLYREF_SupervisorID { get; set; }
        public int? DLYREF_EuroPalletsDelivered { get; set; }
        public int? DLYREF_EuroPalletsReturned { get; set; }
        public string DLYREF_EuroPalletsComments { get; set; }
        public int? DLYREF_UKStandardDelivered { get; set; }
        public int? DLYREF_UKStandardReturned { get; set; }
        public string DLYREF_UKStandardComments { get; set; }
        public int? DLYREF_CHEPDelivered { get; set; }
        public int? DLYREF_CHEPReturned { get; set; }
        public string DLYREF_CHEPComments { get; set; }
        public int? DLYREF_OthersDelivered { get; set; }
        public int? DLYREF_OthersReturned { get; set; }
        public string DLYREF_OthersComments { get; set; }

        //Delivery Unloaded
        public string DLYUNL_OperatorInital { get; set; }

        public DateTime? DLYUNL_DateTime { get; set; }
        public int? DLYUNL_VehicleTypeId { get; set; }
        public int? DLYUNL_NumberOfLift { get; set; }
        public int? DLYUNL_NumberOfPallet { get; set; }
        public int? DLYUNL_NumberOfCartons { get; set; }
        public int? DLYUNL_RefusedPallet { get; set; }
        public int? DLYUNL_RefusedCarton { get; set; }

        public string SupplierType { get; set; }
        public MASSIT_VehicleTypeBE VehicleType { get; set; }
        public MASSIT_DoorNoSetupBE DoorNoSetup { get; set; }
        public string PreAdviseNotification { get; set; }
        public int? BookingTypeID { get; set; }
        public int? NumberOfCartons { get; set; }
        public int? NumberOfPallet { get; set; }
        public int? NumberOfLift { get; set; }
        public int? NumberOfLines { get; set; }
        public int? NumberOfDeliveries { get; set; }

        //Provisional reason edit
        public int? OldNumberOfCartons { get; set; }

        public int? OldNumberOfPallet { get; set; }
        public int? OldNumberOfLift { get; set; }
        public int? OldNumberOfLines { get; set; }

        //
        public bool DLYQCHK_DeliveryBookedCorrectly { get; set; }

        public string DLYQCHK_IncorrectlyBookedComments { get; set; }
        public string HistoryDate { get; set; }
        public string HistoryOperatorInitials { get; set; }
        public string HistoryComments { get; set; }

        //App_BookingVendor
        public int VendorID { get; set; }

        public AdminFunctions.SYS_SlotTimeBE SlotTime { get; set; }
        public string SlotType { get; set; }
        public string Status { get; set; }
        public UP_VendorBE Vendor { get; set; }
        public int? UserID { get; set; }
        public string VendorIDs { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int WeekDay { get; set; }
        public string StartTime { get; set; }
        public int BookingCount { get; set; }

        public bool IsOnlineBooking { get; set; }

        public bool IsBookingAmended { get; set; }

        public string BookingDay { get; set; }
        public string BookingDate { get; set; }

        public DateTime SelectedScheduleDate { get; set; }

        public string BookingComments { get; set; }
        public object TransactionComments { get; set; }
        public string SiteAddress { get; set; }

        public string ViewType { get; set; }
        public int? LanguageID { get; set; }
        public string Language { get; set; }
        public string MultiBookingIDs { get; set; }
        public string MultiPONO { get; set; }
        public string MultiPOIDs { get; set; }
        public string IsMultiVendCarrier { get; set; }

        public int? NonWindowFromTimeID { get; set; }
        public int? NonWindowToTimeID { get; set; }

        public int VendorCarrierID { get; set; }
        public string UserIds { get; set; }
        public string UserName { get; set; }

        public string ProvisionalReason { get; set; }
        public string TimeSlotWindow { get; set; }

        public string SelectedSiteIds { get; set; }
        public int SkuId { get; set; }
        public string ConfirmPOArrival { get; set; }
        public string DeliveredPO { get; set; }
        public string NotDeliveredPO { get; set; }
        public string DeliveredVendorIds { get; set; }
        public string NotDeliveredVendorIds { get; set; }

        public int? SelectedCountryID { get; set; }
        public string SelectedCountryIDs { get; set; }
        public string SelectedVendorIDs { get; set; }
        public string SelectedCarrierIDs { get; set; }
        public string SelectedStockPlannerIDs { get; set; }

        public int? Accepted { get; set; }
        public int? Rejected { get; set; }
        public int? Total { get; set; }
        public int? ProvNoSpace { get; set; }
        public int? ProvWindowCapacity { get; set; }
        public int? ProvFixedSlot { get; set; }
        public int? SiteCountryID { get; set; }
        public string KeyHeader { get; set; }

        public int? ProvBookedById { get; set; }
        public string ProvBookedBy { get; set; }
        public string ProvType { get; set; }
        public string ProvAction { get; set; }
        public int? ProvODUserId { get; set; }
        public string ProvODUser { get; set; }

        public int? NoShowByID { get; set; }
        public string NoShowBy { get; set; }
        public DateTime? NoShowDate { get; set; }
        public int TotalRecords { get; set; }
        public int KeyValue { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string NoShowByUser { get; set; }
        public BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE PurchaseOrder { get; set; }

        public string ScheduledNotBookedIn { get; set; }
        public string NotScheduledBookedIn { get; set; }
        public DateTime? DueDate { get; set; }
        public int? NumberOfContainer { get; set; }
        public int? POChaseCommunicationId { get; set; }
        public string Country { get; set; }
        public string DisplayType { get; set; }

        public int? ProvisionalReasonType { get; set; }
        public DateTime? OriginalDueDate { get; set; }
        public int? Variance { get; set; }
        public string ContactNumber { get; set; }
        public string ContactName { get; set; }
        public bool? IsReviewed { get; set; }

        public string NonWindowFromToTime { get; set; }
        public string WindowStartEndTime { get; set; }

        public DateTime? BookingDat { get; set; }

        public string GraphType { get; set; }

        public string Volume { get; set; }

        public string DueDateFull { get; set; }
        public bool IsBookingAccepted { get; set; }

        public DateTime? DeletedDate { get; set; }

        public int PageCount { get; set; }
        public int RecordCount { get; set; }
        public int PurchaseOrderID { get; set; }
        public bool IsNotDelieveredEmailSent { get; set; }
        public string PO { get; set; }
        public bool IsCrossDayBefore { get; set; }
        public bool IsVenodrPalletChecking { get; set; }
        public bool IsStandardPalletChecking { get; set; }
        public bool DeliveryChecked { get; set; }
        public bool IssuesFound { get; set; }
        public string CheckedBy { get; set; }
        public string ISPM15Comments { get; set; }

        public string NoOfPallets { get; set; }
        public string NoOfCartoons { get; set; }
        public bool IsEnableHazardouesItemPrompt { get; set; }
        public string DLYUNL_UploadedFileName { get; set; }
        public int GridCurrentPageNo { get; set; }
        public int GridPageSize { get; set; }
        public bool IsUnloadImagesUploaded { get; set; }

        public string SKU { get; set; }
        public string CatCode { get; set; } 
        public int PageIndex { get; set; }
        public bool ISPM15CountryPalletChecking { get; set; }
        public int ISPM15FromCountryID { get; set; }
        public string HazardousCommments { get; set; }
    }
}
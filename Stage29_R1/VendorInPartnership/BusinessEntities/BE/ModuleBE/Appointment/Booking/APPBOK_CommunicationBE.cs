﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessEntities.ModuleBE.Appointment.Booking
{
    [Serializable]
    public class APPBOK_CommunicationBE : BaseBe
    {
        public int CommunicationID { get; set; }
        public string CommunicationType { get; set; }
        public string Subject { get; set; }
        public string SentFrom { get; set; }
        public string SentTo { get; set; }
        public string Body { get; set; }
        public string SentDate { get; set; }
        public int? SendByID { get; set; }
        public object BookingID { get; set; }
        public string FullName { get; set; }
        public bool IsMailSent { get; set; }
        public int LanguageID { get; set; }
        public bool MailSentInLanguage { get; set; }

        // PoChase Communication
        public int POChaseId { get; set; }

        public string CommunicationStatus { get; set; }
        public string SentToWithLink { get; set; }
        public string VendorEmailIds { get; set; }
        public DateTime? POChaseSentDate { get; set; }
        public string MultiPoIds { get; set; }
        public int POChaseCommunicationId { get; set; }
        public string Purchase_order { get; set; }
        public int SiteID { get; set; }
        public DateTime OrderRaised { get; set; }
        public string Country { get; set; }
    }

    public class CommunicationType
    {
        public enum Enum
        {
            NotDefined = 0,
            BookingConfirmation,
            UnexpectedAccepted,
            UnexpectedRefused,
            RefusedAll,
            RefusedPartial,
            DeliveryIssue,
            NoShow,
            BookingAmended,
            ProvisionalRefusal,
            ProvisionalReject,
            BookingDeleted,
            ISPM15Issue,
            IsStandardPalletCheck,
        }

        public static string Name(int id)
        {
            return Name((Enum)id);
        }

        public static string Name(Enum id)
        {
            switch (id)
            {
                case Enum.NotDefined:
                    return "Not Defined";

                case Enum.BookingConfirmation:
                    return "Booking Confirmation";

                case Enum.UnexpectedAccepted:
                    return "Unexpected Accepted";

                case Enum.UnexpectedRefused:
                    return "Unexpected Refused";

                case Enum.RefusedAll:
                    return "Refused All";

                case Enum.RefusedPartial:
                    return "Refused Partial";

                case Enum.DeliveryIssue:
                    return "Delivery Issue";

                case Enum.NoShow:
                    return "No Show";

                case Enum.BookingAmended:
                    return "Booking Amended";

                case Enum.ProvisionalRefusal:
                    return "Provisional Refusal";

                case Enum.ProvisionalReject:
                    return "Provisional Reject";

                case Enum.BookingDeleted:
                    return "Booking Deleted";

                case Enum.ISPM15Issue:
                    return "ISPM15 Issue";

                case Enum.IsStandardPalletCheck:
                    return "Standard Pallet Check Issue";

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static List<KeyValuePair<int, string>> List()
        {
            return System.Enum.GetValues(typeof(Enum)).Cast<int>().Select(x => new KeyValuePair<int, string>(x, Name(x))).ToList();
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace BusinessEntities.ModuleBE.Appointment.Booking
{
    public class DashboardBooking
    {
        public int? SiteId { get; set; }
        public DateTime? Date { get; set; }
        public int? Bookings { get; set; }
        public int? Pos { get; set; }
        public int? Pallets { get; set; }
        public int? Cartons { get; set; }
        public int? Lifts { get; set; }
        public int? Lines { get; set; }
        public int? OpenDiscrepancies { get; set; }
        public int? TotalDiscrepancies { get; set; }
        public int? BookingsWithIssues { get; set; }
        public string ID { get; set; }
        public int? ArrivedPallets { get; set; }
        public int? NotArrivedPallets { get; set; }
        public int? NoShowPallets { get; set; }
        public int? ArrivedLines { get; set; }
        public int? NotArrivedLines { get; set; }
        public List<DateDashBoard> DateTimes { get; set; }
    }

    public class DateDashBoard
    {
        public string ID { get; set; }
        public DateTime? Date { get; set; }
        public bool IsActive { get; set; }
    }
}

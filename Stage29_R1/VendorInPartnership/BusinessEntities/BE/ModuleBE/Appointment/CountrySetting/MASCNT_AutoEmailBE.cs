﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System;

namespace BusinessEntities.ModuleBE.Appointment.CountrySetting
{
    [Serializable]
    public class MASCNT_AutoEmailBE
: BaseBe
    {

        public int AutoEmailID { get; set; }
        public string ModifiedOn { get; set; }
        //public bool IsAutoEmailNewPO { get; set; }
        public BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE Country { get; set; }
        public BusinessEntities.ModuleBE.Security.SCT_UserBE ModifiedByID { get; set; }
        public UP_VendorBE UP_VendorBE { get; set; }
        public int GridCurrentPageNo { get; set; }
        public int GridPageSize { get; set; }
        public int TotalRecord { get; set; }
        public int EmailStatus { get; set; }

        public int IsAutoEmailNewPO { get; set; }
    }
}

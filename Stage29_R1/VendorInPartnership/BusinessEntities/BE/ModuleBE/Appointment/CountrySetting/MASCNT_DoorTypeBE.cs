﻿using BusinessEntities.ModuleBE.AdminFunctions;
using System;

namespace BusinessEntities.ModuleBE.Appointment.CountrySetting
{
    [Serializable]
    public class MASCNT_DoorTypeBE : BusinessEntities.BaseBe
    {

        public int DoorTypeID { get; set; }
        public int CountryID { get; set; }
        public string DoorType { get; set; }
        public bool IsActive { get; set; }

        public MAS_CountryBE Country { get; set; }

        public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }
        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
        public BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE Vehicle { get; set; }
    }
}

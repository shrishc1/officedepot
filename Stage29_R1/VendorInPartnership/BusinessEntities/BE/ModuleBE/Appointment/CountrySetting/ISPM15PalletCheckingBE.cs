﻿using System;

namespace BusinessEntities.ModuleBE.Appointment.CountrySetting
{
    [Serializable]
    public class ISPM15PalletCheckingBE : BaseBe
    {
        public int SourceCountryID { get; set; }
        public int DestinationCountryID { get; set; }
        public string SourceCountryName { get; set; }
        public string DestinationCountryName { get; set; }
        public bool ISPM15Check { get; set; }
        public int UserID { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public int ID { get; set; }
        public int GridCurrentPageNo { get; set; }
        public int GridPageSize { get; set; }
        public double TotalRecords { get; set; }
        public string AddedDate { get; set; }
        public string ModifiedDate { get; set; }
        public int SiteID { get; set; }

    }
}

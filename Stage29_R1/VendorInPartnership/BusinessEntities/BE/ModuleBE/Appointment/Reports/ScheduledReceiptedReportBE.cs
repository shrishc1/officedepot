﻿using System;

namespace BusinessEntities.ModuleBE.Appointment.Reports
{
    [Serializable]
    public class ScheduledReceiptedReportDetailsBE : BaseBe
    {
        public string SiteIds { get; set; }
        public int SiteId { get; set; }
        public int VendorID { get; set; }
        public string Date { get; set; }
        public string SiteName { get; set; }
        public int LinesDue { get; set; }
        public int LinesBooked { get; set; }
        public int LinesReceipted { get; set; }
        public string VendorName { get; set; }
        public string BookingReferenceID { get; set; }
        public int TotalReceipted { get; set; }
        public string PO_NO { get; set; }
        public int TotalReceipt { get; set; }
        public int PageCount { get; set; }
    }
}

﻿using System;

namespace BusinessEntities.ModuleBE.Appointment.Reports
{
    [Serializable]
    public class SiteVolumeTrackerBE : BaseBe
    {
        public DateTime? CalculationDate { get; set; }

        public int SiteID { get; set; }

        public string Area { get; set; }

        public string AreaKey { get; set; }
        public double? KeyValue { get; set; }
        public int KeyOrder { get; set; }

        public int? OnBackorder { get; set; }
        public int? LinesDue { get; set; }
        public int? ProjectedBacklog { get; set; }
        public int? MaxLines { get; set; }

        public string AreaKey1 { get; set; }
        public double? Key1Value { get; set; }
        public int Key1Order { get; set; }

        public string AreaKey2 { get; set; }
        public double? Key2Value { get; set; }
        public int Key2Order { get; set; }

        public string AreaKey3 { get; set; }
        public double? Key3Value { get; set; }
        public int Key3Order { get; set; }

        public string AreaKey4 { get; set; }
        public double? Key4Value { get; set; }
        public int Key4Order { get; set; }

        public string AreaKey5 { get; set; }
        public double? Key5Value { get; set; }
        public int Key5Order { get; set; }

        public string AreaKey6 { get; set; }
        public double? Key6Value { get; set; }
        public int Key6Order { get; set; }

        public string AreaKey7 { get; set; }
        public double? Key7Value { get; set; }
        public int Key7Order { get; set; }

        public int? AvgLinesDue { get; set; }
        public int? AvgProjectedBacklog { get; set; }

        public int? DateType { get; set; }
        public int WeekCounter { get; set; }
    }
}

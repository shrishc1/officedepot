﻿using System;

namespace BusinessEntities.ModuleBE.Appointment.Reports
{
    [Serializable]
    public class FixedSlotUsageReportBE : BaseBe
    {

        public string VendorName { get; set; }
        public string SlotTime { get; set; }
        public string DoorNumber { get; set; }
        public int? MaximumPallets { get; set; }
        public int? MaximumCatrons { get; set; }
        public int? MaximumLines { get; set; }
        public string Monday { get; set; }
        public string Tuesday { get; set; }
        public string Wednesday { get; set; }
        public string Thursday { get; set; }
        public string Friday { get; set; }
        public string Saturday { get; set; }
        public string Sunday { get; set; }
        public string Day { get; set; }
        public int TOTALCOUNT { get; set; }
        public int USED { get; set; }
        public decimal PercUsed { get; set; }
        public string UsedPallets { get; set; }
        public string UsedCartons { get; set; }
        public string UsedLines { get; set; }

        public int VendorID { set; get; }
        public int SiteID { set; get; }
        public int Period { set; get; }
        public int FixedSlotID { get; set; }
        public string ScheduleType { get; set; }
        public int CarrierID { set; get; }
        public string AllocationType { get; set; }
    }
}

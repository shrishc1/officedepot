﻿
using System;
namespace BusinessEntities.ModuleBE.Appointment.SiteSettings
{
    [Serializable]
    public class MASSIT_TimeWindowBE : BaseBe
    {
        public int? TimeWindowID { get; set; }

        public string WindowName { get; set; }

        public int SiteID { get; set; }

        public string Weekday { get; set; }

        public string StartWeekday { get; set; }

        public int SiteDoorNumberID { get; set; }

        public int Priority { get; set; }

        public int StartSlotTimeID { get; set; }

        public int EndSlotTimeID { get; set; }

        public int MaximumPallets { get; set; }

        public int MaximumLines { get; set; }

        public int MaximumCartons { get; set; }

        public int MaximumLift { get; set; }

        public int MaxDeliveries { get; set; }

        public bool IsCheckSKUTable { get; set; }

        public int LessVolumePallets { get; set; }

        public int GraterVolumePallets { get; set; }

        public string TotalHrsMinsAvailable { get; set; }

        public string ExcludedVendorIDs { get; set; }

        public string ExcludedCarrierIDs { get; set; }

        public string ExcludedCarrierNames { get; set; }

        public string ExcludedVendorNames { get; set; }

        public string MaximumTime { get; set; }

        public DateTime? ScheduleDate { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public string DoorNumber { get; set; }

        public int SiteDoorTypeID { get; set; }

        public string StartTimeWeekday { get; set; }

        public string EndTimeWeekday { get; set; }

        //Error List
        public string MaxVolumeError { get; set; }

        public string MaxDailyCapacityError { get; set; }
        public string MaxDailyDeliveryError { get; set; }

        public string BookingRef { get; set; }

        public int BeforeOrderBYId { get; set; }

        public int LessVolumeLines { get; set; }
        public int GraterVolumeLines { get; set; }
        public string IncludedVendorIDs { get; set; }
        public string IncludedCarrierIDs { get; set; }
        public string IncludedCarrierNames { get; set; }
        public string IncludedVendorNames { get; set; }

        public bool IsSameConfig { get; set; }

        public bool? Monday { get; set; }

        public bool? Tuesday { get; set; }

        public bool? Wednesday { get; set; }

        public bool? Thursday { get; set; }

        public bool? Friday { get; set; }

        public bool? Saturday { get; set; }

        public bool? Sunday { get; set; }


        public string ScheduleType { get; set; }

        public string VendorName { get; set; }
        public string Vendor_No { get; set; }
        public int ParentVendorID { get; set; }

        public int StartSlotTimeOrderBY { get; set; }
        public int EndSlotTimeOrderBY { get; set; }
    }
}

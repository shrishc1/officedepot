﻿// -----------------------------------------------------------------------
// <copyright file="MASSIT_ODSKUDoorMatrixSetupBE.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace BusinessEntities.ModuleBE.Appointment.SiteSettings
{
    using BusinessEntities.ModuleBE.AdminFunctions;
    using BusinessEntities.ModuleBE.Security;
    using System;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [Serializable]
    public class MASSIT_ODSKUDoorMatrixSetupBE : BaseBe
    {
        public int SkuDoorMatrixID { get; set; }
        public MAS_SiteBE Site { get; set; }
        public string ODSkuNumber { get; set; }
        public MASSIT_DoorNoSetupBE DoorNumber { get; set; }

        public SCT_UserBE User { get; set; }
    }
}

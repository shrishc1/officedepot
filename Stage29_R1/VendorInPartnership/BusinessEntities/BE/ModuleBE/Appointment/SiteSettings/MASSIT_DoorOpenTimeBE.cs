﻿using System;

namespace BusinessEntities.ModuleBE.Appointment.SiteSettings
{
    [Serializable]
    public class MASSIT_DoorOpenTimeBE : BaseBe
    {
        public int? DoorConstraintSpecificID { get; set; }

        public int SiteDoorNumberID { get; set; }

        public string Weekday { get; set; }

        public string StartWeekday { get; set; }
        public string EndWeekday { get; set; }

        public int SlotTimeID { get; set; }

        public bool IsInUsed { get; set; }

        public string DoorConstraintIDs { get; set; }
        public string DoorConstraintIDsForEndDay { get; set; }
        public string StartWeekDays { get; set; }

        public BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE DoorType { get; set; }
        public BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_DoorNoSetupBE DoorNo { get; set; }
        public string PurchaseNumbers { get; set; }
    }
}

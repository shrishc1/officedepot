﻿using System;
namespace BusinessEntities.ModuleBE.Appointment.SiteSettings
{
    [Serializable]
    public class MASSIT_VendorProcessingWindowBE : BusinessEntities.BaseBe
    {

        public int SiteVendorID { get; set; }
        public int SiteID { get; set; }
        public int VendorID { get; set; }
        public int APP_PalletsUnloadedPerHour { get; set; }
        public int APP_CartonsUnloadedPerHour { get; set; }
        public int APP_ReceiptingLinesPerHour { get; set; }
        public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }
        public BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE Vendor { get; set; }
        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
    }
}

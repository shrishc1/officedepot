﻿using System;

namespace BusinessEntities.ModuleBE.Appointment.SiteSettings
{
    [Serializable]
    public class MASSIT_HolidayBE : BaseBe
    {
        public int SiteHolidayID { get; set; }

        public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }

        public DateTime? HolidayDate { get; set; }

        public string Reason { get; set; }

        public bool IsActive { get; set; }

        public string SiteIDs { get; set; }

        public DateTime? HolidayYear { get; set; }
    }
}

﻿using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using System;

namespace BusinessEntities.ModuleBE.Appointment.SiteSettings
{
    [Serializable]
    public class MASSIT_BookingTypeExclusionBE : BaseBe
    {

        public string SiteVendorIDs { get; set; }
        public string SiteCarrierIDs { get; set; }
        public string SiteDeliveryIDs { get; set; }
        public int SiteID { get; set; }
        public int SiteVendorID { get; set; }
        public int SiteCarrierID { get; set; }
        public int SiteDeliveryID { get; set; }

        public MASCNT_CarrierBE Carrier { get; set; }
        public MASCNT_DeliveryTypeBE Delivery { get; set; }

        public bool APP_IsBookingValidationExcluded { get; set; }
        public int Vendor_Country { get; set; }
        public string Vendor_No { get; set; }
        public UP_VendorBE Vendor { get; set; }

        public int BookingTypeId { get; set; }
        public string BookingTypeDescription { get; set; }
    }
}

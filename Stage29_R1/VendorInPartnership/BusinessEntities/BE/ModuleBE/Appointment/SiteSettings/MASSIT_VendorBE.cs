﻿using System;

namespace BusinessEntities.ModuleBE.Appointment.SiteSettings
{

    [Serializable]
    public class MASSIT_VendorBE : BaseBe
    {

        public int SiteVendorID { get; set; }

        public int SiteID { get; set; }

        public int VendorID { get; set; }

        public int BookingID { get; set; }

        public bool? APP_CheckingRequired { get; set; }

        public int? APP_MaximumPallets { get; set; }

        public int? APP_MaximumLines { get; set; }

        public int? APP_MaximumDeliveriesInADay { get; set; }

        public bool? APP_CannotDeliveryOnMonday { get; set; }

        public bool? APP_CannotDeliveryOnTuesday { get; set; }

        public bool? APP_CannotDeliveryOnWednessday { get; set; }

        public bool? APP_CannotDeliveryOnThrusday { get; set; }

        public bool? APP_CannotDeliveryOnFriday { get; set; }

        public bool? APP_CannotDeliveryOnSaturday { get; set; }

        public bool? APP_CannotDeliveryOnSunday { get; set; }

        public DateTime? APP_CannotDeliverAfter { get; set; }

        public DateTime? APP_CannotDeliverBefore { get; set; }

        public bool? APP_IsNonTimeDeliveryAllowed { get; set; }

        public bool? APP_IsBookingValidationExcluded { get; set; }

        public int? APP_PalletsUnloadedPerHour { get; set; }

        public int? APP_CartonsUnloadedPerHour { get; set; }

        public int? APP_ReceiptingLinesPerHour { get; set; }

        public string OTIF_LeadTime { get; set; }

        public bool IsActive { get; set; }
        public int? SlotTimeID { get; set; }
        public int? BeforeSlotTimeID { get; set; }
        public int? AfterSlotTimeID { get; set; }

        public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }
        public BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE Vendor { get; set; }
        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
        public BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE SlotTime { get; set; }
        public int? NumberOfCartons { get; set; }
        public int? NumberOfPallet { get; set; }
        public bool IsConstraintsDefined { get; set; }
        public bool IsProcessingDefined { get; set; }

        public int BeforeOrderBYId { get; set; }
        public int AfterOrderBYId { get; set; }
        public string UserName { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}

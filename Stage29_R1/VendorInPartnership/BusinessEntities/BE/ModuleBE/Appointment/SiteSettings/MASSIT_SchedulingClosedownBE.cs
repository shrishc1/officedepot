﻿using System;

namespace BusinessEntities.ModuleBE.Appointment.SiteSettings
{
    public class MASSIT_SchedulingClosedownBE : BaseBe
    {

        public int SchedulingClosedownID { get; set; }

        public string WarningMessage { get; set; }

        public int SiteID { get; set; }

        public string SiteName { get; set; }

        public int CreateById { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }


    }
}

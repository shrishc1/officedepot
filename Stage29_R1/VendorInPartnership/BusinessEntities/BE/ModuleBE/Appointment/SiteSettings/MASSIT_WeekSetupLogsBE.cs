﻿using System;


namespace BusinessEntities.ModuleBE.Appointment.SiteSettings
{
    public class MASSIT_WeekSetupLogsBE : BaseBe
    {

        public int? SiteID { get; set; }
        public DateTime? LogTime { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int? MaximumLift { get; set; }
        public int? MaximumPallet { get; set; }
        public int? MaximumContainer { get; set; }
        public int? MaximumLine { get; set; }
        public int? MaximumDeliveries { get; set; }
        public string Type { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string SiteName { get; set; }
    }
}

﻿namespace BusinessEntities.ModuleBE.OnTimeInFull
{
    public class OTIFRulesSettingBE : BaseBe
    {
        public int RuleSettingID { get; set; }

        public int AbsoluteDaysAllowedEarly { get; set; }

        public int AbsoluteDaysAllowedLate { get; set; }

        public int AbsoluteNumberOfDeliveriesAllowed { get; set; }

        public int ToleratedDaysAllowedEarly1 { get; set; }

        public int ToleratedDaysAllowedLate1 { get; set; }

        public int ToleratedNumberOfDeliveriesAllowed1 { get; set; }

        public int ODMeasureNumberOfDeliveriesAllowed11 { get; set; }

        public int ODMeasureDaysAllowedEarly11 { get; set; }

        public int ODMeasureDaysAllowedLate11 { get; set; }

        public bool BookingProcess { get; set; }

        public bool DailyScheduler { get; set; }

    }
}

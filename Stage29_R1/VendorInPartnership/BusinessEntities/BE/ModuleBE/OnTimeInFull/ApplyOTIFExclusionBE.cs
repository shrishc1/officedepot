﻿using System;

namespace BusinessEntities.ModuleBE.OnTimeInFull
{
    [Serializable]
    public class ApplyOTIFExclusionBE : BaseBe
    {
        public int? PurchaseOrderID { get; set; }
        public string Purchase_order { get; set; }
        public string ProductDescription { get; set; }
        public int? SKUID { get; set; }
        public int SiteID { get; set; }
        public string UserName { get; set; }
        public string VendorName { get; set; }
        public DateTime? DateApplied { get; set; }
        public string SelectedProductCodes { get; set; }
        public string Reason { get; set; }
        public string Vendor_No { get; set; }
        public int VendorID { get; set; }
        public DateTime? Order_raised { get; set; }
        public string SelectedVendorIDs { get; set; }
        public string CountryName { get; set; }
        public string SiteName { get; set; }
        public BusinessEntities.ModuleBE.Upload.UP_SKUBE SKU { get; set; }
        public int? ApplyExclusionID { get; set; }
        public int GridCurrentPageNo { get; set; }
        public int GridPageSize { get; set; }
        public int TotalRecords { get; set; }

        public string DateTo { get; set; }
        public string DateFrom { get; set; }
        public string CapacityFull { get; set; }
        public int LinesDue { get; set; }
        public int LinesExcluded { get; set; }
        public string PerExcluded { get; set; }

        public string OriginalDueDate { get; set; }
        public string CapacityReachDate { get; set; }
        public int? WorkinDaysCount { get; set; }
        public bool IsDateCheck { get; set; }

        public string Warehouse { get; set; }
        public int LinesExcludedManually { get; set; }
        public int AppliedHitAutomated { get; set; }
        public int AppliedHitManually { get; set; }
        public string PercentwithHits { get; set; }
        public int Exceptions { get; set; }
        public string OverallExceptions { get; set; }
        public bool IsFromExcludedPoPage { get; set; }
        public int CountryID { get; set; }
        public int TotalRecord { get; set; }
        public int? ExcludedStatus { get; set; }
        public int UserID { get; set; }

        public string WhoAppliedTheOtifExclusion { get; set; }
    }
}

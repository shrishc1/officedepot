﻿using BusinessEntities.ModuleBE.GlobalSettings;
using DataAccessLayer.ModuleDAL.GlobalSettings;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.GlobalSettings
{
    public class VendorPOCExclusionBAL : BaseBAL
    {
        public int? SaveVendorPOCExclusionBAL(VendorPOCExclusionBE oVendorPOCExclusion)
        {
            int? intResult = 0;
            try
            {
                VendorPOCExclusionDAL oPOCSummaryDAL = new VendorPOCExclusionDAL();
                intResult = oPOCSummaryDAL.SaveVendorPOCExclusionDAL(oVendorPOCExclusion);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public List<VendorPOCExclusionBE> GetVendorPOCExclusionBAL(VendorPOCExclusionBE oVendorPOCExclusion)
        {
            List<VendorPOCExclusionBE> lst = new List<VendorPOCExclusionBE>();
            try
            {
                VendorPOCExclusionDAL oPOCSummaryDAL = new VendorPOCExclusionDAL();
                lst = oPOCSummaryDAL.GetVendorPOCExclusionDAL(oVendorPOCExclusion);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lst;
        }
        public int? DeleteVendorPOCExclusionBAL(VendorPOCExclusionBE oVendorPOCExclusion)
        {
            int? intResult = 0;
            try
            {
                VendorPOCExclusionDAL oPOCSummaryDAL = new VendorPOCExclusionDAL();
                intResult = oPOCSummaryDAL.DeleteVendorPOCExclusionDAL(oVendorPOCExclusion);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public VendorPOCExclusionBE GetEditVendorPOCExclusionBAL(VendorPOCExclusionBE oVendorPOCExclusion)
        {
            VendorPOCExclusionBE lst = new VendorPOCExclusionBE();
            try
            {
                VendorPOCExclusionDAL oPOCSummaryDAL = new VendorPOCExclusionDAL();
                lst = oPOCSummaryDAL.GetEditVendorPOCExclusionDAL(oVendorPOCExclusion);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lst;
        }
    }
}

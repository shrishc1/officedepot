﻿using BusinessEntities.ModuleBE.GlobalSettings;
using DataAccessLayer.ModuleDAL.GlobalSettings;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.GlobalSettings
{
    public class PointsofContactSummaryBAL
    {

        public List<PointsofContactSummaryBE> GetPOCSummaryBAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            List<PointsofContactSummaryBE> oPointsofContactSummaryBEList = new List<PointsofContactSummaryBE>();

            try
            {
                PointsofContactSummaryDAL oPOCSummaryDAL = new PointsofContactSummaryDAL();
                oPointsofContactSummaryBEList = oPOCSummaryDAL.GetPOCSummaryDAL(oPointsofContactSummaryBE);
                oPOCSummaryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oPointsofContactSummaryBEList;
        }

        public List<PointsofContactSummaryBE> GetPOCDetailsBAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            List<PointsofContactSummaryBE> oPointsofContactSummaryBEList = new List<PointsofContactSummaryBE>();

            try
            {
                PointsofContactSummaryDAL oPOCSummaryDAL = new PointsofContactSummaryDAL();
                oPointsofContactSummaryBEList = oPOCSummaryDAL.GetPOCDetailsDAL(oPointsofContactSummaryBE);
                oPOCSummaryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oPointsofContactSummaryBEList;
        }

        public Dictionary<string, List<PointsofContactSummaryBE>> GetPOCDetailsSiteBAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            Dictionary<string, List<PointsofContactSummaryBE>> oPointsofContactSummaryBEList = new Dictionary<string, List<PointsofContactSummaryBE>>();

            try
            {
                PointsofContactSummaryDAL oPOCSummaryDAL = new PointsofContactSummaryDAL();
                oPointsofContactSummaryBEList = oPOCSummaryDAL.GetPOCDetailsSiteDAL(oPointsofContactSummaryBE);
                oPOCSummaryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oPointsofContactSummaryBEList;
        }

        public List<PointsofContactSummaryBE> GetVendorAPContactSummaryDetailsBAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            List<PointsofContactSummaryBE> oPointsofContactSummaryBEList = new List<PointsofContactSummaryBE>();

            try
            {
                PointsofContactSummaryDAL oPOCSummaryDAL = new PointsofContactSummaryDAL();
                oPointsofContactSummaryBEList = oPOCSummaryDAL.GetVendorAPContactSummaryDetailsDAL(oPointsofContactSummaryBE);
                oPOCSummaryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oPointsofContactSummaryBEList;
        }

        public List<PointsofContactSummaryBE> GetVendorAPContactDetailsBAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            List<PointsofContactSummaryBE> oPointsofContactSummaryBEList = new List<PointsofContactSummaryBE>();

            try
            {
                PointsofContactSummaryDAL oPOCSummaryDAL = new PointsofContactSummaryDAL();
                oPointsofContactSummaryBEList = oPOCSummaryDAL.GetVendorAPContactDetailsDAL(oPointsofContactSummaryBE);
                oPOCSummaryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oPointsofContactSummaryBEList;
        }

        public int? SaveAPClerk(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            int? intResult = 0;
            try
            {
                PointsofContactSummaryDAL oPOCSummaryDAL = new PointsofContactSummaryDAL();
                intResult = oPOCSummaryDAL.SaveAPClerkDAL(oPointsofContactSummaryBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<PointsofContactSummaryBE> GetVendorSPSummaryBAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            List<PointsofContactSummaryBE> oPointsofContactSummaryBEList = new List<PointsofContactSummaryBE>();

            try
            {
                PointsofContactSummaryDAL oPOCSummaryDAL = new PointsofContactSummaryDAL();
                oPointsofContactSummaryBEList = oPOCSummaryDAL.GetVendorSPSummaryDAL(oPointsofContactSummaryBE);
                oPOCSummaryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oPointsofContactSummaryBEList;
        }

        public List<PointsofContactSummaryBE> GetVendorSPDetailBAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            List<PointsofContactSummaryBE> oPointsofContactSummaryBEList = new List<PointsofContactSummaryBE>();

            try
            {
                PointsofContactSummaryDAL oPOCSummaryDAL = new PointsofContactSummaryDAL();
                oPointsofContactSummaryBEList = oPOCSummaryDAL.GetVendorSPDetailBAL(oPointsofContactSummaryBE);
                oPOCSummaryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oPointsofContactSummaryBEList;
        }

    }
}

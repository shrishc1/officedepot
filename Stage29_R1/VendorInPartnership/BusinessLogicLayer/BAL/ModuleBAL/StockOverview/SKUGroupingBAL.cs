﻿using BusinessEntities.ModuleBE.StockOverview;
using DataAccessLayer.ModuleDAL.StockOverview;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.StockOverview
{
    public class SKUGroupingBAL
    {
        public DataTable SaveSkuGroupingBAL(SKUGroupingBE oSKUGroupingBE)
        {
            DataTable dt = null;
            try
            {
                SKUGroupingDAL oSKUGroupingDAL = new SKUGroupingDAL();
                dt = oSKUGroupingDAL.SaveSkuGroupingDAL(oSKUGroupingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

        public DataTable SearchSkuGroupingBAL(SKUGroupingBE oSKUGroupingBE, out int RecordCount)
        {
            DataTable dt = null;
            SKUGroupingDAL oSKUGroupingDAL = new SKUGroupingDAL();
            return dt = oSKUGroupingDAL.SearchSkuGroupingDAL(oSKUGroupingBE, out RecordCount);
        }

        public DataTable ADDRemoveSkuGroupingBAL(SKUGroupingBE oSKUGroupingBE, out int RecordCount)
        {
            DataTable dt = null;
            SKUGroupingDAL oSKUGroupingDAL = new SKUGroupingDAL();
            return dt = oSKUGroupingDAL.ADDRemoveSkuGroupingDAL(oSKUGroupingBE, out RecordCount);
        }
        public List<SKUGroupingBE> GetStockPlannerGroupingsBAL(SKUGroupingBE oStockPlannerGroupingsBE)
        {
            List<SKUGroupingBE> oSKUGroupinglist = new List<SKUGroupingBE>();
            try
            {
                SKUGroupingDAL oSKUGroupingDAL = new SKUGroupingDAL();
                oSKUGroupinglist = oSKUGroupingDAL.GetStockPlannerGroupingsDAL(oStockPlannerGroupingsBE);
                oSKUGroupingDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oSKUGroupinglist;
        }
    }
}

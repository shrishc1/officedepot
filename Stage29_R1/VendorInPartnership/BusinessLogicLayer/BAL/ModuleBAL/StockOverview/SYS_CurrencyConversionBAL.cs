﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessEntities.ModuleBE.StockOverview;
using DataAccessLayer.ModuleDAL.StockOverview;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.StockOverview
{
  public class SYS_CurrencyConversionBAL
    {

      public List<SYS_CurrencyConversionBE> GetCurrenyConversionBAL(SYS_CurrencyConversionBE SYS_CurrencyConversionBE)
      {
          List<SYS_CurrencyConversionBE> oSYS_CurrencyConversionBEList = new List<SYS_CurrencyConversionBE>();

          try
          {
              SYS_CurrencyConversionDAL oSYS_CurrencyConversionDAL = new SYS_CurrencyConversionDAL();

              oSYS_CurrencyConversionBEList = oSYS_CurrencyConversionDAL.GetCurrenyConversionDAL(SYS_CurrencyConversionBE);
          }
          catch (Exception ex)
          {
              LogUtility.SaveLogEntry(ex.Message + " |      Error Source:- " + ex.Source);
          }
          finally { }
          return oSYS_CurrencyConversionBEList;
      }


      public int? addEditCurrencyConversionBAL(SYS_CurrencyConversionBE SYS_CurrencyConversionBE)
      {
          int? intResult = 0;
          try
          {
              SYS_CurrencyConversionDAL oSYS_CurrencyConversionDAL = new SYS_CurrencyConversionDAL();
              intResult = oSYS_CurrencyConversionDAL.addEditCurrencyConversionDAL(SYS_CurrencyConversionBE);
          }
          catch (Exception ex)
          {
              LogUtility.SaveLogEntry(ex.Message + " |      Error Source:- " + ex.Source);
          }
          finally { }
          return intResult;
      }

    }
}

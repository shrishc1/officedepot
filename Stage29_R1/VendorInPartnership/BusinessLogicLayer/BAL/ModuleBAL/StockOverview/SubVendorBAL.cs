﻿using BusinessEntities.ModuleBE.StockOverview;
using DataAccessLayer.ModuleDAL.StockOverview;
using System;
using System.Collections.Generic;
using Utilities;


namespace BusinessLogicLayer.ModuleBAL.StockOverview
{
    public class SubVendorBAL
    {
        public List<POSubVendorBe> GetPOSubVendorBAL(POSubVendorBe pOSubVendor)
        {
            var pOSubVendors = new List<POSubVendorBe>();
            try
            {
                SubVendorDAL subVendorDAL = new SubVendorDAL();
                pOSubVendors = subVendorDAL.GetPOSubVendorDAL(pOSubVendor);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return pOSubVendors;
        }
    }
}

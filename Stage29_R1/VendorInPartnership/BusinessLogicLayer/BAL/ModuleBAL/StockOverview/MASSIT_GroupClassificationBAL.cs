﻿using BusinessEntities.ModuleBE.StockOverview;
using DataAccessLayer.ModuleDAL.StockOverview;
using System;
using System.Collections.Generic;
using Utilities;
namespace BusinessLogicLayer.ModuleBAL.StockOverview
{
    public class MASSIT_GroupClassificationBAL
    {
        public List<MASSIT_GroupClassificationBE> GetGroupClassificationBAL(MASSIT_GroupClassificationBE oMASSIT_GroupClassificationBE)
        {
            List<MASSIT_GroupClassificationBE> oMASSIT_GroupClassificationBEList = new List<MASSIT_GroupClassificationBE>();

            try
            {
                MASSIT_GroupClassificationDAL oMASSIT_GroupClassificationDAL = new MASSIT_GroupClassificationDAL();

                oMASSIT_GroupClassificationBEList = oMASSIT_GroupClassificationDAL.GetGroupClassificationDAL(oMASSIT_GroupClassificationBE);
                oMASSIT_GroupClassificationDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_GroupClassificationBEList;
        }


        public int? addEditGroupClassificationBAL(MASSIT_GroupClassificationBE oMASSIT_GroupClassificationBE)
        {
            int? intResult = 0;
            try
            {
                MASSIT_GroupClassificationDAL oMASSIT_GroupClassificationDAL = new MASSIT_GroupClassificationDAL();
                intResult = oMASSIT_GroupClassificationDAL.addEditGroupClassificationDAL(oMASSIT_GroupClassificationBE);
                oMASSIT_GroupClassificationDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? deleteGroupClassificationBAL(MASSIT_GroupClassificationBE oMASSIT_GroupClassificationBE)
        {
            int? intResult = 0;
            try
            {
                MASSIT_GroupClassificationDAL oMASSIT_GroupClassificationDAL = new MASSIT_GroupClassificationDAL();
                intResult = oMASSIT_GroupClassificationDAL.deleteGroupClassificationDAL(oMASSIT_GroupClassificationBE);
                oMASSIT_GroupClassificationDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

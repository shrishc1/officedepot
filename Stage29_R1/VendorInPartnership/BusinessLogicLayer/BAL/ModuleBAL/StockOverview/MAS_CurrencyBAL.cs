﻿using BusinessEntities.ModuleBE.StockOverview;
using DataAccessLayer.ModuleDAL.StockOverview;
using System;
using System.Collections.Generic;
using Utilities;
namespace BusinessLogicLayer.ModuleBAL.StockOverview
{
    public class MAS_CurrencyBAL
    {

        public List<CurrencyBE> GetCurrenyBAL(CurrencyBE oCurrencyBE)
        {
            List<CurrencyBE> lstCurrencyBE = new List<CurrencyBE>();

            try
            {
                MAS_CurrencyDAL oMAS_CurrencyDAL = new MAS_CurrencyDAL();

                lstCurrencyBE = oMAS_CurrencyDAL.GetCurrenyDAL(oCurrencyBE);

                oMAS_CurrencyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstCurrencyBE;
        }


        public int? addEditCurrencyBAL(CurrencyBE oCurrencyBE)
        {
            int? intResult = 0;
            try
            {
                MAS_CurrencyDAL oMAS_CurrencyDAL = new MAS_CurrencyDAL();
                intResult = oMAS_CurrencyDAL.addEditCurrencyDAL(oCurrencyBE);
                oMAS_CurrencyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

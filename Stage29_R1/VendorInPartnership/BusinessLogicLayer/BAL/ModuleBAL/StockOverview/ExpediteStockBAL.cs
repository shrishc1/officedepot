﻿using BusinessEntities.ModuleBE.StockOverview;
using DataAccessLayer.ModuleDAL.StockOverview;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.StockOverview
{
    public class ExpediteStockBAL
    {

        public List<ExpediteStockBE> GetAllReasonTypesBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            List<ExpediteStockBE> oMAS_ExpStockBEList = new List<ExpediteStockBE>();

            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                oMAS_ExpStockBEList = oExpediteStockDAL.GetAllReasonTypesDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_ExpStockBEList;
        }

        public DataTable GetAllReasonTypeDetailsBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();
            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                dt = oExpediteStockDAL.GetAllReasonTypeDetailsDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public int? addEditReasonTypeSetupBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            int? intResult = 0;
            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                intResult = oExpediteStockDAL.addEditReasonTypeSetupDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public ExpediteStockBE GetExpediteDetailsByIdBAL(ExpediteStockBE oMASCNT_ExpediteBE)
        {
            ExpediteStockBE oNewMASCNT_ExpediteBE = new ExpediteStockBE();
            List<ExpediteStockBE> oMASCNT_ExpediteBEList = new List<ExpediteStockBE>();
            try
            {
                ExpediteStockDAL oAPPCNT_ExpediteDAL = new ExpediteStockDAL();

                oMASCNT_ExpediteBEList = oAPPCNT_ExpediteDAL.GetExpediteDetailsDAL(oMASCNT_ExpediteBE);
                oNewMASCNT_ExpediteBE = oMASCNT_ExpediteBEList[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewMASCNT_ExpediteBE;
        }

        public int? SaveExpediteCommentsBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            int? intResult = 0;
            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                intResult = oExpediteStockDAL.SaveExpediteCommentsDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<ExpediteStockBE> GetExpediteCommentsBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            List<ExpediteStockBE> oMAS_ExpStockBEList = new List<ExpediteStockBE>();

            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                oMAS_ExpStockBEList = oExpediteStockDAL.GetExpediteCommentsDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_ExpStockBEList;
        }

        public DataTable GetReasonBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();
            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                dt = oExpediteStockDAL.GetAllReasonDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataTable ViewHistoricCommntsBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();
            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                dt = oExpediteStockDAL.ViewHistoricCommntsDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public List<ExpediteStockBE> GetPotentialOutputBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            List<ExpediteStockBE> oMAS_ExpStockBEList = new List<ExpediteStockBE>();

            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                oMAS_ExpStockBEList = oExpediteStockDAL.GetPotentialOutputDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_ExpStockBEList;
        }
        public DataTable GetPotentialOutputExportBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            var dt = new DataTable();

            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                dt = oExpediteStockDAL.GetPotentialOutputExportDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public List<ExpediteStockBE> GetTrackingReportBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            List<ExpediteStockBE> oMAS_ExpStockBEList = new List<ExpediteStockBE>();

            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                oMAS_ExpStockBEList = oExpediteStockDAL.GetTrackingReportDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_ExpStockBEList;
        }

        public List<ExpediteStockBE> GetExpediateStockBAL(ExpediteStockBE oMAS_ExpStockBE)
        {

            List<ExpediteStockBE> oMAS_ExpediteBEList = new List<ExpediteStockBE>();
            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                oMAS_ExpediteBEList = oExpediteStockDAL.GetExpediateStockDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_ExpediteBEList;
        }

        public List<ExpediteStockBE> GetAllMrpPurcMatGroupBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            var oMAS_ExpediteBEList = new List<ExpediteStockBE>();
            try
            {
                var oExpediteStockDAL = new ExpediteStockDAL();
                oMAS_ExpediteBEList = oExpediteStockDAL.GetAllMrpPurcMatGroupDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_ExpediteBEList;
        }

        public List<ExpediteStockBE> GetAllSOHBAL(ExpediteStockBE ExpediteStock)
        {
            var oMAS_ExpediteBEList = new List<ExpediteStockBE>();
            try
            {
                var oExpediteStockDAL = new ExpediteStockDAL();
                oMAS_ExpediteBEList = oExpediteStockDAL.GetAllSOHDAL(ExpediteStock);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_ExpediteBEList;
        }

        public List<ExpediteStockBE> GetSKUOnHandQtyBAL(ExpediteStockBE ExpediteStock)
        {
            var oMAS_ExpediteBEList = new List<ExpediteStockBE>();
            try
            {
                var oExpediteStockDAL = new ExpediteStockDAL();
                oMAS_ExpediteBEList = oExpediteStockDAL.GetSKUOnHandQtyDAL(ExpediteStock);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_ExpediteBEList;
        }

        public int? SaveExpediteSKUBAL(ExpediteStockBE oMAS_ExpediteBE)
        {
            int? intResult = 0;
            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                intResult = oExpediteStockDAL.SaveExpediteSKUDAL(oMAS_ExpediteBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;

        }
        public int? SaveExpCommunicationBAL(ExpediteStockBE oMAS_ExpediteBE)
        {

            int? intResult = 0;
            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                intResult = oExpediteStockDAL.SaveExpCommunicationDAL(oMAS_ExpediteBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataTable GetVendorLanguageBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            DataTable dt = new DataTable();
            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                dt = oExpediteStockDAL.GetVendorLanguageDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public List<ExpediteStockBE> GetExpediteCommentsHistoryBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            List<ExpediteStockBE> oMAS_ExpStockBEList = new List<ExpediteStockBE>();

            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                oMAS_ExpStockBEList = oExpediteStockDAL.GetExpediteCommentsHistoryDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_ExpStockBEList;
        }

        public List<ExpediteStockBE> GetBackOrderHistoryBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            List<ExpediteStockBE> oMAS_ExpStockBEList = new List<ExpediteStockBE>();

            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                oMAS_ExpStockBEList = oExpediteStockDAL.GetBackOrderHistoryDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_ExpStockBEList;
        }


        public List<ExpediteStockBE> GetEmailCommunicationBAL(ExpediteStockBE oExpediteStockBE)
        {
            List<ExpediteStockBE> oMAS_ExpStockBEList = new List<ExpediteStockBE>();
            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                oMAS_ExpStockBEList = oExpediteStockDAL.GetEmailCommunicationDAL(oExpediteStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_ExpStockBEList;
        }


        public void GetPriorityItemsListBAL(ExpediteStockBE oMAS_ExpStockBE, out List<ExpediteStockBE> ExpediteBEList, out int RecordCount)
        {
            List<ExpediteStockBE> oMAS_ExpStockBEList = new List<ExpediteStockBE>();
            ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
            oExpediteStockDAL.GetPriorityItemsListDAL(oMAS_ExpStockBE, out ExpediteBEList, out RecordCount);
        }


        public List<ExpediteStockBE> GetDescriptionBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            List<ExpediteStockBE> oMAS_ExpStockBEList = new List<ExpediteStockBE>();

            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                oMAS_ExpStockBEList = oExpediteStockDAL.GetDescriptionDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_ExpStockBEList;
        }

        public int? SavePriorityReasonBAL(ExpediteStockBE oMAS_ExpStockBE)
        {
            int? intResult = 0;
            try
            {
                ExpediteStockDAL oExpediteStockDAL = new ExpediteStockDAL();
                intResult = oExpediteStockDAL.SavePriorityItemReasonDAL(oMAS_ExpStockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


    }
}

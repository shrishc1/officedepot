﻿using BusinessEntities.ModuleBE.StockOverview;
using DataAccessLayer.ModuleDAL.StockOverview;
using System.Collections.Generic;
using System.Data;
namespace BusinessLogicLayer.ModuleBAL.StockOverview
{
    public class SKT_ForecastAccuracyBAL
    {
        public void GetForcastAccuracyDataBAL(STK_ForecastAccuracyBE ForecastAccuracyBE, out List<STK_ForecastAccuracyBE> forecastAccBEList, out int RecordCount)
        {
            SKT_ForecastAccuracyDAL objForecastAccuracyDAL = new SKT_ForecastAccuracyDAL();
            objForecastAccuracyDAL.GetForcastAccuracyDataDAL(ForecastAccuracyBE, out forecastAccBEList, out RecordCount);

            objForecastAccuracyDAL = null;

        }

        public void GetLongTermForcastAccuracyDataBAL(STK_ForecastAccuracyBE ForecastAccuracyBE, out List<STK_ForecastAccuracyBE> forecastAccBEList, out int RecordCount)
        {
            SKT_ForecastAccuracyDAL objForecastAccuracyDAL = new SKT_ForecastAccuracyDAL();
            objForecastAccuracyDAL.GetLongTermForcastAccuracyDataDAL(ForecastAccuracyBE, out forecastAccBEList, out RecordCount);
            objForecastAccuracyDAL = null;

        }

        public void GetForcastAccuracyDataExportBAL(STK_ForecastAccuracyBE ForecastAccuracyBE, out DataTable forecastAccBEList, out int RecordCount)
        {
            SKT_ForecastAccuracyDAL objForecastAccuracyDAL = new SKT_ForecastAccuracyDAL();

            objForecastAccuracyDAL.GetForcastAccuracyDataExportDAL(ForecastAccuracyBE, out forecastAccBEList, out RecordCount);

            objForecastAccuracyDAL = null;

        }

    }
}

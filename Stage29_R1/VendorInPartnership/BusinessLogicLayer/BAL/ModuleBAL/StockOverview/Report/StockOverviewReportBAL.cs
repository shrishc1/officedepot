﻿using BusinessEntities.ModuleBE.StockOverview.Report;
using DataAccessLayer.ModuleDAL.StockOverview.Report;
using System;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.StockOverview.Report
{
    public class StockOverviewReportBAL
    {

        StockOverviewReportBE oStockOverviewReportBE = new StockOverviewReportBE();

        public DataSet getDiscrepancyReportBAL(StockOverviewReportBE oStockOverviewReportBE)
        {
            DataSet Result = null;
            try
            {
                StockOverviewReportDAL oStockOverviewReportDAL = new StockOverviewReportDAL();
                Result = oStockOverviewReportDAL.getStockOverviewReportDAL(oStockOverviewReportBE);
                oStockOverviewReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;

        }

    }
}

﻿using BusinessEntities.ModuleBE.StockOverview.Stock_Turn;
using DataAccessLayer.ModuleDAL.StockOverview.StockTurn;
using System;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.StockOverview.StockTurn
{
    public class StockTurnBAL
    {
        public int? AddStockTurn(StockTurnSettingBE stockTurnSettingBE)
        {
            int? inserted = 0;
            try
            {
                StockTurnDAL oStockTurnDal = new StockTurnDAL();
                inserted = oStockTurnDal.ADDStockTurnSettings(stockTurnSettingBE);
                oStockTurnDal = null;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return inserted;
        }

        public StockTurnSettingBE ViewStockTurn(StockTurnSettingBE stockTurnSettingBE)
        {
            try
            {
                StockTurnDAL oStockTurnDal = new StockTurnDAL();
                stockTurnSettingBE = oStockTurnDal.ViewStockTurn(stockTurnSettingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return stockTurnSettingBE;
        }

        public void StockTurnVendorReport(StockTurnSettingBE stockTurnSettingBE, out DataSet dsResult, out int RecordCount)
        {
            StockTurnDAL oStockTurnDal = new StockTurnDAL();
            oStockTurnDal.StockTurnVendorReport(stockTurnSettingBE, out dsResult, out RecordCount);
        }

    }
}

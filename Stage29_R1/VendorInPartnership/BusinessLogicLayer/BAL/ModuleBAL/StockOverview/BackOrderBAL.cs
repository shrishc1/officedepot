﻿using BusinessEntities.ModuleBE.StockOverview;
using DataAccessLayer.ModuleDAL.StockOverview;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.StockOverview
{
    public class BackOrderBAL
    {
        public List<BackOrderBE> GetVendorPenaltyBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetVendorPenaltyDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public int? AddEditVendorPenaltyBAL(BackOrderBE oBackOrderBE)
        {
            int? intResult = 0;
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                intResult = oBackOrderDAL.AddEditVendorPenaltyDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<BackOrderBE> GetPendingPenaltyApprovalBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetPendingPenaltyApprovalDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public List<BackOrderBE> GetOpenBackOrderBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetOpenBackOrderDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }
        public List<BackOrderBE> GetOpenBackOrderListingBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetOpenBackOrderListingDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public int? UpdateODSKUStatusBAL(BackOrderBE oBackOrderBE)
        {
            int? intResult = 0;
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                intResult = oBackOrderDAL.UpdateODSKUStatusDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<BackOrderBE> GetVendorViewBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetVendorViewDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }
        //----------------------POPUP--------------------
        public List<BackOrderBE> GetVendorViewBALPopUp(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetVendorViewDALPopUp(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public int? UpdateVendorReviewStatusBAL(BackOrderBE oBackOrderBE)
        {
            int? intResult = 0;
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                intResult = oBackOrderDAL.UpdateVendorReviewStatusDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<BackOrderBE> GetGetDisputeReviewListingBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetDisputeReviewListingDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }
        public List<BackOrderBE> GetDisputeDetailBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetDisputeDetailDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public List<BackOrderBE> GetQueryDetailBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetQueryDetailDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }


        public int? UpdateDisputeReviewStatusBAL(BackOrderBE oBackOrderBE)
        {
            int? intResult = 0;
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                intResult = oBackOrderDAL.UpdateDisputeReviewStatusDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<BackOrderBE> GetEsclationOverviewBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetEsclationOverviewDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public int? UpdateMediatorReviewStatusBAL(BackOrderBE oBackOrderBE)
        {
            int? intResult = 0;
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                intResult = oBackOrderDAL.UpdateMediatorReviewStatusDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<BackOrderBE> GetPenaltyCountOnDashboardBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetPenaltyCountOnDashnoardDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public bool CheckVendorSubscribeBAL(BackOrderBE BackOrderBE)
        {
            bool IsVendorSubscribe = false;
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                IsVendorSubscribe = oBackOrderDAL.CheckVendorSubscribeDAL(BackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return IsVendorSubscribe;
        }


        public List<BackOrderBE> GetVendorCommunication2DetailBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetVendorCommunication2DetailDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }
        public int? AddCommunicationDetailBAL(BackOrderMailBE oBackOrderMailBE)
        {
            int? intResult = 0;
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                intResult = oBackOrderDAL.AddCommunicationDetailDAL(oBackOrderMailBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<BackOrderBE> GetVendorDetailBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetVendorDetailDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public List<BackOrderBE> GetBOPenaltyTrackerDataBAL(BackOrderBE oBackOrderBE)
        {
            var lstBackOrderDAL = new List<BackOrderBE>();
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetBOPenaltyTrackerDataDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public List<BackOrderBE> GetBOChargesDetailsBAL(BackOrderBE oBackOrderBE)
        {
            var lstBackOrderDAL = new List<BackOrderBE>();
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetBOChargesDetailsDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public List<BackOrderBE> GetBOChargesEmailDetailsBAL(BackOrderBE oBackOrderBE)
        {
            var lstBackOrderDAL = new List<BackOrderBE>();
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetBOChargesEmailDetailsDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public int? UpdateBOReportingByAPBAL(BackOrderBE oBackOrderBE)
        {
            int? intResult = 0;
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                intResult = oBackOrderDAL.UpdateBOReportingByAPDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? IsVendorPendingForApproveBOBAL(BackOrderBE oBackOrderBE)
        {
            int? intResult = 0;
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                intResult = oBackOrderDAL.IsVendorPendingForApproveBODAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<BackOrderBE> GetOpenBOChargesDetailsBAL(BackOrderBE oBackOrderBE)
        {
            var lstBackOrderDAL = new List<BackOrderBE>();
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetOpenBOChargesDetailsDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public List<BackOrderBE> GetOpenBOChargesEmailDetailsBAL(BackOrderBE oBackOrderBE)
        {
            var lstBackOrderDAL = new List<BackOrderBE>();
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetOpenBOChargesEmailDetailsDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public List<BackOrderBE> GetGetBOEmailEscalationDataBAL(BackOrderBE oBackOrderBE)
        {
            var lstBackOrderDAL = new List<BackOrderBE>();
            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetGetBOEmailEscalationDataDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public List<BackOrderBE> GetDeclineReportBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetDeclineReportDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }
        public List<BackOrderBE> GetBackOrderTop20BAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetBackOrderTop20DAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public List<BackOrderBE> GetBackOrderTop20_ManagerViewBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetBackOrderTop20_ManagerViewDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public List<BackOrderBE> GetSKU_PODetailsBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetSKU_PODetailsDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public DataSet GetPenaltyReviewBAL(BackOrderBE oBackOrderBE)
        {
            DataSet ds = new DataSet();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                ds = oBackOrderDAL.GetPenaltyReviewDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public List<BackOrderBE> GetStockPlannerContactDetailsBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetStockPlannerContactDetailsDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }


        public DataSet GetBackOrderPenaltyReportBAL(BackOrderBE oBackOrderBE)
        {
            DataSet ds = new DataSet();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                ds = oBackOrderDAL.GetBackOrderPenaltyReportDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }


        public List<BackOrderBE> GetBackOrderPenaltyDetailedReportBAL(BackOrderBE oBackOrderBE)
        {
            List<BackOrderBE> lstBackOrderDAL = new List<BackOrderBE>();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                lstBackOrderDAL = oBackOrderDAL.GetBackOrderPenaltyDetailedReportDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstBackOrderDAL;
        }

        public DataSet GetPlannerPenaltyReportBAL(BackOrderBE oBackOrderBE)
        {
            DataSet ds = new DataSet();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                ds = oBackOrderDAL.GetPlannerPenaltyReportDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }
        public DataSet GetMediatorPenaltyReportBAL(BackOrderBE oBackOrderBE)
        {
            DataSet ds = new DataSet();

            try
            {
                BackOrderDAL oBackOrderDAL = new BackOrderDAL();
                ds = oBackOrderDAL.GetMediatorPenaltyReportDAL(oBackOrderBE);
                oBackOrderDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }




    }

}

﻿using BusinessEntities.ModuleBE.StockOverview;
using DataAccessLayer.ModuleDAL.StockOverview;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.StockOverview
{
    public class MAS_DeclineReasonCodeBAL
    {

        public List<MAS_DeclineReasonCodeBE> GetDeclineReasonCodeBAL(MAS_DeclineReasonCodeBE oMAS_DeclineReasonCodeBE)
        {
            List<MAS_DeclineReasonCodeBE> oSYS_DeclineReasonCodeBEList = new List<MAS_DeclineReasonCodeBE>();

            try
            {
                MAS_DeclineReasonCodeDAL oSYS_DeclineReasonCodeDAL = new MAS_DeclineReasonCodeDAL();

                oSYS_DeclineReasonCodeBEList = oSYS_DeclineReasonCodeDAL.GetDeclineReasonCodeDAL(oMAS_DeclineReasonCodeBE);

                oSYS_DeclineReasonCodeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSYS_DeclineReasonCodeBEList;
        }


        public int? addEditDeclineReasonCodeBAL(MAS_DeclineReasonCodeBE oMAS_DeclineReasonCodeBE)
        {
            int? intResult = 0;
            try
            {
                MAS_DeclineReasonCodeDAL oSYS_DeclineReasonCodeDAL = new MAS_DeclineReasonCodeDAL();
                intResult = oSYS_DeclineReasonCodeDAL.addEditDeclineReasonCodeDAL(oMAS_DeclineReasonCodeBE);
                oSYS_DeclineReasonCodeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

    }
}

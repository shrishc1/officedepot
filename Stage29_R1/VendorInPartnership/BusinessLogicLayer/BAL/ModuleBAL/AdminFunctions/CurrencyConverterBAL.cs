﻿using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
    public class CurrencyConverterBAL : BusinessLogicLayer.BaseBAL
    {
        public List<CurrencyConverterBE> BindCurrencyBAL(CurrencyConverterBE o_CurrencyBE)
        {
            List<CurrencyConverterBE> o_CurrencyBElist = new List<CurrencyConverterBE>();

            try
            {
                CurrencyConverterDAL ocurrencyDAL = new CurrencyConverterDAL();
                o_CurrencyBElist = ocurrencyDAL.BindCurrencyDAL(o_CurrencyBE);
                ocurrencyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return o_CurrencyBElist;

        }

        public List<CurrencyConverterBE> BindCurrencyddlBAL(CurrencyConverterBE o_CurrencyBE)
        {
            List<CurrencyConverterBE> o_CurrencyBElist = new List<CurrencyConverterBE>();

            try
            {
                CurrencyConverterDAL ocurrencyDAL = new CurrencyConverterDAL();
                o_CurrencyBElist = ocurrencyDAL.BindCurrencyddlDAL(o_CurrencyBE);
                ocurrencyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return o_CurrencyBElist;

        }

        public int? CheckCurrencyBAL(CurrencyConverterBE o_CurrencyBE)
        {
            int? iCount = 0;
            try
            {
                CurrencyConverterDAL ocurrencyDAL = new CurrencyConverterDAL();
                iCount = ocurrencyDAL.CheckCurrencyDAL(o_CurrencyBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return iCount;
        }



        public int? addEditCurrencyConverterBAL(CurrencyConverterBE o_CurrencyBE)
        {
            int? intResult = 0;
            try
            {

                CurrencyConverterDAL ocurrencyDAL = new CurrencyConverterDAL();
                intResult = ocurrencyDAL.AddEditCurrencyConverterDAL(o_CurrencyBE);
                ocurrencyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }



}
﻿using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
    public class MAS_TradeEntitiesBAL : BusinessLogicLayer.BaseBAL
    {
        public List<MAS_TradeEntitiesBE> GetTradeEntitiesBAL(MAS_TradeEntitiesBE oMAS_TradeEntitiesBE)
        {
            List<MAS_TradeEntitiesBE> oMAS_TradeEntitiesBEList = new List<MAS_TradeEntitiesBE>();
            try
            {
                MAS_TradeEntitiesDAL oMAS_TradeEntitiesDAL = new MAS_TradeEntitiesDAL();
                oMAS_TradeEntitiesBEList = oMAS_TradeEntitiesDAL.GetTradeEntitiesDAL(oMAS_TradeEntitiesBE);
                oMAS_TradeEntitiesDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_TradeEntitiesBEList;
        }


        public int? addEditTradeEntitiesDetailsBAL(MAS_TradeEntitiesBE oMAS_TradeEntitiesBE)
        {
            int? intResult = 0;
            try
            {
                MAS_TradeEntitiesDAL oMAS_TradeEntitiesDAL = new MAS_TradeEntitiesDAL();
                intResult = oMAS_TradeEntitiesDAL.addEditTradeEntitiesDetailsDAL(oMAS_TradeEntitiesBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

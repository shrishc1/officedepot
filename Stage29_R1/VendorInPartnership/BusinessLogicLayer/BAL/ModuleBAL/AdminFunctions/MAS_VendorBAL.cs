﻿using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
    public class MAS_VendorBAL : BusinessLogicLayer.BaseBAL
    {
        public List<MAS_VendorBE> GetVendorBAL(MAS_VendorBE oMAS_VendorBE)
        {
            List<MAS_VendorBE> oMAS_VendorBEList = new List<MAS_VendorBE>();
            try
            {
                MAS_VendorDAL oMAS_VendorDAL = new MAS_VendorDAL();
                oMAS_VendorBEList = oMAS_VendorDAL.GetVendorDAL(oMAS_VendorBE);
                oMAS_VendorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_VendorBEList;
        }

        public MAS_VendorBE UpdateVendorBAL(MAS_VendorBE oMAS_VendorBE)
        {
            try
            {
                MAS_VendorDAL oMAS_VendorDAL = new MAS_VendorDAL();
                oMAS_VendorBE = oMAS_VendorDAL.UpdateVendorBAL(oMAS_VendorBE);
                oMAS_VendorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_VendorBE;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oMAS_VendorBE"></param>
        /// <returns></returns>
        public MAS_VendorBE GetVendorBALByID(MAS_VendorBE oMAS_VendorBE)
        {
            List<MAS_VendorBE> oMAS_VendorBEList = new List<MAS_VendorBE>();
            try
            {
                MAS_VendorDAL oMAS_VendorDAL = new MAS_VendorDAL();
                oMAS_VendorBEList = oMAS_VendorDAL.GetVendorDAL(oMAS_VendorBE);
                oMAS_VendorBE = oMAS_VendorBEList[0];
                oMAS_VendorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_VendorBE;
        }


        public int? UpdateOverstockReturnsDetailsBAL(MAS_VendorBE oMAS_VendorBE)
        {
            int? intResult = 0;
            try
            {
                MAS_VendorDAL oMAS_VendorDAL = new MAS_VendorDAL();
                intResult = oMAS_VendorDAL.UpdateOverstockReturnsDetailsDAL(oMAS_VendorBE);
                oMAS_VendorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MAS_VendorBE> GetOverstockReturnsDetailsBAL(MAS_VendorBE oMAS_VendorBE)
        {
            List<MAS_VendorBE> oMAS_VendorBEList = new List<MAS_VendorBE>();
            try
            {
                MAS_VendorDAL oMAS_VendorDAL = new MAS_VendorDAL();
                oMAS_VendorBEList = oMAS_VendorDAL.GetOverstockReturnsDetailsDAL(oMAS_VendorBE);
                oMAS_VendorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_VendorBEList;
        }

        public List<MAS_VendorBE> GetSiteSchedulingDataBAL(MAS_VendorBE oMAS_VendorBE)
        {
            List<MAS_VendorBE> oMAS_VendorBEList = new List<MAS_VendorBE>();
            try
            {
                MAS_VendorDAL oMAS_VendorDAL = new MAS_VendorDAL();
                oMAS_VendorBEList = oMAS_VendorDAL.GetSiteSchedulingDataDAL(oMAS_VendorBE);
                oMAS_VendorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_VendorBEList;
        }

        public int? UpdateSiteSchedulingSettingsBAL(MAS_VendorBE oMAS_VendorBE)
        {
            int? intResult = 0;
            try
            {
                MAS_VendorDAL oMAS_VendorDAL = new MAS_VendorDAL();
                intResult = oMAS_VendorDAL.UpdateSiteSchedulingSettingsDAL(oMAS_VendorBE);
                oMAS_VendorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


        public List<MAS_VendorBE> GetOverstockReturnsAgreementOverviewBAL(MAS_VendorBE oMAS_VendorBE)
        {
            List<MAS_VendorBE> oMAS_VendorBEList = new List<MAS_VendorBE>();
            try
            {
                MAS_VendorDAL oMAS_VendorDAL = new MAS_VendorDAL();
                oMAS_VendorBEList = oMAS_VendorDAL.GetOverstockReturnsAgreementOverviewDAL(oMAS_VendorBE);
                oMAS_VendorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_VendorBEList;
        }

        public DataSet GetSiteSchedulingSettingsForHazardousBAL(MAS_VendorBE oMAS_VendorBE)
        {
            try
            {
                MAS_VendorDAL oMAS_VendorDAL = new MAS_VendorDAL();
                DataSet dataSet = oMAS_VendorDAL.GetSiteSchedulingSettingsForHazardousDAL(oMAS_VendorBE);
                return dataSet;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                return null;
            }
            finally { }
        }
    }
}

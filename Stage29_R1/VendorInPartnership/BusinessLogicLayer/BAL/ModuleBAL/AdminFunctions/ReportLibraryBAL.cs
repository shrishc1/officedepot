﻿using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
    public class ReportLibraryBAL : BusinessLogicLayer.BaseBAL
    {
        public List<ReportLibraryBE> GetReportLibraryBAL(ReportLibraryBE oReportLibraryBE)
        {
            var lstReportLibrary = new List<ReportLibraryBE>();
            try
            {
                var reportLibraryDAL = new ReportLibraryDAL();
                lstReportLibrary = reportLibraryDAL.GetReportLibraryDAL(oReportLibraryBE);
                reportLibraryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstReportLibrary;
        }
    }
}

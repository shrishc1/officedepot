﻿using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
    public class SYS_SlotTimeBAL
    {
        public List<SYS_SlotTimeBE> GetSlotTimeBAL(SYS_SlotTimeBE oSYS_SlotTimeBE)
        {
            List<SYS_SlotTimeBE> oSYS_SlotTimeBEList = new List<SYS_SlotTimeBE>();

            try
            {
                SYS_SlotTimeDAL oSYS_SlotTimeDAL = new SYS_SlotTimeDAL();

                oSYS_SlotTimeBEList = oSYS_SlotTimeDAL.GetSlotTimeDAL(oSYS_SlotTimeBE);
                oSYS_SlotTimeDAL = null;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSYS_SlotTimeBEList;
        }

        public List<SYS_SlotTimeBE> GetTimeBAL()
        {
            List<SYS_SlotTimeBE> oSYS_SlotTimeBEList = new List<SYS_SlotTimeBE>();
            try
            {
                SYS_SlotTimeDAL oSYS_SlotTimeDAL = new SYS_SlotTimeDAL();

                oSYS_SlotTimeBEList = oSYS_SlotTimeDAL.GetTimeDAL();
                oSYS_SlotTimeDAL = null;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSYS_SlotTimeBEList;
        }
    }
}

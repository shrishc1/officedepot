﻿using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
    public class MAS_CountryBAL : BusinessLogicLayer.BaseBAL
    {

        public List<MAS_CountryBE> GetCountryBAL(MAS_CountryBE oMAS_CountryBE)
        {
            List<MAS_CountryBE> oMAS_CountryBEList = new List<MAS_CountryBE>();

            try
            {
                MAS_CountryDAL oMAS_CountryDAL = new MAS_CountryDAL();
                oMAS_CountryBEList = oMAS_CountryDAL.GetCountryDAL(oMAS_CountryBE);
                oMAS_CountryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_CountryBEList;
        }

        public List<MAS_CountryBE> GetCountryISPM15BAL(MAS_CountryBE oMAS_CountryBE)
        {
            List<MAS_CountryBE> oMAS_CountryBEList = new List<MAS_CountryBE>();
            try
            {
                MAS_CountryDAL oMAS_CountryDAL = new MAS_CountryDAL();
                oMAS_CountryBEList = oMAS_CountryDAL.GetCountryISPM15DAL(oMAS_CountryBE);
                oMAS_CountryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_CountryBEList;
        }
        public int InsertCountryISPM15BAL(MAS_CountryBE oMAS_CountryBE)
        {
            int intResult = 0;
            try
            {
                MAS_CountryDAL oMAS_CountryDAL = new MAS_CountryDAL();
                intResult = oMAS_CountryDAL.InsertCountryISPM15DAL(oMAS_CountryBE);
                oMAS_CountryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

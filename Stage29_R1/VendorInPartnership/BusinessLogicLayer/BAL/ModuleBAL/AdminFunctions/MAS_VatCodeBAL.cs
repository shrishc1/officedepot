﻿using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
    public class MAS_VatCodeBAL
    {
        public List<MAS_VatCodeBE> GetODVatCodeBAL(MAS_VatCodeBE oMAS_VatCodeBE)
        {
            List<MAS_VatCodeBE> lstMAS_VatCodeBE = new List<MAS_VatCodeBE>();

            try
            {
                MAS_VatCodeDAL oMAS_VatCodeDAL = new MAS_VatCodeDAL();
                lstMAS_VatCodeBE = oMAS_VatCodeDAL.GetODVatCodeDAL(oMAS_VatCodeBE);
                oMAS_VatCodeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstMAS_VatCodeBE;
        }

        public string AddODVatCodeDAL(MAS_VatCodeBE oMAS_VatCodeBE)
        {
            string strResult = string.Empty;
            try
            {
                MAS_VatCodeDAL oMAS_VatCodeDAL = new MAS_VatCodeDAL();
                strResult = oMAS_VatCodeDAL.AddODVatCodeDAL(oMAS_VatCodeBE);
                oMAS_VatCodeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }

        public int? EditDeleteODVatCodeBAL(MAS_VatCodeBE oMAS_VatCodeBE)
        {
            int? intResult = 0;
            try
            {
                MAS_VatCodeDAL oMAS_VatCodeDAL = new MAS_VatCodeDAL();
                intResult = oMAS_VatCodeDAL.EditDeleteODVatCodeDAL(oMAS_VatCodeBE);
                oMAS_VatCodeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// GET Vendor Vat Code Listing
        /// </summary>
        /// <param name="oMAS_VatCodeBE"></param>
        /// <returns></returns>
        public List<MAS_VatCodeBE> GetVendorVatCodeBAL(MAS_VatCodeBE oMAS_VatCodeBE)
        {
            List<MAS_VatCodeBE> lstMAS_VatCodeBE = new List<MAS_VatCodeBE>();

            try
            {
                MAS_VatCodeDAL oMAS_VatCodeDAL = new MAS_VatCodeDAL();
                lstMAS_VatCodeBE = oMAS_VatCodeDAL.GetVendorVatCodeDAL(oMAS_VatCodeBE);
                oMAS_VatCodeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstMAS_VatCodeBE;
        }

        /// <summary>
        /// Edit / Delete Vendor Vat Code
        /// </summary>
        /// <param name="oMAS_VatCodeBE"></param>
        /// <returns></returns>
        public int? EditDeleteVendorVatCodeBAL(MAS_VatCodeBE oMAS_VatCodeBE)
        {
            int? intResult = 0;
            try
            {
                MAS_VatCodeDAL oMAS_VatCodeDAL = new MAS_VatCodeDAL();
                intResult = oMAS_VatCodeDAL.EditDeleteVendorVatCodeDAL(oMAS_VatCodeBE);
                oMAS_VatCodeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public MAS_VatCodeBE GetVendorVatCodeByVendorIdBAL(MAS_VatCodeBE MAS_VatCodeBE)
        {
            var mas_VatCodeBE = new MAS_VatCodeBE();
            try
            {
                MAS_VatCodeDAL oMAS_VatCodeDAL = new MAS_VatCodeDAL();
                mas_VatCodeBE = oMAS_VatCodeDAL.GetVendorVatCodeByVendorIdDAL(MAS_VatCodeBE);
                oMAS_VatCodeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return mas_VatCodeBE;
        }

    }
}

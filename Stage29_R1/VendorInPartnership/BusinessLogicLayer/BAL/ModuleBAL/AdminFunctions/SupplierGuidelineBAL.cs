﻿using System;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;
using Utilities;
using System.Data;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
    public class SupplierGuidelineBAL
    {
        public DataTable GetSupplierGuideline(SupplierGuidelineBE oSupplierGuidelineBE)
        {
            DataTable dt = new DataTable();
            try
            {
                SupplierGuidelineDAL oGetSupplierGuideline = new SupplierGuidelineDAL();
                dt = oGetSupplierGuideline.GetSupplierGuideline(oSupplierGuidelineBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveLogEntry(ex.Message + " |      Error Source:- " + ex.Source);
            }
            finally { }
            return dt;
        }
    }
}

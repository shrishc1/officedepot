﻿using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
    public class CommunicationLibraryBAL : BusinessLogicLayer.BaseBAL
    {
        public int? AddCommunicationLibBAL(CommunicationLibraryBE communicationLib)
        {
            int? intResult = 0;
            try
            {
                var communicationLibraryDAL = new CommunicationLibraryDAL();
                intResult = communicationLibraryDAL.AddCommunicationLibDAL(communicationLib);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<CommunicationLibraryBE> GetAllCommunicationLibBAL(CommunicationLibraryBE communicationLib)
        {
            var lstCommunicationLibraryBE = new List<CommunicationLibraryBE>();
            try
            {
                var communicationLibraryDAL = new CommunicationLibraryDAL();
                lstCommunicationLibraryBE = communicationLibraryDAL.GetAllCommunicationLibDAL(communicationLib);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstCommunicationLibraryBE;
        }

        public List<CommunicationLibraryBE> GetEmailCommunicationBAL(CommunicationLibraryBE communicationLib)
        {
            var lstCommunicationLibraryBE = new List<CommunicationLibraryBE>();
            try
            {
                var communicationLibraryDAL = new CommunicationLibraryDAL();
                lstCommunicationLibraryBE = communicationLibraryDAL.GetEmailCommunicationDAL(communicationLib);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstCommunicationLibraryBE;
        }

        public DataSet GetStockPlanarDiscrepancyTrackerBAL(DiscrepancyNotification discrepancyNotification)
        {
            DataSet ds = new DataSet();
            try
            {
                var communicationLibraryDAL = new CommunicationLibraryDAL();
                ds = communicationLibraryDAL.GetStockPlanarDiscrepancyTrackerDAL(discrepancyNotification);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }


        public DataSet GetStockPlanarDiscrepancyTrackerByTrackerIdBAL(DiscrepancyNotification discrepancyNotification)
        {
            DataSet ds = new DataSet();
            try
            {
                var communicationLibraryDAL = new CommunicationLibraryDAL();
                ds = communicationLibraryDAL.GetStockPlanarDiscrepancyTrackerByTrackerIdDAL(discrepancyNotification);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public DataSet UpdateStockPlanarDiscrepancyTrackerByTrackerIdBAL(DiscrepancyNotification discrepancyNotification)
        {
            DataSet ds = new DataSet();
            try
            {
                var communicationLibraryDAL = new CommunicationLibraryDAL();
                ds = communicationLibraryDAL.UpdateStockPlanarDiscrepancyTrackerByTrackerIdDAL(discrepancyNotification);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }
        public string AddDiscrepancyTrackerEmailBAL(DiscrepancyEmailTracker discrepancyEmailTracker)
        {
            string intResult = string.Empty;
            try
            {
                var communicationLibraryDAL = new CommunicationLibraryDAL();
                intResult = communicationLibraryDAL.AddDiscrepancyTrackerEmailDAL(discrepancyEmailTracker);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyEmailTracker> GetALLDiscrepancyTrackerEmaiBAL(DiscrepancyEmailTracker discrepancyEmailTracker)
        {
            var lstCommunicationLibraryBE = new List<DiscrepancyEmailTracker>();
            try
            {
                var communicationLibraryDAL = new CommunicationLibraryDAL();
                lstCommunicationLibraryBE = communicationLibraryDAL.GetALLDiscrepancyTrackerEmailDAL(discrepancyEmailTracker);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstCommunicationLibraryBE;
        }
    }
}

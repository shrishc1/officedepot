﻿using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
    public class CMN_CommunicationErrorLogBAL
    {
        public List<CMN_CommunicationErrorLogBE> GetCommunicationErrorLog(CMN_CommunicationErrorLogBE oCMN_CommunicationErrorLogBE)
        {
            List<CMN_CommunicationErrorLogBE> oMAS_CountryBEList = new List<CMN_CommunicationErrorLogBE>();

            try
            {
                CMN_CommunicationErrorLogDAL oCMN_CommunicationErrorLogDAL = new CMN_CommunicationErrorLogDAL();
                oMAS_CountryBEList = oCMN_CommunicationErrorLogDAL.GetCommunicationErrorLog(oCMN_CommunicationErrorLogBE);
                oCMN_CommunicationErrorLogDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_CountryBEList;
        }

        public void UpdateStatus(CMN_CommunicationErrorLogBE oCMN_CommunicationErrorLogBE)
        {
            try
            {
                CMN_CommunicationErrorLogDAL oCMN_CommunicationErrorLogDAL = new CMN_CommunicationErrorLogDAL();
                oCMN_CommunicationErrorLogDAL.UpdateStatus(oCMN_CommunicationErrorLogBE);
                oCMN_CommunicationErrorLogDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
   public  class MAS_RegionBAL
    {

       public List<MAS_RegionBE> GetRegionsBAL(MAS_RegionBE oMas_RegionBE)
       {
           List<MAS_RegionBE> oUP_RegionBEList = new List<MAS_RegionBE>();

           try
           {
               MAS_RegionDAL oMAS_RegionDAL = new MAS_RegionDAL();

               oUP_RegionBEList = oMAS_RegionDAL.GetRegionsDAL(oMas_RegionBE);
           }
           catch (Exception ex)
           {
               LogUtility.SaveLogEntry(ex.Message + " |      Error Source:- " + ex.Source);
           }
           finally { }
           return oUP_RegionBEList;
       }


       public int? addEditRegionBAL(MAS_RegionBE oMas_RegionBE)
       {
           int? intResult = 0;
           try
           {
               MAS_RegionDAL oMAS_RegionDAL = new MAS_RegionDAL();
               intResult = oMAS_RegionDAL.addEditRegionDAL(oMas_RegionBE);
           }
           catch (Exception ex)
           {
               LogUtility.SaveLogEntry(ex.Message + " |      Error Source:- " + ex.Source);
           }
           finally { }
           return intResult;
       }

    }
}

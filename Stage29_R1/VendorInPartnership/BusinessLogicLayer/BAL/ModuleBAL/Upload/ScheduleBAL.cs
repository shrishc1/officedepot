﻿using BusinessEntities.ModuleBE.Upload;
using DataAccessLayer.ModuleDAL.Upload;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Upload
{
    public class ScheduleBAL
    {
        public void addSchedulingInfoBAL(ScheduleBE scheduleBE)
        {
            try
            {
                ScheduleDAL oUP_ScheduleDAL = new ScheduleDAL();
                oUP_ScheduleDAL.addSchedulingInfoDAL(scheduleBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public int? isAlreadySchedulingInfoBAL(ScheduleBE scheduleBE)
        {
            int? intResult = 0;
            try
            {
                ScheduleDAL oUP_ScheduleDAL = new ScheduleDAL();
                intResult = oUP_ScheduleDAL.isAlreadySchedulingInfoDAL(scheduleBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<ScheduleBE> GetUpcomingJobBAL(ScheduleBE scheduleBE)
        {
            List<ScheduleBE> lstScheduleBE = new List<ScheduleBE>();
            try
            {
                ScheduleDAL oReportRequestDAL = new ScheduleDAL();

                lstScheduleBE = oReportRequestDAL.GetUpcomingJobDAL(scheduleBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstScheduleBE;
        }

        public List<ScheduleBE> GetJobHistoryBAL(ScheduleBE scheduleBE)
        {
            List<ScheduleBE> lstScheduleBE = new List<ScheduleBE>();
            try
            {
                ScheduleDAL oReportRequestDAL = new ScheduleDAL();

                lstScheduleBE = oReportRequestDAL.GetJobHistoryDAL(scheduleBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstScheduleBE;
        }


        public int? EditSchedulingInfoBAL(ScheduleBE scheduleBE)
        {
            int? Result = null;
            try
            {
                ScheduleDAL oUP_ScheduleDAL = new ScheduleDAL();
                Result = oUP_ScheduleDAL.EditSchedulingInfoDAL(scheduleBE);
                oUP_ScheduleDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public int? DeleteSchedulingInfoBAL(ScheduleBE scheduleBE)
        {
            int? Result = null;
            try
            {
                ScheduleDAL oUP_ScheduleDAL = new ScheduleDAL();
                Result = oUP_ScheduleDAL.DeleteSchedulingInfoDAL(scheduleBE);
                oUP_ScheduleDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetProcessesingDataOTIFBAL()
        {
            DataSet ds = null;
            try
            {
                ScheduleDAL oUP_ScheduleDAL = new ScheduleDAL();
                ds = oUP_ScheduleDAL.GetProcessesingDataOTIFDAL();
                oUP_ScheduleDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }
    }
}

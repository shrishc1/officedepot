﻿using BusinessEntities.ModuleBE.Upload;
using DataAccessLayer.ModuleDAL.Upload;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Upload
{
    public class UP_ReceiptInformationBAL
    {

        public void AddReceiptInformation(List<UP_ReceiptInformationBE> pUP_ReceiptInformationBEList)
        {
            try
            {
                UP_ReceiptInformationDAL oUP_ReceiptInformationDAL = new UP_ReceiptInformationDAL();
                oUP_ReceiptInformationDAL.AddReceiptInformation(pUP_ReceiptInformationBEList);
                pUP_ReceiptInformationBEList = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        /// <summary>
        ///  Logs Errors Related to ReceiptInformation Files in ReceiptInformation Error Table
        /// </summary>
        /// <param name="up_ReceiptInformationBEErrorList">void</param>
        public void AddReceiptInformationErrors(List<UP_DataImportErrorBE> up_ReceiptInformationBEErrorList)
        {
            try
            {
                UP_ReceiptInformationDAL oUP_ReceiptInformationDAL = new UP_ReceiptInformationDAL();
                oUP_ReceiptInformationDAL.AddReceiptInformationErrors(up_ReceiptInformationBEErrorList);
                up_ReceiptInformationBEErrorList = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

    }
}

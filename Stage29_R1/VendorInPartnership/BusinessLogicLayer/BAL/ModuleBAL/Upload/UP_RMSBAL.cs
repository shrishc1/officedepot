﻿using BusinessEntities.ModuleBE.Upload;
using DataAccessLayer.ModuleDAL.Upload;
using System;
using System.Collections.Generic;
using Utilities;


namespace BusinessLogicLayer.ModuleBAL.Upload
{
    public class UP_RMSBAL : BaseBAL
    {

        public void AddRMSs(List<UP_RMSBE> pUP_RMSBEList)
        {
            try
            {
                UP_RMSDAL oUP_RMSDAL = new UP_RMSDAL();
                oUP_RMSDAL.AddRMSs(pUP_RMSBEList);
                pUP_RMSBEList = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        /// <summary>
        ///  Logs Errors Related to SKU Files in SKU Error Table
        /// </summary>
        /// <param name="UP_RMSBEErrorList">void</param>
        public void AddRMSErrors(List<UP_DataImportErrorBE> UP_RMSBEErrorList)
        {
            try
            {
                UP_RMSDAL oUP_RMSDAL = new UP_RMSDAL();
                oUP_RMSDAL.AddRMSErrors(UP_RMSBEErrorList);
                UP_RMSBEErrorList = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }
    }
}

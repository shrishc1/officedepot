﻿using BusinessEntities.ModuleBE.Upload;
using DataAccessLayer.ModuleDAL.Upload;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Upload
{
    public class MAS_RMSCategoryBAL
    {

        public List<MAS_RMSCategoryBE> GetRMSCategoryBAL(MAS_RMSCategoryBE oMAS_RMSCategoryBE)
        {
            List<MAS_RMSCategoryBE> lstMAS_RMSCategoryBE = new List<MAS_RMSCategoryBE>();
            try
            {
                MAS_RMSCategoryDAL oMAS_RMSCategoryDAL = new MAS_RMSCategoryDAL();
                lstMAS_RMSCategoryBE = oMAS_RMSCategoryDAL.GetRMSCategoryDAL(oMAS_RMSCategoryBE);
                oMAS_RMSCategoryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }

            return lstMAS_RMSCategoryBE;
        }


        public int? AddRMSCategoryBAL(MAS_RMSCategoryBE oMAS_RMSCategoryBE)
        {
            int? inserted = 0;
            try
            {
                MAS_RMSCategoryDAL oMAS_RMSCategoryDAL = new MAS_RMSCategoryDAL();
                inserted = oMAS_RMSCategoryDAL.AddRMSCategoryDAL(oMAS_RMSCategoryBE);
                oMAS_RMSCategoryDAL = null;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return inserted;


        }


        public int? UpdateRMSCategoryBAL(MAS_RMSCategoryBE oMAS_RMSCategoryBE)
        {
            int? updated = 0;
            try
            {
                MAS_RMSCategoryDAL oMAS_RMSCategoryDAL = new MAS_RMSCategoryDAL();
                updated = oMAS_RMSCategoryDAL.UpdateRMSCategoryDAL(oMAS_RMSCategoryBE);
                oMAS_RMSCategoryDAL = null;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return updated;
        }

        public string GetCategoryByIdBAL(MAS_RMSCategoryBE oMAS_RMSCategoryBE)
        {
            string result = string.Empty;
            try
            {
                MAS_RMSCategoryDAL oMAS_RMSCategoryDAL = new MAS_RMSCategoryDAL();
                result = oMAS_RMSCategoryDAL.GetCategoryByIdDAL(oMAS_RMSCategoryBE);
                oMAS_RMSCategoryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }

            return result;
        }
        public List<MAS_RMSCategoryBE> GetRMSAllCategoryBAL(MAS_RMSCategoryBE oMAS_RMSCategoryBE)
        {
            List<MAS_RMSCategoryBE> lstMAS_RMSCategoryBE = new List<MAS_RMSCategoryBE>();
            try
            {
                MAS_RMSCategoryDAL oMAS_RMSCategoryDAL = new MAS_RMSCategoryDAL();
                lstMAS_RMSCategoryBE = oMAS_RMSCategoryDAL.GetRMSAllCategoryBAL(oMAS_RMSCategoryBE);
                oMAS_RMSCategoryDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }

            return lstMAS_RMSCategoryBE;
        }

        public void GetRMSCategoryDepartmentBAL(MAS_RMSCategoryBE oMAS_RMSCategoryDepartmentBE, out List<UP_RMSBE> lstMAS_RMSCategoryDepartmentBE, out int RecordCount)
        {
            lstMAS_RMSCategoryDepartmentBE = new List<UP_RMSBE>();
            try
            {
                MAS_RMSCategoryDAL oMAS_RMSCategoryDepartmentDAL = new MAS_RMSCategoryDAL();
                oMAS_RMSCategoryDepartmentDAL.GetRMSCategoryDepartmentDAL(oMAS_RMSCategoryDepartmentBE, out lstMAS_RMSCategoryDepartmentBE, out RecordCount);
                oMAS_RMSCategoryDepartmentDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                lstMAS_RMSCategoryDepartmentBE = null;
                RecordCount = 0;
            }

        }




    }
}

﻿using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Upload;
using DataAccessLayer.ModuleDAL.Upload;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;
namespace BusinessLogicLayer.ModuleBAL.Upload
{
    public class UP_PurchaseOrderDetailBAL
    {
        public int? addEditUP_PurchaseOrderDetailBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                intResult = oRevisedLeadTimeDAL.addEditRevisedLeadTimeDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<Up_PurchaseOrderDetailBE> GetODProductCodesBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetODProductCodesDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetSKUCodesBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetSKUCodesDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetRevisedLeadListBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetRevisedLeadListDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetRevisedSKULeadListBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetRevisedSKULeadListDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetProductOrderDetailsBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetProductOrderDetailsDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetAllPODetailsBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetAllPODetailsDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetOpenPODetailsBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetOpenPODetailsDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetRemainingBookingPODetails(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetRemainingBookingPODetails(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetAllBookedProductOrderDetailsBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetAllBookedProductOrderDetailsDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetAllPOList(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oUP_PurchaseOrderDetailDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oUP_PurchaseOrderDetailDAL.GetAllPOList(oUp_PurchaseOrderDetailBE);
                oUP_PurchaseOrderDetailDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetPriorityBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetPriorityDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public void AddPurchaseOrderDetails(List<Up_PurchaseOrderDetailBE> pUP_PurchaseOrderDetailBEList, bool IsSnooty = false)
        {
            try
            {
                UP_PurchaseOrderDetailDAL oUP_PurchaseOrderDetailDAL = new UP_PurchaseOrderDetailDAL();
                oUP_PurchaseOrderDetailDAL.AddPurchaseOrderDetails(pUP_PurchaseOrderDetailBEList, IsSnooty);
                pUP_PurchaseOrderDetailBEList = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public DataTable GetPoForSchedularBAL()
        {
            UP_PurchaseOrderDetailDAL oUP_PurchaseOrderDetailDAL = new UP_PurchaseOrderDetailDAL();
            DataTable dtPoForSchedular = new DataTable();
            try
            {
                dtPoForSchedular = oUP_PurchaseOrderDetailDAL.GetPoForSchedularDAL();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally
            {
                oUP_PurchaseOrderDetailDAL = null;
            }
            return dtPoForSchedular;
        }

        // Sprint 1 - Point 7 - Start
        public List<Up_PurchaseOrderDetailBE> GetAllPODetailsOfSiteAndVendor(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetAllPODetailsOfSiteAndVendor(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }
        // Sprint 1 - Point 7 - End

        public List<UP_VendorBE> GetConsolidateVendorsBAL(UP_VendorBE oUP_VendorBE)
        {
            List<UP_VendorBE> lstVendors = new List<UP_VendorBE>();
            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                lstVendors = oRevisedLeadTimeDAL.GetConsolidateVendorsDAL(oUP_VendorBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstVendors;
        }

        public List<Up_PurchaseOrderDetailBE> GetPurchaseOrderInquiryBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetPurchaseOrderInquiryDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        /// <summary>
        ///  Logs Errors Related to SKU Files in SKU Error Table
        /// </summary>
        /// <param name="up_SKUBEErrorList">void</param>
        public void AddPurchaseOrderDetailErrors(List<UP_DataImportErrorBE> up_PurchaseOrderBEErrorList)
        {
            try
            {
                UP_PurchaseOrderDetailDAL oUP_PurchaseOrderDetailDAL = new UP_PurchaseOrderDetailDAL();
                oUP_PurchaseOrderDetailDAL.AddPurchaseOrderDetailErrors(up_PurchaseOrderBEErrorList);
                up_PurchaseOrderBEErrorList = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public List<Up_PurchaseOrderDetailBE> GetPurchaseOrderInquiryMainBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetPurchaseOrderInquiryMainDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetPurchaseOrderInquiryDetailsBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetPurchaseOrderInquiryDetailsDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<APPBOK_BookingBE> GetPurchaseOrderInquiryBookingDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = new List<APPBOK_BookingBE>();
            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                lstAPPBOK_BookingBE = oRevisedLeadTimeDAL.GetPurchaseOrderInquiryBookingDetailsDAL(oAPPBOK_BookingBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }

        public List<DiscrepancyBE> GetPurchaseOrderInquiryDiscrepanciesBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> lstDiscrepancyBE = new List<DiscrepancyBE>();
            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                lstDiscrepancyBE = oRevisedLeadTimeDAL.GetPurchaseOrderInquiryDiscrepanciesDAL(oDiscrepancyBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDiscrepancyBE;
        }

        public int? GetPOInquiryReceiptVisibilityCountBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                var oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                intResult = oRevisedLeadTimeDAL.GetPOInquiryReceiptVisibilityCountDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<Up_PurchaseOrderDetailBE> GetPOInquiryReceiptDataBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetail = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                var purchaseOrderDetailDAL = new UP_PurchaseOrderDetailDAL();
                lstPurchaseOrderDetail = purchaseOrderDetailDAL.GetPOInquiryReceiptDataDAL(oUp_PurchaseOrderDetailBE);
                purchaseOrderDetailDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstPurchaseOrderDetail;
        }

        public List<Up_PurchaseOrderDetailBE> GetPurchaseOrderVendorBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetPurchaseOrderVendorDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<APPBOK_BookingBE> GetExpeditePOEnquiryBookingDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            var lstAPPBOK_BookingBE = new List<APPBOK_BookingBE>();
            try
            {
                var oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                lstAPPBOK_BookingBE = oRevisedLeadTimeDAL.GetExpeditePOEnquiryBookingDetailsDAL(oAPPBOK_BookingBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }

        public List<Up_PurchaseOrderDetailBE> GetExpediteOpenPOListingBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetExpediteOpenPOListingDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetDelPerPOSummaryReportBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                var oPurchaseOrderDetailDAL = new UP_PurchaseOrderDetailDAL();
                lstPurchaseOrderDetailBE = oPurchaseOrderDetailDAL.GetDelPerPOSummaryReportDAL(oUp_PurchaseOrderDetailBE);
                oPurchaseOrderDetailDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstPurchaseOrderDetailBE;
        }

        public List<Up_PurchaseOrderDetailBE> GetDelPerPODetailByReceiptReportBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                var oPurchaseOrderDetailDAL = new UP_PurchaseOrderDetailDAL();
                lstPurchaseOrderDetailBE = oPurchaseOrderDetailDAL.GetDelPerPODetailByReceiptReportDAL(oUp_PurchaseOrderDetailBE);
                oPurchaseOrderDetailDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstPurchaseOrderDetailBE;
        }

        public List<Up_PurchaseOrderDetailBE> GetDelPerPODetailByBookingReportBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                var oPurchaseOrderDetailDAL = new UP_PurchaseOrderDetailDAL();
                lstPurchaseOrderDetailBE = oPurchaseOrderDetailDAL.GetDelPerPODetailByBookingReportDAL(oUp_PurchaseOrderDetailBE);
                oPurchaseOrderDetailDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstPurchaseOrderDetailBE;
        }

        public List<Up_PurchaseOrderDetailBE> GetGetDelPerPOReceiptDataBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstPurchaseOrderDetail = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                var purchaseOrderDetailDAL = new UP_PurchaseOrderDetailDAL();
                lstPurchaseOrderDetail = purchaseOrderDetailDAL.GetGetDelPerPOReceiptDataDAL(oUp_PurchaseOrderDetailBE);
                purchaseOrderDetailDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstPurchaseOrderDetail;
        }

        public bool CheckIfOTIFHitBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            bool IsOTIFHIt = false;
            try
            {
                var oPurchaseOrderDetailDAL = new UP_PurchaseOrderDetailDAL();
                IsOTIFHIt = oPurchaseOrderDetailDAL.CheckIfOTIFHitDAL(oUp_PurchaseOrderDetailBE);
                oPurchaseOrderDetailDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return IsOTIFHIt;
        }

        public List<Up_PurchaseOrderDetailBE> GetOpenPOVendorDetailsBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetOpenPOVendorDetailsDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> SentPurchaseOrderReportDataBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                var oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.SentPurchaseOrderReportDataDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public List<Up_PurchaseOrderDetailBE> GetSentPOEmailDetailsBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                var oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetSentPOEmailDetailsDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public int? SaveImportNewSentPOEmailDataBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                intResult = oRevisedLeadTimeDAL.SaveImportNewSentPOEmailDataDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<Up_PurchaseOrderDetailBE> GetRushReportBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetRushReportDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }


        public int? UpdatePOReconciliationBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                UP_PurchaseOrderDetailDAL oUP_PurchaseOrderDetailDAL = new UP_PurchaseOrderDetailDAL();
                intResult = oUP_PurchaseOrderDetailDAL.UpdatePOReconciliationDAL(oUp_PurchaseOrderDetailBE);
                oUP_PurchaseOrderDetailDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataTable SentPurchaseOrderExportReportDataBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            DataTable dt = new DataTable();
            try
            {
                var oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                dt = oRevisedLeadTimeDAL.SentPurchaseOrderReportExportDataDAL(oUp_PurchaseOrderDetailBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public List<Up_PurchaseOrderDetailBE> GetUnknownVendorIDForUnexpectedCarrierBookingBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            List<Up_PurchaseOrderDetailBE> oUp_PurchaseOrderDetailBEList = new List<Up_PurchaseOrderDetailBE>();

            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();

                oUp_PurchaseOrderDetailBEList = oRevisedLeadTimeDAL.GetUnknownVendorIDForUnexpectedCarrierBookingDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUp_PurchaseOrderDetailBEList;
        }

        public DataTable GetOpenPOReportBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            UP_PurchaseOrderDetailDAL oUP_PurchaseOrderDetailDAL = new UP_PurchaseOrderDetailDAL();
            DataTable dtGetOpenPOReport = new DataTable();
            try
            {
                dtGetOpenPOReport = oUP_PurchaseOrderDetailDAL.GetOpenPOReportDAL(oUp_PurchaseOrderDetailBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally
            {
                oUP_PurchaseOrderDetailDAL = null;
            }
            return dtGetOpenPOReport;
        }

        public int? DeleteRevisedDueDatesBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                intResult = oRevisedLeadTimeDAL.DeleteRevisedDueDatesDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataSet GetDeletedOtifExclusionRevisedBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            UP_PurchaseOrderDetailDAL oUP_PurchaseOrderDetailDAL = new UP_PurchaseOrderDetailDAL();
            DataSet ds = new DataSet();
            try
            {
                ds = oUP_PurchaseOrderDetailDAL.GetDeletedOtifExclusionRevisedDAL(oUp_PurchaseOrderDetailBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally
            {
                oUP_PurchaseOrderDetailDAL = null;
            }
            return ds;
        }

        public int? SaveManualSentPOEmailDataBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                UP_PurchaseOrderDetailDAL oRevisedLeadTimeDAL = new UP_PurchaseOrderDetailDAL();
                intResult = oRevisedLeadTimeDAL.SaveManualSentPOEmailDataDAL(oUp_PurchaseOrderDetailBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

    }
}

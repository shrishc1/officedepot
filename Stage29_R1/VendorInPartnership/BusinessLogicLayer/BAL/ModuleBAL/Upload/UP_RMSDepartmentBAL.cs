﻿using BusinessEntities.ModuleBE.Upload;
using DataAccessLayer.ModuleDAL.Upload;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Upload
{
    public class UP_RMSDepartmentBAL
    {

        public List<UP_RMSDepartmentBE> GetRMSDepartmentBAL(UP_RMSDepartmentBE oUP_RMSDepartmentBE)
        {
            List<UP_RMSDepartmentBE> lstUP_RMSDepartmentBE = new List<UP_RMSDepartmentBE>();
            try
            {
                UP_RMSDepartmentDAL oUP_RMSDepartmentDAL = new UP_RMSDepartmentDAL();
                lstUP_RMSDepartmentBE = oUP_RMSDepartmentDAL.GetRMSDepartmentBAL(oUP_RMSDepartmentBE);
                oUP_RMSDepartmentDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }

            return lstUP_RMSDepartmentBE;
        }

        public int? UpdateRMSDepartmentBAL(UP_RMSDepartmentBE oUP_RMSDepartmentBE)
        {
            int? updated = 0;
            try
            {
                UP_RMSDepartmentDAL oUP_RMSDepartmentDAL = new UP_RMSDepartmentDAL();
                updated = oUP_RMSDepartmentDAL.UpdateRMSDepartmentBAL(oUP_RMSDepartmentBE);
                oUP_RMSDepartmentDAL = null;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return updated;
        }

        public List<UP_RMSDepartmentBE> GetRMSDepartmentByCategoryIDBAL(UP_RMSDepartmentBE oUP_RMSDepartmentBE)
        {
            List<UP_RMSDepartmentBE> lstUP_RMSDepartmentBE = new List<UP_RMSDepartmentBE>();
            try
            {
                UP_RMSDepartmentDAL oUP_RMSDepartmentDAL = new UP_RMSDepartmentDAL();
                lstUP_RMSDepartmentBE = oUP_RMSDepartmentDAL.GetRMSDepartmentByCategoryIDBAL(oUP_RMSDepartmentBE);
                oUP_RMSDepartmentDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }

            return lstUP_RMSDepartmentBE;
        }
    }
}

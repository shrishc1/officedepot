﻿using BusinessEntities.ModuleBE.Discrepancy;
using DataAccessLayer.ModuleDAL.Discrepancy;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Discrepancy
{
    public class MediatorBAL : BaseBAL
    {

        public List<MediatorBE> GetMediatorBAL(MediatorBE MediatorBE)
        {
            var lstMediatorBE = new List<MediatorBE>();
            try
            {
                var MediatorDAL = new MediatorDAL();
                lstMediatorBE = MediatorDAL.GetMediatorDAL(MediatorBE);
                MediatorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstMediatorBE;
        }

        public DataTable GetMediatorUserBAL(MediatorBE MediatorBE)
        {
            var lstMediatorBE = new DataTable();
            try
            {
                var MediatorDAL = new MediatorDAL();
                lstMediatorBE = MediatorDAL.GetMediatorUserDAL(MediatorBE);
                MediatorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstMediatorBE;
        }

        public int? addEditMediatorBAL(MediatorBE MediatorBE)
        {
            int? intResult = 0;
            try
            {
                MediatorDAL MediatorDAL = new MediatorDAL();
                intResult = MediatorDAL.addEditMediatorDAL(MediatorBE);
                MediatorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

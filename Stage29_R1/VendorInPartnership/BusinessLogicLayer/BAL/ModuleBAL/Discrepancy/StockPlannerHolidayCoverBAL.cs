﻿using BusinessEntities.ModuleBE.Discrepancy;
using DataAccessLayer.ModuleDAL.Discrepancy;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Discrepancy
{
    public class StockPlannerHolidayCoverBAL : BaseBAL
    {
        public List<StockPlannerHolidayCoverBE> GetStockPlannerHolidaysBAL(StockPlannerHolidayCoverBE StockPlannerHolidayCoverBE)
        {
            var lstStockPlannerHolidayCoverBE = new List<StockPlannerHolidayCoverBE>();
            try
            {
                var StockPlannerHolidayCoverDAL = new StockPlannerHolidayCoverDAL();
                lstStockPlannerHolidayCoverBE = StockPlannerHolidayCoverDAL.GetStockPlannerHolidaysDAL(StockPlannerHolidayCoverBE);
                StockPlannerHolidayCoverDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstStockPlannerHolidayCoverBE;
        }
        public int? addEditStockPlannerHolidayCoveBAL(StockPlannerHolidayCoverBE StockPlannerHolidayCoverBE)
        {
            int? intResult = 0;
            try
            {
                var StockPlannerHolidayCoverDAL = new StockPlannerHolidayCoverDAL();
                intResult = StockPlannerHolidayCoverDAL.addEditStockPlannerHolidayCoveDAL(StockPlannerHolidayCoverBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public StockPlannerHolidayCoverBE GetAllStockPlannerHolidaysBAL(StockPlannerHolidayCoverBE StockPlannerHolidayCoverBE)
        {
            var objStockPlannerHolidayCoverBE = new StockPlannerHolidayCoverBE();
            try
            {
                var StockPlannerHolidayCoverDAL = new StockPlannerHolidayCoverDAL();
                objStockPlannerHolidayCoverBE = StockPlannerHolidayCoverDAL.GetAllStockPlannerHolidaysDAL(StockPlannerHolidayCoverBE);
                StockPlannerHolidayCoverDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return objStockPlannerHolidayCoverBE;
        }

        public DataTable GetStockPlannerWithWithUserCountrysBAL(StockPlannerHolidayCoverBE StockPlannerHolidayCoverBE)
        {
            DataTable lstDisc = new DataTable();

            try
            {
                StockPlannerHolidayCoverDAL oSCT_StockPlannerOnHolidayDAL = new StockPlannerHolidayCoverDAL();

                lstDisc = oSCT_StockPlannerOnHolidayDAL.GetStockPlannerWithWithUserCountrysDAL(StockPlannerHolidayCoverBE);
                oSCT_StockPlannerOnHolidayDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDisc;
        }
    }
}

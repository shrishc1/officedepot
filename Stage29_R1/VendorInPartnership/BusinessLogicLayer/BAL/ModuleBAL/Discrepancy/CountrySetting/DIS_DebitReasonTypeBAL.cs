﻿using BusinessEntities.ModuleBE.Discrepancy.APAction;
using DataAccessLayer.ModuleDAL.Discrepancy.CountrySetting;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting
{
    public class DIS_DebitReasonTypeBAL : BaseBAL
    {
        public int CheckExistanceBAL(APActionBE apActionBE)
        {
            int checkExistance = 0;

            try
            {
                var debitReasonTypeDAL = new DIS_DebitReasonTypeDAL();
                checkExistance = debitReasonTypeDAL.CheckExistanceDAL(apActionBE);
                debitReasonTypeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return checkExistance;
        }

        public int? addEditReasonTypeBAL(APActionBE apActionBE)
        {
            int? intResult = 0;
            try
            {
                var debitReasonTypeDAL = new DIS_DebitReasonTypeDAL();
                intResult = debitReasonTypeDAL.addEditReasonTypeDAL(apActionBE);
                debitReasonTypeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APActionBE> GetReasonTypeBAL(APActionBE apActionBE)
        {
            var lstAPActionBE = new List<APActionBE>();
            try
            {
                var debitReasonTypeDAL = new DIS_DebitReasonTypeDAL();
                lstAPActionBE = debitReasonTypeDAL.GetReasonTypeDAL(apActionBE);
                debitReasonTypeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPActionBE;
        }
    }
}

﻿using BusinessEntities.ModuleBE.Discrepancy.Report;
using DataAccessLayer.ModuleDAL.Discrepancy.Report;
using System;
using System.Data;
using Utilities;


namespace BusinessLogicLayer.ModuleBAL.Discrepancy.Report
{
    public class DiscrepancyReportBAL
    {
        DiscrepancyReportBE oDiscrepancyReportBE = new DiscrepancyReportBE();

        public DataSet getDiscrepancyReportBAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.getDiscrepancyReportDAL(oDiscrepancyReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet getAssignedVendorsReportBAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.getAssignedVendorsReportDAL(oDiscrepancyReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetShuttleDiscrepancyReportBAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.GetShuttleDiscrepancyReportDAL(oDiscrepancyReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetDeletedDiscrepanciesReportBAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.GetDeletedDiscrepanciesReportDAL(oDiscrepancyReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }


        public DataSet GetVDRDiscrepanciesReportBAL(DiscrepancyReportBE oDiscrepancyReportBE, out int RecordCount)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.GetVDRDiscrepanciesReportDAL(oDiscrepancyReportBE, out RecordCount);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                RecordCount = 0;
            }
            finally { }
            return Result;
        }
    }
}

﻿using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using DataAccessLayer.ModuleDAL.Discrepancy.LogDiscrepancy;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;


namespace BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy
{
    public class DiscrepancyBAL
    {
        public List<DiscrepancyBE> CheckValidPurchaseOrderBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.CheckValidPurchaseOrderDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetPurchaseOrderDateListBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetPurchaseOrderDateListDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetStockPlannerListBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetStockPlannerListDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }



        public List<DiscrepancyBE> GetContactPurchaseOrderDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetContactPurchaseOrderDetailsDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }
        public List<DiscrepancyBE> CheckVendorChargeStatusBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.CheckVendorChargeStatusDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }
        public String addDiscrepancyLogBAL(DiscrepancyBE oDiscrepancyBE)
        {
            String sResult = "0";
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                sResult = oDiscrepancyDAL.addDiscrepancyLogDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }
        public String updateDiscrepancyLocationBAL(DiscrepancyBE oDiscrepancyBE)
        {
            String sResult = "0";
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                sResult = oDiscrepancyDAL.updateDiscrepancyLocationDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }
        public String GetEsclationTypeBAL(DiscrepancyBE oDiscrepancyBE)
        {
            String sResult = string.Empty;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                sResult = oDiscrepancyDAL.GetEsclationTypeDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }
        public String GetCarrierBAL(DiscrepancyBE oDiscrepancyBE)
        {
            String sResult = string.Empty;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                sResult = oDiscrepancyDAL.GetCarrierDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }
        public String GetVendorSubscriptionStatusBAL(DiscrepancyBE oDiscrepancyBE)
        {
            String sResult = string.Empty;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                sResult = oDiscrepancyDAL.GetVendorSubscriptionStatusDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }
        public int? addDiscrepancyItemBAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                intResult = oDiscrepancyDAL.addDiscrepancyItemDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataSet getLanguageIDforCommunicationBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.getLanguageIDforCommunicationDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public DataSet GetDiscrepancyDetailBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.GetDiscrepancyDetailDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public int? saveDiscrepancyMailBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.saveDiscrepancyMailDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public int? saveDiscrepancyLetterBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.saveDiscrepancyLetterDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public DataSet getDiscrepancyLetterBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.GetDiscrepancyLetterDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet getDiscrepancyCommunicationBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.GetDiscrepancyCommunicationDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet getDiscrepancyCommunicationItemBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.GetDiscrepancyCommunicationItemDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet getDiscrepancyImagesBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.GetDiscrepancyImagesDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public List<DiscrepancyBE> GetODProductDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetODProductDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetODProductDetailsBySiteVendorBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetODProductDetailsBySiteVendorDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyLogDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyLogDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public DataTable GetDiscrepancyLogDetailBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable DiscrepancyBEList = new DataTable();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyLogDetailDAL(oDiscrepancyBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return DiscrepancyBEList;
        }
        public List<DiscrepancyBE> GetDiscrepancyForReassignBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyForReassignDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }
        public List<DiscrepancyBE> GetDiscrepancyLocationReportBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyLocationReportDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }
        public int? saveReassignDiscrepancyBAL(DiscrepancyBE oDiscrepancyMailBE)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.saveReassignDiscrepancyDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public List<DiscrepancyBE> GetDiscrepancyLogSiteVendorDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyLogSiteVendorDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyWorkListForCurrentUserBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyWorkListForCurrentUserDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }


        public List<DiscrepancyBE> GetDiscrepancyItemDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyItemDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyItemsForReturnNote(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyItemsForReturnNote(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyTypeBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyTypeDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetStockPlannersBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetStockPlannersDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetAllPurchaseOrderDateListBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetAllPurchaseOrderDateListDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetStockPlannerListByUserIDBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetStockPlannerListByUserIDDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }


        public int? addWorkFlowHTMLsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? iResult = 0;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                iResult = oDiscrepancyDAL.addWorkFlowHTMLsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return iResult;
        }

        public List<DiscrepancyBE> GetWorkFlowHTMLsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetWorkFlowHTMLsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetUserDetailsForWorkFlowBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetUserDetailsForWorkFlowDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public DataTable GetActionRequiredDetailsForWorkFlowBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                dt = oDiscrepancyDAL.GetActionRequiredDetailsForWorkFlowDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }


        public DataTable GetDiscrepancyReturnNoteCountBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                dt = oDiscrepancyDAL.GetDiscrepancyReturnNoteCountDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataTable GetCheckForGIN003VisibilityBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                dt = oDiscrepancyDAL.GetCheckForGIN003VisibilityDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataTable GetAllDetailsOfWorkFlowBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                dt = oDiscrepancyDAL.GetCheckForGIN003VisibilityDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public int? UpdateDiscrepancyStatus(DiscrepancyBE oDiscrepancyBE)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.UpdateDiscrepancyStatus(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public int? UpdateVendorActionElapsedTimeBAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.UpdateVendorActionElapsedTimeDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataTable GetOpenWorkflowEntry(DiscrepancyBE oNewDiscrepancyBE)
        {
            DataTable lstDisc = new DataTable();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                lstDisc = oDiscrepancyDAL.GetOpenWorkflowEntry(oNewDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDisc;
        }

        public void errorLogBAL(DiscrepancyMailBE oNewDiscrepancyMailBE)
        {
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                oDiscrepancyDAL.errorLogDAL(oNewDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public DataSet GetDiscrepancyLogPrintLabelBAL(DiscrepancyBE oDiscrepancyBE2)
        {
            DataSet ds = new DataSet();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                ds = oDiscrepancyDAL.GetDiscrepancyLogPrintLabelDAL(oDiscrepancyBE2);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public DataSet GetDiscrepancyLogRePrintBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataSet ds = new DataSet();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                ds = oDiscrepancyDAL.GetDiscrepancyLogRePrintDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public int? AddDiscrepancyReturnNote(DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.AddDiscrepancyReturnNote(oDiscrepancyReturnNoteBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DiscrepancyReturnNoteBE GetDiscrepancyReturnNote(DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE)
        {
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                oDiscrepancyReturnNoteBE = oDiscrepancyDAL.GetDiscrepancyReturnNote(oDiscrepancyReturnNoteBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oDiscrepancyReturnNoteBE;
        }
        public DiscrepancyReturnNoteBE GetDiscrepancyReturnNoteByLogID(DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE)
        {
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                oDiscrepancyReturnNoteBE = oDiscrepancyDAL.GetDiscrepancyReturnNoteByLogID(oDiscrepancyReturnNoteBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oDiscrepancyReturnNoteBE;
        }
        public DiscrepancyBE GetDiscrepancyVendorContact(DiscrepancyBE oDiscrepancyBE)
        {
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                oDiscrepancyBE = oDiscrepancyDAL.GetDiscrepancyReturnNote(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oDiscrepancyBE;
        }
        public DataSet GetCommunicationDataToSendMailBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet ds = new DataSet();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                ds = oDiscrepancyDAL.GetCommunicationDataToSendMailDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public DataSet GetDiscrepancyDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataSet ds = new DataSet();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                ds = oDiscrepancyDAL.GetDiscrepancyDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public int? AddDiscrepancyLogCommentBAL(DiscrepancyBE oDiscrepancy)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.AddDiscrepancyLogCommentDAL(oDiscrepancy);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public List<DiscrepancyBE> GetDiscrepancyLogCommentsBAL(DiscrepancyBE oDiscrepancy)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyLogCommentsDAL(oDiscrepancy);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public int? addDebitRaiseBAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                var oDiscrepancyDAL = new DiscrepancyDAL();
                intResult = oDiscrepancyDAL.addDebitRaiseDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? addDebitRaiseItemBAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                var oDiscrepancyDAL = new DiscrepancyDAL();
                intResult = oDiscrepancyDAL.addDebitRaiseItemDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? addDebitRaiseCommunicationBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            int? intResult = 0;
            try
            {
                var oDiscrepancyDAL = new DiscrepancyDAL();
                intResult = oDiscrepancyDAL.addDebitRaiseCommunicationDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyBE> GetAllDebitRaiseBAL(DiscrepancyBE oDiscrepancyBE)
        {
            var lstDiscrepancyBE = new List<DiscrepancyBE>();
            try
            {
                var oDiscrepancyDAL = new DiscrepancyDAL();
                lstDiscrepancyBE = oDiscrepancyDAL.GetAllDebitRaiseDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDiscrepancyBE;
        }

        public int? CancelDebitBAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                var oDiscrepancyDAL = new DiscrepancyDAL();
                intResult = oDiscrepancyDAL.CancelDebitDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyMailBE> GetDebitCommunicationBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            var lstDiscrepancyMailBE = new List<DiscrepancyMailBE>();
            try
            {
                var oDiscrepancyDAL = new DiscrepancyDAL();
                lstDiscrepancyMailBE = oDiscrepancyDAL.GetDebitCommunicationDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDiscrepancyMailBE;
        }

        public List<DiscrepancyBE> GetDebitResentCommunicationBAL(DiscrepancyBE oDiscrepancyBE)
        {
            var lstDiscrepancyBE = new List<DiscrepancyBE>();
            try
            {
                var oDiscrepancyDAL = new DiscrepancyDAL();
                lstDiscrepancyBE = oDiscrepancyDAL.GetDebitResentCommunicationDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDiscrepancyBE;
        }

        public List<DiscrepancyBE> GetAllDebitRaiseDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            var lstDiscrepancyBE = new List<DiscrepancyBE>();
            try
            {
                var oDiscrepancyDAL = new DiscrepancyDAL();
                lstDiscrepancyBE = oDiscrepancyDAL.GetAllDebitRaiseDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDiscrepancyBE;
        }
        /* STAGE-11 POINT-16 CHECK USER ACCESS RIGHT FOR RAISE QUERY  */
        public int? getAccessRightRaiseQueryBAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                intResult = oDiscrepancyDAL.CheckUserQueryTemplate(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataSet CheckForGin0012DuplicateDisc(DiscrepancyBE oDiscrepancyBE)
        {
            DataSet ds = new DataSet();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                ds = oDiscrepancyDAL.CheckForGin0012DuplicateDisc(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public DataSet GetCheckDiscrepancyExistsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataSet dt = new DataSet();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                dt = oDiscrepancyDAL.GetCheckDiscrepancyExistsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataSet GetInvoiceDiscrepancyCommentsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataSet dt = new DataSet();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                dt = oDiscrepancyDAL.GetInvoiceDiscrepancyCommentsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public List<DiscrepancyBE> GetInvoiceQueryDiscrepancyLogDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetQueryDiscrepancyLogDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }
        public DataTable GetUserPhoneNumberBAL(DiscrepancyBE oDISLog_WorkFlowBE)
        {
            DataTable dt = new DataTable();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                dt = oDiscrepancyDAL.GetUserPhoneNumberDAL(oDISLog_WorkFlowBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public string GetDebitCreditStatusBAL(DiscrepancyBE oDiscrepancyBE)
        {
            string strResult = string.Empty;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                strResult = oDiscrepancyDAL.GetDebitCreditStatusDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }


        public DataSet GetIsodToReturnBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataSet dt = new DataSet();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                dt = oDiscrepancyDAL.GetIsodToReturnDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataSet GetItemDetailDiscrepancyReportBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataSet dt = new DataSet();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                dt = oDiscrepancyDAL.GetItemDetailDiscrepancyReportDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }


        public DataTable GetCommentsLoopBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable dt = new DataTable();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                dt = oDiscrepancyDAL.GetCommentsLoopDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }
        public string GetCurrencyBAL(DiscrepancyBE oDiscrepancyBE)
        {
            string strResult = string.Empty;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                strResult = oDiscrepancyDAL.GetCurrencyDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }

        public int? DeleteBlankWorkflowEntriesBAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                intResult = oDiscrepancyDAL.DeleteBlankWorkflowEntriesDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyBE> GetUnclosedDiscrepancyBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetUnclosedDiscrepancyDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public int? UpdateUnclosedDiscrepancyBAL(DiscrepancyBE oDiscrepancyMailBE)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.UpdateUnclosedDiscrepancyDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public int? UpdateVENHtmlBAL(DiscrepancyBE oNewDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                intResult = oDiscrepancyDAL.UpdateVENHtmlDAL(oNewDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyBE> GetCostCenterBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetCostCenterDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public int? UpdateWorkflowForQualityIssueDiscBAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? iResult = 0;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                iResult = oDiscrepancyDAL.UpdateWorkflowForQualityIssueDiscDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return iResult;
        }

        public DataTable GetIsVendorAcceptRefuseChargeBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataTable Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.GetIsVendorAcceptRefuseChargeDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }


        public String GetPurchaseOrderByPOIdBAL(DiscrepancyBE oDiscrepancyBE)
        {
            String sResult = string.Empty;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                sResult = oDiscrepancyDAL.GetPurchaseOrderByPOIdDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }

        public List<DiscrepancyBE> GetStockPlannerOnHolidayCoverBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetStockPlannerOnHolidayCoverDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetActualStockPlannerDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetActualStockPlannerDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetSPInitialCommDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetSPInitialCommDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

    }
}

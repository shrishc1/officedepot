﻿using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using DataAccessLayer.ModuleDAL.Discrepancy.LogDiscrepancy;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy
{
    public class VendorSubsToChargesBAL
    {

        public List<VendorSubsToChargesBE> GetvendorSubscribeBAL(VendorSubsToChargesBE oVendorSubsToChargesBE)
        {
            List<VendorSubsToChargesBE> VendorSubsToChargesBEList = new List<VendorSubsToChargesBE>();

            try
            {
                VendorSubsToChargesDAL oVendorSubsToChargesDAL = new VendorSubsToChargesDAL();

                VendorSubsToChargesBEList = oVendorSubsToChargesDAL.GetvendorSubscribeDAL(oVendorSubsToChargesBE);

                oVendorSubsToChargesDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return VendorSubsToChargesBEList;
        }

        public int? UpdatevendorSubscribeBAL(VendorSubsToChargesBE oVendorSubsToChargesBE)
        {

            int? restult = 0;
            try
            {
                VendorSubsToChargesDAL oVendorSubsToChargesDAL = new VendorSubsToChargesDAL();

                restult = oVendorSubsToChargesDAL.UpdatevendorSubscribeDAL(oVendorSubsToChargesBE);

                oVendorSubsToChargesDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return restult;
        }
    }
}

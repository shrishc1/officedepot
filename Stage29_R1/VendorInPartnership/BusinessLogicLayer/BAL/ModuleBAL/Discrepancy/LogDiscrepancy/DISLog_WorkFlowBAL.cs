﻿using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using DataAccessLayer.ModuleDAL.Discrepancy.LogDiscrepancy;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy
{
    public class DISLog_WorkFlowBAL : BaseBAL
    {
        public int? addWorkFlowActionsBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {
            int? intResult = 0;
            try
            {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();
                intResult = oDISLog_WorkFlowDAL.addWorkFlowActionsDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DISLog_WorkFlowBE> GetWorkFlowActionsBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {
            List<DISLog_WorkFlowBE> DISLog_WorkFlowBEList = new List<DISLog_WorkFlowBE>();

            try
            {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();

                DISLog_WorkFlowBEList = oDISLog_WorkFlowDAL.GetWorkFlowActionsDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DISLog_WorkFlowBEList;
        }

        public List<DISLog_WorkFlowBE> GetPOValueExpectedBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {
            List<DISLog_WorkFlowBE> DISLog_WorkFlowBEList = new List<DISLog_WorkFlowBE>();

            try
            {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();

                DISLog_WorkFlowBEList = oDISLog_WorkFlowDAL.GetPOValueExpectedDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DISLog_WorkFlowBEList;
        }

        public List<DISLog_WorkFlowBE> GetPOCarriageCostBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {
            List<DISLog_WorkFlowBE> DISLog_WorkFlowBEList = new List<DISLog_WorkFlowBE>();

            try
            {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();

                DISLog_WorkFlowBEList = oDISLog_WorkFlowDAL.GetPOCarriageCostDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DISLog_WorkFlowBEList;
        }

        public DataTable GetVendorCollectionDataBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {
            //List<DISLog_WorkFlowBE> DISLog_WorkFlowBEList = new List<DISLog_WorkFlowBE>();
            DataTable dt = new DataTable();
            try
            {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();

                dt = oDISLog_WorkFlowDAL.GetVendorCollectionDataDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataTable GetDataForSpecificActionBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {
            DataTable dt = new DataTable();
            try
            {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();

                dt = oDISLog_WorkFlowDAL.GetDataForSpecificActionDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public string GetVendorAccountPayablesEmail(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {
            string VendorAccountPayablesEmail = string.Empty;
            try
            {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();

                VendorAccountPayablesEmail = oDISLog_WorkFlowDAL.GetVendorAccountPayablesEmail(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return VendorAccountPayablesEmail;
        }

        public int? addWorkFlowInvoiceQueryActionsBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {
            int? intResult = 0;
            try
            {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();
                intResult = oDISLog_WorkFlowDAL.addWorkFlowInvoiceQueryActionsDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int GetGINValidDisputeCountBAL(DISLog_WorkFlowBE oDiscrepancyBE)
        {
            int iResult = 0;
            try
            {
                DISLog_WorkFlowDAL oDiscrepancyDAL = new DISLog_WorkFlowDAL();
                iResult = oDiscrepancyDAL.GetGINValidDisputeCountDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return iResult;
        }

        public DataTable GetDataForQualityIssueWorkflowUpdateBAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {
            DataTable dt = new DataTable();
            try
            {
                DISLog_WorkFlowDAL oDISLog_WorkFlowDAL = new DISLog_WorkFlowDAL();

                dt = oDISLog_WorkFlowDAL.GetDataForQualityIssueWorkflowUpdateDAL(oDISLog_WorkFlowBE);
                oDISLog_WorkFlowDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }


    }
}

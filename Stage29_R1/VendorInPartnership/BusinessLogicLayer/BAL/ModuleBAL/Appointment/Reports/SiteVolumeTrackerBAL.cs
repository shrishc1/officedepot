﻿using BusinessEntities.ModuleBE.Appointment.Reports;
using DataAccessLayer.ModuleDAL.Appointment.Reports;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.Reports
{
    public class SiteVolumeTrackerBAL : BaseBAL
    {
        public List<SiteVolumeTrackerBE> GetSiteVolumeReportDetailsBAL(SiteVolumeTrackerBE oSiteVolumeTrackerBE)
        {
            List<SiteVolumeTrackerBE> oSiteVolumeTrackerBEList = new List<SiteVolumeTrackerBE>();

            try
            {
                SiteVolumeTrackerDAL oSiteVolumeTrackerDAL = new SiteVolumeTrackerDAL();

                oSiteVolumeTrackerBEList = oSiteVolumeTrackerDAL.GetSiteVolumeReportDetailsDAL(oSiteVolumeTrackerBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSiteVolumeTrackerBEList;
        }
    }
}

﻿using BusinessEntities.ModuleBE.Appointment.Reports;
using DataAccessLayer.ModuleDAL.Appointment.Reports;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.Reports
{
    public class FixedSlotUsageReportBAL : BaseBAL
    {
        public List<FixedSlotUsageReportBE> GetFixedSlotUsageReportBAL(FixedSlotUsageReportBE oScheduledReceiptedReportBE)
        {
            List<FixedSlotUsageReportBE> oScheduledReceiptedReportBEList = new List<FixedSlotUsageReportBE>();
            try
            {
                FixedSlotUsageReportDAL oFixedSlotUsageReportDAL = new FixedSlotUsageReportDAL();
                oScheduledReceiptedReportBEList = oFixedSlotUsageReportDAL.GetFixedSlotUsageReportDAL(oScheduledReceiptedReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oScheduledReceiptedReportBEList;
        }

        public List<FixedSlotUsageReportBE> GetFixedSlotUsageDetailsBAL(FixedSlotUsageReportBE oScheduledReceiptedReportBE)
        {
            List<FixedSlotUsageReportBE> oScheduledReceiptedReportBEList = new List<FixedSlotUsageReportBE>();
            try
            {
                FixedSlotUsageReportDAL oFixedSlotUsageReportDAL = new FixedSlotUsageReportDAL();
                oScheduledReceiptedReportBEList = oFixedSlotUsageReportDAL.GetFixedSlotUsageDetailsDAL(oScheduledReceiptedReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oScheduledReceiptedReportBEList;
        }

        public List<FixedSlotUsageReportBE> GetFixedSlotUsageVendorBAL(FixedSlotUsageReportBE oScheduledReceiptedReportBE)
        {
            List<FixedSlotUsageReportBE> oScheduledReceiptedReportBEList = new List<FixedSlotUsageReportBE>();
            try
            {
                FixedSlotUsageReportDAL oFixedSlotUsageReportDAL = new FixedSlotUsageReportDAL();
                oScheduledReceiptedReportBEList = oFixedSlotUsageReportDAL.GetFixedSlotUsageVendorDAL(oScheduledReceiptedReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oScheduledReceiptedReportBEList;
        }
    }


}

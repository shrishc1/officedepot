﻿using BusinessEntities.ModuleBE.Appointment.Reports;
using DataAccessLayer.ModuleDAL.Appointment.Reports;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.Reports
{
    public class ScheduledReceiptedReportBAL : BaseBAL
    {
        public List<ScheduledReceiptedReportDetailsBE> ScheduledReceiptedReportDetailsBAL(ScheduledReceiptedReportDetailsBE oScheduledReceiptedReportBE)
        {
            List<ScheduledReceiptedReportDetailsBE> oScheduledReceiptedReportBEList = new List<ScheduledReceiptedReportDetailsBE>();
            try
            {
                ScheduledReceiptedReportDAL oScheduledReceiptedReportDAL = new ScheduledReceiptedReportDAL();
                oScheduledReceiptedReportBEList = oScheduledReceiptedReportDAL.ScheduledReceiptedReportDetailsDAL(oScheduledReceiptedReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oScheduledReceiptedReportBEList;
        }

        public void ScheduledReceiptedReportDetailsVendorBAL(ScheduledReceiptedReportDetailsBE oScheduledReceiptedReportDetailsBE, out List<ScheduledReceiptedReportDetailsBE> oScheduledReceiptedReportDetailsBEList, out int RecordCount)
        {
            List<ScheduledReceiptedReportDetailsBE> oScheduledReceiptedReportBEList = new List<ScheduledReceiptedReportDetailsBE>();
            try
            {
                ScheduledReceiptedReportDAL oScheduledReceiptedReportDAL = new ScheduledReceiptedReportDAL();
                oScheduledReceiptedReportDAL.ScheduledReceiptedReportDetailsVendorDAL(oScheduledReceiptedReportDetailsBE, out oScheduledReceiptedReportDetailsBEList, out RecordCount);
            }
            catch (Exception ex)
            {
                oScheduledReceiptedReportDetailsBEList = null;
                RecordCount = 0;
                LogUtility.SaveErrorLogEntry(ex);
            }
            // return oScheduledReceiptedReportBEList;
        }
        public List<ScheduledReceiptedReportDetailsBE> ScheduledReceiptedReportDetailsVendorBySiteBAL(ScheduledReceiptedReportDetailsBE oScheduledReceiptedReportBE)
        {
            List<ScheduledReceiptedReportDetailsBE> oScheduledReceiptedReportBEList = new List<ScheduledReceiptedReportDetailsBE>();
            try
            {
                ScheduledReceiptedReportDAL oScheduledReceiptedReportDAL = new ScheduledReceiptedReportDAL();
                oScheduledReceiptedReportBEList = oScheduledReceiptedReportDAL.ScheduledReceiptedReportDetailsVendorBySiteDAL(oScheduledReceiptedReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oScheduledReceiptedReportBEList;
        }
    }
}

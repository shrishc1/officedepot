﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings
{
    public class MASSIT_DoorNoSetupBAL : BusinessLogicLayer.BaseBAL
    {

        public List<MASSIT_DoorNoSetupBE> GetDoorNoSetupDetailsBAL(MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE)
        {
            List<MASSIT_DoorNoSetupBE> oMASSIT_DoorNoSetupBEList = new List<MASSIT_DoorNoSetupBE>();

            try
            {
                APPSIT_DoorNoSetupDAL oMASSIT_DoorNoSetupDAL = new APPSIT_DoorNoSetupDAL();

                oMASSIT_DoorNoSetupBEList = oMASSIT_DoorNoSetupDAL.GetDoorNoSetupDetailsDAL(oMASSIT_DoorNoSetupBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_DoorNoSetupBEList;
        }


        public MASSIT_DoorNoSetupBE GetDoorNoSetupDetailsByIdBAL(MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE)
        {
            MASSIT_DoorNoSetupBE oNewMASSIT_DoorNoSetupBE = new MASSIT_DoorNoSetupBE();
            List<MASSIT_DoorNoSetupBE> oMASSIT_DoorNoSetupBEList = new List<MASSIT_DoorNoSetupBE>();

            try
            {
                APPSIT_DoorNoSetupDAL oMASSIT_DoorNoSetupDAL = new APPSIT_DoorNoSetupDAL();

                oMASSIT_DoorNoSetupBEList = oMASSIT_DoorNoSetupDAL.GetDoorNoSetupDetailsDAL(oMASSIT_DoorNoSetupBE);

                oNewMASSIT_DoorNoSetupBE = oMASSIT_DoorNoSetupBEList[0];

            }

            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewMASSIT_DoorNoSetupBE;
        }

        public int? addEditDoorNoSetupDetailsBAL(MASSIT_DoorNoSetupBE oNewMASSIT_DoorNoSetupBE)
        {
            int? intResult = 0;
            try
            {
                APPSIT_DoorNoSetupDAL ooMASCNT_DoorNoSetupDAL = new APPSIT_DoorNoSetupDAL();
                intResult = ooMASCNT_DoorNoSetupDAL.addEditDoorNoSetupDetailsDAL(oNewMASSIT_DoorNoSetupBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;

        }

        public List<MASSIT_DoorNoSetupBE> GetDoorTypeDetailsBAL(MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE)
        {
            List<MASSIT_DoorNoSetupBE> oMASSIT_DoorNoSetupBEList = new List<MASSIT_DoorNoSetupBE>();

            try
            {
                APPSIT_DoorNoSetupDAL oMASSIT_DoorNoSetupDAL = new APPSIT_DoorNoSetupDAL();

                oMASSIT_DoorNoSetupBEList = oMASSIT_DoorNoSetupDAL.GetDoorNoSetupDetailsDAL(oMASSIT_DoorNoSetupBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_DoorNoSetupBEList;
        }
    }
}

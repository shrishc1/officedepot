﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings
{
    public class APPSIT_SpecificWeekSetupBAL : BaseBAL
    {

        public List<MASSIT_SpecificWeekSetupBE> GetSpecificWeekSetupDetailsBAL(MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE)
        {
            List<MASSIT_SpecificWeekSetupBE> oMASSIT_SpecificWeekSetupBEList = new List<MASSIT_SpecificWeekSetupBE>();

            try
            {
                APPSIT_SpecificWeekSetupDAL oMASSIT_SpecificWeekSetupDAL = new APPSIT_SpecificWeekSetupDAL();

                oMASSIT_SpecificWeekSetupBEList = oMASSIT_SpecificWeekSetupDAL.GetSpecificWeekSetupDetailsDAL(oMASSIT_SpecificWeekSetupBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_SpecificWeekSetupBEList;
        }

        public int? addEditSpecificWeekSetupBAL(MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE)
        {
            int? intResult = 0;
            try
            {
                APPSIT_SpecificWeekSetupDAL oMASSIT_SpecificWeekSetupDAL = new APPSIT_SpecificWeekSetupDAL();
                intResult = oMASSIT_SpecificWeekSetupDAL.addEditSpecificWeekSetupDAL(oMASSIT_SpecificWeekSetupBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? SaveWeekSetupLogsBAL(MASSIT_WeekSetupLogsBE oMASSIT_WeekSetupLogsBE)
        {
            int? intResult = 0;
            try
            {
                APPSIT_SpecificWeekSetupDAL oMASSIT_SpecificWeekSetupDAL = new APPSIT_SpecificWeekSetupDAL();
                intResult = oMASSIT_SpecificWeekSetupDAL.SaveWeekSetupLogsDAL(oMASSIT_WeekSetupLogsBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MASSIT_WeekSetupLogsBE> GetWeekSetupLogsBAL(MASSIT_WeekSetupLogsBE oMASSIT_WeekSetupLogsBE)
        {
            List<MASSIT_WeekSetupLogsBE> oMASSIT_WeekSetupLogsBEList = new List<MASSIT_WeekSetupLogsBE>();

            try
            {
                APPSIT_SpecificWeekSetupDAL oMASSIT_SpecificWeekSetupDAL = new APPSIT_SpecificWeekSetupDAL();

                oMASSIT_WeekSetupLogsBEList = oMASSIT_SpecificWeekSetupDAL.GetWeekSetupLogsDAL(oMASSIT_WeekSetupLogsBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_WeekSetupLogsBEList;
        }

        public List<MASSIT_SpecificWeekSetupBE> GetTotlasBAL(MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE)
        {
            List<MASSIT_SpecificWeekSetupBE> oMASSIT_SpecificWeekSetupBEList = new List<MASSIT_SpecificWeekSetupBE>();

            try
            {
                APPSIT_SpecificWeekSetupDAL oMASSIT_SpecificWeekSetupDAL = new APPSIT_SpecificWeekSetupDAL();

                oMASSIT_SpecificWeekSetupBEList = oMASSIT_SpecificWeekSetupDAL.GetTotalsDAL(oMASSIT_SpecificWeekSetupBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_SpecificWeekSetupBEList;
        }

    }
}

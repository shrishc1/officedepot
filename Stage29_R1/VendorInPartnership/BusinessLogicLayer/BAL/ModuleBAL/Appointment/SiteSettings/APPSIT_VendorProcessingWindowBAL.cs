﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings
{
    public class APPSIT_VendorProcessingWindowBAL
    {

        public List<MASSIT_VendorProcessingWindowBE> GetVendorDetailsBAL(MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE)
        {
            List<MASSIT_VendorProcessingWindowBE> oMASSIT_VendorProcessingWindowBEList = new List<MASSIT_VendorProcessingWindowBE>();

            try
            {
                APPSIT_VendorProcessingWindowDAL oMASSIT_VendorProcessingWindowDAL = new APPSIT_VendorProcessingWindowDAL();

                oMASSIT_VendorProcessingWindowBEList = oMASSIT_VendorProcessingWindowDAL.GetVendorDetailsDAL(oMASSIT_VendorProcessingWindowBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_VendorProcessingWindowBEList;
        }
        public DataTable GetVendorDetailsBAL(MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE, string Nothing = "")
        {
            DataTable dt = new DataTable();
            try
            {
                APPSIT_VendorProcessingWindowDAL oMASSIT_VendorProcessingWindowDAL = new APPSIT_VendorProcessingWindowDAL();

                dt = oMASSIT_VendorProcessingWindowDAL.GetVendorDetailsDAL(oMASSIT_VendorProcessingWindowBE, "");


            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public MASSIT_VendorProcessingWindowBE GetVendorDetailsByIdBAL(MASSIT_VendorProcessingWindowBE oMASSIT_VendorProcessingWindowBE)
        {
            MASSIT_VendorProcessingWindowBE oNewMASSIT_VendorProcessingWindowBE = new MASSIT_VendorProcessingWindowBE();
            List<MASSIT_VendorProcessingWindowBE> oMASSIT_VendorProcessingWindowBEList = new List<MASSIT_VendorProcessingWindowBE>();

            try
            {
                APPSIT_VendorProcessingWindowDAL oMASSIT_VendorProcessingWindowDAL = new APPSIT_VendorProcessingWindowDAL();

                oMASSIT_VendorProcessingWindowBEList = oMASSIT_VendorProcessingWindowDAL.GetVendorDetailsDAL(oMASSIT_VendorProcessingWindowBE);

                if (oMASSIT_VendorProcessingWindowBEList != null && oMASSIT_VendorProcessingWindowBEList.Count > 0)
                    oNewMASSIT_VendorProcessingWindowBE = oMASSIT_VendorProcessingWindowBEList[0];
                else
                    oNewMASSIT_VendorProcessingWindowBE = null;
            }

            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewMASSIT_VendorProcessingWindowBE;
        }
        public int? addEditVendorDetailsBAL(MASSIT_VendorProcessingWindowBE oNewMASSIT_VendorProcessingWindowBE)
        {
            int? intResult = 0;
            try
            {
                APPSIT_VendorProcessingWindowDAL oMASSIT_VendorProcessingWindowDAL = new APPSIT_VendorProcessingWindowDAL();
                intResult = oMASSIT_VendorProcessingWindowDAL.addEditVendorDetailsDAL(oNewMASSIT_VendorProcessingWindowBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }

            return intResult;
        }



    }
}

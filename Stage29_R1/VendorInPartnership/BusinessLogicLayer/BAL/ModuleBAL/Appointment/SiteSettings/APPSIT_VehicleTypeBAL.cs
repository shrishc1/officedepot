﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings
{
    public class APPSIT_VehicleTypeBAL
    {

        public List<MASSIT_VehicleTypeBE> GetVehicleTypeDetailsBAL(MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE)
        {
            List<MASSIT_VehicleTypeBE> oMASSIT_VehicleTypeBEList = new List<MASSIT_VehicleTypeBE>();

            try
            {
                APPSIT_VehicleTypeDAL oMASSIT_VehicleTypeDAL = new APPSIT_VehicleTypeDAL();

                oMASSIT_VehicleTypeBEList = oMASSIT_VehicleTypeDAL.GetVehicleTypeDetailsDAL(oMASSIT_VehicleTypeBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_VehicleTypeBEList;
        }
        public List<MASSIT_VehicleTypeBE> GetVehicleTypeBAL(MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE)
        {
            List<MASSIT_VehicleTypeBE> oMASSIT_VehicleTypeBEList = new List<MASSIT_VehicleTypeBE>();

            try
            {
                APPSIT_VehicleTypeDAL oMASSIT_VehicleTypeDAL = new APPSIT_VehicleTypeDAL();

                oMASSIT_VehicleTypeBEList = oMASSIT_VehicleTypeDAL.GetVehicleTypDAL(oMASSIT_VehicleTypeBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_VehicleTypeBEList;
        }

        public MASSIT_VehicleTypeBE GetVehicleTypeDetailsByIdBAL(MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE)
        {
            MASSIT_VehicleTypeBE oNewMASSIT_VehicleTypeBE = new MASSIT_VehicleTypeBE();
            List<MASSIT_VehicleTypeBE> oMASSIT_VehicleTypeBEList = new List<MASSIT_VehicleTypeBE>();
            try
            {
                APPSIT_VehicleTypeDAL oMASSIT_VehicleTypeDAL = new APPSIT_VehicleTypeDAL();

                oMASSIT_VehicleTypeBEList = oMASSIT_VehicleTypeDAL.GetVehicleTypeDetailsDAL(oMASSIT_VehicleTypeBE);
                if (oMASSIT_VehicleTypeBEList != null && oMASSIT_VehicleTypeBEList.Count > 0)
                    oNewMASSIT_VehicleTypeBE = oMASSIT_VehicleTypeBEList[0];
                else
                    oNewMASSIT_VehicleTypeBE = null;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewMASSIT_VehicleTypeBE;
        }

        public int? addEditVehicleTypeDetailsBAL(MASSIT_VehicleTypeBE oMASSIT_VehicleTypeBE)
        {
            int? intResult = 0;
            try
            {
                APPSIT_VehicleTypeDAL oMASSIT_VehicleTypeDAL = new APPSIT_VehicleTypeDAL();
                intResult = oMASSIT_VehicleTypeDAL.addEditVehicleTypeDetailsDAL(oMASSIT_VehicleTypeBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

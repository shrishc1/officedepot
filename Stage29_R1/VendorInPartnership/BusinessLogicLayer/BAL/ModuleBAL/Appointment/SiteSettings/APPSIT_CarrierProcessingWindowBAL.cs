﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings
{
    public class APPSIT_CarrierProcessingWindowBAL : BusinessLogicLayer.BaseBAL
    {

        public List<MASSIT_CarrierProcessingWindowBE> GetCarrierDetailsBAL(MASSIT_CarrierProcessingWindowBE oMASSIT_CarrierProcessingWindowBE)
        {
            List<MASSIT_CarrierProcessingWindowBE> oMASSIT_CarrierProcessingWindowBEList = new List<MASSIT_CarrierProcessingWindowBE>();

            try
            {
                APPSIT_CarrierProcessingWindowDAL oMASSIT_CarrierProcessingWindowDAL = new APPSIT_CarrierProcessingWindowDAL();

                oMASSIT_CarrierProcessingWindowBEList = oMASSIT_CarrierProcessingWindowDAL.GetCarrierDetailsDAL(oMASSIT_CarrierProcessingWindowBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_CarrierProcessingWindowBEList;
        }

        public DataTable GetCarrierDetailsBAL(MASSIT_CarrierProcessingWindowBE oMASSIT_CarrierProcessingWindowBE, string Nothing = "")
        {
            DataTable dt = new DataTable();
            try
            {
                APPSIT_CarrierProcessingWindowDAL oMASSIT_CarrierProcessingWindowDAL = new APPSIT_CarrierProcessingWindowDAL();

                dt = oMASSIT_CarrierProcessingWindowDAL.GetCarrierDetailsDAL(oMASSIT_CarrierProcessingWindowBE, "");

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public MASSIT_CarrierProcessingWindowBE GetCarrierDetailsByIdBAL(MASSIT_CarrierProcessingWindowBE oMASSIT_CarrierProcessingWindowBE)
        {
            MASSIT_CarrierProcessingWindowBE oNewMASSIT_CarrierProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
            List<MASSIT_CarrierProcessingWindowBE> oMASSIT_CarrierProcessingWindowBEList = new List<MASSIT_CarrierProcessingWindowBE>();

            try
            {
                APPSIT_CarrierProcessingWindowDAL oMASSIT_CarrierProcessingWindowDAL = new APPSIT_CarrierProcessingWindowDAL();

                oMASSIT_CarrierProcessingWindowBEList = oMASSIT_CarrierProcessingWindowDAL.GetCarrierDetailsDAL(oMASSIT_CarrierProcessingWindowBE);

                oNewMASSIT_CarrierProcessingWindowBE = oMASSIT_CarrierProcessingWindowBEList[0];

            }

            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewMASSIT_CarrierProcessingWindowBE;
        }

        public int? addEditCarrierDetailsBAL(MASSIT_CarrierProcessingWindowBE oNewMASSIT_CarrierProcessingWindowBE)
        {
            int? intResult = 0;
            try
            {
                APPSIT_CarrierProcessingWindowDAL oMASSIT_CarrierProcessingWindowDAL = new APPSIT_CarrierProcessingWindowDAL();
                intResult = oMASSIT_CarrierProcessingWindowDAL.addEditCarrierDetailsDAL(oNewMASSIT_CarrierProcessingWindowBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;

        }

        public MASSIT_CarrierProcessingWindowBE GetConstraintsBAL(MASSIT_CarrierProcessingWindowBE oMASSIT_CarrierProcessingWindowBE)
        {
            MASSIT_CarrierProcessingWindowBE oNewMASSIT_CarrierProcessingWindowBE = new MASSIT_CarrierProcessingWindowBE();
            List<MASSIT_CarrierProcessingWindowBE> oMASSIT_CarrierProcessingWindowBEList = new List<MASSIT_CarrierProcessingWindowBE>();

            try
            {
                APPSIT_CarrierProcessingWindowDAL oMASSIT_CarrierProcessingWindowDAL = new APPSIT_CarrierProcessingWindowDAL();

                oMASSIT_CarrierProcessingWindowBEList = oMASSIT_CarrierProcessingWindowDAL.GetConstraintsDAL(oMASSIT_CarrierProcessingWindowBE);

                if (oMASSIT_CarrierProcessingWindowBEList != null && oMASSIT_CarrierProcessingWindowBEList.Count > 0)
                    oNewMASSIT_CarrierProcessingWindowBE = oMASSIT_CarrierProcessingWindowBEList[0];

            }

            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewMASSIT_CarrierProcessingWindowBE;
        }



    }
}

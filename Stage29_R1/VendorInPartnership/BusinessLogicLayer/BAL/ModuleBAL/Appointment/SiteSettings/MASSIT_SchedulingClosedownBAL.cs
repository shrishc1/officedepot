﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings
{
    public class MASSIT_SchedulingClosedownBAL
    {

        public List<MASSIT_SchedulingClosedownBE> GetSchedulingClosedownBAL(MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE)
        {
            List<MASSIT_SchedulingClosedownBE> oMASSIT_SchedulingClosedownBEList = new List<MASSIT_SchedulingClosedownBE>();

            try
            {
                MASSIT_SchedulingClosedownDAL oMASSIT_SchedulingClosedownDAL = new MASSIT_SchedulingClosedownDAL();

                oMASSIT_SchedulingClosedownBEList = oMASSIT_SchedulingClosedownDAL.GetSchedulingClosedownDAL(oMASSIT_SchedulingClosedownBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_SchedulingClosedownBEList;
        }


        public int? AddEditSchedulingClosedownBAL(MASSIT_SchedulingClosedownBE oMASSIT_SchedulingClosedownBE)
        {
            int? intResult = 0;

            try
            {
                MASSIT_SchedulingClosedownDAL oMASSIT_SchedulingClosedownDAL = new MASSIT_SchedulingClosedownDAL();

                intResult = oMASSIT_SchedulingClosedownDAL.AddEditSchedulingClosedownDAL(oMASSIT_SchedulingClosedownBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

    }


}

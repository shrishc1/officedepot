﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using Utilities;


namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings
{
    public class MASSIT_HolidayBAL
    {
        public List<MASSIT_HolidayBE> GetHolidaysBAL(MASSIT_HolidayBE oMASSIT_HolidayBE)
        {
            List<MASSIT_HolidayBE> oMASSIT_HolidayBEList = new List<MASSIT_HolidayBE>();

            try
            {
                MASSIT_HolidayDAL oMASSIT_HolidayDAL = new MASSIT_HolidayDAL();

                oMASSIT_HolidayBEList = oMASSIT_HolidayDAL.GetHolidaysDAL(oMASSIT_HolidayBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_HolidayBEList;
        }

        public int? addEditHolidayBAL(MASSIT_HolidayBE oMASSIT_HolidayBE)
        {
            int? intResult = 0;
            try
            {
                MASSIT_HolidayDAL oMASSIT_HolidayDAL = new MASSIT_HolidayDAL();
                intResult = oMASSIT_HolidayDAL.addEditHolidayDAL(oMASSIT_HolidayBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

    }
}

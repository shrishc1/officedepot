﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings
{
    public class APPSIT_VehicleDoorMatrixBAL
    {

        public List<MASSIT_VehicleDoorMatrixBE> GetVehicleDoorMatrixBAL(int VehicleTypeID)
        {
            List<MASSIT_VehicleDoorMatrixBE> oMASSIT_VehicleDoorMatrixBEList = new List<MASSIT_VehicleDoorMatrixBE>();

            try
            {
                APPSIT_VehicleDoorMatrixDAL oMASSIT_VehicleDoorMatrixDAL = new APPSIT_VehicleDoorMatrixDAL();

                oMASSIT_VehicleDoorMatrixBEList = oMASSIT_VehicleDoorMatrixDAL.GetVehicleDoorMatrixDAL(VehicleTypeID);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_VehicleDoorMatrixBEList;
        }

        public List<MASSIT_VehicleDoorMatrixBE> GetVehicleDoorMatrixDetailsBAL(MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE)
        {
            List<MASSIT_VehicleDoorMatrixBE> oMASSIT_VehicleDoorMatrixBEList = new List<MASSIT_VehicleDoorMatrixBE>();

            try
            {
                APPSIT_VehicleDoorMatrixDAL oMASSIT_VehicleDoorMatrixDAL = new APPSIT_VehicleDoorMatrixDAL();

                oMASSIT_VehicleDoorMatrixBEList = oMASSIT_VehicleDoorMatrixDAL.GetVehicleDoorMatrixDetailsDAL(oMASSIT_VehicleDoorMatrixBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_VehicleDoorMatrixBEList;
        }

        public MASSIT_VehicleDoorMatrixBE GetVehicleDoorMatrixDetailsByIdBAL(MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE)
        {
            MASSIT_VehicleDoorMatrixBE oNewMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();
            List<MASSIT_VehicleDoorMatrixBE> oMASSIT_VehicleDoorMatrixBEList = new List<MASSIT_VehicleDoorMatrixBE>();
            try
            {
                APPSIT_VehicleDoorMatrixDAL oMASSIT_VehicleDoorMatrixDAL = new APPSIT_VehicleDoorMatrixDAL();

                oMASSIT_VehicleDoorMatrixBEList = oMASSIT_VehicleDoorMatrixDAL.GetVehicleDoorMatrixDetailsDAL(oMASSIT_VehicleDoorMatrixBE);
                oNewMASSIT_VehicleDoorMatrixBE = oMASSIT_VehicleDoorMatrixBEList[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewMASSIT_VehicleDoorMatrixBE;
        }

        public int? addEditVehicleDoorMatrixDetailsBAL(MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE)
        {
            int? intResult = 0;
            try
            {
                APPSIT_VehicleDoorMatrixDAL oMASSIT_VehicleDoorMatrixDAL = new APPSIT_VehicleDoorMatrixDAL();
                intResult = oMASSIT_VehicleDoorMatrixDAL.addEditVehicleDoorMatrixDetailsDAL(oMASSIT_VehicleDoorMatrixBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public bool isVehcileTypeMatchedBAL(MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE)
        {
            bool intResult = false;
            try
            {
                APPSIT_VehicleDoorMatrixDAL oMASSIT_VehicleDoorMatrixDAL = new APPSIT_VehicleDoorMatrixDAL();
                intResult = oMASSIT_VehicleDoorMatrixDAL.isVehcileTypeMatchedDAL(oMASSIT_VehicleDoorMatrixBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

    }
}

﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings
{
    public class APPSIT_BookingTypeExclusionBAL : BaseBAL
    {

        public List<MASSIT_BookingTypeExclusionBE> GetBookingTypeExclusionDetailsBAL(MASSIT_BookingTypeExclusionBE oMASSIT_BookingTypeExclusionBE)
        {
            List<MASSIT_BookingTypeExclusionBE> oMASSIT_BookingTypeExclusionBEList = new List<MASSIT_BookingTypeExclusionBE>();

            try
            {
                APPSIT_BookingTypeExclusionDAL oAPPSIT_BookingTypeExclusionDAL = new APPSIT_BookingTypeExclusionDAL();

                oMASSIT_BookingTypeExclusionBEList = oAPPSIT_BookingTypeExclusionDAL.GetBookingTypeExclusionsDAL(oMASSIT_BookingTypeExclusionBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_BookingTypeExclusionBEList;
        }

        public DataTable GetBookingTypeExclusionDetailsBAL(MASSIT_BookingTypeExclusionBE oMASSIT_BookingTypeExclusionBE, string Nothing = "Nothing")
        {
            DataTable dt = new DataTable();

            try
            {
                APPSIT_BookingTypeExclusionDAL oAPPSIT_BookingTypeExclusionDAL = new APPSIT_BookingTypeExclusionDAL();

                dt = oAPPSIT_BookingTypeExclusionDAL.GetBookingTypeExclusionsDAL(oMASSIT_BookingTypeExclusionBE, "");
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public int? addEditBookingTypeExclusionDetailsBAL(MASSIT_BookingTypeExclusionBE oMASSIT_BookingTypeExclusionBE)
        {
            int? intResult = 0;
            try
            {
                APPSIT_BookingTypeExclusionDAL oAPPSIT_BookingTypeExclusionDAL = new APPSIT_BookingTypeExclusionDAL();
                intResult = oAPPSIT_BookingTypeExclusionDAL.addEditBookingTypeExclusionDetailsDAL(oMASSIT_BookingTypeExclusionBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public List<MASSIT_BookingTypeExclusionBE> GetBookingTypeBAL(MASSIT_BookingTypeExclusionBE oMASSIT_BookingTypeExclusionBE)
        {
            List<MASSIT_BookingTypeExclusionBE> oMASSIT_BookingTypeExclusionBEList = new List<MASSIT_BookingTypeExclusionBE>();

            try
            {
                APPSIT_BookingTypeExclusionDAL oAPPSIT_BookingTypeExclusionDAL = new APPSIT_BookingTypeExclusionDAL();

                oMASSIT_BookingTypeExclusionBEList = oAPPSIT_BookingTypeExclusionDAL.GetBookingTypeDAL(oMASSIT_BookingTypeExclusionBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_BookingTypeExclusionBEList;
        }
    }
}

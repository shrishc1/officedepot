﻿using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings
{
    public class APPSIT_FixedSlotBAL
    {

        public int? addEdiFixedSlotBAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            int? intResult = 0;
            try
            {
                APPSIT_FixedSlotDAL oMASSIT_FixedSlotDAL = new APPSIT_FixedSlotDAL();
                intResult = oMASSIT_FixedSlotDAL.addEdiFixedSlotDAL(oMASSIT_FixedSlotBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataSet getUnloadTimeBAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            DataSet dsResult = null;
            try
            {
                APPSIT_FixedSlotDAL oMASSIT_FixedSlotDAL = new APPSIT_FixedSlotDAL();
                dsResult = oMASSIT_FixedSlotDAL.getUnloadTimeDAL(oMASSIT_FixedSlotBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dsResult;
        }

        public List<MASSIT_FixedSlotBE> GetAllFixedSlotBAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            List<MASSIT_FixedSlotBE> oMASSIT_FixedSlotBEList = new List<MASSIT_FixedSlotBE>();
            try
            {
                APPSIT_FixedSlotDAL oMASSIT_FixedSlotDAL = new APPSIT_FixedSlotDAL();
                oMASSIT_FixedSlotBEList = oMASSIT_FixedSlotDAL.GetAllFixedSlotDAL(oMASSIT_FixedSlotBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_FixedSlotBEList;
        }
        public List<MASSIT_FixedSlotBE> GetMaxDeliveriesPerDayBAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            List<MASSIT_FixedSlotBE> oMASSIT_FixedSlotBEList = new List<MASSIT_FixedSlotBE>();
            try
            {
                APPSIT_FixedSlotDAL oMASSIT_FixedSlotDAL = new APPSIT_FixedSlotDAL();
                oMASSIT_FixedSlotBEList = oMASSIT_FixedSlotDAL.GetMaxDeliveriesPerDayDAL(oMASSIT_FixedSlotBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_FixedSlotBEList;
        }

        public List<MASSIT_FixedSlotBE> GetAllFixedSlotByVendorIDBAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            List<MASSIT_FixedSlotBE> oMASSIT_FixedSlotBEList = new List<MASSIT_FixedSlotBE>();
            try
            {
                APPSIT_FixedSlotDAL oMASSIT_FixedSlotDAL = new APPSIT_FixedSlotDAL();
                oMASSIT_FixedSlotBEList = oMASSIT_FixedSlotDAL.GetAllFixedSlotByVendorIDDAL(oMASSIT_FixedSlotBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_FixedSlotBEList;
        }

        public List<MASSIT_FixedSlotBE> GetMaximumBAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            List<MASSIT_FixedSlotBE> oMASSIT_FixedSlotBEList = new List<MASSIT_FixedSlotBE>();
            try
            {
                APPSIT_FixedSlotDAL oMASSIT_FixedSlotDAL = new APPSIT_FixedSlotDAL();
                oMASSIT_FixedSlotBEList = oMASSIT_FixedSlotDAL.GetMaximumDAL(oMASSIT_FixedSlotBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_FixedSlotBEList;
        }
    }
}

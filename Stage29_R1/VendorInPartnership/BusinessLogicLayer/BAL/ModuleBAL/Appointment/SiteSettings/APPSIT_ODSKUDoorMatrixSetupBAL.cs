﻿// -----------------------------------------------------------------------
// <copyright file="APPSIT_ODSKUDoorMatrixSetup.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings
{
    using BusinessEntities.ModuleBE.Appointment.SiteSettings;
    using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;
    using System;
    using System.Collections.Generic;
    using Utilities;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class MASSIT_ODSKUDoorMatrixSetupBAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oMASSIT_ODSKUDoorMatrixSetupBE"></param>
        /// <returns></returns>
        public List<MASSIT_ODSKUDoorMatrixSetupBE> GetOdSkuDoorMatrixSetupDetailsBAL(MASSIT_ODSKUDoorMatrixSetupBE oMASSIT_ODSKUDoorMatrixSetupBE)
        {
            List<MASSIT_ODSKUDoorMatrixSetupBE> oMASSIT_ODSKUDoorMatrixSetupBEList = new List<MASSIT_ODSKUDoorMatrixSetupBE>();

            try
            {
                APPSIT_OdSkuDoorMatrixSetupDAL oMASSIT_OdSkuDoorMatrixSetupDAL = new APPSIT_OdSkuDoorMatrixSetupDAL();
                oMASSIT_ODSKUDoorMatrixSetupBEList = oMASSIT_OdSkuDoorMatrixSetupDAL.GetODSKUDoorMatrixSetupDetailsDAL(oMASSIT_ODSKUDoorMatrixSetupBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_ODSKUDoorMatrixSetupBEList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oMASSIT_ODSKUDoorMatrixSetupBE"></param>
        /// <returns></returns>
        public int? addEditDoorNoSetupDetailsBAL(MASSIT_ODSKUDoorMatrixSetupBE oMASSIT_ODSKUDoorMatrixSetupBE)
        {
            int? intResult = 0;
            try
            {
                APPSIT_OdSkuDoorMatrixSetupDAL oMASSIT_OdSkuDoorMatrixSetupDAL = new APPSIT_OdSkuDoorMatrixSetupDAL();
                intResult = oMASSIT_OdSkuDoorMatrixSetupDAL.addEditODSKUDoorMatrixSetupDetailsDAL(oMASSIT_ODSKUDoorMatrixSetupBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oMASSIT_ODSKUDoorMatrixSetupBE"></param>
        /// <returns></returns>
        public List<MASSIT_DoorNoSetupBE> GetDoorNameBAL(MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE)
        {
            List<MASSIT_DoorNoSetupBE> oMASSIT_DoorNoSetupBEList = new List<MASSIT_DoorNoSetupBE>();
            try
            {
                APPSIT_OdSkuDoorMatrixSetupDAL oAPPSIT_OdSkuDoorMatrixSetupDAL = new APPSIT_OdSkuDoorMatrixSetupDAL();
                oMASSIT_DoorNoSetupBEList = oAPPSIT_OdSkuDoorMatrixSetupDAL.GetDoorNameDAL(oMASSIT_DoorNoSetupBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_DoorNoSetupBEList;

        }

        public MASSIT_ODSKUDoorMatrixSetupBE GetDoorNameByIDBAL(MASSIT_ODSKUDoorMatrixSetupBE oMASSIT_DoorNoSetupBE)
        {
            MASSIT_ODSKUDoorMatrixSetupBE oNewMASSIT_DoorNoSetupBE = new MASSIT_ODSKUDoorMatrixSetupBE();
            List<MASSIT_ODSKUDoorMatrixSetupBE> oMASSIT_ODSKUDoorMatrixSetupBEList = new List<MASSIT_ODSKUDoorMatrixSetupBE>();

            try
            {
                APPSIT_OdSkuDoorMatrixSetupDAL oMASSIT_OdSkuDoorMatrixSetupDAL = new APPSIT_OdSkuDoorMatrixSetupDAL();

                oMASSIT_ODSKUDoorMatrixSetupBEList = oMASSIT_OdSkuDoorMatrixSetupDAL.GetODSKUDoorMatrixSetupDetailsDAL(oMASSIT_DoorNoSetupBE);

                oNewMASSIT_DoorNoSetupBE = oMASSIT_ODSKUDoorMatrixSetupBEList[0];

            }

            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewMASSIT_DoorNoSetupBE;
        }

    }
}

﻿using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using DataAccessLayer.ModuleDAL.Appointment.CountrySetting;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting
{
    public class APPCNT_DoorTypeBAL : BusinessLogicLayer.BaseBAL
    {

        public List<MASCNT_DoorTypeBE> GetDoorTypeDetailsBAL(MASCNT_DoorTypeBE oMASCNT_DoorTypeBE)
        {
            List<MASCNT_DoorTypeBE> oMASCNT_DoorTypeBEList = new List<MASCNT_DoorTypeBE>();

            try
            {
                APPCNT_DoorTypeDAL oAPPCNT_DoorTypeDAL = new APPCNT_DoorTypeDAL();

                oMASCNT_DoorTypeBEList = oAPPCNT_DoorTypeDAL.GetDoorTypeDetailsDAL(oMASCNT_DoorTypeBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASCNT_DoorTypeBEList;
        }
        public List<MASCNT_DoorTypeBE> GetDoorTypeBAL(MASCNT_DoorTypeBE oMASCNT_DoorTypeBE)
        {
            List<MASCNT_DoorTypeBE> oMASCNT_DoorTypeBEList = new List<MASCNT_DoorTypeBE>();

            try
            {
                APPCNT_DoorTypeDAL oAPPCNT_DoorTypeDAL = new APPCNT_DoorTypeDAL();

                oMASCNT_DoorTypeBEList = oAPPCNT_DoorTypeDAL.GetDoorTypeDAL(oMASCNT_DoorTypeBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASCNT_DoorTypeBEList;
        }



        public MASCNT_DoorTypeBE GetDoorTypeDetailsByIdBAL(MASCNT_DoorTypeBE oMASCNT_DoorTypeBE)
        {
            MASCNT_DoorTypeBE oNewMASCNT_DoorTypeBE = new MASCNT_DoorTypeBE();
            List<MASCNT_DoorTypeBE> oMASCNT_DoorTypeBEList = new List<MASCNT_DoorTypeBE>();
            try
            {
                APPCNT_DoorTypeDAL oAPPCNT_DoorTypeDAL = new APPCNT_DoorTypeDAL();

                oMASCNT_DoorTypeBEList = oAPPCNT_DoorTypeDAL.GetDoorTypeDetailsDAL(oMASCNT_DoorTypeBE);
                oNewMASCNT_DoorTypeBE = oMASCNT_DoorTypeBEList[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewMASCNT_DoorTypeBE;
        }

        public int? addEditDoorTypeDetailsBAL(MASCNT_DoorTypeBE oMASCNT_DoorTypeBE)
        {
            int? intResult = 0;
            try
            {
                APPCNT_DoorTypeDAL oAPPCNT_DoorTypeDAL = new APPCNT_DoorTypeDAL();
                intResult = oAPPCNT_DoorTypeDAL.addEditDoorTypeDetailsDAL(oMASCNT_DoorTypeBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

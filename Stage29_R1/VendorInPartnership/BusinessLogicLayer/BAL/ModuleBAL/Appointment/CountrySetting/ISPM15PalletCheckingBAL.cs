﻿using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using DataAccessLayer.ModuleDAL.Appointment.CountrySetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting
{
    public class ISPM15PalletCheckingBAL : BaseBAL
    {
        public List<ISPM15PalletCheckingBE> GetISPM15PalletCheckingDetailsBAL(ISPM15PalletCheckingBE ispm15)
        {
            List<ISPM15PalletCheckingBE> oMASCNT_CarrierBEList = new List<ISPM15PalletCheckingBE>();

            try
            {
                ISPM15PalletCheckingDAL ISPM15PalletCheckingDAL = new ISPM15PalletCheckingDAL();
                oMASCNT_CarrierBEList = ISPM15PalletCheckingDAL.GetISPM15PalletCheckingDetailsDAL(ispm15);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASCNT_CarrierBEList;
        }
        public int? SaveISPM15PalletCheckingBAL(ISPM15PalletCheckingBE ispm15PalletCheckingBE)
        {
            int? intResult = 0;
            try
            {
                ISPM15PalletCheckingDAL ISPM15PalletCheckingDAL = new ISPM15PalletCheckingDAL();
                intResult = ISPM15PalletCheckingDAL.SaveISPM15PalletCheckingDAL(ispm15PalletCheckingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public bool IsSiteCountryExistsBAL(ISPM15PalletCheckingBE ispm15)
        {
            bool intResult = false;
            try
            {
                ISPM15PalletCheckingDAL ISPM15PalletCheckingDAL = new ISPM15PalletCheckingDAL();
                intResult = ISPM15PalletCheckingDAL.IsSiteCountryExistsDAL(ispm15);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        }
}

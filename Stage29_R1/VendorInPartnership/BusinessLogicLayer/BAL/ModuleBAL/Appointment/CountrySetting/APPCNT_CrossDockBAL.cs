﻿using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using DataAccessLayer.ModuleDAL.Appointment.CountrySetting;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting
{
    public class APPCNT_CrossDockBAL : BusinessLogicLayer.BaseBAL
    {

        public List<MASCNT_CrossDockBE> GetCrossDockDetailsBAL(MASCNT_CrossDockBE oMASCNT_CrossDockBE)
        {
            List<MASCNT_CrossDockBE> oMASCNT_CrossDockBEList = new List<MASCNT_CrossDockBE>();

            try
            {
                APPCNT_CrossDockDAL oAPPCNT_CrossDockDAL = new APPCNT_CrossDockDAL();
                oMASCNT_CrossDockBEList = oAPPCNT_CrossDockDAL.GetCrossDockDetailsDAL(oMASCNT_CrossDockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASCNT_CrossDockBEList;
        }

        public MASCNT_CrossDockBE GetCrossDockDetailsByIdBAL(MASCNT_CrossDockBE oMASCNT_CrossDockBE)
        {
            MASCNT_CrossDockBE oNewMASCNT_CrossDockBE = new MASCNT_CrossDockBE();
            List<MASCNT_CrossDockBE> oMASCNT_CrossDockBEList = new List<MASCNT_CrossDockBE>();

            try
            {
                APPCNT_CrossDockDAL oAPPCNT_CrossDockDAL = new APPCNT_CrossDockDAL();

                oMASCNT_CrossDockBEList = oAPPCNT_CrossDockDAL.GetCrossDockDetailsDAL(oMASCNT_CrossDockBE);
                oNewMASCNT_CrossDockBE = oMASCNT_CrossDockBEList[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewMASCNT_CrossDockBE;
        }

        public int? addEditCrossDockDetailsBAL(MASCNT_CrossDockBE oMASCNT_CrossDockBE)
        {
            int? intResult = 0;
            try
            {
                APPCNT_CrossDockDAL oAPPCNT_CrossDockDAL = new APPCNT_CrossDockDAL();
                intResult = oAPPCNT_CrossDockDAL.addEditCrossDockDetailsDAL(oMASCNT_CrossDockBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

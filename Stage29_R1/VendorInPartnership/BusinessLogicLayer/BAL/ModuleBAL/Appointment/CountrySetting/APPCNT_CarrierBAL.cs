﻿using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.CountrySetting;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting
{
    public class APPCNT_CarrierBAL : BusinessLogicLayer.BaseBAL
    {

        public List<MASCNT_CarrierBE> GetCarrierDetailsBAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            List<MASCNT_CarrierBE> oMASCNT_CarrierBEList = new List<MASCNT_CarrierBE>();

            try
            {
                APPCNT_CarrierDAL oAPPCNT_CarrierDAL = new APPCNT_CarrierDAL();

                oMASCNT_CarrierBEList = oAPPCNT_CarrierDAL.GetCarrierDetailsDAL(oMASCNT_CarrierBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASCNT_CarrierBEList;
        }

        public DataTable GetAllCarrierDetailsBAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            DataTable dt = new DataTable();
            try
            {
                APPCNT_CarrierDAL oAPPCNT_CarrierDAL = new APPCNT_CarrierDAL();

                dt = oAPPCNT_CarrierDAL.GetAllCarrierDetailsDAL(oMASCNT_CarrierBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public MASCNT_CarrierBE GetCarrierDetailsByIdBAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            MASCNT_CarrierBE oNewMASCNT_CarrierBE = new MASCNT_CarrierBE();
            List<MASCNT_CarrierBE> oMASCNT_CarrierBEList = new List<MASCNT_CarrierBE>();
            try
            {
                APPCNT_CarrierDAL oAPPCNT_CarrierDAL = new APPCNT_CarrierDAL();

                oMASCNT_CarrierBEList = oAPPCNT_CarrierDAL.GetCarrierDetailsDAL(oMASCNT_CarrierBE);
                oNewMASCNT_CarrierBE = oMASCNT_CarrierBEList[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewMASCNT_CarrierBE;
        }

        public int? addEditCarrierDetailsBAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            int? intResult = 0;
            try
            {
                APPCNT_CarrierDAL oAPPCNT_CarrierDAL = new APPCNT_CarrierDAL();
                intResult = oAPPCNT_CarrierDAL.addEditCarrierDetailsDAL(oMASCNT_CarrierBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MASCNT_CarrierBE> GetAllCarriersBAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            List<MASCNT_CarrierBE> oMASCNT_CarrierBEList = new List<MASCNT_CarrierBE>();
            try
            {
                APPCNT_CarrierDAL oAPPCNT_CarrierDAL = new APPCNT_CarrierDAL();
                oMASCNT_CarrierBEList = oAPPCNT_CarrierDAL.GetAllCarriersDAL(oMASCNT_CarrierBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASCNT_CarrierBEList;
        }

        public int? addEditCarrierMiscConsBAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            int? intResult = 0;
            try
            {
                APPCNT_CarrierDAL oAPPCNT_CarrierDAL = new APPCNT_CarrierDAL();
                intResult = oAPPCNT_CarrierDAL.addEditCarrierMiscConsDAL(oMASCNT_CarrierBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteCarrierMiscConsBAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            int? intResult = 0;
            try
            {
                APPCNT_CarrierDAL oAPPCNT_CarrierDAL = new APPCNT_CarrierDAL();
                intResult = oAPPCNT_CarrierDAL.DeleteCarrierMiscConsDAL(oMASCNT_CarrierBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? IsCarrierMiscConsExistBAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            int? intResult = 0;
            try
            {
                APPCNT_CarrierDAL oAPPCNT_CarrierDAL = new APPCNT_CarrierDAL();
                intResult = oAPPCNT_CarrierDAL.IsCarrierMiscConsExistDAL(oMASCNT_CarrierBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MASSIT_FixedSlotBE> GetAllFixedSlotByCarrierIDBAL(MASSIT_FixedSlotBE oMASSIT_FixedSlotBE)
        {
            List<MASSIT_FixedSlotBE> lstMASSIT_FixedSlotBE = new List<MASSIT_FixedSlotBE>();
            try
            {
                APPCNT_CarrierDAL oAPPCNT_CarrierDAL = new APPCNT_CarrierDAL();
                lstMASSIT_FixedSlotBE = oAPPCNT_CarrierDAL.GetAllFixedSlotByCarrierIDDAL(oMASSIT_FixedSlotBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstMASSIT_FixedSlotBE;
        }

        public List<MASCNT_CarrierBE> GetAllCarrierMiscConsBAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            List<MASCNT_CarrierBE> oMASCNT_CarrierBEList = new List<MASCNT_CarrierBE>();

            try
            {
                APPCNT_CarrierDAL oAPPCNT_CarrierDAL = new APPCNT_CarrierDAL();

                oMASCNT_CarrierBEList = oAPPCNT_CarrierDAL.GetAllCarrierMiscConsDAL(oMASCNT_CarrierBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASCNT_CarrierBEList;
        }

        public List<MASCNT_CarrierBE> GetCarrierUsersEmailsWithLanguageBAL(MASCNT_CarrierBE oMASCNT_CarrierBE)
        {
            List<MASCNT_CarrierBE> oMASCNT_CarrierBEList = new List<MASCNT_CarrierBE>();
            try
            {
                APPCNT_CarrierDAL oAPPCNT_CarrierDAL = new APPCNT_CarrierDAL();
                oMASCNT_CarrierBEList = oAPPCNT_CarrierDAL.GetCarrierUsersEmailsWithLanguageDAL(oMASCNT_CarrierBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASCNT_CarrierBEList;
        }
    }
}

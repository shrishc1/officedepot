﻿using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using DataAccessLayer.ModuleDAL.Appointment.CountrySetting;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.CountrySetting
{
    public class APPCNT_AutoEmailBAL
    {
        public List<MASCNT_AutoEmailBE> GetAutoEmailPODetailsBAL(MASCNT_AutoEmailBE AutoEmailBE)
        {
            var lstAutoEmailBE = new List<MASCNT_AutoEmailBE>();
            try
            {
                APPCNT_AutoEmailDAL AutoEmailDAL = new APPCNT_AutoEmailDAL();
                lstAutoEmailBE = AutoEmailDAL.GetAutoEmailPODetailsDAL(AutoEmailBE);
                AutoEmailDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAutoEmailBE;
        }


        public int? SaveAutoEmailPOToVendorStatusBAL(MASCNT_AutoEmailBE AutoEmailBE)
        {
            int? intResult = 0;
            try
            {
                APPCNT_AutoEmailDAL AutoEmailDAL = new APPCNT_AutoEmailDAL();
                intResult = AutoEmailDAL.SaveAutoEmailPOToVendorStatusDAL(AutoEmailBE);
                AutoEmailDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }



}


﻿using BusinessEntities.ModuleBE.Appointment.Booking;
using DataAccessLayer.ModuleDAL.Appointment.Booking;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.Booking
{
    public class APPBOK_BookingBAL : BaseBAL
    {

        public List<APPBOK_BookingBE> GetGetVolumeProcessedBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetGetVolumeProcessedDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public int? AddVendorWindowBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oMASBOK_BookingDAL.AddVendorWindowBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetBookingDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();


                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetBookingDetailsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetDeliveryUnloadDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetDeliveryUnloadDetailsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<string> GetUnloadImageDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<string> oAPPBOK_BookingBEList = new List<string>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetUnloadImageDetailsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public int? DeleteBookingDetailBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.DeleteBookingDetailDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? addEditBookingDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.addEditBookingDetailsDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// User for Managing Delivery Unloaded First step
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? updateDeliveryUnloadedFirstStepBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.updateDeliveryUnloadedFirstStepDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// User for Managing Delivery Unloaded Final step
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? updateDeliveryUnloadedFinalStepBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.updateDeliveryUnloadedFinalStepDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateDeliveryUnloadedImagesBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.UpdateDeliveryUnloadedImagesDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? updateDeliveryUnloadedAcceptAllBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.updateDeliveryUnloadedAcceptAllDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Used for managing Delivery Refusal
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? addEditDeliveryRefusalBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.addEditDeliveryRefusalDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Used for managing unexpected delivery for Vendor
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? addEditUnexpectedDeliveryBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.addEditUnexpectedDeliveryDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Used for managing unexpected delivery for Carrier
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? addEditUnexpectedDeliveryCarrierBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.addEditUnexpectedDeliveryCarrierDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? addEditQualityCheckBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.addEditQualityCheckDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetBookingHistoryBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetBookingHistoryDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetTimeSlotInfoBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetTimeSlotInfoDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }


        public DataTable GetVolumeDetailsDAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable oAPPBOK_BookingBEList = new DataTable();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetVolumeDetailsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }
        public DataTable GetAllBookingDetailBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable oAPPBOK_BookingBEList = new DataTable();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetAllBookingDetailDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }
        public DataTable GetSchedulePOLineReport(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable oAPPBOK_BookingBEList = new DataTable();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetSchedulePOLineReport(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetVendorBookingFixedSlotDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetVendorBookingFixedSlotDetailsDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }
        public List<APPBOK_BookingBE> GetVendorBookingWindowDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetVendorBookingWindowDetailsDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetVendorBookingFixedSlotDoorBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetVendorBookingFixedSlotDoorDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public int? AddVendorBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oMASBOK_BookingDAL.AddVendorBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? AddCarrierBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oMASBOK_BookingDAL.AddCarrierBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? AddCarrierWindowBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oMASBOK_BookingDAL.AddCarrierWindowBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


        public int? AddCarrierBookingDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oMASBOK_BookingDAL.AddCarrierBookingDetailsDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetBookingDetailsByIDBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetBookingDetailsByIDDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetAllBookingFixedSlotDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetAllBookingFixedSlotDoorDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }


        public List<APPBOK_BookingBE> GetBookedVolumeBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetBookedVolumeDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetBookingVolumeDeatilsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetBookingVolumeDeatilsDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public DataTable GetSchedulingPrintOutBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {

            DataTable oAPPBOK_BookingDatatable = new DataTable();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingDatatable = oMASBOK_BookingDAL.GetSchedulingPrintOutDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingDatatable;
        }

        public List<APPBOK_BookingBE> GetNonTimeBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetNonTimeBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }


        public List<APPBOK_BookingBE> GetVendorBookingDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetVendorBookingDetailsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetCarrierBookingDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetCarrierBookingDetailsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetCarrierVolumeDeatilsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetCarrierVolumeDeatilsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public int? CheckBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.CheckBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? CheckVendorBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.CheckVendorBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        // Sprint 1 - Point 1 - Start - Checking PO is existing or not.
        public List<APPBOK_BookingBE> IsExistingPO(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = null;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                lstAPPBOK_BookingBE = oAPPBOK_BookingDAL.IsExistingPO(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }
        // Sprint 1 - Point 1 - End

        // Sprint 1 - Point 9 - Begin

        public List<APPBOK_BookingBE> GetEmailIdsForStockPlannerBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = null;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                lstAPPBOK_BookingBE = oAPPBOK_BookingDAL.GetEmailIdsForStockPlannerDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }

        public List<APPBOK_BookingBE> GetEmailDataForRefusedDeliveriesBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = null;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                lstAPPBOK_BookingBE = oAPPBOK_BookingDAL.GetEmailDataForRefusedDeliveriesDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }
        // Sprint 1 - Point 9 - End

        public int? UpdateBookingStatusByIdBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oMASBOK_BookingDAL.UpdateBookingStatusByIdDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetNoShowDeliveryLetterBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetNoShowDeliveryLetterDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }
        public List<APPBOK_BookingBE> GetNoShowSPUsersBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetNoShowSPUsersDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetPurchaseOrderForBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetPurchaseOrderForBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetMultiVendorBookingDataBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetMultiVendorBookingDataDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public DataTable GetBookingInqueryBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable oAPPBOK_BookingBEList = new DataTable();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetBookingInqueryDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetOverdueBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetOverdueBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetWindowVolumeDataBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetWindowVolumeDataDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public DataTable GetProvisionalBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataTable oAPPBOK_BookingBEList = new DataTable();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetProvisionalBookingDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        /// <summary>
        /// Used for ading unknown Vendor
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? addUnknownVendorBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.addUnknownVendorDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetConfirmPurchaseOrderArrivalDataBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetConfirmPurchaseOrderArrivalDataDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public int? UpdateConfirmPurchaseOrderArrivalBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oMASBOK_BookingDAL.UpdateConfirmPurchaseOrderArrivalDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_BookingBE> GetDeletedBookingDetailsByIDBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetDeletedBookingDetailsByIDDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetProvisionalBookingReportViewByTypeBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            var oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                var oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetProvisionalBookingReportViewByTypeDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetProvisionalBookingReportSummaryViewBySiteBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            var oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                var oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetProvisionalBookingReportSummaryViewBySiteDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetProvisionalBookingReportDetailViewBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            var oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                var oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetProvisionalBookingReportDetailViewDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetNoShowReportBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            var oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                var oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetNoShowReportDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetPOReconciliationBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            var oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                var oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetPOReconciliationDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public void UpdateBookingReviewedStatusBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            try
            {
                var oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oMASBOK_BookingDAL.UpdateBookingReviewedStatusDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public List<APPBOK_BookingBE> GetDailyVolumeBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetDailyVolumeDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        // Sprint 14 - Point 21 - Start - Checking booking by PO is existing or not.
        public List<APPBOK_BookingBE> CheckExistingBookingsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = null;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                lstAPPBOK_BookingBE = oAPPBOK_BookingDAL.CheckExistingBookingsDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }
        // Sprint 14 - Point 21 - End

        public int? RejectChangesBookingDetailBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.RejectChangesBookingDetailDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataTable GetScheduleAvailabilityBAL(int SiteId, int iYear, int iMonth)
        {
            DataTable oAPPBOK_BookingBEList = new DataTable();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetScheduleAvailabilityDAL(SiteId, iYear, iMonth);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public int? OverdueBookingReportDetailBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.OverdueBookingReportDetailDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataSet GetDailyVolumeTrackerBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataSet ds = new DataSet();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                ds = oMASBOK_BookingDAL.GetDailyVolumeTrackerDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public List<APPBOK_BookingBE> DeletedBookingDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = null;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                lstAPPBOK_BookingBE = oAPPBOK_BookingDAL.DeletedBookingDetailsDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }


        public void InsertISPM15BAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            try
            {
                var oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oMASBOK_BookingDAL.InsertISPM15DAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public void InsertHazardousItemDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            try
            {
                var oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oMASBOK_BookingDAL.InsertHazardousItemDetailsDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }        

        public DataSet etVolumeDetailsAlternateSlotBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataSet ds = new DataSet();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                ds = oMASBOK_BookingDAL.GetVolumeDetailsAlternateSlotDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public DataTable GetAllTodayBookingWithVendorDetailsBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {

            DataTable oAPPBOK_BookingDatatable = new DataTable();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                oAPPBOK_BookingDatatable = oMASBOK_BookingDAL.GetAllTodayBookingWithVendorDetailsDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingDatatable;
        }


        public DataSet GetCurrentBookingInformationBAL(string Booking)
        {
            DataSet ds = new DataSet();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                ds = oMASBOK_BookingDAL.GetCurrentBookingInformationDAL(Booking);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }



        public DataTable GetWindowAlternateSlot(int SiteId, string ScheduleDate, string WeekDay, string action)
        {
            DataTable dt = new DataTable();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                dt = oMASBOK_BookingDAL.GetWindowAlternateSlot(SiteId, ScheduleDate, WeekDay, action);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataTable GetWindowSlotHoverData(int SiteId, string ScheduleDate, string action)
        {
            DataTable dt = new DataTable();
            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                dt = oMASBOK_BookingDAL.GetWindowSlotHoverData(SiteId, ScheduleDate, action);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public List<APPBOK_BookingBE> GetEmailDataForISPMIssueBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = null;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                lstAPPBOK_BookingBE = oAPPBOK_BookingDAL.GetEmailDataForISPMIssueDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }

        public List<APPBOK_BookingBE> GetEmailIdsForStockPlannerISPMIssueBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = null;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                lstAPPBOK_BookingBE = oAPPBOK_BookingDAL.GetEmailIdsForStockPlannerISPMIssueDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }

        public DataSet GetWindowVolumeDetailsAlternateSlotBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            DataSet ds = new DataSet();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();

                ds = oMASBOK_BookingDAL.GetVolumeWindowDetailsAlternateSlotDAL(oAPPBOK_BookingBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public List<APPBOK_BookingBE> GetNonStandardWindowBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> oAPPBOK_BookingBEList = new List<APPBOK_BookingBE>();

            try
            {
                APPBOK_BookingDAL oMASBOK_BookingDAL = new APPBOK_BookingDAL();
                oAPPBOK_BookingBEList = oMASBOK_BookingDAL.GetNonStandardWindowBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_BookingBEList;
        }

        public List<APPBOK_BookingBE> GetUnexpectedBookingDataForStockPlannerBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = null;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                lstAPPBOK_BookingBE = oAPPBOK_BookingDAL.GetUnexpectedBookingDataForStockPlannerDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }

        public List<APPBOK_BookingBE> GetEmailIdsForStockPlannerUnexpectedBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = null;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                lstAPPBOK_BookingBE = oAPPBOK_BookingDAL.GetEmailIdsForStockPlannerUnexpectedBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }


        public List<APPBOK_BookingBE> GetHazardousBookingBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            List<APPBOK_BookingBE> lstAPPBOK_BookingBE = null;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                lstAPPBOK_BookingBE = oAPPBOK_BookingDAL.GetHazardousBookingDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstAPPBOK_BookingBE;
        }

        public int? UndoBookingDeliveryArrivedStatusBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.UndoBookingDeliveryArrivedStatusDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UndoBookingDeliveryUnLoadStatusBAL(APPBOK_BookingBE oAPPBOK_BookingBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                intResult = oAPPBOK_BookingDAL.UndoBookingDeliveryUnLoadStatusDAL(oAPPBOK_BookingBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DashboardBooking> GetDashboardBookingsBAL(string action, int Siteid, DateTime Date)
        {
            List<DashboardBooking> DashboardBookingResult = new List<DashboardBooking>();
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                DashboardBookingResult = oAPPBOK_BookingDAL.GetDashboardBookingsDAL(action,Siteid, Date);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DashboardBookingResult;
        }

        public List<DashboardBooking> GetDashboardOneBookingsBAL(string action, int Siteid, DateTime Date)
        {
            List<DashboardBooking> DashboardBookingResult = new List<DashboardBooking>();
            try
            {
                APPBOK_BookingDAL oAPPBOK_BookingDAL = new APPBOK_BookingDAL();
                DashboardBookingResult = oAPPBOK_BookingDAL.GetDashboardOneBookingsDAL(action, Siteid, Date);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DashboardBookingResult;
        }
    }
}



﻿using BusinessEntities.ModuleBE.Appointment.Booking;
using BusinessEntities.ModuleBE.Languages.Languages;
using DataAccessLayer.ModuleDAL.Appointment.Booking;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Appointment.Booking
{
    public class APPBOK_CommunicationBAL
    {
        public int? AddItemBAL(int BookingID, string fromAddress, string toAddress, string mailSubject, string htmlBody, int? UserId, CommunicationType.Enum communicationType, int LanguageID, bool MailSentInLanguage, bool IsMailSent = true)
        {
            // Resending of Delivery mails
            APPBOK_CommunicationBE oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
            oAPPBOK_CommunicationBE.Action = "AddBookingCommunication";
            oAPPBOK_CommunicationBE.CommunicationType = CommunicationType.Name(communicationType);
            oAPPBOK_CommunicationBE.BookingID = BookingID;
            oAPPBOK_CommunicationBE.Subject = mailSubject;
            oAPPBOK_CommunicationBE.SentFrom = fromAddress;
            oAPPBOK_CommunicationBE.SentTo = toAddress;
            oAPPBOK_CommunicationBE.Body = htmlBody;
            oAPPBOK_CommunicationBE.SendByID = UserId;
            oAPPBOK_CommunicationBE.IsMailSent = IsMailSent;
            oAPPBOK_CommunicationBE.LanguageID = LanguageID;
            oAPPBOK_CommunicationBE.MailSentInLanguage = MailSentInLanguage;

            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            return oAPPBOK_CommunicationBAL.AddItemBAL(oAPPBOK_CommunicationBE);
        }

        public int? AddItemUnexpectedDelieveryBAL(int BookingID, string fromAddress, string toAddress, string mailSubject, string htmlBody, int UserId, CommunicationType.Enum communicationType, int LanguageID, bool MailSentInLanguage, bool IsMailSent = true)
        {
            // Resending of Delivery mails
            APPBOK_CommunicationBE oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
            oAPPBOK_CommunicationBE.Action = "AddBookingCommunicationUnexpectedDelievery";
            oAPPBOK_CommunicationBE.CommunicationType = CommunicationType.Name(communicationType);
            oAPPBOK_CommunicationBE.BookingID = BookingID;
            oAPPBOK_CommunicationBE.Subject = mailSubject;
            oAPPBOK_CommunicationBE.SentFrom = fromAddress;
            oAPPBOK_CommunicationBE.SentTo = toAddress;
            oAPPBOK_CommunicationBE.Body = htmlBody;
            oAPPBOK_CommunicationBE.SendByID = UserId;
            oAPPBOK_CommunicationBE.IsMailSent = IsMailSent;
            oAPPBOK_CommunicationBE.LanguageID = LanguageID;
            oAPPBOK_CommunicationBE.MailSentInLanguage = MailSentInLanguage;

            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            return oAPPBOK_CommunicationBAL.AddItemBAL(oAPPBOK_CommunicationBE);
        }

        public int? getCommunicationByIDBAL(int BookingID, CommunicationType.Enum communicationType)
        {
            APPBOK_CommunicationBE oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
            oAPPBOK_CommunicationBE.Action = "GetCommunicationsBookingID";
            oAPPBOK_CommunicationBE.CommunicationType = CommunicationType.Name(communicationType);
            oAPPBOK_CommunicationBE.BookingID = BookingID;
            //APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            //return oAPPBOK_CommunicationBAL.AddItemBAL(oAPPBOK_CommunicationBE);
            int? intResult = 0;
            try
            {
                APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
                intResult = oAPPBOK_CommunicationDAL.AddBookingCommunicationDAL(oAPPBOK_CommunicationBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MAS_LanguageBE> GetLanguages()
        {
            APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
            return oAPPBOK_CommunicationDAL.GetLanguages();
        }

        /// <summary>
        /// Used to add booking communications
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? AddItemBAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
                intResult = oAPPBOK_CommunicationDAL.AddItemDAL(oAPPBOK_CommunicationBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_CommunicationBE> GetItemsBAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            List<APPBOK_CommunicationBE> oAPPBOK_CommunicationBEList = new List<APPBOK_CommunicationBE>();

            try
            {
                APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
                oAPPBOK_CommunicationBEList = oAPPBOK_CommunicationDAL.GetItemsDAL(oAPPBOK_CommunicationBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_CommunicationBEList;
        }

        public APPBOK_CommunicationBE GetCommunicationDAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            try
            {
                APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
                oAPPBOK_CommunicationBE = oAPPBOK_CommunicationDAL.GetCommunicationDAL(oAPPBOK_CommunicationBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_CommunicationBE;
        }



        public APPBOK_CommunicationBE GetItemBAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            try
            {
                APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
                oAPPBOK_CommunicationBE = oAPPBOK_CommunicationDAL.GetItemDAL(oAPPBOK_CommunicationBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_CommunicationBE;
        }

        public int? AddBookingCommunicationBAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
                intResult = oAPPBOK_CommunicationDAL.AddBookingCommunicationDAL(oAPPBOK_CommunicationBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateChaseStatusBAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
                intResult = oAPPBOK_CommunicationDAL.UpdateChaseStatusDAL(oAPPBOK_CommunicationBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? AddDeletedItemBAL(int BookingID, string fromAddress, string toAddress, string mailSubject, string htmlBody, int UserId, CommunicationType.Enum communicationType, int LanguageID, bool MailSentInLanguage, bool IsMailSent = true)
        {
            // Resending of Delivery mails
            APPBOK_CommunicationBE oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
            oAPPBOK_CommunicationBE.Action = "AddDeletedBookingCommunication";
            oAPPBOK_CommunicationBE.CommunicationType = CommunicationType.Name(communicationType);
            oAPPBOK_CommunicationBE.BookingID = BookingID;
            oAPPBOK_CommunicationBE.Subject = mailSubject;
            oAPPBOK_CommunicationBE.SentFrom = fromAddress;
            oAPPBOK_CommunicationBE.SentTo = toAddress;
            oAPPBOK_CommunicationBE.Body = htmlBody;
            oAPPBOK_CommunicationBE.SendByID = UserId;
            oAPPBOK_CommunicationBE.IsMailSent = IsMailSent;
            oAPPBOK_CommunicationBE.LanguageID = LanguageID;
            oAPPBOK_CommunicationBE.MailSentInLanguage = MailSentInLanguage;

            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            return oAPPBOK_CommunicationBAL.AddItemBAL(oAPPBOK_CommunicationBE);
        }

        public int? GetPOChaseCommunicationIDBAL()
        {
            int? POChaseCommunicationId = 0;
            try
            {
                APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
                POChaseCommunicationId = oAPPBOK_CommunicationDAL.GetPOChaseCommunicationIDDAL();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return POChaseCommunicationId;
        }


        public List<APPBOK_CommunicationBE> GetChaseEmailDataBAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            List<APPBOK_CommunicationBE> oAPPBOK_CommunicationBEList = new List<APPBOK_CommunicationBE>();
            try
            {
                APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
                oAPPBOK_CommunicationBEList = oAPPBOK_CommunicationDAL.GetChaseEmailDataDAL(oAPPBOK_CommunicationBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_CommunicationBEList;
        }

    }
}

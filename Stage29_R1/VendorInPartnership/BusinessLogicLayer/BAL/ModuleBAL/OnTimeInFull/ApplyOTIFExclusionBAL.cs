﻿using BusinessEntities.ModuleBE.OnTimeInFull;
using DataAccessLayer.ModuleDAL.OnTimeInFull;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.OnTimeInFull
{
    public class ApplyOTIFExclusionBAL : BaseBAL
    {
        public int? addEditUP_ApplyOTIFExclusionBAL(ApplyOTIFExclusionBE applyOTIFExclusionBE)
        {
            int? intResult = 0;
            try
            {
                ApplyOTIFExclusionDAL applyOTIFExclusionDAL = new ApplyOTIFExclusionDAL();
                intResult = applyOTIFExclusionDAL.addEditUP_ApplyOTIFExclusionDAL(applyOTIFExclusionBE);
                applyOTIFExclusionDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<ApplyOTIFExclusionBE> GetApplySKUExclusionBAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            List<ApplyOTIFExclusionBE> oApplyOTIFExclusionBEList = new List<ApplyOTIFExclusionBE>();
            try
            {
                ApplyOTIFExclusionDAL applyOTIFExclusionDAL = new ApplyOTIFExclusionDAL();

                oApplyOTIFExclusionBEList = applyOTIFExclusionDAL.GetApplySKUExclusionDAL(oApplyOTIFExclusionBE);
                applyOTIFExclusionDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oApplyOTIFExclusionBEList;
        }

        public List<ApplyOTIFExclusionBE> GetSelectedApplySKUExclusionBAL(ApplyOTIFExclusionBE oOTIF_LeadTimeSKUBE)
        {
            List<ApplyOTIFExclusionBE> oApplyOTIFExclusionBEList = new List<ApplyOTIFExclusionBE>();
            try
            {
                ApplyOTIFExclusionDAL applyOTIFExclusionDAL = new ApplyOTIFExclusionDAL();

                oApplyOTIFExclusionBEList = applyOTIFExclusionDAL.GetSelectedApplySKUExclusionDAL(oOTIF_LeadTimeSKUBE);
                applyOTIFExclusionDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oApplyOTIFExclusionBEList;
        }

        public List<ApplyOTIFExclusionBE> GetSelectedApplySKUCodesBAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            List<ApplyOTIFExclusionBE> oApplyOTIFExclusionBEList = new List<ApplyOTIFExclusionBE>();
            try
            {
                ApplyOTIFExclusionDAL applyOTIFExclusionDAL = new ApplyOTIFExclusionDAL();

                oApplyOTIFExclusionBEList = applyOTIFExclusionDAL.GetSelectedApplySKUCodesDAL(oApplyOTIFExclusionBE);
                applyOTIFExclusionDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oApplyOTIFExclusionBEList;
        }

        public bool CheckApplyExclusionExistanceBAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            bool blnStatus = false;
            try
            {
                ApplyOTIFExclusionDAL applyOTIFExclusionDAL = new ApplyOTIFExclusionDAL();
                blnStatus = applyOTIFExclusionDAL.CheckApplyExclusionExistanceDAL(oApplyOTIFExclusionBE);
                applyOTIFExclusionDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return blnStatus;
        }

        public List<ApplyOTIFExclusionBE> GetExcludedPoBAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            List<ApplyOTIFExclusionBE> oApplyOTIFExclusionBEList = new List<ApplyOTIFExclusionBE>();
            try
            {
                ApplyOTIFExclusionDAL applyOTIFExclusionDAL = new ApplyOTIFExclusionDAL();

                oApplyOTIFExclusionBEList = applyOTIFExclusionDAL.GetExcludedPoDAL(oApplyOTIFExclusionBE);
                applyOTIFExclusionDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oApplyOTIFExclusionBEList;
        }

        public DataTable GetExcludedPoExportBAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            DataTable dt = new DataTable();
            try
            {
                ApplyOTIFExclusionDAL applyOTIFExclusionDAL = new ApplyOTIFExclusionDAL();

                dt = applyOTIFExclusionDAL.GetExcludedPoExportDAL(oApplyOTIFExclusionBE);
                applyOTIFExclusionDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataSet GetVendorExclusionBAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            DataSet dt = new DataSet(); ;
            try
            {
                ApplyOTIFExclusionDAL applyOTIFExclusionDAL = new ApplyOTIFExclusionDAL();

                dt = applyOTIFExclusionDAL.GetVendorExclusionDAL(oApplyOTIFExclusionBE);
                applyOTIFExclusionDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public int? DeleteApplyOtifHitDetailsBAL(ApplyOTIFExclusionBE oApplyOTIFExclusionBE)
        {
            int? intResult = 0;
            try
            {
                ApplyOTIFExclusionDAL oRevisedLeadTimeDAL = new ApplyOTIFExclusionDAL();
                intResult = oRevisedLeadTimeDAL.DeleteApplyOtifHitDetailsDAL(oApplyOTIFExclusionBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

﻿using BusinessEntities.ModuleBE.OnTimeInFull.Report;
using BusinessEntities.ModuleBE.Upload;
using DataAccessLayer.ModuleDAL.OnTimeInFull.Report;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.OnTimeInFull.Report
{
    public class OTIFReportBAL : BaseBAL
    {
        public DataSet GetOTIFReportBAL(OTIFReportBE oOTIFReportBE)
        {
            DataSet Result = null;
            try
            {
                OTIFReportDAL oOTIFReportDAL = new OTIFReportDAL();
                Result = oOTIFReportDAL.GetOTIFReportDAL(oOTIFReportBE);
                oOTIFReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public int? addEditUP_RevisedSKULeadTimeDetailBAL(OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE)
        {
            int? intResult = 0;
            try
            {
                OTIFReportDAL oRevisedLeadTimeDAL = new OTIFReportDAL();
                intResult = oRevisedLeadTimeDAL.addEditUP_RevisedSKULeadTimeDetailDAL(oOTIF_LeadTimeSKUBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<OTIF_LeadTimeSKUBE> GetRevisedSKULeadListBAL(OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE)
        {
            List<OTIF_LeadTimeSKUBE> oOTIF_LeadTimeSKUBEList = new List<OTIF_LeadTimeSKUBE>();

            try
            {
                OTIFReportDAL oRevisedLeadTimeDAL = new OTIFReportDAL();

                oOTIF_LeadTimeSKUBEList = oRevisedLeadTimeDAL.GetRevisedSKULeadListDAL(oOTIF_LeadTimeSKUBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oOTIF_LeadTimeSKUBEList;
        }

        public List<OTIF_LeadTimeSKUBE> GetSelectedPOLeadListSkuBAL(OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE)
        {
            List<OTIF_LeadTimeSKUBE> oOTIF_LeadTimeSKUBEList = new List<OTIF_LeadTimeSKUBE>();

            try
            {
                OTIFReportDAL oRevisedLeadTimeDAL = new OTIFReportDAL();

                oOTIF_LeadTimeSKUBEList = oRevisedLeadTimeDAL.GetSelectedPOLeadListSkuDAL(oOTIF_LeadTimeSKUBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oOTIF_LeadTimeSKUBEList;
        }

        public DataSet GetVendorNumberOfTimeAccessesedBAL(OTIFReportBE oOTIFReportBE)
        {
            DataSet Result = null;
            try
            {
                OTIFReportDAL oOTIFReportDAL = new OTIFReportDAL();
                Result = oOTIFReportDAL.GetVendorNumberOfTimeAccessesedDAL(oOTIFReportBE);
                oOTIFReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public int? UpdateOTIFVendorTrackingBAL(OTIFReportBE oOTIFReportBE)
        {
            int? intResult = 0;
            try
            {
                OTIFReportDAL oOTIFReportDAL = new OTIFReportDAL();
                intResult = oOTIFReportDAL.UpdateOTIFVendorTrackingDAL(oOTIFReportBE);
                oOTIFReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }


        // For Adding Vendor Exclusion
        public int? AddEditVendorExclusionBAL(OTIFReportBE oOTIFReportBE)
        {
            int? intResult = 0;
            try
            {
                OTIFReportDAL oOTIFReportDAL = new OTIFReportDAL();
                intResult = oOTIFReportDAL.AddEditVendorExclusionDAL(oOTIFReportBE);
                oOTIFReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        // For Deleting Vendor Exclusion
        public int? DeleteVendorExclusionBAL(OTIFReportBE oOTIFReportBE)
        {
            int? intResult = 0;
            try
            {
                OTIFReportDAL oOTIFReportDAL = new OTIFReportDAL();
                intResult = oOTIFReportDAL.DeleteVendorExclusionDAL(oOTIFReportBE);
                oOTIFReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        //For Getting List of Exclusions
        public List<OTIFReportBE> GetVendorExclusionBAL(OTIFReportBE oOTIFReportBE)
        {
            List<OTIFReportBE> lstVendorExclusion = new List<OTIFReportBE>();
            try
            {
                OTIFReportDAL oOTIFReportDAL = new OTIFReportDAL();
                lstVendorExclusion = oOTIFReportDAL.GetVendorExclusionDAL(oOTIFReportBE);
                oOTIFReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstVendorExclusion;
        }


        //For Getting List of  Excluded sites
        public List<OTIFReportBE> GetVendorExcludedSitesBAL(OTIFReportBE oOTIFReportBE)
        {
            List<OTIFReportBE> lstVendorExcludedSites = new List<OTIFReportBE>();
            try
            {
                OTIFReportDAL oOTIFReportDAL = new OTIFReportDAL();
                lstVendorExcludedSites = oOTIFReportDAL.GetVendorExcludedSitesDAL(oOTIFReportBE);
                oOTIFReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstVendorExcludedSites;
        }

        //For Getting List of  Excluded PO
        public List<OTIFReportBE> GetVendorExcludedPOBAL(OTIFReportBE oOTIFReportBE)
        {
            List<OTIFReportBE> lstVendorExcludedSites = new List<OTIFReportBE>();
            try
            {
                OTIFReportDAL oOTIFReportDAL = new OTIFReportDAL();
                lstVendorExcludedSites = oOTIFReportDAL.GetVendorExcludedPODAL(oOTIFReportBE);
                oOTIFReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstVendorExcludedSites;
        }

        //For Getting List of  Excluded Delivery Type
        public List<OTIFReportBE> GetVendorExcludedDeliveryTypeBAL(OTIFReportBE oOTIFReportBE)
        {
            List<OTIFReportBE> lstVendorExcludedDeliveryType = new List<OTIFReportBE>();
            try
            {
                OTIFReportDAL oOTIFReportDAL = new OTIFReportDAL();
                lstVendorExcludedDeliveryType = oOTIFReportDAL.GetVendorExcludedDeliveryTypeDAL(oOTIFReportBE);
                oOTIFReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstVendorExcludedDeliveryType;
        }


        //For Getting List of  Excluded Vendor
        public List<OTIFReportBE> GetExcludedVendorBAL(OTIFReportBE oOTIFReportBE)
        {
            List<OTIFReportBE> lstVendorExcluded = new List<OTIFReportBE>();
            try
            {
                OTIFReportDAL oOTIFReportDAL = new OTIFReportDAL();
                lstVendorExcluded = oOTIFReportDAL.GetExcludedVendorDAL(oOTIFReportBE);
                oOTIFReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstVendorExcluded;
        }

        public bool CheckForExclusionExistanceBAL(OTIFReportBE oOTIFReportBE)
        {
            bool IsExclusionExist = false;
            try
            {
                OTIFReportDAL oOTIFReportDAL = new OTIFReportDAL();
                IsExclusionExist = oOTIFReportDAL.CheckForExclusionExistanceDAL(oOTIFReportBE);
                oOTIFReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return IsExclusionExist;
        }

        public int? InsertVendorTrackingInfoBAL(OTIFReportBE oOTIFReportBE)
        {
            int? intResult = 0;
            try
            {
                OTIFReportDAL oOTIFReportDAL = new OTIFReportDAL();
                intResult = oOTIFReportDAL.InsertVendorTrackingInfoDAL(oOTIFReportBE);
                oOTIFReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public void ExcecuteOTIF_DetailReportSchedulerBAL()
        {

            try
            {
                OTIFReportDAL oOTIFReportDAL = new OTIFReportDAL();
                oOTIFReportDAL.ExcecuteOTIF_DetailReportSchedulerDAL();
                oOTIFReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }

        }

        public int? DeleteOtifHitDetailsBAL(OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE)
        {
            int? intResult = 0;
            try
            {
                OTIFReportDAL oRevisedLeadTimeDAL = new OTIFReportDAL();
                intResult = oRevisedLeadTimeDAL.DeleteOtifHitDetailsDAL(oOTIF_LeadTimeSKUBE);
                oRevisedLeadTimeDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

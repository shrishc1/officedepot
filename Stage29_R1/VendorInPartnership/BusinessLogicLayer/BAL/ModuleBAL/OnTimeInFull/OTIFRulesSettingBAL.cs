﻿using BusinessEntities.ModuleBE.OnTimeInFull;
using DataAccessLayer.ModuleDAL.OnTimeInFull;
using System;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.OnTimeInFull
{
    public class OTIFRulesSettingBAL
    {
        public int? AddOTIFRulesBAL(OTIFRulesSettingBE oOTIFRulesSettingBE)
        {
            int? result = null;
            try
            {
                OTIFRulesSettingDAL oOTIFRulesSettingDAL = new OTIFRulesSettingDAL();

                result = oOTIFRulesSettingDAL.AddOTIFRulesDAL(oOTIFRulesSettingBE);
                oOTIFRulesSettingDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return result;
        }

        public OTIFRulesSettingBE GetOTIFRules(OTIFRulesSettingBE oOTIFRulesSettingBE)
        {
            OTIFRulesSettingBE oNewOTIFRulesSettingBE = new OTIFRulesSettingBE();
            try
            {
                OTIFRulesSettingDAL oOTIFRulesSettingDAL = new OTIFRulesSettingDAL();

                oNewOTIFRulesSettingBE = oOTIFRulesSettingDAL.GetOTIFRules(oOTIFRulesSettingBE);
                oOTIFRulesSettingDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewOTIFRulesSettingBE;
        }

        public int? AddOTIFExclusionAutoBAL(OTIFRulesSettingBE oOTIFRulesSettingBE)
        {
            int? result = null;
            try
            {
                OTIFRulesSettingDAL oOTIFRulesSettingDAL = new OTIFRulesSettingDAL();

                result = oOTIFRulesSettingDAL.AddOTIFExclusionAutoDAL(oOTIFRulesSettingBE);
                oOTIFRulesSettingDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return result;
        }

        public OTIFRulesSettingBE GetOTIFExclusionAutoBAL(OTIFRulesSettingBE oOTIFRulesSettingBE)
        {
            OTIFRulesSettingBE oNewOTIFRulesSettingBE = new OTIFRulesSettingBE();
            try
            {
                OTIFRulesSettingDAL oOTIFRulesSettingDAL = new OTIFRulesSettingDAL();

                oNewOTIFRulesSettingBE = oOTIFRulesSettingDAL.GetOTIFExclusionAutoDAL(oOTIFRulesSettingBE);
                oOTIFRulesSettingDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewOTIFRulesSettingBE;
        }
    }
}

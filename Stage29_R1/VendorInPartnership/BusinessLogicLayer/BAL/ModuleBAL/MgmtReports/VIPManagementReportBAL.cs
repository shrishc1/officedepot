﻿using BusinessEntities.ModuleBE.MgmtReports;
using DataAccessLayer.ModuleDAL.MgmtReports;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.MgmtReports
{
    public class VIPManagementReportBAL : BaseBAL
    {

        public List<VIPManagementReportBE> GetVIPReportDetailsBAL(VIPManagementReportBE oVIPManagementReportBE)
        {
            List<VIPManagementReportBE> oVIPManagementReportBEBEList = new List<VIPManagementReportBE>();
            try
            {
                VIPManagementReportDAL oVIPManagementReportDAL = new VIPManagementReportDAL();
                oVIPManagementReportBEBEList = oVIPManagementReportDAL.GetVIPReportDetailsDAL(oVIPManagementReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oVIPManagementReportBEBEList;
        }

        public int? ChangePasswordBAL(VIPManagementReportBE oVIPManagementReportBE)
        {
            int? statusFlag = 0;
            try
            {
                VIPManagementReportDAL oVIPManagementReportDAL = new VIPManagementReportDAL();
                statusFlag = oVIPManagementReportDAL.ChangePasswordDAL(oVIPManagementReportBE);
                oVIPManagementReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return statusFlag;
        }

        public string GetPasswordBAL()
        {
            string password = string.Empty;
            try
            {
                VIPManagementReportDAL oVIPManagementReportDAL = new VIPManagementReportDAL();
                password = oVIPManagementReportDAL.GetPasswordDAL();
                oVIPManagementReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return password;
        }
    }
}

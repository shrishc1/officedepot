﻿using System;
using System.Collections.Generic;
using BusinessEntities;
using BusinessEntities.ModuleBE.Security;
using DataAccessLayer.ModuleDAL.Security;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Security
{
    public class SCT_AssignScreenBAL: BaseBe
    {
        public List<SCT_AssignScreenBE> FillRoleDropDown(int RoleID)
        {
            List<SCT_AssignScreenBE> lstRoleTemplate = new List<SCT_AssignScreenBE>();
            try
            {
                SCT_AssignScreenDAL oSCT_AssignScreenDAL = new SCT_AssignScreenDAL();
                lstRoleTemplate = oSCT_AssignScreenDAL.GetRoleTemplateDAL(RoleID);
            }
            catch (Exception ex)
            {
                LogUtility.SaveLogEntry(ex.Message + " |      Error Source:- " + ex.Source);
            }
            finally { }
            return lstRoleTemplate;
        }

        public SCT_AssignScreenBE GetTemplateInfoBAL(int TemplateID)
        {
            SCT_AssignScreenBE Template = new SCT_AssignScreenBE();
            try
            {
                SCT_AssignScreenDAL oSCT_AssignScreenDAL = new SCT_AssignScreenDAL();
                Template = oSCT_AssignScreenDAL.GetTemplateInfoDAL(TemplateID);
            }
            catch (Exception ex)
            {
                LogUtility.SaveLogEntry(ex.Message + " |      Error Source:- " + ex.Source);
            }
            finally { }
            return Template;
        }
    }
}

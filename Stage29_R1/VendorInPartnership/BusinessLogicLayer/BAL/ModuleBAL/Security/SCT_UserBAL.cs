﻿using BusinessEntities.ModuleBE.GlobalSettings;
using BusinessEntities.ModuleBE.Security;
using DataAccessLayer.ModuleDAL.Security;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Security
{
    public class SCT_UserBAL
    {

        public List<SCT_UserBE> GetUserDetailsBAL(SCT_UserBE oSCT_UserBE)
        {
            SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
            List<SCT_UserBE> oSCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                oSCT_UserBEList = oSCT_UserDAL.GetUserDetailsDAL(oSCT_UserBE);
                if (oSCT_UserBEList.Count > 0)
                    oNewSCT_UserBE = oSCT_UserBEList[0];
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            //return oNewSCT_UserBE;
            return oSCT_UserBEList;
        }
        public List<SCT_UserBE> GetUserBAL(SCT_UserBE oSCT_UserBE)
        {
            List<SCT_UserBE> oSCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                oSCT_UserBEList = oSCT_UserDAL.GetUserDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_UserBEList;
        }
        public List<SCT_UserBE> GetCarrierDetailsBAL(SCT_UserBE oSCT_UserBE)
        {
            List<SCT_UserBE> oSCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                oSCT_UserBEList = oSCT_UserDAL.GetCarrierDetailsDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_UserBEList;
        }

        /// <summary>
        /// Returns list of managers(user where userroleid=4)
        /// </summary>
        /// <param name="oSCT_UserBE"></param>
        /// <returns></returns>
        public List<SCT_UserBE> GetMangersBAL(SCT_UserBE oSCT_UserBE)
        {
            SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
            List<SCT_UserBE> oSCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                oSCT_UserBEList = oSCT_UserDAL.GetMangersDAL(oSCT_UserBE);
                if (oSCT_UserBEList.Count > 0)
                    oNewSCT_UserBE = oSCT_UserBEList[0];
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_UserBEList;
        }

        /// <summary>
        /// Returns list of OD managers(user where userroleid=9)
        /// </summary>
        /// <param name="oSCT_UserBE"></param>
        /// <returns></returns>
        public List<SCT_UserBE> GetODMangersBAL(SCT_UserBE oSCT_UserBE)
        {
            SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
            List<SCT_UserBE> oSCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                oSCT_UserBEList = oSCT_UserDAL.GetODMangersDAL(oSCT_UserBE);
                if (oSCT_UserBEList.Count > 0)
                    oNewSCT_UserBE = oSCT_UserBEList[0];
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_UserBEList;
        }



        public List<SCT_UserBE> GetPlannerBAL(SCT_UserBE oSCT_UserBE)
        {
            SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
            List<SCT_UserBE> oSCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                oSCT_UserBEList = oSCT_UserDAL.GetPlannerDAL(oSCT_UserBE);
                if (oSCT_UserBEList.Count > 0)
                    oNewSCT_UserBE = oSCT_UserBEList[0];
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_UserBEList;
        }


        public SCT_UserBE GetUserDefaultsBAL(SCT_UserBE oSCT_UserBE)
        {
            SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
            List<SCT_UserBE> oSCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                oSCT_UserBEList = oSCT_UserDAL.GetUserDefaultsDAL(oSCT_UserBE);
                if (oSCT_UserBEList.Count > 0)
                    oNewSCT_UserBE = oSCT_UserBEList[0];
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oNewSCT_UserBE;
        }

        public int? ChangePasswordBAL(SCT_UserBE oSCT_UserBE, string NewPassword)
        {
            int? statusFlag = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                statusFlag = oSCT_UserDAL.ChangePasswordDAL(oSCT_UserBE, NewPassword);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return statusFlag;
        }

        public DataTable GetUserSitesBAL(SCT_UserBE oSCT_UserBE)
        {
            //List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
            DataTable dt = new DataTable();
            SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
            try
            {
                dt = oSCT_UserDAL.GetUserSitesDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return dt;
        }

        public List<SCT_UserBE> GetUsersBAL(SCT_UserBE oSCT_UserBE)
        {
            List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
            SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
            try
            {
                lstUser = oSCT_UserDAL.GetUsersDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return lstUser;
        }

        public List<SCT_UserBE> GetUsers(SCT_UserBE oSCT_UserBE)
        {
            List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
            SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
            try
            {
                lstUser = oSCT_UserDAL.GetUsers(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return lstUser;
        }

        public int? addEditUserBAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                intResult = oSCT_UserDAL.addEditUserDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public int? UpdateUserBAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                intResult = oSCT_UserDAL.UpdateUserDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<SCT_UserBE> GetLanguagesBAL(SCT_UserBE oSCT_UserBE)
        {
            SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
            List<SCT_UserBE> oSCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                oSCT_UserBEList = oSCT_UserDAL.GetLanguagesDAL(oSCT_UserBE);
                if (oSCT_UserBEList.Count > 0)
                    oNewSCT_UserBE = oSCT_UserBEList[0];
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_UserBEList;
        }
        public List<SCT_UserBE> GetStockPlannerBAL(SCT_UserBE oSCT_UserBE)
        {
            SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
            List<SCT_UserBE> oSCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                oSCT_UserBEList = oSCT_UserDAL.GetStockPlannerDAL(oSCT_UserBE);
                if (oSCT_UserBEList.Count > 0)
                    oNewSCT_UserBE = oSCT_UserBEList[0];
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_UserBEList;
        }


        public int? UpdateAccountStatus(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                intResult = oSCT_UserDAL.UpdateAccountStatus(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateUserTemplatesBAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                intResult = oSCT_UserDAL.UpdateUserTemplatesDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? AddUserSitesBAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                intResult = oSCT_UserDAL.AddUserSitesDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public void UpdateCarrierActiveSites(SCT_UserBE oSCT_UserBE)
        {
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                oSCT_UserDAL.UpdateCarrierActiveSites(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }
        public DataTable GetCarrierActiveSitesBAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dtVendorActiveSites = null;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                dtVendorActiveSites = oSCT_UserDAL.GetCarrierActiveSitesDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dtVendorActiveSites;
        }

        public bool IsVendorSiteExist(SCT_UserBE oNewoSCT_UserBE)
        {
            bool VendorSiteExist = false;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                VendorSiteExist = oSCT_UserDAL.IsVendorSiteExist(oNewoSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return VendorSiteExist;
        }
        public bool IsCarrierSiteExist(SCT_UserBE oNewoSCT_UserBE)
        {
            bool CarrierSiteExist = false;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                CarrierSiteExist = oSCT_UserDAL.IsCarrierSiteExist(oNewoSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return CarrierSiteExist;
        }

        public DataTable GetAdminDashboardData(SCT_UserBE oSCT_UserBE)
        {
            DataTable lstDisc = new DataTable();

            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                lstDisc = oSCT_UserDAL.GetAdminDashboardData(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDisc;
        }

        public bool CheckForUserExistance(SCT_UserBE oSCT_UserBE)
        {
            bool IsUserExist = false;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                IsUserExist = oSCT_UserDAL.CheckForUserExistance(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return IsUserExist;
        }
        public DataSet GetLanguageIdBAL(SCT_UserBE oSCT_UserBE)
        {
            DataSet dsLanguage = new DataSet();

            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                dsLanguage = oSCT_UserDAL.GetLanguageIdDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dsLanguage;
        }


        public DataTable GetStockPlannerWithCountryBAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable lstDisc = new DataTable();

            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                lstDisc = oSCT_UserDAL.GetStockPlannerWithCountryDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDisc;
        }

        public DataTable GetStockPlannerWithCountryByUserIdsBAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable lstDisc = new DataTable();

            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                lstDisc = oSCT_UserDAL.GetStockPlannerWithCountryByUserIdsDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDisc;
        }

        public DataTable GetStockPlannerByCountryIdBAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable lstDisc = new DataTable();

            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                lstDisc = oSCT_UserDAL.GetStockPlannerByCountryIdDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDisc;
        }
        public List<SCT_UserBE> GetAPContactBAL(SCT_UserBE oSCT_UserBE)
        {
            List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
            SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
            try
            {
                lstUser = oSCT_UserDAL.GetAPContactDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return lstUser;
        }


        public List<SCT_UserBE> GetUserEmailByIDBAL(SCT_UserBE oSCT_UserBE)
        {
            List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
            SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
            try
            {
                lstUser = oSCT_UserDAL.GetUserEmailByIDDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return lstUser;
        }
        public DataTable GetValidlinksForUserBAL(SCT_UserBE oSCT_UserBE)
        {
            DataTable dt = new DataTable();

            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                dt = oSCT_UserDAL.GetValidlinksForUserDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        // Sprint 1 - Point 8 - Start
        public List<SCT_UserBE> GetManageDeliveryUsersBAL(SCT_UserBE oSCT_UserBE)
        {
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();

            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                SCT_UserBEList = oSCT_UserDAL.GetManageDeliveryUsersDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return SCT_UserBEList;
        }
        // Sprint 1 - Point 8 - End

        // Sprint 1 - Point 9 - Start
        public int? GetTodayRefusedDeliveriesCount(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                intResult = oSCT_UserDAL.GetTodayRefusedDeliveriesCount(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        // Sprint 1 - Point 9 - End

        public List<SCT_UserBE> GetCountrySpecificSettingsBAL(SCT_UserBE oSCT_UserBE)
        {
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();

            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                SCT_UserBEList = oSCT_UserDAL.GetCountrySpecificSettingsDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return SCT_UserBEList;
        }

        public int UpdateCountriesBAL(SCT_UserBE oSCT_UserBE)
        {
            int iResult = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                iResult = oSCT_UserDAL.UpdateCountriesDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return iResult;
        }

        public int UpdateModulesBAL(SCT_UserBE oSCT_UserBE)
        {
            int iResult = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                iResult = oSCT_UserDAL.UpdateModulesDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return iResult;
        }

        public int UpdateUserCountryBAL(SCT_UserBE oSCT_UserBE)
        {
            int iResult = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                iResult = oSCT_UserDAL.UpdateUserCountryDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return iResult;
        }

        public int? RegisterNewUserBAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                intResult = oSCT_UserDAL.RegisterNewUserDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<SCT_UserBE> GetAllUsersBAL(SCT_UserBE oSCT_UserBE)
        {
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();

            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                SCT_UserBEList = oSCT_UserDAL.GetAllUsersDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return SCT_UserBEList;
        }

        //IsValidEmailIdDAL
        public int? IsValidEmailIdBAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                intResult = oSCT_UserDAL.IsValidEmailIdDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteUserPendingRejectedDataBAL(SCT_UserBE oSCT_UserBE)
        {
            int? intResult = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                intResult = oSCT_UserDAL.DeleteUserPendingRejectedDataDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<SCT_UserBE> GetCommunicationUpdateVendorsBAL(SCT_UserBE oSCT_UserBE)
        {
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();

            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                SCT_UserBEList = oSCT_UserDAL.GetCommunicationUpdateVendorsDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return SCT_UserBEList;
        }

        public List<SCT_UserBE> GetCommunicationUpdateCarriersBAL(SCT_UserBE oSCT_UserBE)
        {
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();

            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                SCT_UserBEList = oSCT_UserDAL.GetCommunicationUpdateCarriersDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return SCT_UserBEList;
        }

        public List<SCT_UserBE> GetCommunicationUpdateOfficeDepotBAL(SCT_UserBE oSCT_UserBE)
        {
            List<SCT_UserBE> SCT_UserBEList = new List<SCT_UserBE>();

            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                SCT_UserBEList = oSCT_UserDAL.GetCommunicationUpdateOfficeDepotDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return SCT_UserBEList;
        }

        public List<SCT_UserBE> GetODMediatorBAL(SCT_UserBE oSCT_UserBE)
        {
            SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
            List<SCT_UserBE> oSCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                oSCT_UserBEList = oSCT_UserDAL.GetODMediatorDAL(oSCT_UserBE);
                if (oSCT_UserBEList.Count > 0)
                    oNewSCT_UserBE = oSCT_UserBEList[0];
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_UserBEList;
        }


        public List<SCT_UserBE> GetSPBySPNumberBAL(SCT_UserBE oSCT_UserBE)
        {
            SCT_UserBE oNewSCT_UserBE = new SCT_UserBE();
            List<SCT_UserBE> oSCT_UserBEList = new List<SCT_UserBE>();
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();

                oSCT_UserBEList = oSCT_UserDAL.GetSPBySPNumberDAL(oSCT_UserBE);
                if (oSCT_UserBEList.Count > 0)
                    oNewSCT_UserBE = oSCT_UserBEList[0];
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_UserBEList;
        }


        public DataSet GetSPNumberReportBAL(SCT_UserBE oSCT_UserBE)
        {
            DataSet ds = new DataSet();

            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                ds = oSCT_UserDAL.GetSPNumberReportDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public int? UpdateUsersStatusBAL(SCT_UserBE oSCT_UserBE)
        {
            int? result = 0;
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                result = oSCT_UserDAL.UpdateUsersStatusDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return result;
        }
        public List<SCT_UserBE> GetLastLoginUsersBAL(SCT_UserBE oSCT_UserBE)
        {
            List<SCT_UserBE> lstUser = new List<SCT_UserBE>();
            SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
            try
            {
                lstUser = oSCT_UserDAL.GetLastLoginUsersDAL(oSCT_UserBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return lstUser;
        }

        public DataSet GetPointOfContactBAL(PointsofContactSummaryBE oPointsofContactSummaryBE)
        {
            DataSet ds = new DataSet();
            try
            {
                SCT_UserDAL oSCT_UserDAL = new SCT_UserDAL();
                ds = oSCT_UserDAL.GetPointOfContactDAL(oPointsofContactSummaryBE);
                oSCT_UserDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }
    }
}

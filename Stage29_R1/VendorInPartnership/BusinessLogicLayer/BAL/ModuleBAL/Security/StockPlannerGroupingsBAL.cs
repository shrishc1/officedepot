﻿using BusinessEntities.ModuleBE.Security;
using DataAccessLayer.ModuleDAL.Security;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Security
{
    public class StockPlannerGroupingsBAL
    {

        public List<StockPlannerGroupingsBE> GetStockPlannerGroupingsBAL(StockPlannerGroupingsBE oStockPlannerGroupingsBE)
        {
            List<StockPlannerGroupingsBE> oStockPlannerGroupingsBEList = new List<StockPlannerGroupingsBE>();
            try
            {
                StockPlannerGroupingsDAL oStockPlannerGroupingsDAL = new StockPlannerGroupingsDAL();
                oStockPlannerGroupingsBEList = oStockPlannerGroupingsDAL.GetStockPlannerGroupingsDAL(oStockPlannerGroupingsBE);
                oStockPlannerGroupingsDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oStockPlannerGroupingsBEList;
        }

        public int? addEditStockPlannerGroupingsBAL(StockPlannerGroupingsBE StockPlannerGroupingsBE)
        {
            int? intResult = 0;
            try
            {
                StockPlannerGroupingsDAL oStockPlannerGroupingsDAL = new StockPlannerGroupingsDAL();
                intResult = oStockPlannerGroupingsDAL.addEditStockPlannerGroupingsDAL(StockPlannerGroupingsBE);
                oStockPlannerGroupingsDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public void ExpediteTrackerReportDetailsVendorBAL(StockPlannerGroupingsBE oStockPlannerGroupingsBE, out List<StockPlannerGroupingsBE> oStockPlannerGroupingsBEList, out int RecordCount)
        {
            try
            {
                StockPlannerGroupingsDAL oStockPlannerGroupingsDAL = new StockPlannerGroupingsDAL();
                oStockPlannerGroupingsDAL.ExpediteTrackerReportDetailsVendorDAL(oStockPlannerGroupingsBE, out oStockPlannerGroupingsBEList, out RecordCount);
            }
            catch (Exception ex)
            {
                oStockPlannerGroupingsBEList = null;
                RecordCount = 0;
                LogUtility.SaveErrorLogEntry(ex);
            }
            // return oScheduledReceiptedReportBEList;
        }

        public void ExpediteTrackerMonthlyReportDetailsBAL(StockPlannerGroupingsBE oStockPlannerGroupingsBE, out List<StockPlannerGroupingsBE> oStockPlannerGroupingsBEList, out int RecordCount, out DataTable DTexport)
        {
            try
            {
                StockPlannerGroupingsDAL oStockPlannerGroupingsDAL = new StockPlannerGroupingsDAL();
                oStockPlannerGroupingsDAL.ExpediteTrackerMonthlyReportDetailsDAL(oStockPlannerGroupingsBE, out oStockPlannerGroupingsBEList, out RecordCount, out DTexport);
            }
            catch (Exception ex)
            {
                oStockPlannerGroupingsBEList = null;
                RecordCount = 0;
                DTexport = null;
                LogUtility.SaveErrorLogEntry(ex);
            }
            // return oScheduledReceiptedReportBEList;
        }
        public void ExpediteTrackerDailyReportDetailsBAL(StockPlannerGroupingsBE oStockPlannerGroupingsBE, out List<StockPlannerGroupingsBE> oStockPlannerGroupingsBEList, out int RecordCount, out DataTable DTexport)
        {
            try
            {
                StockPlannerGroupingsDAL oStockPlannerGroupingsDAL = new StockPlannerGroupingsDAL();
                oStockPlannerGroupingsDAL.ExpediteTrackerDailyReportDetailsDAL(oStockPlannerGroupingsBE, out oStockPlannerGroupingsBEList, out RecordCount, out DTexport);
            }
            catch (Exception ex)
            {
                oStockPlannerGroupingsBEList = null;
                RecordCount = 0;
                DTexport = null;
                LogUtility.SaveErrorLogEntry(ex);
            }
            // return oScheduledReceiptedReportBEList;
        }

        public DataTable BindStockPlannerGroupingBAL(StockPlannerGroupingsBE oStockPlannerGroupingsBE)
        {
            DataTable dt = new DataTable();
            try
            {
                StockPlannerGroupingsDAL oStockPlannerGroupingsDAL = new StockPlannerGroupingsDAL();
                dt = oStockPlannerGroupingsDAL.BindStockPlannerGroupingDAL(oStockPlannerGroupingsBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return dt;
        }

    }
}

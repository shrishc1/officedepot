﻿using BusinessEntities.ModuleBE.Security;
using DataAccessLayer.ModuleDAL.Security;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Security
{
    public class SCT_TemplateBAL
    {

        public List<SCT_UserRoleBE> GetUserRolesBAL(SCT_UserRoleBE oSCT_UserRoleBE)
        {
            List<SCT_UserRoleBE> lstRoles = new List<SCT_UserRoleBE>();
            try
            {
                SCT_TemplateDAL oSCT_TemplateDAL = new SCT_TemplateDAL();
                lstRoles = oSCT_TemplateDAL.GetUserRolesDAL(oSCT_UserRoleBE);
                oSCT_TemplateDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstRoles;
        }

        public void SavePermissionToDB(SCT_TemplateBE oSCT_TemplateBE)
        {
            try
            {
                SCT_TemplateDAL oSCT_TemplateDAL = new SCT_TemplateDAL();
                oSCT_TemplateDAL.SavePermissionToDB(oSCT_TemplateBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public List<SCT_TemplateBE> GetTemplate(SCT_TemplateBE oSCT_TemplateBE)
        {
            List<SCT_TemplateBE> lstTemplate = new List<SCT_TemplateBE>();
            try
            {
                SCT_TemplateDAL oSCT_TemplateDAL = new SCT_TemplateDAL();
                lstTemplate = oSCT_TemplateDAL.GetTemplate(oSCT_TemplateBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstTemplate;
        }

        public SCT_TemplateBE GetTemplateInfo(SCT_TemplateBE oSCT_TemplateBE)
        {
            try
            {
                SCT_TemplateDAL oSCT_TemplateDAL = new SCT_TemplateDAL();
                oSCT_TemplateBE = oSCT_TemplateDAL.GetTemplateInfo(oSCT_TemplateBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_TemplateBE;
        }

        public List<SCT_TemplateBE> FillRoleDropDown(SCT_TemplateBE oSCT_TemplateBE)
        {
            List<SCT_TemplateBE> lstRoleTemplate = new List<SCT_TemplateBE>();
            try
            {
                SCT_TemplateDAL oSCT_TemplateDAL = new SCT_TemplateDAL();
                lstRoleTemplate = oSCT_TemplateDAL.GetRoleTemplateDAL(oSCT_TemplateBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstRoleTemplate;
        }

        //public SCT_TemplateBE GetTemplateInfoBAL(SCT_TemplateBE oSCT_TemplateBE)
        //{
        //    SCT_TemplateBE Template = new SCT_TemplateBE();
        //    try
        //    {
        //        SCT_TemplateDAL oSCT_TemplateDAL = new SCT_TemplateDAL();
        //        Template = oSCT_TemplateDAL.GetTemplateInfoDAL(oSCT_TemplateBE);
        //    }
        //    catch (Exception ex)
        //    {
        //        LogUtility.SaveLogEntry(ex);
        //    }
        //    finally { }
        //    return Template;
        //}

        public SCT_TemplateBE GetTemplateForReportLibBAL(SCT_TemplateBE oSCT_TemplateBE)
        {
            try
            {
                var oSCT_TemplateDAL = new SCT_TemplateDAL();
                oSCT_TemplateBE = oSCT_TemplateDAL.GetTemplateForReportLibDAL(oSCT_TemplateBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_TemplateBE;
        }

        public List<SCT_TemplateBE> GetVendorTemplate(SCT_TemplateBE oSCT_TemplateBE)
        {
            List<SCT_TemplateBE> lstTemplate = new List<SCT_TemplateBE>();
            try
            {
                SCT_TemplateDAL oSCT_TemplateDAL = new SCT_TemplateDAL();
                lstTemplate = oSCT_TemplateDAL.GetVendorTemplate(oSCT_TemplateBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstTemplate;
        }

        public DataSet SaveVendorTemplate(SCT_TemplateBE oSCT_TemplateBE)
        {
            DataSet ds = new DataSet();
            try
            {
                SCT_TemplateDAL oSCT_TemplateDAL = new SCT_TemplateDAL();
                ds = oSCT_TemplateDAL.SaveVendorTemplate(oSCT_TemplateBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;

        }
        public List<SCT_TemplateBE> GetVendorTemplateById(SCT_TemplateBE oSCT_TemplateBE)
        {
            List<SCT_TemplateBE> lstTemplate = new List<SCT_TemplateBE>();
            try
            {
                SCT_TemplateDAL oSCT_TemplateDAL = new SCT_TemplateDAL();
                lstTemplate = oSCT_TemplateDAL.GetVendorTemplateById(oSCT_TemplateBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstTemplate;
        }

        public int? DeleteTemplateByIdBAL(SCT_TemplateBE oSCT_TemplateBE)
        {
            int? result = 0;
            try
            {
                SCT_TemplateDAL oSCT_TemplateDAL = new SCT_TemplateDAL();
                result = oSCT_TemplateDAL.DeleteTemplateByIdDAL(oSCT_TemplateBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return result;
        }

        public DataSet GetVendorTemplateByIdForExportBAL(SCT_TemplateBE oSCT_TemplateBE)
        {
            DataSet ds = new DataSet();
            try
            {
                SCT_TemplateDAL oSCT_TemplateDAL = new SCT_TemplateDAL();
                ds = oSCT_TemplateDAL.GetVendorTemplateByIdForExportDAL(oSCT_TemplateBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;

        }

        public int? DeleteVendorTemplateByIdBAL(SCT_TemplateBE oSCT_TemplateBE)
        {
            int? result = 0;
            try
            {
                SCT_TemplateDAL oSCT_TemplateDAL = new SCT_TemplateDAL();
                result = oSCT_TemplateDAL.DeleteVendorTemplateByIdDAL(oSCT_TemplateBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return result;
        }

    }
}

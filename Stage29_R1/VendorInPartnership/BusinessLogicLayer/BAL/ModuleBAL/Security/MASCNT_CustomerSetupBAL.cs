﻿using BusinessEntities.ModuleBE.Security;
using DataAccessLayer.ModuleDAL.Security;
using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Security
{
    public class MASCNT_CustomerSetupBAL
    {
        public List<MASCNT_CustomerSetupBE> GetCustomersBAL(MASCNT_CustomerSetupBE mASCNT_CustomerSetupBE)
        {
            List<MASCNT_CustomerSetupBE> mASCNT_CustomerSetupBEs = new List<MASCNT_CustomerSetupBE>();
            try
            {
                MASCNT_CustomerSetupDAL mASCNT_CustomerSetupDAL = new MASCNT_CustomerSetupDAL();
                mASCNT_CustomerSetupBEs = mASCNT_CustomerSetupDAL.GetCustomersDAL(mASCNT_CustomerSetupBE);
                mASCNT_CustomerSetupDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return mASCNT_CustomerSetupBEs;
        }

        public List<MASCNT_CustomerSetupBE> SearchCustomersBAL(MASCNT_CustomerSetupBE mASCNT_CustomerSetupBE)
        {
            List<MASCNT_CustomerSetupBE> mASCNT_CustomerSetupBEs = new List<MASCNT_CustomerSetupBE>();
            try
            {
                MASCNT_CustomerSetupDAL mASCNT_CustomerSetupDAL = new MASCNT_CustomerSetupDAL();
                mASCNT_CustomerSetupBEs = mASCNT_CustomerSetupDAL.SearchCustomersDAL(mASCNT_CustomerSetupBE);
                mASCNT_CustomerSetupDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return mASCNT_CustomerSetupBEs;
        }

        public List<MASCNT_CustomerSetupBE> GetCustomersBySiteBAL(string Action)
        {
            List<MASCNT_CustomerSetupBE> mASCNT_CustomerSetupBEs = new List<MASCNT_CustomerSetupBE>();
            try
            {
                MASCNT_CustomerSetupDAL mASCNT_CustomerSetupDAL = new MASCNT_CustomerSetupDAL();
                mASCNT_CustomerSetupBEs = mASCNT_CustomerSetupDAL.GetCustomersBySiteDAL(Action);
                mASCNT_CustomerSetupDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return mASCNT_CustomerSetupBEs;
        }

        public int? addEditCustomerDetailsBAL(MASCNT_CustomerSetupBE mASCNT_CustomerSetupBE)
        {
            int? intResult = 0;
            try
            {
                MASCNT_CustomerSetupDAL mASCNT_CustomerSetupDAL = new MASCNT_CustomerSetupDAL();
                intResult = mASCNT_CustomerSetupDAL.addEditCustomerDetailsDAL(mASCNT_CustomerSetupBE);
                mASCNT_CustomerSetupDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataTable GetDefaultStockPlannerReportBAL(DefaultStockPlannerBE defaultStockPlannerBE)
        {
            DataTable mASCNT_CustomerSetupBEs = new DataTable();
            try
            {
                MASCNT_CustomerSetupDAL mASCNT_CustomerSetupDAL = new MASCNT_CustomerSetupDAL();
                mASCNT_CustomerSetupBEs = mASCNT_CustomerSetupDAL.GetDefaultStockPlannerReportDAL(defaultStockPlannerBE);
                mASCNT_CustomerSetupDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return mASCNT_CustomerSetupBEs;
        }

        public List<StockPlannerWithSiteBE> GetStockPlannerWithSiteBAL(string Action)
        {
            List<StockPlannerWithSiteBE> stockPlannerWithSites = new List<StockPlannerWithSiteBE>();
            try
            {
                MASCNT_CustomerSetupDAL mASCNT_CustomerSetupDAL = new MASCNT_CustomerSetupDAL();
                stockPlannerWithSites = mASCNT_CustomerSetupDAL.GetStockPlannerWithSiteDAL(Action);
                mASCNT_CustomerSetupDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return stockPlannerWithSites;
        }

        public int? SaveDefaultStockPlannerBAL(DataTable spData)
        {
            int? intResult = 0;
            try
            {
                MASCNT_CustomerSetupDAL mASCNT_CustomerSetupDAL = new MASCNT_CustomerSetupDAL();
                intResult = mASCNT_CustomerSetupDAL.SaveDefaultStockPlannerDAL(spData);
                mASCNT_CustomerSetupDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

﻿using BusinessEntities.ModuleBE.Security;
using DataAccessLayer.ModuleDAL.Security;
using System;
using System.Collections.Generic;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Security
{
    public class SCT_ScreenBAL
    {

        public List<SCT_ScreenBE> GetScreenBAL(SCT_ScreenBE oSCT_ScreenBE)
        {
            List<SCT_ScreenBE> oSCT_ScreenBEList = new List<SCT_ScreenBE>();
            try
            {
                SCT_ScreenDAL oSCT_ScreenDAL = new SCT_ScreenDAL();
                oSCT_ScreenBEList = oSCT_ScreenDAL.GetScreenDAL(oSCT_ScreenBE);
                oSCT_ScreenDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_ScreenBEList;
        }
    }
}

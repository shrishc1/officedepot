﻿using BusinessEntities.ModuleBE.VendorScorecard;
using DataAccessLayer.ModuleDAL.VendorScorecard;
using System;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.VendorScorecard
{
    public class SummaryScoreCardBAL : BaseBAL
    {
        public DataSet GetSummaryScoreCardBAL(SummaryScoreCardBE objSummaryScoreCardBE)
        {
            DataSet Result = null;
            try
            {
                var summaryScoreCardDAL = new SummaryScoreCardDAL();
                Result = summaryScoreCardDAL.GetSummaryScoreCardDAL(objSummaryScoreCardBE);
                summaryScoreCardDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public int? GetCurrentYearBAL(SummaryScoreCardBE summaryScoreCardBE)
        {
            int? intResult = 0;
            try
            {
                var summaryScoreCardDAL = new SummaryScoreCardDAL();
                intResult = summaryScoreCardDAL.GetCurrentYearDAL(summaryScoreCardBE);
                summaryScoreCardDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Reporting.WinForms;
using Utilities;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.Report;
using BusinessEntities.ModuleBE.OnTimeInFull.Report;
using System.Data;
using System.IO;
namespace VIP_ReportingQueueService.ModuleUI.OnTimeInFull
{
    public class RMSCategoryProcessQueue
    {
        public void GenerateRmsCategoryReport(ReportRequestBE pReportRequestBE)
        {
            try
            {
                RMSCategoryBE objRMSCategoryBE = new RMSCategoryBE();
                OTIFReportBAL objReportBAL = new OTIFReportBAL();
                DataSet dsRMSCategoryReport;
                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                path = path.Replace(@"\bin\debug", "");

                objRMSCategoryBE.Action = string.IsNullOrEmpty(pReportRequestBE.ReportAction) ? null : pReportRequestBE.ReportAction;
                objRMSCategoryBE.DateFrom = pReportRequestBE.DateFrom;
                objRMSCategoryBE.DateTo = pReportRequestBE.DateTo;
                objRMSCategoryBE.SelectedCountryIDs = string.IsNullOrEmpty(pReportRequestBE.CountryIDs) ? null : pReportRequestBE.CountryIDs;
                objRMSCategoryBE.SelectedSiteIDs = string.IsNullOrEmpty(pReportRequestBE.SiteIDs) ? null : pReportRequestBE.SiteIDs;
                objRMSCategoryBE.SelectedStockPlannerIDs = string.IsNullOrEmpty(pReportRequestBE.StockPlannerIDs) ? null : pReportRequestBE.StockPlannerIDs;
                objRMSCategoryBE.SelectedVendorIDs = string.IsNullOrEmpty(pReportRequestBE.VendorIDs) ? null : pReportRequestBE.VendorIDs;
                objRMSCategoryBE.SelectedItemClassification = string.IsNullOrEmpty(pReportRequestBE.ItemClassification) ? null : pReportRequestBE.ItemClassification;
                objRMSCategoryBE.SelectedRMSCategoryIDs = string.IsNullOrEmpty(pReportRequestBE.SelectedRMSCategoryIDs) ? null : pReportRequestBE.SelectedRMSCategoryIDs;
                objRMSCategoryBE.PurchaseOrderDue = pReportRequestBE.PurchaseOrderDue;
                objRMSCategoryBE.PurchaseOrderReceived = pReportRequestBE.PurchaseOrderReceived;
                objRMSCategoryBE.ReportType = pReportRequestBE.ReportType;
                dsRMSCategoryReport = objReportBAL.GetRMSCategoryReportBAL(objRMSCategoryBE);
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("\r\n");
                if (dsRMSCategoryReport == null)
                    sbMessage.Append("Record count         : null");
                else
                    sbMessage.Append("Record count          : " + dsRMSCategoryReport.Tables[0].Rows.Count.ToString());
                LogUtility.SaveTraceLogEntry(sbMessage);

                // Prepare RDLC
                string ReportOutputPath;
                ReportDataSource rds;
                ReportViewer ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
                ReportParameter[] reportParameter = null;
                DateTime dt;
                string sDateFrom = string.Empty;
                string sDateTo = string.Empty;
                string requestTime = string.Empty;
                #region "string Date"
                if (pReportRequestBE.DateFrom != null)
                {
                    if (pReportRequestBE.DateFrom.Value.Day.ToString().Length == 1)
                        sDateFrom = sDateFrom + "0" + pReportRequestBE.DateFrom.Value.Day.ToString();
                    else
                        sDateFrom = sDateFrom + pReportRequestBE.DateFrom.Value.Day.ToString();

                    if (pReportRequestBE.DateFrom.Value.Month.ToString().Length == 1)
                        sDateFrom = sDateFrom + "/" + "0" + pReportRequestBE.DateFrom.Value.Month.ToString();
                    else
                        sDateFrom = sDateFrom + "/" + pReportRequestBE.DateFrom.Value.Month.ToString();

                    sDateFrom = sDateFrom + "/" + pReportRequestBE.DateFrom.Value.Year.ToString();
                }

                if (pReportRequestBE.DateTo != null)
                {
                    if (pReportRequestBE.DateTo.Value.Date.ToString().Length == 1)
                        sDateTo = sDateTo + "0" + pReportRequestBE.DateTo.Value.Day.ToString();
                    else
                        sDateTo = sDateTo + pReportRequestBE.DateTo.Value.Day.ToString();

                    if (pReportRequestBE.DateTo.Value.Month.ToString().Length == 1)
                        sDateTo = sDateTo + "/" + "0" + pReportRequestBE.DateTo.Value.Month.ToString();
                    else
                        sDateTo = sDateTo + "/" + pReportRequestBE.DateTo.Value.Month.ToString();

                    sDateTo = sDateTo + "/" + pReportRequestBE.DateTo.Value.Year.ToString();
                }
              #endregion

                #region ReportVariables

                Warning[] warnings;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = string.Empty;
                string reportFileName = string.Empty;
                string TitleType = string.Empty;
                #endregion

                switch (pReportRequestBE.ReportType)
                {
                    case "Summary":
                        reportParameter = new ReportParameter[4];
                        reportParameter[0] = new ReportParameter("Country", pReportRequestBE.CountryName);
                        reportParameter[1] = new ReportParameter("DateFrom", sDateFrom);
                        reportParameter[2] = new ReportParameter("DateTo", sDateTo);
                        if (objRMSCategoryBE.PurchaseOrderDue == 1)
                            TitleType = "Purchase Order Due";

                        if (objRMSCategoryBE.PurchaseOrderReceived == 1)
                            TitleType = "Purchase Order Received";
                         reportParameter[3] = new ReportParameter("Title", "Office Depot Category Summary Report -" + " (" + TitleType + ") ");

                        //reportParameter[10] = new ReportParameter("RMSCategoryIDs", pReportRequestBE.SelectedRMSCategoryIDs);

                        break;
                    case "Monthly":
                        reportParameter = new ReportParameter[4];
                        reportParameter[0] = new ReportParameter("Site", pReportRequestBE.SiteName);
                        reportParameter[1] = new ReportParameter("DateFrom", sDateFrom);
                        reportParameter[2] = new ReportParameter("DateTo", sDateTo);
                        TitleType = string.Empty;
                        if (objRMSCategoryBE.PurchaseOrderDue == 1)
                            TitleType = "Purchase Order Due";

                        if (objRMSCategoryBE.PurchaseOrderReceived == 1)
                            TitleType = "Purchase Order Received";
                            reportParameter[3] = new ReportParameter("Title", "Office Depot Category Monthly Report -" + " (" + TitleType + ") ");
                        break;
                   
                }

                rds = new ReportDataSource(pReportRequestBE.ReportDatatableName, dsRMSCategoryReport.Tables[0]);
                ReportViewer1.LocalReport.ReportPath = path + @"ModuleUI\OnTimeInFull\RDLC\" + pReportRequestBE.ReportNamePath;
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
                ReportViewer1.LocalReport.SetParameters(reportParameter);

                byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                //ReportViewer1.LocalReport.SetParameters(reportParameter);

                ReportOutputPath = path + @"ReportOutput\";
                if (!System.IO.Directory.Exists(ReportOutputPath))
                    System.IO.Directory.CreateDirectory(ReportOutputPath);

                dt = Convert.ToDateTime(pReportRequestBE.RequestTime);

                requestTime = "";
                if (dt.Day.ToString().Length == 1)
                    requestTime = requestTime + "0" + dt.Day.ToString();
                else
                    requestTime = requestTime + dt.Day.ToString();

                if (dt.Month.ToString().Length == 1)
                    requestTime = requestTime + "0" + dt.Month.ToString();
                else
                    requestTime = requestTime + dt.Month.ToString();

                requestTime = requestTime + dt.Year.ToString();

                requestTime = requestTime + "_" + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString();

                ReportOutputPath = ReportOutputPath + pReportRequestBE.ReportRequestID.ToString() + "_";
                ReportOutputPath = ReportOutputPath + pReportRequestBE.ReportName + "_";
                if (string.IsNullOrEmpty(pReportRequestBE.ReportType))
                    ReportOutputPath = ReportOutputPath + pReportRequestBE.UserID + "_" + requestTime + ".xls";
                else
                    ReportOutputPath = ReportOutputPath + pReportRequestBE.ReportType + "_" + pReportRequestBE.UserID + "_" + requestTime + ".xls";

                File.WriteAllBytes(ReportOutputPath, bytes);

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Report Request Id " + pReportRequestBE.ReportRequestID.ToString() + " Successfully Generated");
                sbMessage.Append("\r\n");

                LogUtility.SaveTraceLogEntry(sbMessage);  
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using System;
using System.Text;
using System.Data;
using Microsoft.Reporting.WinForms;
using System.IO;
using BusinessEntities.ModuleBE.OnTimeInFull.Report;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.Report;
using BusinessEntities.ModuleBE.ReportRequest;
using Utilities;
using Aspose.Cells;

namespace VIP_ReportingQueueService
{

    public class OTIFProcessQueue
    {
        public void GenerateOTIFReport(ReportRequestBE pReportRequestBE)
        {

            try
            {
                OTIFReportBE oOTIFReportBE = new OTIFReportBE();
                OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();
                DataSet dsOTIFReport;

                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                path = path.Replace(@"\bin\debug", "");

                //if (pReportRequestBE.ReportAction != "OTIF_ODLineLevelMonthlyReportNewMesaure")
                //{
                //Get Report Data
                oOTIFReportBE.Action = string.IsNullOrEmpty(pReportRequestBE.ReportAction) ? null : pReportRequestBE.ReportAction;
                oOTIFReportBE.DateFrom = pReportRequestBE.DateFrom;
                oOTIFReportBE.DateTo = pReportRequestBE.DateTo;
                oOTIFReportBE.PurchaseOrderDue = pReportRequestBE.PurchaseOrderDue;
                oOTIFReportBE.PurchaseOrderReceived = pReportRequestBE.PurchaseOrderReceived;
                oOTIFReportBE.SelectedCountryIDs = string.IsNullOrEmpty(pReportRequestBE.CountryIDs) ? null : pReportRequestBE.CountryIDs;
                oOTIFReportBE.SelectedSiteIDs = string.IsNullOrEmpty(pReportRequestBE.SiteIDs) ? null : pReportRequestBE.SiteIDs;
                oOTIFReportBE.SelectedStockPlannerIDs = string.IsNullOrEmpty(pReportRequestBE.StockPlannerIDs) ? null : pReportRequestBE.StockPlannerIDs;
                oOTIFReportBE.SelectedVendorIDs = string.IsNullOrEmpty(pReportRequestBE.VendorIDs) ? null : pReportRequestBE.VendorIDs;
                oOTIFReportBE.SelectedSKUCodes = string.IsNullOrEmpty(pReportRequestBE.OD_SKU_No) ? null : pReportRequestBE.OD_SKU_No;
                oOTIFReportBE.SelectedPurchaseOrderNo = string.IsNullOrEmpty(pReportRequestBE.PurchaseOrder) ? null : pReportRequestBE.PurchaseOrder;
                oOTIFReportBE.SelectedItemClassification = string.IsNullOrEmpty(pReportRequestBE.ItemClassification) ? null : pReportRequestBE.ItemClassification;
                oOTIFReportBE.EuropeanorLocal = pReportRequestBE.EuropeanorLocal == null ? (int?)null : pReportRequestBE.EuropeanorLocal;
                oOTIFReportBE.SelectedRMSCategoryIDs = string.IsNullOrEmpty(pReportRequestBE.SelectedRMSCategoryIDs) ? null : pReportRequestBE.SelectedRMSCategoryIDs;
                oOTIFReportBE.SelectedSKuGroupingIDs = string.IsNullOrEmpty(pReportRequestBE.SelectedSKuGroupingIDs) ? null : pReportRequestBE.SelectedSKuGroupingIDs;
                oOTIFReportBE.StockPlannerGroupingIDs = string.IsNullOrEmpty(pReportRequestBE.StockPlannerGroupingIDs) ? null : pReportRequestBE.StockPlannerGroupingIDs;
                oOTIFReportBE.CustomerIDs = string.IsNullOrEmpty(pReportRequestBE.CustomerIDs) ? null : pReportRequestBE.CustomerIDs;

                dsOTIFReport = oOTIFReportBAL.GetOTIFReportBAL(oOTIFReportBE);
                oOTIFReportBAL = null;
              
                StringBuilder sbMessage = new StringBuilder();

                sbMessage.Append("\r\n");

                if (dsOTIFReport == null)
                {
                    sbMessage.Append("Record count         : null");
                }
                else
                {
                    sbMessage.Append("Record count          : " + dsOTIFReport.Tables[0].Rows.Count.ToString());
                }

                LogUtility.SaveTraceLogEntry(sbMessage);

                ExportExcelWithAspose(dsOTIFReport.Tables[0], "E:\\OtifDetalis.xlsx");

                #region Commnts              
                /*
               if (dsOTIFReport != null && dsOTIFReport.Tables.Count > 0 && dsOTIFReport.Tables[0].Rows.Count> 0)
               {
                   // Prepare RDLC
                   string ReportOutputPath;
                   ReportDataSource rds;
                   ReportViewer ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
                   ReportParameter[] reportParameter = null;
                   DateTime dt;
                   string sDateFrom = string.Empty;
                   string sDateTo = string.Empty;
                   string requestTime = string.Empty;

                   #region "string Date"

                   sbMessage.Append("\r\n");

                   if (pReportRequestBE.DateFrom != null)
                   {
                       if (pReportRequestBE.DateFrom.Value.Day.ToString().Length == 1)
                           sDateFrom = sDateFrom + "0" + pReportRequestBE.DateFrom.Value.Day.ToString();
                       else
                           sDateFrom = sDateFrom + pReportRequestBE.DateFrom.Value.Day.ToString();

                       if (pReportRequestBE.DateFrom.Value.Month.ToString().Length == 1)
                           sDateFrom = sDateFrom + "/" + "0" + pReportRequestBE.DateFrom.Value.Month.ToString();
                       else
                           sDateFrom = sDateFrom + "/" + pReportRequestBE.DateFrom.Value.Month.ToString();

                       sDateFrom = sDateFrom + "/" + pReportRequestBE.DateFrom.Value.Year.ToString();

                       sbMessage.AppendLine("sDateFrom: " + sDateFrom);
                   }


                   if (pReportRequestBE.DateTo != null)
                   {
                       if (pReportRequestBE.DateTo.Value.Date.ToString().Length == 1)
                           sDateTo = sDateTo + "0" + pReportRequestBE.DateTo.Value.Day.ToString();
                       else
                           sDateTo = sDateTo + pReportRequestBE.DateTo.Value.Day.ToString();

                       if (pReportRequestBE.DateTo.Value.Month.ToString().Length == 1)
                           sDateTo = sDateTo + "/" + "0" + pReportRequestBE.DateTo.Value.Month.ToString();
                       else
                           sDateTo = sDateTo + "/" + pReportRequestBE.DateTo.Value.Month.ToString();

                       sDateTo = sDateTo + "/" + pReportRequestBE.DateTo.Value.Year.ToString();

                       sbMessage.AppendLine("sDateTo: " + sDateTo);
                   }
                   #endregion

                   #region ReportVariables

                   Warning[] warnings;
                   string[] streamIds;
                   string mimeType = string.Empty;
                   string encoding = string.Empty;
                   string extension = string.Empty;
                   string reportFileName = string.Empty;

                   #endregion

                   ReportViewer1.ProcessingMode =   Microsoft.Reporting.WinForms.ProcessingMode.Local;

                   switch (pReportRequestBE.ReportName)
                   {

                       case "OTIF Report":
                           sbMessage.AppendLine("OTIF Report");
                           reportParameter = new ReportParameter[10];

                           sbMessage.AppendLine("reportParameter[0]: Country" + pReportRequestBE.CountryName);
                           reportParameter[0] = new ReportParameter("Country", pReportRequestBE.CountryName);

                           sbMessage.AppendLine("reportParameter[1]: " + "Site" + pReportRequestBE.SiteName);
                           reportParameter[1] = new ReportParameter("Site", pReportRequestBE.SiteName);

                           sbMessage.AppendLine("reportParameter[2]: " + "Vendor" + pReportRequestBE.VendorName);
                           reportParameter[2] = new ReportParameter("Vendor", pReportRequestBE.VendorName);

                           sbMessage.AppendLine("reportParameter[3]: " + "DateFrom" + sDateFrom);
                           reportParameter[3] = new ReportParameter("DateFrom", sDateFrom);

                           sbMessage.AppendLine("reportParameter[4]: " + "DateTo" + sDateTo);
                           reportParameter[4] = new ReportParameter("DateTo", sDateTo);

                           sbMessage.AppendLine("reportParameter[5]: " + "ItemClassification" + pReportRequestBE.ItemClassification);
                           reportParameter[5] = new ReportParameter("ItemClassification", pReportRequestBE.ItemClassification);

                           sbMessage.AppendLine("reportParameter[6]:" + "ODSKUCode" + pReportRequestBE.OD_SKU_No);
                           reportParameter[6] = new ReportParameter("ODSKUCode", pReportRequestBE.OD_SKU_No);

                           sbMessage.AppendLine("reportParameter[7]: " + "PurchaseOrder" + pReportRequestBE.PurchaseOrder);
                           reportParameter[7] = new ReportParameter("PurchaseOrder", pReportRequestBE.PurchaseOrder);

                           sbMessage.AppendLine("reportParameter[8]: " + "StockPlanner" + pReportRequestBE.StockPlannerName);
                           reportParameter[8] = new ReportParameter("StockPlanner", pReportRequestBE.StockPlannerName);

                           string TitleType = string.Empty;

                           if (oOTIFReportBE.PurchaseOrderDue == 1)
                               TitleType = "Purchase Order Due";

                           if (oOTIFReportBE.PurchaseOrderReceived == 1)
                               TitleType = "Purchase Order Received";

                           if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelMonthlyReport")
                           {
                               sbMessage.AppendLine("reportParameter[9]: " + "Title" + " Office Depot Line Level Monthly Report - Old OTIF Measure" + " (" + TitleType + ") ");
                               reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Monthly Report - Old OTIF Measure" + " (" + TitleType + ") ");                       
                           }
                           else if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelMonthlyReportNewMesaure")
                           {
                               sbMessage.AppendLine("reportParameter[9]: " + "Title" + " Office Depot Line Level Monthly Report - New OTIF Measure" + " (" + TitleType + ") ");
                               reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Monthly Report - New OTIF Measure" + " (" + TitleType + ") ");
                           }
                           else if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelMonthlyReportAbsolute")
                           {
                               sbMessage.AppendLine("reportParameter[9]:" + "Title" + " Office Depot Line Level Monthly Report - Absolute OTIF Measure" + " (" + TitleType + ") ");
                               reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Monthly Report - Absolute OTIF Measure" + " (" + TitleType + ") ");
                           }
                           else if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelSummaryReport")
                           {
                               sbMessage.AppendLine("reportParameter[9]:" + "Title" + " Office Depot Line Level Summary Report - Old OTIF Summary" + " (" + TitleType + ") ");
                               reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Summary Report - Old OTIF Summary" + " (" + TitleType + ") ");
                           }
                           else if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelSummaryReportNew")
                           {
                               sbMessage.AppendLine("reportParameter[9]:"+ "Title" + " Office Depot Line Level Summary Report - New OTIF Summary" + " (" + TitleType + ") ");
                               reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Summary Report - New OTIF Summary" + " (" + TitleType + ") ");
                           }
                           else if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelSummaryReportAbsolute")
                           {
                               sbMessage.AppendLine("reportParameter[9]: " + "Title" + " Office Depot Line Level Summary Report - Absolute OTIF Summary" + " (" + TitleType + ") ");
                               reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Summary Report - Absolute OTIF Summary" + " (" + TitleType + ") ");
                           }
                           else if (pReportRequestBE.ReportAction == "OTIF_VendorLineLevelDetailedReport")
                           {
                               sbMessage.AppendLine("reportParameter[9]:" + "Title" + " Vendor Line Level Detailed Report" + " (" + TitleType + ") ");
                               reportParameter[9] = new ReportParameter("Title", " Vendor Line Level Detailed Report" + " (" + TitleType + ") ");
                           }
                           else if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelDetailedReport")
                           {
                               sbMessage.AppendLine("reportParameter[9]: "+ "Title" + " Office Depot Line Level Detailed Report" + " (" + TitleType + ") ");
                               reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Detailed Report" + " (" + TitleType + ") ");
                           }                             
                           break;

                       case "Master Vendor OTIF Report":
                           sbMessage.AppendLine("Master Vendor OTIF Report");
                           reportParameter = new ReportParameter[4];
                           sbMessage.AppendLine("reportParameter[0]: " + "Vendor :" + pReportRequestBE.VendorName);
                           reportParameter[0] = new ReportParameter("Vendor", pReportRequestBE.VendorName);
                           sbMessage.AppendLine("reportParameter[1]: " + "DateFrom :" + sDateFrom);
                           reportParameter[1] = new ReportParameter("DateFrom", sDateFrom);
                           sbMessage.AppendLine("reportParameter[2]: " + "DateTo :" + sDateTo);
                           reportParameter[2] = new ReportParameter("DateTo", sDateTo);

                           if (pReportRequestBE.ReportAction == "OTIF_MasterVendorReportNewMeasure")
                           {
                               sbMessage.AppendLine("reportParameter[3]: " + "Title" + "Monthly (NEW Measure)");
                               reportParameter[3] = new ReportParameter("Title", "Monthly (NEW Measure)");
                           }
                           else if (pReportRequestBE.ReportAction == "OTIF_MasterVendorReportOldMeasure")
                           {
                               sbMessage.AppendLine("reportParameter[3]: " + "Title"+ "Monthly (OLD Measure)");
                               reportParameter[3] = new ReportParameter("Title", "Monthly (OLD Measure)");
                           }
                           else if (pReportRequestBE.ReportAction == "OTIF_MasterVendorReportAbsolute")
                           {
                               sbMessage.AppendLine("reportParameter[3]: " + "Title"+ "Monthly (Absolute)");
                               reportParameter[3] = new ReportParameter("Title", "Monthly (Absolute)");
                           }
                           break;

                       case "OTIF Tracking Report":
                           reportParameter = new ReportParameter[4];
                           sbMessage.AppendLine("OTIF Tracking Report");
                           sbMessage.AppendLine("reportParameter[0]: Country" + pReportRequestBE.CountryName);
                           reportParameter[0] = new ReportParameter("Country", pReportRequestBE.CountryName);
                           sbMessage.AppendLine("reportParameter[1]: " + "Vendor" + pReportRequestBE.VendorName);
                           reportParameter[1] = new ReportParameter("Vendor", pReportRequestBE.VendorName);
                           sbMessage.AppendLine("reportParameter[2]: " + "DateFrom" + sDateFrom);
                           reportParameter[2] = new ReportParameter("DateFrom", sDateFrom);
                           sbMessage.AppendLine("reportParameter[3]: "+ "DateTo" + sDateTo);
                           reportParameter[3] = new ReportParameter("DateTo", sDateTo);
                           break;

                       case "Lead Time Variance":
                       case "OTIF Stock Planner Report":
                           sbMessage.AppendLine("Lead Time Variance" + " or " +"OTIF Stock Planner Report");
                           reportParameter = new ReportParameter[9];
                           sbMessage.AppendLine("reportParameter[0]: " + "Country :" + pReportRequestBE.CountryName);
                           reportParameter[0] = new ReportParameter("Country", pReportRequestBE.CountryName);
                           sbMessage.AppendLine("reportParameter[1]: " + "Site:"+ pReportRequestBE.SiteName);
                           reportParameter[1] = new ReportParameter("Site", pReportRequestBE.SiteName);
                           sbMessage.AppendLine("reportParameter[2]: " + "Vendor:"+ pReportRequestBE.VendorName);
                           reportParameter[2] = new ReportParameter("Vendor", pReportRequestBE.VendorName);
                           sbMessage.AppendLine("reportParameter[3]: " + "DateFrom:" + sDateFrom);
                           reportParameter[3] = new ReportParameter("DateFrom", sDateFrom);
                           sbMessage.AppendLine("reportParameter[4]: " + "DateTo:" + sDateTo);
                           reportParameter[4] = new ReportParameter("DateTo", sDateTo);
                           sbMessage.AppendLine("reportParameter[5]: " + "ItemClassification:"+ pReportRequestBE.ItemClassification);
                           reportParameter[5] = new ReportParameter("ItemClassification", pReportRequestBE.ItemClassification);
                           sbMessage.AppendLine("reportParameter[6]: " + "ODSKUCode:"+ pReportRequestBE.OD_SKU_No);
                           reportParameter[6] = new ReportParameter("ODSKUCode", pReportRequestBE.OD_SKU_No);
                           sbMessage.AppendLine("reportParameter[7]: " + "PurchaseOrder:"+ pReportRequestBE.PurchaseOrder);
                           reportParameter[7] = new ReportParameter("PurchaseOrder", pReportRequestBE.PurchaseOrder);
                           sbMessage.AppendLine("reportParameter[8]: " + "StockPlanner"+ pReportRequestBE.StockPlannerName);
                           reportParameter[8] = new ReportParameter("StockPlanner", pReportRequestBE.StockPlannerName);
                           break;

                   }

                   sbMessage.AppendLine(pReportRequestBE.ReportDatatableName);
                   rds = new ReportDataSource(pReportRequestBE.ReportDatatableName, dsOTIFReport.Tables[0]);
                   sbMessage.AppendLine(path + @"ModuleUI\OnTimeInFull\RDLC\" + pReportRequestBE.ReportNamePath);
                   ReportViewer1.LocalReport.ReportPath = path + @"ModuleUI\OnTimeInFull\RDLC\" + pReportRequestBE.ReportNamePath;
                   ReportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                   ReportViewer1.LocalReport.Refresh();
                   ReportViewer1.LocalReport.DataSources.Clear();
                   ReportViewer1.LocalReport.DataSources.Add(rds);
                   ReportViewer1.LocalReport.SetParameters(reportParameter);

                   sbMessage.AppendLine("Reports are working on bytes genaeration starts.");
                   byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                   sbMessage.AppendLine("Reports are working on bytes genaeration ends.");

                   sbMessage.AppendLine("Reports path:" + path + @"ReportOutput\");
                   ReportOutputPath = path + @"ReportOutput\";
                   if (!System.IO.Directory.Exists(ReportOutputPath))
                       System.IO.Directory.CreateDirectory(ReportOutputPath);

                   sbMessage.AppendLine("Directory exists at" + ReportOutputPath);


                   dt = Convert.ToDateTime(pReportRequestBE.RequestTime);

                   requestTime = "";

                   requestTime = dt.Day.ToString().Length == 1 ? requestTime + "0" + dt.Day.ToString() : requestTime + dt.Day.ToString();

                   requestTime = dt.Month.ToString().Length == 1 ? requestTime + "0" + dt.Month.ToString() : requestTime + dt.Month.ToString();

                   requestTime = requestTime + dt.Year.ToString();

                   requestTime = requestTime + "_" + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString();

                   sbMessage.AppendLine("Requested Time:" + requestTime);
                   ReportOutputPath = ReportOutputPath + pReportRequestBE.ReportRequestID.ToString() + "_";
                   ReportOutputPath = ReportOutputPath + pReportRequestBE.ReportName + "_";
                   ReportOutputPath = string.IsNullOrEmpty(pReportRequestBE.ReportType)
                       ? ReportOutputPath + pReportRequestBE.UserID + "_" + requestTime + ".xls"
                       : ReportOutputPath + pReportRequestBE.ReportType + "_" + pReportRequestBE.UserID + "_" + requestTime + ".xls";

                   sbMessage.AppendLine("ReportOutputPath:" + ReportOutputPath);

                   sbMessage.AppendLine("Files writing started.");
                   File.WriteAllBytes(ReportOutputPath, bytes);
                   sbMessage.AppendLine("Files writing ended.");

                   sbMessage.Append("Report Request Id " + pReportRequestBE.ReportRequestID.ToString() + " Successfully Generated");
                   sbMessage.Append("\r\n");

                   LogUtility.SaveTraceLogEntry(sbMessage);
               }*/
                #endregion
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                throw ex;
            }
        }
        public static bool ExportExcelWithAspose(System.Data.DataTable data, string filepath)
        {
            try
            {
                if (data == null)
                {
                    return false;
                }
                Aspose.Cells.License li = new Aspose.Cells.License();
                li.SetLicense("Aspose.Cells.lic"); //Cracking Certificate

                Workbook book = new Workbook(); // Create A Workbook
                Worksheet sheet = book.Worksheets[0]; // Create a worksheet
                Cells cells = sheet.Cells; // Cells
                                           // Create Styles
                Aspose.Cells.Style style = book.Styles[book.Styles.Add()];
                style.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle = Aspose.Cells.CellBorderType.Thin; // Apply the left boundary of the boundary line 
                style.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin; // Apply the right boundary of the boundary line 
                style.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle = Aspose.Cells.CellBorderType.Thin; // Apply the boundary line on the boundary line 
                style.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle = Aspose.Cells.CellBorderType.Thin; // Apply the underlying boundary 
                style.HorizontalAlignment = TextAlignmentType.Center; // Cell content is horizontally aligned with text in the middle
                                                                      // style.Font.Name = Song Style;//Font.Name
                                                                      // style1. Font. IsBold = true; // Set bold
                style.Font.Size = 11; // Set font size
                                      // Style. ForegroundColor = System. Drawing. Color. FromArgb (153, 204, 0); //Background color
                                      //style.Pattern = Aspose.Cells.BackgroundType.Solid; 

                int Colnum = data.Columns.Count; // Table column number 
                int Rownum = data.Rows.Count; // Table row count 
                                              // Generate row and column name rows 
                for (int i = 0; i < Colnum; i++)
                {
                    cells[0, i].PutValue(data.Columns[i].ColumnName); // Add table headers                    
                }
                // Generate data rows 
                for (int i = 0; i < Rownum; i++)
                {
                    for (int k = 0; k < Colnum; k++)
                    {
                        cells[1 + i, k].PutValue(data.Rows[i][k].ToString()); //Add data 
                    }
                }
                sheet.AutoFitColumns(); // Adaptive Width
                book.Save(filepath); //save
                                     //MessageBox.Show("Excel successfully saved to D disk!!! "";

                GC.Collect();
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

    }
}

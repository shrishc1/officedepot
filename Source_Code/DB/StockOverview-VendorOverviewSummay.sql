SELECT UP_Vendor.Vendor_no, 
		UP_Vendor.Vendor_Name, 
		COUNT(DISTINCT UP_SKU.OD_SKU_NO) AS TotalSKU, 
		SUM(UP_SKU.Qty_On_Hand * UP_SKU.Item_Val) AS TotalValue, 
        SUM(UP_ReceiptInformation.Qty_receipted * UP_PurchaseOrderDetails.PO_cost) AS Receipt_Value, 
        SUM(UP_SKUUpdateSold.Qty_Sold_Yesterday * UP_SKUUpdateSold.Item_Val) AS SoldValue, 
        Count(Distinct UP_SKUUpdateStockOut.OD_SKU_NO) AS StockOutSKU
FROM UP_SKUUpdate AS UP_SKUUpdateStockOut 
		RIGHT OUTER JOIN UP_SKU 
				ON UP_SKUUpdateStockOut.SKUID = UP_SKU.SKUID 
			   AND UP_SKUUpdateStockOut.Qty_On_Hand = 0
			LEFT OUTER JOIN UP_SKUUpdate AS UP_SKUUpdateSold 
				ON UP_SKU.SKUID = UP_SKUUpdateSold.SKUID 
			LEFT OUTER JOIN UP_ReceiptInformation 
			INNER JOIN UP_PurchaseOrderDetails 
				ON UP_ReceiptInformation.PurcahseOrderID = UP_PurchaseOrderDetails.PurchaseOrderID 
				ON UP_SKU.SKUID = UP_ReceiptInformation.SKUID 
			Right OUTER JOIN
              dbo.UP_Vendor ON dbo.UP_SKU.VendorID = dbo.UP_Vendor.VendorID
Group by UP_Vendor.Vendor_no, UP_Vendor.Vendor_Name





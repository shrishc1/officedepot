--delete from MAS_BookingStatus
--delete from MAS_BookingType
--delete from MAS_Country
--delete from MAS_Currency
--delete from MAS_DiscrepancyType
--delete from MAS_Language
--delete from SCT_Module
--delete from SCT_Screen
--delete from SYS_SlotTime
--delete from UserRole

delete from APP_BookedSlots
delete from APP_BookedVendorPO
delete from APP_BookingHistory
delete from APP_BookingVendor
delete from APP_Booking

delete from CMN_CommunicationErrorLog
delete from DeletedFixedSlot
delete from DIS_WorkflowActions

delete from MAS_CurrencyConversion
delete from MAS_SiteNumberList
delete from MAS_VendorAccountsPayableMatrix

delete from MASCNT_CrossDocking
delete from MASCNT_DoorType
delete from MASSIT_Carrier
delete from MASSIT_DeliveryConstraint
delete from MASSIT_DeliveryConstraintSpecific
delete from MASSIT_DeliveryType
delete from MASSIT_DoorConstraintSpecific
delete from MASSIT_DoorOpenTime
delete from MASSIT_FixedSlot
delete from MASSIT_GroupClassification
delete from MASSIT_VehicleType
delete from MASSIT_VehicleTypeDoorTypeMatrix
delete from MASSIT_Vendor
delete from MASSIT_WeekSetupGeneric
delete from MASSIT_WeekSetupSpecific

delete from MASCNT_DeliveryType
delete from MASSIT_DoorNumber

delete from OTIF_Report
delete from OTIF_RuleSetting

delete from RPT_DiscrepancyLetter
delete from RPT_DiscrepancyReturnNote
delete from RPT_DiscrepancyWorkflow

delete from SCT_TemplateScreen where TemplateID > 1
delete from SCT_Template where TemplateID > 1

delete from SCT_UserSite
delete from SCT_UserStockPlannerMatrix
delete from SCT_VendorSite
delete from SCT_User where UserID > 1

delete from TRN_DiscrepancyCommunication
delete from TRN_DiscrepancyItem
delete from TRN_DiscrepancyLogImage
delete from TRN_DiscrepancyLog

delete from UP_ReceiptInformation
delete from UP_PurchaseOrderDetails
delete from UP_SKUUpdate
delete from UP_SKU
delete from UP_StockPlanner
delete from UP_Vendor
delete from UP_VendorEdit

delete from UP_FileProcessed
delete from UP_FolderDownload
delete from UP_ErrorLog
delete from UP_DataImportScheduler

delete from MASCNT_Carrier
delete from MAS_Site


DBCC CHECKIDENT  ('APP_BookedSlots' , RESEED, 0)
DBCC CHECKIDENT  ('APP_BookedVendorPO' , RESEED, 0)
DBCC CHECKIDENT  ('APP_BookingHistory' , RESEED, 0)
DBCC CHECKIDENT  ('APP_BookingVendor' , RESEED, 0)
DBCC CHECKIDENT  ('APP_Booking' , RESEED, 0)

DBCC CHECKIDENT  ('CMN_CommunicationErrorLog' , RESEED, 0)
DBCC CHECKIDENT  ('DeletedFixedSlot' , RESEED, 0)
DBCC CHECKIDENT  ('DIS_WorkflowActions' , RESEED, 0)

DBCC CHECKIDENT  ('MAS_CurrencyConversion' , RESEED, 0)
DBCC CHECKIDENT  ('MAS_RegionCountryMatrix' , RESEED, 0)
DBCC CHECKIDENT  ('MAS_SiteNumberList' , RESEED, 0)
DBCC CHECKIDENT  ('MAS_VendorAccountsPayableMatrix' , RESEED, 0)

DBCC CHECKIDENT  ('MASCNT_CrossDocking' , RESEED, 0)
DBCC CHECKIDENT  ('MASCNT_DoorType' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_Carrier' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_DeliveryConstraint' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_DeliveryConstraintSpecific' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_DeliveryType' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_DoorConstraintSpecific' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_DoorOpenTime' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_FixedSlot' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_GroupClassification' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_VehicleType' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_VehicleTypeDoorTypeMatrix' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_Vendor' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_WeekSetupGeneric' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_WeekSetupSpecific' , RESEED, 0)

DBCC CHECKIDENT  ('MASCNT_DeliveryType' , RESEED, 0)
DBCC CHECKIDENT  ('MASSIT_DoorNumber' , RESEED, 0)

DBCC CHECKIDENT  ('OTIF_Report' , RESEED, 0)
DBCC CHECKIDENT  ('OTIF_RuleSetting' , RESEED, 0)

DBCC CHECKIDENT  ('RPT_DiscrepancyLetter' , RESEED, 0)
DBCC CHECKIDENT  ('RPT_DiscrepancyReturnNote' , RESEED, 0)
DBCC CHECKIDENT  ('RPT_DiscrepancyWorkflow' , RESEED, 0)

DBCC CHECKIDENT  ('SCT_TemplateScreen' , RESEED, 0)
DBCC CHECKIDENT  ('SCT_Template' , RESEED, 0)

DBCC CHECKIDENT  ('SCT_UserSite' , RESEED, 0)
DBCC CHECKIDENT  ('SCT_UserStockPlannerMatrix' , RESEED, 0)
DBCC CHECKIDENT  ('SCT_VendorSite' , RESEED, 0)
DBCC CHECKIDENT  ('SCT_User' , RESEED, 0)

DBCC CHECKIDENT  ('TRN_DiscrepancyCommunication' , RESEED, 0)
DBCC CHECKIDENT  ('TRN_DiscrepancyItem' , RESEED, 0)
DBCC CHECKIDENT  ('TRN_DiscrepancyLogImage' , RESEED, 0)
DBCC CHECKIDENT  ('TRN_DiscrepancyLog' , RESEED, 0)

DBCC CHECKIDENT  ('UP_ReceiptInformation' , RESEED, 0)
DBCC CHECKIDENT  ('UP_PurchaseOrderDetails' , RESEED, 0)
DBCC CHECKIDENT  ('UP_SKU' , RESEED, 0)
DBCC CHECKIDENT  ('UP_SKUUpdate' , RESEED, 0)
DBCC CHECKIDENT  ('UP_StockPlanner' , RESEED, 0)
DBCC CHECKIDENT  ('UP_Vendor' , RESEED, 0)
DBCC CHECKIDENT  ('UP_VendorEdit' , RESEED, 0)

DBCC CHECKIDENT  ('UP_FileProcessed' , RESEED, 0)
DBCC CHECKIDENT  ('UP_FolderDownload' , RESEED, 0)
DBCC CHECKIDENT  ('P_ErrorLog' , RESEED, 0)
DBCC CHECKIDENT  ('UP_DataImportScheduler' , RESEED, 0)

DBCC CHECKIDENT  ('MASCNT_Carrier' , RESEED, 0)
DBCC CHECKIDENT  ('MAS_Site' , RESEED, 0)


USE [OD-VendorsInPartnership]
GO
/****** Object:  Trigger [dbo].[PostUPSKUUpdate]    Script Date: 01/27/2012 19:34:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create TRIGGER [dbo].[PostUPSKUUpdate]
   ON  [dbo].[UP_SKU] 
   AFTER UPDATE
AS 
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @SKUID AS INT
	DECLARE @Qty_Sold_Yesterday AS Decimal(10,2)
	DECLARE @Qty_On_Hand AS Decimal(10,2)
	DECLARE @qty_on_backorder AS Decimal(10,2)
	DECLARE @Item_Val AS Decimal(10,2)
	DECLARE @OD_SKU_NO AS VARCHAR(15)
	DECLARE @Warehouse AS VARCHAR(10)
	DECLARE @UpdatedDate AS Date
	
	
	SELECT  @Qty_Sold_Yesterday = Qty_Sold_Yesterday,
			@Qty_On_Hand = Qty_On_Hand,
			@qty_on_backorder = qty_on_backorder,
			@Item_Val = Item_Val,
			@OD_SKU_NO = OD_SKU_NO,
			@Warehouse = Warehouse,
			@UpdatedDate = UpdatedDate 
	  FROM Inserted 
	
	SELECT @SKUID = SKUID FROM Deleted
   
	Insert Into UP_SKUUpdate
		(SKUID, Qty_Sold_Yesterday, Qty_On_Hand, qty_on_backorder,Item_Val,OD_SKU_NO, Warehouse, DateUploaded )
	Values
		(@SKUID,@Qty_Sold_Yesterday,@Qty_On_Hand,@qty_on_backorder,@Item_Val,@OD_SKU_NO,@Warehouse,@UpdatedDate)
END

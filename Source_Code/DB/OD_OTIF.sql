SELECT * INTO #WarehouseSiteNames 
FROM 
(Select SiteCountryID,MAS_Site.SiteID,MAS_SiteNumberList.SiteNumber,SitePrefix,SiteName
  From MAS_SiteNumberList
		Inner Join MAS_Site
			ON MAS_SiteNumberList.SiteID = MAS_Site.SiteID
 UNION
  SELECT SiteCountryID, MAS_Site.SiteID, MAS_Site.SiteNumber,SitePrefix,SiteName
	FROM MAS_Site  
   WHERE SitePrefix IS NOT NULL AND SiteNumber IS NOT NULL
)TempWarehouseSiteNames	


Select * INTO #StockPlannerNames
from (Select UP_StockPlanner.StockPlannerID, SCT_User.FirstName + ' ' + SCT_User.Lastname AS StockPlannerName
		From UP_StockPlanner
			Inner Join SCT_UserStockPlannerMatrix
				ON SCT_UserStockPlannerMatrix.StockPlannerID = UP_StockPlanner.StockPlannerID
				Inner Join SCT_User
					ON SCT_User.UserID =  SCT_UserStockPlannerMatrix.UserID
)TempStockPlannerNames

Select * Into #FirstReceived
From 
( Select OuterUP_ReceiptInformation.PurcahseOrderID,
		 OuterUP_ReceiptInformation.SKUID,
	 	 OuterUP_ReceiptInformation.Qty_receipted,
	 	 OuterUP_ReceiptInformation.[Date] AS ReceivedDate
   From UP_ReceiptInformation AS OuterUP_ReceiptInformation
	       Where OuterUP_ReceiptInformation.Date  = (Select Min(InnerUP_ReceiptInformation .Date)
											 	From UP_ReceiptInformation InnerUP_ReceiptInformation 
											   WHERE InnerUP_ReceiptInformation.PurcahseOrderID = OuterUP_ReceiptInformation.PurcahseOrderID)
)TempFirstReceived

Select * Into #TempReceivedCount
From 
(  Select UP_ReceiptInformation.PurcahseOrderID,
		  UP_ReceiptInformation.SKUID,
	 	  Count(UP_ReceiptInformation.ReceiptInformationID) AS NumberOfTimeReceived,
	 	  DateDiff(dd,  Min(UP_ReceiptInformation.Date),Max(UP_ReceiptInformation.Date)) AS NumberOfDaysDelivered
     From UP_ReceiptInformation 
 Group By UP_ReceiptInformation.PurcahseOrderID,
		  UP_ReceiptInformation.SKUID
)TempReceivedCount

Select * INTO #OTIFParameters from 
(Select	UP_Vendor.VendorID,
		UP_PurchaseOrderDetails.PurchaseOrderID,
		UP_SKU.SKUID,
						
		Case when 
			( Case when OTIF_RuleSetting.AbsoluteDaysAllowedEarly is null OR ( DateDiff(dd,#FirstReceived.ReceivedDate,  ISNULL(UP_PurchaseOrderDetails.RevisedDueDate,UP_PurchaseOrderDetails.Original_due_date)) 
						<= OTIF_RuleSetting.AbsoluteDaysAllowedEarly) then 1 else 0 end
			 + Case  when OTIF_RuleSetting.AbsoluteDaysAllowedLate is null OR ( DateDiff(dd,  ISNULL(UP_PurchaseOrderDetails.RevisedDueDate,UP_PurchaseOrderDetails.Original_due_date), 	 #FirstReceived.ReceivedDate) 
						<= OTIF_RuleSetting.AbsoluteDaysAllowedLate) then 1 else 0 end
			 + Case when OTIF_RuleSetting.AbsoluteNumberOfDeliveriesAllowed is null OR #TempReceivedCount.NumberOfDaysDelivered 
						<= OTIF_RuleSetting.AbsoluteNumberOfDeliveriesAllowed then 1 else 0 end
			) = 3 then 'HIT' Else 'MISS' End AS ODAbsolute,
		
		
		Case when 
			( Case when OTIF_RuleSetting.ToleratedDaysAllowedEarly1 is null OR ( DateDiff(dd,#FirstReceived.ReceivedDate,  ISNULL(UP_PurchaseOrderDetails.RevisedDueDate,UP_PurchaseOrderDetails.Original_due_date)) 
						<= OTIF_RuleSetting.ToleratedDaysAllowedEarly1) then 1 else 0 end
			 + Case  when OTIF_RuleSetting.ToleratedDaysAllowedLate1 is null OR ( DateDiff(dd,  ISNULL(UP_PurchaseOrderDetails.RevisedDueDate,UP_PurchaseOrderDetails.Original_due_date), 	 #FirstReceived.ReceivedDate) 
						<= OTIF_RuleSetting.ToleratedDaysAllowedLate1) then 1 else 0 end
			 + Case when OTIF_RuleSetting.ToleratedNumberOfDeliveriesAllowed1 is null OR #TempReceivedCount.NumberOfDaysDelivered 
						<= OTIF_RuleSetting.ToleratedNumberOfDeliveriesAllowed1 then 1 else 0 end
			) = 3 then 'HIT' Else 'MISS' End AS ODTolerate,
		
		Case when 
			( Case when OTIF_RuleSetting.ODMeasureDaysAllowedEarly11 is null OR ( DateDiff(dd,#FirstReceived.ReceivedDate,  ISNULL(UP_PurchaseOrderDetails.RevisedDueDate,UP_PurchaseOrderDetails.Original_due_date)) 
						<= OTIF_RuleSetting.ODMeasureDaysAllowedEarly11) then 1 else 0 end
			 + Case  when OTIF_RuleSetting.ODMeasureDaysAllowedLate11 is null OR ( DateDiff(dd,  ISNULL(UP_PurchaseOrderDetails.RevisedDueDate,UP_PurchaseOrderDetails.Original_due_date), 	 #FirstReceived.ReceivedDate) 
						<= OTIF_RuleSetting.ODMeasureDaysAllowedLate11) then 1 else 0 end
			 + Case when OTIF_RuleSetting.ODMeasureNumberOfDeliveriesAllowed11 is null OR #TempReceivedCount.NumberOfDaysDelivered 
						<= OTIF_RuleSetting.ODMeasureNumberOfDeliveriesAllowed11 then 1 else 0 end
			) = 3 then 'HIT' Else 'MISS' End AS ODMeasure
  From UP_Vendor
			Left Outer Join UP_PurchaseOrderDetails
				ON UP_PurchaseOrderDetails.VendorID = UP_Vendor.VendorID	
				
				Left Outer Join UP_SKU
					ON UP_SKU.SKUID = UP_PurchaseOrderDetails.SKUID
				Left Outer Join #FirstReceived
					ON #FirstReceived.PurcahseOrderID = UP_PurchaseOrderDetails.PurchaseOrderID	
				   AND #FirstReceived.SKUID = UP_PurchaseOrderDetails.SKUID
				Left Outer Join #TempReceivedCount
					ON #TempReceivedCount.PurcahseOrderID = UP_PurchaseOrderDetails.PurchaseOrderID	
				   AND #TempReceivedCount.SKUID = UP_PurchaseOrderDetails.SKUID
				Cross Join OTIF_RuleSetting
) AS OTIFParameters


Select	UP_Vendor.Vendor_No,
		UP_Vendor.Vendor_Name,
		UP_PurchaseOrderDetails.Purchase_order,
		UP_PurchaseOrderDetails.Order_raised,
		#WarehouseSiteNames.SitePrefix + ' - ' + #WarehouseSiteNames.SiteName [Site],
		StockPlannerName,
		MAS_Country.CountryName,
		UP_SKU.Item_classification,
		UP_SKU.OD_SKU_NO,
		UP_SKU.Vendor_Code,
		UP_SKU.Direct_SKU,
		UP_PurchaseOrderDetails.Line_No,
		UP_SKU.DESCRIPTION,
		UP_SKU.Product_Lead_time,
		UP_PurchaseOrderDetails.Order_raised,		
		ISNULL(UP_PurchaseOrderDetails.RevisedDueDate,UP_PurchaseOrderDetails.Original_due_date) FinalDueDate,
		UP_PurchaseOrderDetails.Original_quantity * UP_PurchaseOrderDetails.PO_cost AS OrderLineValue,
		UP_PurchaseOrderDetails.Original_quantity,
		
		Case When (DateDiff(dd,  ISNULL(UP_PurchaseOrderDetails.RevisedDueDate,UP_PurchaseOrderDetails.Original_due_date), 
				 #FirstReceived.ReceivedDate)) < 0 
				 Then DateDiff(dd,#FirstReceived.ReceivedDate,  ISNULL(UP_PurchaseOrderDetails.RevisedDueDate,UP_PurchaseOrderDetails.Original_due_date) )
				 Else null End AS [DaysEarlyAgainstAgreedDate],				    
		Case When (DateDiff(dd,  ISNULL(UP_PurchaseOrderDetails.RevisedDueDate,UP_PurchaseOrderDetails.Original_due_date), 
				 #FirstReceived.ReceivedDate)) > 0 
				 Then  DateDiff(dd,  ISNULL(UP_PurchaseOrderDetails.RevisedDueDate,UP_PurchaseOrderDetails.Original_due_date), 
				 #FirstReceived.ReceivedDate)
				 Else null End AS [DaysLateAgainstAgreedDate],				    
		
		Case When #FirstReceived.Qty_receipted >= UP_PurchaseOrderDetails.Original_quantity 
			 Then 'HIT' Else 'MISS' END AS InFullFirstTime,
		
		Case When DateDiff(dd,  ISNULL(UP_PurchaseOrderDetails.RevisedDueDate,UP_PurchaseOrderDetails.Original_due_date), 
				 #FirstReceived.ReceivedDate) = 0
			Then 'YES' Else 'NO' End  AS [100%HitAgainstAgreedDate],
		
		#OTIFParameters.ODAbsolute,
		#OTIFParameters.ODTolerate,
		#OTIFParameters.ODMeasure,
		
		
		
		
		(Select	((Sum( Case When #OTIFParameters.ODAbsolute = 'HIT' 
					Then 1.0 Else 0.0 END)/Count(*)) * 100.0) 
		  from #OTIFParameters
			Where #OTIFParameters.VendorID = UP_PurchaseOrderDetails.VendorID				   
		) AS PercentageODAbsolute ,
		
		(Select	((Sum( Case When #OTIFParameters.ODTolerate = 'HIT' 
					Then 1.0 Else 0.0 END)/Count(*)) * 100.0) 
		  from #OTIFParameters
			Where #OTIFParameters.VendorID = UP_PurchaseOrderDetails.VendorID				   
		) AS PercentageODTolerate ,
		
		(Select	((Sum( Case When #OTIFParameters.ODMeasure = 'HIT' 
					Then 1.0 Else 0.0 END)/Count(*)) * 100.0) 
		  from #OTIFParameters
			Where #OTIFParameters.VendorID = UP_PurchaseOrderDetails.VendorID				   
		) AS PercentageODMeasure ,
		
		
		
		
		
		Case when (Select ((Sum( Case When #OTIFParameters.ODAbsolute = 'HIT' 
									  Then 1.0 Else 0.0 END)/Count(*)) * 100.0) 
					from #OTIFParameters
				   Where #OTIFParameters.VendorID = UP_PurchaseOrderDetails.VendorID				   
				  ) < UP_Vendor.OTIF_AbsolutePenaltyBelowPercentage 
			 Then (UP_PurchaseOrderDetails.Original_quantity * UP_PurchaseOrderDetails.PO_cost)*(UP_Vendor.OTIF_AbsoluteAppliedPenaltyPercentage/100)
			 Else 0 End	AS PenaltyODAbsolute,
		
		Case when (Select ((Sum( Case When #OTIFParameters.ODTolerate = 'HIT' 
									  Then 1.0 Else 0.0 END)/Count(*)) * 100.0) 
					from #OTIFParameters
				   Where #OTIFParameters.VendorID = UP_PurchaseOrderDetails.VendorID				   
				  ) < UP_Vendor.OTIF_ToleratedPenaltyBelowPercentage
			 Then (UP_PurchaseOrderDetails.Original_quantity * UP_PurchaseOrderDetails.PO_cost)*(UP_Vendor.OTIF_ToleratedAppliedPenaltyPercentage/100)
			 Else 0 End	AS PenaltyODTolerate,
			 
		Case when (Select ((Sum( Case When #OTIFParameters.ODMeasure = 'HIT' 
									  Then 1.0 Else 0.0 END)/Count(*)) * 100.0) 
					from #OTIFParameters
				   Where #OTIFParameters.VendorID = UP_PurchaseOrderDetails.VendorID				   
				  ) < UP_Vendor.OTIF_ODMeasurePenaltyBelowPercentage 
			 Then (UP_PurchaseOrderDetails.Original_quantity * UP_PurchaseOrderDetails.PO_cost)*(UP_Vendor.OTIF_ODMeasureAppliedPenaltyPercentage/100)
			 Else 0 End	AS PenaltyODMeasure,
		
		
		
		
		#TempReceivedCount.NumberOfTimeReceived,
		#TempReceivedCount.NumberOfDaysDelivered,
		
		UP_ReceiptInformation.Date,
		UP_ReceiptInformation.Qty_receipted
  From UP_Vendor
			Left Outer Join UP_PurchaseOrderDetails
				ON UP_PurchaseOrderDetails.VendorID = UP_Vendor.VendorID
				Left Outer Join #WarehouseSiteNames
					ON UP_PurchaseOrderDetails.Warehouse = #WarehouseSiteNames.SiteNumber
				Left Outer Join #StockPlannerNames
					ON #StockPlannerNames.StockPlannerID = UP_PurchaseOrderDetails.StockPlannerID
				Left Outer Join MAS_Country
					ON MAS_Country.CountryID = UP_PurchaseOrderDetails.CountryID
				Left Outer Join UP_SKU
					ON UP_SKU.SKUID = UP_PurchaseOrderDetails.SKUID
				Left Outer Join #FirstReceived
					ON #FirstReceived.PurcahseOrderID = UP_PurchaseOrderDetails.PurchaseOrderID	
				   AND #FirstReceived.SKUID = UP_PurchaseOrderDetails.SKUID
				Left Outer Join #TempReceivedCount
					ON #TempReceivedCount.PurcahseOrderID = UP_PurchaseOrderDetails.PurchaseOrderID	
				   AND #TempReceivedCount.SKUID = UP_PurchaseOrderDetails.SKUID
				Left Outer Join #OTIFParameters
					ON #OTIFParameters.VendorID = UP_PurchaseOrderDetails.VendorID
				   AND #OTIFParameters.PurchaseOrderID = UP_PurchaseOrderDetails.PurchaseOrderID
				   AND #OTIFParameters.SKUID = UP_PurchaseOrderDetails.SKUID				
				Cross Join OTIF_RuleSetting
				Left Outer Join UP_ReceiptInformation
					ON UP_ReceiptInformation.PurcahseOrderID = UP_PurchaseOrderDetails.PurchaseOrderID
				   AND UP_ReceiptInformation.SKUID = UP_PurchaseOrderDetails.SKUID

Order by 1


Drop Table #WarehouseSiteNames
Drop Table #StockPlannerNames
Drop Table #FirstReceived
Drop Table #TempReceivedCount
Drop Table #OTIFParameters
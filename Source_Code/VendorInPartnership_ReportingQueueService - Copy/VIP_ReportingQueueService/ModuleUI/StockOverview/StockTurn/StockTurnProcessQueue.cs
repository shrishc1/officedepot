﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Reporting.WinForms;
using Utilities;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessLogicLayer.ModuleBAL.StockOverview.StockTurn;
using BusinessEntities.ModuleBE.StockOverview.StockTurn;
using System.Data;
using System.IO;

namespace VIP_ReportingQueueService.ModuleUI.StockOverview.StockTurn
{
    public class StockTurnProcessQueue
    {
        public void GenerateStockTurnReport(ReportRequestBE pReportRequestBE)
        {

            try
            {
                StockTurnReportBE oStockTurnReportBE = new StockTurnReportBE();
                StockTurnReportBAL oStockTurnReportBAL = new StockTurnReportBAL();
                DataSet dsStockTurnReport;

                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                path = path.Replace(@"\bin\debug", "");
                
                //Get Report Data
                oStockTurnReportBE.Action = string.IsNullOrEmpty(pReportRequestBE.ReportAction) ? null : pReportRequestBE.ReportAction;
                //oStockTurnReportBE.DateFrom =pReportRequestBE.DateTo.Value.AddMonths(-2).AddDays(-pReportRequestBE.DateTo.Value.Day).AddDays(1);
                oStockTurnReportBE.DateFrom = pReportRequestBE.DateFrom;
                oStockTurnReportBE.DateTo = pReportRequestBE.DateTo;               
                oStockTurnReportBE.SelectedCountryIDs = string.IsNullOrEmpty(pReportRequestBE.CountryIDs) ? null : pReportRequestBE.CountryIDs;
                oStockTurnReportBE.SelectedSiteIDs = string.IsNullOrEmpty(pReportRequestBE.SiteIDs) ? null : pReportRequestBE.SiteIDs;
                oStockTurnReportBE.SelectedStockPlannerIDs = string.IsNullOrEmpty(pReportRequestBE.StockPlannerIDs) ? null : pReportRequestBE.StockPlannerIDs;
                oStockTurnReportBE.SelectedVendorIDs = string.IsNullOrEmpty(pReportRequestBE.VendorIDs) ? null : pReportRequestBE.VendorIDs;
                oStockTurnReportBE.SelectedSKUCodes = string.IsNullOrEmpty(pReportRequestBE.OD_SKU_No) ? null : pReportRequestBE.OD_SKU_No;                
                oStockTurnReportBE.SelectedItemClassification = string.IsNullOrEmpty(pReportRequestBE.ItemClassification) ? null : pReportRequestBE.ItemClassification;
                oStockTurnReportBE.SelectedRMSCategoryIDs = string.IsNullOrEmpty(pReportRequestBE.SelectedRMSCategoryIDs) ? null : pReportRequestBE.SelectedRMSCategoryIDs;
                oStockTurnReportBE.ReportAction=string.IsNullOrEmpty(pReportRequestBE.ReportAction) ? null :pReportRequestBE.ReportAction;
                oStockTurnReportBE.ReportType=string.IsNullOrEmpty(pReportRequestBE.ReportType) ? null : pReportRequestBE.ReportType;
                dsStockTurnReport = oStockTurnReportBAL.getStockTurnODViewReportBAL(oStockTurnReportBE);
                oStockTurnReportBAL = null;

                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("\r\n");
                if (dsStockTurnReport == null)
                    sbMessage.Append("Record count         : null");
                else
                    sbMessage.Append("Record count          : " + dsStockTurnReport.Tables[0].Rows.Count.ToString());
                LogUtility.SaveTraceLogEntry(sbMessage);

                // Prepare RDLC
                string ReportOutputPath;
                ReportDataSource rds;
                ReportViewer ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
                ReportParameter[] reportParameter = null;
                DateTime dt;
                string sDateFrom = string.Empty;
                string sDateTo = string.Empty;
                string requestTime = string.Empty;

                #region "string Date"
                if (pReportRequestBE.DateFrom != null)
                {
                    if (pReportRequestBE.DateFrom.Value.Day.ToString().Length == 1)
                        sDateFrom = sDateFrom + "0" + oStockTurnReportBE.DateFrom.Value.Day.ToString();
                    else
                        sDateFrom = sDateFrom + oStockTurnReportBE.DateFrom.Value.Day.ToString();

                    if (pReportRequestBE.DateFrom.Value.Month.ToString().Length == 1)
                        sDateFrom = sDateFrom + "/" + "0" + oStockTurnReportBE.DateFrom.Value.Month.ToString();
                    else
                        sDateFrom = sDateFrom + "/" + oStockTurnReportBE.DateFrom.Value.Month.ToString();

                    sDateFrom = sDateFrom + "/" + oStockTurnReportBE.DateFrom.Value.Year.ToString();
                }


                if (pReportRequestBE.DateTo != null)
                {
                    if (pReportRequestBE.DateTo.Value.Day.ToString().Length == 1)
                        sDateTo = sDateTo + "0" + pReportRequestBE.DateTo.Value.Day.ToString();
                    else
                        sDateTo = sDateTo + pReportRequestBE.DateTo.Value.Day.ToString();

                    if (pReportRequestBE.DateTo.Value.Month.ToString().Length == 1)
                        sDateTo = sDateTo + "/" + "0" + pReportRequestBE.DateTo.Value.Month.ToString();
                    else
                        sDateTo = sDateTo + "/" + pReportRequestBE.DateTo.Value.Month.ToString();

                    sDateTo = sDateTo + "/" + pReportRequestBE.DateTo.Value.Year.ToString();
                }
                #endregion
                #region ReportVariables

                Warning[] warnings;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = string.Empty;
                string reportFileName = string.Empty;

                #endregion

                switch (pReportRequestBE.ReportName)
                {
                    case "Stock Turn Detailed Report (OD View)":
                         reportParameter = new ReportParameter[10];
                         reportParameter[0] = new ReportParameter("Country", pReportRequestBE.CountryName);
                         reportParameter[1] = new ReportParameter("Site", pReportRequestBE.SiteName);
                         reportParameter[2] = new ReportParameter("Vendor", pReportRequestBE.VendorName);
                         reportParameter[3] = new ReportParameter("DateFrom", sDateFrom);
                         reportParameter[4] = new ReportParameter("DateTo", sDateTo);
                         reportParameter[5] = new ReportParameter("ItemClassification", pReportRequestBE.ItemClassification);
                         reportParameter[6] = new ReportParameter("ODSKUCode", pReportRequestBE.OD_SKU_No);
                         reportParameter[7] = new ReportParameter("StockPlanner", pReportRequestBE.StockPlannerName);
                         reportParameter[8] = new ReportParameter("Category", pReportRequestBE.SelectedRMSCategoryName); 

                       // string TitleType = string.Empty;
                         if (pReportRequestBE.ReportAction == "ALL")
                             reportParameter[9] = new ReportParameter("Title", "Stock Turn Detailed Report (OD View)-All Stock Turn");
                         else if (pReportRequestBE.ReportAction == "ACTIVE")
                             reportParameter[9] = new ReportParameter("Title", "Stock Turn Detailed Report (OD View)-Only Active Stock Turn");
                         else if (pReportRequestBE.ReportAction == "EXCLUDE XXDC")
                             reportParameter[9] = new ReportParameter("Title", "Stock Turn Detailed Report (OD View)-Exclude XXDC Stock Turn");
                           else if (pReportRequestBE.ReportAction == "EXCLUDE DISC")
                             reportParameter[9] = new ReportParameter("Title", "Stock Turn Detailed Report (OD View)-Exclude DSIC Stock Turn");
                        break;

                    case "Stock Turn Summary Report":
                        reportParameter = new ReportParameter[10];
                        reportParameter[0] = new ReportParameter("Country", pReportRequestBE.CountryName);
                        reportParameter[1] = new ReportParameter("Site", pReportRequestBE.SiteName);
                        reportParameter[2] = new ReportParameter("Vendor", pReportRequestBE.VendorName);
                        reportParameter[3] = new ReportParameter("DateFrom", sDateFrom);
                        reportParameter[4] = new ReportParameter("DateTo", sDateTo);
                        reportParameter[5] = new ReportParameter("ItemClassification", pReportRequestBE.ItemClassification);
                        reportParameter[6] = new ReportParameter("ODSKUCode", pReportRequestBE.OD_SKU_No);
                        reportParameter[7] = new ReportParameter("StockPlanner", pReportRequestBE.StockPlannerName);
                        reportParameter[8] = new ReportParameter("Category", pReportRequestBE.SelectedRMSCategoryName); 

                        // string TitleType = string.Empty;
                        if (pReportRequestBE.ReportAction == "ALL")
                            reportParameter[9] = new ReportParameter("Title", "Stock Turn Summary-All Stock Turn");
                        else if (pReportRequestBE.ReportAction == "ACTIVE")
                            reportParameter[9] = new ReportParameter("Title", "Stock Turn Summary-Only Active Stock Turn");
                        else if (pReportRequestBE.ReportAction == "EXCLUDE XXDC")
                            reportParameter[9] = new ReportParameter("Title", "Stock Turn Summary-Exclude XXDC Stock Turn");
                        else if (pReportRequestBE.ReportAction == "EXCLUDE DISC")
                            reportParameter[9] = new ReportParameter("Title", "Stock Turn Summary-Exclude DSIC Stock Turn");
                        break;

                    case "Stock Turn Vendor Report":
                         reportParameter = new ReportParameter[10];
                         reportParameter[0] = new ReportParameter("Country", pReportRequestBE.CountryName);
                         reportParameter[1] = new ReportParameter("Site", pReportRequestBE.SiteName);
                         reportParameter[2] = new ReportParameter("Vendor", pReportRequestBE.VendorName);
                         reportParameter[3] = new ReportParameter("DateFrom", sDateFrom);
                         reportParameter[4] = new ReportParameter("DateTo", sDateTo);
                         reportParameter[5] = new ReportParameter("ItemClassification", pReportRequestBE.ItemClassification);
                         reportParameter[6] = new ReportParameter("ODSKUCode", pReportRequestBE.OD_SKU_No);
                         reportParameter[7] = new ReportParameter("StockPlanner", pReportRequestBE.StockPlannerName);
                         reportParameter[8] = new ReportParameter("Category", pReportRequestBE.SelectedRMSCategoryName); 

                        // string TitleType = string.Empty;
                        if (pReportRequestBE.ReportAction == "ALL")
                            reportParameter[9] = new ReportParameter("Title", "Stock Turn Vendor-All Stock Turn");
                        else if (pReportRequestBE.ReportAction == "ACTIVE")
                            reportParameter[9] = new ReportParameter("Title", "Stock Turn Vendor-Only Active Stock Turn");
                        else if (pReportRequestBE.ReportAction == "EXCLUDE XXDC")
                            reportParameter[9] = new ReportParameter("Title", "Stock Turn Vendor-Exclude XXDC Stock Turn");
                        else if (pReportRequestBE.ReportAction == "EXCLUDE DISC")
                            reportParameter[9] = new ReportParameter("Title", "Stock Turn Vendor-Exclude DSIC Stock Turn");
                        break;
                }

                rds = new ReportDataSource(pReportRequestBE.ReportDatatableName, dsStockTurnReport.Tables[0]);
                ReportViewer1.LocalReport.ReportPath = path + @"ModuleUI\StockOverview\StockTurn\RDLC\" + pReportRequestBE.ReportNamePath;
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
                ReportViewer1.LocalReport.SetParameters(reportParameter);

                byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                //ReportViewer1.LocalReport.SetParameters(reportParameter);

                ReportOutputPath = path + @"ReportOutput\";
                if (!System.IO.Directory.Exists(ReportOutputPath))
                    System.IO.Directory.CreateDirectory(ReportOutputPath);

                dt = Convert.ToDateTime(pReportRequestBE.RequestTime);

                requestTime = "";
                if (dt.Day.ToString().Length == 1)
                    requestTime = requestTime + "0" + dt.Day.ToString();
                else
                    requestTime = requestTime + dt.Day.ToString();

                if (dt.Month.ToString().Length == 1)
                    requestTime = requestTime + "0" + dt.Month.ToString();
                else
                    requestTime = requestTime + dt.Month.ToString();

                requestTime = requestTime + dt.Year.ToString();

                requestTime = requestTime + "_" + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString();

                ReportOutputPath = ReportOutputPath + pReportRequestBE.ReportRequestID.ToString() + "_";
                ReportOutputPath = ReportOutputPath + pReportRequestBE.ReportName + "_";
                if (string.IsNullOrEmpty(pReportRequestBE.ReportType))
                    ReportOutputPath = ReportOutputPath + pReportRequestBE.UserID + "_" + requestTime + ".xls";
                else
                    ReportOutputPath = ReportOutputPath + pReportRequestBE.ReportType + "_" + pReportRequestBE.UserID + "_" + requestTime + ".xls";

                File.WriteAllBytes(ReportOutputPath, bytes);

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Report Request Id " + pReportRequestBE.ReportRequestID.ToString() + " Successfully Generated");
                sbMessage.Append("\r\n");

                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);               
            }
        }

    }
}

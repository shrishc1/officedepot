﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessEntities.ModuleBE.StockOverview.Report;
using BusinessLogicLayer.ModuleBAL.StockOverview.Report;
using System.Data;
using Microsoft.Reporting.WinForms;
using System.IO;
using System.Configuration;
using Utilities;

namespace VIP_ReportingQueueService.ModuleUI.StockOverview
{
    class ItemProcessQueue
    {
        public void GenerateItemSummaryReport(ReportRequestBE pReportRequestBE)
        {

            try
            {
                StockOverviewReportBE oStockOverviewReportBE = new StockOverviewReportBE();
                StockOverviewReportBAL oStockOverviewReportBAL = new StockOverviewReportBAL();
                DataSet dsStockOverview;

                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                path = path.Replace(@"\bin\debug", "");

                //Get Report Data
                oStockOverviewReportBE.Action = string.IsNullOrEmpty(pReportRequestBE.ReportAction) ? null : pReportRequestBE.ReportAction;
                oStockOverviewReportBE.DateFrom = pReportRequestBE.DateFrom;
                oStockOverviewReportBE.DateTo = pReportRequestBE.DateTo;                
                oStockOverviewReportBE.SelectedSiteIDs = string.IsNullOrEmpty(pReportRequestBE.SiteIDs) ? null : pReportRequestBE.SiteIDs;
                oStockOverviewReportBE.SelectedVendorIDs = string.IsNullOrEmpty(pReportRequestBE.VendorIDs) ? null : pReportRequestBE.VendorIDs;
                oStockOverviewReportBE.OfficeDepotSKU = string.IsNullOrEmpty(pReportRequestBE.OD_SKU_No) ? null : pReportRequestBE.OD_SKU_No;
                oStockOverviewReportBE.ODCatCode = string.IsNullOrEmpty(pReportRequestBE.ODCatCode) ? null : pReportRequestBE.ODCatCode;
                oStockOverviewReportBE.StockPlannerIDs = string.IsNullOrEmpty(pReportRequestBE.StockPlannerIDs) ? null : pReportRequestBE.StockPlannerIDs;
                dsStockOverview = oStockOverviewReportBAL.getItemSummaryReportBAL(oStockOverviewReportBE);
                oStockOverviewReportBAL = null;

                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("\r\n");
                if (dsStockOverview == null)
                    sbMessage.Append("Record count         : null");
                else
                    sbMessage.Append("Record count          : " + dsStockOverview.Tables[0].Rows.Count.ToString());
                LogUtility.SaveTraceLogEntry(sbMessage);


                // Prepare RDLC
                string ReportOutputPath;
                ReportDataSource rds;
                ReportViewer ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
                ReportParameter[] reportParameter = null;
                DateTime dt;
                string sDateFrom = string.Empty;
                string sDateTo = string.Empty;
                string requestTime = string.Empty;
                #region "string Date"
                if (pReportRequestBE.DateFrom != null)
                {
                    if (pReportRequestBE.DateFrom.Value.Day.ToString().Length == 1)
                        sDateFrom = sDateFrom + "0" + pReportRequestBE.DateFrom.Value.Day.ToString();
                    else
                        sDateFrom = sDateFrom + pReportRequestBE.DateFrom.Value.Day.ToString();

                    if (pReportRequestBE.DateFrom.Value.Month.ToString().Length == 1)
                        sDateFrom = sDateFrom + "/" + "0" + pReportRequestBE.DateFrom.Value.Month.ToString();
                    else
                        sDateFrom = sDateFrom + "/" + pReportRequestBE.DateFrom.Value.Month.ToString();

                    sDateFrom = sDateFrom + "/" + pReportRequestBE.DateFrom.Value.Year.ToString();
                }

                if (pReportRequestBE.DateTo != null)
                {
                    if (pReportRequestBE.DateTo.Value.Date.ToString().Length == 1)
                        sDateTo = sDateTo + "0" + pReportRequestBE.DateTo.Value.Day.ToString();
                    else
                        sDateTo = sDateTo + pReportRequestBE.DateTo.Value.Day.ToString();

                    if (pReportRequestBE.DateTo.Value.Month.ToString().Length == 1)
                        sDateTo = sDateTo + "/" + "0" + pReportRequestBE.DateTo.Value.Month.ToString();
                    else
                        sDateTo = sDateTo + "/" + pReportRequestBE.DateTo.Value.Month.ToString();

                    sDateTo = sDateTo + "/" + pReportRequestBE.DateTo.Value.Year.ToString();
                }
                #endregion
                #region ReportVariables

                Warning[] warnings;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = string.Empty;
                string reportFileName = string.Empty;

                #endregion

                reportParameter = new ReportParameter[7];
                reportParameter[0] = new ReportParameter("Vendor", pReportRequestBE.VendorName);
                reportParameter[1] = new ReportParameter("Planner", pReportRequestBE.StockPlannerName);
                reportParameter[2] = new ReportParameter("Site", pReportRequestBE.SiteName);
                reportParameter[3] = new ReportParameter("OfficeDepotSKU", pReportRequestBE.OD_SKU_No);
                reportParameter[4] = new ReportParameter("ODCatCode", pReportRequestBE.ODCatCode);
                reportParameter[5] = new ReportParameter("DateFrom", sDateFrom);
                reportParameter[6] = new ReportParameter("DateTo", sDateTo);
                
                rds = new ReportDataSource(pReportRequestBE.ReportDatatableName, dsStockOverview.Tables[0]);
                ReportViewer1.LocalReport.ReportPath = path + @"ModuleUI\StockOverview\RDLC\" + pReportRequestBE.ReportNamePath;
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
                ReportViewer1.LocalReport.SetParameters(reportParameter);

                byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                ReportOutputPath = path + @"ReportOutput\";
                if (!System.IO.Directory.Exists(ReportOutputPath))
                    System.IO.Directory.CreateDirectory(ReportOutputPath);

                dt = Convert.ToDateTime(pReportRequestBE.RequestTime);

                requestTime = "";
                if (dt.Day.ToString().Length == 1)
                    requestTime = requestTime + "0" + dt.Day.ToString();
                else
                    requestTime = requestTime + dt.Day.ToString();

                if (dt.Month.ToString().Length == 1)
                    requestTime = requestTime + "0" + dt.Month.ToString();
                else
                    requestTime = requestTime + dt.Month.ToString();

                requestTime = requestTime + dt.Year.ToString();

                requestTime = requestTime + "_" + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString();

                ReportOutputPath = ReportOutputPath + pReportRequestBE.ReportRequestID.ToString() + "_";
                ReportOutputPath = ReportOutputPath + pReportRequestBE.ReportName + "_";
                if (string.IsNullOrEmpty(pReportRequestBE.ReportType))
                    ReportOutputPath = ReportOutputPath + pReportRequestBE.UserID + "_" + requestTime + ".xls";
                else
                    ReportOutputPath = ReportOutputPath + pReportRequestBE.ReportType + "_" + pReportRequestBE.UserID + "_" + requestTime + ".xls";

                File.WriteAllBytes(ReportOutputPath, bytes);

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Report Request Id " + pReportRequestBE.ReportRequestID.ToString() + " Successfully Generated");
                sbMessage.Append("\r\n");

                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

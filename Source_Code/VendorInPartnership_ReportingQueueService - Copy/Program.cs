﻿using System;
using System.Collections.Generic;
using System.Threading;
using BusinessEntities.ModuleBE.ReportRequest;
using BusinessLogicLayer.ModuleBAL.ReportRequest;
using Utilities;
using System.Configuration;
using System.Text;
using System.IO;
using System.Linq;
using System.Diagnostics;
using VIP_ReportingQueueService.ModuleUI.StockOverview.StockTurn;
using VIP_ReportingQueueService.ModuleUI.OnTimeInFull;
using VIP_ReportingQueueService.ModuleUI.StockOverview;

namespace VIP_ReportingQueueService {

    public static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        //[STAThread]
        static void Main() {

            var sourceFilePath = AppDomain.CurrentDomain.BaseDirectory;
           
            StringBuilder sbMessage = new StringBuilder();
            sbMessage.Append("Started at            : " + DateTime.Now.ToString());
            sbMessage.Append("\r\n");
            sbMessage.Append("Folder              : " + sourceFilePath + @"ReportOutput\");           
            sbMessage.Append("\r\n");
            sbMessage.Append("****************************************************************************************");
            sbMessage.Append("\r\n");
            LogUtility.SaveTraceLogEntry(sbMessage);

            ReportRequestBE oReportRequestBE = new ReportRequestBE();
            ReportRequestBE oUpdateStatusBE = new ReportRequestBE();
            ReportRequestBAL oReportRequestBAL = new ReportRequestBAL();

            OTIFProcessQueue oOTIFProcessQueue = new OTIFProcessQueue();
            STKProcessQueue oSTKProcessQueue = new STKProcessQueue();
            StockTurnProcessQueue oStockTurnProcessQueue = new StockTurnProcessQueue();
            RMSCategoryProcessQueue oRmsCategoryProcessQueue = new RMSCategoryProcessQueue();
            ItemProcessQueue oItemProcessQueue = new ItemProcessQueue();
            List<ReportRequestBE> oReportRequestBEList = new List<ReportRequestBE>();

           
            bool IsReportStatusUpdated = false;
                      

            while (true) {
                try {
            
                    IsReportStatusUpdated = false;
                    DeletingMoreThanSevenDaysFileAndRecords();
                    oReportRequestBEList = null;
                    oReportRequestBE = new ReportRequestBE();
                    oReportRequestBE.Action = "GetReportRequestByReportStatus";
                    oReportRequestBE.RequestStatus = "Pending";
                    oReportRequestBEList = oReportRequestBAL.GetReportRequestByReportStatusBAL(oReportRequestBE);

                    GC.Collect();
                    if (oReportRequestBEList.Count == 0) {
                        //Thread.Sleep(30000);
                        //IsReportStatusUpdated = true;
                        //continue;                      
                       Environment.Exit(0);
                    }

                    oUpdateStatusBE.Action = "UpdateReportStatus";
                    oUpdateStatusBE.ReportRequestID = oReportRequestBEList[0].ReportRequestID;
                    oUpdateStatusBE.RequestStatus = "InProgress";
                    oReportRequestBAL.UpdateReportStatusBAL(oUpdateStatusBE);

                    sbMessage.Clear();
                    sbMessage.Append("Report Status Updated Pending for : " + Convert.ToString(oReportRequestBEList[0].ReportRequestID));
                    LogUtility.SaveTraceLogEntry(sbMessage);

                    if (oReportRequestBEList[0].ModuleName == "OTIF") {
                        sbMessage.Clear();
                        sbMessage.Append("Report Request for OTIF");
                        LogUtility.SaveTraceLogEntry(sbMessage);

                        oOTIFProcessQueue.GenerateOTIFReport(oReportRequestBEList[0]);
                    }
                    else if (oReportRequestBEList[0].ModuleName == "StockOverview") {
                        sbMessage.Clear();
                        sbMessage.Append("Report Request for StockOverview");
                        LogUtility.SaveTraceLogEntry(sbMessage);

                        oSTKProcessQueue.GenerateSTKReport(oReportRequestBEList[0]);
                    }

                    else if (oReportRequestBEList[0].ModuleName == "Stock Turn")
                    {
                        sbMessage.Clear();
                        sbMessage.Append("Report Request for StockTurn");
                        LogUtility.SaveTraceLogEntry(sbMessage);

                        oStockTurnProcessQueue.GenerateStockTurnReport(oReportRequestBEList[0]);
                    }
                    else if (oReportRequestBEList[0].ModuleName == "RMSCategory")
                    {
                        sbMessage.Clear();
                        sbMessage.Append("Report Request for RMS Category");
                        LogUtility.SaveTraceLogEntry(sbMessage);

                        oRmsCategoryProcessQueue.GenerateRmsCategoryReport(oReportRequestBEList[0]);
                    }
                    else if (oReportRequestBEList[0].ModuleName == "StockOverview_ItemSummaryReport")
                    {
                        sbMessage.Clear();
                        sbMessage.Append("Report Request for StockOverview");
                        LogUtility.SaveTraceLogEntry(sbMessage);

                        oItemProcessQueue.GenerateItemSummaryReport(oReportRequestBEList[0]);
                    }

                    oUpdateStatusBE.Action = "UpdateReportStatus";
                    oUpdateStatusBE.ReportRequestID = oReportRequestBEList[0].ReportRequestID;
                    oUpdateStatusBE.RequestStatus = "Generated";
                    oUpdateStatusBE.ReportGeneratedTime = DateTime.Now;
                    oReportRequestBAL.UpdateReportStatusBAL(oUpdateStatusBE);
                    oUpdateStatusBE.ReportGeneratedTime = null;
                    
                    IsReportStatusUpdated = true;

                    sbMessage.Clear();
                    sbMessage.Append("Report Status Updated Generated for : " + Convert.ToString(oReportRequestBEList[0].ReportRequestID));
                    sbMessage.Append("\r\n");
                    sbMessage.Append("****************************************************************************************");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                }
                catch (Exception ex) {
                   
                    oUpdateStatusBE.Action = "UpdateReportStatus";
                    oUpdateStatusBE.ReportRequestID = oReportRequestBEList[0].ReportRequestID;
                    oUpdateStatusBE.RequestStatus = "Failed";
                    oReportRequestBAL.UpdateReportStatusBAL(oUpdateStatusBE);
                    oUpdateStatusBE.ReportGeneratedTime = null;

                    IsReportStatusUpdated = true;

                    sbMessage.Clear();
                    sbMessage.Append("Report Generation Failed:" + oReportRequestBEList[0].ReportRequestID.ToString());
                    sbMessage.Append("\r\n");
                    sbMessage.Append("****************************************************************************************");
                    LogUtility.SaveTraceLogEntry(sbMessage);

                    LogUtility.SaveErrorLogEntry(ex);

                    //send the mail to concerned person
                    SendEmailToAdmin(oReportRequestBEList[0].ReportRequestID, ex);
                }
                finally {
                    if (IsReportStatusUpdated == false) {
                        oUpdateStatusBE.Action = "UpdateReportStatus";
                        oUpdateStatusBE.ReportRequestID = oReportRequestBEList[0].ReportRequestID;
                        oUpdateStatusBE.RequestStatus = "Failed";
                        oReportRequestBAL.UpdateReportStatusBAL(oUpdateStatusBE);
                        oUpdateStatusBE.ReportGeneratedTime = null;
                    }
                }
            }

           
            sbMessage.Clear();
            sbMessage.Append("\r\n");
            sbMessage.Append("Ended at              : " + DateTime.Now.ToString());
           
            sbMessage.Append("\r\n");
            sbMessage.Append("****************************************************************************************");
            sbMessage.Append("\r\n");
            LogUtility.SaveTraceLogEntry(sbMessage);
        }

    
        static void DeletingMoreThanSevenDaysFileAndRecords()
        {
            var ReportRequestBE = new ReportRequestBE();
            var ReportRequestBAL = new ReportRequestBAL();
            var sourceFilePath = AppDomain.CurrentDomain.BaseDirectory;

            try {
                sourceFilePath = sourceFilePath + @"ReportOutput\";
                DirectoryInfo di = new DirectoryInfo(sourceFilePath);

                // Logic to get here the all OldReportRequestIDs which are less than 7 days.
                ReportRequestBE.Action = "GetOldReportRequestIDToDeleteFiles";
                ReportRequestBE.UserID = 0;
                var lstRequestIDs = ReportRequestBAL.GetOldReportRequestIDsBAL(ReportRequestBE);

                // Logic to delete the excel files from folder corresponding to OldReportRequestId.
                foreach (int requestId in lstRequestIDs) {
                    foreach (FileInfo fi in di.GetFiles("*.xls")) {
                        int subIndex = fi.Name.IndexOf("_");
                        int reportRequestId = Convert.ToInt32(fi.Name.Substring(0, subIndex));
                        if (reportRequestId.Equals(requestId)) {
                            // Here excel file is being delete corresponding to OldReportRequestId.
                            fi.Delete();
                            break;
                        }
                    }
                }
            }
            catch (Exception ex) {
                //StringBuilder sbMessage = new StringBuilder();
                //sbMessage.Append("Error in deleting more than seven days old excel fiels : " + ex.InnerException + " " + ex.Message);
                //sbMessage.Append("\r\n");
                //sbMessage.Append("****************************************************************************************");
                //sbMessage.Append("\r\n");
                //LogUtility.SaveTraceLogEntry(sbMessage);
                throw ex;
            }
            finally
            {
                // Logic to deleting all records which are seven days old.
                ReportRequestBE = new ReportRequestBE();
                ReportRequestBE.Action = "DeleteSevenDaysOldReportRequest";
                ReportRequestBE.ReportRequestID = 0;
                ReportRequestBAL.DeleteReportRequestBAL(ReportRequestBE);
            }
        }

        static void SendEmailToAdmin(int requestId, Exception ex) {
            try {
                clsEmail mail = new clsEmail();
                string htmlBody = string.Empty;

                string sFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                string toAddress = Convert.ToString(ConfigurationManager.AppSettings["toAddress"]);
                string mailSubjectText = "Unexpected Error occured while processing the request queue ID - " + requestId.ToString();
                string EmailMessageText = string.Empty;
              
                EmailMessageText = "An unexpected error occured while processing the report from request queue, while this error has been logged, here are the error details for your action : ";
                EmailMessageText += ex.Message;
                if (ex.InnerException != null)
                    EmailMessageText += " " + ex.InnerException.Message;
              
                clsEmail oclsEmail = new clsEmail();
                oclsEmail.sendMail(toAddress, EmailMessageText, mailSubjectText, sFromAddress, true);
            }
            catch (Exception ex1) {
                LogUtility.SaveErrorLogEntry(ex1, "Mail failed for Error Exception");                
            }
        }
    }
}

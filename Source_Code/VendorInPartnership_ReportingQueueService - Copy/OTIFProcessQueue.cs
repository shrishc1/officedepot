﻿using System;
using System.Text;
using System.Data;
using Microsoft.Reporting.WinForms;
using System.IO;
using BusinessEntities.ModuleBE.OnTimeInFull.Report;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.Report;
using BusinessEntities.ModuleBE.ReportRequest;
using Utilities;
using System.Threading;
using System.Linq;
using System.Collections.Generic; 
using System.Configuration;

namespace VIP_ReportingQueueService
{
    public class OTIFProcessQueue
    {
        public void GenerateOTIFReport(ReportRequestBE pReportRequestBE)
        {

            try
            {
                OTIFReportBE oOTIFReportBE = new OTIFReportBE();
                OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();
                DataSet dsOTIFReport;

                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                path = path.Replace(@"\bin\debug", "");
                string rptTab = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["RPTtab"]);

                if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelMonthlyReportNewMesaure" && rptTab == "1")
                {
                    //Thread.Sleep(30000);
                    oOTIFReportBE.Action = "OTIF_ODLineLevelMonthlyReportNewMesaure";
                    oOTIFReportBE.ReportRequestID = pReportRequestBE.ReportRequestID;
                    dsOTIFReport = oOTIFReportBAL.GetOTIFReportFromTabBAL(oOTIFReportBE);
                }
                else
                {
                    //Get Report Data
                    oOTIFReportBE.Action = string.IsNullOrEmpty(pReportRequestBE.ReportAction) ? null : pReportRequestBE.ReportAction;
                    oOTIFReportBE.DateFrom = pReportRequestBE.DateFrom;
                    oOTIFReportBE.DateTo = pReportRequestBE.DateTo;
                    oOTIFReportBE.PurchaseOrderDue = pReportRequestBE.PurchaseOrderDue;
                    oOTIFReportBE.PurchaseOrderReceived = pReportRequestBE.PurchaseOrderReceived;
                    oOTIFReportBE.SelectedCountryIDs = string.IsNullOrEmpty(pReportRequestBE.CountryIDs) ? null : pReportRequestBE.CountryIDs;
                    oOTIFReportBE.SelectedSiteIDs = string.IsNullOrEmpty(pReportRequestBE.SiteIDs) ? null : pReportRequestBE.SiteIDs;
                    oOTIFReportBE.SelectedStockPlannerIDs = string.IsNullOrEmpty(pReportRequestBE.StockPlannerIDs) ? null : pReportRequestBE.StockPlannerIDs;
                    oOTIFReportBE.SelectedVendorIDs = string.IsNullOrEmpty(pReportRequestBE.VendorIDs) ? null : pReportRequestBE.VendorIDs;
                    oOTIFReportBE.SelectedSKUCodes = string.IsNullOrEmpty(pReportRequestBE.OD_SKU_No) ? null : pReportRequestBE.OD_SKU_No;
                    oOTIFReportBE.SelectedPurchaseOrderNo = string.IsNullOrEmpty(pReportRequestBE.PurchaseOrder) ? null : pReportRequestBE.PurchaseOrder;
                    oOTIFReportBE.SelectedItemClassification = string.IsNullOrEmpty(pReportRequestBE.ItemClassification) ? null : pReportRequestBE.ItemClassification;
                    oOTIFReportBE.EuropeanorLocal = pReportRequestBE.EuropeanorLocal;
                    oOTIFReportBE.SelectedRMSCategoryIDs = string.IsNullOrEmpty(pReportRequestBE.SelectedRMSCategoryIDs) ? null : pReportRequestBE.SelectedRMSCategoryIDs;
                    oOTIFReportBE.SelectedSKuGroupingIDs = string.IsNullOrEmpty(pReportRequestBE.SelectedSKuGroupingIDs) ? null : pReportRequestBE.SelectedSKuGroupingIDs;
                    oOTIFReportBE.StockPlannerGroupingIDs = string.IsNullOrEmpty(pReportRequestBE.StockPlannerGroupingIDs) ? null : pReportRequestBE.StockPlannerGroupingIDs;
                    oOTIFReportBE.CustomerIDs = string.IsNullOrEmpty(pReportRequestBE.CustomerIDs) ? null : pReportRequestBE.CustomerIDs;
                    dsOTIFReport = oOTIFReportBAL.GetOTIFReportBAL(oOTIFReportBE);
                }
                oOTIFReportBAL = null;

                StringBuilder sbMessage = new StringBuilder();

                sbMessage.Append("\r\n");

                if (dsOTIFReport == null)
                {
                    sbMessage.Append("Record count         : null");
                }
                else
                {
                    sbMessage.Append("Record count          : " + dsOTIFReport.Tables[0].Rows.Count.ToString());
                }

                LogUtility.SaveTraceLogEntry(sbMessage);

                if (dsOTIFReport != null && dsOTIFReport.Tables.Count > 0 && dsOTIFReport.Tables[0].Rows.Count > 0)
                {
                    // Prepare RDLC
                    StringBuilder ReportOutputPath = new StringBuilder();
                    ReportDataSource rds;
                    ReportViewer ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
                    ReportParameter[] reportParameter = null;
                    DateTime dt;
                    StringBuilder sDateFrom = new StringBuilder();
                    StringBuilder sDateTo = new StringBuilder();
                    StringBuilder requestTime = new StringBuilder();

                    #region "string Date"

                    sbMessage.Append("\r\n");

                    #region "string Date"
                    if (pReportRequestBE.DateFrom != null)
                    {
                        sDateFrom.Append(pReportRequestBE.DateFrom.Value.Day.ToString().Length == 1
                            ? "0" + pReportRequestBE.DateFrom.Value.Day.ToString()
                            : pReportRequestBE.DateFrom.Value.Day.ToString());

                        sDateFrom.Append(pReportRequestBE.DateFrom.Value.Month.ToString().Length == 1
                            ? "/" + "0" + pReportRequestBE.DateFrom.Value.Month.ToString()
                            : "/" + pReportRequestBE.DateFrom.Value.Month.ToString());

                        sDateFrom.Append("/" + pReportRequestBE.DateFrom.Value.Year.ToString());
                    }


                    if (pReportRequestBE.DateTo != null)
                    {
                        sDateTo.Append(pReportRequestBE.DateTo.Value.Date.ToString().Length == 1
                            ? "0" + pReportRequestBE.DateTo.Value.Day.ToString()
                            : pReportRequestBE.DateTo.Value.Day.ToString());

                        sDateTo.Append(pReportRequestBE.DateTo.Value.Month.ToString().Length == 1
                            ? "/" + "0" + pReportRequestBE.DateTo.Value.Month.ToString()
                            : "/" + pReportRequestBE.DateTo.Value.Month.ToString());

                        sDateTo.Append("/" + pReportRequestBE.DateTo.Value.Year.ToString());
                    }
                    #endregion
                    #endregion

                    #region ReportVariables

                    Warning[] warnings;
                    string[] streamIds;
                    string mimeType = string.Empty;
                    string encoding = string.Empty;
                    string extension = string.Empty;
                    string reportFileName = string.Empty;

                    #endregion

                    ReportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;

                    switch (pReportRequestBE.ReportName)
                    {

                        case "OTIF Report":
                            sbMessage.AppendLine("OTIF Report");
                            reportParameter = new ReportParameter[10];

                            sbMessage.AppendLine("reportParameter[0]: Country" + pReportRequestBE.CountryName);
                            reportParameter[0] = new ReportParameter("Country", pReportRequestBE.CountryName);

                            sbMessage.AppendLine("reportParameter[1]: " + "Site" + pReportRequestBE.SiteName);
                            reportParameter[1] = new ReportParameter("Site", pReportRequestBE.SiteName);

                            sbMessage.AppendLine("reportParameter[2]: " + "Vendor" + pReportRequestBE.VendorName);
                            reportParameter[2] = new ReportParameter("Vendor", pReportRequestBE.VendorName);

                            sbMessage.AppendLine("reportParameter[3]: " + "DateFrom" + sDateFrom);
                            reportParameter[3] = new ReportParameter("DateFrom", sDateFrom.ToString());

                            sbMessage.AppendLine("reportParameter[4]: " + "DateTo" + sDateTo);
                            reportParameter[4] = new ReportParameter("DateTo", sDateTo.ToString());

                            sbMessage.AppendLine("reportParameter[5]: " + "ItemClassification" + pReportRequestBE.ItemClassification);
                            reportParameter[5] = new ReportParameter("ItemClassification", pReportRequestBE.ItemClassification);

                            sbMessage.AppendLine("reportParameter[6]:" + "ODSKUCode" + pReportRequestBE.OD_SKU_No);
                            reportParameter[6] = new ReportParameter("ODSKUCode", pReportRequestBE.OD_SKU_No);

                            sbMessage.AppendLine("reportParameter[7]: " + "PurchaseOrder" + pReportRequestBE.PurchaseOrder);
                            reportParameter[7] = new ReportParameter("PurchaseOrder", pReportRequestBE.PurchaseOrder);

                            sbMessage.AppendLine("reportParameter[8]: " + "StockPlanner" + pReportRequestBE.StockPlannerName);
                            reportParameter[8] = new ReportParameter("StockPlanner", pReportRequestBE.StockPlannerName);

                            string TitleType = string.Empty;

                            if (oOTIFReportBE.PurchaseOrderDue == 1)
                                TitleType = "Purchase Order Due";

                            if (oOTIFReportBE.PurchaseOrderReceived == 1)
                                TitleType = "Purchase Order Received";

                            if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelMonthlyReport")
                            {
                                sbMessage.AppendLine("reportParameter[9]: " + "Title" + " Office Depot Line Level Monthly Report - Old OTIF Measure" + " (" + TitleType + ") ");
                                reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Monthly Report - Old OTIF Measure" + " (" + TitleType + ") ");
                            }
                            else if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelMonthlyReportNewMesaure")
                            {
                                sbMessage.AppendLine("reportParameter[9]: " + "Title" + " Office Depot Line Level Monthly Report - New OTIF Measure" + " (" + TitleType + ") ");
                                reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Monthly Report - New OTIF Measure" + " (" + TitleType + ") ");
                            }
                            else if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelMonthlyReportAbsolute")
                            {
                                sbMessage.AppendLine("reportParameter[9]:" + "Title" + " Office Depot Line Level Monthly Report - Absolute OTIF Measure" + " (" + TitleType + ") ");
                                reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Monthly Report - Absolute OTIF Measure" + " (" + TitleType + ") ");
                            }
                            else if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelSummaryReport")
                            {
                                sbMessage.AppendLine("reportParameter[9]:" + "Title" + " Office Depot Line Level Summary Report - Old OTIF Summary" + " (" + TitleType + ") ");
                                reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Summary Report - Old OTIF Summary" + " (" + TitleType + ") ");
                            }
                            else if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelSummaryReportNew")
                            {
                                sbMessage.AppendLine("reportParameter[9]:" + "Title" + " Office Depot Line Level Summary Report - New OTIF Summary" + " (" + TitleType + ") ");
                                reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Summary Report - New OTIF Summary" + " (" + TitleType + ") ");
                            }
                            else if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelSummaryReportAbsolute")
                            {
                                sbMessage.AppendLine("reportParameter[9]: " + "Title" + " Office Depot Line Level Summary Report - Absolute OTIF Summary" + " (" + TitleType + ") ");
                                reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Summary Report - Absolute OTIF Summary" + " (" + TitleType + ") ");
                            }
                            else if (pReportRequestBE.ReportAction == "OTIF_VendorLineLevelDetailedReport")
                            {
                                sbMessage.AppendLine("reportParameter[9]:" + "Title" + " Vendor Line Level Detailed Report" + " (" + TitleType + ") ");
                                reportParameter[9] = new ReportParameter("Title", " Vendor Line Level Detailed Report" + " (" + TitleType + ") ");
                            }
                            else if (pReportRequestBE.ReportAction == "OTIF_ODLineLevelDetailedReport")
                            {
                                sbMessage.AppendLine("reportParameter[9]: " + "Title" + " Office Depot Line Level Detailed Report" + " (" + TitleType + ") ");
                                reportParameter[9] = new ReportParameter("Title", " Office Depot Line Level Detailed Report" + " (" + TitleType + ") ");
                            }
                            break;

                        case "Master Vendor OTIF Report":
                            sbMessage.AppendLine("Master Vendor OTIF Report");
                            reportParameter = new ReportParameter[4];
                            sbMessage.AppendLine("reportParameter[0]: " + "Vendor :" + pReportRequestBE.VendorName);
                            reportParameter[0] = new ReportParameter("Vendor", pReportRequestBE.VendorName);
                            sbMessage.AppendLine("reportParameter[1]: " + "DateFrom :" + sDateFrom.ToString());
                            reportParameter[1] = new ReportParameter("DateFrom", sDateFrom.ToString());
                            sbMessage.AppendLine("reportParameter[2]: " + "DateTo :" + sDateTo.ToString());
                            reportParameter[2] = new ReportParameter("DateTo", sDateTo.ToString());

                            if (pReportRequestBE.ReportAction == "OTIF_MasterVendorReportNewMeasure")
                            {
                                sbMessage.AppendLine("reportParameter[3]: " + "Title" + "Monthly (NEW Measure)");
                                reportParameter[3] = new ReportParameter("Title", "Monthly (NEW Measure)");
                            }
                            else if (pReportRequestBE.ReportAction == "OTIF_MasterVendorReportOldMeasure")
                            {
                                sbMessage.AppendLine("reportParameter[3]: " + "Title" + "Monthly (OLD Measure)");
                                reportParameter[3] = new ReportParameter("Title", "Monthly (OLD Measure)");
                            }
                            else if (pReportRequestBE.ReportAction == "OTIF_MasterVendorReportAbsolute")
                            {
                                sbMessage.AppendLine("reportParameter[3]: " + "Title" + "Monthly (Absolute)");
                                reportParameter[3] = new ReportParameter("Title", "Monthly (Absolute)");
                            }
                            break;

                        case "OTIF Tracking Report":
                            reportParameter = new ReportParameter[4];
                            sbMessage.AppendLine("OTIF Tracking Report");
                            sbMessage.AppendLine("reportParameter[0]: Country" + pReportRequestBE.CountryName);
                            reportParameter[0] = new ReportParameter("Country", pReportRequestBE.CountryName);
                            sbMessage.AppendLine("reportParameter[1]: " + "Vendor" + pReportRequestBE.VendorName);
                            reportParameter[1] = new ReportParameter("Vendor", pReportRequestBE.VendorName);
                            sbMessage.AppendLine("reportParameter[2]: " + "DateFrom" + sDateFrom);
                            reportParameter[2] = new ReportParameter("DateFrom", sDateFrom.ToString());
                            sbMessage.AppendLine("reportParameter[3]: " + "DateTo" + sDateTo);
                            reportParameter[3] = new ReportParameter("DateTo", sDateTo.ToString());
                            break;

                        case "Lead Time Variance":
                        case "OTIF Stock Planner Report":
                            sbMessage.AppendLine("Lead Time Variance" + " or " + "OTIF Stock Planner Report");
                            reportParameter = new ReportParameter[9];
                            sbMessage.AppendLine("reportParameter[0]: " + "Country :" + pReportRequestBE.CountryName);
                            reportParameter[0] = new ReportParameter("Country", pReportRequestBE.CountryName);
                            sbMessage.AppendLine("reportParameter[1]: " + "Site:" + pReportRequestBE.SiteName);
                            reportParameter[1] = new ReportParameter("Site", pReportRequestBE.SiteName);
                            sbMessage.AppendLine("reportParameter[2]: " + "Vendor:" + pReportRequestBE.VendorName);
                            reportParameter[2] = new ReportParameter("Vendor", pReportRequestBE.VendorName);
                            sbMessage.AppendLine("reportParameter[3]: " + "DateFrom:" + sDateFrom);
                            reportParameter[3] = new ReportParameter("DateFrom", sDateFrom.ToString());
                            sbMessage.AppendLine("reportParameter[4]: " + "DateTo:" + sDateTo);
                            reportParameter[4] = new ReportParameter("DateTo", sDateTo.ToString());
                            sbMessage.AppendLine("reportParameter[5]: " + "ItemClassification:" + pReportRequestBE.ItemClassification);
                            reportParameter[5] = new ReportParameter("ItemClassification", pReportRequestBE.ItemClassification);
                            sbMessage.AppendLine("reportParameter[6]: " + "ODSKUCode:" + pReportRequestBE.OD_SKU_No);
                            reportParameter[6] = new ReportParameter("ODSKUCode", pReportRequestBE.OD_SKU_No);
                            sbMessage.AppendLine("reportParameter[7]: " + "PurchaseOrder:" + pReportRequestBE.PurchaseOrder);
                            reportParameter[7] = new ReportParameter("PurchaseOrder", pReportRequestBE.PurchaseOrder);
                            sbMessage.AppendLine("reportParameter[8]: " + "StockPlanner" + pReportRequestBE.StockPlannerName);
                            reportParameter[8] = new ReportParameter("StockPlanner", pReportRequestBE.StockPlannerName);
                            break;

                    }

                    sbMessage.AppendLine(pReportRequestBE.ReportDatatableName);

                    DataTable dataTable = dsOTIFReport.Tables[0];

                    int batchSize = Convert.ToInt32(ConfigurationManager.AppSettings["BatchSize"]);

                    List<DataTable> lstTab = SplitTable(dataTable, batchSize);
                    List<byte[]> lstBytes = new List<byte[]>();
                    foreach (var item in lstTab)
                    {
                        rds = new ReportDataSource(pReportRequestBE.ReportDatatableName, item);
                        sbMessage.AppendLine(path + @"ModuleUI\OnTimeInFull\RDLC\" + pReportRequestBE.ReportNamePath);
                        ReportViewer1.LocalReport.ReportPath = path + @"ModuleUI\OnTimeInFull\RDLC\" + pReportRequestBE.ReportNamePath;
                        ReportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;

                        //new line added
                        ReportViewer1.LocalReport.ReportEmbeddedResource = path + @"ModuleUI\OnTimeInFull\RDLC\" + "assemblyname.subnamespace." + pReportRequestBE.ReportNamePath; ;
                        ReportViewer1.LocalReport.Refresh();
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.LocalReport.SetParameters(reportParameter);
                        sbMessage.AppendLine("Reports are working on bytes genaeration starts.");
                        lstBytes.Add(ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings));
                        sbMessage.AppendLine("Reports are working on bytes genaeration ends.");

                        sbMessage.Append("\r\n");
                        sbMessage.AppendLine("Total Records :"+item.Rows.Count.ToString());
                        LogUtility.SaveTraceLogEntry(sbMessage );
                        sbMessage.Clear();
                    }

                    sbMessage.AppendLine("Reports path:" + path + @"ReportOutput\");

                    ReportOutputPath.Append(path + @"ReportOutput\");
                    if (!System.IO.Directory.Exists(ReportOutputPath.ToString()))
                        System.IO.Directory.CreateDirectory(ReportOutputPath.ToString());

                    sbMessage.AppendLine("Directory exists at" + ReportOutputPath);


                    dt = Convert.ToDateTime(pReportRequestBE.RequestTime);

                    requestTime = new StringBuilder();
                    requestTime.Append(dt.Day.ToString().Length == 1 ? "0" + dt.Day.ToString() : dt.Day.ToString());
                    requestTime.Append(dt.Month.ToString().Length == 1 ? "0" + dt.Month.ToString() : dt.Month.ToString());
                    requestTime.Append(dt.Year.ToString());
                    requestTime.Append("_" + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString());

                    sbMessage.AppendLine("Requested Time:" + requestTime.ToString());

                    ReportOutputPath.Append(pReportRequestBE.ReportRequestID.ToString() + "_");
                    ReportOutputPath.Append(pReportRequestBE.ReportName + "_");
                    ReportOutputPath.Append(string.IsNullOrEmpty(pReportRequestBE.ReportType)
                        ? pReportRequestBE.UserID + "_" + requestTime.ToString() + ".xls"
                        : pReportRequestBE.ReportType + "_" + pReportRequestBE.UserID + "_" + requestTime.ToString() + ".xls");

                    sbMessage.AppendLine("ReportOutputPath:" + ReportOutputPath.ToString());

                    sbMessage.AppendLine("Files writing started.");
                     
                    byte[] array = lstBytes.SelectMany(a => a).ToArray();

                    File.WriteAllBytes(ReportOutputPath.ToString(), array);
                    sbMessage.AppendLine("Files writing ended.");
                    sbMessage.Append("Report Request Id " + pReportRequestBE.ReportRequestID.ToString() + " Successfully Generated");
                    sbMessage.Append("\r\n");

                    LogUtility.SaveTraceLogEntry(sbMessage);
                }
            }
            catch (Exception ex)
            {
                if(ex.InnerException !=null && ex.InnerException.InnerException != null)
                {
                    LogUtility.SaveErrorLogEntry(ex.InnerException.InnerException, ex.Message);
                }
                LogUtility.SaveErrorLogEntry(ex);
                throw ex;
            }
        }

        
        private static List<DataTable> SplitTable(DataTable originalTable, int batchSize)
        {
            List<DataTable> tables = new List<DataTable>();
            int i = 0;
            int j = 1;
            DataTable newDt = originalTable.Clone();
            newDt.TableName = "Table_" + j;
            newDt.Clear();
            foreach (DataRow row in originalTable.Rows)
            {
                DataRow newRow = newDt.NewRow();
                newRow.ItemArray = row.ItemArray;
                newDt.Rows.Add(newRow);
                i++;
                if (i == batchSize)
                {
                    tables.Add(newDt);
                    j++;
                    newDt = originalTable.Clone();
                    newDt.TableName = "Table_" + j;
                    newDt.Clear();
                    i = 0;
                }
            }
            return tables;
        }
    }
}

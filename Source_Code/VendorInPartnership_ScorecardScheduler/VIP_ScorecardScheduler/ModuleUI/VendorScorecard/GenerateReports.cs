﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Reporting.WinForms;
using System.IO;
using BusinessEntities.ModuleBE.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using Utilities;
using System.Threading;
using System.Collections.Concurrent;

namespace VIP_ScorecardScheduler
{
    class GenerateReports
    {
        public bool GenerateReportExcelFiles(int year,bool processRunning)
        {
            //year = 2014;
            #region Declarations ...
            var upVendorBAL = new UP_VendorBAL();
            var upVendorBE = new UP_VendorBE();
            var summaryScoreCardBAL = new SummaryScoreCardBAL();
            var summaryScoreCardBE = new SummaryScoreCardBE();
            var sbMessage = new StringBuilder();
            #endregion

            sbMessage.Clear();
            sbMessage.Append("Step 4 : Generating for year           : " + year);
            sbMessage.Append("\r\n");
            sbMessage.Append("****************************************************************************************");
            sbMessage.Append("\r\n");
            LogUtility.SaveTraceLogEntry(sbMessage);

            if (processRunning)
            {
                #region Getting OD view - (European, Local and All) & Vendor View data from database ...

                /* Start Getting here OD Summary European otif option Data. */
                summaryScoreCardBE.Action = "ODSummary";
                summaryScoreCardBE.Year = year;
                summaryScoreCardBE.EuropeanorLocal = "1"; // Here 1 for European data
                summaryScoreCardBE.ModuleType = "OTIF";
                var getODSummaryEuropean = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryEuropean == null)
                {
                    sbMessage.Append("OD Summary European otif Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary European otif Record count is  : {0}", Convert.ToString(getODSummaryEuropean.Rows.Count)));
                    GenerateReport(getODSummaryEuropean, -1, year, "OTIF");
                    getODSummaryEuropean.Dispose();
                    GC.Collect();
                }

                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary European otif report. */

                /* Start Getting here OD Summary European Stock option Data. */
                summaryScoreCardBE.Action = "ODSummary";
                summaryScoreCardBE.Year = year;
                summaryScoreCardBE.EuropeanorLocal = "1"; // Here 1 for European data
                summaryScoreCardBE.ModuleType = "Stock";
                getODSummaryEuropean = null;
                getODSummaryEuropean = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryEuropean == null)
                {
                    sbMessage.Append("OD Summary European  Stock Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary European Stock Record count is  : {0}", Convert.ToString(getODSummaryEuropean.Rows.Count)));
                    GenerateReport(getODSummaryEuropean, -1, year, "Stock");
                    getODSummaryEuropean.Dispose();
                    GC.Collect();
                }

                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary European Score report. */


                /* Start Getting here OD Summary European Discrepancies option Data. */
                summaryScoreCardBE.Action = "ODSummary";
                summaryScoreCardBE.Year = year;
                summaryScoreCardBE.EuropeanorLocal = "1"; // Here 1 for European data
                summaryScoreCardBE.ModuleType = "Discrepancies";
                getODSummaryEuropean = null;
                getODSummaryEuropean = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryEuropean == null)
                {
                    sbMessage.Append("OD Summary European  Discrepancies Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary European Discrepancies Record count is  : {0}", Convert.ToString(getODSummaryEuropean.Rows.Count)));
                    GenerateReport(getODSummaryEuropean, -1, year, "Discrepancies");
                    getODSummaryEuropean.Dispose();
                    GC.Collect();
                }

                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary European Discrepancies report. */


                /* Start Getting here OD Summary European Scheduling option Data. */
                summaryScoreCardBE.Action = "ODSummary";
                summaryScoreCardBE.Year = year;
                summaryScoreCardBE.EuropeanorLocal = "1"; // Here 1 for European data
                summaryScoreCardBE.ModuleType = "Scheduling";
                getODSummaryEuropean = null;
                getODSummaryEuropean = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryEuropean == null)
                {
                    sbMessage.Append("OD Summary European Scheduling Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary European Scheduling Record count is  : {0}", Convert.ToString(getODSummaryEuropean.Rows.Count)));
                    GenerateReport(getODSummaryEuropean, -1, year, "Scheduling");
                    getODSummaryEuropean.Dispose();
                    GC.Collect();
                }

                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary European Scheduling report. */



                /* Start Getting here OD Summary European OverallScore option Data. */
                summaryScoreCardBE.Action = "ODSummary";
                summaryScoreCardBE.Year = year;
                summaryScoreCardBE.EuropeanorLocal = "1"; // Here 1 for European data
                summaryScoreCardBE.ModuleType = "OverallScore";
                getODSummaryEuropean = null;
                getODSummaryEuropean = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryEuropean == null)
                {
                    sbMessage.Append("OD Summary European OverallScore Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary European OverallScore Record count is  : {0}", Convert.ToString(getODSummaryEuropean.Rows.Count)));
                    GenerateReport(getODSummaryEuropean, -1, year, "OverallScore");
                    getODSummaryEuropean.Dispose();
                    GC.Collect();
                }

                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary European report. */



                /* Start Getting here OD Summary Local OTIF option Data. */
                summaryScoreCardBE.EuropeanorLocal = "2"; // Here 2 for Local data
                summaryScoreCardBE.ModuleType = "OTIF";
                getODSummaryEuropean = null;
                var getODSummaryLocal = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryLocal == null)
                {
                    sbMessage.Append("OD Summary Local OTIF Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary Local OTIF Record count is  : {0}", Convert.ToString(getODSummaryLocal.Rows.Count)));
                    GenerateReport(getODSummaryLocal, -2, year, "OTIF");
                    getODSummaryLocal.Dispose();
                    GC.Collect();
                }
                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary Local OTIF report. */



                /* Start Getting here OD Summary Local Stock option Data. */
                summaryScoreCardBE.EuropeanorLocal = "2"; // Here 2 for Local data
                summaryScoreCardBE.ModuleType = "Stock";
                getODSummaryEuropean = null;
                getODSummaryLocal = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryLocal == null)
                {
                    sbMessage.Append("OD Summary Local Score Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary Local Stock Record count is  : {0}", Convert.ToString(getODSummaryLocal.Rows.Count)));
                    GenerateReport(getODSummaryLocal, -2, year, "Stock");
                    getODSummaryLocal.Dispose();
                    GC.Collect();
                }
                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary Local Stock report. */

                /* Start Getting here OD Summary Local Discrepancies option Data. */
                summaryScoreCardBE.EuropeanorLocal = "2"; // Here 2 for Local data
                summaryScoreCardBE.ModuleType = "Discrepancies";
                getODSummaryEuropean = null;
                getODSummaryLocal = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryLocal == null)
                {
                    sbMessage.Append("OD Summary Local Discrepancies Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary Local Discrepancies Record count is  : {0}", Convert.ToString(getODSummaryLocal.Rows.Count)));
                    GenerateReport(getODSummaryLocal, -2, year, "Discrepancies");
                    getODSummaryLocal.Dispose();
                    GC.Collect();
                }
                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary Local Discrepancies report. */

                /* Start Getting here OD Summary Local Scheduling option Data. */
                summaryScoreCardBE.EuropeanorLocal = "2"; // Here 2 for Local data
                summaryScoreCardBE.ModuleType = "Scheduling";
                getODSummaryEuropean = null;
                getODSummaryLocal = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryLocal == null)
                {
                    sbMessage.Append("OD Summary Local Scheduling Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary Local Scheduling Record count is  : {0}", Convert.ToString(getODSummaryLocal.Rows.Count)));
                    GenerateReport(getODSummaryLocal, -2, year, "Scheduling");
                    getODSummaryLocal.Dispose();
                    GC.Collect();
                }
                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary Local Scheduling report. */

                /* Start Getting here OD Summary Local OverallScore option Data. */
                summaryScoreCardBE.EuropeanorLocal = "2"; // Here 2 for Local data
                summaryScoreCardBE.ModuleType = "OverallScore";
                getODSummaryEuropean = null;
                getODSummaryLocal = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryLocal == null)
                {
                    sbMessage.Append("OD Summary Local OverallScore Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary Local OverallScore Record count is  : {0}", Convert.ToString(getODSummaryLocal.Rows.Count)));
                    GenerateReport(getODSummaryLocal, -2, year, "OverallScore");
                    getODSummaryLocal.Dispose();
                    GC.Collect();
                }
                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary Local Stock report. */


                /* Start Getting here OD Summary All option OTIF Data. */
                summaryScoreCardBE.EuropeanorLocal = "1,2"; // Here 1,2 for All data
                summaryScoreCardBE.ModuleType = "OTIF";
                getODSummaryEuropean = null;
                var getODSummaryAll = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryAll == null)
                {
                    sbMessage.Append("OD Summary All OTIF Report Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary All OTIF Report Record count is  : {0}", Convert.ToString(getODSummaryAll.Rows.Count)));
                    GenerateReport(getODSummaryAll, -3, year, "OTIF");
                    GC.Collect();
                }
                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary All OTIF report. */

                /* Start Getting here OD Summary All  option Score Data. */
                summaryScoreCardBE.EuropeanorLocal = "1,2"; // Here 1,2 for All data
                summaryScoreCardBE.ModuleType = "Stock";
                getODSummaryEuropean = null;
                getODSummaryAll = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryAll == null)
                {
                    sbMessage.Append("OD Summary All Stock Score Report Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary All  Stock Score Report Record count is  : {0}", Convert.ToString(getODSummaryAll.Rows.Count)));
                    GenerateReport(getODSummaryAll, -3, year, "Stock");
                    GC.Collect();
                }
                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary All Score report. */

                /* Start Getting here OD Summary All option Discrepancies Data. */
                summaryScoreCardBE.EuropeanorLocal = "1,2"; // Here 1,2 for All data
                summaryScoreCardBE.ModuleType = "Discrepancies";
                getODSummaryEuropean = null;
                getODSummaryAll = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryAll == null)
                {
                    sbMessage.Append("OD Summary All Discrepancies Report Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary All Discrepancies Report Record count is  : {0}", Convert.ToString(getODSummaryAll.Rows.Count)));
                    GenerateReport(getODSummaryAll, -3, year, "Discrepancies");
                    GC.Collect();
                }
                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary All Discrepancies report. */

                /* Start Getting here OD Summary All option Scheduling Data. */
                summaryScoreCardBE.EuropeanorLocal = "1,2"; // Here 1,2 for All data
                summaryScoreCardBE.ModuleType = "Scheduling";
                getODSummaryEuropean = null;
                getODSummaryAll = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryAll == null)
                {
                    sbMessage.Append("OD Summary All Scheduling Report Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary All Scheduling Report Record count is  : {0}", Convert.ToString(getODSummaryAll.Rows.Count)));
                    GenerateReport(getODSummaryAll, -3, year, "Scheduling");
                    GC.Collect();
                }
                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary All Scheduling report. */

                /* Start Getting here OD Summary All option OverallScore Data. */
                summaryScoreCardBE.EuropeanorLocal = "1,2"; // Here 1,2 for All data
                summaryScoreCardBE.ModuleType = "OverallScore";
                getODSummaryEuropean = null;
                getODSummaryAll = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (getODSummaryAll == null)
                {
                    sbMessage.Append("OD Summary OverallScore Stock Report Record count is  : null");
                }
                else
                {
                    sbMessage.Append(string.Format("OD Summary OverallScore OverallScore Report Record count is  : {0}", Convert.ToString(getODSummaryAll.Rows.Count)));
                    GenerateReport(getODSummaryAll, -3, year, "OverallScore");
                    GC.Collect();
                }
                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End Generating OD Summary All Stock report. */

                #endregion
            }
            summaryScoreCardBE.ModuleType = null;

            /* Generating Vendor Summary Grand Parent, Parent and Stand Alone report. */
            /* Getting here All Grand Parent, Parent and stand alone vendors Data. */
            upVendorBE.Action = "GetAllGrandParentParentAndStandAloneVendors_New";
            string vendorCountToCreate = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["VendorCountToCreateReport"]);
            if (!string.IsNullOrEmpty(vendorCountToCreate))
                upVendorBE.VendorCountToCreateReport = Convert.ToInt32(vendorCountToCreate);
            var getAllGrandParentAndStandAloneVendors = upVendorBAL.GetAllGrandParentAndStandAloneVendorsBAL(upVendorBE);

            sbMessage = new StringBuilder();
            sbMessage.Append("\r\n");
            if (getAllGrandParentAndStandAloneVendors == null)
            {
                sbMessage.Append("Vendor Summary Grand Parent, Parent and Stand Alone Report Record count is  : null");
                return true;
            }
            else
            {
                sbMessage.Append(string.Format("Vendor Summary Grand Parent, Parent and Stand Alone Report Record count is  : {0}",
                    Convert.ToString(getAllGrandParentAndStandAloneVendors.Count)));

                #region Declarations ...
                var path = AppDomain.CurrentDomain.BaseDirectory.ToLower().Replace(@"\bin\debug", string.Empty);
                sbMessage.Clear();
                sbMessage.Append("Step 5 : Generating reports for vendors at path           : " + path);
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                var ReportOutputPath = string.Empty;
                var mimeType = string.Empty;
                var encoding = string.Empty;
                var extension = string.Empty;
                var ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
                ReportDataSource reportDataSource;
                //ReportParameter[] reportParameter = null;
                Warning[] warnings;
                string[] streamIds;
                var reportTypeText = string.Empty;
                int iSleepCounter = 0;
                string ReportPath = string.Empty;

                ReportOutputPath = path + @"ReportOutput\" + year + @"\";
                if (!System.IO.Directory.Exists(ReportOutputPath))
                    System.IO.Directory.CreateDirectory(ReportOutputPath);

                #endregion


                foreach (UP_VendorBE vendorBE in getAllGrandParentAndStandAloneVendors)
                {
                    /* getting here Vendor Summary Data. */
                    summaryScoreCardBE.Action = "VendorSummary";
                    summaryScoreCardBE.Year = year;
                    summaryScoreCardBE.VendorID = vendorBE.VendorID;
                    DataSet dsSummaryScoreCard = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE);
                    var getVendorSummary = dsSummaryScoreCard.Tables.Count > 0 && dsSummaryScoreCard.Tables[0].Rows.Count > 0 ? dsSummaryScoreCard.Tables[0] : null;

                    //  var getVendorSummary = summaryScoreCardBAL.GetSummaryScoreCardBAL(summaryScoreCardBE).Tables[0];
                    //GenerateReport(getVendorSummary.Tables[0], vendorBE.VendorID, year);

                    if (getVendorSummary != null)
                    {
                        #region Generate report logic ...
                        try
                        {
                            ReportParameter[] reportParameter = null;
                            reportParameter = new ReportParameter[1];
                            reportParameter[0] = new ReportParameter("Year", Convert.ToString(year));
                            reportTypeText = vendorBE.VendorID.ToString();
                            reportDataSource = new ReportDataSource("dsRptVSCSSummary", getVendorSummary);
                            ReportViewer1.LocalReport.ReportPath = path + @"ModuleUI\VendorScorecard\RDLC\VendorScoreCardSummaryReport.rdlc";
                            ReportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                            ReportViewer1.LocalReport.Refresh();
                            ReportViewer1.LocalReport.DataSources.Clear();
                            ReportViewer1.LocalReport.DataSources.Add(reportDataSource);
                            ReportViewer1.LocalReport.SetParameters(reportParameter);
                            byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                            if (!string.IsNullOrEmpty(reportTypeText))
                            {
                                ReportPath = string.Format("{0}{1}_{2}.xls", ReportOutputPath, reportTypeText, year);
                            }

                            File.WriteAllBytes(ReportPath, bytes);
                            bytes = null;
                        }
                        catch (Exception ex)
                        {
                            sbMessage.Clear();
                            LogUtility.SaveErrorLogEntry(ex);
                            sbMessage.Append("\r\n");
                            sbMessage.Append("Report Generation Failed in GenerateReportExcelFiles() function");
                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************************************************************");
                            LogUtility.SaveTraceLogEntry(sbMessage);
                        }
                        finally
                        {
                            reportDataSource = null;
                            warnings = null;
                            streamIds = null;
                            sbMessage.Clear();
                            getVendorSummary = null;

                            iSleepCounter++;
                            if (iSleepCounter == 10)
                            {
                                System.Threading.Thread.Sleep(3);
                                iSleepCounter = 0;
                            }
                            GC.Collect();
                        }
                        #endregion
                    }

                    summaryScoreCardBE.Action = "UpdateAllGrandParentParentAndStandAloneVendors";
                    summaryScoreCardBE.VendorID = vendorBE.VendorID;
                    summaryScoreCardBAL.UpdateSummaryScoreCardBAL(summaryScoreCardBE);
                }

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Vendor Summary Grand Parent, Parent and Stand Alone Report Has Been Successfully Generated.");
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

               
            }

            summaryScoreCardBE.Action = "CheckVendorListScorecard";
            DataSet ds = summaryScoreCardBAL.GetVendorListScoreCardBAL(summaryScoreCardBE);
            if (ds != null && ds.Tables.Count > 0)
            {
                var data = ds.Tables[0].Rows[0][0].ToString();
                return data != "1";
            }
            else
            {
                return true;
            }
        }

        private void GenerateReport(DataTable dtSummaryScoreCard, int reportType = 0, int year = 0, string ModuleType = "")
        {
            #region Declarations ...

            var path = AppDomain.CurrentDomain.BaseDirectory.ToLower().Replace(@"\bin\debug", string.Empty);

            var sbMessage = new StringBuilder();
            sbMessage.Clear();
            sbMessage.Append("Generating at path           : " + path);
            sbMessage.Append("\r\n");
            sbMessage.Append("****************************************************************************************");
            sbMessage.Append("\r\n");
            LogUtility.SaveTraceLogEntry(sbMessage);


            var ReportOutputPath = string.Empty;
            var mimeType = string.Empty;
            var encoding = string.Empty;
            var extension = string.Empty;

            var ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            ReportDataSource reportDataSource;
            ReportParameter[] reportParameter = null;
            Warning[] warnings;
            string[] streamIds;
            var reportTypeText = string.Empty;
            #endregion

            try
            {
                switch (reportType)
                {
                    case -1:
                        reportTypeText = "European";
                        break;
                    case -2:
                        reportTypeText = "Local";
                        break;
                    case -3:
                        reportTypeText = "All";
                        break;
                    default:
                        reportTypeText = reportType.ToString();
                        break;
                }
                reportParameter = new ReportParameter[2];
                reportParameter[0] = new ReportParameter("Year", Convert.ToString(year));
                if (ModuleType == "OTIF")
                {
                    reportParameter[1] = new ReportParameter("ModuleType", "OTIF");
                }
                else if (ModuleType == "Stock")
                {
                    reportParameter[1] = new ReportParameter("ModuleType", "Stock");
                }
                else if (ModuleType == "Discrepancies")
                {
                    reportParameter[1] = new ReportParameter("ModuleType", "Disc");
                }
                else if (ModuleType == "Scheduling")
                {
                    reportParameter[1] = new ReportParameter("ModuleType", "Scheduling");
                }
                else if (ModuleType == "OverallScore")
                {
                    reportParameter[1] = new ReportParameter("ModuleType", "All");
                }
                reportDataSource = new ReportDataSource("dsRptVSCSSummary", dtSummaryScoreCard);
                ReportViewer1.LocalReport.ReportPath = path + @"ModuleUI\VendorScorecard\RDLC\VendorScoreCardSummaryReport.rdlc";
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(reportDataSource);
                ReportViewer1.LocalReport.SetParameters(reportParameter);
                byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                ReportOutputPath = path + @"ReportOutput\" + year + @"\";
                if (!System.IO.Directory.Exists(ReportOutputPath))
                    System.IO.Directory.CreateDirectory(ReportOutputPath);

                if (!string.IsNullOrEmpty(reportTypeText))
                    ReportOutputPath = string.Format("{0}{1}_{2}.xls", ReportOutputPath, reportTypeText + "_" + ModuleType, year);

                File.WriteAllBytes(ReportOutputPath, bytes);
                bytes = null;

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                if (reportTypeText.Equals("European"))
                    sbMessage.Append("OD Summary European " + ModuleType + " Option Report Has Been Successfully Generated.");
                else if (reportTypeText.Equals("Local"))
                    sbMessage.Append("OD Summary " + ModuleType + " Local Option Report Has Been Successfully Generated.");
                else if (reportTypeText.Equals("All"))
                    sbMessage.Append("OD Summary All " + ModuleType + " Option Report Has Been Successfully Generated.");

                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
                bytes = null;

            }
            catch (Exception ex)
            {
                sbMessage.Clear();
                LogUtility.SaveErrorLogEntry(ex);
                sbMessage.Append("Report Generation Failed in GenerateReport() funtion");
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            finally
            {
                ReportViewer1.Clear();
                ReportViewer1.Dispose();
                reportDataSource = null;
                warnings = null;
                streamIds = null;
                sbMessage.Clear();
            }
        }
    }
}

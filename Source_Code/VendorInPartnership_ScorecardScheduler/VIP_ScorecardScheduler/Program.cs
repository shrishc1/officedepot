﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using BusinessLogicLayer.ModuleBAL.VendorScorecard;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System.Collections.Generic;
using System.Text;
using Utilities;
using BusinessEntities.ModuleBE.VendorScorecard;
using System.Data.SqlClient;

namespace VIP_ScorecardScheduler
{
    static class Program
    {
        static int dbMonth = 0;
        static int dbYear = 0;
        static int? schedulerId = null;
        static string connString = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["sConn"]);
        static bool processRunning = false;

        [STAThread]
        static void Main()
        {
            try
            {
                var sbMessage = new StringBuilder();
                sbMessage.Append("Step 1 : Started at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                SummaryScoreCardBE summaryScoreCardBE = new SummaryScoreCardBE();
                summaryScoreCardBE.Action = "CheckVendorListScorecard";
                var summaryScoreCardBAL = new SummaryScoreCardBAL();
                DataSet ds = summaryScoreCardBAL.GetVendorListScoreCardBAL(summaryScoreCardBE);
                if (ds != null && ds.Tables.Count > 0)
                {
                    var data = ds.Tables[0].Rows[0][0].ToString();
                    processRunning = data != "1";
                }

                #region Logic to Updating scorecard tables ...
                sbMessage.Clear();
                sbMessage.Append("Step 2 : Updating scorecard tables start at           : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                GlobalObjects.lstLanguageTags = UIUtility.GetAllResources();

                //getSchedulerInfo();

                GetSchedulerInfo();

                sbMessage.Clear();
                sbMessage.Append("Step 2.2 : UpdateJobStartTime start at           : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                //Update Job Start Time
                UpdateJobStartTime(schedulerId, DateTime.Now);
                sbMessage.Clear();
                sbMessage.Append("Step 2.3 : UpdateJobStartTime end at           : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                string ExecuteOnlyMails = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["ExecuteOnlyMails"]);

                if (ExecuteOnlyMails == "" || ExecuteOnlyMails == "false")
                {
                    try
                    {
                        sbMessage.Clear();
                        sbMessage.Append("Step 2.4 : ExcecuteScorecardBAL start at           : " + DateTime.Now.ToString());
                        sbMessage.Append("\r\n");
                        sbMessage.Append("****************************************************************************************");
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);

                        int iVSCMonth = dbMonth == 0 ? Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["ScoreCardMonth"]) : dbMonth;

                        sbMessage.Clear();
                        sbMessage.Append("Step 2.5 : iVSCMonth           : " + iVSCMonth.ToString());
                        sbMessage.Append("\r\n");
                        sbMessage.Append("****************************************************************************************");
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);

                        if (processRunning)
                        {
                            MAS_VendorScoreCardBAL oMAS_VendorScoreCardBAL = new MAS_VendorScoreCardBAL();
                            oMAS_VendorScoreCardBAL.ExcecuteScorecardBAL(iVSCMonth);
                        }

                        sbMessage.Clear();
                        sbMessage.Append("\r\n");
                        sbMessage.Append("Step 3 : Updation scorecard tables Successfully done ");
                        sbMessage.Append("\r\n");
                        sbMessage.Append("**************************************");
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                    }
                    catch (Exception ex)
                    {
                        LogUtility.SaveErrorLogEntry(ex);
                    }
                    #endregion

                    #region Logic to get current year ...
                    try
                    {
                        var year = 0;

                        if (dbYear == 0)
                        {
                            summaryScoreCardBAL = new SummaryScoreCardBAL();
                            summaryScoreCardBE = new SummaryScoreCardBE();
                            summaryScoreCardBE.Action = "GetCurrentYear";
                            year = Convert.ToInt32(summaryScoreCardBAL.GetCurrentYearBAL(summaryScoreCardBE));
                            if (year.Equals(0))
                                year = DateTime.Now.DayOfYear;

                            var configYear = Convert.ToString(ConfigurationManager.AppSettings["Year"]);
                            if (!string.IsNullOrEmpty(configYear))
                                year = Convert.ToInt32(configYear);
                        }
                        else
                        {
                            year = dbYear;
                        }

                        #endregion

                        #region Logic to generate the excel reports ...
                        var generateReports = new GenerateReports();
                        processRunning = generateReports.GenerateReportExcelFiles(year, processRunning);
                    }
                    catch (Exception ex)
                    {
                        LogUtility.SaveErrorLogEntry(ex);
                    }

                    sbMessage.Clear();
                    sbMessage.Append("Vendor Score Card Generated           : " + DateTime.Now.ToString());
                    sbMessage.Append("\r\n");
                    sbMessage.Append("****************************************************************************************");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                    #endregion
                }

                #region Logic to send email ...

                string IsMailSend = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["IsMailToBeSend"]);


                if (IsMailSend == "true" && processRunning)
                {
                    GC.Collect();
                    System.Threading.Thread.Sleep(30);

                    sbMessage.Clear();
                    sbMessage.Append("Started email sending process           : " + DateTime.Now.ToString());
                    sbMessage.Append("\r\n");
                    sbMessage.Append("****************************************************************************************");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                    int iUserId = 0;

                    string sLanguage, sUserEmail, StockPlannerName, PhoneNumber, sReportMonth = string.Empty;
                    int iVendorId = 0;
                    try
                    {
                        UP_VendorBE oUP_VendorBE = new UP_VendorBE();
                        UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
                        oUP_VendorBE.Action = "getVendorScorecardDetail";
                        List<UP_VendorBE> lstVendor = new List<UP_VendorBE>();
                        lstVendor = oUP_VendorBAL.GetVendorReportBAL(oUP_VendorBE);

                        sbMessage.Clear();
                        if (lstVendor == null)
                            sbMessage.Append("Record count for vendor Scorecard         : null");
                        else
                            sbMessage.Append("Record count for vendor Scorecard         : " + lstVendor.Count.ToString());

                        LogUtility.SaveTraceLogEntry(sbMessage);

                        if (lstVendor != null && lstVendor.Count > 0)
                        {
                            for (int i = 0; i < lstVendor.Count; i++)
                            {
                                sUserEmail = lstVendor[i].User.EmailId;
                                sLanguage = lstVendor[i].Language;
                                iUserId = lstVendor[i].User.UserID;
                                StockPlannerName = lstVendor[i].StockPlannerName;
                                PhoneNumber = lstVendor[i].StockPlannerNumber;
                                iVendorId = lstVendor[i].VendorID;
                                sentMailToVendor(sUserEmail, sLanguage, iUserId, StockPlannerName, PhoneNumber);
                                GC.Collect();


                                sbMessage.Clear();
                                sbMessage.Append("\r\n");
                                sbMessage.Append(" Mail Successfully Sent to UserVendor: " + Convert.ToString(iUserId) + " and EmailID is: " + sUserEmail);
                                sbMessage.Append("\r\n");
                                sbMessage.Append("**************************************");
                                sbMessage.Append("\r\n");
                                LogUtility.SaveTraceLogEntry(sbMessage);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogUtility.SaveErrorLogEntry(ex);
                    }
                }
                #endregion

                sbMessage.Clear();
                sbMessage.Append("Ended at              : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                //Update Job End Time
                if (processRunning)
                {
                    UpdateJobEndTime(schedulerId, DateTime.Now);
                    System.Environment.Exit(0);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
                //Update Job End Time When Failed
                UpdateJobEndTime(schedulerId, (DateTime?)null);
                System.Environment.Exit(0);
            }
        }

        private static void sentMailToVendor(string toAddress, string language, int iUserid, string StockPlannerName, string PhoneNumber)
        {

            string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
            path = path.Replace(@"\bin\debug", "");

            try
            {
                language = language.TrimEnd(',');
                language = language.Trim();

                string templatesPath = path + @"\emailtemplates\Scorecard";
                string templateFile = templatesPath + @"\communication" + "." + language + ".htm";
                if (!System.IO.File.Exists(templateFile.ToLower()))
                {
                    templateFile = templatesPath + @"\communication" + "." + "english" + ".htm";
                }

                string htmlBody = string.Empty;

                string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]) + @"/ModuleUI/Security/Login.aspx";


                using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                {
                    DateTime dt = DateTime.Now.AddMonths(-1);

                    #region mailBody
                    htmlBody = sReader.ReadToEnd();
                    htmlBody = htmlBody.Replace("{DearSirMadam}", UIUtility.getGlobalResourceValue("DearSirMadam", language));
                    htmlBody = htmlBody.Replace("{ScorecardText1}", UIUtility.getGlobalResourceValue("ScorecardText1", language));
                    htmlBody = htmlBody.Replace("{ScorecardText3}", UIUtility.getGlobalResourceValue("ScorecardText3", language));
                    htmlBody = htmlBody.Replace("{ScorecardText4}", UIUtility.getGlobalResourceValue("ScorecardText4", language));
                    htmlBody = htmlBody.Replace("{ScorecardText2}", UIUtility.getGlobalResourceValue("ScorecardText2", language));
                    htmlBody = htmlBody.Replace("{RegardsNew}", UIUtility.getGlobalResourceValue("RegardsNew", language));
                    htmlBody = htmlBody.Replace("{InventoryManager}", StockPlannerName);
                    htmlBody = htmlBody.Replace("{PhoneNumber}", PhoneNumber);

                    htmlBody = htmlBody.Replace("{ReportPeriod}", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dt.Month) + " - " + dt.Year.ToString());

                    htmlBody = htmlBody.Replace("{PortalLinkValue}", "<a href='" + sPortalLink + "'>" + sPortalLink + " </a>");
                    #endregion


                    string fromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                    string mailSubjectText = "Scorecard Reporting for " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dt.Month) + " - " + dt.Year.ToString();

                    clsEmail oclsEmail = new clsEmail();
                    oclsEmail.sendMail(toAddress, htmlBody, mailSubjectText, fromAddress, true);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex, "Mail failed for User ID: " + Convert.ToString(iUserid));
            }
        }

        private static void getSchedulerInfo()
        {

            SqlCommand vscSqlCommand = new SqlCommand();
            SqlConnection vscSqlConnection = new SqlConnection(connString);
            vscSqlCommand.CommandType = CommandType.Text;
            vscSqlCommand.CommandText = "Select * from dbo.SchedulingApp where IsExecuted=0 and ApplicationName='VSC'";
            vscSqlCommand.Connection = vscSqlConnection;
            vscSqlCommand.Connection.Open();
            DataTable dt = new DataTable();
            dt.Load(vscSqlCommand.ExecuteReader());
            vscSqlCommand.Connection.Close();
            if (dt.Rows.Count > 0)
            {
                dbMonth = Convert.ToInt32(dt.Rows[0]["Month"]);
                dbYear = Convert.ToInt32(dt.Rows[0]["Year"]);
            }

        }

        #region New Methods
        private static void GetSchedulerInfo()
        {

            SqlConnection sqlConnection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand("spSchedulingApp", sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Action", "GetTopOneScheduledJob");
            command.Parameters.AddWithValue("@ApplicationName", "VSC");
            command.Parameters.AddWithValue("@SchedulerDate", DateTime.Now.Date);
            command.Parameters.AddWithValue("@SchedulerTime", DateTime.Now.TimeOfDay);
            sqlConnection.Open();
            DataTable dt = new DataTable();
            dt.Load(command.ExecuteReader());
            sqlConnection.Close();

            if (dt.Rows.Count > 0)
            {
                dbMonth = Convert.ToInt32(dt.Rows[0]["Month"]);
                dbYear = Convert.ToInt32(dt.Rows[0]["Year"]);
                schedulerId = Convert.ToInt32(Convert.ToString(dt.Rows[0]["SchedulerId"]));

                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Clear();
                sbMessage.Append("Step 2a : GetSchedulerInfo start at           : " + dbMonth.ToString() + " " + dbYear.ToString() + " " + schedulerId.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
        }

        private static void UpdateJobStartTime(int? schedulerId, DateTime startTime)
        {
            if (schedulerId != null)
            {
                SqlCommand vscSqlCommand = new SqlCommand();
                SqlConnection vscSqlConnection = new SqlConnection(connString);
                vscSqlCommand.CommandType = CommandType.Text;
                vscSqlCommand.Connection = vscSqlConnection;
                vscSqlCommand.Connection.Open();
                vscSqlCommand.CommandText = "Update SchedulingApp Set StartTime='" + startTime.ToString("MM/dd/yyyy hh:mm") + "' Where SchedulerId=" + schedulerId + "";
                vscSqlCommand.ExecuteNonQuery();
                vscSqlCommand.Connection.Close();
            }
        }

        private static void UpdateJobEndTime(int? schedulerId, DateTime? EndTime)
        {
            if (schedulerId != null)
            {
                SqlCommand vscSqlCommand = new SqlCommand();
                SqlConnection vscSqlConnection = new SqlConnection(connString);
                vscSqlCommand.CommandType = CommandType.Text;
                vscSqlCommand.Connection = vscSqlConnection;
                vscSqlCommand.Connection.Open();
                vscSqlCommand.CommandText = EndTime == null
                    ? "Update SchedulingApp Set IsExecuted=1 Where SchedulerId=" + schedulerId + ""
                    : "Update SchedulingApp Set IsExecuted=1,EndTime='" + Convert.ToDateTime(EndTime).ToString("MM/dd/yyyy hh:mm") + "'  Where SchedulerId=" + schedulerId + "";
                vscSqlCommand.ExecuteNonQuery();
                vscSqlCommand.Connection.Close();
            }
        }
        #endregion
    }
}

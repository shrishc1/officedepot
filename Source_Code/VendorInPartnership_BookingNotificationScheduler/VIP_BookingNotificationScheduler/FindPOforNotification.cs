﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Diagnostics;
using BusinessLogicLayer.ModuleBAL.Upload;
using Utilities;
using BusinessLogicLayer.ModuleBAL.Languages;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using BusinessEntities.ModuleBE.Languages;


namespace VIP_BookingNotificationScheduler
{
    public class FindPOforNotification
    {
        public SqlConnection sqlCon;
        public SqlCommand sqlComm;

        string sConn = ConfigurationManager.AppSettings["sConn"];
        DataTable dtOutstandingPO = null;

        public FindPOforNotification()
        {
            this.GetPOforNotification();
        }

        private void GetPOforNotification()
        {
            try
            {

                UP_PurchaseOrderDetailBAL oUP_PurchaseOrderDetailBAL = new UP_PurchaseOrderDetailBAL();

                dtOutstandingPO = oUP_PurchaseOrderDetailBAL.GetPoForSchedularBAL();

                if (dtOutstandingPO != null && dtOutstandingPO.Rows.Count > 0)
                {
                    SendMailToVendors();
                }

                foreach (Process process in Process.GetProcessesByName("VIP_BookingNotificationScheduler"))
                {
                    process.Kill();
                    process.WaitForExit();
                }

                //                using (sqlCon = new SqlConnection(sConn)) {
                //                    sqlCon.Open();
                //                    sqlComm = new SqlCommand();
                //                    sqlComm.Connection = sqlCon;
                //                    sqlComm.CommandText = @"SELECT distinct *  FROM (
                //                                            SELECT (Warehouse+Purchase_order) AS FILTER,
                //                                            Purchase_order as PurchaseNumber,MAS_Site.APP_ToleranceDueDay,
                //                                            Original_due_date, MAS_Site.SiteName,
                //                                            MAS_Site.APP_BookingNoticePeriodInDays,
                //                                            MAS_Site.APP_BookingNoticeTime, UP_Vendor.Vendor_Name,UP_Vendor.VendorID,
                //                                            Isnull((SELECT dbo.Udfgetusersemail(UP_PurchaseOrderDetails.VendorID)), 
                //                                            UP_Vendor.[Vendor Contact Email]) AS contactMail,
                //                                            (Select count(*) from UP_PurchaseOrderDetails PO where 
                //                                            PO.Purchase_order = UP_PurchaseOrderDetails.Purchase_order
                //                                            and PO.Warehouse = UP_PurchaseOrderDetails.Warehouse and PO.LastUploadedFlag = 'Y'
                //                                            group by PO.Purchase_order) as OutstandingLines
                //                                            FROM UP_PurchaseOrderDetails	
                //                                            INNER JOIN UP_Vendor
                //                                            ON UP_Vendor.VendorID = UP_PurchaseOrderDetails.VendorID
                //                                            INNER JOIN MAS_Site
                //                                            ON MAS_Site.SiteNumber = UP_PurchaseOrderDetails.Warehouse
                //                                            WHERE 		
                //                                            LastUploadedFlag = 'Y' AND MAS_Site.IsActive=1	
                //                                            AND Original_due_date <= DATEADD(DD,8,GETDATE())	
                //                                            AND UP_PurchaseOrderDetails.Purchase_order NOT IN 
                //                                            (SELECT APP_BookedVendorPO.Purchase_order
                //                                            FROM   APP_BookedVendorPO
                //                                            INNER JOIN UP_PurchaseOrderDetails
                //                                            ON APP_BookedVendorPO.PurchaseOrderID = UP_PurchaseOrderDetails.PurchaseOrderID)
                //                                            and Original_due_date <= DATEADD(DD,8,GETDATE())) PODETAILS
                //                                            WHERE contactMail IS NOT NULL AND contactMail <> '' ORDER BY VendorID";

                //                    SqlDataReader sdr = sqlComm.ExecuteReader();
                //                    //DataTable dt = new DataTable();
                //                    //dt.Load(sdr);

                //                    //if (dt.Rows.Count > 0) {
                //                    //    Filter(dt);
                //                    //}

                //                    dtOutstandingPO = new DataTable();
                //                    dtOutstandingPO.Load(sdr);
                //                    sqlCon.Close();

                //                    if (dtOutstandingPO != null && dtOutstandingPO.Rows.Count > 0) {
                //                        SendMailToVendors();
                //                    }

                //                    foreach (Process process in Process.GetProcessesByName("VIP_BookingNotificationScheduler")) {
                //                        process.Kill();
                //                        process.WaitForExit();
                //                    }

                //                }               
            }
            catch (Exception ex)
            {
                Utilities.LogUtility.SaveErrorLogEntry(ex);
            }
        }

        private void SendMailToVendors()
        {
            try
            {
                if (dtOutstandingPO.Rows.Count > 0)
                {
                    DataView POView = dtOutstandingPO.DefaultView;
                    POView.RowFilter = "VendorID='" + Convert.ToString(dtOutstandingPO.Rows[0]["VendorID"]) + "'";

                    SendMail(POView.ToTable());

                    DataView POViewFilter = dtOutstandingPO.DefaultView;
                    POViewFilter.RowFilter = "VendorID <> '" + Convert.ToString(dtOutstandingPO.Rows[0]["VendorID"]) + "'";

                    dtOutstandingPO = POViewFilter.ToTable();
                    SendMailToVendors();
                }
            }
            catch (Exception ex)
            {
                Utilities.LogUtility.SaveErrorLogEntry(ex);
            }
        }

        private void Filter(DataTable lstPOLocal)
        {
            try
            {
                if (lstPOLocal.Rows.Count > 0)
                {

                    DateTime? Original_due_date;
                    int OutStandingLines;
                    string Purchase_order;
                    string SiteName;
                    string contactMail;
                    string VendorID;
                    string Warehouse;
                    string Vendor_Name;

                    Original_due_date = Convert.ToDateTime(lstPOLocal.Rows[0]["Original_due_date"]);
                    Purchase_order = Convert.ToString(lstPOLocal.Rows[0]["Purchase_order"]);
                    SiteName = Convert.ToString(lstPOLocal.Rows[0]["SiteName"]);
                    contactMail = Convert.ToString(lstPOLocal.Rows[0]["contactMail"]);
                    VendorID = Convert.ToString(lstPOLocal.Rows[0]["VendorID"]);
                    Warehouse = Convert.ToString(lstPOLocal.Rows[0]["Warehouse"]);
                    Vendor_Name = Convert.ToString(lstPOLocal.Rows[0]["Vendor_Name"]);

                    DataView dv = lstPOLocal.DefaultView;
                    //dv.RowFilter = "SiteName ='" + SiteName + "' And Purchase_order ='" + Purchase_order + "'";
                    dv.RowFilter = "FILTER = '" + Warehouse + Purchase_order + "'";

                    DataTable lstPOTemp = dv.ToTable();

                    OutStandingLines = lstPOTemp.Rows.Count;

                    DataView dvTemp = lstPOLocal.DefaultView;
                    //dvTemp.RowFilter = "NOT Purchase_order ='" + Purchase_order + "'";
                    dvTemp.RowFilter = "FILTER <> '" + Warehouse + Purchase_order + "'";


                    if (dtOutstandingPO == null)
                    {

                        dtOutstandingPO = new DataTable();


                        DataColumn myDataColumn = new DataColumn();
                        myDataColumn.ColumnName = "PurchaseNumber";
                        dtOutstandingPO.Columns.Add(myDataColumn);

                        myDataColumn = new DataColumn();
                        myDataColumn.ColumnName = "Original_due_date";
                        dtOutstandingPO.Columns.Add(myDataColumn);


                        myDataColumn = new DataColumn();
                        myDataColumn.ColumnName = "OutstandingLines";
                        dtOutstandingPO.Columns.Add(myDataColumn);

                        myDataColumn = new DataColumn();
                        myDataColumn.ColumnName = "SiteName";
                        dtOutstandingPO.Columns.Add(myDataColumn);

                        myDataColumn = new DataColumn();
                        myDataColumn.ColumnName = "contactMail";
                        dtOutstandingPO.Columns.Add(myDataColumn);

                        myDataColumn = new DataColumn();
                        myDataColumn.ColumnName = "VendorID";
                        dtOutstandingPO.Columns.Add(myDataColumn);

                        myDataColumn = new DataColumn();
                        myDataColumn.ColumnName = "Vendor_Name";
                        dtOutstandingPO.Columns.Add(myDataColumn);
                    }

                    DataRow dataRow = dtOutstandingPO.NewRow();

                    dataRow["PurchaseNumber"] = Purchase_order;
                    dataRow["Original_due_date"] = Original_due_date.Value.ToString("dd/MM/yyyy");
                    dataRow["OutstandingLines"] = OutStandingLines;
                    dataRow["SiteName"] = SiteName;
                    dataRow["contactMail"] = contactMail;
                    dataRow["VendorID"] = VendorID;
                    dataRow["Vendor_Name"] = Vendor_Name;

                    dtOutstandingPO.Rows.Add(dataRow);

                    Filter(dvTemp.ToTable());
                }
            }

            catch (Exception ex)
            {
                Utilities.LogUtility.SaveErrorLogEntry(ex);
            }
        }

        private void SendMail(DataTable tab)
        {

            try
            {
                string toAddress = string.Empty;
                string language = "English";

                object value = tab.Rows[0]["contactMail"];
                if (value != DBNull.Value && (value.ToString() != ""))
                {
                    toAddress = tab.Rows[0]["contactMail"].ToString();  //"abhinavj@damcogroup.com"; // 
                }
                else
                {
                    toAddress = System.Configuration.ConfigurationSettings.AppSettings["DefaultEmailAddress"];
                }

                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                path = path.Replace(@"\bin\debug", "");

                string templatesPath = path + @"EmailTemplate\";
                string templateFile = templatesPath + @"DeliveryChase.english.htm";
                string mailBoby = string.Empty;
                string bodyData = string.Empty;
                string POs = string.Empty;
                DateTime? Original_due_date;
                string logoimagepath = path + @"Images\OfficeDepotLogo.jpg";
                foreach (DataRow dr in tab.Rows)
                {
                    POs = POs + dr["PurchaseNumber"] + ",";

                    Original_due_date = Convert.ToDateTime(dr["Original_due_date"]);  // == "" && dr["Original_due_date"] == null ? string.Empty : Convert.ToDateTime(dr["Original_due_date"]).ToString("dd/MM/yyyy");
                    //dueDate = dueDate.Value.ToString("dd/MM/yyyy");
                    bodyData += "<tr><td style='text-align:center;'>" + dr["SiteName"] + "</td>" +
                        "<td style='text-align:center;'>" + dr["PurchaseNumber"] + "</td>" +
                        "<td style='text-align:center;'>" + dr["OutstandingLines"] + "</td>" +
                        "<td style='text-align:center;'>" + Original_due_date.Value.ToString("dd/MM/yyyy") + "</td></tr>";
                }

                using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                {
                    mailBoby = sReader.ReadToEnd();
                    //mailBoby = mailBoby.Replace("{logoInnerPath}", logoimagepath);
                    mailBoby = mailBoby.Replace("{DearSirMadam}", getGlobalResourceValue("DearSirMadam", language));
                    mailBoby = mailBoby.Replace("{BookingNtfText1}", getGlobalResourceValue("BookingNtfText1", language));
                    mailBoby = mailBoby.Replace("{BookingNtfText2}", getGlobalResourceValue("BookingNtfText2", language));
                    mailBoby = mailBoby.Replace("{SiteRequiredKey}", getGlobalResourceValue("SiteRequiredKey", language));
                    mailBoby = mailBoby.Replace("{PurchaseOrderNumber}", getGlobalResourceValue("PurchaseOrderNumber", language));
                    mailBoby = mailBoby.Replace("{NoOfLinesOutstanding}", getGlobalResourceValue("NoOfLinesOutstanding", language));
                    mailBoby = mailBoby.Replace("{PODueDate}", getGlobalResourceValue("PODueDate", language));
                    mailBoby = mailBoby.Replace("{datatableValue}", bodyData);
                    mailBoby = mailBoby.Replace("{BookingNtfText3}", getGlobalResourceValue("BookingNtfText3", language));
                    mailBoby = mailBoby.Replace("{Yoursfaithfully}", getGlobalResourceValue("Yoursfaithfully", language));
                }

                string mailSubject = "URGENT - DO NOT IGNORE";
                string fromAddress = System.Configuration.ConfigurationManager.AppSettings["fromAddress"] as string;
                bool blsHTMLMail = true;
                if (!string.IsNullOrEmpty(toAddress))
                {

                    clsEmail oclsEmail = new clsEmail();
                    oclsEmail.sendMail(toAddress, mailBoby, mailSubject, fromAddress, blsHTMLMail);
                }

                LogUtility.SaveLogEntry(toAddress, "Send", POs);
            }
            catch (Exception ex)
            {
                Utilities.LogUtility.SaveErrorLogEntry(ex);
            }
        }

        public static string getGlobalResourceValue(string key, string UserLanguage)
        {
            string ResourceValues = string.Empty;
            var lstLanguageTags = new List<MAS_LanguageTagsBE>();
            try
            {
                //Get All LanguageTags From DB
                MAS_LanguageTagsBE oMAS_LanguageTagsBE = new MAS_LanguageTagsBE();
                MAS_LanguageTagsBAL oMAS_LanguageTagsBAL = new MAS_LanguageTagsBAL();
                if (lstLanguageTags != null && lstLanguageTags.Count <= 0)
                {
                    oMAS_LanguageTagsBE.Action = "GetAllLanguageTags";
                    lstLanguageTags = oMAS_LanguageTagsBAL.GetAllLanguageTagsBAL(oMAS_LanguageTagsBE);
                    var lstResult = lstLanguageTags.Where(x => x.LanguageKey.ToLower() == key.ToLower()).ToList();
                    if (lstResult != null && lstResult.Count > 0)
                    {
                        Type type = lstResult[0].GetType();
                        PropertyInfo prop = type.GetProperty(UserLanguage);
                        if (prop != null)
                        {
                            ResourceValues = Convert.ToString(prop.GetValue(lstResult[0], null));
                        }
                    }
                }
                else
                {

                    var lstResult = lstLanguageTags.Where(x => x.LanguageKey.ToLower() == key.ToLower()).ToList();
                    if (lstResult != null && lstResult.Count > 0)
                    {
                        Type type = lstResult[0].GetType();
                        PropertyInfo prop = type.GetProperty(UserLanguage);
                        if (prop != null)
                        {
                            ResourceValues = Convert.ToString(prop.GetValue(lstResult[0], null));
                        }
                    }
                }
            }

            catch (Exception ex)
            {

            }
            return ResourceValues;
        }


        //Not in Use-----//
        private void GetPOforNotification_OLD()
        {
            try
            {
                using (sqlCon = new SqlConnection(sConn))
                {
                    sqlCon.Open();
                    sqlComm = new SqlCommand();
                    sqlComm.Connection = sqlCon;
                    sqlComm.CommandText = @"SELECT PurchaseOrderID,
                                                   Purchase_order,
                                                   UP_PurchaseOrderDetails.Line_No,
                                                   UP_Vendor.Vendor_Name,
                                                   Isnull((SELECT dbo.Udfgetusersemail(UP_PurchaseOrderDetails.VendorID)), UP_Vendor.[Vendor Contact Email]) AS contactMail,
                                                   MAS_Site.SiteID,
                                                   MAS_Site.SiteName,
                                                   Original_due_date,
                                                   MAS_Site.APP_BookingNoticePeriodInDays,
                                                   MAS_Site.APP_BookingNoticeTime
                                            FROM   UP_PurchaseOrderDetails
                                                   INNER JOIN UP_Vendor
                                                     ON UP_Vendor.VendorID = UP_PurchaseOrderDetails.VendorID
                                                   INNER JOIN UP_SKU
                                                     ON UP_SKU.SKUID = UP_PurchaseOrderDetails.SKUID
                                                   INNER JOIN MAS_Site
                                                     ON MAS_Site.SiteNumber = UP_PurchaseOrderDetails.Warehouse
                                            WHERE  LastUploadedFlag = 'Y'
                                                   AND MAS_Site.IsActive = 1
                                                   AND UP_PurchaseOrderDetails.PurchaseOrderID NOT IN (SELECT APP_BookedVendorPO.PurchaseOrderID
                                                                                                       FROM   APP_BookedVendorPO
                                                                                                              INNER JOIN UP_PurchaseOrderDetails
                                                                                                                ON APP_BookedVendorPO.PurchaseOrderID = UP_PurchaseOrderDetails.PurchaseOrderID
                                                                                                       WHERE  UP_PurchaseOrderDetails.Order_raised > Dateadd(MM, -1, Getdate()))
                                            ORDER  BY SiteID,
                                                      Original_due_date
                                            ";
                    SendCommunication oSendCommunication = new SendCommunication();
                    SqlDataReader sdr = sqlComm.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(sdr);
                    if (dt.Rows.Count > 0)
                    {
                        // linq to find the groups of po
                        var groups = from drr in dt.AsEnumerable()
                                     group drr by drr.Field<string>("Purchase_order") into g
                                     select g;
                        foreach (IGrouping<string, DataRow> tab in groups)
                        {
                            bool sendmailflag = false;
                            DataTable datatab = tab.CopyToDataTable();
                            DataTable dtSendMail = datatab.Clone();
                            foreach (DataRow dr in datatab.AsEnumerable())
                            {
                                sendmailflag = false;
                                int? NoticePeriodDays = dr["APP_BookingNoticePeriodInDays"] != DBNull.Value ? Convert.ToInt32(dr["APP_BookingNoticePeriodInDays"]) : (int?)null;
                                DateTime? BookingNoticeTime = dr["APP_BookingNoticeTime"] != DBNull.Value ? Convert.ToDateTime(dr["APP_BookingNoticeTime"]) : (DateTime?)null;
                                DateTime? Due_Date = dr["Original_due_date"] != DBNull.Value ? Convert.ToDateTime(dr["Original_due_date"]) : (DateTime?)null;

                                if (Due_Date.HasValue && NoticePeriodDays.HasValue &&
                                    (Due_Date.Value.AddDays(NoticePeriodDays.Value) < DateTime.Now))
                                {
                                    // no need to check time part as duedate + noticeday is less than current day
                                    sendmailflag = true;
                                    dtSendMail.ImportRow(dr);
                                }
                                else if (Due_Date.HasValue && NoticePeriodDays.HasValue &&
                                    (Due_Date.Value.AddDays(NoticePeriodDays.Value).ToShortDateString() == DateTime.Now.ToShortDateString())
                                    && BookingNoticeTime.HasValue &&
                                    (BookingNoticeTime.Value.TimeOfDay < DateTime.Now.TimeOfDay))
                                {
                                    // check for time
                                    sendmailflag = true;
                                    dtSendMail.ImportRow(dr);
                                }
                            }
                            if (dtSendMail.Rows.Count > 0 && sendmailflag)
                            {
                                // send mail for booking notification
                                SendMail(dtSendMail);
                            }
                        }
                    }
                    sqlCon.Close();
                }
            }
            catch (Exception ex)
            {
                Utilities.LogUtility.SaveErrorLogEntry(ex);
            }
        }
        //----------//
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using System.Linq;
using System.Configuration;


namespace VIP_BookingNotificationScheduler
{
    public class LogUtility
    {
        public static bool blnErrorLog = true;
        public static bool SaveLogEntry(string Message, string Type, string POs)
        {
            try
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("\r\n");
                sbMessage.Append("\r\n");
                sbMessage.Append("Date --" + System.DateTime.Now);
                sbMessage.Append("\r\n");
                if (Type == "Error")
                    sbMessage.Append("ErrorMessage --" + Message);
                else
                    sbMessage.Append("Mail Send To --" + Message);                
                sbMessage.Append("\r\n");
                sbMessage.Append("PO Numbers --" + POs);
                sbMessage.Append("\r\n");
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");

                bool flag = WriteToLog(sbMessage, Type);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private static bool WriteToLog(StringBuilder sbMessage, string Type)
        {
            try
            {
                FileStream fs;
                string strLogFileName = string.Empty;
                if (Type == "Error")
                    strLogFileName = "BookingNotificationScheduler_Log_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                else
                    strLogFileName = "BookingNotificationScheduler_Send_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";

                string strLogFilePath = AppDomain.CurrentDomain.BaseDirectory + "Logs\\";
                if (!Directory.Exists(strLogFilePath))
                    Directory.CreateDirectory(strLogFilePath);

                if (File.Exists(strLogFilePath + strLogFileName) == true) { fs = File.Open(strLogFilePath + strLogFileName, FileMode.Append, FileAccess.Write); }
                else { fs = File.Create(strLogFilePath + strLogFileName); }
                {
                    using (StreamWriter sw = new StreamWriter(fs)) { sw.Write(sbMessage.ToString() + Environment.NewLine); }
                    fs.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

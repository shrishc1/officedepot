﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using Utilities;

namespace VIP_BookingNotificationScheduler
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            StringBuilder sbMessage = new StringBuilder();
            sbMessage.Append("Started at            : " + DateTime.Now.ToString());
            sbMessage.Append("\r\n");
            sbMessage.Append("****************************************************************************************");
            sbMessage.Append("\r\n");
            Utilities.LogUtility.SaveTraceLogEntry(sbMessage);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FindPOforNotification oFindPOforNotification = new FindPOforNotification();

            sbMessage.Clear();
            sbMessage.Append("Ended at              : " + DateTime.Now.ToString());
            sbMessage.Append("\r\n");
            sbMessage.Append("****************************************************************************************");
            sbMessage.Append("\r\n");
            Utilities.LogUtility.SaveTraceLogEntry(sbMessage);
        }
    }
}

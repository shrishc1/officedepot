﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Configuration;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Diagnostics;

namespace RestartReportQueue
{
    class Program
    {
        static string connString = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["sConn"]);

        static void Main(string[] args)
        {
            try
            {
                var sbMessage = new StringBuilder();
                sbMessage.Append("Step 1 : Started at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                sbMessage = new StringBuilder();

                #region Make monthly entry for VSC and OTIF scheduler
                int dayVSC = 3;
                int dayOTIF = 3;


                if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["ScoreCardDayOfMonth"]))
                    dayVSC = Convert.ToInt32(ConfigurationSettings.AppSettings["ScoreCardDayOfMonth"]);

                if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["OTIFDayOfMonth"]))
                    dayOTIF = Convert.ToInt32(ConfigurationSettings.AppSettings["OTIFDayOfMonth"]);

                if (DateTime.Now.Date.Day == dayVSC)
                    SaveMonthlySchedulerEntry("VSC");

                if (DateTime.Now.Date.Day == dayOTIF)
                    SaveMonthlySchedulerEntry("OTIF");

                #endregion Make monthly entry for VSC and OTIF scheduler

                sbMessage.Append("Step 2 : Make monthly entry for VSC and OTIF scheduler Section started at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
                sbMessage = new StringBuilder();

                //string connString = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["sConn"]);
                string ReportExePath = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["ReportExePath"]);
                string ReportVSCExePath = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["ReportVSCExePath"]);
                string ReportOTIFExePath = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["ReportOTIFExePath"]);
                bool isOTIFSchedulerStarted = false;

                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                sbMessage.Append("Step A :Start VIP_ReportingQueueService exe ");
                LogUtility.SaveTraceLogEntry(sbMessage);
                sbMessage = new StringBuilder();

                SqlCommand sqlCommand = new SqlCommand();
                SqlConnection sqlConnection = new SqlConnection(connString);
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = "SELECT COUNT(*) FROM RPT_ReportRequest WHERE RequestStatus='Pending'";
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Connection.Open();
                var PendingRequest = sqlCommand.ExecuteScalar();
                sqlCommand.Connection.Close();
                if (Convert.ToInt16(PendingRequest) > 0)
                {
                    Process[] pname = Process.GetProcessesByName("VIP_ReportingQueueService");
                    if (pname.Length == 0)
                    {
                        //ProcessStartInfo pi = new ProcessStartInfo();
                        //pi.FileName = ReportExePath + "VIP_ReportingQueueService.exe";
                      
                        sbMessage.Append("Step L1 : launching VIP_ReportingQueueService..");

                        ExecuteAsAdmin(ReportExePath + "VIP_ReportingQueueService.exe");

                        //Process.Start(pi);

                        sbMessage.Append("\r\n");
                        sbMessage.Append("****************************************************************************************");
                        sbMessage.Append("\r\n");
                        sbMessage.Append("Step A1 : running VIP_ReportingQueueService..");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                        sbMessage = new StringBuilder();
                    }
                }


                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                sbMessage.Append("Step B :Checking pending count for running OTIF");
                LogUtility.SaveTraceLogEntry(sbMessage);
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                sbMessage = new StringBuilder();
                // Check for OTIF Schedular Execution.                 
                SqlCommand otifSqlCommand = new SqlCommand();
                SqlConnection otifSqlConnection = new SqlConnection(connString);
                otifSqlCommand.CommandType = CommandType.Text;
                otifSqlCommand.CommandText = "Select dbo.[udfGetOTIFPendReportGeneratedStatus]()";
                otifSqlCommand.Connection = otifSqlConnection;
                otifSqlCommand.Connection.Open();
                var PendingCount = otifSqlCommand.ExecuteScalar();
                otifSqlCommand.Connection.Close();
                if (Convert.ToInt16(PendingCount) > 0)
                {
                    sbMessage.Append("\r\n");
                    sbMessage.Append("****************************************************************************************");
                    sbMessage.Append("\r\n");
                    sbMessage.Append("Step C :Checking Process in TS");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                    sbMessage = new StringBuilder();
                    Process[] pname = Process.GetProcessesByName("VIP_VendorGenerateOTIFScheduler");
                    if (pname.Length == 0)
                    {
                        sbMessage.Append("\r\n");
                        sbMessage.Append("****************************************************************************************");
                        sbMessage.Append("\r\n");
                        sbMessage.Append("Step D :Started OTIF scheduler Process in TS");
                        sbMessage = new StringBuilder();
                        isOTIFSchedulerStarted = true;


                        //ProcessStartInfo pi = new ProcessStartInfo();
                        //pi.FileName = ReportOTIFExePath + "VIP_VendorGenerateOTIFScheduler.exe";

                        sbMessage.Append("\r\n");
                        sbMessage.Append("Step L2 : Launching " + ReportOTIFExePath + "VIP_VendorGenerateOTIFScheduler.exe");

                        ExecuteAsAdmin(ReportOTIFExePath + "VIP_VendorGenerateOTIFScheduler.exe");

                        //Process.Start(pi);


                        sbMessage.Append("\r\n");
                        sbMessage.Append("****************************************************************************************");
                        sbMessage.Append("\r\n");
                        sbMessage.Append("Step E :Running   OTIF scheduler Process in TS");
                        sbMessage.Append("\r\n");
                        sbMessage.Append("****************************************************************************************");
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                        sbMessage = new StringBuilder();
                    }
                    if (pname.Length > 0)
                    {
                        sbMessage.Append("\r\n");
                        sbMessage.Append("****************************************************************************************");
                        sbMessage.Append("\r\n");
                        sbMessage.Append("Step E :already Running   OTIF scheduler Process in TS");
                        sbMessage.Append("\r\n");
                        sbMessage.Append("****************************************************************************************");
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                        sbMessage = new StringBuilder();
                    }
                }


                #region  OTIF/VSC  Schedular

                sbMessage.Append("Step 3 : OTIF/VSC  Schedular Section started at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
                sbMessage = new StringBuilder();
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                string scheduledJobName = string.Empty;
                scheduledJobName = GetTopScheduledJob();

                if (!string.IsNullOrEmpty(scheduledJobName))
                {
                    sbMessage.Append("\r\n");
                    sbMessage.Append("****************************************************************************************");
                    sbMessage.Append("\r\n");
                    sbMessage.Append("Step F :Checking " + scheduledJobName + "  in the TS process.");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                    sbMessage = new StringBuilder();

                    if (scheduledJobName.ToUpper() == "OTIF")
                    {
                        Process[] pname = Process.GetProcessesByName("VIP_VendorGenerateOTIFScheduler");
                        if (pname.Length == 0 && isOTIFSchedulerStarted == false)
                        {
                            isOTIFSchedulerStarted = true;
                            //ProcessStartInfo pi = new ProcessStartInfo();
                            //pi.FileName = ReportOTIFExePath + "VIP_VendorGenerateOTIFScheduler.exe";

                            sbMessage.Append("\r\n");
                            sbMessage.Append("Step L3 : Launching " + ReportOTIFExePath + "VIP_VendorGenerateOTIFScheduler.exe");

                            ExecuteAsAdmin(ReportOTIFExePath + "VIP_VendorGenerateOTIFScheduler.exe");

                            //Process.Start(pi);

                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************************************************************");
                            sbMessage.Append("\r\n");
                            sbMessage.Append("Step G :After Checking VIP_VendorGenerateOTIFScheduler strated with path " + ReportOTIFExePath);

                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************************************************************");
                            sbMessage.Append("\r\n");
                            LogUtility.SaveTraceLogEntry(sbMessage);
                            sbMessage = new StringBuilder();
                        }
                        if (pname.Length > 0 )
                        {
                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************************************************************");
                            sbMessage.Append("\r\n");
                            sbMessage.Append("Step E :Already running   OTIF scheduler Process in TS");
                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************************************************************");
                            sbMessage.Append("\r\n");
                            LogUtility.SaveTraceLogEntry(sbMessage);
                            sbMessage = new StringBuilder();
                        }
                    }
                    else if (scheduledJobName.ToUpper() == "VSC")
                    {
                        Process[] pname = Process.GetProcessesByName("VIP_ScorecardScheduler");
                        if (pname.Length == 0)
                        {
                            //ProcessStartInfo pi = new ProcessStartInfo();
                            //pi.FileName = ReportVSCExePath + "VIP_ScorecardScheduler.exe";

                            sbMessage.Append("\r\n");
                            sbMessage.Append("Step L4 : Launching " + ReportVSCExePath + "VIP_ScorecardScheduler.exe");

                            ExecuteAsAdmin(ReportVSCExePath + "VIP_ScorecardScheduler.exe");

                            //Process.Start(pi);

                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************************************************************");
                            sbMessage.Append("\r\n");
                            sbMessage.Append("Step H:After Checking VIP_ScorecardScheduler strated with path " + ReportVSCExePath);
                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************************************************************");
                            sbMessage.Append("\r\n");
                            LogUtility.SaveTraceLogEntry(sbMessage);
                            sbMessage = new StringBuilder();
                        }
                        if (pname.Length > 0)
                        {
                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************************************************************");
                            sbMessage.Append("\r\n");
                            sbMessage.Append("Step E :Already running   VSC scheduler Process in TS");
                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************************************************************");
                            sbMessage.Append("\r\n");
                            LogUtility.SaveTraceLogEntry(sbMessage);
                            sbMessage = new StringBuilder();
                        }
                    }
                }
                /*
                // Check DB for OTIF/VSC  Schedular Execution.             
                TimeSpan currentTime = DateTime.Now.TimeOfDay;
                SqlCommand vscSqlCommand = new SqlCommand();
                SqlConnection vscSqlConnection = new SqlConnection(connString);
                vscSqlCommand.CommandType = CommandType.Text;
                //vscSqlCommand.CommandText = "Select * from dbo.SchedulingApp where IsExecuted=0 and SchedulerTime<='" + currentTime + "'";
                vscSqlCommand.CommandText = "Select * from dbo.SchedulingApp where IsExecuted=0";
                vscSqlCommand.Connection = vscSqlConnection;
                vscSqlCommand.Connection.Open();
                DataTable dt = new DataTable();
                dt.Load(vscSqlCommand.ExecuteReader());
                vscSqlCommand.Connection.Close();

                if (dt.Rows.Count > 0)
                {

                    var schedulerName = dt.Rows[0][1];
                    var schedulerId = dt.Rows[0][0];

                    bool IsOtifReady = GetSchedulerInfo("OTIF");
                    bool IsVscReady = GetSchedulerInfo("VSC");

                    if (IsOtifReady)
                    {
                        Process[] pname = Process.GetProcessesByName("VIP_VendorGenerateOTIFScheduler");
                        if (pname.Length == 0 && isOTIFSchedulerStarted == false)
                        {
                            isOTIFSchedulerStarted = true;
                            ProcessStartInfo pi = new ProcessStartInfo();
                            pi.FileName = ReportOTIFExePath + "VIP_VendorGenerateOTIFScheduler.exe";
                            Process.Start(pi);

                            //vscSqlCommand.CommandType = CommandType.Text;
                            //vscSqlCommand.Connection = vscSqlConnection;
                            //vscSqlCommand.Connection.Open();
                            //vscSqlCommand.CommandText = "UPDATE SchedulingApp SET IsExecuted=1 WHERE SchedulerId='" + schedulerId + "'";
                            //vscSqlCommand.ExecuteNonQuery();
                            //vscSqlCommand.Connection.Close();
                        }
                    }
                    else if (IsVscReady)
                    {
                        Process[] pname = Process.GetProcessesByName("VIP_ScorecardScheduler");
                        if (pname.Length == 0)
                        {
                            ProcessStartInfo pi = new ProcessStartInfo();
                            pi.FileName = ReportVSCExePath + "VIP_ScorecardScheduler.exe";
                            Process.Start(pi);

                            //vscSqlCommand.CommandType = CommandType.Text;
                            //vscSqlCommand.Connection = vscSqlConnection;
                            //vscSqlCommand.Connection.Open();
                            //vscSqlCommand.CommandText = "UPDATE SchedulingApp SET IsExecuted=1 WHERE SchedulerId='" + schedulerId + "'";
                            //vscSqlCommand.ExecuteNonQuery();
                            //vscSqlCommand.Connection.Close();
                        }
                    }
                }*/
                #endregion  OTIF/VSC  Schedular

                #region Check last month's OTIF scheculer was triggred by task scheduler or not
                //int month = 1;
                //string applicationName = "OTIFReportScheduler";
                //DateTime fromDate = new DateTime();
                //DateTime toDate = new DateTime();

                //if (month > 0)
                //{
                //    DateTime dtBase = DateTime.Now.AddMonths(-month);
                //    fromDate = new DateTime(dtBase.Year, dtBase.Month, 1);
                //    toDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.AddMonths(1).AddDays(-1).Day);

                //    bool isSchedulerInProcess = IsSchedulerInProcess(applicationName, fromDate, toDate);
                //    if (!isSchedulerInProcess)
                //    {
                //        Process[] pname = Process.GetProcessesByName("VIP_VendorGenerateOTIFScheduler");
                //        if (pname.Length == 0)
                //        {
                //            isOTIFSchedulerStarted = true;
                //            ProcessStartInfo pi = new ProcessStartInfo();
                //            pi.FileName = ReportOTIFExePath + "VIP_VendorGenerateOTIFScheduler.exe";
                //            Process.Start(pi);
                //        }
                //    }
                //}
                #endregion

                sbMessage.Append("Step 4 : End at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

            }
            catch (Exception ex)
            {
                var sbMessage = new StringBuilder();
                sbMessage.Append("Step Exception : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                sbMessage.Append(ex.Message);
                if (ex.InnerException != null)
                {
                    sbMessage.Append(ex.InnerException.StackTrace);
                }
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
        }

        private static bool IsSchedulerInProcess(string applicationName, DateTime fromDate, DateTime toDate)
        {
            SqlCommand vscSqlCommand = new SqlCommand();
            SqlConnection vscSqlConnection = new SqlConnection(connString);
            vscSqlCommand.CommandType = CommandType.Text;
            vscSqlCommand.CommandText = @"Select * from MAS_SchedulingAppHistory Where ApplicationName='" + applicationName + "' And FromDate='" + fromDate + "' And ToDate='" + toDate + "'";
            vscSqlCommand.Connection = vscSqlConnection;
            vscSqlCommand.Connection.Open();
            DataTable dt = new DataTable();
            dt.Load(vscSqlCommand.ExecuteReader());
            vscSqlCommand.Connection.Close();
            if (dt.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        private static bool GetSchedulerInfo(string applicationName)
        {
            SqlConnection sqlConnection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand("spSchedulingApp", sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Action", "GetTopOneScheduledJob");
            command.Parameters.AddWithValue("@ApplicationName", applicationName);
            command.Parameters.AddWithValue("@SchedulerDate", DateTime.Now.Date);
            command.Parameters.AddWithValue("@SchedulerTime", DateTime.Now.TimeOfDay);
            sqlConnection.Open();
            DataTable dt = new DataTable();
            dt.Load(command.ExecuteReader());
            sqlConnection.Close();
            if (dt.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        private static string GetTopScheduledJob()
        {
            string jobName = string.Empty;
            SqlConnection sqlConnection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand("spSchedulingApp", sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Action", "GetTopScheduledJob");
            sqlConnection.Open();
            DataTable dt = new DataTable();
            dt.Load(command.ExecuteReader());
            sqlConnection.Close();

            if (dt.Rows.Count > 0)
            {
                jobName = dt.Rows[0].Field<string>("ApplicationName");
            }
            return jobName;
        }

        private static void SaveMonthlySchedulerEntry(string applicationName)
        {
            var dateNow = DateTime.Now;
            int day = 3;
            int hour = 21;
            int minutes = 0;

            if (applicationName == "VSC")
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationSettings.AppSettings["ScoreCardDayOfMonth"]))
                    day = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["ScoreCardDayOfMonth"]);
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationSettings.AppSettings["ScoreCardTimeIn24Hours"]))
                    hour = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["ScoreCardTimeIn24Hours"]);
            }
            else if (applicationName == "OTIF")
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationSettings.AppSettings["OTIFDayOfMonth"]))
                    day = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["OTIFDayOfMonth"]);
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationSettings.AppSettings["OTIFTimeIn24Hours"]))
                    hour = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["OTIFTimeIn24Hours"]);

            }

            TimeSpan scheduledTime = new TimeSpan(hour, minutes, 0);


            // Execute only 3rd of every Month
            DateTime dateToInsertEntry = new DateTime(dateNow.Year, dateNow.Month, day);

            DateTime scheduleDate = new DateTime(dateNow.Year, dateNow.Month, day, 0, minutes, 0);

            // Check Is Already Scheduled
            DateTime dtBase = DateTime.Now.AddMonths(-1);

            int month = 1;
            int year = dtBase.Year;


            if (!IsSchedulerAlreadySchedule(month, year, applicationName))
            {
                // Insert Enrty
                InsertScheduler(month, year, scheduledTime, scheduleDate, applicationName);
            }
        }

        private static bool IsSchedulerAlreadySchedule(int month, int year, string applicationName)
        {
            SqlConnection sqlConnection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand("spSchedulingApp", sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Action", "isAlreadySchedule");
            command.Parameters.AddWithValue("@ApplicationName", applicationName);
            command.Parameters.AddWithValue("@Month", month);
            command.Parameters.AddWithValue("@Year", year);
            sqlConnection.Open();
            DataTable dt = new DataTable();
            dt.Load(command.ExecuteReader());
            sqlConnection.Close();

            if (dt.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        private static void InsertScheduler(int month, int year, TimeSpan SchedulerTimeToRun, DateTime SchedulerDateToRun, string applicationName)
        {
            SqlConnection sqlConnection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand("spSchedulingApp", sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Action", "addScheduling");
            command.Parameters.AddWithValue("@ApplicationName", applicationName);
            command.Parameters.AddWithValue("@Month", month);
            command.Parameters.AddWithValue("@Year", year);
            command.Parameters.AddWithValue("@IsExecuted", false);
            command.Parameters.AddWithValue("@InputDate", DateTime.Now);
            command.Parameters.AddWithValue("@UserID", 1);
            command.Parameters.AddWithValue("@SchedulerTime", SchedulerTimeToRun);
            command.Parameters.AddWithValue("@SchedulerDate", SchedulerDateToRun);
            sqlConnection.Open();
            command.ExecuteNonQuery();
            sqlConnection.Close();
        }

        public static void ExecuteAsAdmin(string fileName)
        {
            Process proc = new Process();
            proc.StartInfo.FileName = fileName;
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.Verb = "runas";         
            proc.Start();
        }
    
    }
}

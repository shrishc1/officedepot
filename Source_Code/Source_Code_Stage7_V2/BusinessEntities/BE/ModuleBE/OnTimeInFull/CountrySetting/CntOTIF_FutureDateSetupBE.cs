﻿
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Security;
using System;
namespace BusinessEntities.ModuleBE.OnTimeInFull.CountrySetting {
    [Serializable]
    public class CntOTIF_FutureDateSetupBE :BaseBe {
        public int CountryID { get; set; }
        public MAS_CountryBE Country { get; set; }
        public int? FutureDatePOMonth { get; set; }
        public int? FutureDatePODay { get; set; }
        public string FutureDate { get; set; }
        public SCT_UserBE User { get; set; }
    }
}

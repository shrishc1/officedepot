﻿
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System;

namespace BusinessEntities.ModuleBE.Appointment.SiteSettings {
    [Serializable]
    public class MASSIT_DeliveryConstraintSpecificBE : BaseBe{

        public int? DeliveryConstraintSpecificID { get; set; }

        public System.DateTime? WeekStartDate { get; set; }

        public MASSIT_DeliveryConstraintBE DeliveryConstraint { get; set; }        
    }
}

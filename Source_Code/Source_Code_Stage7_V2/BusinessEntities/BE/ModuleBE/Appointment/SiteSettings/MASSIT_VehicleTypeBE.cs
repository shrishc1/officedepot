﻿using BusinessEntities.ModuleBE.AdminFunctions;
using System;

namespace BusinessEntities.ModuleBE.Appointment.SiteSettings {
    [Serializable]
    public class MASSIT_VehicleTypeBE : BusinessEntities.BaseBe {
        public string Action { get; set; }
        public int VehicleTypeID { get; set; }
        public int SiteID { get; set; }
        public MAS_SiteBE Site { get; set; }
        public string VehicleType { get; set; }
        public int APP_PalletsUnloadedPerHour { get; set; }
        public int APP_CartonsUnloadedPerHour { get; set; }
        public int TotalUnloadTimeInMinute { get; set; }
        public string TotalUnloadTimeString { get; set; }
        public bool IsActive { get; set; }

        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
        public bool IsNarrativeRequired { get; set; }
    }
}

﻿using System;
namespace BusinessEntities.ModuleBE.Appointment.SiteSettings {
    [Serializable]
    public class MASSIT_FixedSlotBE : BaseBe {


        public int FixedSlotID { get; set; }

        public int SiteID { get; set; }

        public int? VendorID { get; set; }

        public int? CarrierID { get; set; }

        public string ScheduleType { get; set; }

        public int? SlotTimeID { get; set; }

        public int? SiteDoorNumberID { get; set; }

        public bool? Monday { get; set; }

        public bool? Tuesday { get; set; }

        public bool? Wednesday { get; set; }

        public bool? Thursday { get; set; }

        public bool? Friday { get; set; }

        public bool? Saturday { get; set; }

        public bool? Sunday { get; set; }

        public int? MaximumPallets { get; set; }

        public int? MaximumCatrons { get; set; }

        public int? MaximumLines { get; set; }

        public string WeekDay { get; set; }

        public BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE Vendor { get; set; }

        public BusinessEntities.ModuleBE.AdminFunctions.SYS_SlotTimeBE SlotTime { get; set; }

        public BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_DoorNoSetupBE DoorNo { get; set; }

        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }



        public string SiteIDs { get; set; }
    }


}

﻿using BusinessEntities.ModuleBE.AdminFunctions;
using System;

namespace BusinessEntities.ModuleBE.Appointment.CountrySetting {
    [Serializable]
    public class MASCNT_CrossDockBE : BusinessEntities.BaseBe {
        public string Action { get; set; }
        public int CrossDockingID { get; set; }
        public int CountryID { get; set; }        
        public int SiteID { get; set; }        
        public int DestinationSiteID { get; set; }      
    
        public MAS_CountryBE Country { get; set; }
        public MAS_SiteBE Site { get; set; }
        public MAS_SiteBE DestinationSite { get; set; }
       
       
        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
    }
}

﻿using BusinessEntities.ModuleBE.AdminFunctions;
using System;

namespace BusinessEntities.ModuleBE.Appointment.CountrySetting {
    [Serializable]
    public class MASCNT_DeliveryTypeBE : BusinessEntities.BaseBe {         
            public string Action { get; set; }
            public int DeliveryTypeID { get; set; }
            public int CountryID { get; set; }
            public MAS_CountryBE Country { get; set; }
            public string DeliveryType { get; set; }
            public string EnabledAsVendorOption { get; set; }
            public bool IsEnabledAsVendorOption { get; set; }
            public bool IsActive { get; set; }
            public string CountryDeliveryType { get; set; }
            public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }
            public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace BusinessEntities.ModuleBE.Appointment.Booking
{
    public class VendorDetails
    {
        public string VendorName { get; set; }
        public Color CellColor { get; set; }
        public bool isCurrentSlotFixed { get; set; }
        public string CurrentFixedSlotID { get; set; }
    }
}

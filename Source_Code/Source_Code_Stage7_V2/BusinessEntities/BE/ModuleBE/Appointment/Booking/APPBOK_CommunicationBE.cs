﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.Appointment.Booking
{
    public class APPBOK_CommunicationBE : BaseBe
    {
        public int CommunicationID { get; set; }
        public string CommunicationType { get; set; }
        public string Subject { get; set; }
        public string SentFrom { get; set; }
        public string SentTo { get; set; }
        public string Body { get; set; }
        public string SentDate { get; set; }
        public int SendByID { get; set; }
        public object BookingID { get; set; }
        public string FullName { get; set; }
    }

    public class CommunicationType
    {
        public enum Enum
        {
            NotDefined = 0,
            BookingConfirmation,
            UnexpectedAccepted,
            UnexpectedRefused,
            RefusedAll,
            RefusedPartial,
            DeliveryIssue,
            NoShow,
            BookingAmended,
        }

        public static string Name(int id)
        {
            return Name((Enum)id);
        }

        public static string Name(Enum id)
        {
            switch (id)
            {
                case Enum.NotDefined:
                    return "Not Defined";
                case Enum.BookingConfirmation:
                    return "Booking Confirmation";
                case Enum.UnexpectedAccepted:
                    return "Unexpected Accepted";
                case Enum.UnexpectedRefused:
                    return "Unexpected Refused";
                case Enum.RefusedAll:
                    return "Refused All";
                case Enum.RefusedPartial:
                    return "Refused Partial";
                case Enum.DeliveryIssue:
                    return "Delivery Issue";
                case Enum.NoShow:
                    return "No Show";
                case Enum.BookingAmended:
                    return "Booking Amended";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static List<KeyValuePair<int, string>> List()
        {
            return System.Enum.GetValues(typeof(Enum)).Cast<int>().Select(x => new KeyValuePair<int, string>(x, Name(x))).ToList();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;

namespace BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy
{
    [Serializable]
    public class DISLog_WorkFlowBE : BaseBe
    {

        public int? WorkflowActionID { get; set; }
        public int? DiscrepancyLogID { get; set; }
        public DateTime? LogDate { get; set; }
        public string ActionName { get; set; }
        public string StageCompletedForWhom { get; set; }
        public int? DiscrepancyWorkFlowID { get; set; }
        public string Comment { get; set; }
        public string VenCommunicationType { get; set; }
        public bool StageCompleted { get; set; }
      

        //INV Properties
        public string INV001_GoodsKeptDecision { get; set; }
        public string INV001_NAX_PO { get; set; }

        public char? INV002_GoodsDecision { get; set; }

        public char? INV006_GoodsDecision { get; set; }       

        public string INV007_IsCharged { get; set; }
        public decimal? INV007_CarriageCost { get; set; }       

        public string INV009_IsCharged { get; set; }
        public int? INV009_NoOfPallets { get; set; }
        public decimal? INV009_LabourCost { get; set; }       

        public string INV008_ReworkType { get; set; }       

        public string INV010_IsCharged { get; set; }
        public decimal? INV010_LabourCost { get; set; }    

       //GIN Properties

        public bool GIN002_IsGoodsCollected { get; set; }
        public DateTime? GIN002_ArrangedCollectionDate { get; set; }
        public string GIN002_Carrier { get; set; }
        public string GIN002_CollectionAuthorisationNumber { get; set; }      

        public int? GIN003_Carrier { get; set; }
        public string GIN003_CarrierTrackingNumber { get; set; }
        public decimal? GIN003_CarriageCharge { get; set; }
        public int? GIN003_PalletsExchanged { get; set; }
        public int? GIN003_CartonsExchanged { get; set; }
        
        public decimal? GIN010_LabourCost { get; set; }    

        public decimal GIN012_LabourCost { get; set; }  
      
        public bool GIN014_IsValidDispute {get;set;}
        public bool GIN014_IsStockCountCarriedOn { get; set; }
        public string GIN014_ValidDisputeReasonError { get; set; }
        public string GIN014_NotValidDisputeReasonError { get; set; }
        public string GIN014_DiscrepancyNoIfExist { get; set; }

        //VEN Properties
        public string VEN001_ReasonForGoodsOverDelivered { get; set; }
        public string VEN001_HowGoodsReturned { get; set; }
        public DateTime? VEN001_DateGoodsAreToBeCollected { get; set; }
        public int? VEN001_CarrierWhoWillCollectGoods { get; set; }
        public string VEN001_CollectionAuthorisationNumber { get; set; }     
      
        public string VEN002_ReasonForGoodsOverDelivered { get; set; }
      
        public string VEN005_HowGoodsReturned { get; set; }
        public DateTime? VEN005_DateGoodsAreToBeCollected { get; set; }
        public int? VEN005_CarrierWhoWillCollectGoods { get; set; }
        public string VEN005_CollectionAuthorisationNumber { get; set; }
        

        public string VEN006_HowGoodsReturned { get; set; }
        public DateTime? VEN006_DateGoodsAreToBeCollected { get; set; }
        public int? VEN006_CarrierWhoWillCollectGoods { get; set; }
        public string VEN006_CollectionAuthorisationNumber { get; set; }
      

        public string VEN011_HowGoodsReturned { get; set; }
        public DateTime? VEN011_DateGoodsAreToBeCollected { get; set; }
        public int? VEN011_CarrierWhoWillCollectGoods { get; set; }
        public string VEN011_CollectionAuthorisationNumber { get; set; }
      

        public string VEN017_HowGoodsReturned { get; set; }
        public DateTime? VEN017_DateGoodsAreToBeCollected { get; set; }
        public int? CarrierWhoWillCollectGoods { get; set; }
        public string CarrierOther { get; set; }
        public string VEN017_CollectionAuthorisationNumber { get; set; }
      
        //AP Properties
        public char? ACP001_NoteType { get; set; }
        public string ACP001_NoteNumber { get; set; }
        public decimal? ACP001_ValueExpected { get; set; }
        public decimal? ACP001_ShortageValueExpected { get; set; }
        public decimal? ACP001_WrongPackValueExpected { get; set; }
        public decimal? ACP001_DamageValueExpected { get; set; }
        public decimal? ACP001_QualityValueExpected { get; set; }
        public decimal? ACP001_ActualValue { get; set; }
        public decimal? ACP001_IncorrectProductValueExpected { get; set; }
        public decimal? ACP001_ChaseValueExpected { get; set; }
      
        public char? ACP002_NoteType { get; set; }
        public string ACP002_NoteNumber { get; set; }
        public decimal? ACP002_CarriageCost { get; set; }
        public decimal? ACP002_ActualValue { get; set; }
       
       
        public char? ACP003_NoteType { get; set; }
        public string ACP003_NoteNumber { get; set; }
        public decimal? ACP003_LabourCost { get; set; }
        public decimal? ACP003_ActualValue { get; set; }
      
        public string PurchaseOrderNumber { get; set; }
        public bool INV012_PassBackToGoodsIn { get; set; }
        public bool Ven26_YesNo { get; set; }        
    }
}

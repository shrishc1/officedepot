﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;

namespace BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy
{
    [Serializable]
    public class DISLog_QueryDiscrepancyBE : BaseBe
    {
        public int? QueryDiscrepancyID { get; set; }
        public int? DiscrepancyLogID { get; set; }
        public string VendorComment { get; set; }
        public string GoodsInComment { get; set; }
        public string GoodsInAction { get; set; }
        public DateTime? VendorQueryDate { get; set; }
        public DateTime? GoodsInQueryDate { get; set; }
        public string Query { get; set; }
        public int? VendorUserId { get; set; }
        public string VendorUserName { get; set; }
        public int? GoodsInUserId { get; set; }
        public string GoodsInUserName { get; set; }
        public string QueryAction { get; set; }
    }
}

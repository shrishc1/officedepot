﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy {
    [Serializable]
    public class DISLog_ImagesBE : BaseBe{
        public int? DiscrepancyLogImageID { get; set; }
        public string ImageName { get; set; }
        public DiscrepancyBE Discrepancy { get; set; }

        public string ImageNames { get; set; }
        public string DiscrepancyLogImageIDs { get; set; }
        public decimal? Freight_Charges { get; set; }
    }
}

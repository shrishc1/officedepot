﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.Discrepancy
{
    public class DiscrepancyEmailTracker :BaseBe
    {
        public string EmailType { get; set; }
        public string EmailID { get; set; }
        public int UserID { get; set; }
        public int? ID { get; set; }
        public bool IsActive { get; set; }
    }
}

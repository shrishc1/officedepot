﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.ReportRequest {
    [Serializable]
    public class ReportRequestBE : BaseBe 
    {
        public int ReportRequestID { get; set; }
        public string ModuleName { get; set; }
        public string ReportName { get; set; }
        public string ReportNamePath { get; set; }
        public string ReportAction { get; set; }
        public string ReportDatatableName { get; set; }
        public string CountryIDs { get; set; }
        public string CountryName { get; set; }
        public string SiteIDs { get; set; }
        public string SiteName { get; set; }
        public string StockPlannerIDs { get; set; }
        public string StockPlannerName { get; set; }
        public string VendorIDs { get; set; }
        public string VendorName { get; set; }
        public string OD_SKU_No { get; set; }
        public string ItemClassification { get; set; }
        public DateTime? DateTo { get; set; }
        public DateTime? DateFrom { get; set; }
        public string ReportType { get; set; }
        public int PendingSKUs { get; set; }
        public int DiscSKUs { get; set; }
        public int ActiveSKUs { get; set; }
        public string PurchaseOrder { get; set; }
        public int? PurchaseOrderDue { get; set; }
        public int? PurchaseOrderReceived { get; set; }
        public string OfficeDepotSKU { get; set; }
        public string SKUMarkedAs { get; set; }
        public string RequestStatus { get; set; }
        public int? UserID { get; set; }
        public DateTime RequestTime { get; set; }
        public string ReportPosition { get; set; }
        public string UserName { get; set; }
        public string GeneratedReportCount { get; set; }
        public string ReportNameAndType { get; set; }
        public string DateRange { get; set; }
        public DateTime? ReportGeneratedTime { get; set; }
        public int EuropeanorLocal { get; set; }
        public string SelectedRMSCategoryIDs { get; set; }
        public string SelectedRMSCategoryName { get; set; }
        public string SelectedSKuGroupingIDs { get; set; }
        public int UnderReviewSKUs { get; set; }
        public string StockPlannerGroupingIDs { get; set; }
        public string ODCatCode { get; set; }
        public string CustomerIDs { get; set; }

        public bool IsGenerated { get; set; }
    }
}

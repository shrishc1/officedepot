﻿using System;
namespace BusinessEntities.ModuleBE.Security {
    [Serializable]
    public class SCT_UserRoleBE : BusinessEntities.BaseBe
    {

        public int UserRoleID { get; set; }

        public string RoleName { get; set; } 
    }
}

﻿using System;
namespace BusinessEntities.ModuleBE.Security
{

    [Serializable]
    public class SCT_UserBE : BaseBe
    {

        public int UserID { get; set; }

        public string FullName { get; set; }

        public string FirstName { get; set; }

        public string Lastname { get; set; }

        public string LoginID { get; set; }

        public string Password { get; set; }

        public string EmailId { get; set; }

        public string PhoneNumber { get; set; }       

        public string FaxNumber { get; set; }

        public bool IsActive { get; set; }

        public int? UserRoleID { get; set; }

        public int? VendorID { get; set; }

        public string VendorName { get; set; }

        public int? CarrierID { get; set; }

        public string AccountsPayablePNumber { get; set; }

        public BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE Site { get; set; }

        public string RoleName { get; set; }

        public string RoleTypeFlag { get; set; }

        public int IsDeliveryRefusalAuthorization { get; set; }

        public string UserIDPassword { get; set; }

        public int? LanguageID { get; set; }

        public string Language { get; set; }

        public string CarrierIDs { get; set; }
        public string CarrierName { get; set; }

        public char? SchedulingContact { get; set; }

        public char? DiscrepancyContact { get; set; }

        public char? OTIFContact { get; set; }

        public char? ScorecardContact { get; set; }

        public string AccountStatus { get; set; }

        public int? CountryId { get; set; }

        public int? StockPlannerId { get; set; }

        public string StockPlannerCountry { get; set; }

        public string SelectedStockPlannerIds { get; set; }

        public int? MasterTemplateID { get; set; }

        public int? ReportTemplateID { get; set; }

        public string RejectionComments { get; set; }

        public string VendorNo { get; set; }

        public string Country { get; set; }

        //SCT_UserSite

        public Boolean IsDefaultSite { get; set; }
        public int? TemplateID { get; set; }
        public int? SiteId { get; set; }
        public string SelectedSites { get; set; }

        public int? CarrierCountryID { get; set; }

        public string APUserName
        {
            get
            {
                return string.Format("{0} ( {1} )",this.FullName, this.EmailId);
            }
        }


        public string OtherCarrierName { get; set; }

        public string VendorIDs { get; set; }

        public DateTime? CratedDate { get; set; }
        public DateTime? LastLoggedOn { get; set; }
        public int? Elapseddays { get; set; }
        public string SiteName { get; set; }
        
        public string CountryIds { get; set; }
        public string AppointmentSites { get; set; }
        public string DiscrepancySites { get; set; }
        public string OTIFCountries { get; set; }
        public string ScorecardCountries { get; set; }
        public string UserName { get; set; }
        public string SearchVendorText { get; set; }
        public string SearchCarrierText { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public string UserSearchText { get; set; }

        //---Stage 7 V1 point 10-----//
        //----------------------//
        public string DefaultSite { get; set; }
        public string TemplateName { get; set; }

        public string ReportType { get; set; }

        public BusinessEntities.ModuleBE.AdminFunctions.MAS_VendorBE Vendor { get; set; }

        public BusinessEntities.ModuleBE.AdminFunctions.MAS_MaintainVendorPointsContactBE VendorPOC { get; set; }
    }

}

﻿
namespace BusinessEntities.ModuleBE.AdminFunctions
{
    public class SupplierGuidelineBE : BaseBe
    {
        public int SupplierGuidlineID { get; set; }

        public string GuidelineDescriptionID { get; set; }

        public string DocumentName { get; set; }
    }
}

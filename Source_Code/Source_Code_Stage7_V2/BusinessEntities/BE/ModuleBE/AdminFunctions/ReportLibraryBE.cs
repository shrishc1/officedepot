﻿using System;

namespace BusinessEntities.ModuleBE.AdminFunctions
{
    [Serializable]
    public class ReportLibraryBE : BaseBe
    {

        public int? ReportLibraryId { get; set; }
        public int? ScreenId { get; set; }
        public string ScreenName { get; set; }
        public string ScreenUrl { get; set; }
        public int? ModuleId { get; set; }
        public int? OrderBy { get; set; }
        public string ModuleName { get; set; }
        public string ReportName { get; set; }
        public string Description { get; set; }
        public string FurtherInfo { get; set; }
    }
}

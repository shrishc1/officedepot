using System;
using System.Net;
using System.Text;
using System.IO;

public class HttpRequestResponse
{
    /// <summary>
    /// This Function is used to get an HTTP response
    /// This function is also use to POST a form on a website
    /// </summary>
    /// <param name="URL">This attribute is use to set the URL location</param>
    /// <param name="Post">This attribute Contain the POSTDATA of a website. This can be null string also</param>
    /// <returns></returns>
    public static string getHTTP(string URL, string Post)
    {
        HttpWebRequest httprequest;
        HttpWebResponse httpresponse;
        StreamReader bodyreader;
        String bodytext = "";
        Stream responsestream = null;
        Stream requeststream = null;
        try
        {
            httprequest = (HttpWebRequest)WebRequest.Create(URL);
            httprequest.Method = "POST";
            httprequest.ContentType = "application/x-www-form-urlencoded";
            httprequest.ContentLength = Post.Length;
            //httprequest.ReadWriteTimeout = 60000; // request timeout, The default is 100,000 milliseconds (100 seconds). 
            //httprequest.Timeout = 60000;
            requeststream = httprequest.GetRequestStream();
            requeststream.Write(Encoding.ASCII.GetBytes(Post), 0, Post.Length);
            requeststream.Close();
            httpresponse = (HttpWebResponse)httprequest.GetResponse();
            responsestream = httpresponse.GetResponseStream();
            bodyreader = new StreamReader(responsestream);
            bodytext = bodyreader.ReadToEnd();
            responsestream.Close();
            return bodytext;
        }
        finally
        {
            if (requeststream != null)
            {
                requeststream.Dispose();
            }
            if (responsestream != null)
            {
                responsestream.Dispose();
            }
        }
    }
}


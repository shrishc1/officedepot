﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Utilities
{
    public class GenericListHelper<T>
    {
        public static List<T> SortList(List<T> listToBeSorted, string sortBasePropertyName, SortDirection sortDirection)
        {
            IOrderedEnumerable<T> result;

            if (!string.IsNullOrEmpty(sortBasePropertyName))
            {
                if (sortDirection == SortDirection.Ascending)
                {
                    result = from i in listToBeSorted
                             orderby  GetSortBaseProperty(i, sortBasePropertyName)
                             select i;
                }
                else
                {
                    result = from i in listToBeSorted
                             orderby GetSortBaseProperty(i, sortBasePropertyName) descending
                             select i;
                }
                return result.ToList<T>();
            }
            else
            {
                return listToBeSorted;
            }
        }

        protected static object GetSortBaseProperty(object o, string SortBasePropertyName)
        {
            if (SortBasePropertyName.Contains('.'))
            {
                int dotIndex = SortBasePropertyName.IndexOf(".") + 1;
                object top1Prop = o.GetType().InvokeMember(SortBasePropertyName.Split('.')[0], System.Reflection.BindingFlags.GetProperty, null, o, null);
                return GetSortBaseProperty(top1Prop, SortBasePropertyName.Substring(dotIndex, SortBasePropertyName.Length - dotIndex));
            }
            else
            {
                return o.GetType().InvokeMember(SortBasePropertyName, System.Reflection.BindingFlags.GetProperty, null, o, null);
            }
        }
    }
}

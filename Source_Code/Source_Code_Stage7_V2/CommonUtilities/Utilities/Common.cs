﻿using System;
using System.Collections;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;    // Added on 19 Feb 2013
using System.Net;
using System.IO;
using System.Text;
using System.Reflection;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;



namespace Utilities
{
    /// <summary>
    /// Class contains all common methods and static values used in application
    /// </summary>
    public class Common
    {
        /// <summary>
        /// Method to filll dropdown list
        /// </summary>
        /// <param name="drpList">ref DropDownList, DropDownList to be bind</param>
        /// <param name="lstData">List, DataSource</param>
        /// <param name="dataText">string, DataTextField</param>
        /// <param name="dataValueFiels">string, DataValueField</param>
        /// <param name="initialText">string, Default selected text</param>
        /// 


        public static string GlobalURN = string.Empty;
        public static string GlobalDonorName = string.Empty;
        public static string GlobalPledgeDate = string.Empty;

        public static string GlobalURN1 = string.Empty;
        public static string GlobalDonorName1 = string.Empty;
        public static string GlobalPledgeDate1 = string.Empty;


        public static DateTime getTime(string url)
        {
            WebRequest request;

            request = WebRequest.Create(url);
            // Set the Method property of the request to POST.
            request.Method = "GET";
            // Get the response.
            WebResponse response = request.GetResponse();
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            StringBuilder document = new StringBuilder();
            document.Append(reader.ReadToEnd().ToLower());

            int i = document.ToString().IndexOf("in 24-hour notation:");
            document = new StringBuilder(document.ToString().Substring(i + "in 24-hour notation:".Length, 48));
            document.Replace("</td>", "");

            document.Replace("<td>", "");
            document.Replace("<b>", "");

            document.Replace(".", "");
            document.Replace(",", "");

            DateTime? countryTime;
            try
            {
                countryTime = Convert.ToDateTime(document.ToString().Trim().Replace(".", ""));
            }
            catch
            {
                String[] dt = document.ToString().Split(new char[] { ' ' });

                //String[] monthArr = "jan,feb,mar,apr,may,jun,jul,aug,sep,nov,dec".Split(new char[]{','});

                DateTime now = new DateTime(2009, 1, 1);
                Hashtable monthArr = new Hashtable();
                for (int j = 1; j < 13; j++)
                    monthArr.Add(now.AddMonths(j).ToString("MMM").ToLower(), j.ToString());


                int year = Convert.ToInt32(dt[3]);
                int day = Convert.ToInt32(dt[2]);
                String[] tm = dt[4].Split(new char[] { ':' });
                int h = Convert.ToInt32(tm[0]) + (dt[5] == "am" ? 0 : 12);
                int mn = Convert.ToInt32(tm[1]);
                int s = Convert.ToInt32(tm[2]);
                int mth = Convert.ToInt32(monthArr[dt[1]]);
                String month = dt[1];

                countryTime = new DateTime(year, mth, day, h, mn, s);
            }

            return countryTime.Value;
        }

        public static IFormatProvider DateFormat
        {
            get { return new CultureInfo("en-GB"); }
        }

        /// <summary>
        /// Returns the first day of the week that the specified date 
        /// is in. 
        /// </summary>
        public static DateTime GetFirstDayOfWeek(DateTime dayInWeek)
        {
            System.Globalization.CultureInfo cultureInfo = new CultureInfo("en-GB");
            DayOfWeek firstDay = cultureInfo.DateTimeFormat.FirstDayOfWeek;
            DateTime firstDayInWeek = dayInWeek.Date;
            while (firstDayInWeek.DayOfWeek != firstDay)
                firstDayInWeek = firstDayInWeek.AddDays(-1);
            return firstDayInWeek;
        }

        /// <summary>
        /// Method used to remove unnessary space between words
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>        
        public static string TrimString(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                return System.Text.RegularExpressions.Regex.Replace(str, @"\s", " ").Trim();
            }
            else
            {
                return string.Empty;
            }
            //string[] strArr = { " " };
            //string returnString = string.Empty; ;
            //string[] strTempArr = str.Split(strArr, StringSplitOptions.RemoveEmptyEntries);
            //for (int x = 0; x < strTempArr.Length; x++)
            //{
            //    returnString += strTempArr[x].Trim() + " ";
            //}
            //return returnString.Trim();
        }

        public static int? ConvertToNumeric(string str)
        {
            try
            {
                if (str != null && str.Trim() == "")
                    return null;
                else
                    return Convert.ToInt32(str);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static int? ConvertToNumericFRStatus(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
                return Convert.ToInt32(str);
        }

        public static decimal? ConvertToDecimal(string str)
        {
            if (str.Trim() == "")
                return null;
            else
                return Convert.ToDecimal(str);
        }

        public static decimal? ConvertToDecimalPayroll(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
                return Convert.ToDecimal(str);
        }

        public static double? ConvertToDouble(string str)
        {
            if (str.Trim() == "")
                return null;
            else
                return Convert.ToDouble(str);
        }

        public static DateTime? SetDateTime(string str)
        {
            try
            {
                if (str.Trim() == "")
                    return null;
                else
                    return DateTime.Parse(str, Common.DateFormat, DateTimeStyles.NoCurrentDateDefault);

            }
            catch
            {
                try
                {
                    return Convert.ToDateTime(str);
                }
                catch
                {
                    return null;
                }
            }
        }

        public static DateTime? GetDateTime(string str)
        {
            try
            {
                if (str.Trim() == "")
                    return null;
                else
                    return Convert.ToDateTime(str);
            }
            catch
            {
                return null;
            }
        }

        public static string ConvertDateToString(DateTime? dt)
        {
            if (dt == null)
                return "";
            else
            {
                return dt.Value.ToString("dd-MM-yyyy");
            }
        }

        public static bool checkDate(string date)
        {
            bool retVal = false;
            try
            {
                DateTime.ParseExact(date, "dd/MM/yyyy", null);
                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }

        public static DateTime? GetCurrentDateFormat(string strDate)
        {
            if (!string.IsNullOrEmpty(strDate))
            {
                System.Globalization.CultureInfo ci = new CultureInfo("en-GB");

                DateTime FormatedDate = Convert.ToDateTime(strDate, ci);

                return FormatedDate;
            }
            else
            {
                return (DateTime?)null;
            }
        }

        public static DateTime TextToDateFormat(string strddMMyyy)
        {
            CultureInfo cultInfo = new CultureInfo("en-GB", true);
            DateTimeFormatInfo formatInfo = cultInfo.DateTimeFormat;
            formatInfo.ShortDatePattern = "dd/MM/yy";
            formatInfo.ShortDatePattern = "dd/MM/yyyy";
            formatInfo.LongDatePattern = "dd MMMM yyyy";
            formatInfo.FullDateTimePattern = "dd MMMM yyyy HH:mm:ss";

            return System.Convert.ToDateTime(strddMMyyy, formatInfo);

        }

        /// Returns -1 if StartDate less than EndDate, 
        /// 0 if Equal.
        /// 1 if StartDate greater than EndDate  
        public static int CompareDates(string strStartDate, string strEndDate)
        {
            try
            {
                // Creates and initializes the CultureInfo which uses the international sort.
                //I have used English (United Kingdom) cultural inforamtion to convert data into dd/MM/yyyy format 
                CultureInfo cultInfo = new CultureInfo("en-GB", true);
                DateTimeFormatInfo formatInfo = cultInfo.DateTimeFormat;

                formatInfo.ShortDatePattern = "dd/MM/yy";
                formatInfo.ShortDatePattern = "dd/MM/yyyy";
                formatInfo.LongDatePattern = "dd MMMM yyyy";
                formatInfo.FullDateTimePattern = "dd MMMM yyyy HH:mm:ss";

                DateTime startDate = new DateTime();
                DateTime endDate = new DateTime();

                //Convert strStartDate which is passed as string argument into date
                if (!String.IsNullOrEmpty(strStartDate))
                    startDate = System.Convert.ToDateTime(strStartDate, formatInfo);

                //Convert strEndDate which is passed as string argument into date
                if (!String.IsNullOrEmpty(strEndDate))
                    endDate = System.Convert.ToDateTime(strEndDate, formatInfo);

                return DateTime.Compare(startDate, endDate);


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool ConvertToBool(string str)
        {

            if (str == null || str.Trim() == "0" || str.Trim() == "F" || str.Trim() == "False" || str.Trim() == "")
                return false;
            else
                return true;
        }

        public static string TrimStringToFixedLength(string str)
        {
            if (str.Length > 25)
            {
                str = str.Substring(0, 23);
                str += "...";
                return str;
            }
            return str;
        }

        /// <summary>
        /// Method to truncate a string by given length
        /// </summary>
        /// <param name="strText">String to be truncate</param>
        /// <param name="length">length of the expected string</param>
        /// <returns>String of given length from starting</returns>
        public static string TruncateString(string strText, int length)
        {
            if (strText != null && strText.Length > length)
                return strText.Substring(0, length - 3) + "...";
            return strText;
        }

        public static string FormatTime(string strTime) {
            if (strTime != null && strTime.Length < 2)
                return "0" + strTime;
            return strTime;
        }

        public enum EmailTemplateEnum
        {
            ForgotPassword = 3,
            RoleAssigned = 4,
            ComplaintSubmit = 5,
            SitePlan = 6,
            ExternalComplaint = 7,
            InternalComplaint = 8,
            EmployeeCertificationsReminder = 9,
            WelcomeCommonEmail = 12,
            CharityLiveStatusTemplate = 13,
            InternalComplaintSubmit = 22
        }

        public enum BookingStatus
        {
            NoStatusChange = 0,
            ConfirmedSlot = 1,
            DeliveryArrived = 2,
            DeliveryUnloaded = 3,
            RefusedDelivery = 4,
            PartiallyDelivery = 5,
            QualityChecked = 6
        }

        public enum DiscrepancyType
        {
            Overs = 1,
            Shortage = 2,
            GoodReceivedDamaged = 3,
            NoPurchaseOrder = 4,
            NoPaperwork = 5,
            IncorrectProductCode = 6,
            PresentationIssue = 7,
            IncorrectDeliveryAddress = 8,
            PaperworkAmended = 9,
            IncorrectPackSize = 10,
            FailPalletSpecification = 11,
            PrintQualityIssue = 12,
            PrematureInvoiceReceipt = 13,
            GenericDiscrepance = 14,
            ShuttleDiscrepancy = 15,
            ReservationDiscrepancy = 16,
            InvoiceDiscrepancy = 17
        }
        

        /// <summary>
        /// date format = dd/MM/YYYY
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetMM_DD_YYYY(string date)
        {
            string[] dtArr = date.Split('/');
            return new DateTime(Convert.ToInt32(dtArr[2]), Convert.ToInt32(dtArr[1]), Convert.ToInt32(dtArr[0]));
        }

        public static DateTime GetDD_MM_YYYYDatetime(string date)
        {
            string[] dtArr = date.Split('/');
            return (Convert.ToDateTime(dtArr[0] + "/" + dtArr[1] + "/" + dtArr[2]));
        }

        public static string GetDD_MM_YYYY(string date)
        {
            string[] dtArr = date.Split('/');
            return (dtArr[1] + "/" + dtArr[0] + "/" + dtArr[2]);
        }

        public static string GetYYYY_MM_DD(string date)
        {
            string[] dtArr = date.Split('/');
            return (dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0]);
        }

        public static string ToDateTimeInMMDDYYYY(object dateTime)
        {
            if (dateTime != null)
            {
                try
                {
                    DateTime dt = Convert.ToDateTime(dateTime);
                    return dt.ToString("dd/MM/yyyy");
                }
                catch { return ""; }
            }
            else
                return "";
        }

        public static List<T> DynamicSort<T>(List<T> genericList, string sortExpression, string sortDirection)
        {
            string[] sortexpressionspilt = sortExpression.Split('.');

            if (sortexpressionspilt.Length > 1)
            {

                Type genericType = genericList.GetType();
                object sortExpressionValue1 = genericType.GetProperty(sortexpressionspilt[0]).GetValue(genericList, null);
                Type genericType1 = sortExpressionValue1.GetType();
                //return Dynamicspit(genericType1, sortexpressionspilt[1], sortDirection);

            }

            int sortReverser = sortDirection.ToLower().StartsWith("asc") ? 1 : -1;
            Comparison<T> comparisonDelegate = new Comparison<T>(delegate(T x, T y)
            {
                MethodInfo compareToMethod = GetCompareToMethod<T>(x, sortExpression);
                object xSortExpressionValue = x.GetType().GetProperty(sortExpression).GetValue(x, null);
                object ySortExpressionValue = y.GetType().GetProperty(sortExpression).GetValue(y, null);
                object result = compareToMethod.Invoke(xSortExpressionValue, new object[] { ySortExpressionValue });
                return sortReverser * Convert.ToInt16(result);
            });
            genericList.Sort(comparisonDelegate);
            return genericList;
        }

        public static List<T> Dynamicspit<T>(List<T> genericList, string sortExpression, string sortDirection)
        {
            int sortReverser = sortDirection.ToLower().StartsWith("asc") ? 1 : -1;
            Comparison<T> comparisonDelegate = new Comparison<T>(delegate(T x, T y)
            {
                MethodInfo compareToMethod = GetCompareToMethod<T>(x, sortExpression);
                object xSortExpressionValue = x.GetType().GetProperty(sortExpression).GetValue(x, null);
                object ySortExpressionValue = y.GetType().GetProperty(sortExpression).GetValue(y, null);
                object result = compareToMethod.Invoke(xSortExpressionValue, new object[] { ySortExpressionValue });
                return sortReverser * Convert.ToInt16(result);
            });
            genericList.Sort(comparisonDelegate);
            return genericList;
        }

        private static MethodInfo GetCompareToMethod<T>(T genericInstance, string sortExpression)
        {
            Type genericType = genericInstance.GetType();
            string[] sortexpressionspilt = sortExpression.Split('.');

            if (sortexpressionspilt.Length > 1)
            {

                object sortExpressionValue1 = genericType.GetProperty(sortexpressionspilt[0]).GetValue(genericInstance, null);
                Type genericType1 = sortExpressionValue1.GetType();

                object sortExpressionValue2 = genericType1.GetProperty(sortexpressionspilt[1]).GetValue(sortExpressionValue1, null);
                Type sortExpressionType1 = sortExpressionValue2.GetType();
                MethodInfo compareToMethodOfSortExpressionType1 = sortExpressionType1.GetMethod("CompareTo", new Type[] { sortExpressionType1 });

                return compareToMethodOfSortExpressionType1;
            }



            object sortExpressionValue = genericType.GetProperty(sortExpression).GetValue(genericInstance, null);
            Type sortExpressionType = sortExpressionValue.GetType();
            MethodInfo compareToMethodOfSortExpressionType = sortExpressionType.GetMethod("CompareTo", new Type[] { sortExpressionType });

            return compareToMethodOfSortExpressionType;
        }

        public static DataTable ListToDataTable<T>(List<T> list)
        {
            DataTable dt = new DataTable();

            foreach (PropertyInfo info in typeof(T).GetProperties())
            {
                dt.Columns.Add(new DataColumn(info.Name, info.PropertyType));
            }
            foreach (T t in list)
            {
                DataRow row = dt.NewRow();
                foreach (PropertyInfo info in typeof(T).GetProperties())
                {
                    row[info.Name] = info.GetValue(t, null);
                }
                dt.Rows.Add(row);
            }
            return dt;
        }

        public static string EncryptPassword(string plainText)
        {
            string hashValue = "";
            if (!string.IsNullOrEmpty(plainText))
            {
                byte[] saltBytes = Encoding.UTF8.GetBytes("a%#@?,:*");
                // Convert plain text into a byte array.
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

                // Allocate array, which will hold plain text and salt.
                byte[] plainTextWithSaltBytes =
                        new byte[plainTextBytes.Length + saltBytes.Length];

                // Copy plain text bytes into resulting array.
                for (int i = 0; i < plainTextBytes.Length; i++)
                    plainTextWithSaltBytes[i] = plainTextBytes[i];

                // Append salt bytes to the resulting array.
                for (int i = 0; i < saltBytes.Length; i++)
                    plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];

                HashAlgorithm hash = new SHA512Managed();

                // Compute hash value of our plain text with appended salt.
                byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

                // Create array which will hold hash and original salt bytes.
                byte[] hashWithSaltBytes = new byte[hashBytes.Length +
                                                    saltBytes.Length];

                // Copy hash bytes into resulting array.
                for (int i = 0; i < hashBytes.Length; i++)
                    hashWithSaltBytes[i] = hashBytes[i];

                // Append salt bytes to the result.
                for (int i = 0; i < saltBytes.Length; i++)
                    hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

                // Convert result into a base64-encoded string.
                hashValue = Convert.ToBase64String(hashWithSaltBytes);

                // Return the result.
            }
            return hashValue;
        }

        public static bool VerifyPassword(string plainText, string hashValue)
        {

            //// Convert base64-encoded hash value into a byte array.
            //byte[] hashWithSaltBytes = Convert.FromBase64String(hashValue);

            //// We must know size of hash (without salt).
            //int hashSizeInBits, hashSizeInBytes;
            //hashSizeInBits = 512;

            //// Convert size of hash from bits to bytes.
            //hashSizeInBytes = hashSizeInBits / 8;

            //// Make sure that the specified hash value is long enough.
            //if (hashWithSaltBytes.Length < hashSizeInBytes)
            //    return false;

            //// Allocate array to hold original salt bytes retrieved from hash.
            //byte[] saltBytes = new byte[hashWithSaltBytes.Length - hashSizeInBytes];

            //// Copy salt from the end of the hash to the new array.
            //for (int i = 0; i < saltBytes.Length; i++)
            //    saltBytes[i] = hashWithSaltBytes[hashSizeInBytes + i];

            // Compute a new hash string.
            string expectedHashString = EncryptPassword(plainText);

            // If the computed hash matches the specified hash,
            // the plain text value must be correct.
            return (hashValue == expectedHashString);
        }

        public static bool IsPasswordStrong(string password)
        {
            //By Akshay: Removed the need for special character in the password
            //return Regex.IsMatch(password, @"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@!#$%^&+=]).*$");            
            return Regex.IsMatch(password, @"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$");
        }

        public static string EncryptQuery(string pURL)
        {
            string returnURL;
            if (pURL.Contains("?"))
            {
                string[] urlSplit = pURL.Split('?');
                string EncriptedQueryStripg = Encryption.Encrypt(urlSplit[1]);
                returnURL = urlSplit[0] + "?" + EncriptedQueryStripg;
            }
            else
            {
                returnURL = pURL;
            }
            return returnURL;
        }

        public static string HtmlEncode(string textToEncode)
        {
            return HttpUtility.HtmlEncode(textToEncode);
        }

        public static string HtmlDecode(string textToDecode)
        {
            return HttpUtility.HtmlDecode(textToDecode);
        }
    }
}
/// <summary>
/// Class contains all posible action  values to be perform on database
/// </summary>
public static class sqlAction
{
    public static string Search = "Search";
    public static string ValidateUserName = "ValidateUserName";
    public static string ShowAll = "ShowAll";
    public static string ShowAllVendor = "ShowAllVendor";
    public static string ShowById = "ShowById";
    public static string ShowByConsultantId = "ShowByConsultantId";
    public static string EditVendor = "EditVendor";
    public static string ShowVendorId = "ShowVendorId";
    public static string Validate = "Validate";
    public static string ValidateAdd = "ValidateAdd";
    public static string ValidateEdit = "ValidateEdit";
    public static string ValidateCustomer = "ValidateCustomer";
    public static string Sort = "Sort";
    public static string Active = "Active";
    public static string Add = "Add";
    public static string AddDealLocation = "AddDealLocation";
    public static string Edit = "Edit";
    public static string Delete = "Delete";
    public static string ValidateLogin = "ValidateLogin";
    public static string PasswordRecovery = "PasswordRecovery";
    public static string ShowAllRoleModule = "ShowAllRoleModule";
    public static string ValidatePassword = "ValidatePassword";
    public static string UpdateRole = "UpdateRole";
    public static string SortUsers = "SortUsers";
    public static string SearchUsers = "SearchUsers";
    public static string GetUserEmailID = "GetUserEmailID";
    public static string DropdownData = "DropdownData";

    public static string GetSingleValue = "GetSingleValue";
    public static string GetStringSingleValue = "GetStringSingleValue";

    public static class webDeals
    {
        public static string ShowDealsByLocation = "ShowDealsByLocation";
        public static string ShowDealById = "ShowDealById";
    }
}

public static class validationFunctions
{
    public static bool IsNumeric(this object input)
    {
        try
        {
            if (input == null)
                return false;
            else
            {
                int retVal = Convert.ToInt32(input);
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public static bool IsDecimal(this object input)
    {
        try
        {
            if (input == null)
                return false;
            else
            {
                Decimal retVal = Convert.ToDecimal(input);
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    public static bool IsDate(this object input)
    {
        try
        {
            if (input == null)
                return false;
            else
            {
                DateTime retVal = Convert.ToDateTime(input);
                return true;
            }
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="stringDateValue"></param>
    /// <returns></returns>
    public static bool IsValidDateFormat(string stringDateValue)
    {
        try
        {
            CultureInfo CultureInfoDateCulture = new CultureInfo("fr-FR");
            DateTime d = DateTime.ParseExact(stringDateValue, "ddMMyyyy", CultureInfoDateCulture);
            return true;
        }
        catch
        {
            return false;
        }
    }

    private static bool IsNumeric(string input)
    {
        if (input == null) throw new ArgumentNullException("input");
        if (string.IsNullOrEmpty(input)) return false;
        
        //accept a string w/ 1 decimal in it to support values w/ a floating point.
        int periodCount = 0;

        return input.Trim()
                    .ToCharArray()
                    .Where(c =>
                    {
                        if (c == '.') periodCount++;
                        return Char.IsDigit(c) && periodCount <= 1;
                    })
                    .Count() == input.Trim().Length;
    }

}







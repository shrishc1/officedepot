using System;
using System.Text;
using System.IO;

namespace Utilities
{
    public class LogUtility {

        #region ErrorLoging
        public static bool SaveErrorLogEntry(Exception exceptionObject) {
            try {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("\r\n");
                sbMessage.Append("Date              : " + System.DateTime.Now);
                sbMessage.Append("\r\n");
                sbMessage.Append("Error Source      : " + exceptionObject.Source);
                sbMessage.Append("\r\n");

                if (exceptionObject.InnerException == null)
                    sbMessage.Append("Error Message     : " + exceptionObject.Message);
                else
                    sbMessage.Append("Inner Error Message: " + exceptionObject.InnerException.Message);

                if (exceptionObject.InnerException!=null && exceptionObject.InnerException.InnerException != null)
                    sbMessage.Append("Inner InnerException Error Message: " + exceptionObject.InnerException.InnerException.Message);

                sbMessage.Append("\r\n");
                sbMessage.Append("Stack Information : " + exceptionObject.StackTrace);
                sbMessage.Append("\r\n");

                sbMessage.Append("\r\n");
                if (exceptionObject.InnerException != null)
                    sbMessage.Append("Stack Information  InnerException: " + exceptionObject.InnerException.StackTrace);
                sbMessage.Append("\r\n");

                sbMessage.Append("\r\n");
                if (exceptionObject.InnerException!=null && exceptionObject.InnerException.InnerException != null)
                    sbMessage.Append("Stack Information  InnerException: " + exceptionObject.InnerException.InnerException.StackTrace);
                sbMessage.Append("\r\n");


                sbMessage.Append("****************************************************************************************");

                bool flag = LogUtility.WriteError(sbMessage);
                return true;
            }
            catch {
                return false;
            }
        }

        public static bool SaveErrorLogEntry(Exception exceptionObject,string additionalErrorMessage) {
            try {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("\r\n");
                sbMessage.Append("Date              : " + System.DateTime.Now);
                sbMessage.Append("\r\n");
                sbMessage.Append("Error Source      : " + exceptionObject.Source);
                sbMessage.Append("\r\n");
                if(exceptionObject.InnerException==null)
                    sbMessage.Append("Error Message     : " + exceptionObject.Message);
                else
                    sbMessage.Append("Inner Error Message: " + exceptionObject.InnerException.Message);                
                
                sbMessage.Append("\r\n");
                sbMessage.Append("Stack Information : " + exceptionObject.StackTrace);
                sbMessage.Append("\r\n");
                sbMessage.Append("Additonal Information : " + additionalErrorMessage);
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");

                bool flag = LogUtility.WriteError(sbMessage);
                return true;
            }
            catch  {
                return false;
            }
        }

        public static bool WriteError(StringBuilder sbMessage)
        {
            try
            {
                FileStream fs;
                string[] appPath = AppDomain.CurrentDomain.BaseDirectory.Split('\\');

                string LogDirectoryPath = AppDomain.CurrentDomain.BaseDirectory + "Logs\\";
                if (!Directory.Exists(LogDirectoryPath))
                    Directory.CreateDirectory(LogDirectoryPath);

                string logFileName = appPath[appPath.Length - 2] + "_Error_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                if (File.Exists(LogDirectoryPath + logFileName) == true)
                    fs = File.Open(LogDirectoryPath + logFileName, FileMode.Append, FileAccess.Write);
                else
                    fs = File.Create(LogDirectoryPath + logFileName);

                using (StreamWriter sw = new StreamWriter(fs)) {
                    sw.Write(sbMessage.ToString() + Environment.NewLine);
                }
                fs.Close();

                return true;
            }
            catch 
            {
                return false;
            }
        }

        #endregion

        #region Tracing       

        public static bool SaveTraceLogEntry(StringBuilder traceInformation) {
            try {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append(traceInformation);

                bool flag = LogUtility.WriteTrace(sbMessage);
                return true;
            }
            catch {
                return false;
            }
        }        
        
        public static bool WriteTrace(StringBuilder sbMessage) {
            try {
                FileStream fs;
                string[] appPath = AppDomain.CurrentDomain.BaseDirectory.Split('\\');

                string LogDirectoryPath = AppDomain.CurrentDomain.BaseDirectory + "Logs\\";
                if (!Directory.Exists(LogDirectoryPath))
                    Directory.CreateDirectory(LogDirectoryPath);

                string logFileName = appPath[appPath.Length - 2] + "_Trace_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                if (File.Exists(LogDirectoryPath + logFileName) == true)
                    fs = File.Open(LogDirectoryPath + logFileName, FileMode.Append, FileAccess.Write);
                else
                    fs = File.Create(LogDirectoryPath + logFileName);

                using (StreamWriter sw = new StreamWriter(fs)) {
                    sw.Write(sbMessage.ToString() + Environment.NewLine);
                }
                fs.Close();

                return true;
            }
            catch {
                return false;
            }
        }

        #endregion
    }

}

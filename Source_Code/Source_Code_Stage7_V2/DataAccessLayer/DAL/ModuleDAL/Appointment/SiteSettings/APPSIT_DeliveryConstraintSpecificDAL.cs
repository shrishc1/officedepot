﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using System.Data;
using Utilities;
using System.Data.SqlClient;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings {
    public class APPSIT_DeliveryConstraintSpecificDAL : BaseDAL{

        public List<MASSIT_DeliveryConstraintSpecificBE> GetOpenDoorSlotTimeDAL(MASSIT_DeliveryConstraintSpecificBE oMASSIT_DeliveryConstraintSpecificBE) {
            DataTable dt = new DataTable();
            List<MASSIT_DeliveryConstraintSpecificBE> oMASSIT_DeliveryConstraintSpecificBEList = new List<MASSIT_DeliveryConstraintSpecificBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_DeliveryConstraintSpecificBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SiteID);
                param[index++] = new SqlParameter("@Weekday", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.Weekday);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_DeliveryConstraintSpecific", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows) {
                    MASSIT_DeliveryConstraintSpecificBE oNewMASSIT_DeliveryConstraintSpecificBE = new MASSIT_DeliveryConstraintSpecificBE();
                    oNewMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint = new MASSIT_DeliveryConstraintBE();
                    oNewMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartTime = Convert.ToInt32(dr["StartTime"]);

                    oMASSIT_DeliveryConstraintSpecificBEList.Add(oNewMASSIT_DeliveryConstraintSpecificBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_DeliveryConstraintSpecificBEList;
        }

        public List<MASSIT_DeliveryConstraintSpecificBE> GetDeliveryConstraintDAL(MASSIT_DeliveryConstraintSpecificBE oMASSIT_DeliveryConstraintSpecificBE) {
            DataTable dt = new DataTable();
            List<MASSIT_DeliveryConstraintSpecificBE> oMASSIT_DeliveryConstraintSpecificBEList = new List<MASSIT_DeliveryConstraintSpecificBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_DeliveryConstraintSpecificBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SiteID);
                param[index++] = new SqlParameter("@Weekday", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.Weekday);
                param[index++] = new SqlParameter("@StartTime", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartTime);
                param[index++] = new SqlParameter("@WeekStartDate", oMASSIT_DeliveryConstraintSpecificBE.WeekStartDate);
                param[index++] = new SqlParameter("@StartWeekday", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartWeekday);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_DeliveryConstraintSpecific", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows) {
                    MASSIT_DeliveryConstraintSpecificBE oNewMASSIT_DeliveryConstraintSpecificBE = new MASSIT_DeliveryConstraintSpecificBE();

                    oNewMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraintSpecificID = dr["DeliveryConstraintSpecificID"] != DBNull.Value ? Convert.ToInt32(dr["DeliveryConstraintSpecificID"]) : (int?)null;
                    //oNewMASSIT_DeliveryConstraintSpecificBE.WeekStartDate = dr["WeekStartDate"] != DBNull.Value ? Convert.ToDateTime(dr["WeekStartDate"]) : (DateTime?)null;
                    oNewMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint = new MASSIT_DeliveryConstraintBE();
                    oNewMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.DeliveryConstraintID = dr["DeliveryConstraintID"] != DBNull.Value ? Convert.ToInt32(dr["DeliveryConstraintID"]) : (int?)null;
                    oNewMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartTime = Convert.ToInt32(dr["StartTime"]);
                    oNewMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.RestrictDelivery = dr["RestrictDelivery"] != DBNull.Value ? Convert.ToInt32(dr["RestrictDelivery"]) : 0;
                    oNewMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.MaximumLine = dr["MaximumLine"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLine"]) : 0;
                    oNewMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.MaximumLift = dr["MaximumLift"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLift"]) : 0;
                    oNewMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SingleDeliveryPalletRestriction = dr["SingleDeliveryPalletRestriction"] != DBNull.Value ? Convert.ToInt32(dr["SingleDeliveryPalletRestriction"]) : 0;
                    oNewMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SingleDeliveryLineRestriction = dr["SingleDeliveryLineRestriction"] != DBNull.Value ? Convert.ToInt32(dr["SingleDeliveryLineRestriction"]) : 0;
                    oNewMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.Weekday = dr["Weekday"] != DBNull.Value ? dr["Weekday"].ToString() : "";
                    oNewMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartWeekday = dr["StartWeekday"] != DBNull.Value ? dr["StartWeekday"].ToString() : "";

                    oMASSIT_DeliveryConstraintSpecificBEList.Add(oNewMASSIT_DeliveryConstraintSpecificBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_DeliveryConstraintSpecificBEList;
        }

        public int? addEdiDeliveryConstraintSpecificDAL(MASSIT_DeliveryConstraintSpecificBE oMASSIT_DeliveryConstraintSpecificBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oMASSIT_DeliveryConstraintSpecificBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SiteID);
                param[index++] = new SqlParameter("@Weekday", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.Weekday.ToLower());
                param[index++] = new SqlParameter("@WeekStartDate", oMASSIT_DeliveryConstraintSpecificBE.WeekStartDate);
                param[index++] = new SqlParameter("@StartTime", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartTime);
                param[index++] = new SqlParameter("@StartWeekday", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.StartWeekday.ToLower());                
                param[index++] = new SqlParameter("@RestrictDelivery", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.RestrictDelivery);
                param[index++] = new SqlParameter("@MaximumLine", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.MaximumLine);
                param[index++] = new SqlParameter("@MaximumLift", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.MaximumLift);
                param[index++] = new SqlParameter("@SingleDeliveryPalletRestriction", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SingleDeliveryPalletRestriction);
                param[index++] = new SqlParameter("@SingleDeliveryLineRestriction", oMASSIT_DeliveryConstraintSpecificBE.DeliveryConstraint.SingleDeliveryLineRestriction);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_DeliveryConstraintSpecific", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

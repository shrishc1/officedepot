﻿using System;
using System.Data.SqlClient;
using System.Data;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using Utilities;
using System.Collections.Generic;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings {
    public class APPSIT_DoorNoSetupDAL : DataAccessLayer.BaseDAL {
        public List<MASSIT_DoorNoSetupBE> GetDoorNoSetupDetailsDAL(MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE) {
            DataTable dt = new DataTable();
            List<MASSIT_DoorNoSetupBE> oMASSIT_DoorNoSetupBEList = new List<MASSIT_DoorNoSetupBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASSIT_DoorNoSetupBE.Action);
                param[index++] = new SqlParameter("@SiteDoorNumberID", oMASSIT_DoorNoSetupBE.SiteDoorNumberID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DoorNoSetupBE.SiteID);
                param[index++] = new SqlParameter("@SiteDoorTypeID", oMASSIT_DoorNoSetupBE.SiteDoorTypeID);
                param[index++] = new SqlParameter("@DoorNumber", oMASSIT_DoorNoSetupBE.DoorNumber);
                param[index++] = new SqlParameter("@IsActive", oMASSIT_DoorNoSetupBE.IsActive);
                param[index++] = new SqlParameter("@UserID", oMASSIT_DoorNoSetupBE.User.UserID);
                param[index++] = new SqlParameter("@Reference", oMASSIT_DoorNoSetupBE.Reference);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_DoorNumber", param);

                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows) {
                    MASSIT_DoorNoSetupBE oNewMASSIT_DoorNoSetupBE = new MASSIT_DoorNoSetupBE();
                    oNewMASSIT_DoorNoSetupBE.SiteDoorNumberID = Convert.ToInt32(dr["SiteDoorNumberID"]);
                    oNewMASSIT_DoorNoSetupBE.DoorNumber = Convert.ToString(dr["DoorNumber"]);
                    oNewMASSIT_DoorNoSetupBE.Reference = Convert.ToString(dr["Reference"]);
                    oNewMASSIT_DoorNoSetupBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMASSIT_DoorNoSetupBE.SiteDoorTypeID = Convert.ToInt32(dr["SiteDoorTypeID"]);
                    oNewMASSIT_DoorNoSetupBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewMASSIT_DoorNoSetupBE.Site.SiteName = dr["SiteDescription"].ToString();
                    oNewMASSIT_DoorNoSetupBE.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
                    oNewMASSIT_DoorNoSetupBE.DoorType.DoorType = dr["DoorType"].ToString();
                    oMASSIT_DoorNoSetupBEList.Add(oNewMASSIT_DoorNoSetupBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_DoorNoSetupBEList;
        }

        public int? addEditDoorNoSetupDetailsDAL(MASSIT_DoorNoSetupBE oMASSIT_DoorNoSetupBE) {

            int? intResult = 0;
            List<MASSIT_DoorNoSetupBE> oMASSIT_DoorNoSetupBEList = new List<MASSIT_DoorNoSetupBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oMASSIT_DoorNoSetupBE.Action);
                param[index++] = new SqlParameter("@SiteDoorNumberID", oMASSIT_DoorNoSetupBE.SiteDoorNumberID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_DoorNoSetupBE.SiteID);
                param[index++] = new SqlParameter("@SiteDoorTypeID", oMASSIT_DoorNoSetupBE.SiteDoorTypeID);
                param[index++] = new SqlParameter("@DoorNumber", oMASSIT_DoorNoSetupBE.DoorNumber);
                param[index++] = new SqlParameter("@IsActive", oMASSIT_DoorNoSetupBE.IsActive);
                param[index++] = new SqlParameter("@Reference", oMASSIT_DoorNoSetupBE.Reference);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_DoorNumber", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;            
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }

            return intResult;

        }
       
      
    }
}

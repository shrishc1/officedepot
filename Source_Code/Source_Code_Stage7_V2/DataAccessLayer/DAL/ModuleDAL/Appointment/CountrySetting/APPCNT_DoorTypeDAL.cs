﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using Utilities;


namespace DataAccessLayer.ModuleDAL.Appointment.CountrySetting {
    public class APPCNT_DoorTypeDAL : DataAccessLayer.BaseDAL {
        public List<MASCNT_DoorTypeBE> GetDoorTypeDetailsDAL(MASCNT_DoorTypeBE oMASCNT_DoorTypeBE) {
            DataTable dt = new DataTable();
            List<MASCNT_DoorTypeBE> oMASCNT_DoorTypeBEList = new List<MASCNT_DoorTypeBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oMASCNT_DoorTypeBE.Action);
                param[index++] = new SqlParameter("@DoorTypeID", oMASCNT_DoorTypeBE.DoorTypeID);
                param[index++] = new SqlParameter("@DoorType", oMASCNT_DoorTypeBE.DoorType);
                param[index++] = new SqlParameter("@CountryID", oMASCNT_DoorTypeBE.CountryID);
                param[index++] = new SqlParameter("@UserID", oMASCNT_DoorTypeBE.User.UserID);
                
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASCNT_DoorType", param);

                dt = ds.Tables[0];
                
                foreach (DataRow dr in dt.Rows) {
                    MASCNT_DoorTypeBE oNewMASCNT_DoorTypeBE = new MASCNT_DoorTypeBE();
                    oNewMASCNT_DoorTypeBE.DoorTypeID = Convert.ToInt32(dr["DoorTypeID"]);
                    oNewMASCNT_DoorTypeBE.CountryID = Convert.ToInt32(dr["CountryID"]);

                    oNewMASCNT_DoorTypeBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    oNewMASCNT_DoorTypeBE.Country.CountryName = dr["CountryName"].ToString();

                    oNewMASCNT_DoorTypeBE.DoorType = dr["DoorType"].ToString();

                    oMASCNT_DoorTypeBEList.Add(oNewMASCNT_DoorTypeBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASCNT_DoorTypeBEList;
        }
        public List<MASCNT_DoorTypeBE> GetDoorTypeDAL(MASCNT_DoorTypeBE oMASCNT_DoorTypeBE)
        {
            DataTable dt = new DataTable();
            List<MASCNT_DoorTypeBE> oMASCNT_DoorTypeBEList = new List<MASCNT_DoorTypeBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oMASCNT_DoorTypeBE.Action);
                param[index++] = new SqlParameter("@UserID", oMASCNT_DoorTypeBE.User.UserID);
                param[index++] = new SqlParameter("@SiteID", oMASCNT_DoorTypeBE.Site.SiteID);
                param[index++] = new SqlParameter("@VehicleTypeID", oMASCNT_DoorTypeBE.Vehicle.VehicleTypeID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASCNT_DoorType", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASCNT_DoorTypeBE oNewMASCNT_DoorTypeBE = new MASCNT_DoorTypeBE();
                    oNewMASCNT_DoorTypeBE.DoorTypeID = Convert.ToInt32(dr["DoorTypeID"]);
                    oNewMASCNT_DoorTypeBE.CountryID = Convert.ToInt32(dr["CountryID"]);

                    oNewMASCNT_DoorTypeBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    oNewMASCNT_DoorTypeBE.Country.CountryName = dr["CountryName"].ToString();

                    oNewMASCNT_DoorTypeBE.DoorType = dr["DoorType"].ToString();

                    oMASCNT_DoorTypeBEList.Add(oNewMASCNT_DoorTypeBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASCNT_DoorTypeBEList;
        }

        public int? addEditDoorTypeDetailsDAL(MASCNT_DoorTypeBE oMASCNT_DoorTypeBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASCNT_DoorTypeBE.Action);
                param[index++] = new SqlParameter("@DoorTypeID", oMASCNT_DoorTypeBE.DoorTypeID);
                param[index++] = new SqlParameter("@CountryID", oMASCNT_DoorTypeBE.CountryID);
                param[index++] = new SqlParameter("@DoorType", oMASCNT_DoorTypeBE.DoorType);
                param[index++] = new SqlParameter("@IsActive", oMASCNT_DoorTypeBE.IsActive);


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASCNT_DoorType", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

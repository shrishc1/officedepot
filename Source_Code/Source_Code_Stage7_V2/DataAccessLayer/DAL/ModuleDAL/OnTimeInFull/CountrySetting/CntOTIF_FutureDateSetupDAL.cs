﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;

using Utilities;
using BusinessEntities.ModuleBE.OnTimeInFull.CountrySetting;

namespace DataAccessLayer.ModuleDAL.OnTimeInFull.CountrySetting {
    public class CntOTIF_FutureDateSetupDAL : BaseDAL {
        public List<CntOTIF_FutureDateSetupBE> GetCntOTIF_FutureDateSetupDetailsDAL(CntOTIF_FutureDateSetupBE oCntOTIF_FutureDateSetupBE) {
            DataTable dt = new DataTable();
            List<CntOTIF_FutureDateSetupBE> oCntOTIF_FutureDateSetupBEList = new List<CntOTIF_FutureDateSetupBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oCntOTIF_FutureDateSetupBE.Action);
                param[index++] = new SqlParameter("@CountryID", oCntOTIF_FutureDateSetupBE.CountryID);
                param[index++] = new SqlParameter("@UserID", oCntOTIF_FutureDateSetupBE.User.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spCntOTIF_FutureDateSetup", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    CntOTIF_FutureDateSetupBE oNewCntOTIF_FutureDateSetupBE = new CntOTIF_FutureDateSetupBE();

                    oNewCntOTIF_FutureDateSetupBE.CountryID = Convert.ToInt32(dr["CountryID"]);
                    oNewCntOTIF_FutureDateSetupBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    oNewCntOTIF_FutureDateSetupBE.Country.CountryName = dr["CountryName"].ToString();
                    oNewCntOTIF_FutureDateSetupBE.FutureDatePODay = dr["FutureDatePODay"]!= DBNull.Value ? Convert.ToInt32(dr["FutureDatePODay"]): (int?) null;
                    oNewCntOTIF_FutureDateSetupBE.FutureDatePOMonth = dr["FutureDatePOMonth"]!= DBNull.Value ? Convert.ToInt32(dr["FutureDatePOMonth"]): (int?) null;
                    if (oNewCntOTIF_FutureDateSetupBE.FutureDatePODay != null && oNewCntOTIF_FutureDateSetupBE.FutureDatePOMonth != null)
                        oNewCntOTIF_FutureDateSetupBE.FutureDate = oNewCntOTIF_FutureDateSetupBE.FutureDatePODay.ToString() + " " + (new DateTime(1900, Convert.ToInt32(oNewCntOTIF_FutureDateSetupBE.FutureDatePOMonth), 1)).ToString("MMM");
                    else
                        oNewCntOTIF_FutureDateSetupBE.FutureDate = "-";
                    oCntOTIF_FutureDateSetupBEList.Add(oNewCntOTIF_FutureDateSetupBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oCntOTIF_FutureDateSetupBEList;
        }
        public int? addEditCntOTIF_FutureDateSetupDetailsDAL(CntOTIF_FutureDateSetupBE oCntOTIF_FutureDateSetupBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oCntOTIF_FutureDateSetupBE.Action);
                param[index++] = new SqlParameter("@CountryID", oCntOTIF_FutureDateSetupBE.CountryID);
                param[index++] = new SqlParameter("@FutureDatePODay", oCntOTIF_FutureDateSetupBE.FutureDatePODay);
                param[index++] = new SqlParameter("@FutureDatePOMonth", oCntOTIF_FutureDateSetupBE.FutureDatePOMonth);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spCntOTIF_FutureDateSetup", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }    
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.StockOverview.Report;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.StockOverview.Report {
    public class StockOverviewReportDAL {
        StockOverviewReportBE oStockOverviewReportBE = new StockOverviewReportBE();
        public DataSet getStockOverviewReportDAL(StockOverviewReportBE oStockOverviewReportBE) {
            DataSet Result = null;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[15];
                param[index++] = new SqlParameter("@Action", oStockOverviewReportBE.Action);
                param[index++] = new SqlParameter("@SELECTedCountryIDs", oStockOverviewReportBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SELECTedSiteIDs", oStockOverviewReportBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@VendorID", oStockOverviewReportBE.SelectedVendorIDs);
                if (!string.IsNullOrEmpty(oStockOverviewReportBE.OfficeDepotSKU))
                    param[index++] = new SqlParameter("@OfficeDepotSKU", oStockOverviewReportBE.OfficeDepotSKU);
                else
                    param[index++] = new SqlParameter("@OfficeDepotSKU", DBNull.Value);
                if (!string.IsNullOrEmpty(oStockOverviewReportBE.OfficeDepotSKU))
                    param[index++] = new SqlParameter("@SELECTedItemClassification", oStockOverviewReportBE.ItemClassification);
                else
                    param[index++] = new SqlParameter("@SELECTedItemClassification", DBNull.Value);
                param[index++] = new SqlParameter("@EndDate", oStockOverviewReportBE.DateTo);
                param[index++] = new SqlParameter("@StartDate", oStockOverviewReportBE.DateFrom);
                param[index++] = new SqlParameter("@PendingSKUs", oStockOverviewReportBE.PendingSKUs);
                param[index++] = new SqlParameter("@DiscSKUs", oStockOverviewReportBE.DiscSKUs);
                param[index++] = new SqlParameter("@ActiveSKUs", oStockOverviewReportBE.ActiveSKUs);
                param[index++] = new SqlParameter("@SKUMarkedAs", oStockOverviewReportBE.SKUMarkedAs);
                param[index++] = new SqlParameter("@UnderReviewSKUs", oStockOverviewReportBE.UnderReviewSKUs);
                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spSTK_StockOverview", param);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public static DataSet getStockOverviewReportDAL2(string sFromDate, string sToDate, int StartRow, int PageSize) {
            DataSet Result = null;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[12];
                param[index++] = new SqlParameter("@Action", "VendorOverviewReportSummary");
                param[index++] = new SqlParameter("@SELECTedCountryIDs", DBNull.Value);
                param[index++] = new SqlParameter("@SELECTedSiteIDs", DBNull.Value);
                param[index++] = new SqlParameter("@VendorID", DBNull.Value);
                param[index++] = new SqlParameter("@OfficeDepotSKU", DBNull.Value);
                param[index++] = new SqlParameter("@SELECTedItemClassification", DBNull.Value);
                param[index++] = new SqlParameter("@EndDate", sToDate);
                param[index++] = new SqlParameter("@StartDate", sFromDate);
                param[index++] = new SqlParameter("@SKUMarkedAs", DBNull.Value);
                param[index++] = new SqlParameter("@StartRow", StartRow);
                param[index++] = new SqlParameter("@PageSize", PageSize);


                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "[spSTK_StockOverviewPaging]", param);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }


        public static int getStockOverviewCount(string sFromDate, string sToDate) {
            object oResult=0;
          
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", "VendorOverviewReportSummaryCount");
                param[index++] = new SqlParameter("@SELECTedCountryIDs", DBNull.Value);
                param[index++] = new SqlParameter("@SELECTedSiteIDs", DBNull.Value);
                param[index++] = new SqlParameter("@VendorID", DBNull.Value);
                param[index++] = new SqlParameter("@OfficeDepotSKU", DBNull.Value);
                param[index++] = new SqlParameter("@SELECTedItemClassification", DBNull.Value);
                param[index++] = new SqlParameter("@EndDate", sToDate);
                param[index++] = new SqlParameter("@StartDate", sFromDate);

                param[index++] = new SqlParameter("@SKUMarkedAs", DBNull.Value);


                oResult = SqlHelper.ExecuteScalar(DBConnection.Connection, CommandType.StoredProcedure, "[spSTK_StockOverviewPaging]", param);
                if (oResult == null) {
                    return 0;
                }

                else
                    return Convert.ToInt32(oResult);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Convert.ToInt32(oResult);
        }

        public DataSet getItemSummaryReportDAL(StockOverviewReportBE oStockOverviewReportBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[15];
                param[index++] = new SqlParameter("@Action", oStockOverviewReportBE.Action);                
                param[index++] = new SqlParameter("@SelectedSiteIDs", oStockOverviewReportBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@VendorID", oStockOverviewReportBE.SelectedVendorIDs);
                if (!string.IsNullOrEmpty(oStockOverviewReportBE.OfficeDepotSKU))
                    param[index++] = new SqlParameter("@OfficeDepotSKU", oStockOverviewReportBE.OfficeDepotSKU);
                else
                    param[index++] = new SqlParameter("@OfficeDepotSKU", DBNull.Value);
                if (!string.IsNullOrEmpty(oStockOverviewReportBE.ODCatCode))
                    param[index++] = new SqlParameter("@ODCatCode", oStockOverviewReportBE.ODCatCode);
                else
                    param[index++] = new SqlParameter("@ODCatCode", DBNull.Value);
                param[index++] = new SqlParameter("@StockPlannerIDs", oStockOverviewReportBE.StockPlannerIDs);
                param[index++] = new SqlParameter("@EndDate", oStockOverviewReportBE.DateTo);
                param[index++] = new SqlParameter("@StartDate", oStockOverviewReportBE.DateFrom);
               
                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spSTK_StockOverview", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
    }
}

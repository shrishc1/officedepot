﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using BusinessEntities.ModuleBE.AdminFunctions;
using Utilities;

namespace DataAccessLayer.ModuleDAL.AdminFunctions
{
    public class CMN_CommunicationErrorLogDAL
    {
        public List<CMN_CommunicationErrorLogBE> GetCommunicationErrorLog(CMN_CommunicationErrorLogBE oCMN_CommunicationErrorLogBE)
        {

            List<CMN_CommunicationErrorLogBE> lstCMN_CommunicationErrorLogBE = new List<CMN_CommunicationErrorLogBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oCMN_CommunicationErrorLogBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spCMN_CommunicationErrorLog", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    CMN_CommunicationErrorLogBE oNewCMN_CommunicationErrorLogBE = new CMN_CommunicationErrorLogBE();

                    oNewCMN_CommunicationErrorLogBE.Body = dr["Body"].ToString();
                    oNewCMN_CommunicationErrorLogBE.DiscrepancyLogID = dr["DiscrepancyLogID"].ToString();
                    oNewCMN_CommunicationErrorLogBE.ErrorDescription = dr["ErrorDescription"].ToString();
                    oNewCMN_CommunicationErrorLogBE.sentDate = Convert.ToDateTime(dr["sentDate"]);
                    oNewCMN_CommunicationErrorLogBE.SentTo = dr["SentTo"].ToString();
                    oNewCMN_CommunicationErrorLogBE.Subject = dr["Subject"].ToString();
                    oNewCMN_CommunicationErrorLogBE.CommunicationErrorID = Convert.ToInt32(dr["CommunicationErrorID"]);
                    oNewCMN_CommunicationErrorLogBE.VDRNo = dr["VDRNo"].ToString();
                    lstCMN_CommunicationErrorLogBE.Add(oNewCMN_CommunicationErrorLogBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstCMN_CommunicationErrorLogBE;
        }

        public void UpdateStatus(CMN_CommunicationErrorLogBE oCMN_CommunicationErrorLogBE)
        {
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oCMN_CommunicationErrorLogBE.Action);
                param[index++] = new SqlParameter("@CommunicationErrorID", oCMN_CommunicationErrorLogBE.CommunicationErrorID);
                param[index++] = new SqlParameter("@Status", oCMN_CommunicationErrorLogBE.Status);

                int? i = SqlHelper.ExecuteNonQuery(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spCMN_CommunicationErrorLog", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.Upload;
using DataAccessLayer.ModuleDAL.Upload;
using Utilities;
using System.Data;

namespace BusinessLogicLayer.ModuleBAL.Upload
{
    public class UP_DataImportSchedulerBAL
    {
        public List<UP_DataImportSchedulerBE> GetDataImportDetailBAL(UP_DataImportSchedulerBE oUp_PurchaseOrderDetailBE)
        {
            List<UP_DataImportSchedulerBE> oUP_DataImportSchedulerBE = new List<UP_DataImportSchedulerBE>();

            try
            {
                UP_DataImportSchedulerDAL oUP_DataImportSchedulerDAL = new UP_DataImportSchedulerDAL();

                oUP_DataImportSchedulerBE = oUP_DataImportSchedulerDAL.GetDataImportDetailDAL(oUp_PurchaseOrderDetailBE);
                oUP_DataImportSchedulerDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_DataImportSchedulerBE;
        }

        public int? GetDataImportDetailErrorCountBAL(UP_DataImportSchedulerBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                UP_DataImportSchedulerDAL oUP_DataImportSchedulerDAL = new UP_DataImportSchedulerDAL();
                intResult = oUP_DataImportSchedulerDAL.GetDataImportDetailErrorCountDAL(oUp_PurchaseOrderDetailBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<UP_DataImportSchedulerBE> GetGetFileProcessedDataBAL(UP_DataImportSchedulerBE oUp_PurchaseOrderDetailBE)
        {
            List<UP_DataImportSchedulerBE> lstUP_DataImportSchedulerBE = new List<UP_DataImportSchedulerBE>();

            try
            {
                UP_DataImportSchedulerDAL oUP_DataImportSchedulerDAL = new UP_DataImportSchedulerDAL();

                lstUP_DataImportSchedulerBE = oUP_DataImportSchedulerDAL.GetGetFileProcessedDataDAL(oUp_PurchaseOrderDetailBE);
                oUP_DataImportSchedulerDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstUP_DataImportSchedulerBE;
        }

        public List<UP_DataImportSchedulerBE> GetErrorMessage(UP_DataImportSchedulerBE oUP_DataImportSchedulerBE)
        {
            List<UP_DataImportSchedulerBE> lstUP_DataImportSchedulerBE = new List<UP_DataImportSchedulerBE>();

            try
            {
                UP_DataImportSchedulerDAL oUP_DataImportSchedulerDAL = new UP_DataImportSchedulerDAL();

                lstUP_DataImportSchedulerBE = oUP_DataImportSchedulerDAL.GetErrorMessage(oUP_DataImportSchedulerBE);
                oUP_DataImportSchedulerDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstUP_DataImportSchedulerBE;
        }

        public DataSet GetErrorDetailBAL(UP_DataImportSchedulerBE oUP_DataImportSchedulerBE)
        {
            DataSet ds = new DataSet();

            try
            {
                UP_DataImportSchedulerDAL oUP_DataImportSchedulerDAL = new UP_DataImportSchedulerDAL();

                ds = oUP_DataImportSchedulerDAL.GetErrorDetailDAL(oUP_DataImportSchedulerBE);
                oUP_DataImportSchedulerDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public DataSet CheckIsDataImportRunningBAL()
        {
            DataSet ds = new DataSet();
            try
            {
                UP_DataImportSchedulerDAL oUP_DataImportSchedulerDAL = new UP_DataImportSchedulerDAL();
                ds = oUP_DataImportSchedulerDAL.CheckIsDataImportRunningDAL();
                oUP_DataImportSchedulerDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public string CheckUpdateImportStatusBAL(UP_DataImportSchedulerBE oUP_DataImportSchedulerBE)
        {
            string strResult = string.Empty;
            try
            {
                UP_DataImportSchedulerDAL oUP_DataImportSchedulerDAL = new UP_DataImportSchedulerDAL();
                strResult = oUP_DataImportSchedulerDAL.CheckUpdateImportStatusDAL(oUP_DataImportSchedulerBE);
                oUP_DataImportSchedulerDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }

        public string DeleteImportStatusBAL(UP_DataImportSchedulerBE oUP_DataImportSchedulerBE)
        {
            string strResult = string.Empty;
            try
            {
                UP_DataImportSchedulerDAL oUP_DataImportSchedulerDAL = new UP_DataImportSchedulerDAL();
                strResult = oUP_DataImportSchedulerDAL.DeleteImportStatusDAL(oUP_DataImportSchedulerBE);
                oUP_DataImportSchedulerDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }

        public List<Up_PurchaseOrderDetailBE> GetImportNewPOLetterMainBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstUp_PurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                var oUP_DataImportSchedulerDAL = new UP_DataImportSchedulerDAL();
                lstUp_PurchaseOrderDetailBE = oUP_DataImportSchedulerDAL.GetImportNewPOLetterMainDAL(oUp_PurchaseOrderDetailBE);
                oUP_DataImportSchedulerDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstUp_PurchaseOrderDetailBE;
        }

        public List<Up_PurchaseOrderDetailBE> GetImportInsertNewSentPOEmailDataBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            var lstUp_PurchaseOrderDetailBE = new List<Up_PurchaseOrderDetailBE>();
            try
            {
                var oUP_DataImportSchedulerDAL = new UP_DataImportSchedulerDAL();
                lstUp_PurchaseOrderDetailBE = oUP_DataImportSchedulerDAL.GetImportInsertNewSentPOEmailDataDAL(oUp_PurchaseOrderDetailBE);
                oUP_DataImportSchedulerDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstUp_PurchaseOrderDetailBE;
        }

        public int? SaveImportNewSentPOEmailDataBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                var oUP_DataImportSchedulerDAL = new UP_DataImportSchedulerDAL();
                intResult = oUP_DataImportSchedulerDAL.SaveImportNewSentPOEmailDataDAL(oUp_PurchaseOrderDetailBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? ImportSetIsEmailSentStatusBAL(Up_PurchaseOrderDetailBE oUp_PurchaseOrderDetailBE)
        {
            int? intResult = 0;
            try
            {
                var oUP_DataImportSchedulerDAL = new UP_DataImportSchedulerDAL();
                intResult = oUP_DataImportSchedulerDAL.ImportSetIsEmailSentStatusDAL(oUp_PurchaseOrderDetailBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataTable dtGetCancelSlotInformationBAL(UP_DataImportSchedulerBE oUP_DataImportSchedulerBE)
        {
            DataTable dt=null;
            try
            {
                var oUP_DataImportSchedulerDAL = new UP_DataImportSchedulerDAL();
                dt = oUP_DataImportSchedulerDAL.dtGetCancelSlotInformationDAL(oUP_DataImportSchedulerBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }
    }
}

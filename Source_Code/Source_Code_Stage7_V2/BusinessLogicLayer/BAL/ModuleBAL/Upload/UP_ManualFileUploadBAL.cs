﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.Upload;
using DataAccessLayer.ModuleDAL.Upload;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Upload {
  public  class UP_ManualFileUploadBAL {

      public int? addEditManualUploadBAL(UP_ManualFileUploadBE oUP_ManualFileUploadBE) {
          int? intResult = 0;
          try {
              UP_ManualFileUploadDAL oUP_ManualFileUploadDAL = new UP_ManualFileUploadDAL();
              intResult = oUP_ManualFileUploadDAL.addEditManualUploadDAL(oUP_ManualFileUploadBE);
              oUP_ManualFileUploadDAL = null;
          }
          catch (Exception ex) {
              LogUtility.SaveErrorLogEntry(ex);
          }
          finally { }
          return intResult;
      }

      public List<UP_ManualFileUploadBE> GetManualUploadBAL(UP_ManualFileUploadBE oUP_ManualFileUploadBE) {
          List<UP_ManualFileUploadBE> oUP_ManualFileUploadBEList = new List<UP_ManualFileUploadBE>();

          try {
              UP_ManualFileUploadDAL oUP_ManualFileUploadDAL = new UP_ManualFileUploadDAL();

              oUP_ManualFileUploadBEList = oUP_ManualFileUploadDAL.GetManualUploadDAL(oUP_ManualFileUploadBE);
              oUP_ManualFileUploadDAL = null;
          }
          catch (Exception ex) {
              LogUtility.SaveErrorLogEntry(ex);
          }
          finally { }
          return oUP_ManualFileUploadBEList;
      }

    }
}

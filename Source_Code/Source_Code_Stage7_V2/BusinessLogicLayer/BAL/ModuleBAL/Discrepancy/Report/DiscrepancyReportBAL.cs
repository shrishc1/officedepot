﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using DataAccessLayer.ModuleDAL.Discrepancy.Report;
using Utilities;
using BusinessEntities.ModuleBE.Discrepancy;

namespace BusinessLogicLayer.ModuleBAL.Discrepancy.Report
{
    public class DiscrepancyReportBAL
    {
        DiscrepancyReportBE oDiscrepancyReportBE = new DiscrepancyReportBE();

        public DataSet getDiscrepancyReportBAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.getDiscrepancyReportDAL(oDiscrepancyReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet getAssignedVendorsReportBAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.getAssignedVendorsReportDAL(oDiscrepancyReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetShuttleDiscrepancyReportBAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.GetShuttleDiscrepancyReportDAL(oDiscrepancyReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }


        public DataSet getDiscrepancySchedulerReportBAL()
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.getDiscrepancySchedulerReportDAL();
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet getDiscrepancySchedulerNotificationBAL(DiscrepancyNotification discrepancyNotification)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.getDiscrepancySchedulerNotificationDAL(discrepancyNotification);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DiscrepancyNotification getDiscrepancyNotificationForSendingMailBAL(DiscrepancyNotification discrepancy)
        {
            DiscrepancyNotification discrepancyNotification = new DiscrepancyNotification();
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                discrepancyNotification = oDiscrepancyReportDAL.getDiscrepancyNotificationForSendingMailDAL(discrepancy);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return discrepancyNotification;
        }


        public DataSet UpdateDiscrepancySchedulerNotificationBAL(DiscrepancyNotification discrepancyNotification)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.UpdateDiscrepancySchedulerNotificationDAL(discrepancyNotification);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public List<DiscrepancyEmailTracker> GetALLDiscrepancyTrackerEmailBAL(DiscrepancyEmailTracker discrepancyEmailTracker)
        {
            var Result = new List<DiscrepancyEmailTracker>();
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.GetALLDiscrepancyTrackerEmailDAL(discrepancyEmailTracker);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Resources;

using Utilities;
using BusinessEntities.ModuleBE.Discrepancy.CountrySetting;
using DataAccessLayer.ModuleDAL.Discrepancy.CountrySetting;

namespace BusinessLogicLayer.ModuleBAL.Discrepancy.CountrySetting {
    public class DISCNT_AccountPayableContactBAL : BaseBAL {

        public List<DISCnt_AccountPayableContactBE> GetAccountPayableContactDetailsBAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE) {
            List<DISCnt_AccountPayableContactBE> oDISCnt_AccountPayableContactBEList = new List<DISCnt_AccountPayableContactBE>();

            try {
                DISCNT_AccountPayableContactDAL oDISCnt_AccountPayableContactDAL = new DISCNT_AccountPayableContactDAL();

                oDISCnt_AccountPayableContactBEList = oDISCnt_AccountPayableContactDAL.GetAccountPayableContactDetailsDAL(oDISCnt_AccountPayableContactBE);
                oDISCnt_AccountPayableContactDAL = null; 
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oDISCnt_AccountPayableContactBEList;
        }

        public DataTable GetAccountPayableContactDetailsBAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE, string Nothing = "") {
            DataTable dt = new DataTable();

            try {
                DISCNT_AccountPayableContactDAL oDISCnt_AccountPayableContactDAL = new DISCNT_AccountPayableContactDAL();

                dt = oDISCnt_AccountPayableContactDAL.GetAccountPayableContactDetailsDAL(oDISCnt_AccountPayableContactBE,"");
                oDISCnt_AccountPayableContactDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataTable GetAccountPayableVendorDetailsBAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE, string Nothing = "")
        {
            DataTable dt = new DataTable();

            try
            {
                DISCNT_AccountPayableContactDAL oDISCnt_AccountPayableContactDAL = new DISCNT_AccountPayableContactDAL();

                dt = oDISCnt_AccountPayableContactDAL.GetAccountPayableVendorDetailsDAL(oDISCnt_AccountPayableContactBE, "");
                oDISCnt_AccountPayableContactDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public int GetUnAssignedVendorsCountBAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE, string Nothing = "")
        {
            int vendorCount = 0;

            try
            {
                DISCNT_AccountPayableContactDAL oDISCnt_AccountPayableContactDAL = new DISCNT_AccountPayableContactDAL();

                vendorCount = oDISCnt_AccountPayableContactDAL.GetUnAssignedVendorsCountDAL(oDISCnt_AccountPayableContactBE, "");
                oDISCnt_AccountPayableContactDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return vendorCount;
        }

        public DataTable GetUnAssignedVendorsBAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE, string Nothing = "")
        {
            DataTable vendors = null;

            try
            {
                DISCNT_AccountPayableContactDAL oDISCnt_AccountPayableContactDAL = new DISCNT_AccountPayableContactDAL();

                vendors = oDISCnt_AccountPayableContactDAL.GetUnAssignedVendorsDAL(oDISCnt_AccountPayableContactBE, "");
                oDISCnt_AccountPayableContactDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return vendors;
        }

        public int? addEditAccountPayableContactDetailsBAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE) {
            int? intResult = 0;
            try {
                DISCNT_AccountPayableContactDAL oDISCnt_AccountPayableContactDAL = new DISCNT_AccountPayableContactDAL();
                intResult = oDISCnt_AccountPayableContactDAL.addEditAccountPayableContactDetailsDAL(oDISCnt_AccountPayableContactBE);
                oDISCnt_AccountPayableContactDAL = null; 
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DISCnt_AccountPayableContactBE> GetAllAssignedVendorsByUserIDBAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE)
        {
            List<DISCnt_AccountPayableContactBE> oDISCnt_AccountPayableContactBEList = new List<DISCnt_AccountPayableContactBE>();
            try
            {
                DISCNT_AccountPayableContactDAL oDISCnt_AccountPayableContactDAL = new DISCNT_AccountPayableContactDAL();

                oDISCnt_AccountPayableContactBEList = oDISCnt_AccountPayableContactDAL.GetAllAssignedVendorsByUserIDDAL(oDISCnt_AccountPayableContactBE);
                oDISCnt_AccountPayableContactDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oDISCnt_AccountPayableContactBEList;
        }

        public List<DISCnt_AccountPayableContactBE> GetAllUnAssignedVendorsBAL(DISCnt_AccountPayableContactBE oDISCnt_AccountPayableContactBE)
        {
            List<DISCnt_AccountPayableContactBE> oDISCnt_AccountPayableContactBEList = new List<DISCnt_AccountPayableContactBE>();
            try
            {
                DISCNT_AccountPayableContactDAL oDISCnt_AccountPayableContactDAL = new DISCNT_AccountPayableContactDAL();

                oDISCnt_AccountPayableContactBEList = oDISCnt_AccountPayableContactDAL.GetAllUnAssignedVendorsDAL(oDISCnt_AccountPayableContactBE);
                oDISCnt_AccountPayableContactDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oDISCnt_AccountPayableContactBEList;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Data.SqlClient;

using Utilities;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using DataAccessLayer.ModuleDAL.Discrepancy.LogDiscrepancy;

namespace BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy {
    public class DISLog_ImagesBAL : BaseBAL {

        public List<DISLog_ImagesBE> GetDISLogImageDetailsBAL(DISLog_ImagesBE oDISLog_ImagesBE) {
            List<DISLog_ImagesBE> DISLog_ImagesBEList = new List<DISLog_ImagesBE>();

            try {
                DISLog_ImagesDAL oDISLog_ImagesDAL = new DISLog_ImagesDAL();

                DISLog_ImagesBEList = oDISLog_ImagesDAL.GetDISLogImageDetailsDAL(oDISLog_ImagesBE);
                oDISLog_ImagesDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DISLog_ImagesBEList;
        }

        public int? addEditDISLogImageDetailsBAL(DISLog_ImagesBE oDISLog_ImagesBE) {
            int? intResult = 0;
            try {
                DISLog_ImagesDAL oDISLog_ImagesDAL = new DISLog_ImagesDAL();
                intResult = oDISLog_ImagesDAL.addEditDISLogImageDetailsDAL(oDISLog_ImagesBE);
                oDISLog_ImagesDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

    }
}

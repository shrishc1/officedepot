﻿using System;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using DataAccessLayer.ModuleDAL.Discrepancy.LogDiscrepancy;
using Utilities;
using System.Collections.Generic;

namespace BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy
{
    public class DISLog_QueryDiscrepancyBAL : BaseBAL
    {
        public int? addQueryDiscrepancyBAL(DISLog_QueryDiscrepancyBE queryDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                var queryDiscrepancyDAL = new DISLog_QueryDiscrepancyDAL();
                intResult = queryDiscrepancyDAL.addQueryDiscrepancyDAL(queryDiscrepancyBE);
                queryDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? IsAlreadyDisputeExistBAL(DISLog_QueryDiscrepancyBE queryDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                var queryDiscrepancyDAL = new DISLog_QueryDiscrepancyDAL();
                intResult = queryDiscrepancyDAL.IsAlreadyDisputeExistDAL(queryDiscrepancyBE);
                queryDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<DiscrepancyBE> GetDiscrepancyLogDetailsBAL(DiscrepancyBE discrepancyBE)
        {
            var lstDiscrepancyBE = new List<DiscrepancyBE>();
            try
            {
                var queryDiscrepancyDAL = new DISLog_QueryDiscrepancyDAL();
                lstDiscrepancyBE = queryDiscrepancyDAL.GetDiscrepancyLogDetailsDAL(discrepancyBE);
                queryDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDiscrepancyBE;
        }

        public List<DISLog_QueryDiscrepancyBE> GetQueryDiscrepancyBAL(DISLog_QueryDiscrepancyBE queryDiscrepancyBE)
        {
            var lstDiscrepancyBE = new List<DISLog_QueryDiscrepancyBE>();
            try
            {
                var queryDiscrepancyDAL = new DISLog_QueryDiscrepancyDAL();
                lstDiscrepancyBE = queryDiscrepancyDAL.GetQueryDiscrepancyDAL(queryDiscrepancyBE);
                queryDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDiscrepancyBE;
        }

        public DiscrepancyMailBE GetQueryDiscrepancyEmailContentBAL(DiscrepancyMailBE discrepancyMail)
        {
            var discrepancyMailBE = new DiscrepancyMailBE();
            try
            {
                var queryDiscrepancyDAL = new DISLog_QueryDiscrepancyDAL();
                discrepancyMailBE = queryDiscrepancyDAL.GetQueryDiscrepancyEmailContentDAL(discrepancyMail);
                queryDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return discrepancyMailBE;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using Utilities;
using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
    public class MAS_VendorBAL : BusinessLogicLayer.BaseBAL
    {
        public List<MAS_VendorBE> GetVendorBAL(MAS_VendorBE oMAS_VendorBE)
        {
            List<MAS_VendorBE> oMAS_VendorBEList = new List<MAS_VendorBE>();
            try
            {
                MAS_VendorDAL oMAS_VendorDAL = new MAS_VendorDAL();
                oMAS_VendorBEList = oMAS_VendorDAL.GetVendorDAL(oMAS_VendorBE);
                oMAS_VendorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_VendorBEList;
        }

        public MAS_VendorBE UpdateVendorBAL(MAS_VendorBE oMAS_VendorBE)
        {
            try
            {
                MAS_VendorDAL oMAS_VendorDAL = new MAS_VendorDAL();
                oMAS_VendorBE = oMAS_VendorDAL.UpdateVendorBAL(oMAS_VendorBE);
                oMAS_VendorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_VendorBE;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oMAS_VendorBE"></param>
        /// <returns></returns>
        public MAS_VendorBE GetVendorBALByID(MAS_VendorBE oMAS_VendorBE)
        {
            List<MAS_VendorBE> oMAS_VendorBEList = new List<MAS_VendorBE>();
            try
            {
                MAS_VendorDAL oMAS_VendorDAL = new MAS_VendorDAL();
                oMAS_VendorBEList = oMAS_VendorDAL.GetVendorDAL(oMAS_VendorBE);
                oMAS_VendorBE = oMAS_VendorBEList[0];
                oMAS_VendorDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_VendorBE;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using Utilities;
using BusinessEntities.ModuleBE.AdminFunctions;
using DataAccessLayer.ModuleDAL.AdminFunctions;

namespace BusinessLogicLayer.ModuleBAL.AdminFunctions
{
    public class MAS_MaintainVendorPointsContactBAL
    {
        public int? AddVendorPointsContactBAL(MAS_MaintainVendorPointsContactBE vendorPointsContactBE)
        {
            int? intResult = 0;
            try
            {
                var vendorPointsContactDAL = new MAS_MaintainVendorPointsContactDAL();
                intResult = vendorPointsContactDAL.AddVendorPointsContactDAL(vendorPointsContactBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateVendorPointsContactBAL(MAS_MaintainVendorPointsContactBE vendorPointsContactBE)
        {
            int? intResult = 0;
            try
            {
                var vendorPointsContactDAL = new MAS_MaintainVendorPointsContactDAL();
                intResult = vendorPointsContactDAL.UpdateVendorPointsContactDAL(vendorPointsContactBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteVendorPointsContactBAL(MAS_MaintainVendorPointsContactBE vendorPointsContactBE)
        {
            int? intResult = 0;
            try
            {
                var vendorPointsContactDAL = new MAS_MaintainVendorPointsContactDAL();
                intResult = vendorPointsContactDAL.DeleteVendorPointsContactDAL(vendorPointsContactBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MAS_MaintainVendorPointsContactBE> GetVendorPointsContactsBAL(MAS_MaintainVendorPointsContactBE vendorPointsContactBE)
        {
            var lstVendorPointsContacts = new List<MAS_MaintainVendorPointsContactBE>();
            try
            {
                var vendorPointsContactDAL = new MAS_MaintainVendorPointsContactDAL();
                lstVendorPointsContacts = vendorPointsContactDAL.GetVendorPointsContactsDAL(vendorPointsContactBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstVendorPointsContacts;
        }
    }
}

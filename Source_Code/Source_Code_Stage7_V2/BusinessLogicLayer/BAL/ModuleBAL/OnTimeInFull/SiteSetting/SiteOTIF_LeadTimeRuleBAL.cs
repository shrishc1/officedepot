﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Resources;

using Utilities;

using BusinessEntities.ModuleBE.OnTimeInFull.SiteSetting;
using DataAccessLayer.ModuleDAL.OnTimeInFull.SiteSetting;

namespace BusinessLogicLayer.ModuleBAL.OnTimeInFull.SiteSetting {
    public class SiteOTIF_LeadTimeRuleBAL : BaseBAL{

        public List<SiteOTIF_LeadTimeRuleBE> GetSiteOTIFLeadTimeDetailsBAL(SiteOTIF_LeadTimeRuleBE oSiteOTIF_LeadTimeRuleBE) {
            List<SiteOTIF_LeadTimeRuleBE> oSiteOTIF_LeadTimeRuleBEList = new List<SiteOTIF_LeadTimeRuleBE>();

            try {
                SiteOTIF_LeadTimeRuleDAL oSiteOTIF_LeadTimeRuleDAL = new SiteOTIF_LeadTimeRuleDAL();

                oSiteOTIF_LeadTimeRuleBEList = oSiteOTIF_LeadTimeRuleDAL.GetSiteOTIFLeadTimeDetailsDAL(oSiteOTIF_LeadTimeRuleBE);
                oSiteOTIF_LeadTimeRuleDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSiteOTIF_LeadTimeRuleBEList;
        }

        public int? addEditSiteOTIFLeadTimeDetailsBAL(SiteOTIF_LeadTimeRuleBE oSiteOTIF_LeadTimeRuleBE) {
            int? intResult = 0;
            try {
                SiteOTIF_LeadTimeRuleDAL oSiteOTIF_LeadTimeRuleDAL = new SiteOTIF_LeadTimeRuleDAL();
                intResult = oSiteOTIF_LeadTimeRuleDAL.addEditSiteOTIFLeadTimeDetailsDAL(oSiteOTIF_LeadTimeRuleBE);
                oSiteOTIF_LeadTimeRuleDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    
    }
}

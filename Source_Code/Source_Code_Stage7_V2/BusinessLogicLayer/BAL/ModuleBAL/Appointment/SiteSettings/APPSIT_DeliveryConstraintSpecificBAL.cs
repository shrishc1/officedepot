﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Utilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings {
    public class APPSIT_DeliveryConstraintSpecificBAL : BaseBAL{

        public List<MASSIT_DeliveryConstraintSpecificBE> GetOpenDoorSlotTimeBAL(MASSIT_DeliveryConstraintSpecificBE oMASSIT_DeliveryConstraintSpecificBE) {
            List<MASSIT_DeliveryConstraintSpecificBE> oMASSIT_DeliveryConstraintSpecificBEList = new List<MASSIT_DeliveryConstraintSpecificBE>();

            try {
                APPSIT_DeliveryConstraintSpecificDAL oAPPSIT_DeliveryConstraintSpecificDAL = new APPSIT_DeliveryConstraintSpecificDAL();

                oMASSIT_DeliveryConstraintSpecificBEList = oAPPSIT_DeliveryConstraintSpecificDAL.GetOpenDoorSlotTimeDAL(oMASSIT_DeliveryConstraintSpecificBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_DeliveryConstraintSpecificBEList;
        }

        public List<MASSIT_DeliveryConstraintSpecificBE> GetDeliveryConstraintSpecificBAL(MASSIT_DeliveryConstraintSpecificBE oMASSIT_DeliveryConstraintSpecificBE) {
            List<MASSIT_DeliveryConstraintSpecificBE> oMASSIT_DeliveryConstraintSpecificBEList = new List<MASSIT_DeliveryConstraintSpecificBE>();

            try {
                APPSIT_DeliveryConstraintSpecificDAL oAPPSIT_DeliveryConstraintSpecificDAL = new APPSIT_DeliveryConstraintSpecificDAL();

                oMASSIT_DeliveryConstraintSpecificBEList = oAPPSIT_DeliveryConstraintSpecificDAL.GetDeliveryConstraintDAL(oMASSIT_DeliveryConstraintSpecificBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_DeliveryConstraintSpecificBEList;
        }
        public int? addEdiDeliveryConstraintSpecificBAL(MASSIT_DeliveryConstraintSpecificBE oMASSIT_DeliveryConstraintSpecificBE) {
            int? intResult = 0;
            try {
                APPSIT_DeliveryConstraintSpecificDAL oAPPSIT_DeliveryConstraintSpecificDAL = new APPSIT_DeliveryConstraintSpecificDAL();
                intResult = oAPPSIT_DeliveryConstraintSpecificDAL.addEdiDeliveryConstraintSpecificDAL(oMASSIT_DeliveryConstraintSpecificBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

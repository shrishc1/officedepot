﻿using System;
using System.Collections.Generic;
using System.Data;

using Utilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings {
    public class APPSIT_SpecificWeekSetupBAL : BaseBAL {

        public List<MASSIT_SpecificWeekSetupBE> GetSpecificWeekSetupDetailsBAL(MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE) {
            List<MASSIT_SpecificWeekSetupBE> oMASSIT_SpecificWeekSetupBEList = new List<MASSIT_SpecificWeekSetupBE>();

            try {
                APPSIT_SpecificWeekSetupDAL oMASSIT_SpecificWeekSetupDAL = new APPSIT_SpecificWeekSetupDAL();

                oMASSIT_SpecificWeekSetupBEList = oMASSIT_SpecificWeekSetupDAL.GetSpecificWeekSetupDetailsDAL(oMASSIT_SpecificWeekSetupBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_SpecificWeekSetupBEList;
        }

        public int? addEditSpecificWeekSetupBAL(MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE) {
            int? intResult = 0;
            try {
                APPSIT_SpecificWeekSetupDAL oMASSIT_SpecificWeekSetupDAL = new APPSIT_SpecificWeekSetupDAL();
                intResult = oMASSIT_SpecificWeekSetupDAL.addEditSpecificWeekSetupDAL(oMASSIT_SpecificWeekSetupBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MASSIT_SpecificWeekSetupBE> GetTotlasBAL(MASSIT_SpecificWeekSetupBE oMASSIT_SpecificWeekSetupBE) {
            List<MASSIT_SpecificWeekSetupBE> oMASSIT_SpecificWeekSetupBEList = new List<MASSIT_SpecificWeekSetupBE>();

            try {
                APPSIT_SpecificWeekSetupDAL oMASSIT_SpecificWeekSetupDAL = new APPSIT_SpecificWeekSetupDAL();

                oMASSIT_SpecificWeekSetupBEList = oMASSIT_SpecificWeekSetupDAL.GetTotalsDAL(oMASSIT_SpecificWeekSetupBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_SpecificWeekSetupBEList;
        }

    }
}

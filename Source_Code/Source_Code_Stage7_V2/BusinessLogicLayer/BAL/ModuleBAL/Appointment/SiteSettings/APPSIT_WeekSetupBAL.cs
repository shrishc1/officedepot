﻿using System;
using System.Collections.Generic;
using System.Data;

using Utilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings {
    public class MASSIT_WeekSetupBAL : BaseBAL{

        public List<MASSIT_WeekSetupBE> GetWeekSetupDetailsBAL(MASSIT_WeekSetupBE oMASSIT_WeekSetupBE) {
            List<MASSIT_WeekSetupBE> oMASSIT_WeekSetupBEList = new List<MASSIT_WeekSetupBE>();

            try {
                APPSIT_WeekSetupDAL oMASSIT_WeekSetupDAL = new APPSIT_WeekSetupDAL();
                                

                oMASSIT_WeekSetupBEList = oMASSIT_WeekSetupDAL.GetWeekSetupDetailsDAL(oMASSIT_WeekSetupBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_WeekSetupBEList;
        }

        public int? addEditWeekSetupBAL(MASSIT_WeekSetupBE oMASSIT_WeekSetupBE) {
            int? intResult = 0;
            try {
                APPSIT_WeekSetupDAL oMASSIT_WeekSetupDAL = new APPSIT_WeekSetupDAL();
                intResult = oMASSIT_WeekSetupDAL.addEditWeekSetupDAL(oMASSIT_WeekSetupBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public int? openAllDoorBAL(MASSIT_WeekSetupBE oMASSIT_WeekSetupBE)
        {
            int? intResult = 0;
            try
            {
                APPSIT_WeekSetupDAL oMASSIT_WeekSetupDAL = new APPSIT_WeekSetupDAL();
                intResult = oMASSIT_WeekSetupDAL.openAllDoorDAL(oMASSIT_WeekSetupBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MASSIT_WeekSetupBE> GetTotalsBAL(MASSIT_WeekSetupBE oMASSIT_WeekSetupBE) {
            List<MASSIT_WeekSetupBE> oMASSIT_WeekSetupBEList = new List<MASSIT_WeekSetupBE>();

            try {
                APPSIT_WeekSetupDAL oMASSIT_WeekSetupDAL = new APPSIT_WeekSetupDAL();

                oMASSIT_WeekSetupBEList = oMASSIT_WeekSetupDAL.GetTotalsDAL(oMASSIT_WeekSetupBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_WeekSetupBEList;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;

using Utilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings {
    public class APPSIT_VendorBAL {

        public List<MASSIT_VendorBE> GetVendorConstraintsBAL(MASSIT_VendorBE oMASSIT_VendorBE) {
            List<MASSIT_VendorBE> oMASSIT_VendorBEList = new List<MASSIT_VendorBE>();

            try {
                APPSIT_VendorDAL oMASSIT_VendorDAL = new APPSIT_VendorDAL();

                oMASSIT_VendorBEList = oMASSIT_VendorDAL.GetVendorConstraintsDAL(oMASSIT_VendorBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_VendorBEList;
        }

        public List<MASSIT_VendorBE> GetVendorMicellaneousConstraintsDetailsBAL(MASSIT_VendorBE oMASSIT_VendorBE)
        {
            List<MASSIT_VendorBE> oMASSIT_VendorBEList = new List<MASSIT_VendorBE>();

            try
            {
                APPSIT_VendorDAL oMASSIT_VendorDAL = new APPSIT_VendorDAL();

                oMASSIT_VendorBEList = oMASSIT_VendorDAL.GetVendorMicellaneousConstraintsDetailsDAL(oMASSIT_VendorBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_VendorBEList;
        }

        public int? addEditCarrierDetailsBAL(MASSIT_VendorBE oMASSIT_VendorBE) {
            int? intResult = 0;
            try {
                APPSIT_VendorDAL oMASSIT_VendorDAL = new APPSIT_VendorDAL();
                intResult = oMASSIT_VendorDAL.addEditVendorConstraintsDAL(oMASSIT_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MASSIT_VendorBE> GetVendorContactDetailsBAL(MASSIT_VendorBE oMASSIT_VendorBE) {
            List<MASSIT_VendorBE> oMASSIT_VendorBEList = new List<MASSIT_VendorBE>();

            try {
                APPSIT_VendorDAL oMASSIT_VendorDAL = new APPSIT_VendorDAL();

                oMASSIT_VendorBEList = oMASSIT_VendorDAL.GetVendorContactDetailsDAL(oMASSIT_VendorBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_VendorBEList;
        }

        public List<MASSIT_VendorBE> GetVendorCheckingRequiredBAL(MASSIT_VendorBE oMASSIT_VendorBE)
        {
            List<MASSIT_VendorBE> oMASSIT_VendorBEList = new List<MASSIT_VendorBE>();

            try
            {
                APPSIT_VendorDAL oMASSIT_VendorDAL = new APPSIT_VendorDAL();

                oMASSIT_VendorBEList = oMASSIT_VendorDAL.GetVendorCheckingRequiredDAL(oMASSIT_VendorBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_VendorBEList;
        }

        public List<MASSIT_VendorBE> GetCarriersVendorIDBAL(MASSIT_VendorBE oMASSIT_VendorBE)
        {
            List<MASSIT_VendorBE> oMASSIT_VendorBEList = new List<MASSIT_VendorBE>();

            try
            {
                APPSIT_VendorDAL oMASSIT_VendorDAL = new APPSIT_VendorDAL();

                oMASSIT_VendorBEList = oMASSIT_VendorDAL.GetCarriersVendorIDDAL(oMASSIT_VendorBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_VendorBEList;
        }

        public int? IsMiscConstVendorExistBAL(MASSIT_VendorBE oMASSIT_VendorBE)
        {
            int? intResult = 0;
            try
            {
                APPSIT_VendorDAL oMASSIT_VendorDAL = new APPSIT_VendorDAL();
                intResult = oMASSIT_VendorDAL.IsMiscConstVendorExistDAL(oMASSIT_VendorBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.VendorScorecard;
using DataAccessLayer.ModuleDAL.VendorScorecard;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.VendorScorecard
{
    public class MAS_VendorScoreCardBAL :BaseBAL
    {
        public int? UpdateVendorScoreCardBAL(MAS_VendorScoreCardBE objVendorScoreCardBE)
        {
            int? intResult = 0;
            try
            {
                MAS_VendorScoreCardDAL objVendorScoreCardDAL = new MAS_VendorScoreCardDAL();
                intResult = objVendorScoreCardDAL.UpdateVendorScoreCardDAL(objVendorScoreCardBE);
                objVendorScoreCardDAL = null; 
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? AddEditFrequencyBAL(MAS_VendorScoreCardBE objVendorScoreCardBE)
        {
            int? intResult = 0;
            try
            {
                MAS_VendorScoreCardDAL objVendorScoreCardDAL = new MAS_VendorScoreCardDAL();
                intResult = objVendorScoreCardDAL.AddEditFrequencyDAL(objVendorScoreCardBE);
                objVendorScoreCardDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteVendorFreqBAL(MAS_VendorScoreCardBE objVendorScoreCardBE)
        {
            int? intResult = 0;
            try
            {
                MAS_VendorScoreCardDAL objVendorScoreCardDAL = new MAS_VendorScoreCardDAL();
                intResult = objVendorScoreCardDAL.DeleteVendorFreqDAL(objVendorScoreCardBE);
                objVendorScoreCardDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public MAS_VendorScoreCardBE GetVendorScoreCardBAL()
        {
            MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
            try
            {
                MAS_VendorScoreCardDAL objVendorScoreCardDAL = new MAS_VendorScoreCardDAL();
                objVendorScoreCardBE = objVendorScoreCardDAL.GetVendorScoreCardDAL("GetVendorScoreCard");
                objVendorScoreCardDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return objVendorScoreCardBE;
        }

        public MAS_VendorScoreCardBE GetVendorFreqBAL(MAS_VendorScoreCardBE objVendorScoreCardBE)
        {
            MAS_VendorScoreCardBE objRetVendorScoreCardBE = new MAS_VendorScoreCardBE();
            try
            {
                MAS_VendorScoreCardDAL objVendorScoreCardDAL = new MAS_VendorScoreCardDAL();
                objRetVendorScoreCardBE = objVendorScoreCardDAL.GetVendorFreqDAL(objVendorScoreCardBE);
                objVendorScoreCardDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return objRetVendorScoreCardBE;
        }


        public List<MAS_VendorScoreCardBE> GetVendorByCountryBAL(MAS_VendorScoreCardBE objVendorScoreCardBE)
        {
            List<MAS_VendorScoreCardBE> objListVendorScoreCardBE = new List<MAS_VendorScoreCardBE>();
            try
            {
                MAS_VendorScoreCardDAL objVendorScoreCardDAL = new MAS_VendorScoreCardDAL();

                objListVendorScoreCardBE = objVendorScoreCardDAL.GetVendorByCountryDAL(objVendorScoreCardBE);
                objVendorScoreCardDAL = null;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return objListVendorScoreCardBE;
        }

        public List<MAS_VendorScoreCardBE> GetALLVendorFreqBAL(MAS_VendorScoreCardBE objVendorScoreCardBE)
        {
            List<MAS_VendorScoreCardBE> objListVendorFreqBE = new List<MAS_VendorScoreCardBE>();
            try
            {
                MAS_VendorScoreCardDAL objVendorFreqDAL = new MAS_VendorScoreCardDAL();

                objListVendorFreqBE = objVendorFreqDAL.GetALLVendorFreqDAL(objVendorScoreCardBE);
                objVendorFreqDAL = null;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return objListVendorFreqBE;
        }

        public void ExcecuteScorecardBAL(int Month) {
          
            try {
                MAS_VendorScoreCardDAL objVendorScoreCardDAL = new MAS_VendorScoreCardDAL();
                objVendorScoreCardDAL.ExcecuteScorecardDAL(Month);
                objVendorScoreCardDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
           
        }
    }
}

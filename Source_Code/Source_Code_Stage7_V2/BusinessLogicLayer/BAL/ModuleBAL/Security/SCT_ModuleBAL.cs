﻿using System;
using System.Collections.Generic;

using BusinessEntities.ModuleBE.Security;
using DataAccessLayer.ModuleDAL.Security;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.Security {
    public class SCT_ModuleBAL {

        public List<SCT_ModuleBE> GetModuleBAL(SCT_ModuleBE oSCT_ModuleBE) {
            List<SCT_ModuleBE> oSCT_ModuleBEList = new List<SCT_ModuleBE>();
            try {
                SCT_ModuleDAL oSCT_ModuleDAL = new SCT_ModuleDAL();
                oSCT_ModuleBEList = oSCT_ModuleDAL.GetModuleDAL(oSCT_ModuleBE);
                oSCT_ModuleDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_ModuleBEList;
        }
    }
}

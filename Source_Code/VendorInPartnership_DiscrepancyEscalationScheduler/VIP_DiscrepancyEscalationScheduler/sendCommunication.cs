﻿using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.IO;
using System.Resources;
using System.Threading;
using System.Globalization;
using System.Reflection;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Languages.Languages;
using System.Linq;



namespace VIP_DiscrepancyEscalationScheduler
{
    public class sendCommunicationCommon
    {
        public string sConn = ConfigurationManager.AppSettings["sConn"];
        public string fromAddress = System.Configuration.ConfigurationManager.AppSettings["fromAddress"] as string;
        public string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]);
        public string sPortalLinkNew = Convert.ToString(ConfigurationManager.AppSettings["PortallinkNew"]);

        public DataSet getDiscrepancyCommunicationDetail(string Action, int iDiscrepancyLogID, string sCurrentCommunicationLevel)
        {
            DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyMailBE.Action = "getDiscrepancyCommunicationDetail";
            DataSet dsSendCommunication = oDiscrepancyBAL.GetCommunicationDataToSendMailBAL(oDiscrepancyMailBE);
            return dsSendCommunication;
        }

        public DataSet getLanguage(int? vendorID, string sSentTo)
        {

            DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyMailBE.Action = "GetLanguageIDForCommunication";
            oDiscrepancyMailBE.vendorID = vendorID;
            oDiscrepancyMailBE.sentTo = sSentTo;

            DataSet ds = oDiscrepancyBAL.getLanguageIDforCommunicationBAL(oDiscrepancyMailBE);
            return ds;
        }

        public DataSet getDiscrepancyLogDetail(string Action, int iDiscrepancyLogID)
        {
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyBE.Action = Action;
            oDiscrepancyBE.DiscrepancyLogID = iDiscrepancyLogID;

            DataSet ds = oDiscrepancyBAL.GetDiscrepancyDetailsBAL(oDiscrepancyBE);
            return ds;
        }

        public DataSet getEsclationDetail(string Action, int iDiscrepancyLogID)
        {
            DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
            DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
            oDiscrepancyBE.Action = Action;
            oDiscrepancyBE.DiscrepancyLogID = iDiscrepancyLogID;

            DataSet ds = oDiscrepancyBAL.GetDiscrepancyDetailsBAL(oDiscrepancyBE);
            return ds;
        }

        public void sendMail(string toAddress, string mailBody, string mailSubject, string fromAddress, bool bIsHTMLMail)
        {
            try
            {
                clsEmail oclsEmail = new clsEmail();
                oclsEmail.sendMail(toAddress, mailBody, mailSubject, fromAddress, true);

                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("\r\n");
                sbMessage.Append("Mail successfully sent to " + toAddress);
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            catch (Exception ex)
            {
                errorLog(1, toAddress, mailBody, mailSubject, ex.Message);
                LogUtility.SaveErrorLogEntry(ex, "Mail failed for: " + toAddress);
            }
        }

        public int? saveMail(int iDiscrepancyLogID, string sentTo, string mailBody, string mailSubject, int? languageID,
            string sCommunicationLevel, bool mailSentInLanguage)
        {
            int? result = 0;
            try
            {
                DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
                oDiscrepancyMailBE.QueryDiscrepancy = new DISLog_QueryDiscrepancyBE();
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
                oDiscrepancyMailBE.Action = "saveDiscrepancyMail";
                oDiscrepancyMailBE.discrepancyLogID = iDiscrepancyLogID;
                oDiscrepancyMailBE.sentTo = sentTo;
                oDiscrepancyMailBE.mailBody = mailBody;
                oDiscrepancyMailBE.mailSubject = mailSubject;
                oDiscrepancyMailBE.sentDate = System.DateTime.Now;
                oDiscrepancyMailBE.languageID = languageID;
                oDiscrepancyMailBE.communicationLevel = sCommunicationLevel;
                oDiscrepancyMailBE.MailSentInLanguage = mailSentInLanguage;
                result = oDiscrepancyBAL.saveDiscrepancyMailBAL(oDiscrepancyMailBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return result;
        }

        public string getResourceValue(string sEscalationLevel)
        {
            return getGlobalResourceValue(sEscalationLevel);
        }

        public static string getGlobalResourceValue(string key)
        {
            ResourceManager LocRM = new ResourceManager("VIP_DiscrepancyEscalationScheduler.Properties.OfficeDepot", Assembly.GetExecutingAssembly());
            return LocRM.GetString(key);
        }

        public void errorLog(int iDiscrepancyID, string sSubject, string sSentTo, string sBody, string sErrorDescription)
        {
            try
            {
                DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
                oDiscrepancyMailBE.Action = "errorLog";
                oDiscrepancyMailBE.discrepancyLogID = iDiscrepancyID;
                oDiscrepancyMailBE.mailSubject = sSubject;
                oDiscrepancyMailBE.sentTo = sSentTo;
                oDiscrepancyMailBE.mailBody = sBody;
                oDiscrepancyMailBE.errorDescription = sErrorDescription.Length > 3000 ? sErrorDescription.Substring(0, 3000) : sErrorDescription;
                oDiscrepancyMailBE.sentDate = System.DateTime.Now;

                oDiscrepancyBAL.errorLogBAL(oDiscrepancyMailBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
        }


        public string[] GetVendorOTIFContactWithLanguage(int VendorID)
        {
            string[] VendorData = new string[3];
            BackOrderBAL oBackOrderVUBAL = new BackOrderBAL();
            BackOrderBE oBackOrderVUBE = new BackOrderBE();
            oBackOrderVUBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            string VendorEmail = string.Empty;
            List<BackOrderBE> lstVendorDetails = null;

            oBackOrderVUBE.Action = "GetVendorOTIFContact";
            oBackOrderVUBE.Vendor.VendorID = VendorID;

            lstVendorDetails = oBackOrderVUBAL.GetVendorContactDetailsBAL(oBackOrderVUBE);
            if (lstVendorDetails != null && lstVendorDetails.Count > 0)
            {

                foreach (BackOrderBE item in lstVendorDetails)
                {
                    if (item.Vendor.VendorContactEmail == null || item.Vendor.VendorContactEmail == "")
                    {
                        VendorData = GetVendorDefaultEmailWithLanguageID(VendorID);
                    }
                    else
                    {
                        VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                        VendorData[1] += item.Vendor.LanguageID.ToString() + ",";
                        VendorData[2] += item.User.UserName + ",";
                    }
                }
            }
            else
            {
                VendorData = GetVendorDefaultEmailWithLanguageID(VendorID);
            }
            return VendorData;
        }

        private string[] GetVendorDefaultEmailWithLanguageID(int VendorID)
        {
            string[] VendorData = new string[3];
            BackOrderBAL oBackOrderVUBAL = new BackOrderBAL();
            BackOrderBE oBackOrderVUBE = new BackOrderBE();
            oBackOrderVUBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            List<BackOrderBE> lstVendorDetails = null;
            string VendorEmail = string.Empty;
            oBackOrderVUBE.Action = "GetContactDetails"; // from Up_vendor table
            oBackOrderVUBE.Vendor.VendorID = VendorID;

            VendorData[0] = "";
            VendorData[1] = "";

            lstVendorDetails = oBackOrderVUBAL.GetVendorContactDetailsBAL(oBackOrderVUBE);

            foreach (BackOrderBE item in lstVendorDetails)
            {
                if (!string.IsNullOrEmpty(item.Vendor.VendorContactEmail))
                {
                    VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                    VendorData[1] += item.Vendor.LanguageID.ToString() + ",";
                }
            }
            if (VendorData[0] == "")
            {
                VendorData[0] = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"] + ","; ;
                VendorData[1] = "1,";
                VendorData[2] = lstVendorDetails[0].Vendor.VendorName + ","; 
            }

            return VendorData;
        }

        public string[] GetVendorInitailEmailWithLanguage(int VendorID, DateTime InitialCommDate)
        {
            string[] VendorData = new string[2];
            BackOrderBAL oBackOrderVUBAL = new BackOrderBAL();
            BackOrderBE oBackOrderVUBE = new BackOrderBE();
            oBackOrderVUBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            string VendorEmail = string.Empty;
            List<BackOrderBE> lstVendorDetails = null;

            oBackOrderVUBE.Action = "GetVendorInitialEmailList";
            oBackOrderVUBE.Vendor.VendorID = VendorID;
            oBackOrderVUBE.InitialCommDate = InitialCommDate;

            lstVendorDetails = oBackOrderVUBAL.GetVendorInitialEmailListBAL(oBackOrderVUBE);
            if (lstVendorDetails != null && lstVendorDetails.Count > 0)
            {
                foreach (BackOrderBE item in lstVendorDetails)
                {

                    VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                    VendorData[1] += item.Vendor.LanguageID.ToString() + ",";
                }
            }
            else
            {

            }
            return VendorData;
        }


    }

    public class sendCommunication : sendCommunicationCommon
    {
        public int? sendCommunicationByEMail(int iDiscrepancy, string discrepancyType, string toAddress, int languageID, string language, string sCurrentEscalationLevel, string sNextEscalationLevel, string sEscalationType2SentDate, bool closedDicrepancy, DateTime? dtCollectionDate, ref string sSendMailLinkAll, bool doWrite, bool mailSentInLanguage)
        {
            int? retValue = -1;
            if (!string.IsNullOrEmpty(toAddress))
                retValue = sendAndSaveMail(iDiscrepancy, discrepancyType, toAddress, languageID, language, sCurrentEscalationLevel, sNextEscalationLevel, sEscalationType2SentDate, closedDicrepancy, dtCollectionDate, ref sSendMailLinkAll, doWrite, mailSentInLanguage);
            else
                retValue = -1;
            return retValue;
        }

        private int? sendAndSaveMail(int iDiscrepancyLogID, string discrepancyType, string toAddress, int languageID, string language,
            string sCurrentEscalationLevel, string sNextEscalationLevel, string sEscalationType2SentDate, bool closeDiscrepancy,
            DateTime? dtCollectionDate, ref string sSendMailLinkAll, bool doWrite, bool mailSentInLanguage)
        {
            string mailSubjectText = string.Empty;
            string htmlBody = string.Empty;
            try
            {
                DataSet dsDisLog = getDiscrepancyLogDetail("GetDiscrepancyLog", iDiscrepancyLogID);
                DataSet dsSecondEsclation = getEsclationDetail("GetEsclationDate", iDiscrepancyLogID);
                string siteAddress, accountPayable = string.Empty;

                //string templateFile = templatesPath + @"\" + sNextEscalationLevel + @"\dis_communication" + "." + language + ".htm";
                string templateFile = string.Empty;
                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                path = path.Replace(@"\bin\debug", "");

                string templatesPath = path + @"emailtemplates\";
                if (discrepancyType == "invoicequerydiscrepancy")
                {
                    templateFile = templatesPath + sNextEscalationLevel + @"\InvoiceQuery.english.htm";
                }
                else
                {
                     templateFile = templatesPath + sNextEscalationLevel + @"\dis_communication.english.htm";
                }
                if (System.IO.File.Exists(templateFile.ToLower()) && dsDisLog != null && dsDisLog.Tables.Count > 0 && dsDisLog.Tables[0].Rows.Count > 0)
                {                   

                    DiscrepancyBE oDiscrepancyBE1 = new DiscrepancyBE();
                    DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
                    oDiscrepancyBE1.Action = "GetVendorSubscriptionStatus";
                    oDiscrepancyBE1.DiscrepancyLogID = Convert.ToInt32(iDiscrepancyLogID);
                    string VendorStatus = oDiscrepancyBAL.GetDebitCreditStatusBAL(oDiscrepancyBE1);
                    string EscalationLevel = string.Empty;
                    string EsclationDateInitial = string.Empty;
                    if (sNextEscalationLevel == "EscalationType3")
                    {
                        if (VendorStatus == "false")
                        {
                            EscalationLevel = sNextEscalationLevel;
                            EsclationDateInitial = Utilities.Common.ToDateTimeInMMDDYYYY(dsSecondEsclation.Tables[0].Rows[0]["SentDate"]);
                        }
                        else
                        {
                            EscalationLevel = "EscalationType3_1";
                            EsclationDateInitial = Utilities.Common.ToDateTimeInMMDDYYYY(dsSecondEsclation.Tables[0].Rows[0]["SentDate"]);
                        }
                    }
                    else
                    {
                        EscalationLevel = sNextEscalationLevel;
                        EsclationDateInitial = Utilities.Common.ToDateTimeInMMDDYYYY(dsDisLog.Tables[0].Rows[0]["DiscrepancyLogDate"]);
                    }

                    string sDiscrepancyRelatedText = getDiscrepancyText(discrepancyType, EsclationDateInitial, sEscalationType2SentDate, EscalationLevel, Utilities.Common.ToDateTimeInMMDDYYYY(dtCollectionDate));

                    htmlBody = getMailBody(iDiscrepancyLogID, discrepancyType, dsDisLog, templateFile, sDiscrepancyRelatedText, out siteAddress, out accountPayable, language);

                    mailSubjectText = "Vendor Discrepancy Report " + dsDisLog.Tables[0].Rows[0]["VDRNo"] + " : Urgent Responses Required";
                    if (discrepancyType == "invoicequerydiscrepancy")
                    {
                        htmlBody = htmlBody.Replace("##dd/mm/yyyy##", EsclationDateInitial);
                        htmlBody = htmlBody.Replace("##dd/mm/yyyy_1##", EsclationDateInitial);
                        htmlBody = htmlBody.Replace("##dd/mm/yyyy_2##", sEscalationType2SentDate);
                    }

                    if (mailSentInLanguage)
                    {
                        /* Logic to send email on developer email Id which defiened in App.config file */
                        //string debugEmailAddress = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["DebugEmailAddress"]);
                        //sendMail(debugEmailAddress, htmlBody, mailSubjectText, fromAddress, true);

                        /* Logic to send email to actual vendor's user */
                        sendMail(toAddress, htmlBody, mailSubjectText, fromAddress, true);
                    }

                    htmlBody = htmlBody.Replace("{logoInnerPath}", System.Configuration.ConfigurationManager.AppSettings["WebsiteAddress"] + @"/Images/OfficeDepotLogo.jpg");
                    int? iRetVal = saveMail(iDiscrepancyLogID, toAddress, htmlBody, mailSubjectText, languageID, sNextEscalationLevel, mailSentInLanguage);

                    string sSendMailLink = string.Empty;
                    if (mailSentInLanguage)
                    {
                        sSendMailLink = "<a target='_blank' href='" + Common.EncryptQuery("DISLog_ReSendCommunication.aspx?communicationid=" + iRetVal + "&CommunicationLevel=" + sNextEscalationLevel) + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + toAddress + "</a>" + System.Environment.NewLine;
                    }

                    sSendMailLinkAll = sSendMailLinkAll + sSendMailLink;                    

                    if (doWrite)
                    {
                        InsertHTML(iDiscrepancyLogID, toAddress, sCurrentEscalationLevel, sNextEscalationLevel, closeDiscrepancy, sSendMailLinkAll, discrepancyType);
                        sSendMailLinkAll = "";
                    }

                    if (sNextEscalationLevel.ToLower() == "escalationtype4")
                    {
                        if (doWrite)
                        {
                            sNextEscalationLevel = "EscalationType41";

                            InsertHTML(iDiscrepancyLogID, toAddress, sCurrentEscalationLevel, sNextEscalationLevel, closeDiscrepancy, sSendMailLinkAll,discrepancyType);
                        }
                    }


                    return iRetVal;
                }
                else
                    errorLog(iDiscrepancyLogID, mailSubjectText, toAddress, htmlBody, !System.IO.File.Exists(templateFile.ToLower()) ? "Template file not found" : "No data found in DiscrepancyLog table");
            }
            catch (Exception ex)
            {
                errorLog(iDiscrepancyLogID, mailSubjectText, toAddress, htmlBody, ex.Message);
            }
            return null;
        }

        private string getDiscrepancyText(string discrepancyType, string DiscrepancyLogDate, string sEscalationType2SentDate, string sNextCommunicationLevel, string sCollectionDate)
        {
            string sDiscrepancyRelatedtext = getResourceValue(sNextCommunicationLevel);
            if (!string.IsNullOrEmpty(sDiscrepancyRelatedtext))
            {
                sDiscrepancyRelatedtext = sDiscrepancyRelatedtext.Replace("##DiscrepancyLogDate##", DiscrepancyLogDate);
                sDiscrepancyRelatedtext = sDiscrepancyRelatedtext.Replace("##EscalationType2SentDate##", sEscalationType2SentDate);
                sDiscrepancyRelatedtext = sDiscrepancyRelatedtext.Replace("##PortalLink##", sPortalLink);
                sDiscrepancyRelatedtext = sDiscrepancyRelatedtext.Replace("##CollectionDate##", sCollectionDate);
            }
            return sDiscrepancyRelatedtext;
        }
        public DataSet getDiscrepancyLogSiteVendorDetail(int iDiscrepancyLogID, string discrepancyType, out string siteAddress, out string accountPayable)
        {
            DataSet dsDisLogSiteVendor = new DataSet();
            try
            {

                DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
                oDiscrepancyBE.Action = "GetDiscrepancySiteVendorDetail";
                oDiscrepancyBE.DiscrepancyLogID = iDiscrepancyLogID;

                dsDisLogSiteVendor = oDiscrepancyBAL.GetDiscrepancyDetailsBAL(oDiscrepancyBE);


                siteAddress = accountPayable = string.Empty;
                if (dsDisLogSiteVendor != null && dsDisLogSiteVendor.Tables.Count > 0 && dsDisLogSiteVendor.Tables[0].Rows.Count > 0)
                {
                    //if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteName"]))) { siteAddress = Convert.ToString(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteName"])); }
                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine1"]))) { siteAddress = Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine1"]); }
                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine2"]))) { siteAddress = siteAddress + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine2"]); }
                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine3"]))) { siteAddress = siteAddress + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine3"]); }
                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine4"]))) { siteAddress = siteAddress + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine4"]); }
                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine5"]))) { siteAddress = siteAddress + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine5"]); }
                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine6"]))) { siteAddress = siteAddress + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine6"]); }
                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SitePincode"]))) { siteAddress = siteAddress + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SitePincode"]); }

                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APAddressLine1"]))) { accountPayable = Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APAddressLine1"]); }
                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APAddressLine2"]))) { accountPayable = accountPayable + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APAddressLine2"]); }
                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APAddressLine3"]))) { accountPayable = accountPayable + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APAddressLine3"]); }
                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APAddressLine4"]))) { accountPayable = accountPayable + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APAddressLine4"]); }
                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APAddressLine5"]))) { accountPayable = accountPayable + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APAddressLine5"]); }
                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APAddressLine6"]))) { accountPayable = accountPayable + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APAddressLine6"]); }
                    if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APPincode"]))) { accountPayable = accountPayable + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["APPincode"]); }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            siteAddress = accountPayable = string.Empty;
            return dsDisLogSiteVendor;
        }

        public string getMailBody(int iDiscrepancyLogID, string discrepancyType, DataSet dsDisLog, string templateFile, string sDiscrepancyRelatedText, out string siteAddress, out string accountPayable, string language)
        {

            string htmlBody = null;
            using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
            {
                DataSet dsDisLogSiteVendor = getDiscrepancyLogSiteVendorDetail(iDiscrepancyLogID, discrepancyType, out siteAddress, out accountPayable);

                htmlBody = sReader.ReadToEnd();
                htmlBody = htmlBody.Replace("{DearSir/Madam}", Common.getGlobalResourceValue("DearSirMadam", language));
                htmlBody = htmlBody.Replace("{OurPuchaseOrderNumber}", Common.getGlobalResourceValue("OurPuchaseOrderNumber", language));
                htmlBody = htmlBody.Replace("{Yoursfaithfully}", Common.getGlobalResourceValue("Yoursfaithfully", language));
                htmlBody = htmlBody.Replace("{InventoryManager}", Common.getGlobalResourceValue("InventoryManager", language));
                htmlBody = htmlBody.Replace("{YourDeliveryNoteNumber}", Common.getGlobalResourceValue("YourDeliveryNoteNumber", language));
                htmlBody = htmlBody.Replace("{VENDORDISCREPANCYREPORT}", Common.getGlobalResourceValue("VENDORDISCREPANCYREPORT", language));
                htmlBody = htmlBody.Replace("{VDRNOText}", Common.getGlobalResourceValue("VDRNOText", language));
                htmlBody = htmlBody.Replace("{DateofDelivery}", Common.getGlobalResourceValue("DateofDelivery", language));
                htmlBody = htmlBody.Replace("{Date}", Common.getGlobalResourceValue("Date", language));

                if (discrepancyType.ToLower() == "invoicequerydiscrepancy")
                {

                    htmlBody = htmlBody.Replace("{POHash}", Common.getGlobalResourceValue("POHash", language));
                    htmlBody = htmlBody.Replace("{InvoiceNumHash}", Common.getGlobalResourceValue("InvoiceNumHash", language));
                    htmlBody = htmlBody.Replace("{DiscrepancyNo}", Common.getGlobalResourceValue("DiscrepancyNo", language));
                    htmlBody = htmlBody.Replace("{DearSirMadam}", Common.getGlobalResourceValue("DearSirMadam", language));
                    htmlBody = htmlBody.Replace("{InvoiceQueryReminder1Line1}", Common.getGlobalResourceValue("InvoiceQueryReminder1Line1", language));
                    htmlBody = htmlBody.Replace("{InvoiceQueryReminder1Line2}", Common.getGlobalResourceValue("InvoiceQueryReminder1Line2", language));
                    htmlBody = htmlBody.Replace("{InvoiceQueryReminder1Line3}", Common.getGlobalResourceValue("InvoiceQueryReminder1Line3", language));
                    htmlBody = htmlBody.Replace("{InvoiceQueryReminder3Line2}", Common.getGlobalResourceValue("InvoiceQueryReminder3Line2", language));

                    htmlBody = htmlBody.Replace("{InvoiceQueryReminder4Line1}", Common.getGlobalResourceValue("InvoiceQueryReminder4Line1", language));
                    htmlBody = htmlBody.Replace("{InvoiceQueryReminder4Line2}", Common.getGlobalResourceValue("InvoiceQueryReminder4Line2", language));
                    htmlBody = htmlBody.Replace("{InvoiceQueryReminder4Line3}", Common.getGlobalResourceValue("InvoiceQueryReminder4Line3", language));

                    htmlBody = htmlBody.Replace("{InvoiceQueryReminder4Line3}", Common.getGlobalResourceValue("InvoiceQueryReminder4Line3", language));
                     
                    htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLink + ">" + sPortalLinkNew + " </a>");
                    htmlBody = htmlBody.Replace("{DiscrepancyValue}", Convert.ToString(dsDisLog.Tables[0].Rows[0]["VDRNo"]));
                    htmlBody = htmlBody.Replace("{POValue}", Convert.ToString(dsDisLog.Tables[0].Rows[0]["PurchaseOrderNumber"]));
                    htmlBody = htmlBody.Replace("{InvoiceValue}", Convert.ToString(dsDisLog.Tables[0].Rows[0]["DeliveryNoteNumber"]));
                    htmlBody = htmlBody.Replace("{APClerkName}", Convert.ToString(dsDisLog.Tables[0].Rows[0]["StockPlannerName"]));
                    htmlBody = htmlBody.Replace("{APClerkContactNo}", Convert.ToString(dsDisLog.Tables[0].Rows[0]["StockContactNo"]));

                    htmlBody = htmlBody.Replace("{APClerk}", Common.getGlobalResourceValue("APClerkletter", language));

                    htmlBody = htmlBody.Replace("{GoodsInTeam}", Common.getGlobalResourceValue("GoodsINTeam", language));

                    if (dsDisLogSiteVendor != null && dsDisLogSiteVendor.Tables.Count > 0 && dsDisLogSiteVendor.Tables[0].Rows.Count > 0)
                    {
                        //if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteName"]))) { siteAddress = Convert.ToString(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteName"])); }
                        if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine1"]))) { siteAddress = Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine1"]); }
                        if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine2"]))) { siteAddress = siteAddress + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine2"]); }
                        if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine3"]))) { siteAddress = siteAddress + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine3"]); }
                        if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine4"]))) { siteAddress = siteAddress + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine4"]); }
                        if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine5"]))) { siteAddress = siteAddress + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine5"]); }
                        if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine6"]))) { siteAddress = siteAddress + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SiteAddressLine6"]); }
                        if (!string.IsNullOrEmpty(Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SitePincode"]))) { siteAddress = siteAddress + ", " + Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["SitePincode"]); }
                        htmlBody = htmlBody.Replace("{SiteAddress}", siteAddress);
                    }

                }
                else
                {
                    htmlBody = htmlBody.Replace("{SiteAddress}", siteAddress);
                }               
                htmlBody = htmlBody.Replace("{CommunicationDate}", System.DateTime.Now.ToString("dd/MM/yyyy"));
                htmlBody = htmlBody.Replace("{VendorCode}", Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["Vendor_No"]));
                htmlBody = htmlBody.Replace("{VendorName}", Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["Vendor_Name"]));
                htmlBody = htmlBody.Replace("{VendorAddress1}", Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["address 1"]));
                htmlBody = htmlBody.Replace("{VDRNo}", Convert.ToString(dsDisLog.Tables[0].Rows[0]["VDRNo"]));
                htmlBody = htmlBody.Replace("{VendorAddress2}", Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["address 2"]));
                htmlBody = htmlBody.Replace("{VMPPIN}", Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["VMPPIN"]));
                htmlBody = htmlBody.Replace("{VMPPOU}", Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["VMPPOU"]));
                htmlBody = htmlBody.Replace("{VendorCity}", Convert.ToString(dsDisLogSiteVendor.Tables[0].Rows[0]["city"]));
                htmlBody = htmlBody.Replace("{DearSirMadam}", Common.getGlobalResourceValue("DearSirMadam", language));


               

                if (discrepancyType.ToLower() == "nopurchaseordernumber")
                    htmlBody = htmlBody.Replace("{PurchaseOrderNumber}", "Not quoted on paperwork");
                else
                    htmlBody = htmlBody.Replace("{PurchaseOrderNumber}", Convert.ToString(dsDisLog.Tables[0].Rows[0]["PurchaseOrderNumber"]));

                htmlBody = htmlBody.Replace("{DeliveryNoteNumber}", Convert.ToString(dsDisLog.Tables[0].Rows[0]["DeliveryNoteNumber"]));


                if (discrepancyType.ToLower() == "prematureinvoicereceipt" || discrepancyType.ToLower() == "prematureinvoice")
                    htmlBody = htmlBody.Replace("{DeliveryNoteNumberText}", Common.getGlobalResourceValue("InvoiceNumberText", language));
                else
                    htmlBody = htmlBody.Replace("{DeliveryNoteNumberText}", Common.getGlobalResourceValue("DeliveryNoteNumberText", language));

                htmlBody = htmlBody.Replace("{DeliveryDate}", Convert.ToString(dsDisLog.Tables[0].Rows[0]["DeliveryArrivedDate"]) == null ? "" : Utilities.Common.ToDateTimeInMMDDYYYY(Convert.ToString(dsDisLog.Tables[0].Rows[0]["DeliveryArrivedDate"])));
                //htmlBody = htmlBody.Replace("{Reference}", Convert.ToString(dsDisLog.Tables[0].Rows[0]["PaperworkReference"]));
                htmlBody = htmlBody.Replace("{DiscrepancyRelatedText}", sDiscrepancyRelatedText);
                htmlBody = htmlBody.Replace("{InventoryManagerName}", Convert.ToString(dsDisLog.Tables[0].Rows[0]["StockPlannerName"]));
                htmlBody = htmlBody.Replace("({InventoryManagerNumber})", Convert.ToString(dsDisLog.Tables[0].Rows[0]["StockContactNo"]));
                htmlBody = htmlBody.Replace("{AccountPayable}", accountPayable);
            }
            return htmlBody;
        }
      
        private void InsertHTML(int iDiscrepancyLogID, string toAddress, string sCurrentEscalationLevel, string sNextEscalationLevel, bool CloseDiscrepancy, string sLink,string DiscrepancyType)
        {
            try
            {
                string Action = "InsertHTML";
                DateTime LoggedDateTime = DateTime.Now;
                int LevelNumber = 1;

                WorkflowHTML oWorkflowHTML = new WorkflowHTML();

                string INVHTML = oWorkflowHTML.function3("White", null, "", "");
                bool INVActionRequired = false;
                string INVUserControl = null;

                string APHTML = oWorkflowHTML.function3("White", null, "", "");
                bool APActionRequired = false;
                string APUserControl = null;

                string GINHTML, GINUserControl, VENHTML, VENUserControl;
                GINHTML = GINUserControl = VENHTML = VENUserControl = null;
                bool GINActionRequired, VenActionRequired = false;
                GINActionRequired = VenActionRequired = false;
                //***************** VendorStatus **********************
                 DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
                oDiscrepancyBE.Action = "GetVendorSubscriptionStatus";
                oDiscrepancyBE.DiscrepancyLogID = iDiscrepancyLogID;
                string Status = oDiscrepancyBAL.GetVendorStatusDAL(oDiscrepancyBE);

                //*****************************************************


                //***************** Debit SreditStatus **********************
                oDiscrepancyBE = new DiscrepancyBE();
                oDiscrepancyBAL = new DiscrepancyBAL();
                oDiscrepancyBE.Action = "GetDebitCreditStatus";
                oDiscrepancyBE.DiscrepancyLogID = iDiscrepancyLogID;
                string GetDebitCreditStatus = oDiscrepancyBAL.GetDebitCreditStatusBAL(oDiscrepancyBE);

                //*****************************************************
                if (DiscrepancyType.ToLower() == "invoicequerydiscrepancy")
                {
                    if (sNextEscalationLevel.ToLower() == "escalationtype2")
                    {
                        VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sLink, "", @"Escalation communication sent asking for a response to the discrepancy");
                        VenActionRequired = false;
                    }
                    else if (sNextEscalationLevel.ToLower() == "escalationtype3")
                    {
                        APHTML = oWorkflowHTML.function4("Blue", "", null, "Accounts Payable", "", "", @"
Vendor has not responded to this discrepancy. Please contact manually to find a resolution", "", "", DateTime.Now, "");
                        APActionRequired = true;
                        APUserControl = "ACP008";

                        VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sLink, "", @"Vendor not responded to escalation. The vendor action is closed and Accounts Payable will now intervene");
                        VenActionRequired = false;
                        VENUserControl = string.Empty;

                    }

                    else if (sNextEscalationLevel.ToLower() == "escalationtype4")
                    {
                        VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sLink, "", @"Escalation communication sent asking for a response to the discrepancy");
                        VenActionRequired = false;
                    }
                    else if (sNextEscalationLevel.ToLower() == "escalationtype5" && GetDebitCreditStatus == "D")
                    {
                        APHTML = oWorkflowHTML.function4("Blue", "", null, "Accounts Payable", "", "", @"
Goods In have investigated but vendor has not replied. Please debit vendor for the missing item", "", "", DateTime.Now, "");
                        APActionRequired = true;
                        APUserControl = "ACP009";

                        VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sLink, "", @"Vendor not responded to escalation. The vendor action is closed and Accounts Payable will now intervene");
                        VenActionRequired = false;
                        VENUserControl = string.Empty;
                    }
                    else if (sNextEscalationLevel.ToLower() == "escalationtype5" && GetDebitCreditStatus == "C")
                    {
                        APHTML = oWorkflowHTML.function4("Blue", "", null, "Accounts Payable", "", "", @"
Goods In have investigated but vendor has not replied. Please debit vendor for the missing item", "", "", DateTime.Now, "");
                        APActionRequired = true;
                        APUserControl = "ACP010";

                        VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sLink, "", @"Vendor not responded to escalation. The vendor action is closed and Accounts Payable will now intervene");
                        VenActionRequired = false;
                        VENUserControl = string.Empty;
                    }

                    else if (sNextEscalationLevel.ToLower() == "escalationtype5" && GetDebitCreditStatus == "N")
                    {
                        APHTML = oWorkflowHTML.function3("White", null, "", "");
                        APActionRequired = false;

                        VENHTML = oWorkflowHTML.function3("White", null, "", "");
                        VenActionRequired = false;                        
                    }
                }

                else
                {

                    if (sNextEscalationLevel.ToLower() == "escalationtype2")
                    {
                        VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sLink, "", @"Escalation communication sent asking for a response to the discrepancy");
                        VenActionRequired = false;
                    }
                    else if (sNextEscalationLevel.ToLower() == "escalationtype3")
                    {
                        if (Status == "False")
                        {
                            GINHTML = oWorkflowHTML.function4("Blue", "", null, "Goods In", "", "", @"Vendor has not responded to requests to arrange collection. Please contact vendor directly to request what we should do with the goods", "", "", DateTime.Now, "");
                            GINActionRequired = true;
                            GINUserControl = "GIN003";

                            VENHTML = oWorkflowHTML.function4("Blue", "", DateTime.Now, sLink, "", @"Escalation communication sent asking for a response to the discrepancy");
                            VenActionRequired = false;
                            VENUserControl = string.Empty;
                        }
                        else
                        {
                            GINHTML = oWorkflowHTML.function4("Blue", "", null, "Goods In", "", "", @"Please contact vendor to arrange next steps in returning the goods", "", "", DateTime.Now, "");
                            GINActionRequired = true;
                            GINUserControl = "GIN003";

                            VENHTML = oWorkflowHTML.function4("Yellow", "", DateTime.Now, sLink, "", @"Escalation communication sent. Due to lack of vendor response, SYSTEM force closed the vendor action.");
                            VenActionRequired = false;
                            VENUserControl = string.Empty;
                        }
                    }
                    else if (sNextEscalationLevel.ToLower() == "escalationtype5")
                    {

                        VENHTML = oWorkflowHTML.function4("Yellow", "", DateTime.Now, sLink, "", @"Escalation communication sent. Due to lack of vendor response, SYSTEM force closed the vendor action.");
                        VenActionRequired = false;
                        CloseDiscrepancy = true;

                    }
                    else if (sNextEscalationLevel.ToLower() == "escalationtype4")
                    {
                        GINHTML = oWorkflowHTML.function4("Yellow", "", null, "Goods In", "", "", @"Vendor failed to collect goods", "", "", DateTime.Now, "");
                        GINActionRequired = false;

                        VENHTML = oWorkflowHTML.function4("Yellow", "", DateTime.Now, sLink, "", @"Escalation communication sent.  Forced action taken by SYSTEM, as vendor failed to collect goods on scheduled collection date.");
                        VenActionRequired = false;
                        VENUserControl = string.Empty;
                    }
                    else if (sNextEscalationLevel.ToLower() == "escalationtype41")
                    {
                        GINHTML = oWorkflowHTML.function4("Blue", "", null, "Goods In", "", "", @"Please arrange for goods to be returned to the vendor and confirm the carriage charge", "", "", DateTime.Now, "");
                        GINActionRequired = true;
                        GINUserControl = "GIN003";
                    }
                }
                addWorkFlowHTML(Action, iDiscrepancyLogID, LoggedDateTime, LevelNumber, GINHTML, GINActionRequired, GINUserControl, INVHTML, INVActionRequired, INVUserControl, APHTML, APActionRequired, APUserControl, VENHTML, VenActionRequired, VENUserControl, CloseDiscrepancy, sNextEscalationLevel, Status);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex, "Error in InsertHTML method");
            }
        }

        public void addWorkFlowHTML(string Action, int iDiscrepancyLogID, DateTime LoggedDateTime, int LevelNumber, string GINHTML, bool GINActionRequired, string GINUserControl, string INVHTML, bool INVActionRequired, string INVUserControl, string APHTML, bool APActionRequired, string APUserControl, string VENHTML, bool VenActionRequired, string VENUserControl, bool CloseDiscrepancy, string NextEscalationLevel,string status)
        {
            try
            {
                DiscrepancyBE oDiscrepancyBE = new DiscrepancyBE();
                DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
                oDiscrepancyBE.Action = Action;
                oDiscrepancyBE.DiscrepancyLogID = iDiscrepancyLogID;
                oDiscrepancyBE.LoggedDateTime = LoggedDateTime;
                oDiscrepancyBE.LevelNumber = LevelNumber;
                oDiscrepancyBE.GINHTML = GINHTML;
                oDiscrepancyBE.GINActionRequired = GINActionRequired;
                oDiscrepancyBE.GINUserControl = GINUserControl;
                oDiscrepancyBE.INVHTML = INVHTML;
                oDiscrepancyBE.INVActionRequired = INVActionRequired;
                oDiscrepancyBE.INVUserControl = INVUserControl;
                oDiscrepancyBE.APHTML = APHTML;
                oDiscrepancyBE.APActionRequired = APActionRequired;
                oDiscrepancyBE.APUserControl = APUserControl;
                oDiscrepancyBE.VENHTML = VENHTML;
                oDiscrepancyBE.VenActionRequired = VenActionRequired;
                oDiscrepancyBE.VENUserControl = VENUserControl;
                oDiscrepancyBE.CloseDiscrepancy = CloseDiscrepancy;
                oDiscrepancyBE.NextEscalationLevel = NextEscalationLevel;
                oDiscrepancyBE.VendorStatus = status;
                oDiscrepancyBAL.addWorkFlowHTMLsBAL(oDiscrepancyBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex, "Error in addWorkFlowHTML method");
            }


        }


        public void SendBOPenaltyCommunication(List<BackOrderBE> lstPenaltyInfo, string toAddress, string language, int VendorId, string CommunicationType, string VendorEmailList, string username = "")
        {
            string SelectedBOReportingIDs = string.Empty;
            if (!string.IsNullOrEmpty(toAddress))
            {
                #region Logic to Save & Send email ...
                var oBackOrderBAL = new BackOrderBAL();
                var oLanguages = oBackOrderBAL.GetLanguages();

                foreach (MAS_LanguageBE objLanguage in oLanguages)
                {
                    bool MailSentInLanguage = false;
                    if (objLanguage.LanguageID.Equals(Convert.ToInt32(language)))
                        MailSentInLanguage = true;

                   
                    var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                    path = path.Replace(@"\bin\debug", "");
                    var templateFile = string.Empty; ;
                    if (CommunicationType.ToLower() == "initial")
                    {
                        templateFile = string.Format(@"{0}emailtemplates/BOPenalty/VendorCommunication1.english.htm", path);
                    }
                    else if (CommunicationType.ToLower() == "resend")
                    {
                        templateFile = string.Format(@"{0}emailtemplates/BOPenalty/ReminderComm4a.english.htm", path);
                    }
                    else if (CommunicationType.ToLower() == "resendclosed")
                        templateFile = string.Format(@"{0}emailtemplates/BOPenalty/ReminderCommClosed.english.htm", path);
                    #region GettingPenaltyInfoByVendor

                    string PurchaseOrders = string.Empty;
                    int NoOfBO = 0;
                    decimal? PotentialCharge = 0;

                    if (lstPenaltyInfo != null && lstPenaltyInfo.Count > 0)
                    {

                        List<int?> BackOrderIds = lstPenaltyInfo.Select(x => x.BOReportingID).Distinct().ToList();
                        List<string> lstPurchaseOrders = lstPenaltyInfo.Select(x => x.PurchaseOrder.Purchase_order).Distinct().ToList();
                        
                        
                        if (BackOrderIds != null && BackOrderIds.Count > 0)
                        {
                            for (int BO = 0; BO < BackOrderIds.Count; BO++)
                            {
                                if (MailSentInLanguage)
                                {
                                    SelectedBOReportingIDs += string.Format(",{0}", BackOrderIds[BO]);
                                   
                                }
                            }
                        }

                        //concating Purchase Orders
                        if (lstPurchaseOrders != null && lstPurchaseOrders.Count > 0)
                        {
                            for (int PO = 0; PO < lstPurchaseOrders.Count; PO++)
                            {
                                PurchaseOrders += string.Format(",{0}", lstPurchaseOrders[PO]);
                            }
                        }

                        if (BackOrderIds != null && BackOrderIds.Count > 0)
                        {
                            for (int BO = 0; BO < BackOrderIds.Count; BO++)
                            {
                                List<BackOrderBE> lstVendorBOPenalty = lstPenaltyInfo.Where(x => x.BOReportingID == BackOrderIds[BO]).ToList();
                                NoOfBO += Convert.ToInt16(lstVendorBOPenalty[0].CountofBackOrder);
                                PotentialCharge += lstVendorBOPenalty[0].PotentialPenaltyCharge;

                            }
                        }


                    }

                    PurchaseOrders = PurchaseOrders.TrimStart(',');
                    var PurchaseOrderList = PurchaseOrders.Split(',');

                    #endregion

                    #region  Prepairing html body format ...
                    string htmlBody = null;

                    #region InitialComm HTML
                    if (CommunicationType.ToLower() == "initial")
                    {
                        using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                        {
                            htmlBody = sReader.ReadToEnd();
                            if (string.IsNullOrEmpty(username) || username == "")
                                htmlBody = htmlBody.Replace("{DearSirMadam}", Common.getGlobalResourceValue("DearSirMadam", objLanguage.Language));
                            else
                                htmlBody = htmlBody.Replace("{DearSirMadam}", Common.getGlobalResourceValue("Dear", objLanguage.Language) + " " + username);

                            htmlBody = htmlBody.Replace("{VendorComm1Message1}", Common.getGlobalResourceValue("VendorComm1Message1", objLanguage.Language));

                            #region Logic for creating PO detail Grid
                            StringBuilder sPoDetail = new StringBuilder();
                            sPoDetail.Append("<table width=10%'>");
                            int iCount = 0;
                            if (PurchaseOrderList != null)
                            {
                                foreach (string order in PurchaseOrderList)
                                {
                                    iCount++;
                                    if (iCount == 1)
                                        sPoDetail.Append("<tr style='font-size:12;font-family:Arial ;'>");

                                    if (iCount == 10) {
                                        iCount = 0;
                                        sPoDetail.Append("<td>" + order + "</td>");
                                        sPoDetail.Append("</tr>");
                                    }
                                    else {
                                        sPoDetail.Append("<td>" + order + "</td>");
                                    }
                                }

                                if (iCount > 0)
                                    sPoDetail.Append("</tr>");
                            }
                            sPoDetail.Append("</table>");
                            #endregion

                            htmlBody = htmlBody.Replace("{POValue}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br />" + sPoDetail + "</td></tr>");

                            string VendorComm1Message2 = Common.getGlobalResourceValue("VendorComm1Message2", objLanguage.Language);
                            VendorComm1Message2 = VendorComm1Message2.Replace("##NoofBO##", Convert.ToString(NoOfBO));
                            htmlBody = htmlBody.Replace("{VendorComm1Message2}", VendorComm1Message2);

                            htmlBody = htmlBody.Replace("{VendorComm1Message3}", Common.getGlobalResourceValue("VendorComm1Message3", objLanguage.Language));


                            string VendorComm1Message4 = Common.getGlobalResourceValue("VendorComm1Message4", objLanguage.Language);
                            VendorComm1Message4 = VendorComm1Message4.Replace("##PenaltyCharge##", Convert.ToString(PotentialCharge));
                            htmlBody = htmlBody.Replace("{VendorComm1Message4}", VendorComm1Message4);


                            htmlBody = htmlBody.Replace("{VendorComm1Message5}", Common.getGlobalResourceValue("VendorComm1Message5", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{VendorComm1Message6}", Common.getGlobalResourceValue("VendorComm1Message6", objLanguage.Language));


                            htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLinkNew + ">" + sPortalLinkNew + " </a>");


                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", Common.getGlobalResourceValue("Yoursfaithfully", objLanguage.Language));


                            htmlBody = htmlBody.Replace("{StockPlannerName}", lstPenaltyInfo[0].Vendor.StockPlannerName);

                            if (!string.IsNullOrEmpty(lstPenaltyInfo[0].Vendor.StockPlannerContact) && !string.IsNullOrWhiteSpace(lstPenaltyInfo[0].Vendor.StockPlannerContact))
                                htmlBody = htmlBody.Replace("{StockPlannerContactNo}", lstPenaltyInfo[0].Vendor.StockPlannerContact);
                            else
                                htmlBody = htmlBody.Replace("{StockPlannerContactNo}", string.Empty);


                            var apAddress = string.Empty;
                            if (!string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress1) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress2)
                                && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress3) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress4)
                                && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress5) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress6))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", lstPenaltyInfo[0].APAddress1, lstPenaltyInfo[0].APAddress2,
                                    lstPenaltyInfo[0].APAddress3, lstPenaltyInfo[0].APAddress4, lstPenaltyInfo[0].APAddress5, lstPenaltyInfo[0].APAddress6);
                            }
                            else if (!string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress1) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress2)
                                && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress3) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress4)
                                && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress5))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", lstPenaltyInfo[0].APAddress1, lstPenaltyInfo[0].APAddress2,
                                    lstPenaltyInfo[0].APAddress3, lstPenaltyInfo[0].APAddress4, lstPenaltyInfo[0].APAddress5);
                            }
                            else if (!string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress1) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress2)
                               && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress3) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress4))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", lstPenaltyInfo[0].APAddress1, lstPenaltyInfo[0].APAddress2,
                                    lstPenaltyInfo[0].APAddress3, lstPenaltyInfo[0].APAddress4);
                            }
                            else if (!string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress1) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress2)
                               && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress3))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", lstPenaltyInfo[0].APAddress1, lstPenaltyInfo[0].APAddress2,
                                    lstPenaltyInfo[0].APAddress3);
                            }
                            else if (!string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress1) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress2))
                            {
                                apAddress = string.Format("{0},&nbsp;{1}", lstPenaltyInfo[0].APAddress1, lstPenaltyInfo[0].APAddress2);
                            }
                            else
                            {
                                apAddress = lstPenaltyInfo[0].APAddress1;
                            }

                            if (!string.IsNullOrEmpty(lstPenaltyInfo[0].APPincode))
                                apAddress = string.Format("{0},&nbsp;{1}", apAddress, lstPenaltyInfo[0].APPincode);

                            htmlBody = htmlBody.Replace("{APClerkAddress}", apAddress);

                        }
                    }
                    #endregion

                    #region ResendComm HTML
                    if (CommunicationType.ToLower() == "resend")
                    {
                        using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                        {
                            htmlBody = sReader.ReadToEnd();

                            //htmlBody = htmlBody.Replace("{DearSirMadam}", Common.getGlobalResourceValue("DearSirMadam", objLanguage.Language));
                            if (string.IsNullOrEmpty(username) || username == "")
                                htmlBody = htmlBody.Replace("{DearSirMadam}", Common.getGlobalResourceValue("DearSirMadam", objLanguage.Language));
                            else
                                htmlBody = htmlBody.Replace("{DearSirMadam}", Common.getGlobalResourceValue("Dear", objLanguage.Language) + " " + username);

                            string  Initialdate = string.Format("{0: dd/MM/yyyy}",lstPenaltyInfo[0].InitialCommDate);
                           // Initialdate = Utilities.Common.GetDD_MM_YYYY(Initialdate);

                            string ReminderCommMessage1 = Common.getGlobalResourceValue("ReminderCommMessage1", objLanguage.Language);
                            ReminderCommMessage1 = ReminderCommMessage1.Replace("##CommunicationDate##",Initialdate);
                            htmlBody = htmlBody.Replace("{ReminderCommMessage1}", ReminderCommMessage1);

                            htmlBody = htmlBody.Replace("{ReminderCommMessage2}", Common.getGlobalResourceValue("ReminderCommMessage2", objLanguage.Language));

                            string ReminderCommMessage3 = Common.getGlobalResourceValue("ReminderCommMessage3", objLanguage.Language);
                            ReminderCommMessage3 = ReminderCommMessage3.Replace("##PenaltyCharge##", Convert.ToString(PotentialCharge));
                            htmlBody = htmlBody.Replace("{ReminderCommMessage3}", ReminderCommMessage3);

                            htmlBody = htmlBody.Replace("{ReminderCommMessage4}", Common.getGlobalResourceValue("ReminderCommMessage4", objLanguage.Language));

                            htmlBody = htmlBody.Replace("{ReminderCommMessage5}", Common.getGlobalResourceValue("ReminderCommMessage5", objLanguage.Language));

                            htmlBody = htmlBody.Replace("{PortalLink}", "<a href=" + sPortalLinkNew + ">" + sPortalLinkNew + " </a>");

                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", Common.getGlobalResourceValue("Yoursfaithfully", objLanguage.Language));


                            htmlBody = htmlBody.Replace("{StockPlannerName}", lstPenaltyInfo[0].Vendor.StockPlannerName);

                            if (!string.IsNullOrEmpty(lstPenaltyInfo[0].Vendor.StockPlannerContact) && !string.IsNullOrWhiteSpace(lstPenaltyInfo[0].Vendor.StockPlannerContact))
                                htmlBody = htmlBody.Replace("{StockPlannerContactNo}", lstPenaltyInfo[0].Vendor.StockPlannerContact);
                            else
                                htmlBody = htmlBody.Replace("{StockPlannerContactNo}", string.Empty);


                            var apAddress = string.Empty;
                            if (!string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress1) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress2)
                                && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress3) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress4)
                                && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress5) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress6))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", lstPenaltyInfo[0].APAddress1, lstPenaltyInfo[0].APAddress2,
                                    lstPenaltyInfo[0].APAddress3, lstPenaltyInfo[0].APAddress4, lstPenaltyInfo[0].APAddress5, lstPenaltyInfo[0].APAddress6);
                            }
                            else if (!string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress1) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress2)
                                && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress3) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress4)
                                && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress5))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", lstPenaltyInfo[0].APAddress1, lstPenaltyInfo[0].APAddress2,
                                    lstPenaltyInfo[0].APAddress3, lstPenaltyInfo[0].APAddress4, lstPenaltyInfo[0].APAddress5);
                            }
                            else if (!string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress1) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress2)
                               && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress3) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress4))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", lstPenaltyInfo[0].APAddress1, lstPenaltyInfo[0].APAddress2,
                                    lstPenaltyInfo[0].APAddress3, lstPenaltyInfo[0].APAddress4);
                            }
                            else if (!string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress1) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress2)
                               && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress3))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", lstPenaltyInfo[0].APAddress1, lstPenaltyInfo[0].APAddress2,
                                    lstPenaltyInfo[0].APAddress3);
                            }
                            else if (!string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress1) && !string.IsNullOrEmpty(lstPenaltyInfo[0].APAddress2))
                            {
                                apAddress = string.Format("{0},&nbsp;{1}", lstPenaltyInfo[0].APAddress1, lstPenaltyInfo[0].APAddress2);
                            }
                            else
                            {
                                apAddress = lstPenaltyInfo[0].APAddress1;
                            }

                            if (!string.IsNullOrEmpty(lstPenaltyInfo[0].APPincode))
                                apAddress = string.Format("{0},&nbsp;{1}", apAddress, lstPenaltyInfo[0].APPincode);

                            htmlBody = htmlBody.Replace("{APClerkAddress}", apAddress);

                        }
                    }
                    #endregion

                    #region ResendCommClosed HTML
                    if (CommunicationType.ToLower() == "resendclosed")
                    {
                        using (StreamReader streamReader = new StreamReader(templateFile.ToLower()))
                        {
                            htmlBody = streamReader.ReadToEnd();
                            htmlBody = htmlBody.Replace("{DearSirMadam}", Common.getGlobalResourceValue("DearSirMadam", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{ReminderCommClosed1}", Common.getGlobalResourceValue("ReminderCommClosed1", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{ReminderCommClosed2}", Common.getGlobalResourceValue("ReminderCommClosed2", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{ReminderCommClosed3}", Common.getGlobalResourceValue("ReminderCommClosed3", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", Common.getGlobalResourceValue("Yoursfaithfully", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{StockPlannerName}", lstPenaltyInfo[0].Vendor.StockPlannerName);
                            if (!string.IsNullOrEmpty(lstPenaltyInfo[0].Vendor.StockPlannerContact) && !string.IsNullOrWhiteSpace(lstPenaltyInfo[0].Vendor.StockPlannerContact))
                                htmlBody = htmlBody.Replace("{StockPlannerContactNo}", lstPenaltyInfo[0].Vendor.StockPlannerContact);
                            else
                                htmlBody = htmlBody.Replace("{StockPlannerContactNo}", string.Empty);
                        }
                    }
                     #endregion
                    #endregion

                    #region Sending and saving email details ...

                    var VendorComm1Subject = Common.getGlobalResourceValue("VendorComm1Subject", objLanguage.Language);
                    var ReminderCommSubject = Common.getGlobalResourceValue("ReminderCommSubject", objLanguage.Language);
                    var ReminderCommClosedSubject = Common.getGlobalResourceValue("ReminderCommSubjectClosed", objLanguage.Language);
                    var sentToWithLink = new System.Text.StringBuilder();

                    sentToWithLink.Append("<a target='_blank' href='" + Common.EncryptQuery("Penalty_ReSendCommunication.aspx?VendorId=" + Convert.ToInt32(VendorId) + "&CommTitle=QueryAgree") + "' onclick='window.open(this.href, this.target,\"scrollbars=1,left=100px,top=100px,width=1000px,height=800px\"); return false;'>" + toAddress + "</a>" + System.Environment.NewLine);

                    var oBackOrderMailBE = new BackOrderMailBE();
                    oBackOrderMailBE.Action = "AddCommunication";
                    oBackOrderMailBE.BOReportingID = 0;
                    oBackOrderMailBE.CommunicationType = CommunicationType.ToLower() == "initial" ? "VendorCommunication1" : (CommunicationType.ToLower() == "resend" ? "VendorCommunication4a" : "ReminderCommClosed");
                    oBackOrderMailBE.Subject = CommunicationType.ToLower() == "initial" ? VendorComm1Subject : (CommunicationType.ToLower() == "resend" ? ReminderCommSubject : ReminderCommClosedSubject);
                    oBackOrderMailBE.SentTo = toAddress;
                    oBackOrderMailBE.SentDate = DateTime.Now;
                    oBackOrderMailBE.Body = htmlBody;
                    oBackOrderMailBE.SendByID = 0;
                    oBackOrderMailBE.LanguageId = objLanguage.LanguageID;
                    oBackOrderMailBE.MailSentInLanguage = MailSentInLanguage;
                    oBackOrderMailBE.CommunicationStatus = CommunicationType.ToLower() =="initial" ? "InitialFromScheduler" : (CommunicationType.ToLower() == "resend" ? "ResendFromScheduler" : "ResendFromSchedulerClosed");                     
                    oBackOrderMailBE.SentToWithLink = sentToWithLink.ToString();
                    oBackOrderMailBE.VendorId = VendorId;
                    oBackOrderMailBE.SelectedBOIds = SelectedBOReportingIDs.TrimStart(',');
                    oBackOrderMailBE.VendorEmailIds = VendorEmailList;


                    BackOrderBAL oBackOrderCommBAL = new BackOrderBAL();
                    var CommId = oBackOrderCommBAL.AddCommunicationDetailBAL(oBackOrderMailBE);
                    if (MailSentInLanguage)
                    {
                        var emailToAddress = toAddress;
                        var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                        var emailToSubject = CommunicationType.ToLower() == "initial" ? VendorComm1Subject : (CommunicationType.ToLower() == "resend" ? ReminderCommSubject : ReminderCommClosedSubject);
                        var emailBody = htmlBody;
                        Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                        oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);

                        ////Updating BOReporingIds.
                        //BackOrderBAL oBackOrderBOBAL = new BackOrderBAL();
                        //BackOrderBE oBackOrderBOBE = new BackOrderBE();
                        //if (CommunicationType.ToLower() == "initial")
                        //{
                        //    oBackOrderBOBE.Action = "UpdateBOIdByVendorComm1";
                        //}
                        //else if (CommunicationType.ToLower() == "resend")
                        //{
                        //    oBackOrderBOBE.Action="UpdateBOIdByResendComm";
                        //}
                        //oBackOrderBOBE.SelectedBOReportingID = SelectedBOReportingIDs;
                        //int? Result = oBackOrderBOBAL.UpdateBOIDBAL(oBackOrderBOBE);

                    }


                    #endregion
                    
                }

                #endregion
            }
        }



    }





}

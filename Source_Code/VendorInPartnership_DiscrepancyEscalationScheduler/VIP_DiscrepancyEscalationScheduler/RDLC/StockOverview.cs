﻿namespace VIP_DiscrepancyEscalationScheduler.RDLC
{
    public class StockOverview
    {
        public string weekNum { get; set; }
        public string PendingWith { get; set; }
        public string CountofVDRNumber { get; set; }
        public decimal perAge { get; set; }
    }
}
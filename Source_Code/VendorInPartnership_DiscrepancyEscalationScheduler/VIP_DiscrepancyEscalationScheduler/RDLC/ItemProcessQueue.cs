﻿using BusinessEntities.ModuleBE.Discrepancy.Report;
using BusinessLogicLayer.ModuleBAL.Discrepancy.Report;
using Microsoft.Reporting.WinForms;
using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using Utilities;
using System.Drawing;
using Excel = Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using BusinessEntities.ModuleBE.Discrepancy;

namespace VIP_DiscrepancyEscalationScheduler
{
    internal class ItemProcessQueue
    {
        public void GenerateItemSummaryReport()
        {
            try
            {
                DiscrepancyReportBAL oDiscrepancyReportBAL = new DiscrepancyReportBAL();

                DataSet dsStockOverview;

                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                path = path.Replace(@"\bin\debug", "");

                dsStockOverview = oDiscrepancyReportBAL.getDiscrepancySchedulerReportBAL();

                oDiscrepancyReportBAL = new DiscrepancyReportBAL();

                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("\r\n");
                if (dsStockOverview == null)
                    sbMessage.Append("Record count         : null");
                else
                    sbMessage.Append("Record count          : " + dsStockOverview.Tables[0].Rows.Count.ToString());
                LogUtility.SaveTraceLogEntry(sbMessage);

                #region Report Image

                string ReportOutputPath;

                ReportDataSource rds, rds2, rds3, rds4, rds5, rds6, rds7, rds8;
                ReportViewer ReportViewerImage = new Microsoft.Reporting.WinForms.ReportViewer();
                DateTime dt;
                string sDateFrom = string.Empty;
                string sDateTo = string.Empty;
                string requestTime = string.Empty;


                #region ReportVariables

                Warning[] warnings;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = string.Empty;
                string reportFileName = string.Empty;

                #endregion ReportVariables

                CultureInfo ciCurr = CultureInfo.CurrentCulture;
                int week1 = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                int week2 = ciCurr.Calendar.GetWeekOfYear(DateTime.Now.AddDays(-7), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                int week3 = ciCurr.Calendar.GetWeekOfYear(DateTime.Now.AddDays(-14), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                int week4 = ciCurr.Calendar.GetWeekOfYear(DateTime.Now.AddDays(-21), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                int week5 = ciCurr.Calendar.GetWeekOfYear(DateTime.Now.AddDays(-28), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

                var myInClause = new string[] { week1.ToString(), week2.ToString(), week3.ToString(), week4.ToString(), week5.ToString() };

                System.Data.DataTable dtStockOverviewImage0 = (from image in dsStockOverview.Tables[0].Rows.Cast<DataRow>()
                                                               where myInClause.Contains(image.Field<string>("weekNum"))
                                                               select image
                                                               ).CopyToDataTable();

                System.Data.DataTable dtStockOverviewImage1 = (from image in dsStockOverview.Tables[1].Rows.Cast<DataRow>()
                                                               where myInClause.Contains(image.Field<string>("weekNum"))
                                                               select image).CopyToDataTable();

                System.Data.DataTable dtStockOverviewImage2 = (from image in dsStockOverview.Tables[2].Rows.Cast<DataRow>()
                                                               where myInClause.Contains(image.Field<string>("weekNum"))
                                                               select image).CopyToDataTable();

                rds = new ReportDataSource("dtSPDisAnalyis", dtStockOverviewImage0);
                rds2 = new ReportDataSource("dtSPDisSiteWise", dtStockOverviewImage1);
                rds3 = new ReportDataSource("dtSPDisWise", dtStockOverviewImage2);
                rds4 = new ReportDataSource("dtSPDisRpt", dsStockOverview.Tables[3]);

                ReportViewerImage.LocalReport.ReportPath = path + @"RDLC\StockPlannerDiscrepancyReport.rdlc";
                ReportViewerImage.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                ReportViewerImage.LocalReport.EnableHyperlinks = true;
                ReportViewerImage.LocalReport.Refresh();
                ReportViewerImage.LocalReport.DataSources.Clear();
                ReportViewerImage.LocalReport.DataSources.Add(rds);
                ReportViewerImage.LocalReport.DataSources.Add(rds2);
                ReportViewerImage.LocalReport.DataSources.Add(rds3);
                ReportViewerImage.LocalReport.DataSources.Add(rds4);

                string deviceInfo = "<DeviceInfo>" + "<OutputFormat>png</OutputFormat>" + "</DeviceInfo>";

                ReportViewerImage.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;

                byte[] bytes = ReportViewerImage.LocalReport.Render("Image", deviceInfo, out mimeType, out encoding, out extension, out streamIds, out warnings);

                ReportOutputPath = path + @"ReportOutput\";

                if (!System.IO.Directory.Exists(ReportOutputPath))
                    System.IO.Directory.CreateDirectory(ReportOutputPath);

                dt = Convert.ToDateTime(DateTime.Now);

                requestTime = "";
                if (dt.Day.ToString().Length == 1)
                    requestTime = requestTime + "0" + dt.Day.ToString();
                else
                    requestTime = requestTime + dt.Day.ToString();

                if (dt.Month.ToString().Length == 1)
                    requestTime = requestTime + "0" + dt.Month.ToString();
                else
                    requestTime = requestTime + dt.Month.ToString();

                requestTime = requestTime + dt.Year.ToString();

                requestTime = requestTime + "_" + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString();
                ReportOutputPath = ReportOutputPath + "StockPlannerDiscrepancyReport_" + requestTime + ".jpg";
                File.WriteAllBytes(ReportOutputPath, bytes);
                #endregion Report Image

                #region Excel Report
                string ReportOutputExcelPath = string.Empty;
                ReportViewer ReportViewerExcel = new Microsoft.Reporting.WinForms.ReportViewer();
                #region ReportVariables

                string reportExcelFileName = string.Empty;

                #endregion ReportVariables

                System.Data.DataTable dtStockOverviewExcel0 = dsStockOverview.Tables[0];
                System.Data.DataTable dtStockOverviewExcel1 = dsStockOverview.Tables[1];
                System.Data.DataTable dtStockOverviewExcel2 = dsStockOverview.Tables[2];

                System.Data.DataTable dtStockOverviewExcel3 = dsStockOverview.Tables[3];
                System.Data.DataTable dtStockOverviewExcel4 = dsStockOverview.Tables[4];
                System.Data.DataTable dtStockOverviewExcel5 = dsStockOverview.Tables[5];
                System.Data.DataTable dtStockOverviewExcel6 = dsStockOverview.Tables[6];
                System.Data.DataTable dtStockOverviewExcel7 = dsStockOverview.Tables[7];

                rds = new ReportDataSource("dtSPDisAnalyis", dtStockOverviewExcel0);
                rds2 = new ReportDataSource("dtSPDisSiteWise", dtStockOverviewExcel1);
                rds3 = new ReportDataSource("dtSPDisWise", dtStockOverviewExcel2);
                rds4 = new ReportDataSource("dtSPDisRpt", dtStockOverviewExcel3);
                rds5 = new ReportDataSource("dtDiscrepancyDetails", dtStockOverviewExcel4);


                ReportViewerImage = new Microsoft.Reporting.WinForms.ReportViewer();
                ReportViewerExcel.LocalReport.ReportPath = path + @"RDLC\StockPlannerDiscrepancyExcelReport.rdlc";
                ReportViewerExcel.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                ReportViewerExcel.LocalReport.EnableHyperlinks = true;
                ReportViewerExcel.LocalReport.Refresh();
                ReportViewerExcel.LocalReport.DataSources.Clear();
                ReportViewerExcel.LocalReport.DataSources.Add(rds);
                ReportViewerExcel.LocalReport.DataSources.Add(rds2);
                ReportViewerExcel.LocalReport.DataSources.Add(rds3);
                ReportViewerExcel.LocalReport.DataSources.Add(rds4);
                ReportViewerExcel.LocalReport.DataSources.Add(rds5);

                ReportViewerExcel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;

                byte[] byteExcels = ReportViewerExcel.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                ReportOutputExcelPath = path + @"ReportOutput\";

                if (!System.IO.Directory.Exists(ReportOutputExcelPath))
                    System.IO.Directory.CreateDirectory(ReportOutputExcelPath);

                dt = Convert.ToDateTime(DateTime.Now);
                requestTime = "";
                if (dt.Day.ToString().Length == 1)
                    requestTime = requestTime + "0" + dt.Day.ToString();
                else
                    requestTime = requestTime + dt.Day.ToString();

                if (dt.Month.ToString().Length == 1)
                    requestTime = requestTime + "0" + dt.Month.ToString();
                else
                    requestTime = requestTime + dt.Month.ToString();

                requestTime = requestTime + dt.Year.ToString();

                requestTime = requestTime + "_" + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString();

                ReportOutputExcelPath = ReportOutputExcelPath + "StockPlannerDiscrepancyReport_" + requestTime + ".xls";

                File.WriteAllBytes(ReportOutputExcelPath, byteExcels);

                #endregion Excel Report


                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Report Successfully Generated");
                sbMessage.Append("\r\n");
                sbMessage.Append("\r\n");
                sbMessage.Append("Sending mails to stock planner");
                sbMessage.Append("\r\n");

                if (dsStockOverview.Tables[3] != null && dsStockOverview.Tables[3].Rows.Count > 0)
                {
                    SaveMailsWithImage(oDiscrepancyReportBAL, ReportOutputPath, ReportOutputExcelPath);
                    SendMailNotification(ReportOutputExcelPath);
                }

                sbMessage.Append("\r\n");
                sbMessage.Append("Mails send Successfully");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RenameTab(string Destination)
        {
            if (File.Exists(Destination))
            {
                FileInfo fi = new FileInfo(Destination);

                var excelFile = Path.GetFullPath(Destination);
                var excel = new Excel.Application();
                var workbook = excel.Workbooks.Open(excelFile);
                var firstWorksheet = (Excel.Worksheet)workbook.Worksheets.Item[1];
                var secondWorksheet = (Excel.Worksheet)workbook.Worksheets.Item[2];
                var thirdWorksheet = (Excel.Worksheet)workbook.Worksheets.Item[3];

                firstWorksheet.Name = "SUMMARY";
                secondWorksheet.Name = "DETAILS";
                thirdWorksheet.Name = "PIVOT";

                workbook.Save();
                excel.Application.Workbooks.Close();
            }
        }

        private void SendMails(System.Data.DataTable dtDetails, string attachment)
        {
            DataView view = new DataView(dtDetails);
            System.Data.DataTable distinctValues = view.ToTable(true, "StockPlannerEmailID");
            var SelectedValues = distinctValues.AsEnumerable().Select(s => s.Field<string>("StockPlannerEmailID")).ToArray();
            string commaSeperatedValues = string.Join(",", SelectedValues).TrimStart(',').TrimEnd(',').Replace(",,", ",");

            if (!string.IsNullOrEmpty(commaSeperatedValues))
            {
                var emailToAddress = commaSeperatedValues;
                var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

                CultureInfo ciCurr = CultureInfo.CurrentCulture;

                int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

                var emailToSubject = "Inbound discrepancy tracker wk " + weekNum.ToString();
                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                path = path.Replace(@"\bin\debug", "");
                string templatesPath = path + @"emailtemplates\";
                string templateFile = templatesPath + @"Weeklydiscrepancy\weekly-discrepancy.english.htm";
                string htmlBody = string.Empty;

                if (System.IO.File.Exists(templateFile.ToLower()))
                {
                    using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                    {
                        htmlBody = sReader.ReadToEnd();
                        htmlBody = htmlBody.Replace("{week}", weekNum.ToString());
                    }
                }

                string emailBody = htmlBody;
                string attachmentPath = attachment;
                Utilities.clsEmail oclsEmail = new Utilities.clsEmail();

                oclsEmail.sendMailWithAttachment(emailToAddress, emailBody, emailToSubject, emailFromAddress, true, attachmentPath);
            }
        }

        private void SendMailNotification(string attachment)
        {
            var emailToAddress = Convert.ToString(ConfigurationManager.AppSettings["ODAdminEmailId"]);

            var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);

            CultureInfo ciCurr = CultureInfo.CurrentCulture;

            int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var emailToSubject = "Inbound discrepancy tracker wk " + weekNum.ToString() + " has been created!";

            string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
            path = path.Replace(@"\bin\debug", "");
            string templatesPath = path + @"emailtemplates\";
            string templateFile = templatesPath + @"Weeklydiscrepancy\weekly-discrepancy-Notification.english.htm";
            string htmlBody = string.Empty;

            string Portallink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]);

            if (System.IO.File.Exists(templateFile.ToLower()))
            {
                using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                {
                    htmlBody = sReader.ReadToEnd();
                    htmlBody = htmlBody.Replace("{week}", weekNum.ToString());
                    htmlBody = htmlBody.Replace("{url}", Portallink.ToString());
                }
            }

            string emailBody = htmlBody;
            Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
            oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true, attachment);
        }

        public void SendMailsWithImage(List<DiscrepancyEmailTracker> EmailAddress)
        {
            DiscrepancyReportBAL oDiscrepancyReportBAL = new DiscrepancyReportBAL();
            DataSet dsStockOverview = oDiscrepancyReportBAL.getDiscrepancySchedulerReportBAL();
            DataView view = new DataView(dsStockOverview.Tables[3]);
            System.Data.DataTable distinctValues = view.ToTable(true, "StockPlannerEmailID");
            var SelectedValues = distinctValues.AsEnumerable().Select(s => s.Field<string>("StockPlannerEmailID")).ToArray();
            string commaSeperatedValues = string.Join(",", SelectedValues).TrimStart(',').TrimEnd(',').Replace(",,", ",");

            string toEmails = string.Join(",", EmailAddress.Where(x => x.EmailType == "TO").Select(x => x.EmailID).ToArray());
            string ccEmails = string.Join(",", EmailAddress.Where(x => x.EmailType == "CC").Select(x => x.EmailID).ToArray());
            string bccEmails = string.Join(",", EmailAddress.Where(x => x.EmailType == "BCC").Select(x => x.EmailID).ToArray());

            if (!string.IsNullOrEmpty(commaSeperatedValues))
            {
                var emailToAddress = commaSeperatedValues;
                var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                CultureInfo ciCurr = CultureInfo.CurrentCulture;
                int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                path = path.Replace(@"\bin\debug", "");
                string templatesPath = path + @"emailtemplates\";
                string templateFile = templatesPath + @"Weeklydiscrepancy\weekly-discrepancy-db.english.htm";
                string htmlBody = string.Empty;

                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Clear();
                sbMessage.Append("Fetch data from DiscrepancyNotification  : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");


                DiscrepancyNotification discrepancyNotification = new DiscrepancyNotification
                {
                    Week = weekNum,
                    Year = DateTime.Now.Year,
                    Action = "GetDiscrepancyTrackerCommunicationByWeekYear"
                };

                DiscrepancyNotification notification = oDiscrepancyReportBAL.getDiscrepancyNotificationForSendingMailBAL(discrepancyNotification);

                if (System.IO.File.Exists(templateFile.ToLower()))
                {
                    using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                    {
                        htmlBody = sReader.ReadToEnd();
                        htmlBody = htmlBody.Replace("{body}", notification.EmailContent.ToString());
                        htmlBody = htmlBody.Replace("{path}", "cid:logo");
                    }
                }

                sbMessage.Clear();
                sbMessage.Append("Data took from DiscrepancyNotification  : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");


                string subject = notification.EmailSubject;
                string Imagepath = notification.ImagePath;
                string excelAttachment = notification.ExcelPath;

                System.Net.Mail.LinkedResource imageResourceEs = new System.Net.Mail.LinkedResource(Imagepath, MediaTypeNames.Image.Jpeg);
                imageResourceEs.ContentId = "logo";
                imageResourceEs.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;

                string emailBody = htmlBody;

                try
                {

                    sbMessage.Clear();
                    sbMessage.Append("Mail is going to send  : " + DateTime.Now.ToString());
                    sbMessage.Append("\r\n");
                    sbMessage.Append("****************************************************************************************");
                    sbMessage.Append("\r\n");

                    Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                    oclsEmail.SendMail(emailToAddress, emailBody, subject, emailFromAddress, true, excelAttachment, imageResourceEs, toEmails, ccEmails, bccEmails);

                    sbMessage.Clear();
                    sbMessage.Append("Mail sent  : " + DateTime.Now.ToString());
                    sbMessage.Append("\r\n");
                    sbMessage.Append("****************************************************************************************");
                    sbMessage.Append("\r\n");

                    LogUtility.SaveTraceLogEntry(sbMessage); ;
                }
                catch (Exception ex)
                {
                    LogUtility.SaveErrorLogEntry(ex, "Mail not sent"); ;
                }

                discrepancyNotification.DiscrepancyTrackerId = notification.DiscrepancyTrackerId;
                discrepancyNotification.Action = "UpdateDiscrepancyTrackerCommunicationMailSent";
                var discrepancy = oDiscrepancyReportBAL.UpdateDiscrepancySchedulerNotificationBAL(discrepancyNotification);
            }
        }

        private void SaveMailsWithImage(DiscrepancyReportBAL oDiscrepancyReportBAL, string Imagepath, string excelAttachment)
        {
            try
            {
                CultureInfo ciCurr = CultureInfo.CurrentCulture;
                int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

                var emailToSubject = "Inbound discrepancy tracker wk " + weekNum.ToString();
                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                path = path.Replace(@"\bin\debug", "");
                string templatesPath = path + @"emailtemplates\";
                string templateFile = templatesPath + @"Weeklydiscrepancy\weekly-discrepancy.english.htm";
                string htmlBody = string.Empty;

                if (System.IO.File.Exists(templateFile.ToLower()))
                {
                    using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                    {
                        htmlBody = sReader.ReadToEnd();
                        htmlBody = htmlBody.Replace("{week}", weekNum.ToString());
                    }
                }

                string emailBody = htmlBody;

                DiscrepancyNotification discrepancyNotification = new DiscrepancyNotification
                {
                    Action = "AddDiscrepancyTrackerCommunication",
                    Week = weekNum,
                    Year = DateTime.Now.Year,
                    EmailSubject = emailToSubject,
                    MailSent = false,
                    AddedBy = 1,
                    EditedBy = 1,
                    EmailContent = emailBody,
                    ExcelPath = excelAttachment,
                    ImagePath = Imagepath,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    StockPlanersEmailIds = string.Empty,
                    DiscrepancyTrackerId = 0
                };

                DataSet dataSet = oDiscrepancyReportBAL.getDiscrepancySchedulerNotificationBAL(discrepancyNotification);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using System.Text;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using Utilities;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.Languages;
using BusinessLogicLayer.ModuleBAL.Languages;
using System.Reflection;
using BusinessLogicLayer.ModuleBAL.Discrepancy.Report;
using BusinessEntities.ModuleBE.Discrepancy;

namespace VIP_DiscrepancyEscalationScheduler
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            StringBuilder sbMessage = new StringBuilder();
            sbMessage.Append("Started at            : " + DateTime.Now.ToString());
            sbMessage.Append("\r\n");
            sbMessage.Append("****************************************************************************************");
            sbMessage.Append("\r\n");
            LogUtility.SaveTraceLogEntry(sbMessage);

            //dicrepancy goodsin open scheduler
            DiscrepancyCountStatusScheduler.RunSchedulerForDiscrepancyCount();

            sendCommunication oSendCommunication = new sendCommunication();
            int daysPast, daysBeforeEscalationType2, daysBeforeFinalEscalation, DiscrepancyLogID = 0, languageID;

            string IsDiscrepancyEscal = System.Configuration.ConfigurationManager.AppSettings["IsDiscrepancyEscal"];

            if (IsDiscrepancyEscal == "true")
            {
                #region DiscrepancyCommunication
                string language, goodsKeptOrReturn, sentTo = string.Empty, currentCommunicationLevel, sEscalationType2SentDate, sNextCommunicationLevel;
                try
                {
                    int? retValue = null;
                    int APP_ToleranceDueDay = 0;

                    DiscrepancyMailBE oDiscrepancyMailBE = new DiscrepancyMailBE();
                    DiscrepancyBAL oDiscrepancyBAL = new DiscrepancyBAL();
                    oDiscrepancyMailBE.Action = "GetDiscrepanySendCommunication";
                    DataSet dsSendCommunication = oDiscrepancyBAL.GetCommunicationDataToSendMailBAL(oDiscrepancyMailBE);

                    sbMessage.Clear();
                    sbMessage.Append("\r\n");
                    if (dsSendCommunication == null)
                        sbMessage.Append("Record count       : null");
                    else if (dsSendCommunication.Tables[0].Rows.Count > 0)
                        sbMessage.Append("EscalationType 2,3,5 Record count        : " + dsSendCommunication.Tables[0].Rows.Count.ToString());
                    else if (dsSendCommunication.Tables[1].Rows.Count > 0)
                        sbMessage.Append("EscalationType 4 Record count        : " + dsSendCommunication.Tables[1].Rows.Count.ToString());
                    LogUtility.SaveTraceLogEntry(sbMessage);

                    DateTime dtDiscrepancyLogDate;
                    DateTime? dtCollectionDate = null;
                    string sSendMailLinkAll = "";
                    bool doWtite = false;

                    bool closedDicrepancy = false;
                    DiscrepancyLogID = 0;
                    sNextCommunicationLevel = string.Empty;

                    #region Send EscalationType 2,3,5 Mail
                    if (dsSendCommunication != null && dsSendCommunication.Tables.Count > 0 && dsSendCommunication.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dsSendCommunication.Tables[0].Rows.Count; i++)
                        {
                            string discrepanyType = Convert.ToString(dsSendCommunication.Tables[0].Rows[i]["DiscrepancyType"]).Replace(".", "").Replace("-", "").Replace(" ", "").ToLower();

                            if (dsSendCommunication.Tables[0].Rows[i]["SentTo"] != null && !string.IsNullOrEmpty(Convert.ToString(dsSendCommunication.Tables[0].Rows[i]["SentTo"])))
                            {

                                daysPast = Convert.ToInt32(dsSendCommunication.Tables[0].Rows[i]["daysPast"]);
                                if (dsSendCommunication.Tables[0].Rows[i]["DiscrepancyLogDate"] is DateTime)
                                    dtDiscrepancyLogDate = Convert.ToDateTime(dsSendCommunication.Tables[0].Rows[i]["DiscrepancyLogDate"]);

                                daysBeforeEscalationType2 = Convert.ToInt32(dsSendCommunication.Tables[0].Rows[i]["DIS_DaysBeforeEscalationType2"]);
                                daysBeforeFinalEscalation = Convert.ToInt32(dsSendCommunication.Tables[0].Rows[i]["DIS_DaysBeforeFinalEscalation"]);
                                DiscrepancyLogID = Convert.ToInt32(dsSendCommunication.Tables[0].Rows[i]["DiscrepancyLogID"]);
                                languageID = Convert.ToInt32(dsSendCommunication.Tables[0].Rows[i]["LanguageID"]);
                                language = Convert.ToString(dsSendCommunication.Tables[0].Rows[i]["language"]);
                                goodsKeptOrReturn = Convert.ToString(dsSendCommunication.Tables[0].Rows[i]["VenCommunicationType"]);
                                sentTo = Convert.ToString(dsSendCommunication.Tables[0].Rows[i]["SentTo"]);
                                currentCommunicationLevel = Convert.ToString(dsSendCommunication.Tables[0].Rows[i]["CommunicationLevel"]);
                                sEscalationType2SentDate = sNextCommunicationLevel = string.Empty;
                                closedDicrepancy = false;

                                // Status : communication -->  escalationtype2 
                                if (currentCommunicationLevel.ToLower().Contains("communication") && daysPast > daysBeforeEscalationType2)
                                {
                                    // if (currentCommunicationLevel.ToLower().Contains("communication")) {

                                    sNextCommunicationLevel = "EscalationType2";
                                    closedDicrepancy = false;
                                }
                                // Status : escalationtype2 -->  escalationtype3 or escalationtype5 
                                else if (currentCommunicationLevel.ToLower().Contains("escalationtype2") && daysPast > daysBeforeFinalEscalation)
                                {
                                    // else if (currentCommunicationLevel.ToLower().Contains("escalationtype2")) {

                                    if (goodsKeptOrReturn.ToLower() == "kept")
                                    {
                                        // Case of Goods Kept
                                        sNextCommunicationLevel = "EscalationType5";
                                        closedDicrepancy = true;
                                    }
                                    else if (goodsKeptOrReturn.ToLower() == "return")
                                    {
                                        // Case of Goods Return
                                        sNextCommunicationLevel = "EscalationType3";
                                        closedDicrepancy = false;
                                    }
                                    else if (discrepanyType == "invoicequerydiscrepancy")
                                    {

                                        sNextCommunicationLevel = "EscalationType3";
                                        closedDicrepancy = false;
                                    }
                                    else
                                    {
                                        sNextCommunicationLevel = "EscalationType5";
                                        closedDicrepancy = true;
                                    }
                                    sEscalationType2SentDate = Utilities.Common.ToDateTimeInMMDDYYYY(dsSendCommunication.Tables[0].Rows[i]["sentdate"]);
                                }

                                doWtite = false;
                                if ((i + 1 != dsSendCommunication.Tables[0].Rows.Count) && (DiscrepancyLogID != Convert.ToInt32(dsSendCommunication.Tables[0].Rows[i + 1]["DiscrepancyLogID"])))
                                    doWtite = true;

                                if (i + 1 == dsSendCommunication.Tables[0].Rows.Count)
                                    doWtite = true;

                                bool mailSentInLanguage = false;
                                if (Convert.ToInt32(dsSendCommunication.Tables[0].Rows[i]["MailSentInLanguage"]) > 0)
                                    mailSentInLanguage = true;

                                retValue = oSendCommunication.sendCommunicationByEMail(DiscrepancyLogID, discrepanyType, sentTo, languageID, language, currentCommunicationLevel, sNextCommunicationLevel, sEscalationType2SentDate, closedDicrepancy, dtCollectionDate, ref sSendMailLinkAll, doWtite, mailSentInLanguage);
                            }
                            sbMessage.Clear();
                            sbMessage.Append("\r\n");
                            sbMessage.Append("For DiscrepancyId: " + DiscrepancyLogID + " Email Id: " + sentTo + " Next communication set to: " + sNextCommunicationLevel);
                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************");
                            sbMessage.Append("\r\n");
                            LogUtility.SaveTraceLogEntry(sbMessage);
                        }
                    }
                    #endregion



                    #region Send EscalationType Invoice Query Email
                    if (dsSendCommunication != null && dsSendCommunication.Tables.Count > 0 && dsSendCommunication.Tables[2].Rows.Count > 0)
                    {
                        for (int i = 0; i < dsSendCommunication.Tables[2].Rows.Count; i++)
                        {
                            string discrepanyType = Convert.ToString(dsSendCommunication.Tables[2].Rows[i]["DiscrepancyType"]).Replace(".", "").Replace("-", "").Replace(" ", "").ToLower();

                            if (dsSendCommunication.Tables[2].Rows[i]["SentTo"] != null && !string.IsNullOrEmpty(Convert.ToString(dsSendCommunication.Tables[2].Rows[i]["SentTo"])))
                            {

                                daysPast = Convert.ToInt32(dsSendCommunication.Tables[2].Rows[i]["daysPast"]);
                                if (dsSendCommunication.Tables[2].Rows[i]["DiscrepancyLogDate"] is DateTime)
                                    dtDiscrepancyLogDate = Convert.ToDateTime(dsSendCommunication.Tables[2].Rows[i]["DiscrepancyLogDate"]);

                                daysBeforeEscalationType2 = Convert.ToInt32(dsSendCommunication.Tables[2].Rows[i]["DIS_DaysBeforeEscalationType2"]);
                                daysBeforeFinalEscalation = Convert.ToInt32(dsSendCommunication.Tables[2].Rows[i]["DIS_DaysBeforeFinalEscalation"]);
                                DiscrepancyLogID = Convert.ToInt32(dsSendCommunication.Tables[2].Rows[i]["DiscrepancyLogID"]);
                                languageID = Convert.ToInt32(dsSendCommunication.Tables[2].Rows[i]["LanguageID"]);
                                language = Convert.ToString(dsSendCommunication.Tables[2].Rows[i]["language"]);
                                goodsKeptOrReturn = Convert.ToString(dsSendCommunication.Tables[2].Rows[i]["VenCommunicationType"]);
                                sentTo = Convert.ToString(dsSendCommunication.Tables[2].Rows[i]["SentTo"]);
                                currentCommunicationLevel = Convert.ToString(dsSendCommunication.Tables[2].Rows[i]["CommunicationLevel"]);
                                sEscalationType2SentDate = sNextCommunicationLevel = string.Empty;
                                closedDicrepancy = false;

                                // Status : communication -->  escalationtype2 
                                if (currentCommunicationLevel.ToLower().Contains("communication") && daysPast > daysBeforeEscalationType2)
                                {
                                    // if (currentCommunicationLevel.ToLower().Contains("communication")) {

                                    sNextCommunicationLevel = "EscalationType4";
                                    closedDicrepancy = false;
                                }
                                // Status : escalationtype2 -->  escalationtype3 or escalationtype5 
                                else if (currentCommunicationLevel.ToLower().Contains("escalationtype4") && daysPast > daysBeforeFinalEscalation)
                                {

                                    sNextCommunicationLevel = "EscalationType5";
                                    closedDicrepancy = false;
                                    sEscalationType2SentDate = Utilities.Common.ToDateTimeInMMDDYYYY(dsSendCommunication.Tables[2].Rows[i]["sentdate"]);
                                }

                                doWtite = false;
                                if ((i + 1 != dsSendCommunication.Tables[2].Rows.Count) && (DiscrepancyLogID != Convert.ToInt32(dsSendCommunication.Tables[2].Rows[i + 1]["DiscrepancyLogID"])))
                                    doWtite = true;

                                if (i + 1 == dsSendCommunication.Tables[2].Rows.Count)
                                    doWtite = true;

                                bool mailSentInLanguage = false;
                                if (Convert.ToInt32(dsSendCommunication.Tables[2].Rows[i]["MailSentInLanguage"]) > 0)
                                    mailSentInLanguage = true;

                                retValue = oSendCommunication.sendCommunicationByEMail(DiscrepancyLogID, discrepanyType, sentTo, languageID, language, currentCommunicationLevel, sNextCommunicationLevel, sEscalationType2SentDate, closedDicrepancy, dtCollectionDate, ref sSendMailLinkAll, doWtite, mailSentInLanguage);
                            }
                            sbMessage.Clear();
                            sbMessage.Append("\r\n");
                            sbMessage.Append("For DiscrepancyId: " + DiscrepancyLogID + " Email Id: " + sentTo + " Next communication set to: " + sNextCommunicationLevel);
                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************");
                            sbMessage.Append("\r\n");
                            LogUtility.SaveTraceLogEntry(sbMessage);
                        }
                    }
                    #endregion


                    sSendMailLinkAll = "";
                    doWtite = false;

                    #region Send EscalationType 4 Mail
                    APP_ToleranceDueDay = 0;
                    // Status : escalationtype4 -->  escalationtype4
                    if (dsSendCommunication != null && dsSendCommunication.Tables.Count > 1 && dsSendCommunication.Tables[1].Rows.Count > 0)
                    {
                        for (int i = 0; i < dsSendCommunication.Tables[1].Rows.Count; i++)
                        {
                            string discrepanyType = Convert.ToString(dsSendCommunication.Tables[1].Rows[i]["DiscrepancyType"]).Replace(".", "").Replace("-", "").Replace(" ", "").ToLower();

                            if (dsSendCommunication.Tables[1].Rows[i]["SentTo"] != null && !string.IsNullOrEmpty(Convert.ToString(dsSendCommunication.Tables[1].Rows[i]["SentTo"])))
                            {
                                daysPast = Convert.ToInt32(dsSendCommunication.Tables[1].Rows[i]["daysPast"]);
                                if (dsSendCommunication.Tables[1].Rows[i]["DiscrepancyLogDate"] is DateTime)
                                    dtDiscrepancyLogDate = Convert.ToDateTime(dsSendCommunication.Tables[1].Rows[i]["DiscrepancyLogDate"]);

                                if (dsSendCommunication.Tables[1].Rows[i]["APP_ToleranceDueDay"] is int)
                                    APP_ToleranceDueDay = Convert.ToInt32(dsSendCommunication.Tables[1].Rows[i]["APP_ToleranceDueDay"]);

                                daysBeforeEscalationType2 = Convert.ToInt32(dsSendCommunication.Tables[1].Rows[i]["DIS_DaysBeforeEscalationType2"]);
                                daysBeforeFinalEscalation = Convert.ToInt32(dsSendCommunication.Tables[1].Rows[i]["DIS_DaysBeforeFinalEscalation"]);
                                DiscrepancyLogID = Convert.ToInt32(dsSendCommunication.Tables[1].Rows[i]["DiscrepancyLogID"]);
                                languageID = Convert.ToInt32(dsSendCommunication.Tables[1].Rows[i]["LanguageID"]);
                                language = Convert.ToString(dsSendCommunication.Tables[1].Rows[i]["language"]);
                                goodsKeptOrReturn = Convert.ToString(dsSendCommunication.Tables[1].Rows[i]["VenCommunicationType"]);
                                sentTo = Convert.ToString(dsSendCommunication.Tables[1].Rows[i]["SentTo"]);
                                currentCommunicationLevel = Convert.ToString(dsSendCommunication.Tables[1].Rows[i]["CommunicationLevel"]);
                                if (dsSendCommunication.Tables[1].Rows[i]["CollectionDate"] is DateTime)
                                    dtCollectionDate = Convert.ToDateTime(dsSendCommunication.Tables[1].Rows[i]["CollectionDate"]);

                                sEscalationType2SentDate = sNextCommunicationLevel = string.Empty;
                                closedDicrepancy = false;

                                doWtite = false;
                                if ((i + 1 != dsSendCommunication.Tables[1].Rows.Count) && (DiscrepancyLogID != Convert.ToInt32(dsSendCommunication.Tables[1].Rows[i + 1]["DiscrepancyLogID"])))
                                    doWtite = true;

                                if (i + 1 == dsSendCommunication.Tables[1].Rows.Count)
                                    doWtite = true;

                                if (daysPast > APP_ToleranceDueDay)
                                {
                                    sNextCommunicationLevel = "EscalationType4";

                                    bool mailSentInLanguage = false;
                                    if (Convert.ToInt32(dsSendCommunication.Tables[0].Rows[i]["MailSentInLanguage"]) > 0)
                                        mailSentInLanguage = true;

                                   // retValue = oSendCommunication.sendCommunicationByEMail(DiscrepancyLogID, discrepanyType, sentTo, languageID, language, currentCommunicationLevel, sNextCommunicationLevel, sEscalationType2SentDate, false, dtCollectionDate, ref sSendMailLinkAll, doWtite, mailSentInLanguage);

                                }
                            }
                            sbMessage.Clear();
                            sbMessage.Append("\r\n");
                            sbMessage.Append("For DiscrepancyId: " + DiscrepancyLogID + " Email Id: " + sentTo + " Next communication set to: " + sNextCommunicationLevel);
                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************");
                            sbMessage.Append("\r\n");
                            LogUtility.SaveTraceLogEntry(sbMessage);
                        }

                    }
                    #endregion

                    if (dsSendCommunication != null && dsSendCommunication.Tables.Count > 1 && dsSendCommunication.Tables[0].Rows.Count <= 0 && dsSendCommunication.Tables[1].Rows.Count <= 0)
                    {
                        sbMessage.Clear();
                        sbMessage.Append("No Record Found for communication");
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                    }
                }
                catch (Exception ex)
                {
                    oSendCommunication.errorLog(DiscrepancyLogID, "", sentTo, "", ex.Message);
                    LogUtility.SaveErrorLogEntry(ex, DiscrepancyLogID.ToString());
                }
                #endregion

                #region EsclationForVendorTocolect

                DiscrepancyMailBE oDiscrepancyBE = new DiscrepancyMailBE();
                DiscrepancyBAL DiscrepancyBAL = new DiscrepancyBAL();
                oDiscrepancyBE.Action = "GetDiscrepancyGoodsinTobeColleted";
                DataSet dsDiscrepancy = DiscrepancyBAL.GetDiscrepancyForVendorToCollectEscBAL(oDiscrepancyBE);
                if (dsDiscrepancy.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt = dsDiscrepancy.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        string Action = "InsertHTML";
                        DateTime LoggedDateTime = DateTime.Now;
                        int LevelNumber = 1;

                        WorkflowHTML oWorkflowHTML = new WorkflowHTML();

                        string INVHTML = oWorkflowHTML.function3("White", null, "", "");
                        bool INVActionRequired = false;
                        string INVUserControl = null;

                        string APHTML = oWorkflowHTML.function3("White", null, "", "");
                        bool APActionRequired = false;
                        bool CloseDiscrepancy = false;
                        string APUserControl = null;
                        string sNextEscalationLevel = "VendorToCollect";
                        string GINHTML, GINUserControl, VENHTML, VENUserControl;
                        GINHTML = GINUserControl = VENHTML = VENUserControl = null;
                        bool GINActionRequired, VenActionRequired = false;
                        GINActionRequired = VenActionRequired = false;
                        GINHTML = oWorkflowHTML.function4("Blue", "", null, "Goods In", "", "", @"Vendor has not responded to requests to arrange collection. Please contact vendor directly to request what we should do with the goods", "", "", DateTime.Now, "");
                        GINActionRequired = true;
                        GINUserControl = "GIN003";
                        oSendCommunication.addWorkFlowHTML(Action, Convert.ToInt32(dr["DiscrepancyLogID"]), LoggedDateTime, LevelNumber, GINHTML, GINActionRequired, GINUserControl, INVHTML, INVActionRequired, INVUserControl, APHTML, APActionRequired, APUserControl, VENHTML, VenActionRequired, VENUserControl, CloseDiscrepancy, sNextEscalationLevel, Convert.ToString(dr["WorkflowActionID"]));
                    }
                }
                #endregion
            }


            #region Check BO Penalty Communication
            try
            {
                string BOInitialCommDate = System.Configuration.ConfigurationManager.AppSettings["BOInitialCommDate"];
                string BOResendCommDate = System.Configuration.ConfigurationManager.AppSettings["BOResendCommDate"];
                string BOResendCommDateClosed = System.Configuration.ConfigurationManager.AppSettings["BOResendCommDateClosed"];
                string today = DateTime.Now.ToString("dd");
                string IsDevServer = System.Configuration.ConfigurationManager.AppSettings["IsDevServer"];


                //For testing on DEv only -- below line should not be commented on PROD server
                if (today == BOInitialCommDate || IsDevServer == "true")
                {
                    SendBOPenaltyInitailComm();
                }

                if (today == BOResendCommDate)
                {
                    SendBOPenaltyResendComm();
                }

                if (today == BOResendCommDateClosed)
                {
                    SendBOPenaltyResendCommClosed();
                }
            }
            catch (Exception ex)
            {
                sbMessage.Clear();
                sbMessage.Append("Send BO Penalty communication failed at        : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
                LogUtility.SaveErrorLogEntry(ex);
            }
            #endregion


            #region Generate & send stock planner Discrepancy report
            try
            {
                string SPDiscrepancyReportWeekday = System.Configuration.ConfigurationManager.AppSettings["SPDiscrepancyReportWeekday"];
                string todaydayName = System.DateTime.Now.DayOfWeek.ToString();

                if (SPDiscrepancyReportWeekday.ToLower() == todaydayName.ToLower())
                {
                    ItemProcessQueue reportProcess = new ItemProcessQueue();
                    reportProcess.GenerateItemSummaryReport();
                }


                string SPDiscrepancyReportSentDay = System.Configuration.ConfigurationManager.AppSettings["SPDiscrepancyReportSentDay"];
                string todayName = System.DateTime.Now.DayOfWeek.ToString();

                string ccEmailAddress = System.Configuration.ConfigurationManager.AppSettings["ccEmailAddress"];

                //append emailids in this ccEmailAddress for sending mails..

                var discrepancyEmailTracker = new DiscrepancyEmailTracker();
                discrepancyEmailTracker.Action = "ALL";
                List<DiscrepancyEmailTracker> lstEmailIds = new DiscrepancyReportBAL().GetALLDiscrepancyTrackerEmailBAL(discrepancyEmailTracker);


                List<DiscrepancyEmailTracker> lstEmailId = new List<DiscrepancyEmailTracker>();
                var ccEmails=ccEmailAddress.Split(',');

                foreach (var item in ccEmails)
                {
                    lstEmailId.Add(new DiscrepancyEmailTracker { EmailID = item, EmailType = "CC", IsActive = true });
                }

                lstEmailId.AddRange(lstEmailIds);

                if (SPDiscrepancyReportSentDay.ToLower() == todayName.ToLower())
                {
                    ItemProcessQueue reportProcess = new ItemProcessQueue();
                    reportProcess.SendMailsWithImage(lstEmailId);
                }

            }
            catch (Exception ex)
            {
                sbMessage.Clear();
                sbMessage.Append("Generate & send stock planner Discrepancy report failed at        : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
                LogUtility.SaveErrorLogEntry(ex);
            }


            #endregion

            sbMessage.Clear();
            sbMessage.Append("Ended at              : " + DateTime.Now.ToString());
            sbMessage.Append("\r\n");
            sbMessage.Append("****************************************************************************************");
            sbMessage.Append("\r\n");
            LogUtility.SaveTraceLogEntry(sbMessage);
        }
        private static void SendBOPenaltyInitailComm()
        {
            #region Send BO Penalty Communication 1
            int VendorID = 0;
            try
            {

                StringBuilder sbMessage = new StringBuilder();
                sendCommunication oSendCommunication = new sendCommunication();

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append(" BO Penalty Initail Communication Started at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);


                string IsBOPenaltyEscal = System.Configuration.ConfigurationManager.AppSettings["IsBOPenaltyEscal"];

                BackOrderBAL oBackOrderVCBAL = new BackOrderBAL();
                BackOrderBE oBackOrderVCBE = new BackOrderBE();
                oBackOrderVCBE.Action = "GetVendorComm1";

                List<BackOrderBE> lstVendorCommList = oBackOrderVCBAL.GetVendorComm1BAL(oBackOrderVCBE);
                // find distinct vendorid
                List<int> vendorIDs = lstVendorCommList.Select(x => x.Vendor.VendorID).Distinct().ToList();

                if (vendorIDs != null && vendorIDs.Count > 0)
                {
                    for (int i = 0; i < vendorIDs.Count; i++)
                    {

                        VendorID = vendorIDs[i];
                        //  BackOrderBAL oBackOrderVSBAL = new BackOrderBAL();
                        //   BackOrderBE oBackOrderVSBE = new BackOrderBE();
                        //  oBackOrderVSBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();


                        //  oBackOrderVSBE.Action = "CheckSkuStatusByVendor";
                        //   oBackOrderVSBE.Vendor.VendorID = VendorID;
                        //   bool IsAllVendorProcessed = oBackOrderVSBAL.CheckSkuStatusByVendorBAL(oBackOrderVSBE);

                        //if (IsAllVendorProcessed)
                        //{
                        List<BackOrderBE> lstVendorCommListByVendor = lstVendorCommList.Where(x => x.Vendor.VendorID == VendorID).ToList();

                        BackOrderBAL oBackOrderVUBAL = new BackOrderBAL();
                        BackOrderBE oBackOrderVUBE = new BackOrderBE();
                        oBackOrderVUBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oBackOrderVUBE.Vendor.VendorID = VendorID;

                        string[] VendorAddress;
                        string[] Vendorlanguage;
                        string[] UserName;


                        string[] VendorData = new string[2];
                        VendorData = oSendCommunication.GetVendorOTIFContactWithLanguage(oBackOrderVUBE.Vendor.VendorID);

                        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                        {

                            VendorAddress = VendorData[0].Split(',');
                            Vendorlanguage = VendorData[1].Split(',');
                            UserName = VendorData[2].Split(',');

                            for (int j = 0; j < VendorAddress.Length; j++)
                            {
                                if (VendorAddress[j] != " ")
                                {
                                    if (IsBOPenaltyEscal == "true")
                                        oSendCommunication.SendBOPenaltyCommunication(lstVendorCommListByVendor, VendorAddress[j], Vendorlanguage[j], VendorID, "initial", VendorData[0], UserName[j]);
                                }
                            }
                            //Updating IsVendorCommunicationSent Flag for Selected BOReportedIds
                            string SelectedBOReportingIDs = string.Empty;
                            List<int?> BackOrderIds = lstVendorCommListByVendor.Select(x => x.BOReportingID).Distinct().ToList();
                            if (BackOrderIds != null && BackOrderIds.Count > 0)
                            {
                                for (int BO = 0; BO < BackOrderIds.Count; BO++)
                                {
                                    SelectedBOReportingIDs += string.Format(",{0}", BackOrderIds[BO]);
                                }
                            }

                            BackOrderBAL oBackOrderBOBAL = new BackOrderBAL();
                            BackOrderBE oBackOrderBOBE = new BackOrderBE();

                            oBackOrderBOBE.Action = "UpdateBOIdByVendorComm1";
                            oBackOrderBOBE.SelectedBOReportingID = SelectedBOReportingIDs;
                            int? Result = oBackOrderBOBAL.UpdateBOIDBAL(oBackOrderBOBE);
                        }


                    }
                    //}

                }
                else
                {
                    sbMessage.Clear();
                    sbMessage.Append("No Record Found for BO Vendor Communication1");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                }

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append(" BO Penalty Communication Ended at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex, VendorID.ToString());
            }
            #endregion
        }
        private static void SendBOPenaltyResendComm()
        {
            #region Send BO Penalty Communication 1
            int VendorID = 0;
            try
            {

                StringBuilder sbMessage = new StringBuilder();
                sendCommunication oSendCommunication = new sendCommunication();

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append(" BO Penalty Resend Communication Started at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
                string IsBOPenaltyEscal = System.Configuration.ConfigurationManager.AppSettings["IsBOPenaltyEscal"];
                BackOrderBAL oBackOrderVCBAL = new BackOrderBAL();
                BackOrderBE oBackOrderVCBE = new BackOrderBE();
                oBackOrderVCBE.Action = "GetResendVendorComm1";

                List<BackOrderBE> lstVendorCommList = oBackOrderVCBAL.GetVendorComm1BAL(oBackOrderVCBE);
                // find distinct vendorid
                List<int> vendorIDs = lstVendorCommList.Select(x => x.Vendor.VendorID).Distinct().ToList();

                if (vendorIDs != null && vendorIDs.Count > 0)
                {
                    for (int i = 0; i < vendorIDs.Count; i++)
                    {

                        VendorID = vendorIDs[i];
                        BackOrderBAL oBackOrderVSBAL = new BackOrderBAL();
                        BackOrderBE oBackOrderVSBE = new BackOrderBE();
                        oBackOrderVSBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                        List<BackOrderBE> lstVendorCommListByVendor = lstVendorCommList.Where(x => x.Vendor.VendorID == VendorID).ToList();

                        BackOrderBAL oBackOrderVUBAL = new BackOrderBAL();
                        BackOrderBE oBackOrderVUBE = new BackOrderBE();
                        oBackOrderVUBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oBackOrderVUBE.Vendor.VendorID = VendorID;

                        string[] VendorAddress;
                        string[] Vendorlanguage;
                        string[] VendorData = new string[2];


                        VendorData = oSendCommunication.GetVendorInitailEmailWithLanguage(oBackOrderVUBE.Vendor.VendorID, Convert.ToDateTime(lstVendorCommListByVendor[0].InitialCommDate));

                        // VendorData = oSendCommunication.GetVendorOTIFContactWithLanguage(oBackOrderVUBE.Vendor.VendorID);

                        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                        {

                            VendorAddress = VendorData[0].Split(',');
                            Vendorlanguage = VendorData[1].Split(',');

                            for (int j = 0; j < VendorAddress.Length; j++)
                            {
                                if (VendorAddress[j] != " ")
                                {
                                    if (IsBOPenaltyEscal == "true")
                                        oSendCommunication.SendBOPenaltyCommunication(lstVendorCommListByVendor, VendorAddress[j], Vendorlanguage[j], VendorID, "resend", VendorData[0]);
                                }
                            }


                            //Updating IsVendorCommunicationSent Flag for Selected BOReportedIds
                            string SelectedBOReportingIDs = string.Empty;
                            List<int?> BackOrderIds = lstVendorCommListByVendor.Select(x => x.BOReportingID).Distinct().ToList();
                            if (BackOrderIds != null && BackOrderIds.Count > 0)
                            {
                                for (int BO = 0; BO < BackOrderIds.Count; BO++)
                                {
                                    SelectedBOReportingIDs += string.Format(",{0}", BackOrderIds[BO]);
                                }
                            }

                            BackOrderBAL oBackOrderBOBAL = new BackOrderBAL();
                            BackOrderBE oBackOrderBOBE = new BackOrderBE();
                            oBackOrderBOBE.Action = "UpdateBOIdByResendComm";
                            oBackOrderBOBE.SelectedBOReportingID = SelectedBOReportingIDs;
                            int? Result = oBackOrderBOBAL.UpdateBOIDBAL(oBackOrderBOBE);

                        }


                    }


                }
                else
                {
                    sbMessage.Clear();
                    sbMessage.Append("No Record Found for BO Vendor Communication1");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                }

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append(" BO Penalty Communication Ended at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex, VendorID.ToString());
            }
            #endregion

        }
        private static void SendBOPenaltyResendCommClosed()
        {
            #region Send BO Penalty Communication Closed
            int VendorID = 0;
            try
            {

                StringBuilder sbMessage = new StringBuilder();
                sendCommunication oSendCommunication = new sendCommunication();

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append(" BO Penalty Resend Closed Communication Started at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                string IsBOPenaltyEscal = System.Configuration.ConfigurationManager.AppSettings["IsBOPenaltyEscal"];

                BackOrderBAL oBackOrderVCBAL = new BackOrderBAL();
                BackOrderBE oBackOrderVCBE = new BackOrderBE();
                oBackOrderVCBE.Action = "GetResendVendorCommClosed";

                List<BackOrderBE> lstVendorCommList = oBackOrderVCBAL.GetVendorComm1BAL(oBackOrderVCBE);
                // find distinct vendorid
                List<int> vendorIDs = lstVendorCommList.Select(x => x.Vendor.VendorID).Distinct().ToList();

                if (vendorIDs != null && vendorIDs.Count > 0)
                {
                    for (int i = 0; i < vendorIDs.Count; i++)
                    {

                        VendorID = vendorIDs[i];
                        BackOrderBAL oBackOrderVSBAL = new BackOrderBAL();
                        BackOrderBE oBackOrderVSBE = new BackOrderBE();
                        oBackOrderVSBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                        List<BackOrderBE> lstVendorCommListByVendor = lstVendorCommList.Where(x => x.Vendor.VendorID == VendorID).ToList();

                        BackOrderBAL oBackOrderVUBAL = new BackOrderBAL();
                        BackOrderBE oBackOrderVUBE = new BackOrderBE();
                        oBackOrderVUBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oBackOrderVUBE.Vendor.VendorID = VendorID;

                        string[] VendorAddress;
                        string[] Vendorlanguage;
                        string[] VendorData = new string[2];


                        VendorData = oSendCommunication.GetVendorInitailEmailWithLanguage(oBackOrderVUBE.Vendor.VendorID, Convert.ToDateTime(lstVendorCommListByVendor[0].InitialCommDate));

                        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                        {

                            VendorAddress = VendorData[0].Split(',');
                            Vendorlanguage = VendorData[1].Split(',');

                            for (int j = 0; j < VendorAddress.Length; j++)
                            {
                                if (VendorAddress[j] != " ")
                                {
                                    if (IsBOPenaltyEscal == "true")
                                        oSendCommunication.SendBOPenaltyCommunication(lstVendorCommListByVendor, VendorAddress[j], Vendorlanguage[j], VendorID, "resendclosed", VendorData[0]);
                                }
                            }


                            //Updating IsVendorCommunicationSent Flag for Selected BOReportedIds
                            string SelectedBOReportingIDs = string.Empty;
                            List<int?> BackOrderIds = lstVendorCommListByVendor.Select(x => x.BOReportingID).Distinct().ToList();
                            if (BackOrderIds != null && BackOrderIds.Count > 0)
                            {
                                for (int BO = 0; BO < BackOrderIds.Count; BO++)
                                {
                                    SelectedBOReportingIDs += string.Format(",{0}", BackOrderIds[BO]);
                                }
                            }

                            BackOrderBAL oBackOrderBOBAL = new BackOrderBAL();
                            BackOrderBE oBackOrderBOBE = new BackOrderBE();
                            oBackOrderBOBE.Action = "UpdateBOIdByResendCommClosed";
                            oBackOrderBOBE.SelectedBOReportingID = SelectedBOReportingIDs;
                            int? Result = oBackOrderBOBAL.UpdateBOIDBAL(oBackOrderBOBE);

                        }


                    }


                }
                else
                {
                    sbMessage.Clear();
                    sbMessage.Append("No Record Found for BO Vendor Closed Communication");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                }

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append(" BO Penalty Closed Communication Ended at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex, VendorID.ToString());
            }
            #endregion
        }
    }
}

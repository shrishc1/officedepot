﻿using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy;
using BusinessLogicLayer.ModuleBAL.Discrepancy.Report;
using Microsoft.Reporting.WinForms;
using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using Utilities;

namespace VIP_DiscrepancyEscalationScheduler
{
    public class DiscrepancyCountStatusScheduler
    {
        public static string timeForDiscreapncy = System.Configuration.ConfigurationManager.AppSettings["timeForDiscreapncyScheduler"];

        public static void RunSchedulerForDiscrepancyCount()
        {
            var time = DateTime.Now.TimeOfDay;

            var timeForDiscreapncyScheduler = timeForDiscreapncy.Split(':');

            TimeSpan timeSpan = new TimeSpan(Convert.ToInt32(timeForDiscreapncyScheduler[0]),
                                             Convert.ToInt32(timeForDiscreapncyScheduler[1]),
                                             Convert.ToInt32(timeForDiscreapncyScheduler[2]));

            TimeSpan time1 = TimeSpan.FromHours(1);

            if (time >= timeSpan && timeSpan <= time.Add(time1))
            {
                DiscrepancyBE discrepancyReportBE = new DiscrepancyBE();
                DiscrepancyBAL discrepancyReportBAL = new DiscrepancyBAL();
                discrepancyReportBAL.GetDiscrepancyCountStatusBAL(discrepancyReportBE);
            }
        }
    }
}

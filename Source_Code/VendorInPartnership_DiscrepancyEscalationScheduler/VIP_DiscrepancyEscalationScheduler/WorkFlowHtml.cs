﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;


using System.Reflection;
using Utilities;
using BusinessEntities.ModuleBE.Languages;
using BusinessLogicLayer.ModuleBAL.Languages;
namespace VIP_DiscrepancyEscalationScheduler
{
    public class WorkflowHTML
    {
        public WorkflowHTML()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region First Block HTML Functions

        /// <summary>
        /// Generate HTML for date, user id, action, po number, 
        /// debit note number, coll date, carrier, coll auth number, 
        /// GoodsCollectedOn, CarriageCharge, credit note number and comments
        /// </summary>
        /// <param name="pRole"></param>
        /// <param name="pDate"></param>
        /// <param name="pCollectionDate"></param>
        /// <param name="pGoodsCollectedOn"></param>
        /// <param name="pColor"></param>
        /// <param name="pAction"></param>
        /// <param name="pUserId"></param>
        /// <param name="pComments"></param>
        /// <param name="pPurchaseOrderNumber"></param>
        /// <param name="pDebitNoteNumber"></param>
        /// <param name="pCarrier"></param>
        /// <param name="pCollAuthNumber"></param>
        /// <param name="pCarriageCharge"></param>
        /// <param name="pCreditNoteNumber"></param>
        /// <returns></returns>

        public string function1(//string pRole,
                                DateTime pDate,
                                DateTime? pCollectionDate,
                                DateTime? pGoodsCollectedOn,
                                string pColor,
                                string pAction = "",
                                string pAction1 = "",
                                string pUserName = "",
                                string pComments = "",
                                string pPurchaseOrderNumber = "",
                                string pDebitNoteNumber = "",
                                string pCarrier = "",
                                string pCollAuthNumber = "",
                                string pCarriageCharge = "",
                                string pCreditNoteNumber = "",
                                string pReason = "",
                                string pActionTaken = "",
                                string pCost = "",
                                string pLabourCost = "",
                                string pAdminCost = "",
                                string pPallets = "",
                                string pPackSizeOrdered = "",
                                string pPackSizeReceived = "",
                                string pNoEsclationOnFromPage = "",
                                string pNoEsclationOn = "",
                                string language = "English")
        {
            string sDate = Common.getGlobalResourceValue("Date", language);
            string sUserName = Common.getGlobalResourceValue("UserName", language);
            string sAction = Common.getGlobalResourceValue("Action", language);
            string sComments = Common.getGlobalResourceValue("Comments", language);
            string sPurchaseOrderNumber = Common.getGlobalResourceValue("PurchaseOrderNumber", language);
            string sDebitNoteNumber = Common.getGlobalResourceValue("DebitNoteNumber", language);
            string sCollectionDate = Common.getGlobalResourceValue("CollectionDate", language);
            string sCollectionAuthorisationNumber = Common.getGlobalResourceValue("CollectionAuthorisationNumber", language);
            string sCarrier = Common.getGlobalResourceValue("Carrier", language);
            string sGoodsCollectedOn = Common.getGlobalResourceValue("GoodsCollectedOn", language);
            string sCarriageCharge = Common.getGlobalResourceValue("CarriageCharge", language);
            string sCreditNoteNumber = Common.getGlobalResourceValue("CreditNoteNumber", language);
            string sReason = Common.getGlobalResourceValue("Reason", language);
            string sActionTaken = Common.getGlobalResourceValue("ActionTaken", language);
            string sCost = Common.getGlobalResourceValue("Cost", language);
            string sLabourCost = Common.getGlobalResourceValue("LabourCost", language);
            string sAdminCost = Common.getGlobalResourceValue("AdminCost", language);
            string sNoOfPallets = Common.getGlobalResourceValue("#Pallets", language);
            string sPackSizeOrdered = Common.getGlobalResourceValue("PackSizeOrdered", language);
            string sPackSizeReceived = Common.getGlobalResourceValue("PackSizeReceived", language);
            string sNoEsclationOn = Common.getGlobalResourceValue("NoEsclationOn", language);

            StringBuilder sHTML = new StringBuilder();

            if (pColor.ToLower() == "yellow")
            {
                pColor = "f9eec4";
                sHTML.Append("<table class='yellow-bg'>");
            }

            if (pColor.ToLower() == "blue")
            {
                pColor = "EAF2FC";
                sHTML.Append("<table class='blue-bg'>");
            }
            //sHTML.Append("<table style='Width:100%;  background-color:#" + pColor + "'>");


            /* if (pAction != "") {
                 sHTML.Append("<tr>");
                 sHTML.Append("<td colspan='3' align='center'>");
                 sHTML.Append("<span style='border-bottom:1px solid'>" + pRole + "</span>\r\n");
                 sHTML.Append("<span style='border-bottom:1px solid'>" + pAction + "</span>");
                 sHTML.Append("</td>");
                 sHTML.Append("</tr>");
             }
             else {
                 sHTML.Append("<tr>");
                 sHTML.Append("<td colspan='3' style='border-bottom:1px solid' align='center'>");
                 sHTML.Append(pRole);
                 sHTML.Append("</td>");
                 sHTML.Append("</tr>");
             }*/

            if (pDate != null)
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pDate.ToString("dd/MM/yyyy") + "</td>");
                sHTML.Append("</tr>");
            }
            if (pReason != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sReason + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pReason + "</td>");
                sHTML.Append("</tr>");
            }

            sHTML.Append("<tr>");
            sHTML.Append("<td style='font-style;font-weight:bold'>" + sUserName + "</td>");
            sHTML.Append("<td>:</td>");
            sHTML.Append("<td>" + pUserName + "</td>");
            sHTML.Append("</tr>");

            if (pAction1 != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sAction + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pAction1 + "</td>");
                sHTML.Append("</tr>");
            }
            if (pDebitNoteNumber != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sDebitNoteNumber + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pDebitNoteNumber + "</td>");
                sHTML.Append("</tr>");
            }

            if (pPurchaseOrderNumber != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sPurchaseOrderNumber + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pPurchaseOrderNumber + "</td>");
                sHTML.Append("</tr>");
            }

            if (pCollectionDate != null)
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionDate + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + Convert.ToDateTime(pCollectionDate).ToString("dd/MM/yyyy") + "</td>");
                sHTML.Append("</tr>");
            }

            if (pCarrier != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sCarrier + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pCarrier + "</td>");
                sHTML.Append("</tr>");
            }

            if (pCollAuthNumber != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionAuthorisationNumber + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pCollAuthNumber + "</td>");
                sHTML.Append("</tr>");
            }

            if (pGoodsCollectedOn != null)
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sGoodsCollectedOn + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + Convert.ToDateTime(pGoodsCollectedOn).ToString("dd/MM/yyyy") + "</td>");
                sHTML.Append("</tr>");
            }

            if (pCarriageCharge != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sCarriageCharge + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pCarriageCharge + "</td>");
                sHTML.Append("</tr>");
            }

            if (pCreditNoteNumber != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sCreditNoteNumber + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pCreditNoteNumber + "</td>");
                sHTML.Append("</tr>");
            }

            if (pActionTaken != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sActionTaken + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pActionTaken + "</td>");
                sHTML.Append("</tr>");
            }

            if (pPallets != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sNoOfPallets + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pPallets + "</td>");
                sHTML.Append("</tr>");
            }

            if (pCost != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sCost + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pCost + "</td>");
                sHTML.Append("</tr>");
            }

            if (pLabourCost != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sLabourCost + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pLabourCost + "</td>");
                sHTML.Append("</tr>");
            }

            if (pAdminCost != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sAdminCost + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pAdminCost + "</td>");
                sHTML.Append("</tr>");
            }

            if (pPackSizeOrdered != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sPackSizeOrdered + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pPackSizeOrdered + "</td>");
                sHTML.Append("</tr>");
            }

            if (pPackSizeReceived != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sPackSizeReceived + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pPackSizeReceived + "</td>");
                sHTML.Append("</tr>");
            }

            if (pNoEsclationOnFromPage != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sNoEsclationOn + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pNoEsclationOn + "</td>");
                sHTML.Append("</tr>");
            }

            if (pComments != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td  style='font-style;font-weight:bold'>" + sComments + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td colspan='2'>:</td>");
                sHTML.Append("</tr>");

                sHTML.Append("<tr>");
                sHTML.Append("<td colspan='3' style='font-style:italic;font-weight:bold'>" + pComments + "</td>");
                sHTML.Append("</tr>");
            }

            //sHTML.Append("<br/><br/>");
            sHTML.Append("</table>");

            return sHTML.ToString();
        }

        /// <summary>
        /// Generate HTML for date, sent to,top comments, bottom comments, view communication, 
        /// collection date, carrier, collection authorisation number, action and comments
        /// </summary>
        /// <param name="pRole"></param>
        /// <param name="pDate"></param>
        /// <param name="pCollectionDate"></param>
        /// <param name="pColor"></param>
        /// <param name="pSentTo"></param>
        /// <param name="pTopComments"></param>
        /// <param name="pBottomComments"></param>
        /// <param name="pAction"></param>
        /// <param name="pViewCommNumber"></param>
        /// <param name="pViewCommLink"></param>
        /// <param name="pCarrier"></param>
        /// <param name="pCollAuthNumber"></param>
        /// <returns></returns>
        public string function2(//string pRole,
                                DateTime pDate,
                                DateTime? pCollectionDate,
                                string pColor,
                                string pSentTo = "",
                                string pTopComments = "",
                                string pBottomComments = "",
                                string pAction = "",
                                string pAction1 = "",
                                string pViewCommNumber = "",
                                string pViewCommLink = "",
                                string pCarrier = "",
                                string pCollAuthNumber = "",
                                string pComments = "",
                                string pFromPage = "",
            string language = "English")
        {

            string sDate = Common.getGlobalResourceValue("Date", language);
            string sSentTo = Common.getGlobalResourceValue("SentTo", language);
            string sActionRequired = Common.getGlobalResourceValue("ActionRequired", language);
            string sViewCommunication = Common.getGlobalResourceValue("ViewCommunication", language);
            string sCollectionDate = Common.getGlobalResourceValue("CollectionDate", language);
            string sCollectionAuthorisationNumber = Common.getGlobalResourceValue("CollectionAuthorisationNumber", language);
            string sCarrier = Common.getGlobalResourceValue("Carrier", language);
            string sComments = Common.getGlobalResourceValue("Comments", language);

            StringBuilder sHTML = new StringBuilder();



            if (pColor.ToLower() == "yellow")
            {
                pColor = "f9eec4";
                sHTML.Append("<table class='yellow-bg'>");
            }

            if (pColor.ToLower() == "blue")
            {
                pColor = "EAF2FC";
                sHTML.Append("<table class='blue-bg'>");
            }
            //sHTML.Append("<table style='Width:100%; background-color:#" + pColor + "'>");

            /*if (pAction != "") {
                sHTML.Append("<tr>");
                sHTML.Append("<td colspan='3' align='center'>");
                sHTML.Append("<span style='border-bottom:1px solid'>" + pRole + "</span>\r\n");
                sHTML.Append("<span style='border-bottom:1px solid'>" + pAction + "</span>");
                sHTML.Append("</td>");
                sHTML.Append("</tr>");
                sHTML.Append("<br/>");
            }
            else {
                sHTML.Append("<tr>");
                sHTML.Append("<td colspan='3' align='center'>");
                sHTML.Append(pRole);
                sHTML.Append("</td>");
                sHTML.Append("</tr>");
            }*/

            if (pTopComments != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td colspan='3' align='left'>");
                sHTML.Append(pTopComments);
                sHTML.Append("</td>");
                sHTML.Append("</tr>");
            }

            if (pFromPage == "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pDate.ToString("dd/MM/yyyy") + "</td>");
                sHTML.Append("</tr>");
            }

            if (pSentTo != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sSentTo + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>");

                pSentTo = pSentTo.Trim(',');
                foreach (string str in pSentTo.Split(','))
                {
                    sHTML.Append(str + "<br />");
                }
                sHTML.Append("</td>");
                sHTML.Append("</tr>");
            }

            if (pCollectionDate != null)
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionDate + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + Convert.ToDateTime(pCollectionDate).ToString("dd/MM/yyyy") + "</td>");
                sHTML.Append("</tr>");
            }

            if (pCarrier != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sCarrier + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pCarrier + "</td>");
                sHTML.Append("</tr>");
            }

            if (pCollAuthNumber != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sCollectionAuthorisationNumber + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pCollAuthNumber + "</td>");
                sHTML.Append("</tr>");
            }
            if (pBottomComments != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td colspan='3' align='left' style='font-style:italic;font-weight:bold'>" + pBottomComments + "</td>");
                sHTML.Append("</tr>");
            }
            if (pComments != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sComments + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td style='font-style:italic;font-weight:bold'>" + pComments + "</td>");
                sHTML.Append("</tr>");
            }
            if (pViewCommLink != "")
            {
                sHTML.Append("<tr>");

                sHTML.Append("<td align='left' colspan='2'>" + pViewCommNumber + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td align='right'><asp:HyperLink ID='btnViewComm' runat='server' NavigateUrl='");
                sHTML.Append(pViewCommLink);
                sHTML.Append("' style='border:1px solid;text-decoration:none;border-style:outset'");
                sHTML.Append("Text='" + sViewCommunication + "'></asp:HyperLink></td>");
                sHTML.Append("</tr>");
            }

            //sHTML.Append("<br/><br/>");
            sHTML.Append("</table>");

            return sHTML.ToString();
        }

        /// <summary>
        /// Generate HTML without any data just only role 
        /// </summary>
        /// <param name="pRole"></param>
        /// <param name="pAction"></param>
        /// <returns></returns>
        public string function3(//string pRole,
                                string pColor,
                                DateTime? pDate,
                                string pComments = "",
                                string pAction = "",
            string language = "English")
        {

            string sDate = Common.getGlobalResourceValue("Date", language);
            string sComments = Common.getGlobalResourceValue("Comments", language);

            StringBuilder sHTML = new StringBuilder();
            if (pColor.ToLower() == "yellow")
            {
                pColor = "f9eec4";
                sHTML.Append("<table class='yellow-bg'>");
            }

            if (pColor.ToLower() == "blue")
            {
                pColor = "EAF2FC";
                sHTML.Append("<table class='blue-bg'>");
            }

            sHTML.Append("<table style='Width:100%;  background-color:#" + pColor + "'>");

            /*if (pAction != "") {
                sHTML.Append("<tr>");
                sHTML.Append("<td colspan='3' align='center'>");
                sHTML.Append("<span style='border-bottom:1px solid'>" + pRole + "</span>\r\n");
                sHTML.Append("<span style='border-bottom:1px solid'>" + pAction + "</span>");
                sHTML.Append("</td>");
                sHTML.Append("</tr>");
            }
            else {
                sHTML.Append("<tr>");
                sHTML.Append("<td colspan='3' style='border-bottom:1px solid' align='center'>");
                sHTML.Append(pRole);
                sHTML.Append("</td>");
                sHTML.Append("</tr>");
            }*/
            if (pDate != null)
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + Convert.ToDateTime(pDate).ToString("dd/MM/yyyy") + "</td>");
                sHTML.Append("</tr>");
            }
            else
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td>&nbsp;</td>");
                sHTML.Append("<td>&nbsp;</td>");
                sHTML.Append("<td>&nbsp;</td>");
                sHTML.Append("</tr>");
            }
            if (pComments != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sComments + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pComments + "</td>");
                sHTML.Append("</tr>");
            }
            else
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td>&nbsp;</td>");
                sHTML.Append("<td>&nbsp;</td>");
                sHTML.Append("<td>&nbsp;</td>");
                sHTML.Append("</tr>");
            }
            sHTML.Append("<tr>");
            sHTML.Append("<td>&nbsp;</td>");
            sHTML.Append("<td>&nbsp;</td>");
            sHTML.Append("<td>&nbsp;</td>");
            sHTML.Append("</tr>");

            //sHTML.Append("<br/><br/>");
            sHTML.Append("</table>");

            return sHTML.ToString();
        }

        /// <summary>
        /// Generate HTML for Sent on, Sent to, Sent from, 
        /// comments, action and view communication link
        /// </summary>
        /// <param name="pRole"></param>
        /// <param name="pColor"></param>
        /// <param name="pSentOn"></param>
        /// <param name="pSentTo"></param>
        /// <param name="pSentFrom"></param>
        /// <param name="pTopComments"></param>
        /// <param name="pBottomComments"></param>
        /// <param name="pAction"></param>
        /// <param name="pViewCommLink"></param>
        /// <returns></returns>
        public string function4(//string pRole,                                
                                string pColor,
                                string pAction = "",
                                DateTime? pSentOn = null,
                                string pSentTo = "",
                                string pSentFrom = "",
                                string pTopComments = "",
                                string pBottomComments = "",
                                string pViewCommNumber = "",
                                string pViewCommLink = "",
                                DateTime? pDate = null,
                                string pLabourCost = "",
            string language = "English")
        {

            string sDate = Common.getGlobalResourceValue("Date", language);
            string sSentOn = Common.getGlobalResourceValue("SentOn", language);
            string sSentTo = Common.getGlobalResourceValue("SentTo", language);
            string sSentFrom = Common.getGlobalResourceValue("SentFrom", language);
            string sActionRequired = Common.getGlobalResourceValue("ActionRequired", language);
            string sViewCommunication = Common.getGlobalResourceValue("ViewCommunication", language);
            string sLabourCost = Common.getGlobalResourceValue("LabourCost", language);

            StringBuilder sHTML = new StringBuilder();
            if (pColor.ToLower() == "yellow")
            {
                pColor = "f9eec4";
                sHTML.Append("<table class='yellow-bg'>");
            }

            if (pColor.ToLower() == "blue")
            {
                pColor = "EAF2FC";
                sHTML.Append("<table class='blue-bg'>");
            }



            // sHTML.Append("<table style='Width:100%; background-color:#" + pColor + "'>");

            /* if (pAction != "") {
                 sHTML.Append("<tr>");
                 sHTML.Append("<td colspan='3' align='center'>");
                 sHTML.Append("<span style='border-bottom:1px solid'>" + pRole + "</span>\r\n");
                 sHTML.Append("<span style='border-bottom:1px solid'>" + pAction + "</span>");
                 sHTML.Append("</td>");
                 sHTML.Append("</tr>");
                 sHTML.Append("<br/>");
             }
             else {
                 sHTML.Append("<tr>");
                 sHTML.Append("<td colspan='3' align='center'>");
                 sHTML.Append(pRole);
                 sHTML.Append("</td>");
                 sHTML.Append("</tr>");
             }*/

            if (pTopComments != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td colspan='3' align='left'>");
                sHTML.Append(pTopComments);
                sHTML.Append("</td>");
                sHTML.Append("</tr>");
            }

            if (pDate != null)
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sDate + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + Convert.ToDateTime(pDate).ToString("dd/MM/yyyy") + "</td>");
                sHTML.Append("</tr>");
            }
            if (pSentOn != null)
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sSentOn + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + Convert.ToDateTime(pSentOn).ToString("dd/MM/yyyy") + "</td>");
                sHTML.Append("</tr>");
            }
            if (pLabourCost != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sLabourCost + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>" + pLabourCost + "</td>");
                sHTML.Append("</tr>");
            }
            if (pSentTo != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td style='font-style;font-weight:bold'>" + sSentTo + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td>:</td>");
                sHTML.Append("<td>");

                //pSentTo = pSentTo.Trim(',');
                //foreach (string str in pSentTo.Split(','))
                //{
                //    sHTML.Append(str + "<br />");
                //}
                sHTML.Append(pSentTo);
                sHTML.Append("</td>");
                sHTML.Append("</tr>");
            }
            //if (pSentFrom != "") {
            //    sHTML.Append("<tr>");
            //    sHTML.Append("<td style='font-style;font-weight:bold'>" + sSentFrom + "&nbsp;&nbsp;</td>");
            //    sHTML.Append("<td>:</td>");
            //    sHTML.Append("<td>" + pSentFrom + "</td>");
            //    sHTML.Append("</tr>");
            //}

            if (pBottomComments != "")
            {
                sHTML.Append("<tr>");
                sHTML.Append("<td colspan='3' align='left' style='font-style:italic;font-weight:bold'>" + pBottomComments + "</td>");
                sHTML.Append("</tr>");
            }

            if (pViewCommLink != "")
            {
                sHTML.Append("<tr>");

                sHTML.Append("<td align='left' colspan='2'>" + pViewCommNumber + "&nbsp;&nbsp;</td>");
                sHTML.Append("<td align='right'><asp:HyperLink ID='btnViewComm' runat='server' NavigateUrl='");
                sHTML.Append(pViewCommLink);
                sHTML.Append("' style='border:1px solid;text-decoration:none;border-style:outset'");
                sHTML.Append("Text='" + sViewCommunication + "'></asp:HyperLink></td>");
                sHTML.Append("</tr>");
            }



            //sHTML.Append("<br/><br/>");
            sHTML.Append("</table>");
            return sHTML.ToString();
        }
        #endregion
    }

    public class Common
    {
        //public static string getGlobalResourceValue(string key)
        //{
        //    //ResourceManager LocRM = new ResourceManager("VIP_DiscrepancyEscalationScheduler.Properties.OfficeDepot", typeof(frmDummyForm).Assembly);
        //    ResourceManager LocRM = new ResourceManager("VIP_DiscrepancyEscalationScheduler.Properties.OfficeDepot",Assembly.GetExecutingAssembly() );
        //    return LocRM.GetString(key);
        //}

        public static string getGlobalResourceValue(string key, string UserLanguage)
        {
            string ResourceValues = string.Empty;
            var lstLanguageTags = new List<MAS_LanguageTagsBE>();
            try
            {
                //Get All LanguageTags From DB
                MAS_LanguageTagsBE oMAS_LanguageTagsBE = new MAS_LanguageTagsBE();
                MAS_LanguageTagsBAL oMAS_LanguageTagsBAL = new MAS_LanguageTagsBAL();
                if (lstLanguageTags != null && lstLanguageTags.Count <= 0)
                {
                    oMAS_LanguageTagsBE.Action = "GetAllLanguageTags";
                    lstLanguageTags = oMAS_LanguageTagsBAL.GetAllLanguageTagsBAL(oMAS_LanguageTagsBE);
                    var lstResult = lstLanguageTags.Where(x => x.LanguageKey.ToLower() == key.ToLower()).ToList();
                    if (lstResult != null && lstResult.Count > 0)
                    {
                        Type type = lstResult[0].GetType();
                        PropertyInfo prop = type.GetProperty(UserLanguage);
                        if (prop != null)
                        {
                            ResourceValues = Convert.ToString(prop.GetValue(lstResult[0], null));
                        }
                    }
                }
                else
                {

                    var lstResult = lstLanguageTags.Where(x => x.LanguageKey.ToLower() == key.ToLower()).ToList();
                    if (lstResult != null && lstResult.Count > 0)
                    {
                        Type type = lstResult[0].GetType();
                        PropertyInfo prop = type.GetProperty(UserLanguage);
                        if (prop != null)
                        {
                            ResourceValues = Convert.ToString(prop.GetValue(lstResult[0], null));
                        }
                    }
                }
            }

            catch (Exception ex)
            {

            }
            return ResourceValues;
        }

        public static string EncryptQuery(string pURL)
        {
            string returnURL;
            if (pURL.Contains("?"))
            {
                string[] urlSplit = pURL.Split('?');
                string EncriptedQueryStripg = Encryption.Encrypt(urlSplit[1]);
                returnURL = urlSplit[0] + "?" + EncriptedQueryStripg;
            }
            else
            {
                returnURL = pURL;
            }
            return returnURL;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BusinessEntities.ModuleBE.StockOverview.Report;
using BusinessLogicLayer.ModuleBAL.StockOverview.Report;
using Microsoft.Reporting.WinForms;
using System.IO;
using System.Data.SqlClient;
using System.Windows.Forms;
using BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.OnTimeInFull.Report;
using BusinessLogicLayer.ModuleBAL.OnTimeInFull.Report;
using System.Configuration;
using Utilities;

namespace VIP_VendorGenerateOTIFScheduler
{
    public class GenerateReport
    {
        private DataRow dr;

        DataSet dsStockOverview = new DataSet();
        StockOverviewReportBE oStockOverviewReportBE = new StockOverviewReportBE();
        StockOverviewReportBAL oStockOverviewReportBAL = new StockOverviewReportBAL();

        #region ReportVariables

        Warning[] warnings;
        string[] streamIds;
        string mimeType = string.Empty;
        string encoding = string.Empty;
        string extension = string.Empty;
        string reportFileName = string.Empty;

        #endregion

        public void StartReportGeneration(DateTime dtDateFrom, DateTime dtDateTo, int? otifSchedularId, string otifApplicationName)
        {
            string MonthPath = "";

            //string OTIFMonth = System.Configuration.ConfigurationSettings.AppSettings["OTIFMonth"];
            //int iOTIFMonth = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["OTIFMonth"]);

            //Vendor Selection
            UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
            UP_VendorBE oUP_VendorBE = new UP_VendorBE();
            //DateTime dt = DateTime.Now.AddMonths(-iOTIFMonth);
            //DateTime dtDateFrom = new DateTime(dt.Year, dt.Month, 1);
            //DateTime dtDateTo = new DateTime(dtDateFrom.Year, dtDateFrom.Month, dtDateFrom.AddMonths(1).AddDays(-1).Day);

            // dtDateFrom.AddDays(-1).AddMonths(1); // (new DateTime(DateTime.Now.Year, DateTime.Now.Month - (iOTIFMonth - 1), 1).AddDays(-1));

            string sDateFrom = string.Empty;
            string sDateTo = string.Empty;

            string ReportOutputPath;
            ReportDataSource rds;
            ReportViewer ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            //ReportParameter[] reportParameter = new ReportParameter[9];
            string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
            path = path.Replace(@"\bin\debug", "");

            if (dtDateFrom.Day.ToString().Length == 1)
                sDateFrom = sDateFrom + "0" + dtDateFrom.Day.ToString();
            else
                sDateFrom = sDateFrom + dtDateFrom.Day.ToString();

            if (dtDateFrom.Month.ToString().Length == 1)
                sDateFrom = sDateFrom + "/" + "0" + dtDateFrom.Month.ToString();
            else
                sDateFrom = sDateFrom + "/" + dtDateFrom.Month.ToString();

            sDateFrom = sDateFrom + "/" + dtDateFrom.Year.ToString();


            if (dtDateTo.Date.ToString().Length == 1)
                sDateTo = sDateTo + "0" + dtDateTo.Day.ToString();
            else
                sDateTo = sDateTo + dtDateTo.Day.ToString();

            if (dtDateTo.Month.ToString().Length == 1)
                sDateTo = sDateTo + "/" + "0" + dtDateTo.Month.ToString();
            else
                sDateTo = sDateTo + "/" + dtDateTo.Month.ToString();

            sDateTo = sDateTo + "/" + dtDateTo.Year.ToString();

            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            //oUP_VendorBE.Action = "SearchVendor";
            //oUP_VendorBE.IsChildRequired = true;
            //oUP_VendorBE.IsStandAloneRequired = true;
            ////Get all Child and Standalone Vendors
            //oUP_VendorBEList = oUP_VendorBAL.GetVendorsBAL(oUP_VendorBE);

            oUP_VendorBE.Action = "GetOTIFVendorReports";

            string vendorCountToCreate = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["VendorCountToCreateReport"]);
            if (!string.IsNullOrEmpty(vendorCountToCreate))
                oUP_VendorBE.VendorCountToCreateReport = Convert.ToInt32(vendorCountToCreate);
            
            //changes
            oUP_VendorBE.otifSchedularId = otifSchedularId;
            oUP_VendorBE.otifApplicationName = otifApplicationName;

            oUP_VendorBEList = oUP_VendorBAL.GetOTIFVendorReportsBAL(oUP_VendorBE);

            //oUP_VendorBEList = oUP_VendorBEList.Where(y => y.VendorID == 16518).ToList(); 

            try
            {
                // Dataset Population
                OTIFReportBE oOTIFReportBE = new OTIFReportBE();
                OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();

                oOTIFReportBE.Action = "OTIF_ODLineLevelDetailedReport"; // "OTIF_VendorLineLevelDetailedReport";
                oOTIFReportBE.DateFrom = dtDateFrom;
                oOTIFReportBE.DateTo = dtDateTo;
                oOTIFReportBE.PurchaseOrderDue = 1;
                oOTIFReportBE.PurchaseOrderReceived = 0;

                //reportParameter[0] = new ReportParameter("Country", string.Empty);
                //reportParameter[1] = new ReportParameter("Site", string.Empty);
                //reportParameter[2] = new ReportParameter("Vendor", string.Empty);
                //reportParameter[3] = new ReportParameter("DateFrom", sDateFrom);
                //reportParameter[4] = new ReportParameter("DateTo", sDateTo);
                //reportParameter[5] = new ReportParameter("ItemClassification", string.Empty);
                //reportParameter[6] = new ReportParameter("ODSKUCode", string.Empty);
                //reportParameter[7] = new ReportParameter("PurchaseOrder", string.Empty);
                //reportParameter[8] = new ReportParameter("StockPlanner", string.Empty);

                //Deployment Directory Path
                MonthPath = path + @"ReportOutput\" + dtDateFrom.Year.ToString() + @"\";

                if (dtDateFrom.Month.ToString().Length == 1)
                    MonthPath = MonthPath + "0" + dtDateFrom.Month.ToString() + @"\";
                else
                    MonthPath = MonthPath + dtDateFrom.Month.ToString() + @"\";

                if (!System.IO.Directory.Exists(MonthPath))
                    System.IO.Directory.CreateDirectory(MonthPath);

                StringBuilder sbMessage = new StringBuilder();
                DataSet dsOTIFReport = null;
                int iSleepCounter = 0;

                foreach (UP_VendorBE iUP_VendorBE in oUP_VendorBEList)
                {
                    try
                    {
                        sbMessage.Clear();
                        ReportOutputPath = MonthPath + iUP_VendorBE.CountryName + "_" + iUP_VendorBE.Vendor_No + "_" + "VendorOTIFReport.xls";
                        //if (!File.Exists(ReportOutputPath))
                        //{
                            sbMessage.Append("Vendor ID             : " + iUP_VendorBE.CountryName + "_" + iUP_VendorBE.Vendor_No);

                       // oOTIFReportBE.SelectedVendorIDs = "1901";// iUP_VendorBE.VendorID.ToString();

                            dsOTIFReport = null;
                            dsOTIFReport = oOTIFReportBAL.GetOTIFReportBAL(oOTIFReportBE);

                            if (dsOTIFReport != null)
                            {
                                dsOTIFReport.Tables[0].Columns.Add("DateFrom");
                                dsOTIFReport.Tables[0].Columns.Add("DateTo");
                                dsOTIFReport.Tables[0].Rows.OfType<DataRow>().ToList().ForEach(r => r["DateFrom"] = sDateFrom);
                                dsOTIFReport.Tables[0].Rows.OfType<DataRow>().ToList().ForEach(r => r["DateTo"] = sDateTo);
                            }

                            sbMessage.Append("\r\n");
                            if (dsOTIFReport == null)
                                sbMessage.Append("Record count          : null");
                            else
                                sbMessage.Append("Record count          : " + dsOTIFReport.Tables[0].Rows.Count.ToString());

                            // Report Processing
                            rds = new ReportDataSource("dtVendorLineLevelDetailedReport", dsOTIFReport.Tables[0]);
                            ReportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                            ReportViewer1.LocalReport.Refresh();
                            ReportViewer1.LocalReport.DataSources.Clear();
                            ReportViewer1.LocalReport.DataSources.Add(rds);

                            ReportViewer1.LocalReport.ReportPath = path + @"ModuleUI\OnTimeInFull\RDLC\VendorLineLevelDetailedReport.rdlc";
                            // ReportViewer1.LocalReport.SetParameters(reportParameter);

                            byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                            File.WriteAllBytes(ReportOutputPath, bytes);

                            // Logic to update the generated status in table.
                            oUP_VendorBE.Action = "UpdateOTIFVendorReport";
                            oUP_VendorBE.VendorID = iUP_VendorBE.VendorID;
                            oUP_VendorBAL.UpdateOTIFVendorReportBAL(oUP_VendorBE);

                            sbMessage.Append("\r\n");
                            sbMessage.Append("Successfully Generated at - " + DateTime.Now.ToString());
                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************************************************************");
                            sbMessage.Append("\r\n");
                            LogUtility.SaveTraceLogEntry(sbMessage);
                        //}
                        //else
                        //{
                        //    // Logic to update the generated status in table.
                        //    oUP_VendorBE.Action = "UpdateOTIFVendorReport";
                        //    oUP_VendorBE.VendorID = iUP_VendorBE.VendorID;
                        //    oUP_VendorBAL.UpdateOTIFVendorReportBAL(oUP_VendorBE);
                        //}
                    }
                    catch (Exception ex)
                    {
                        LogUtility.SaveErrorLogEntry(ex, iUP_VendorBE.CountryName + "_" + iUP_VendorBE.Vendor_No);

                        sbMessage.Append("Report Generation Failed");
                        sbMessage.Append("\r\n");
                        sbMessage.Append("****************************************************************************************");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                    }
                    finally
                    {
                        dsOTIFReport = null;
                        ReportOutputPath = string.Empty;
                        iSleepCounter++;

                        if (iSleepCounter == 10)
                        {
                            System.Threading.Thread.Sleep(3);
                            iSleepCounter = 0;
                        }
                    }
                }
                oOTIFReportBAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
        }

        public void StartReportGenerationOTIFMasterVendor(DateTime toDate, int? otifSchedularId, string otifApplicationName)
        {

            #region Old code to set FROM and TO date
            /*
            int iOTIFMonth = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MasterOTIFMonth"]);

            

            DateTime dtRequireddate = DateTime.Now.AddMonths(-iOTIFMonth); //new DateTime(DateTime.Now.Year, DateTime.Now.Month - iOTIFMonth, 1);

            DateTime dtDateFrom = new DateTime(dtRequireddate.Year, 1, 1);

            DateTime temp = new DateTime(dtRequireddate.Year, dtRequireddate.AddMonths(1).Month, 1);

            DateTime dtDateTo = new DateTime(dtDateFrom.Year, dtRequireddate.Month, temp.AddDays(-1).Day);
            //DateTime dtDateTo = (new DateTime(DateTime.Now.Year, DateTime.Now.Month - (iOTIFMonth - 1), 1).AddDays(-1));

            */
            #endregion Old code to set FROM and TO date

            string sDateFrom = string.Empty;
            string sDateTo = string.Empty;

            DateTime dtDateFrom = new DateTime(toDate.Year, 1, 1);
            DateTime dtDateTo = toDate;

            if (dtDateFrom.Day.ToString().Length == 1)
                sDateFrom = sDateFrom + "0" + dtDateFrom.Day.ToString();
            else
                sDateFrom = sDateFrom + dtDateFrom.Day.ToString();

            if (DateTime.Now.Month.ToString().Length == 1)
                sDateFrom = sDateFrom + "/" + "0" + dtDateFrom.Month.ToString();
            else
                sDateFrom = sDateFrom + "/" + dtDateFrom.Month.ToString();

            sDateFrom = sDateFrom + "/" + dtDateFrom.Year.ToString();


            if (dtDateTo.Date.ToString().Length == 1)
                sDateTo = sDateTo + "0" + dtDateTo.Day.ToString();
            else
                sDateTo = sDateTo + dtDateTo.Day.ToString();

            if (dtDateTo.Month.ToString().Length == 1)
                sDateTo = sDateTo + "/" + "0" + dtDateTo.Month.ToString();
            else
                sDateTo = sDateTo + "/" + dtDateTo.Month.ToString();

            sDateTo = sDateTo + "/" + dtDateTo.Year.ToString();


            //var dsTemp = new DataSet();
            ReportDataSource rds;
            ReportViewer ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();

            //ReportParameter[] reportParameter = new ReportParameter[5];
            //reportParameter[1] = new ReportParameter("DateFrom", dtDateFrom.ToString("dd/MM/yyyy"));
            //reportParameter[2] = new ReportParameter("DateTo", dtDateTo.ToString("dd/MM/yyyy"));
            //reportParameter[3] = new ReportParameter("Title", "NEW Measure");
            //reportParameter[4] = new ReportParameter("EuropeanorLocal", "EuropeanorLocal");

            string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
            path = path.Replace(@"\bin\debug", "");

            string ReportOutputPath;
            ReportOutputPath = path + @"ReportOutput\" + dtDateFrom.Year.ToString() + @"\";
            if (!System.IO.Directory.Exists(ReportOutputPath))
                System.IO.Directory.CreateDirectory(ReportOutputPath);

            string ReportPath = string.Empty;

            var dv = new DataView();
            int iSleepCounter = 0;

            StringBuilder sbMessage = new StringBuilder();
            var dsOTIFReport = new DataSet();

            try
            {
                List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
                UP_VendorBAL oUP_VendorBAL = new UP_VendorBAL();
                UP_VendorBE objUP_VendorBE = new UP_VendorBE();

                //objUP_VendorBE.Action = "GetMasterVendors";
                ////Get Master Parent Vendors
                //oUP_VendorBEList = oUP_VendorBAL.GetMasterVendorsBAL(objUP_VendorBE);

                objUP_VendorBE.Action = "GetMasterVendorsNew";
                string vendorCountToCreate = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["VendorCountToCreateReport"]);
                if (!string.IsNullOrEmpty(vendorCountToCreate))
                    objUP_VendorBE.VendorCountToCreateReport = Convert.ToInt32(vendorCountToCreate);
                //changes
                objUP_VendorBE.otifSchedularId = otifSchedularId;
                objUP_VendorBE.otifApplicationName = otifApplicationName;
                oUP_VendorBEList = oUP_VendorBAL.GetOTIFMasterVendorReportsBAL(objUP_VendorBE);

                // oUP_VendorBEList = oUP_VendorBEList.Where(y => y.MasterParentVendorID == 16643).ToList();

                //GenerateMasterVendorOTIF(oUP_VendorBEList, dtDateFrom, dtDateTo);

                if (oUP_VendorBEList.Count > 0)
                {

                    OTIFReportBE oOTIFReportBE = new OTIFReportBE();
                    OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();

                    //Get Report Data
                    oOTIFReportBE.Action = "OTIF_MasterVendorReportNewMeasure";
                    oOTIFReportBE.DateFrom = dtDateFrom;
                    oOTIFReportBE.DateTo = dtDateTo;
                    oOTIFReportBE.PurchaseOrderDue = 1;
                    oOTIFReportBE.PurchaseOrderReceived = 0;

                    //oOTIFReportBE.SelectedVendorIDs = oUP_VendorBEList[0].VendorIDs;

                    // "5446,5449,7817,7820,16755,16756,16757,120,13658,16843";  //  oUP_VendorBE.VendorIDs; 

                    dsOTIFReport = oOTIFReportBAL.GetOTIFReportBAL(oOTIFReportBE);
                    if (dsOTIFReport != null)
                    {
                        dsOTIFReport.Tables[0].Columns.Add("FromDate");
                        dsOTIFReport.Tables[0].Columns.Add("ToDate");
                        dsOTIFReport.Tables[0].Columns.Add("VendorName");
                        dsOTIFReport.Tables[0].Rows.OfType<DataRow>().ToList().ForEach(r => r["FromDate"] = sDateFrom);
                        dsOTIFReport.Tables[0].Rows.OfType<DataRow>().ToList().ForEach(r => r["ToDate"] = sDateTo);
                    }

                    oOTIFReportBAL = null;

                    foreach (UP_VendorBE oUP_VendorBE in oUP_VendorBEList)
                    {
                        try
                        {
                            sbMessage.Clear();
                            //dsOTIFReport.Tables[0].Rows.OfType<DataRow>().ToList().ForEach(r => r["VendorName"] = oUP_VendorBE.VendorName);

                            dv = dsOTIFReport.Tables[0].DefaultView;
                            dv.RowFilter = "MasterVendorID = '" + oUP_VendorBE.MasterParentVendorID.ToString() + "'";

                            if (dv.Count > 0)
                            {

                                dv[0]["VendorName"] = oUP_VendorBE.VendorName;

                                sbMessage.Append("\r\n");
                                sbMessage.Append("Report generation start for " + oUP_VendorBE.MasterParentVendorID.ToString());
                                sbMessage.Append("\r\n");
                                sbMessage.Append("****************************************************************************************");
                                sbMessage.Append("\r\n");
                                LogUtility.SaveTraceLogEntry(sbMessage);

                                //dsTemp = new DataSet();
                                //dsTemp.Tables.Add(dv.ToTable());


                                rds = new ReportDataSource("spOTIF_Reports", dv.ToTable()); // dsTemp.Tables[0]);                           
                                ReportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
                                ReportViewer1.LocalReport.Refresh();
                                ReportViewer1.LocalReport.DataSources.Clear();
                                ReportViewer1.LocalReport.DataSources.Add(rds);

                                ReportViewer1.LocalReport.ReportPath = path + @"ModuleUI\OnTimeInFull\RDLC\MasterVendorOtifReport.rdlc";

                                //reportParameter[0] = new ReportParameter("Vendor", oUP_VendorBE.VendorName);
                                //ReportViewer1.LocalReport.SetParameters(reportParameter);

                                byte[] bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                                ReportPath = ReportOutputPath + oUP_VendorBE.MasterParentVendorID + ".xls";
                                File.WriteAllBytes(ReportPath, bytes);

                                sbMessage.Clear();
                                sbMessage.Append("\r\n");
                                sbMessage.Append("Report MasterParentVendorID Id " + oUP_VendorBE.MasterParentVendorID.ToString() + " Successfully Generated at - " + DateTime.Now.ToString());
                                sbMessage.Append("\r\n");
                                LogUtility.SaveTraceLogEntry(sbMessage);

                                bytes = null;
                            }

                            // Logic to update the generated status in table.
                            oUP_VendorBE.Action = "UpdateOTIFMasterVendorReport";
                            oUP_VendorBE.MasterParentVendorID = oUP_VendorBE.MasterParentVendorID;
                            oUP_VendorBAL.UpdateOTIFMasterVendorReportBAL(oUP_VendorBE);
                        }
                        catch (Exception ex)
                        {
                            sbMessage.Clear();

                            LogUtility.SaveErrorLogEntry(ex);

                            sbMessage.Append("Report Generation Failed");
                            sbMessage.Append("\r\n");
                            sbMessage.Append("****************************************************************************************");
                            LogUtility.SaveTraceLogEntry(sbMessage);
                        }
                        finally
                        {
                            dv = null;
                            ReportPath = string.Empty;
                            iSleepCounter++;

                            if (iSleepCounter == 10)
                            {
                                System.Threading.Thread.Sleep(3);
                                iSleepCounter = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
        }

        //private void GenerateMasterVendorOTIF(List<UP_VendorBE> oUP_VendorBEList, DateTime sDateFrom, DateTime sDateTo) {
        //    StringBuilder sbMessage = new StringBuilder();
        //    UP_VendorBE oUP_VendorBE = new UP_VendorBE();
        //    byte[] bytes;

        //    OTIFReportBE oOTIFReportBE = new OTIFReportBE();
        //    OTIFReportBAL oOTIFReportBAL = new OTIFReportBAL();
        //    //DataSet dsOTIFReport,dsTemp;

        //    var dsOTIFReport = new DataSet();


        //    //dsTemp = dsOTIFReport;

        //    //Get Report Data
        //    oOTIFReportBE.Action = "OTIF_MasterVendorReportNewMeasure";
        //    oOTIFReportBE.DateFrom = sDateFrom;
        //    oOTIFReportBE.DateTo = sDateTo;
        //    oOTIFReportBE.PurchaseOrderDue = 1;
        //    oOTIFReportBE.PurchaseOrderReceived = 0;
        //    oOTIFReportBE.SelectedVendorIDs = oUP_VendorBE.VendorIDs;  //"5446,5449,7817,7820,16755,16756,16757,120,13658,16843";  // 

        //    dsOTIFReport = oOTIFReportBAL.GetOTIFReportBAL(oOTIFReportBE);
        //    oOTIFReportBAL = null;


        //  //  System.Threading.Tasks.Parallel.For(0, oUP_VendorBEList.Count-1, iCount => {

        //     for (int iCount = 0; iCount < oUP_VendorBEList.Count; iCount++) {
        //        oUP_VendorBE = oUP_VendorBEList[iCount];

        //        if (iCount % 100 == 0) {
        //            GC.Collect();
        //            //GC.WaitForPendingFinalizers();
        //        }

        //        try {

        //            var dv = dsOTIFReport.Tables[0].DefaultView;
        //            dv.RowFilter = "MasterVendorID = '" + oUP_VendorBE.MasterParentVendorID.ToString() + "'";
        //            if (dv.Count > 0) {

        //                sbMessage.Append("\r\n");
        //                sbMessage.Append("Report generation start for " + oUP_VendorBE.MasterParentVendorID.ToString());
        //                sbMessage.Append("\r\n");
        //                sbMessage.Append("****************************************************************************************");
        //                sbMessage.Append("\r\n");
        //                LogUtility.SaveTraceLogEntry(sbMessage);

        //                // Prepare RDLC
        //                ReportDataSource rds;
        //                ReportViewer ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
        //                ReportParameter[] reportParameter = new ReportParameter[9];

        //                #region ReportVariables

        //                Warning[] warnings;
        //                string[] streamIds;
        //                string mimeType = string.Empty;
        //                string encoding = string.Empty;
        //                string extension = string.Empty;
        //                string reportFileName = string.Empty;

        //                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
        //                path = path.Replace(@"\bin\debug", "");

        //                string ReportOutputPath;

        //                #endregion

        //                // Prepare RDLC
        //                rds = null;
        //                ReportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();

        //                reportParameter = new ReportParameter[5];
        //                reportParameter[0] = new ReportParameter("Vendor", oUP_VendorBE.VendorName);
        //                reportParameter[1] = new ReportParameter("DateFrom", sDateFrom.ToString("dd/MM/yyyy"));
        //                reportParameter[2] = new ReportParameter("DateTo", sDateTo.ToString("dd/MM/yyyy"));
        //                reportParameter[3] = new ReportParameter("Title", "NEW Measure");
        //                reportParameter[4] = new ReportParameter("EuropeanorLocal", "EuropeanorLocal");

        //                var dsTemp = new DataSet();
        //                dsTemp.Tables.Add(dv.ToTable());


        //                rds = new ReportDataSource("spOTIF_Reports", dsTemp.Tables[0]);


        //                ReportViewer1.LocalReport.ReportPath = path + @"ModuleUI\OnTimeInFull\RDLC\MasterVendorOtifReport.rdlc";
        //                ReportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
        //                ReportViewer1.LocalReport.Refresh();
        //                ReportViewer1.LocalReport.DataSources.Clear();
        //                ReportViewer1.LocalReport.DataSources.Add(rds);
        //                ReportViewer1.LocalReport.SetParameters(reportParameter);

        //                bytes = null;
        //                bytes = ReportViewer1.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

        //                //ReportViewer1.LocalReport.SetParameters(reportParameter);

        //                ReportOutputPath = path + @"ReportOutput\" + sDateFrom.Year.ToString() + @"\";
        //                if (!System.IO.Directory.Exists(ReportOutputPath))
        //                    System.IO.Directory.CreateDirectory(ReportOutputPath);

        //                ReportOutputPath = ReportOutputPath + oUP_VendorBE.MasterParentVendorID + ".xls";

        //                using (FileStream stream = new FileStream(ReportOutputPath, FileMode.Create, FileAccess.ReadWrite)) {
        //                    stream.Write(bytes, 0, bytes.Length);
        //                    stream.Close();
        //                }

        //                //File.WriteAllBytes(ReportOutputPath, bytes);

        //                dsTemp.Clear();
        //                dsTemp.Tables.Clear();


        //                sbMessage.Clear();
        //                sbMessage.Append("\r\n");
        //                sbMessage.Append("Report MasterParentVendorID Id " + oUP_VendorBE.MasterParentVendorID.ToString() + " Successfully Generated");
        //                sbMessage.Append("\r\n");

        //                LogUtility.SaveTraceLogEntry(sbMessage);

        //            }
        //        }
        //        catch (Exception ex) {
        //            LogUtility.SaveErrorLogEntry(ex);

        //            sbMessage.Append("Report Generation Failed");
        //            sbMessage.Append("\r\n");
        //            sbMessage.Append("****************************************************************************************");
        //            LogUtility.SaveTraceLogEntry(sbMessage);
        //        }
        //    }
        //}

    }
}

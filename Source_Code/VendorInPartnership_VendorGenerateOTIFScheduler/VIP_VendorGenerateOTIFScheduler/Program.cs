﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Data.SqlClient;
using Utilities;
using System.Text;
using System.Data;

namespace VIP_VendorGenerateOTIFScheduler
{

    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        //[STAThread]
        static int? otifSchedularId;
        static int? masterSchedularId;
        //static DateTime InputDate=new DateTime();

        static DateTime InputDate = DateTime.Now;

        static int dbMonth = 0;
        static int dbYear = 0;

        static string applicationNameOTIF = "OTIFReportScheduler";
        static string applicationNameOTIFMaster = "OTIFMasterReportScheduler";

        static DateTime toDateOTIF_History = new DateTime();
        static DateTime fromDateOTIF_History = new DateTime();

        static DateTime toDateOTIF_MasterHistory = new DateTime();
        static DateTime fromDateOTIF_MasterHistory = new DateTime();

        static string connString = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["sConn"]);

        static void Main()
        {
            try
            {
                bool isOTIFReportSchedulerInProcess = GetSchedulerInfo(applicationNameOTIF);
                bool isOTIFMasterReportSchedulerInProcess = GetSchedulerInfo(applicationNameOTIFMaster);

                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("Started at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                int iOTIFMonth = 0;
                int iMasterOTIFMonth = 0;


                int year = 0;

                DateTime dtDateFrom = new DateTime();
                DateTime dtDateTo = new DateTime();
                DateTime dtMasterOTIFDateTo = new DateTime();
                DateTime dtMasterOTIFDateFrom = new DateTime();


                if (dbMonth == 0)  //Automate process
                {
                    // When Get month from App.Config

                    //Set month for OTIF
                    iOTIFMonth = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["OTIFMonth"]);
                    if (iOTIFMonth > 0)
                        iOTIFMonth = DateTime.Now.AddMonths(-iOTIFMonth).Month;

                    //Set month for OTIF Master
                    iMasterOTIFMonth = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MasterOTIFMonth"]);
                    if (iMasterOTIFMonth > 0)
                        iMasterOTIFMonth = DateTime.Now.AddMonths(-iMasterOTIFMonth).Month;

                    //Set year for both OTIF  and OTIF Master

                    if (iOTIFMonth == 12)
                        year = DateTime.Now.Year - 1;
                    else
                        year = DateTime.Now.Year;
                }
                else   // manaual entry from web application
                {
                    // When Get month and year from Database

                    //Set Month
                    iOTIFMonth = dbMonth;
                    iMasterOTIFMonth = dbMonth;


                    //set year
                    if (dbYear > 0)
                        year = dbYear;
                    else
                        year = DateTime.Now.Year;
                }

                //DateTime dt = DateTime.Now.AddMonths(-iOTIFMonth);

                if (isOTIFReportSchedulerInProcess)
                {
                    // When OTIF Scheduler is already processing

                    //Set TO and FROM date from history for OTIF
                    dtDateTo = toDateOTIF_History;
                    dtDateFrom = fromDateOTIF_History;

                    iOTIFMonth = dtDateTo.Month;
                    year = dtDateTo.Year;
                }
                else
                {
                    if (iOTIFMonth > 0)
                    {
                        // When OTIF Scheduler is in new process
                        if (dbMonth == 0)
                        {
                            iOTIFMonth = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["OTIFMonth"]);
                            //Set TO and FROM date from Month and Year Value for OTIF                            
                        }

                        dtDateFrom = new DateTime(year, InputDate.AddMonths(-iOTIFMonth).Month, 1);
                        //dtDateFrom = new DateTime(year, 1, 1);
                        dtDateTo = new DateTime(dtDateFrom.Year, dtDateFrom.Month, dtDateFrom.AddMonths(1).AddDays(-1).Day);

                        //Insert Record in Scheduling App History 
                        InsertSchedulerInfoInAppHistory(dtDateFrom, dtDateTo, applicationNameOTIF, otifSchedularId);
                    }
                }


                if (isOTIFMasterReportSchedulerInProcess)
                {
                    // When OTIF Master Scheduler is already processing

                    //Set TO and FROM date from history for OTIF Master
                    dtMasterOTIFDateTo = toDateOTIF_MasterHistory;
                    dtMasterOTIFDateFrom = fromDateOTIF_MasterHistory;
                    iMasterOTIFMonth = dtMasterOTIFDateTo.Month;
                    year = dtMasterOTIFDateTo.Year;
                }
                else
                {
                    if (iMasterOTIFMonth > 0)
                    {
                        // When OTIF Master Scheduler is in new process

                        //Set TO and FROM date from Month and Year Value for OTIF Master
                        DateTime dt = new DateTime(year, iMasterOTIFMonth, 1);

                        dtMasterOTIFDateFrom = new DateTime(year, 1, 1);
                        if (year == DateTime.Now.Year)
                        {//change the date end of month for issue resolved 17/05/2018
                            DateTime lastDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1);
                            dtMasterOTIFDateTo = lastDay;
                        }
                        else
                        {
                            dtMasterOTIFDateTo = new DateTime(year, 12, 31);
                        }

                        //Insert Record in Scheduling App History 
                        InsertSchedulerInfoInAppHistory(dtMasterOTIFDateFrom, dtMasterOTIFDateTo, applicationNameOTIFMaster, masterSchedularId);
                    }
                }

                GenerateReport lGenerateReport = new GenerateReport();

                //comment it for testing 
                if (iOTIFMonth >= 1)
                { 
                    lGenerateReport.StartReportGeneration(dtDateFrom, dtDateTo, otifSchedularId, applicationNameOTIF);

                    if (!IsAnyPendingProcessByApplication(applicationNameOTIF, otifSchedularId))
                    {
                        //Update Scheduling App History
                        UpdateSchedulerInfoInAppHistory(applicationNameOTIF, otifSchedularId, DateTime.Now);
                    } 
                }

                GC.Collect();

                if (iMasterOTIFMonth >= 1)
                { //change the todate   

                    lGenerateReport.StartReportGenerationOTIFMasterVendor(dtMasterOTIFDateTo, masterSchedularId, applicationNameOTIFMaster);
                    if (!IsAnyPendingProcessByApplication(applicationNameOTIFMaster, masterSchedularId))
                    {
                        //Update Scheduling App History
                        UpdateSchedulerInfoInAppHistory(applicationNameOTIFMaster, masterSchedularId, DateTime.Now);
                        //this should be commented for some of OtifVendor of applicationNameOTIF may not be completed...
                        //UpdateSchedulingAppStatus(masterSchedularId);
                    }
                }

                if ((!IsAnyPendingProcessByApplication(applicationNameOTIF, otifSchedularId)) && (!IsAnyPendingProcessByApplication(applicationNameOTIFMaster, masterSchedularId)))
                {
                    UpdateSchedulerInfoInAppHistory(applicationNameOTIF, otifSchedularId, DateTime.Now);
                    UpdateSchedulerInfoInAppHistory(applicationNameOTIFMaster, masterSchedularId, DateTime.Now);
                    UpdateSchedulingAppStatus(masterSchedularId);
                }

                sbMessage.Clear();
                sbMessage.Append("Ended at              : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            catch
            {
                //Update Scheduling App History When Failed
                //UpdateSchedulerInfoInAppHistory(applicationNameOTIFMaster, masterSchedularId, (DateTime?)null);
                UpdateSchedulingAppStatus(masterSchedularId);
            }
        }

        private static bool GetSchedulerInfo(string applicationName)
        {
            if (IsSchedulerInProcess(applicationName))
            {
                return true;
            }
            else
            {
                SqlConnection sqlConnection = new SqlConnection(connString);
                SqlCommand command = new SqlCommand("spSchedulingApp", sqlConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Action", "GetTopOneScheduledJob");
                command.Parameters.AddWithValue("@ApplicationName", "OTIF");
                command.Parameters.AddWithValue("@SchedulerDate", DateTime.Now.Date);
                command.Parameters.AddWithValue("@SchedulerTime", DateTime.Now.TimeOfDay);
                sqlConnection.Open();
                DataTable dt = new DataTable();
                dt.Load(command.ExecuteReader());
                sqlConnection.Close();

                #region Old Query
                //SqlCommand vscSqlCommand = new SqlCommand();
                //SqlConnection vscSqlConnection = new SqlConnection(connString);
                //vscSqlCommand.CommandType = CommandType.Text;
                //vscSqlCommand.CommandText = "Select * from dbo.SchedulingApp where IsExecuted=0 and ApplicationName='OTIF'";
                //vscSqlCommand.Connection = vscSqlConnection;
                //vscSqlCommand.Connection.Open();
                //DataTable dt = new DataTable();
                //dt.Load(vscSqlCommand.ExecuteReader());
                //vscSqlCommand.Connection.Close();
                #endregion Old Query

                if (dt.Rows.Count > 0)
                {
                    dbMonth = Convert.ToInt32(dt.Rows[0]["Month"]);
                    dbYear = Convert.ToInt32(dt.Rows[0]["Year"]);
                    otifSchedularId = Convert.ToInt32(Convert.ToString(dt.Rows[0]["SchedulerId"]));
                    masterSchedularId = Convert.ToInt32(Convert.ToString(dt.Rows[0]["SchedulerId"]));
                    InputDate = Convert.ToDateTime(Convert.ToString(dt.Rows[0]["InputDate"]));
                }
                return false;
            }

        }

        private static bool IsAnyPendingProcessByApplication(string applicationName, int? SchedulerId)
        {
            bool result = false;
            SqlCommand otifSqlCommand = new SqlCommand();
            SqlConnection otifSqlConnection = new SqlConnection(connString);
            otifSqlCommand.CommandType = CommandType.Text;
            otifSqlCommand.CommandText = "Select dbo.[udfGetOTIFPendReportGeneratedStatusByApp]('" + applicationName + "','" + SchedulerId + "')";
            otifSqlCommand.Connection = otifSqlConnection;
            otifSqlCommand.Connection.Open();
            var PendingCount = otifSqlCommand.ExecuteScalar();
            otifSqlCommand.Connection.Close();
            if (Convert.ToInt16(PendingCount) > 0)
            {
                result = true;
            }
            return result;
        }

        private static bool IsSchedulerInProcess(string applicationName)
        {
            SqlConnection sqlConnection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand("spSchedulingApp", sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Action", "IsSchedulerInProcess");
            command.Parameters.AddWithValue("@ApplicationName", applicationName);
            sqlConnection.Open();
            DataTable dt = new DataTable();
            dt.Load(command.ExecuteReader());
            sqlConnection.Close();

            #region Old Query
            //SqlCommand vscSqlCommand = new SqlCommand();
            //SqlConnection vscSqlConnection = new SqlConnection(connString);
            //vscSqlCommand.CommandType = CommandType.StoredProcedure;
            //vscSqlCommand.CommandText = "Select * from MAS_SchedulingAppHistory Where ApplicationName='" + applicationName + "' And IsCompleted=0";
            //vscSqlCommand.Connection = vscSqlConnection;
            //vscSqlCommand.Connection.Open();
            //DataTable dt = new DataTable();
            //dt.Load(vscSqlCommand.ExecuteReader());
            //vscSqlCommand.Connection.Close();
            #endregion Old Query

            if (dt.Rows.Count > 0)
            {
                if (applicationName == applicationNameOTIF)
                {
                    fromDateOTIF_History = Convert.ToDateTime(dt.Rows[0]["FromDate"]);
                    toDateOTIF_History = Convert.ToDateTime(dt.Rows[0]["ToDate"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["SchedularId"])))
                        otifSchedularId = Convert.ToInt32(Convert.ToString(dt.Rows[0]["SchedularId"]));
                }
                else if (applicationName == applicationNameOTIFMaster)
                {
                    fromDateOTIF_MasterHistory = Convert.ToDateTime(dt.Rows[0]["FromDate"]);
                    toDateOTIF_MasterHistory = Convert.ToDateTime(dt.Rows[0]["ToDate"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["SchedularId"])))
                        masterSchedularId = Convert.ToInt32(Convert.ToString(dt.Rows[0]["SchedularId"]));
                }
                return true;
            }
            return false;
        }

        private static void InsertSchedulerInfoInAppHistory(DateTime fromDate, DateTime toDate, string applicationName, int? schedulerId)
        {
            SqlConnection sqlConnection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand("spSchedulingApp", sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Action", "InsertSchedulingHistory");
            command.Parameters.AddWithValue("@FromDate", fromDate);
            command.Parameters.AddWithValue("@ToDate", toDate);
            command.Parameters.AddWithValue("@ApplicationName", applicationName);
            command.Parameters.AddWithValue("@SchedulerId", schedulerId);
            command.Parameters.AddWithValue("@StartTime", DateTime.Now);
            sqlConnection.Open();
            command.ExecuteNonQuery();
            sqlConnection.Close();

            #region Old Query
            //SqlCommand vscSqlCommand = new SqlCommand();
            //SqlConnection vscSqlConnection = new SqlConnection(connString);
            //vscSqlCommand.CommandType = CommandType.Text;
            //vscSqlCommand.Connection = vscSqlConnection;
            //vscSqlCommand.Connection.Open();
            //vscSqlCommand.CommandText = "INSERT INTO MAS_SchedulingAppHistory(FromDate,ToDate,ApplicationName) VALUES('" + fromDate + "','" + toDate + "','" + applicationName + "')";
            //vscSqlCommand.ExecuteNonQuery();
            //vscSqlCommand.Connection.Close();
            #endregion Old Query
        }

        private static void UpdateSchedulerInfoInAppHistory(string applicationName, int? SchedularId, DateTime? EndTime)
        {
            SqlConnection sqlConnection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand("spSchedulingApp", sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Action", "UpdateSchedulingHistory");
            command.Parameters.AddWithValue("@ApplicationName", applicationName);
            command.Parameters.AddWithValue("@EndTime", EndTime);
            command.Parameters.AddWithValue("@SchedulerId", SchedularId);

            sqlConnection.Open();
            command.ExecuteNonQuery();
            sqlConnection.Close();

            #region Old Query
            //SqlCommand vscSqlCommand = new SqlCommand();
            //SqlConnection vscSqlConnection = new SqlConnection(connString);
            //vscSqlCommand.CommandType = CommandType.Text;
            //vscSqlCommand.Connection = vscSqlConnection;
            //vscSqlCommand.Connection.Open();
            //vscSqlCommand.CommandText = "UPDATE MAS_SchedulingAppHistory SET IsCompleted=1 WHERE ApplicationName='" + applicationName + "'";
            //vscSqlCommand.ExecuteNonQuery();
            //vscSqlCommand.Connection.Close();
            #endregion Old Query
        }

        private static void UpdateSchedulingAppStatus(int? SchedularId)
        {
            if (SchedularId != null && SchedularId > 0)
            {
                SqlConnection sqlConnection = new SqlConnection(connString);
                SqlCommand command = new SqlCommand("spSchedulingApp", sqlConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Action", "UpdateSchedulingAppStatus");
                command.Parameters.AddWithValue("@EndTime", DateTime.Now);
                command.Parameters.AddWithValue("@SchedulerId", SchedularId);
                sqlConnection.Open();
                command.ExecuteNonQuery();
                sqlConnection.Close();
            }
        }
    }
}

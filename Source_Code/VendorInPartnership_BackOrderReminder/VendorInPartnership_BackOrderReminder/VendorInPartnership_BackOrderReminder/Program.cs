﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogicLayer.ModuleBAL.StockOverview;
using BusinessEntities.ModuleBE.StockOverview;
using Utilities;
using BusinessEntities.ModuleBE.Languages.Languages;
using System.IO;
using BusinessEntities.ModuleBE.Languages;
using BusinessLogicLayer.ModuleBAL.Languages;
using System.Reflection;
using System.Globalization;
using System.Threading;
using System.Configuration;

namespace VendorInPartnership_BackOrderReminder {
    static class Program {

        [STAThread]
        static void Main() {
            SendBOPenaltyInitailComm();
            SendBOPenaltyMedaitorComm();            
        }

        private static void SendBOPenaltyMedaitorComm()
        {
            #region Send BO Penalty Communication Medaitor
            int VendorID = 0;
            try
            {

                StringBuilder sbMessage = new StringBuilder();         

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append(" BO Penalty Medaitor Communication Started at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);


                //string IsBOPenaltyEscal = System.Configuration.ConfigurationManager.AppSettings["IsBOPenaltyEscal"];

                BackOrderBAL oBackOrderVCBAL = new BackOrderBAL();
                BackOrderBE oBackOrderVCBE = new BackOrderBE();
                oBackOrderVCBE.Action = "GetMediatorEmailDetails";

                List<BackOrderBE> lstVendorCommList = oBackOrderVCBAL.GetMediatorCommDetailsBAL(oBackOrderVCBE);
                // find distinct vendorid
                List<int> vendorIDs = lstVendorCommList.Select(x => x.Vendor.VendorID).Distinct().ToList();

                for (int t = 0; t < vendorIDs.Count; t++)
                {
                    List<BackOrderBE> lstVendorCommListfilter = lstVendorCommList.Where(x => x.Vendor.VendorID == vendorIDs[t]).ToList();

                    if (lstVendorCommListfilter.Count > 0)
                    {

                        VendorID = lstVendorCommListfilter[0].Vendor.VendorID;
                            string StockPlannerName = string.Empty;
                            string StockPlannerContact = string.Empty;
                            StockPlannerName = lstVendorCommListfilter[0].Vendor.StockPlannerName;
                            StockPlannerContact = lstVendorCommListfilter[0].Vendor.StockPlannerContact;
                            BackOrderBAL oBackOrderVSBAL = new BackOrderBAL();
                            BackOrderBE oBackOrderVSBE = new BackOrderBE();
                            //oBackOrderVSBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();


                            //oBackOrderVSBE.Action = "CheckSkuStatusByVendor";
                            //oBackOrderVSBE.Vendor.VendorID = VendorID;
                            //bool IsAllVendorProcessed = oBackOrderVSBAL.CheckSkuStatusByVendorBAL(oBackOrderVSBE);

                            //if (IsAllVendorProcessed)
                            //{
                            List<BackOrderBE> lstVendorCommListByVendor = lstVendorCommListfilter.Where(x => x.Vendor.VendorID == VendorID).ToList();

                            BackOrderBAL oBackOrderVUBAL = new BackOrderBAL();
                            BackOrderBE oBackOrderVUBE = new BackOrderBE();
                            oBackOrderVUBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                            oBackOrderVUBE.Vendor.VendorID = VendorID;

                            string[] VendorAddress;
                            string[] Vendorlanguage;
                            string[] UserName;


                            string[] VendorData = new string[2];
                            VendorData = GetVendorOTIFContactWithLanguage(oBackOrderVUBE.Vendor.VendorID);

                            if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0]))
                            {

                                VendorAddress = VendorData[0].Split(',');
                                Vendorlanguage = VendorData[1].Split(',');
                                UserName = VendorData[2].Split(',');

                                for (int j = 0; j < VendorAddress.Length; j++)
                                {
                                    if (VendorAddress[j].Trim() != "")
                                    {
                                        // if (IsBOPenaltyEscal == "true")
                                        SendBOPenaltyMedaitorCommunication(lstVendorCommListByVendor, VendorAddress[j], Vendorlanguage[j], VendorID, "initialreminder", VendorData[0]);
                                    }
                                }                          

                        }                       

                    }
                    else
                    {
                        sbMessage.Clear();
                        sbMessage.Append("No Record Found for BO Vendor Medaitor Communication1");
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                    }                   
                }
                //Updating IsMedaitionEmailSent Flag for Selected VendorIDs
                string BOSelectedVendorIDs = string.Empty;
                List<int> SelectedVendorIDs = vendorIDs.Distinct().ToList();
                if (SelectedVendorIDs != null && SelectedVendorIDs.Count > 0)
                {
                    for (int BO = 0; BO < SelectedVendorIDs.Count; BO++)
                    {
                        BOSelectedVendorIDs += string.Format(",{0}", SelectedVendorIDs[BO]);
                    }
                }

                BackOrderBAL oBackOrderBOBAL = new BackOrderBAL();
                BackOrderBE oBackOrderBOBE = new BackOrderBE();

                oBackOrderBOBE.Action = "UpdateMediatorReminder";
                oBackOrderBOBE.SelectedVendorIDs = BOSelectedVendorIDs;
                int? Result = oBackOrderBOBAL.UpdateVendorIDBAL(oBackOrderBOBE);
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append(" BO Penalty Medaitor Communication Ended at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex, VendorID.ToString());
            }
            #endregion
        }

        private static void SendBOPenaltyInitailComm() {
            #region Send BO Penalty Communication 1
            int VendorID = 0;
            try {

                StringBuilder sbMessage = new StringBuilder();
                //  sendCommunication oSendCommunication = new sendCommunication();

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append(" BO Penalty Initial Reminder Communication Started at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);


                //string IsBOPenaltyEscal = System.Configuration.ConfigurationManager.AppSettings["IsBOPenaltyEscal"];

                BackOrderBAL oBackOrderVCBAL = new BackOrderBAL();
                BackOrderBE oBackOrderVCBE = new BackOrderBE();
                oBackOrderVCBE.Action = "GetBOinitialCommDetails";

                List<BackOrderBE> lstVendorCommList = oBackOrderVCBAL.GetBOinitialCommDetailsBAL(oBackOrderVCBE);
                // find distinct vendorid
                List<int> vendorIDs = lstVendorCommList.Select(x => x.Vendor.VendorID).ToList();

                if (lstVendorCommList.Count > 0) {
                    for (int i = 0; i < lstVendorCommList.Count; i++) {

                        VendorID = lstVendorCommList[i].Vendor.VendorID;
                        string StockPlannerName = string.Empty;
                        string StockPlannerContact = string.Empty;
                        StockPlannerName = lstVendorCommList[i].Vendor.StockPlannerName;
                        StockPlannerContact = lstVendorCommList[i].Vendor.StockPlannerContact;
                        BackOrderBAL oBackOrderVSBAL = new BackOrderBAL();
                        BackOrderBE oBackOrderVSBE = new BackOrderBE();
                        //oBackOrderVSBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();


                        //oBackOrderVSBE.Action = "CheckSkuStatusByVendor";
                        //oBackOrderVSBE.Vendor.VendorID = VendorID;
                        //bool IsAllVendorProcessed = oBackOrderVSBAL.CheckSkuStatusByVendorBAL(oBackOrderVSBE);

                        //if (IsAllVendorProcessed)
                        //{
                        List<BackOrderBE> lstVendorCommListByVendor = lstVendorCommList.Where(x => x.Vendor.VendorID == VendorID).ToList();

                        BackOrderBAL oBackOrderVUBAL = new BackOrderBAL();
                        BackOrderBE oBackOrderVUBE = new BackOrderBE();
                        oBackOrderVUBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                        oBackOrderVUBE.Vendor.VendorID = VendorID;

                        string[] VendorAddress;
                        string[] Vendorlanguage;
                        string[] UserName;


                        string[] VendorData = new string[2];
                        VendorData = GetVendorOTIFContactWithLanguage(oBackOrderVUBE.Vendor.VendorID);

                        if (VendorData.Length > 0 && !string.IsNullOrEmpty(VendorData[0])) {

                            VendorAddress = VendorData[0].Split(',');
                            Vendorlanguage = VendorData[1].Split(',');
                            UserName = VendorData[2].Split(',');

                            for (int j = 0; j < VendorAddress.Length; j++) {
                                if (VendorAddress[j] != " ") {
                                    // if (IsBOPenaltyEscal == "true")
                                    SendBOPenaltyCommunication(lstVendorCommListByVendor, VendorAddress[j], Vendorlanguage[j], VendorID, "initialreminder", VendorData[0], UserName[j], StockPlannerName: StockPlannerName, SPContact: StockPlannerContact);
                                }
                            }
                        }


                    }
                    //Updating IsVendorCommunicationSent Flag for Selected VendorIDs
                    string BOSelectedVendorIDs = string.Empty;
                    List<int> SelectedVendorIDs = vendorIDs.Distinct().ToList();
                    if (SelectedVendorIDs != null && SelectedVendorIDs.Count > 0) {
                        for (int BO = 0; BO < SelectedVendorIDs.Count; BO++) {
                            BOSelectedVendorIDs += string.Format(",{0}", SelectedVendorIDs[BO]);
                        }
                    }

                    BackOrderBAL oBackOrderBOBAL = new BackOrderBAL();
                    BackOrderBE oBackOrderBOBE = new BackOrderBE();

                    oBackOrderBOBE.Action = "UpdateVendorByInitialReminder";
                    oBackOrderBOBE.SelectedVendorIDs = BOSelectedVendorIDs;
                    int? Result = oBackOrderBOBAL.UpdateVendorIDBAL(oBackOrderBOBE);

                }
                else {
                    sbMessage.Clear();
                    sbMessage.Append("No Record Found for BO Vendor Reminder Communication1");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                }

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append(" BO Penalty Reminder Communication Ended at            : " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex, VendorID.ToString());
            }
            #endregion
        }
        public static string[] GetVendorOTIFContactWithLanguage(int VendorID) {
            string[] VendorData = new string[3];
            BackOrderBAL oBackOrderVUBAL = new BackOrderBAL();
            BackOrderBE oBackOrderVUBE = new BackOrderBE();
            oBackOrderVUBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            string VendorEmail = string.Empty;
            List<BackOrderBE> lstVendorDetails = null;

            oBackOrderVUBE.Action = "GetVendorOTIFContact";
            oBackOrderVUBE.Vendor.VendorID = VendorID;

            lstVendorDetails = oBackOrderVUBAL.GetVendorContactDetailsBAL(oBackOrderVUBE);
            if (lstVendorDetails != null && lstVendorDetails.Count > 0) {

                foreach (BackOrderBE item in lstVendorDetails) {
                    if (item.Vendor.VendorContactEmail == null || item.Vendor.VendorContactEmail == "") {
                        VendorData = GetVendorDefaultEmailWithLanguageID(VendorID);
                    }
                    else {
                        VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                        VendorData[1] += item.Vendor.LanguageID.ToString() + ",";
                        VendorData[2] += item.User.UserName + ",";
                    }
                }
            }
            else {
                VendorData = GetVendorDefaultEmailWithLanguageID(VendorID);
            }
            return VendorData;
        }

        private static string[] GetVendorDefaultEmailWithLanguageID(int VendorID) {
            string[] VendorData = new string[3];
            BackOrderBAL oBackOrderVUBAL = new BackOrderBAL();
            BackOrderBE oBackOrderVUBE = new BackOrderBE();
            oBackOrderVUBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            List<BackOrderBE> lstVendorDetails = null;
            string VendorEmail = string.Empty;
            oBackOrderVUBE.Action = "GetContactDetails"; // from Up_vendor table
            oBackOrderVUBE.Vendor.VendorID = VendorID;

            VendorData[0] = "";
            VendorData[1] = "";

            lstVendorDetails = oBackOrderVUBAL.GetVendorContactDetailsBAL(oBackOrderVUBE);

            foreach (BackOrderBE item in lstVendorDetails) {
                if (!string.IsNullOrEmpty(item.Vendor.VendorContactEmail)) {
                    VendorData[0] += item.Vendor.VendorContactEmail.ToString() + ",";
                    VendorData[1] += item.Vendor.LanguageID.ToString() + ",";
                }
            }
            if (VendorData[0] == "") {
                VendorData[0] = System.Configuration.ConfigurationManager.AppSettings["DefaultEmailAddress"] + ","; ;
                VendorData[1] = "1,";
                VendorData[2] = lstVendorDetails[0].Vendor.VendorName + ",";
            }

            return VendorData;
        }

        public static string getGlobalResourceValue(string key, string UserLanguage) {
            string ResourceValues = string.Empty;
            var lstLanguageTags = new List<MAS_LanguageTagsBE>();
            try {
                //Get All LanguageTags From DB
                MAS_LanguageTagsBE oMAS_LanguageTagsBE = new MAS_LanguageTagsBE();
                MAS_LanguageTagsBAL oMAS_LanguageTagsBAL = new MAS_LanguageTagsBAL();
                if (lstLanguageTags != null && lstLanguageTags.Count <= 0) {
                    oMAS_LanguageTagsBE.Action = "GetAllLanguageTags";
                    lstLanguageTags = oMAS_LanguageTagsBAL.GetAllLanguageTagsBAL(oMAS_LanguageTagsBE);
                    var lstResult = lstLanguageTags.Where(x => x.LanguageKey.ToLower() == key.ToLower()).ToList();
                    if (lstResult != null && lstResult.Count > 0) {
                        Type type = lstResult[0].GetType();
                        PropertyInfo prop = type.GetProperty(UserLanguage);
                        if (prop != null) {
                            ResourceValues = Convert.ToString(prop.GetValue(lstResult[0], null));
                        }
                    }
                }
                else {

                    var lstResult = lstLanguageTags.Where(x => x.LanguageKey.ToLower() == key.ToLower()).ToList();
                    if (lstResult != null && lstResult.Count > 0) {
                        Type type = lstResult[0].GetType();
                        PropertyInfo prop = type.GetProperty(UserLanguage);
                        if (prop != null) {
                            ResourceValues = Convert.ToString(prop.GetValue(lstResult[0], null));
                        }
                    }
                }
            }

            catch (Exception ex) {

            }
            return ResourceValues;
        }
        public static void SendBOPenaltyCommunication(List<BackOrderBE> lstPenaltyInfo, string toAddress, string language, int VendorId, string CommunicationType, string VendorEmailList, string username = "", string StockPlannerName = "", string SPContact = "") {
            string fromAddress = System.Configuration.ConfigurationManager.AppSettings["fromAddress"] as string;
            string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]);
            string sPortalLinkNew = Convert.ToString(ConfigurationManager.AppSettings["PortallinkNew"]);

            string SelectedBOReportingIDs = string.Empty;
            if (!string.IsNullOrEmpty(toAddress)) {
                #region Logic to Save & Send email ...
                var oBackOrderBAL = new BackOrderBAL();
                var oLanguages = oBackOrderBAL.GetLanguages();

                foreach (MAS_LanguageBE objLanguage in oLanguages) {
                    bool MailSentInLanguage = false;
                    if (objLanguage.LanguageID.Equals(Convert.ToInt32(language)))
                        MailSentInLanguage = true;


                    var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                    path = path.Replace(@"\bin\debug", "");
                    var templateFile = string.Empty; ;
                    if (CommunicationType.ToLower() == "initialreminder") {
                        templateFile = string.Format(@"{0}emailtemplates/BoReminderCommunication1.english.htm", path);
                    }

                    #region  Prepairing html body format ...
                    string htmlBody = null;

                    #region InitialComm HTML
                    if (CommunicationType.ToLower() == "initialreminder") {
                        using (StreamReader sReader = new StreamReader(templateFile.ToLower())) {
                            htmlBody = sReader.ReadToEnd();
                            htmlBody = htmlBody.Replace("{BoInitialEmail2}", getGlobalResourceValue("BoInitialEmail2", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{BoInitialEmail3}", getGlobalResourceValue("BoInitialEmail3", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{BoInitialEmail4}", getGlobalResourceValue("BoInitialEmail4", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{BoInitialEmail5}", getGlobalResourceValue("BoInitialEmail5", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{BoInitialEmail6}", getGlobalResourceValue("BoInitialEmail6", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", getGlobalResourceValue("Yoursfaithfully", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{StockPlannerName}", StockPlannerName);

                            if (!string.IsNullOrEmpty(SPContact) && !string.IsNullOrWhiteSpace(SPContact))
                                htmlBody = htmlBody.Replace("{StockPlannerContactNo}", SPContact);
                            else
                                htmlBody = htmlBody.Replace("{StockPlannerContactNo}", string.Empty);

                        }
                    }
                    #endregion

                    #endregion

                    #region Sending and saving email details ...

                    var BoInitialEmailSubject1 = getGlobalResourceValue("BoInitialEmail1", objLanguage.Language);

                    var oBackOrderMailBE = new BackOrderMailBE();
                    oBackOrderMailBE.Action = "AddCommunication";
                    oBackOrderMailBE.BOReportingID = 0;
                    oBackOrderMailBE.CommunicationType = "VendorCommunication1_InitialReminder";
                    oBackOrderMailBE.Subject = BoInitialEmailSubject1;
                    oBackOrderMailBE.SentTo = toAddress;
                    oBackOrderMailBE.SentDate = DateTime.Now;
                    oBackOrderMailBE.Body = htmlBody;
                    oBackOrderMailBE.SendByID = 0;
                    oBackOrderMailBE.LanguageId = objLanguage.LanguageID;
                    oBackOrderMailBE.MailSentInLanguage = MailSentInLanguage;
                    oBackOrderMailBE.CommunicationStatus = "InitialBOReminderFromScheduler";
                    oBackOrderMailBE.VendorId = VendorId;
                    oBackOrderMailBE.VendorEmailIds = VendorEmailList;


                    BackOrderBAL oBackOrderCommBAL = new BackOrderBAL();
                    var CommId = oBackOrderCommBAL.AddCommunicationDetailBAL(oBackOrderMailBE);
                    if (MailSentInLanguage) {
                        var emailToAddress = toAddress;
                        var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                        var emailToSubject = BoInitialEmailSubject1;
                        var emailBody = htmlBody;
                        Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                        oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                    }
                    #endregion
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");  // English
                }
                #endregion
            }
        }



        public static void SendBOPenaltyMedaitorCommunication(List<BackOrderBE> lstBackOrderBE,string toAddress ,string language, int VendorId,string CommunicationType,string VendorEmailList)
        {
            string fromAddress = System.Configuration.ConfigurationManager.AppSettings["fromAddress"] as string;
            string sPortalLink = Convert.ToString(ConfigurationManager.AppSettings["Portallink"]);
            string sPortalLinkNew = Convert.ToString(ConfigurationManager.AppSettings["PortallinkNew"]);
            BackOrderBE oBackOrderBE = new BackOrderBE();
            oBackOrderBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
            oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
            oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
            string SelectedBOReportingIDs = string.Empty;
            if (!string.IsNullOrEmpty(toAddress))
            {
                #region Logic to Save & Send email ...
                var oBackOrderBAL = new BackOrderBAL();
                var oLanguages = oBackOrderBAL.GetLanguages();

                foreach (MAS_LanguageBE objLanguage in oLanguages)
                {
                    bool MailSentInLanguage = false;
                    if (objLanguage.LanguageID.Equals(Convert.ToInt32(language)))
                        MailSentInLanguage = true;


                    var path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                    path = path.Replace(@"\bin\debug", "");
                    var templateFile = string.Empty; ;
                    if (CommunicationType.ToLower() == "initialreminder")
                    {
                        templateFile = string.Format(@"{0}emailtemplates/VendorCommunication2.english.htm", path);
                    }

                    #region  Prepairing html body format ...
                    string htmlBody = null;

                    #region InitialComm HTML
                    if (CommunicationType.ToLower() == "initialreminder")
                    {
                        using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                        {
                            htmlBody = sReader.ReadToEnd();                  

                            htmlBody = htmlBody.Replace("{VendorNumber}", lstBackOrderBE[0].Vendor.Vendor_No);
                            htmlBody = htmlBody.Replace("{VendorName}", lstBackOrderBE[0].Vendor.VendorName);
                            htmlBody = htmlBody.Replace("{VendorAddress1}", lstBackOrderBE[0].Vendor.address1);
                            htmlBody = htmlBody.Replace("{VendorAddress2}", lstBackOrderBE[0].Vendor.address2);
                            if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.VMPPIN) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.VMPPIN))
                                htmlBody = htmlBody.Replace("{VMPPIN}", lstBackOrderBE[0].Vendor.VMPPIN);
                            else
                                htmlBody = htmlBody.Replace("{VMPPIN}", string.Empty);

                            if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.VMPPOU) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.VMPPOU))
                                htmlBody = htmlBody.Replace("{VMPPOU}", lstBackOrderBE[0].Vendor.VMPPOU);
                            else
                                htmlBody = htmlBody.Replace("{VMPPOU}", string.Empty);

                            if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.city) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.city))
                                htmlBody = htmlBody.Replace("{VendorCity}", lstBackOrderBE[0].Vendor.city);
                            else
                                htmlBody = htmlBody.Replace("{VendorCity}", string.Empty);

                            htmlBody = htmlBody.Replace("{DearSirMadam}", getGlobalResourceValue("DearSirMadam", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{Date}", getGlobalResourceValue("Date", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{DateValue}", DateTime.Now.ToString("dd/MM/yyyy"));

                            htmlBody = htmlBody.Replace("{VendorCommunication2Message1}", getGlobalResourceValue("VendorCommunication2Message1", objLanguage.Language));
                            htmlBody = htmlBody.Replace("{VendorCommunication2Message2}", getGlobalResourceValue("VendorCommunication2Message2", objLanguage.Language));
                            //{gridValue}
                            #region Logic for creating SKU detail Grid
                            StringBuilder sSkuDetail = new StringBuilder();
                            sSkuDetail.Append("<table style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=100%'>");
                            string OurCode = getGlobalResourceValue("OurCode", objLanguage.Language);
                            string VendorCode = getGlobalResourceValue("VendorCode", objLanguage.Language);
                            string Description = getGlobalResourceValue("Description", objLanguage.Language);
                            string DateBOIncurred = getGlobalResourceValue("DateBOIncurred", objLanguage.Language);
                            string Site2 = getGlobalResourceValue("Site", objLanguage.Language);
                            string CountOfBoIncurred = getGlobalResourceValue("CountofBO(s)Incurred", objLanguage.Language);
                            string OriginalPenaltyValue = getGlobalResourceValue("OriginalPenaltyValue", objLanguage.Language);
                            string DisputedValue = getGlobalResourceValue("DisputedValue", objLanguage.Language);
                            string RevisedPenaltyCharge = getGlobalResourceValue("RevisedPenaltyCharge", objLanguage.Language);
                            sSkuDetail.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='10%'>" + OurCode + "</cc1:ucLabel></td><td width='10%'>" + VendorCode + "</td><td width='20%'>" + Description + "</td><td  width='10%'>" + DateBOIncurred + "</td><td  width='10%'>" + Site2 + "</td><td  width='10%'>" + CountOfBoIncurred + "</td><td  width='10%'>" + OriginalPenaltyValue + "</td><td  width='10%'>" + DisputedValue + "</td><td  width='10%'>" + RevisedPenaltyCharge + "</td></tr>");
                            for (int i = 0; i < lstBackOrderBE.Count; i++)
                            {

                                sSkuDetail.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + lstBackOrderBE[i].SKU.OD_SKU_NO + "</td><td>" + lstBackOrderBE[i].SKU.Vendor_Code + "</td><td>" + lstBackOrderBE[i].SKU.DESCRIPTION);
                                sSkuDetail.Append("</td><td>" + lstBackOrderBE[i].BOIncurredDate.Value.ToString("dd/MM/yyyy") + "</td><td>" + lstBackOrderBE[i].SiteName + "</td><td>" + lstBackOrderBE[i].NoofBO + "</td>");
                                sSkuDetail.Append("<td>" + lstBackOrderBE[i].PotentialPenaltyCharge + "</td><td>" + lstBackOrderBE[i].DisputedValue + "</td><td>" + lstBackOrderBE[i].RevisedPenalty + "</td></tr>");                              
                                
                            }
                            sSkuDetail.Append("</table>");
                            #endregion

                            htmlBody = htmlBody.Replace(" {GridValue}", "<tr><td style='font-family:Arial;font-size:12' colspan='2'><br />" + sSkuDetail + "</td></tr>");

                            htmlBody = htmlBody.Replace("{VendorCommunication2Message3}", getGlobalResourceValue("VendorCommunication2Message3", objLanguage.Language));

                            //Penalty Mgmt change
                            htmlBody = htmlBody.Replace("{VendorCommunication2Message4}", getGlobalResourceValue("VendorCommunication2Message4", objLanguage.Language));

                            htmlBody = htmlBody.Replace("{Yoursfaithfully}", getGlobalResourceValue("Yoursfaithfully", objLanguage.Language));


                            htmlBody = htmlBody.Replace("{StockPlannerName}", lstBackOrderBE[0].Vendor.StockPlannerName);

                            if (!string.IsNullOrEmpty(lstBackOrderBE[0].Vendor.StockPlannerContact) && !string.IsNullOrWhiteSpace(lstBackOrderBE[0].Vendor.StockPlannerContact))
                                htmlBody = htmlBody.Replace("{StockPlannerContactNo}", lstBackOrderBE[0].Vendor.StockPlannerContact);
                            else
                                htmlBody = htmlBody.Replace("{StockPlannerContactNo}", string.Empty);


                            var apAddress = string.Empty;
                            if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                                && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4)
                                && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress5) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress6))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4},&nbsp;{5}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                    lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4, lstBackOrderBE[0].APAddress5, lstBackOrderBE[0].APAddress6);
                            }
                            else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                                && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4)
                                && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress5))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3},&nbsp;{4}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                    lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4, lstBackOrderBE[0].APAddress5);
                            }
                            else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                               && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress4))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2},&nbsp;{3}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                    lstBackOrderBE[0].APAddress3, lstBackOrderBE[0].APAddress4);
                            }
                            else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2)
                               && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress3))
                            {
                                apAddress = string.Format("{0},&nbsp;{1},&nbsp;{2}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2,
                                    lstBackOrderBE[0].APAddress3);
                            }
                            else if (!string.IsNullOrEmpty(lstBackOrderBE[0].APAddress1) && !string.IsNullOrEmpty(lstBackOrderBE[0].APAddress2))
                            {
                                apAddress = string.Format("{0},&nbsp;{1}", lstBackOrderBE[0].APAddress1, lstBackOrderBE[0].APAddress2);
                            }
                            else
                            {
                                apAddress = lstBackOrderBE[0].APAddress1;
                            }

                            if (!string.IsNullOrEmpty(lstBackOrderBE[0].APPincode))
                                apAddress = string.Format("{0},&nbsp;{1}", apAddress, lstBackOrderBE[0].APPincode);

                            htmlBody = htmlBody.Replace("{APClerkAddress}", apAddress);                           

                        }
                    }
                    #endregion

                    #endregion

                    #region Sending and saving email details ...

                    var NewVendorCommunication2EmailSubject = getGlobalResourceValue("NewVendorCommunication2EmailSubject", objLanguage.Language);

                    var oBackOrderMailBE = new BackOrderMailBE();
                    oBackOrderMailBE.Action = "AddCommunication";
                    oBackOrderMailBE.BOReportingID = 0;
                    oBackOrderMailBE.CommunicationType = "Vendor Communication 2";
                    oBackOrderMailBE.Subject = NewVendorCommunication2EmailSubject;
                    oBackOrderMailBE.SentTo = toAddress;
                    oBackOrderMailBE.SentDate = DateTime.Now;
                    oBackOrderMailBE.Body = htmlBody;
                    oBackOrderMailBE.SendByID = 0;
                    oBackOrderMailBE.LanguageId = objLanguage.LanguageID;
                    oBackOrderMailBE.MailSentInLanguage = MailSentInLanguage;
                    oBackOrderMailBE.CommunicationStatus = "Initial";
                    oBackOrderMailBE.VendorId = VendorId;
                    oBackOrderMailBE.VendorEmailIds = VendorEmailList;


                    BackOrderBAL oBackOrderCommBAL = new BackOrderBAL();
                    var CommId = oBackOrderCommBAL.AddCommunicationDetailBAL(oBackOrderMailBE);
                    if (MailSentInLanguage)
                    {
                        var emailToAddress = toAddress;
                        var emailFromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                        var emailToSubject = NewVendorCommunication2EmailSubject;
                        var emailBody = htmlBody;
                        Utilities.clsEmail oclsEmail = new Utilities.clsEmail();
                        oclsEmail.sendMail(emailToAddress, emailBody, emailToSubject, emailFromAddress, true);
                    }
                    #endregion
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");  // English
                }
                #endregion
            }
        }
    }


}

﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using Utilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings {
    public class MASSIT_TimeWindowDAL : BaseDAL {

        public string GetWeekSetupDAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            string WeekSetupError = string.Empty;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASSIT_TimeWindowBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_TimeWindowBE.SiteID);
                param[index++] = new SqlParameter("@ScheduleDate", oMASSIT_TimeWindowBE.ScheduleDate);
                param[index++] = new SqlParameter("@MaximumPallets", oMASSIT_TimeWindowBE.MaximumPallets);
                param[index++] = new SqlParameter("@MaximumCartons", oMASSIT_TimeWindowBE.MaximumCartons);
                param[index++] = new SqlParameter("@MaximumLines", oMASSIT_TimeWindowBE.MaximumLines);
                param[index++] = new SqlParameter("@MaximumLift", oMASSIT_TimeWindowBE.MaximumLift);

                WeekSetupError = Convert.ToString(SqlHelper.ExecuteScalar(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_TimeWindow", param));
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Convert.ToString(WeekSetupError);
        }

        public List<MASSIT_TimeWindowBE> GetDoorNoDetailsBAL(int SiteID)
        {
            DataTable dt = new DataTable();
            List<MASSIT_TimeWindowBE> olstMASSIT_TimeWindowBE = new List<MASSIT_TimeWindowBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", "GetGetDoorNo");
                param[index++] = new SqlParameter("@SiteID", SiteID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(),
                    CommandType.StoredProcedure, "spMASSIT_TimeWindow", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_TimeWindowBE oNewMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
                    oNewMASSIT_TimeWindowBE.SiteDoorNumberID = dr["SiteDoorNumberID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorNumberID"]) : 0;
                    oNewMASSIT_TimeWindowBE.DoorNumber = dr["DoorNumber"] != DBNull.Value ? dr["DoorNumber"].ToString() : string.Empty;
                   
                    olstMASSIT_TimeWindowBE.Add(oNewMASSIT_TimeWindowBE);

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return olstMASSIT_TimeWindowBE;
        }

        public MASSIT_TimeWindowBE GetWindowDetailDAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            DataTable dt = new DataTable();
            MASSIT_TimeWindowBE oNewMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", oMASSIT_TimeWindowBE.Action);
                param[index++] = new SqlParameter("@ExcludedVendorIDs", oMASSIT_TimeWindowBE.ExcludedVendorIDs);
                param[index++] = new SqlParameter("@ExcludedCarrierIDs", oMASSIT_TimeWindowBE.ExcludedCarrierIDs);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_TimeWindowBE.SiteID);
                param[index++] = new SqlParameter("@ScheduleDate", oMASSIT_TimeWindowBE.ScheduleDate);
                param[index++] = new SqlParameter("@MaximumPallets", oMASSIT_TimeWindowBE.MaximumPallets);
                param[index++] = new SqlParameter("@MaximumCartons", oMASSIT_TimeWindowBE.MaximumCartons);
                param[index++] = new SqlParameter("@MaximumLines", oMASSIT_TimeWindowBE.MaximumLines);
                param[index++] = new SqlParameter("@TimeWindowID", oMASSIT_TimeWindowBE.TimeWindowID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(),
                    CommandType.StoredProcedure, "spMASSIT_TimeWindow", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    oNewMASSIT_TimeWindowBE.TimeWindowID = dr["TimeWindowID"] != DBNull.Value ? Convert.ToInt32(dr["TimeWindowID"]) : 0;
                    oNewMASSIT_TimeWindowBE.TotalHrsMinsAvailable = dr["TotalHrsMinsAvailable"] != DBNull.Value ? dr["TotalHrsMinsAvailable"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.Priority = dr["Priority"] != DBNull.Value ? Convert.ToInt32(dr["Priority"]) : 0;
                    oNewMASSIT_TimeWindowBE.MaximumTime = dr["MaximumTime"] != DBNull.Value ? dr["MaximumTime"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.IsCheckSKUTable = dr["IsCheckSKUTable"] != DBNull.Value ? Convert.ToBoolean(dr["IsCheckSKUTable"]) : false;
                    oNewMASSIT_TimeWindowBE.DoorNumber = dr["DoorNumber"] != DBNull.Value ? dr["DoorNumber"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.StartTime = dr["StartSlotTime"] != DBNull.Value ? dr["StartSlotTime"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.EndTime = dr["EndSlotTime"] != DBNull.Value ? dr["EndSlotTime"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.MaximumPallets = dr["MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["MaximumPallets"]) : 0;
                    oNewMASSIT_TimeWindowBE.MaximumCartons = dr["MaximumCartons"] != DBNull.Value ? Convert.ToInt32(dr["MaximumCartons"]) : 0;
                    oNewMASSIT_TimeWindowBE.SiteDoorNumberID = dr["SiteDoorNumberID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorNumberID"]) : 0;
                    oNewMASSIT_TimeWindowBE.SiteDoorTypeID = dr["SiteDoorTypeID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorTypeID"]) : 0;
                    oNewMASSIT_TimeWindowBE.StartSlotTimeID = dr["StartSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["StartSlotTimeID"]) : 0;
                    oNewMASSIT_TimeWindowBE.EndSlotTimeID = dr["EndSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["EndSlotTimeID"]) : 0;

                    oNewMASSIT_TimeWindowBE.WindowName = dr["WindowName"] != DBNull.Value ? dr["WindowName"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.MaxVolumeError = dr["MaxVolumeError"] != DBNull.Value ? dr["MaxVolumeError"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.MaxDailyCapacityError = dr["MaxDailyCapacityError"] != DBNull.Value ? dr["MaxDailyCapacityError"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.MaxDailyDeliveryError = dr["MaxDailyDeliveryError"] != DBNull.Value ? dr["MaxDailyDeliveryError"].ToString() : string.Empty;

                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oNewMASSIT_TimeWindowBE;
        }

        public List<MASSIT_TimeWindowBE> GetAlternateWindowDAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_TimeWindowBE> olstMASSIT_TimeWindowBE = new List<MASSIT_TimeWindowBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oMASSIT_TimeWindowBE.Action);
                param[index++] = new SqlParameter("@ExcludedVendorIDs", oMASSIT_TimeWindowBE.ExcludedVendorIDs);
                param[index++] = new SqlParameter("@ExcludedCarrierIDs", oMASSIT_TimeWindowBE.ExcludedCarrierIDs);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_TimeWindowBE.SiteID);
                param[index++] = new SqlParameter("@ScheduleDate", oMASSIT_TimeWindowBE.ScheduleDate);
                param[index++] = new SqlParameter("@MaximumPallets", oMASSIT_TimeWindowBE.MaximumPallets);
                param[index++] = new SqlParameter("@MaximumCartons", oMASSIT_TimeWindowBE.MaximumCartons);
                param[index++] = new SqlParameter("@MaximumLines", oMASSIT_TimeWindowBE.MaximumLines);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(),
                    CommandType.StoredProcedure, "spMASSIT_TimeWindow", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_TimeWindowBE oNewMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();
                    oNewMASSIT_TimeWindowBE.TimeWindowID = dr["TimeWindowID"] != DBNull.Value ? Convert.ToInt32(dr["TimeWindowID"]) : 0;
                    oNewMASSIT_TimeWindowBE.WindowName = dr["WindowName"] != DBNull.Value ? dr["WindowName"].ToString() : string.Empty;
                    olstMASSIT_TimeWindowBE.Add(oNewMASSIT_TimeWindowBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return olstMASSIT_TimeWindowBE;
        }

        public List<MASSIT_TimeWindowBE> GetProposedWindowDAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_TimeWindowBE> olstMASSIT_TimeWindowBE = new List<MASSIT_TimeWindowBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[9];
                param[index++] = new SqlParameter("@Action", oMASSIT_TimeWindowBE.Action);
                param[index++] = new SqlParameter("@ExcludedVendorIDs", oMASSIT_TimeWindowBE.ExcludedVendorIDs);
                param[index++] = new SqlParameter("@ExcludedCarrierIDs", oMASSIT_TimeWindowBE.ExcludedCarrierIDs);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_TimeWindowBE.SiteID);
                param[index++] = new SqlParameter("@ScheduleDate", oMASSIT_TimeWindowBE.ScheduleDate);
                param[index++] = new SqlParameter("@MaximumPallets", oMASSIT_TimeWindowBE.MaximumPallets);
                param[index++] = new SqlParameter("@MaximumCartons", oMASSIT_TimeWindowBE.MaximumCartons);
                param[index++] = new SqlParameter("@MaximumLines", oMASSIT_TimeWindowBE.MaximumLines);
                param[index++] = new SqlParameter("@BookingRef", oMASSIT_TimeWindowBE.BookingRef);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(),
                    CommandType.StoredProcedure, "spMASSIT_TimeWindow", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_TimeWindowBE oNewMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

                    oNewMASSIT_TimeWindowBE.TimeWindowID = dr["TimeWindowID"] != DBNull.Value ? Convert.ToInt32(dr["TimeWindowID"]) : 0;
                    oNewMASSIT_TimeWindowBE.TotalHrsMinsAvailable = dr["TotalHrsMinsAvailable"] != DBNull.Value ? dr["TotalHrsMinsAvailable"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.Priority = dr["Priority"] != DBNull.Value ? Convert.ToInt32(dr["Priority"]) : 0;
                    oNewMASSIT_TimeWindowBE.MaximumTime = dr["MaximumTime"] != DBNull.Value ? dr["MaximumTime"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.IsCheckSKUTable = dr["IsCheckSKUTable"] != DBNull.Value ? Convert.ToBoolean(dr["IsCheckSKUTable"]) : false;
                    oNewMASSIT_TimeWindowBE.DoorNumber = dr["DoorNumber"] != DBNull.Value ? dr["DoorNumber"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.StartTime = dr["StartSlotTime"] != DBNull.Value ? dr["StartSlotTime"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.EndTime = dr["EndSlotTime"] != DBNull.Value ? dr["EndSlotTime"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.MaximumPallets = dr["MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["MaximumPallets"]) : 0;
                    oNewMASSIT_TimeWindowBE.MaximumCartons = dr["MaximumCartons"] != DBNull.Value ? Convert.ToInt32(dr["MaximumCartons"]) : 0;
                    oNewMASSIT_TimeWindowBE.SiteDoorNumberID = dr["SiteDoorNumberID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorNumberID"]) : 0;
                    oNewMASSIT_TimeWindowBE.SiteDoorTypeID = dr["SiteDoorTypeID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorTypeID"]) : 0;
                    oNewMASSIT_TimeWindowBE.StartSlotTimeID = dr["StartSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["StartSlotTimeID"]) : 0;
                    oNewMASSIT_TimeWindowBE.EndSlotTimeID = dr["EndSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["EndSlotTimeID"]) : 0;
                    oNewMASSIT_TimeWindowBE.BeforeOrderBYId = dr["BeforeOrderBYId"] != DBNull.Value ? Convert.ToInt32(dr["BeforeOrderBYId"]) : 0;
                
                    olstMASSIT_TimeWindowBE.Add(oNewMASSIT_TimeWindowBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return olstMASSIT_TimeWindowBE;
        }

        public int? addEditTimeWindowDAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[26];
                param[index++] = new SqlParameter("@Action", oMASSIT_TimeWindowBE.Action);
                param[index++] = new SqlParameter("@TimeWindowID", oMASSIT_TimeWindowBE.TimeWindowID);
                param[index++] = new SqlParameter("@WindowName", oMASSIT_TimeWindowBE.WindowName);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_TimeWindowBE.SiteID);
                param[index++] = new SqlParameter("@Weekday", oMASSIT_TimeWindowBE.Weekday);
                param[index++] = new SqlParameter("@StartTimeWeekday", oMASSIT_TimeWindowBE.StartTimeWeekday);
                param[index++] = new SqlParameter("@EndTimeWeekday", oMASSIT_TimeWindowBE.EndTimeWeekday);
                param[index++] = new SqlParameter("@SiteDoorNumberID", oMASSIT_TimeWindowBE.SiteDoorNumberID);
                param[index++] = new SqlParameter("@Priority", oMASSIT_TimeWindowBE.Priority);
                param[index++] = new SqlParameter("@StartSlotTimeID", oMASSIT_TimeWindowBE.StartSlotTimeID);
                param[index++] = new SqlParameter("@EndSlotTimeID", oMASSIT_TimeWindowBE.EndSlotTimeID);
                param[index++] = new SqlParameter("@MaximumPallets", oMASSIT_TimeWindowBE.MaximumPallets);
                param[index++] = new SqlParameter("@MaximumLines", oMASSIT_TimeWindowBE.MaximumLines);
                param[index++] = new SqlParameter("@MaximumCartons", oMASSIT_TimeWindowBE.MaximumCartons);
                param[index++] = new SqlParameter("@MaxDeliveries", oMASSIT_TimeWindowBE.MaxDeliveries);
                param[index++] = new SqlParameter("@IsCheckSKUTable", oMASSIT_TimeWindowBE.IsCheckSKUTable);
                param[index++] = new SqlParameter("@LessVolumePallets", oMASSIT_TimeWindowBE.LessVolumePallets);
                param[index++] = new SqlParameter("@GraterVolumePallets", oMASSIT_TimeWindowBE.GraterVolumePallets);
                param[index++] = new SqlParameter("@TotalHrsMinsAvailable", oMASSIT_TimeWindowBE.TotalHrsMinsAvailable);
                param[index++] = new SqlParameter("@ExcludedVendorIDs", oMASSIT_TimeWindowBE.ExcludedVendorIDs);
                param[index++] = new SqlParameter("@ExcludedCarrierIDs", oMASSIT_TimeWindowBE.ExcludedCarrierIDs);
                param[index++] = new SqlParameter("@MaximumTime", oMASSIT_TimeWindowBE.MaximumTime);
                param[index++] = new SqlParameter("@LessVolumeLines", oMASSIT_TimeWindowBE.LessVolumeLines);
                param[index++] = new SqlParameter("@GraterVolumeLines", oMASSIT_TimeWindowBE.GraterVolumeLines);
                param[index++] = new SqlParameter("@IncludedVendorIDs", oMASSIT_TimeWindowBE.IncludedVendorIDs);
                param[index++] = new SqlParameter("@IncludedCarrierIDs", oMASSIT_TimeWindowBE.IncludedCarrierIDs);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), 
                    CommandType.StoredProcedure, "spMASSIT_TimeWindow", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? IsStartEndTimeExistDAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            DataTable dt = new DataTable();
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_TimeWindowBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_TimeWindowBE.SiteID);
                param[index++] = new SqlParameter("@SiteDoorNumberId", oMASSIT_TimeWindowBE.SiteDoorNumberID);
                param[index++] = new SqlParameter("@StartSlotTimeId", oMASSIT_TimeWindowBE.StartSlotTimeID);
                param[index++] = new SqlParameter("@EndSlotTimeId", oMASSIT_TimeWindowBE.EndSlotTimeID);
                param[index++] = new SqlParameter("@TimeWindowID", oMASSIT_TimeWindowBE.TimeWindowID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), 
                    CommandType.StoredProcedure, "spMASSIT_TimeWindow", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;                
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return intResult;
        }

        public List<MASSIT_TimeWindowBE> GetTimeWindowDAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_TimeWindowBE> olstMASSIT_TimeWindowBE = new List<MASSIT_TimeWindowBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oMASSIT_TimeWindowBE.Action);
                param[index++] = new SqlParameter("@TimeWindowID", oMASSIT_TimeWindowBE.TimeWindowID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_TimeWindowBE.SiteID);
                param[index++] = new SqlParameter("@Weekday", oMASSIT_TimeWindowBE.Weekday);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), 
                    CommandType.StoredProcedure, "spMASSIT_TimeWindow", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_TimeWindowBE oNewMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

                    oNewMASSIT_TimeWindowBE.TimeWindowID = dr["TimeWindowID"] != DBNull.Value ? Convert.ToInt32(dr["TimeWindowID"]) : 0;
                    oNewMASSIT_TimeWindowBE.WindowName = dr["WindowName"] != DBNull.Value ? dr["WindowName"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.SiteID = dr["SiteID"] != DBNull.Value ? Convert.ToInt32(dr["SiteID"]) : 0;
                    oNewMASSIT_TimeWindowBE.Weekday = dr["Weekday"] != DBNull.Value ? dr["Weekday"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.StartTimeWeekday = dr["StartTimeWeekday"] != DBNull.Value ? dr["StartTimeWeekday"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.EndTimeWeekday = dr["EndTimeWeekday"] != DBNull.Value ? dr["EndTimeWeekday"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.SiteDoorNumberID = dr["SiteDoorNumberID"] != DBNull.Value ? Convert.ToInt32(dr["SiteDoorNumberID"]) : 0;
                    oNewMASSIT_TimeWindowBE.Priority = dr["Priority"] != DBNull.Value ? Convert.ToInt32(dr["Priority"]) : 0;
                    oNewMASSIT_TimeWindowBE.StartSlotTimeID = dr["StartSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["StartSlotTimeID"]) : 0;
                    oNewMASSIT_TimeWindowBE.EndSlotTimeID = dr["EndSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["EndSlotTimeID"]) : 0;
                    oNewMASSIT_TimeWindowBE.MaximumPallets = dr["MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["MaximumPallets"]) : 0;
                    oNewMASSIT_TimeWindowBE.MaximumLines = dr["MaximumLines"] != DBNull.Value ? Convert.ToInt32(dr["MaximumLines"]) : 0;
                    oNewMASSIT_TimeWindowBE.MaximumCartons = dr["MaximumCartons"] != DBNull.Value ? Convert.ToInt32(dr["MaximumCartons"]) : 0;
                    oNewMASSIT_TimeWindowBE.MaxDeliveries = dr["MaxDeliveries"] != DBNull.Value ? Convert.ToInt32(dr["MaxDeliveries"]) : 0;
                    oNewMASSIT_TimeWindowBE.IsCheckSKUTable = dr["IsCheckSKUTable"] != DBNull.Value ? Convert.ToBoolean(dr["IsCheckSKUTable"]) : false;
                    oNewMASSIT_TimeWindowBE.LessVolumePallets = dr["LessVolumePallets"] != DBNull.Value ? Convert.ToInt32(dr["LessVolumePallets"]) : 0;
                    oNewMASSIT_TimeWindowBE.GraterVolumePallets = dr["GraterVolumePallets"] != DBNull.Value ? Convert.ToInt32(dr["GraterVolumePallets"]) : 0;
                    oNewMASSIT_TimeWindowBE.TotalHrsMinsAvailable = dr["TotalHrsMinsAvailable"] != DBNull.Value ? dr["TotalHrsMinsAvailable"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.ExcludedVendorIDs = dr["ExcludedVendorIDs"] != DBNull.Value ? dr["ExcludedVendorIDs"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.ExcludedVendorNames = dr["ExcludedVendorNames"] != DBNull.Value ? dr["ExcludedVendorNames"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.ExcludedCarrierIDs = dr["ExcludedCarrierIDs"] != DBNull.Value ? dr["ExcludedCarrierIDs"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.ExcludedCarrierNames = dr["ExcludedCarrierNames"] != DBNull.Value ? dr["ExcludedCarrierNames"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.MaximumTime = dr["MaximumTime"] != DBNull.Value ? dr["MaximumTime"].ToString() : string.Empty;

                    oNewMASSIT_TimeWindowBE.LessVolumeLines = dr["LessVolumeLines"] != DBNull.Value ? Convert.ToInt32(dr["LessVolumeLines"]) : 0;
                    oNewMASSIT_TimeWindowBE.GraterVolumeLines = dr["GraterVolumeLines"] != DBNull.Value ? Convert.ToInt32(dr["GraterVolumeLines"]) : 0;
                    oNewMASSIT_TimeWindowBE.IncludedVendorIDs = dr["IncludedVendorIDs"] != DBNull.Value ? dr["IncludedVendorIDs"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.IncludedVendorNames = dr["IncludedVendorNames"] != DBNull.Value ? dr["IncludedVendorNames"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.IncludedCarrierIDs = dr["IncludedCarrierIDs"] != DBNull.Value ? dr["IncludedCarrierIDs"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.IncludedCarrierNames = dr["IncludedCarrierNames"] != DBNull.Value ? dr["IncludedCarrierNames"].ToString() : string.Empty;

                    olstMASSIT_TimeWindowBE.Add(oNewMASSIT_TimeWindowBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return olstMASSIT_TimeWindowBE;
        }

        public List<MASSIT_TimeWindowBE> GetStartEndTimeBasedOnSiteDAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            DataTable dt = new DataTable();
            List<MASSIT_TimeWindowBE> olstMASSIT_TimeWindowBE = new List<MASSIT_TimeWindowBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_TimeWindowBE.Action);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_TimeWindowBE.SiteID);
                param[index++] = new SqlParameter("@SiteDoorNumberId", oMASSIT_TimeWindowBE.SiteDoorNumberID);
                param[index++] = new SqlParameter("@TimeWindowID", oMASSIT_TimeWindowBE.TimeWindowID);
                param[index++] = new SqlParameter("@Weekday", oMASSIT_TimeWindowBE.Weekday);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(),
                    CommandType.StoredProcedure, "spMASSIT_TimeWindow", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_TimeWindowBE oNewMASSIT_TimeWindowBE = new MASSIT_TimeWindowBE();

                    oNewMASSIT_TimeWindowBE.TimeWindowID = dr["TimeWindowID"] != DBNull.Value ? Convert.ToInt32(dr["TimeWindowID"]) : 0;
                    oNewMASSIT_TimeWindowBE.WindowName = dr["WindowName"] != DBNull.Value ? dr["WindowName"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.Weekday = dr["Weekday"] != DBNull.Value ? dr["Weekday"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.StartTimeWeekday = dr["StartTimeWeekday"] != DBNull.Value ? dr["StartTimeWeekday"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.EndTimeWeekday = dr["EndTimeWeekday"] != DBNull.Value ? dr["EndTimeWeekday"].ToString() : string.Empty;
                    oNewMASSIT_TimeWindowBE.Priority = dr["Priority"] != DBNull.Value ? Convert.ToInt32(dr["Priority"]) : 0;
                    oNewMASSIT_TimeWindowBE.StartSlotTimeID = dr["StartSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["StartSlotTimeID"]) : 0;
                    oNewMASSIT_TimeWindowBE.EndSlotTimeID = dr["EndSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["EndSlotTimeID"]) : 0;                    
                    olstMASSIT_TimeWindowBE.Add(oNewMASSIT_TimeWindowBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return olstMASSIT_TimeWindowBE;
        }

        public int? IsBookingExistForWindowDAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            DataTable dt = new DataTable();
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMASSIT_TimeWindowBE.Action);
                param[index++] = new SqlParameter("@TimeWindowID", oMASSIT_TimeWindowBE.TimeWindowID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(),
                    CommandType.StoredProcedure, "spMASSIT_TimeWindow", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return intResult;
        }

        public int? DeleteWindowDAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            DataTable dt = new DataTable();
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMASSIT_TimeWindowBE.Action);
                param[index++] = new SqlParameter("@TimeWindowID", oMASSIT_TimeWindowBE.TimeWindowID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(),
                    CommandType.StoredProcedure, "spMASSIT_TimeWindow", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return intResult;
        }
    }
}

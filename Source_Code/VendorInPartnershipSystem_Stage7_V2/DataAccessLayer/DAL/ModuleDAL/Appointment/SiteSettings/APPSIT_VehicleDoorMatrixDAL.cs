﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings {
    public class APPSIT_VehicleDoorMatrixDAL {

        public List<MASSIT_VehicleDoorMatrixBE> GetVehicleDoorMatrixDAL(int VehicleTypeID)
        {
            DataTable dt = new DataTable();
            List<MASSIT_VehicleDoorMatrixBE> oMASSIT_VehicleDoorMatrixBEList = new List<MASSIT_VehicleDoorMatrixBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", "VehicleDoorMatrix");
                param[index++] = new SqlParameter("@VehicleTypeID", VehicleTypeID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_VehicleDoorMatrix", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MASSIT_VehicleDoorMatrixBE oNewMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();
                    oNewMASSIT_VehicleDoorMatrixBE.DoorTypeID = Convert.ToInt32(dr["DoorTypeID"]);
                    oNewMASSIT_VehicleDoorMatrixBE.Priority = Convert.ToInt32(dr["Priority"]);
                    oMASSIT_VehicleDoorMatrixBEList.Add(oNewMASSIT_VehicleDoorMatrixBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_VehicleDoorMatrixBEList;
        }

        public List<MASSIT_VehicleDoorMatrixBE> GetVehicleDoorMatrixDetailsDAL(MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE) {
            DataTable dt = new DataTable();
            List<MASSIT_VehicleDoorMatrixBE> oMASSIT_VehicleDoorMatrixBEList = new List<MASSIT_VehicleDoorMatrixBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oMASSIT_VehicleDoorMatrixBE.Action);
                param[index++] = new SqlParameter("@VehicleTypeDoorTypeID", oMASSIT_VehicleDoorMatrixBE.VehicleTypeDoorTypeID);
                param[index++] = new SqlParameter("@DoorTypeID", oMASSIT_VehicleDoorMatrixBE.DoorTypeID);
                param[index++] = new SqlParameter("@VehicleTypeID", oMASSIT_VehicleDoorMatrixBE.VehicleTypeID);
                param[index++] = new SqlParameter("@SiteID", oMASSIT_VehicleDoorMatrixBE.SiteID);
                param[index++] = new SqlParameter("@Priority", oMASSIT_VehicleDoorMatrixBE.Priority);
                param[index++] = new SqlParameter("@IsActive", oMASSIT_VehicleDoorMatrixBE.IsActive);
               

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_VehicleDoorMatrix", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    MASSIT_VehicleDoorMatrixBE oNewMASSIT_VehicleDoorMatrixBE = new MASSIT_VehicleDoorMatrixBE();
                    oNewMASSIT_VehicleDoorMatrixBE.VehicleTypeDoorTypeID = Convert.ToInt32(dr["VehicleTypeDoorTypeID"]);
                    oNewMASSIT_VehicleDoorMatrixBE.DoorTypeID = Convert.ToInt32(dr["DoorTypeID"]);

                    oNewMASSIT_VehicleDoorMatrixBE.VehicleTypeID =Convert.ToInt32(dr["VehicleTypeID"]);

                    oNewMASSIT_VehicleDoorMatrixBE.VehicleType = new BusinessEntities.ModuleBE.Appointment.SiteSettings.MASSIT_VehicleTypeBE();
                    oNewMASSIT_VehicleDoorMatrixBE.VehicleType.VehicleType = dr["VehicleType"].ToString();

                    oNewMASSIT_VehicleDoorMatrixBE.Priority = Convert.ToInt32(dr["Priority"]);

                    oNewMASSIT_VehicleDoorMatrixBE.DoorType = new BusinessEntities.ModuleBE.Appointment.CountrySetting.MASCNT_DoorTypeBE();
                    oNewMASSIT_VehicleDoorMatrixBE.DoorType.DoorType = dr["DoorType"].ToString();

                    oMASSIT_VehicleDoorMatrixBEList.Add(oNewMASSIT_VehicleDoorMatrixBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASSIT_VehicleDoorMatrixBEList;
        }

        public int? addEditVehicleDoorMatrixDetailsDAL(MASSIT_VehicleDoorMatrixBE oMASSIT_VehicleDoorMatrixBE) {

            int? intResult = 0;
            List<MASSIT_VehicleDoorMatrixBE> oMASSIT_VehicleDoorMatrixBEList = new List<MASSIT_VehicleDoorMatrixBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASSIT_VehicleDoorMatrixBE.Action);
                param[index++] = new SqlParameter("@VehicleTypeDoorTypeID", oMASSIT_VehicleDoorMatrixBE.VehicleTypeDoorTypeID);
                param[index++] = new SqlParameter("@DoorTypeID", oMASSIT_VehicleDoorMatrixBE.DoorTypeID);
                param[index++] = new SqlParameter("@VehicleTypeID", oMASSIT_VehicleDoorMatrixBE.VehicleTypeID);
                param[index++] = new SqlParameter("@Priority", oMASSIT_VehicleDoorMatrixBE.Priority);
                param[index++] = new SqlParameter("@IsActive", oMASSIT_VehicleDoorMatrixBE.IsActive);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_VehicleDoorMatrix", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
      
    }
}

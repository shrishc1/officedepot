﻿using System;
using System.Data.SqlClient;
using System.Data;

using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using Utilities;
using System.Collections.Generic;

namespace DataAccessLayer.ModuleDAL.Appointment.SiteSettings {
   public class APPSIT_VendorDAL {

       public List<MASSIT_VendorBE> GetVendorConstraintsDAL(MASSIT_VendorBE oMASSIT_VendorBE) {
           DataTable dt = new DataTable();
           List<MASSIT_VendorBE> oMASSIT_VendorBEList = new List<MASSIT_VendorBE>();
           try {
               int index = 0;
               SqlParameter[] param = new SqlParameter[3];
               param[index++] = new SqlParameter("@Action", oMASSIT_VendorBE.Action);
               param[index++] = new SqlParameter("@SiteVendorID", oMASSIT_VendorBE.SiteVendorID);
               param[index++] = new SqlParameter("@UserID", oMASSIT_VendorBE.User.UserID);

               DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Vendor", param);

               dt = ds.Tables[0];

               foreach (DataRow dr in dt.Rows) {
                   MASSIT_VendorBE oNewMASSIT_VendorBE = new MASSIT_VendorBE();
                   oNewMASSIT_VendorBE.SiteVendorID = Convert.ToInt32(dr["SiteVendorID"]);
                   oNewMASSIT_VendorBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                   oNewMASSIT_VendorBE.APP_MaximumPallets = dr["APP_MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["APP_MaximumPallets"]) : (int?)null; //Convert.ToInt32(dr["APP_MaximumPallets"]);
                   oNewMASSIT_VendorBE.APP_MaximumLines = dr["APP_MaximumLines"] != DBNull.Value ? Convert.ToInt32(dr["APP_MaximumLines"]) : (int?)null; //Convert.ToInt32(dr["APP_MaximumLines"]);
                   oNewMASSIT_VendorBE.APP_MaximumDeliveriesInADay = dr["APP_MaximumDeliveriesInADay"] != DBNull.Value ? Convert.ToInt32(dr["APP_MaximumDeliveriesInADay"]) : (int?)null;  //Convert.ToInt32(dr["APP_MaximumDeliveriesInADay"]);
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnSunday = dr["APP_CannotDeliveryOnSunday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnSunday"]) : (bool?)null;  //Convert.ToBoolean(dr["APP_CannotDeliveryOnSunday"]);
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnMonday = dr["APP_CannotDeliveryOnMonday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnMonday"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnFriday = dr["APP_CannotDeliveryOnFriday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnFriday"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnSaturday = dr["APP_CannotDeliveryOnSaturday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnSaturday"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnThrusday = dr["APP_CannotDeliveryOnThrusday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnThrusday"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnTuesday = dr["APP_CannotDeliveryOnTuesday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnTuesday"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnWednessday = dr["APP_CannotDeliveryOnWednessday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnWednessday"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CheckingRequired = dr["APP_CheckingRequired"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CheckingRequired"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CannotDeliverAfter = dr["APP_CannotDeliverAfter"] != DBNull.Value ? Convert.ToDateTime(dr["APP_CannotDeliverAfter"]) : (DateTime?)null;  //Convert.ToDateTime(dr["APP_CannotDeliverAfter"]);
                   oNewMASSIT_VendorBE.APP_CannotDeliverBefore = dr["APP_CannotDeliverBefore"] != DBNull.Value ? Convert.ToDateTime(dr["APP_CannotDeliverBefore"]) : (DateTime?)null;  //Convert.ToDateTime(dr["APP_CannotDeliverBefore"]);

                   oNewMASSIT_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                   oNewMASSIT_VendorBE.Site.SiteName = dr["SiteName"].ToString();
                   oNewMASSIT_VendorBE.SiteID = dr["siteid"] != DBNull.Value ? Convert.ToInt32(dr["siteid"]) : 0;

                   oNewMASSIT_VendorBE.Vendor = new UP_VendorBE();
                   oNewMASSIT_VendorBE.Vendor.VendorName = dr["VendorName"].ToString();
                   oNewMASSIT_VendorBE.AfterSlotTimeID = dr["APP_AfterSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["APP_AfterSlotTimeID"]) : (int?)null;
                   oNewMASSIT_VendorBE.BeforeSlotTimeID = dr["APP_BeforeSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["APP_BeforeSlotTimeID"]) : (int?)null;
                   oMASSIT_VendorBEList.Add(oNewMASSIT_VendorBE);
               }
           }
           catch (Exception ex) {
               LogUtility.SaveErrorLogEntry(ex);
           }
           return oMASSIT_VendorBEList;
       }

       public List<MASSIT_VendorBE> GetVendorMicellaneousConstraintsDetailsDAL(MASSIT_VendorBE oMASSIT_VendorBE)
       {
           DataTable dt = new DataTable();
           List<MASSIT_VendorBE> oMASSIT_VendorBEList = new List<MASSIT_VendorBE>();
           try
           {
               int index = 0;
               SqlParameter[] param = new SqlParameter[4];
               param[index++] = new SqlParameter("@Action", oMASSIT_VendorBE.Action);
               param[index++] = new SqlParameter("@SiteID", oMASSIT_VendorBE.SiteID);
               param[index++] = new SqlParameter("@VendorID", oMASSIT_VendorBE.VendorID);
               param[index++] = new SqlParameter("@UserID", oMASSIT_VendorBE.User.UserID);

               DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Vendor", param);

               dt = ds.Tables[0];

               foreach (DataRow dr in dt.Rows)
               {
                   MASSIT_VendorBE oNewMASSIT_VendorBE = new MASSIT_VendorBE();
                   oNewMASSIT_VendorBE.SiteVendorID = Convert.ToInt32(dr["SiteVendorID"]);
                   oNewMASSIT_VendorBE.VendorID = Convert.ToInt32(dr["VendorID"]);
                   oNewMASSIT_VendorBE.APP_MaximumPallets = dr["APP_MaximumPallets"] != DBNull.Value ? Convert.ToInt32(dr["APP_MaximumPallets"]) : (int?)null; //Convert.ToInt32(dr["APP_MaximumPallets"]);
                   oNewMASSIT_VendorBE.APP_MaximumLines = dr["APP_MaximumLines"] != DBNull.Value ? Convert.ToInt32(dr["APP_MaximumLines"]) : (int?)null; //Convert.ToInt32(dr["APP_MaximumLines"]);
                   oNewMASSIT_VendorBE.APP_MaximumDeliveriesInADay = dr["APP_MaximumDeliveriesInADay"] != DBNull.Value ? Convert.ToInt32(dr["APP_MaximumDeliveriesInADay"]) : (int?)null;  //Convert.ToInt32(dr["APP_MaximumDeliveriesInADay"]);
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnSunday = dr["APP_CannotDeliveryOnSunday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnSunday"]) : (bool?)null;  //Convert.ToBoolean(dr["APP_CannotDeliveryOnSunday"]);
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnMonday = dr["APP_CannotDeliveryOnMonday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnMonday"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnFriday = dr["APP_CannotDeliveryOnFriday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnFriday"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnSaturday = dr["APP_CannotDeliveryOnSaturday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnSaturday"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnThrusday = dr["APP_CannotDeliveryOnThrusday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnThrusday"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnTuesday = dr["APP_CannotDeliveryOnTuesday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnTuesday"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CannotDeliveryOnWednessday = dr["APP_CannotDeliveryOnWednessday"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CannotDeliveryOnWednessday"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CheckingRequired = dr["APP_CheckingRequired"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CheckingRequired"]) : (bool?)null;
                   oNewMASSIT_VendorBE.APP_CannotDeliverAfter = dr["APP_CannotDeliverAfter"] != DBNull.Value ? Convert.ToDateTime(dr["APP_CannotDeliverAfter"]) : (DateTime?)null;  //Convert.ToDateTime(dr["APP_CannotDeliverAfter"]);
                   oNewMASSIT_VendorBE.APP_CannotDeliverBefore = dr["APP_CannotDeliverBefore"] != DBNull.Value ? Convert.ToDateTime(dr["APP_CannotDeliverBefore"]) : (DateTime?)null;  //Convert.ToDateTime(dr["APP_CannotDeliverBefore"]);

                   oNewMASSIT_VendorBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                   oNewMASSIT_VendorBE.Site.SiteName = dr["SiteName"].ToString();

                   oNewMASSIT_VendorBE.Vendor = new UP_VendorBE();
                   oNewMASSIT_VendorBE.Vendor.VendorName = dr["VendorName"].ToString();

                   oNewMASSIT_VendorBE.AfterSlotTimeID = dr["APP_AfterSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["APP_AfterSlotTimeID"]) : (int?)null;
                   oNewMASSIT_VendorBE.BeforeSlotTimeID = dr["APP_BeforeSlotTimeID"] != DBNull.Value ? Convert.ToInt32(dr["APP_BeforeSlotTimeID"]) : (int?)null;

                   oMASSIT_VendorBEList.Add(oNewMASSIT_VendorBE);
               }
           }
           catch (Exception ex)
           {
               LogUtility.SaveErrorLogEntry(ex);
           }
           return oMASSIT_VendorBEList;
       }

       public int? addEditVendorConstraintsDAL(MASSIT_VendorBE oMASSIT_VendorBE) {
           int? intResult = 0;
           try {
               int index = 0;
               SqlParameter[] param = new SqlParameter[20];
               param[index++] = new SqlParameter("@Action", oMASSIT_VendorBE.Action);
               param[index++] = new SqlParameter("@SiteVendorID", oMASSIT_VendorBE.SiteVendorID);
               param[index++] = new SqlParameter("@SiteID", oMASSIT_VendorBE.SiteID);
               param[index++] = new SqlParameter("@VendorID", oMASSIT_VendorBE.VendorID);
               param[index++] = new SqlParameter("@APP_CheckingRequired", oMASSIT_VendorBE.APP_CheckingRequired);
               param[index++] = new SqlParameter("@APP_MaximumPallets", oMASSIT_VendorBE.APP_MaximumPallets);
               param[index++] = new SqlParameter("@APP_MaximumLines", oMASSIT_VendorBE.APP_MaximumLines);
               param[index++] = new SqlParameter("@APP_MaximumDeliveriesInADay", oMASSIT_VendorBE.APP_MaximumDeliveriesInADay);
               param[index++] = new SqlParameter("@APP_CannotDeliverAfter", oMASSIT_VendorBE.APP_CannotDeliverAfter);
               param[index++] = new SqlParameter("@APP_CannotDeliverBefore", oMASSIT_VendorBE.APP_CannotDeliverBefore);
               param[index++] = new SqlParameter("@APP_CannotDeliveryOnFriday", oMASSIT_VendorBE.APP_CannotDeliveryOnFriday);
               param[index++] = new SqlParameter("@APP_CannotDeliveryOnMonday", oMASSIT_VendorBE.APP_CannotDeliveryOnMonday);
               param[index++] = new SqlParameter("@APP_CannotDeliveryOnSaturday", oMASSIT_VendorBE.APP_CannotDeliveryOnSaturday);
               param[index++] = new SqlParameter("@APP_CannotDeliveryOnSunday", oMASSIT_VendorBE.APP_CannotDeliveryOnSunday);
               param[index++] = new SqlParameter("@APP_CannotDeliveryOnThrusday", oMASSIT_VendorBE.APP_CannotDeliveryOnThrusday);
               param[index++] = new SqlParameter("@APP_CannotDeliveryOnTuesday", oMASSIT_VendorBE.APP_CannotDeliveryOnTuesday);
               param[index++] = new SqlParameter("@APP_CannotDeliveryOnWednessday", oMASSIT_VendorBE.APP_CannotDeliveryOnWednessday);
               param[index++] = new SqlParameter("@APP_BeforeSlotTimeID", oMASSIT_VendorBE.BeforeSlotTimeID);
               param[index++] = new SqlParameter("@APP_AfterSlotTimeID", oMASSIT_VendorBE.AfterSlotTimeID);
              

               DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Vendor", param);

               if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                   intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
               else
                   intResult = 0;

           }
           catch (Exception ex) {
               LogUtility.SaveErrorLogEntry(ex);
           }
           finally { }
           return intResult;
       }

       public List<MASSIT_VendorBE> GetVendorContactDetailsDAL(MASSIT_VendorBE oMASSIT_VendorBE) {
           DataTable dt = new DataTable();
           List<MASSIT_VendorBE> oMASSIT_VendorBEList = new List<MASSIT_VendorBE>();
           try {
               int index = 0;
               SqlParameter[] param = new SqlParameter[3];
               param[index++] = new SqlParameter("@Action", oMASSIT_VendorBE.Action);
               param[index++] = new SqlParameter("@SiteVendorID", oMASSIT_VendorBE.SiteVendorID);
               //if(oMASSIT_VendorBE.SiteID != (int?)null)
               param[index++] = new SqlParameter("@SiteID", oMASSIT_VendorBE.SiteID);

               DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Vendor", param);

               dt = ds.Tables[0];

               foreach (DataRow dr in dt.Rows) {
                   MASSIT_VendorBE oNewMASSIT_VendorBE = new MASSIT_VendorBE();
                   
                   oNewMASSIT_VendorBE.Vendor = new UP_VendorBE();
                   oNewMASSIT_VendorBE.Vendor.VendorName = dr["VendorName"].ToString();
                   oNewMASSIT_VendorBE.Vendor.VendorContactEmail = dr["VendorContactEmail"] != DBNull.Value ? dr["VendorContactEmail"].ToString() : "";
                   oNewMASSIT_VendorBE.Vendor.VendorContactFax = dr["VendorContactFax"] != DBNull.Value ? dr["VendorContactFax"].ToString() : "";
                   oNewMASSIT_VendorBE.Vendor.Language = dr["Language"] != DBNull.Value ? dr["Language"].ToString() : "";
                   oMASSIT_VendorBEList.Add(oNewMASSIT_VendorBE);
               }
           }
           catch (Exception ex) {
               LogUtility.SaveErrorLogEntry(ex);
           }
           return oMASSIT_VendorBEList;
       }


       public List<MASSIT_VendorBE> GetVendorCheckingRequiredDAL(MASSIT_VendorBE oMASSIT_VendorBE)
       {
           DataTable dt = new DataTable();
           List<MASSIT_VendorBE> oMASSIT_VendorBEList = new List<MASSIT_VendorBE>();
           try
           {
               int index = 0;
               SqlParameter[] param = new SqlParameter[3];
               param[index++] = new SqlParameter("@Action", oMASSIT_VendorBE.Action);
               param[index++] = new SqlParameter("@SiteID", oMASSIT_VendorBE.SiteID);
               param[index++] = new SqlParameter("@VendorID", oMASSIT_VendorBE.VendorID);

               DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Vendor", param);

               dt = ds.Tables[0];

               foreach (DataRow dr in dt.Rows)
               {
                   MASSIT_VendorBE oNewMASSIT_VendorBE = new MASSIT_VendorBE();

                   oNewMASSIT_VendorBE.Vendor = new UP_VendorBE();
                   oNewMASSIT_VendorBE.APP_CheckingRequired = dr["APP_CheckingRequired"] != DBNull.Value ? Convert.ToBoolean(dr["APP_CheckingRequired"]) :false;
                   oMASSIT_VendorBEList.Add(oNewMASSIT_VendorBE);
               }
           }
           catch (Exception ex)
           {
               LogUtility.SaveErrorLogEntry(ex);
           }
           return oMASSIT_VendorBEList;
       }

       public List<MASSIT_VendorBE> GetCarriersVendorIDDAL(MASSIT_VendorBE oMASSIT_VendorBE)
       {
           DataTable dt = new DataTable();
           List<MASSIT_VendorBE> oMASSIT_VendorBEList = new List<MASSIT_VendorBE>();
           try
           {
               int index = 0;
               SqlParameter[] param = new SqlParameter[3];
               param[index++] = new SqlParameter("@Action", oMASSIT_VendorBE.Action);
               param[index++] = new SqlParameter("@BookingID", oMASSIT_VendorBE.BookingID);
               

               DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Vendor", param);

               dt = ds.Tables[0];

               if (dt.Rows.Count > 0)
               {
                   foreach (DataRow dr in dt.Rows)
                   {
                       MASSIT_VendorBE oNewMASSIT_VendorBE = new MASSIT_VendorBE();

                       oNewMASSIT_VendorBE.Vendor = new UP_VendorBE();
                       oNewMASSIT_VendorBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : -1; 
                       oNewMASSIT_VendorBE.NumberOfCartons = dr["NumberOfCartons"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfCartons"]) : (int?)null;
                       oNewMASSIT_VendorBE.NumberOfPallet = dr["NumberOfPallet"] != DBNull.Value ? Convert.ToInt32(dr["NumberOfPallet"]) : (int?)null;
                       oMASSIT_VendorBEList.Add(oNewMASSIT_VendorBE);
                   }
               }
           }
           catch (Exception ex)
           {
               LogUtility.SaveErrorLogEntry(ex);
           }
           return oMASSIT_VendorBEList;
       }

       public int? IsMiscConstVendorExistDAL(MASSIT_VendorBE oMASSIT_VendorBE)
       {
           int? intResult = 0;
           try
           {
               int index = 0;
               SqlParameter[] param = new SqlParameter[5];
               param[index++] = new SqlParameter("@Action", oMASSIT_VendorBE.Action);
               param[index++] = new SqlParameter("@VendorID", oMASSIT_VendorBE.VendorID);
               param[index++] = new SqlParameter("@IsProcessingDefined", oMASSIT_VendorBE.IsProcessingDefined);
               param[index++] = new SqlParameter("@IsConstraintsDefined", oMASSIT_VendorBE.IsConstraintsDefined);
               param[index++] = new SqlParameter("@SiteID", oMASSIT_VendorBE.SiteID);
               DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_Vendor", param);

               if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                   intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
               else
                   intResult = 0;
           }
           catch (Exception ex)
           {
               LogUtility.SaveErrorLogEntry(ex);
           }
           finally { }
           return intResult;
       }
    }
}

﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;

using BusinessEntities.ModuleBE.Appointment.CountrySetting;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Appointment.CountrySetting {
    public class APPCNT_CrossDockDAL : DataAccessLayer.BaseDAL{

        public List<MASCNT_CrossDockBE> GetCrossDockDetailsDAL(MASCNT_CrossDockBE oMASCNT_CrossDockBE) {
            DataTable dt = new DataTable();
            List<MASCNT_CrossDockBE> oMASCNT_CrossDockBEList = new List<MASCNT_CrossDockBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", oMASCNT_CrossDockBE.Action);
                param[index++] = new SqlParameter("@CrossDockingID", oMASCNT_CrossDockBE.CrossDockingID);
                param[index++] = new SqlParameter("@CountryID", oMASCNT_CrossDockBE.CountryID);
                param[index++] = new SqlParameter("@SiteID", oMASCNT_CrossDockBE.SiteID);
                param[index++] = new SqlParameter("@DestinationSiteID", oMASCNT_CrossDockBE.DestinationSiteID);
                param[index++] = new SqlParameter("@UserID", oMASCNT_CrossDockBE.User.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASCNT_CrossDocking", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    MASCNT_CrossDockBE oNewMASCNT_CrossDockBE = new MASCNT_CrossDockBE();
                    oNewMASCNT_CrossDockBE.CrossDockingID = Convert.ToInt32(dr["CrossDockingID"]);
                    oNewMASCNT_CrossDockBE.CountryID = Convert.ToInt32(dr["CountryID"]);
                    oNewMASCNT_CrossDockBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewMASCNT_CrossDockBE.DestinationSiteID = Convert.ToInt32(dr["DestinationSiteID"]);
                    oNewMASCNT_CrossDockBE.Country = new BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE();
                    oNewMASCNT_CrossDockBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewMASCNT_CrossDockBE.DestinationSite = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();

                    oNewMASCNT_CrossDockBE.Country.CountryName = dr["CountryName"].ToString();
                    oNewMASCNT_CrossDockBE.Site.SiteName = dr["SiteName"].ToString();
                    oNewMASCNT_CrossDockBE.DestinationSite.SiteName = dr["DestinationSiteName"].ToString();

                    oMASCNT_CrossDockBEList.Add(oNewMASCNT_CrossDockBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMASCNT_CrossDockBEList;
        }

        public int? addEditCrossDockDetailsDAL(MASCNT_CrossDockBE oMASCNT_CrossDockBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oMASCNT_CrossDockBE.Action);
                param[index++] = new SqlParameter("@CrossDockingID", oMASCNT_CrossDockBE.CrossDockingID);
                param[index++] = new SqlParameter("@CountryID", oMASCNT_CrossDockBE.CountryID);
                param[index++] = new SqlParameter("@SiteID", oMASCNT_CrossDockBE.SiteID);
                param[index++] = new SqlParameter("@DestinationSiteID", oMASCNT_CrossDockBE.DestinationSiteID);


                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASCNT_CrossDocking", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

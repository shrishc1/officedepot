﻿using System;
using System.Data;
using System.Data.SqlClient;
using Utilities;

using BusinessEntities.ModuleBE.AdminFunctions;
using System.Collections.Generic;

namespace DataAccessLayer.ModuleDAL.AdminFunctions {
    public class MAS_CountryDAL : DataAccessLayer.BaseDAL{

        public List<MAS_CountryBE> GetCountryDAL(MAS_CountryBE oMAS_CountryBE) {
            
            List<MAS_CountryBE> oMAS_CountryBEList = new List<MAS_CountryBE>();
            try {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oMAS_CountryBE.Action);
                param[index++] = new SqlParameter("@UserID", oMAS_CountryBE.User.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Country", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    MAS_CountryBE oNewMAS_CountryBE = new MAS_CountryBE();
                    oNewMAS_CountryBE.CountryID = Convert.ToInt32(dr["CountryID"]);
                    oNewMAS_CountryBE.CountryName = dr["CountryName"].ToString();

                    oMAS_CountryBEList.Add(oNewMAS_CountryBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_CountryBEList;
        }
    }
}

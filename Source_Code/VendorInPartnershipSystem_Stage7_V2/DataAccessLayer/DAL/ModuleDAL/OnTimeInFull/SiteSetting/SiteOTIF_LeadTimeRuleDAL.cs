﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;

using Utilities;
using BusinessEntities.ModuleBE.OnTimeInFull.SiteSetting;

namespace DataAccessLayer.ModuleDAL.OnTimeInFull.SiteSetting {
    public class SiteOTIF_LeadTimeRuleDAL : BaseDAL{

        public List<SiteOTIF_LeadTimeRuleBE> GetSiteOTIFLeadTimeDetailsDAL(SiteOTIF_LeadTimeRuleBE oSiteOTIF_LeadTimeRuleBE) {
            DataTable dt = new DataTable();
            List<SiteOTIF_LeadTimeRuleBE> oSiteOTIF_LeadTimeRuleBEList = new List<SiteOTIF_LeadTimeRuleBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oSiteOTIF_LeadTimeRuleBE.Action);
                param[index++] = new SqlParameter("@UserID", oSiteOTIF_LeadTimeRuleBE.User.UserID);
                param[index++] = new SqlParameter("@SiteID", oSiteOTIF_LeadTimeRuleBE.SiteID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSiteOTIF_LeadTimeRule", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows) {
                    SiteOTIF_LeadTimeRuleBE oNewSiteOTIF_LeadTimeRuleBE = new SiteOTIF_LeadTimeRuleBE();

                    oNewSiteOTIF_LeadTimeRuleBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                    oNewSiteOTIF_LeadTimeRuleBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    oNewSiteOTIF_LeadTimeRuleBE.Site.SiteDescription = dr["SiteName"].ToString() != "" ? dr["SiteNumber"].ToString() + " - " + dr["SiteName"].ToString() : dr["SiteNumber"].ToString();
                    oNewSiteOTIF_LeadTimeRuleBE.Site.OTI_LeadTime = dr["OTI_LeadTime"].ToString().ToLower() == "c" ? "Calender days" : "Working days";

                    oSiteOTIF_LeadTimeRuleBEList.Add(oNewSiteOTIF_LeadTimeRuleBE);
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oSiteOTIF_LeadTimeRuleBEList;
        }

        public int? addEditSiteOTIFLeadTimeDetailsDAL(SiteOTIF_LeadTimeRuleBE oSiteOTIF_LeadTimeRuleBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oSiteOTIF_LeadTimeRuleBE.Action);
                param[index++] = new SqlParameter("@SiteID", oSiteOTIF_LeadTimeRuleBE.SiteID);
                param[index++] = new SqlParameter("@OTI_LeadTime", oSiteOTIF_LeadTimeRuleBE.Site.OTI_LeadTime);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spSiteOTIF_LeadTimeRule", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }    
    }
}

﻿using System;
using System.Data;
using System.Data.SqlClient;
using BusinessEntities.ModuleBE.OnTimeInFull.Report;
using Utilities;
using System.Collections.Generic;
using System.Text;
using BusinessEntities.ModuleBE.Upload;

namespace DataAccessLayer.ModuleDAL.OnTimeInFull.Report
{
    public class OTIFReportDAL : BaseDAL
    {

        public DataSet GetOTIFReportDAL(OTIFReportBE oOTIFReportBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[19];
                param[index++] = new SqlParameter("@Action", oOTIFReportBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", oOTIFReportBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oOTIFReportBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oOTIFReportBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oOTIFReportBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@OfficeDepotSKU", oOTIFReportBE.SelectedSKUCodes);
                param[index++] = new SqlParameter("@SelectedPurchaseOrders", oOTIFReportBE.SelectedPurchaseOrderNo);
                param[index++] = new SqlParameter("@SelectedItemClassification", oOTIFReportBE.SelectedItemClassification);
                param[index++] = new SqlParameter("@SelectedAPIDs", oOTIFReportBE.SelectedAPIDs);
                param[index++] = new SqlParameter("@VendorNumber", oOTIFReportBE.VendorNumber);
                param[index++] = new SqlParameter("@VendorName", oOTIFReportBE.VendorName);
                param[index++] = new SqlParameter("@DateTo", oOTIFReportBE.DateTo);
                param[index++] = new SqlParameter("@DateFrom", oOTIFReportBE.DateFrom);
                param[index++] = new SqlParameter("@PurchaseOrderDue", oOTIFReportBE.PurchaseOrderDue);
                param[index++] = new SqlParameter("@PurchaseOrderReceived", oOTIFReportBE.PurchaseOrderReceived);
                param[index++] = new SqlParameter("@RMSCategoryIDs", oOTIFReportBE.SelectedRMSCategoryIDs);
                param[index++] = new SqlParameter("@SkugroupingID", oOTIFReportBE.SelectedSKuGroupingIDs);
                param[index++] = new SqlParameter("@StockPlannerGroupingID", oOTIFReportBE.StockPlannerGroupingIDs);
                param[index++] = new SqlParameter("@CustomerIDs", oOTIFReportBE.CustomerIDs);

                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("DAL SP name           : spOTIF_Reports");
                sbMessage.Append("\r\n");

                for (int i = 0; i <= param.Length - 1; i++)
                {
                    sbMessage.Append("DAL Param Values      : " + i.ToString() + ") " + param[i].ParameterName + " - " + Convert.ToString(param[i].Value));
                    sbMessage.Append("\r\n");
                }

                LogUtility.SaveTraceLogEntry(sbMessage);

                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spOTIF_Reports", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetVendorNumberOfTimeAccessesedDAL(OTIFReportBE oOTIFReportBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oOTIFReportBE.Action);
                param[index++] = new SqlParameter("@UserID", oOTIFReportBE.UserId);
                param[index++] = new SqlParameter("@ReportMonth", oOTIFReportBE.ReportMonth);
                Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spOTIF_Reports", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public int? UpdateOTIFVendorTrackingDAL(OTIFReportBE oOTIFReportBE)
        {
            int? statusFlag = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", oOTIFReportBE.Action);
                param[index++] = new SqlParameter("@UserID", oOTIFReportBE.UserId);
                param[index++] = new SqlParameter("@ReportMonth", oOTIFReportBE.ReportMonth);
                param[index++] = new SqlParameter("@NumberofTimeAccessed", oOTIFReportBE.NumberOfTimeAccessed);
                param[index++] = new SqlParameter("@DateFirstAccessed", DateTime.Now);

                statusFlag = SqlHelper.ExecuteNonQuery(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_Reports", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return statusFlag;
        }


        // For Adding Vendor Exclusion
        public int? AddEditVendorExclusionDAL(OTIFReportBE oOTIFReportBE)
        {
            int? statusFlag = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[14];
                param[index++] = new SqlParameter("@Action", oOTIFReportBE.Action);
                param[index++] = new SqlParameter("@ExclusionID", oOTIFReportBE.ExclusionID);
                param[index++] = new SqlParameter("@ExclusionNarrative", oOTIFReportBE.ExclusionNarrative);
                param[index++] = new SqlParameter("@AppliedBy", oOTIFReportBE.AppliedBy);
                param[index++] = new SqlParameter("@ApplicableDate", oOTIFReportBE.ApplicableDate);
                param[index++] = new SqlParameter("@SelectedExcludedVendorId", oOTIFReportBE.SelectedExcludedVendorId);
                param[index++] = new SqlParameter("@SelectedExcludedSiteId", oOTIFReportBE.SelectedExcludedSiteId);
                param[index++] = new SqlParameter("@SelectedExcludedSKUsName", oOTIFReportBE.SelectedExcludedSKUsName);
                param[index++] = new SqlParameter("@SelectedExcludedDeliveryTypeId", oOTIFReportBE.SelectedExcludedDeliveryTypeId);
                param[index++] = new SqlParameter("@SelectedExcludedBookingNo", oOTIFReportBE.SelectedExcludedBookingNo);
                param[index++] = new SqlParameter("@SelectedExcludedDiscrepancyNo", oOTIFReportBE.SelectedExcludedDiscrepancyNo);
                oOTIFReportBE.SelectedExcludedPurchaseNo = oOTIFReportBE.SelectedExcludedPurchaseNo + ",";
                param[index++] = new SqlParameter("@SelectedExcludedPurchaseNo", oOTIFReportBE.SelectedExcludedPurchaseNo);
                param[index++] = new SqlParameter("@ExcludedDateFrom", oOTIFReportBE.ExcludedDateFrom);
                param[index++] = new SqlParameter("@ExcludedDateTo", oOTIFReportBE.ExcludedDateTo);

                // statusFlag = SqlHelper.ExecuteNonQuery(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_VendorReportingExclusion", param);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_VendorReportingExclusion", param);
                statusFlag = Convert.ToInt32(ds.Tables[0].Rows[0][0]);


            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return statusFlag;
        }

        // For Deleting Vendor Exclusion
        public int? DeleteVendorExclusionDAL(OTIFReportBE oOTIFReportBE)
        {
            int? statusFlag = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oOTIFReportBE.Action);
                param[index++] = new SqlParameter("@ExclusionID", oOTIFReportBE.ExclusionID);
                statusFlag = SqlHelper.ExecuteNonQuery(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_VendorReportingExclusion", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return statusFlag;
        }

        //For Getting List of Exclusions
        public List<OTIFReportBE> GetVendorExclusionDAL(OTIFReportBE oOTIFReportBE)
        {
            DataTable dt = new DataTable();
            List<OTIFReportBE> lstVendorExclusion = new List<OTIFReportBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oOTIFReportBE.Action);
                param[index++] = new SqlParameter("@ExclusionID", oOTIFReportBE.ExclusionID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_VendorReportingExclusion", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    OTIFReportBE oVendorExclusionBE = new OTIFReportBE();
                    oVendorExclusionBE.ExclusionID = Convert.ToInt32(dr["ExclusionID"]);
                    oVendorExclusionBE.ExclusionNarrative = dr["ExclusionNarrative"].ToString();
                    oVendorExclusionBE.AppliedBy = dr["AppliedBy"].ToString();
                    oVendorExclusionBE.ApplicableDate = dr["ApplicableDate"] != null ? Convert.ToDateTime(dr["ApplicableDate"]) : (DateTime?)null;
                    oVendorExclusionBE.SelectedExcludedVendorId = dr["SelectedExcludedVendorId"].ToString();
                    oVendorExclusionBE.SelectedExcludedSiteId = dr["SelectedExcludedSiteId"].ToString();
                    oVendorExclusionBE.SelectedExcludedSKUsName = dr["SelectedExcludedSKUsName"].ToString();
                    oVendorExclusionBE.SelectedExcludedDeliveryTypeId = dr["SelectedExcludedDeliveryTypeId"].ToString();
                    oVendorExclusionBE.SelectedExcludedBookingNo = dr["SelectedExcludedBookingNo"].ToString();
                    oVendorExclusionBE.SelectedExcludedDiscrepancyNo = dr["SelectedExcludedDiscrepancyNo"].ToString();
                    oVendorExclusionBE.ExcludedDateFrom = dr["ExcludedDateFrom"] != DBNull.Value ? Convert.ToDateTime(dr["ExcludedDateFrom"]) : (DateTime?)null;
                    oVendorExclusionBE.ExcludedDateTo = dr["ExcludedDateTo"] != DBNull.Value ? Convert.ToDateTime(dr["ExcludedDateTo"]) : (DateTime?)null;
                    lstVendorExclusion.Add(oVendorExclusionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstVendorExclusion;

        }

        //For Getting List of  Excluded sites
        public List<OTIFReportBE> GetVendorExcludedSitesDAL(OTIFReportBE oOTIFReportBE)
        {
            DataTable dt = new DataTable();
            List<OTIFReportBE> lstVendorExcludedSites = new List<OTIFReportBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oOTIFReportBE.Action);
                param[index++] = new SqlParameter("@SelectedExcludedSiteId", oOTIFReportBE.SelectedExcludedSiteId);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_VendorReportingExclusion", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    OTIFReportBE oVendorExclusionBE = new OTIFReportBE();
                    oVendorExclusionBE.SelectedExcludedSiteId = dr["SiteID"].ToString();
                    oVendorExclusionBE.SiteName = dr["SiteCountryName"].ToString();
                    lstVendorExcludedSites.Add(oVendorExclusionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstVendorExcludedSites;

        }

        //For Getting List of  Excluded PO
        public List<OTIFReportBE> GetVendorExcludedPODAL(OTIFReportBE oOTIFReportBE)
        {
            DataTable dt = new DataTable();
            List<OTIFReportBE> lstVendorExcludedSites = new List<OTIFReportBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oOTIFReportBE.Action);
                param[index++] = new SqlParameter("@ExclusionID", oOTIFReportBE.ExclusionID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_VendorReportingExclusion", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    OTIFReportBE oVendorExclusionBE = new OTIFReportBE();
                    oVendorExclusionBE.SelectedExcludedSiteId = dr["ExclusionID"].ToString();
                    oVendorExclusionBE.SitePurchaseOrderRaisedName = dr["SitePurchaseOrderRaisedName"].ToString();
                    oVendorExclusionBE.SitePurchaseOrderRaisedIds = dr["SitePurchaseOrderRaisedIds"].ToString();
                    lstVendorExcludedSites.Add(oVendorExclusionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstVendorExcludedSites;

        }

        //For Getting List of  Excluded Delivery Type
        public List<OTIFReportBE> GetVendorExcludedDeliveryTypeDAL(OTIFReportBE oOTIFReportBE)
        {
            DataTable dt = new DataTable();
            List<OTIFReportBE> lstVendorExcludedDeliveryType = new List<OTIFReportBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oOTIFReportBE.Action);
                param[index++] = new SqlParameter("@SelectedExcludedDeliveryTypeId", oOTIFReportBE.SelectedExcludedDeliveryTypeId);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_VendorReportingExclusion", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    OTIFReportBE oVendorExclusionBE = new OTIFReportBE();
                    oVendorExclusionBE.SelectedExcludedDeliveryTypeId = dr["DeliveryTypeID"].ToString();
                    oVendorExclusionBE.CountryDeliveryType = dr["CountryDeliveryType"].ToString();
                    lstVendorExcludedDeliveryType.Add(oVendorExclusionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstVendorExcludedDeliveryType;

        }


        public bool CheckForExclusionExistanceDAL(OTIFReportBE oOTIFReportBE)
        {
            bool IsExclusionExist = false;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oOTIFReportBE.Action);
                param[index++] = new SqlParameter("@ExclusionNarrative", oOTIFReportBE.ExclusionNarrative);

                DataSet dsIsExclusionExist = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_VendorReportingExclusion", param);
                if (dsIsExclusionExist != null && dsIsExclusionExist.Tables.Count > 0)
                {
                    DataTable dtIsExclusionExist = dsIsExclusionExist.Tables[0];
                    if (dtIsExclusionExist.Rows.Count > 0)
                        IsExclusionExist = true;
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return IsExclusionExist;
        }

        public List<OTIFReportBE> GetExcludedVendorDAL(OTIFReportBE oOTIFReportBE)
        {
            DataTable dt = new DataTable();
            List<OTIFReportBE> lstVendorExcluded = new List<OTIFReportBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", oOTIFReportBE.Action);
                param[index++] = new SqlParameter("@ExclusionID", oOTIFReportBE.ExclusionID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_VendorReportingExclusion", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    OTIFReportBE oVendorExclusionBE = new OTIFReportBE();
                    oVendorExclusionBE.SelectedExcludedVendorId = dr["VendorID"].ToString();
                    oVendorExclusionBE.VendorName = dr["VendorName"].ToString();
                    lstVendorExcluded.Add(oVendorExclusionBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstVendorExcluded;

        }

        public int? InsertVendorTrackingInfoDAL(OTIFReportBE oOTIFReportBE)
        {
            int? statusFlag = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[index++] = new SqlParameter("@Action", oOTIFReportBE.Action);
                param[index++] = new SqlParameter("@ReportGeneratedDate", DateTime.Now);
                param[index++] = new SqlParameter("@Vendor", oOTIFReportBE.Vendor.VendorName);
                param[index++] = new SqlParameter("@VendorCountry", oOTIFReportBE.Vendor.CountryName);
                param[index++] = new SqlParameter("@UserId", oOTIFReportBE.User.UserID);
                param[index++] = new SqlParameter("@VendorID", oOTIFReportBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@ReportMonth", oOTIFReportBE.ReportMonth);
                param[index++] = new SqlParameter("@CommunicationSentTo", oOTIFReportBE.User.EmailId);


                statusFlag = SqlHelper.ExecuteNonQuery(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spOTIF_Reports", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return statusFlag;
        }

        public void ExcecuteOTIF_DetailReportSchedulerDAL()
        {
            try
            {
                SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spOTIF_DetailReprotScheduler");
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }

        }

        public List<OTIF_LeadTimeSKUBE> GetSelectedPOLeadListSkuDAL(OTIF_LeadTimeSKUBE oUp_PurchaseOrderDetailBE)
        {
            List<OTIF_LeadTimeSKUBE> Up_PurchaseOrderDetailBEList = new List<OTIF_LeadTimeSKUBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oUp_PurchaseOrderDetailBE.SelectedVendorIDs);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    OTIF_LeadTimeSKUBE oNewOTIF_LeadTimeSkuBEList = new OTIF_LeadTimeSKUBE();
                    oNewOTIF_LeadTimeSkuBEList.PurchaseOrderID = Convert.ToInt32(dr["SkuExclusionId"]);
                    oNewOTIF_LeadTimeSkuBEList.Purchase_order = dr["PurchaseNumber"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.DateApplied = dr["DateApplied"] != DBNull.Value ? Convert.ToDateTime(dr["DateApplied"]) : (DateTime?)null;
                    oNewOTIF_LeadTimeSkuBEList.ProductDescription = dr["ProductDescription"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.CountryName = dr["CountryName"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.SiteName = dr["SiteName"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.UserName = dr["UserName"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.Reason = dr["Reason"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.VendorName = dr["VendorName"].ToString();
                    Up_PurchaseOrderDetailBEList.Add(oNewOTIF_LeadTimeSkuBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }


        public List<OTIF_LeadTimeSKUBE> GetRevisedSKULeadListDAL(OTIF_LeadTimeSKUBE oUp_PurchaseOrderDetailBE)
        {
            List<OTIF_LeadTimeSKUBE> Up_PurchaseOrderDetailBEList = new List<OTIF_LeadTimeSKUBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oUp_PurchaseOrderDetailBE.Action);
                param[index++] = new SqlParameter("@PurchaseOrderID", oUp_PurchaseOrderDetailBE.PurchaseOrderID);
                param[index++] = new SqlParameter("@Order_raised", oUp_PurchaseOrderDetailBE.Order_raised);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    OTIF_LeadTimeSKUBE oNewOTIF_LeadTimeSkuBEList = new OTIF_LeadTimeSKUBE();
                    oNewOTIF_LeadTimeSkuBEList.PurchaseOrderID = Convert.ToInt32(dr["SkuExclusionId"]);
                    oNewOTIF_LeadTimeSkuBEList.Purchase_order = dr["PurchaseNumber"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.DateApplied = dr["DateApplied"] != DBNull.Value ? Convert.ToDateTime(dr["DateApplied"]) : (DateTime?)null;
                    oNewOTIF_LeadTimeSkuBEList.ProductDescription = dr["ProductDescription"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.CountryName = dr["CountryName"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.SiteName = dr["SiteName"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.UserName = dr["UserName"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.Reason = dr["Reason"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.VendorName = dr["VendorName"].ToString();
                    oNewOTIF_LeadTimeSkuBEList.Order_raised = dr["OrderRaised"] != DBNull.Value ? Convert.ToDateTime(dr["OrderRaised"]) : (DateTime?)null;
                    Up_PurchaseOrderDetailBEList.Add(oNewOTIF_LeadTimeSkuBEList);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return Up_PurchaseOrderDetailBEList;
        }

        public int? addEditUP_RevisedSKULeadTimeDetailDAL(OTIF_LeadTimeSKUBE oOTIF_LeadTimeSKUBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[7];
                param[index++] = new SqlParameter("@Action", oOTIF_LeadTimeSKUBE.Action);
                param[index++] = new SqlParameter("@SiteID", oOTIF_LeadTimeSKUBE.SiteID );
                param[index++] = new SqlParameter("@UserName", oOTIF_LeadTimeSKUBE.UserName);
                param[index++] = new SqlParameter("@Reason", oOTIF_LeadTimeSKUBE.Reason);
                param[index++] = new SqlParameter("@SelectedProductCodes", oOTIF_LeadTimeSKUBE.SelectedProductCodes);
                param[index++] = new SqlParameter("@PurchaseOrderID", oOTIF_LeadTimeSKUBE.Purchase_order);
                param[index++] = new SqlParameter("@Order_raised", oOTIF_LeadTimeSKUBE.Order_raised);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spUP_PurchaseOrderDetails", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataSet GetRMSCategoryReportDAL(RMSCategoryBE oRMSCategoryBE)
        {
            DataSet Result = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[11];
                param[index++] = new SqlParameter("@Action", oRMSCategoryBE.Action);
                param[index++] = new SqlParameter("@SelectedCountryIDs", oRMSCategoryBE.SelectedCountryIDs);
                param[index++] = new SqlParameter("@SelectedSiteIDs", oRMSCategoryBE.SelectedSiteIDs);
                param[index++] = new SqlParameter("@SelectedVendorIDs", oRMSCategoryBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@SelectedStockPlannerIDs", oRMSCategoryBE.SelectedStockPlannerIDs);
                param[index++] = new SqlParameter("@SelectedItemClassification", oRMSCategoryBE.SelectedItemClassification);
                param[index++] = new SqlParameter("@DateTo", oRMSCategoryBE.DateTo);
                param[index++] = new SqlParameter("@DateFrom", oRMSCategoryBE.DateFrom);
                param[index++] = new SqlParameter("@PurchaseOrderDue", oRMSCategoryBE.PurchaseOrderDue);
                param[index++] = new SqlParameter("@PurchaseOrderReceived", oRMSCategoryBE.PurchaseOrderReceived);
                param[index++] = new SqlParameter("@RMSCategoryIDs", oRMSCategoryBE.SelectedRMSCategoryIDs);

                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("DAL SP name           : spRMS_ReportsMonthly");
                sbMessage.Append("\r\n");

                for (int i = 0; i <= param.Length - 1; i++)
                {
                    sbMessage.Append("DAL Param Values      : " + i.ToString() + ") " + param[i].ParameterName + " - " + Convert.ToString(param[i].Value));
                    sbMessage.Append("\r\n");
                }

                LogUtility.SaveTraceLogEntry(sbMessage);
                if (oRMSCategoryBE.Action.Contains("Monthly"))
                {
                    Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spRMS_ReportsMonthly", param);
                }
                else
                {
                    Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spRMS_ReportsSummary", param);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
    }
}

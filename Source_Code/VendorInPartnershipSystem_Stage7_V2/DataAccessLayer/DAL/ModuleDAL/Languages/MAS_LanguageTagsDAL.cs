﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.Languages;
using System.Data;
using System.Data.SqlClient;
using Utilities;

namespace DataAccessLayer.ModuleDAL.Languages
{
    public class MAS_LanguageTagsDAL
    {
        public List<MAS_LanguageTagsBE> GetAllLanguageTagsDAL(MAS_LanguageTagsBE oMAS_LanguageTagsBE)
        {
            List<MAS_LanguageTagsBE> oMAS_LanguageTagsBEList = new List<MAS_LanguageTagsBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", oMAS_LanguageTagsBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_LanguageTags", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    MAS_LanguageTagsBE oNewMAS_LanguageTagsBE = new MAS_LanguageTagsBE();
                    oNewMAS_LanguageTagsBE.LanguageTagsId = Convert.ToInt32(dr["LanguageTagsId"]);
                    oNewMAS_LanguageTagsBE.LanguageKey = dr["LanguageKey"].ToString();
                    oNewMAS_LanguageTagsBE.English = dr["English"] == DBNull.Value ? null : Convert.ToString(dr["English"]);
                    oNewMAS_LanguageTagsBE.French = dr["French"] == DBNull.Value ? null : Convert.ToString(dr["French"]);
                    oNewMAS_LanguageTagsBE.German = dr["German"] == DBNull.Value ? null : Convert.ToString(dr["German"]);
                    oNewMAS_LanguageTagsBE.Dutch = dr["Dutch"] == DBNull.Value ? null : Convert.ToString(dr["Dutch"]);
                    oNewMAS_LanguageTagsBE.Spanish = dr["Spanish"] == DBNull.Value ? null : Convert.ToString(dr["Spanish"]);
                    oNewMAS_LanguageTagsBE.Italian = dr["Italian"] == DBNull.Value ? null : Convert.ToString(dr["Italian"]);
                    oNewMAS_LanguageTagsBE.Czech = dr["Czech"] == DBNull.Value ? null : Convert.ToString(dr["Czech"]);
                    oMAS_LanguageTagsBEList.Add(oNewMAS_LanguageTagsBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_LanguageTagsBEList;
        }


        public int? EditLanguageTagDAL(MAS_LanguageTagsBE oMAS_LanguageTagsBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oMAS_LanguageTagsBE.Action);
                param[index++] = new SqlParameter("@LanguageTagsId", oMAS_LanguageTagsBE.LanguageTagsId);
                param[index++] = new SqlParameter("@LanguageName", oMAS_LanguageTagsBE.LanguageName);
                param[index++] = new SqlParameter("@LanguageText", oMAS_LanguageTagsBE.LanguageText);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMAS_LanguageTags", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

    }
}
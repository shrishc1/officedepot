﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

using Utilities;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;


namespace DataAccessLayer.ModuleDAL.Discrepancy.LogDiscrepancy
{
    public class DISLog_WorkFlowDAL : BaseDAL
    {

        public int? addWorkFlowActionsDAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[253];
                param[index++] = new SqlParameter("@Action", oDISLog_WorkFlowBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyWorkFlowID", oDISLog_WorkFlowBE.DiscrepancyWorkFlowID);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDISLog_WorkFlowBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@Comment", oDISLog_WorkFlowBE.Comment);
                param[index++] = new SqlParameter("@StageCompletedForWhom", oDISLog_WorkFlowBE.StageCompletedForWhom);
                param[index++] = new SqlParameter("@VenCommunicationType", oDISLog_WorkFlowBE.VenCommunicationType);
                param[index++] = new SqlParameter("@CarrierWhoWillCollectGoods", oDISLog_WorkFlowBE.CarrierWhoWillCollectGoods);
                param[index++] = new SqlParameter("@CarrierOther", oDISLog_WorkFlowBE.CarrierOther);
                param[index++] = new SqlParameter("@StageCompleted", oDISLog_WorkFlowBE.StageCompleted);

                param[index++] = new SqlParameter("@INV001_GoodsKeptDecision", oDISLog_WorkFlowBE.INV001_GoodsKeptDecision);
                param[index++] = new SqlParameter("@INV001_NAX_PO", oDISLog_WorkFlowBE.INV001_NAX_PO);

                param[index++] = new SqlParameter("@VEN002_ReasonForGoodsOverDelivered", oDISLog_WorkFlowBE.VEN002_ReasonForGoodsOverDelivered);
               
                param[index++] = new SqlParameter("@ACP001_NoteType", oDISLog_WorkFlowBE.ACP001_NoteType);
                param[index++] = new SqlParameter("@ACP001_NoteNumber", oDISLog_WorkFlowBE.ACP001_NoteNumber);
                param[index++] = new SqlParameter("@ACP001_ValueExpected", oDISLog_WorkFlowBE.ACP001_ValueExpected);
                param[index++] = new SqlParameter("@ACP001_ActualValue", oDISLog_WorkFlowBE.ACP001_ActualValue);             
              

                param[index++] = new SqlParameter("@ACP002_NoteType", oDISLog_WorkFlowBE.ACP002_NoteType);
                param[index++] = new SqlParameter("@ACP002_NoteNumber", oDISLog_WorkFlowBE.ACP002_NoteNumber);
                param[index++] = new SqlParameter("@ACP002_CarriageCost", oDISLog_WorkFlowBE.ACP002_CarriageCost);
                param[index++] = new SqlParameter("@ACP002_ActualValue", oDISLog_WorkFlowBE.ACP002_ActualValue);            
              

                param[index++] = new SqlParameter("@GIN002_IsGoodsCollected", oDISLog_WorkFlowBE.GIN002_IsGoodsCollected);
                param[index++] = new SqlParameter("@GIN002_ArrangedCollectionDate", oDISLog_WorkFlowBE.GIN002_ArrangedCollectionDate);
                param[index++] = new SqlParameter("@GIN002_Carrier", oDISLog_WorkFlowBE.GIN002_Carrier);
                param[index++] = new SqlParameter("@GIN002_CollectionAuthorisationNumber", oDISLog_WorkFlowBE.GIN002_CollectionAuthorisationNumber);
            


                param[index++] = new SqlParameter("@GIN003_Carrier", oDISLog_WorkFlowBE.GIN003_Carrier);
                param[index++] = new SqlParameter("@GIN003_CarrierTrackingNumber", oDISLog_WorkFlowBE.GIN003_CarrierTrackingNumber);
                param[index++] = new SqlParameter("@GIN003_CarriageCharge", oDISLog_WorkFlowBE.GIN003_CarriageCharge);
                param[index++] = new SqlParameter("@GIN003_PalletsExchanged", oDISLog_WorkFlowBE.GIN003_PalletsExchanged);
                param[index++] = new SqlParameter("@GIN003_CartonsExchanged", oDISLog_WorkFlowBE.GIN003_CartonsExchanged);
               


                param[index++] = new SqlParameter("@VEN001_ReasonForGoodsOverDelivered", oDISLog_WorkFlowBE.VEN001_ReasonForGoodsOverDelivered);
                param[index++] = new SqlParameter("@VEN001_HowGoodsReturned", oDISLog_WorkFlowBE.VEN001_HowGoodsReturned);
                param[index++] = new SqlParameter("@VEN001_DateGoodsAreToBeCollected", oDISLog_WorkFlowBE.VEN001_DateGoodsAreToBeCollected);
                param[index++] = new SqlParameter("@VEN001_CarrierWhoWillCollectGoods", oDISLog_WorkFlowBE.VEN001_CarrierWhoWillCollectGoods);
                param[index++] = new SqlParameter("@VEN001_CollectionAuthorisationNumber", oDISLog_WorkFlowBE.VEN001_CollectionAuthorisationNumber);
             
                param[index++] = new SqlParameter("@INV002_GoodsDecision", oDISLog_WorkFlowBE.INV002_GoodsDecision);
             
                param[index++] = new SqlParameter("@INV007_IsCharged", oDISLog_WorkFlowBE.INV007_IsCharged);
                param[index++] = new SqlParameter("@INV007_CarriageCost", oDISLog_WorkFlowBE.INV007_CarriageCost);
              

                param[index++] = new SqlParameter("@INV010_IsCharged", oDISLog_WorkFlowBE.INV010_IsCharged);
                param[index++] = new SqlParameter("@INV010_LabourCost", oDISLog_WorkFlowBE.INV010_LabourCost);
               

                param[index++] = new SqlParameter("@ACP003_NoteType", oDISLog_WorkFlowBE.ACP003_NoteType);
                param[index++] = new SqlParameter("@ACP003_NoteNumber", oDISLog_WorkFlowBE.ACP003_NoteNumber);
                param[index++] = new SqlParameter("@ACP003_LabourCost", oDISLog_WorkFlowBE.ACP003_LabourCost);
                param[index++] = new SqlParameter("@ACP003_ActualValue", oDISLog_WorkFlowBE.ACP003_ActualValue);
            

                param[index++] = new SqlParameter("@INV009_IsCharged", oDISLog_WorkFlowBE.INV009_IsCharged);
                param[index++] = new SqlParameter("@INV009_NoOfPallets", oDISLog_WorkFlowBE.INV009_NoOfPallets);
                param[index++] = new SqlParameter("@INV009_LabourCost", oDISLog_WorkFlowBE.INV009_LabourCost);
             

                param[index++] = new SqlParameter("@INV008_ReworkType", oDISLog_WorkFlowBE.INV008_ReworkType);
               

                param[index++] = new SqlParameter("@VEN017_HowGoodsReturned", oDISLog_WorkFlowBE.VEN017_HowGoodsReturned);
                param[index++] = new SqlParameter("@VEN017_DateGoodsAreToBeCollected", oDISLog_WorkFlowBE.VEN017_DateGoodsAreToBeCollected);
              

                param[index++] = new SqlParameter("@VEN017_CollectionAuthorisationNumber", oDISLog_WorkFlowBE.VEN017_CollectionAuthorisationNumber);
               

                param[index++] = new SqlParameter("@GIN010_LabourCost", oDISLog_WorkFlowBE.GIN010_LabourCost);
               


                param[index++] = new SqlParameter("@VEN011_HowGoodsReturned", oDISLog_WorkFlowBE.VEN011_HowGoodsReturned);
                param[index++] = new SqlParameter("@VEN011_DateGoodsAreToBeCollected", oDISLog_WorkFlowBE.VEN011_DateGoodsAreToBeCollected);
                param[index++] = new SqlParameter("@VEN011_CarrierWhoWillCollectGoods", oDISLog_WorkFlowBE.VEN011_CarrierWhoWillCollectGoods);
                param[index++] = new SqlParameter("@VEN011_CollectionAuthorisationNumber", oDISLog_WorkFlowBE.VEN011_CollectionAuthorisationNumber);
               

                param[index++] = new SqlParameter("@VEN005_HowGoodsReturned", oDISLog_WorkFlowBE.VEN005_HowGoodsReturned);
                param[index++] = new SqlParameter("@VEN005_DateGoodsAreToBeCollected", oDISLog_WorkFlowBE.VEN005_DateGoodsAreToBeCollected);
                param[index++] = new SqlParameter("@VEN005_CarrierWhoWillCollectGoods", oDISLog_WorkFlowBE.VEN005_CarrierWhoWillCollectGoods);
                param[index++] = new SqlParameter("@VEN005_CollectionAuthorisationNumber", oDISLog_WorkFlowBE.VEN005_CollectionAuthorisationNumber);
                

                param[index++] = new SqlParameter("@INV006_GoodsDecision", oDISLog_WorkFlowBE.INV006_GoodsDecision);
              
                param[index++] = new SqlParameter("@PurchaseOrderNumber", oDISLog_WorkFlowBE.PurchaseOrderNumber);
               

                param[index++] = new SqlParameter("@VEN006_HowGoodsReturned", oDISLog_WorkFlowBE.VEN006_HowGoodsReturned);
                param[index++] = new SqlParameter("@VEN006_DateGoodsAreToBeCollected", oDISLog_WorkFlowBE.VEN006_DateGoodsAreToBeCollected);
                param[index++] = new SqlParameter("@VEN006_CollectionAuthorisationNumber", oDISLog_WorkFlowBE.VEN006_CollectionAuthorisationNumber);
               
                param[index++] = new SqlParameter("@UserID", Convert.ToInt32(System.Web.HttpContext.Current.Session["UserID"]));
                param[index++] = new SqlParameter("@INV012_PassBackToGoodsIn", oDISLog_WorkFlowBE.INV012_PassBackToGoodsIn);
                param[index++] = new SqlParameter("@Ven26_YesNo", oDISLog_WorkFlowBE.Ven26_YesNo);
                
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        //not in use
        public List<DISLog_WorkFlowBE> GetWorkFlowActionsDAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {
            
            List<DISLog_WorkFlowBE> DISLog_WorkFlowBEList = new List<DISLog_WorkFlowBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDISLog_WorkFlowBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDISLog_WorkFlowBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@WorkflowActionID", oDISLog_WorkFlowBE.WorkflowActionID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DISLog_WorkFlowBE oNewDISLog_WorkFlowBE = new DISLog_WorkFlowBE();

                    oNewDISLog_WorkFlowBE.WorkflowActionID = Convert.ToInt32(dr["WorkflowActionID"].ToString());
                    oNewDISLog_WorkFlowBE.DiscrepancyLogID = Convert.ToInt32(dr["DiscrepancyLogID"].ToString());
                    oNewDISLog_WorkFlowBE.INV001_GoodsKeptDecision = dr["INV001_GoodsKeptDecision"] != DBNull.Value ? dr["INV001_GoodsKeptDecision"].ToString() : "";
                    oNewDISLog_WorkFlowBE.INV001_NAX_PO = dr["INV001_NAX_PO"] != DBNull.Value ? dr["INV001_NAX_PO"].ToString() : "";
                   // oNewDISLog_WorkFlowBE.INV001_Comments = dr["INV001_Comments"] != DBNull.Value ? dr["INV001_Comments"].ToString() : "";
                   // oNewDISLog_WorkFlowBE.INV001_StageCompleted = dr["INV001_StageCompleted"] != DBNull.Value ? (dr["INV001_GoodsKeptDecision"].ToString().ToLower() == "true" ? true : false) : false;
                   // oNewDISLog_WorkFlowBE.GIN001_Comments = dr["GIN001_Comments"] != DBNull.Value ? dr["GIN001_Comments"].ToString() : "";
                   // oNewDISLog_WorkFlowBE.GIN001_StageCompleted = dr["GIN001_StageCompleted"] != DBNull.Value ? (dr["GIN001_StageCompleted"].ToString().ToLower() == "true" ? true : false) : false;

                    DISLog_WorkFlowBEList.Add(oNewDISLog_WorkFlowBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DISLog_WorkFlowBEList;
        }

        public List<DISLog_WorkFlowBE> GetPOValueExpectedDAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {

            List<DISLog_WorkFlowBE> DISLog_WorkFlowBEList = new List<DISLog_WorkFlowBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDISLog_WorkFlowBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDISLog_WorkFlowBE.DiscrepancyLogID);
                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DISLog_WorkFlowBE oNewDISLog_WorkFlowBE = new DISLog_WorkFlowBE();

                    oNewDISLog_WorkFlowBE.ACP001_ValueExpected = dr["ValueExpected"] != DBNull.Value ? Convert.ToDecimal(dr["ValueExpected"].ToString()) : (decimal?)null;
                    oNewDISLog_WorkFlowBE.ACP001_ShortageValueExpected = dr["ShortageValueExpected"] != DBNull.Value ? Convert.ToDecimal(dr["ShortageValueExpected"].ToString()) : (decimal?)null;
                    oNewDISLog_WorkFlowBE.ACP001_WrongPackValueExpected = dr["WrongPackValueExpected"] != DBNull.Value ? Convert.ToDecimal(dr["WrongPackValueExpected"].ToString()) : (decimal?)null;
                    oNewDISLog_WorkFlowBE.ACP001_DamageValueExpected = dr["DamageValueExpected"] != DBNull.Value ? Convert.ToDecimal(dr["DamageValueExpected"].ToString()) : (decimal?)null;
                    oNewDISLog_WorkFlowBE.ACP001_QualityValueExpected = dr["QualityValueExpected"] != DBNull.Value ? Convert.ToDecimal(dr["QualityValueExpected"].ToString()) : (decimal?)null;
                    oNewDISLog_WorkFlowBE.ACP001_IncorrectProductValueExpected = dr["IncorrectProductValueExpected"] != DBNull.Value ? Convert.ToDecimal(dr["IncorrectProductValueExpected"].ToString()) : (decimal?)null;
                    
                    DISLog_WorkFlowBEList.Add(oNewDISLog_WorkFlowBE);
                }

            }

            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DISLog_WorkFlowBEList;

        }

        public List<DISLog_WorkFlowBE> GetPOCarriageCostDAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {

            List<DISLog_WorkFlowBE> DISLog_WorkFlowBEList = new List<DISLog_WorkFlowBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDISLog_WorkFlowBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDISLog_WorkFlowBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@StageCompletedForWhom", oDISLog_WorkFlowBE.StageCompletedForWhom);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    DISLog_WorkFlowBE oNewDISLog_WorkFlowBE = new DISLog_WorkFlowBE();

                    oNewDISLog_WorkFlowBE.GIN003_CarriageCharge = dr["CarriageCost"] != DBNull.Value ? Convert.ToDecimal(dr["CarriageCost"].ToString()) : (decimal?)null;

                    DISLog_WorkFlowBEList.Add(oNewDISLog_WorkFlowBE);
                }

            }

            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return DISLog_WorkFlowBEList;

        }

        public DataTable GetVendorCollectionDataDAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {

            //List<DISLog_WorkFlowBE> DISLog_WorkFlowBEList = new List<DISLog_WorkFlowBE>();
            DataTable dt = new DataTable();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDISLog_WorkFlowBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDISLog_WorkFlowBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@StageCompletedForWhom", oDISLog_WorkFlowBE.StageCompletedForWhom);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds.Tables[0];

                //foreach (DataRow dr in dt.Rows)
                //{
                //    DISLog_WorkFlowBE oNewDISLog_WorkFlowBE = new DISLog_WorkFlowBE();

                //    oNewDISLog_WorkFlowBE.GIN002_ArrangedCollectionDate=dr["VEN001_DateGoodsAreToBeCollected"]!=DBNull.Value?Convert.ToDateTime( dr["VEN001_DateGoodsAreToBeCollected"]):(DateTime?)null;
                //    oNewDISLog_WorkFlowBE.GIN002_Carrier = dr["CarrierName"] != DBNull.Value?dr["CarrierName"].ToString():"" ;
                //    oNewDISLog_WorkFlowBE.GIN002_CollectionAuthorisationNumber = dr["VEN001_CollectionAuthorisationNumber"] != DBNull.Value ? dr["VEN001_CollectionAuthorisationNumber"].ToString() : "";
                //    oNewDISLog_WorkFlowBE.VEN001_CarrierWhoWillCollectGoods = dr["VEN001_CarrierWhoWillCollectGoods"] != DBNull.Value ? Convert.ToInt32(dr["VEN001_CarrierWhoWillCollectGoods"].ToString()) : (int?)null;

                //    DISLog_WorkFlowBEList.Add(oNewDISLog_WorkFlowBE);
                //}

            }

            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            //return DISLog_WorkFlowBEList;
            return dt;

        }

        public DataTable GetDataForSpecificActionDAL(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDISLog_WorkFlowBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDISLog_WorkFlowBE.DiscrepancyLogID);
                param[index++] = new SqlParameter("@StageCompletedForWhom", oDISLog_WorkFlowBE.StageCompletedForWhom);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public string GetVendorAccountPayablesEmail(DISLog_WorkFlowBE oDISLog_WorkFlowBE)
        {
            string VendorAccountPayablesEmail = string.Empty;
            DataTable dt = new DataTable();
            try
            {

                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", oDISLog_WorkFlowBE.Action);
                param[index++] = new SqlParameter("@DiscrepancyLogID", oDISLog_WorkFlowBE.DiscrepancyLogID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spLogDiscrepancyWorkFlow", param);

                dt = ds.Tables[0];
                if (dt != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["EmailId"] != DBNull.Value) {
                            if (string.IsNullOrEmpty(VendorAccountPayablesEmail))
                                VendorAccountPayablesEmail = Convert.ToString(dr["EmailId"]);
                            else
                                VendorAccountPayablesEmail = string.Format("{0}, {1}", VendorAccountPayablesEmail, Convert.ToString(dr["EmailId"]));
                        }
                    }
                }

                #region Old code ...
                //dt = ds.Tables[0];
                //if (dt != null)
                //{
                //    foreach (DataRow dr in dt.Rows)
                //    {
                //        VendorAccountPayablesEmail += ", " + dr["EmailId"].ToString();
                //    }

                //    //VendorAccountPayablesEmail.TrimStart(',');

                //    VendorAccountPayablesEmail = VendorAccountPayablesEmail.Substring(2);
                //}
                #endregion
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return VendorAccountPayablesEmail;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Data;

using Utilities;
using BusinessEntities.ModuleBE.StockOverview;


namespace DataAccessLayer.ModuleDAL.StockOverview
{
   public class MASSIT_GroupClassificationDAL
    {

       public List<MASSIT_GroupClassificationBE> GetGroupClassificationDAL(MASSIT_GroupClassificationBE oMASSIT_GroupClassificationBE)
       {
           List<MASSIT_GroupClassificationBE> MASSIT_GroupClassificationBEList = new List<MASSIT_GroupClassificationBE>();
           try
           {
               DataTable dt = new DataTable();
               int index = 0;
               SqlParameter[] param = new SqlParameter[2];
               param[index++] = new SqlParameter("@Action", oMASSIT_GroupClassificationBE.Action);
               param[index++] = new SqlParameter("@SiteID", oMASSIT_GroupClassificationBE.SiteID);

               DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_GroupClassification", param);

               dt = ds.Tables[0];

               foreach (DataRow dr in dt.Rows)
               {
                   MASSIT_GroupClassificationBE oNewMASSIT_GroupClassificationBE = new MASSIT_GroupClassificationBE();
                   oNewMASSIT_GroupClassificationBE.GroupClassificationID = Convert.ToInt32(dr["GroupClassificationID"]);
                   oNewMASSIT_GroupClassificationBE.SiteID = Convert.ToInt32(dr["SiteID"]);
                   oNewMASSIT_GroupClassificationBE.SKUEndingWith = dr["SKUEndingWith"].ToString();
                   oNewMASSIT_GroupClassificationBE.ReviewStatus = dr["ReviewStatus"]!=DBNull.Value?Convert.ToChar(dr["ReviewStatus"]):(char?)null;

                   MASSIT_GroupClassificationBEList.Add(oNewMASSIT_GroupClassificationBE);
               }
           }
           catch (Exception ex)
           {
               LogUtility.SaveErrorLogEntry(ex);
           }
           return MASSIT_GroupClassificationBEList;
       }


       public int? addEditGroupClassificationDAL(MASSIT_GroupClassificationBE oMASSIT_GroupClassificationBE)
       {
           int? intResult = 0;
           try
           {
               int index = 0;
               SqlParameter[] param = new SqlParameter[5];
               param[index++] = new SqlParameter("@Action", oMASSIT_GroupClassificationBE.Action);
               param[index++] = new SqlParameter("@GroupClassificationID", oMASSIT_GroupClassificationBE.GroupClassificationID);
               param[index++] = new SqlParameter("@SiteID", oMASSIT_GroupClassificationBE.SiteID);
               param[index++] = new SqlParameter("@SKUEndingWith", oMASSIT_GroupClassificationBE.SKUEndingWith);
               param[index++] = new SqlParameter("@ReviewStatus", oMASSIT_GroupClassificationBE.ReviewStatus);

               DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_GroupClassification", param);

               if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                   intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
               else
                   intResult = 0;

           }
           catch (Exception ex)
           {
               LogUtility.SaveErrorLogEntry(ex);
           }
           finally { }
           return intResult;
       }

       public int? deleteGroupClassificationDAL(MASSIT_GroupClassificationBE oMASSIT_GroupClassificationBE)
       {
           int? intResult = 0;
           try
           {
               int index = 0;
               SqlParameter[] param = new SqlParameter[2];
               param[index++] = new SqlParameter("@Action", oMASSIT_GroupClassificationBE.Action);
               param[index++] = new SqlParameter("@SiteID", oMASSIT_GroupClassificationBE.SiteID);
               DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spMASSIT_GroupClassification", param);

               if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                   intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
               else
                   intResult = 0;

           }
           catch (Exception ex)
           {
               LogUtility.SaveErrorLogEntry(ex);
           }
           finally { }
           return intResult;
       }
    }
}

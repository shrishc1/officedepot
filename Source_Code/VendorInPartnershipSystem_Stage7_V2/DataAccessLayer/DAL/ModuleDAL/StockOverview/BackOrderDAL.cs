﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.StockOverview;
using BusinessEntities.ModuleBE.Languages;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Utilities;
using BusinessEntities.ModuleBE.Languages.Languages;

namespace DataAccessLayer.ModuleDAL.StockOverview
{
    public class BackOrderDAL
    {

        public List<BackOrderBE> GetVendorComm1DAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
                    oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();

                    oBackOrderBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;
                    oBackOrderBE.SiteID = dr["BOSiteId"] != DBNull.Value ? Convert.ToInt32(dr["BOSiteId"].ToString()) : 0;
                    oBackOrderBE.BOReportingID = Convert.ToInt32(dr["BOReportingID"]);
                    oBackOrderBE.SKU.OD_SKU_NO = dr["OD_SKU_NO"] == DBNull.Value ? null : Convert.ToString(dr["OD_SKU_NO"]);
                    oBackOrderBE.PurchaseOrder.Purchase_order = dr["Purchase_order"] == DBNull.Value ? null : Convert.ToString(dr["Purchase_order"]);
                    oBackOrderBE.CountofBackOrder = dr["NoofBOWithPenalty"] != DBNull.Value ? Convert.ToInt32(dr["NoofBOWithPenalty"].ToString()) : 0;
                    oBackOrderBE.PotentialPenaltyCharge = dr["PotentialCharge"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["PotentialCharge"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.Vendor.StockPlannerName = string.IsNullOrWhiteSpace(dr["StockPlannerName"].ToString()) ? Convert.ToString(dr["StockPlannerNo"]) : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.StockPlannerContact = dr["SPContactNo"] == DBNull.Value ? null : Convert.ToString(dr["SPContactNo"]);
                    oBackOrderBE.APAddress1 = dr["APAddress1"] != DBNull.Value ? dr["APAddress1"].ToString() : string.Empty;
                    oBackOrderBE.APAddress2 = dr["APAddress2"] != DBNull.Value ? dr["APAddress2"].ToString() : string.Empty;
                    oBackOrderBE.APAddress3 = dr["APAddress3"] != DBNull.Value ? dr["APAddress3"].ToString() : string.Empty;
                    oBackOrderBE.APAddress4 = dr["APAddress4"] != DBNull.Value ? dr["APAddress4"].ToString() : string.Empty;
                    oBackOrderBE.APAddress5 = dr["APAddress5"] != DBNull.Value ? dr["APAddress5"].ToString() : string.Empty;
                    oBackOrderBE.APAddress6 = dr["APAddress6"] != DBNull.Value ? dr["APAddress6"].ToString() : string.Empty;
                    oBackOrderBE.APCountryID = dr["APCountryID"] != DBNull.Value ? Convert.ToInt32(dr["APCountryID"].ToString()) : (int?)null;
                    oBackOrderBE.APCountryName = dr["APCountryName"] != DBNull.Value ? dr["APCountryName"].ToString() : string.Empty;
                    oBackOrderBE.APPincode = dr["APPincode"] != DBNull.Value ? dr["APPincode"].ToString() : string.Empty;
                    if (dr.Table.Columns.Contains("InitialCommDate"))
                        oBackOrderBE.InitialCommDate = dr["InitialCommDate"] != DBNull.Value ? Convert.ToDateTime(dr["InitialCommDate"]) : (DateTime?)null;



                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public bool CheckSkuStatusByVendorDAL(BackOrderBE BackOrderBE)
        {
            bool IsAllVendorProcessed = true;
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@VendorID", BackOrderBE.Vendor.VendorID);
                DataSet dsVendorExist = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);


                if (dsVendorExist != null && dsVendorExist.Tables.Count > 0)
                {
                    DataTable dtVendorExist = dsVendorExist.Tables[0];
                    if (dtVendorExist.Rows.Count > 0)
                        IsAllVendorProcessed = false;
                }

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return IsAllVendorProcessed;
        }

        public List<BackOrderBE> GetVendorContactDetailsDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            DataTable dt = new DataTable();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SiteVendorID", BackOrderBE.Vendor.VendorID);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMASSIT_Vendor", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderNewBE = new BackOrderBE();
                    oBackOrderNewBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oBackOrderNewBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();

                    oBackOrderNewBE.Vendor.VendorName = dr["VendorName"] != DBNull.Value ? dr["VendorName"].ToString() : "";
                    oBackOrderNewBE.Vendor.VendorContactEmail = dr["VendorContactEmail"] != DBNull.Value ? dr["VendorContactEmail"].ToString() : "";
                    oBackOrderNewBE.Vendor.VendorContactFax = dr["VendorContactFax"] != DBNull.Value ? dr["VendorContactFax"].ToString() : "";
                    oBackOrderNewBE.Vendor.Language = dr["Language"] != DBNull.Value ? dr["Language"].ToString() : "";
                    if (dr.Table.Columns.Contains("LanguageID"))
                        oBackOrderNewBE.Vendor.LanguageID = dr["LanguageID"] != DBNull.Value ? Convert.ToInt32(dr["LanguageID"]) : 0;

                    if (dr.Table.Columns.Contains("FirstName"))
                        oBackOrderNewBE.User.UserName = dr["FirstName"] != DBNull.Value ? Convert.ToString(dr["FirstName"]) + " " + Convert.ToString(dr["LastName"])  : string.Empty;


                    lstBackOrderBEList.Add(oBackOrderNewBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

        public List<BackOrderBE> GetVendorInitialEmailListDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            DataTable dt = new DataTable();

            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[3];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@VendorID", BackOrderBE.Vendor.VendorID);
                param[index++] = new SqlParameter("@InitialCommDate", BackOrderBE.InitialCommDate);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderNewBE = new BackOrderBE();
                    oBackOrderNewBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oBackOrderNewBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();


                    oBackOrderNewBE.Vendor.VendorContactEmail = dr["VendorContactEmail"] != DBNull.Value ? dr["VendorContactEmail"].ToString() : "";
                    oBackOrderNewBE.Vendor.LanguageID = dr["LanguageID"] != DBNull.Value ? Convert.ToInt32(dr["LanguageID"]) : 0;
                    lstBackOrderBEList.Add(oBackOrderNewBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }



        public List<MAS_LanguageBE> GetLanguages()
        {
            DataTable dt = new DataTable();
            List<MAS_LanguageBE> oMAS_LanguageBEList = new List<MAS_LanguageBE>();
            try
            {
                int index = 0;


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spMAS_Language");
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        MAS_LanguageBE oNewMAS_LanguageBE = new MAS_LanguageBE();

                        oNewMAS_LanguageBE.LanguageID = dr["LanguageID"] != DBNull.Value ? Convert.ToInt32(dr["LanguageID"]) : 0;
                        oNewMAS_LanguageBE.Language = dr["Language"] != DBNull.Value ? Convert.ToString(dr["Language"]) : "";
                        oNewMAS_LanguageBE.ISOLanguageName = dr["ISOLanguageName"] != DBNull.Value ? Convert.ToString(dr["ISOLanguageName"]) : string.Empty;
                        oNewMAS_LanguageBE.IsEnable = dr["IsEnable"] != DBNull.Value ? Convert.ToBoolean(dr["IsEnable"]) : false;
                        oNewMAS_LanguageBE.IsActiveForRegistration = dr["IsActiveForRegistration"] != DBNull.Value ? Convert.ToBoolean(dr["IsActiveForRegistration"]) : false;
                        oNewMAS_LanguageBE.ImagePath = Convert.ToString(dr["ImagePath"]) + " " + Convert.ToString(dr["ImagePath"].ToString());

                        oMAS_LanguageBEList.Add(oNewMAS_LanguageBE);
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return oMAS_LanguageBEList;
        }

        /// <summary>
        /// Used to save communication detail
        /// </summary>
        /// <param name="oBackOrderMailBE"></param>
        /// <returns></returns>
        public int? AddCommunicationDetailDAL(BackOrderMailBE oBackOrderMailBE)
        {
            int? intResult = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[15];
                param[index++] = new SqlParameter("@Action", oBackOrderMailBE.Action);
                param[index++] = new SqlParameter("@BOReportingID", oBackOrderMailBE.BOReportingID);
                param[index++] = new SqlParameter("@CommunicationType", oBackOrderMailBE.CommunicationType);
                param[index++] = new SqlParameter("@Subject", oBackOrderMailBE.Subject);
                param[index++] = new SqlParameter("@SentTo", oBackOrderMailBE.SentTo);
                param[index++] = new SqlParameter("@SentDate", oBackOrderMailBE.SentDate);
                param[index++] = new SqlParameter("@Body", oBackOrderMailBE.Body);
                param[index++] = new SqlParameter("@SendByID", oBackOrderMailBE.SendByID);
                param[index++] = new SqlParameter("@LanguageId", oBackOrderMailBE.LanguageId);
                param[index++] = new SqlParameter("@MailSentInLanguage", oBackOrderMailBE.MailSentInLanguage);
                param[index++] = new SqlParameter("@CommunicationStatus", oBackOrderMailBE.CommunicationStatus);
                param[index++] = new SqlParameter("@SentToWithLink", oBackOrderMailBE.SentToWithLink);
                param[index++] = new SqlParameter("@VendorID", oBackOrderMailBE.VendorId);
                param[index++] = new SqlParameter("@SelectedBOReportingID", oBackOrderMailBE.SelectedBOIds);
                param[index++] = new SqlParameter("@VendorEmailIds", oBackOrderMailBE.VendorEmailIds);


                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorPenalty", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateBOIDDAL(BackOrderBE BackOrderBE)
        {
            int? intResult = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedBOReportingID", BackOrderBE.SelectedBOReportingID);


                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorPenalty", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        public int? UpdateVendorIDDAL(BackOrderBE BackOrderBE)
        {
            int? intResult = null;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);
                param[index++] = new SqlParameter("@SelectedVendorIDs", BackOrderBE.SelectedVendorIDs);


                var Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorPenalty", param);
                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<BackOrderBE> GetBOinitialCommDetailsDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
                    oBackOrderBE.PurchaseOrder = new BusinessEntities.ModuleBE.Upload.Up_PurchaseOrderDetailBE();

                    oBackOrderBE.Vendor.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"].ToString()) : 0;       
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);         
                    oBackOrderBE.Vendor.StockPlannerName = string.IsNullOrWhiteSpace(dr["StockPlannerName"].ToString()) ? Convert.ToString(dr["StockPlannerNo"]) : Convert.ToString(dr["StockPlannerNo"] + " - " + dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.StockPlannerContact = dr["SPContactNo"] == DBNull.Value ? null : Convert.ToString(dr["SPContactNo"]);
                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }



        public List<BackOrderBE> GetMediatorCommDetailsDAL(BackOrderBE BackOrderBE)
        {
            List<BackOrderBE> lstBackOrderBEList = new List<BackOrderBE>();
            try
            {
                DataTable dt = new DataTable();
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", BackOrderBE.Action);


                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorPenalty", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    BackOrderBE oBackOrderBE = new BackOrderBE();
                    oBackOrderBE.User = new BusinessEntities.ModuleBE.Security.SCT_UserBE();
                    oBackOrderBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    oBackOrderBE.SKU = new BusinessEntities.ModuleBE.Upload.UP_SKUBE();
                    oBackOrderBE.Vendor.Vendor_No = dr["Vendor_No"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_No"]);
                    oBackOrderBE.Vendor.VendorName = dr["VendorName"] == DBNull.Value ? null : Convert.ToString(dr["VendorName"]);
                    oBackOrderBE.Vendor.address1 = dr["VendorAddress1"] == DBNull.Value ? null : Convert.ToString(dr["VendorAddress1"]);
                    oBackOrderBE.Vendor.address2 = dr["VendorAddress2"] == DBNull.Value ? null : Convert.ToString(dr["VendorAddress2"]);
                    oBackOrderBE.Vendor.city = dr["VendorCity"] == DBNull.Value ? null : Convert.ToString(dr["VendorCity"]);
                    oBackOrderBE.Vendor.VMPPIN = dr["VMPPIN"] == DBNull.Value ? null : Convert.ToString(dr["VMPPIN"]);
                    oBackOrderBE.Vendor.VMPPOU = dr["VMPPOU"] == DBNull.Value ? null : Convert.ToString(dr["VMPPOU"]);
                    oBackOrderBE.Vendor.VendorContactEmail = dr["VendorEmail"] == DBNull.Value ? string.Empty: Convert.ToString(dr["VendorEmail"]);
                    oBackOrderBE.Vendor.LanguageID = dr["LanguageID"] == DBNull.Value ? 1 : Convert.ToInt32(dr["LanguageID"]);
                    oBackOrderBE.Vendor.StockPlannerName = string.IsNullOrWhiteSpace(dr["StockPlannerName"].ToString()) ? Convert.ToString(dr["StockPlannerNo"]) : Convert.ToString(dr["StockPlannerName"]);
                    oBackOrderBE.Vendor.StockPlannerNumber = dr["StockPlannerNo"] == DBNull.Value ? null : Convert.ToString(dr["StockPlannerNo"]);
                    oBackOrderBE.Vendor.StockPlannerContact = dr["SPContactNo"] == DBNull.Value ? null : Convert.ToString(dr["SPContactNo"]);
                    oBackOrderBE.APAddress1 = dr["APAddress1"] != DBNull.Value ? dr["APAddress1"].ToString() : string.Empty;
                    oBackOrderBE.APAddress2 = dr["APAddress2"] != DBNull.Value ? dr["APAddress2"].ToString() : string.Empty;
                    oBackOrderBE.APAddress3 = dr["APAddress3"] != DBNull.Value ? dr["APAddress3"].ToString() : string.Empty;
                    oBackOrderBE.APAddress4 = dr["APAddress4"] != DBNull.Value ? dr["APAddress4"].ToString() : string.Empty;
                    oBackOrderBE.APAddress5 = dr["APAddress5"] != DBNull.Value ? dr["APAddress5"].ToString() : string.Empty;
                    oBackOrderBE.APAddress6 = dr["APAddress6"] != DBNull.Value ? dr["APAddress6"].ToString() : string.Empty;
                    oBackOrderBE.APCountryID = dr["APCountryID"] != DBNull.Value ? Convert.ToInt32(dr["APCountryID"].ToString()) : (int?)null;
                    oBackOrderBE.APCountryName = dr["APCountryName"] != DBNull.Value ? dr["APCountryName"].ToString() : string.Empty;
                    oBackOrderBE.APPincode = dr["APPincode"] != DBNull.Value ? dr["APPincode"].ToString() : string.Empty;


                    oBackOrderBE.SKU.OD_SKU_NO = dr["OD_SKU_NO"] == DBNull.Value ? null : Convert.ToString(dr["OD_SKU_NO"]);
                    oBackOrderBE.SKU.Direct_SKU = dr["VikingSKU"] == DBNull.Value ? null : Convert.ToString(dr["VikingSKU"]);
                    oBackOrderBE.SKU.Vendor_Code = dr["Vendor_Code"] == DBNull.Value ? null : Convert.ToString(dr["Vendor_Code"]);
                    oBackOrderBE.SKU.DESCRIPTION = dr["DESCRIPTION"] == DBNull.Value ? null : Convert.ToString(dr["DESCRIPTION"]);
                    if (dr.Table.Columns.Contains("DateBOIncurred"))
                        oBackOrderBE.BOIncurredDate = dr["DateBOIncurred"] != DBNull.Value ? Convert.ToDateTime(dr["DateBOIncurred"]) : (DateTime?)null;                 
                    oBackOrderBE.SiteName = dr["Site"] == DBNull.Value ? null : Convert.ToString(dr["Site"]);
                    oBackOrderBE.NoofBO = dr["NumberofBO"] != DBNull.Value ? Convert.ToInt32(dr["NumberofBO"]) : 0;                   
                    oBackOrderBE.Vendor.VendorID = Convert.ToInt32(dr["BOVendorID"]);                 

                    oBackOrderBE.PotentialPenaltyCharge = dr["PotentialPenaltyCharge"] == DBNull.Value ? (decimal?)null : Convert.ToDecimal(dr["PotentialPenaltyCharge"]);            
                   oBackOrderBE.DisputedValue = dr["DisputedValue"] != DBNull.Value ? Convert.ToDecimal(dr["DisputedValue"]) : (decimal?)null;
                   oBackOrderBE.RevisedPenalty = dr["RevisedPenalty"] != DBNull.Value ? Convert.ToDecimal(dr["RevisedPenalty"]) : (decimal?)null;


                    lstBackOrderBEList.Add(oBackOrderBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return lstBackOrderBEList;
        }

    }
}

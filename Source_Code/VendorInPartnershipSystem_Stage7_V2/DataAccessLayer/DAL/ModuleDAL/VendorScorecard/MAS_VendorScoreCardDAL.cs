﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.VendorScorecard;
using Utilities;
using System.Data.SqlClient;
using System.Data;

namespace DataAccessLayer.ModuleDAL.VendorScorecard
{
    public class MAS_VendorScoreCardDAL : BaseDAL {
        public int? UpdateVendorScoreCardDAL(MAS_VendorScoreCardBE objVendorScoreCardBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[123];

                param[index++] = new SqlParameter("@Action", objVendorScoreCardBE.Action);
                //Overall Weight Property
                param[index++] = new SqlParameter("@OtifWeight", objVendorScoreCardBE.OtifWeight);
                param[index++] = new SqlParameter("@SchedulingWeight", objVendorScoreCardBE.SchedulingWeight);
                param[index++] = new SqlParameter("@DiscrepenciesWeight", objVendorScoreCardBE.DiscrepenciesWeight);
                param[index++] = new SqlParameter("@StockWeight", objVendorScoreCardBE.StockWeight);

                //Overall Score Property
                param[index++] = new SqlParameter("@FailureScore", objVendorScoreCardBE.FailureScore);
                param[index++] = new SqlParameter("@TargetScore", objVendorScoreCardBE.TargetScore);
                param[index++] = new SqlParameter("@IdealScore", objVendorScoreCardBE.IdealScore);

                //OTIF Weight and Targets Property
                param[index++] = new SqlParameter("@CatAWeight", objVendorScoreCardBE.CatAWeight);
                param[index++] = new SqlParameter("@CatBWeight", objVendorScoreCardBE.CatBWeight);
                param[index++] = new SqlParameter("@CatCWeight", objVendorScoreCardBE.CatCWeight);
                param[index++] = new SqlParameter("@CatDWeight", objVendorScoreCardBE.CatDWeight);
                param[index++] = new SqlParameter("@CatOWeight", objVendorScoreCardBE.CatOWeight);

                param[index++] = new SqlParameter("@CatAIdeal", objVendorScoreCardBE.CatAIdeal);
                param[index++] = new SqlParameter("@CatBIdeal", objVendorScoreCardBE.CatBIdeal);
                param[index++] = new SqlParameter("@CatCIdeal", objVendorScoreCardBE.CatCIdeal);
                param[index++] = new SqlParameter("@CatDIdeal", objVendorScoreCardBE.CatDIdeal);
                param[index++] = new SqlParameter("@CatOIdeal", objVendorScoreCardBE.CatOIdeal);

                param[index++] = new SqlParameter("@CatATarget", objVendorScoreCardBE.CatATarget);
                param[index++] = new SqlParameter("@CatBTarget", objVendorScoreCardBE.CatBTarget);
                param[index++] = new SqlParameter("@CatCTarget", objVendorScoreCardBE.CatCTarget);
                param[index++] = new SqlParameter("@CatDTarget", objVendorScoreCardBE.CatDTarget);
                param[index++] = new SqlParameter("@CatOTarget", objVendorScoreCardBE.CatOTarget);

                param[index++] = new SqlParameter("@CatAFailure", objVendorScoreCardBE.CatAFailure);
                param[index++] = new SqlParameter("@CatBFailure", objVendorScoreCardBE.CatBFailure);
                param[index++] = new SqlParameter("@CatCFailure", objVendorScoreCardBE.CatCFailure);
                param[index++] = new SqlParameter("@CatDFailure", objVendorScoreCardBE.CatDFailure);
                param[index++] = new SqlParameter("@CatOFailure", objVendorScoreCardBE.CatOFailure);

                //Discrepancies Weight and Targets Property
                param[index++] = new SqlParameter("@DiscWeight", objVendorScoreCardBE.DiscWeight);
                param[index++] = new SqlParameter("@AvgWeight", objVendorScoreCardBE.AvgWeight);

                param[index++] = new SqlParameter("@DiscIdeal", objVendorScoreCardBE.DiscIdeal);
                param[index++] = new SqlParameter("@AvgIdeal", objVendorScoreCardBE.AvgIdeal);

                param[index++] = new SqlParameter("@DiscTarget", objVendorScoreCardBE.DiscTarget);
                param[index++] = new SqlParameter("@AvgTarget", objVendorScoreCardBE.AvgTarget);

                param[index++] = new SqlParameter("@DiscFailure", objVendorScoreCardBE.DiscFailure);
                param[index++] = new SqlParameter("@AvgFailure", objVendorScoreCardBE.AvgFailure);

                //Scheduling Weight and Targets Property
                param[index++] = new SqlParameter("@Accepted", objVendorScoreCardBE.Accepted);
                param[index++] = new SqlParameter("@Refused", objVendorScoreCardBE.Refused);

                param[index++] = new SqlParameter("@AcceptedInsufficientNoticeWeight", objVendorScoreCardBE.AcceptedInsufficientNoticeWeight);
                param[index++] = new SqlParameter("@AcceptedRefusedToWaitWeight", objVendorScoreCardBE.AcceptedRefusedToWaitWeight);
                param[index++] = new SqlParameter("@AcceptedVolumeMismatchWeight", objVendorScoreCardBE.AcceptedVolumeMismatchWeight);
                param[index++] = new SqlParameter("@AcceptedIncorrecTimeWeight", objVendorScoreCardBE.AcceptedIncorrecTimeWeight);
                param[index++] = new SqlParameter("@AcceptedDamagePakagingWeight", objVendorScoreCardBE.AcceptedDamagePakagingWeight);
                param[index++] = new SqlParameter("@AcceptedUnsafeWeight", objVendorScoreCardBE.AcceptedUnsafeWeight);
                param[index++] = new SqlParameter("@AcceptedPaperworkWeight", objVendorScoreCardBE.AcceptedPaperworkWeight);
                param[index++] = new SqlParameter("@AcceptedUnscheduleWeight", objVendorScoreCardBE.AcceptedUnscheduleWeight);
                param[index++] = new SqlParameter("@AcceptedOtherWeight", objVendorScoreCardBE.AcceptedOtherWeight);

                param[index++] = new SqlParameter("@AcceptedInsufficientNoticeIdeal", objVendorScoreCardBE.AcceptedInsufficientNoticeIdeal);
                param[index++] = new SqlParameter("@AcceptedRefusedToWaitIdeal", objVendorScoreCardBE.AcceptedRefusedToWaitIdeal);
                param[index++] = new SqlParameter("@AcceptedVolumeMismatchIdeal", objVendorScoreCardBE.AcceptedVolumeMismatchIdeal);
                param[index++] = new SqlParameter("@AcceptedIncorrecTimeIdeal", objVendorScoreCardBE.AcceptedIncorrecTimeIdeal);
                param[index++] = new SqlParameter("@AcceptedDamagePakagingIdeal", objVendorScoreCardBE.AcceptedDamagePakagingIdeal);
                param[index++] = new SqlParameter("@AcceptedUnsafeIdeal", objVendorScoreCardBE.AcceptedUnsafeIdeal);
                param[index++] = new SqlParameter("@AcceptedPaperworkIdeal", objVendorScoreCardBE.AcceptedPaperworkIdeal);
                param[index++] = new SqlParameter("@AcceptedUnscheduleIdeal", objVendorScoreCardBE.AcceptedUnscheduleIdeal);
                param[index++] = new SqlParameter("@AcceptedOtherIdeal", objVendorScoreCardBE.AcceptedOtherIdeal);

                param[index++] = new SqlParameter("@AcceptedInsufficientNoticeTarget", objVendorScoreCardBE.AcceptedInsufficientNoticeTarget);
                param[index++] = new SqlParameter("@AcceptedRefusedToWaitTarget", objVendorScoreCardBE.AcceptedRefusedToWaitTarget);
                param[index++] = new SqlParameter("@AcceptedVolumeMismatchTarget", objVendorScoreCardBE.AcceptedVolumeMismatchTarget);
                param[index++] = new SqlParameter("@AcceptedIncorrecTimeTarget", objVendorScoreCardBE.AcceptedIncorrecTimeTarget);
                param[index++] = new SqlParameter("@AcceptedDamagePakagingTarget", objVendorScoreCardBE.AcceptedDamagePakagingTarget);
                param[index++] = new SqlParameter("@AcceptedUnsafeTarget", objVendorScoreCardBE.AcceptedUnsafeTarget);
                param[index++] = new SqlParameter("@AcceptedPaperworkTarget", objVendorScoreCardBE.AcceptedPaperworkTarget);
                param[index++] = new SqlParameter("@AcceptedUnscheduleTarget", objVendorScoreCardBE.AcceptedUnscheduleTarget);
                param[index++] = new SqlParameter("@AcceptedOtherTarget", objVendorScoreCardBE.AcceptedOtherTarget);

                param[index++] = new SqlParameter("@AcceptedInsufficientNoticeFailure", objVendorScoreCardBE.AcceptedInsufficientNoticeFailure);
                param[index++] = new SqlParameter("@AcceptedRefusedToWaitFailure", objVendorScoreCardBE.AcceptedRefusedToWaitFailure);
                param[index++] = new SqlParameter("@AcceptedVolumeMismatchFailure", objVendorScoreCardBE.AcceptedVolumeMismatchFailure);
                param[index++] = new SqlParameter("@AcceptedIncorrecTimeFailure", objVendorScoreCardBE.AcceptedIncorrecTimeFailure);
                param[index++] = new SqlParameter("@AcceptedDamagePakagingFailure", objVendorScoreCardBE.AcceptedDamagePakagingFailure);
                param[index++] = new SqlParameter("@AcceptedUnsafeFailure", objVendorScoreCardBE.AcceptedUnsafeFailure);
                param[index++] = new SqlParameter("@AcceptedPaperworkFailure", objVendorScoreCardBE.AcceptedPaperworkFailure);
                param[index++] = new SqlParameter("@AcceptedUnscheduleFailure", objVendorScoreCardBE.AcceptedUnscheduleFailure);
                param[index++] = new SqlParameter("@AcceptedOtherFailure", objVendorScoreCardBE.AcceptedOtherFailure);

                param[index++] = new SqlParameter("@RefusedInsufficientNoticeWeight", objVendorScoreCardBE.RefusedInsufficientNoticeWeight);
                param[index++] = new SqlParameter("@RefusedRefusedToWaitWeight", objVendorScoreCardBE.RefusedRefusedToWaitWeight);
                param[index++] = new SqlParameter("@RefusedVolumeMismatchWeight", objVendorScoreCardBE.RefusedVolumeMismatchWeight);
                param[index++] = new SqlParameter("@RefusedIncorrecTimeWeight", objVendorScoreCardBE.RefusedIncorrecTimeWeight);
                param[index++] = new SqlParameter("@RefusedDamagePakagingWeight", objVendorScoreCardBE.RefusedDamagePakagingWeight);
                param[index++] = new SqlParameter("@RefusedUnsafeWeight", objVendorScoreCardBE.RefusedUnsafeWeight);
                param[index++] = new SqlParameter("@RefusedPaperworkWeight", objVendorScoreCardBE.RefusedPaperworkWeight);
                param[index++] = new SqlParameter("@RefusedUnscheduleWeight", objVendorScoreCardBE.RefusedUnscheduleWeight);
                param[index++] = new SqlParameter("@RefusedOtherWeight", objVendorScoreCardBE.RefusedOtherWeight);

                param[index++] = new SqlParameter("@RefusedInsufficientNoticeIdeal", objVendorScoreCardBE.RefusedInsufficientNoticeIdeal);
                param[index++] = new SqlParameter("@RefusedRefusedToWaitIdeal", objVendorScoreCardBE.RefusedRefusedToWaitIdeal);
                param[index++] = new SqlParameter("@RefusedVolumeMismatchIdeal", objVendorScoreCardBE.RefusedVolumeMismatchIdeal);
                param[index++] = new SqlParameter("@RefusedIncorrecTimeIdeal", objVendorScoreCardBE.RefusedIncorrecTimeIdeal);
                param[index++] = new SqlParameter("@RefusedDamagePakagingIdeal", objVendorScoreCardBE.RefusedDamagePakagingIdeal);
                param[index++] = new SqlParameter("@RefusedUnsafeIdeal", objVendorScoreCardBE.RefusedUnsafeIdeal);
                param[index++] = new SqlParameter("@RefusedPaperworkIdeal", objVendorScoreCardBE.RefusedPaperworkIdeal);
                param[index++] = new SqlParameter("@RefusedUnscheduleIdeal", objVendorScoreCardBE.RefusedUnscheduleIdeal);
                param[index++] = new SqlParameter("@RefusedOtherIdeal", objVendorScoreCardBE.RefusedOtherIdeal);

                param[index++] = new SqlParameter("@RefusedInsufficientNoticeTarget", objVendorScoreCardBE.RefusedInsufficientNoticeTarget);
                param[index++] = new SqlParameter("@RefusedRefusedToWaitTarget", objVendorScoreCardBE.RefusedRefusedToWaitTarget);
                param[index++] = new SqlParameter("@RefusedVolumeMismatchTarget", objVendorScoreCardBE.RefusedVolumeMismatchTarget);
                param[index++] = new SqlParameter("@RefusedIncorrecTimeTarget", objVendorScoreCardBE.RefusedIncorrecTimeTarget);
                param[index++] = new SqlParameter("@RefusedDamagePakagingTarget", objVendorScoreCardBE.RefusedDamagePakagingTarget);
                param[index++] = new SqlParameter("@RefusedUnsafeTarget", objVendorScoreCardBE.RefusedUnsafeTarget);
                param[index++] = new SqlParameter("@RefusedPaperworkTarget", objVendorScoreCardBE.RefusedPaperworkTarget);
                param[index++] = new SqlParameter("@RefusedUnscheduleTarget", objVendorScoreCardBE.RefusedUnscheduleTarget);
                param[index++] = new SqlParameter("@RefusedOtherTarget", objVendorScoreCardBE.RefusedOtherTarget);

                param[index++] = new SqlParameter("@RefusedInsufficientNoticeFailure", objVendorScoreCardBE.RefusedInsufficientNoticeFailure);
                param[index++] = new SqlParameter("@RefusedRefusedToWaitFailure", objVendorScoreCardBE.RefusedRefusedToWaitFailure);
                param[index++] = new SqlParameter("@RefusedVolumeMismatchFailure", objVendorScoreCardBE.RefusedVolumeMismatchFailure);
                param[index++] = new SqlParameter("@RefusedIncorrecTimeFailure", objVendorScoreCardBE.RefusedIncorrecTimeFailure);
                param[index++] = new SqlParameter("@RefusedDamagePakagingFailure", objVendorScoreCardBE.RefusedDamagePakagingFailure);
                param[index++] = new SqlParameter("@RefusedUnsafeFailure", objVendorScoreCardBE.RefusedUnsafeFailure);
                param[index++] = new SqlParameter("@RefusedPaperworkFailure", objVendorScoreCardBE.RefusedPaperworkFailure);
                param[index++] = new SqlParameter("@RefusedUnscheduleFailure", objVendorScoreCardBE.RefusedUnscheduleFailure);
                param[index++] = new SqlParameter("@RefusedOtherFailure", objVendorScoreCardBE.RefusedOtherFailure);

                //Stock Weight and Targets Property
                param[index++] = new SqlParameter("@AvgBackorderDaysWeight", objVendorScoreCardBE.AvgBackorderDaysWeight);
                param[index++] = new SqlParameter("@LeadTimeVarianceWeight", objVendorScoreCardBE.LeadTimeVarianceWeight);

                param[index++] = new SqlParameter("@AvgBackorderDaysIdeal", objVendorScoreCardBE.AvgBackorderDaysIdeal);
                param[index++] = new SqlParameter("@LeadTimeVarianceIdeal", objVendorScoreCardBE.LeadTimeVarianceIdeal);

                param[index++] = new SqlParameter("@AvgBackorderDaysTargetPositive", objVendorScoreCardBE.AvgBackorderDaysTargetPositive);
                param[index++] = new SqlParameter("@LeadTimeVarianceTargetPositive", objVendorScoreCardBE.LeadTimeVarianceTargetPositive);

                param[index++] = new SqlParameter("@AvgBackorderDaysTargetNegative", objVendorScoreCardBE.AvgBackorderDaysTargetNegative);
                param[index++] = new SqlParameter("@LeadTimeVarianceTargetNegative", objVendorScoreCardBE.LeadTimeVarianceTargetNegative);

                param[index++] = new SqlParameter("@AvgBackorderDaysFailurePositive", objVendorScoreCardBE.AvgBackorderDaysFailurePositive);
                param[index++] = new SqlParameter("@LeadTimeVarianceFailurePositive", objVendorScoreCardBE.LeadTimeVarianceFailurePositive);

                param[index++] = new SqlParameter("@AvgBackorderDaysFailureNegative", objVendorScoreCardBE.AvgBackorderDaysFailureNegative);
                param[index++] = new SqlParameter("@LeadTimeVarianceFailureNegative", objVendorScoreCardBE.LeadTimeVarianceFailureNegative);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorScoreCard", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? AddEditFrequencyDAL(MAS_VendorScoreCardBE objVendorScoreCardBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[index++] = new SqlParameter("@Action", objVendorScoreCardBE.Action);
                param[index++] = new SqlParameter("@CountryID", objVendorScoreCardBE.CountryID);
                param[index++] = new SqlParameter("@SelectedVendorIDs", objVendorScoreCardBE.SelectedVendorIDs);
                param[index++] = new SqlParameter("@Frequency", objVendorScoreCardBE.Frequency);

                param[index++] = new SqlParameter("@VendorCountryFreqID", objVendorScoreCardBE.VendorCountryFreqID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorScoreCard", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteVendorFreqDAL(MAS_VendorScoreCardBE objVendorScoreCardBE) {
            int? intResult = 0;
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", objVendorScoreCardBE.Action);
                param[index++] = new SqlParameter("@VendorCountryFreqID", objVendorScoreCardBE.VendorCountryFreqID);

                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spVendorScoreCard", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public MAS_VendorScoreCardBE GetVendorScoreCardDAL(string Action) {
            MAS_VendorScoreCardBE objVendorScoreCardBE = new MAS_VendorScoreCardBE();
            int index = 0;
            DataTable dtOverallWeight = new DataTable();
            DataTable dtOverallScore = new DataTable();
            DataTable dtOTIFWeight = new DataTable();
            DataTable dtDiscrepanciesWeight = new DataTable();
            DataTable dtStockWeight = new DataTable();
            DataTable dtSchedulingWeight = new DataTable();

            SqlParameter[] param = new SqlParameter[1];
            param[index++] = new SqlParameter("@Action", Action);
            DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorScoreCard", param);

            dtOverallWeight = ds.Tables[0];
            if (dtOverallWeight.Rows.Count > 0) {
                foreach (DataRow dr in dtOverallWeight.Rows) {
                    objVendorScoreCardBE.OtifWeight = dr["OtifWeight"] != DBNull.Value ? Convert.ToInt32(dr["OtifWeight"]) : (int?)null;
                    objVendorScoreCardBE.SchedulingWeight = dr["SchedulingWeight"] != DBNull.Value ? Convert.ToInt32(dr["SchedulingWeight"]) : (int?)null;
                    objVendorScoreCardBE.DiscrepenciesWeight = dr["DiscrepenciesWeight"] != DBNull.Value ? Convert.ToInt32(dr["DiscrepenciesWeight"]) : (int?)null;
                    objVendorScoreCardBE.StockWeight = dr["StockWeight"] != DBNull.Value ? Convert.ToInt32(dr["StockWeight"]) : (int?)null;
                }
            }
            dtOverallScore = ds.Tables[1];
            if (dtOverallScore.Rows.Count > 0) {
                foreach (DataRow dr in dtOverallScore.Rows) {
                    objVendorScoreCardBE.FailureScore = dr["FailureScore"] != DBNull.Value ? Convert.ToDecimal(dr["FailureScore"]) : (decimal?)null;
                    objVendorScoreCardBE.TargetScore = dr["TargetScore"] != DBNull.Value ? Convert.ToDecimal(dr["TargetScore"]) : (decimal?)null;
                    objVendorScoreCardBE.IdealScore = dr["IdealScore"] != DBNull.Value ? Convert.ToDecimal(dr["IdealScore"]) : (decimal?)null;
                }
            }
            dtOTIFWeight = ds.Tables[2];
            if (dtOTIFWeight.Rows.Count > 0) {
                foreach (DataRow dr in dtOTIFWeight.Rows) {
                    objVendorScoreCardBE.CatAWeight = dr["CatAWeight"] != DBNull.Value ? Convert.ToInt32(dr["CatAWeight"]) : (int?)null;
                    objVendorScoreCardBE.CatBWeight = dr["CatBWeight"] != DBNull.Value ? Convert.ToInt32(dr["CatBWeight"]) : (int?)null;
                    objVendorScoreCardBE.CatCWeight = dr["CatCWeight"] != DBNull.Value ? Convert.ToInt32(dr["CatCWeight"]) : (int?)null;
                    objVendorScoreCardBE.CatDWeight = dr["CatDWeight"] != DBNull.Value ? Convert.ToInt32(dr["CatDWeight"]) : (int?)null;
                    objVendorScoreCardBE.CatOWeight = dr["CatOWeight"] != DBNull.Value ? Convert.ToInt32(dr["CatOWeight"]) : (int?)null;
                    objVendorScoreCardBE.CatAIdeal = dr["CatAIdeal"] != DBNull.Value ? Convert.ToInt32(dr["CatAIdeal"]) : (int?)null;
                    objVendorScoreCardBE.CatBIdeal = dr["CatBIdeal"] != DBNull.Value ? Convert.ToInt32(dr["CatBIdeal"]) : (int?)null;
                    objVendorScoreCardBE.CatCIdeal = dr["CatCIdeal"] != DBNull.Value ? Convert.ToInt32(dr["CatCIdeal"]) : (int?)null;
                    objVendorScoreCardBE.CatDIdeal = dr["CatDIdeal"] != DBNull.Value ? Convert.ToInt32(dr["CatDIdeal"]) : (int?)null;
                    objVendorScoreCardBE.CatOIdeal = dr["CatOIdeal"] != DBNull.Value ? Convert.ToInt32(dr["CatOIdeal"]) : (int?)null;
                    objVendorScoreCardBE.CatATarget = dr["CatATarget"] != DBNull.Value ? Convert.ToInt32(dr["CatATarget"]) : (int?)null;
                    objVendorScoreCardBE.CatBTarget = dr["CatBTarget"] != DBNull.Value ? Convert.ToInt32(dr["CatBTarget"]) : (int?)null;
                    objVendorScoreCardBE.CatCTarget = dr["CatCTarget"] != DBNull.Value ? Convert.ToInt32(dr["CatCTarget"]) : (int?)null;
                    objVendorScoreCardBE.CatDTarget = dr["CatDTarget"] != DBNull.Value ? Convert.ToInt32(dr["CatDTarget"]) : (int?)null;
                    objVendorScoreCardBE.CatOTarget = dr["CatOTarget"] != DBNull.Value ? Convert.ToInt32(dr["CatOTarget"]) : (int?)null;
                    objVendorScoreCardBE.CatAFailure = dr["CatAFailure"] != DBNull.Value ? Convert.ToInt32(dr["CatAFailure"]) : (int?)null;
                    objVendorScoreCardBE.CatBFailure = dr["CatBFailure"] != DBNull.Value ? Convert.ToInt32(dr["CatBFailure"]) : (int?)null;
                    objVendorScoreCardBE.CatCFailure = dr["CatCFailure"] != DBNull.Value ? Convert.ToInt32(dr["CatCFailure"]) : (int?)null;
                    objVendorScoreCardBE.CatDFailure = dr["CatDFailure"] != DBNull.Value ? Convert.ToInt32(dr["CatDFailure"]) : (int?)null;
                    objVendorScoreCardBE.CatOFailure = dr["CatOFailure"] != DBNull.Value ? Convert.ToInt32(dr["CatOFailure"]) : (int?)null;
                }
            }
            dtDiscrepanciesWeight = ds.Tables[3];
            if (dtDiscrepanciesWeight.Rows.Count > 0) {
                foreach (DataRow dr in dtDiscrepanciesWeight.Rows) {
                    objVendorScoreCardBE.DiscWeight = dr["DiscWeight"] != DBNull.Value ? Convert.ToInt32(dr["DiscWeight"]) : (int?)null;
                    objVendorScoreCardBE.AvgWeight = dr["AvgWeight"] != DBNull.Value ? Convert.ToInt32(dr["AvgWeight"]) : (int?)null;
                    objVendorScoreCardBE.DiscIdeal = dr["DiscIdeal"] != DBNull.Value ? Convert.ToInt32(dr["DiscIdeal"]) : (int?)null;
                    objVendorScoreCardBE.AvgIdeal = dr["AvgIdeal"] != DBNull.Value ? Convert.ToInt32(dr["AvgIdeal"]) : (int?)null;
                    objVendorScoreCardBE.DiscTarget = dr["DiscTarget"] != DBNull.Value ? Convert.ToInt32(dr["DiscTarget"]) : (int?)null;
                    objVendorScoreCardBE.AvgTarget = dr["AvgTarget"] != DBNull.Value ? Convert.ToInt32(dr["AvgTarget"]) : (int?)null;
                    objVendorScoreCardBE.DiscFailure = dr["DiscFailure"] != DBNull.Value ? Convert.ToInt32(dr["DiscFailure"]) : (int?)null;
                    objVendorScoreCardBE.AvgFailure = dr["AvgFailure"] != DBNull.Value ? Convert.ToInt32(dr["AvgFailure"]) : (int?)null;
                }
            }
            dtStockWeight = ds.Tables[4];
            if (dtStockWeight.Rows.Count > 0) {
                foreach (DataRow dr in dtStockWeight.Rows) {
                    objVendorScoreCardBE.AvgBackorderDaysWeight = dr["AvgBackorderDaysWeight"] != DBNull.Value ? Convert.ToInt32(dr["AvgBackorderDaysWeight"]) : (int?)null;
                    objVendorScoreCardBE.LeadTimeVarianceWeight = dr["LeadTimeVarianceWeight"] != DBNull.Value ? Convert.ToInt32(dr["LeadTimeVarianceWeight"]) : (int?)null;
                    objVendorScoreCardBE.AvgBackorderDaysIdeal = dr["AvgBackorderDaysIdeal"] != DBNull.Value ? Convert.ToInt32(dr["AvgBackorderDaysIdeal"]) : (int?)null;
                    objVendorScoreCardBE.LeadTimeVarianceIdeal = dr["LeadTimeVarianceIdeal"] != DBNull.Value ? Convert.ToInt32(dr["LeadTimeVarianceIdeal"]) : (int?)null;
                    objVendorScoreCardBE.AvgBackorderDaysTargetPositive = dr["AvgBackorderDaysTargetPos"] != DBNull.Value ? Convert.ToInt32(dr["AvgBackorderDaysTargetPos"]) : (int?)null;
                    objVendorScoreCardBE.LeadTimeVarianceTargetPositive = dr["LeadTimeVarianceTargetPos"] != DBNull.Value ? Convert.ToInt32(dr["LeadTimeVarianceTargetPos"]) : (int?)null;
                    objVendorScoreCardBE.AvgBackorderDaysTargetNegative = dr["AvgBackorderDaysTargetNeg"] != DBNull.Value ? Convert.ToInt32(dr["AvgBackorderDaysTargetNeg"]) : (int?)null;
                    objVendorScoreCardBE.LeadTimeVarianceTargetNegative = dr["LeadTimeVarianceTargetNeg"] != DBNull.Value ? Convert.ToInt32(dr["LeadTimeVarianceTargetNeg"]) : (int?)null;
                    objVendorScoreCardBE.AvgBackorderDaysFailurePositive = dr["AvgBackorderDaysFailurePos"] != DBNull.Value ? Convert.ToInt32(dr["AvgBackorderDaysFailurePos"]) : (int?)null;
                    objVendorScoreCardBE.LeadTimeVarianceFailurePositive = dr["LeadTimeVarianceFailurePos"] != DBNull.Value ? Convert.ToInt32(dr["LeadTimeVarianceFailurePos"]) : (int?)null;
                    objVendorScoreCardBE.AvgBackorderDaysFailureNegative = dr["AvgBackorderDaysFailureNeg"] != DBNull.Value ? Convert.ToInt32(dr["AvgBackorderDaysFailureNeg"]) : (int?)null;
                    objVendorScoreCardBE.LeadTimeVarianceFailureNegative = dr["LeadTimeVarianceFailureNeg"] != DBNull.Value ? Convert.ToInt32(dr["LeadTimeVarianceFailureNeg"]) : (int?)null;
                }
            }
            dtSchedulingWeight = ds.Tables[5];
            if (dtSchedulingWeight.Rows.Count > 0) {
                objVendorScoreCardBE.Accepted = dtSchedulingWeight.Rows[0]["Accepted"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[0]["Accepted"]) : (int?)null;
                objVendorScoreCardBE.Refused = dtSchedulingWeight.Rows[0]["Refused"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[0]["Refused"]) : (int?)null;

                objVendorScoreCardBE.AcceptedInsufficientNoticeWeight = dtSchedulingWeight.Rows[0]["AcceptWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[0]["AcceptWeight"]) : (int?)null;
                objVendorScoreCardBE.AcceptedRefusedToWaitWeight = dtSchedulingWeight.Rows[1]["AcceptWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[1]["AcceptWeight"]) : (int?)null;
                objVendorScoreCardBE.AcceptedVolumeMismatchWeight = dtSchedulingWeight.Rows[2]["AcceptWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[2]["AcceptWeight"]) : (int?)null;
                objVendorScoreCardBE.AcceptedIncorrecTimeWeight = dtSchedulingWeight.Rows[3]["AcceptWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[3]["AcceptWeight"]) : (int?)null;
                objVendorScoreCardBE.AcceptedDamagePakagingWeight = dtSchedulingWeight.Rows[4]["AcceptWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[4]["AcceptWeight"]) : (int?)null;
                objVendorScoreCardBE.AcceptedUnsafeWeight = dtSchedulingWeight.Rows[5]["AcceptWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[5]["AcceptWeight"]) : (int?)null;
                objVendorScoreCardBE.AcceptedPaperworkWeight = dtSchedulingWeight.Rows[6]["AcceptWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[6]["AcceptWeight"]) : (int?)null;
                objVendorScoreCardBE.AcceptedUnscheduleWeight = dtSchedulingWeight.Rows[7]["AcceptWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[7]["AcceptWeight"]) : (int?)null;
                objVendorScoreCardBE.AcceptedOtherWeight = dtSchedulingWeight.Rows[8]["AcceptWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[8]["AcceptWeight"]) : (int?)null;

                objVendorScoreCardBE.AcceptedInsufficientNoticeIdeal = dtSchedulingWeight.Rows[0]["AcceptIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[0]["AcceptIdeal"]) : (int?)null;
                objVendorScoreCardBE.AcceptedRefusedToWaitIdeal = dtSchedulingWeight.Rows[1]["AcceptIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[1]["AcceptIdeal"]) : (int?)null;
                objVendorScoreCardBE.AcceptedVolumeMismatchIdeal = dtSchedulingWeight.Rows[2]["AcceptIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[2]["AcceptIdeal"]) : (int?)null;
                objVendorScoreCardBE.AcceptedIncorrecTimeIdeal = dtSchedulingWeight.Rows[3]["AcceptIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[3]["AcceptIdeal"]) : (int?)null;
                objVendorScoreCardBE.AcceptedDamagePakagingIdeal = dtSchedulingWeight.Rows[4]["AcceptIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[4]["AcceptIdeal"]) : (int?)null;
                objVendorScoreCardBE.AcceptedUnsafeIdeal = dtSchedulingWeight.Rows[5]["AcceptIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[5]["AcceptIdeal"]) : (int?)null;
                objVendorScoreCardBE.AcceptedPaperworkIdeal = dtSchedulingWeight.Rows[6]["AcceptIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[6]["AcceptIdeal"]) : (int?)null;
                objVendorScoreCardBE.AcceptedUnscheduleIdeal = dtSchedulingWeight.Rows[7]["AcceptIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[7]["AcceptIdeal"]) : (int?)null;
                objVendorScoreCardBE.AcceptedOtherIdeal = dtSchedulingWeight.Rows[8]["AcceptIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[8]["AcceptIdeal"]) : (int?)null;

                objVendorScoreCardBE.AcceptedInsufficientNoticeTarget = dtSchedulingWeight.Rows[0]["AcceptTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[0]["AcceptTarget"]) : (int?)null;
                objVendorScoreCardBE.AcceptedRefusedToWaitTarget = dtSchedulingWeight.Rows[1]["AcceptTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[1]["AcceptTarget"]) : (int?)null;
                objVendorScoreCardBE.AcceptedVolumeMismatchTarget = dtSchedulingWeight.Rows[2]["AcceptTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[2]["AcceptTarget"]) : (int?)null;
                objVendorScoreCardBE.AcceptedIncorrecTimeTarget = dtSchedulingWeight.Rows[3]["AcceptTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[3]["AcceptTarget"]) : (int?)null;
                objVendorScoreCardBE.AcceptedDamagePakagingTarget = dtSchedulingWeight.Rows[4]["AcceptTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[4]["AcceptTarget"]) : (int?)null;
                objVendorScoreCardBE.AcceptedUnsafeTarget = dtSchedulingWeight.Rows[5]["AcceptTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[5]["AcceptTarget"]) : (int?)null;
                objVendorScoreCardBE.AcceptedPaperworkTarget = dtSchedulingWeight.Rows[6]["AcceptTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[6]["AcceptTarget"]) : (int?)null;
                objVendorScoreCardBE.AcceptedUnscheduleTarget = dtSchedulingWeight.Rows[7]["AcceptTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[7]["AcceptTarget"]) : (int?)null;
                objVendorScoreCardBE.AcceptedOtherTarget = dtSchedulingWeight.Rows[8]["AcceptTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[8]["AcceptTarget"]) : (int?)null;

                objVendorScoreCardBE.AcceptedInsufficientNoticeFailure = dtSchedulingWeight.Rows[0]["AcceptFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[0]["AcceptFailure"]) : (int?)null;
                objVendorScoreCardBE.AcceptedRefusedToWaitFailure = dtSchedulingWeight.Rows[1]["AcceptFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[1]["AcceptFailure"]) : (int?)null;
                objVendorScoreCardBE.AcceptedVolumeMismatchFailure = dtSchedulingWeight.Rows[2]["AcceptFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[2]["AcceptFailure"]) : (int?)null;
                objVendorScoreCardBE.AcceptedIncorrecTimeFailure = dtSchedulingWeight.Rows[3]["AcceptFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[3]["AcceptFailure"]) : (int?)null;
                objVendorScoreCardBE.AcceptedDamagePakagingFailure = dtSchedulingWeight.Rows[4]["AcceptFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[4]["AcceptFailure"]) : (int?)null;
                objVendorScoreCardBE.AcceptedUnsafeFailure = dtSchedulingWeight.Rows[5]["AcceptFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[5]["AcceptFailure"]) : (int?)null;
                objVendorScoreCardBE.AcceptedPaperworkFailure = dtSchedulingWeight.Rows[6]["AcceptFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[6]["AcceptFailure"]) : (int?)null;
                objVendorScoreCardBE.AcceptedUnscheduleFailure = dtSchedulingWeight.Rows[7]["AcceptFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[7]["AcceptFailure"]) : (int?)null;
                objVendorScoreCardBE.AcceptedOtherFailure = dtSchedulingWeight.Rows[8]["AcceptFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[8]["AcceptFailure"]) : (int?)null;

                objVendorScoreCardBE.RefusedInsufficientNoticeWeight = dtSchedulingWeight.Rows[0]["RefuseWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[0]["RefuseWeight"]) : (int?)null;
                objVendorScoreCardBE.RefusedRefusedToWaitWeight = dtSchedulingWeight.Rows[1]["RefuseWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[1]["RefuseWeight"]) : (int?)null;
                objVendorScoreCardBE.RefusedVolumeMismatchWeight = dtSchedulingWeight.Rows[2]["RefuseWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[2]["RefuseWeight"]) : (int?)null;
                objVendorScoreCardBE.RefusedIncorrecTimeWeight = dtSchedulingWeight.Rows[3]["RefuseWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[3]["RefuseWeight"]) : (int?)null;
                objVendorScoreCardBE.RefusedDamagePakagingWeight = dtSchedulingWeight.Rows[4]["RefuseWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[4]["RefuseWeight"]) : (int?)null;
                objVendorScoreCardBE.RefusedUnsafeWeight = dtSchedulingWeight.Rows[5]["RefuseWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[5]["RefuseWeight"]) : (int?)null;
                objVendorScoreCardBE.RefusedPaperworkWeight = dtSchedulingWeight.Rows[6]["RefuseWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[6]["RefuseWeight"]) : (int?)null;
                objVendorScoreCardBE.RefusedUnscheduleWeight = dtSchedulingWeight.Rows[7]["RefuseWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[7]["RefuseWeight"]) : (int?)null;
                objVendorScoreCardBE.RefusedOtherWeight = dtSchedulingWeight.Rows[8]["RefuseWeight"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[8]["RefuseWeight"]) : (int?)null;

                objVendorScoreCardBE.RefusedInsufficientNoticeIdeal = dtSchedulingWeight.Rows[0]["RefuseIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[0]["RefuseIdeal"]) : (int?)null;
                objVendorScoreCardBE.RefusedRefusedToWaitIdeal = dtSchedulingWeight.Rows[1]["RefuseIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[1]["RefuseIdeal"]) : (int?)null;
                objVendorScoreCardBE.RefusedVolumeMismatchIdeal = dtSchedulingWeight.Rows[2]["RefuseIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[2]["RefuseIdeal"]) : (int?)null;
                objVendorScoreCardBE.RefusedIncorrecTimeIdeal = dtSchedulingWeight.Rows[3]["RefuseIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[3]["RefuseIdeal"]) : (int?)null;
                objVendorScoreCardBE.RefusedDamagePakagingIdeal = dtSchedulingWeight.Rows[4]["RefuseIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[4]["RefuseIdeal"]) : (int?)null;
                objVendorScoreCardBE.RefusedUnsafeIdeal = dtSchedulingWeight.Rows[5]["RefuseIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[5]["RefuseIdeal"]) : (int?)null;
                objVendorScoreCardBE.RefusedPaperworkIdeal = dtSchedulingWeight.Rows[6]["RefuseIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[6]["RefuseIdeal"]) : (int?)null;
                objVendorScoreCardBE.RefusedUnscheduleIdeal = dtSchedulingWeight.Rows[7]["RefuseIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[7]["RefuseIdeal"]) : (int?)null;
                objVendorScoreCardBE.RefusedOtherIdeal = dtSchedulingWeight.Rows[8]["RefuseIdeal"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[8]["RefuseIdeal"]) : (int?)null;

                objVendorScoreCardBE.RefusedInsufficientNoticeTarget = dtSchedulingWeight.Rows[0]["RefuseTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[0]["RefuseTarget"]) : (int?)null;
                objVendorScoreCardBE.RefusedRefusedToWaitTarget = dtSchedulingWeight.Rows[1]["RefuseTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[1]["RefuseTarget"]) : (int?)null;
                objVendorScoreCardBE.RefusedVolumeMismatchTarget = dtSchedulingWeight.Rows[2]["RefuseTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[2]["RefuseTarget"]) : (int?)null;
                objVendorScoreCardBE.RefusedIncorrecTimeTarget = dtSchedulingWeight.Rows[3]["RefuseTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[3]["RefuseTarget"]) : (int?)null;
                objVendorScoreCardBE.RefusedDamagePakagingTarget = dtSchedulingWeight.Rows[4]["RefuseTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[4]["RefuseTarget"]) : (int?)null;
                objVendorScoreCardBE.RefusedUnsafeTarget = dtSchedulingWeight.Rows[5]["RefuseTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[5]["RefuseTarget"]) : (int?)null;
                objVendorScoreCardBE.RefusedPaperworkTarget = dtSchedulingWeight.Rows[6]["RefuseTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[6]["RefuseTarget"]) : (int?)null;
                objVendorScoreCardBE.RefusedUnscheduleTarget = dtSchedulingWeight.Rows[7]["RefuseTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[7]["RefuseTarget"]) : (int?)null;
                objVendorScoreCardBE.RefusedOtherTarget = dtSchedulingWeight.Rows[8]["RefuseTarget"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[8]["RefuseTarget"]) : (int?)null;

                objVendorScoreCardBE.RefusedInsufficientNoticeFailure = dtSchedulingWeight.Rows[0]["RefuseFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[0]["RefuseFailure"]) : (int?)null;
                objVendorScoreCardBE.RefusedRefusedToWaitFailure = dtSchedulingWeight.Rows[1]["RefuseFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[1]["RefuseFailure"]) : (int?)null;
                objVendorScoreCardBE.RefusedVolumeMismatchFailure = dtSchedulingWeight.Rows[2]["RefuseFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[2]["RefuseFailure"]) : (int?)null;
                objVendorScoreCardBE.RefusedIncorrecTimeFailure = dtSchedulingWeight.Rows[3]["RefuseFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[3]["RefuseFailure"]) : (int?)null;
                objVendorScoreCardBE.RefusedDamagePakagingFailure = dtSchedulingWeight.Rows[4]["RefuseFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[4]["RefuseFailure"]) : (int?)null;
                objVendorScoreCardBE.RefusedUnsafeFailure = dtSchedulingWeight.Rows[5]["RefuseFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[5]["RefuseFailure"]) : (int?)null;
                objVendorScoreCardBE.RefusedPaperworkFailure = dtSchedulingWeight.Rows[6]["RefuseFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[6]["RefuseFailure"]) : (int?)null;
                objVendorScoreCardBE.RefusedUnscheduleFailure = dtSchedulingWeight.Rows[7]["RefuseFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[7]["RefuseFailure"]) : (int?)null;
                objVendorScoreCardBE.RefusedOtherFailure = dtSchedulingWeight.Rows[8]["RefuseFailure"] != DBNull.Value ? Convert.ToInt32(dtSchedulingWeight.Rows[8]["RefuseFailure"]) : (int?)null;
            }
            return objVendorScoreCardBE;
        }

        public MAS_VendorScoreCardBE GetVendorFreqDAL(MAS_VendorScoreCardBE objVendorScoreCardBE) {
            MAS_VendorScoreCardBE objRetVendorScoreCardBE = new MAS_VendorScoreCardBE();
            int index = 0;
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[2];
            param[index++] = new SqlParameter("@Action", objVendorScoreCardBE.Action);
            param[index++] = new SqlParameter("@VendorCountryFreqID", objVendorScoreCardBE.VendorCountryFreqID);

            DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorScoreCard", param);

            dt = ds.Tables[0];
            if (dt.Rows.Count > 0) {
                foreach (DataRow dr in dt.Rows) {
                    objRetVendorScoreCardBE.VendorName = dr["VendorName"] != DBNull.Value ? (dr["VendorName"]).ToString() : "";
                    objRetVendorScoreCardBE.CountryName = dr["CountryName"] != DBNull.Value ? (dr["CountryName"]).ToString() : "";
                    objRetVendorScoreCardBE.Frequency = dr["Frequency"] != DBNull.Value ? Convert.ToChar(dr["Frequency"]) : 'N';
                }
            }

            return objRetVendorScoreCardBE;
        }

        public List<MAS_VendorScoreCardBE> GetVendorByCountryDAL(MAS_VendorScoreCardBE objVendorScoreCardBE) {
            DataTable dt = new DataTable();
            List<MAS_VendorScoreCardBE> objListVendorScoreCardBE = new List<MAS_VendorScoreCardBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", objVendorScoreCardBE.Action);
                param[index++] = new SqlParameter("@CountryID", objVendorScoreCardBE.CountryID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorScoreCard", param);

                if (ds.Tables.Count > 0) {
                    dt = ds.Tables[0];

                    foreach (DataRow dr in dt.Rows) {
                        MAS_VendorScoreCardBE oNew_VendorScoreCardBE = new MAS_VendorScoreCardBE();
                        oNew_VendorScoreCardBE.VendorID = dr["VendorID"] != DBNull.Value ? Convert.ToInt32(dr["VendorID"]) : (int?)null;
                        oNew_VendorScoreCardBE.VendorName = dr["VendorName"] != DBNull.Value ? (dr["VendorName"]).ToString() : "";
                        oNew_VendorScoreCardBE.IsActiveVendor = dr["IsActiveVendor"] == DBNull.Value ? null : Convert.ToString(dr["IsActiveVendor"]); /// Added on 4 march 2013
                        
                        objListVendorScoreCardBE.Add(oNew_VendorScoreCardBE);
                    }
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return objListVendorScoreCardBE;
        }

        public List<MAS_VendorScoreCardBE> GetALLVendorFreqDAL(MAS_VendorScoreCardBE objVendorScoreCardBE) {
            DataTable dt = new DataTable();
            List<MAS_VendorScoreCardBE> objListVendorFreqBE = new List<MAS_VendorScoreCardBE>();
            try {
                int index = 0;
                SqlParameter[] param = new SqlParameter[2];
                param[index++] = new SqlParameter("@Action", objVendorScoreCardBE.Action);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spVendorScoreCard", param);

                if (ds.Tables.Count > 0) {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows) {
                        MAS_VendorScoreCardBE oNew_VendorScoreCardBE = new MAS_VendorScoreCardBE();
                        oNew_VendorScoreCardBE.VendorCountryFreqID = dr["VendorCountryFreqID"] != DBNull.Value ? Convert.ToInt32(dr["VendorCountryFreqID"]) : (int?)null;
                        oNew_VendorScoreCardBE.VendorName = dr["VendorName"] != DBNull.Value ? (dr["VendorName"]).ToString() : "";
                        oNew_VendorScoreCardBE.CountryName = dr["CountryName"] != DBNull.Value ? (dr["CountryName"]).ToString() : "";
                        oNew_VendorScoreCardBE.gridFrequency = dr["gridFrequency"] != DBNull.Value ? (dr["gridFrequency"]).ToString() : "";
                        objListVendorFreqBE.Add(oNew_VendorScoreCardBE);
                    }
                }
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return objListVendorFreqBE;
        }

        //public void ExcecuteScorecardDAL() {
        //    try {
        //        int index = 0;

        //        SqlParameter[] param = new SqlParameter[1];
        //        param[index] = new SqlParameter("@Date", DateTime.Today.AddMonths(-1));
        //        SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spScoreCardSchedular", param);

        //        param[index] = new SqlParameter("@Date", DateTime.Today.AddMonths(-2));
        //        SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spScoreCardSchedular", param);

        //    }
        //    catch (Exception ex) {
        //        LogUtility.SaveErrorLogEntry(ex);
        //    }
        //    finally { }


        //}

        public void ExcecuteScorecardDAL(int Month)
        {
            try
            {
                int index = 0;

                SqlParameter[] param = new SqlParameter[1];
                param[index] = new SqlParameter("@Date", DateTime.Today.AddMonths(-Month));
                SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spScoreCardSchedular", param);

                param[index] = new SqlParameter("@Date", DateTime.Today.AddMonths(-Month - 1));
                SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spScoreCardSchedular", param);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.VendorScorecard;
using Utilities;
using System.Data.SqlClient;
using System.Data;

namespace DataAccessLayer.ModuleDAL.VendorScorecard
{
    public class SummaryScoreCardDAL : BaseDAL
    {
        public DataSet GetSummaryScoreCardDAL(SummaryScoreCardBE objSummaryScoreCardBE)
        {
            var ds = new DataSet();
            var dt = new DataTable();
            var objListVendorFreqBE = new List<SummaryScoreCardBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[6];
                param[index++] = new SqlParameter("@Action", objSummaryScoreCardBE.Action);
                param[index++] = new SqlParameter("@EuropeanorLocal", objSummaryScoreCardBE.EuropeanorLocal);
                param[index++] = new SqlParameter("@Year", objSummaryScoreCardBE.Year);
                param[index++] = new SqlParameter("@VendorID", objSummaryScoreCardBE.VendorID);
                param[index++] = new SqlParameter("@ModuleType", objSummaryScoreCardBE.ModuleType);
                ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spRPT_SummaryScoreCard", param);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return ds;
        }

        public int? GetCurrentYearDAL(SummaryScoreCardBE summaryScoreCardBE)
        {
            int? intResult = 0;
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[1];
                param[index++] = new SqlParameter("@Action", summaryScoreCardBE.Action);
                DataSet Result = SqlHelper.ExecuteDataset(DBConnection.Connection, CommandType.StoredProcedure, "spRPT_ReportRequest", param);

                if (Result != null && Result.Tables.Count > 0 && Result.Tables[0].Rows.Count > 0)
                    intResult = Convert.ToInt32(Result.Tables[0].Rows[0][0]);
                else
                    intResult = 0;

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

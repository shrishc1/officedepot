﻿using System;
using System.Collections.Generic;
using System.Data;

using BusinessEntities.ModuleBE.Security;
using Utilities;
using System.Data.SqlClient;

namespace DataAccessLayer.ModuleDAL.Security
{
    public class SCT_UserScreenDAL
    {

        public List<SCT_UserScreenBE> GetUserScreenDAL(SCT_UserScreenBE oSCT_UserScreenBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserScreenBE> SCT_UserScreenBEList = new List<SCT_UserScreenBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oSCT_UserScreenBE.Action);
                param[index++] = new SqlParameter("@ModuleID", oSCT_UserScreenBE.Screen.ModuleID);
                param[index++] = new SqlParameter("@UserID", oSCT_UserScreenBE.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_UserScreen", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserScreenBE oNewSCT_UserScreenBE = new SCT_UserScreenBE();

                    oNewSCT_UserScreenBE.Screen = new SCT_ScreenBE();
                    oNewSCT_UserScreenBE.Screen.ScreenName = dr["ScreenName"].ToString();
                    oNewSCT_UserScreenBE.ScreenID = Convert.ToInt32(dr["ScreenID"]);
                    oNewSCT_UserScreenBE.Screen.ScreenUrl = dr["ScreenUrl"].ToString();
                    oNewSCT_UserScreenBE.Screen.FunctionType = Convert.ToChar(dr["FunctionType"].ToString());

                    SCT_UserScreenBEList.Add(oNewSCT_UserScreenBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserScreenBEList;
        }

        public List<SCT_UserScreenBE> GetModuleScreenDAL(SCT_UserScreenBE oSCT_UserScreenBE)
        {
            DataTable dt = new DataTable();
            List<SCT_UserScreenBE> SCT_UserScreenBEList = new List<SCT_UserScreenBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oSCT_UserScreenBE.Action);
                param[index++] = new SqlParameter("@ModuleID", oSCT_UserScreenBE.Screen.ModuleID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_UserScreen", param);

                dt = ds.Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    SCT_UserScreenBE oNewSCT_UserScreenBE = new SCT_UserScreenBE();

                    oNewSCT_UserScreenBE.Screen = new SCT_ScreenBE();
                    oNewSCT_UserScreenBE.Module = new SCT_ModuleBE();
                    oNewSCT_UserScreenBE.Screen.ScreenName = dr["ScreenName"].ToString();
                    oNewSCT_UserScreenBE.ScreenID = Convert.ToInt32(dr["ScreenID"]);
                    oNewSCT_UserScreenBE.Screen.ScreenUrl = dr["ScreenUrl"].ToString();
                    oNewSCT_UserScreenBE.Screen.FunctionType = Convert.ToChar(dr["FunctionType"].ToString());
                    if (dr.Table.Columns.Contains("ParentModuleID"))
                    {
                        oNewSCT_UserScreenBE.Module.ParentModuleID = (dr["ParentModuleID"] != DBNull.Value ? Convert.ToInt32(dr["ParentModuleID"]) : 0);
                    }
                    SCT_UserScreenBEList.Add(oNewSCT_UserScreenBE);
                }
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return SCT_UserScreenBEList;
        }

        public int AuthenticateUserSubScreen(SCT_UserScreenBE oSCT_UserScreenBE)
        {
            int returnValue = 0;
            DataTable dt = new DataTable();
            List<SCT_UserScreenBE> SCT_UserScreenBEList = new List<SCT_UserScreenBE>();
            try
            {
                int index = 0;
                SqlParameter[] param = new SqlParameter[4];
                param[index++] = new SqlParameter("@Action", oSCT_UserScreenBE.Action);
                param[index++] = new SqlParameter("@ScreenUrl", oSCT_UserScreenBE.Screen.ScreenUrl);
                param[index++] = new SqlParameter("@UserID", oSCT_UserScreenBE.UserID);

                DataSet ds = SqlHelper.ExecuteDataset(DBConnection.Connection.ToString(), CommandType.StoredProcedure, "spSCT_UserScreen", param);
                returnValue = ds.Tables[0].Rows.Count;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            return returnValue;
        }

    }
}

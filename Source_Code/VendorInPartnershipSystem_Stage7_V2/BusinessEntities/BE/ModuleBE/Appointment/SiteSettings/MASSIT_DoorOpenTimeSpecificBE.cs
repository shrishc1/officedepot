﻿using System;
namespace BusinessEntities.ModuleBE.Appointment.SiteSettings {
    [Serializable]
    public class MASSIT_DoorOpenTimeSpecificBE : BaseBe {

        public int? DoorConstraintSpecificID { get; set; }

        public System.DateTime? WeekStartDate { get; set; }

        public MASSIT_DoorOpenTimeBE DoorOpenTime { get; set; }        
    }
}

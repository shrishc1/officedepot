﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.Appointment.SiteSettings {
    [Serializable]
    public class MASSIT_DeliveryConstraintBE : BaseBe{

        public int? DeliveryConstraintID { get; set; }

        public string Weekday { get; set; }

        public int SiteID { get; set; }

        public int StartTime { get; set; }

        public string StartWeekday { get; set; }

        public int? RestrictDelivery { get; set; }

        public int? MaximumLine { get; set; }

        public int? MaximumLift { get; set; }

        public int? SingleDeliveryPalletRestriction { get; set; }

        public int? SingleDeliveryLineRestriction { get; set; }

        
    }
}

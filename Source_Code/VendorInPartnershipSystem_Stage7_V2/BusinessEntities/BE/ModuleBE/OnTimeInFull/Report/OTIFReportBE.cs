﻿using System;

namespace BusinessEntities.ModuleBE.OnTimeInFull.Report
{
    [Serializable]
    public class OTIFReportBE : BaseBe
    {
        public string SelectedCountryIDs { get; set; }

        public string SelectedSiteIDs { get; set; }

        public string SelectedVendorIDs { get; set; }

        public string SelectedStockPlannerIDs { get; set; }

        public string SelectedAPIDs { get; set; }

        public string SelectedSKUCodes { get; set; }

        public string SelectedPurchaseOrderNo { get; set; }

        public string SelectedItemClassification { get; set; }

        public string VendorNumber { get; set; }

        public string VendorName { get; set; }

        public DateTime? DateTo { get; set; }

        public DateTime? DateFrom { get; set; }

       

        public int? PurchaseOrderDue { get; set; }

        public int? PurchaseOrderReceived  { get; set; }

        public int UserId { get; set; }

        public string ReportMonth { get; set; }

        public int NumberOfTimeAccessed { get; set; }

        //VendorReportingExclusion
        public int? ExclusionID { get; set; }

        public string ExclusionNarrative { get; set; }

        public string AppliedBy { get; set; }

        public DateTime? ApplicableDate { get; set; }

        public string SelectedExcludedVendorId { get; set; }

        public string SelectedExcludedSiteId { get; set; }

        public string SelectedExcludedSKUsName { get; set; }

        public string SelectedExcludedDeliveryTypeId { get; set; }

        public string SelectedExcludedBookingNo { get; set; }

        public string SelectedExcludedDiscrepancyNo { get; set; }

        public string SelectedExcludedPurchaseNo { get; set; }

        public DateTime? ExcludedDateFrom { get; set; }

        public DateTime? ExcludedDateTo { get; set; }

        public string SiteName { get; set; }

        public string CountryDeliveryType { get; set; }

        public string SitePurchaseOrderRaisedName { get; set; }
        public string SitePurchaseOrderRaisedIds { get; set; }


        public BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE Vendor { get; set; }
        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
        public string SelectedProductCodes { get; set; }

        public int? EuropeanorLocal { get; set; }

        public string SelectedRMSCategoryIDs { get; set; }

        public string SelectedSKuGroupingIDs { get; set; }

        public string  StockPlannerGroupingIDs { get; set; }

        public string CustomerIDs { get; set; }
    }
}

﻿
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessEntities.ModuleBE.Security;
using System;
namespace BusinessEntities.ModuleBE.OnTimeInFull.SiteSetting {
    [Serializable]
    public class SiteOTIF_LeadTimeRuleBE : BaseBe{

        public int SiteID { get; set; }
        public MAS_SiteBE Site { get; set; }
        public SCT_UserBE User { get; set; }
    }
}

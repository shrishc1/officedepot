﻿using System;

namespace BusinessEntities.ModuleBE.OnTimeInFull
{
    [Serializable]
    public class ApplyOTIFExclusionBE : BaseBe
    {
        public int? PurchaseOrderID { get; set; }
        public string Purchase_order { get; set; }
        public string ProductDescription { get; set; }
        public int? SKUID { get; set; }
        public int SiteID { get; set; }
        public string UserName { get; set; }
        public string VendorName { get; set; }
        public DateTime? DateApplied { get; set; }
        public string SelectedProductCodes { get; set; }
        public string Reason { get; set; }
        public string Vendor_No { get; set; }
        public int VendorID { get; set; }
        public DateTime? Order_raised { get; set; }
        public string SelectedVendorIDs { get; set; }
        public string CountryName { get; set; }
        public string SiteName { get; set; }
        public BusinessEntities.ModuleBE.Upload.UP_SKUBE SKU { get; set; }
        public int? ApplyExclusionID { get; set; }
    }
}

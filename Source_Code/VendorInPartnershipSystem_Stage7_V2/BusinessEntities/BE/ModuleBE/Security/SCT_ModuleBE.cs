﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.Security {
    [Serializable]
    public class SCT_ModuleBE : BaseBe {

        public int ModuleID { get; set; }

        public string ModuleName { get; set; }

        public int ParentModuleID { get; set; } 
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.Languages
{
    //[Serializable]
    public class MAS_LanguageTagsBE : BaseBe
    {
       public int LanguageTagsId { get; set; }
       public string LanguageKey { get; set; }
       public string English { get; set; }
       public string French { get; set; }
       public string German { get; set; }
       public string Dutch { get; set; }
       public string Spanish { get; set; }
       public string Italian { get; set; }
       public string Czech { get; set; }
       public string LanguageName { get; set; }
       public string LanguageText { get; set; }
    }
}

﻿
using System;
namespace BusinessEntities.ModuleBE.AdminFunctions
{

    [Serializable]
    public class MAS_SiteBE : BaseBe
    {
        public int SiteID { get; set; }

        public string SiteName { get; set; }

        public string SitePrefix { get; set; }

        public string SiteNumber { get; set; }

        public string SiteAddressLine1 { get; set; }

        public string SiteAddressLine2 { get; set; }

        public string SiteAddressLine3 { get; set; }

        public string SiteAddressLine4 { get; set; }

        public string SiteAddressLine5 { get; set; }

        public string SiteAddressLine6 { get; set; }

        public int? SiteCountryID { get; set; }

        public string SiteCountryName { get; set; }

        public string SitePincode { get; set; }

        public string APAddressLine1 { get; set; }

        public string APAddressLine2 { get; set; }

        public string APAddressLine3 { get; set; }

        public string APAddressLine4 { get; set; }

        public string APAddressLine5 { get; set; }

        public string APAddressLine6 { get; set; }

        public int? APCountryID { get; set; }

        public string APPincode { get; set; }

        public string PhoneNumbers { get; set; }

        public string FaxNumber { get; set; }

        public string EmailID { get; set; }

        public int? SiteMangerUserID { get; set; }

        public string CommunicationWithEscalationType { get; set; }

        public int? DaysAfterGoodsReturned { get; set; }

        public int? NonTimeDeliveryPalletVolume { get; set; }

        public int? NonTimeDeliveryCartonVolume { get; set; }

        public bool? IsPreAdviseNoteRequired { get; set; }

        public int? ToleranceDueDay { get; set; }

        public decimal? LinePerFTE { get; set; }

        public int? AveragePalletUnloadedPerManHour { get; set; }

        public int? AverageCartonUnloadedPerManHour { get; set; }

        public int? AverageLinesReceiving { get; set; }

        public int? BookingNoticePeriodInDays { get; set; }

        public DateTime? BookingNoticeTime { get; set; }

        public string SiteDescription { get; set; }

        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }

        public BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE Country { get; set; }

        public string DIS_CommunicationWithEscalationType { get; set; }

        public int? DIS_DaysBeforeEscalationType2 { get; set; }

        public int? DIS_DaysBeforeFinalEscalation { get; set; }

        public int? DIS_DaysAfterGoodsReturned { get; set; }

        public bool? IsActive { get; set; }

        public string Remarks { get; set; }

        public string SiteManagerName { get; set; }

        public string OTI_LeadTime { get; set; }

        public string SiteNumberDescription { get; set; }

        public string SiteNumberList { get; set; }
        public bool SchedulingContact { get; set; }
        public bool DiscrepancyContact { get; set; }

        public string ByDatePO { get; set; }
        public string PerBookingVendor { get; set; }
        public int? DeliveryTypeId { get; set; }
        public string DeliveryType { get; set; }
        public int? VehicleTypeId { get; set; }
        public string VehicleType { get; set; }
        public string AlternateSlotOnOff { get; set; }
        public string DeliveryArrived { get; set; }
        public string DeliveryUnload { get; set; }
        public string UnexpectedDelivery { get; set; }
        public string TimeSlotWindow { get; set; }

        //----Stage 4 Point 13----//
        public int? MaxPalletsPerDelivery { get; set; }
        public int? MaxCartonsPerDelivery { get; set; }
        public int? MaxLinesPerDelivery { get; set; }
        //------------------------//

        //---Stage 5 Point 1-----//
        public bool IsBookingExclusions { get; set; }
        //-----------------------//

        public string APCountry { get; set; }


        //---Stage 7 Point 29-----//
        public string NonTimeStart { get; set; }
        public string NonTimeEnd { get; set; }
        //-----------------------//
    }
}

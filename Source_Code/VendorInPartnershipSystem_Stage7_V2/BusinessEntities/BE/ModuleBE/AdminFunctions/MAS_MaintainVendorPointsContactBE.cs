﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.AdminFunctions
{
    [Serializable]
    public class MAS_MaintainVendorPointsContactBE : BaseBe
    {
        public int? VendorPointID { get; set; }
        public DateTime? VendorPointDate { get; set; }
        public BusinessEntities.ModuleBE.Security.SCT_UserBE User { get; set; }
        public BusinessEntities.ModuleBE.AdminFunctions.MAS_CountryBE Country { get; set; }
        public BusinessEntities.ModuleBE.AdminFunctions.MAS_VendorBE Vendor { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string RoleTitle { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string InventoryPOC { get; set; }
        public string ProcurementPOC { get; set; }
        public string MerchandisingPOC { get; set; }
        public string ExecutivePOC { get; set; }
        public int? IsActive { get; set; }
        public DateTime? VendorPointUpdateDate { get; set; }
        public DateTime? VendorPointDeleteDate { get; set; }
    }
}

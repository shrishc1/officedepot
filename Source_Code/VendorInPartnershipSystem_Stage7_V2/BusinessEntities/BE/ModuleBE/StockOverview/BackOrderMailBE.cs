﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.StockOverview
{
    [Serializable]
    public class BackOrderMailBE : BaseBe
    {

        public int PenaltyCommunicationId { get; set; }
        public int BOReportingID { get; set; }
        public string CommunicationType { get; set; }
        public string Subject { get; set; }
        public string SentTo { get; set; }
        public DateTime SentDate { get; set; }
        public string Body { get; set; }
        public int SendByID { get; set; }
        public int? LanguageId { get; set; }
        public bool MailSentInLanguage { get; set; }
        public string CommunicationStatus { get; set; }
        public string SentToWithLink { get; set; }
        public int VendorId { get; set; }
        public string SelectedBOIds { get; set; }
        public string VendorEmailIds { get; set; }
        public string errorDescription { get; set; }

    }
}

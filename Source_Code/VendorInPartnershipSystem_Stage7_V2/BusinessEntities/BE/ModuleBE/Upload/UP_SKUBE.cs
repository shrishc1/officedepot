﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.ModuleBE.Upload {
     [Serializable]
    public class UP_SKUBE:BaseBe {
        public int SKUID { get; set; }
        public string Direct_SKU { get; set; }
        public string OD_SKU_NO { get; set; }
        public string DESCRIPTION { get; set; }
        public string Vendor_Code { get; set; }
        public string Warehouse { get; set; }
        public string Date { get; set; }
        public string Qty_Sold_Yesterday { get; set; }
        public string Qty_On_Hand { get; set; }
        public string qty_on_backorder { get; set; }
        public int? Balance { get; set; }
        public string Item_Val { get; set; }
        public string Valuated_Stock { get; set; }
        public string Currency { get; set; }
        public string Vendor_no { get; set; }
        public string Vendor_Name { get; set; }
        public string subvndr { get; set; }
        public string Product_Lead_time { get; set; }
        public string UOM { get; set; }
        public string Buyer_no { get; set; }
        public string Item_Category { get; set; }
        public string Item_classification { get; set; }
        public string New_Item { get; set; }
        public string ICASUN { get; set; }
        public string ILAYUN { get; set; }
        public string IPALUN { get; set; }
        public string IMINQT { get; set; }
        public string IBMULT { get; set; }
        public string Category { get; set; }
        public string Leadtime_Variance { get; set; }
        public string BackOrder { get; set; }
        public string Country { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}

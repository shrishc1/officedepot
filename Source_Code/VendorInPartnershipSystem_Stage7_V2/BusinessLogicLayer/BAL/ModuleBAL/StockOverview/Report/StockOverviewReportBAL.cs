﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.StockOverview.Report;
using DataAccessLayer.ModuleDAL.StockOverview.Report;
using System.Data;
using Utilities;

namespace BusinessLogicLayer.ModuleBAL.StockOverview.Report
{
    public class StockOverviewReportBAL
    {

        StockOverviewReportBE oStockOverviewReportBE = new StockOverviewReportBE();

        public DataSet getDiscrepancyReportBAL(StockOverviewReportBE oStockOverviewReportBE) {
            DataSet Result = null;
            try {
                StockOverviewReportDAL oStockOverviewReportDAL = new StockOverviewReportDAL();
                Result = oStockOverviewReportDAL.getStockOverviewReportDAL(oStockOverviewReportBE);
                oStockOverviewReportDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;

        }

        public DataSet getItemSummaryReportBAL(StockOverviewReportBE oStockOverviewReportBE)
        {
            DataSet Result = null;
            try
            {
                StockOverviewReportDAL oStockOverviewReportDAL = new StockOverviewReportDAL();
                Result = oStockOverviewReportDAL.getItemSummaryReportDAL(oStockOverviewReportBE);
                oStockOverviewReportDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.Languages;
using Utilities;
using DataAccessLayer.ModuleDAL.Languages;

namespace BusinessLogicLayer.ModuleBAL.Languages
{
    public class MAS_LanguageTagsBAL
    {

        public List<MAS_LanguageTagsBE> GetAllLanguageTagsBAL(MAS_LanguageTagsBE oMAS_LanguageTagsBE)
        {
            List<MAS_LanguageTagsBE> oMAS_LanguageTagsBEList = new List<MAS_LanguageTagsBE>();

            try
            {
                MAS_LanguageTagsDAL oMAS_LanguageTagsDAL = new MAS_LanguageTagsDAL();
                oMAS_LanguageTagsBEList = oMAS_LanguageTagsDAL.GetAllLanguageTagsDAL(oMAS_LanguageTagsBE);
                oMAS_LanguageTagsDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_LanguageTagsBEList;
        }

        public int? EditLanguageTagBAL(MAS_LanguageTagsBE oMAS_LanguageTagsBE)
        {
            int? intResult = 0;
            try
            {
                MAS_LanguageTagsDAL oMAS_LanguageTagsDAL = new MAS_LanguageTagsDAL();
                intResult = oMAS_LanguageTagsDAL.EditLanguageTagDAL(oMAS_LanguageTagsBE);
                oMAS_LanguageTagsDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

    }
}

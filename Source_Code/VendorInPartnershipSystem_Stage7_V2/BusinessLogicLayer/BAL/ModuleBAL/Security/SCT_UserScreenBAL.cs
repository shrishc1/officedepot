﻿using System;
using System.Collections.Generic;
using System.Data;

using Utilities;
using BusinessEntities.ModuleBE.Security;
using DataAccessLayer.ModuleDAL.Security;

namespace BusinessLogicLayer.ModuleBAL.Security
{
    public class SCT_UserScreenBAL
    {
        public List<SCT_UserScreenBE> GetUserScreenBAL(SCT_UserScreenBE oSCT_UserScreenBE)
        {
            List<SCT_UserScreenBE> oSCT_UserScreenBEList = new List<SCT_UserScreenBE>();
            try
            {
                SCT_UserScreenDAL oSCT_UserScreenDAL = new SCT_UserScreenDAL();
                oSCT_UserScreenBEList = oSCT_UserScreenDAL.GetUserScreenDAL(oSCT_UserScreenBE);
                oSCT_UserScreenDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_UserScreenBEList;
        }

        public List<SCT_UserScreenBE> GetModuleScreenBAL(SCT_UserScreenBE oSCT_UserScreenBE)
        {
            List<SCT_UserScreenBE> oSCT_UserScreenBEList = new List<SCT_UserScreenBE>();
            try
            {
                SCT_UserScreenDAL oSCT_UserScreenDAL = new SCT_UserScreenDAL();
                oSCT_UserScreenBEList = oSCT_UserScreenDAL.GetModuleScreenDAL(oSCT_UserScreenBE);
                oSCT_UserScreenDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oSCT_UserScreenBEList;
        }

        public int AuthenticateSubScreen(SCT_UserScreenBE oSCT_UserScreenBE)
        {
            int returnValue = 0;
            try
            {
                SCT_UserScreenDAL oSCT_UserScreenDAL = new SCT_UserScreenDAL();
                returnValue = oSCT_UserScreenDAL.AuthenticateUserSubScreen(oSCT_UserScreenBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return returnValue;
        }
    }
}

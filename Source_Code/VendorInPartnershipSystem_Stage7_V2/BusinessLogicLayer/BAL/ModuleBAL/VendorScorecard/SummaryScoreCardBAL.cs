﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.VendorScorecard;
using DataAccessLayer.ModuleDAL.VendorScorecard;
using Utilities;
using System.Data;

namespace BusinessLogicLayer.ModuleBAL.VendorScorecard
{
    public class SummaryScoreCardBAL : BaseBAL
    {
        public DataSet GetSummaryScoreCardBAL(SummaryScoreCardBE objSummaryScoreCardBE)
        {
            DataSet Result = null;
            try
            {
                var summaryScoreCardDAL = new SummaryScoreCardDAL();
                Result = summaryScoreCardDAL.GetSummaryScoreCardDAL(objSummaryScoreCardBE);
                summaryScoreCardDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public int? GetCurrentYearBAL(SummaryScoreCardBE summaryScoreCardBE)
        {
            int? intResult = 0;
            try
            {
                var summaryScoreCardDAL = new SummaryScoreCardDAL();
                intResult = summaryScoreCardDAL.GetCurrentYearDAL(summaryScoreCardBE);
                summaryScoreCardDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

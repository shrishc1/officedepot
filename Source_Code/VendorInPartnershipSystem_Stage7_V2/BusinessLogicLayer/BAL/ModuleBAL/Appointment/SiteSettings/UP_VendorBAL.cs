﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;
using Utilities;
using BusinessEntities.ModuleBE.Security;
using System.Data;
using BusinessEntities.ModuleBE.Upload;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings {
    public class UP_VendorBAL {
        public List<UP_VendorBE> GetVendorsBAL(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();

            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();

                oUP_VendorBEList = oUP_VendorDAL.GetVendorsDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> GetMasterVendorsBAL(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();

            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();

                oUP_VendorBEList = oUP_VendorDAL.GetMasterVendorsDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> GetCarrierDetailsBAL(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();

            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();

                oUP_VendorBEList = oUP_VendorDAL.GetCarrierDetailsDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> ValidateVendorBAL(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                oUP_VendorBEList = oUP_VendorDAL.ValidateVendorDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_VendorBEList;
        }

        /// <summary>
        /// Used for managing consolidating vendors
        /// </summary>
        /// <param name="oUP_VendorBE"></param>
        /// <returns></returns>
        public int? addEditConsolidateVendorBAL(UP_VendorBE oUP_VendorBE) {
            int? intResult = 0;
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                intResult = oUP_VendorDAL.addEditConsolidateVendorDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteConsolidateVendorBAL(UP_VendorBE oUP_VendorBE) {
            int? intResult = 0;
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                intResult = oUP_VendorDAL.DeleteConsolidateVendorDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        /// <summary>
        /// Used for listing all vendors
        /// </summary>
        /// <param name="oUP_VendorBE"></param>
        /// <returns></returns>
        public List<UP_VendorBE> GetVendorConsolidationBAL(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();

            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();

                oUP_VendorBEList = oUP_VendorDAL.GetVendorConsolidationDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> GetVendorByIdBAL(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();

            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();

                oUP_VendorBEList = oUP_VendorDAL.GetVendorByIdDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_VendorBEList;
        }

        /// <summary>
        /// Used for getting OTIF Country Miscellenaous Settings
        /// </summary>
        /// <param name="oUP_VendorBE"></param>
        /// <returns></returns>
        public List<UP_VendorBE> GetOTIFCountrySettingsBAL(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();

            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();

                oUP_VendorBEList = oUP_VendorDAL.GetOTIFCountrySettingsDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_VendorBEList;
        }

        /// <summary>
        /// Used for managing OTIF Country Miscellenaous Settings
        /// </summary>
        /// <param name="oUP_VendorBE"></param>
        /// <returns></returns>
        public int? addEditAddEditOTIFCountrySettingsBAL(UP_VendorBE oUP_VendorBE) {
            int? intResult = 0;
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                intResult = oUP_VendorDAL.addEditAddEditOTIFCountrySettingsDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public UP_VendorBE GetVendorDetailBAL(UP_VendorBE oUP_VendorBE) {
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                oUP_VendorBE = oUP_VendorDAL.GetVendorDetailDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_VendorBE;
        }

        public List<SCT_UserBE> GetVendorContact(UP_VendorBE oUP_VendorBE) {
            List<SCT_UserBE> oUSCT_UserBEList = new List<SCT_UserBE>();

            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();

                oUSCT_UserBEList = oUP_VendorDAL.GetVendorContact(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUSCT_UserBEList;
        }

        public void UpdateReturnAddress(UP_VendorBE oUP_VendorBE) {
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                oUP_VendorDAL.UpdateReturnAddress(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }
        public void UpdateVendorActiveSites(UP_VendorBE oUP_VendorBE) {
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                oUP_VendorDAL.UpdateVendorActiveSites(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }
        public DataTable GetVendorActiveSitesBAL(UP_VendorBE oUP_VendorBE) {
            DataTable dtVendorActiveSites = null;
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                dtVendorActiveSites = oUP_VendorDAL.GetVendorActiveSitesDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dtVendorActiveSites;
        }

        public List<UP_VendorBE> GetVendorByUserIdBAL(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();

            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();

                oUP_VendorBEList = oUP_VendorDAL.GetVendorByUserIdDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> GetVendorByUserCountryBAL(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();

            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();

                oUP_VendorBEList = oUP_VendorDAL.GetVendorByUserCountryDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_VendorBEList;
        }
        public List<UP_VendorBE> GetVendorForEdit(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> lstVendor = new List<UP_VendorBE>();

            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();

                lstVendor = oUP_VendorDAL.GetVendorForEdit(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstVendor;
        }

        public void UpdateDeleteVendor(UP_VendorBE oUP_VendorBE) {
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                oUP_VendorDAL.UpdateDeleteVendor(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public DataTable CheckExistingSettingsForVendorBAL(UP_VendorBE oUP_VendorBE) {
            DataTable dtExistingVendor = null;
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                dtExistingVendor = oUP_VendorDAL.CheckExistingSettingsForVendorDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dtExistingVendor;
        }
        public bool CheckVendorNoAlreadyExistBAL(UP_VendorBE oUP_VendorBE) {
            bool IsVendorNoExist = false;
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                IsVendorNoExist = oUP_VendorDAL.CheckVendorNoAlreadyExistDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return IsVendorNoExist;
        }

        public void AddVendors(List<UP_VendorBE> pUP_VendorBEList) {
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                oUP_VendorDAL.AddVendors(pUP_VendorBEList);
                pUP_VendorBEList = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }
        public List<UP_VendorBE> GetVendorReportBAL(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> lstVendor = new List<UP_VendorBE>();

            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();

                lstVendor = oUP_VendorDAL.GetVendorReportDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstVendor;
        }

        public List<UP_VendorBE> GetVendorByVendorSearchBAL(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> lstVendor = new List<UP_VendorBE>();

            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                lstVendor = oUP_VendorDAL.GetVendorByVendorSearchDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstVendor;
        }

        /// <summary>
        ///  Logs Errors Related to Vendors Files in Vendors Error Table
        /// </summary>
        /// <param name="up_ReceiptInformationBEErrorList">void</param>
        public void AddVendorsErrors(List<UP_DataImportErrorBE> up_VendorBEErrorList) {
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                oUP_VendorDAL.AddVendorsErrors(up_VendorBEErrorList);
                up_VendorBEErrorList = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
        }

        public string GetGlobalConsolidateVendorIdsBAL(UP_VendorBE oUP_VendorBE) {
            string strVendorIds = string.Empty;
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                strVendorIds = oUP_VendorDAL.GetGlobalConsolidateVendorIdsDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strVendorIds;
        }

        public List<UP_VendorBE> GetAllGrandParentAndStandAloneVendorsBAL(UP_VendorBE oMAS_VendorBE) {
            var oMAS_VendorBEList = new List<UP_VendorBE>();
            try {
                var oMAS_VendorDAL = new UP_VendorDAL();
                oMAS_VendorBEList = oMAS_VendorDAL.GetAllGrandParentAndStandAloneVendorsDAL(oMAS_VendorBE);
                oMAS_VendorDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMAS_VendorBEList;
        }

        /* Added methods for OTIF report */
        public List<UP_VendorBE> GetOTIFVendorReportsBAL(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                oUP_VendorBEList = oUP_VendorDAL.GetOTIFVendorReportsDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_VendorBEList;
        }

        public List<UP_VendorBE> GetOTIFMasterVendorReportsBAL(UP_VendorBE oUP_VendorBE) {
            List<UP_VendorBE> oUP_VendorBEList = new List<UP_VendorBE>();
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                oUP_VendorBEList = oUP_VendorDAL.GetOTIFMasterVendorReportsDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oUP_VendorBEList;
        }

        public int? UpdateOTIFMasterVendorReportBAL(UP_VendorBE oUP_VendorBE) {
            int? intResult = 0;
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                intResult = oUP_VendorDAL.UpdateOTIFMasterVendorReportDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? UpdateOTIFVendorReportBAL(UP_VendorBE oUP_VendorBE) {
            int? intResult = 0;
            try {
                UP_VendorDAL oUP_VendorDAL = new UP_VendorDAL();
                intResult = oUP_VendorDAL.UpdateOTIFVendorReportDAL(oUP_VendorBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Utilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings {
    public class APPSIT_DeliveryConstraintBAL {

        public List<MASSIT_DeliveryConstraintBE> GetOpenDoorSlotTimeBAL(MASSIT_DeliveryConstraintBE oMASSIT_DeliveryConstraintBE) {
            List<MASSIT_DeliveryConstraintBE> oMASSIT_DeliveryConstraintBEList = new List<MASSIT_DeliveryConstraintBE>();

            try {
                APPSIT_DeliveryConstraintDAL oMASSIT_DeliveryConstraintDAL = new APPSIT_DeliveryConstraintDAL();

                oMASSIT_DeliveryConstraintBEList = oMASSIT_DeliveryConstraintDAL.GetOpenDoorSlotTimeDAL(oMASSIT_DeliveryConstraintBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_DeliveryConstraintBEList;
        }

        public List<MASSIT_DeliveryConstraintBE> GetDeliveryConstraintBAL(MASSIT_DeliveryConstraintBE oMASSIT_DeliveryConstraintBE) {
            List<MASSIT_DeliveryConstraintBE> oMASSIT_DeliveryConstraintBEList = new List<MASSIT_DeliveryConstraintBE>();

            try {
                APPSIT_DeliveryConstraintDAL oMASSIT_DeliveryConstraintDAL = new APPSIT_DeliveryConstraintDAL();

                oMASSIT_DeliveryConstraintBEList = oMASSIT_DeliveryConstraintDAL.GetDeliveryConstraintDAL(oMASSIT_DeliveryConstraintBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_DeliveryConstraintBEList;
        }


        public int? addEdiDeliveryConstraintBAL(MASSIT_DeliveryConstraintBE oMASSIT_DeliveryConstraintBE) {
            int? intResult = 0;
            try {
                APPSIT_DeliveryConstraintDAL oMASSIT_DeliveryConstraintDAL = new APPSIT_DeliveryConstraintDAL();
                intResult = oMASSIT_DeliveryConstraintDAL.addEdiDeliveryConstraintDAL(oMASSIT_DeliveryConstraintBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Resources;

using Utilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;

namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings {
    public class APPSIT_DoorOpenTimeBAL {

        public List<MASSIT_DoorOpenTimeBE> GetDoorNoSetupDetailsBAL(MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE) {
            List<MASSIT_DoorOpenTimeBE> oMASSIT_DoorOpenTimeBEList = new List<MASSIT_DoorOpenTimeBE>();

            try {
                APPSIT_DoorOpenTimeDAL oMASSIT_DoorOpenTimeDAL = new APPSIT_DoorOpenTimeDAL();

                oMASSIT_DoorOpenTimeBEList = oMASSIT_DoorOpenTimeDAL.GetDoorNoAndTypeDAL(oMASSIT_DoorOpenTimeBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_DoorOpenTimeBEList;
        }
        public List<MASSIT_DoorOpenTimeBE> GetDoorOpenTimeBAL(MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE)
        {
            List<MASSIT_DoorOpenTimeBE> oMASSIT_DoorOpenTimeBEList = new List<MASSIT_DoorOpenTimeBE>();

            try
            {
                APPSIT_DoorOpenTimeDAL oMASSIT_DoorOpenTimeDAL = new APPSIT_DoorOpenTimeDAL();

                oMASSIT_DoorOpenTimeBEList = oMASSIT_DoorOpenTimeDAL.GetDoorOpenTimeDAL(oMASSIT_DoorOpenTimeBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_DoorOpenTimeBEList;
        }

        public List<MASSIT_DoorOpenTimeBE> GetDoorConstraintsBAL(MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE) {
            List<MASSIT_DoorOpenTimeBE> oMASSIT_DoorOpenTimeBEList = new List<MASSIT_DoorOpenTimeBE>();

            try {
                APPSIT_DoorOpenTimeDAL oMASSIT_DoorOpenTimeDAL = new APPSIT_DoorOpenTimeDAL();

                oMASSIT_DoorOpenTimeBEList = oMASSIT_DoorOpenTimeDAL.GetDoorConstraintsDAL(oMASSIT_DoorOpenTimeBE);

            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oMASSIT_DoorOpenTimeBEList;
        }

        public int? addEditVehicleTypeDetailsBAL(MASSIT_DoorOpenTimeBE oMASSIT_DoorOpenTimeBE) {
            int? intResult = 0;
            try {
                APPSIT_DoorOpenTimeDAL oMASSIT_DoorOpenTimeDAL = new APPSIT_DoorOpenTimeDAL();
                intResult = oMASSIT_DoorOpenTimeDAL.AddEditDoorConstraintsDAL(oMASSIT_DoorOpenTimeBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;

using Utilities;
using BusinessEntities.ModuleBE.Appointment.SiteSettings;
using DataAccessLayer.ModuleDAL.Appointment.SiteSettings;


namespace BusinessLogicLayer.ModuleBAL.Appointment.SiteSettings {
    public class MASSIT_TimeWindowBAL : BaseBAL {

        public string GetWeekSetupBAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            string WeekSetupError = string.Empty;
            try
            {
                MASSIT_TimeWindowDAL oMASSIT_TimeWindowDAL = new MASSIT_TimeWindowDAL();
                WeekSetupError = oMASSIT_TimeWindowDAL.GetWeekSetupDAL(oMASSIT_TimeWindowBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return WeekSetupError;
        }

        public List<MASSIT_TimeWindowBE> GetDoorNoDetailsBAL(int SiteID)
        {
            List<MASSIT_TimeWindowBE> olstMASSIT_TimeWindowBE = null;
            try
            {
                MASSIT_TimeWindowDAL oMASSIT_TimeWindowDAL = new MASSIT_TimeWindowDAL();
                olstMASSIT_TimeWindowBE = oMASSIT_TimeWindowDAL.GetDoorNoDetailsBAL(SiteID);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return olstMASSIT_TimeWindowBE;
        }


        public MASSIT_TimeWindowBE GetWindowDetailBAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            MASSIT_TimeWindowBE olstMASSIT_TimeWindowBE = null;
            try
            {
                MASSIT_TimeWindowDAL oMASSIT_TimeWindowDAL = new MASSIT_TimeWindowDAL();
                olstMASSIT_TimeWindowBE = oMASSIT_TimeWindowDAL.GetWindowDetailDAL(oMASSIT_TimeWindowBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return olstMASSIT_TimeWindowBE;
        }

        public List<MASSIT_TimeWindowBE> GetAlternateWindowBAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            List<MASSIT_TimeWindowBE> olstMASSIT_TimeWindowBE = null;
            try
            {
                MASSIT_TimeWindowDAL oMASSIT_TimeWindowDAL = new MASSIT_TimeWindowDAL();
                olstMASSIT_TimeWindowBE = oMASSIT_TimeWindowDAL.GetAlternateWindowDAL(oMASSIT_TimeWindowBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return olstMASSIT_TimeWindowBE;
        }

        public List<MASSIT_TimeWindowBE> GetProposedWindowBAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            List<MASSIT_TimeWindowBE> olstMASSIT_TimeWindowBE = null;
            try
            {
                MASSIT_TimeWindowDAL oMASSIT_TimeWindowDAL = new MASSIT_TimeWindowDAL();
                olstMASSIT_TimeWindowBE = oMASSIT_TimeWindowDAL.GetProposedWindowDAL(oMASSIT_TimeWindowBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return olstMASSIT_TimeWindowBE;
        }

        public int? addEditWeekSetupBAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE) {
            int? intResult = 0;
            try {
                MASSIT_TimeWindowDAL oMASSIT_TimeWindowDAL = new MASSIT_TimeWindowDAL();
                intResult = oMASSIT_TimeWindowDAL.addEditTimeWindowDAL(oMASSIT_TimeWindowBE);
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? IsStartEndTimeExistBAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            int? intResult = 0;
            try
            {
                MASSIT_TimeWindowDAL oMASSIT_TimeWindowDAL = new MASSIT_TimeWindowDAL();
                intResult = oMASSIT_TimeWindowDAL.IsStartEndTimeExistDAL(oMASSIT_TimeWindowBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MASSIT_TimeWindowBE> GetTimeWindowBAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            List<MASSIT_TimeWindowBE> olstMASSIT_TimeWindowBE = null;
            try
            {
                MASSIT_TimeWindowDAL oMASSIT_TimeWindowDAL = new MASSIT_TimeWindowDAL();
                olstMASSIT_TimeWindowBE = oMASSIT_TimeWindowDAL.GetTimeWindowDAL(oMASSIT_TimeWindowBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return olstMASSIT_TimeWindowBE;
        }

        public List<MASSIT_TimeWindowBE> GetStartEndTimeBasedOnSiteBAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            List<MASSIT_TimeWindowBE> olstMASSIT_TimeWindowBE = null;
            try
            {
                MASSIT_TimeWindowDAL oMASSIT_TimeWindowDAL = new MASSIT_TimeWindowDAL();
                olstMASSIT_TimeWindowBE = oMASSIT_TimeWindowDAL.GetStartEndTimeBasedOnSiteDAL(oMASSIT_TimeWindowBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return olstMASSIT_TimeWindowBE;
        }

        public int? IsBookingExistForWindowBAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            int? intResult = 0;
            try
            {
                MASSIT_TimeWindowDAL oMASSIT_TimeWindowDAL = new MASSIT_TimeWindowDAL();
                intResult = oMASSIT_TimeWindowDAL.IsBookingExistForWindowDAL(oMASSIT_TimeWindowBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public int? DeleteWindowBAL(MASSIT_TimeWindowBE oMASSIT_TimeWindowBE)
        {
            int? intResult = 0;
            try
            {
                MASSIT_TimeWindowDAL oMASSIT_TimeWindowDAL = new MASSIT_TimeWindowDAL();
                intResult = oMASSIT_TimeWindowDAL.DeleteWindowDAL(oMASSIT_TimeWindowBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }
    }

}

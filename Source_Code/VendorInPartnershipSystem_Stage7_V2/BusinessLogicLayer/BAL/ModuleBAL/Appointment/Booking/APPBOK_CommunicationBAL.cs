﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.ModuleBE.Appointment.Booking;
using DataAccessLayer.ModuleDAL.Appointment.Booking;
using Utilities;
using BusinessEntities.ModuleBE.Languages.Languages;

namespace BusinessLogicLayer.ModuleBAL.Appointment.Booking
{
    public class APPBOK_CommunicationBAL
    {
        public int? AddItemBAL(int BookingID, string fromAddress, string toAddress, string mailSubject, string htmlBody, int UserId, CommunicationType.Enum communicationType)
        {
            // Resending of Delivery mails
            APPBOK_CommunicationBE oAPPBOK_CommunicationBE = new APPBOK_CommunicationBE();
            oAPPBOK_CommunicationBE.Action = "AddBookingCommunication";
            oAPPBOK_CommunicationBE.CommunicationType = CommunicationType.Name(communicationType);
            oAPPBOK_CommunicationBE.BookingID = BookingID;
            oAPPBOK_CommunicationBE.Subject = mailSubject;
            oAPPBOK_CommunicationBE.SentFrom = fromAddress;
            oAPPBOK_CommunicationBE.SentTo = toAddress;
            oAPPBOK_CommunicationBE.Body = htmlBody;
            oAPPBOK_CommunicationBE.SendByID = UserId;
            APPBOK_CommunicationBAL oAPPBOK_CommunicationBAL = new APPBOK_CommunicationBAL();
            return oAPPBOK_CommunicationBAL.AddItemBAL(oAPPBOK_CommunicationBE);
        }
        /// <summary>
        /// Used to add booking communications
        /// </summary>
        /// <param name="oAPPBOK_BookingBE"></param>
        /// <returns></returns>
        public int? AddItemBAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
                intResult = oAPPBOK_CommunicationDAL.AddItemDAL(oAPPBOK_CommunicationBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<APPBOK_CommunicationBE> GetItemsBAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            List<APPBOK_CommunicationBE> oAPPBOK_CommunicationBEList = new List<APPBOK_CommunicationBE>();

            try
            {
                APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
                oAPPBOK_CommunicationBEList = oAPPBOK_CommunicationDAL.GetItemsDAL(oAPPBOK_CommunicationBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_CommunicationBEList;
        }

        public APPBOK_CommunicationBE GetItemBAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            try
            {
                APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
                oAPPBOK_CommunicationBE = oAPPBOK_CommunicationDAL.GetItemDAL(oAPPBOK_CommunicationBE);

            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oAPPBOK_CommunicationBE;
        }

        public int? AddBookingCommunicationBAL(APPBOK_CommunicationBE oAPPBOK_CommunicationBE)
        {
            int? intResult = 0;
            try
            {
                APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
                intResult = oAPPBOK_CommunicationDAL.AddBookingCommunicationDAL(oAPPBOK_CommunicationBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public List<MAS_LanguageBE> GetLanguages()
        {
            APPBOK_CommunicationDAL oAPPBOK_CommunicationDAL = new APPBOK_CommunicationDAL();
            return oAPPBOK_CommunicationDAL.GetLanguages();
        }
    }
}

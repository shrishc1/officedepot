﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Utilities;
using BusinessEntities.ModuleBE.Discrepancy.LogDiscrepancy;
using DataAccessLayer.ModuleDAL.Discrepancy.LogDiscrepancy;


namespace BusinessLogicLayer.ModuleBAL.Discrepancy.LogDiscrepancy
{
    public class DiscrepancyBAL
    {
        public List<DiscrepancyBE> CheckValidPurchaseOrderBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.CheckValidPurchaseOrderDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetPurchaseOrderDateListBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetPurchaseOrderDateListDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }


        public List<DiscrepancyBE> GetContactPurchaseOrderDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetContactPurchaseOrderDetailsDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public String addDiscrepancyLogBAL(DiscrepancyBE oDiscrepancyBE)
        {
            String sResult = "0";
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                sResult = oDiscrepancyDAL.addDiscrepancyLogDAL(oDiscrepancyBE);

                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return sResult;
        }

        public int? addDiscrepancyItemBAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? intResult = 0;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                intResult = oDiscrepancyDAL.addDiscrepancyItemDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return intResult;
        }

        public DataSet  getLanguageIDforCommunicationBAL(DiscrepancyMailBE oDiscrepancyMailBE) {
            DataSet  Result = null;
            try {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.getLanguageIDforCommunicationDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public DataSet GetDiscrepancyDetailBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.GetDiscrepancyDetailDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public int? saveDiscrepancyMailBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.saveDiscrepancyMailDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;                                  
        }
        public int? saveDiscrepancyLetterBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.saveDiscrepancyLetterDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public DataSet getDiscrepancyLetterBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.GetDiscrepancyLetterDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet getDiscrepancyCommunicationBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.GetDiscrepancyCommunicationDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public DataSet getDiscrepancyImagesBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.GetDiscrepancyImagesDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }
        public List<DiscrepancyBE> GetODProductDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetODProductDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetODProductDetailsBySiteVendorBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetODProductDetailsBySiteVendorDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyLogDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyLogDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyLogSiteVendorDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyLogSiteVendorDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyWorkListForCurrentUserBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyWorkListForCurrentUserDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }


        public List<DiscrepancyBE> GetDiscrepancyItemDetailsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyItemDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyItemsForReturnNote(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyItemsForReturnNote(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetDiscrepancyTypeBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyTypeDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetStockPlannersBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetStockPlannersDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetAllPurchaseOrderDateListBAL(DiscrepancyBE oDiscrepancyBE)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetAllPurchaseOrderDateListDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public int? addWorkFlowHTMLsBAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? iResult = 0;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                iResult = oDiscrepancyDAL.addWorkFlowHTMLsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return iResult;
        }

        public List<DiscrepancyBE> GetWorkFlowHTMLsBAL(DiscrepancyBE oDiscrepancyBE) {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try 
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetWorkFlowHTMLsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            } 
            catch (Exception ex) 
            {
                LogUtility.SaveErrorLogEntry(ex);
            } 
            finally { }
            return DiscrepancyBEList;
        }

        public List<DiscrepancyBE> GetUserDetailsForWorkFlowBAL(DiscrepancyBE oDiscrepancyBE) {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();

            try {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                DiscrepancyBEList = oDiscrepancyDAL.GetUserDetailsForWorkFlowDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public DataTable GetActionRequiredDetailsForWorkFlowBAL(DiscrepancyBE oDiscrepancyBE) {
            DataTable dt = new DataTable();
            try {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                dt = oDiscrepancyDAL.GetActionRequiredDetailsForWorkFlowDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataTable GetCheckForGIN003VisibilityBAL(DiscrepancyBE oDiscrepancyBE) {
            DataTable dt = new DataTable();
            try {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                dt = oDiscrepancyDAL.GetCheckForGIN003VisibilityDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public DataTable GetAllDetailsOfWorkFlowBAL(DiscrepancyBE oDiscrepancyBE) {
            DataTable dt = new DataTable();
            try {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                dt = oDiscrepancyDAL.GetCheckForGIN003VisibilityDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return dt;
        }

        public int? UpdateDiscrepancyStatus(DiscrepancyBE oDiscrepancyBE)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.UpdateDiscrepancyStatus(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public int? UpdateVendorActionElapsedTimeBAL(DiscrepancyBE oDiscrepancyBE)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.UpdateVendorActionElapsedTimeDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataTable GetOpenWorkflowEntry(DiscrepancyBE oNewDiscrepancyBE)
        {
            DataTable lstDisc = new DataTable();

            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();

                lstDisc = oDiscrepancyDAL.GetOpenWorkflowEntry(oNewDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return lstDisc;
        }

        
        public void errorLogBAL(DiscrepancyMailBE oNewDiscrepancyMailBE)
        {
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                oDiscrepancyDAL.errorLogDAL(oNewDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }            
        }

        public DataSet GetDiscrepancyLogPrintLabelBAL(DiscrepancyBE oDiscrepancyBE2)
        {
            DataSet ds = new DataSet();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                ds = oDiscrepancyDAL.GetDiscrepancyLogPrintLabelDAL(oDiscrepancyBE2);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public DataSet GetDiscrepancyLogRePrintBAL(DiscrepancyBE oDiscrepancyBE)
        {
            DataSet ds = new DataSet();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                ds = oDiscrepancyDAL.GetDiscrepancyLogRePrintDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }

        public int? AddDiscrepancyReturnNote(DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.AddDiscrepancyReturnNote(oDiscrepancyReturnNoteBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DiscrepancyReturnNoteBE GetDiscrepancyReturnNote(DiscrepancyReturnNoteBE oDiscrepancyReturnNoteBE)
        {
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                oDiscrepancyReturnNoteBE = oDiscrepancyDAL.GetDiscrepancyReturnNote(oDiscrepancyReturnNoteBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oDiscrepancyReturnNoteBE;
        }

        public DiscrepancyBE GetDiscrepancyVendorContact(DiscrepancyBE oDiscrepancyBE)
        {
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                oDiscrepancyBE = oDiscrepancyDAL.GetDiscrepancyReturnNote(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return oDiscrepancyBE;
        }
        public DataSet GetCommunicationDataToSendMailBAL(DiscrepancyMailBE oDiscrepancyMailBE) {
            DataSet ds = new DataSet();
            try {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                ds = oDiscrepancyDAL.GetCommunicationDataToSendMailDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }
        public DataSet GetDiscrepancyForVendorToCollectEscBAL(DiscrepancyMailBE oDiscrepancyMailBE)
        {
            DataSet ds = new DataSet();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                ds = oDiscrepancyDAL.GetDiscrepancyForVendorToCollectEscDAL(oDiscrepancyMailBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }
        public DataSet GetDiscrepancyDetailsBAL(DiscrepancyBE oDiscrepancyBE) {
            DataSet ds = new DataSet();
            try {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                ds = oDiscrepancyDAL.GetDiscrepancyDetailsDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return ds;
        }
        public string GetVendorStatusDAL(DiscrepancyBE oDiscrepancyBE)
        {
            string strResult = string.Empty;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                strResult = oDiscrepancyDAL.GetVendorStatusDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }
        public int? AddDiscrepancyLogCommentBAL(DiscrepancyBE oDiscrepancy)
        {
            int? Result = null;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                Result = oDiscrepancyDAL.AddDiscrepancyLogCommentDAL(oDiscrepancy);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public List<DiscrepancyBE> GetDiscrepancyLogCommentsBAL(DiscrepancyBE oDiscrepancy)
        {
            List<DiscrepancyBE> DiscrepancyBEList = new List<DiscrepancyBE>();
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                DiscrepancyBEList = oDiscrepancyDAL.GetDiscrepancyLogCommentsDAL(oDiscrepancy);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return DiscrepancyBEList;
        }

        public string GetDebitCreditStatusBAL(DiscrepancyBE oDiscrepancyBE)
        {
            string strResult = string.Empty;
            try
            {
                DiscrepancyDAL oDiscrepancyDAL = new DiscrepancyDAL();
                strResult = oDiscrepancyDAL.GetDebitCreditStatusDAL(oDiscrepancyBE);
                oDiscrepancyDAL = null;
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return strResult;
        }
    }
}

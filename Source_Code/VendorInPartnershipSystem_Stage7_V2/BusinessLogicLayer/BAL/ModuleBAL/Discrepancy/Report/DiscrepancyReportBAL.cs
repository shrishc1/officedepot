﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BusinessEntities.ModuleBE.Discrepancy.Report;
using DataAccessLayer.ModuleDAL.Discrepancy.Report;
using Utilities;


namespace BusinessLogicLayer.ModuleBAL.Discrepancy.Report
{
    public class DiscrepancyReportBAL
    {
        DiscrepancyReportBE oDiscrepancyReportBE = new DiscrepancyReportBE();

        public DataSet getDiscrepancyReportBAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.getDiscrepancyReportDAL(oDiscrepancyReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet getAssignedVendorsReportBAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.getAssignedVendorsReportDAL(oDiscrepancyReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }

        public DataSet GetShuttleDiscrepancyReportBAL(DiscrepancyReportBE oDiscrepancyReportBE)
        {
            DataSet Result = null;
            try
            {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.GetShuttleDiscrepancyReportDAL(oDiscrepancyReportBE);
            }
            catch (Exception ex)
            {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }


        public DataSet getDiscrepancySchedulerReportBAL() {
            DataSet Result = null;
            try {
                DiscrepancyReportDAL oDiscrepancyReportDAL = new DiscrepancyReportDAL();
                Result = oDiscrepancyReportDAL.getDiscrepancySchedulerReportDAL();
            }
            catch (Exception ex) {
                LogUtility.SaveErrorLogEntry(ex);
            }
            finally { }
            return Result;
        }     
    }
}

﻿using System;
using System.Security.Cryptography;
using System.IO;

namespace Utilities
{
     
  public class Encryption
    {
        public static string Encrypt(string strText)
        {
            string strEncrKey= "a%#@?,:*";            
            byte[] IV = { 18, 52, 86, 120, 144, 171, 205, 239 };
            try
            {
                byte[] bykey = System.Text.Encoding.UTF8.GetBytes(strEncrKey.PadLeft(8));
                byte[] InputByteArray = System.Text.Encoding.UTF8.GetBytes(strText);
                var des = new DESCryptoServiceProvider();
                var ms = new MemoryStream();
                var cs = new CryptoStream(ms, des.CreateEncryptor(bykey, IV), CryptoStreamMode.Write);
                cs.Write(InputByteArray, 0, InputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }      

        public static string Decrypt(string strText)
        {
            string sDecrKey = "a%#@?,:*";           
            byte[] IV = { 18, 52, 86, 120, 144, 171, 205, 239 };
            byte[] inputByteArray = new byte[strText.Length + 1];
            try
            {
                byte[] byKey = System.Text.Encoding.UTF8.GetBytes(sDecrKey.PadLeft(8));
                var des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(strText);
                var ms = new MemoryStream();
                var cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}

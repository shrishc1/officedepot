﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Threading;

namespace ForcastAccuracyStarter
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {           
            ForcastAccuracyDataImport();
        }
        public static void ForcastAccuracyDataImport()
        {
            try
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("\r\n");
                sbMessage.Append("Step 14A Short term Forecast Accuracy Start.  " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                //Validate and move data main tables
                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "sp_InsertForecastAccuracyData";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;
                GlobalVariable.sqlComm.ExecuteNonQuery();
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Step 14A Short term Forecast Accuracy end.  " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
                sbMessage.Clear();
            }

            catch (Exception ex)
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Scheduler Terminated at " + DateTime.Now.ToString() + "\r\n" + ex.Message);
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
        }
        public class LogUtility
        {
            public static bool SaveTraceLogEntry(StringBuilder traceInformation)
            {
                try
                {
                    StringBuilder sbMessage = new StringBuilder();
                    sbMessage.Append(traceInformation);

                    bool flag = LogUtility.WriteTrace(sbMessage);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            public static bool WriteTrace(StringBuilder sbMessage)
            {
                try
                {
                    FileStream fs;
                    string[] appPath = AppDomain.CurrentDomain.BaseDirectory.Split('\\');

                    string LogDirectoryPath = AppDomain.CurrentDomain.BaseDirectory + "Logs\\";
                    if (!Directory.Exists(LogDirectoryPath))
                        Directory.CreateDirectory(LogDirectoryPath);

                    string logFileName = appPath[appPath.Length - 2] + "_Trace_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                    if (File.Exists(LogDirectoryPath + logFileName) == true)
                        fs = File.Open(LogDirectoryPath + logFileName, FileMode.Append, FileAccess.Write);
                    else
                        fs = File.Create(LogDirectoryPath + logFileName);

                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.Write(sbMessage.ToString() + Environment.NewLine);
                    }
                    fs.Close();

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

        }
        public static class GlobalVariable
        {
            public static SqlConnection sqlCon;
            public static SqlCommand sqlComm;

            public static string folderName = string.Empty;
            public static string SourceFilePath = string.Empty;
            public static string InvokedBy = string.Empty;
            public static string folderName102 = string.Empty;

            public static int? dataImportSchedulerID;
            public static int? folderDownloadedID;
            public static string TimeSpan = string.Empty;

            public static char[] delimiters = new char[] { '|' };
            public static string textFileLine = string.Empty;
            public static string[] textFileFieldValues;

            public static int CommandTimeOutTime = 4200;

            static GlobalVariable()
            {
                string ConnStr;
                ConnStr = ConfigurationManager.AppSettings["sConn"];
                sqlCon = new SqlConnection(ConnStr);
                sqlCon.Open();

                sqlComm = new SqlCommand();
                sqlComm.Connection = sqlCon;
            }
        }
    }
}
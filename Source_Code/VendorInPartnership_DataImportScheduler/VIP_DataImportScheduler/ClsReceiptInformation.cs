﻿using System.Data.SqlClient;
using System.IO;
using System;
using System.Data;
using System.Text;
using Utilities;

namespace VendorInPartnership_DataImportScheduler
{
    class ClsReceiptInformation
    {
        public void UploadReceiptInformation(string receiptInformationFilePath, string receiptInformationFileName)
        {

            DataTable dt = new DataTable();
            DataRow dr;
            int TotalRecordCount = 0;
            int Qty_receiptedIndex = 0;
            int DateIndex = 0;
            int orgorddateIndex = 0;
            //------------------------
            DataTable errorDt = new DataTable();
            DataRow errorDr;
            int Viking_sku_no_Index = 0;
            int OD_SKU_Number_Index = 0;
            int Vendor_prod_code_Index = 0;
            int Warehouse_Index = 0;
            int PO_number_Index = 0;
            int Other_Index = 0;
            int Country_Index = 0;

            //------------------------

            string FPID = Common.InsertUP_FileProcessed(GlobalVariable.folderDownloadedID.Value, TotalRecordCount, 0, receiptInformationFileName);
            int fileProcessedID = Convert.ToInt32(FPID);
            try
            {
                using (StreamReader oStreamReader = new StreamReader(receiptInformationFilePath))
                {
                    GlobalVariable.textFileLine = oStreamReader.ReadLine().ToLower();
                    var columns = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);
                    foreach (string col in columns)
                    {
                        switch (col.ToLower())
                        {
                            case "viking_sku_no":
                                dt.Columns.Add(new DataColumn("Viking_sku_no"));
                                Viking_sku_no_Index = Array.IndexOf(columns, "viking_sku_no");
                                break;
                            case "od_sku_number":
                                dt.Columns.Add(new DataColumn("OD_SKU_Number"));
                                OD_SKU_Number_Index = Array.IndexOf(columns, "od_sku_number");
                                break;
                            case "vendor_prod_code":
                                dt.Columns.Add(new DataColumn("Vendor_prod_code"));
                                Vendor_prod_code_Index = Array.IndexOf(columns, "vendor_prod_code");
                                break;
                            case "warehouse":
                                dt.Columns.Add(new DataColumn("Warehouse"));
                                Warehouse_Index = Array.IndexOf(columns, "warehouse");
                                break;
                            case "po_number":
                                dt.Columns.Add(new DataColumn("PO_number"));
                                PO_number_Index = Array.IndexOf(columns, "po_number");
                                break;
                            case "qty_receipted":
                                dt.Columns.Add(new DataColumn("Qty_receipted", typeof(System.Int32)));
                                Qty_receiptedIndex = Array.IndexOf(columns, "qty_receipted");
                                break;
                            case "other":
                                dt.Columns.Add(new DataColumn("Other"));
                                Other_Index = Array.IndexOf(columns, "other");
                                break;
                            case "date":
                                dt.Columns.Add(new DataColumn("Date", typeof(System.DateTime)));
                                DateIndex = Array.IndexOf(columns, "date");
                                break;
                            case "country":
                                dt.Columns.Add(new DataColumn("Country"));
                                Country_Index = Array.IndexOf(columns, "country");
                                break;
                            case "orgorddate":
                                dt.Columns.Add(new DataColumn("ORGOrdDate", typeof(System.DateTime)));
                                orgorddateIndex = Array.IndexOf(columns, "orgorddate");
                                break;
                            default:
                                break;
                        }
                    }
                    dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));
                    //--------------------- Added on 14 Feb 2013 -----------------------
                    errorDt.Columns.Add(new DataColumn("FolderDownloadID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ManualFileID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                    errorDt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));
                    //--------------------- Added on 14 Feb 2013 -----------------------

                    int iCount = 0;
                    DateTime? tempDate = DateTime.Now;
                    while ((GlobalVariable.textFileLine = oStreamReader.ReadLine()) != null)
                    {
                        string errorMessage = string.Empty;
                        errorDr = errorDt.NewRow();
                        iCount++;
                        dr = dt.NewRow();
                        GlobalVariable.textFileFieldValues = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);

                        for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++)
                        {
                            GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                        }

                        GlobalVariable.textFileFieldValues[Qty_receiptedIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Qty_receiptedIndex]);

                        try
                        {
                            tempDate = Convert.ToDateTime(RemoveDecimal(GlobalVariable.textFileFieldValues[DateIndex]));
                            GlobalVariable.textFileFieldValues[DateIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[DateIndex]);
                        }
                        catch
                        {
                            errorMessage += columns[DateIndex].ToString() + " has invalid date - " + GlobalVariable.textFileFieldValues[DateIndex] + " ";
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + receiptInformationFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                            TotalRecordCount += 1;
                            continue;
                        }

                        try
                        {
                            tempDate = Convert.ToDateTime(RemoveDecimal(GlobalVariable.textFileFieldValues[orgorddateIndex]));
                            GlobalVariable.textFileFieldValues[orgorddateIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[orgorddateIndex]);
                        }
                        catch
                        {
                            errorMessage += columns[orgorddateIndex].ToString() + " has invalid date - " + GlobalVariable.textFileFieldValues[orgorddateIndex] + " ";
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + receiptInformationFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                            TotalRecordCount += 1;
                            continue;
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Viking_sku_no_Index])) && GlobalVariable.textFileFieldValues[Viking_sku_no_Index].Length > 25)
                        {

                            errorMessage += GetErrorMessage(columns[Viking_sku_no_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Viking_sku_no_Index],
                                                                          25);
                            GlobalVariable.textFileFieldValues[Viking_sku_no_Index] = GlobalVariable.textFileFieldValues[Viking_sku_no_Index].Substring(0, 25);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[OD_SKU_Number_Index])) && GlobalVariable.textFileFieldValues[OD_SKU_Number_Index].Length > 25)
                        {

                            errorMessage += GetErrorMessage(columns[OD_SKU_Number_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[OD_SKU_Number_Index],
                                                                          25);

                            GlobalVariable.textFileFieldValues[OD_SKU_Number_Index] = GlobalVariable.textFileFieldValues[OD_SKU_Number_Index].Substring(0, 25);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Vendor_prod_code_Index])) && GlobalVariable.textFileFieldValues[Vendor_prod_code_Index].Length > 50)
                        {

                            errorMessage += GetErrorMessage(columns[Vendor_prod_code_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Vendor_prod_code_Index],
                                                                          50);
                            GlobalVariable.textFileFieldValues[Vendor_prod_code_Index] = GlobalVariable.textFileFieldValues[Vendor_prod_code_Index].Substring(0, 50);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Warehouse_Index])) && GlobalVariable.textFileFieldValues[Warehouse_Index].Length > 10)
                        {

                            errorMessage += GetErrorMessage(columns[Warehouse_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Warehouse_Index],
                                                                          10);
                            GlobalVariable.textFileFieldValues[Warehouse_Index] = GlobalVariable.textFileFieldValues[Warehouse_Index].Substring(0, 10);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[PO_number_Index])) && GlobalVariable.textFileFieldValues[PO_number_Index].Length > 15)
                        {

                            errorMessage += GetErrorMessage(columns[PO_number_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[PO_number_Index],
                                                                          15);
                            GlobalVariable.textFileFieldValues[PO_number_Index] = GlobalVariable.textFileFieldValues[PO_number_Index].Substring(0, 15);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Other_Index])) && GlobalVariable.textFileFieldValues[Other_Index].Length > 50)
                        {

                            errorMessage += GetErrorMessage(columns[Other_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Other_Index],
                                                                          50);
                            GlobalVariable.textFileFieldValues[Other_Index] = GlobalVariable.textFileFieldValues[Other_Index].Substring(0, 50);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Country_Index])) && GlobalVariable.textFileFieldValues[Country_Index].Length > 20)
                        {

                            errorMessage += GetErrorMessage(columns[Country_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Country_Index],
                                                                          20);
                            GlobalVariable.textFileFieldValues[Country_Index] = GlobalVariable.textFileFieldValues[Country_Index].Substring(0, 20);
                        }


                        dr.ItemArray = GlobalVariable.textFileFieldValues;
                        dr["UpdatedDate"] = Common.GetFileDate(receiptInformationFileName);
                        dt.Rows.Add(dr);

                        //--------------------- Added on 14 Feb 2013 -----------------------
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + receiptInformationFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                        }
                        //--------------------- Added on 14 Feb 2013 -----------------------
                        TotalRecordCount += 1;
                    }
                    SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null)
                    {
                        DestinationTableName = "UP_StageReceiptInformation"
                    };
                    oSqlBulkCopy.ColumnMappings.Add("Viking_sku_no", "Viking_sku_no");
                    oSqlBulkCopy.ColumnMappings.Add("OD_SKU_Number", "OD_SKU_Number");
                    oSqlBulkCopy.ColumnMappings.Add("Vendor_prod_code", "Vendor_prod_code");
                    oSqlBulkCopy.ColumnMappings.Add("Warehouse", "Warehouse");
                    oSqlBulkCopy.ColumnMappings.Add("PO_number", "PO_number");
                    oSqlBulkCopy.ColumnMappings.Add("Qty_receipted", "Qty_receipted");
                    oSqlBulkCopy.ColumnMappings.Add("Other", "Other");
                    oSqlBulkCopy.ColumnMappings.Add("Date", "Date");
                    oSqlBulkCopy.ColumnMappings.Add("Country", "Country");
                    oSqlBulkCopy.ColumnMappings.Add("ORGOrdDate", "ORGOrdDate");
                    oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                    oSqlBulkCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime;
                    oSqlBulkCopy.BatchSize = dt.Rows.Count;
                    oSqlBulkCopy.WriteToServer(dt);
                    oSqlBulkCopy.Close();

                    //--------------------- Added on 14 Feb 2013 -----------------------
                    SqlBulkCopy oSqlBulkErrorCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null)
                    {
                        DestinationTableName = "UP_ErrorReceiptInformation"
                    };
                    oSqlBulkErrorCopy.ColumnMappings.Add("FolderDownloadID", "FolderDownloadID");
                    oSqlBulkErrorCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                    oSqlBulkErrorCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");
                    oSqlBulkErrorCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime;
                    oSqlBulkErrorCopy.BatchSize = errorDt.Rows.Count;
                    oSqlBulkErrorCopy.WriteToServer(errorDt);
                    oSqlBulkErrorCopy.Close();
                    //--------------------- Added on 14 Feb 2013 -----------------------
                }
                Common.UpdateUP_FileProcessed(fileProcessedID, TotalRecordCount, 0);
            }
            catch (Exception ex)
            {
                Common.UpdateUP_SchedulerExecutionStatus(GlobalVariable.dataImportSchedulerID.Value);
            }
            finally
            {
            }

            /// -----------------------------------------------------
            /// --- Added on 19 Feb 2013 
            /// -----------------------------------------------------
            try
            {
                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "spUP_ManualFileUpload";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.Parameters.Add("@Action", SqlDbType.VarChar).Value = "InsertFiles";
                GlobalVariable.sqlComm.Parameters.Add("@DateUploaded", SqlDbType.DateTime).Value = Common.GetFileDate(receiptInformationFileName);
                GlobalVariable.sqlComm.Parameters.Add("@DownloadedFilename", SqlDbType.VarChar).Value = receiptInformationFileName;
                GlobalVariable.sqlComm.Parameters.Add("@UserID", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalPORecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalReceiptInformationRecord", SqlDbType.Int).Value = TotalRecordCount;
                GlobalVariable.sqlComm.Parameters.Add("@TotalSKURecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalVendorRecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@UploadType", SqlDbType.VarChar).Value = "AUTO";
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;

                GlobalVariable.sqlComm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                GlobalVariable.sqlComm.CommandType = CommandType.Text;
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("No of Lines:" + TotalRecordCount);
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            /// -----------------------------------------------------
        }

        private string RemoveDecimal(string val)
        {
            string retVal;
            retVal = RemoveSpace(val);

            if (retVal != null)
            {
                if (retVal.Contains("."))
                {
                    retVal = retVal.Substring(0, retVal.IndexOf("."));
                }
                if (retVal.Contains("-"))
                {
                    retVal = '-' + (retVal.Trim('-'));
                }
                if (retVal.Contains("/"))
                {
                    string[] arrDate = retVal.Split('/');
                    retVal = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                }
            }
            return retVal;
        }

        private string RemoveSpace(string val)
        {
            string retVal;
            if (val != string.Empty)
                retVal = val.Trim(' ');
            else
                retVal = null;
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ColumnName"></param>
        /// <param name="Description"></param>
        /// <param name="ColumnLength"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public string GetErrorMessage(string ColumnName, string Description, int ColumnLength)
        {
            var Message = ColumnName + " length greater than " + ColumnLength + " for " + Description + ",";
            return Message;
        }
    }
}

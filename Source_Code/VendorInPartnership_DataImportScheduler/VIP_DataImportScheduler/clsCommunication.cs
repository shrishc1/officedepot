﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilities;
using System.Configuration;
using BusinessEntities.ModuleBE.Upload;
using BusinessLogicLayer.ModuleBAL.Upload;
using System.IO;
using BusinessEntities.ModuleBE.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.AdminFunctions;
using BusinessLogicLayer.ModuleBAL.Appointment.Booking;
using System.Data;

namespace VendorInPartnership_DataImportScheduler
{
    
    class clsCommunication
    {
        StringBuilder sbMessage = new StringBuilder();
        void SendMail(string toAddress, string mailBody, string mailSubject, string fromAddress, bool bIsHTMLMail)
        {
            try
            {
                clsEmail oclsEmail = new clsEmail();
                oclsEmail.sendMail(toAddress, mailBody, mailSubject, fromAddress, true);

                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("\r\n");
                sbMessage.Append("Mail successfully sent to " + toAddress);
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            catch (Exception ex)
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("\r\n");
                sbMessage.Append("Mail sent Error ");
                sbMessage.Append("\r\n");
                LogUtility.SaveErrorLogEntry(ex, "Mail failed for: " + toAddress);
            }
        }

        #region Import scheduler upload status email to OD Admin user ...

        List<UP_DataImportSchedulerBE> GetParentImportSchedulerDetails()
        {
            var dataImportSchedulerBE = new UP_DataImportSchedulerBE();
            var dataImportSchedulerBAL = new UP_DataImportSchedulerBAL();
            dataImportSchedulerBE.Action = "GetDataImportScheduler";
            return dataImportSchedulerBAL.GetDataImportDetailBAL(dataImportSchedulerBE).Take(2).ToList();
        }

        List<UP_DataImportSchedulerBE> GetSchedulerProcessedFilesDetails(int? dataImportSchedulerID)
        {
            var dataImportSchedulerBE = new UP_DataImportSchedulerBE();
            var dataImportSchedulerBAL = new UP_DataImportSchedulerBAL();
            dataImportSchedulerBE.Action = "GetFileProcessedData";
            dataImportSchedulerBE.DataImportSchedulerID = dataImportSchedulerID;
            return dataImportSchedulerBAL.GetGetFileProcessedDataBAL(dataImportSchedulerBE);
        }

        string GetMailBody(string templateFile, UP_DataImportSchedulerBE latestImportData,
            string latestFileName, string latestFileTotalRecords, string previousFileName, string previousFileTotalRecords)
        {
            string htmlBody = string.Empty;
            using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
            {
                htmlBody = sReader.ReadToEnd();

                DateTime dtmStartDateTime = Convert.ToDateTime(latestImportData.StartTime);
                var startDateTime = dtmStartDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                htmlBody = htmlBody.Replace("{StartDateTime}", startDateTime);

                DateTime dtmEndDateTime = Convert.ToDateTime(latestImportData.EndTime);
                var startEndDateTime = dtmEndDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                htmlBody = htmlBody.Replace("{EndDateTime}", startEndDateTime);

                htmlBody = htmlBody.Replace("{VendorRecordsValue}", latestImportData.TotalVendorRecord);
                htmlBody = htmlBody.Replace("{SKURecordsValue}", latestImportData.TotalSKURecord);

                htmlBody = htmlBody.Replace("{VendorErrorsValue}", latestImportData.ErrorVendorCount);
                htmlBody = htmlBody.Replace("{SKUErrorsValue}", latestImportData.ErrorSKUCount);

                htmlBody = htmlBody.Replace("{PORecordsValue}", latestImportData.TotalPORecord);
                htmlBody = htmlBody.Replace("{RecieptRecordsValue}", latestImportData.TotalReceiptInformationRecord);

                htmlBody = htmlBody.Replace("{POErrorsValue}", latestImportData.ErrorPOCount);
                htmlBody = htmlBody.Replace("{RecieptErrorsValue}", latestImportData.ErrorReceiptCount);

                htmlBody = htmlBody.Replace("{ExpediteRecordsValue}", latestImportData.TotalExpediteRecord);
                htmlBody = htmlBody.Replace("{ExpediteErrorsValue}", latestImportData.ErrorExpediteCount);

                htmlBody = htmlBody.Replace("{ListofFilesUploadedKey}", latestFileName);
                htmlBody = htmlBody.Replace("{ListofFilesUploadedValue}", latestFileTotalRecords);

                htmlBody = htmlBody.Replace("{PreviousImportKey}", previousFileName);
                htmlBody = htmlBody.Replace("{PreviousImportValue}", previousFileTotalRecords);
            }
            return htmlBody;
        }

        public void SendEmailForImportSchedulerStatus()
        {
            string htmlBody = string.Empty;
            var importSchedulerDetails = this.GetParentImportSchedulerDetails();
            if (importSchedulerDetails != null && importSchedulerDetails.Count > 0)
            {
                var latestImportFilesDetails = this.GetSchedulerProcessedFilesDetails(importSchedulerDetails[0].DataImportSchedulerID);
                string latestFileName = string.Empty;
                string latestFileTotalRecords = string.Empty;

                StringBuilder sbLatestFileName = new StringBuilder();
                StringBuilder sbLatestFileTotalRecords = new StringBuilder();
                foreach (UP_DataImportSchedulerBE item in latestImportFilesDetails)
                {
                    if (!string.IsNullOrEmpty(item.FileName) && !string.IsNullOrWhiteSpace(item.FileName))
                    {
                        sbLatestFileName.Append(string.Format("{0}<br />", item.FileName));
                        sbLatestFileTotalRecords.Append(string.Format("{0}<br />", Convert.ToString(item.NoOfRecordProcessed)));
                    }
                }

                var previousImportFilesDetails = this.GetSchedulerProcessedFilesDetails(importSchedulerDetails[1].DataImportSchedulerID);
                StringBuilder sbPreviousFileName = new StringBuilder();
                StringBuilder sbPreviousFileTotalRecords = new StringBuilder();
                foreach (UP_DataImportSchedulerBE item in previousImportFilesDetails)
                {
                    if (!string.IsNullOrEmpty(item.FileName) && !string.IsNullOrWhiteSpace(item.FileName))
                    {
                        sbPreviousFileName.Append(string.Format("{0}<br />", item.FileName));
                        sbPreviousFileTotalRecords.Append(string.Format("{0}<br />", Convert.ToString(item.NoOfRecordProcessed)));
                    }
                }

                string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
                path = path.Replace(@"\bin\debug", "");

                string templatesPath = path + @"emailtemplates\";
                string templateFile = templatesPath + @"\uploadupdate.english.htm";

                if (System.IO.File.Exists(templateFile.ToLower()))
                {
                    htmlBody = this.GetMailBody(templateFile, importSchedulerDetails[0], sbLatestFileName.ToString(),
                        sbLatestFileTotalRecords.ToString(), sbPreviousFileName.ToString(), sbPreviousFileTotalRecords.ToString());

                    string emailSubject = "Upload Update"; // As per specs suggested by client.                    
                    string fromAddress = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                    string toAddress = Convert.ToString(ConfigurationManager.AppSettings["toAddress"]);
                    this.SendMail(toAddress, htmlBody, emailSubject, fromAddress, true);
                }
            }
        }

        #endregion

        #region Functions to send emails and save data for new imported Purchase Orders ...

        public void SendAndSaveNewPOInformation()
        {
            var dataImportSchedulerBAL = new UP_DataImportSchedulerBAL();
            var purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE
            {
                Action = "ImportNewPOLetterMain"
            };
            var lstNewPOsData = dataImportSchedulerBAL.GetImportNewPOLetterMainBAL(purchaseOrderDetailBE);

            var sbMessage = new StringBuilder();
            sbMessage.Clear();
            sbMessage.Append("\r\n");
            sbMessage.Append("Todays's New PO count is : " + lstNewPOsData.Count + "\r\n");
            sbMessage.Append("\r\n");
            LogUtility.SaveTraceLogEntry(sbMessage);

            foreach (Up_PurchaseOrderDetailBE purchaseOrderBE in lstNewPOsData)
            {
                #region Logic to getting here Vendor Point of Contact for particular vendor ...

                var vendorPointsContactBAL = new MAS_MaintainVendorPointsContactBAL();
                var VendorPointsContact = new MAS_MaintainVendorPointsContactBE();
                VendorPointsContact.Action = "GetMaintainVendorPointsContact";
                VendorPointsContact.Vendor = new MAS_VendorBE();
                VendorPointsContact.Vendor.VendorID = purchaseOrderBE.Vendor.VendorID; ;
                var lstVendorInvPointsContact = new List<MAS_MaintainVendorPointsContactBE>();
                lstVendorInvPointsContact = vendorPointsContactBAL.GetVendorPointsContactsBAL(VendorPointsContact)
                    .FindAll(x => x.InventoryPOC.Equals("Y"));

                vendorPointsContactBAL = null;
                var pointEmailContacts = string.Empty;
                var firstPointOfConntact = string.Empty;
                foreach (MAS_MaintainVendorPointsContactBE pointContact in lstVendorInvPointsContact)
                {
                    if (!string.IsNullOrEmpty(pointContact.EmailAddress) && !string.IsNullOrWhiteSpace(pointContact.EmailAddress))
                    {
                        if (string.IsNullOrEmpty(pointEmailContacts))
                        {
                            pointEmailContacts = firstPointOfConntact = pointContact.EmailAddress;
                        }
                        else if (!pointEmailContacts.Contains(pointContact.EmailAddress))
                        {
                            pointEmailContacts = string.Format("{0},{1}", pointEmailContacts, pointContact.EmailAddress);
                        }
                    }
                }

                if (string.IsNullOrEmpty(pointEmailContacts))
                    pointEmailContacts = firstPointOfConntact = Convert.ToString(ConfigurationManager.AppSettings["DefaultEmailAddress"]);

                #endregion

                #region Logic to getting here PO line details ...
                purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                purchaseOrderDetailBE.Action = "ImportNewPOLetterDetails";
                purchaseOrderDetailBE.Purchase_order = purchaseOrderBE.Purchase_order;
                purchaseOrderDetailBE.Warehouse = purchaseOrderBE.Warehouse;
                purchaseOrderDetailBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                purchaseOrderDetailBE.Vendor.VendorID = purchaseOrderBE.Vendor.VendorID;
                var lstNewPOsDetails = dataImportSchedulerBAL.GetImportInsertNewSentPOEmailDataBAL(purchaseOrderDetailBE);
                lstNewPOsDetails = lstNewPOsDetails.OrderBy(x => x.Line_No).ToList();
                decimal poTotalCost = 0;
                string currency = string.Empty;
                var poLinesDetails = new StringBuilder();
                foreach (Up_PurchaseOrderDetailBE purchaseDetailsBE in lstNewPOsDetails)
                {
                    poTotalCost = Convert.ToDecimal(poTotalCost + purchaseDetailsBE.TotalCost);

                    if (!string.IsNullOrEmpty(purchaseDetailsBE.Currency) && string.IsNullOrEmpty(currency))
                        currency = purchaseDetailsBE.Currency;

                    //var c="&nbsp;";
                    //var a = !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.Line_No)) ? Convert.ToString(purchaseDetailsBE.Line_No) : "&nbsp;";

                    #region Logic to creating dynamic PO lines data ...
                    poLinesDetails.Append("<tr>");
                    poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; border-left: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.Line_No)) ? Convert.ToString(purchaseDetailsBE.Line_No) : "&nbsp;"));
                    poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.Direct_code)) ? Convert.ToString(purchaseDetailsBE.Direct_code) : "&nbsp;"));
                    poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.OD_Code)) ? Convert.ToString(purchaseDetailsBE.OD_Code) : "&nbsp;"));
                    poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.Vendor_Code)) ? Convert.ToString(purchaseDetailsBE.Vendor_Code) : "&nbsp;"));
                    poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.Product_description)) ? Convert.ToString(purchaseDetailsBE.Product_description) : "&nbsp;"));
                    poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.Original_quantity)) ? Convert.ToString(purchaseDetailsBE.Original_quantity) : "&nbsp;"));
                    poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.Outstanding_Qty)) ? Convert.ToString(purchaseDetailsBE.Outstanding_Qty) : "&nbsp;"));
                    poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.UOM)) ? Convert.ToString(purchaseDetailsBE.UOM) : "&nbsp;"));
                    poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.PO_cost)) ? Convert.ToString(purchaseDetailsBE.PO_cost) : "&nbsp;"));
                    poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; border-right: solid 2px #272727; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", !string.IsNullOrEmpty(Convert.ToString(purchaseDetailsBE.TotalCost)) ? Convert.ToString(purchaseDetailsBE.TotalCost) : "&nbsp;"));
                    DateTime deliveryDate = Convert.ToDateTime(purchaseDetailsBE.Original_due_date);
                    poLinesDetails.Append(string.Format("<td style=\"padding-top: 3px; padding-bottom: 3px; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\">{0}</td>", String.Format("{0:dd/MM/yyyy}", deliveryDate)));
                    poLinesDetails.Append("</tr>");
                    #endregion
                }
                #endregion

                #region Logic to sending email to Vendors's point of contacts and inserting records in database ...
                var communicationBAL = new APPBOK_CommunicationBAL();
                var lstLanguages = communicationBAL.GetLanguages();
                foreach (BusinessEntities.ModuleBE.Languages.Languages.MAS_LanguageBE objLanguage in lstLanguages)
                {
                    var isMailSentInLanguage = false;
                    if (objLanguage.Language.Trim().ToUpper() == purchaseOrderBE.Vendor.Language.Trim().ToUpper())
                        isMailSentInLanguage = true;

                    var sentFrom = Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]);
                    var emailSubject = UIUtility.getGlobalResourceValue("ImportNewPOLetterSubject", objLanguage.Language).Replace("{PONumber}", purchaseOrderBE.Purchase_order);
                    var htmlBody = this.GetEmailTemplateData(objLanguage.Language, purchaseOrderBE, Convert.ToString(poLinesDetails), poTotalCost, currency);

                    /* Logic to Inserting new imported PO records in database. */
                    purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                    purchaseOrderDetailBE.Action = "ImportInsertNewSentPOEmailData";
                    purchaseOrderDetailBE.Purchase_order = purchaseOrderBE.Purchase_order;
                    purchaseOrderDetailBE.Site = new BusinessEntities.ModuleBE.AdminFunctions.MAS_SiteBE();
                    purchaseOrderDetailBE.Site.SiteID = purchaseOrderBE.Site.SiteID;
                    purchaseOrderDetailBE.StockPlannerID = purchaseOrderBE.StockPlannerID;
                    purchaseOrderDetailBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                    purchaseOrderDetailBE.Vendor.VendorID = purchaseOrderBE.Vendor.VendorID;
                    purchaseOrderDetailBE.Order_raised = purchaseOrderBE.Order_raised;
                    purchaseOrderDetailBE.Original_due_date = purchaseOrderBE.Original_due_date;
                    purchaseOrderDetailBE.EntryType = purchaseOrderBE.EntryType;
                    purchaseOrderDetailBE.SentFrom = sentFrom;
                    purchaseOrderDetailBE.SentTo = pointEmailContacts;
                    purchaseOrderDetailBE.Subject = emailSubject;
                    purchaseOrderDetailBE.Body = htmlBody;
                    purchaseOrderDetailBE.LanguageID = objLanguage.LanguageID;
                    purchaseOrderDetailBE.IsMailSentInLanguage = isMailSentInLanguage;
                    purchaseOrderDetailBE.CommunicationType = "New PO";
                    purchaseOrderDetailBE.Warehouse = purchaseOrderBE.Warehouse;
                    purchaseOrderDetailBE.FirstSentToEmail = firstPointOfConntact;
                    dataImportSchedulerBAL.SaveImportNewSentPOEmailDataBAL(purchaseOrderDetailBE);
                    if (isMailSentInLanguage)
                    {
                        this.SendMail(pointEmailContacts, htmlBody, emailSubject, sentFrom, true);
                    }
                }
                #endregion

                #region Logic to update status in Purchase order table for sent email status ...
                purchaseOrderDetailBE = new Up_PurchaseOrderDetailBE();
                purchaseOrderDetailBE.Action = "ImportSetIsEmailSentStatus";
                purchaseOrderDetailBE.Purchase_order = purchaseOrderBE.Purchase_order;
                purchaseOrderDetailBE.Warehouse = purchaseOrderBE.Warehouse;
                purchaseOrderDetailBE.Vendor = new BusinessEntities.ModuleBE.Appointment.SiteSettings.UP_VendorBE();
                purchaseOrderDetailBE.Vendor.VendorID = purchaseOrderBE.Vendor.VendorID;
                dataImportSchedulerBAL.ImportSetIsEmailSentStatusBAL(purchaseOrderDetailBE);
                #endregion
            }
        }

        private string GetEmailTemplateData(string language, Up_PurchaseOrderDetailBE poMain, string poLineItemsDynamicTRs, decimal poTotalCost, string currency)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
            path = path.Replace(@"\bin\debug", "");
            string templatesPath = path + @"emailtemplates\";
            string templateFile = templatesPath + @"\PurchaseOrderLetter.english.htm";

            string htmlBody = string.Empty;
            if (System.IO.File.Exists(templateFile.ToLower()))
            {
                using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                {
                    htmlBody = sReader.ReadToEnd();
                    htmlBody = htmlBody.Replace("{DearSirMadam}", UIUtility.getGlobalResourceValue("DearSirMadam", language));
                    htmlBody = htmlBody.Replace("{PurchaseOrderLetter1}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter1", language));
                    htmlBody = htmlBody.Replace("{PurchaseOrderLetter2}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter2", language));
                    htmlBody = htmlBody.Replace("{PurchaseOrderLetter3}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter3", language));
                    htmlBody = htmlBody.Replace("{Yoursfaithfully}", UIUtility.getGlobalResourceValue("Yoursfaithfully", language));

                    if (string.IsNullOrEmpty(poMain.StockPlannerName) && string.IsNullOrEmpty(poMain.StockPlannerContactNo))
                    {
                        htmlBody = htmlBody.Replace("{StockPlannerNameValue}", poMain.Buyer_no);
                    }


                    if (!string.IsNullOrEmpty(poMain.StockPlannerName))
                        htmlBody = htmlBody.Replace("{StockPlannerNameValue}", poMain.StockPlannerName);
                    else
                        htmlBody = htmlBody.Replace("{StockPlannerNameValue}", "&nbsp;");

                    htmlBody = htmlBody.Replace("{StockPlanner}", UIUtility.getGlobalResourceValue("StockPlanner", language));

                    if (!string.IsNullOrEmpty(poMain.StockPlannerContactNo))
                        htmlBody = htmlBody.Replace("{StockPlannerNumberValue}", poMain.StockPlannerContactNo);
                    else
                    {
                        htmlBody = htmlBody.Replace("(StockPlannerNumberValue}", "&nbsp;");
                        htmlBody = htmlBody.Replace("({StockPlannerNumberValue})", "&nbsp;");
                    }



                    htmlBody = htmlBody.Replace("{PurchaseOrder}", UIUtility.getGlobalResourceValue("PurchaseOrder", language));
                    htmlBody = htmlBody.Replace("{PurchaseOrderValue}", poMain.Purchase_order);

                    htmlBody = htmlBody.Replace("{TOInCaps}", UIUtility.getGlobalResourceValue("TOInCaps", language));
                    htmlBody = htmlBody.Replace("{DELIVERTOInCaps}", UIUtility.getGlobalResourceValue("DELIVERTOInCaps", language));
                    htmlBody = htmlBody.Replace("{INVOICETOInCaps}", UIUtility.getGlobalResourceValue("INVOICETOInCaps", language));

                    var supplierAddress = string.Empty;
                    //supplierAddress = string.Format("{0}<br />{1}<br />{2}, {3} {4}", poMain.Vendor.address1, poMain.Vendor.address2, 
                    //    poMain.Vendor.city, poMain.Vendor.county, poMain.Vendor.VMPPIN);

                    if (!string.IsNullOrEmpty(poMain.Vendor.VendorName) && !string.IsNullOrWhiteSpace(poMain.Vendor.VendorName))
                        supplierAddress = string.Format("{0}", poMain.Vendor.VendorName);

                    if (!string.IsNullOrEmpty(poMain.Vendor.address1) && !string.IsNullOrWhiteSpace(poMain.Vendor.address1))
                        supplierAddress = string.Format("{0},<br />{1}", supplierAddress, poMain.Vendor.address1);


                    //if (!string.IsNullOrEmpty(poMain.Vendor.address1) && !string.IsNullOrWhiteSpace(poMain.Vendor.address1))
                    //    supplierAddress = string.Format("{0}", poMain.Vendor.address1);

                    if (!string.IsNullOrEmpty(poMain.Vendor.address2) && !string.IsNullOrWhiteSpace(poMain.Vendor.address2))
                        supplierAddress = string.Format("{0},<br />{1}", supplierAddress, poMain.Vendor.address2);

                    if (!string.IsNullOrEmpty(poMain.Vendor.city) && !string.IsNullOrWhiteSpace(poMain.Vendor.city))
                        supplierAddress = string.Format("{0},<br />{1}", supplierAddress, poMain.Vendor.city);

                    if (!string.IsNullOrEmpty(poMain.Vendor.county) && !string.IsNullOrWhiteSpace(poMain.Vendor.county))
                        supplierAddress = string.Format("{0},<br />{1} {2}", supplierAddress, poMain.Vendor.county, poMain.Vendor.VMPPIN);

                    htmlBody = htmlBody.Replace("{SupplierAddressValue}", supplierAddress);

                    var odDeliveryAddress = string.Empty;
                    //odDeliveryAddress = string.Format("{0}<br />{1}<br />{2}, {3}<br />{4}, {5}<br />{6}, {7}", poMain.Site.SiteAddressLine1, 
                    //    poMain.Site.SiteAddressLine2, poMain.Site.SiteAddressLine3, poMain.Site.SiteAddressLine4, poMain.Site.SiteAddressLine5, 
                    //    poMain.Site.SiteAddressLine6, poMain.Site.SiteCountryName, poMain.Site.SitePincode);

                    if (!string.IsNullOrEmpty(poMain.Site.SiteAddressLine1) && !string.IsNullOrWhiteSpace(poMain.Site.SiteAddressLine1))
                        odDeliveryAddress = string.Format("{0}", poMain.Site.SiteAddressLine1);

                    if (!string.IsNullOrEmpty(poMain.Site.SiteAddressLine2) && !string.IsNullOrWhiteSpace(poMain.Site.SiteAddressLine2))
                        odDeliveryAddress = string.Format("{0},<br />{1}", odDeliveryAddress, poMain.Site.SiteAddressLine2);

                    if (!string.IsNullOrEmpty(poMain.Site.SiteAddressLine3) && !string.IsNullOrWhiteSpace(poMain.Site.SiteAddressLine3))
                        odDeliveryAddress = string.Format("{0},<br />{1}", odDeliveryAddress, poMain.Site.SiteAddressLine3);

                    if (!string.IsNullOrEmpty(poMain.Site.SiteAddressLine4) && !string.IsNullOrWhiteSpace(poMain.Site.SiteAddressLine4))
                        odDeliveryAddress = string.Format("{0},<br />{1}", odDeliveryAddress, poMain.Site.SiteAddressLine4);

                    if (!string.IsNullOrEmpty(poMain.Site.SiteAddressLine5) && !string.IsNullOrWhiteSpace(poMain.Site.SiteAddressLine5))
                        odDeliveryAddress = string.Format("{0},<br />{1}", odDeliveryAddress, poMain.Site.SiteAddressLine5);

                    if (!string.IsNullOrEmpty(poMain.Site.SiteAddressLine6) && !string.IsNullOrWhiteSpace(poMain.Site.SiteAddressLine6))
                        odDeliveryAddress = string.Format("{0},<br />{1}", odDeliveryAddress, poMain.Site.SiteAddressLine6);

                    if (!string.IsNullOrEmpty(poMain.Site.SiteCountryName) && !string.IsNullOrWhiteSpace(poMain.Site.SiteCountryName))
                        odDeliveryAddress = string.Format("{0},<br />{1} {2}", odDeliveryAddress, poMain.Site.SiteCountryName, poMain.Site.SitePincode);

                    htmlBody = htmlBody.Replace("{OfficeDepotDeliveryAddressValue}", odDeliveryAddress);

                    var apAddress = string.Empty;
                    //apAddress = string.Format("{0}<br />{1}<br />{2}, {3}<br />{4}, {5}<br />{6}, {7}", poMain.Site.APAddressLine1,
                    //    poMain.Site.APAddressLine2, poMain.Site.APAddressLine3, poMain.Site.APAddressLine4, poMain.Site.APAddressLine5,
                    //    poMain.Site.APAddressLine6, poMain.Site.APCountry, poMain.Site.APPincode);

                    if (!string.IsNullOrEmpty(poMain.Site.APAddressLine1) && !string.IsNullOrWhiteSpace(poMain.Site.APAddressLine1))
                        apAddress = string.Format("{0}", poMain.Site.APAddressLine1);

                    if (!string.IsNullOrEmpty(poMain.Site.APAddressLine2) && !string.IsNullOrWhiteSpace(poMain.Site.APAddressLine2))
                        apAddress = string.Format("{0},<br />{1}", apAddress, poMain.Site.APAddressLine2);

                    if (!string.IsNullOrEmpty(poMain.Site.APAddressLine3) && !string.IsNullOrWhiteSpace(poMain.Site.APAddressLine3))
                        apAddress = string.Format("{0},<br />{1}", apAddress, poMain.Site.APAddressLine3);

                    if (!string.IsNullOrEmpty(poMain.Site.APAddressLine4) && !string.IsNullOrWhiteSpace(poMain.Site.APAddressLine4))
                        apAddress = string.Format("{0},<br />{1}", apAddress, poMain.Site.APAddressLine4);

                    if (!string.IsNullOrEmpty(poMain.Site.APAddressLine5) && !string.IsNullOrWhiteSpace(poMain.Site.APAddressLine5))
                        apAddress = string.Format("{0},<br />{1}", apAddress, poMain.Site.APAddressLine5);

                    if (!string.IsNullOrEmpty(poMain.Site.APAddressLine6) && !string.IsNullOrWhiteSpace(poMain.Site.APAddressLine6))
                        apAddress = string.Format("{0},<br />{1}", apAddress, poMain.Site.APAddressLine6);

                    if (!string.IsNullOrEmpty(poMain.Site.APCountry) && !string.IsNullOrWhiteSpace(poMain.Site.APCountry))
                        apAddress = string.Format("{0},<br />{1} {2}", apAddress, poMain.Site.APCountry, poMain.Site.APPincode);

                    htmlBody = htmlBody.Replace("{APLocationLinkedWithDeliverySiteValue}", apAddress);

                    htmlBody = htmlBody.Replace("{FORATTENTIONOFInCaps}", UIUtility.getGlobalResourceValue("FORATTENTIONOFInCaps", language));
                    htmlBody = htmlBody.Replace("{BUYERInCaps}", UIUtility.getGlobalResourceValue("BUYERInCaps", language));
                    htmlBody = htmlBody.Replace("{PURCHASEORDERNUMBERInCaps}", UIUtility.getGlobalResourceValue("PURCHASEORDERNUMBERInCaps", language));
                    htmlBody = htmlBody.Replace("{DATEORDERWASRAISEDInCaps}", UIUtility.getGlobalResourceValue("DATEORDERWASRAISEDInCaps", language));
                    htmlBody = htmlBody.Replace("{PurchaseOrderLetter4}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter4", language));

                    if (!string.IsNullOrEmpty(poMain.StockPlannerContactNo))
                        htmlBody = htmlBody.Replace("{StockPlannerNumberValue}", poMain.StockPlannerContactNo);
                    else
                        htmlBody = htmlBody.Replace("{StockPlannerNumberValue}", "&nbsp;");

                    htmlBody = htmlBody.Replace("{PurchaseOrderValue}", poMain.Purchase_order);

                    DateTime orderRaised = Convert.ToDateTime(poMain.Order_raised);
                    htmlBody = htmlBody.Replace("{PODDMonthYearValue}", String.Format("{0:dd MMMMM yyyy}", orderRaised));

                    htmlBody = htmlBody.Replace("{LINEInCaps}", UIUtility.getGlobalResourceValue("LINEInCaps", language));
                    htmlBody = htmlBody.Replace("{VIKINGCODEInCaps}", UIUtility.getGlobalResourceValue("VIKINGCODEInCaps", language));
                    htmlBody = htmlBody.Replace("{OFFICEDEPOTCODEInCaps}", UIUtility.getGlobalResourceValue("OFFICEDEPOTCODEInCaps", language));
                    htmlBody = htmlBody.Replace("{VENDORCODEInCaps}", UIUtility.getGlobalResourceValue("VENDORCODEInCaps", language));
                    htmlBody = htmlBody.Replace("{ITEMDESCRIPTIONInCaps}", UIUtility.getGlobalResourceValue("ITEMDESCRIPTIONInCaps", language));
                    htmlBody = htmlBody.Replace("{UNITSREQUIREDInCaps}", UIUtility.getGlobalResourceValue("OriginalQty", language));
                    htmlBody = htmlBody.Replace("{OutStandingQtyInCaps}", UIUtility.getGlobalResourceValue("OutstandingQty", language));
                    htmlBody = htmlBody.Replace("{UNITInCaps}", UIUtility.getGlobalResourceValue("UNITInCaps", language));
                    htmlBody = htmlBody.Replace("{UNITCOSTInCaps}", UIUtility.getGlobalResourceValue("UNITCOSTInCaps", language));
                    htmlBody = htmlBody.Replace("{TOTALCOSTInCaps}", UIUtility.getGlobalResourceValue("TOTALCOSTInCaps", language));
                    htmlBody = htmlBody.Replace("{DELIVERYDATEInCaps}", UIUtility.getGlobalResourceValue("DELIVERYDATEInCaps", language));

                    htmlBody = htmlBody.Replace("{POLineItemsDynamicTRs}", poLineItemsDynamicTRs);

                    htmlBody = htmlBody.Replace("{TOTALCOSTInCaps}", UIUtility.getGlobalResourceValue("TOTALCOSTInCaps", language));
                    htmlBody = htmlBody.Replace("{CURRENCYInCaps}", UIUtility.getGlobalResourceValue("CURRENCYInCaps", language));
                    htmlBody = htmlBody.Replace("{IMPORTANTNOTICEInCaps}", UIUtility.getGlobalResourceValue("IMPORTANTNOTICEInCaps", language));
                    htmlBody = htmlBody.Replace("{PurchaseOrderLetter5}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter5", language));
                    htmlBody = htmlBody.Replace("{PurchaseOrderLetter6}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter6", language));
                    htmlBody = htmlBody.Replace("{PurchaseOrderLetter7}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter7", language));
                    htmlBody = htmlBody.Replace("{PurchaseOrderLetter8}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter8", language));
                    htmlBody = htmlBody.Replace("{TOTALInCaps}", UIUtility.getGlobalResourceValue("TOTALInCaps", language));

                    htmlBody = htmlBody.Replace("{TotalValue}", Convert.ToString(poTotalCost));
                    htmlBody = htmlBody.Replace("{CurrencyValue}", currency);

                    htmlBody = htmlBody.Replace("{CONDITIONSOFPURCHASEInCaps}", UIUtility.getGlobalResourceValue("CONDITIONSOFPURCHASEInCaps", language));
                    htmlBody = htmlBody.Replace("{PurchaseOrderLetter9}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter9", language));
                    htmlBody = htmlBody.Replace("{PurchaseOrderLetter10}", UIUtility.getGlobalResourceValue("PurchaseOrderLetter10", language));

                }
            }
            return htmlBody;
        }

        #endregion


        public void GetEmailTemplateDataCancelSlot()
        {
            string language = string.Empty;
            string DynamicTRs = string.Empty;
            string emailAddress = string.Empty;
            string VendorIDs = string.Empty;
            string CarrierIDs = string.Empty;
            string StockPlannerIDs = string.Empty;
            bool IsDtNull = false;
            try
            {
               
                DataTable dt = new DataTable();
                DataTable DTMainMaintainvalues = new DataTable();
                dt = GetCancelSlotDetails("GetCancelSlotDetails", null, null, null);
                DTMainMaintainvalues = dt;
                if (dt.Rows.Count > 0)
                {
                    IsDtNull = true;
                    var distinctIds = dt.AsEnumerable()
                           .Select(s => new
                           {
                               id = s.Field<int>("ID"),
                               SupplierType = s.Field<string>("ScheduleType"),
                               Canceldate = s.Field<string>("CancelDate")
                           })
                           .Distinct().ToList();

                    foreach (var item in distinctIds)
                    {
                        string canceldate = item.Canceldate;
                        dt = DTMainMaintainvalues.AsEnumerable().Where(x => x.Field<int>("ID") == item.id && x.Field<string>("CancelDate") == item.Canceldate).CopyToDataTable();

                        DataTable dtFiltered = new DataTable();
                        dtFiltered = dt;
                                         
                        if (item.SupplierType == "V")
                        {

                            dt = null;
                            dt = GetCancelSlotDetails("getVendorEmailIDCancelSlot", Convert.ToInt32(item.id), null, null);
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    language = dt.Rows[i]["Language"].ToString();
                                    emailAddress = dt.Rows[i]["EmailID"].ToString();

                                    StringBuilder sb = new StringBuilder();  
                                    sb.Append("<table width=55% style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=90%'>");
                                    sb.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='30%'>" + UIUtility.getGlobalResourceValue("Site", language) + "</cc1:ucLabel></td><td width='30%'>" + UIUtility.getGlobalResourceValue("Date", language) + "</td><td width='30%'>" + UIUtility.getGlobalResourceValue("TimeArrival", language) + "</td></tr>");
                                    for (int j = 0; j < dtFiltered.Rows.Count; j++)
                                    {
                                        sb.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + dtFiltered.Rows[j]["Site"].ToString() + "</td><td>" + dtFiltered.Rows[j]["Canceldate"].ToString() + "</td><td>" + dtFiltered.Rows[j]["Time"].ToString() + "</td></tr>");

                                    }
                                    sb.Append("</table>");
                                    DynamicTRs = sb.ToString();
                                    SendEmailtoVendor(language, emailAddress, DynamicTRs, item.Canceldate);
                                }
                                VendorIDs += item.id + " , ";
                            }
                        }
                        else
                        {
                            dt = null;
                            dt = GetCancelSlotDetails("getCarrierEmailIDCancelSlot", null, Convert.ToInt32(item.id), null);
                            
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    language = dt.Rows[i]["Language"].ToString();
                                    emailAddress = dt.Rows[i]["EmailID"].ToString();

                                    StringBuilder sb = new StringBuilder();
                                    sb.Append("<table width=55% style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=90%'>");
                                    sb.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='30%'>" + UIUtility.getGlobalResourceValue("Site", language) + "</cc1:ucLabel></td><td width='30%'>" + UIUtility.getGlobalResourceValue("Date", language) + "</td><td width='30%'>" + UIUtility.getGlobalResourceValue("TimeArrival", language) + "</td></tr>");
                                    for (int j = 0; j < dtFiltered.Rows.Count; j++)
                                    {
                                        sb.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td>" + dtFiltered.Rows[j]["Site"].ToString() + "</td><td>" + dtFiltered.Rows[j]["Canceldate"].ToString() + "</td><td>" + dtFiltered.Rows[j]["Time"].ToString() + "</td></tr>");

                                    }
                                    sb.Append("</table>");
                                    DynamicTRs = sb.ToString();
                                    SendEmailtoVendor(language, emailAddress, DynamicTRs, item.Canceldate);
                                }
                                CarrierIDs += item.id + " , ";
                            }
                        }

                    }
                    sbMessage.Clear();
                    sbMessage.Append("\r\n");
                    sbMessage.Append("Soft Cancel slot mail sent for VendorIDs " + VendorIDs + "\r\n");
                    sbMessage.Append("Soft Cancel slot mail sent for CarrierIDs " + CarrierIDs + "\r\n");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                }
               // dt = GetCancelSlotDetails("GetCancelSlotDetails", null, null, null);
                if (DTMainMaintainvalues.Rows.Count > 0)
                {
                    var distinctIds = DTMainMaintainvalues.AsEnumerable()
                               .Select(s => new
                               {
                                   id = s.Field<int>("SiteID"),
                                   Canceldate = s.Field<string>("CancelDate"),
                                   Site = s.Field<string>("Site")
                               })
                               .Distinct().ToList();
                    foreach (var item in distinctIds)
                    {
                        string CancelDate = item.Canceldate;
                        string sitename = string.Empty;
                        sitename = item.Site;
                        DynamicTRs = string.Empty;
                        dt = DTMainMaintainvalues.AsEnumerable().Where(x => x.Field<int>("SiteID") == item.id && x.Field<string>("CancelDate") == item.Canceldate).CopyToDataTable();
                        DataTable dtFiltered = new DataTable();
                        dtFiltered = dt;
                        dt = null;
                        dt = GetCancelSlotDetails("getSPEmailIDCancelSlot", null, null, Convert.ToInt32(item.id));
                       
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                language = dt.Rows[i]["Language"].ToString();
                                emailAddress = dt.Rows[i]["EmailID"].ToString();                                
                                StringBuilder sb = new StringBuilder();
                                sb.Append("<table width=55% style='background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #DADADA;cellspacing=0 cellpadding=0 border=0 border-collapse:collapse;width=100%'>");
                                sb.Append("<tr style='color:#934500;border-bottom: 1px solid #E8E8E8;font-weight: bold;font-family:Arial ;font-size:12;vertical-align: top;'><td width='40%'>" + UIUtility.getGlobalResourceValue("Vendor/Carrier", language) + "</cc1:ucLabel></td><td width='15%'>" + UIUtility.getGlobalResourceValue("TimeArrival", language) + "</td><td width='15%'>" + UIUtility.getGlobalResourceValue("Plts", language) + "</td><td width='15%'>" + UIUtility.getGlobalResourceValue("Ctns", language) + "</td><td width='15%'>" + UIUtility.getGlobalResourceValue("Lns", language) + "</td></tr>");
                                for (int j = 0; j < dtFiltered.Rows.Count; j++)
                                {
                                    sb.Append("<tr style='background-color: #F7F6F3;font-size:12;font-family:Arial ;'><td >" + dtFiltered.Rows[j]["Name"].ToString() + "</td><td>" + dtFiltered.Rows[j]["Time"].ToString() + "</td><td>" + dtFiltered.Rows[j]["MaximumPallets"].ToString() + "</td><td>" + dtFiltered.Rows[j]["MaximumCatrons"].ToString() + "</td><td>" + dtFiltered.Rows[j]["MaximumLines"].ToString() + "</td></tr>");                                   
                                }
                                sb.Append("</table>");
                                DynamicTRs = sb.ToString();
                                StockPlannerIDs = dt.Rows[i]["StockPlannerID"] + " ,";
                                SendEmailtoStockPlanner(language, emailAddress, DynamicTRs, CancelDate, sitename);                                
                            }
                        }
                    }

                    sbMessage.Clear();
                    sbMessage.Append("\r\n");
                    sbMessage.Append("Soft Cancel slot mail sent for StockPlannerIDs " + StockPlannerIDs + "\r\n");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                }
            }
            catch (Exception ex)
            {

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Error Soft Cancel slot. " + ex.Message + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Soft Cancel slot mail sent for VendorIDs " + VendorIDs + "\r\n");
                sbMessage.Append("Soft Cancel slot mail sent for CarrierIDs " + CarrierIDs + "\r\n");
                sbMessage.Append("\r\n");
                sbMessage.Append("Soft Cancel slot mail sent for StockPlannerIDs " + StockPlannerIDs + "\r\n");
                sbMessage.Append("\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            finally
            {
                if (IsDtNull == true)
                {
                    GetCancelSlotDetails("UpdateMailSentCancelSlotRecord", null, null, null, null, VendorIDs, CarrierIDs, StockPlannerIDs);
                }
            }
        }
        public void SendEmailtoVendor(string language, string EmailAddress, string DynamicTRs,string CancelDate)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
            path = path.Replace(@"\bin\debug", "");
            string templatesPath = path + @"emailtemplates\";
            string templateFile = templatesPath + @"\VendorCancelLetter.english.htm";

            string htmlBody = string.Empty;
            if (System.IO.File.Exists(templateFile.ToLower()))
            {
                using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                {
                    htmlBody = sReader.ReadToEnd();
                    htmlBody = htmlBody.Replace("{DearSirMadam}", UIUtility.getGlobalResourceValue("DearSirMadam", language));
                    htmlBody = htmlBody.Replace("{VendorCancelSlotMesg1}", UIUtility.getGlobalResourceValue("VendorCancelSlotMesg1", language).Replace("dd/mm/yyyy", CancelDate));
                    htmlBody = htmlBody.Replace("{VendorCancelSlotMesg2}", UIUtility.getGlobalResourceValue("VendorCancelSlotMesg2", language));
                    htmlBody = htmlBody.Replace("{VendorCancelSlotMesg3}", DynamicTRs);
                    htmlBody = htmlBody.Replace("{ManyThanksNew}", UIUtility.getGlobalResourceValue("ManyThanksNew", language));

                }
            }
            this.SendMail(EmailAddress, htmlBody, UIUtility.getGlobalResourceValue("CancelSlotSubject", language), Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]), true);
        }
        public void SendEmailtoStockPlanner(string language, string EmailAddress, string DynamicTRs, string CancelDate, string SiteName)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory.ToLower();
            path = path.Replace(@"\bin\debug", "");
            string templatesPath = path + @"emailtemplates\";
            string templateFile = templatesPath + @"\StockPlannerCancelLetter.english.htm";

            string htmlBody = string.Empty;
            if (System.IO.File.Exists(templateFile.ToLower()))
            {
                using (StreamReader sReader = new StreamReader(templateFile.ToLower()))
                {
                    htmlBody = sReader.ReadToEnd();                  
                    htmlBody = htmlBody.Replace("{Site}", UIUtility.getGlobalResourceValue("Site", language));
                    htmlBody = htmlBody.Replace("{SiteValue}", SiteName);
                    htmlBody = htmlBody.Replace("{Date}", UIUtility.getGlobalResourceValue("Date", language));
                    htmlBody = htmlBody.Replace("{DateValue}", CancelDate);                    
                    htmlBody = htmlBody.Replace("{StockPlannerCancelSlotMesg1}", DynamicTRs);
                    htmlBody = htmlBody.Replace("{StockPlannerCancelSlotMesg2}", UIUtility.getGlobalResourceValue("StockPlannerCancelSlotMesg1", language));
                    htmlBody = htmlBody.Replace("{ManyThanksNew}", UIUtility.getGlobalResourceValue("ManyThanksNew", language));
                    htmlBody = htmlBody.Replace("{VIP Admin}", UIUtility.getGlobalResourceValue("VIPAdmin", language));
                }
            }
            this.SendMail(EmailAddress, htmlBody, UIUtility.getGlobalResourceValue("CancelSlotSubject", language), Convert.ToString(ConfigurationManager.AppSettings["fromAddress"]), true);
        }
        public DataTable GetCancelSlotDetails(string Action, int? vendorID, int? CarrierID, int? SiteID, string canceldate = "",string vendorIDs="",string CarrierIds="",string SPIds="")
         {
             var dataImportSchedulerBE = new UP_DataImportSchedulerBE();
             var dataImportSchedulerBAL = new UP_DataImportSchedulerBAL();
             dataImportSchedulerBE.Action = Action;
             dataImportSchedulerBE.CarrierID = CarrierID;
             dataImportSchedulerBE.VendorID = vendorID;
             dataImportSchedulerBE.SiteID = SiteID;
             dataImportSchedulerBE.CarrierIDs = CarrierIds;
             dataImportSchedulerBE.VendorIDs = vendorIDs;
             dataImportSchedulerBE.StockPlannerIDs = SPIds;
             return dataImportSchedulerBAL.dtGetCancelSlotInformationBAL(dataImportSchedulerBE);
         }


    }
}
﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Utilities;
using VIP_DataImportScheduler;

namespace VendorInPartnership_DataImportScheduler
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            StringBuilder sbMessage = new StringBuilder();
            try
            {
                GlobalObjects.lstLanguageTags = UIUtility.GetAllResources();
                clsCommunication communication = new clsCommunication();


                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Soft Cancel slot. " + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                communication.GetEmailTemplateDataCancelSlot();

                Process[] pname = Process.GetProcessesByName("VendorInPartnership_DataImportScheduler");
                if (pname.Length == 2)
                {
                    sbMessage.Clear();
                    sbMessage.Append("\r\n");
                    sbMessage.Append("Import Process is already Running. Please wait for few minutes.");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                    AutoClosingMessageBox.Show("Import Process is already Running. Please wait for few minutes.", "Information", 2000);
                    return;
                }

                DirectoryInfo di = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + @"\VIP_DATA\");
                DirectoryInfo[] dirs = di.GetDirectories("*", SearchOption.TopDirectoryOnly);
                var DirectoryArray = dirs.OrderBy(m => m.Name).ToList();
                int iDirCount = DirectoryArray.Count;

                if (iDirCount == 0)
                {
                    sbMessage.Clear();
                    sbMessage.Append("\r\n");
                    sbMessage.Append("Folder not found " + DateTime.Now.ToString() + "\r\n");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                    return;
                }

                String checkImportStatus = Common.CheckImportStatus();
                if (checkImportStatus == "True")
                {
                    sbMessage.Clear();
                    sbMessage.Append("\r\n");
                    sbMessage.Append("Manual Upload is Running.. Please wait for few minutes");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                    return;
                }

                String dsDataImportRunningStatus = Common.CheckIsDataImportRunning();

                if (dsDataImportRunningStatus == "True")
                {
                    sbMessage.Clear();
                    sbMessage.Append("\r\n");
                    sbMessage.Append("Manual Upload is Running.. Please wait for few minutes");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                    return;
                }

                for (int iCount = 0; iCount < DirectoryArray.Count; iCount++)
                {
                    sbMessage.Clear();
                    sbMessage.Append("\r\n");
                    sbMessage.Append(string.Format("Folder {0} in process. {1} \r\n", DirectoryArray[iCount].ToString(),
                        DateTime.Now.ToString()));
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);

                    UploadData(DirectoryArray[iCount].ToString());
                }

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Import Done." + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);


                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("***Start OTIF syncronization***" + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "spOTIF_DetailReprotScheduler";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;
                GlobalVariable.sqlComm.ExecuteNonQuery();

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("***Finished OTIF syncronization***" + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("***Finally reset import status start***" + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                String updateImportStatus = Common.UpdateImportStatus();

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Import status has been reset. " + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                // clsCommunication communication = new clsCommunication();
                /* Begin : Email logic by Jai Date : 11/Sep/2014 */
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Sending import email status. " + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                communication.SendEmailForImportSchedulerStatus();

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Email has been sent successfully . " + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End : Email logic by Jai Date : 11/Sep/2014 */

                /* Begin : Logic to send emails and save data for new imported Purchase Orders : 13/Nov/2014 */
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Saving & Sending new imported PO email data. " + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                communication.SendAndSaveNewPOInformation();

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("New Imported PO's Email has been sent & saved successfully . " + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
                /* End : Logic to send emails and save data for new imported Purchase Orders : 13/Nov/2014 */


                var IsShareDrive = ConfigurationManager.AppSettings["IsShareDrive"].ToString();

                if (IsShareDrive == "1")
                {
                    sbMessage.Clear();
                    sbMessage.Append("\r\n");
                    sbMessage.Append("Started Copying file from Current Local Drive for France successfully . " + DateTime.Now.ToString() + "\r\n");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);

                    try
                    {
                        CopyFileSharedDrive();
                    }
                    catch (Exception ex)
                    {
                        sbMessage.Clear();
                        sbMessage.Append(ex.Message + "\r\n" + ex.Source + "\r\n" + ex.InnerException);
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                    }
                    sbMessage.Clear();
                    sbMessage.Append("\r\n");
                    sbMessage.Append("Copied file from Current Local Drive for France successfully . " + DateTime.Now.ToString() + "\r\n");
                    sbMessage.Append("\r\n");

                    LogUtility.SaveTraceLogEntry(sbMessage);
                }
                if (true)
                {
                    sbMessage.Clear();
                    sbMessage.Append("\r\n");
                    sbMessage.Append("RunSchedulerForDiscrepancyCount " + DateTime.Now.ToString() + "\r\n");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);

                    try
                    {

                        //dicrepancy goodsin open scheduler
                        DiscrepancyCountStatusScheduler.RunSchedulerForDiscrepancyCount();

                    }
                    catch (Exception ex)
                    {
                        sbMessage.Clear();
                        sbMessage.Append(ex.Message + "\r\n" + ex.Source + "\r\n" + ex.InnerException);
                        sbMessage.Append("\r\n");
                        LogUtility.SaveTraceLogEntry(sbMessage);
                    }
                    sbMessage.Clear();
                    sbMessage.Append("\r\n");
                    sbMessage.Append("RunSchedulerForDiscrepancyCount " + DateTime.Now.ToString() + "\r\n");
                    sbMessage.Append("\r\n");

                    LogUtility.SaveTraceLogEntry(sbMessage);
                }

            }
            catch (Exception ex)
            {
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Scheduler Terminated at " + DateTime.Now.ToString() + "\r\n" + ex.Message);
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            finally
            {
                GlobalVariable.sqlCon.Close();
            }
        }
        static void UploadData(string FolderName)
        {
            StringBuilder sbMessage = new StringBuilder();
            try
            {
                sbMessage.Clear();
                sbMessage.Append("Started at            : " + DateTime.Now.ToString() + " - Data Folder -" + FolderName);
                sbMessage.Append("\r\n");
                sbMessage.Append("****************************************************************************************");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                string argValue = "";

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                sbMessage.Clear();
                sbMessage.Append("Step 1 Environment Variable Set");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                argValue = FolderName;

                if ((argValue.Length != 8)
                    || (Convert.ToInt32(argValue.Substring(0, 4)) < 2010)
                    || (Convert.ToInt32(argValue.Substring(0, 4)) > 2099)
                    || (Convert.ToInt32(argValue.Substring(4, 2)) < 1)
                    || (Convert.ToInt32(argValue.Substring(4, 2)) > 12)
                    || (Convert.ToInt32(argValue.Substring(6, 2)) < 1)
                    || (Convert.ToInt32(argValue.Substring(6, 2)) > 31))
                {

                    sbMessage.Clear();

                    sbMessage.Append("\r\n");
                    sbMessage.Append("Invalid configuration date value. Please specify date value in YYYYMMDD format");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                    return;
                }

                sbMessage.Clear();
                sbMessage.Append("Step 2 File naming convention checking done.");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                GlobalVariable.folderName = argValue;
                GlobalVariable.InvokedBy = "System";
                GlobalVariable.SourceFilePath = AppDomain.CurrentDomain.BaseDirectory + @"\VIP_DATA\" + GlobalVariable.folderName;
                GlobalVariable.folderName102 = GlobalVariable.folderName.Substring(0, 4) + "/" + GlobalVariable.folderName.Substring(4, 2) + "/" + GlobalVariable.folderName.Substring(6, 2);
                GlobalVariable.TimeSpan = DateTime.Now.ToString("HH-mm");

                DateTime FolderDate = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2))).Date;

                if (FolderDate > DateTime.Now.Date)
                {
                    sbMessage.Clear();

                    sbMessage.Append("\r\n");
                    sbMessage.Append("Invalid configuration date value. Please specified date value should not be greater then today's date.");
                    sbMessage.Append("\r\n");
                    LogUtility.SaveTraceLogEntry(sbMessage);
                    return;
                }

                bool isForcastFileExists = false;
                DirectoryInfo di = new DirectoryInfo(GlobalVariable.SourceFilePath);
                foreach (FileInfo fi in di.GetFiles("*.txt"))
                {
                    if (fi.Name.ToLower().Contains("forecastedsales"))
                    {
                        isForcastFileExists = true;
                        break;
                    }
                }

                sbMessage.Clear();
                sbMessage.Append("Step 3 Global variables set done.");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                //Delete All records from Staging Tables
                Common.Initialize_Staging();

                sbMessage.Clear();
                sbMessage.Append("Step 3.1 Deleted all records done.");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                //Insert into Scheduler table
                GlobalVariable.dataImportSchedulerID = Convert.ToInt32(Common.InsertUP_Scheduler());


                sbMessage.Clear();
                sbMessage.Append("Step 3.2 Scheduler Id set.");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                //Insert into Folder table
                GlobalVariable.folderDownloadedID = Convert.ToInt32(Common.InsertUP_FolderDownload(GlobalVariable.dataImportSchedulerID.Value, GlobalVariable.folderName));

                sbMessage.Clear();
                sbMessage.Append("Step 3.3 Download Id set.");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                sbMessage.Clear();
                sbMessage.Append("Step 4 Started.");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                //Place all records in staging tables
                UploadFilesToDatabase.UploadFiles();

                sbMessage.Clear();
                sbMessage.Append("Step 13 All files uploaded in Staging tables.  " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                //Validate and move data main tables
                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "spDataImport";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.Parameters.Add("@FolderDownloadID", SqlDbType.Int).Value = GlobalVariable.folderDownloadedID.Value;
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;
                GlobalVariable.sqlComm.Connection.InfoMessage += Connection_InfoMessage;
                GlobalVariable.sqlComm.ExecuteNonQuery();

                sbMessage.Clear();
                sbMessage.Append("Step 13-b spDataImport executed successfully.  " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                sbMessage.Clear();
                sbMessage.Append("Step 14 Update Expedite Caluculated Details.  " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);


                //Validate and move data main tables
                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "spExpediteCaluculatedDetails";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;
                GlobalVariable.sqlComm.ExecuteNonQuery();

                /* For week spExpediteCaluculatedDetailsForWeek 
                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "spExpediteCaluculatedDetailsForWeek";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;
                GlobalVariable.sqlComm.ExecuteNonQuery(); */


                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Step 14 A Forcasted Background Thread started. " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
                sbMessage.Clear();

                LaunchForcastAccuracyStarter();

                sbMessage.Append("\r\n");
                sbMessage.Append("Step 14 A Forcasted  Background Thread end. " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);


                sbMessage.Clear();
                sbMessage.Append("Step 15 Data Import completed. " + DateTime.Now.ToString());
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);


                //-------------Insert data in VIP management report table-----------------------
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Step 16 Start Updation on VIP Management Report Data***" + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "spMAS_VIPManagementReportInsert";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.Parameters.Add("@CalcultionDate", SqlDbType.Date).Value = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2))).Date;
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;
                GlobalVariable.sqlComm.ExecuteScalar();

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("***Finished Updation on VIP Management Report Data***" + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
                //--------------------------------------------------------------------------------


                //-------------Insert data in POC tracker report table-----------------------
                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("Step 17 Start Updation on POC tracker report table***" + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "spRPT_POCTrackerReportInsert";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.Parameters.Add("@CalcultionDate", SqlDbType.Date).Value = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2))).Date;
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;
                GlobalVariable.sqlComm.ExecuteScalar();

                sbMessage.Clear();
                sbMessage.Append("\r\n");
                sbMessage.Append("***Finished Updation on POC tracker report table***" + DateTime.Now.ToString() + "\r\n");
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);
                //--------------------------------------------------------------------------------

            }
            catch (Exception ex)
            {
                sbMessage.Clear();
                sbMessage.Append(ex.Message + "\r\n" + ex.Source + "\r\n" + ex.InnerException);
                sbMessage.Append("\r\n");
                LogUtility.SaveTraceLogEntry(sbMessage);

                GlobalVariable.sqlComm.CommandType = CommandType.Text;
                Common.InsertUP_ErrorLog("Scheduler Terminated at " + DateTime.Now.ToString() + "\r\n" + ex.Message, GlobalVariable.dataImportSchedulerID.Value);

            }
            finally
            {
                GlobalVariable.sqlComm.CommandType = CommandType.Text;
                Common.UpdateUP_Scheduler(GlobalVariable.dataImportSchedulerID.Value);
                Thread.Sleep(3000);
            }

            sbMessage.Clear();
            sbMessage.Append("Ended at              : " + DateTime.Now.ToString());
            sbMessage.Append("\r\n");
            sbMessage.Append("****************************************************************************************");
            sbMessage.Append("\r\n");
            LogUtility.SaveTraceLogEntry(sbMessage);
        }
        static void LaunchForcastAccuracyStarter()
        {
            ProcessStartInfo pi = new ProcessStartInfo
            {
                FileName = ConfigurationManager.AppSettings["ForcastAccuracyStarter"].ToString()
            };
            Process.Start(pi);
        }
        static void Connection_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(e.Message.ToString());
            LogUtility.SaveTraceLogEntry(sb);
        }

        #region Copy file Drive        
        public static void CopyFileSharedDrive()
        {
            // var path = ConfigurationManager.AppSettings["path"].ToString();
            //var user = ConfigurationManager.AppSettings["user"].ToString();
            //var password = ConfigurationManager.AppSettings["password"].ToString();

            var shareDrive = ConfigurationManager.AppSettings["shareDrive"].ToString();
            var sConn = ConfigurationManager.AppSettings["sConn"].ToString();
            var countryID = ConfigurationManager.AppSettings["countryID"].ToString();

            //NetworkCredential theNetworkCredential = new NetworkCredential(user, password);
            //CredentialCache theNetCache = new CredentialCache();
            //theNetCache.Add(new Uri(shareDrive), "Basic", theNetworkCredential);

            string[] theFolders = Directory.GetDirectories(shareDrive);

            //checking data uploaded 
            SqlConnection conn = new SqlConnection(sConn);

            SqlCommand cmdShow = new SqlCommand
            {
                Connection = conn,
                CommandText = "spCopyFileDrive",
                CommandType = CommandType.StoredProcedure
            };

            cmdShow.Parameters.Add("@Action", SqlDbType.VarChar).Value = "GetShareDate";
            cmdShow.Parameters.Add("@datetime", SqlDbType.DateTime).Value = DateTime.Now;

            DataTable dataSet = new DataTable();
            SqlDataAdapter reader = new SqlDataAdapter(cmdShow);
            reader.Fill(dataSet);

            var count = dataSet.Rows[0][0].ToString();

            if (count == "2")
            {
                SqlCommand cmd = new SqlCommand
                {
                    Connection = conn,
                    CommandText = "spUP_PurchaseOrderDetails",
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add("@Action", SqlDbType.VarChar).Value = "GetOPenPOCountryWise";
                cmd.Parameters.Add("@CountryID", SqlDbType.Int).Value = countryID;

                DataTable dataSet1 = new DataTable();
                SqlDataAdapter reader1 = new SqlDataAdapter(cmd);
                reader1.Fill(dataSet1);

                string path1 = string.Empty;
                string result = GenerateDetailFile(shareDrive, dataSet1, out path1);

                //WebClient request = new WebClient();
                //request.Credentials = new NetworkCredential(user, password);

                //var local = result;
                //var remote = Path.Combine(shareDrive, path1);
                //File.Copy(result, remote, true);

                SqlCommand cmdInsert = new SqlCommand
                {
                    Connection = conn,
                    CommandText = "spCopyFileDrive",
                    CommandType = CommandType.StoredProcedure
                };

                cmdInsert.Parameters.Add("@Action", SqlDbType.VarChar).Value = "InsertShareDate";
                cmdInsert.Parameters.Add("@datetime", SqlDbType.DateTime).Value = DateTime.Now;

                DataTable dataSet2 = new DataTable();
                SqlDataAdapter reader2 = new SqlDataAdapter(cmdInsert);
                reader2.Fill(dataSet2);
            }
        }
        private static string PrintColumnNames(DataTable table)
        {
            // For each DataTable, print the ColumnName.
            string txt = string.Empty;
            foreach (DataColumn column in table.Columns)
            {
                txt += column.ColumnName + "|";
            }
            string str = txt.Remove(txt.Length - 1, 1);
            return str;
        }
        public static string GenerateDetailFile(string filepath, DataTable table, out string path1)
        {
            if (!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);
            }

            DateTime DateTime = DateTime.Now;

            path1 = "Open_Purchase_Order.txt";

            string path = filepath + path1;


            using (StreamWriter sw = File.CreateText(path))
            {
                string strColumn = PrintColumnNames(table);
                sw.WriteLine(strColumn);

                foreach (DataRow row in table.Rows)
                {
                    string txt = string.Empty;

                    foreach (DataColumn column in table.Columns)
                    {
                        txt += row[column.ColumnName].ToString() + "|";
                    }

                    string str = txt.Remove(txt.Length - 1, 1);

                    sw.WriteLine(str);
                }
            }

            return path;
        }
        #endregion
    }

    public class AutoClosingMessageBox
    {
        System.Threading.Timer _timeoutTimer;
        string _caption;
        AutoClosingMessageBox(string text, string caption, int timeout)
        {
            _caption = caption;
            _timeoutTimer = new System.Threading.Timer(OnTimerElapsed,
                null, timeout, System.Threading.Timeout.Infinite);
            MessageBox.Show(text, caption);
        }
        public static void Show(string text, string caption, int timeout)
        {
            new AutoClosingMessageBox(text, caption, timeout);
        }
        void OnTimerElapsed(object state)
        {
            IntPtr mbWnd = FindWindow(null, _caption);
            if (mbWnd != IntPtr.Zero)
                SendMessage(mbWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
            _timeoutTimer.Dispose();
        }
        const int WM_CLOSE = 0x0010;
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
    }
}

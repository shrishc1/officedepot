﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using Utilities;
using System.Data.SqlClient;

namespace VendorInPartnership_DataImportScheduler
{
    class ClsBOImport
    {
        public void UploadBO(string boFilePath, string boFileName)
        {
            DataTable dt = new DataTable();
            DataRow dr;
            int TotalRecordCount = 0;

            //------------------------------------

            DataTable errorDt = new DataTable();
            DataRow errorDr;

            int Direct_SKU_Index = 0;
            int OD_SKU_NO_Index = 0;
            int Region_Area_Index = 0;
            int Site_Index = 0;
            int Date_Index = 0;
            int Daily_Count_Index = 0;
            int Daily_Qty_Index = 0;
            int Daily_ValueIndex = 0;
            int Total_CountIndex = 0;
            int Total_QtyIndex = 0;
            int Total_ValueIndex = 0;

            //-------------------------------------

            string FPID = Common.InsertUP_FileProcessed(GlobalVariable.folderDownloadedID.Value, TotalRecordCount, 0, boFileName);
            int fileProcessedID = Convert.ToInt32(FPID);
            try
            {
                using (StreamReader oStreamReader = new StreamReader(boFilePath, System.Text.Encoding.Default))
                {
                    GlobalVariable.textFileLine = oStreamReader.ReadLine().ToLower();
                    var columns = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);
                    foreach (string col in columns)
                    {
                        switch (col.ToLower())
                        {
                            case "country":
                                dt.Columns.Add(new DataColumn("Region_Area"));
                                Region_Area_Index = Array.IndexOf(columns, "country");
                                break;
                            case "direct_sku":
                                dt.Columns.Add(new DataColumn("Direct_SKU"));
                                Direct_SKU_Index = Array.IndexOf(columns, "direct_sku");
                                break;
                            case "od_sku_no":
                                dt.Columns.Add(new DataColumn("OD_SKU_NO"));
                                OD_SKU_NO_Index = Array.IndexOf(columns, "od_sku_no");
                                break;
                            case "site":
                                dt.Columns.Add(new DataColumn("site"));
                                Site_Index = Array.IndexOf(columns, "site");
                                break;
                            case "date":
                                dt.Columns.Add(new DataColumn("Date"));
                                Date_Index = Array.IndexOf(columns, "date");
                                break;
                            case "daily_count":
                                dt.Columns.Add(new DataColumn("Daily_Count", typeof(System.Decimal)));
                                Daily_Count_Index = Array.IndexOf(columns, "daily_count");
                                break;
                            case "daily_qty":
                                dt.Columns.Add(new DataColumn("Daily_Qty", typeof(System.Decimal)));
                                Daily_Qty_Index = Array.IndexOf(columns, "daily_qty");
                                break;
                            case "daily_value":
                                dt.Columns.Add(new DataColumn("Daily_Value", typeof(System.Decimal)));
                                Daily_ValueIndex = Array.IndexOf(columns, "daily_value");
                                break;
                            case "total_count":
                                dt.Columns.Add(new DataColumn("Total_Count", typeof(System.Int32)));
                                Total_CountIndex = Array.IndexOf(columns, "total_count");
                                break;
                            case "total_qty":
                                dt.Columns.Add(new DataColumn("Total_Qty", typeof(System.Decimal)));
                                Total_QtyIndex = Array.IndexOf(columns, "total_qty");
                                break;
                            case "total_value":
                                dt.Columns.Add(new DataColumn("Total_Value", typeof(System.Decimal)));
                                Total_ValueIndex = Array.IndexOf(columns, "total_value");
                                break;

                            default:
                                break;
                        }
                    }
                    dt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));

                    //--------------------- Added on 14 Feb 2013 -----------------------
                    errorDt.Columns.Add(new DataColumn("FolderDownloadID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ManualFileID", typeof(System.Int32)));
                    errorDt.Columns.Add(new DataColumn("ErrorDescription", typeof(System.String)));
                    errorDt.Columns.Add(new DataColumn("UpdatedDate", typeof(System.DateTime)));
                    //--------------------- Added on 14 Feb 2013 -----------------------

                    int iCount = 0;
                    DateTime? tempDate = DateTime.Now;
                    while ((GlobalVariable.textFileLine = oStreamReader.ReadLine()) != null)
                    {
                        string errorMessage = string.Empty;
                        errorDr = errorDt.NewRow();
                        iCount++;
                        dr = dt.NewRow();
                        GlobalVariable.textFileFieldValues = GlobalVariable.textFileLine.Split(GlobalVariable.delimiters);
                        if (GlobalVariable.textFileFieldValues.Length != dt.Columns.Count - 1)
                        {
                            StringBuilder sbMessage = new StringBuilder();
                            sbMessage.Append("\r\n");
                            sbMessage.Append("Data Incomplete at position " + iCount.ToString());
                            sbMessage.Append("\r\n");
                            LogUtility.SaveTraceLogEntry(sbMessage);
                            continue;
                        }
                        for (int i = 0; i <= GlobalVariable.textFileFieldValues.Length - 1; i++)
                        {
                            GlobalVariable.textFileFieldValues[i] = GlobalVariable.textFileFieldValues[i].Replace("'", "''");
                        }

                        GlobalVariable.textFileFieldValues[Direct_SKU_Index] = FormatDirectSKU(GlobalVariable.textFileFieldValues[Direct_SKU_Index]);
                        GlobalVariable.textFileFieldValues[OD_SKU_NO_Index] = RemoveDecimal(GlobalVariable.textFileFieldValues[OD_SKU_NO_Index]);

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[Direct_SKU_Index])) && GlobalVariable.textFileFieldValues[Direct_SKU_Index].Length > 25)
                        {

                            errorMessage += GetErrorMessage(columns[Direct_SKU_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[Direct_SKU_Index],
                                                                          25);
                            GlobalVariable.textFileFieldValues[Direct_SKU_Index] = GlobalVariable.textFileFieldValues[Direct_SKU_Index].Substring(0, 25);
                        }

                        if (!(string.IsNullOrEmpty(GlobalVariable.textFileFieldValues[OD_SKU_NO_Index])) && GlobalVariable.textFileFieldValues[OD_SKU_NO_Index].Length > 25)
                        {

                            errorMessage += GetErrorMessage(columns[OD_SKU_NO_Index].ToString(),
                                                                          GlobalVariable.textFileFieldValues[OD_SKU_NO_Index],
                                                                          25);

                            GlobalVariable.textFileFieldValues[OD_SKU_NO_Index] = GlobalVariable.textFileFieldValues[OD_SKU_NO_Index].Substring(0, 25);
                        }

                        GlobalVariable.textFileFieldValues[Region_Area_Index] = RemoveDecimal(GlobalVariable.textFileFieldValues[Region_Area_Index]);
                        GlobalVariable.textFileFieldValues[Site_Index] = RemoveDecimal(GlobalVariable.textFileFieldValues[Site_Index]);

                        GlobalVariable.textFileFieldValues[Daily_Count_Index] = RemoveDecimal(GlobalVariable.textFileFieldValues[Daily_Count_Index]);
                        GlobalVariable.textFileFieldValues[Daily_Qty_Index] = RemoveDecimal(GlobalVariable.textFileFieldValues[Daily_Qty_Index]);
                        GlobalVariable.textFileFieldValues[Daily_ValueIndex] = FormatDirectSKU(GlobalVariable.textFileFieldValues[Daily_ValueIndex]);
                        GlobalVariable.textFileFieldValues[Total_CountIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Total_CountIndex]);
                        GlobalVariable.textFileFieldValues[Total_QtyIndex] = RemoveDecimal(GlobalVariable.textFileFieldValues[Total_QtyIndex]);
                        GlobalVariable.textFileFieldValues[Total_ValueIndex] = FormatDirectSKU(GlobalVariable.textFileFieldValues[Total_ValueIndex]);

                        try
                        {                          
                            GlobalVariable.textFileFieldValues[Date_Index] = RemoveDecimal(GlobalVariable.textFileFieldValues[Date_Index].Replace('-', '/'));
                        }
                        catch
                        {
                            errorMessage += columns[Date_Index].ToString() + " has invalid date - " + GlobalVariable.textFileFieldValues[Date_Index] + " ";
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + boFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                            TotalRecordCount += 1;
                            continue;
                        }      

                        //--------------------- Added on 13 Feb 2013 - End ---------------- 

                        dr.ItemArray = GlobalVariable.textFileFieldValues;
                        dr["UpdatedDate"] = Common.GetFileDate(boFileName); // new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                        dt.Rows.Add(dr);

                        //--------------------- Added on 14 Feb 2013 -----------------------
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            errorDr["ErrorDescription"] = errorMessage.Substring(0, errorMessage.Length - 1) + " in file " + boFileName;
                            errorDr["FolderDownloadID"] = GlobalVariable.folderDownloadedID;
                            errorDr["UpdatedDate"] = new DateTime(Convert.ToInt32(GlobalVariable.folderName102.Substring(0, 4)), Convert.ToInt32(GlobalVariable.folderName102.Substring(5, 2)), Convert.ToInt32(GlobalVariable.folderName102.Substring(8, 2)));
                            errorDt.Rows.Add(errorDr);
                        }
                        //--------------------- Added on 14 Feb 2013 -----------------------

                        TotalRecordCount += 1;                     
                    }
                    SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null);
                    oSqlBulkCopy.DestinationTableName = "up_StageBO";
                    oSqlBulkCopy.ColumnMappings.Add("Direct_SKU", "Direct_SKU");
                    oSqlBulkCopy.ColumnMappings.Add("OD_SKU_NO", "OD_SKU_NO");
                    oSqlBulkCopy.ColumnMappings.Add("Daily_Count", "Daily_Count");
                    oSqlBulkCopy.ColumnMappings.Add("Daily_Qty", "Daily_Qty");
                    oSqlBulkCopy.ColumnMappings.Add("Daily_Value", "Daily_Value");
                    oSqlBulkCopy.ColumnMappings.Add("Date", "Date");
                    oSqlBulkCopy.ColumnMappings.Add("Total_Count", "Total_Count");
                    oSqlBulkCopy.ColumnMappings.Add("Total_Value", "Total_Value");
                    oSqlBulkCopy.ColumnMappings.Add("Total_Qty", "Total_Qty");
                    oSqlBulkCopy.ColumnMappings.Add("Region_Area", "Region_Area");
                    oSqlBulkCopy.ColumnMappings.Add("Site", "Site");
                    oSqlBulkCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                    oSqlBulkCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime; ;
                    oSqlBulkCopy.BatchSize = dt.Rows.Count;
                    oSqlBulkCopy.WriteToServer(dt);
                    oSqlBulkCopy.Close();

                    //--------------------- Added on 14 Feb 2013 -----------------------
                    SqlBulkCopy oSqlBulkErrorCopy = new SqlBulkCopy(GlobalVariable.sqlCon, SqlBulkCopyOptions.TableLock, null);
                    oSqlBulkErrorCopy.DestinationTableName = "UP_ErrorBO";

                    oSqlBulkErrorCopy.ColumnMappings.Add("FolderDownloadID", "FolderDownloadID");
                    oSqlBulkErrorCopy.ColumnMappings.Add("UpdatedDate", "UpdatedDate");
                    oSqlBulkErrorCopy.ColumnMappings.Add("ErrorDescription", "ErrorDescription");

                    oSqlBulkErrorCopy.BulkCopyTimeout = GlobalVariable.CommandTimeOutTime;
                    oSqlBulkErrorCopy.BatchSize = errorDt.Rows.Count;

                    oSqlBulkErrorCopy.WriteToServer(errorDt);
                    oSqlBulkErrorCopy.Close();
                    //--------------------- Added on 14 Feb 2013 -----------------------
                }
                Common.UpdateUP_FileProcessed(fileProcessedID, TotalRecordCount, 0);
            }
            catch (Exception ex)
            {
                Common.UpdateUP_SchedulerExecutionStatus(GlobalVariable.dataImportSchedulerID.Value);
            }
            finally
            {
                //File.Delete(itemFilePath);
                //File.Delete(itemFilePath + ".gz");
            }

            /// -----------------------------------------------------
            /// --- Added on 19 Feb 2013 
            /// -----------------------------------------------------
            try
            {
                GlobalVariable.sqlComm.CommandTimeout = GlobalVariable.CommandTimeOutTime;
                GlobalVariable.sqlComm.CommandText = "spUP_ManualFileUpload";
                GlobalVariable.sqlComm.Parameters.Clear();
                GlobalVariable.sqlComm.Parameters.Add("@Action", SqlDbType.VarChar).Value = "InsertFiles";
                GlobalVariable.sqlComm.Parameters.Add("@DateUploaded", SqlDbType.DateTime).Value = Common.GetFileDate(boFileName);
                GlobalVariable.sqlComm.Parameters.Add("@DownloadedFilename", SqlDbType.VarChar).Value = boFileName;
                GlobalVariable.sqlComm.Parameters.Add("@UserID", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalPORecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalReceiptInformationRecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@TotalBORecord", SqlDbType.Int).Value = TotalRecordCount;
                GlobalVariable.sqlComm.Parameters.Add("@TotalVendorRecord", SqlDbType.Int).Value = DBNull.Value;
                GlobalVariable.sqlComm.Parameters.Add("@UploadType", SqlDbType.VarChar).Value = "AUTO";
                GlobalVariable.sqlComm.CommandType = CommandType.StoredProcedure;
                GlobalVariable.sqlComm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                GlobalVariable.sqlComm.CommandType = CommandType.Text;
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("No of Lines:" + TotalRecordCount);
                LogUtility.SaveTraceLogEntry(sbMessage);
            }
            /// -----------------------------------------------------
        }

        private string RemoveDecimal(string val)
        {
            string retVal;
            retVal = RemoveSpace(val);

            if (retVal != null)
            {
                if (retVal.Contains("."))
                {
                    retVal = retVal.Substring(0, retVal.IndexOf("."));                   
                }
                if (retVal.Contains("-"))
                {
                    retVal = '-' + (retVal.Trim('-'));
                }
                if (retVal.Contains("/"))
                {
                    string[] arrDate = retVal.Split('/');
                    retVal = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                }
            }
            if (string.IsNullOrEmpty(retVal))
            {
                retVal = "0";
            }
            return retVal;
        }

        private string FormatDirectSKU(string val)
        {
            string retVal;
            retVal = RemoveSpace(val);
            if (string.IsNullOrEmpty(retVal))
            {
                retVal = "0";
            }
            return retVal;
        }

        private string RemoveSpace(string val)
        {
            string retVal;
            if (!string.IsNullOrEmpty(val))
                retVal = val.Trim(' ');
            else
                retVal = null;
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ColumnName"></param>
        /// <param name="Description"></param>
        /// <param name="ColumnLength"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public string GetErrorMessage(string ColumnName, string Description, int ColumnLength)
        {
            var Message = ColumnName + " length greater than " + ColumnLength + " for " + Description + ",";
            return Message;
        }
    }
}

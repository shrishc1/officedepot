﻿using System;
using System.IO;
using System.IO.Compression;

namespace VendorInPartnership_DataImportScheduler {

    //This class only uncompress .gz files
    public class FileUncrompression {

        string localDestnDir = AppDomain.CurrentDomain.BaseDirectory;

        public static bool UncompressFiles() {
            try {
                DirectoryInfo di = new DirectoryInfo(GlobalVariable.SourceFilePath);
                // Decompress all *.gz files in the directory.
                foreach (FileInfo fi in di.GetFiles("*.gz"))
                    Decompress(fi);

                return true;
            }
            catch (Exception ex) {
                Common.InsertUP_ErrorLog("Unable to un-compress the downloaded files" + DateTime.Now.ToString(), GlobalVariable.dataImportSchedulerID.Value);
                Common.UpdateUP_SchedulerExecutionStatus(GlobalVariable.dataImportSchedulerID.Value);
                return false;
            }
        }
        /// <summary>
        ///  Decompress gz files.
        /// </summary>
        /// <param name="fi">file information for uncompressing</param>
        public static void Decompress(FileInfo fi) {
            try {   // Get the stream of the source file.
                using (FileStream inFile = fi.OpenRead()) {
                    // Get original file extension, for example
                    // "doc" from report.doc.gz.
                    string curFile = fi.FullName;
                    string origName = curFile.Remove(curFile.Length - fi.Extension.Length);

                    //Create the decompressed file.
                    using (FileStream outFile = File.Create(origName)) {

                        
                        using (GZipStream Decompress = new GZipStream(inFile, CompressionMode.Decompress)) {
                            // Copy the decompression stream into the output file.
                            Decompress.CopyTo(outFile);
                            Console.WriteLine("Decompressed: {0}", fi.Name);
                        }
                    }
                }
            }
            catch (Exception ex) {
                Common.UpdateUP_SchedulerExecutionStatus(GlobalVariable.dataImportSchedulerID.Value);
                Common.InsertUP_ErrorLog("Unable to Uncompress file " + fi.Name + " at " + DateTime.Now.ToString() + "\r\n" + ex.Message, GlobalVariable.dataImportSchedulerID.Value);
            }
        }
    }
}
